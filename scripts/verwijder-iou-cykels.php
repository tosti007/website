#!/usr/bin/php -q
<?php

/*
 * Geef aan hoe IOU-balansen versimpeld moeten worden door cykels weg te halen.
 */

ini_set('memory_limit', '1G');

require('script-init.php');

/**
 * @brief Stop een edge in de graaf met flow en capaciteit = bedrag.
 *
 * Zorgt er ook voor dat de duale graaf bijgewerkt is.
 *
 * @param bedrag Capaciteit en flow over de edge. Moet positief zijn.
 */
function nieuwe_edge($van, $naar, $bedrag, $kosten)
{
	global $graaf, $duaal, $knopen;

	// Als we geen capaciteit hebben, kunnen we de edge negeren.
	if ($bedrag == 0) {
		return;
	}

	assert($bedrag > 0);

	$knopen[$van] = true;
	$knopen[$naar] = true;

	// stop de vooruitedge erin
	if (!isset($graaf[$van]))
	{
		$graaf[$van] = array();
	}

	$edge = array();
	$edge['naar'] = $naar;
	$edge['bedrag'] = $bedrag;
	$edge['capaciteit'] = $bedrag;
	$edge['kosten'] = $kosten;
	$edge['flow'] = $bedrag;
	$index = count($graaf[$van]); // houd index bij waar we insert doen, voor de duale graaf
	$graaf[$van][] = $edge;

	// stop de backedge erin
	if (!isset($duaal[$naar]))
	{
		$duaal[$naar] = array();
	}
	if (!isset($duaal[$naar][$van]))
	{
		$duaal[$naar][$van] = array();
	}
	$duaal[$naar][$van][] = $index;
}

/**
 * @brief Geef weer hoe het met een knoop zit in de graaf.
 */
function print_toestand($id)
{
	global $graaf, $duaal;
	foreach ($graaf[$id] as $edge)
	{
		echo sprintf("%s -> %s: %f van %f; kosten %d\n",
				$id,
				$edge['naar'],
				$edge['flow'],
				$edge['capaciteit'],
				$edge['kosten']
		);
	}
	foreach ($duaal[$id] as $van => $indices)
	{
		foreach ($indices as $index)
		{
			$edge = $graaf[$van][$index];
			if ($edge['naar'] != $id)
			{
				echo "!!!inconsistentie: \$graaf[$van][$index] is helemaal niet naar $id!!!\n";
			}
			echo sprintf("%s -> %s: %f van %f; kosten %d\n",
					$van,
					$id,
					$edge['flow'],
					$edge['capaciteit'],
					$edge['kosten']
			);
		}
	}
}

/**
 * @brief Zoek in de graaf een negatieve cykel, gegeven de lijst voorgangers.
 *
 * Input en output zien eruit als lijst `[['back': back, 'van': van, 'naar': naar, 'index': index, 'beschikbaar': beschikbaar]]`
 */
function maak_negatieve_cykel($knoop, $voorgangers)
{
	$gezien = array();
	$pad = array();
	// loop naar achter tot je een knoop twee keer tegenkomt
	while (!isset($gezien[$knoop]))
	{
		$gezien[$knoop] = count($pad);
		$pad[] = $voorgangers[$knoop];
		if ($voorgangers[$knoop]['back'])
		{
			$knoop = $voorgangers[$knoop]['naar'];
		}
		else
		{
			$knoop = $voorgangers[$knoop]['van'];
		}
	}
	// nu hebben we dat $gezien[$knoop] de index in het pad is waar deze knoop eerder was
	// dus als we van $gezien[$knoop] het pad aflopen, hebben we een cykel
	return array_slice($pad, $gezien[$knoop]);
}

function check_geldige_circulatie()
{
	global $graaf, $duaal, $knopen;
	foreach ($knopen as $knoop => $boeitniet)
	{
		$flow = 0;
		if (isset($graaf[$knoop]))
		{
			foreach ($graaf[$knoop] as $edge)
			{
				$flow += $edge['flow'];
			}
		}
		if (isset($duaal[$knoop]))
		{
			foreach ($duaal[$knoop] as $van => $indices)
			{
				foreach ($indices as $index)
				{
					$flow -= $graaf[$van][$index]['flow'];
				}
			}
		}
		if ($flow != 0)
		{
			throw new Exception("$knoop heeft flow $flow, dat is geen goede circulatie!");
		}
	}
}

// Sleur betalingen uit de databaas
$kwerrie = IOUBetalingQuery::table()
	->select('van_contactID', 'naar_contactID', 'bedrag')
//	->limit(100) // voor debuggen
	;

// eerst triviale cykels wegslopen: verrekende schulden
$schulden = array();
foreach ($kwerrie->get() as $row)
{
	$van = $row['van_contactID'];
	$naar = $row['naar_contactID'];
	// blijkbaar is er geen functie float->int die ook correct afrondt...
	$row['bedrag'] *= 100;
	$bedrag = (int) ($row['bedrag'] > 0 ? $row['bedrag'] + 0.5 : $row['bedrag'] - 0.5);

	if (!isset($schulden[$van])) {
		$schulden[$van] = array();
	}
	if (!isset($schulden[$van][$naar])) {
		$schulden[$van][$naar] = 0;
	}
	$schulden[$van][$naar] += $bedrag;
}

// De graaf heeft de vorm:
//  [van: [['naar': naar, 'capaciteit': bedrag, 'kosten': 1, 'flow': bedrag], ...], ...]
// De duaal houdt bij waar de backedges zijn, en heeft de vorm:
//  [naar: [van: [index, ...], ...], ...]
$graaf = array();
$duaal = array();
$knopen = array();
foreach ($schulden as $van => $vanSchulden)
{
	foreach ($vanSchulden as $naar => $bedrag)
	{
		// tel schulden maar een keer: sla over als naar < van
		assert($naar != $van);
		if ($naar < $van)
		{
			continue;
		}

		// houd rekening met schulden die de andere kant opgaan
		if (isset($schulden[$naar]) && isset($schulden[$naar][$van]))
		{
			$bedrag -= $schulden[$naar][$van];
		}

		if ($bedrag < 0)
		{
			nieuwe_edge($naar, $van, -$bedrag, 1);
		}
		else
		{
			nieuwe_edge($van, $naar, $bedrag, 1);
		}
	}
}

// maak de circulatie rond door een bron- en putknoop toe te voegen
$totale_flow = 0;
$totale_balans = 0;
$max_bedrag = 0;
foreach ($knopen as $id => $boeitniet)
{
	$balans = 0;
	if (isset($graaf[$id]))
	{
		foreach ($graaf[$id] as $edge)
		{
			$balans += $edge['bedrag'];
			$totale_balans += $edge['bedrag'];
			$max_bedrag = max($max_bedrag, $edge['bedrag']);
		}
	}
	if (isset($duaal[$id]))
	{
		foreach ($duaal[$id] as $target => $indices)
		{
			foreach ($indices as $index)
			{
				assert($graaf[$target][$index]['naar'] == $id);
				$balans -= $graaf[$target][$index]['bedrag'];
			}
		}
	}

	// meer uitgaand dan inkomend
	if ($balans > 0)
	{
		nieuwe_edge('bron', $id, $balans, 0);
		$totale_flow += $balans;
	}
	else
	{
		nieuwe_edge($id, 'put', -$balans, 0);
	}
}
// zorg ervoor dat we geen schulden gaan vergeven
echo "Totale flow in heel IOU: " . $totale_flow . "\n";
echo "Totale balans in heel IOU: " . $totale_balans . "\n";
echo "Maximale winst: " . ($totale_balans - $totale_flow) . "\n";
nieuwe_edge('put', 'bron', $totale_flow, -100000);

check_geldige_circulatie();

// houd bij hoeveel we al hebben kunnen ontcykelen
$profit = 0;

// we zoeken nu cykels die de flow goedkoper maken
// ivm polynomiale looptijd hebben we een ondergrens aan de capaciteit die we elke iteratie accepteren

for ($ondergrens = $max_bedrag; $ondergrens > 0; $ondergrens = (int)($ondergrens / 2))
{
	echo "Capaciteitsondergrens: " . $ondergrens . "\n";
	do
	{
		$gevonden = false;

		// dit is een variant op het algoritme dat vernoemd is naar Bellman en Ford
		// en soms naar Moore, maar oorspronkelijk door Shimbel is ontdekt
		// het idee is dat je een afstandschatting en een voorganger hebt,
		// de schatting verfijn je net zolang tot het correct *moet* zijn
		// door steeds een andere voorganger in het pad te proberen
		// (is het nog niet correct, kun je achteruit lopen om een cykel te vinden)
		$afstanden = array(); // [knoop => kortste bekende afstand uit 'bron']
		$voorganger = array(); // [knoop => edge waarover deze afstand behaald is]

		// TODO: klopt deze initialisatie wel?
		foreach ($knopen as $knoop => $boeitniet)
		{
			$afstanden[$knoop] = 1;
		}
		$i = 0;
		echo "Shimbel(-Bellman-Ford(-Moore))...\n";
		foreach ($knopen as $knoop => $boeitniet)
		{
			$i++;
			foreach ($graaf as $van => $edges)
			{
				// probeer over deze edge goedkoper af te zijn
				foreach ($edges as $index => $edge)
				{
					$naar = $edge['naar'];

					// update over de vooruitedges
					// we moeten hebben dat de flow nog bijgewerkt kan worden
					assert($edge['flow'] <= $edge['capaciteit']);
					if ($edge['capaciteit'] - $edge['flow'] >= $ondergrens)
					{
						// probeer of we zo inderdaad goedkoper af zijn
						if ($afstanden[$naar] > $afstanden[$van] + $edge['kosten'])
						{
							$afstanden[$naar] = $afstanden[$van] + $edge['kosten'];
							$voorganger[$naar] = array(
								'back' => false,
								'van' => $van,
								'naar' => $naar,
								'index' => $index,
								'beschikbaar' => $edge['capaciteit'] - $edge['flow'],
							);
						}
					}

					// en over de backedges
					assert($edge['flow'] >= 0);
					if ($edge['flow'] >= $ondergrens)
					{
						// probeer of we zo inderdaad goedkoper af zijn
						if ($afstanden[$van] > $afstanden[$naar] - $edge['kosten'])
						{
							$afstanden[$van] = $afstanden[$naar] - $edge['kosten'];
							$voorganger[$van] = array(
								'back' => true,
								'van' => $van,
								'naar' => $naar,
								'index' => $index,
								'beschikbaar' => $edge['flow'],
							);
						}
					}
				}
			}
		}

		echo "negatieve cykel vinden...\n";

		// check op cykels met negatief gewicht
		// TODO: dit is letterlijk overgenomen van de code hierboven...
		foreach ($graaf as $van => $edges)
		{
			// we weten nog niets over deze knoop, dus skip die
			if (!isset($afstanden[$van]))
			{
				continue;
			}
			// is er een negatieve cykel over deze edge?
			foreach ($edges as $index => $edge)
			{
				$naar = $edge['naar'];
				// we weten nog niets over deze knoop, dus skip die
				if (!isset($afstanden[$naar]))
				{
					continue;
				}

				assert($edge['flow'] <= $edge['capaciteit']);
				if ($edge['capaciteit'] - $edge['flow'] >= $ondergrens)
				{
					// probeer of we zo inderdaad goedkoper af zijn
					if ($afstanden[$naar] > $afstanden[$van] + $edge['kosten'])
					{
						$cykel = maak_negatieve_cykel($van, $voorganger);
						$gevonden = true;
						break;
					}
				}

				assert($edge['flow'] >= 0);
				if ($edge['flow'] > $ondergrens)
				{
					// probeer of we zo inderdaad goedkoper af zijn
					if ($afstanden[$van] > $afstanden[$naar] - $edge['kosten'])
					{
						$cykel = maak_negatieve_cykel($naar, $voorganger);
						$gevonden = true;
						break;
					}
				}
			}
			if ($gevonden)
			{
				break;
			}
		}

		if ($gevonden)
		{
			// aanname: er zijn niet veel cykels met capaciteit meer dan een miljoen
			$capaciteit = 100000000;
			foreach ($cykel as $stap => $edge)
			{
				$capaciteit = min($capaciteit, $edge['beschikbaar']);
			}
			foreach ($cykel as $stap => $edge)
			{
				$van = $edge['van'];
				$naar = $edge['naar'];
				$index = $edge['index'];
				if ($edge['back']) {
					$verschil = -$capaciteit;
				} else {
					$verschil = $capaciteit;
				}
				assert($graaf[$van][$index]['naar'] == $naar);
				echo "flow $van -> $naar += $verschil\n";
				$profit -= $verschil * $graaf[$van][$index]['kosten'];
				$graaf[$van][$index]['flow'] += $verschil;
			}
		}
		else
		{
			echo "Niets meer te ontcykelen :(\n";
		}

		check_geldige_circulatie();
		echo "Profit: " . $profit . "\n";
	}
	while ($gevonden);
}

echo "Klaar! Profit: " . $profit . "\n";
