#!/usr/bin/php -q
<?php
/*
 * Als je nog helemaal geen leden in de database hebt, heb je een kip-ei-probleem om iets toe te voegen.
 * Daarom maakt dit script lid nummer 1 aan en genereert een magische code waarmee je kan inloggen.
 */

require_once('script-init.php');

// Kijk of er al een lid bestaat.
$lid = Lid::geef(1);

if (!$lid) {
	$lid = new Lid();
	$lid->setAchternaam("Arceus");
	if (!$lid->valid()) {
		echo "Er waren errors :(\n";
		foreach ($lid->getErrors() as $veld => $error) {
			if ($error)
				echo $veld . ": " . $error . "\n";
		}
		die();
	}
	$lid->opslaan();
	echo "Nieuw lid met id: " . $lid->geefID() . " aangemaakt!\n";
} else {
	echo "We gaan verder met bestaand lid nummero 1...\n";
}

// geef het lid een (onzinnig) mailadres
$lid->setEmail("www@a-eskwadraat.nl");

$login = Wachtwoord::geefOfNieuw($lid->geefID());
if (!$login->valid()) {
	echo "Er waren errors :(\n";
	foreach ($lid->getErrors() as $veld => $error) {
		if ($error)
			echo $veld . ": " . $error . "\n";
	}
	die();
}
$login->sendMagicMail();
$login->opslaan();
