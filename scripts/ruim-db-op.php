<?php
// Gooi foute overervingen weg: als je een row hebt in de Activiteit-tabel maar geen in de ActiviteitInformatie,
// dan gaat nogal wat code stuk.

define('BOEKWEB2', true);
require_once 'script-init.php';
setZeurmodus();

if (!DEBUG)
{
	echo "Dit script gooit allerlei databaasinhoud weg, dat mag niet op live!\n";
	exit(1);
}

/**
 * Verwijder alle databasewaarden die ongeldige overervingen hebben:
 * alle rijen in de tabel $ouder zodanig dat hun overerving $kind is,
 * maar ze geen bijbehorende rij in de $kind-tabel hebben.
 *
 * @param string $ouder De naam van de oudertabel/-klasse.
 * @param string $kind De naam van de kindtabel/-klasse.
 * @param string $idKolom De naam van de primary key.
 *
 * !!!LET OP!!! Al deze strings worden direct in de query geinterpoleerd,
 * dus ze moeten hardcoded zijn en zeker niet voor de gebruiker invulbaar!
 *
 * @return int[] Een array van de verwijderde objecten.
 */
function verwijderOngeldigeOverervingen($ouder, $kind, $idKolom)
{
	global $WSW4DB;

	// Ja, stringinterpolatie in SQL is considered harmful, maar dit zijn hardcoded strings.
	$kwerrie = "SELECT $idKolom FROM $ouder WHERE overerving = '$kind' AND $idKolom NOT IN (SELECT $idKolom FROM $kind);";
	$ids = $WSW4DB->q($kwerrie);

	foreach ($ids as $id)
	{
		$id = $id[$idKolom];
		$WSW4DB->q("DELETE FROM $ouder WHERE $idKolom = %i;", $id);
	}
	return $ids;
}

// TODO: genereer de volgende lijst automatisch ipv op basis van wat we regelmatig foutdoen.
verwijderOngeldigeOverervingen('Activiteit', 'ActiviteitInformatie', 'activiteitID');
verwijderOngeldigeOverervingen('Activiteit', 'ActiviteitHerhaling', 'activiteitID');
verwijderOngeldigeOverervingen('Artikel', 'ColaProduct', 'artikelID');
verwijderOngeldigeOverervingen('Artikel', 'Dictaat', 'artikelID');
verwijderOngeldigeOverervingen('Artikel', 'DibsProduct', 'artikelID');
verwijderOngeldigeOverervingen('Artikel', 'iDealKaartje', 'artikelID');
verwijderOngeldigeOverervingen('Artikel', 'Boek', 'artikelID');
verwijderOngeldigeOverervingen('Artikel', 'AesArtikel', 'artikelID');
