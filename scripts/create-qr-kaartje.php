#!/usr/bin/php -q
<?php
// ./create-qr-kaartje.php <code> <actnr>
// Produceer een kaartje met gegeven code voor de gegeven activiteit
// (handig voor als iemand zijn kaartje kwijt is)

require('script-init.php');

require(LIBDIR . "/phpqrcode/qrlib.php");

$code = $argv[1];
$activiteit = Activiteit::geef($argv[2]);
$kaartjePNG = iDealKaartjeCode::makeQR($code);
$black = imagecolorallocate($kaartjePNG, 0x00, 0x00, 0x00);

imagefttext($kaartjePNG, 16, 0, 5, 		30, 	$black, LIBDIR . "/Consolas.ttf", '1 kaartje ' . $activiteit->getTitel());
imagefttext($kaartjePNG, 28, 0, 10, 	320,	$black, LIBDIR . "/Consolas.ttf", $code);
imagefttext($kaartjePNG, 16, 0, 160, 	305, 	$black, LIBDIR . "/Consolas.ttf", date("y-m-d"));
imagefttext($kaartjePNG, 16, 0, 160, 	325, 	$black, LIBDIR . "/Consolas.ttf", date("H:i:s"));
imagefttext($kaartjePNG, 20, 0, 275, 	315, 	$black, LIBDIR . "/Consolas.ttf", 1 . "/" . 1);

ob_start();
imagepng($kaartjePNG);
$stringdata = ob_get_contents();
ob_end_clean();
echo($stringdata);
