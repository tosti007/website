<?php

define('BOEKWEB2', true);
require_once('script-init.php');

echo "Geef de transacties die opnieuw afgehandeld moet worden, een per regel:\n";
echo "Geef een lege regel om te stoppen.\n";
while ($line = readline('transactieid? '))
{
	$transactie = iDeal::geef($line);
	$voorraad = $transactie->getVoorraad();
	switch ($voorraad->geefID()) {
	case Register::getValue('introKampVoorraad'):
		// Als het een introkampbetaling is zet dat deelnemer betaald heeft
		IntroDeelnemer::idealBetaald($this->getEmailAdres());
		break;
	case LIDMAATSCHAP_VOORRAAD:
		$transactie->getContact()->bijnaLidToLid();
		$lidm = LidmaatschapsBetaling::geef($transactie->getContact());
		$lidm->setIdeal($transactie);
		$lidm->opslaan();
		break;
	default:
		echo "Die kan ik nog niet afhandelen, gelieve dit uit te programmeren!\n";
		continue;
	}
}
