#!/usr/bin/php -q
<?php

require('script-init.php');

$string = $argv[1];

global $WSW4DB;

$ids = $WSW4DB->q("COLUMN SELECT `persoon_contactID` FROM `PersoonBadge`");

$pbs = PersoonBadgeVerzameling::verzamel($ids);

foreach($pbs as $pb) {
	$pers = $pb->getPersoon();
	$badges = json_decode($pb->getBadges());
	foreach($badges as $key => $badge) {
		if(strpos($badge, $string) === 0) {
			unset($badges[array_search($badge,$badges)]);
		}
	}
	$pb->setBadges(json_encode(array_values($badges)));
	$pb->opslaan();
	PersoonBadge::checkBSGPBadge($argv[1], $pers, false);
}
