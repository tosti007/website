#!/usr/bin/php -q
<?php

require('script-init.php');

echo 'Checking controllers:' . "\r\n\r\n";

$path = $WSW . '/Controllers';

$dir = dir($path);
while (($bestand = $dir->read()) !== false)
{
	$naam = $path . '/' . $bestand;
	$input = file_get_contents($naam);

	preg_match_all('/class ([a-zA-Z_]+)[ \r\n\t]+\{(.*)\}/s', $input, $matches, PREG_SET_ORDER);
	foreach ($matches as $match)
	{
		$class = $match[1];
		$content = $match[2];

		preg_match_all('/public function ([a-zA-Z_0-9]+)/s', $content, $function_matches, PREG_SET_ORDER);
		foreach ($function_matches as $fmatch)
		{
			$function = $fmatch[1];

			$line = $class . ':' . $function;

			if (($count =  $BMDB->q('MAYBEVALUE SELECT COUNT(1) AS `t` FROM `benamite` WHERE `special` LIKE %c', $line)) == 0)
				echo $line . ' wordt niet aangeroepen in Benamite.' . "\r\n";
		}
	}
}

echo "\r\n" . 'Checking views:' . "\r\n\r\n";

$path = $WSW;

$dir = dir($path);
while (($bestand = $dir->read()) !== false)
{
	if (!preg_match('/\.php$/s', $bestand))
		continue;

	$naam = $path . '/' . $bestand;
	$input = file_get_contents($naam);

	preg_match_all('/class ([a-zA-Z_]+View)[ \r\n\t]+(?:extends [a-zA-Z_]+[ \r\n\t]+)?\{(.*)\}/s', $input, $matches, PREG_SET_ORDER);
	foreach ($matches as $match)
	{
		$class = $match[1];
		$content = $match[2];

		preg_match_all('/(?:static public|public static) function (?!label|waarde|form|opmerking)([a-zA-Z_0-9]+)/s', $content, $function_matches, PREG_SET_ORDER);
		foreach ($function_matches as $fmatch)
		{
			$function = $fmatch[1];

			$output = array();
			exec('grep -r "' . $class . '::' . $function . '(" ' . $WSW, $output);

			if (count($output) < 1)
				if (preg_match('/self::'.$function.'\(/s', $content))
					echo $class . ':' . $function . ' wordt niet buiten de klasse gebruikt, verander dit in protected.' . "\r\n";
				else
					echo  $class . ':' . $function . ' lijkt niet gebruikt te worden.' . "\r\n";
		}
	}
}
