#!/usr/bin/php -q
<?php

define('OGD_LOGO', 'ogd.jpg');

require('../script-init.php');

require(LIBDIR . "/phpqrcode/qrlib.php");

$ogd = imagecreatefromjpeg(OGD_LOGO);

for ($i = 0; $i < 50; $i++)
{
	$code = ProjectSixKaartje::randomCode();

	$kaartje = new ProjectSixKaartje();
	$kaartje->setCode($code)
			->opslaan();

	$plaatje = QRcode::png($code, false, QR_ECLEVEL_M, 10, 6, false);

	$plaatje2 = imagecreatetruecolor(850, 330);
	imagefill($plaatje2, 0, 0, imagecolorallocate($plaatje2, 0xFF, 0xFF, 0xFF));
	imagecopy($plaatje2, $ogd, 0, 15, 0, 0, 520, 300);
	imagecopy($plaatje2, $plaatje, 520, 0, 0, 0, 330, 330);

	imagejpeg($plaatje2, 'tickets/'.$i.'.jpg');
}
?>
