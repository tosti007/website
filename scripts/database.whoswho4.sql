# $Id: $


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `ActivatieCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActivatieCode` (
  `lid_contactID` int(11) NOT NULL,
  `code` varchar(190) NOT NULL,
  `gewijzigdWanneer` date DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`lid_contactID`),
  UNIQUE KEY `code` (`code`),
  CONSTRAINT `ActivatieCode_Lid` FOREIGN KEY (`lid_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Activiteit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Activiteit` (
  `activiteitID` int(11) NOT NULL AUTO_INCREMENT,
  `overerving` enum('Activiteit','ActiviteitInformatie','ActiviteitHerhaling') NOT NULL DEFAULT 'Activiteit',
  `titel_NL` varchar(255) NOT NULL DEFAULT '',
  `titel_EN` varchar(255) DEFAULT '',
  `werftekst_NL` text,
  `werftekst_EN` text,
  `homepage` varchar(511) DEFAULT NULL,
  `momentBegin` datetime NOT NULL,
  `momentEind` datetime DEFAULT NULL,
  `herhalingsParent_activiteitID` int(11) DEFAULT NULL,
  `locatie_NL` varchar(255) DEFAULT NULL,
  `locatie_EN` varchar(255) DEFAULT NULL,
  `prijs_NL` varchar(255) DEFAULT NULL,
  `prijs_EN` varchar(255) DEFAULT NULL,
  `inschrijfbaar` int(1) NOT NULL DEFAULT '0',
  `datumInschrijvenMax` date DEFAULT NULL,
  `datumUitschrijven` date DEFAULT NULL,
  `maxDeelnemers` int(11) DEFAULT NULL,
  `toegang` enum('PUBLIEK','PRIVE') NOT NULL DEFAULT 'PUBLIEK',
  `actsoort` enum('AES2','UU','LAND','ZUS','.COM','EXT') NOT NULL DEFAULT 'AES2',
  `onderwijs` enum('J','N','B') NOT NULL DEFAULT 'J',
  `aantalComputers` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`activiteitID`),
  KEY `Activiteit_HerhalingsParent` (`herhalingsParent_activiteitID`),
  CONSTRAINT `Activiteit_HerhalingsParent` FOREIGN KEY (`herhalingsParent_activiteitID`) REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ActiviteitCategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActiviteitCategorie` (
  `actCategorieID` int(11) NOT NULL AUTO_INCREMENT,
  `titel_NL` varchar(255) NOT NULL DEFAULT '',
  `titel_EN` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`actCategorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ActiviteitHerhaling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActiviteitHerhaling` (
  `activiteitID` int(11) NOT NULL,
  `parent_activiteitID` int(11) NOT NULL,
  PRIMARY KEY (`activiteitID`),
  KEY `ActiviteitHerhaling_Parent` (`parent_activiteitID`),
  CONSTRAINT `ActiviteitHerhaling_fk` FOREIGN KEY (`activiteitID`) REFERENCES `Activiteit` (`activiteitID`),
  CONSTRAINT `ActiviteitHerhaling_Parent` FOREIGN KEY (`parent_activiteitID`) REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ActiviteitInformatie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActiviteitInformatie` (
  `activiteitID` int(11) NOT NULL,
  `titel_NL` varchar(255) DEFAULT '',
  `titel_EN` varchar(255) DEFAULT '',
  `werftekst_NL` text CHARACTER SET utf8mb4,
  `werftekst_EN` text CHARACTER SET utf8mb4,
  `homepage` varchar(511) DEFAULT NULL,
  `locatie_NL` varchar(255) DEFAULT NULL,
  `locatie_EN` varchar(255) DEFAULT NULL,
  `prijs_NL` varchar(255) DEFAULT NULL,
  `prijs_EN` varchar(255) DEFAULT NULL,
  `inschrijfbaar` int(1) NOT NULL DEFAULT '0',
  `datumInschrijvenMax` date DEFAULT NULL,
  `datumUitschrijven` date DEFAULT NULL,
  `maxDeelnemers` int(11) DEFAULT NULL,
  `actsoort` enum('AES2','UU','LAND','ZUS','.COM','EXT') NOT NULL DEFAULT 'AES2',
  `onderwijs` enum('N','B','J') NOT NULL DEFAULT 'N',
  `aantalComputers` int(11) NOT NULL,
  `stuurMail` int(1) NOT NULL DEFAULT '1',
  `categorie_actCategorieID` int(11) DEFAULT NULL,
  PRIMARY KEY (`activiteitID`),
  KEY `ActiviteitInformatie_Categorie` (`categorie_actCategorieID`),
  CONSTRAINT `ActiviteitInformatie_Categorie` FOREIGN KEY (`categorie_actCategorieID`) REFERENCES `ActiviteitCategorie` (`actCategorieID`),
  CONSTRAINT `ActiviteitInformatie_fk` FOREIGN KEY (`activiteitID`) REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ActiviteitVraag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActiviteitVraag` (
  `activiteit_activiteitID` int(11) NOT NULL,
  `vraagID` int(11) NOT NULL,
  `vraag_NL` varchar(255) NOT NULL DEFAULT '',
  `vraag_EN` varchar(255) DEFAULT '',
  `type` enum('STRING','ENUM') NOT NULL DEFAULT 'STRING',
  `opties` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`activiteit_activiteitID`,`vraagID`),
  CONSTRAINT `ActiviteitVraag_Activiteit` FOREIGN KEY (`activiteit_activiteitID`) REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `AesArtikel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AesArtikel` (
  `artikelID` int(11) NOT NULL,
  `commissie_commissieID` int(11) NOT NULL,
  PRIMARY KEY (`artikelID`),
  KEY `AesArtikel_Commissie` (`commissie_commissieID`),
  CONSTRAINT `AesArtikel_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`),
  CONSTRAINT `AesArtikel_fk` FOREIGN KEY (`artikelID`) REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Artikel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Artikel` (
  `artikelID` int(11) NOT NULL AUTO_INCREMENT,
  `overerving` enum('Artikel','ColaProduct','Dictaat','DibsProduct','iDealKaartje','Boek','AesArtikel') NOT NULL,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `btw` enum('NULL','0','6','21') DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`artikelID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Barcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Barcode` (
  `barcodeID` int(11) NOT NULL AUTO_INCREMENT,
  `voorraad_voorraadID` int(11) NOT NULL,
  `wanneer` datetime NOT NULL,
  `barcode` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`barcodeID`),
  KEY `Barcode_Voorraad` (`voorraad_voorraadID`),
  CONSTRAINT `Barcode_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Bedrijf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bedrijf` (
  `contactID` int(11) NOT NULL,
  `status` enum('AFGEWEZEN','HUIDIG','KOSTENLOOS','OUD','POTENTIEEL','OVERGENOME','OPGEHEVEN','OVERIG') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'POTENTIEEL',
  `type` enum('ICT','CONSULTANCY','FINANCIEEL','ONDERZOEK','OVERIG') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ICT',
  PRIMARY KEY (`contactID`),
  CONSTRAINT `Bedrijf_ibfk_1` FOREIGN KEY (`contactID`) REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `BedrijfStudie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BedrijfStudie` (
  `Studie_studieID` int(11) NOT NULL,
  `Bedrijf_contactID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`Studie_studieID`,`Bedrijf_contactID`),
  KEY `BedrijfStudie_Bedrijf` (`Bedrijf_contactID`),
  CONSTRAINT `BedrijfStudie_Bedrijf` FOREIGN KEY (`Bedrijf_contactID`) REFERENCES `Bedrijf` (`contactID`),
  CONSTRAINT `BedrijfStudie_Studie` FOREIGN KEY (`Studie_studieID`) REFERENCES `Studie` (`studieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Bedrijfsprofiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bedrijfsprofiel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractonderdeel_id` int(11) DEFAULT NULL,
  `datumVerloop` date NOT NULL,
  `inhoud_NL` text COLLATE utf8_unicode_ci,
  `inhoud_EN` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Bedrijfsprofiel_Contractonderdeel` (`contractonderdeel_id`),
  CONSTRAINT `Bedrijfsprofiel_Contractonderdeel` FOREIGN KEY (`contractonderdeel_id`) REFERENCES `Contractonderdeel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Bestelling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bestelling` (
  `bestellingID` int(11) NOT NULL AUTO_INCREMENT,
  `persoon_contactID` int(11) NOT NULL,
  `voorraad_voorraadID` int(11) NOT NULL,
  `datumGeplaatst` date NOT NULL,
  `status` enum('OPEN','GERESERVEERD','GESLOTEN','VERLOPEN') NOT NULL DEFAULT 'OPEN',
  `aantal` int(11) NOT NULL,
  `vervalDatum` date NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bestellingID`),
  KEY `Bestelling_Persoon` (`persoon_contactID`),
  KEY `Bestelling_Voorraad` (`voorraad_voorraadID`),
  CONSTRAINT `Bestelling_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `Bestelling_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `BijnaLidCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BijnaLidCode` (
  `persoon_contactID` int(11) NOT NULL,
  `transactie_id` int(11) DEFAULT NULL,
  `code` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`),
  KEY `BijnaLidCode_Transactie` (`transactie_id`),
  CONSTRAINT `BijnaLidCode_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `BijnaLidCode_Transactie` FOREIGN KEY (`transactie_id`) REFERENCES `IDEALTransactie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Boek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Boek` (
  `artikelID` int(11) NOT NULL,
  `isbn` bigint(11) NOT NULL,
  `auteur` varchar(255) DEFAULT NULL,
  `druk` varchar(255) DEFAULT NULL,
  `uitgever` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`artikelID`),
  CONSTRAINT `Boek_fk` FOREIGN KEY (`artikelID`) REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Boekverkoop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Boekverkoop` (
  `verkoopdag` date NOT NULL,
  `volgnummer` int(11) NOT NULL,
  `opener_contactID` int(11) NOT NULL,
  `open` datetime NOT NULL,
  `sluiter_contactID` int(11) DEFAULT NULL,
  `pin1` decimal(8,2) NOT NULL,
  `pin2` decimal(8,2) NOT NULL,
  `kasverschil` decimal(8,2) NOT NULL,
  `gesloten` datetime DEFAULT NULL,
  `opmerking` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`verkoopdag`,`volgnummer`),
  KEY `Boekverkoop_Opener` (`opener_contactID`),
  KEY `Boekverkoop_Sluiter` (`sluiter_contactID`),
  CONSTRAINT `Boekverkoop_Opener` FOREIGN KEY (`opener_contactID`) REFERENCES `Lid` (`contactID`),
  CONSTRAINT `Boekverkoop_Sluiter` FOREIGN KEY (`sluiter_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Bug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bug` (
  `bugID` int(11) NOT NULL AUTO_INCREMENT,
  `melder_contactID` int(11) DEFAULT NULL,
  `melderEmail` varchar(255) DEFAULT NULL,
  `titel` varchar(255) NOT NULL DEFAULT '',
  `categorie_bugCategorieID` int(11) NOT NULL,
  `status` enum('OPEN','BEZIG','OPGELOST','GECOMMIT','UITGESTELD','GESLOTEN','BOGUS','DUBBEL','WANNAHAVE','FEEDBACK','WONTFIX') NOT NULL DEFAULT 'OPEN',
  `prioriteit` enum('ONBENULLIG','LAAG','GEMIDDELD','HOOG','URGENT') NOT NULL DEFAULT 'ONBENULLIG',
  `niveau` enum('OPEN','INGELOGD','CIE') NOT NULL DEFAULT 'OPEN',
  `moment` datetime NOT NULL,
  `type` varchar(255) DEFAULT '',
  `level` enum('BEGINNER','GEMIDDELD','EXPERT') NOT NULL DEFAULT 'GEMIDDELD',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bugID`),
  KEY `Bug_Melder` (`melder_contactID`),
  KEY `Bug_Categorie` (`categorie_bugCategorieID`),
  CONSTRAINT `Bug_Categorie` FOREIGN KEY (`categorie_bugCategorieID`) REFERENCES `BugCategorie` (`bugCategorieID`),
  CONSTRAINT `Bug_Melder` FOREIGN KEY (`melder_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `BugBericht`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BugBericht` (
  `bugBerichtID` int(11) NOT NULL AUTO_INCREMENT,
  `bug_bugID` int(11) NOT NULL,
  `melder_contactID` int(11) DEFAULT NULL,
  `melderEmail` varchar(255) DEFAULT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `categorie_bugCategorieID` int(11) DEFAULT NULL,
  `status` enum('OPEN','BEZIG','OPGELOST','GECOMMIT','UITGESTELD','GESLOTEN','BOGUS','DUBBEL','WANNAHAVE','FEEDBACK','WONTFIX') NOT NULL DEFAULT 'OPEN',
  `prioriteit` enum('ONBENULLIG','LAAG','GEMIDDELD','HOOG','URGENT') NOT NULL DEFAULT 'ONBENULLIG',
  `niveau` enum('OPEN','INGELOGD','CIE') NOT NULL DEFAULT 'OPEN',
  `bericht` text NOT NULL,
  `moment` datetime NOT NULL,
  `revisie` int(11) DEFAULT NULL,
  `commit` varchar(255) DEFAULT NULL,
  `level` enum('BEGINNER','GEMIDDELD','EXPERT') NOT NULL DEFAULT 'GEMIDDELD',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bugBerichtID`),
  KEY `BugBericht_Bug` (`bug_bugID`),
  KEY `BugBericht_Melder` (`melder_contactID`),
  KEY `BugBericht_Categorie` (`categorie_bugCategorieID`),
  CONSTRAINT `BugBericht_Bug` FOREIGN KEY (`bug_bugID`) REFERENCES `Bug` (`bugID`),
  CONSTRAINT `BugBericht_Categorie` FOREIGN KEY (`categorie_bugCategorieID`) REFERENCES `BugCategorie` (`bugCategorieID`),
  CONSTRAINT `BugBericht_Melder` FOREIGN KEY (`melder_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `BugCategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BugCategorie` (
  `bugCategorieID` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `commissie_commissieID` int(11) NOT NULL,
  `actief` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bugCategorieID`),
  KEY `BugCategorie_Commissie` (`commissie_commissieID`),
  CONSTRAINT `BugCategorie_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `BugToewijzing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BugToewijzing` (
  `bugToewijzingID` int(11) NOT NULL AUTO_INCREMENT,
  `bug_bugID` int(11) NOT NULL,
  `persoon_contactID` int(11) NOT NULL,
  `begin_bugBerichtID` int(11) DEFAULT NULL,
  `eind_bugBerichtID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bugToewijzingID`),
  KEY `BugToewijzing_Bug` (`bug_bugID`),
  KEY `BugToewijzing_Persoon` (`persoon_contactID`),
  KEY `BugToewijzing_Begin` (`begin_bugBerichtID`),
  KEY `BugToewijzing_Eind` (`eind_bugBerichtID`),
  CONSTRAINT `BugToewijzing_Begin` FOREIGN KEY (`begin_bugBerichtID`) REFERENCES `BugBericht` (`bugBerichtID`),
  CONSTRAINT `BugToewijzing_Bug` FOREIGN KEY (`bug_bugID`) REFERENCES `Bug` (`bugID`),
  CONSTRAINT `BugToewijzing_Eind` FOREIGN KEY (`eind_bugBerichtID`) REFERENCES `BugBericht` (`bugBerichtID`),
  CONSTRAINT `BugToewijzing_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `BugVolger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BugVolger` (
  `bug_bugID` int(11) NOT NULL,
  `persoon_contactID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bug_bugID`,`persoon_contactID`),
  KEY `BugVolger_Persoon` (`persoon_contactID`),
  CONSTRAINT `BugVolger_Bug` FOREIGN KEY (`bug_bugID`) REFERENCES `Bug` (`bugID`),
  CONSTRAINT `BugVolger_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CSPReport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CSPReport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingelogdLid_contactID` int(11) DEFAULT NULL,
  `ip` varchar(255) NOT NULL DEFAULT '',
  `documentUri` varchar(255) NOT NULL DEFAULT '',
  `referrer` varchar(255) DEFAULT NULL,
  `blockedUri` varchar(255) NOT NULL DEFAULT '',
  `violatedDirective` varchar(255) NOT NULL DEFAULT '',
  `originalPolicy` varchar(255) NOT NULL DEFAULT '',
  `userAgent` varchar(255) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CSPReport_IngelogdLid` (`ingelogdLid_contactID`),
  CONSTRAINT `CSPReport_IngelogdLid` FOREIGN KEY (`ingelogdLid_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ColaProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ColaProduct` (
  `artikelID` int(11) NOT NULL,
  `volgorde` int(11) NOT NULL,
  PRIMARY KEY (`artikelID`),
  CONSTRAINT `ColaProduct_fk` FOREIGN KEY (`artikelID`) REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Collectie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Collectie` (
  `collectieID` int(11) NOT NULL AUTO_INCREMENT,
  `parentCollectie_collectieID` int(11) DEFAULT NULL,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `omschrijving` varchar(255) DEFAULT NULL,
  `actief` int(1) NOT NULL DEFAULT '1',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`collectieID`),
  KEY `Collectie_ParentCollectie` (`parentCollectie_collectieID`),
  CONSTRAINT `Collectie_ParentCollectie` FOREIGN KEY (`parentCollectie_collectieID`) REFERENCES `Collectie` (`collectieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Commissie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Commissie` (
  `commissieID` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `soort` enum('CIE','GROEP','DISPUUT') NOT NULL DEFAULT 'CIE',
  `categorie_categorieID` int(11) DEFAULT NULL,
  `thema` varchar(255) DEFAULT NULL,
  `omschrijving` varchar(255) DEFAULT NULL,
  `datumBegin` date DEFAULT NULL,
  `datumEind` date DEFAULT NULL,
  `login` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `emailZichtbaar` int(1) NOT NULL DEFAULT '1',
  `homepage` varchar(511) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`commissieID`),
  KEY `Commissie_Categorie` (`categorie_categorieID`),
  CONSTRAINT `Commissie_Categorie` FOREIGN KEY (`categorie_categorieID`) REFERENCES `CommissieCategorie` (`categorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CommissieActiviteit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommissieActiviteit` (
  `commissie_commissieID` int(11) NOT NULL,
  `activiteit_activiteitID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`commissie_commissieID`,`activiteit_activiteitID`),
  KEY `CommissieActiviteit_Activiteit` (`activiteit_activiteitID`),
  CONSTRAINT `CommissieActiviteit_Activiteit` FOREIGN KEY (`activiteit_activiteitID`) REFERENCES `Activiteit` (`activiteitID`),
  CONSTRAINT `CommissieActiviteit_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CommissieCategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommissieCategorie` (
  `categorieID` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`categorieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CommissieFoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommissieFoto` (
  `commissie_commissieID` int(11) NOT NULL,
  `media_mediaID` int(11) NOT NULL,
  `status` enum('OUD','HUIDIG') NOT NULL DEFAULT 'OUD',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`commissie_commissieID`,`media_mediaID`),
  KEY `CommissieFoto_Media` (`media_mediaID`),
  CONSTRAINT `CommissieFoto_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`),
  CONSTRAINT `CommissieFoto_Media` FOREIGN KEY (`media_mediaID`) REFERENCES `Media` (`mediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CommissieLid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommissieLid` (
  `persoon_contactID` int(11) NOT NULL,
  `commissie_commissieID` int(11) NOT NULL,
  `datumBegin` date DEFAULT NULL,
  `datumEind` date DEFAULT NULL,
  `spreuk` varchar(255) DEFAULT NULL,
  `spreukZichtbaar` int(1) NOT NULL DEFAULT '0',
  `functie` enum('OVERIG','VOORZ','SECR','PENNY','PR','SPONS','BESTUUR') NOT NULL DEFAULT 'OVERIG',
  `functieNaam` varchar(255) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`,`commissie_commissieID`),
  KEY `CommissieLid_Commissie` (`commissie_commissieID`),
  CONSTRAINT `CommissieLid_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`),
  CONSTRAINT `CommissieLid_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contact` (
  `contactID` int(11) NOT NULL AUTO_INCREMENT,
  `overerving` enum('Contact','Lid','Persoon','Organisatie','Bedrijf','Leverancier') NOT NULL DEFAULT 'Contact',
  `email` varchar(255) DEFAULT '',
  `homepage` varchar(511) DEFAULT NULL,
  `vakidOpsturen` int(1) NOT NULL DEFAULT '0',
  `aes2rootsOpsturen` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ContactAdres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactAdres` (
  `adresID` int(11) NOT NULL AUTO_INCREMENT,
  `contact_contactID` int(11) NOT NULL,
  `soort` enum('THUIS','OUDERS','WERK','POST','BEZOEK','FACTUUR') NOT NULL DEFAULT 'THUIS',
  `straat1` varchar(255) DEFAULT '',
  `straat2` varchar(255) DEFAULT NULL,
  `huisnummer` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT '',
  `woonplaats` varchar(255) DEFAULT '',
  `land` varchar(255) DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`adresID`),
  KEY `ContactAdres_Contact` (`contact_contactID`),
  CONSTRAINT `ContactAdres_Contact` FOREIGN KEY (`contact_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ContactMailingList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactMailingList` (
  `contact_contactID` int(11) NOT NULL,
  `mailingList_mailingListID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_contactID`,`mailingList_mailingListID`),
  KEY `ContactMailingList_MailingList` (`mailingList_mailingListID`),
  CONSTRAINT `ContactMailingList_Contact` FOREIGN KEY (`contact_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ContactMailingList_MailingList` FOREIGN KEY (`mailingList_mailingListID`) REFERENCES `MailingList` (`mailingListID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ContactPersoon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactPersoon` (
  `persoon_contactID` int(11) NOT NULL,
  `organisatie_contactID` int(11) NOT NULL,
  `type` enum('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS') NOT NULL,
  `Functie` varchar(255) DEFAULT '',
  `opmerking` varchar(255) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  `beginDatum` date DEFAULT NULL,
  `eindDatum` date DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`,`organisatie_contactID`,`type`),
  KEY `ContactPersoon_Organisatie` (`organisatie_contactID`),
  CONSTRAINT `ContactPersoon_Organisatie` FOREIGN KEY (`organisatie_contactID`) REFERENCES `Organisatie` (`contactID`),
  CONSTRAINT `ContactPersoon_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ContactTelnr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactTelnr` (
  `telnrID` int(11) NOT NULL AUTO_INCREMENT,
  `contact_contactID` int(11) NOT NULL,
  `telefoonnummer` varchar(255) NOT NULL DEFAULT '',
  `soort` enum('THUIS','MOBIEL','WERK','OUDERS','RECEPTIE','FAX') NOT NULL DEFAULT 'THUIS',
  `voorkeur` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`telnrID`),
  KEY `ContactTelnr_Contact` (`contact_contactID`),
  CONSTRAINT `ContactTelnr_Contact` FOREIGN KEY (`contact_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Contant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contant` (
  `transactieID` int(11) NOT NULL,
  `kas` enum('HOOFDKAS','COLAKAS') NOT NULL DEFAULT 'HOOFDKAS',
  PRIMARY KEY (`transactieID`),
  CONSTRAINT `Contant_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contract` (
  `contractID` int(11) NOT NULL AUTO_INCREMENT,
  `bedrijf_contactID` int(11) NOT NULL,
  `korting` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('GEFACTUREERD','MISLUKT','OPGESTUURD','RETOUR','VOORSTEL') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'GEFACTUREERD',
  `ingangsdatum` date NOT NULL,
  `opmerking` text COLLATE utf8_unicode_ci,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`contractID`),
  KEY `Contract_Bedrijf` (`bedrijf_contactID`),
  CONSTRAINT `Contract_Bedrijf` FOREIGN KEY (`bedrijf_contactID`) REFERENCES `Bedrijf` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Contractonderdeel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contractonderdeel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_contractID` int(11) NOT NULL,
  `soort` enum('VAKID','VACATURE','MAILING','PROFIEL','LEZING','OVERIG') NOT NULL DEFAULT 'OVERIG',
  `bedrag` int(11) NOT NULL,
  `uitvoerDatum` date DEFAULT NULL,
  `omschrijving` text,
  `uitgevoerd` varchar(255) DEFAULT NULL,
  `verantwoordelijke_contactID` int(11) DEFAULT NULL,
  `gefactureerd` int(11) NOT NULL DEFAULT '0',
  `commissie_commissieID` int(11) DEFAULT NULL,
  `latexString` text,
  `annulering` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Contractonderdeel_Contract` (`contract_contractID`),
  KEY `Contractonderdeel_Verantwoordelijke` (`verantwoordelijke_contactID`),
  KEY `Contractonderdeel_Commissie` (`commissie_commissieID`),
  CONSTRAINT `Contractonderdeel_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`),
  CONSTRAINT `Contractonderdeel_Contract` FOREIGN KEY (`contract_contractID`) REFERENCES `Contract` (`contractID`),
  CONSTRAINT `Contractonderdeel_Verantwoordelijke` FOREIGN KEY (`verantwoordelijke_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Deelnemer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Deelnemer` (
  `persoon_contactID` int(11) NOT NULL,
  `activiteit_activiteitID` int(11) NOT NULL,
  `momentInschrijven` datetime NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`,`activiteit_activiteitID`),
  KEY `Deelnemer_Activiteit` (`activiteit_activiteitID`),
  CONSTRAINT `Deelnemer_Activiteit` FOREIGN KEY (`activiteit_activiteitID`) REFERENCES `Activiteit` (`activiteitID`),
  CONSTRAINT `Deelnemer_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DeelnemerAntwoord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeelnemerAntwoord` (
  `deelnemer_persoon_contactID` int(11) NOT NULL,
  `deelnemer_activiteit_activiteitID` int(11) NOT NULL,
  `vraag_activiteit_activiteitID` int(11) NOT NULL,
  `vraag_vraagID` int(11) NOT NULL,
  `antwoord` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`deelnemer_persoon_contactID`,`deelnemer_activiteit_activiteitID`,`vraag_activiteit_activiteitID`,`vraag_vraagID`),
  KEY `DeelnemerAntwoord_Vraag` (`vraag_activiteit_activiteitID`,`vraag_vraagID`),
  KEY `DeelnemerAntwoord_Deelnemer` (`deelnemer_persoon_contactID`,`deelnemer_activiteit_activiteitID`),
  CONSTRAINT `DeelnemerAntwoord_Deelnemer` FOREIGN KEY (`deelnemer_persoon_contactID`, `deelnemer_activiteit_activiteitID`) REFERENCES `Deelnemer` (`persoon_contactID`, `activiteit_activiteitID`),
  CONSTRAINT `DeelnemerAntwoord_Vraag` FOREIGN KEY (`vraag_activiteit_activiteitID`, `vraag_vraagID`) REFERENCES `ActiviteitVraag` (`activiteit_activiteitID`, `vraagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DibsInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DibsInfo` (
  `persoon_contactID` int(11) NOT NULL,
  `pas_pasID` int(11) DEFAULT NULL,
  `kudos` int(11) NOT NULL,
  `laatste_transactieID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`),
  KEY `DibsInfo_Pas` (`pas_pasID`),
  KEY `DibsInfo_Laatste` (`laatste_transactieID`),
  CONSTRAINT `DibsInfo_Laatste` FOREIGN KEY (`laatste_transactieID`) REFERENCES `DibsTransactie` (`transactieID`),
  CONSTRAINT `DibsInfo_Pas` FOREIGN KEY (`pas_pasID`) REFERENCES `Pas` (`pasID`),
  CONSTRAINT `DibsInfo_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DibsProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DibsProduct` (
  `artikelID` int(11) NOT NULL,
  `kudos` int(11) NOT NULL,
  PRIMARY KEY (`artikelID`),
  CONSTRAINT `DibsProduct_fk` FOREIGN KEY (`artikelID`) REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DibsTransactie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DibsTransactie` (
  `transactieID` int(11) NOT NULL,
  `bron` enum('WWW','TABLET','BVK','IDEAL') NOT NULL DEFAULT 'WWW',
  `pas_pasID` int(11) DEFAULT NULL,
  PRIMARY KEY (`transactieID`),
  KEY `DibsTransactie_Pas` (`pas_pasID`),
  CONSTRAINT `DibsTransactie_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`),
  CONSTRAINT `DibsTransactie_Pas` FOREIGN KEY (`pas_pasID`) REFERENCES `Pas` (`pasID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DibsTransactieColaProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DibsTransactieColaProduct` (
  `transactie_transactieID` int(11) NOT NULL,
  `product_artikelID` int(11) NOT NULL,
  `aantal` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`transactie_transactieID`,`product_artikelID`),
  KEY `DibsTransactieColaProduct_Product` (`product_artikelID`),
  CONSTRAINT `DibsTransactieColaProduct_Product` FOREIGN KEY (`product_artikelID`) REFERENCES `ColaProduct` (`artikelID`),
  CONSTRAINT `DibsTransactieColaProduct_Transactie` FOREIGN KEY (`transactie_transactieID`) REFERENCES `DibsTransactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DibsTransactieDibsProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DibsTransactieDibsProduct` (
  `transactie_transactieID` int(11) NOT NULL,
  `product_artikelID` int(11) NOT NULL,
  `aantal` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`transactie_transactieID`,`product_artikelID`),
  KEY `DibsTransactieDibsProduct_Product` (`product_artikelID`),
  CONSTRAINT `DibsTransactieDibsProduct_Product` FOREIGN KEY (`product_artikelID`) REFERENCES `DibsProduct` (`artikelID`),
  CONSTRAINT `DibsTransactieDibsProduct_Transactie` FOREIGN KEY (`transactie_transactieID`) REFERENCES `DibsTransactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Dictaat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dictaat` (
  `artikelID` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_contactID` int(11) NOT NULL,
  `uitgaveJaar` int(11) NOT NULL,
  `digitaalVerspreiden` int(1) NOT NULL DEFAULT '0',
  `beginGebruik` date NOT NULL,
  `eindeGebruik` date NOT NULL,
  `copyrightOpmerking` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`artikelID`),
  KEY `Dictaat_Auteur` (`auteur_contactID`),
  CONSTRAINT `Dictaat_Auteur` FOREIGN KEY (`auteur_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DictaatOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DictaatOrder` (
  `orderID` int(11) NOT NULL,
  `kaftPrijs` decimal(8,2) NOT NULL,
  `marge` decimal(8,2) NOT NULL,
  `factuurverschil` decimal(8,2) NOT NULL,
  PRIMARY KEY (`orderID`),
  CONSTRAINT `DictaatOrder_fk` FOREIGN KEY (`orderID`) REFERENCES `Order` (`orderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DocuBestand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocuBestand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titel` varchar(255) NOT NULL DEFAULT '',
  `bestand` varchar(255) DEFAULT NULL,
  `extensie` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `categorie_id` int(11) NOT NULL,
  `datum` datetime NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `DocuBestand_Categorie` (`categorie_id`),
  CONSTRAINT `DocuBestand_Categorie` FOREIGN KEY (`categorie_id`) REFERENCES `DocuCategorie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `DocuCategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocuCategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `omschrijving` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Donateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Donateur` (
  `persoon_contactID` int(11) NOT NULL,
  `anderPersoon_contactID` int(11) DEFAULT NULL,
  `aanhef` varchar(255) DEFAULT NULL,
  `jaarBegin` int(11) NOT NULL,
  `jaarEind` int(11) DEFAULT NULL,
  `donatie` decimal(8,2) NOT NULL,
  `machtiging` int(1) NOT NULL,
  `machtigingOud` int(1) NOT NULL DEFAULT '0',
  `almanak` int(1) NOT NULL DEFAULT '0',
  `avNotulen` enum('N','POST','EMAIL') NOT NULL DEFAULT 'N',
  `jaarverslag` int(1) NOT NULL DEFAULT '0',
  `studiereis` int(1) NOT NULL DEFAULT '0',
  `symposium` int(1) NOT NULL DEFAULT '0',
  `vakid` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`),
  KEY `Donateur_AnderPersoon` (`anderPersoon_contactID`),
  CONSTRAINT `Donateur_AnderPersoon` FOREIGN KEY (`anderPersoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `Donateur_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ExtraPoster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExtraPoster` (
  `extraPosterID` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `beschrijving` varchar(255) NOT NULL DEFAULT '',
  `zichtbaar` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`extraPosterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Giro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Giro` (
  `transactieID` int(11) NOT NULL,
  `rekening` varchar(255) DEFAULT NULL,
  `tegenrekening` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`transactieID`),
  CONSTRAINT `Giro_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IDEALIssuer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IDEALIssuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerId` text NOT NULL,
  `issuerName` text NOT NULL,
  `shortlist` int(1) NOT NULL DEFAULT '0',
  `probleem` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IDEALTransactie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IDEALTransactie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactieId` varchar(255) NOT NULL DEFAULT '',
  `status` enum('INACTIVE','OPEN','SUCCESS','FAILURE','CANCELLED','EXPIRED','OPENPOGING2','OPENPOGING3','OPENPOGING4','OPENPOGING5','BANKFAILURE') NOT NULL DEFAULT 'INACTIVE',
  `bedrag` int(11) NOT NULL,
  `titel` varchar(255) NOT NULL DEFAULT '',
  `omschrijving` text NOT NULL,
  `artikelId` varchar(255) NOT NULL DEFAULT '',
  `returnURL` varchar(255) NOT NULL DEFAULT '',
  `klantNaam` varchar(255) DEFAULT '',
  `klantRekening` varchar(255) DEFAULT '',
  `klantStad` varchar(255) DEFAULT '',
  `expire` datetime NOT NULL,
  `entranceCode` varchar(255) NOT NULL DEFAULT '',
  `magOpvragenTijdstip` datetime NOT NULL,
  `opSlot` int(1) NOT NULL DEFAULT '0',
  `emailadres` varchar(255) DEFAULT '',
  `contact_contactID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_contactID` (`contact_contactID`),
  CONSTRAINT `IDEALTransactie_ibfk_1` FOREIGN KEY (`contact_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IDEALv3Issuer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IDEALv3Issuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerId` varchar(255) NOT NULL DEFAULT '',
  `issuerName` varchar(255) NOT NULL DEFAULT '',
  `land` varchar(255) NOT NULL DEFAULT '',
  `probleem` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IOUBetaling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUBetaling` (
  `betalingID` int(11) NOT NULL AUTO_INCREMENT,
  `bon_bonID` int(11) DEFAULT NULL,
  `van_contactID` int(11) NOT NULL,
  `naar_contactID` int(11) NOT NULL,
  `bedrag` decimal(8,2) NOT NULL,
  `omschrijving` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`betalingID`),
  KEY `IOUBetaling_Bon` (`bon_bonID`),
  KEY `IOUBetaling_Van` (`van_contactID`),
  KEY `IOUBetaling_Naar` (`naar_contactID`),
  CONSTRAINT `IOUBetaling_Bon` FOREIGN KEY (`bon_bonID`) REFERENCES `IOUBon` (`bonID`),
  CONSTRAINT `IOUBetaling_Naar` FOREIGN KEY (`naar_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `IOUBetaling_Van` FOREIGN KEY (`van_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IOUBon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUBon` (
  `bonID` int(11) NOT NULL AUTO_INCREMENT,
  `persoon_contactID` int(11) NOT NULL,
  `omschrijving` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bonID`),
  KEY `IOUBon_Persoon` (`persoon_contactID`),
  CONSTRAINT `IOUBon_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IOUSplit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IOUSplit` (
  `bon_bonID` int(11) NOT NULL,
  `persoon_contactID` int(11) NOT NULL,
  `moetBetalen` decimal(8,2) NOT NULL,
  `heeftBetaald` decimal(8,2) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`bon_bonID`,`persoon_contactID`),
  KEY `IOUSplit_Persoon` (`persoon_contactID`),
  CONSTRAINT `IOUSplit_Bon` FOREIGN KEY (`bon_bonID`) REFERENCES `IOUBon` (`bonID`),
  CONSTRAINT `IOUSplit_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Interactie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Interactie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spook_persoon_contactID` int(11) NOT NULL,
  `spook_organisatie_contactID` int(11) NOT NULL,
  `spook_type` enum('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS') CHARACTER SET utf8 NOT NULL DEFAULT 'NORMAAL',
  `bedrijf_contactID` int(11) NOT NULL,
  `bedrijfPersoon_persoon_contactID` int(11) NOT NULL,
  `bedrijfPersoon_organisatie_contactID` int(11) NOT NULL,
  `bedrijfPersoon_type` enum('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS') CHARACTER SET utf8 DEFAULT NULL,
  `commissie_commissieID` int(11) DEFAULT NULL,
  `datum` datetime NOT NULL,
  `inhoud` text CHARACTER SET utf8 NOT NULL,
  `medium` enum('EMAIL','FAX','GESPREK','NOTITIE','ONBEKEND','OVERIG','POST','TELEFOON') CHARACTER SET utf8 NOT NULL DEFAULT 'EMAIL',
  `todo` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Interactie_Spook` (`spook_persoon_contactID`,`spook_organisatie_contactID`,`spook_type`),
  KEY `Interactie_Bedrijf` (`bedrijf_contactID`),
  KEY `Interactie_BedrijfPersoon` (`bedrijfPersoon_persoon_contactID`,`bedrijfPersoon_organisatie_contactID`,`bedrijfPersoon_type`),
  KEY `Interactie_Commissie` (`commissie_commissieID`),
  CONSTRAINT `Interactie_Bedrijf` FOREIGN KEY (`bedrijf_contactID`) REFERENCES `Bedrijf` (`contactID`),
  CONSTRAINT `Interactie_BedrijfPersoon` FOREIGN KEY (`bedrijfPersoon_persoon_contactID`, `bedrijfPersoon_organisatie_contactID`, `bedrijfPersoon_type`) REFERENCES `ContactPersoon` (`persoon_contactID`, `organisatie_contactID`, `type`),
  CONSTRAINT `Interactie_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`),
  CONSTRAINT `Interactie_Spook` FOREIGN KEY (`spook_persoon_contactID`, `spook_organisatie_contactID`, `spook_type`) REFERENCES `ContactPersoon` (`persoon_contactID`, `organisatie_contactID`, `type`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IntroCluster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IntroCluster` (
  `clusterID` int(11) NOT NULL AUTO_INCREMENT,
  `jaar` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `persoon_contactID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`clusterID`),
  KEY `IntroCluster_Persoon` (`persoon_contactID`),
  CONSTRAINT `IntroCluster_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IntroDeelnemer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IntroDeelnemer` (
  `lid_contactID` int(11) NOT NULL,
  `betaald` enum('NEE','BANK','PIN','KAS','IDEAL','ONBEKEND') NOT NULL DEFAULT 'NEE',
  `kamp` int(1) NOT NULL DEFAULT '0',
  `vega` int(1) NOT NULL DEFAULT '0',
  `allergie` int(1) NOT NULL DEFAULT '0',
  `aanwezig` int(1) NOT NULL DEFAULT '0',
  `allergieen` text,
  `introOpmerkingen` text,
  `voedselBeperking` text,
  `magischeCode` varchar(196) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`lid_contactID`),
  UNIQUE KEY `magischeCode` (`magischeCode`),
  CONSTRAINT `IntroDeelnemer_Lid` FOREIGN KEY (`lid_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `IntroGroep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IntroGroep` (
  `groepID` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_clusterID` int(11) DEFAULT NULL,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`groepID`),
  KEY `IntroGroep_Cluster` (`cluster_clusterID`),
  CONSTRAINT `IntroGroep_Cluster` FOREIGN KEY (`cluster_clusterID`) REFERENCES `IntroCluster` (`clusterID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Jaargang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Jaargang` (
  `jaargangID` int(11) NOT NULL AUTO_INCREMENT,
  `vak_vakID` int(11) NOT NULL,
  `jaar` enum('0','1') NOT NULL DEFAULT '0',
  `contactPersoon_contactID` int(11) NOT NULL,
  `datumBegin` date NOT NULL,
  `datumEinde` date NOT NULL,
  `inschrijvingen` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`jaargangID`),
  KEY `Jaargang_Vak` (`vak_vakID`),
  KEY `Jaargang_ContactPersoon` (`contactPersoon_contactID`),
  CONSTRAINT `Jaargang_ContactPersoon` FOREIGN KEY (`contactPersoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `Jaargang_Vak` FOREIGN KEY (`vak_vakID`) REFERENCES `Vak` (`vakID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `JaargangArtikel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JaargangArtikel` (
  `jaargang_jaargangID` int(11) NOT NULL,
  `artikel_artikelID` int(11) NOT NULL,
  `schattingNodig` int(11) DEFAULT NULL,
  `opmerking` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`jaargang_jaargangID`,`artikel_artikelID`),
  KEY `JaargangArtikel_Artikel` (`artikel_artikelID`),
  CONSTRAINT `JaargangArtikel_Artikel` FOREIGN KEY (`artikel_artikelID`) REFERENCES `Artikel` (`artikelID`),
  CONSTRAINT `JaargangArtikel_Jaargang` FOREIGN KEY (`jaargang_jaargangID`) REFERENCES `Jaargang` (`jaargangID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `KartVoorwerp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KartVoorwerp` (
  `kartVoorwerpID` int(11) NOT NULL AUTO_INCREMENT,
  `persoonKart_persoon_contactID` int(11) NOT NULL,
  `persoonKart_naam` varchar(255) NOT NULL DEFAULT '',
  `persoon_contactID` int(11) DEFAULT NULL,
  `commissie_commissieID` int(11) DEFAULT NULL,
  `media_mediaID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`kartVoorwerpID`),
  KEY `KartVoorwerp_PersoonKart` (`persoonKart_persoon_contactID`,`persoonKart_naam`),
  KEY `KartVoorwerp_Persoon` (`persoon_contactID`),
  KEY `KartVoorwerp_Commissie` (`commissie_commissieID`),
  KEY `KartVoorwerp_Media` (`media_mediaID`),
  CONSTRAINT `KartVoorwerp_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `KartVoorwerp_Media` FOREIGN KEY (`media_mediaID`) REFERENCES `Media` (`mediaID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `KartVoorwerp_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `KartVoorwerp_PersoonKart` FOREIGN KEY (`persoonKart_persoon_contactID`, `persoonKart_naam`) REFERENCES `PersoonKart` (`persoon_contactID`, `naam`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Kudos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kudos` (
  `transactieID` int(11) NOT NULL,
  `pas_pasID` int(11) DEFAULT NULL,
  PRIMARY KEY (`transactieID`),
  KEY `Kudos_Pas` (`pas_pasID`),
  CONSTRAINT `Kudos_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`),
  CONSTRAINT `Kudos_Pas` FOREIGN KEY (`pas_pasID`) REFERENCES `Pas` (`pasID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Leverancier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Leverancier` (
  `contactID` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `dictaten` int(1) NOT NULL DEFAULT '0',
  `dictatenIntern` int(1) NOT NULL DEFAULT '0',
  `dibsproducten` int(1) NOT NULL DEFAULT '0',
  `colaproducten` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contactID`),
  CONSTRAINT `Leverancier_fk` FOREIGN KEY (`contactID`) REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `LeverancierRetour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LeverancierRetour` (
  `voorraadMutatieID` int(11) NOT NULL,
  `order_orderID` int(11) DEFAULT NULL,
  `leverancier_contactID` int(11) NOT NULL,
  PRIMARY KEY (`voorraadMutatieID`),
  KEY `LeverancierRetour_Order` (`order_orderID`),
  KEY `LeverancierRetour_Leverancier` (`leverancier_contactID`),
  CONSTRAINT `LeverancierRetour_fk` FOREIGN KEY (`voorraadMutatieID`) REFERENCES `VoorraadMutatie` (`voorraadMutatieID`),
  CONSTRAINT `LeverancierRetour_Leverancier` FOREIGN KEY (`leverancier_contactID`) REFERENCES `Leverancier` (`contactID`),
  CONSTRAINT `LeverancierRetour_Order` FOREIGN KEY (`order_orderID`) REFERENCES `Order` (`orderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Levering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Levering` (
  `voorraadMutatieID` int(11) NOT NULL,
  `order_orderID` int(11) DEFAULT NULL,
  `leverancier_contactID` int(11) NOT NULL,
  PRIMARY KEY (`voorraadMutatieID`),
  KEY `Levering_Order` (`order_orderID`),
  KEY `Levering_Leverancier` (`leverancier_contactID`),
  CONSTRAINT `Levering_fk` FOREIGN KEY (`voorraadMutatieID`) REFERENCES `VoorraadMutatie` (`voorraadMutatieID`),
  CONSTRAINT `Levering_Leverancier` FOREIGN KEY (`leverancier_contactID`) REFERENCES `Leverancier` (`contactID`),
  CONSTRAINT `Levering_Order` FOREIGN KEY (`order_orderID`) REFERENCES `Order` (`orderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Lid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lid` (
  `contactID` int(11) NOT NULL,
  `lidToestand` enum('BIJNALID','LID','OUDLID','BAL','LIDVANVERDIENSTE','ERELID','GESCHORST','BEESTJE','INGESCHREVEN') NOT NULL DEFAULT 'BIJNALID',
  `lidMaster` int(1) NOT NULL DEFAULT '0',
  `lidExchange` int(1) NOT NULL DEFAULT '0',
  `lidVan` date DEFAULT NULL,
  `lidTot` date DEFAULT NULL,
  `studentnr` varchar(255) DEFAULT NULL,
  `opzoekbaar` enum('A','J','N','CIE') NOT NULL DEFAULT 'A',
  `inAlmanak` int(1) NOT NULL DEFAULT '0',
  `planetRSS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contactID`),
  CONSTRAINT `Lid_fk` FOREIGN KEY (`contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `LidStudie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LidStudie` (
  `lidStudieID` int(11) NOT NULL AUTO_INCREMENT,
  `studie_studieID` int(11) NOT NULL,
  `lid_contactID` int(11) NOT NULL,
  `datumBegin` date NOT NULL,
  `datumEind` date DEFAULT NULL,
  `status` enum('STUDEREND','ALUMNUS','GESTOPT') NOT NULL DEFAULT 'STUDEREND',
  `groep_groepID` int(11) DEFAULT NULL,
  `groepID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`lidStudieID`),
  KEY `LidStudie_Lid` (`lid_contactID`),
  KEY `LidStudie_Studie` (`studie_studieID`),
  KEY `LidStudie_Groep` (`groep_groepID`),
  CONSTRAINT `LidStudie_Groep` FOREIGN KEY (`groep_groepID`) REFERENCES `IntroGroep` (`groepID`),
  CONSTRAINT `LidStudie_Lid` FOREIGN KEY (`lid_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LidStudie_Studie` FOREIGN KEY (`studie_studieID`) REFERENCES `Studie` (`studieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `LidmaatschapsBetaling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LidmaatschapsBetaling` (
  `lid_contactID` int(11) NOT NULL,
  `soort` enum('BOEKVERKOOP','MACHTIGING','IDEAL') NOT NULL DEFAULT 'BOEKVERKOOP',
  `rekeningnummer` varchar(255) DEFAULT NULL,
  `rekeninghouder` varchar(255) DEFAULT NULL,
  `ideal_transactieID` int(11) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`lid_contactID`),
  KEY `LidmaatschapsBetaling_Ideal` (`ideal_transactieID`),
  CONSTRAINT `LidmaatschapsBetaling_Ideal` FOREIGN KEY (`ideal_transactieID`) REFERENCES `iDeal` (`transactieID`),
  CONSTRAINT `LidmaatschapsBetaling_Lid` FOREIGN KEY (`lid_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Mailing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mailing` (
  `mailingID` int(11) NOT NULL AUTO_INCREMENT,
  `commissie_commissieID` int(11) DEFAULT NULL,
  `omschrijving` varchar(255) NOT NULL DEFAULT '',
  `momentIngepland` datetime DEFAULT NULL,
  `momentVerwerkt` datetime DEFAULT NULL,
  `mailHeaders` text,
  `mailSubject` varchar(255) NOT NULL DEFAULT '',
  `mailFrom` varchar(255) NOT NULL DEFAULT '',
  `isDefinitief` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mailingID`),
  KEY `Mailing_Commissie` (`commissie_commissieID`),
  CONSTRAINT `Mailing_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `MailingBounce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailingBounce` (
  `mailingBounceID` int(11) NOT NULL AUTO_INCREMENT,
  `mailing_mailingID` int(11) DEFAULT NULL,
  `contact_contactID` int(11) NOT NULL,
  `bounceContent` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mailingBounceID`),
  KEY `MailingBounce_Mailing` (`mailing_mailingID`),
  KEY `MailingBounce_Contact` (`contact_contactID`),
  CONSTRAINT `MailingBounce_Contact` FOREIGN KEY (`contact_contactID`) REFERENCES `Contact` (`contactID`),
  CONSTRAINT `MailingBounce_Mailing` FOREIGN KEY (`mailing_mailingID`) REFERENCES `Mailing` (`mailingID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `MailingList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailingList` (
  `mailingListID` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `omschrijving` text NOT NULL,
  `subjectPrefix` varchar(255) DEFAULT NULL,
  `bodyPrefix` text,
  `bodySuffix` text,
  `verborgen` int(1) NOT NULL,
  `query` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mailingListID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `MailingTemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailingTemplate` (
  `mailingTemplateID` int(11) NOT NULL AUTO_INCREMENT,
  `omschrijving` text NOT NULL,
  `standaardPreviewers` varchar(255) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mailingTemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Mailing_MailingList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mailing_MailingList` (
  `mailing_mailingID` int(11) NOT NULL,
  `mailingList_mailingListID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mailing_mailingID`,`mailingList_mailingListID`),
  KEY `Mailing_MailingList_MailingList` (`mailingList_mailingListID`),
  CONSTRAINT `Mailing_MailingList_Mailing` FOREIGN KEY (`mailing_mailingID`) REFERENCES `Mailing` (`mailingID`),
  CONSTRAINT `Mailing_MailingList_MailingList` FOREIGN KEY (`mailingList_mailingListID`) REFERENCES `MailingList` (`mailingListID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Mededeling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mededeling` (
  `mededelingID` int(11) NOT NULL AUTO_INCREMENT,
  `commissie_commissieID` int(11) DEFAULT NULL,
  `doelgroep` enum('ALLEN','LEDEN','ACTIEF') NOT NULL DEFAULT 'ALLEN',
  `prioriteit` enum('LAAG','NORMAAL','HOOG') NOT NULL DEFAULT 'LAAG',
  `datumBegin` date NOT NULL,
  `datumEind` date NOT NULL,
  `url` varchar(511) DEFAULT NULL,
  `omschrijving_NL` varchar(255) DEFAULT '',
  `omschrijving_EN` varchar(255) DEFAULT '',
  `mededeling_NL` text,
  `mededeling_EN` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mededelingID`),
  KEY `Mededeling_Commissie` (`commissie_commissieID`),
  CONSTRAINT `Mededeling_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Media` (
  `mediaID` int(11) NOT NULL AUTO_INCREMENT,
  `activiteit_activiteitID` int(11) DEFAULT NULL,
  `uploader_contactID` int(11) DEFAULT NULL,
  `fotograaf_contactID` int(11) DEFAULT NULL,
  `soort` enum('FOTO','FILM') NOT NULL DEFAULT 'FOTO',
  `gemaakt` datetime DEFAULT NULL,
  `licence` enum('CC3.0-BY-NC-SA','CC3.0-BY-NC-SA-AES2','CC3.0-BY-NC','CC3.0-BY-ND-AES2') NOT NULL DEFAULT 'CC3.0-BY-NC-SA',
  `premium` int(1) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL,
  `lock` int(1) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '50',
  `omschrijving` text,
  `breedte` int(11) NOT NULL,
  `hoogte` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mediaID`),
  KEY `Media_Activiteit` (`activiteit_activiteitID`),
  KEY `Media_Uploader` (`uploader_contactID`),
  KEY `Media_Fotograaf` (`fotograaf_contactID`),
  CONSTRAINT `Media_Activiteit` FOREIGN KEY (`activiteit_activiteitID`) REFERENCES `Activiteit` (`activiteitID`),
  CONSTRAINT `Media_Fotograaf` FOREIGN KEY (`fotograaf_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `Media_Uploader` FOREIGN KEY (`uploader_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Mentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mentor` (
  `lid_contactID` int(11) NOT NULL,
  `groep_groepID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`lid_contactID`,`groep_groepID`),
  KEY `Mentor_Groep` (`groep_groepID`),
  CONSTRAINT `Mentor_Groep` FOREIGN KEY (`groep_groepID`) REFERENCES `IntroGroep` (`groepID`),
  CONSTRAINT `Mentor_Lid` FOREIGN KEY (`lid_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `MixitupKaartje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MixitupKaartje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `idealtransactie_id` int(11) DEFAULT NULL,
  `gescand` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `MixitupKaartje_Idealtransactie` (`idealtransactie_id`),
  CONSTRAINT `MixitupKaartje_Idealtransactie` FOREIGN KEY (`idealtransactie_id`) REFERENCES `IDEALTransactie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Offerte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Offerte` (
  `offerteID` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_artikelID` int(11) NOT NULL,
  `leverancier_contactID` int(11) NOT NULL,
  `waardePerStuk` decimal(8,2) NOT NULL,
  `vanafDatum` date NOT NULL,
  `vervalDatum` date NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`offerteID`),
  KEY `Offerte_Artikel` (`artikel_artikelID`),
  KEY `Offerte_Leverancier` (`leverancier_contactID`),
  CONSTRAINT `Offerte_Artikel` FOREIGN KEY (`artikel_artikelID`) REFERENCES `Artikel` (`artikelID`),
  CONSTRAINT `Offerte_Leverancier` FOREIGN KEY (`leverancier_contactID`) REFERENCES `Leverancier` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Order` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_artikelID` int(11) NOT NULL,
  `leverancier_contactID` int(11) NOT NULL,
  `waardePerStuk` decimal(8,2) DEFAULT NULL,
  `datumGeplaatst` date NOT NULL,
  `datumBinnen` date DEFAULT NULL,
  `geannuleerd` tinyint(1) NOT NULL DEFAULT '0',
  `aantal` int(11) NOT NULL,
  `waarde` decimal(8,2) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  `overerving` enum('Order','DictaatOrder') NOT NULL DEFAULT 'Order',
  PRIMARY KEY (`orderID`),
  KEY `Order_Artikel` (`artikel_artikelID`),
  KEY `Order_Leverancier` (`leverancier_contactID`),
  CONSTRAINT `Order_Artikel` FOREIGN KEY (`artikel_artikelID`) REFERENCES `Artikel` (`artikelID`),
  CONSTRAINT `Order_Leverancier` FOREIGN KEY (`leverancier_contactID`) REFERENCES `Leverancier` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Organisatie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Organisatie` (
  `contactID` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `soort` enum('ZUS_UU','ZUS_NL','OG_MED','UU','VAKGROEP','BEDRIJF','OVERIG') NOT NULL DEFAULT 'ZUS_UU',
  `omschrijving` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contactID`),
  CONSTRAINT `Organisatie_fk` FOREIGN KEY (`contactID`) REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `OuderdagInschrijving`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OuderdagInschrijving` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) NOT NULL DEFAULT '',
  `naamStudent` varchar(255) NOT NULL DEFAULT '',
  `emailadres` varchar(255) NOT NULL DEFAULT '',
  `programma` varchar(255) NOT NULL DEFAULT '',
  `aantal` int(11) NOT NULL,
  `idealId` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PapierMolen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PapierMolen` (
  `papierMolenID` int(11) NOT NULL AUTO_INCREMENT,
  `geplaatst` datetime NOT NULL,
  `lid_contactID` int(11) NOT NULL,
  `ean` bigint(11) NOT NULL,
  `auteur` varchar(255) DEFAULT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `druk` varchar(255) DEFAULT NULL,
  `prijs` decimal(8,2) NOT NULL,
  `opmerking` varchar(255) DEFAULT NULL,
  `studie` enum('WI','NA','IC','GT','IK') NOT NULL DEFAULT 'WI',
  `verloopdatum` date NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`papierMolenID`),
  KEY `PapierMolen_Lid` (`lid_contactID`),
  CONSTRAINT `PapierMolen_Lid` FOREIGN KEY (`lid_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Pas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pas` (
  `pasID` int(11) NOT NULL AUTO_INCREMENT,
  `rfidTag` varchar(255) NOT NULL DEFAULT '',
  `persoon_contactID` int(11) NOT NULL,
  `actief` int(1) NOT NULL DEFAULT '1',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`pasID`),
  KEY `Pas_Persoon` (`persoon_contactID`),
  CONSTRAINT `Pas_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Periode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Periode` (
  `periodeID` int(11) NOT NULL AUTO_INCREMENT,
  `datumBegin` date NOT NULL,
  `collegejaar` int(11) NOT NULL,
  `periodeNummer` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`periodeID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Persoon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Persoon` (
  `contactID` int(11) NOT NULL,
  `voornaam` varchar(255) DEFAULT NULL,
  `bijnaam` varchar(255) DEFAULT NULL,
  `voorletters` varchar(255) DEFAULT '',
  `geboortenamen` varchar(255) DEFAULT NULL,
  `tussenvoegsels` varchar(255) DEFAULT NULL,
  `achternaam` varchar(255) NOT NULL DEFAULT '',
  `titelsPrefix` varchar(255) DEFAULT NULL,
  `titelsPostfix` varchar(255) DEFAULT NULL,
  `geslacht` enum('?','M','V') DEFAULT NULL,
  `datumGeboorte` date DEFAULT NULL,
  `overleden` int(1) NOT NULL DEFAULT '0',
  `GPGkey` varchar(255) DEFAULT NULL,
  `Rekeningnummer` varchar(255) DEFAULT NULL,
  `taalvoorkeur` enum('NL','EN','GEEN') NOT NULL DEFAULT 'GEEN',
  `opmerkingen` text,
  `voornaamwoord_woordID` int(11) NOT NULL,
  `voornaamwoordZichtbaar` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`contactID`),
  KEY `Persoon_Voornaamwoord` (`voornaamwoord_woordID`),
  CONSTRAINT `Persoon_fk` FOREIGN KEY (`contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Persoon_Voornaamwoord` FOREIGN KEY (`voornaamwoord_woordID`) REFERENCES `Voornaamwoord` (`woordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PersoonBadge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersoonBadge` (
  `persoon_contactID` int(11) NOT NULL,
  `badges` text NOT NULL,
  `last` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`),
  CONSTRAINT `PersoonBadge_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PersoonIM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersoonIM` (
  `persoon_contactID` int(11) NOT NULL,
  `soort` enum('ICQ','MSN','JABBER','AOL','YAHOO!') NOT NULL,
  `accountName` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`,`soort`),
  CONSTRAINT `PersoonIM_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PersoonKart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersoonKart` (
  `persoon_contactID` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`,`naam`),
  CONSTRAINT `PersoonKart_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PersoonRating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersoonRating` (
  `ratingObject_ratingObjectID` int(11) NOT NULL,
  `persoon_contactID` int(11) NOT NULL,
  `rating` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT '1',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`ratingObject_ratingObjectID`,`persoon_contactID`),
  KEY `PersoonRating_Persoon` (`persoon_contactID`),
  CONSTRAINT `PersoonRating_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `PersoonRating_RatingObject` FOREIGN KEY (`ratingObject_ratingObjectID`) REFERENCES `RatingObject` (`ratingObjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PersoonVoorkeur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersoonVoorkeur` (
  `persoon_contactID` int(11) NOT NULL,
  `taal` enum('GEEN','NL','EN') NOT NULL DEFAULT 'GEEN',
  `bugSortering` enum('ASC','DESC') NOT NULL DEFAULT 'ASC',
  `favoBugCategorie_bugCategorieID` int(11) DEFAULT NULL,
  `tourAfgemaakt` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`),
  KEY `PersoonVoorkeur_FavoBugCategorie` (`favoBugCategorie_bugCategorieID`),
  CONSTRAINT `PersoonVoorkeur_FavoBugCategorie` FOREIGN KEY (`favoBugCategorie_bugCategorieID`) REFERENCES `BugCategorie` (`bugCategorieID`) ON DELETE SET NULL,
  CONSTRAINT `PersoonVoorkeur_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Pin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pin` (
  `transactieID` int(11) NOT NULL,
  `rekening` enum('507122690') NOT NULL DEFAULT '507122690',
  `apparaat` enum('SEPAY','XENTISSIMO') NOT NULL DEFAULT 'SEPAY',
  PRIMARY KEY (`transactieID`),
  CONSTRAINT `Pin_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Planner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Planner` (
  `plannerID` int(11) NOT NULL AUTO_INCREMENT,
  `persoon_contactID` int(11) NOT NULL,
  `titel` varchar(255) NOT NULL DEFAULT '',
  `omschrijving` text NOT NULL,
  `mail` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`plannerID`),
  KEY `Planner_Persoon` (`persoon_contactID`),
  CONSTRAINT `Planner_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PlannerData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlannerData` (
  `plannerDataID` int(11) NOT NULL AUTO_INCREMENT,
  `planner_plannerID` int(11) NOT NULL,
  `momentBegin` datetime NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`plannerDataID`),
  UNIQUE KEY `planner_plannerID` (`planner_plannerID`,`momentBegin`),
  CONSTRAINT `PlannerData_Planner` FOREIGN KEY (`planner_plannerID`) REFERENCES `Planner` (`plannerID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `PlannerDeelnemer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlannerDeelnemer` (
  `plannerData_plannerDataID` int(11) NOT NULL,
  `persoon_contactID` int(11) NOT NULL,
  `antwoord` enum('ONBEKEND','LIEVER_NIET','NEE','JA') NOT NULL DEFAULT 'ONBEKEND',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`plannerData_plannerDataID`,`persoon_contactID`),
  KEY `PlannerDeelnemer_Persoon` (`persoon_contactID`),
  CONSTRAINT `PlannerDeelnemer_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `PlannerDeelnemer_PlannerData` FOREIGN KEY (`plannerData_plannerDataID`) REFERENCES `PlannerData` (`plannerDataID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ProfielFoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProfielFoto` (
  `persoon_contactID` int(11) NOT NULL,
  `media_mediaID` int(11) NOT NULL,
  `status` enum('OUD','HUIDIG','NIEUW') NOT NULL DEFAULT 'OUD',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`,`media_mediaID`),
  KEY `ProfielFoto_Media` (`media_mediaID`),
  CONSTRAINT `ProfielFoto_Media` FOREIGN KEY (`media_mediaID`) REFERENCES `Media` (`mediaID`) ON DELETE CASCADE,
  CONSTRAINT `ProfielFoto_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rating` (
  `media_mediaID` int(11) NOT NULL,
  `persoon_contactID` int(11) NOT NULL,
  `rating` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT '1',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_mediaID`,`persoon_contactID`),
  KEY `Rating_Persoon` (`persoon_contactID`),
  KEY `Rating_Media` (`media_mediaID`),
  CONSTRAINT `Rating_Media` FOREIGN KEY (`media_mediaID`) REFERENCES `Media` (`mediaID`),
  CONSTRAINT `Rating_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `RatingObject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RatingObject` (
  `ratingObjectID` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) NOT NULL DEFAULT '50',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`ratingObjectID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL DEFAULT '',
  `beschrijving` varchar(255) NOT NULL DEFAULT '',
  `waarde` varchar(255) NOT NULL DEFAULT '',
  `type` enum('STRING','INT','BOOLEAN','FLOAT') NOT NULL DEFAULT 'STRING',
  `auth` enum('BESTUUR','GOD','INGELOGD') NOT NULL DEFAULT 'BESTUUR',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Reservering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reservering` (
  `reserveringID` int(11) NOT NULL AUTO_INCREMENT,
  `persoon_contactID` int(11) NOT NULL,
  `artikel_artikelID` int(11) NOT NULL,
  `datumGeplaatst` date NOT NULL,
  `voorraad_voorraadID` int(11) DEFAULT NULL,
  `status` enum('OPEN','CLOSED','EXPIRED') NOT NULL DEFAULT 'OPEN',
  `aantal` int(11) NOT NULL,
  `vervalDatum` date NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`reserveringID`),
  KEY `Reservering_Persoon` (`persoon_contactID`),
  KEY `Reservering_Artikel` (`artikel_artikelID`),
  KEY `Reservering_Voorraad` (`voorraad_voorraadID`),
  CONSTRAINT `Reservering_Artikel` FOREIGN KEY (`artikel_artikelID`) REFERENCES `Artikel` (`artikelID`),
  CONSTRAINT `Reservering_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`),
  CONSTRAINT `Reservering_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Schuld`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Schuld` (
  `transactieID` int(11) NOT NULL,
  `afgeschreven` datetime DEFAULT NULL,
  `transactie_transactieID` int(11) DEFAULT NULL,
  PRIMARY KEY (`transactieID`),
  KEY `Schuld_Transactie` (`transactie_transactieID`),
  CONSTRAINT `Schuld_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`),
  CONSTRAINT `Schuld_Transactie` FOREIGN KEY (`transactie_transactieID`) REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Studie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Studie` (
  `studieID` int(11) NOT NULL AUTO_INCREMENT,
  `naam_NL` varchar(255) NOT NULL DEFAULT '',
  `naam_EN` varchar(255) NOT NULL DEFAULT '',
  `soort` enum('IC','IK','GT','NA','WI','WIT','OVERIG') NOT NULL DEFAULT 'IC',
  `fase` enum('BA','MA') NOT NULL DEFAULT 'BA',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`studieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tag` (
  `tagID` int(11) NOT NULL AUTO_INCREMENT,
  `media_mediaID` int(11) NOT NULL,
  `persoon_contactID` int(11) DEFAULT NULL,
  `commissie_commissieID` int(11) DEFAULT NULL,
  `introgroep_groepID` int(11) DEFAULT NULL,
  `collectie_collectieID` int(11) DEFAULT NULL,
  `tagger_contactID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`tagID`),
  KEY `Tag_Media` (`media_mediaID`),
  KEY `Tag_Persoon` (`persoon_contactID`),
  KEY `Tag_Commissie` (`commissie_commissieID`),
  KEY `Tag_Introgroep` (`introgroep_groepID`),
  KEY `Tag_Tagger` (`tagger_contactID`),
  KEY `Tag_Collectie` (`collectie_collectieID`),
  CONSTRAINT `Tag_Commissie` FOREIGN KEY (`commissie_commissieID`) REFERENCES `Commissie` (`commissieID`),
  CONSTRAINT `Tag_Introgroep` FOREIGN KEY (`introgroep_groepID`) REFERENCES `IntroGroep` (`groepID`),
  CONSTRAINT `Tag_Media` FOREIGN KEY (`media_mediaID`) REFERENCES `Media` (`mediaID`),
  CONSTRAINT `Tag_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE SET NULL,
  CONSTRAINT `Tag_Tagger` FOREIGN KEY (`tagger_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TagArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TagArea` (
  `tag_tagID` int(11) NOT NULL,
  `beginPixelX` int(11) NOT NULL,
  `eindPixelX` int(11) NOT NULL,
  `beginPixelY` int(11) NOT NULL,
  `eindPixelY` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_tagID`),
  CONSTRAINT `TagArea_Tag` FOREIGN KEY (`tag_tagID`) REFERENCES `Tag` (`tagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Telling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Telling` (
  `tellingID` int(11) NOT NULL AUTO_INCREMENT,
  `wanner` datetime NOT NULL,
  `teller_contactID` int(11) NOT NULL,
  `Opmerking` text NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`tellingID`),
  KEY `Telling_Teller` (`teller_contactID`),
  CONSTRAINT `Telling_Teller` FOREIGN KEY (`teller_contactID`) REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TellingItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TellingItem` (
  `telling_tellingID` int(11) NOT NULL,
  `voorraad_voorraadID` int(11) NOT NULL,
  `aantal` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`telling_tellingID`,`voorraad_voorraadID`),
  KEY `TellingItem_Voorraad` (`voorraad_voorraadID`),
  CONSTRAINT `TellingItem_Telling` FOREIGN KEY (`telling_tellingID`) REFERENCES `Telling` (`tellingID`),
  CONSTRAINT `TellingItem_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL DEFAULT '',
  `beschrijving` varchar(255) NOT NULL DEFAULT '',
  `waarde` text NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Tentamen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tentamen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vak_vakID` int(11) NOT NULL,
  `datum` date NOT NULL,
  `naam` varchar(255) DEFAULT NULL,
  `locatie` varchar(255) DEFAULT NULL,
  `oud` int(1) NOT NULL DEFAULT '0',
  `tentamen` int(1) NOT NULL DEFAULT '0',
  `uitwerking` varchar(255) DEFAULT NULL,
  `opmerking` text,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Tentamen_Vak` (`vak_vakID`),
  CONSTRAINT `Tentamen_Vak` FOREIGN KEY (`vak_vakID`) REFERENCES `Vak` (`vakID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TentamenUitwerking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TentamenUitwerking` (
  `uitwerkingID` int(11) NOT NULL AUTO_INCREMENT,
  `tentamen_id` int(11) NOT NULL,
  `uploader_contactID` int(11) NOT NULL,
  `wanneer` datetime NOT NULL,
  `opmerking` text,
  `gecontroleerd` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  `rating_ratingObjectID` int(11) DEFAULT NULL,
  PRIMARY KEY (`uitwerkingID`),
  KEY `TentamenUitwerking_Tentamen` (`tentamen_id`),
  KEY `TentamenUitwerking_Uploader` (`uploader_contactID`),
  KEY `rating_ratingObjectID` (`rating_ratingObjectID`),
  CONSTRAINT `TentamenUitwerking_Tentamen` FOREIGN KEY (`tentamen_id`) REFERENCES `Tentamen` (`id`),
  CONSTRAINT `TentamenUitwerking_Uploader` FOREIGN KEY (`uploader_contactID`) REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TinyMailingUrl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TinyMailingUrl` (
  `mailing_mailingID` int(11) NOT NULL,
  `tinyUrl_id` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`mailing_mailingID`,`tinyUrl_id`),
  KEY `TinyMailingUrl_TinyUrl` (`tinyUrl_id`),
  CONSTRAINT `TinyMailingUrl_Mailing` FOREIGN KEY (`mailing_mailingID`) REFERENCES `Mailing` (`mailingID`),
  CONSTRAINT `TinyMailingUrl_TinyUrl` FOREIGN KEY (`tinyUrl_id`) REFERENCES `TinyUrl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TinyUrl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TinyUrl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tinyUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `outgoingUrl` varchar(511) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `TinyUrlHit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TinyUrlHit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tinyUrl_id` int(11) NOT NULL,
  `host` varchar(255) DEFAULT NULL,
  `hitTime` datetime NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TinyUrlHit_TinyUrl` (`tinyUrl_id`),
  CONSTRAINT `TinyUrlHit_TinyUrl` FOREIGN KEY (`tinyUrl_id`) REFERENCES `TinyUrl` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Transactie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transactie` (
  `transactieID` int(11) NOT NULL AUTO_INCREMENT,
  `overerving` enum('Transactie','DibsTransactie','Pin','Giro','Contant','Schuld','Kudos','iDeal') NOT NULL DEFAULT 'Transactie',
  `rubriek` enum('BOEKWEB','COLAKAS','WWW') NOT NULL DEFAULT 'BOEKWEB',
  `soort` enum('VERKOOP','DONATIE','BETALING','AFSCHRIJVING') NOT NULL DEFAULT 'VERKOOP',
  `contact_contactID` int(11) DEFAULT NULL,
  `bedrag` decimal(8,2) DEFAULT NULL,
  `uitleg` varchar(255) NOT NULL DEFAULT '',
  `wanneer` datetime NOT NULL,
  `status` enum('INACTIVE','OPEN','SUCCESS','FAILURE','CANCELLED','EXPIRED','OPENPOGING2','OPENPOGING3','OPENPOGING4','OPENPOGING5','BANKFAILURE') NOT NULL DEFAULT 'SUCCESS',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`transactieID`),
  KEY `Transactie_Contact` (`contact_contactID`),
  CONSTRAINT `Transactie_Contact` FOREIGN KEY (`contact_contactID`) REFERENCES `Contact` (`contactID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Vacature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vacature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractonderdeel_id` int(11) DEFAULT NULL,
  `datumVerloop` date NOT NULL,
  `datumPlaatsing` date NOT NULL,
  `type` enum('BIJBAAN','PROMOTIEPLAATS','STAGEPLEK','STARTERSFUNCTIE','AFSTUDEEROPDRACHT','WERKSTUDENTSCHAP') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'BIJBAAN',
  `ica` int(1) NOT NULL DEFAULT '0',
  `iku` int(1) NOT NULL DEFAULT '0',
  `na` int(1) NOT NULL DEFAULT '0',
  `wis` int(1) NOT NULL DEFAULT '0',
  `titel_NL` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `titel_EN` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `inleiding_NL` text COLLATE utf8_unicode_ci,
  `inleiding_EN` text COLLATE utf8_unicode_ci,
  `inhoud_NL` text COLLATE utf8_unicode_ci,
  `inhoud_EN` text COLLATE utf8_unicode_ci,
  `views` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Vacature_Contractonderdeel` (`contractonderdeel_id`),
  CONSTRAINT `Vacature_Contractonderdeel` FOREIGN KEY (`contractonderdeel_id`) REFERENCES `Contractonderdeel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Vak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vak` (
  `vakID` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `naam` varchar(255) DEFAULT '',
  `departement` enum('na','wi','ic') NOT NULL,
  `opmerking` varchar(255) DEFAULT NULL,
  `inTentamenLijsten` int(1) NOT NULL DEFAULT '1',
  `tentamensOutdated` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`vakID`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `VakStudie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VakStudie` (
  `vak_vakID` int(11) NOT NULL,
  `studie_studieID` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`vak_vakID`,`studie_studieID`),
  KEY `VakStudie_Studie` (`studie_studieID`),
  CONSTRAINT `VakStudie_Studie` FOREIGN KEY (`studie_studieID`) REFERENCES `Studie` (`studieID`),
  CONSTRAINT `VakStudie_Vak` FOREIGN KEY (`vak_vakID`) REFERENCES `Vak` (`vakID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Verkoop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Verkoop` (
  `voorraadMutatieID` int(11) NOT NULL,
  `transactie_transactieID` int(11) NOT NULL,
  PRIMARY KEY (`voorraadMutatieID`),
  KEY `Verkoop_Transactie` (`transactie_transactieID`),
  CONSTRAINT `Verkoop_fk` FOREIGN KEY (`voorraadMutatieID`) REFERENCES `VoorraadMutatie` (`voorraadMutatieID`),
  CONSTRAINT `Verkoop_Transactie` FOREIGN KEY (`transactie_transactieID`) REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `VerkoopPrijs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VerkoopPrijs` (
  `voorraad_voorraadID` int(11) NOT NULL,
  `wanneer` datetime NOT NULL,
  `prijs` decimal(8,2) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`voorraad_voorraadID`,`wanneer`),
  CONSTRAINT `VerkoopPrijs_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Verplaatsing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Verplaatsing` (
  `voorraadMutatieID` int(11) NOT NULL,
  `tegenVoorraad_voorraadID` int(11) NOT NULL,
  PRIMARY KEY (`voorraadMutatieID`),
  KEY `Verplaatsing_TegenVoorraad` (`tegenVoorraad_voorraadID`),
  CONSTRAINT `Verplaatsing_fk` FOREIGN KEY (`voorraadMutatieID`) REFERENCES `VoorraadMutatie` (`voorraadMutatieID`),
  CONSTRAINT `Verplaatsing_TegenVoorraad` FOREIGN KEY (`tegenVoorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Voornaamwoord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Voornaamwoord` (
  `woordID` int(11) NOT NULL AUTO_INCREMENT,
  `soort_NL` varchar(255) NOT NULL DEFAULT '',
  `soort_EN` varchar(255) NOT NULL DEFAULT '',
  `onderwerp_NL` varchar(255) NOT NULL DEFAULT '',
  `onderwerp_EN` varchar(255) NOT NULL DEFAULT '',
  `voorwerp_NL` varchar(255) NOT NULL DEFAULT '',
  `voorwerp_EN` varchar(255) NOT NULL DEFAULT '',
  `bezittelijk_NL` varchar(255) NOT NULL DEFAULT '',
  `bezittelijk_EN` varchar(255) NOT NULL DEFAULT '',
  `wederkerend_NL` varchar(255) NOT NULL DEFAULT '',
  `wederkerend_EN` varchar(255) NOT NULL DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  `ouder_NL` varchar(255) NOT NULL DEFAULT '',
  `ouder_EN` varchar(255) NOT NULL DEFAULT '',
  `kind_NL` varchar(255) NOT NULL DEFAULT '',
  `kind_EN` varchar(255) NOT NULL DEFAULT '',
  `brusje_NL` varchar(255) NOT NULL DEFAULT '',
  `brusje_EN` varchar(255) NOT NULL DEFAULT '',
  `kozijn_NL` varchar(255) NOT NULL DEFAULT '',
  `kozijn_EN` varchar(255) NOT NULL DEFAULT '',
  `kindGrootouder_NL` varchar(255) NOT NULL DEFAULT '',
  `kindGrootouder_EN` varchar(255) NOT NULL DEFAULT '',
  `kleinkindOuder_NL` varchar(255) NOT NULL DEFAULT '',
  `kleinkindOuder_EN` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`woordID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Voorraad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Voorraad` (
  `voorraadID` int(11) NOT NULL AUTO_INCREMENT,
  `locatie` enum('BW1','MAGAZIJN','BOEKENHOK','EJBV','VIRTUEEL') NOT NULL DEFAULT 'BW1',
  `artikel_artikelID` int(11) NOT NULL,
  `waardePerStuk` decimal(8,2) NOT NULL DEFAULT '0.00',
  `btw` enum('21','6','0','NULL') NOT NULL DEFAULT 'NULL',
  `omschrijving` text,
  `verkoopbaar` int(1) NOT NULL DEFAULT '0',
  `digitaalVerkrijgbaar` enum('NEE','NAMENLIJST','CODE') NOT NULL DEFAULT 'NEE',
  `externVerkrijgbaar` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`voorraadID`),
  KEY `Voorraad_Artikel` (`artikel_artikelID`),
  CONSTRAINT `Voorraad_Artikel` FOREIGN KEY (`artikel_artikelID`) REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `VoorraadMutatie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoorraadMutatie` (
  `voorraadMutatieID` int(11) NOT NULL AUTO_INCREMENT,
  `overerving` enum('VoorraadMutatie','Levering','Verplaatsing','Verkoop') NOT NULL DEFAULT 'VoorraadMutatie',
  `voorraad_voorraadID` int(11) NOT NULL,
  `wanneer` datetime NOT NULL,
  `aantal` int(11) NOT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`voorraadMutatieID`),
  KEY `VoorraadMutatie_Voorraad` (`voorraad_voorraadID`),
  CONSTRAINT `VoorraadMutatie_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `Wachtwoord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Wachtwoord` (
  `persoon_contactID` int(11) NOT NULL,
  `login` varchar(255) NOT NULL DEFAULT '',
  `unixlogin` varchar(255) DEFAULT NULL,
  `wachtwoord` varchar(255) DEFAULT NULL,
  `laatsteLogin` date DEFAULT NULL,
  `magic` varchar(255) DEFAULT NULL,
  `magicExpire` datetime DEFAULT NULL,
  `cryptHash` varchar(255) DEFAULT NULL,
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`persoon_contactID`),
  CONSTRAINT `Wachtwoord_Persoon` FOREIGN KEY (`persoon_contactID`) REFERENCES `Persoon` (`contactID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `iDeal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iDeal` (
  `transactieID` int(11) NOT NULL,
  `titel` varchar(255) NOT NULL DEFAULT '',
  `voorraad_voorraadID` int(11) DEFAULT NULL,
  `externeID` varchar(255) DEFAULT NULL,
  `klantNaam` varchar(255) DEFAULT NULL,
  `klantStad` varchar(255) DEFAULT NULL,
  `expire` datetime NOT NULL,
  `entranceCode` varchar(255) NOT NULL DEFAULT '',
  `magOpvragenTijdstip` datetime NOT NULL,
  `opSlot` int(1) NOT NULL DEFAULT '0',
  `emailadres` varchar(255) DEFAULT NULL,
  `verificatieCode` varchar(255) DEFAULT NULL,
  `returnURL` varchar(255) DEFAULT NULL,
  `processURL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`transactieID`),
  KEY `iDeal_Voorraad` (`voorraad_voorraadID`),
  CONSTRAINT `iDeal_fk` FOREIGN KEY (`transactieID`) REFERENCES `Transactie` (`transactieID`) ON DELETE CASCADE,
  CONSTRAINT `iDeal_Voorraad` FOREIGN KEY (`voorraad_voorraadID`) REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `iDealAntwoord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iDealAntwoord` (
  `ideal_transactieID` int(11) NOT NULL,
  `vraag_activiteit_activiteitID` int(11) NOT NULL,
  `vraag_vraagID` int(11) NOT NULL,
  `antwoord` varchar(255) DEFAULT '',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`ideal_transactieID`,`vraag_activiteit_activiteitID`,`vraag_vraagID`),
  KEY `iDealAntwoord_Vraag` (`vraag_activiteit_activiteitID`,`vraag_vraagID`),
  CONSTRAINT `iDealAntwoord_Ideal` FOREIGN KEY (`ideal_transactieID`) REFERENCES `iDeal` (`transactieID`),
  CONSTRAINT `iDealAntwoord_Vraag` FOREIGN KEY (`vraag_activiteit_activiteitID`, `vraag_vraagID`) REFERENCES `ActiviteitVraag` (`activiteit_activiteitID`, `vraagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `iDealKaartje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iDealKaartje` (
  `artikelID` int(11) NOT NULL,
  `activiteit_activiteitID` int(11) NOT NULL,
  `autoInschrijven` int(1) NOT NULL DEFAULT '0',
  `verkoopbaarViaSite` int(1) NOT NULL DEFAULT '1',
  `returnURL` varchar(255) DEFAULT NULL,
  `externVerkrijgbaar` int(1) NOT NULL DEFAULT '0',
  `digitaalVerkrijgbaar` enum('NEE','NAMENLIJST','CODE','PERSOONLIJK') NOT NULL DEFAULT 'NAMENLIJST',
  `magScannen` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`artikelID`),
  KEY `Kaartje_Activiteit` (`activiteit_activiteitID`),
  CONSTRAINT `Kaartje_Activiteit` FOREIGN KEY (`activiteit_activiteitID`) REFERENCES `Activiteit` (`activiteitID`),
  CONSTRAINT `Kaartje_fk` FOREIGN KEY (`artikelID`) REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `iDealKaartjeCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iDealKaartjeCode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kaartje_artikelID` int(11) NOT NULL,
  `code` varchar(255) NOT NULL DEFAULT '',
  `transactie_transactieID` int(11) DEFAULT NULL,
  `gescand` int(1) NOT NULL DEFAULT '0',
  `gewijzigdWanneer` datetime DEFAULT NULL,
  `gewijzigdWie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `KaartjeCode_Kaartje` (`kaartje_artikelID`),
  KEY `KaartjeCode_Transactie` (`transactie_transactieID`),
  CONSTRAINT `KaartjeCode_Kaartje` FOREIGN KEY (`kaartje_artikelID`) REFERENCES `iDealKaartje` (`artikelID`),
  CONSTRAINT `KaartjeCode_Transactie` FOREIGN KEY (`transactie_transactieID`) REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

