#!/usr/bin/php -q
<?php

require('test/init.php');

// Als we argumenten hebben, zijn dit de tests om te runnen.
if ($argc > 1)
{
	$tests = array_slice($argv, 1);
	foreach ($tests as $testnaam)
	{
		$runner = new WhosWho4\TestHuis\TestRunner($testnaam);
		print_r($runner->runLosseTest($testnaam));
	}
}
else
{
	$runner = new WhosWho4\TestHuis\TestRunner();
	exit($runner->runAlleTests());
}
