#!/usr/bin/php -q
<?php
require_once('script-init.php');
require_once('WhosWho4/init.php');

/* $Id: $
  Hier worden binnenkomende mailings (minimaal) verwerkt en in de database
  opgeslagen. Optioneel kan er een commandline parameter meegegeven worden: de
  'local part' van de inkomende mail (het adres waarop de mailing is
  binnengekomen). Op deze manier kunnen mailings gedifferentieerd wroden,
  bijvoorbeeld voor de SpoCie.

  die(...) resulteert in een bounce met ... als melding

  TODO:
   - nooit bouncen?

	stap 1:
		maak de mail zoals je hem wilt hebben in je MUA
	stap 2: (dit ding)
		mailen naar mailings@ (oid), die roept dit script aan
	stap 3:
		ga naar website en doe wat beheer dingen met de mail
		* selecteer de mailinglijst
		* verstuur preview
		* zet wanneer de mailing verstuurd moet worden
	stap 4:
		cron: zorg dat mail verstuurd wordt
*/

// lees mail van stdin in
$stdin = fopen('php://stdin', 'r');
$mail = '';
while($line = fgets($stdin, 4096))
{
	if (preg_match('/^To: ([^@]+)/i', $line, $matches)) {
	  $localPart = $matches[1];
	}
	$mail .= $line;
}
unset($stdin, $line);

// Even subject-header opzoeken, dat is de enige die we nodig hebben...
$lines_array = explode("\n", $mail);
$subject = "Mailimport zonder subject";
foreach($lines_array as $line)
{
	if(strtolower(substr($line, 0, 9)) === "subject: ")
		$subject = trim(substr($line, 8));
}

$mailing = new Mailing();
$mailing->setOmschrijving($subject);
$mailing->setMailSubject($subject);
$mailing->setMailFrom("bestuur@a-eskwadraat.nl");

$ciemail = null;

//Is er afhankelijke van het adres kunnen we extra params zetten...
switch(strtolower($localPart)) {
case "spocie-mailings":
	$cienaam = "SpoCie";
	$cieid = 36;
	$ciemail = "spocie@a-eskwadraat.nl";
	$mailing->setMailFrom("extern@a-eskwadraat.nl");
	break;
case "comint-mailings":
case "activiteiten-mailings":
	$cienaam = "ActiviteitenMailingsGroep";
	$cieid = 603;
	$ciemail = "activiteiten@a-eskwadraat.nl";
	$mailing->setMailFrom("A-Eskwadraat <activiteiten@a-eskwadraat.nl>");
	break;
case "default":
	break;
}

// Cie koppelen en mailing opslaan
if($cieid)
	$mailing->setCommissie($cieid);

$mailing->opslaan();

// Cie notifyen
if ($ciemail){
	$subject = "Nieuwe mailing in MailingWeb!";
	$message = "Hoi $cienaam!\n\n";
	$message .= "Gefeliciteerd! Zojuist is er een nieuwe mailing voor jullie binnengekomen in MailingWeb! ";
	$message .= "Hier kunnen jullie allerlei interessante dingen mee doen door de volgende link te volgen:\n";
	$message .= "http://www.A-Eskwadraat.nl" . $mailing->url();
	$message .= "\n\n";
	$message .= "Veel plezier ermee!\n\n";
	$message .= "  -- MailingWeb";
	sendmail("MailingWeb <www@a-eskwadraat.nl>", $ciemail, $subject, wordwrap($message));
}


// Nu body en headers parsen uit input
$mailing->parseExternalMailInput($mail);
$mailing->koppelTinyUrls();
