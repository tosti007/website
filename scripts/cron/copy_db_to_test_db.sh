#!/bin/sh
# $Id$

# Maak een kopier van alle databases naar test_<db> onder controle van de www

dbs="$@"
if [ -z "$dbs" ]; then
	dbs="benamite boeken whoswho4"
fi

for db in $dbs; do
	mysqldump --add-drop-table $db \
		| sed -e '1i SET FOREIGN_KEY_CHECKS=0;' -e '$a SET FOREIGN_KEY_CHECKS=1;' \
		| mysql test_$db
	mysqldump --add-drop-table $db \
		| sed -e '1i SET FOREIGN_KEY_CHECKS=0;' -e '$a SET FOREIGN_KEY_CHECKS=1;' \
		| ssh root@vm-www-debug -i ~/.ssh/debug test_$db
done
