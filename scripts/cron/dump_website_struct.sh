#!/bin/sh

# dumpt de structuur van de website, zodat wijzigingen in de structuur
# in source control komen
# Wil een environment var LIVEBRANCH: naam van branch waar development op gebeurt

set -e

if ! /usr/local/bin/servercheck; then
	echo "Alleen op square draaien!"
	exit 1
fi

dump()
{
	db=$1
	if test ! -e databasecontent.$db.sql; then
		echo '# $''Id: $' > databasecontent.$db.sql
		echo >> databasecontent.$db.sql
	else
		TMPFILE=`mktemp /tmp/mkdump.XXXXXX.tmp`
		head -2 databasecontent.$db.sql > $TMPFILE
		cp $TMPFILE databasecontent.$db.sql
		rm -f $TMPFILE
	fi

	# Replace " AUTO_INCREMENT=123456 " with " AUTO_INCREMENT=_____ "
	# Neem geen publisher pagina's(2) of geuploade bestanden mee (3).
	mysqldump --skip-extended-insert --skip-comments --tables benamite benamite --where="type NOT IN(2,3)" | sed 's/ AUTO_INCREMENT=[0-9]* / AUTO_INCREMENT=_____ /' >> databasecontent.$db.sql

}

cd ..
# kijk wat er allemaal gecommit is
git fetch origin &> /dev/null

dump benamite

git diff origin/$LIVEBRANCH -- databasecontent.benamite.sql 2>/dev/null | grep -q . || exit

echo "De structuur van de website is veranderd. Commit dit ajb zsm."
echo "Zie ~www/scripts"
echo 
git diff origin/$LIVEBRANCH databasecontent.benamite.sql | grep -v 'revision [0-9]'

