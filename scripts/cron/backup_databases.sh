#!/bin/sh
# $Id$ 

# Maak incrementele backups van alle databases onder controle van de www

set -e

if ! [ -d /archief/www ]; then
	echo "De backupschijf moet gemount zijn op /archief en een schrijfbare subdirectory op /archief/www hebben!"
	echo "(Je wilt uiteraard niet backuppen naar dezelfde (virtuele) machine)"
	echo "FIX DIT MOEILIJK SNEL!"
	echo "Tot gauw, het backupscript"
	exit 1
fi

# Niet 2x tegelijkertijd draaien, da's eng
lockfile -r0 /var/lock/www/backup_databases.lock

cd /archief/www

newdir=`date +%Y-%m/%d`
newfile=`date +%Y-%m-%d_%H_%M`

mkdir -p $newdir
ln -snf $newdir today

for db in benamite boeken whoswho4; do
	new=$newdir/$db.$newfile

	if test "$1" = "--full"; then
		mysqldump --complete-insert --skip-extended-insert --skip-comments --add-drop-table $db > $new.full
	else
		# Non-volledige backups zijn deprecated.
		echo "Waarschuwing: de incrementele backup via een diff van de mysqldump is deprecated."
		echo "We maken nu een kopie van de binaire logs van mysql."
		mysqldump --complete-insert --skip-extended-insert --skip-comments --add-drop-table $db > $new

		diff -U 0 -H $db.old $new --speed-large-files > $new.diff && rm $new.diff
		rm $db.old
		mv $new $db.old
	fi
done

rm -f /var/lock/www/backup_databases.lock
