#!/usr/bin/php -q
<?php
require('script-init.php');

// Zoek actieve BugCategorieen bij inactieve Commissies
$res = $WSW4DB->q('
	TABLE SELECT `BugCategorie`.`naam` AS `BC_naam`, `BugCategorie`.`bugCategorieID`,
		         `Commissie`.`naam`    AS `C_naam`, `Commissie`.`commissieID`
	FROM `BugCategorie` JOIN `Commissie`
			ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
	WHERE NOT (`Commissie`.`datumEind` IS NULL)
		  AND `BugCategorie`.`actief` = 1');

foreach ($res as $row)
{
	echo sprintf("Actieve BugCategorie '%s' (%d) hoort bij inactieve Commissie %s (%d)!\n",
		$row['BC_naam'], $row['bugCategorieID'], $row['C_naam'], $row['commissieID']);
}

// TODO: Zoek Bugs waarvan de cache-waarden niet kloppen

// Zoek BugToewijzingen waarvan het beginbericht niet voor het eindbericht komt
$res = $WSW4DB->q('
	TABLE SELECT `BugToewijzing`.`bugToewijzingID`, `BeginBericht`.`bugBerichtID` AS `begin_bugBerichtID`, `EindBericht`.`bugBerichtID` AS `eind_bugBerichtID`
	FROM `BugToewijzing` JOIN `BugBericht` `BeginBericht`
		ON `BugToewijzing`.`begin_bugBerichtID` = `BeginBericht`.`bugBerichtID`
	JOIN `BugBericht` `EindBericht`
		ON `BugToewijzing`.`eind_bugBerichtID` = `EindBericht`.`bugBerichtID`
	WHERE `BeginBericht`.`moment` >= `EindBericht`.`moment`');

foreach ($res as $row)
{
	echo sprintf("In BugToewijzing %d: beginbericht %d komt niet voor eindbericht %d!\n",
		$row['bugToewijzingID'], $row['begin_bugBerichtID'], $row['eind_bugBerichtID']);
}

// Zoek BugToewijzingen waarvan het beginbericht niet bij de Bug van de toewijzing hoort
$res = $WSW4DB->q('
	TABLE SELECT `BugToewijzing`.`bugToewijzingID`, `BeginBericht`.`bugBerichtID`
	FROM `BugToewijzing` JOIN `BugBericht` `BeginBericht`
		ON `BugToewijzing`.`begin_bugBerichtID` = `BeginBericht`.`bugBerichtID`
	WHERE `BeginBericht`.`bug_bugID` != `BugToewijzing`.`bug_bugID`');

foreach ($res as $row)
{
	echo sprintf("In BugToewijzing %d: beginbericht %d heeft niet dezelfde bug als de toewijzing!\n",
		$row['bugToewijzingID'], $row['bugBerichtID']);
}

// Zoek BugToewijzingen waarvan het eindbericht niet bij de Bug van de toewijzing hoort
$res = $WSW4DB->q('
	TABLE SELECT `BugToewijzing`.`bugToewijzingID`, `EindBericht`.`bugBerichtID`
	FROM `BugToewijzing` JOIN `BugBericht` `EindBericht`
		ON `BugToewijzing`.`eind_bugBerichtID` = `EindBericht`.`bugBerichtID`
	WHERE `EindBericht`.`bug_bugID` != `BugToewijzing`.`bug_bugID`');

foreach ($res as $row)
{
	echo sprintf("In BugToewijzing %d: beginbericht %d heeft niet dezelfde bug als de toewijzing!\n",
		$row['bugToewijzingID'], $row['bugBerichtID']);
}
