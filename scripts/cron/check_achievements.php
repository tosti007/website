#!/usr/bin/php -q
<?php
require('script-init.php');

ini_set('memory_limit', '265M');

global $WSW4DB, $BADGES;
global $logger;

$res = $WSW4DB->q("COLUMN SELECT `persoon_contactID` FROM `PersoonBadge`");
// We gaan niet al deze id's verzamelen omdat het niet in memory past,
// in plaats daarvan itereren we over de id's voor n+1 queries.
// Dit is een tradeoff voor snelheid en geheugengebruik,
// en het is een cronjob dus snelheid boeit niet echt.
foreach($res as $badgeid) {
	$persoonBadge = PersoonBadge::geef($badgeid);

	// Check eerst of alle badges die bij de persoon horen wel echt nog bestaan
	$badges = json_decode($persoonBadge->getBadges());
	$last = json_decode($persoonBadge->getLast());

	foreach($badges as $badge)
	{
		if(!array_key_exists($badge, $BADGES['onetime']))
		{
			$pos = strrpos($badge, '_');
			$bsgp_badge = substr($badge, 0, $pos);
			$bsgp_count = substr($badge, $pos+1);
			if(!array_key_exists($bsgp_badge, $BADGES['bsgp'])) {
				$persoonBadge->removeBadge($badge);
				$logger->info("Badge $badge weggehaald bij lid " . $persoonBadge->getPersoon()->geefID() . "\n");
			} else {
				if($BADGES['bsgp'][$bsgp_badge]['multi'] == 'long') {
					$multi = array(1, 2, 5, 10);
				} else {
					$multi = array(1, 2, 3, 4);
				}
				$val = $BADGES['bsgp'][$bsgp_badge]['value'];
				if(!in_array((float)($bsgp_count/$val), $multi)) {
					$persoonBadge->removeBadge($badge);
					$logger->info("Badge $badge weggehaald bij lid " . $persoonBadge->getPersoon()->geefID() . "\n");
				}
			}
		}
	}

	// Check voor alle onetime-badges of ze voor de persoon nog geldig zijn
	foreach($BADGES['onetime'] as $naam => $badge) {
		$moetkrijgen = PersoonBadge::checkOneTimeBadge($naam, $persoonBadge->getPersoon(), true);
		$heeft = $persoonBadge->bevatBadge($naam);

		if(!$badge['herhaaldChecken']) {
			continue;
		}

		if(!$moetkrijgen && $heeft) {
			$persoonBadge->removeBadge($naam);
			$logger->info("Badge $naam weggehaald bij lid " . $persoonBadge->getPersoon()->geefID() . "\n");
		}
	}

	// Check dan voor alle bsgp-badges of ze nog geldig zijn voor de persoon
	foreach($BADGES['bsgp'] as $naam => $badge) {
		for($i = 0; $i < 4; $i++) {
			$moetkrijgen = PersoonBadge::checkSingleBSGPBadge($naam, $i, $persoonBadge->getPersoon(), true);

			if($badge['multi'] == 'long') {
				$multiplier = array(1, 2, 5, 10);
			} elseif($badge['multi'] == 'short') {
				$multiplier = array(1, 2, 3, 4);
			}

			$naamVolledig = $naam . '_' . ($badge['value'] * $multiplier[$i]);

			$heeft = $persoonBadge->bevatBadge($naamVolledig);

			if(!$moetkrijgen && $heeft) {
				$persoonBadge->removeBadge($naamVolledig);
				$logger->info("Badge $naamVolledig weggehaald bij lid " . $persoonBadge->getPersoon()->geefID() . "\n");
			}
		}
	}
}
