#!/usr/bin/php -q
<?php
#
# Check of dudes nog studeren door het aan ldap te vragen.
#
# $Id$
#
require('script-init.php');

$leden = LidVerzameling::alleLeden();

// Anonymous connection naar UU-LDAP server opzetten
$ldapconn = ldap_connect("ldap.science.uu.nl");
ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

$ldapwerkt = @ldap_bind($ldapconn);

$arr_nietstuderend = array();

foreach ($leden as $lid)
{
	$date = new DateTimeLocale();
	if($lid->getLidTot()->hasTime() && $lid->getLidTot() < $date) {
		continue;
	}

	$studnr = $lid->getStudentnr();
	$lidnr = $lid->getContactID();

	if(is_null($studnr))
		continue;

	$studerend = false; // studeert deze persoon nog?

	if ($ldapwerkt) {
		if (!$searchres = @ldap_search($ldapconn, "ou=Students,ou=Users,dc=science,dc=uu,dc=nl", "(uid=$studnr)")) {
			// Kan niet gevonden worden
			$studerend = false;
		} else {
			// Wel een resultaat gevonden, dus hij/zij bestaat wel in de UU-LDAP database
			$studerend = true;

			$ldapentries = ldap_get_entries($ldapconn, $searchres);
			if($ldapentries['count'] == 0)
			{
				// niets gevonden, dus studeerd niet
				$studerend = false;
			}
		}
	}

	if (!$studerend) {
		$arr_nietstuderend[] = array($lidnr, $studnr);
	}
}

if(sizeof($arr_nietstuderend))
{
	echo("Lieve secretaris,\n\n"
		."Van de volgende leden hebben we geen studentnummer in de databaas "
		."van de UU gevonden, dat betekent dat ze niet meer studeren of dat ze "
		."gewoon raar zijn.\n\n");

	foreach($arr_nietstuderend as $lid)
	{
		echo("Lid met lidnr $lid[0] en studentnr $lid[1]\n");
	}

	echo("\nGroetjes,\nDe ldap-check, de ldap-check, Kentucky Fried Chicken en de ldap-check\n");
}
