#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id$

global $errors;

// haal weg om alle SQL queries toch te zien
$SQLMSG = false;
$errors = array();

vakMailadres();
voorraadControle();
checkVerkoopgeld();
checkLeveranciersbestellingen();
foreignKeysControle();
levprijsVoorraadGeldig();
verkopenAanBijnaEnNietLeden();
//checkOudeStudentbestellingen();

if (sizeOf($errors) > 0) {
	echo implode("\n", $errors);
	echo "\n";
}


function vakMailadres()
{
	global $BWDB;

	$res = $BWDB->q('TABLE SELECT * FROM `vak` ORDER BY `vak`.`vaknr`');

	foreach($res as $row) {

		if (!empty($row['email']) && !checkEmailFormat($row['email'])){
			error("Vak $row[vaknr] heeft ongeldig emailadres: \"$row[email]\"");
		}
	}
}

function checkOudeStudentbestellingen()
{
	global $BWDB;
	$res = $BWDB->q("TABLE SELECT bestelnr,lidnr FROM studentbestelling
		WHERE gemailddatum IS NULL and vervaldatum IS NULL and
		TO_DAYS(bestelddatum) < (TO_DAYS(NOW())-365)
		ORDER BY studentbestelling.bestelnr");
	foreach($res as $row) {
		error("Studentbestelling $row[bestelnr] door lid $row[lidnr] staat al meer dan 365 dagen open.");
	}
}

function verkopenAanBijnaEnNietLeden()
{
	global $BWDB;
	$res = $BWDB->q("TABLE SELECT verkoopnr, lidnr, EAN, lidToestand "
					. "FROM verkoop "
					. "LEFT JOIN " . WHOSWHO4_DB . ".Lid ON (lidnr = Lid.contactID) "
					. "LEFT JOIN voorraad USING (voorraadnr) "
					. "WHERE lidToestand = 'BIJNALID' "
					. "ORDER BY verkoop.verkoopnr");
	foreach($res as $row) {
		$wat = '';
		switch($row['EAN']) {
		case '2100000000159':			// lidmaatschap
			$wat = ' (lidmaatschap)';
			break;
		case '2100000000678':			// master lidmaatschap
			$wat = ' (masterlidmaatschap)';
			break;
		}
		if($row['lidnr']){
			if($row['lidToestand'] == 'BIJNALID')
			{
				$status = 'bijna';
			}
			error("Verkoop $row[verkoopnr] gedaan aan $status-lid $row[lidnr]!$wat");
		}
	}

	$res = $BWDB->q("TABLE SELECT bestelnr, lidnr "
				.	"FROM studentbestelling "
				.	"LEFT JOIN " . WHOSWHO4_DB . ".Lid ON (lidnr = Lid.contactID) "
				.	"WHERE Lid.lidToestand='BIJNALID' "
				.	"ORDER BY studentbestelling.bestelnr");
	foreach($res as $row) {
		error("Studentbestelling $row[bestelnr] gedaan 'door' bijna-lid $row[lidnr]!");
	}
}

function voorraadControle()
{
	global $BWDB;

	$res = $BWDB->q('TABLE SELECT * FROM voorraad ORDER BY EAN');
	foreach($res as $row) {
		$voorraadnr = $row['voorraadnr'];

		$geleverd	  = $BWDB->q("VALUE SELECT SUM(aantal) FROM levering WHERE voorraadnr = %i", $voorraadnr);
		$verkocht	  = $BWDB->q("VALUE SELECT SUM(aantal) FROM verkoop WHERE voorraadnr = %i", $voorraadnr);
		$teruggekocht = $BWDB->q("VALUE SELECT SUM(aantal) FROM terugkoop WHERE voorraadnr = %i", $voorraadnr);
		$retour		  = $BWDB->q("VALUE SELECT SUM(aantal) FROM retour WHERE voorraadnr = %i", $voorraadnr);

		$voorraad = $row['voorraad'] + $row['buitenvk'] + $row['ejbv_voorraad'];

		$shoudbe_voorraad= $geleverd - $retour - $verkocht + $teruggekocht;

		if ($voorraad != $shoudbe_voorraad) {
			error("Van voorraad $row[EAN]:$voorraadnr zouden nog $shoudbe_voorraad boeken ".
				"moeten zijn, er zijn er echter op dit moment $voorraad.");

		}
	}
}


function checkVerkoopgeld()
{

	global $BWDB;

	// alle dagen waar een geldigtot is
	$res = $BWDB->q("TABLE SELECT `verkoopdag`, `geldigtot`, `sluitstamp` FROM `verkoopgeld` ORDER BY `verkoopdag`");
	// elk van deze dagen moet precies een (1) geldigtot=null hebben

	foreach($res as $dag) {
		if (!isset($checkdag[$dag['verkoopdag']])) {
			$checkdag[$dag['verkoopdag']]=0;
		}

		if (!isset($dag['geldigtot']) || !$dag['geldigtot']) {
			$checkdag[$dag['verkoopdag']]++;

			if (isset($dag['sluitstamp'])) {
				$incons = finPutCSV($dag['verkoopdag'], strftime('%Y-%m-%d',
					cal_AddDays(strtotime($dag['verkoopdag']), 1)), 'dag', true);
				foreach ($incons as  $d => $bedrag) {
					error ("Op $d is er voor $bedrag meer verkocht dan er is betaald");
				}
			}
		}
	}
	// checkdag moet voor elke dag precies 1 zijn
	foreach($checkdag as $dag=>$aantal) {
		if($aantal != 1) {
			error ("Op $dag zijn er $aantal geldigtots die NULL zijn! (moet er precies 1 zijn)");
		}
	}
}

// foreign keys & data constraint check
// per tabel worden de foreign keys opgenoemd (door hun tabel), een *
// betekent: kan null zijn. Ook de data constraints worden opgenoemd, met
// 'geen' als die er niet zijn

function check_foreign($tabel1, $prikey, $forkey, $tabel2, $for2key, $optioneel = false)
{
	global $BWDB;
	$res = $BWDB->q("TABLE SELECT *, $tabel1.$forkey as forkey, $prikey as prikey"
		. " FROM $tabel1 LEFT JOIN $tabel2 ON ($tabel1.$forkey = $tabel2.$for2key)"
		. " WHERE " . ($optioneel?("$tabel1.$forkey IS NULL"):"1")
		. " AND $tabel2.$for2key IS NULL");
	foreach($res as $row) {
		error("$tabel1 $row[prikey] heeft een ongeldige foreign ".
			"key in kolom $forkey (\"$row[forkey]\") ".
			"naar $tabel2");
	}
}

function checkLeveranciersbestellingen()
{
	global $BWDB;

	// bestelling data: aantal>=aantalgeleverd>=0, datum<=now,
	$res = $BWDB->q("TABLE SELECT * FROM `bestelling` WHERE `aantal` < `aantalgeleverd` OR `aantalgeleverd` < 0 OR `datum` > NOW()");
	foreach($res as $row) {
		error("Bestelling $row[levbestelnr]: aantallen of datum fuckedup");
	}
}

function foreignKeysControle()
{
	global $BWDB;

	// boeken data: valid EAN, nette auteur-syntax (checkbaar?)
	// boekvak data: nvt
	// leverancier data: nvt
	// levering data: aantal>=vooraad+buitenvk, aantal>0, voorraad>=0, buitenvk>=0,
		// levprijs>=0,vkprijs>=0,wanneer<=NOW()
	// nieuws data: nvt
	// offerte data: datum<now, datum<verloopdatum
	// onverwacht: OBSOLETE
	// retour data: aantal > 0, wanneer<NOW, WAARDE OBSOLETE
	// studentbestelling for: leden, boekvak*
	check_foreign('studentbestelling', 'bestelnr', 'lidnr', WHOSWHO4_DB . '.Persoon', 'contactID');
	check_foreign('studentbestelling', 'bestelnr', 'EAN', 'boeken', 'EAN');
	// TODO: of EAN en boekvaknr->EAN overeenkomen
	// studentbestelling data: gemailddatum<=now,gekochtdatum<=now,bestelddatum<=now
	// studievak data: nvt
	// telling for: leden
	check_foreign('telling', 'tellingnr', 'lidnr', WHOSWHO4_DB . '.Lid', 'contactID');
	// telling data: wanneer<=now
	// tellingitem data: aantal_voorraad>=0, aantal_buitenvk>=0
	// terugkoop for: leden*
	check_foreign('terugkoop', 'CONCAT(terugkoop.verkoopnr,"-",terugkoop.wanneer)', 'lidnr', WHOSWHO4_DB . '.Persoon', 'contactID', true);
	// terugkoop data: wanneer <=now, aantal>0, aantal<= verkoop.aantal
	// vak data: collegejaar>1990, begindatum<einddatum, studiejaar!=''
	// verkoop for: leden*
	check_foreign('verkoop', 'verkoopnr', 'lidnr', WHOSWHO4_DB . '.Persoon', 'contactID', true);
	// verkoop data: aantal>0, wanneer<now
	// verkoopgeld for: leden
	//   Kan niet via de functie omdat de kolom anders heet in verkoopgeld
	check_foreign('verkoopgeld', "CONCAT(verkoopdag,'-',volgnr)", 'verkoper', WHOSWHO4_DB . '.Lid', 'contactID');
	// verkoopgeld data: elders
	// verplaatsing data: aantal>0, van != '', naar != ''
}

function levprijsVoorraadGeldig()
{
	global $BWDB;

	// voorraad data: levprijs not null, ...
	$res = $BWDB->q("TABLE SELECT `voorraadnr` FROM `voorraad` WHERE `levprijs` IS NULL ORDER BY `voorraad`.`voorraadnr`");
	foreach($res as $row) {
		error("voorraad $row[voorraadnr] heeft een levprijs die NULL is");
	}
}

function error($msg)
{
	global $errors;
	$errors[] = $msg;
}

