#!/usr/bin/php -q
<?php
global $WSW4DB;

require('script-init.php');

// Vraag alle whoswho4-objecten op
exec("find ".FS_ROOT."/WhosWho4 -name 'DBObject' -exec ls {}\/Generated \; | sed s/\.gen\.php//g", $objs);

sort($objs);

$tmpfname = tempnam("/tmp", 'db_check_');
$tmp = fopen($tmpfname, 'w+');

fwrite($tmp, "/* WhosWho4 DB Table definities */\n/* Vergeet de DB niet aan te passen! */\n");

foreach($objs as $obj)
{
	$res = $WSW4DB->pdo->query("SHOW TABLES LIKE '$obj'")->rowCount();
	if($res == 0) {
		continue;
	}
	$create = $WSW4DB->pdo->query("SHOW CREATE TABLE `$obj`");
	$layout = $create->fetch()[1];

	// Allemaal manipulaties zodat het in hetzelfde format staat als WhosWho4/database.sql
	$exp_layout = explode(" (\n ", $layout);
	$layout = implode("\n(", $exp_layout);
	$exp_layout = explode(",\n ", $layout);
	$layout = implode("\n,", $exp_layout);
	$exp_layout = explode(" FOREIGN KEY ", $layout);
	$layout = implode("\n    FOREIGN KEY ", $exp_layout);
	$exp_layout = explode(" REFERENCES ", $layout);
	$layout = implode("\n    REFERENCES ", $exp_layout);

	$layout_new = '';
	$replace = [];

	foreach(preg_split("/((\r?\n)|(\r\n?))/", $layout) as $line)
	{
		if(substr($line, 0, 5) !== ", KEY")
		{
			$layout_new .= "\n$line";
		}
	}

	$layout_new = str_replace("DEFAULT '0'", 'DEFAULT False', $layout_new);
	$layout_new = str_replace('`,`', '`, `', $layout_new);
	$layout_new = preg_replace('/AUTO_INCREMENT=[0-9]+\ /', '', $layout_new);

	fwrite($tmp, $layout_new.";\n");
}

exec("diff -u $tmpfname ".FS_ROOT."/WhosWho4/database.sql", $res);

if(count($res) != 0)
{
	echo "De volgende verschillen bestaan tussen de database (de -'s)\n"
		."en het bestand database.sql (de +'s):\n\n";
	echo implode("\n", $res);
}

unlink($tmpfname);
