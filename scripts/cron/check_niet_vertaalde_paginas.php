#!/usr/bin/php -q
<?php

require('script-init.php');

global $BMDB;

$entries = $BMDB->q("COLUMN SELECT `id` FROM `benamite` WHERE `type` = 2");

foreach($entries as $entry) {
	$vfs = vfs::get($entry);
	if(is_null($vfs->getContent('en'))) {
		if(strpos($vfs->url(), "/Vereniging/Commissies/") === 0) {
			continue;
		} else if(strpos($vfs->url(), "/Activiteiten/*/") === 0) {
			continue;
		}
		echo "Pagina " . $vfs->url() . " heeft nog geen Engelse vertaling!\n";
	}
}
