#!/bin/sh
# $Id$

# dumpt de structuur van alle www-databases, zodat wijzigingen in de structuur
# in source control komen
# Wil een environment var LIVEBRANCH: naam van branch waar development op gebeurt

set -e

if ! /usr/local/bin/servercheck; then
	echo "Script alleen op square draaien!"
	exit 1
fi

dump()
{
	db=$1
	if test ! -e database.$db.sql; then
		echo '# $''Id: $' > database.$db.sql
		echo >> database.$db.sql
	else
		TMPFILE=`mktemp /tmp/mkdump.XXXXXX.tmp`
		head -2 database.$db.sql > $TMPFILE
		cp $TMPFILE database.$db.sql
		rm -f $TMPFILE
	fi

	extra=
	test "$2" != "full" && extra=--no-data

	# Replace " AUTO_INCREMENT=123456 " with " AUTO_INCREMENT=_____ "
	mysqldump --skip-extended-insert --skip-comments --events $db $extra | sed 's/ AUTO_INCREMENT=[0-9]* / AUTO_INCREMENT=_____ /' >> database.$db.sql
}

cd ..
# kijk wat er allemaal gecommit is
git fetch origin &> /dev/null

for db in benamite boeken whoswho4; do
	dump $db
done

dump mysql full

# kijk wat voor een veranderingen er zijn
git diff origin/$LIVEBRANCH -- database.* 2>/dev/null | grep -q . || exit

echo "De structuur van de database is veranderd. Commit dit ajb zsm."
echo "Zie ~www/scripts"
echo 
git diff origin/$LIVEBRANCH -- database.* | grep -v 'revision [0-9]'

