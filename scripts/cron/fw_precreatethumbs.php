#!/usr/bin/php -q
<?php
require("script-init.php");
global $WSW4DB, $filesystem;

$mediaids = MediaVerzameling::geefNietVideos();

$chunks = array_chunk($mediaids, 200);
foreach($chunks as $mediaids) {
	foreach($mediaids as $mediaid) {
		$medium = Media::geefAbsLoc("medium", $mediaid);
		$large = Media::geefAbsLoc("groot", $mediaid);
		$thumb = Media::geefAbsLoc("thumbnail", $mediaid);

		if (!$filesystem->has($medium)){
			$mediafile = Media::geef($mediaid);
			$mediafile->createThumbnail("medium");
		}

		if (!$filesystem->has($large)){
			$mediafile = Media::geef($mediaid);
			$mediafile->createThumbnail("groot");
		}

		if (!$filesystem->has($thumb)){
			$mediafile = Media::geef($mediaid);
			$mediafile->createThumbnail("thumbnail");
		}

		flush();
	}
}

$mediaids = MediaVerzameling::geefVideos();

$chunks = array_chunk($mediaids, 200);
foreach($chunks as $mediaids) {
	foreach($mediaids as $mediaid) {
		$medium = Media::geefAbsLoc("medium", $mediaid);
		$large = Media::geefAbsLoc("groot", $mediaid, false, 'mp4');
		$thumb = Media::geefAbsLoc("thumbnail", $mediaid);

		if (!$filesystem->has($medium)){
			$mediafile = Media::geef($mediaid);
			$mediafile->createThumbnail("medium");
		}

		if (!$filesystem->has($large)){
			$mediafile = Media::geef($mediaid);
			$mediafile->createThumbnail("groot");
		}

		if (!$filesystem->has($thumb)){
			$mediafile = Media::geef($mediaid);
			$mediafile->createThumbnail("thumbnail");
		}

		flush();
	}
}
?>
