#!/usr/bin/php -q
<?php

require("script-init.php");
global $WSW4DB;

// We willen alleen de cieleden die vandaag veranderen kwa datum
$ids = $WSW4DB->q('TABLE SELECT `persoon_contactID`,`commissie_commissieID` '
		. 'FROM `CommissieLid` '
		. 'WHERE DATEDIFF(`datumBegin`, NOW()) = 0');

foreach($ids as $id)
{
	$cielid = CommissieLid::geef($id[0], $id[1]);

	if(!DEBUG && !DEMO)
		$cielid->voegOpSysteemToe();
}

// We willen alleen de cieleden die vandaag veranderen kwa datum
$ids = $WSW4DB->q('TABLE SELECT `persoon_contactID`,`commissie_commissieID` '
		. 'FROM `CommissieLid` '
		. 'WHERE DATEDIFF(`datumEind`, NOW()) = 0');

foreach($ids as $id)
{
	$cielid = CommissieLid::geef($id[0], $id[1]);

	if(!DEBUG && !DEMO)
		$cielid->haalOpSysteemWeg();
}
