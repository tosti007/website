#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id 

global $errors;
$errors = array();

$SQLMSG=FALSE;

// Check of alle emailadressen er valide uitzien
$res = $WSW4DB->q('TABLE SELECT contactID, email FROM Contact');
foreach($res as $row) {

	if (isset($row['email']) && !empty($row['email']) && !checkEmailFormat($row['email'], true)){
		error("Contact $row[contactID] heeft ongeldig emailadres: \"$row[email]\"");
	}
}

$maand = date("M");
$nu = time();

// Check of alle personen voorletters hebben
$res = $WSW4DB->q("TABLE SELECT contactID, voorletters FROM Persoon WHERE voorletters = ''");
foreach($res as $row) {
	error("Persoon heeft geen voorletters: lidnummer $row[contactID]");
}

$res = $WSW4DB->q('TABLE SELECT Lid.contactID, lidToestand, lidTot, lidVan, overleden FROM Lid JOIN Persoon ON Lid.contactID = Persoon.contactID');
foreach($res as $row) {
	// Als LidTot geset is, moet ook LidVan er zijn
	if (!isset($row['lidVan']) && isset($row['lidTot']) )
		error("Lid $row[contactID] heeft wel lidtot maar geen lidvan!");

	// LidTot > LidVan
	if (isset($row['lidVan']) && isset($row['lidTot']) && 
		$row['lidVan'] != "0000-00-00" && $row['lidTot'] != "0000-00-00" &&
		strcmp($row['lidVan'], $row['lidTot']) > 0) {
		error("Lid $row[contactID] heeft lidvan ($row[lidVan]) > lidtot ($row[lidTot])!");
	}
	
	// Huidige leden moeten LidTot > vandaag hebben
	if ($row['lidToestand'] == 'LID' && isset($row['lidTot']) && strtotime($row['lidTot']) < $nu)
		error("Huidig lid $row[contactID] heeft een lidtot in het verleden ($row[lidTot])!");
	
	if ($row['lidToestand'] == 'BIJNALID')
	{
		// In de maanden dat de intro-inschrijvingen draaien, doen we nergens moeilijk over
		if ($maand != "Jul" && $maand != "Aug" && $maand != "Sep") {
			// en ook als de lidVan in de toekomst ligt
			if (!isset($row['lidVan']) || strtotime($row['lidVan']) < $nu) {
				error("Bijna-lid $row[contactID] mag wel eens opgeruimd worden");
			}
		}
	}
	if ($row['overleden'] && $row['lidToestand'] == "LID") {
		error("Lid $row[contactID] heeft geen lidtot maar is 'overleden'!");
	}

	// Controleren of lidtot ver in de toekomst ligt. Mag hooguit 365 dagen ver weg liggen
	if (isset($row['lidTot'])){
		$limitstamp = strtotime("+1 year 1 month");
		$lidtotstamp = strtotime($row['lidTot']);

		if ($lidtotstamp > $limitstamp){
			// 'lidtot' ligt te ver in de toekomst!
			error("Lid $row[contactID] heeft een lidtot ver in de toekomst: $row[lidTot]");
		}
	}
}

$res = $WSW4DB->q('TABLE SELECT Lid.contactID, lidToestand, opmerkingen, studentnr FROM Lid JOIN Persoon ON Lid.contactID = Persoon.contactID');
foreach($res as $row) {
	// Studentnummers
	if (empty($row['studentnr']))
	{
		//Sommige speciale leden hebben geen studentnummer, dat zou in de opmerkingen moeten staan
		if(@$row['lidToestand'] == 'LID' && empty($row['opmerkingen']))
		{
			error("Lid $row[contactID] heeft geen studentnr en ook geen opmerkingen om dat uit te leggen!");
		}
	}
	// Correcte syntax?
	else if(!preg_match('/[A-Z0-9][0-9]{6}/', $row['studentnr'])) 
	{
		error("Studentnummer onjuiste syntax: '$row[studentnr]' (lid $row[contactID])");
	}
	
}

// check op geen studies maar wel nog lid... dat klopt niet
// lidtot moet null zijn, we gaan er vanuit dat een einddatum in de
// toekomst wel acceptabel is.
// MAX(status) geeft ons de juiste waarde omdat Alumnus, Gestopt en Studerend
// mooi oplopend zijn voor SQL ;-)
$res = $WSW4DB->q('TABLE SELECT contactID FROM Lid
	WHERE '. WSW4_WHERE_LID .'
	AND lidTot IS NULL
	AND lidToestand NOT IN ("BAL","LIDVANVERDIENSTE","ERELID")
	AND contactID IN (
		SELECT lid_contactID FROM (
			SELECT lid_contactID, MAX(status) as maxs FROM LidStudie GROUP BY lid_contactID) a
		WHERE a.maxs != "STUDEREND")
	');

foreach($res as $row) {
	error("Lid $row[contactID] is lid, heeft geen studies en is ook geen BAL.");
}

$res = $WSW4DB->q('TABLE SELECT commissieID, login FROM Commissie');
foreach($res as $row) {
	if (!$row['login']) {
		// toch checken, want "" kan ingevuld worden. Wellicht ook alleen
		// a-z0-9 ofzo toestaan
		error("Cie $row[commissieID] heeft geen login!");
	}
}

//Check of de overerving niet de mist in gegaan is
//Als in: iemand in de contact-tabel die niet in de Lid- of Persoontabel staat maar dat wel zou moeten,
// of er juist niet in moet staan
function checkOvererving($basis, $extend, $using, $supers)
{
	global $WSW4DB;

	//Check op missende entries
	$res = $WSW4DB->q('TABLE SELECT * FROM `%l` '
		.'LEFT OUTER JOIN `%l` USING(`%l`) '
		.'WHERE `%l`.`overerving` = %s '
		.'AND `%l`.`contactID` IS NULL '
		, $basis, $extend, $using, $basis, $extend, $extend);
	foreach($res as $row) {
			error($basis . " #" . $row[$using] . " heeft overerving '" . $row['overerving'] ."' maar geen bijbehorende `" . $extend . "`-entry!");
	}

	//Check op overcomplete entries, hierdoor moet je op jezelf checken en je mogelijke superclasses
	$res = $WSW4DB->q('TABLE SELECT * FROM `%l` '
		.'LEFT OUTER JOIN `%l` USING(`%l`) '
		.'WHERE `%l`.`overerving` NOT IN %As '
		, $extend, $basis, $using, $basis, $supers);
	foreach($res as $row) {
			error($basis . " #" . $row[$using] . " heeft geen overerving naar '" . $extend ."' maar wel een `" . $extend . "`-entry!");
	}
}
//TODO: automatisering... ook andere tabellen moeten nog
checkOvererving('Contact', 'Persoon', 'contactID', array("Persoon", "Lid"));
checkOvererving('Contact', 'Lid', 'contactID', array("Lid"));
checkOvererving('Contact', 'Organisatie', 'contactID', array("Organisatie", "Bedrijf"));
checkOvererving('Contact', 'Bedrijf', 'contactID', array("Bedrijf"));
checkOvererving('Contact', 'Leverancier', 'contactID', array("Leverancier"));

if (sizeOf($errors) > 0) {
	echo implode("\n", $errors)."\n";
}

function error($msg)
{
	global $errors;
	$errors[] = $msg;
}
