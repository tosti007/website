#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id $

// aantal seconden in een week: 3600*24*7 = 604800

// alle email auth cookies wegdoen na 1 week
$WSW4DB->q('DELETE LOW_PRIORITY FROM Wachtwoord
	WHERE (UNIX_TIMESTAMP() - magicExpire) > 604800');

// expire email_bounces na 3 maanden (verander je dit, verander het dan
// ook in whoswho/bounces.php)
$WSW4DB->q('DELETE LOW_PRIORITY FROM MailingBounce
	WHERE (TO_DAYS(NOW()) - TO_DAYS(gewijzigdWanneer)) > (3*30)');



// Als lidafdatum < vandaag verander dan de status van het lid (of bal) in oudlid
$WSW4DB->q("UPDATE `Lid` SET `lidToestand` = 'OUDLID' WHERE `lidToestand` IN ('LID', 'BAL') AND `lidTot` < NOW()");

//Pak alle planners ouder dan 1 maand en gooi ze weg
$planners = $WSW4DB->q("TABLE SELECT `planner_plannerID` AS id, MAX(`momentBegin`) AS laatstedatum FROM `PlannerData` GROUP BY `planner_plannerID`");

$now = new DateTimeLocale();
foreach($planners as $row) {
	$plannerDate = new DateTimeLocale($row["laatstedatum"]);
	$diff = date_diff(new DateTimeLocale($row["laatstedatum"]), $now);
	if($diff->format("%r%a") > 31){
		$planner = Planner::geef($row["id"]);
		//Planner object heeft zelf alle logica om ook alle aanverwante entries weg te gooien, win!
		$planner->verwijderen();
	}
}
