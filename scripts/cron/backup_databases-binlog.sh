#!/bin/bash
# Maakt met behulp van binaire logs een databasebackup.
# Uit deze logs kun je SQL-statements halen met behulp van
# het programma `mysqlbinlog'.
# Zie ook:
# https://dev.mysql.com/doc/refman/8.0/en/point-in-time-recovery.html

set -e

# Configuratie:
MYSQL_LOGDIR='/var/lib/mysql/' # Directory waar binaire logs staan, met afsluitende '/'
MYSQL_LOGFORMAT='mariadb-bin' # Bestandsnaam zonder extensie van binaire logs.
MYSQL_LOGINDEX="${MYSQL_LOGDIR}${MYSQL_LOGFORMAT}.index" # Volledig pad naar index van logbestanden.

if ! [ -d /archief/www ]; then
	echo "De backupschijf moet gemount zijn op /archief en een schrijfbare subdirectory op /archief/www hebben!"
	echo "(Je wilt uiteraard niet backuppen naar dezelfde (virtuele) machine)"
	echo "FIX DIT MOEILIJK SNEL!"
	echo "Tot gauw, het backupscript"
	exit 1
fi

# Niet 2x tegelijkertijd draaien, da's eng
lockfile -r0 /var/lock/www/backup_databases.lock

cd /archief/www

newdir=`date +%Y-%m/%d`
newfile=`date +%Y-%m-%d_%H_%M`

mkdir -p $newdir
ln -snf $newdir today

# Nu het doel in orde is, gaan we bestanden kopiëren.
# Zorg ervoor dat we schone data hebben.
echo "FLUSH LOGS;" | mysql -u root
# Nu zoeken we op in welk logbestand MySQL op dit moment aan het schrijven is,
# die is namelijk (nog) niet geldig.
# TODO: maak dit robuuster, door geen outputparsing te doen in bash :P
geschreven_log=$(echo "SHOW MASTER STATUS;" | mysql -u root |
	sed '1d' |
	cut -f 1
)

while read -r logfile; do
	volledig_pad="${MYSQL_LOGDIR}${logfile}"
	base_logfile=$(basename $volledig_pad)
	if [ "$base_logfile" == "$geschreven_log" ]; then
		continue
	fi
	cp $volledig_pad "today/${newfile}.${base_logfile}"
done <$MYSQL_LOGINDEX

# Let op: als we replicatie hebben, kan een offline replicator niet meer bijlopen.
# Verzin dus een goede oplossing hiervoor indien je replicatie aanzet!
echo "PURGE BINARY LOGS TO '${geschreven_log}';" | mysql -u root

rm -f /var/lock/www/backup_databases.lock
