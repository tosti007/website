#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id$

global $errors, $BMDB;
$errors = array();

$SQLMSG=FALSE;

/***
 *	check dat node met ROOT_ID bestaat
 */
$res = $BMDB->q('MAYBEVALUE SELECT id FROM benamite WHERE id = %i'
				, ROOT_ID);
if(!$res) {
	error("BENAMITE: root node niet gevonden!");
}

/***
 *	check voor entries zonder parent
 */
$res = $BMDB->q('COLUMN SELECT id '
				.'FROM benamite '
				.'WHERE parent = 0 '
				.'  AND id != %i'
				, ROOT_ID
				);
if(count($res)) {
	error("BENAMITE: er zijn nodes zonder parent, namelijk: "
			. implode(', ', $res)
			);
}

/***
 *	check dat ROOT_ID de kindjes {site,act}_{live,publisher} heeft
 */
$res = $BMDB->q('MAYBEVALUE SELECT id '
				.'FROM benamite '
				.'WHERE name = %s '
				.'  AND parent = %i'
				, 'site_live'
				, ROOT_ID
				);
if(!$res) {
	error("BENAMITE: ROOT_ID heeft geen kindje met naam 'site_live'!");
}

/***
 *	check ofdat elke entry wel een geldige parent heeft
 */
$res = $BMDB->q('TABLE SELECT b1.id, b1.parent '
				.'FROM benamite b1 '
				.'   LEFT JOIN benamite b2 ON (b1.parent=b2.id) '
				.'WHERE b2.id IS NULL '
				.'  AND b1.id != %i '
				.'ORDER BY b1.id'
				, ROOT_ID
				);
foreach($res as $row) {
	errorEntry($row['id'], "parent ($row[parent]) ongeldig");
}

/***
 *	check dat parent een ENTRYDIR of ENTRYVARIABLE is
 */
$res = $BMDB->q('TABLE SELECT DISTINCT b1.parent '
				.'FROM benamite b1 '
				.'   LEFT JOIN benamite b2 ON (b1.parent=b2.id) '
				.'WHERE b2.type != %i AND b2.type != %i'
				.'  AND b1.id != %i '
				.'ORDER BY b1.id'
				, ENTRYDIR
				, ENTRYVARIABLE
				, ROOT_ID
				);
foreach($res as $row) {
	errorEntry($row["parent"] , "Is non-directory maar heeft toch een child?!");
}

/***
 *	check dat kindjes allemaal een andere rank hebben
 */
$res = $BMDB->q('TABLE SELECT DISTINCT b1.parent '
				.'FROM benamite b1 '
				.'   LEFT JOIN benamite b2 ON (b1.parent=b2.parent) '
				.'WHERE b1.rank = b2.rank '
				.'  AND b1.id != b2.id '
				.'ORDER BY b1.parent '
				);
foreach($res as $row) {
	$drs = $BMDB->q('COLUMN SELECT DISTINCT b1.id '
					.'FROM benamite b1 '
					.'   LEFT JOIN benamite b2 ON (b1.parent=b2.parent) '
					.'WHERE b1.parent = %i '
					.'  AND b1.rank = b2.rank '
					.'  AND b1.id != b2.id '
					.'ORDER BY b1.id'
					, $row['parent']
					);
	errorEntry($row['parent'], "heeft kindjes met de zelfde rank\n  "
			. implode(', ', $drs)
			);
}


/***
 *	Check dat de entries geldige waardes hebben.
 */
$res = $BMDB->q('TABLE SELECT * FROM benamite ORDER BY id');
foreach($res as $row) {
	if ($row['visible'] < 0 || $row['visible'] > 1) {
		errorEntry($row["id"], "hidden=$row[hidden]");
	}
	switch($row['type']) {
	case ENTRY:
		errorEntry($row['id'], " type=ENTRY");
		break;
	case ENTRYDIR:
		if(!empty($row['content']))
			errorEntry($row['id'], "Dir heeft 'content'");
		if(isset($row['special']))
			checkSpecialFile($row['id'], $row['special']);
		break;
	case ENTRYFILE:
		if(!empty($row['special']))
			errorEntry($row['id'], "File heeft 'special'");
		break;
	case ENTRYUPLOADED:
		if(!empty($row['content']))
			errorEntry($row['id'], "Upload heeft 'content'");
		if(empty($row['special']))
			errorEntry($row['id'], "Upload heeft geen 'special'");
		elseif(!file_exists('/srv/http/www/benamite_files/'.substr($row['special'], 0, 32)))
			errorEntry($row['id'], "geupload bestand is zoek");
		break;
	case ENTRYHOOK:
		if(!empty($row['content']))
			errorEntry($row['id'], "Hook met 'content'");
		if(empty($row['special']))
			errorEntry($row['id'], "Hook heeft geen 'special'");
		else
			checkSpecialFile($row['id'], $row['special']);
		break;
	case ENTRYLINK:
		if(!empty($row['content']))
			errorEntry($row['id'], "Link met 'content'");
		if(empty($row['special']))
			errorEntry($row['id'], "Link heeft geen 'special'");
		else {
			$entry = vfs::get($row['id']);
			if(substr($row['special'], 0, 1) == '/') {
				$target = hrefToReference($entry->url());
			} else {
				$target = hrefToReference($entry->getLink(), NULL, $entry->getParent());
			}
			if(strpos($entry->getLink(), '..') !== false)
			{
				errorEntry($row['id'], "geen geldige uri '".$entry->getLink()."'");
			}
			else if(!empty($target['remaining']) && !$target['entry']->isHook()) {
				if (filter_var($entry->getLink(), FILTER_VALIDATE_URL) === FALSE)
				{
					errorEntry($row['id'], "niet gevonden '".$entry->getLink()."'");
				}
			}
		}
		break;
	case ENTRYINCLUDE:
		if(!empty($row['content']))
			errorEntry($row['id'], "Include met 'content'");
		if(empty($row['special']))
			errorEntry($row['id'], "Include heeft geen 'special'");
		else
			checkSpecialFile($row['id'], $row['special']);
		break;
	case ENTRYVARIABLE:
		if (!empty($row['content']))
			errorEntry($row['id'], "Variable heeft 'content'");
		if (empty($row['special']))
			errorEntry($row['id'], "Include heeft geen 'special'");
		else
			checkSpecialFile($row['id'], $row['special']);
		break;
	default:
		error("Benamite Id=$row[id] heeft onbekend type=$row[type]");
	}
}

/***
 *	check of dat alles wat op fs bestaat ook in benamite zit
 *  disable check (wordt 's nachts gefixed door purge_uploaded_files.php)
 */
$DIR = opendir('/srv/http/www/benamite_files/');
while(($file = readdir($DIR)) && false)
{
	if($file == '.' || $file == '..')
		continue;

	$count = $BMDB->q('RETURNAFFECTED SELECT id'
					.' FROM benamite'
					.' WHERE type = %i'
					.'	AND  special like %s'
					, ENTRYUPLOADED
					, $file . '%');
	if($count == 0) {
		error("Geupload bestand '$file' heeft geen entry.");
	}
}
closedir($DIR);

/***
 * Check of alle links die becommentarieert staan in de controllers ook kloppen in benamite
 */

// Lelijk! Dit moet beter kunnen
exec('grep -srn "Controller" '.FS_ROOT.'/WhosWho4/* | grep "abstract class"', $controllers);

foreach($controllers as $c)
{
	$class = explode(' ', $c);
	$class = $class[2];
	$file = explode(':', $c);
	$file = $file[0];
	require_once($file);

	foreach(get_class_methods($class) as $methode)
	{
		$r = new ReflectionMethod($class.'::'.$methode);
		$comment = $r->getDocComment();

		if(!is_null($comment))
		{
			foreach(preg_split("/((\r?\n)|(\r\n?))/", $comment) as $line)
			{
				if(strpos($line, '@site') === false)
					continue;

				$link = explode('@site', $line);
				// Sloop speciale / doxygenkarakters eruit.
				$link = trim(str_replace(array(' ', '{', '}'), '', $link[1]));

				$entry = benamiteHrefToReference($link);

				if(!$entry || !empty($entry['remaining']))
				{
					error("BENAMITE: $class::$methode heeft als link $link maar die pagina bestaat niet in benamite");
					continue;
				}

				$entry = $entry['entry'];

				$special = $entry->getSpecial();

				$special_target = explode('/WhosWho4', $file);
				$special_target = '/WhosWho4'.($special_target[1]).":$class:$methode";

				if(is_null($special) || $special !== $special_target)
				{
					error("BENAMITE: $link heeft niet als speciaal veld $special_target");
				}
			}
		}
	}
}

#$res = $BMDB->q('COLUMN SELECT `target` '
#				.'FROM `shortcuts` ');
#
#foreach($res as $target) {
#	if(!hrefToEntry($target))
#		error("shortcut: niet gevonden '$target'");
#}

if (sizeOf($errors) > 0) {
	echo implode("\n", $errors)."\n";
}

function error($msg)
{
	global $errors;
	$errors[] = $msg;
}
function errorEntry($id, $msg)
{
	global $errors;
	$e = vfs::get($id);
	if($e->isLink()) {
		$url = $e->getParent()->url().$e->getName();
	} else {
		$url = $e->url();
	}

	$errors[] = "EntryID $id @ $url\n  $msg";
}
function checkSpecialFile($id, $special)
{
	//  $special = /path/bestand[[:class]:functie]
	$arr = explode(':', FS_ROOT . $special);
	$file = $arr[0];
	if(!is_file($file) || !is_readable($file))
		errorEntry($id, "bestand '$file' is zoek");
	if(isset($arr[1]) && !isset($arr[2])) {
		$function = $arr[1];
		if(!stripos(file_get_contents($file), 'function ' . $function)) {
			errorEntry($id, "bestand '$file' heeft geen functie ".$function);
		}
	} elseif(isset($arr[2])) {
		$class = $arr[1];
		$function = $arr[2];
		// hier stripos omdat kennelijk sommige dingen in benamite case-insensitive zijn
		if(!stripos(file_get_contents($file), 'class ' . $class)) {
			errorEntry($id, "class '".$class."' bestaat niet in " . $file);
		}
		if(!stripos(file_get_contents($file), 'function ' . $function)) {
			errorEntry($id, "class '".$class."' in '".$file."' heeft geen functie ".$function);
		}
	}
}
