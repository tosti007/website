#!/usr/bin/php -q
<?php
define('BOEKWEB2', 1);
require('script-init.php');
global $logger;

$lijst = iDealVerzameling::openAndExpired();

foreach($lijst as $transactie)
{
	$logger->info('Gevonden: ' . $transactie->getTransactieId());

	$payment = iDeal::getMolliePayment($transactie->getExterneID());
	$status = $transactie->haalStatusOp($payment);
	$klantInfo = iDealView::klantInfo($transactie);

	//Is er nu wel een eindstatus bereikt?
	if($status == 'SUCCESS')
	{
		//Regel alle administratieve dingen af die bij dit artikel horen
		$ret = $transactie->handelAf();
		$mail = "
Hallo,

Met wat vertraging is uw transactie bij A-Eskwadraat via IDEAL verwerkt.

Transactie: " . iDealView::waardeTitel($transactie) . "
Omschrijving: " . TransactieView::waardeUitleg($transactie) . "
Bedrag: € " . number_format($transactie->getBedrag(),2) . "

Met vriendelijke groet,
De WebCie";
		sendmail("www@a-eskwadraat.nl", $transactie->getEmailadres(), "iDEAL-betaling A-Eskwadraat", $mail);
		//Regel alle administratieve dingen af die bij dit artikel horen
		$transactie->handelAf();
	}
	else if($status == 'CANCELLED' || $status == 'FAILURE' || $status == 'EXPIRED') {
		//Klant op de hoogte stellen:
		sendmail("www@a-eskwadraat.nl",
			$transactie->getEmailadres(),
			_("iDEAL-betaling A-Eskwadraat"),
"Hallo,

Er heeft een mislukte transactie plaatsgevonden in ons iDEAL-systeem.

$klantInfo

U kunt contact opnemen met de organiserende commissie om het probleem op te lossen

Met vriendelijke groet,
De WebCie");
	}
	else if($status == 'OPENPOGING5') //OPEN na poging 5, we stoppen ermee
	{
		$transactie->setStatus('BANKFAILURE');
		$transactie->opslaan();
		echo("iDEAL-probleem: Halp, na vijf pogingen weet de bank nog steeds niet of transactie " . $transactie->getTransactieId() . " geslaagd is -- ABORT!!!\n");
	}
	else //OPEN na minder dan 5 pogingen, we doen nog één poging
	{
		$transactie->setStatus('OPENPOGING5');
		$transactie->setExpire(new DateTimeLocale("+1 hour"));
		$transactie->opslaan();
	}
}
