#!/usr/bin/php -q
<?php
require('script-init.php');
# $Id 

global $errors;
$errors = array();

$SQLMSG=FALSE;

$lidlimiet = Register::getValue('lidLimiet');
$aeslimiet = Register::getvalue('aesLimiet');
$aeskudos = Register::getvalue('aesKudos');

$res = $WSW4DB->q('TABLE SELECT * FROM `DibsInfo`');
foreach($res as $row) {

	$laatste = (int)$row["laatste_transactieID"];
	if ($transactie = DibsTransactie::geef($laatste)) {
		if ((int)$row["persoon_contactID"] !== $transactie->getContactContactID())
			error("Laatste transactie (nr $laatste) van lid $row[persoon_contactID] klopt niet met het lidnr in de transactie");
	}
	$pasid = (int)$row["pas_pasID"];
	if ($pas = Pas::geef($pasid)) {
		if ((int)$row["persoon_contactID"] !== $pas->getPersoonContactID())
			error("Pas $pasid hoort niet bij lid $row[persoon_contactID] maar staat er wel");
	}
	if ($row['kudos'] < 0)
		error("Lid $row[persoon_contactID] heeft een negatief aantal kudos: $row[kudos]");
}

$res = $WSW4DB->q('TABLE SELECT `Transactie`.`transactieID`, `Transactie`.`contact_contactID`, `DibsTransactie`.`pas_pasID` FROM `Transactie` JOIN `DibsTransactie` ON `Transactie`.`transactieID` = `DibsTransactie`.`transactieID` AND `Transactie`.`overerving` = \'DibsTransactie\'');
foreach($res as $row) {
	$pasid = (int)$row["pas_pasID"];
	if (0 == $pasid)
		continue;
	if ($pas = Pas::geef($pasid)) {
		if ($pas->getPersoonContactID() !== (int)$row["contact_contactID"])
			error("Pas en Lid matchen niet bij transactie $row[transactieID]");
	}
	else
		error("Pas bij transactie $row[transactieID] bestaat niet");
}

$res = $WSW4DB->q('TABLE SELECT `Transactie`.`transactieID` FROM `Transactie` JOIN `DibsTransactie` ON `Transactie`.`transactieID` = `DibsTransactie`.`transactieID`
	WHERE `DibsTransactie`.`bron` = \'BVK\' AND `Transactie`.`bedrag` > 0');
foreach($res as $row) {
	error("Transactie $row[transactieID] is een boekverkoop dat het lid kudos kost!");
}

$res = $WSW4DB->q('TABLE SELECT d.persoon_contactID, d.kudos, t.sum
					FROM `DibsInfo` as d
					LEFT JOIN (SELECT *, SUM(100 * `bedrag`) as sum FROM `Transactie` WHERE `Transactie`.`overerving` = \'DibsTransactie\' GROUP BY `contact_contactID`) as t
					ON d.persoon_contactID = t.contact_contactID
					WHERE (NOT (d.kudos = -t.sum)) OR (d.kudos > %i)
					GROUP BY d.persoon_contactID', $lidlimiet);
foreach ($res as $row)
{
// todo: check hier of het nu wel klopt, eg is er niet net een transactie voltooid
	error("Ongeldig aantal kudos bij $row[persoon_contactID]! Verwacht " . (-1 * $row['sum']) . ", gevonden $row[kudos], mag niet groter zijn dan $lidlimiet.");
}

$totaalkudos = $WSW4DB->q('VALUE SELECT SUM(`kudos`) FROM `DibsInfo`');
$geteld = $totaalkudos + $aeskudos;
if ($geteld > $aeslimiet)
	error("Er zijn te veel kudos in omloop! Verwacht $aeslimiet, gevonden $geteld.");

if ($geteld < $aeslimiet)
	error("Er zijn te weinig kudos in omloop! Verwacht $aeslimiet, gevonden $geteld.");

if (sizeOf($errors) > 0) {
	echo implode("\n", $errors)."\n";
}

function error($msg)
{
	global $errors;
	$errors[] = $msg;
}

