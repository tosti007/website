#!/usr/bin/php -q
<?php
require('script-init.php');

if (sizeOf($argv) < 3 || sizeOf($argv) > 4) {
	echo 'FOUT: Onjuist aantal argumenten!';
	echo "\nGebruik:\n";
	echo "$argv[0] <rapportbestand> <'--journaal' | rapport [logbestand]>\n\n";
	echo "rapportbestand Het bestand waarin de gegevens bewaard moeten\n"
		."               worden. Dit bestand wordt (als het bestaat) ook\n"
		."               gebruikt om te vergelijken met het rapport.\n";
	echo "--journaal     Wanneer deze optie meegegeven is, wordt alleen het\n"
		."               rapportbestand gemaild.\n";
	echo "rapport        De tekst met de nieuwste gegevens. Dit kan ook - \n"
		."               zijn om aan te geven dat uit de standaardinvoer\n"
		."               gelezen moet worden.\n";
	echo "logbestand     Het logbestand waarin het verschil wordt opgeslagen\n"
		."               tussen het rapportbestand en het rapport\n";
	return;
}

$currentdatafile = "/website-output/" . $argv[1];
$tmpdatafile = $currentdatafile . ".new.tmp";
$newdata = $argv[2];
if (isset($argv[3])) {
	$logfile = $argv[3];
}

if (!file_exists($currentdatafile)) {
	touch($currentdatafile);
}

//Moet ik direct alles laten zien wat ik heb?
if ($argv[2] == '--journaal') {
	if (filesize($currentdatafile) > 0) {
		$mailsubj = 'Integriteitscontrole foutenoverzicht';
		$mailtekst = "De oorlog is nog niet voorbij
Ach en wee en wat een pech
Het geval is namelijk dit:
De volgende fouten zijn nog niet weg:\n\n* "
			.implode('* ', file($currentdatafile))."\n\n\n";
		cronmail($mailsubj, $mailtekst);
	}
	return;
}

exec("cat $newdata > $tmpdatafile");
exec("diff -U 0 $currentdatafile $tmpdatafile", $difflines);

$erbij = $eruit = array();
foreach ($difflines as $line) {
	if (substr($line, 0, 3) == '+++' || substr($line, 0, 3) == '---') {
		continue;
	}
	switch ($line[0]) {
		case '+':
			$erbij[] = substr($line, 1);
			break;
		case '-':
			$eruit[] = substr($line, 1);
			break;
		default:
	}
}

//Is er iets veranderd?
if (sizeOf($erbij) != 0 || sizeOf($eruit) != 0) {
	$mailsubj = 'Integriteitscontrole: fouten ontdekt en opgelost';
	if (sizeOf($erbij) == 0) {
		$mailsubj = 'Integriteitscontrole: fouten opgelost';
	} else if (sizeOf($eruit) == 0) {
		$mailsubj = 'Integriteitscontrole: fouten ontdekt';
	}
	$mailtekst = '';
	if (sizeOf($erbij) > 0) {
		$mailtekst .= "Muze, bezing mij en vertel
Want in de wereld is niets perfect
Wat de schade van het geheel is en
Welke fouten zijn ONTDEKT:\n\n* "
			.implode("\n* ", $erbij)."\n\n\n";
	}
	if (sizeOf($eruit) > 0) {
		$mailtekst .= "Er is goed nieuws in aantocht
De bode brengt weer post
Met het goede bericht dat
Deze fouten zijn OPGELOST:\n\n* "
			.implode("\n* ", $eruit)."\n\n\n";
	}

	//Mail het rond
	cronmail($mailsubj, $mailtekst);
	logfile($eruit, $erbij);

	copy($tmpdatafile, $currentdatafile);
}

// Alles netjes achterlaten
unlink($tmpdatafile);


function cronmail($subject, $body)
{
	# gebruik (cron) MAILTO environment variable voor mail besteming
	# met fallback naar www

	$MAILTO = getenv('MAILTO');
	if(empty($MAILTO))
		$MAILTO = "www@a-eskwadraat.nl";

	sendmail("www@a-eskwadraat.nl", $MAILTO, $subject, $body."Groeten,\nC.R.O.N.");
}

function logfile($eruit, $erbij)
{
	global  $logfile;
	if (!$logfile) {
		return;
	}

	$handler = fopen($logfile, 'a');
	if(!$handler)
		return;

	$date = date('[Y-m-d H:i:s]');
	foreach ($eruit as $msg) {
		fwrite($handler, "$date [OPGELOST] $msg\n");
	}
	foreach ($erbij as $msg) {
		fwrite($handler, "$date [ONTDEKT] $msg\n");
	}
	fclose($handler);
}
