#!/bin/bash
set -e

# scripts/zetLive.sh [commit=origin/master] [origineleUser=$USER] [scriptStep=0]
# Zet de nieuwste commits (op de masterbranch) live.

# Het eerste argument $commit specifieert wat er precies livegezet moet worden,
# per default "origin/master" maar kan een willekeurige commit-achtige string zijn.
# (Waarbij de centrale repo dus aangeduid wordt als origin!)

# Het tweede argument $origineleUser geeft aan wie het script in werking heeft gezet,
# zodat we dat kunnen bijhouden in het mailtje enzo.

# Het derde argument $scriptStep is voor intern gebruik,
# maar een defaultwaarde van 0 zou geen problemen moeten geven.



# Configuratie:
# De map waarin de livesite staat
www="/website"

# De user als wie we alles moeten uitvoeren
serverUser="webcie-temp"

# De hostname (uit $HOSTNAME) van de machine waar de webserver draait.
sshTo="vm-www2"

# Wie we een berichtje sturen als dingen livegezet worden
mailTo="www@a-eskwadraat.nl"

# Bepaal hoe dit script aangeroepen kan worden.
# Dit doen we om als juiste user @ juiste server te werken:
# na elke verbindingsstap moeten we dit script opnieuw aanroepen.
scriptnaam="$www/scripts/zetLive.sh"

# Zet dit op een niet-lege string om debugoutput te ontvangen.
verboos=""



# Wat moet er livegezet worden?
if [ -z "$1" ]; then
	commit="origin/master"
else
	commit=$1
fi
# Van wie moet dit livegezet worden?
if [ -z "$2" ]; then
	origineleUser=$USER
else
	origineleUser=$2
fi
# Om oneindige loops te voorkomen:
# geef bij herstarten door wat er al gelukt zou moeten zijn.
if [ -z "$3" ]; then
	scriptStep=0
else
	scriptStep=$3
fi



# Overgenomen van http://www.unix.com/shell-programming-and-scripting/98889-display-runnning-countdown-bash-script.html
# Tel af in het formaat "HH:MM:SS", onderwijl een berichtje zien latend.
function countdown
{
	local OLD_IFS="${IFS}"
	IFS=":"
	local ARR=( $1 ) ; shift
	IFS="${OLD_IFS}"
	local PREFIX="$*" ; [ -n "${PREFIX}" ] && PREFIX="${PREFIX} > "
	local SECONDS=$(( ${ARR[0]#0} * 3600 + ${ARR[1]#0} * 60 + ${ARR[2]#0} ))
	local START=$(date +%s)
	local END=$((START + SECONDS))
	local CUR=$START

	while [[ $CUR -lt $END ]]
	do
		CUR=$(date +%s)
		LEFT=$((END-CUR))

		printf "\r%s%02d:%02d:%02d" \
			"${PREFIX}" $((LEFT/3600)) $(( (LEFT/60)%60)) $((LEFT%60))

		sleep 1
	done
	echo "        "
}


# Maak verbinding met de webserver.
if [[ "$HOSTNAME" != "$sshTo" ]]; then
	if (( $scriptStep > 0 )); then
		echo "Error: we zouden verbonden moeten zijn met $sshTo maar we hebben $HOSTNAME." >&2
		exit 1
	fi
	echo 'Voer hier het WebCie-wachtwoord in:'
	ssh -xt $sshTo "su $serverUser -c 'bash $scriptnaam $commit $origineleUser 2'"
	exit;
fi;

if [ -n $verboos ]; then
	echo "Verbonden met $HOSTNAME!"
fi

if [ -n $verboos ]; then
	echo "$USER-rechten verkregen!"
fi

# Ga naar de repository waar alles livegezet kan worden, en haal alle commits binnen.
cd $www
if [ -n $verboos ]; then
	git fetch -v
else
	git fetch -q
fi



# Waarschuw de gebruiker
echo "De volgende commits worden livegezet:"
git log --oneline HEAD..$commit
# ... en waarschuw de anderen.
#sendmail -s "[git] Commits worden livegezet" $mailTo <<<"Lieve WebCie,
#
#Op dit moment is $origineleUser van plan om $commit live te zetten.
#Als jullie het hier niet mee eens zijn, kunnen jullie nu nog heel hard schreeuwen.
#
#De volgende commits zullen dan livegezet worden:
#$(git log --oneline HEAD..$commit)
#
#Robotische groetjes,
#Het livezetscript
#namens $origineleUser"

echo "Even geduld terwijl de andere WebCie'ers tijd krijgen om heel hard te schreeuwen..."
countdown "00:00:30" "Ctrl-C om het script te stoppen"

# Nog wat boekhouden (we nemen dit datumformaat want het wordt in een git-tag verwerkt)
datum=$(date +"%Y-%m-%d_%H-%M-%S")

# Zet alles live!
git merge --ff-only $commit

# En onthoud het in de git-geschiedenis.
git tag -m "Livegezet op $(date) door $origineleUser." "live_${datum}_${origineleUser}"
git push --tags origin


# Volautomatisch composer draaien
cd www
./composer update
# Volautomatisch ook nog even minify draaien voor de luxe.
./minify
cd ..
# en ook de gettext updaten
cd gettext
./update
cd ..

if [ -n $verboos ]; then
	echo "Alles is livegezet, hoera!"
fi
