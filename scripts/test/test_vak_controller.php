<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

use Studie;
use Symfony\Component\HttpFoundation\Session\Session;
use Vak_Controller;
use WhosWho4\TestHuis\Fixture\SessionFixture;
use WhosWho4\TestHuis\Fixture\StudieFixture;
use WhosWho4\TestHuis\Support\vfsEntrySupport;

require_once(FS_ROOT . '/WhosWho4/Controllers/Vak.php');

assert(vfsEntrySupport::urlDiscovery($this, '/Onderwijs/Vak'));

$this->def("lege Vak-CSV invoeren geeft errors", function() {
	$regels = [];
	list($errors, $opTeSlaan) = Vak_Controller::verwerkVakCSV($regels);
	assert(count($errors), "een Vak-CSV zonder inhoud geeft errors");
});

$this->def("Vak-CSV zonder headers geeft error", function() {
	$regels = [
		"TEST,Testvak,Bob-Jan Witbaard,www@a-eskwadraat.nl,wiskunde,N,Verplicht,B,31-06-2018,25-10-2018",
	];
	list($errors, $opTeSlaan) = Vak_Controller::verwerkVakCSV($regels);
	assert(count($errors), "een Vak-CSV zonder header geeft errors");
});

$this->nieuw("vak-CSV mag komma's bevatten")
	->fixture(new StudieFixture())
	->fixture(new SessionFixture())
	->doet(function(Studie $studie, Session $mockSession) {
		// De gebruiker krijgt meldingen via Page:addMelding, daarvoor hebben we een sessie nodig.
		global $session;
		$session = $mockSession;

		// Maak een wiskundebachelorstudie aan indien die nog niet in de databaas zit.
		if (!Studie::metAfkorting('WI', 'BA'))
		{
			$studie->setSoort('WI');
			$studie->setFase('BA');
			$studie->setNaam("Wiskunde", 'nl');
			$studie->setNaam("Mathematics", 'en');
			$studie->opslaan();
		}

		$regels = [
			"Vakcode,Vaknaam,Docent,Email,Studie,Dictaat,Boek,B/M,Begindatum,Einddatum",
			"TEST2,Testvak,Bob-Jan Witbaard,www@a-eskwadraat.nl,wiskunde,N,Verplicht,B,31-06-2018,25-10-2018",
		];
		list($errors, $opTeSlaan) = Vak_Controller::verwerkVakCSV($regels);
		assert(!count($errors), print_r($errors, true));
		assert($opTeSlaan->allValid(), print_r($opTeSlaan->allErrors(), true));
	})->registreer();

$this->nieuw("Vak-CSV mag tabs bevatten")
	->fixture(new StudieFixture())
	->fixture(new SessionFixture())
	->doet(function(Studie $studie, Session $mockSession) {
		// De gebruiker krijgt meldingen via Page:addMelding, daarvoor hebben we een sessie nodig.
		global $session;
		$session = $mockSession;

		// Maak een wiskundebachelorstudie aan indien die nog niet in de databaas zit.
		if (!Studie::metAfkorting('WI', 'BA'))
		{
			$studie->setSoort('WI');
			$studie->setFase('BA');
			$studie->setNaam("Wiskunde", 'nl');
			$studie->setNaam("Mathematics", 'en');
			$studie->opslaan();
		}

	$regels = [
		"Vakcode\tVaknaam\tDocent\tEmail\tStudie\tDictaat\tBoek\tB/M\tBegindatum\tEinddatum",
		"TEST3\tTestvak\tBob-Jan Witbaard\twww@a-eskwadraat.nl\twiskunde\tN\tVerplicht\tB\t31-06-2018\t25-10-2018",
	];
	list($errors, $opTeSlaan) = Vak_Controller::verwerkVakCSV($regels);
	assert(!count($errors), print_r($errors, true));
	assert($opTeSlaan->allValid(), print_r($opTeSlaan->allErrors(), true));
})->registreer();
