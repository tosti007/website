<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

//gegeven een commissielid, kun je de functie zetten op sfeercommissaris
use Commissie;
use CommissieLid;
use DateTimeLocale;
use Lid;
use WhosWho4\TestHuis\Fixture\LidFixture;

$this->nieuw('sfeercommissaris worden')
	->fixture(new LidFixture())
	->doet(function (Lid $lid) {
		//maak nieuwe commissie
		$cie = new Commissie();
		$cie->setNaam('RixtCie');
		$cie->setDatumBegin(new DateTimeLocale());
		$cie->setLogin('rixtcie');
		$cie->setEmail('rixtcie@a-eskwadraat.nl');
		assert($cie->valid(), print_r($cie->getErrors(), true));
		//voeg rixt eraan toe
		$lid = new CommissieLid($lid, $cie);
		//pas functie van commissielid aan zodat ie sfeercommissaris is
		$lid->setFunctie('SFEER');
		assert($lid->valid(), print_r($lid->getErrors(), true));
	})->registreer();
