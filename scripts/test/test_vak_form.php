<?php
declare(strict_types=1);

use WhosWho4\TestHuis\Fixture\VakFixture;

$this->nieuw('een vak toevoegen met unieke code geeft geen melding')
	->fixture(new VakFixture())
	->doet(function (Vak $vak) {
		// Gooi eventuele overgebleven vakken met deze code weg.
		$dubbelVak = Vak::geefDoorCode('TESTUNIEK');
		if ($dubbelVak)
		{
			$dubbelVak->verwijderen();
		}

		$vak->setCode('TESTUNIEK');
		assert(!$vak->checkCode(), "een vak maken met unieke code geeft geen melding");

		$vak->opslaan();
		assert(!$vak->checkCode(), "een vak wijzigen geeft geen uniciteitprobleem met zichzelf");
	})
	->registreer();

$this->nieuw('een vak toevoegen met dezelfde vakcode geeft een melding')
	->fixture(new VakFixture())
	->fixture(new VakFixture())
	->doet(function (Vak $vak1, Vak $vak2) {
		// Gooi eventuele overgebleven vakken met deze code weg.
		$dubbelVak = Vak::geefDoorCode('TEST1');
		if ($dubbelVak)
		{
			$dubbelVak->verwijderen();
		}

		$vak1->setCode('TEST1');
		$vak1->opslaan();
		$vak2->setCode('TEST1');
		assert((bool)$vak2->checkCode(), "een vak maken met dezelfde code geeft een error");
	})
	->registreer();
