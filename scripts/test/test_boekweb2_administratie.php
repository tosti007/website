<?php

use Space\Auth\Auth;
use WhosWho4\Boekweb2\AdministratieController;
use WhosWho4\TestHuis\Fixture\GodAuthFixture;
use WhosWho4\TestHuis\Fixture\LidFixture;
use WhosWho4\TestHuis\TestRunner;
/** @var TestRunner $this */

$this->nieuw('ledenexport naar loyverse bevat een testlid en geen oudlid')
	->fixture(new LidFixture())
	->fixture(new LidFixture())
	->fixture(new GodAuthFixture())
	->doet(function (Lid $lid, Lid $oudlid, Auth $auth)
	{
		// Geef het lid een unieke naam.
		$lid->setVoornaam("Uunyieckque");
		$lid->setAchternaam("Gnaeamh");
		assert($lid->getNaam() == "Uunyieckque Gnaeamh");
		$lid->setLidToestand('LID');
		$lid->opslaan();

		// Geef het oudlid een unieke naam.
		$oudlid->setVoornaam("Uunyieckque");
		$oudlid->setAchternaam("Audthliddt");
		$oudlid->setLidToestand('OUDLID');
		$oudlid->opslaan();

		// Doe de export.
		require_once FS_ROOT . '/WhosWho4/Boekweb2/Controllers/Administratie.php';
		$controller = new AdministratieController($auth);
		$response = $controller->loyverseExport();

		// Check dat de juiste leden in de response zitten.
		assert($response instanceof CSVResponse);
		/** @var CSVResponse $response */
		$eerste = true; // om de eerste regel te skippen
		$gevonden = false; // we gaan op zoek naar $lid
		foreach ($response->getData() as $line)
		{
			// Skip de eerste regel: headers.
			if ($eerste)
			{
				$eerste = false;
				continue;
			}

			// Het lid moet erin zitten.
			if (
				$line['Naam klant'] == $lid->getNaam() ||
				$line['Notitie'] == $lid->getContactID()
			) {
				$gevonden = true;
			}

			// Het oudlid niet.
			assert($line['Notitie'] != $oudlid->getContactID());
			assert($line['Naam klant'] != $oudlid->getNaam());
		}
		assert($gevonden, "Testlid komt niet voor in de loyverse-export :(");
	})->registreer();
