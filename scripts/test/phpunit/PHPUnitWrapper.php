<?php
namespace WhosWho4\TestHuis;

use function getcwd;
use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestSuite;
use PHPUnit_Framework_TestResult;

if (!defined('BOEKWEB2')) define('BOEKWEB2', true);
if (!defined('ERROR_HANDLER_SET')) define('ERROR_HANDLER_SET', true);
require_once(__DIR__ . '/../../script-init.php');

/**
 * Klasse die WhosWho4\TestHuis-tests toegankelijk maakt voor PHPUnit.
 *
 * Om deze te gebruiken moet je deze file in je phpunit.xml als testsuite zetten.
 *
 * @package WhosWho4\TestHuis
 */
class PHPUnitWrapper extends TestSuite
{
	/**
	 * Laad tests in vanuit het TestHuis naar PHPUnit.
	 *
	 * Wordt door PHPUnit aangeroepen als we het goed doen.
	 *
	 * @param $name string De naam van deze klasse. Wordt niet gebruikt.
	 *
	 * @return null|Test
	 * Een PHPUnit-test die alle tests van het TestHuis bevat.
	 */
	public static function suite($name)
	{
		// PHPUnit raakt soms in de war als je niet canonieke paden gebruikt,
		// dus we gaan even een sanity check doen.
		// (Je kan dan dingen krijgen als dezelfde file wordt twee keer gerequired,
		// dus krijg je allemaal errors van 'function foo redefined'.)
		$cwd = getcwd();
		if ($cwd != realpath($cwd))
		{
			echo "LET OP: de huidige locatie is niet een canoniek pad!" . PHP_EOL;
			echo "Als PHPUnit problemen geeft, kan dit de verklaring zijn." . PHP_EOL;
		}

		// We besteden het inladen van tests uit aan een andere klasse.
		return new PHPUnitWrapperTest();
	}
}

/**
 * Representeert een test in het TestHuis voor PHPUnit.
 */
class PHPUnitWrapperCase implements Test
{
	protected $naam;

	/**
	 * Constructor op basis van de naam van de test.
	 *
	 * @param string $naam
	 * De naam van de test, zoals in TestRunner::getGevondenTests().
	 */
	public function __construct($naam)
	{
		$this->naam = $naam;
	}

	public function count()
	{
		return 1;
	}

	/**
	 * Wordt door PHPUnit aangeroepen om de test te runnen.
	 *
	 * Omdat het TestHuis een runner nodig heeft, doen we dat hier niet.
	 *
	 * @param PHPUnit_Framework_TestResult|null $result
	 * @return PHPUnit_Framework_TestResult
	 * @throws \Exception
	 */
	public function run(PHPUnit_Framework_TestResult $result = null)
	{
		throw new \Exception("Gebruik PHPUnitWrapperTest om tests echt te runnen!");
	}

	/**
	 * Geef de naam van de test.
	 *
	 * Nodig als PHPUnit_Framework_TestResult::addError aangeroepen wordt.
	 *
	 * @return string
	 * De naam van de test.
	 */
	public function getName()
	{
		return $this->naam;
	}
}

class PHPUnitWrapperTest implements Test
{
	/**
	 * @var TestRunner
	 * Bij het constructen wordt deze geconstrueerd.
	 */
	protected $runner;

	public function __construct()
	{
		// Start de TestRunner op.
		$vorigeDir = getcwd();
		chdir('scripts');
		$this->runner = new TestRunner();
		$this->runner->fataleErrorsAfhandelen = false;
		chdir($vorigeDir);
	}

	/**
	 * Count elements of an object
	 * @link http://php.net/manual/en/countable.count.php
	 * @return int The custom count as an integer.
	 *
	 * The return value is cast to an integer.
	 * @since 5.1.0
	 */
	public function count()
	{
		return count($this->runner->getGevondenTests());
	}

	/**
	 * Runs a test and collects its result in a PHPUnit_Framework_TestResult instance.
	 * @param PHPUnit_Framework_TestResult|null $result
	 * Een PHPUnit_Framework_TestResult-object om bij te werken. Default: maak een nieuwe.
	 * @return PHPUnit_Framework_TestResult
	 */
	public function run(PHPUnit_Framework_TestResult $result = null)
	{
		// We doen setup en teardown ongeveer als TestSuite,
		// en het runnen ongeveer als TestCase.

		// Defaultwaarde voor $result instellen.
		if ($result === null) {
			$result = new PHPUnit_Framework_TestResult();
		}
		// Zonder tests hebben we niets te doen.
		if (\count($this) == 0) {
			return $result;
		}

		// Run alle gevonden tests.
		// TODO: willen we iets doen als $result->startTestSuite($this); ?
		foreach ($this->runner->getGevondenTests() as $testNaam => $testObject) {
			if ($result->shouldStop()) {
				break;
			}

			// Start de test volgens PHPUnit.
			// Coverage moeten we handmatig starten.
			$test = new PHPUnitWrapperCase($testNaam);
			$result->startTest($test);
			$cov = $result->getCodeCoverage();
			if ($cov)
			{
				$cov->start($testNaam);
			}

			// Run de test met behulp van de TestRunner, en stop het in het resultaat.
			$uitkomst = $this->runner->runLosseTest($testNaam);
			if ($uitkomst['status'] != 'geslaagd')
			{
				$result->addError($test, $uitkomst['throwable'], 0);
			}

			// Coverage moet ook weer handmatig gestopt worden.
			if ($cov)
			{
				$cov->stop();
			}
			// En dan is de test(case) over.
			$result->endTest($test, 0);
		}

		// TODO: willen we iets doen als $result->endTestSuite($this); ?

		return $result;
	}
}
