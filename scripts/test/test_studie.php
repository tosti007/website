<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

use function is_null;
use Studie;
use WhosWho4\TestHuis\Fixture\StudieFixture;

$this->nieuw("unieke studie zoeken werkt")
	->fixture(new StudieFixture())
	->doet(function (Studie $studie) {
		// Op dit moment is er geen Wiskunde-en-toepassingenmaster,
		// en we kunnen niet zomaar de enumwaarden aanpassen,
		// dus we doen het lekker hardcoded :P
		$studie->setSoort('WIT');
		$studie->setFase('MA');
		$studie->opslaan();

		$uitDB = Studie::metAfkorting('WIT', 'MA');
		assert($uitDB == $studie, "Een studie met unieke afkorting/fase kun je terugvinden");
	});

$this->nieuw("onbestaande studie zoeken geeft null")
	->doet(function () {
		// Op dit moment is er geen Wiskunde-en-toepassingenmaster,
		// en we kunnen niet zomaar de enumwaarden aanpassen,
		// dus we doen het lekker hardcoded :P
		$uitDB = Studie::metAfkorting('WIT', 'MA');
		assert(is_null($uitDB), "Een niet-bestaande studie opzoeken geeft null");
	});

$this->nieuw("niet-unieke studie zoeken gooit error")
	->fixture(new StudieFixture())
	->fixture(new StudieFixture())
	->doet(function (Studie $studie1, Studie $studie2) {
		// Maakt niet precies uit wat we instellen, als het maar hetzelfde is.
		$studie1->setSoort('WIT');
		$studie1->setFase('MA');
		$studie1->opslaan();
		$studie2->setSoort('WIT');
		$studie2->setFase('MA');
		$studie2->opslaan();

		try
		{
			$uitkomst = Studie::metAfkorting('WIT', 'MA');
			assert(false, "Niet-unieke studies zouden error moeten geven, maar vond: " . $uitkomst);
		}
        catch (\InvalidArgumentException $e)
		{
			// We verwachten deze error wel te krijgen.
		}
	});
