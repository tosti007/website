<?php

namespace WhosWho4\TestHuis;

// Run alle unit tests.
// We gaan ervan uit dat deze code aangeroepen wordt in scripts/runTests.php
// (maar in principe is de enige magie die we doen, het dynamisch opsporen van
// tests: als je in de juiste working dir zit, dit bestand includet en dan
// runAlleTests() aanroept, moeten de tests wel werken)
// (We gaan ervan uit dat de working dir dus scripts/ is, want dan hebben we
// toegang tot script-init.php en de rest)

use const PHP_VERSION;
use function user_error;
use function version_compare;

// We moeten dit definiëren omdat we anders incompatibele bookweb-tsjak binnenkrijgen.
// TODO: kan dit beter?
if (!defined('BOEKWEB2')) define('BOEKWEB2', true);
// We willen onze eigen errorhandler instellen, dus zet de defaulthandler uit.
if (!defined('ERROR_HANDLER_SET')) define('ERROR_HANDLER_SET', true);

if (ini_get('zend.assertions' != 1))
{
	user_error('Tests gaan ervan uit dat assertions aanstaan!');
}

require_once(__DIR__ . '/../script-init.php');

// Superillegaal even geheugenlimiten opkrikken omdat code coverage door de standaardlimiet heengaat.
ini_set('memory_limit', '1G');

// FIXME: testrunner kan json geven, of naar stdout schrijven
// veralgemeniseer dit, misschien met een viewklasse ofzo?

/**
 * @brief Deze klasse zorgt ervoor dat tests uiteindelijk worden uitgevoerd.
 * Daarvoor handelt het ook het registreren van tests af.
 */
class TestRunner
{
	/**
	 * @var Test[]
	 * Alle tests die geregistreerd zijn, komen hierin terecht.
	 * Is een array testNaam => testobject.
	 *
	 * @see registreerTest
	 * @see directeTests
	 * @see uitgesteldeTests
	 * @see getGevondenTests
	 */
	private $gevondenTests = [];

	/**
	 * @var Test[]
	 * Geregistreerde tests die niet zijn uitgesteld.
	 * Is een array testNaam => testobject.
	 *
	 * @see registreerTest
	 * @see gevondenTests
	 * @see uitgesteldeTests
	 */
	private $directeTests = [];

	/**
	 * @var Test[]
	 * Geregistreerde tests die wel zijn uitgesteld.
	 * Is een array testNaam => testobject.
	 *
	 * @see registreerTest
	 * @see gevondenTests
	 * @see directeTests
	 */
	private $uitgesteldeTests = [];

	/**
	 * @var Test|null
	 * Houdt de huidig runnende test bij.
	 * Handig voor het netjes afvangen van errors,
	 * maar het is niet de bedoeling dat je dit wil gaan gebruiken voor leipe trucs.
	 * @see eetErrorOp
	 * @see runAlleTests
	 * @see alleTestResultaten
	 * @see startSubTest
	 * @see stopSubTest
	 */
	private $huidigRunnendeTest = null;

	/**
	 * @var string[]|null
	 * Houdt de testresultaten bij.
	 * Handig voor het netjes afvangen van errors,
	 * maar het is niet de bedoeling dat je dit wil gaan gebruiken voor leipe trucs.
	 * @see eetErrorOp
	 * @see runAlleTests
	 * @see huidigRunnendeTest
	 */
	private $alleTestResultaten = null;

	/**
	 * @var string|null
	 * Maybe de naam van de test die we willen discoveren.
	 * Indien null, willen we alle tests discoveren.
	 */
	public $verwachteTestNaam = null;

	/**
	 * @var bool
	 * Als we in PHPUnit draaien, hoeven we niet na te denken op fatale errors, dus staat dit op false.
	 */
	public $fataleErrorsAfhandelen = true;

	/**
	 * @brief Constructorfunctie: spoort automatisch de tests op.
	 *
	 * @param string|null $naam NULL, of de naam van een losse test om uit te voeren.
	 * Moet een key zijn in $this->gevondenTests.
	 * Kan gebruikt worden door test discovery om minder tsjak in te hoeven laden.
	 *
	 * @param string|null $testFile Null, of de naam van een testfile om te includen.
	 * Dit is niet de volledige filenaam, maar bijvoorbeeld 'mededeling'
	 * voor het bestand 'script/test/test_mededeling.php'.
	 */
	public function __construct($naam = null, $testFile = null)
	{
		$this->verwachteTestNaam = $naam;

		if (!is_null($testFile))
		{
			// Spoor alleen de gegeven testfile op.
			require("test/test_" . $testFile . ".php");
		}
		else
		{
			// Check welke tests er allemaal bestaan, en zet ze klaar om te runnen.
			foreach (glob("test/test_*.php") as $testFile)
			{
				require($testFile);
			}
		}

		$this->verwachteTestNaam = null;
	}

	/**
	 * @brief Wordt gebruikt door runAlleTests om errors op te eten.
	 *
	 * Op die manier kunnen we zoveel mogelijk tests runnen voordat alles stuk is.
	 * Gooit een PHPErrorException als de error niet dodelijk was,
	 * anders toont het de resultaten tot nu toe.
	 * Moet public zijn, anders kan de error handler er blijkbaar niet bij :s
	 *
	 * Deze functie gaat ervan uit dat we op de command line runnen.
	 *
	 * @see runAlleTests
	 *
	 * @throws PHPErrorException
	 *
	 * @return bool
	 */
	public function eetErrorOp($errno, $errstr, $errfile, $errline, $errcontext = null)
	{
		if (!(error_reporting() & $errno))
		{
			// Telt blijkbaar niet als "echte" error, dus we gaan vrolijk door...
			return false;
		}

		// E_ERROR is geset als de error fataal is, dat vereist meteen afhandelen.
		if (($errno & E_ERROR) && $this->paniekBijFataleError())
		{
			if (is_null($this->alleTestResultaten))
			{
				// We bevinden ons in een lastig parket: de error handler is stuk
				// dus we kunnen ook geen vrolijk errorberichtje tonen.
				// Dan maar op stdout gaan miepen.
				echo "Meta-error: error handler kan geen resultaten opschrijven!" . PHP_EOL;
				// En geef het op.
				return false;
			}
			// Dodelijke error, dus we moeten meteen testresultaten uitpoepen.
			$this->alleTestResultaten[$this->huidigRunnendeTest] = $errstr;
			$this->showTestResultaten();
			return true;
		}

		// Vertel runAlleTests dat er iets grondig mis ging.
		throw new PHPErrorException($errfile . ':' . $errline . ': ' . $errstr);
	}

	/**
	 * @brief Wordt gebruikt door runAlleTests om fatal errors op te eten.
	 *
	 * Op die manier kunnen we zoveel mogelijk tests runnen voordat alles stuk is.
	 * PHP Fatal errors kunnen niet op de normale manier worden opgegeten,
	 * dus moeten we het doen via register_shutdown_function.
	 * Moet public zijn, anders kan de error handler er blijkbaar niet bij :s
	 *
	 * @throws PHPErrorException
	 *
	 * @see runAlleTests
	 */
	public function eetDodelijkeErrorOp()
	{
		if (is_null($this->huidigRunnendeTest))
		{
			// Als er geen test is, dan doen we niets.
			return;
		}

		// Indien er een error is gebeurd, dan rapporteren we die.
		// Anders is er die() of exit() aangeroepen tijdens een test.
		$error = error_get_last();
		if ($error)
		{
			$this->eetErrorOp($error["type"], $error["message"], $error["file"], $error["line"]);
		}
		else
		{
			$this->eetErrorOp(E_ERROR, "Aanroep op die of exit tijdens testen", "?", "?");
		}
	}

	/**
	 * @brief Toon de resultaten van (zoveel mogelijk) tests op stdout.
	 * Wordt door runAlleTests en eetError op aangeroepen.
	 * Heeft statische state nodig in de vorm van alleTestResultaten.
	 * @return int
	 * Het aantal gefaalde tests.
	 */
	private function showTestResultaten()
	{
		// Plak er een extra witregeltje bij, voor het overzicht.
		echo PHP_EOL;

		if (count($this->alleTestResultaten) <= 0)
		{
			echo "Alle tests geslaagd!\n";
			return 0;
		}

		foreach ($this->alleTestResultaten as $test => $info)
		{
			echo "Test gefaald: $test" . PHP_EOL;
			echo "Reden: $info" . PHP_EOL;
			echo PHP_EOL;
		}
		return count($this->alleTestResultaten);
	}

	/**
	 * @brief Zorg ervoor dat we tests kunnen runnen.
	 *
	 * Dit zorgt voor dingen als error reporting.
	 *
	 * @return void
	 */
	private function initialiseerTests()
	{
		// Voorkom gênante ongelukjes.
		if (!DEBUG)
		{
			echo "Tests kunnen niet op live gedraaid worden!" . PHP_EOL;
			return;
		}

		// Vang errors netjes af zodat we zoveel mogelijk door kunnen testen.
		// TODO: als we PHP 7 gebruiken, kan dit weg en doen we catch op Throwable.
		error_reporting(E_ALL | E_STRICT);
		set_error_handler([$this, 'eetErrorOp'], E_ALL | E_STRICT);
		register_shutdown_function([$this, 'eetDodelijkeErrorOp']);
	}

	/**
	 * Stop met het afvangen van errors voor error reporting.
	 *
	 * @return void
	 */
	private function klaarMetTests()
	{
		restore_error_handler();
		$this->huidigRunnendeTest = null;
	}

	/**
	 * @brief Voer de gegeven test uit.
	 *
	 * Deze functie doet hetzelfde initialiseren als runAlleTests,
	 * dus als je alle tests wil uitvoeren is het beter die te gebruiken.
	 *
	 * @param string naam
	 * De naam van de test om uit te voeren. Moet een key zijn in $this->gevondenTests.
	 *
	 * @see runAlleTests
	 *
	 * @return array
	 * De uitkomst van de test, als array.
	 * In ieder geval is de key 'status' aanwezig, met waarden 'gefaald' of 'geslaagd'.
	 * In geval van error is de key 'info' aanwezig, met een string als waarde.
	 */
	public function runLosseTest($naam)
	{
		$this->huidigRunnendeTest = $naam;

		// Doe de errors voordat we errors op gaan eten.
		if (!isset($this->gevondenTests[$naam]))
		{
			return ['status' => 'gefaald', 'info' => "Er is geen test met naam " . $naam];
		}
		$test = $this->gevondenTests[$naam];

		$this->initialiseerTests();

		try
		{
			$test->run();
		}
		catch (\Exception $e)
		{
			// Er is iets grondig misgegaan!
			return ['status' => 'gefaald', 'info' => $e->getMessage() . "\n" . $e->getTraceAsString(), 'throwable' => $e];
		}
		catch (\Throwable $e) // Gelukkig geeft PHP 5 geen error bij ontbrekende exceptieklassen :D
		{
			// Er is iets grondig misgegaan!
			return ['status' => 'gefaald', 'info' => $e->getMessage() . "\n" . $e->getTraceAsString(), 'throwable' => $e];
		}

		$this->klaarMetTests();

		return ['status' => 'geslaagd'];
	}

	/**
	 * @brief Run de gegeven tests een voor een.
	 *
	 * Updatet de testresultaten, maar doet niet aan output.
	 * We gaan ervan uit dat deze functie op de command line wordt gerund.
	 *
	 * @param Test[] $tests Een array van de vorm [testnaam => Test-object].
	 *
	 * @return void
	 */
	public function runTests($tests)
	{
		foreach ($tests as $naam => $test)
		{
			$output = $this->runLosseTest($naam);
			if ($output['status'] == 'gefaald')
			{
				$this->alleTestResultaten[$this->huidigRunnendeTest] = $output['info'];
				progressBarGefaald();
			}
			else
			{
				progressBarGeslaagd();
			}
		}
	}

	/**
	 * @brief Itereer over alle gevonden tests en voer die uit.
	 * Testresultaten komen terecht in $this->alleTestResultaten voor eventuele errors.
	 * Maar in principe regelt de functie zelf dat die netjes worden geprint.
	 *
	 * We gaan ervan uit dat deze functie op de command line wordt gerund.
	 *
	 * @return int
	 * Het aantal gefaalde tests.
	 */
	public function runAlleTests()
	{
		$this->initialiseerTests();

		// Reset de testresultaten.
		$this->alleTestResultaten = [];

		// Voer eerst directe tests uit...
		$this->runTests($this->directeTests);
		// ... en uitgestelde tests als we alleen succes hadden.
		if (!$this->testIsGefaald())
		{
			$this->runTests($this->uitgesteldeTests);
		}

		// ruim error handlers op
		$this->klaarMetTests();

		// Sluit de progressbar af.
		echo PHP_EOL;
		return $this->showTestResultaten();
	}

	/**
	 * @brief Maak snel een nieuwe test aan en registreer deze.
	 *
	 * Wil je meer customizen, gebruik dan de methode nieuw.
	 *
	 * @see nieuw
	 *
	 * @param string $naam De (unieke) naam van de test om te maken.
	 * @param callable $functie De functie die uitgevoerd wordt als de test runt.
	 *
	 * @return Test
	 * De test die zojuist is aangemaakt.
	 */
	public function def($naam, $functie)
	{
		$builder = new TestFactory($naam, $this);
		return $builder->doet($functie)->registreer();
	}

	/**
	 * @brief Maak een nieuwe test handmatig aan.
	 *
	 * Vervolgens moet je in de gereturnde TestFactory alle settings instellen.
	 * Wil je snel een test maken, gebruik dan de methode def.
	 *
	 * @see def
	 *
	 * @param string $naam De (unieke) naam van de test om te maken.
	 *
	 * @return TestFactory
	 */
	public function nieuw($naam)
	{
		return new TestFactory($naam, $this);
	}

	/**
	 * @brief Zet de huidige test tussen de geregistreerde tests.
	 * @see gevondenTests
	 *
	 * @param string naam Een (zo goed als) unieke naam voor de test.
	 * @param Test test Een Test-object dat ge->run()d kan worden.
	 * @param bool uitgesteld Indien true, wordt deze test pas uitgevoerd nadat de andere succesvol waren.
	 */
	public function registreerTest($naam, Test $test, $uitgesteld = false)
	{
		if (array_key_exists($naam, $this->gevondenTests))
		{
			user_error("Test met naam $naam bestaat al!");
		}
		$this->gevondenTests[$naam] = $test;
		if ($uitgesteld)
		{
			$this->uitgesteldeTests[$naam] = $test;
		}
		else
		{
			$this->directeTests[$naam] = $test;
		}
	}

	/**
	 * @brief Geef alle tests die geregistreerd staan.
	 *
	 * @return array
	 * Een array van de vorm [$testNaam => $testobject].
	 */
	public function getGevondenTests()
	{
		return $this->gevondenTests;
	}

	/**
	 * @brief Bepaal of er al een test is gefaald.
	 */
	public function testIsGefaald()
	{
		return count($this->alleTestResultaten);
	}

	/**
	 * @brief Worden de tests interactief gerund op de commandline?
	 *
	 * Dit geeft bijvoorbeeld false als het in een browser gebeurt.
	 *
	 * @return bool
	 */
	public static function isInteractieveCLI()
	{
		// TODO: detecteer niet-interactieve command lines.
		// (Kijk bijvoorbeeld hoe `rg` dat doet.)
		return is_script();
	}

	/**
	 * Moet de testrunner meteen stoppen bij een fatale error?
	 *
	 * In PHP 5 wordt executie meteen gestopt zodra de error handler een E_ERROR opeet,
	 * in PHP 7 kun je doorwerken.
	 *
	 * @return bool
	 */
	private function paniekBijFataleError()
	{
		return $this->fataleErrorsAfhandelen && version_compare(PHP_VERSION, '7.0.0') < 0;
	}
}
