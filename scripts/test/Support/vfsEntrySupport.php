<?php

namespace WhosWho4\TestHuis\Support;

use Exception;

use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpFoundation\Request;

use Space\Auth\Auth;
use vfsEntry;

use WhosWho4\TestHuis\Fixture\GodAuthFixture;
use WhosWho4\TestHuis\TestRunner;
use WhosWho4\TestHuis\Fixture\ChdirFixture;
use WhosWho4\TestHuis\Fixture\ZeurmodusFixture;

/**
 * Bevat nuttige functies om tests voor Benamite (en de vfsEntry-klasse in het bijzonder) te runnen.
 *
 * Het belangrijkste nut is dat je hiermee automatisch een test kan aanmaken voor pagina's:
 * * entryDiscovery als je al een vfsEntry hebt
 * * urlDiscovery als je alleen een testbare url hebt
 *
 * @package WhosWho4\TestHuis\Support
 */
class vfsEntrySupport
{
	/**
	 * @brief Gegeven een vfsEntry, test of `$entry->display()` geen foute dingen doet.
	 *
	 * Omdat vfsEntry::display ervan uitgaat dat het aangeroepen wordt vanuit een request,
	 * zijn requestparameters nodig.
	 * Deze kun je overriden via de parameter $uriRefOverride.
	 * (Als je je code goed schrijft, hebben alleen hooks dit soort trucs nodig!)
	 *
	 * @param Auth $mockAuth
	 * Het object dat alle autorisatie-informatie bevat om de pagina te tonen.
	 * @param vfsEntry $entry
	 * De vfsEntry die getest moet worden.
	 * @param array $uriRefOverride
	 * Een array die de waarden in $uriRef overridet, met volgende defaults:
	 *  [ 'entry' => (vfsEntry) $entry // De vfsEntry om te testen. Kan niet geoverride worden.
	 *  , 'params' => (array) [] // slecht gedocumenteerd (TODO!)
	 *  , 'remaining' => (string) '' // Het stukje URL dat na de entry komt (bijvoorbeeld voor hooks).
	 *  , 'request' => (string) $entry->url() // De volledige URL van de request.
	 *  , 'access' => (bool) true // Of de huidige gebruiker de pagina mag zien. (Optioneel)
	 *  , 'rewrite' => (bool) false // Of de request-URL door Benamite is aangepast. (Optioneel)
	 *  ]
	 *
	 * @return void
	 *
	 * @throws Exception Als er een fout is bij het displayen.
	 */
	public static function testEntryDisplay(Auth $mockAuth, vfsEntry $entry, $uriRefOverride = [])
	{
		// De volgende globals moeten minstens worden ingesteld:
		// (TODO: deze zouden ook in de Request kunnen, toch?)
		global $auth, $cache, $request, $session, $uriRef;

		// Geen idee wat deze variabele precies inhoudt maar dit lijkt te werken...
		$cache = new TagAwareAdapter(new ApcuAdapter(), new ApcuAdapter());

		// Maak een mok van de request en sessie.
		// Mocht dit niet werken, fix dan je pagina en/of maak een nieuwe test.
		$request = new Request();
		$session = new CustomSession();

		// Bouw een auth-object.
		$auth = $mockAuth;

		// Stel de defaultwaarden in de uriRef in.
		$uriRef
			= ['access' => true // Want anders heeft testen geen zin.
			, 'entry' => $entry, 'params' => [] // TODO: misschien willen we hier GET-parameters parsen?
			, 'remaining' => '', 'request' => $entry->url(), 'rewrite' => false];
		// Override de uriRef met de gegeven waarden.
		// De +-operator behoudt de waarden aan de linkerkant.
		$uriRef = $uriRefOverride + $uriRef;
		if ($uriRef['entry'] != $entry)
		{
			// vfsEntry::display vereist dat de uitgevoerde entry in de uriRef staat,
			// dus gooi een error als dit niet zo is.
			// (TODO: beter om deze check in vfsEntry zelf te zetten?)
			throw new \Exception("De entry in testEntryDisplay mag niet geoverride worden:\n" . "verwacht: " . (string)$entry . "\n" . "gekregen: " . (string)$uriRef['entry']);
		}

		// Zet zeurmodus aan zodat we testbare errors krijgen ipv output.
		setZeurmodus();

		// Toon de entry maar negeer de echo's.
		$level = ob_get_level();
		ob_start();

		// We kunnen op meer manieren een response krijgen, dus we moeten try/catch doen.
		// TODO: kunnen we gemeenschappelijke code met space delen?
		try
		{
			// Toon de entry...
			$response = $entry->display();
			// ... en verkrijg eventuele output.
			$stdout = ob_get_clean();
		}
		catch (\ResponseException $responseException)
		{
			$response = $responseException->maakResponse();
			$stdout = ob_get_clean();
		}
		catch (Exception $e)
		{
			// Ruim outputbuffers ook op bij exceptions,
			// anders kan het resultaat niet geoutput worden :P
			while (ob_get_level() > $level)
			{
				ob_end_clean();
			}
			throw $e;
		}

		// Kijk of per ongeluk teveel outputbuffers aanstaan
		$buffer_verschil = ob_get_level() - $level;
		if ($buffer_verschil)
		{
			// Zorg ervoor dat outputbuffers uitstaan, maar sla op wat erin stond.
			while (ob_get_level() > $level)
			{
				$stdout .= ob_get_clean();
			}
			throw new Exception($entry->url() . " doet outputbufferen fout: " . $buffer_verschil . " keer meer begonnen dan beëindigd");
		}
		if ($stdout)
		{
			throw new Exception($entry->url() . " gaf " . strlen($stdout) . " karakters output, ruim dit op: '" . $stdout . "'");
		}
	}

	/**
	 * @brief Voeg aan de runner een test toe die testEntryDisplay aanroept op de entry.
	 *
	 * Indien de entry geen interessante display-methode heeft, gebeurt er niets.
	 * (Dit is bijvoorbeeld zo voor vfsEntryLink: die doet sowieso een redirect.)
	 *
	 * @param TestRunner $runner
	 * De TestRunner om de test aan toe te voegen.
	 * @param vfsEntry $entry
	 * De vfsEntry om te testen.
	 *
	 * @param array uriRefOverride
	 * Een array die de waarden in de globale `$uriRef` overridet.
	 * Zie documentatie van `testEntryDisplay` voor defaultwaarden van `$uriRef`.
	 *
	 * @param bool auto
	 * Indien true, is de test automatisch aangemaakt door test_benamite_contents.
	 * Door hierop te checken zal test_benamite_contents niet per ongeluk handmatig aangemaakte tests overriden.
	 *
	 * @return bool Of de test is toegevoegd.
	 *
	 * @throws Exception indien de $uriRefOverride al een entry bevat.
	 */
	public static function entryDiscovery(TestRunner $runner, vfsEntry $entry, $uriRefOverride = [], $auto = false)
	{
		$url = $entry->url();
		if (array_key_exists('request', $uriRefOverride))
		{
			$url = $uriRefOverride['request'];
		}
		// Geef in de naam aan of het een automatisch aangemaakte test is.
		// LET OP dat dit overeenkomt met de code in test_benamite_contents!
		$naam = $url . ": display";
		if ($auto)
		{
			$naam .= " (auto)";
		}

		// Skip ontoegankelijke entries.
		if (!$entry->mayAccess())
		{
			return false;
		}

		// Het oude bookweb is te legacy om hier te testen.
		// TODO: misschien willen we liever een nettere check?
		if (substr($entry, 0, strlen("/Onderwijs/Boeken/")) == "/Onderwijs/Boeken/")
		{
			return false;
		}

		switch ($entry->getType())
		{
			case ENTRY:
				throw new Exception((string)$entry . " heeft geen type!");
			case ENTRYFILE:
			case ENTRYUPLOADED:
				// TODO: maak hier misschien een aparte test voor?
				$speciaalVeld = substr($entry->getSpecial(), 0, 32);
				//assert(file_exists('/srv/http/www/benamite_files/'.$speciaalVeld));
				return false;
			case ENTRYINCLUDE:
				// We doen hier een paar capriolen omdat we alleen php-bestanden willen checken
				// en Benamite niet altijd even slimme checks heeft...
				$speciaalVeld = $entry->getSpecial();
				$onderdelen = explode(":", FS_ROOT . $speciaalVeld);
				$bestand = $onderdelen[0];

				// Bepaal het soort bestand aan de hand van de extensie.
				$pos = strrpos($bestand, '.');
				if ($pos === false)
				{
					return false;
				}
				$ext = substr($bestand, $pos + 1);

				// Check alleen php-bestanden.
				if ($ext == "php")
				{
					$runner->nieuw($naam)
						->fixture(new GodAuthFixture())
						->fixture(new ChdirFixture(FS_ROOT . '/space'))
						->fixture(new ZeurmodusFixture())
						->doet(function (Auth $auth) use ($entry, $uriRefOverride) {
							static::testEntryDisplay($auth, $entry, $uriRefOverride);
						})->registreer();
					return true;
				}
				return false;
			case ENTRYDIR:
			case ENTRYVARIABLE:
			case ENTRYHOOK:
				// Probeer de contenthook uit.
				$runner->nieuw($naam)
					->fixture(new GodAuthFixture())
					->fixture(new ChdirFixture(FS_ROOT . '/space'))
					->fixture(new ZeurmodusFixture())
					->doet(function (Auth $auth) use ($entry, $uriRefOverride) {
						static::testEntryDisplay($auth, $entry, $uriRefOverride);
					})->registreer();
				return true;
			default:
				break;
		}
		return false;
	}

	/**
	 * @brief Voeg aan de runner een test toe die testEntryDisplay aanroept op de entry met gegeven URL.
	 *
	 * Indien de entry geen interessante display-methode heeft, gebeurt er niets.
	 * (Dit is bijvoorbeeld zo voor vfsEntryLink: die doet sowieso een redirect.)
	 *
	 * @param TestRunner $runner
	 * Hieraan wordt de test toegevoegd.
	 * @param string $url De url waarop de entry te vinden is.
	 * @param bool $auto Indien true, is de test automatisch aangemaakt door test_benamite_contents.
	 * Door hierop te checken zal test_benamite_contents niet per ongeluk handmatig aangemaakte tests overriden.
	 *
	 * @return bool
	 * Geeft aan of de test succesvol is toegevoegd.
	 */
	public static function urlDiscovery(TestRunner $runner, $url, $auto = false)
	{
		// TODO: denk hier beter over na
		$naam = $runner->verwachteTestNaam;
		if (!is_null($naam))
		{
			if ($naam != $url . ': display' && $naam != $url . ': display (auto)')
			{
				return true;
			}
		}

		// Bouw de Benamite-reference op.
		$reference = hrefToReference($url);

		// Check of de reference naar een zinnige entry wijst:
		/** @var \vfsEntry $entry */
		$entry = $reference['entry'];
		// Zonder entry is er niets te testen.
		if (is_null($entry))
		{
			return false;
		}
		// PHP-haken kunnen de rest van de URL gebruiken, andere entries niet.
		if (!empty($reference['remaining']) && !$entry->isHook())
		{
			return false;
		}

		return static::entryDiscovery($runner, $entry, $reference, $auto);
	}
}
