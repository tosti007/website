<?php

namespace WhosWho4\TestHuis;

/**
 * @brief Representeert een test die uitgevoerd wordt door de TestRunner.
 */
class Test
{
	/**
	 * @brief De instances van Fixture die bij begin en eind van de test gerund worden.
	 */
	private $fixtures = [];

	/**
	 * @brief De functie die daadwerkelijk voor het testen zorgt.
	 *
	 * Parameters zijn fixtures in volgorde van toevoegen.
	 *
	 * De test telt als gefaald als deze functie een PHP-error of exceptie gooit.
	 */
	private $functie;

	/**
	 * @brief Voeg een extra Fixture toe aan de test.
	 *
	 * Gelieve dit niet te doen nadat de test al is geregistreerd ivm begrijpelijkheid.
	 *
	 * @param Fixture $fixture
	 * Een Fixture-object waarop `->erin()` en `->eruit()` aangeroepen kan worden.
	 *
	 * @see fixtures
	 *
	 * @return $this
	 */
	public function addFixture(Fixture $fixture)
	{
		$this->fixtures[] = $fixture;
		return $this;
	}

	/**
	 * @brief Stel de functie in die gerund wordt.
	 *
	 * Gelieve dit niet te doen nadat de test al is geregistreerd ivm begrijpelijkheid.
	 *
	 * @param callable $functie
	 * De nieuwe waarde van $this->functie.
	 *
	 * @see functie
	 *
	 * @return $this
	 */
	public function setFunctie($functie)
	{
		$this->functie = $functie;
		return $this;
	}

	/**
	 * @brief Voer de test uit en geef een exceptie/PHP-error bij falen.
	 *
	 * Zorgt voor fixtures, maar niet voor uitstellen van tests.
	 */
	public function run()
	{
		// Zet fixtures klaar voor uitvoeren.
		$params = [];
		foreach ($this->fixtures as $fixture)
		{
			$params[] = $fixture->erin();
		}

		// Voer de test uit!
		$testBody = $this->functie;
		try
		{
			$testBody(...$params);
		}
		finally
		{
			// Ruim fixtures op.
			// We doen dit in omgekeerde volgorde,
			// zodat de laatst geactiveerde als eerste gedeactiveerd wordt.
			/** @var Fixture $fixture */
			foreach (array_reverse($this->fixtures) as $fixture)
			{
				$fixture->eruit();
			}
		}
	}
}