<?php

namespace WhosWho4\TestHuis;

/**
 * @brief Implementeer dit in een klasse die als testfixture moet werken.
 *
 * Zoals Wikipedia zegt:
 * "[A fixture] sets up the system for the testing process by providing it with
 * all the necessary code to initialize it".
 *
 * In dit framework komt het neer op code die gerund wordt voordat de test zelf,
 * dit is de methode Fixture::erin, en code die na de test gerund wordt,
 * dit is de methode Fixture::eruit.
 * Die laatste wordt zowel gerund bij een succes als bij een gefaalde test.
 */
interface Fixture
{
	/**
	 * @brief Wordt voor elke test met deze fixture aangeroepen voor de test zelf.
	 *
	 * Kun je gebruiken om bijvoorbeeld een databaseconnectie op te zetten,
	 * of een generiek testobject aan te maken.
	 *
	 * @returns object
	 * Een waarde die als parameter aan de test wordt gegeven.
	 */
	public function erin();

	/**
	 * @brief Wordt voor elke test met deze fixture aangeroepen na de test.
	 *
	 * Kun je gebruiken om bijvoorbeeld een databaseconnectie te stoppen,
	 * of een generiek testobject weer weg te gooien.
	 */
	public function eruit();
}