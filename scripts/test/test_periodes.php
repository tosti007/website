<?php

$this->def('de huidige periodes hebben het huidige collegejaar', function() {
	$periodeColjaar = Periode::vanColjaar()->getCollegejaar();
	$periodeDatum = Periode::vanDatum()->getCollegejaar();
	$coljaar = coljaar();

	assert($periodeColjaar == $coljaar, "de periode in dit collegejaar heeft het juiste collegejaar");
	assert($periodeDatum == $coljaar, "de huidige periode heeft het juiste collegejaar");
});

$this->def('periodes in augustus hebben het vorige collegejaar', function () {
	$periode = Periode::vanDatum(new DateTimeLocale('2018-08-15'));
	$collegejaar = $periode->getCollegejaar();
	assert($collegejaar == 2017, "het collegejaar van de periode in augustus is het vorige jaar");
});
