<?php

declare(strict_types=1);

namespace WhosWho4\TestHuis;

use \Space\Space;
use \WhosWho4\TestHuis\Fixture\SessionFixture;

/** @var TestRunner $this */
$this->nieuw('bekom een response uit de Space-klasse')
	->fixture(new SessionFixture())
	->doet(function($mockSession) {
		global $session;
		$session = $mockSession;

		$space = new Space();

		$response = $space->urlResponse('/Home');

		assert($response->getStatusCode() == 200);
	})->registreer();
