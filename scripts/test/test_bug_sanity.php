<?php

// Bugs moeten dezelfde enums hebben als berichten,
// anders gaat het helemaal mis, want dan kun je de waardes niet aanpassen.
$this->def('de enums van de Bug-klasse zijn de enums van de BugBericht-klasse', function() {
	$bugStatus = Bug::enumsStatus();
	$berichtStatus = BugBericht::enumsStatus();
	assert($bugStatus == $berichtStatus, "Bug::enumsStatus moet hetzelfde geven als BugBericht::enumsStatus");

	$bugNiveau = Bug::enumsNiveau();
	$berichtNiveau = BugBericht::enumsNiveau();
	assert($bugNiveau == $berichtNiveau, "Bug::enumsNiveau moet hetzelfde geven als BugBericht::enumsNiveau");

	$bugPrioriteit = Bug::enumsPrioriteit();
	$berichtPrioriteit = BugBericht::enumsPrioriteit();
	assert($bugPrioriteit == $berichtPrioriteit, "Bug::enumsPrioriteit moet hetzelfde geven als BugBericht::enumsPrioriteit");
});
