<?php

namespace WhosWho4\TestHuis\Fixture;

use Space\Auth\GodAuth;
use Symfony\Component\HttpFoundation\Request;
use WhosWho4\TestHuis\Fixture;

/**
 * Fixture om een GodAuth te geven aan je test (bijvoorbeeld om een controller te runnen).
 *
 * Gebruikt intern nog een AuthConstantsFixture en een SessionFixture.
 * (Indien nodig kunnen we die als parameters meegeven?)
 */
class GodAuthFixture implements Fixture
{
	/**
	 * @var AuthConstantsFixture $authConstantsFixture
	 * De fixture die voor ons de AuthConstants aanmaakt.
	 */
	private $authConstantsFixture;
	/**
	 * @var SessionFixture $sessionFixture
	 * De fixture die voor ons de Session aanmaakt.
	 */
	private $sessionFixture;

	public function __construct()
	{
		$this->authConstantsFixture = new AuthConstantsFixture();
		$this->sessionFixture = new SessionFixture();
	}

	/**
	 * @inheritdoc
	 *
	 * @returns GodAuth
	 * Een waarde die als parameter aan de test wordt gegeven.
	 */
	public function erin()
	{
		$request = new Request();
		$authConstants = $this->authConstantsFixture->erin();
		$session = $this->sessionFixture->erin();

		return new GodAuth($request, $session, $authConstants, []);
	}

	/**
	 * @inheritdoc
	 */
	public function eruit()
	{
		$this->sessionFixture->eruit();
		$this->authConstantsFixture->eruit();
	}
}