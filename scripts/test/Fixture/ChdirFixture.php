<?php

namespace WhosWho4\TestHuis\Fixture;

use WhosWho4\TestHuis;

/**
 * @brief Deze fixture doet een chdir naar de gegeven working directory.
 */
class ChdirFixture implements TestHuis\Fixture
{
	/**
	 * @var string
	 * De directory waarheen wordt gechdir'd.
	 */
	private $newDir;

	/**
	 * @var string
	 * De directory waar we oorspronkelijk waren.
	 */
	private $originalDir;

	public function __construct($newDir)
	{
		$this->newDir = $newDir;
	}

	public function erin()
	{
		$this->originalDir = getcwd();
		chdir($this->newDir);
	}

	public function eruit()
	{
		chdir($this->originalDir);
	}
}

