<?php

namespace WhosWho4\TestHuis\Fixture;

use ActiviteitCategorie;
use ActiviteitHerhaling;
use ActiviteitInformatie;
use DateInterval;
use DateTimeLocale;
use WhosWho4\TestHuis;

/**
 * @brief Fixture die een activiteit en activiteitherhaling maakt.
 */
class ActHerhalingFixture implements TestHuis\Fixture
{
	/**
	 * @var ActiviteitInformatie
	 */
	private $act;
	/**
	 * @var ActiviteitHerhaling
	 */
	private $actHerhaling;

	function erin()
	{
		$this->act = new ActiviteitInformatie();
		$this->actHerhaling = new ActiviteitHerhaling($this->act);
		// Begin- en eindmoment zijn verplichte velden
		$begin = new DateTimeLocale();
		$this->act->setMomentBegin($begin);
		$eind = new DateTimeLocale();
		$eind->add(new DateInterval("P1D"));
		$this->act->setMomentEind($eind);

		// Titel is een verplicht veld
		$this->act->setTitel("Herhalende activiteit ter test", 'nl');
		$this->act->setTitel("Repeating activity to test", 'en');
		// Werftekst is een verplicht veld
		$this->act->setWerftekst("Dit is een herhalende activiteit voor unit tests, cool he?", 'nl');
		$this->act->setWerftekst("This is a repeating activity for unit tests, isn't that great?", 'en');

		// Categorie is een verplicht veld
		$cat = new ActiviteitCategorie("testcategorie", "testcategory");
		$this->act->setCategorie($cat);

		// Laat de herhaling later zijn dan het origineel
		$begin = new DateTimeLocale();
		$begin->add(new DateInterval("P2D"));
		$this->actHerhaling->setMomentBegin($begin);
		$eind = new DateTimeLocale();
		$eind->add(new DateInterval("P3D"));
		$this->actHerhaling->setMomentEind($eind);

		if (!$this->act->valid())
		{
			throw new \LogicException("De activiteit is niet geldig: " . print_r($this->act->getErrors(), true));
		}
		if (!$this->actHerhaling->valid())
		{
			throw new \LogicException("De herhaling is niet geldig: " . print_r($this->actHerhaling->getErrors(), true));
		}

		return [$this->act, $this->actHerhaling];
	}

	function eruit()
	{
		if ($this->act->getInDB())
		{
			// Verwijdert ook de herhaling.
			$this->act->verwijderen();
		}
	}
}

?>
