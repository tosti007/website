<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis\Fixture;

use Vak;
use WhosWho4\TestHuis\Fixture;

class VakFixture implements Fixture
{
	/** @var Vak */
	private $vak = null;

	/**
	 * @inheritdoc
	 *
	 * @returns Vak
	 * Een waarde die als parameter aan de test wordt gegeven.
	 */
	public function erin()
	{
		$this->vak = new Vak();
		return $this->vak;
	}

	/**
	 * @inheritdoc
	 */
	public function eruit()
	{
		if ($this->vak->getInDB())
		{
			$this->vak->verwijderen();
		}
	}
}