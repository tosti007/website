<?php

namespace WhosWho4\TestHuis\Fixture;
use ActiviteitInformatie;
use DateInterval;
use DateTimeLocale;
use momentBegin;
use WhosWho4\TestHuis;

/**
 * @brief Fixture die een activiteit aanmaakt en meegeeft aan de test.
 */
class ActFixture implements TestHuis\Fixture
{
	/**
	 * @var DateTimeLocale
	 * De DateTimeLocale waarop deze activiteit begint.
	 */
	private $momentBegin;

	/**
	 * @var ActiviteitInformatie De activiteit, wordt aangemaakt in erin en weggegooid in eruit.
	 */
	private $act;

	/**
	 * @brief Maak de fixture aan.
	 *
	 * @param DateTimeLocale|null momentBegin Het moment dat de activiteit begint. Default: nu meteen.
	 */
	function __construct($momentBegin = null)
	{
		if (is_null($momentBegin))
		{
			$momentBegin = new DateTimeLocale();
		}
		$this->momentBegin = $momentBegin;
	}

	function erin()
	{
		$this->act = new ActiviteitInformatie();
		// TODO: dit kunnen we beter in de constructor zetten als het verplicht is
		$this->act->setTitel("testactiviteit", 'nl');
		$this->act->setTitel("testactivity", 'en');

		$straks = clone $this->momentBegin;
		$straks->add(new DateInterval('P1M'));
		$this->act->setMomentBegin($this->momentBegin);
		$this->act->setMomentEind($straks);

		return $this->act;
	}

	function eruit()
	{
		if ($this->act->getInDB())
		{
			$this->act->verwijderen();
		}
	}
}