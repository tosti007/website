<?php

namespace WhosWho4\TestHuis\Fixture;

use IntroCluster;
use IntroGroep;
use Lid;
use LidStudie;
use Mentor;
use WhosWho4\TestHuis;

/**
 * @brief Fixture die een IntroGroep aanmaakt en meegeeft aan de test.
 *
 * Het object is nog niet opgeslagen bij in de test gaan.
 */
class IntroGroepFixture implements TestHuis\Fixture
{
	/**
	 * @var IntroGroep
	 * Het object, wordt aangemaakt in erin en weggegooid in eruit.
	 */
	private $object;

	/**
	 * @var IntroCluster
	 * Omdat elke IntroGroep een cluster wil, maken we die ook aan.
	 */
	private $cluster;

	function erin()
	{
		$this->cluster = new IntroCluster();
		$this->object = new IntroGroep();
		$this->object->setNaam('Q');
		$this->object->setCluster($this->cluster);

		return $this->object;
	}

	function eruit()
	{
		if ($this->object->getInDB())
		{
			$this->object->verwijderen();
		}
		if ($this->cluster->getInDB())
		{
			$this->cluster->verwijderen();
		}
	}

	/**
	 * Maak een mentor-kindjerelatie.
	 *
	 * Is niet geschikt voor non-tests want stopt allerlei extra objecten in de databaas.
	 * De Lid-objecten moeten geldig zijn (want ze worden opgeslagen).
	 *
	 * @param Lid $ouder
	 * @param Lid $kindje
	 */
	public static function maakMentorVan(Lid $ouder, Lid $kindje)
	{
		$ouder->opslaan();
		$kindje->opslaan();

		$cluster = new IntroCluster();
		$cluster->setNaam('pimpelpaars');
		$cluster->opslaan();
		$groepje = new IntroGroep();
		$groepje->setCluster($cluster);
		$groepje->setNaam('Q');
		$groepje->opslaan();
		$ouderMentor = new Mentor($ouder, $groepje);
		$ouderMentor->opslaan();
		$kindjeStudie = new LidStudie($kindje);
		$kindjeStudie->setStudie(1);
		$kindjeStudie->setGroep($groepje);
		$kindjeStudie->opslaan();
	}
}
