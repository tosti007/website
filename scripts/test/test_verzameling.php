<?php
declare(strict_types=1);

namespace WhosWho4\TestHuis;

use Boek;
use DateTimeLocale;
use Lid;
use LidVerzameling;
use TypeError;
use Vak;
use VakVerzameling;
use Verzameling;

$this->def("een Vak en een Boek mogen in een Verzameling", function() {
	// Want je wilt bijvoorbeeld een verzameling maken van alles wat via een CSV-bestand gedefinieerd is.
	$verzameling = new Verzameling();
	$verzameling->voegtoe(new Vak());
	$verzameling->voegtoe(new Boek());
});

$this->def("een Boek mag niet in een VakVerzameling", function() {
	$verzameling = new VakVerzameling();
	try
	{
		$verzameling->voegtoe(new Boek());
	}
	catch (TypeError $e)
	{
		// Goed zo: we willen deze error.
		return;
	}
    user_error("VakVerzameling::voegtoe zou een error moeten geven indien aangeroepen op een Boek");
});

$this->def("Verzameling::allErrors geeft per veld een array van fouten", function() {
	// De error is dat LidTot voor LidVan is.
	$lidVan = new DateTimeLocale('2018-06-13');
	$lidTot = new DateTimeLocale('2018-01-01');

	$foutLid1 = new Lid();
	$foutLid1->setLidVan($lidVan);
	$foutLid1->setLidTot($lidTot);

	$foutLid2 = new Lid();
	$foutLid2->setLidVan($lidVan);
	$foutLid2->setLidTot($lidTot);

	$goedLid = new Lid();

	$verzameling = new LidVerzameling();
	$verzameling->voegtoe($foutLid1);
	$verzameling->voegtoe($foutLid2);
	$verzameling->voegtoe($goedLid);

	$errors = $verzameling->allErrors();
	assert(count($errors['LidTot']) == 2, "Er zijn twee leden met foute lidTot");
});
