<?php

namespace WhosWho4\TestHuis;

/**
 * @brief Wordt gegooid door eetErrorOp als een test een PHP-error geeft.
 * Verpakt de error netjes zodat runAlleTests er iets mee kan.
 */
class PHPErrorException extends \Exception
{
}