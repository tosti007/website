#!/usr/bin/php -q
<?php
define('BOEKWEB2', true);
require('script-init.php');

// Scriptje om alle pasfoto's aan de eerstejaars te koppelen op de site
// Voor de invoer van dit bestand moet je een csv-tje invoeren
// met per regel EERST het lidnr, dan het fotonummer, verolgens moet je dit script
// aanroepen met ./pasfotos.php csvbestandje.csv
// De foto's moeten in de map /scratch/pasfotos/ staan en moeten als naam
// IMG_'id'.jpg hebben, waarbij id het foto-id is.

global $WSW4DB;

if(sizeof($argv) < 2) {
	echo("Gebruik: $argv[0] csvbestandje.csv\n");
	echo("Dit csvbestandje moet beginnen met een regel die 'lidnr'/'studentnr' en 'fotonr' bevat\n");
	exit;
}

// Om te voorkomen dat niet iemand op z'n debug foto's upload
if (DEBUG || DEMO) {
	echo "Op debugs krijg je alleen foutmeldingen, de foto's komen niet op de site!\n";
}
$nietuploaden = DEBUG || DEMO;

$file = fopen($argv[1], 'r');
$hoofd = fgetcsv($file);
// parse hoofdregel om te ontdekken hoe de data eruitziet
for ($kolom = 0; $kolom < count($hoofd); $kolom++) {
	if ($hoofd[$kolom] == "lidnr") {
		$lidnrKolom = $kolom;
	} elseif ($hoofd[$kolom] == 'studentnr') {
		$studentnrKolom = $kolom;
	} elseif ($hoofd[$kolom] == 'fotonr' || $hoofd[$kolom] == 'fotoid') {
		$fotonrKolom = $kolom;
	} elseif ($hoofd[$kolom] == 'negeer' || !$hoofd[$kolom]) {
		echo "Kolom $kolom negeren...\n";
	} elseif ($hoofd[$kolom] == 'voornaam') {
		$voornaamKolom = $kolom;
	} elseif ($hoofd[$kolom] == 'tussenvoegsel') {
		$tussenvoegselKolom = $kolom;
	} elseif ($hoofd[$kolom] == 'achternaam') {
		$achternaamKolom = $kolom;
	} else {
		echo "Kolomsoort " . $hoofd[$kolom] . " ken ik niet, probeer lidnr / studentnr / fotonr\n";
		exit;
	}
}
if (!isset($fotonrKolom)) {
	echo "Ik zie geen fotonr-kolom in het bestand, geef aan hoe je data eruitziet!\n";
	exit;
}
if (
		!isset($lidnrKolom) && !isset($studentnrKolom) &&
		(!isset($voornaamKolom) || !isset($tussenvoegselKolom) || !isset($achternaamKolom))
) {
	echo "Je csv-bestandje moet een van de volgende eisen voldoen:\n";
	echo "* heeft een kolom 'lidnr'\n";
	echo "* heeft een kolom 'studentnr'\n";
	echo "* heeft kolommen 'voornaam', 'tussenvoegsel' en 'achternaam'\n";
	exit;
}

$bestaatuploader = false;
// Er moet een uploader zijn; de foto's krijgen een tag erbij en van een Tag-object
// moet iemand de tagger zijn (je kan hier dus ook iemand anders invullen)
while(!$bestaatuploader) {
	echo("Geef lidid van de uploader: ");
	$line = fgets(STDIN);

	if($uploader = Contact::geef($line)) {
		$bestaatuploader = true;
		break;
	} else {
		echo("Uploaderlidid $line niet gevonden!\n");
	}
}

$bestaatfotograaf = false;

while(!$bestaatfotograaf) {
	echo("Geef lidid van de fotograaf: ");
	$line = fgets(STDIN);

	if($fotograaf = Contact::geef($line)) {
		$bestaatfotograaf = true;
		break;
	} else {
		echo("Fotograaflidid $line niet gevonden!\n");
	}
}

$voorzetsel = "";
while(!$voorzetsel) {
	echo("Geef aan wat er voor het fotonr komt in de bestandsnaam\n");
	echo("(Bijvoorbeeld: '/scratch/pasfotos/IMG_1337.jpg' => '/scratch/pasfotos/IMG_'\n");
	echo("> ");
	$voorzetsel = trim(fgets(STDIN));
}
$prestring = $voorzetsel;

// Poep nog een keer voor de zekerheid uit of je de juiste settings hebt
echo("Koppelingsbestand: $argv[1]\n");
echo("Uploader: " . $uploader->geefID() . "\n");
echo("Fotograaf: " . $fotograaf->geefID() . "\n");
echo("Weet je zeker dat wilt doorgaan? (j/n)\n");
$input = "";

while($input != "j\n") {
	$input = fgets(STDIN);
	if($input == "n\n") {
		exit;
	}
}

// Koppel hier alle lidnrs aan fotonrs door ze uit de csv te lezen
$koppel = array();

while($line = fgetcsv($file)) {
	$geparst = array();
	if (isset($lidnrKolom))
	{
		$geparst["lidnr"] = $line[$lidnrKolom];
	}
	if (isset($studentnrKolom))
	{
		$geparst["studentnr"] = $line[$studentnrKolom];
	}
	if (isset($voornaamKolom))
	{
		$geparst["voornaam"] = $line[$voornaamKolom];
	}
	if (isset($tussenvoegselKolom))
	{
		$geparst["tussenvoegsel"] = $line[$tussenvoegselKolom];
	}
	if (isset($achternaamKolom))
	{
		$geparst["achternaam"] = $line[$achternaamKolom];
	}
	$geparst["fotonr"] = $line[$fotonrKolom];
	$koppel[] = $geparst;
}
fclose($file);

$successen = 0;
foreach($koppel as $regelnr => $huidig) {
	$regelnr += 2; // want header en 0-based indexing

	// Probeer data in csv te koppelen met data in WhosWho, in volgorde van precisie.
	if (isset($huidig["lidnr"]) && trim($huidig['lidnr'])) {
		$lid = Contact::geef($huidig["lidnr"]);
		if (!$lid)
		{
			echo "regel $regelnr: lidnr " . $huidig['lidnr'] . " bestaat niet!\n";
			continue;
		}
	} elseif (isset($huidig["studentnr"]) && trim($huidig['studentnr'])) {
		$leden = Lid::geefLedenMetStudentnummer($huidig["studentnr"]);
		if ($leden->aantal() == 0)
		{
			echo "regel $regelnr: studentnummer bestaat niet!\n";
			continue;
		}
		elseif ($leden->aantal() > 1)
		{
			echo "regel $regelnr: studentnummer is niet uniek!\n";
			continue;
		}
		$lid = $leden->first();
	} elseif (isset($huidig['voornaam']) && isset($huidig['achternaam'])) {
		$naam = trim($huidig['voornaam']);
		$naam .= ' ' . trim($huidig['tussenvoegsel']);
		$naam = trim($naam);
		$naam .= ' ' . trim($huidig['achternaam']);

		// Hopelijk komt deze naam precies een keer voor...
		$leden = PersoonVerzameling::zoek($naam);
		if ($leden->aantal() == 0)
		{
			echo "regel $regelnr: Kan niemand vinden die `$naam' heet!\n";
			echo "lidnummer? > ";
			$lid = Lid::geef(trim(fgets(STDIN)));
		}
		elseif ($leden->aantal() > 1)
		{
			echo "regel $regelnr: Er zijn meerdere mensen die `$naam' heten!\n";
			echo "lidnummer? > ";
			$lid = Lid::geef(trim(fgets(STDIN)));
		}
		$lid = $leden->first();
	} else {
		echo "regel $regelnr: Geen lidnaam/-nummer/-studentnummer, dus geen idee wie dit is!\n";
		continue;
	}
	// Het lid moet natuurlijk wel bestaan!
	if(!$lid) {
		echo("regel $regelnr: lid bestaat niet!\n");
		continue;
	}
	$lidid = $lid->geefId();
	$fotonr = $huidig["fotonr"];
	if (!$fotonr) {
		echo("regel $regelnr: geen fotonummer gegeven, overslaan...\n");
		continue;
	}

	// Ook de foto moet op het fs staan
	$filenaam = $prestring . $fotonr . ".jpg";
	if(!file_exists($filenaam)) {
		echo("regel $regelnr: Foto $filenaam bij lid $lidid kan niet gevonden worden!\n");
		continue;
	}

	// Iemand die al een foto heeft veranderen we niet
	$profielfoto = ProfielFoto::zoekHuidig($lid);
	if($profielfoto) {
		echo("regel $regelnr: Lid $lidid heeft al een pasfoto, deze slaan we over...\n");
		$successen++; // Dit telt als een succes :P
		continue;
	}

	// Sla uploaden over op debugs.
	if ($nietuploaden)
	{
		$successen++;
		continue;
	}

	// Maak hier de nieuwe foto aan en sla m op
    $media = new Media(null, $uploader);
    $media->setFotograaf($fotograaf);
    $media->setSoort("FOTO");
    $media->setGemaakt(new DateTimeLocale());
	$media->setLock(true);
	$media->opslaan();

	$mediaid = $media->getMediaID();

	// Kijk welk fotoid bij de foto hoort en kopieer daarna de foto van scratch
	// naar de foto-map van de website
	$lastdigits = substr('00' . $mediaid, -2);
	$basedir = "/srv/fotos/Public/fotoweb/$lastdigits/";
	$file = $basedir . $mediaid . ".jpg";
	if(!copy($filenaam, $file)) {
		echo("Lid $lidid: Copying to $file failed. Exiting...");
		die();
	}

	// zet de rechten van de foto goed
	if(!chmod($file, 0664)) {
		echo("Lid $lidid: Chmod $file failed. Exiting...");
		die();
	}

	// zet de groep van de foto goed
	if(!chgrp($file, 'apache')) {
		echo("Lid $lidid: Chgrp apache $file failed. Exiting...");
		die();
	}

	// Maak de profielfoto aan en tag
	$profielfoto = new ProfielFoto($lid, $media);
	$profielfoto->setStatus("HUIDIG");
	$profielfoto->opslaan();
	$tag = new Tag();
	$tag->setMedia($media);
	$tag->setPersoon($lid);
	$tag->setTagger($uploader);
	$tag->opslaan();

	$successen++;
}
echo("Klaar!\n");
echo "We hadden " . count($koppel) . " regels verwerkt, waarvan " . $successen . " gelukt zijn\n";
