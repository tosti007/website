# $Id$


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `bestelling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bestelling` (
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `datum` date NOT NULL DEFAULT '0000-00-00',
  `levbestelnr` int(11) NOT NULL AUTO_INCREMENT,
  `leveranciernr` int(11) NOT NULL DEFAULT '0',
  `stamp_bestelling` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `aantalgeleverd` int(11) NOT NULL DEFAULT '0',
  `offertenr` int(11) DEFAULT NULL,
  `wie` int(11) DEFAULT NULL,
  PRIMARY KEY (`levbestelnr`),
  KEY `datum` (`datum`),
  KEY `EAN` (`EAN`),
  KEY `leveranciernr` (`leveranciernr`),
  KEY `offertenr` (`offertenr`),
  CONSTRAINT `bestelling_ibfk_3` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`),
  CONSTRAINT `bestelling_ibfk_4` FOREIGN KEY (`leveranciernr`) REFERENCES `leverancier` (`leveranciernr`),
  CONSTRAINT `bestelling_ibfk_5` FOREIGN KEY (`offertenr`) REFERENCES `offerte` (`offertenr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `boeken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boeken` (
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `titel` tinytext NOT NULL,
  `auteur` tinytext NOT NULL,
  `druk` tinytext,
  `uitgever` tinytext,
  `cienr` int(11) DEFAULT NULL,
  `bestelbaar` enum('Y','N') NOT NULL DEFAULT 'Y',
  `telbaar` enum('Y','N') NOT NULL DEFAULT 'Y',
  `stamp_boeken` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`EAN`),
  KEY `auteur` (`auteur`(10)),
  KEY `titel` (`titel`(10)),
  KEY `cienr` (`cienr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1 COMMENT='Moet eigenlijk artikelen heten';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `boekvak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boekvak` (
  `verplicht` enum('Y','N') NOT NULL DEFAULT 'Y',
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `vaknr` int(11) NOT NULL DEFAULT '0',
  `boekvaknr` int(11) NOT NULL AUTO_INCREMENT,
  `stamp_boekvak` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`boekvaknr`),
  KEY `EAN` (`EAN`),
  KEY `vaknr` (`vaknr`),
  CONSTRAINT `boekvak_ibfk_1` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`),
  CONSTRAINT `boekvak_ibfk_2` FOREIGN KEY (`vaknr`) REFERENCES `vak` (`vaknr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fin_rekeningafschriften`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fin_rekeningafschriften` (
  `afschriftnr` int(11) NOT NULL AUTO_INCREMENT,
  `volgnr` int(11) NOT NULL DEFAULT '0',
  `datum` date NOT NULL DEFAULT '0000-00-00',
  `rekeningnr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`afschriftnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COMMENT='Tabel waarin rekeningafschriften worden opgeslagen';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fin_rekeningmutaties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fin_rekeningmutaties` (
  `mutatienr` int(11) NOT NULL AUTO_INCREMENT,
  `afschriftnr` int(11) NOT NULL DEFAULT '0',
  `boekdatum` date NOT NULL DEFAULT '0000-00-00',
  `bedrag` double NOT NULL DEFAULT '0',
  `omschrijving` text,
  `tegenrekening` int(11) NOT NULL DEFAULT '0',
  `transactietype` enum('chip','pin') DEFAULT NULL,
  `debcrednr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mutatienr`),
  KEY `afschriftnr` (`afschriftnr`),
  CONSTRAINT `fin_rekeningmutaties_ibfk_1` FOREIGN KEY (`afschriftnr`) REFERENCES `fin_rekeningafschriften` (`afschriftnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COMMENT='Tabel waarin rekeningmutaties van de bankrekening worden opg';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `kasverschil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kasverschil` (
  `verkoopdag` date NOT NULL DEFAULT '0000-00-00',
  `volgnr` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `kasverschil` decimal(13,2) DEFAULT NULL,
  `reden` text,
  `wie` int(11) NOT NULL DEFAULT '0',
  `geldigtot` datetime DEFAULT NULL,
  PRIMARY KEY (`verkoopdag`,`volgnr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `leverancier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leverancier` (
  `naam` tinytext NOT NULL,
  `leveranciernr` int(11) NOT NULL AUTO_INCREMENT,
  `adres` tinytext,
  `email` tinytext,
  `telefoon` tinytext,
  `contactpersoon` tinytext,
  `boekenmarge` decimal(6,4) NOT NULL DEFAULT '0.0000',
  `stamp_leverancier` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rubriek` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`leveranciernr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `leverancierfactuur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leverancierfactuur` (
  `levfactuurnr` int(11) NOT NULL AUTO_INCREMENT,
  `factuurdatum` date NOT NULL DEFAULT '0000-00-00',
  `betaaldatum` date DEFAULT NULL,
  `factuurperiode_start` date DEFAULT NULL,
  `factuurperiode_eind` date DEFAULT NULL,
  `leveranciernr` int(11) NOT NULL DEFAULT '0',
  `opmerkingen` text,
  `status` enum('concept','ongeldig','definitief') NOT NULL DEFAULT 'concept',
  PRIMARY KEY (`levfactuurnr`),
  KEY `leveranciernr` (`leveranciernr`),
  CONSTRAINT `leverancierfactuur_ibfk_1` FOREIGN KEY (`leveranciernr`) REFERENCES `leverancier` (`leveranciernr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COMMENT='Tabel met facturen van leveranciers';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `leverancierfactuurregel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leverancierfactuurregel` (
  `levfactuurregelnr` int(11) NOT NULL AUTO_INCREMENT,
  `levfactuurnr` int(11) NOT NULL DEFAULT '0',
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `voorraadnr` int(11) DEFAULT '0',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `bedrag` decimal(4,2) NOT NULL DEFAULT '0.00',
  `opmerkingen` text,
  `bezwaren` text,
  `forceGeldigheid` enum('Y','N') NOT NULL DEFAULT 'N',
  `factuurperiode_start` date DEFAULT NULL,
  `factuurperiode_eind` date DEFAULT NULL,
  PRIMARY KEY (`levfactuurregelnr`),
  KEY `levfactuurnr` (`levfactuurnr`,`voorraadnr`),
  KEY `voorraadnr` (`voorraadnr`),
  CONSTRAINT `leverancierfactuurregel_ibfk_1` FOREIGN KEY (`levfactuurnr`) REFERENCES `leverancierfactuur` (`levfactuurnr`),
  CONSTRAINT `leverancierfactuurregel_ibfk_2` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 COMMENT='Hier staan alle regels in uit een factuur van een leverancie';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `levering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `levering` (
  `voorraadnr` int(11) NOT NULL DEFAULT '0',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `factuur` tinytext,
  `levbestelnr` int(11) DEFAULT NULL,
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `wie` int(11) DEFAULT NULL,
  `invoerdatum` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `wanneer` (`wanneer`),
  KEY `voorraadnr` (`voorraadnr`),
  KEY `levbestelnr` (`levbestelnr`),
  CONSTRAINT `levering_ibfk_1` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`),
  CONSTRAINT `levering_ibfk_2` FOREIGN KEY (`levbestelnr`) REFERENCES `bestelling` (`levbestelnr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `toegevoegd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dag` date NOT NULL DEFAULT '0000-00-00',
  `gebeurtenis` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `wie` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `isbn` varchar(10) CHARACTER SET latin1 DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `nieuws`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nieuws` (
  `nieuwsid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nieuws` text NOT NULL,
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `wie` int(11) DEFAULT NULL,
  PRIMARY KEY (`nieuwsid`),
  KEY `EAN` (`EAN`),
  CONSTRAINT `nieuws_ibfk_1` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `offerte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerte` (
  `offertenr` int(11) NOT NULL AUTO_INCREMENT,
  `leveranciernr` int(11) NOT NULL DEFAULT '0',
  `leveranciersoffertenr` char(11) NOT NULL,
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `levprijs` decimal(4,2) NOT NULL DEFAULT '0.00',
  `offertedatum` date NOT NULL DEFAULT '0000-00-00',
  `verloopdatum` date NOT NULL DEFAULT '0000-00-00',
  `wanneer` datetime DEFAULT NULL,
  `wie` int(11) DEFAULT NULL,
  PRIMARY KEY (`offertenr`),
  KEY `EAN` (`EAN`),
  KEY `leveranciernr` (`leveranciernr`),
  CONSTRAINT `offerte_ibfk_1` FOREIGN KEY (`leveranciernr`) REFERENCES `leverancier` (`leveranciernr`),
  CONSTRAINT `offerte_ibfk_2` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oud_offerte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oud_offerte` (
  `offertenr` int(11) NOT NULL AUTO_INCREMENT,
  `leveranciernr` int(11) NOT NULL DEFAULT '0',
  `leveranciersoffertenr` char(11) NOT NULL,
  `offertedatum` date NOT NULL DEFAULT '0000-00-00',
  `verloopdatum` date NOT NULL DEFAULT '0000-00-00',
  `wanneer` datetime DEFAULT NULL,
  `wie` int(11) DEFAULT NULL,
  PRIMARY KEY (`offertenr`)
) ENGINE=MyISAM AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oud_offerteitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oud_offerteitem` (
  `offerteitemnr` int(11) NOT NULL AUTO_INCREMENT,
  `offertenr` int(11) NOT NULL DEFAULT '0',
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `levprijs` decimal(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`offerteitemnr`),
  KEY `offertenr` (`offertenr`),
  KEY `EAN` (`EAN`)
) ENGINE=MyISAM AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oud_onverwacht`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oud_onverwacht` (
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ISBN` varchar(10) NOT NULL,
  `voorraadwijziging` int(11) NOT NULL DEFAULT '0',
  `reden` tinytext,
  `stamp_onverwacht` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `plaats` enum('voorraad','buitenvk') NOT NULL DEFAULT 'voorraad'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oud_verplaatsing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oud_verplaatsing` (
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `van` enum('voorraad','buitenvk') NOT NULL DEFAULT 'buitenvk',
  `naar` enum('voorraad','buitenvk') NOT NULL DEFAULT 'buitenvk',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `stamp_verplaatsing` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reden` tinytext,
  `wie` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pakket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pakket` (
  `pakketnr` int(11) NOT NULL AUTO_INCREMENT,
  `naam` tinytext NOT NULL,
  PRIMARY KEY (`pakketnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pakketitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pakketitem` (
  `pakketnr` int(11) NOT NULL DEFAULT '0',
  `voorraadnr` int(11) NOT NULL DEFAULT '0',
  `aantal` int(11) NOT NULL DEFAULT '1',
  `voorraadlocatie` enum('voorraad','ejbv_voorraad') NOT NULL DEFAULT 'voorraad' COMMENT 'Voorraadlocatie waarvandaan dit artikel verkocht dient te worden',
  PRIMARY KEY (`pakketnr`,`voorraadnr`),
  KEY `voorraadnr` (`voorraadnr`),
  CONSTRAINT `pakketitem_ibfk_1` FOREIGN KEY (`pakketnr`) REFERENCES `pakket` (`pakketnr`),
  CONSTRAINT `pakketitem_ibfk_2` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `papiermolen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `papiermolen` (
  `aanbiedingid` int(11) NOT NULL AUTO_INCREMENT,
  `lidnr` int(11) NOT NULL DEFAULT '0',
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `auteur` tinytext NOT NULL,
  `titel` tinytext NOT NULL,
  `druk` tinytext,
  `prijs` varchar(10) DEFAULT NULL,
  `opmerkingen` text,
  `studie` enum('ic','ik','na','wi') DEFAULT NULL,
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `verloopdatum` date NOT NULL DEFAULT '0000-00-00',
  `herinnerdatum` date DEFAULT NULL,
  PRIMARY KEY (`aanbiedingid`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `retour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retour` (
  `voorraadnr` int(11) NOT NULL DEFAULT '0',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `waarde` decimal(13,2) DEFAULT NULL,
  `stamp_retour` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `factuurnr` tinytext,
  `wie` int(11) DEFAULT NULL,
  KEY `wanneer` (`wanneer`),
  KEY `voorraadnr` (`voorraadnr`),
  CONSTRAINT `retour_ibfk_1` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `studentbestelling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentbestelling` (
  `gemailddatum` date DEFAULT NULL,
  `gekochtdatum` date DEFAULT NULL,
  `lidnr` int(11) NOT NULL DEFAULT '0',
  `boekvaknr` int(10) unsigned DEFAULT NULL,
  `bestelnr` int(11) NOT NULL AUTO_INCREMENT,
  `bestelddatum` date NOT NULL DEFAULT '0000-00-00',
  `prioriteit` tinyint(4) NOT NULL DEFAULT '0',
  `stamp_studentbestelling` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `vervaldatum` date DEFAULT NULL,
  `vervalreden` tinytext,
  `wie` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`bestelnr`),
  KEY `lidnr` (`lidnr`),
  KEY `EAN` (`EAN`,`prioriteit`,`bestelddatum`),
  KEY `boekvaknr` (`boekvaknr`),
  CONSTRAINT `studentbestelling_ibfk_1` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `studievak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studievak` (
  `vaknr` int(11) NOT NULL DEFAULT '0',
  `studie` enum('ic','ik','na','wi') NOT NULL DEFAULT 'ic',
  `jaar` enum('bachelor','master') NOT NULL DEFAULT 'bachelor',
  `stamp_studievak` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vaknr`,`studie`,`jaar`),
  CONSTRAINT `studievak_ibfk_1` FOREIGN KEY (`vaknr`) REFERENCES `vak` (`vaknr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `telling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telling` (
  `tellingnr` int(11) NOT NULL AUTO_INCREMENT,
  `telling_start` datetime DEFAULT NULL,
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lidnr` int(11) DEFAULT NULL,
  `opmerkingen` text,
  PRIMARY KEY (`tellingnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tellingitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tellingitem` (
  `tellingnr` int(11) NOT NULL DEFAULT '0',
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `aantal_voorraad` int(11) NOT NULL DEFAULT '0',
  `aantal_buitenvk` int(11) NOT NULL DEFAULT '0',
  `aantal_ejbv_voorraad` int(11) NOT NULL DEFAULT '0' COMMENT 'Aantal geteld uit EJBV-voorraad',
  PRIMARY KEY (`tellingnr`,`EAN`),
  KEY `EAN` (`EAN`),
  CONSTRAINT `tellingitem_ibfk_1` FOREIGN KEY (`tellingnr`) REFERENCES `telling` (`tellingnr`),
  CONSTRAINT `tellingitem_ibfk_2` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `terugkoop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terugkoop` (
  `terugkoopnr` int(11) NOT NULL AUTO_INCREMENT,
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `stamp_terugkoop` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lidnr` int(11) DEFAULT NULL,
  `aantal` int(11) NOT NULL DEFAULT '1',
  `verkoopnr` int(11) NOT NULL DEFAULT '0',
  `studprijs` decimal(13,2) NOT NULL DEFAULT '0.00',
  `betaalmethode` enum('pin','contant','chip','debiteur','crediteur','pinII','pin1','pin2') DEFAULT NULL,
  `wie` int(11) DEFAULT NULL,
  `voorraadnr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`terugkoopnr`),
  KEY `wanneer` (`wanneer`),
  KEY `voorraadnr` (`voorraadnr`),
  KEY `verkoopnr` (`verkoopnr`),
  CONSTRAINT `terugkoop_ibfk_1` FOREIGN KEY (`verkoopnr`) REFERENCES `verkoop` (`verkoopnr`),
  CONSTRAINT `terugkoop_ibfk_2` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vak` (
  `naam` tinytext NOT NULL,
  `afkorting` varchar(16) DEFAULT NULL,
  `collegejaar` year(4) NOT NULL DEFAULT '0000',
  `docent` tinytext,
  `email` tinytext,
  `beginweek` int(11) NOT NULL DEFAULT '0',
  `eindweek` int(11) NOT NULL DEFAULT '0',
  `begindatum` date NOT NULL DEFAULT '0000-00-00',
  `einddatum` date NOT NULL DEFAULT '0000-00-00',
  `url` tinytext,
  `vaknr` int(11) NOT NULL AUTO_INCREMENT,
  `dictaat` enum('Y','N') NOT NULL DEFAULT 'N',
  `stamp_vak` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vaknr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `verkoop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verkoop` (
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lidnr` int(11) DEFAULT NULL,
  `aantal` int(11) NOT NULL DEFAULT '1',
  `voorraadlocatie` enum('voorraad','ejbv_voorraad') NOT NULL DEFAULT 'voorraad' COMMENT 'Voorraadlocatie waarvandaan de verkoop heeft plaatsgevonden',
  `studprijs` decimal(13,2) NOT NULL DEFAULT '0.00',
  `btw` varchar(13) DEFAULT NULL COMMENT 'BTW die bij deze verkoop hoort, kan NULL, 0 of een ander bedrag zijn.',
  `betaalmethode` enum('pin','contant','chip','debiteur','crediteur','pinII','pin1','pin2') DEFAULT NULL,
  `verkoopnr` int(11) NOT NULL AUTO_INCREMENT,
  `wie` int(11) DEFAULT NULL,
  `voorraadnr` int(11) NOT NULL DEFAULT '0',
  `Geslaagd` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`verkoopnr`),
  KEY `lidnr` (`lidnr`),
  KEY `wanneer` (`wanneer`),
  KEY `voorraadnr` (`voorraadnr`),
  CONSTRAINT `verkoop_ibfk_1` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `verkoopgeld`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verkoopgeld` (
  `verkoopdag` date NOT NULL DEFAULT '0000-00-00',
  `volgnr` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `verkoper` int(11) NOT NULL DEFAULT '0',
  `pin` decimal(13,2) DEFAULT NULL,
  `pinII` decimal(13,2) DEFAULT NULL,
  `pin1` decimal(13,2) DEFAULT NULL,
  `pin2` decimal(13,2) DEFAULT NULL,
  `chip` decimal(13,2) DEFAULT NULL,
  `facturen` decimal(13,2) DEFAULT NULL,
  `debiteuren` decimal(13,2) DEFAULT NULL,
  `crediteuren` decimal(13,2) DEFAULT NULL,
  `cash` decimal(13,2) DEFAULT NULL,
  `donaties` decimal(13,2) DEFAULT NULL,
  `kasverschil` decimal(13,2) DEFAULT NULL,
  `mutaties` decimal(13,2) DEFAULT NULL,
  `sluitstamp` datetime DEFAULT NULL,
  `openstamp` datetime DEFAULT NULL,
  `kv_reden` text,
  `geldigtot` datetime DEFAULT NULL,
  PRIMARY KEY (`verkoopdag`,`volgnr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `verplaatsing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verplaatsing` (
  `wanneer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `voorraadnr` int(11) NOT NULL DEFAULT '0',
  `van` enum('voorraad','buitenvk','ejbv_voorraad') NOT NULL DEFAULT 'buitenvk',
  `naar` enum('voorraad','buitenvk','ejbv_voorraad') NOT NULL DEFAULT 'buitenvk',
  `aantal` int(11) NOT NULL DEFAULT '0',
  `stamp_verplaatsing` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reden` tinytext,
  `wie` int(11) DEFAULT NULL,
  KEY `voorraadnr` (`voorraadnr`),
  CONSTRAINT `verplaatsing_ibfk_1` FOREIGN KEY (`voorraadnr`) REFERENCES `voorraad` (`voorraadnr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `voorraad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voorraad` (
  `voorraadnr` int(11) NOT NULL AUTO_INCREMENT,
  `EAN` bigint(13) unsigned zerofill NOT NULL DEFAULT '0000000000000',
  `levprijs` decimal(13,2) DEFAULT NULL,
  `adviesprijs` decimal(13,2) DEFAULT NULL,
  `btwtarief` enum('0','6','21') NOT NULL DEFAULT '0',
  `btw` decimal(13,2) DEFAULT NULL,
  `voorraad` int(11) NOT NULL DEFAULT '0',
  `buitenvk` int(11) NOT NULL DEFAULT '0',
  `ejbv_voorraad` int(11) NOT NULL DEFAULT '0' COMMENT 'Voorraad gereserveerd voor EJBV',
  `leveranciernr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`voorraadnr`),
  UNIQUE KEY `Wolfje` (`EAN`,`levprijs`,`leveranciernr`,`adviesprijs`),
  KEY `leveranciernr` (`leveranciernr`),
  CONSTRAINT `voorraad_ibfk_1` FOREIGN KEY (`EAN`) REFERENCES `boeken` (`EAN`),
  CONSTRAINT `voorraad_ibfk_2` FOREIGN KEY (`leveranciernr`) REFERENCES `leverancier` (`leveranciernr`)
) ENGINE=InnoDB AUTO_INCREMENT=_____ DEFAULT CHARSET=utf8 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

