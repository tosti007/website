#!/usr/bin/env python3

"""debugconfig: Scriptje om de configuratie van debugomgevingen te genereren.

De webserver moet namelijk weten dat $BLA-debug.a-eskwadraat.nl
doorverwijst naar een bepaalde PHP/Python-installatie enzo.

Instellingen worden via de configuratiebestanden ingelezen.
"""

import subprocess

import whoswhopy.config as config

# In feite is dit niets meer dan een YAML-naar-ander-formaat-parser,
# voor een Bashscript dat de daadwerkelijke templating doet.
# TODO: op den duur willen we misschien de hele configuratie
# in hetzelfde script instellen?
regels = []
for webcielid in config.geef('webcie'):
    regels.append('{} {} {}'.format(webcielid.login, webcielid.debugsite, webcielid.mountpoint))
script_input = '\n'.join(regels)

subprocess.run(['./config/vm-www-debug/httpd/conf.d/wwwdebug.sh', './config/vm-www-debug/httpd/conf.d/wwwdebug.conf'], input=script_input.encode('ascii'))
