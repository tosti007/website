#!/usr/bin/env python3

"""maak-account: Scriptje om een account op een lege debugomgeving te maken,
zodat je de debugomgeving in je browser kan gaan testen.

Instellingen worden via de input-functie ingelezen.
"""

import getpass

import whoswhopy.config as config
import whoswhopy.database as database
from whoswhopy.gen.Lid import Lid
from whoswhopy.gen.Wachtwoord import Wachtwoord
import whoswhopy.wachtwoord as wachtwoord

if not config.debug():
	raise Exception('Dit script mag alleen op een debug gerund worden!')

account_naam = input('Geef de naam van je nieuwe account: ')
account_wachtwoord = wachtwoord.genereer(getpass.getpass('Geef het wachtwoord: '))

# TODO: maak een functie die gewoon een lid maakt zonder gezeur
account_lid = Lid(
    contactID=int(input('Geef het contactID (Hint: dit wil je waarschijnlijk je eigen lidnummer hebben): ')),
    aes2rootsOpsturen=False,
    achternaam=input('Geef de achternaam: '),
    lidExchange=False,
    lidMaster=False,
    lidToestand='LID',
    voornaamwoord_woordID=0,
)
account_wachtwoord = Wachtwoord(login=account_naam, crypthash=account_wachtwoord)
account_wachtwoord.persoon = account_lid
database.wsw4db.add(account_lid)
database.wsw4db.add(account_wachtwoord)
database.wsw4db.commit()
