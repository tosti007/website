<?php

/**
 * Dit script zet alle bugs uit BugWeb in GitLab d.m.v. de API.
 */

require('script-init.php');

const GITLAB_INSTANCE = "https://gitlab.com";

const WEBCIE_ID = 61;

/**
 * Een map van commissie-id's naar GitLabproject-id's.
 */
const ISSUE_TARGETS = [
	// WebCie
	WEBCIE_ID => 8061599,
	// Sysop
	42 => 10769055,
	// Overige commissies
	'OVERIG' => 10769073
];

const LABELKLEUREN = [
	'level' => '#8e44ad',
	'prioriteit' => '#d9534f',
	'status' => '#5cb85c',
	'type' => '#428bca',
	'categorie' => '#f2c224'
];

const LABELS = [
	'level' => [
		'BEGINNER' => 'Makkelijk 👶',
		'GEMIDDELD' => 'Gemiddeld 😊',
		'EXPERT' => 'Expert 🤓'
	],

	'prioriteit' => [
		'ONBENULLIG' => 'Onbenullig 💤',
		'LAAG' => 'Laag 🤫',
		'GEMIDDELD' => 'Gemiddeld 🤔',
		'HOOG' => 'Hoog 😖',
		'URGENT' => 'Urgent 🔥',
	],

	// Sommige statussen zijn in GL niet meer zinnig, dus die krijgen geen label.
	// Ook is niet elke 'status' echt een status, maar eerder een type issue;
	// vandaar dat de structuur net iets anders is.
	'status' => [
		'OPEN' => ['type', 'Bug 🐞'],
		'BEZIG' => ['status', 'Bezig 🕴️'],
		'OPGELOST' => null,
		'GECOMMIT' => null,
		'UITGESTELD' => ['status', 'Uitgesteld 📅'],
		'GESLOTEN' => null,
		'BOGUS' => ['type', 'Geen bug 🙅'],
		'DUBBEL' => ['type', 'Dubbel ♊'],
		'WANNAHAVE' => ['type', 'Feature ✨'],
		'FEEDBACK' => ['status', 'Meer info nodig ❓'],
		'WONTFIX' => ['status', 'Won\'t Fix 🤷']
	]
];

const CATEGORIELABELS = [
	'FotoWeb2' => 'FotoWeb 2',
	'Introsmurf/DiLI' => 'Intro',
	'Niet geclassificeerd' => null,
	'Overig' => null,
	'Project SOEPMES' => 'Soepmes',
	'Website' => null,
	'Who\'s Who - Scripts' => 'Who\'s Who',
	'Who\'s Who IV' => 'Who\'s Who',
];

function main() {
	$GITLAB_TOKEN = getenv("GITLAB_TOKEN")
		?: sla("Je moet een Gitlab auth token mee geven, slimpie!\n");

	$gitlab = \Gitlab\Client::create(GITLAB_INSTANCE)
		->authenticate($GITLAB_TOKEN, \Gitlab\Client::AUTH_HTTP_TOKEN);

	$projects = [];

	foreach (ISSUE_TARGETS as $cieID => $projectId) {
		$projects[$cieID] = \Gitlab\Model\Project::fromArray(
			$gitlab, $gitlab->projects()->show($projectId)
		);
	}

	$argv = $_SERVER['argv'];

	if (count($argv) > 1) {
		$command = $argv[1];
	} else {
		sla("Je moet een commando geven, slimpie! (clear, migrate, of fix-references).\n");
	}

	switch ($command) {
	case 'clear':
		foreach ($projects as $project) {
			echo $project->web_url . ":\n";

			deleteAlleIssues($project);
			deleteAlleLabels($project);

			echo "\n";
		}
		break;
	case 'migrate':
		foreach ($projects as $cieID => $project) {
			$cie = Commissie::geef($cieID);
			$cieNaam = $cie ? $cie->getNaam() : 'overig';

			echo $project->web_url . " (" . $cieNaam . ") :\n";
			voegAlleLabelsToe($project, $cie);
		}

		$nieuweIDs = voegAlleBugsToe($projects);

		$file = fopen('nieuwe-ids.json', 'w');
		fwrite($file, json_encode($nieuweIDs));
		fclose($file);
		break;
	case 'fix-references':
		$nieuweIDs = json_decode(file_get_contents('nieuwe-ids.json'), true);
		foreach ($projects as $project) {
			fixAlleBugLinks($project, $nieuweIDs);
		}
		break;
	default:
		sla("You can't get ye '$command'. Probeer clear, migrate, of fix-references.\n");
		break;
	}
}

/**
 * @brief Maak alle labels die we gaan gebruiken aan.
 *
 * @param cie De commissie waar we labels voor maken.
 */
function voegAlleLabelsToe($project, $cie) {
	echo "Alle labels toevoegen ...\n";

	foreach (LABELS as $groep => $labels) {
		foreach ($labels as $waarde => $labelDesc) {
			if (is_null($labelDesc)) {
				continue;
			}

			if (is_array($labelDesc)) {
				$soort = $labelDesc[0];
				$naam = $labelDesc[1];
			} else {
				$soort = $groep;
				$naam = $labelDesc;
			}

			$project->addLabel($naam, LABELKLEUREN[$soort]);
			echo "Label '$naam' toegevoegd.\n";
		}
	}

	if (!$cie) {
		$nietOverig = array_filter(array_keys(ISSUE_TARGETS),
			function ($c) { return $c !== 'OVERIG'; });
		$nietOverig = array_map(function ($c) { return Commissie::geef($c); },
			$nietOverig);
		// PHP PLEASE, WAAROM IS DE PARAMETERVOLGORDE NIET CONSISTENT.

		$categorieen = BugCategorieQuery::table()
			->whereNotInProp('commissie', $nietOverig);
	} else {
		$categorieen = BugCategorieQuery::table()
			->whereProp('commissie', $cie);
	}

	$categorieen = $categorieen->verzamel();

	$toegevoegd = [];

	foreach ($categorieen as $categorie) {
		$naam = $categorie->getNaam();

		if ($cie && $cie->getCommissieID() === WEBCIE_ID
			&& array_key_exists($naam, CATEGORIELABELS)) {
			$label = CATEGORIELABELS[$naam];
		} else {
			$label = $naam;
		}

		if (is_null($label) || in_array($label, $toegevoegd)) {
			continue;
		}

		// GitLab wil geen komma's in labelnamen.
		$label = str_replace(',', '', $label);

		$project->addLabel($label, LABELKLEUREN['categorie']);
		$toegevoegd[] = $label;

		echo "Label '$label' toegevoegd.\n";
	}

	echo "Klaar.\n";
}

/**
 * @brief Geef een dictionary die per SVN-revisienummer aangeeft bij welke
 *     commit die hoort
 */
function svnRevisies() {
	echo "Revisie-commit relaties inlezen...\n";

	$revisies = [];

	// We laten Git alle commits geven: eerst de hele hash, dan een newline
	// gevolgd door de title + body, en een NUL byte als delimiter.
	$commits = popen('git log --all --format="%H%n%B%x00"', 'r');

	while (($entry = stream_get_line($commits, PHP_INT_MAX, "\0\n")) !== false) {
		// De eerste regel is de hash, dus die lezen we ook nog.
		$split = explode("\n", $entry, 2);
		$commit = $split[0];
		$message = $split[1];

		// De SVN import heeft speciale markers in de body gezet waar het
		// oorspronkelijke revisienummer in staat.
		if (preg_match('/git-svn-id: https:\/\/(www|svn).a-eskwadraat.nl\/svn\/www\/trunk@(\\d+)/', $message, $groups)) {
			$rev = $groups[2];
			$revisies[$rev] = $commit;
		}
	}

	echo "Klaar.\n";

	return $revisies;
}

/**
 * @brief Voeg alle bugs in BugWeb toe aan het bijbehorende project op GitLab
 *
 * @param projects Map van comissie-id naar `\Gitlab\Model\Project` waar de 
 *     issues moeten worden toegevoegd.
 *
 * @returns Een associative array die aangeeft welke issue-IDs de bugs nu hebben.
 */
function voegAlleBugsToe($projects) {
	$svnRevisies = svnRevisies();

	echo "Alle bugs ophalen...\n";

	$nieuweIDs = [];

	// Geef een iterator over alle bugs en hun categorie.
	$bugs = BugQuery::table()
		->orderByAsc('bugID')
		->verzamelIter();

	$bugs = appendIter([
		// Dit is een voorbeeld van een bug die gesloten en weer geopend is.
		Bug::geef(6070),
		// Dit is een voorbeeld van een bug die een lidreferentie heeft (e.g. +1234).
		Bug::geef(7691),
		// Dit is een voorbeeld van een bug die een bugreferentie heeft (e.g. #1234).
		Bug::geef(7244),
		// (En de bug waarnaar verwezen.)
		Bug::geef(7312),
		// Een bug met SVN-revisieverwijzingen in het bericht.
		Bug::geef(7170)
	], $bugs);

	echo "Klaar.\n";

	echo "Alle bugs in GitLab stoppen...\n";

	foreach ($bugs as $bug) {
		$bugID = $bug->getBugID();
		$cieID = $bug->getCategorie()->getCommissieCommissieID();

		if (array_key_exists($cieID, $projects)) {
			$project = $projects[$cieID];
		} else {
			$project = $projects['OVERIG'];
		}

		$issue = voegBugToe($project, $bug, $svnRevisies);
		$nieuweIDs[$bugID] = [$project->id, $issue->iid];
	}

	echo "Klaar.\n";

	return $nieuweIDs;
}

/**
 * @brief Voeg een bug in BugWeb toe aan een project op GitLab
 *
 * @param project Het `\Gitlab\Model\Project` waar de issues moeten worden
 *     toegevoegd.
 * @param bug De BugWeb-bug die in een issue moet worden omgezet.
 * @param svnRevisies Dictionary die per SVN-revisie aangeeft bij welke commit
 *     die hoort.
 *
 * @returns De gecreëerde issue.
 */
function voegBugToe($project, $bug, $svnRevisies) {
	global $bugStatusSets;

	$issue = $project->createIssue($bug->getTitel(), [
		'created_at' => $bug->getMoment()->format('c'),
		'description' => formatBug($bug, $svnRevisies),
		'labels' => implode(",", labels($bug, $project)),
		'confidential' => isConfidential($bug)
	]);

	$berichten = $bug->getReacties();
	$vorige = $bug->getEersteBericht();
	foreach ($berichten as $bericht) {
		$body = formatBericht($bericht, $vorige, $svnRevisies);

		$gitlab = $project->getClient();
		$gitlab->issues()->addComment($project->id, $issue->iid, [
			'body' => $body,
			'created_at' => $bericht->getMoment()->format('c')
		]);

		$vorige = $bericht;
	}

	if (!in_array($bug->getStatus(), $bugStatusSets['OPENS'])) {
		$issue->close();
	}

	$bugID = $bug->getBugID();
	$projectURL = $project->web_url;
	echo "Bug $bugID toegevoegd aan $projectURL.\n";

	return $issue;
}

/**
 * @brief Geef de labels die deze bug in GitLab moet hebben.
 *
 * @returns Een array met de namen van de labels.
 */
function labels($bug) {
	$labels = [];

	$labels[] = LABELS['level'][$bug->getLevel()];
	$labels[] = LABELS['prioriteit'][$bug->getPrioriteit()];

	$statusLabel = LABELS['status'][$bug->getStatus()];
	if (!is_null($statusLabel)) {
		$labels[] = $statusLabel[1];

		if ($statusLabel[0] != 'type') {
			$labels[] = LABELS['status']['OPEN'][1];
		}
	} else {
		$labels[] = LABELS['status']['OPEN'][1];
	}

	$categorie = $bug->getCategorie()->getNaam();
	if (array_key_exists($categorie, CATEGORIELABELS)) {
		$categorieLabel = CATEGORIELABELS[$bug->getCategorie()->getNaam()];
	} else {
		$categorieLabel = $categorie;
	}

	if (!is_null($categorieLabel)) {
		$labels[] = $categorieLabel;
	}

	return $labels;
}

/**
 * @brief Bepaal of een bug 'confidential' moet zijn in GitLab.
 */
function isConfidential($bug) {
	switch ($bug->getNiveau()) {
	case 'OPEN':
		return false;
	case 'INGELOGD':
	case 'CIE':
		// TODO: misschien toch wel openbaar bij INGELOGD?
		return true;
	}
}

/**
 * @brief Plak een array van stukken tekst aan elkaar met lijntjes ertussen.
 *
 * Helperfunctie voor de andere formatfuncties.
 */
function formatBlokken($blokken) {
	return implode(
		"\n\n---\n\n",
		array_filter($blokken, function ($x) { return !is_null($x); })
	);
}

/**
 * @brief Format een Bug als Markdown/HTML.
 */
function formatBug($bug, $svnRevisies) {
	$eerste = $bug->getEersteBericht();

	return formatBlokken([
		formatBerichtHeader($eerste, true),
		formatBerichtTekst($eerste, $svnRevisies),
		formatBugInfo($bug),
	]);
}

/**
 * @brief Format een BugBericht als Markdown/HTML.
 */
function formatBericht($bericht, $vorige, $svnRevisies) {
	return formatBlokken([
		formatBerichtHeader($bericht),
		formatBerichtTekst($bericht, $svnRevisies),
		formatBerichtVeranderingen($bericht, $vorige),
		formatBerichtInfo($bericht)
	]);
}

/**
 * @brief Format de details (melderemail, etc.) van een Bug als Markdown/HTML.
 */
function formatBugInfo($bug) {
	$info = [];

	$info[] = ['ID', $bug->getBugID()];
	$info[] = ['Commissie', $bug->getCategorie()->getCommissie()->getNaam()];

	foreach (Bug::velden() as $veld) {
		if ($veld === 'bericht') {
			continue;
		}

		// Aanschouw: een van de weinige gevallen waarin de gestoorde reflectie
		// syntax van PHP nuttig is!
		$label = BugView::{'label' . $veld}($bug);

		switch ($veld) {
		case 'Categorie':
			$waarde = BugCategorieView::waardeNaam($bug->getCategorie());
			break;

		default:
			$waarde = BugView::{'waarde' . $veld}($bug);
			break;
		}

		if (!$waarde) {
			$waarde = "<em>Onbekend</em>";
		}

		$info[] = [$label, $waarde];
	}

	$info[] = ["Toegewezen aan", BugView::waardeToewijzingen($bug)];

	$tabel = HtmlTable::fromArray($info, true);

	return "<details><summary>Details uit BugWeb</summary>$tabel</details>";
}

/**
 * @brief Format de header (melder, datum) van een bericht.
 *
 * @param bericht Een BugBericht.
 * @param eerste Of dit het eerste bericht van een bug is.
 */
function formatBerichtHeader($bericht, $eerste = false) {
	$tekst = "";

	if ($eerste) {
		$tekst .=
			"_Deze issue is geïmporteerd uit BugWeb. De opmaak kan dus een beetje raar zijn._";
		$tekst .= "\n\n";
	}

	$naam = PersoonView::naam($bericht->getMelder());
	$moment = BugBerichtView::waardeMoment($bericht);
	$tekst .= "__{$naam}__ op $moment";

	return $tekst;
}

// Deze regexes gebruiken we om verscheidene soorten autolinks om te zetten
// naar Markdown.
//
// Ze gebruiken allemaal het stukje '(?<=^|\s)' wat er voor zorgt dat
// er alleen gematcht wordt wanneer de substring aan het begin van de regel
// zit, of whitespace ervoor heeft, maar dat het niet mee wordt genomen in
// de daadwerkelijke match. Dit is zodat we _alleen_ het stukje syntax
// kunnen vervangen, i.p.v. de witruimte eromheen.

// E.g. '+1234' wordt 'https://www.a-eskwadraat.nl/Leden/1234'.
const LID_REGEX = '/(?<=^|\s)\+(\d+)/';

// E.g. '^deadbeef' wordt 'deadbeef' (de rest laten we door GitLab doen).
const COMMIT_REGEX = '/(?<=^|\s)\^([0-9a-f]{7,40})/';

// E.g. 'r12345' wordt 'deadbeef' (i.e. de bijbehorende commit).
const REVISIE_REGEX = '/(?<=^|\s)r(\d+)/';

// E.g. '/Service/Bugweb' wordt 'https://www.a-eskwadraat.nl/Service/Bugweb'.
const RELALINK_REGEX = '/(?<=^|\s)(\/[A-Za-z0-9\-\.\_~\:\?\&\#\=\;\+\'\!\@\$\%\/\[\]]+)/';

/**
 * @brief Format de tekst van een BugBericht.
 */
function formatBerichtTekst($bericht, $svnRevisies) {
	$tekst = $bericht->getBericht();

	if ($tekst === "") {
		return null;
	}

	$tekst = preg_replace_callback(LID_REGEX, function ($match) {
		$origineel = $match[0];
		$lidnr = $match[1];
		$url = HTTPS_ROOT . "/Leden/${lidnr}";
		return "[$origineel]($url)";
	}, $tekst);

	$tekst = preg_replace_callback(REVISIE_REGEX, function ($match) use ($svnRevisies) {
		$origineel = $match[0];
		$revnr = (int)$match[1];
		$commit = get($svnRevisies[$revnr]);
		return !is_null($revnr) ? $commit : $origineel;
	}, $tekst);

	$tekst = preg_replace(COMMIT_REGEX, "$1", $tekst);
	$tekst = preg_replace(RELALINK_REGEX, "[$1](" . HTTPS_ROOT . "$1)", $tekst);

	return $tekst;
}

/**
 * @brief Format extra info bij een bericht.
 */
function formatBerichtInfo($bericht) {
	$regels = [];

	foreach (['melderEmail', 'revisie', 'commit'] as $veld) {
		$label = BugBerichtView::{'label' . $veld}($bericht);
		$waarde = $bericht->{'get' . $veld}();
		if ($waarde) {
			$regels[] = "__$label:__ $waarde";
		}
	}

	return count($regels) > 0 ? implode("<br>\n", $regels) : null;
}

/**
 * @brief Format de veranderingen die door een BugBericht zijn gemaakt.
 */
function formatBerichtVeranderingen($bericht, $vorige) {
	if (is_null($vorige)) {
		return null;
	}

	$veranderingen = BugBerichtView::veranderingen($vorige, $bericht);
	$regels = [];

	foreach ($veranderingen as $veld => $diff) {
		if (is_null($diff)) {
			continue;
		}

		if ($veld === 'toewijzing') {
			// Hier is geen labelfunctie voor ofzo.
			$label = "Toewijzing";
		} else {
			// Aanschouw: een van de weinige gevallen waarin de gestoorde reflectie
			// syntax van PHP nuttig is!
			$label = BugBerichtView::{'label' . $veld}($bericht);
		}

		$oud = $diff[0];
		$nieuw = $diff[1];

		$regels[] = "__$label:__ $oud &#8594; $nieuw";
	}

	return count($regels) > 0 ? implode("<br>\n", $regels) : null;
}

function fixAlleBugLinks($project, $nieuweIDs) {
	$projectURL = $project->web_url;
	echo "Bug links fixen voor $projectURL\n";

	$gitlab = $project->getClient();

	foreach (alleIssues($project) as $issue) {
		$id = $issue->iid;
		echo "Issue $id fixen...\n";

		$issue->update([
			'description' => fixBugLinks($project, $issue->description, $nieuweIDs)
		]);

		foreach ($issue->showComments() as $comment) {
			if ($comment->system) {
				// Dit zijn de berichten dat de issue gesloten is e.d., dus die
				// slaan we over.
				continue;
			}

			$gitlab
				->issues()
				->updateComment(
					$project->id, $issue->iid, $comment->id,
					fixBugLinks($project, $comment->body, $nieuweIDs)
				);
		}
	}

	echo "Klaar.\n";
}

function fixBugLinks($huidigProject, $tekst, $nieuweIDs) {
	// Merk op dat de regex checkt of er '&' voor en ';' na de reference
	// staat; het is dan namelijk een HTML entity, niet een issuereference.
	return preg_replace_callback('/(&?)#(\d+)(;?)/', function ($match) use ($huidigProject, $nieuweIDs) {
		if ($match[1] === '&' && $match[3] === ';') {
			// Dit is een entity (bijv. om een '->' teken neer te zetten),
			// dus we doen niks.
			return $match[0];
		}

		$gitlab = $huidigProject->getClient();
		$oudeID = (int)$match[2];

		if (isset($nieuweIDs[$oudeID])) {
			$projectID = $nieuweIDs[$oudeID][0];
			$issueIID = $nieuweIDs[$oudeID][1];

			$project = (new \Gitlab\Model\Project($projectID, $gitlab))->show();
			$issue = (new \Gitlab\Model\Issue($project, $issueIID, $gitlab))->show();

			if ($project->id === $huidigProject->id) {
				$reference = "#" . $issue->iid;
			} else {
				$reference = $project->path_with_namespace . "#" . $issue->iid;
			}

			echo "$oudeID -> $reference\n";
		} else {
			$reference = "#$oudeID";
			echo "Geen nieuwe ID gevonden voor $oudeID\n";
		}

		return $match[1] . $reference . $match[3];
	}, $tekst);
}

function alleIssues($project) {
	// De GitLab API geeft by default max 20 issues per request, dus we moeten
	// meerdere requests doen om alle issues te krijgen.
	$page_number = 1;

	do {
		$page = $project->issues(['page' => $page_number, 'per_page' => 100]);
		foreach ($page as $issue) {
			yield $issue;
		}
		$page_number += 1;
	} while (count($page) > 0);
}

function alleLabels($project) {
	// De GitLab API geeft by default max 20 labels per request, dus we moeten
	// meerdere requests doen om alle labels te krijgen.
	$page_number = 1;

	do {
		$page = $project->labels(['page' => $page_number, 'per_page' => 100]);
		foreach ($page as $label) {
			yield $label;
		}
		$page_number += 1;
	} while (count($page) > 0);
}

/**
 * @brief Delete alle issues in een GitLab project.
 *
 * Vooral handig voor het debuggen van dit script, zodat alles helemaal opnieuw
 * kan worden gedaan.
 *
 * @see deleteAlleLabels
 */
function deleteAlleIssues($project) {
	echo "Alle issues verwijderen...\n";

	$gitlab = $project->getClient();

	foreach (alleIssues($project) as $issue) {
		$gitlab->issues()->remove($project->id, $issue->iid);
	}

	echo "Klaar.\n";
}

/**
 * @brief Delete alle labels in een GitLab project.
 *
 * Vooral handig voor het debuggen van dit script, zodat alles helemaal opnieuw
 * kan worden gedaan.
 *
 * @see deleteAlleIssues
 */
function deleteAlleLabels($project) {
	echo "Alle labels verwijderen...\n";

	foreach (alleLabels($project) as $label) {
		$project->removeLabel($label->name);
	}

	echo "Klaar.\n";
}

function get(&$ref, $default = null) {
	return isset($ref) ? $ref : $default;
}

/**
 * Geeft een iterator die eerst alle waardes van de eerste, en dan die
 * van de tweede geeft.
 * Hier alleen bedoeld om wat testcases aan de bugquery toe te voegen.
 */
function appendIter($as, $bs) {
	foreach ($as as $a) {
		yield $a;
	}

	foreach ($bs as $b) {
		yield $b;
	}
}

/**
 * Omdat PHP kennelijk nutteloos wil zijn voor scripts.
 */
function sla($message = null) {
	if ($message) {
		fwrite(STDERR, $message);
	}
	exit(1);
}

main();

