<?php

namespace Generator\WSW4objects;

use DocCommentTemplate;
use Generator\Bestand;

class WSW4var
{
	public $name;
	public $nameForDB;
	public $type;
	public $class		= NULL;
	public $foreign		= NULL;
	/**
	 * Als dit een foreign object is,
	 * dan wordt de relatie uitgedrukt door subKeys,
	 * die staan in deze array.
	 */
	public $subKeys = [];
	public $array		= False;
	public $default;
	public $comment;
	public $visibility	= 'public';
	public $static		= False;
	public $annotation	= array();

	function __construct($name)
	{
		if($name == "overerving")
			error(NULL, "'$name' is een gereserveerde variabale naam");
		$this->name = $name;
		$this->nameForDB = $name;
		$this->annotation['LANG'] = false;
		$this->annotation['LAZY'] = false;
		$this->annotation['NULL'] = false;
		$this->annotation['LEGACY_NULL'] = false;
		$this->annotation['SETBESTUUR'] = false;
		$this->annotation['ENUM'] = false;
		$this->annotation['PRIMARY'] = false;
		$this->annotation['CONSTRAINT_REGEX'] = false;
		$this->annotation['PSEUDO'] = false;
		$this->annotation['NO_CONSTRUCTOR'] = false;
	}
	public function setType($type)
	{
		$this->type = strtolower($type);
		switch($this->type) {
		case 'bool':
		case 'date':
		case 'datetime':
		case 'enum':
		case 'int':
		case 'bigint':
		case 'money':
		case 'string':
		case 'text':
		case 'html':
		case 'url':
		case '':
			break;
		default:	// FOREIGN
			$this->type = 'foreign';
			$this->setForeign($type);
			break;
		}
	}

	public function setComment($comment)
	{
		$this->comment = $comment;

		// Annotations in comments parsen. Enkel de laatste regel van de
		// comments wordt bekeken en alleen als deze begint met een
		// apenstaartje
		$comments_arr = explode("\n", $comment);
		$lastline = array_pop($comments_arr);
		if(substr($lastline, 0, 1) === "@")
		{
			// Parse annotations!
			$lastline = substr($lastline, 1);
			$keywords = explode(',',$lastline); // todo wat als een constraint een , bevat?
			foreach($keywords as $keyword)
			{
				$keyword = trim($keyword);
				switch(strtoupper($keyword))
				{
					case 'PRIMARY':
					case 'LANG':
					case 'NULL':
					case 'LEGACY_NULL':
					case 'SETBESTUUR':
					case 'PSEUDO':
					case 'NO_CONSTRUCTOR':
						$this->annotation[$keyword] = true;
						break;
					case 'LAZY':
						// no-op, LAZY is broken en dus gedisabled
						break;
					default:
						$key = explode(':', $keyword);
						if(count($key) != 2)
							error($this, 'Unknown Annotation: ' . $keyword);
						switch (strtoupper($key[0]))
						{
							case 'ENUM':
								$this->annotation['ENUM'] = explode('/', strtoupper($key[1]));
								break;
							case 'CONSTRAINT_REGEX':
								$this->annotation['CONSTRAINT_REGEX'] = $key[1];
								break;
							default:
								error($this, 'Unkown Annotation: ' . $keyword);
						}
				}
			}
		} // else: laatste regel van commentaar start niet met '@' => geen annotations
	}

	public function setDefault($default)
	{
		if(empty($default)) return;
		$this->default = $default;
	}
	public function setStatic($static)
	{
		$this->static = (bool)$static;
	}
	public function setVisibility($visibility)
	{
		$this->visibility = $visibility;
	}

	public function check ($objects)
	{
		if($this->default)
		{
			switch($this->type) {
			case 'string':
			case 'text':
			case 'html':
			case 'enum':
			case 'url':
				// TODO escaping ??
				$this->default = "'".$this->default."'";
				break;
			case 'date':
			case 'datetime':
				$this->default = 'new DateTimeLocale("'.$this->default.'")';
				break;
			case 'bool':
				if(strtoupper($this->default) == 'TRUE')
					$this->default = 'True';
				else
					$this->default = 'False';
				break;
			default:
				break;
			}
		}
		else
		{
			if($this->annotation['NULL']) {
				switch($this->type) {
				case 'date':
				case 'datetime':
					$this->default = 'new DateTimeLocale(NULL)';
					break;
				default:
					$this->default = 'NULL';
					break;
				}
			}
			else {
				switch($this->type) {
				case 'int':
				case 'bigint':
				case 'money':
					$this->default = 0;
					break;
				case 'enum':
					if(!count($this->annotation['ENUM']))
						error($this, "enum type but no annotation");
					$this->default = "'".$this->annotation['ENUM'][0]."'";
					break;
				case 'bool':
					$this->default = 'False';
					break;
				case 'date':
				case 'datetime':
					$this->default = 'new DateTimeLocale()';
					break;
				case 'foreign':
					$this->default = 'NULL';
					break;
				case 'string':
				case 'text':
				case 'html':
				case 'url':
					$this->default = "''";
					break;
				default:
					error($this, 'onbekend type 1:'.$this->type);
				}
			}
		}
		if($this->type == 'foreign')
			$this->setForeign($objects['class'][$this->foreign]);
		if($this->annotation['PRIMARY'] && $this->annotation['LAZY'])
			error($this, 'attribuut primary & lazy kan niet!');

		if ($this->type == 'enum')
		{
			// De opties van een enum mogen geen lege string bevatten,
			// want lege string in een HtmlSelectbox staat al voor "geen waarde".
			// Dit zou in NULL-bare enums voor een conflict zorgen.
			// We checken dit voor elke enum om migraties naar NULL-baarheid
			// makkelijk(er) te maken.
			foreach ($this->annotation['ENUM'] as $optie)
			{
				if ($optie == '')
				{
					error($this, 'Een enumoptie mag niet leeg zijn.');
				}
			}
		}
	}

	public function getSQLType()
	{
		switch ($this->type)
		{
			case 'int':
				return "int(11)";
			case 'bigint':
				return "bigint(11)";
			case 'money':
				return "decimal(8,2)";
			case 'bool':
				return "int(1)";
			case 'string':
				return "varchar(255)";
			case 'url':
				return "varchar(511)";
			case 'text':
				return "text";
			case 'html':
				return "text";
			case 'date':
				return "date";
			case 'datetime':
				return "datetime";
			case 'enum':
				return "enum('"
					. implode("','", $this->annotation['ENUM'])
					. "')";
			default:
				error($this, "onbekend type 2:" . $this->type);
		}
	}

	public function getLibDBType()
	{
		switch($this->type) {
		case 'int':
		case 'bigint':
		case 'bool':
			return "i";
		case 'money':
			return "f";
		case 'string':
		case 'text':
		case 'html':
		case 'date':
		case 'datetime':
		case 'enum':
		case 'url':
			return "s";
		}
	}

	/**
	 * @brief Geef het type van deze variabele als PHP(-Doc)-type.
	 *
	 * @return string
	 * Een string met het type van deze variabele in PHP.
	 */
	public function getPHPType()
	{
		switch($this->type)
		{
			case 'int':
			case 'bigint':
				return 'int';
			case 'bool':
				return 'bool';
			case 'money':
				return 'float';
			case 'string':
			case 'text':
			case 'html':
			case 'enum':
			case 'url':
				return 'string';
			case 'date':
			case 'datetime':
				return 'DateTimeLocale';
			case 'foreign':
				return $this->foreign->name;
			case '':
				return 'mixed';
			default:
				error($this, "onbekend PHP-type: " . $this->type);
		}
	}

	public function upper()
	{
		return strtoupper($this->name{0}).substr($this->name, 1);
	}

	public function generateDBCreate(Bestand $bestand)
	{
		if($this->isForeignObject() || $this->annotation['PSEUDO'])
			return;

		$ns = array();
		if($this->annotation['LANG']) {
			$ns[] = $this->nameForDB.'_NL';
			$ns[] = $this->nameForDB.'_EN';
		} else {
			$ns[] = $this->nameForDB;
		}
		foreach($ns as $name)
		{
			// TODO type, default, NULL?, autoincrement, ...
			$bestand->addContent(", `".$name."`");
			$type = $this->getSQLType()
					. ($this->annotation['NULL']
					   ||($this->annotation['LEGACY_NULL']
					      &&(!$this->annotation['LANG']
					         ||($this->annotation['LANG']
						        &&(bool)preg_match('/_EN$/',$name)
						 )  )  )
					  ? '' : ' NOT NULL');
			if($this->annotation['NULL']) {
				$type .= ' DEFAULT NULL';
			} else if (!$this->annotation['NULL'] && $this->default != 'NULL'
					&& $this->type != 'date' && $this->type != 'datetime') {
				$type .= ' DEFAULT '.$this->default;
			}

			$bestand->addContent(" $type\n");
		}
	}

	/**
	 * @brief Wijst deze variabele naar een foreign object?
	 *
	 * Dit betekent dat het een van de keys is van dat object,
	 * of een virtuele variabele die dat object zelf bevat.
	 * (Denk hier bijvoorbeeld aan het verschil tussen
	 * $bla->lid_contactID en $bla->lid respectievelijk.)
	 *
	 * @see isForeignKey
	 * @see isForeignObject
	 *
	 * @return bool
	 */
	public function isForeign()
	{
		return !is_null($this->foreign);
	}

	/**
	 * @brief set de foreign key op dit veld.
	 * @see isForeign
	 * @see setForeignKey
	 * @param obj object|bool foreign-object of TRUE voor een key
	 */
	public function setForeign($obj)
	{
		$this->foreign = $obj;
	}

	/**
	 * @brief Maakt dit veld een deel van een foreign key
	 * @see isForeignKey
	 */
	public function setForeignKey()
	{
		$this->setForeign(true);
	}

	/**
	 * @brief Representeert dit een veld dat deel is van een foreign key?
	 *
	 * @see isForeign
	 * @see isForeignObject
	 * @see setForeignKey
	 *
	 * @return bool
	 */
	public function isForeignKey()
	{
		return $this->foreign === true;
	}

	/**
	 * @brief Representeert dit een variabele die een WSW4-object bevat?
	 *
	 * Zo ja, zou $this->subKeys geset moeten zijn.
	 *
	 * @see isForeign
	 * @see isForeignKey
	 *
	 * @return bool
	 */
	public function isForeignObject()
	{
		return is_object($this->foreign);
	}

	/**
	 * Deze methode maakt de inialisatie-regel voor in de header van de klasse
	 * @param Bestand $bestand
	 */
	public function generateCode (Bestand $bestand)
	{
		if ($this->annotation['PSEUDO'])
			return;

		$var = "\t";

		if ($this->static)
			$var .= 'static '.$this->visibility;
		elseif ($this->type == 'object')
			$var .= 'private';
		else
			$var .= 'protected';

		$var .= ' $'.$this->name;

		if ($this->static && !empty($this->default))
			$var .= ' = ' . $this->default;

		$var .= ';';

		if (!empty($this->comment))
		{
			$var .= @str_repeat("\t", 10 - (strlen($var) / 4));
			$var .= '/**< \brief '.str_replace(array("\n","@"), array(" ","")
									, $this->comment) .' */';
		}

		$bestand->addContent("$var\n");

		return;
	}

	/**
	 * Deze methode maakt de inialisatie-regel voor in de constructor van de klasse
	 * @param Bestand $bestand
	 */
	public function generateCodeDefault (Bestand $bestand)
	{
		if ($this->static)
			return;

		$var = "\t\t\$this->" . $this->name . ' = ';

		if ($this->annotation['LAZY'])
			if ($this->annotation['LANG'])
				$var .= 'array(\'nl\' => NULL, \'en\' => NULL)';
			else
				$var .= 'NULL';
		else
			if ($this->annotation['LANG'])
				$var .= 'array(\'nl\' => ' . $this->default
					. ', \'en\' => ' . $this->default . ')';
			else
				$var .= $this->default;

		$bestand->addContent("$var;\n");

		return;
	}

	/**
	 * Deze methode maakt de enum methoden in een object
	 * @param Bestand $bestand
	 */
	public function generateCodeGetEnum (Bestand $bestand)
	{
		if ($this->type != 'enum')
			return;

		$name = $this->name;

		// De niet-NULLe enumopties staan in de annotatie.
		$opties = $this->annotation['ENUM'];
		// Als de enum NULL-baar is, willen we ook NULL kunnen selecteren.
		// Omdat dat ook zo werkt in een <select>-tag,
		// representeren we die als de lege string.
		if ($this->annotation['NULL'])
		{
			$opties[] = '';
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef de mogelijke enums van het veld {$name}.")
			->setReturn('string[]', "Een array met alle enum-waarden van het veld {$name}.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function enums".$this->upper()."()\n"
				.	"\t{\n"
				.	"\t\tstatic \$vals = array('"
					. implode("','", $opties)
					. "');\n"
				.	"\t\treturn \$vals;\n"
				.	"\t}\n"
				);
	}

	/**
	 * Deze methode maakt de get methoden in een object
	 * todo dit is een bijzonder lelijke methode, herschrijven is waarschijnlijk de moeite waard
	 * @param Bestand $bestand
	 */
	public function generateCodeGetter (Bestand $bestand)
	{
		if($this->visibility == 'implementation'
			|| $this->static)
			return;

		$name = $this->name;
		$getter = preg_replace_callback("/_(.)/",
			function($matches) { return strtoupper($matches[1]); },
			$this->upper()
		);

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef de waarde van het veld {$name}.")
			->setReturn($this->getPHPType(), "De waarde van het veld {$name}.");

		if ($this->annotation['LANG']) {
			$docCommentTemplate->addParam("lang", 'string|null', "De taal waarin de waarde verkregen wordt.");
		}

		$bestand->addContent($docCommentTemplate->generate(1));
		$bestand->addContent("\tpublic function get$getter(");

		if ($this->annotation['LANG'])
		{
			$langV = '$lang';
			$langA = "[$langV]";

			$bestand->addContent('$lang = NULL');
		}
		else
		{
			$langV = 'NULL';
			$langA = '';
		}

		$bestand->addContent(")\n"
				.	"\t{\n");

		if ($this->annotation['PSEUDO'])
		{
			$bestand->addContent("\t\tuse_error('Implementeer mij', E_USER_ERROR);\n"
					.	"\t}\n");
			return;
		}

		if ($this->type == 'object')
		{
			$bestand->addContent("\t\tif (is_null(\$this->$name)"
					.		" && isset(\$this->${name}ID)) {\n"
					.	"\t\t\treturn ".$this->upper()."::geef(\$this->${name}ID);\n"
					.	"\t\t}\n"
					.	"\t\treturn \$this->$name;\n");
		}
		elseif ($this->isForeignKey())
		{
			list($objvar, $keyget) = explode('_', $name, 2);
			$objcls = @$this->class->IDs[$objvar];
			if (isset($objcls) && $objcls->isForeignObject())
			{
				foreach($objcls->foreign->IDs as $a)
				{
					if (!$a->isForeignKey())
						continue;

					$b = substr($a->name, strrpos($a->name, '_') + 1);

					if($b != $keyget)
						continue;

					$keyget = $a;
					break;
				}
				$keyget = preg_replace_callback("/_(.)/",
					function($matches) { return strtoupper($matches[1]); },
					"get_".$a->upper()
				);
			}
			else
			{
				$keyget = preg_replace_callback("/_(.)/",
					function($matches) { return strtoupper($matches[1]); },
					"get_".$keyget
				);
			}

			$bestand->addContent("\t\tif (is_null(\$this->$name)"
					.		" && isset(\$this->$objvar)) {\n"
					.	"\t\t\t\$this->$name = \$this->${objvar}->$keyget();\n"
					.	"\t\t}\n");
		}
		elseif ($this->isForeignObject())
		{
			$foreignforeign = array();
			$bestand->addContent("\t\tif(!isset(\$this->$name)\n");
			foreach($this->foreign->IDs as $n => $id)
			{
				if ($id->isForeignObject())
					continue;

				$string = "\t\t && isset(\$this->${name}_";
				if($id->isForeign()) {
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					//Increment if we see the same foreign key twice
					if(@$foreignforeign[$substr])
					{
						$string .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					}
					else
					{
						$string .= $substr;
						$foreignforeign[$substr] = 2;
					}
				}
				else
				{
					$string .= $id->name;
				}
				$string .= ")\n";
				$bestand->addContent($string);
			}

			$bestand->addContent("\t\t ) {\n"
					.	"\t\t\t\$this->$name = ".$this->foreign->name."::geef\n"
					);

			$first = true;
			$foreignforeign = array();
			foreach ($this->foreign->IDs as $n => $id)
			{
				if ($id->isForeignObject())
					continue;

				$string = "\t\t\t\t\t".($first?"(":",")." \$this->${name}_";

				if ($id->isForeign())
				{
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					//Increment if we see the same foreign key twice
					if(@$foreignforeign[$substr]){
						$string .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					} else {
						$string .= $substr;
						$foreignforeign[$substr] = 2;
					}
				}
				else
				{
					$string .= $id->name;
				}

				$string .= "\n";
				$bestand->addContent($string);

				$first = false;
			}

			$bestand->addContent("\t\t\t\t\t);\n"
					.	"\t\t}\n");
		}
		else
		{
			if ($this->annotation['LANG'])
				$bestand->addContent("\t\tif (!isset($langV))\n"
						.	"\t\t\t$langV = getLang();\n");

			if ($this->annotation['LAZY'])
			{
				$bestand->addContent("\t\tif (!isset(\$this->$name$langA)) {\n"
						.	"\t\t\tif (!\$this->inDB)\n"
						.	"\t\t\t{\n"
						.	"\t\t\t\t\$this->$name$langA = ".$this->default.";\n"
						.	"\t\t\t} else {\n"
						.	"\t\t\t\t\$this->$name$langA = "
						.		"\$this->uitDB('$name', $langV);\n"
						);
				switch($this->type)
				{
					case 'date':
					case 'datetime':
						$bestand->addContent("\t\t\t\t\$this->$name$langA = ");
						$bestand->addContent("new DateTimeLocale(\$this->$name$langA);\n");
						break;
					default:
						break;
				}
				$bestand->addContent("\t\t\t}\n"
						.	"\t\t}\n");
			}
		}

		$bestand->addContent("\t\treturn \$this->$name$langA;\n"
				. "\t}\n");

		return;
	}

	/**
	 * Deze methode maakt de set methoden in een object
	 * todo dit is een bijzonder lelijke methode, herschrijven is waarschijnlijk de moeite waard
	 * @param Bestand $bestand
	 * @return bool|void
	 */
	public function generateCodeSetter (Bestand $bestand)
	{
		if($this->static
			|| $this->visibility == 'implementation'
			|| $this->visibility == 'private'
			|| $this->visibility == 'protected'
			|| $this->annotation['PSEUDO'])
			return;

		$name = $this->name;
		$upper = $this->upper();
		$className = $this->class->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Stel de waarde van het veld {$name} in.")
			->setReturn($className, "Dit {$className}-object.");

		if($this->type == 'foreign')
		{
			$fids = $this->foreign->IDs;
			$fkeys = array_keys($fids);

			foreach ($fkeys as $key) {
				$docCommentTemplate->addParam("new_{$key}", 'mixed', "De nieuwe waarde.");
			}

			$bestand->addContent($docCommentTemplate->generate(1)
				. "\tpublic function set$upper(");
			$f = True;
			foreach($fkeys as $key) {
				$bestand->addContent(($f?"":", ").'$new_'.$key.($f?"":" = NULL"));
				$f = False;
			}
			$bestand->addContent(")\n"
					.	"\t{\n"
					.	"\t\tunset(\$this->errors['$upper']);\n"
					);
			if($this->annotation['SETBESTUUR']) {
				$bestand->addContent("\t\tif (!hasAuth('bestuur')) {\n"
						.	"\t\t\ttrigger_error('Alleen >= bestuur mag dit wijzigen!', E_USER_WARNING);\n"
						.	"\t\t\treturn \$this;\n"
						.	"\t\t}\n"
						);
			}
			$constraints = array();
			$f = True;
			foreach($fkeys as $key) {
				$bestand->addContent("\t\t".($f?"if(":"&& is_null(")
							.	'$new_'.$key
							.	($f?" instanceof ".$this->foreign->name:")")
							.	"\n"
							);
				$f = False;
			}
			$bestand->addContent("\t\t) {\n");
			$f = True;
			foreach($fids as $key => $id) {
				//Doordat value names de $key gebruiken en functienamen $upper moeten
				// we de functienaam zelf herleiden uit de key.
				$keypieces = explode("_" ,$key);
				foreach($keypieces as $index => $keypiece)
					$keypieces[$index] = ucfirst($keypiece);
				$keyresult = implode("",$keypieces);

				if($f) {
					$f = false;
					$bestand->addContent("\t\t\tif(\$this->$name == \$new_$key\n");
				}
				if($id->isForeignObject()) continue;
				//If it's a foreign relationship check extra on the keys
				if($id->isForeign()){
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					$keyname = "";
					//Increment if we see the same foreign key twice
					if(@$foreignforeign[$substr]){
						$keyname .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					} else {
						$keyname .= $substr;
						$foreignforeign[$substr] = 2;
					}

					$last = end($fids);
					$bestand->addContent("\t\t\t&& \$this->${name}_$keyname == \$this->${name}->get".$keyresult."()".(($last == $id) ? ")\n" : "\n"));
				} else {
					$last = end($fids);
					$bestand->addContent("\t\t\t&& \$this->${name}_$key == \$this->${name}->get".$keyresult."()".(($last == $id) ? ")\n" : "\n"));
				}
			}

			$bestand->addContent("\t\t\t\treturn \$this;\n"
					);
			$f = True;
			$foreignforeign = array();
			foreach($fids as $key => $id) {
				if($f) {
					$f = false;
					$bestand->addContent("\t\t\t\$this->$name = \$new_$key;\n");
				}
				//Doordat value names de $key gebruiken en functienamen $upper moeten
				// we de functienaam zelf herleiden uit de key.
				$keypieces = explode("_" ,$key);
				foreach($keypieces as $index => $keypiece)
					$keypieces[$index] = ucfirst($keypiece);
				$keyresult = implode("",$keypieces);

				if($id->isForeignObject()) continue;

				//If it's a foreign relationship check extra on the keys
				if($id->isForeign()){
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					$keyname = "";
					//Increment if we see the same foreign key twice
					if(@$foreignforeign[$substr]){
						$keyname .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					} else {
						$keyname .= $substr;
						$foreignforeign[$substr] = 2;
					}

					$bestand->addContent("\t\t\t\$this->${name}_$keyname\n"
							.	"\t\t\t\t\t= \$this->${name}->get".$keyresult."();\n"
							);
				} else {
					$bestand->addContent("\t\t\t\$this->${name}_$key\n"
							.	"\t\t\t\t\t= \$this->${name}->get".$keyresult."();\n"
							);
				}
			}

			$bestand->addContent("\t\t\t\$this->gewijzigd();\n"
					.	"\t\t\treturn \$this;\n"
					.	"\t\t}\n"
					);
			foreach ($fkeys as $key)
				$constraints[] = '(is_null($new_' . $key . ') || $new_' . $key . ' == 0)';
			if($this->annotation['NULL']) {
				$bestand->addContent("\t\tif (".implode($constraints, ' && ').") {\n"
						.	"\t\t\tif(\$this->$name == NULL && \$this->${name}_".implode(" == NULL && ", $fkeys)." == NULL)\n"
						.	"\t\t\t\treturn \$this;\n"
						.	"\t\t\t\$this->$name = NULL;\n"
						.	"\t\t\t\$this->${name}_".implode(" = NULL;\n"
						.	"\t\t\t\$this->${name}_", $fkeys)." = NULL;\n"
						.	"\t\t\t\$this->gewijzigd();\n"
						.	"\t\t\treturn \$this;\n"
						.	"\t\t}\n"
						);
			}
			$bestand->addContent("\t\tif(isset(\$new_".implode(")\n"
					.	"\t\t&& isset(\$new_", $fkeys).")\n"
					.	"\t\t) {\n"
					);

			$bestand->addContent("\t\t\tif(\$this->$name == NULL \n");
			$foreignforeign = array();

			foreach($fids as $key => $id) {
				$keyname = "";
				if($id->isForeignObject()) continue;
				//If it's a foreign relationship check extra on the keys
				if($id->isForeign()) {
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					//Increment if we see the same foreign key twice
					if(@$foreignforeign[$substr]){
						$keyname .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					} else {
						$keyname .= $substr;
						$foreignforeign[$substr] = 2;
					}
				} else {
					$keyname = $key;
				}
				$last = end($fids);
				$bestand->addContent("\t\t\t\t&& \$this->${name}_$keyname == ".maybeint($keyname)."\$new_$key" . (($last == $id) ? ")\n" : " \n")
						);
			}
			$bestand->addContent("\t\t\t\treturn \$this;\n");
			$bestand->addContent("\t\t\t\$this->$name = NULL;\n"
					);

			$foreignforeign = array();

			foreach($fids as $key => $id) {
				$keyname = "";
				if($id->isForeignObject()) continue;
				//If it's a foreign relationship check extra on the keys
				if($id->isForeign()){
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					//Increment if we see the same foreign key twice
					if(@$foreignforeign[$substr]){
						$keyname .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					} else {
						$keyname .= $substr;
						$foreignforeign[$substr] = 2;
					}
				} else {
					$keyname = $key;
				}

				$bestand->addContent("\t\t\t\$this->${name}_$keyname\n"
						.	"\t\t\t\t\t= ".maybeint($keyname)."\$new_$key;\n"
						);
			}
			$bestand->addContent("\t\t\t\$this->gewijzigd();\n"
					.	"\t\t\treturn \$this;\n"
					.	"\t\t}\n"
					);
			if($this->annotation['LEGACY_NULL']) {
				$bestand->addContent("\t\tif (isset(\$this->${name})\n"
						.	"\t\t || isset(\$this->${name}_".implode(")\n"
						.	"\t\t || isset(\$this->${name}_", $fkeys)."))\n"
						.	"\t\t{\n"
						.	"\t\t\t\$this->errors['$upper']"
						.			" = _('dit is een verplicht veld');\n"
						.	"\t\t\t\$this->$name = NULL;\n"
						.	"\t\t\t\$this->${name}_".implode(" = NULL;\n"
						.	"\t\t\t\$this->${name}_", $fkeys)." = NULL;\n"
						.	"\t\t}\n"
						);
			}
			$bestand->addContent("\t\treturn \$this;\n"
					.	"\t}\n");
			return;
		}

		$docCommentTemplate->addParam("new{$upper}", 'mixed', "De nieuwe waarde.");

		if ($this->annotation['LANG']) {
			$docCommentTemplate->addParam("lang", 'string|null', "De taal die wordt ingesteld.");
		}

		$bestand->addContent($docCommentTemplate->generate(1)
			. "\tpublic function set$upper(\$new$upper");
		if($this->annotation['LANG']) {
			$langV = '$lang';
			$langA = "[$langV]";

			$bestand->addContent(", $langV = NULL");
		} else {
			$langV = 'NULL';
			$langA = '';
		}
		$bestand->addContent(")\n\t{\n"
				.	"\t\tunset(\$this->errors['$upper']);\n"
				);

		if($this->annotation['SETBESTUUR']) {
			$bestand->addContent("\t\tif (!hasAuth('bestuur')) {\n"
					.	"\t\t\ttrigger_error('Alleen >= bestuur mag dit wijzigen!', E_USER_WARNING);\n"
					.	"\t\t\treturn \$this;\n"
					.	"\t\t}\n"
					);
		}
		if($this->annotation['LANG'])
			$bestand->addContent("\t\tif(!isset($langV))\n"
						. "\t\t\t$langV = getLang();\n");

		if ($this->type != 'datetime' && $this->type != 'date')
			$bestand->addContent("\t\tif(!is_null(\$new$upper))\n");
		switch($this->type)
		{
			case 'int':
			case 'bool':
				$bestand->addContent("\t\t\t\$new$upper = ($this->type)\$new$upper;\n");
				break;
			case 'bigint':
				$bestand->addContent("\t\t\t\$new$upper = (int)\$new$upper;\n");
				break;
			case 'money':
				$bestand->addContent("\t\t\t\$new$upper = (float)\$new$upper;\n");
				break;
			case 'string':
			case 'text':
			case 'html':
			case 'url':
				$bestand->addContent("\t\t\t\$new$upper = trim(\$new$upper);\n"
						.	"\t\tif(\$new$upper === \"\")\n"
						.	"\t\t\t\$new$upper = NULL;\n"
						);
				break;
			case 'date':
			case 'datetime':
				$bestand->addContent("\t\tif(!\$new$upper instanceof DateTimeLocale) {\n"
						.   "\t\t\ttry {\n"
						.	"\t\t\t\t\$new$upper = new DateTimeLocale(\$new$upper);\n"
						.	"\t\t\t} catch(Exception \$e) {\n"
						.	"\t\t\t\treturn \$this;\n"
						.	"\t\t\t}\n"
						.   "\t\t}\n"
						);
				break;
			case 'enum':
				// Enumopties zijn uppercase strings, waar '' staat voor NULL.
				// Om die reden mag '' niet gebruikt worden als enumoptie.
				$bestand->addContent("\t\t\t\$new$upper = strtoupper(trim(\$new$upper));\n"
						.	"\t\tif(\$new$upper === \"\")\n"
						.	"\t\t\t\$new$upper = NULL;\n"
						);
				break;
			default:
				error($this, 'onbekend type 3');
		}
		// check dat er geen foute tags in zitten
		if ($this->type == 'html') {
			$bestand->addContent("\t\tif (!is_null(\$new$upper))\n"
				. "\t\t\t\$new$upper = Purifier::Purify(\$new$upper);\n"
				);
		}
		switch($this->type)
		{
			case 'date':
			case 'datetime':
				$bestand->addContent("\t\tif(\$this->".$name.$langA."->strftime('%F %T') =="
						.	" \$new".$upper."->strftime('%F %T'))\n"
						.	"\t\t\treturn \$this;\n"
						.	"\n"
						);
				break;
			default:
				$bestand->addContent("\t\tif(\$this->$name$langA === \$new$upper)\n"
						.	"\t\t\treturn \$this;\n"
						.	"\n"
						);
				break;
		}
		if($this->annotation['LEGACY_NULL']) {
			switch($this->type) {
			case 'date':
			case 'datetime':
				$bestand->addContent("\t\tif(!\$new${upper}->hasTime() && \$this->$name${langA}->hasTime())\n");
				break;
			default:
				$bestand->addContent("\t\tif(is_null(\$new$upper) && isset(\$this->$name$langA))\n");

			}
			$bestand->addContent("\t\t\t\$this->errors['$upper']"
					.		" = _('dit is een verplicht veld');\n"
					.	"\n"
					);
		}
		$bestand->addContent("\t\t\$this->$name$langA = \$new$upper;\n"
				.	"\n"
				.	"\t\t\$this->gewijzigd();\n"
				.(	$this->annotation['LAZY']
				?	"\n"
				.	"\t\tif(\$this->inDB)\n"
				.	"\t\t\t\$this->naarDB('$name', \$this->$name$langA"
				.						", $langV);\n"
				:	"" )
				.	"\n"
				.	"\t\treturn \$this;\n"
				.	"\t}\n"
				);
		return False;
	}

	/**
	 * Deze methode maakt de check methoden in een object
	 * @param Bestand $bestand
	 */
	public function generateCodeConstraint (Bestand $bestand)
	{
		if($this->static
			|| $this->visibility == 'implementation'
			|| $this->visibility == 'private'
			|| $this->visibility == 'protected'
			|| $this->isForeignKey()
			|| $this->annotation['PSEUDO'])
			return;

		$name = $this->name;
		$upper = $this->upper();

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Controleer of de waarde van het veld {$name} geldig is.")
			->setReturn('bool|string', "False als de huidige waarde van het veld {$name} geldig is; anders een string met een foutmelding.");

		if ($this->annotation['LANG']) {
			$docCommentTemplate->addParam("lang", 'string|null', "De taal.");

			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tpublic function check$upper(\$lang = null)\n"
					.	"\t{\n"
					.	"\t\tif (is_null(\$lang)) {\n"
					.	"\t\t\t\$ret = array();\n"
					.	"\t\t\t\$ret['nl'] = \$this->check$upper('nl');\n"
					.	"\t\t\t\$ret['en'] = \$this->check$upper('en');\n"
					.	"\t\t\treturn \$ret;\n"
					.	"\t\t}\n"
					.	"\n"
					.	"\t\tif (array_key_exists('$upper', \$this->errors))\n"
					.	"\t\t\tif (is_array(\$this->errors['$upper']) && array_key_exists(\$lang, \$this->errors['$upper']))\n"
					.	"\t\t\t\treturn \$this->errors['$upper'][\$lang];\n"
					.	"\t\t\$waarde = \$this->get$upper(\$lang);\n"
					);
		} else {
			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tpublic function check$upper()\n"
					.	"\t{\n"
					.	"\t\tif (array_key_exists('$upper', \$this->errors))\n"
					.	"\t\t\treturn \$this->errors['$upper'];\n"
				);
		}

		if ($this->isForeignObject())
		{
			if (!$this->annotation['LANG'])
				$bestand->addContent("\t\t\$waarde1 = \$this->get$upper();\n");

			$waarde = 2;

			$ids = array();
			$encouterd = array();
			foreach($this->foreign->IDs as $n => $id) {
				if(!$id->annotation['PRIMARY'])
					continue;
				$keypieces = explode("_" ,$n);
				foreach($keypieces as $index => $keypiece)
					$keypieces[$index] = ucfirst($keypiece);
				$end = end($keypieces);
				if(array_key_exists($end, $ids)) {
					$ids[$end . $encountered[$end]] = $id;
					$encountered[$end]++;
				} else {
					$ids[$end] = $id;
					$encountered[$end] = 2;
				}
			}

			// De functie empty kan niet rechtstreeks op returnvalues toegepast worden
			// Daarom moeten we eerst de value opslaan in een variabele.
			foreach($ids as $id => $var) {
				if(!empty($var->foreign->name))
					continue;
				$bestand->addContent("\t\t\$waarde" . $waarde . " = \$this->get" . $upper . $id . "();\n");
				$waarde++;
			}

			$bestand->addContent("\t\tif(empty(\$waarde1)");
			$waarde = 2;

			$first = true;
			foreach($ids as $id => $var) {
				if(!empty($var->foreign->name))
					continue;
				if($first) {
					$bestand->addContent("\n\t\t\t&& (empty(\$waarde".$waarde.")");
				} else {
					$bestand->addContent("\n\t\t\t\t|| empty(\$waarde".$waarde.")");
				}
				$waarde++;
				$first = false;
			}
			if(!$first) {
				$bestand->addContent(")");
			}

			$bestand->addContent(")\n"
					.	"\t\t{\n");

			if ($this->annotation['NULL'])	// is NULL & mag NULL zijn
				$bestand->addContent("\t\t\treturn False;\n\n");
			else							// is NULL, maar mag niet
				$bestand->addContent("\t\t\treturn _('dit is een verplicht veld');\n\n");

			$bestand->addContent("\t\t}\n"
				.	"\t\treturn False;\n"
				.	"\t}\n");

			return;
		} else {
			if (!$this->annotation['LANG'])
				$bestand->addContent("\t\t\$waarde = \$this->get$upper();\n");
		}

		// NULL CHECK
		switch($this->type)
		{
			case 'date':
			case 'datetime':
				$bestand->addContent("\t\tif (!\$waarde->hasTime())\n");
				break;
			case 'string';
				$bestand->addContent("\t\tif (empty(\$waarde))\n");
				break;
			default:
				$bestand->addContent("\t\tif (is_null(\$waarde))\n");
				break;
		}
		if ($this->annotation['NULL'])	// is NULL & mag NULL zijn
			$bestand->addContent("\t\t\treturn False;\n\n");
		else							// is NULL, maar mag niet
			$bestand->addContent("\t\t\treturn _('dit is een verplicht veld');\n\n");

		// Enum check
		if ($this->type == 'enum')
		{
			$bestand->addContent("\t\tif(!in_array(\$waarde, static::enums$upper()))\n"
					.	"\t\t\treturn _('onbekende waarde');\n\n");
		}

		// Alle checks zijn goed doorlopen
		$bestand->addContent("\t\treturn False;\n"
				.	"\t}\n");

		return;
	}

	/**
	 * Deze methode maakt de label* methoden voor in de views, wordt
	 * aangeroepen vanuit WSW4class mits het om een view gaat
	 * @param Bestand $bestand
	 */
	public function generateCodeToonLabel (Bestand $bestand)
	{
		if($this->static
			|| $this->visibility == 'implementation'
			|| $this->visibility == 'protected'
			|| $this->type == 'object'
			|| $this->isForeignKey())
			return;

		$name = $this->name;
		$className = $this->class->real->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef het label van het veld {$name}.")
			->addParam("obj", $className, "Het {$className}-object waarvoor het veldlabel nodig is.")
			->setReturn('string', "Een string die het veld {$name} labelt.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function label".$this->upper()
				.		"($className \$obj)\n"
				.	"\t{\n"
				.	"\t\treturn '".$this->upper()."';\n"
				.	"\t}\n"
				);

		if ($this->type == 'enum')
		{
			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief("Geef de view-waarde van een enum-waarde van het veld {$name}.")
				->addParam("value", 'string', "Een enum-waarde van het veld {$name}.")
				->setReturn('string', "Een html-veilige string die value representeert.");

			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tpublic static function labelenum".$this->upper()
							. "(\$value)\n"
					.	"\t{\n"
					.	"\t\treturn \$value;\n"
					.	"\t}\n"
					);

			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief("Geef alle view-waarde die bij het enum-veld {$name} horen.")
				->addSee("labelenum{$this->upper()}")
				->setReturn('string[]', "Een array met html-veilige strings die de enum-waarden van het veld {$name} representeren.");

			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tpublic static function labelenum".$this->upper()."Array()\n"
					.	"\t{\n"
					.	"\t\t\$soorten = array();\n"
					.	"\t\tforeach(".substr($this->class->real->name,0,-4)
					.				 "::enums".$this->upper()."() as \$id)\n"
					.	"\t\t\t\$soorten[\$id] = "
					.				$this->class->real->name
					.				"::labelenum".$this->upper()."(\$id);\n"
					.	"\t\treturn \$soorten;\n"
					.	"\t}\n"
					);
		}

		return;
	}

	/**
	 * Deze methode maakt de waarde* methoden voor in de views, wordt
	 * aangeroepen vanuit WSW4class mits het om een view gaat
	 * @param Bestand $bestand
	 */
	public function generateCodeToonWaarde (Bestand $bestand)
	{
		if($this->static
			|| $this->visibility == 'implementation'
			|| $this->visibility == 'protected'
			|| $this->type == 'object'
			|| $this->isForeignKey())
			return;

		$name = $this->name;
		$className = $this->class->real->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef de waarde van het veld {$name}.")
			->addParam("obj", $className, "Het {$className}-object waarvan de waarde wordt verkregen.")
			->setReturn('string', "Een html-veilige string die de waarde van het veld {$name} van het object obj representeert.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function waarde".$this->upper()."($className \$obj)\n"
				.	"\t{\n"
				);

		switch($this->type)
		{
			case 'int':
			case 'bool':
			case 'money':
			case 'string':
			case 'text':
			case 'html':
			case 'date':
			case 'datetime':
			case 'enum':
			case 'url':
				$type = ucfirst($this->type);
				$bestand->addContent("\t\treturn static::defaultWaarde$type(\$obj"
								. ", '".$this->upper()."');\n");
				break;
			case 'bigint':
				$bestand->addContent("\t\treturn static::defaultWaardeInt(\$obj"
								. ", '".$this->upper()."');\n");
				break;
				break;
			default:	// Array
				$foreignname = $this->foreign->name;
				$bestand->addContent("\t\t//Als het object niet bestaat geven we niets terug\n"
						.	"\t\tif(!\$obj->get".$this->upper()."())\n"
						.	"\t\t\treturn NULL;\n"
						.	"\t\treturn ".$foreignname."View::defaultWaarde".$foreignname."(\$obj->get".$this->upper()."());\n");
		}

		$bestand->addContent("\t}\n");

		return;
	}

	/**
	 * Deze methode maakt de form* en genericForm* methoden voor in de views, wordt
	 * aangeroepen vanuit WSW4class mits het om een view gaat
	 * @param Bestand $bestand
	 */
	public function generateCodeToonForm (Bestand $bestand)
	{
		if($this->static
			|| $this->visibility == 'implementation'
			|| $this->visibility == 'protected'
			|| $this->type == 'object'
			|| $this->isForeignKey()
			|| $this->annotation['PRIMARY']
			|| $this->annotation['PSEUDO'])
			return;

		$name = $this->name;
		$className = $this->class->real->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Maak een specifiek formulieronderdeel voor het veld {$name}.")
			->addSee("genericForm{$name}")
			->addParam("obj", $className, "Het object waarvoor een formulieronderdeel nodig is.")
			->addParam("include_id", 'bool', "Indien True wordt de ID van obj meegenomen in de naam van het formulieronderdeel.")
			->setReturn('HtmlElement|null', "Een HtmlElement waarin de huidige waarde van het veld {$name} "
				.	"staat en kan worden bewerkt. Indien {$name} read-only is betreft "
				.	"het een statisch html-element.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function form".$this->upper()."($className \$obj, \$include_id = false)\n"
				.	"\t{\n"
				);

		if($this->visibility == 'private'
			|| $this->visibility == 'protected')
		{
			$bestand->addContent("\t\treturn static::waarde".$this->upper()."(\$obj);\n");
		}
		else
		{
			switch($this->type)
			{
				case 'string':
				case 'text':
				case 'html':
					$type = ucfirst($this->type);
					if ($this->annotation['LANG'])
						$bestand->addContent("\t\treturn array(\n"
								. "\t\t\t'nl' => static::defaultForm$type(\$obj, '".$this->upper()."', 'nl', \$include_id),\n"
								. "\t\t\t'en' => static::defaultForm$type(\$obj, '".$this->upper()."', 'en', \$include_id)\n"
								. "\t\t);\n");
					else
						$bestand->addContent("\t\treturn static::defaultForm$type(\$obj, '".$this->upper()."', null, \$include_id);\n");
					break;
				case 'int':
				case 'money':
					$type = ucfirst($this->type);
					$bestand->addContent("\t\treturn static::defaultForm$type(\$obj"
									. ", '".$this->upper()."', null, null, \$include_id);\n");
					break;
				case 'bigint':
					$bestand->addContent("\t\treturn static::defaultFormInt(\$obj"
									. ", '".$this->upper()."', null, null, \$include_id);\n");
					break;
					break;
				case 'bool':
				case 'date':
				case 'datetime':
				case 'enum':
				case 'url':
					$type = ucfirst($this->type);
					$bestand->addContent("\t\treturn static::defaultForm$type(\$obj"
									. ", '".$this->upper()."', \$include_id);\n");
					break;
				case 'foreign':
					$bestand->addContent("\t\treturn " . $this->foreign->name . "View::defaultForm(\$obj->get" . $this->upper() . "());\n");
					break;
				default:	// Array
					$bestand->addContent("\t\treturn NULL;\n");
			}
		}

		$bestand->addContent("\t}\n");

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Maak een generiek formulieronderdeel voor het veld {$name}. "
				.	"In tegenstelling tot form{$name} moeten naam en waarde meegegeven worden, "
				.	"en worden niet uit het object geladen.")
			->addSee("form{$name}")
			->addParam("name", 'string', "De naam van het formulieronderdeel.")
			->addParam("waarde", 'mixed', "De waarde waar het formulieronderdeel mee begint, of NULL voor default.")
			->setReturn('HtmlElement|null', "Een HtmlElement waarin de huidige waarde van het veld {$name} "
				.	"staat en kan worden bewerkt. Indien {$name} read-only is, betreft "
				.	"het een statisch html-element.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function genericForm".$this->upper()."(\$name, \$waarde=NULL)\n"
				.	"\t{\n"
				);
		if($this->visibility == 'private'
			|| $this->visibility == 'protected')
		{
			$bestand->addContent("\t\treturn \$waarde;\n");
		}
		else
		{
			switch($this->type)
			{
				case 'string':
				case 'text':
				case 'html':
					$type = ucfirst($this->type);
					if ($this->annotation['LANG'])
						$bestand->addContent("\t\treturn array(\n"
								. "\t\t\t'nl' => static::genericDefaultForm$type(\$name, \$waarde, '".$this->upper()."', 'nl'),\n"
								. "\t\t\t'en' => static::genericDefaultForm$type(\$name, \$waarde, '".$this->upper()."', 'en'),\n"
								. "\t\t);\n");
					else
						$bestand->addContent("\t\treturn static::genericDefaultForm$type(\$name, \$waarde, '".$this->upper()."', null);\n");
					break;
				case 'int':
				case 'money':
				case 'bool':
				case 'date':
				case 'datetime':
				case 'url':
					$type = ucfirst($this->type);
					$bestand->addContent("\t\treturn static::genericDefaultForm$type(\$name, \$waarde, '".$this->upper()."');\n");
					break;
				case 'bigint':
					$bestand->addContent("\t\treturn static::genericDefaultFormInt(\$name, \$waarde, '".$this->upper()."');\n");
					break;
				case 'enum':
					// we moeten hier ook de leden van de enum opzoeken
					$type = ucfirst($this->type);
					$bestand->addContent("\t\treturn static::genericDefaultForm$type(\$name, \$waarde, '".$this->upper()."', $className::enums$name());\n");
					break;
				case 'foreign':
					$bestand->addContent("\t\treturn " . $this->foreign->name . "View::genericDefaultForm('" . $this->upper() . "');\n");
					break;
				default:	// Array
					$bestand->addContent("\t\treturn NULL;\n");
			}
		}

		$bestand->addContent("\t}\n");


		return;
	}

	/**
	 * Deze methode maakt de opmerking* methoden voor in de views, wordt
	 * aangeroepen vanuit WSW4class mits het om een view gaat
	 * @param Bestand $bestand
	 */
	public function generateCodeToonOpmerking (Bestand $bestand)
	{
		if($this->static
			|| $this->visibility == 'implementation'
			|| $this->visibility == 'protected'
			|| $this->type == 'object'
			|| $this->isForeignKey())
			return;

		$name = $this->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef een opmerking die gebruikers te zien krijgen als ze het veld {$name} bewerken.")
			->setReturn('string|null', "Een string die een opmerking bij het veld {$name} representeert.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function opmerking".$this->upper()."()\n"
				.	"\t{\n"
				);
		switch($this->type)
		{
			case 'date':
			case 'datetime':
			case 'int':
			case 'bigint':
			case 'bool':
			case 'enum':
			case 'money':
			case 'string':
			case 'text':
			case 'html':
			case 'url':
			default:
				$bestand->addContent("\t\treturn NULL;\n");
		}

		$bestand->addContent("\t}\n");

		return;
	}
}
