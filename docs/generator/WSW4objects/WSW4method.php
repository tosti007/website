<?php

namespace Generator\WSW4objects;

use DocCommentTemplate;
use Generator\Bestand;

class WSW4method
{
	public $name;
	public $comment;
	public $visibility	= 'public';
	public $abstract	= False;
	public $static		= False;
	public $virtual		= False;
	public $generated	= False;

	/**
	 * @var WSW4param[]
	 */
	public $param		= array();

	function __construct($name)
	{
		$this->name = $name;
	}
	public function setComment($comment)
	{
		$this->comment = $comment;
	}
	public function setAbstract($abstract)
	{
		$this->abstract = (bool)$abstract;
	}
	public function setStatic($static)
	{
		$this->static = (bool)$static;
	}
	public function setVisibility($visibility)
	{
		$this->visibility = $visibility;
	}
	public function setVirtual($virtual)
	{
		$this->virtual = $virtual;
		if($virtual && !$this->static)
			$this->abstract = True;
	}
	public function addParam($param)
	{
		$this->param[] = $param;
	}

	/**
	 * Deze methode maakt de bijbehorende implementatie code en schrijft die
	 * gelijk naar $fp
	 * @param Bestand $bestand
	 * @param $cls
	 */
	public function generateCode (Bestand $bestand, $cls)
	{
		if($this->virtual && !$this->generated)
		{
			$this->abstract = True;

			// Static abstract methods bestaan niet! Kan voorkomen dat een
			// method static en virtual gedefinieerd wordt in het model (bijv.
			// Entiteit::geef()), in dat geval wordt 'ie automatisch abstract
			// static in Entiteit.
			if ($this->static) return;
		}

		if ($this->abstract)
		{
			$this->codeMethodHeader($bestand, false);
			$bestand->addContent(";\n");
			return;
		}
		elseif (!$this->virtual)
		{
			$this->codeMethodHeader($bestand);
			$bestand->addContent("\t\tuser_error('501 Not Implemented', E_USER_ERROR);\n"
						."\t}\n");
			return;
		}

		if($cls->getPackage()->name == "Basis")
			return;

		// Voor virtual methods ergens bovenin de klassenhierarchie worden
		// gedeclareerd maar pas lager in de klassehierarchie worden
		// gegenereerd. Uitzonderingsgevallen dus!
		switch($this->name)
		{
			/* Entiteit */
			case 'geefID':
				$this->codeMethodHeader($bestand);
				$this->codeGeefID($bestand, $cls);
				break;
			case 'stopInCache':
				$this->codeInEnUitCache($bestand, $cls);
				break;
			case 'geef':
				// Let op! Methode 'codeGeef' hieronder genereert ook method header!
				$this->codeGeef($bestand, $cls);
				break;
			case 'magKlasseBekijken':
				$this->codeMagKlasseBekijken($bestand, $cls);
				break;
			case 'magKlasseWijzigen':
				$this->codeMagKlasseWijzigen($bestand, $cls);
				break;
			case 'magBekijken':
				$this->codeMagBekijken($bestand, $cls);
				break;
			case 'magWijzigen':
				$this->codeMagWijzigen($bestand, $cls);
				break;
			case 'geefExtensies':
				$this->codeGeefExtensies($bestand, $cls);
				break;
			case 'cache':
				$this->codeCache($bestand, $cls);
				break;
			case 'opslaan':
				$this->codeOpslaan($bestand, $cls);
				break;
			case 'verwijderen':
				$param = new WSW4param("foreigncall", 'NULL');
				$this->addParam($param);
				$this->codeMethodHeader($bestand);
				$this->codeVerwijderen($bestand, $cls);
				break;
			case 'uitDB':
				$this->codeMethodHeader($bestand);
				$this->codeUitDB($bestand, $cls);
				break;
			case 'naarDB':
				$this->codeMethodHeader($bestand);
				$this->codeNaarDB($bestand, $cls);
				break;
			case 'gewijzigd':
				$this->codeGewijzigd($bestand, $cls);
				break;
			case 'velden':
				$this->codeVelden($bestand, $cls);
				break;
			case 'getVeldType':
				$this->codeGetVeldType($bestand, $cls);
				break;
			case 'getBekijkbareVars':
				$this->codeGetBekijkbareVars($bestand, $cls);
				break;
			case 'getWijzigbareVars':
				$this->codeGetWijzigbareVars($bestand, $cls);
				break;
			case 'getDependencies':
				$this->codeGetDependencies($bestand, $cls);
				break;
			default:
				error($this, "Virtual method '" . $this->name . "' gedefinieerd in class diagram, maar is onbekend in codegenerator!");
		}
		return;
	}

	/**
	 * Geneert methode-header en commentaar voor deze method, bijv:
	 * 'protected function bladiebla($myparam, $andereparam)'
	 *
	 * (de accolade die een method body opent wordt _niet_ gegenereerd!)
	 * @param Bestand $bestand
	 * @param bool $print_curly_bracket
	 */
	private function codeMethodHeader(Bestand $bestand, $print_curly_bracket = true)
	{
		$this->comment = trim($this->comment);
		if(!empty($this->comment)) {
			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief($this->comment);
			$bestand->addContent($docCommentTemplate->generate(1));
		}

		$bestand->addContent("\t");
		if($this->abstract)	$bestand->addContent('abstract ');
		if($this->static)	$bestand->addContent('static ');
		$bestand->addContent($this->visibility . ' function '.$this->name.'(');

		$paramString = "";
		$first = True;
		foreach($this->param as $p) {
			$paramString .= $p->generatedCode($first);
			$first = false;
		}

		$bestand->addContent($paramString);
		$bestand->addContent(")");

		if($print_curly_bracket)
			$bestand->addContent("\n\t{\n");
	}

	/* Geef de string met daarin alle IDs terug.
	 * Deze methode besteed al het aan elkaar plak werk uit aan naarID(...)
	 */
	private function codeGeefID (Bestand $bestand, $cls)
	{
		$bestand->addContent("\t\treturn static::naarID("
				.	(count($cls->IDs) > 1 ? " " : "")
				);

		$first = True;
		foreach($cls->IDs as $idn => $idv)
		{
			if($idv->isForeignObject()) continue;
			$getter = preg_replace_callback("/_(.)/",
				function($matches) { return strtoupper($matches[1]); },
				'get_'.$idn
			);
			$bestand->addContent(($first ? "" : "\n\t\t                      , ")
					.	"\$this->$getter()"
					);
			$first = False;
		}
		$bestand->addContent((count($cls->IDs) > 1 ? "\n\t\t                      " : "")
					.	");\n"
				.	"\t}\n"
				);
	}

	private function codeMagKlasseBekijken(Bestand $bestand, $cls)
	{
		$extended = true;
		if(!$cls->IDsource)
			$extended = false;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Retourneert of de gebruiker deze klasse mag zien.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function magKlasseBekijken()\n"
				.	"\t{\n"
				.	"\t\treturn hasAuth('".$cls->annotation['AUTH_GET']."');\n"
				.	"\t}\n");
	}

	private function codeMagKlasseWijzigen(Bestand $bestand, $cls)
	{
		$extended = true;
		if(!$cls->IDsource)
			$extended = false;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Retourneert of de gebruiker een object van deze klasse mag aanpassen.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function magKlasseWijzigen()\n"
				.	"\t{\n"
				.	"\t\treturn hasAuth('".$cls->annotation['AUTH_SET']."');\n"
				.	"\t}\n");
	}

	/**
	 * Implementeert magBekijken()
	 */
	private function codeMagBekijken (Bestand $bestand, $cls)
	{
		$extended = true;
		if(!$cls->IDsource)
			$extended = false;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Retourneert of de gebruiker dit mag zien.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic function magBekijken()\n"
				.	"\t{\n"
				.	"\t\treturn ".$cls->real->name."::magKlasseBekijken()"
				.	($extended ? " || parent::magBekijken();\n" : ";\n")
				.	"\t}\n");
	}

	/**
	 * Implementeert magWijzigen()
	 */
	private function codeMagWijzigen (Bestand $bestand, $cls)
	{
		$extended = true;
		if(!$cls->IDsource)
			$extended = false;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Retourneert of de gebruiker dit object mag aanpassen.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic function magWijzigen()\n"
				.	"\t{\n"
				.	"\t\treturn ".$cls->real->name."::magKlasseWijzigen()"
				.	($extended ? " || parent::magWijzigen();\n" : ";\n")
				.	"\t}\n");
	}

	/**
	 * Implementeer geefExtensies()
	 */
	private function codeGeefExtensies (Bestand $bestand, $cls)
	{
		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft alle klassen terug die een extensie zijn van {$cls->real->name}.")
			->setReturn('array|false', "Een array met de klassen as string, of False als er geen klassen zijn.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function geefExtensies()\n"
				.	"\t{\n");
		if($cls->IDsource)
		{
			$bestand->addContent("\t\treturn false;\n"
					.	"\t}\n"
					);
		} else {

			$bestand->addContent("\t\treturn array(");
			$first = true;
			foreach($cls->IDchildren as $c)
			{
				if(!$first)
					$bestand->addContent(",\n\t\t\t");
				$first = false;
				$bestand->addContent("'" . $c->name . "'");
			}
			$bestand->addContent(");\n"
					.	"\t}\n");
		}
	}

	/**
	 * Implementeer de methoden stopInCache en haalUitCache
	 * @param Bestand $bestand
	 * @param $cls
	 */
	private function codeInEnUitCache(Bestand $bestand, $cls)
	{
		$bestand->addContent("\tstatic public function stopInCache(\$id, \$obj, \$class = NULL, \$key = NULL)\n"
				.	"\t{\n"
				);
		if($cls->IDsource)
		{
			$bestand->addContent("\t\tparent::stopInCache(\$id, \$obj);\n"
					.	"\t}\n\n"
					);
		} else {

			$bestand->addContent("\t\tparent::stopInCache(\$id, \$obj, '"
					.		$cls->real->name."');\n");
			foreach($cls->IDchildren as $c)
			{
				$bestand->addContent("\t\tparent::stopInCache(\$id, \$obj, '"
						.		$c->name."');\n");
			}
			$bestand->addContent("\t}\n\n");
		}
		return;
	}

	/* Retourneert een object, dan wel NULL als het object niet bestaat.
	 * Deze methode besteed het querien uit aan cache(...)
	 */
	private function codeGeef (Bestand $bestand, $cls)
	{
		$classname = $cls->real->name;
		$hasFK = false;

		$letter = 'a';
		foreach($cls->IDs as $n => $id)
		{
			if($id->isForeignObject()) {
				$hasFK = true;
				continue;
			}

			$args[] = $letter++;
			$ids[$n] = $id;
		}
		$argstr = '$' . implode(', $', $args);
		$idstr = '$' . implode(', $', array_keys($ids));
		$fstarg = $args[0];

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief($this->comment . "\n"
				.	"\n"
				.	"geef() kan op verschillende manieren aangeroepen worden:\n"
				.	"- alle parameters netjes invullen (dit heeft de voorkeur)\n"
				.	"- alleen de eerste parameter, met als vorm: \"\$aID_\$bID\"\n"
				.	"- alleen de eerste parameter, met als vorm: array(\$aID, \$bID)"
			)->setReturn("{$classname}|false", "Een {$classname}-object als deze gevonden wordt; False anders.");

		foreach ($args as $arg) {
			$docCommentTemplate->addparam($arg, 'mixed', "Object of ID waarop gezocht moet worden.");
		}

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function geef("
				);
		$first = true;
		foreach($args as $arg) {
			$bestand->addContent(($first?'':', ').'$'.$arg.($first?'':' = NULL'));
			$first = false;
		}
		$bestand->addContent(")\n\t{\n"
				.	"\t\tif(is_null(\$"
				.		implode(")\n"
				.	"\t\t&& is_null(\$", $args) . "))\n"
				.	"\t\t\treturn false;\n"
				.	"\n"
				.	"\t\tif(is_string(\$"
				.		implode(")\n"
				.	"\t\t&& is_null(\$", $args) . "))\n"
				.	"\t\t\t\$$fstarg = static::vanID(\$$fstarg); # string -> array\n"
				.	"\n"
				.	"\t\tif(is_array(\$"
					. implode(")\n"
				.	"\t\t&& is_null(\$", $args) . ")\n"
				.	"\t\t&& count(\$$fstarg) == ".count($args).")\n"
				.	"\t\t{\n"
				);
		$nr = 0;
		foreach($ids as $n => $id) {
			$bestand->addContent("\t\t\t$$n = ".maybeint($n)."$".$args[0]."[".$nr++."];\n");
		}
		$bestand->addContent("\t\t}\n");

		if($hasFK) {
			$nr = 0;
			$first = true;
			foreach($cls->IDs as $id)
			{
				if($id->isForeignObject()) {
					$bestand->addContent(($first
								? "\t\telse if("
								: "\n\t\t     && ")
							.	'$'.$args[$nr++].' instanceof '.$id->foreign->name
							);
					$first = false;
					continue;
				}
				if($id->isForeign())
					continue;

				$bestand->addContent(($first
							? "\t\telse if("
							: "\n\t\t     && ")
						.	'isset($'.$args[$nr++].')'
						);
				$first = false;
			}
			while($nr < count($args)) {
				$bestand->addContent("\n\t\t     && !isset($".$args[$nr++].')');
			}
			$bestand->addContent(")\n"
					.	"\t\t{\n"
					);
			$nr = 0;
			foreach($cls->IDs as $n => $id)
			{
				if(!$id->isForeign()) {
					$bestand->addContent("\t\t\t$$n = ".maybeint($n)."$".$args[$nr++].";\n");

					continue;
				}
				if(!$id->isForeignObject())
					continue;

				foreach($id->foreign->IDs as $a => $b)
				{
					if($b->isForeignObject()) continue;
					$keyget = preg_replace_callback("/_(.)/",
						function($matches) { return strtoupper($matches[1]); },
						"get_".$b->upper()
					);
					$tail = array_reverse(explode('_', $a));
					$bestand->addContent("\t\t\t$${n}_$tail[0] = $".$args[$nr]."->$keyget();\n");
				}
				$nr++;
#							, "get_".$found->upper());
			}
			$bestand->addContent("\t\t}\n");
		}
		$bestand->addContent("\t\telse if(isset(\$"
					. implode(")\n"
				.	"\t\t     && isset(\$", $args) . "))\n"
				.	"\t\t{\n"
				);
		$nr = 0;
		foreach($ids as $n => $id) {
			$bestand->addContent("\t\t\t$$n = ".maybeint($n)."$".$args[$nr++].";\n");
		}
		$bestand->addContent("\t\t}\n"
				.	"\n"
				.	"\t\tif(is_null(\$"
					. implode(")\n"
				.	"\t\t|| is_null($", array_keys($ids))."))\n"
				.	"\t\t\tthrow new BadMethodCallException();\n"
				.	"\n"
				.	"\t\tstatic::cache(array( array($idstr) ));\n"
				.	"\t\treturn Entiteit::geefCache(array($idstr), '$classname');\n"
				.	"\t}\n"
				);
	}

	/* Deze methode cache($ids) probeert de gegeven $ids in de cache te stoppen.
	 * $ids die al in de cache zitten worden overgeslagen
	 * de overige $ids proberen we uit de DB te halen
	 * alles wat ook daar niet in zat setten we op False
	 */
	private function codeCache(Bestand $bestand, $cls)
	{
		$classname = $cls->real->name;

		// Array van de private/id-velden van het object.
		// Deze moeten meegegeven worden aan de constructor.
		// Format: [phpveldnaam => $veld]
		$constructorParams = array();
		// Deze velden worden door de constructor geset.
		// Merk op dat het niet hetzelfde is als $constructorParams:
		// daarin staan de parameters, hierin het gevolg.
		// In het bijzonder zijn foreign keys naar WSW4-objecten
		// wel in deze array maar niet in de vorige.
		$constructedFields = array();

		// Zoek in de inheritancehierarchie alle variabelen op.
		$vars = array();
		for($nxt = $cls; $nxt; $nxt = $nxt->inherits)
		{
			if($nxt->name === 'Entiteit')
				break;
			$vars = array_merge($nxt->vars, $vars);
		}

		foreach($vars as $id)
		{
			if($id->visibility !== 'private' && !$id->annotation['PRIMARY'])
				continue;

			// Het auto-increment-veld gaat vanzelf.
			if($id->annotation['PRIMARY'] && $id->type == 'int' && !$id->isForeign())
				continue;

			// We werken bij foreign variabele vanuit het object, niet de subkeys.
			if ($id->isForeignKey())
			{
				continue;
			}
			if ($id->isForeignObject())
			{
				// Door het object te setten, worden ook zijn keys geset.
				foreach ($id->subKeys as $subkey)
				{
					$constructedFields[$subkey->name] = $subkey;
				}
			}

			$constructorParams[$id->name] = $id;
			$constructedFields[$id->name] = $id;
		}

		// Zoek de keys op waarmee we gaan SELECT-en.
		$primaryKeys = []; // Lijst van WSW4Vars
		$primaryKeysForDB = []; // Lijst van strings
		$primaryKeysForPHP = []; // Lijst van strings
		foreach($cls->IDs as $id)
		{
			if($id->isForeignObject()) continue;
			$primaryKeys[] = $id;
			$primaryKeysForDB[] = $id->nameForDB;
			$primaryKeysForPHP[] = $id->name;
		}

		// Geef de queryparameter die bij de id's hoort.
		if(count($primaryKeys) > 1) {
			$idsParameter = '%A{';
			foreach($primaryKeys as $id) {
				$idsParameter .= $id->getLibDBType();
			}
			$idsParameter .= '}';
		} else {
			$idsParameter = '%A{i}';
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief(str_replace("\n", "\n\t * ", $this->comment));

		if(count($primaryKeys) == 1) {
			$docCommentTemplate->addParam("ids", 'array', "is een ARRAY met per object een int. "
				.	"\$ids = array( aID, bID, ...)");
		} else {
			$docCommentTemplate->addParam("ids", 'array', "is een ARRAY met per object een array met de ID's. "
				.	"\$ids = array( array(a1ID,a2ID), array(b1ID,b2ID), ...)");
		}
		$docCommentTemplate->addParam("recur", 'bool|array', "recur is een parameter om aan te "
				.	"geven of we een recursieve invulstap moeten doen.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function cache(\$ids, \$recur = False)\n"
				.	"\t{\n"
				.	"\t\tglobal \$WSW4DB;\n"
				.	"\t\t\$cachetm = NULL;\n"
				.	"\n"
				);
		$ind = "\t\t";
		if($cls->IDsource || $cls->IDchildren)
		{
			$bestand->addContent("\t\tif(\$recur === False)\n"
					.	"\t\t{\n"
					);
			$ind = "\t\t\t";
		}
		if($cls->IDsource)
		{
			$bestand->addContent("\t\t\t// de basis klasse gaat het allemaal fixen\n"
					.	"\t\t\treturn ".$cls->IDsource->name."::cache(\$ids);\n"
					.	"\t\t}\n"
					.	"\n"
					);
		} else
		{
			$bestand->addContent("$ind\$ids = Entiteit::nietInCache(\$ids, '$classname');\n"
					.	"\n"
					.	"$ind// is er nog iets te doen?\n"
					.	"${ind}if(empty(\$ids)) return;\n"
					.	"\n"
					);
			if($cls->IDchildren)
			{
				$bestand->addContent("\t\t\t// zoek uit wat de klasse van een id is\n"
						.	"\t\t\t\$res = \$WSW4DB->q('TABLE SELECT `"
						.	implode("`'\n\t\t\t                 .     ', `"
								, $primaryKeysForDB
								) . "`'\n"
						.	"\t\t\t                 .     ', `overerving`'\n"
						.	"\t\t\t                 .' FROM `$classname`'\n"
						.	"\t\t\t                 .' WHERE (`"
						.	implode("`, `" , $primaryKeysForDB) . "`)'\n"
						.	"\t\t\t                 .      ' IN (".$idsParameter.")'\n"
						.	"\t\t\t                 , \$ids\n"
						.	"\t\t\t                 );\n"
						.	"\n"
						.	"\t\t\t\$recur = array();\n"
						.	"\t\t\tforeach(\$res as \$row)\n"
						.	"\t\t\t{\n"
						.	"\t\t\t\t\$recur[\$row['overerving']][]\n"
						.	"\t\t\t\t    = array(\$row['"
						.	implode("']\n"
						.	"\t\t\t\t           ,\$row['"
								, $primaryKeysForDB
								) . "']);\n"
						.	"\t\t\t}\n"
						.	"\t\t\tforeach(\$recur as \$class => \$obj)\n"
						.	"\t\t\t{\n"
						.	"\t\t\t\t\$class::cache(\$obj, True);\n"
						.	"\t\t\t}\n"
						.	"\n"
						.	"\t\t\treturn;\n"
						.	"\t\t}\n"
						.	"\n"
						);
			}
		}

		if($cls->IDsource || $cls->IDchildren)
		{
			$bestand->addContent("\t\tif(!is_array(\$recur))\n"
					.	"\t\t{\n"
					);
			$ind = "\t\t\t";
		}
		else
		{
			$ind = "\t\t";
		}

		$bestand->addContent("$ind\$cachetm = "
					.	"new ProfilerTimerMark('$classname"
						.	"::cache( #'.count(\$ids).' )');\n"
				.	"${ind}Profiler::getSingleton()->addTimerMark(\$cachetm);\n"
				.	"\n"
				.	"$ind// query alles in 1x\n");
		$select_vars = array();
		$select_from = array();
		for($nxt = $cls; $nxt; $nxt = $nxt->inherits)
		{
			if(!$nxt->generated)
				continue;

			$clsprefix = '`'.$nxt->real->name.'`.`';
			foreach($nxt->vars as $var)
			{
				if ($var->annotation['PSEUDO'])
					continue;
				if($var->type == 'object') continue;
				if($var->isForeignObject()) continue;
				if($var->annotation['LAZY']) continue;
				if($var->annotation['LANG']) {
					$select_vars[] = $clsprefix.$var->nameForDB.'_NL`';
					$select_vars[] = $clsprefix.$var->nameForDB.'_EN`';
					continue;
				}
				$select_vars[] = $clsprefix.$var->nameForDB.'`';
			}
			if($nxt != $cls)
				$select_from[] = $nxt->real->name;

			if(!$nxt->IDsource)
				break;
		}
		if($cls->IDsource)
		{
			$select_vars[] = "`".$cls->IDsource->name."`.`gewijzigdWanneer`";
			$select_vars[] = "`".$cls->IDsource->name."`.`gewijzigdWie`";
		}
		else
		{
			$select_vars[] = "`$classname`.`gewijzigdWanneer`";
			$select_vars[] = "`$classname`.`gewijzigdWie`";
		}

		$first = True;
		foreach($select_vars as $var) {
			$bestand->addContent(($first
							? "$ind\$res = \$WSW4DB->q('TABLE SELECT"
							: "$ind                 .     ',")
					.	" $var'\n");
			$first = False;
		}
		$bestand->addContent("$ind                 .' FROM `$classname`'\n");
		foreach($select_from as $from)
		{
		$bestand->addContent("$ind                 .' LEFT JOIN `$from` USING (`"
				.	implode("`'\n"
				.	"$ind                 .                         ',`"
						, $primaryKeysForDB
						) . "`)'\n"
				);
		}
		$bestand->addContent("$ind                 .' WHERE (`"
				.	implode("`, `", $primaryKeysForDB). "`)'\n"
				.	"$ind                 .      ' IN (".$idsParameter.")'\n"
				.	"$ind                 , \$ids\n"
				.	"$ind                 );\n"
				);

		if($cls->IDsource || $cls->IDchildren)
		{
			$bestand->addContent("\t\t}\n"
					.	"\t\telse\n"
					.	"\t\t{\n"
					.	"\t\t\t\$res = array(\$recur);\n"
					.	"\t\t}\n"
					);
		}
		$bestand->addContent("\n"
				.	"\t\t\$found = array();\n"
				.	"\t\tforeach(\$res as \$row)\n"
				.	"\t\t{\n"
				.	"\t\t\t\$id = static::naarID(\$row['"
				.	implode("']\n"
				.	"\t\t\t                  ,\$row['"
						, $primaryKeysForDB
						) . "']);\n"
				.	"\n"
				);

		// Vertaal de rij uit de db naar args voor de constructor
		// en velden die geassigned moeten worden.
		// Eerst de constructor:
		$constructorArgsStr = '';
		if(count($constructorParams))
		{
			$first = true;
			foreach($constructorParams as $veld)
			{
				if($first)
				{
					$first = false;
				}
				else
				{
					$constructorArgsStr .= ', ';
				}

				// Als het veld een object is,
				// geven we de subkeys als array.
				if ($veld->isForeignObject())
				{
					$constructorArgsStr .= 'array(';
					$subKeyCount = 0;
					foreach ($veld->subKeys as $subKey)
					{
						if ($subKeyCount > 0)
						{
							$constructorArgsStr .= ', ';
						}
						$constructorArgsStr .= "\$row['" . $subKey->nameForDB . "']";
						$subKeyCount++;
					}
					$constructorArgsStr .= ')';
				}
				else
				{
					// Dit veld is niet een object of foreign key,
					// en komt direct uit de query rollen.
					assert(!$veld->isForeign());
					$constructorArgsStr .= "\$row['" . $veld->nameForDB . "']";
				}
			}
		}

		if($cls->IDsource || $cls->IDchildren)
		{
			$bestand->addContent("\t\t\tif(is_array(\$recur)) {"
						.	" // dit is een recursieve invulstap\n"
					.	"\t\t\t\t\$obj = static::\$objcache['$classname'][\$id];\n"
					.	"\t\t\t} else {\n"
					.	"\t\t\t\t\$obj = new $classname($constructorArgsStr);\n"
					.	"\t\t\t}\n"
				);
		}
		else
		{
			$bestand->addContent("\t\t\t\$obj = new $classname($constructorArgsStr);\n");
		}
		$bestand->addContent("\n"
				.	"\t\t\t\$obj->inDB = True;\n"
				.	"\n"
				);
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
			{
				continue;
			}
			// Verwijzingen naar objecten hoeven niet geset.
			if ($var->isForeignObject())
			{
				continue;
			}
			if($var->annotation['LAZY'])
			{
				continue;
			}

			// Dit veld wordt al in de constructor geset:
			// of direct als parameter van de constructor
			if(isset($constructedFields[$var->name]))
			{
				continue;
			}

			switch($var->type) {
			case 'bool':
				$type = '(bool)';
				break;
			default:
				$type = '';
			}

			if($var->annotation['LANG']) {
				$bestand->addContent("\t\t\t\$obj->" . $var->name . "['nl']"
							. " = $type\$row['" . $var->nameForDB . "_NL'];\n"
						.	"\t\t\t\$obj->" . $var->name . "['en']"
							. " = $type\$row['" . $var->nameForDB . "_EN'];\n"
						);
				continue;
			}
				if($var->annotation['NULL'])
					$null = '(is_null($row[\'' . $var->nameForDB . '\'])) ? null : ';
				else
					$null = '';

				switch ($var->type)
				{
					case 'int':
					case 'bool':
						$bestand->addContent("\t\t\t" . '$obj->' . $var->name .
							'  = ' . $null . '(' . $var->type . ') $row[\'' . $var->nameForDB . '\'];' . "\n");
						break;
					case 'bigint':
						$bestand->addContent("\t\t\t" . '$obj->' . $var->name .
							'  = ' . $null . '(int) $row[\'' . $var->nameForDB . '\'];' . "\n");
						break;
					case 'money':
						$bestand->addContent("\t\t\t" . '$obj->' . $var->name .
							'  = ' . $null . '(float) $row[\'' . $var->nameForDB . '\'];' . "\n");
						break;
					case 'string':
					case 'text':
						$bestand->addContent("\t\t\t" . '$obj->' . $var->name .
								'  = ' . $null . 'trim($row[\'' . $var->nameForDB . '\']);' . "\n");
						break;
					case 'date':
					case 'datetime':
						$bestand->addContent("\t\t\t" . '$obj->' . $var->name .
								'  = new DateTimeLocale($row[\'' . $var->nameForDB . '\']);' . "\n");
						break;
					case 'enum':
						$bestand->addContent("\t\t\t" . '$obj->' . $var->name .
								'  = ' . $null . 'strtoupper(trim($row[\'' . $var->nameForDB . '\']));' . "\n");
						break;
					default:
						$bestand->addContent("\t\t\t\$obj->" . $var->name . " = $type\$row['" . $var->nameForDB . "'];\n");
						break;
					}
		}
		if(!$cls->IDsource)
		{
			$bestand->addContent("\t\t\t\$obj->gewijzigdWanneer"
						. " = new DateTimeLocale(\$row['gewijzigdWanneer']);\n"
					.	"\t\t\t\$obj->gewijzigdWie"
						. " = \$row['gewijzigdWie'];\n"
					.	"\t\t\t\$obj->dirty = false;\n"
					);
		}
		if($cls->IDsource || $cls->IDchildren)
		{
			$bestand->addContent("\t\t\tif(\$recur === True)\n"
					.	"\t\t\t\tself::stopInCache(\$id, \$obj);\n"
				.	($cls->IDsource
					?	"\n"
					.	"\t\t\t// naar de super klasse de andere velden\n"
					.	"\t\t\t".$cls->inherits->name."::cache(array(), \$row);\n"
					:	""
					)
					.	"\n"
					.	"\t\t\tif(is_array(\$recur))\n"
					.	"\t\t\t\treturn; // dit was een recursieve invul stap\n"
					);
		}
		else
		{
			$bestand->addContent("\t\t\tself::stopInCache(\$id, \$obj);\n"
					);
		}
		$bestand->addContent("\n"
				.	"\t\t\t\$obj->errors = array();\n"
				.	"\t\t\tforeach(static::velden('get') as \$veld)\n"
				.	"\t\t\t\t\$obj->errors[\$veld] = False;\n"
				.	"\n"
				.	"\t\t\t\$found[\$id] = True;\n"
				.	"\t\t}\n"
				.	"\n"
				.	"\t\t// markeer ook de ids die we niet konden vinden\n"
				.	"\t\tforeach(\$ids as \$id)\n"
				.	"\t\t{\n"
				.	"\t\t\tif(!isset(\$found[static::naarID(\$id)]))\n"
				.	"\t\t\t\tself::stopInCache(\$id, False);\n"
				.	"\t\t}\n"
				.	"\n"
				.	"\t\tif(\$cachetm) \$cachetm->markEnd();\n"
				.	"\t}\n"
				);
	}

	// object aanmaken in db & toevoegen aan cache
	private function codeOpslaan(Bestand $bestand, WSW4class $cls)
	{
		// TODO what als ==eigenschap en $key == increment ?

		$classname = $cls->real->name;
		$single = False;
		if(count($cls->IDs) == 1)
			$single = reset($cls->IDs)->name;

		if(!empty($this->comment)) {
			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief($this->comment);

			$bestand->addContent($docCommentTemplate->generate(1));
		}

		$keystr = '';
		$str = <<<EOT
	public function $this->name($keystr\$classname = '$classname')
	{
		global \$WSW4DB;

		if(!\$this->valid()) {
			\$errorInfo = "";
			\$first = true;
			foreach (\$this->getErrors() as \$id => \$error) {
				if (is_string(\$error)) {
					if (\$first) \$first = false;
					else \$errorInfo .= ",";
					\$errorInfo .= "\\n\$id: '\$error'";
				}
				else if (is_array(\$error))
					foreach (\$error as \$lang => \$msg)
						if (is_string(\$msg)) {
							if (\$first) \$first = false;
							else \$errorInfo .= ",";
							\$errorInfo .= "\\n\$id (\$lang): '\$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . \$errorInfo);
		}
EOT;
		$bestand->addContent($str);

		foreach($cls->IDs as $id => $var)
		{
			if(!$var->isForeignObject()) continue;

			$objname = $var->upper();
			$bestand->addContent("\n"
				.	"\t\t\$rel = \$this->get$objname();\n"
				.	"\t\tif(!\$rel->getInDB())\n"
				.	"\t\t\tthrow new LogicException('foreign $objname is not in DB');\n"
				);
			foreach($var->foreign->IDs as $n => $c) {
				if($c->isForeignObject()) continue;
				$getter = preg_replace_callback("/_(.)/",
					function($matches) { return strtoupper($matches[1]); },
					$c->upper()
				);

				$name = $c->name;
				if($c->isForeign())
					$name = substr($name, strrpos($name, '_') + 1);
				$bestand->addContent("\t\t\$this->".$id."_".$name
						.	" = \$rel->get$getter();\n"
						);
			}
		}
		if($cls->IDsource)
		{
			$bestand->addContent("\n"
					  . "\t\t\$isDirty = \$this->dirty;");
			$bestand->addContent("\n"
					.	"\t\tparent::opslaan(\$classname);\n"
					);
			$bestand->addContent("\n"
					.	"\t\t\$stillDirty = \$this->dirty;\n"
					.   "\n"
					.	"\t\t// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan\n"
					.	"\t\tif (\$isDirty == \$stillDirty && !\$this->dirty)\n"
					.	"\t\t\treturn;\n");
			$single = False;
		} else {
			$bestand->addContent("\n"
					.	"\t\tif (!\$this->dirty)\n"
					.	"\t\t\treturn;\n");
		}
		$first = true;
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if(in_array($var, $cls->IDs))
				continue;
			if(!$var->isForeignKey())
				continue;

			$getter = preg_replace_callback("/_(.)/",
				function($matches) { return strtoupper($matches[1]); },
				'get_'.$var->name
			);
			$bestand->addContent("\t\t\$this->$getter();\n");
			continue;
		}
		$bestand->addContent("\n"
				.	"\t\t\$this->gewijzigd();\n"
				);
		if(!$cls->IDsource) {
			$bestand->addContent("\n"
					);
		}
		$bestand->addContent("\t\tif(!\$this->inDB) {\n");
		$bestand->addContent(($single ? "\t\t\t\$this->$single =\n" : "")
				.	"\t\t\t\$WSW4DB->q('".($single ? "RETURNID " : "")
					.	"INSERT INTO `$classname`'\n"
				);

		/** @var string[] $insertVelden
		 * Mapt veldnaam naar waarde (bijvoorbeeld 'overerving' => '%s'.
		 */
		$insertVelden = [];

		if($cls->IDchildren)
		{
			$insertVelden['overerving'] = '%s';
		}
		if($cls->IDsource)
		{
			foreach($cls->IDs as $var)
			{
				$insertVelden[$var->nameForDB] = '%' . $var->getLibDBType();
			}
		}
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if($single && isset($cls->IDs[$var->name])) {
				$single = $var->name;
				continue;
			}
			if($var->type == 'object') continue;
			if($var->isForeignObject()) continue;
			if($var->annotation['LANG']) {
				$insertVelden[$var->nameForDB . '_NL'] = '%' . $var->getLibDBType();
				$insertVelden[$var->nameForDB . '_EN'] = '%' . $var->getLibDBType();
			} else {
				$insertVelden[$var->nameForDB] = '%' . $var->getLibDBType();
			}
		}
		if(!$cls->IDsource)
		{
			$insertVelden['gewijzigdWanneer'] = '%s';
			$insertVelden['gewijzigdWie'] = '%i';
		}

		// Geef de veldnamen om in te inserten.
		$bestand->addContent("\t\t\t          . ' (");
		$first = true;
		foreach ($insertVelden as $veldnaam => $expressie)
		{
			if (!$first)
			{
				$bestand->addContent(', ');
			}
			$bestand->addContent('`' . $veldnaam . '`');
			$first = false;
		}
		$bestand->addContent(")'\n");
		// Geef de waarden om te inserten.
		$bestand->addContent("\t\t\t          . ' VALUES (");
		$first = true;
		foreach ($insertVelden as $expressie)
		{
			if (!$first)
			{
				$bestand->addContent(', ');
			}
			$bestand->addContent($expressie);
			$first = false;
		}
		$bestand->addContent(")'\n");

		if($cls->IDchildren)
		{
			$bestand->addContent("\t\t\t          , \$classname\n");
		}
		if($cls->IDsource)
		{
			foreach($cls->IDs as $var)
			{
				$bestand->addContent("\t\t\t          , \$this->". $var->name . "\n");
			}
		}
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if($single == $var->name) continue;
			if($var->type == 'object') continue;
			if($var->isForeignObject()) continue;
			if($var->annotation['LAZY']) //Code duplicatie, doe iets slims?
			{
				if($var->annotation['LANG']) {
					$bestand->addContent("\t\t\t          , isset(\$this->". $var->name . "['nl'])?\$this->". $var->name . "['nl']:".$var->default."\n"
							.	"\t\t\t          , isset(\$this->". $var->name . "['en'])?\$this->". $var->name . "['en']:".$var->default."\n"
							);
				} elseif ($var->type == 'date' || $var->type == 'datetime') {
					$bestand->addContent("\t\t\t          , isset(\$this->". $var->name . ")?\$this->". $var->name . "->strftime('%F %T'):".$var->default."\n");
				} else {
					$bestand->addContent("\t\t\t          , isset(\$this->". $var->name . ")?\$this->". $var->name . ":".$var->default."\n");
				}
			} else {
				if($var->annotation['LANG']) {
					$bestand->addContent("\t\t\t          , \$this->". $var->name . "['nl']\n"
							.	"\t\t\t          , \$this->". $var->name . "['en']\n"
							);
				} elseif ($var->type == 'date' || $var->type == 'datetime') {
					if ($var->annotation['NULL'])
						$bestand->addContent("\t\t\t          , (!is_null(\$this->". $var->name . "))?\$this->". $var->name . "->strftime('%F %T'):null\n");
					else
						$bestand->addContent("\t\t\t          , \$this->". $var->name . "->strftime('%F %T')\n");
				} else {
					$bestand->addContent("\t\t\t          , \$this->". $var->name . "\n");
				}
			}
		}
		$bestand->addContent((!$cls->IDsource
				?	"\t\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
				.	"\t\t\t          , \$this->gewijzigdWie\n"
				:	"")
				.	"\t\t\t          );\n"
				.	"\n"
				.	"\t\t\tif(\$classname == '$classname')\n"
				.	"\t\t\t\t\$this->inDB = True;\n"
				.	"\n"
				.	"\t\t\tself::stopInCache(\$this->geefID(), \$this);\n"
				.	"\t\t} else {\n"
				.	"\t\t\t\$WSW4DB->q('UPDATE `$classname`'\n"
				);
		// Komende regels zijn enorme code-duplicatie, moet iets beters voor te verzinnen zijn
		$first = True;
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if(isset($cls->IDs[$var->name])) continue;
			if($var->annotation['LAZY']) continue;
			if($var->isForeignObject()) continue;
			if($var->type == 'object') continue;
			if($var->annotation['LANG']) {
				$bestand->addContent("\t\t\t          "
							. ($first ? ".' SET" : ".   ',") . " `"
							. $var->name . "_NL` = %"
							. $var->getLibDBType() . "'\n"
						.	"\t\t\t          .   ', `"
							. $var->name . "_EN` = %"
							. $var->getLibDBType() . "'\n");
			} else {
				$bestand->addContent("\t\t\t          "
							. ($first ? ".' SET" : ".   ',")
							. " `" . $var->name . "` = %"
							. $var->getLibDBType() . "'\n");
			}
			$first = False;
		}
		if(!$cls->IDsource)
		{
			$bestand->addContent("\t\t\t" . ($first ? ".' SET" : ".   ',") . " `gewijzigdWanneer` = %s'\n"
					.	"\t\t\t          .   ', `gewijzigdWie` = %i'\n"
					);
		}

		$bestand->addContent($cls->genSQLWhere(3));

		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if(isset($cls->IDs[$var->name])) continue;
			if($var->isForeignObject()) continue;
			if($var->type == 'object') continue;
			if($var->annotation['LAZY']) continue;
			if($var->annotation['LANG']) {
				$bestand->addContent("\t\t\t          , \$this->". $var->name . "['nl']\n"
						.	"\t\t\t          , \$this->". $var->name . "['en']\n"
						);
			} elseif ($var->type == 'date' || $var->type == 'datetime') {
				if ($var->annotation['NULL'])
					$bestand->addContent("\t\t\t          , (!is_null(\$this->". $var->name . "))?\$this->". $var->name . "->strftime('%F %T'):null\n");
				else
					$bestand->addContent("\t\t\t          , \$this->". $var->name . "->strftime('%F %T')\n");
			} else {
				$bestand->addContent("\t\t\t          , \$this->". $var->name . "\n");
			}
		}
		$bestand->addContent((!$cls->IDsource
				?	"\t\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
				.	"\t\t\t          , \$this->gewijzigdWie\n"
				:	"")
				);
		$bestand->addContent($cls->genSQLWhereArgs(3));
		$bestand->addContent("\t\t\t          );\n");
		$bestand->addContent("\t\t}\n\t\t\$this->dirty = false;\n");
		$bestand->addContent("\t}\n");
	}

	// verwijder object uit cache & db
	private function codeVerwijderen(Bestand $bestand, WSW4class $cls)
	{
		global $objects;

		$classname = $cls->real->name;

		$bestand->addContent("\t\tglobal \$WSW4DB;\n"
				.	"\n"
				.	"\t\tif(!\$this->inDB)\n"
				.	"\t\t\tthrow new LogicException('object is not inDB');\n"
				.	"\n"
				.	"\t\t\$magVerwijderen = \$this->magVerwijderen();\n"
				.	"\t\tif(!\$magVerwijderen)\n"
				.	"\t\t{\n"
				.	"\t\t\treturn(\"Je mag het object niet verwijderen.\");\n"
				.	"\t\t}\n"
				);

		foreach($cls->foreignrelations as $c => $isnull) {
			$childclass = $objects['class'][$c."_Generated"];
			foreach($childclass->vars as $id)
			{
				if($id->isForeignObject() && $id->foreign->name == $cls->real->name) {
					// We kunnen alle objecten met een foreign key naar dit object verwijderen,
					// maar als het kan, zetten we dat veld gewoon op null.
					// We mogen een veld op null zetten als die null lust,
					// en als die niet private is (want private staat voor immutable...)
					if ($isnull && $id->visibility != 'private')
					{
						// Welke operaties doen we op de foreign klasse?
						// Komt in de comment bij dit stukje code.
						$foreignOperatie = "om de foreign key op null te zetten";

						// Welke methode roepen we aan om te checken
						// dat deze operatie mag?
						$rechtenCheck = "magWijzigen";

						// Wat melden we aan de gebruiker als het misgaat?
						$foutmelding = "mag niet gewijzigd worden";
					}
					else
					{
						$foreignOperatie = "om te verwijderen";
						$rechtenCheck = "magVerwijderen";
						$foutmelding = "mag niet verwijderd worden";
					}

					// De naam van de foreign key.
					$foreignKeyNaam = ucfirst($id->name);
					// De naam van de verzameling gerelateerde objecten.
					// (Dit willen we uniek hebben, dus vandaar klasse en foreign key).
					$verzamelingNaam = '$' . $c . "From" . $foreignKeyNaam . "Verz";

					$bestand->addContent("\n"
						.	"\t\t// Verzamel alle ".$c."-objecten die op dit object dependen\n"
						.	"\t\t// " . $foreignOperatie . ",\n"
						.	"\t\t// en return een error als ze niet aangepakt mogen worden.\n"
						.	"\t\t".$verzamelingNaam." = ".$c."Verzameling::from".$foreignKeyNaam."(\$this);\n"
						.	"\t\t\$returnValue = ".$verzamelingNaam."->".$rechtenCheck."();\n"
						.	"\t\tif(\$returnValue !== true)\n"
						.	"\t\t{\n"
						.	"\t\t\tforeach(\$returnValue as \$i => \$val)\n"
						.	"\t\t\t{\n"
						.	"\t\t\t\t\$returnValue[\$i] = \"Een object ".$c." met id \".\$val.\" verwijst naar het te verwijderen object, maar " . $foutmelding . ".\";\n"
						.	"\t\t\t}\n"
						.	"\t\t\treturn \$returnValue;\n"
						.	"\t\t}\n"
					);
				}
			}
		}

		$bestand->addContent("\n"
			.       "\t\t//Hier begint de mysql-transactie\n"
			.       "\t\tif(!\$foreigncall)\n"
			.       "\t\t\t\$WSW4DB->q('START TRANSACTION');\n"
			.       "\t\t\$queryarray = array();\n"
		);

		$foreigndeletearray = array();

		if(isset($cls->foreignrelations)) {
			foreach($cls->foreignrelations as $c => $isnull) {
				$childclass = $objects['class'][$c."_Generated"];
				foreach($childclass->vars as $id)
				{
					if($id->isForeignObject() && $id->foreign->name == $cls->real->name) {
						if(!$isnull || $id->visibility == 'private') {
							$foreigndeletearray[$c] = $id->name;
						} else {
							$bestand->addContent("\t\tforeach(\$".$c."From".ucfirst($id->name)."Verz as \$v)\n"
								.       "\t\t{\n"
								.       "\t\t\t\$v->set".ucfirst($id->name)."(NULL);\n"
								.       "\t\t}\n"
								.       "\t\t\$".$c."From".ucfirst($id->name)."Verz->opslaan();\n"
								.       "\n"
							);
						}
					}
				}
			}
		}

		$bestand->addContent("\t\t// Als de return-value van het verwijderen van dependencies\n"
			.		"\t\t// niet een array is, is het een string met een foutcode\n");
		foreach($foreigndeletearray as $c => $name) {
			$bestand->addContent("\t\t\$returnValue = \$" . $c . "From" . ucfirst($name) . "Verz->verwijderen(true);\n"
				.		"\t\tif(!is_array(\$returnValue))\n"
				.		"\t\t{\n"
				.		"\t\t\treturn \$returnValue;\n"
				.		"\t\t} else {\n"
				.		"\t\t\t\$queryarray[] = \$returnValue;\n"
				.		"\t\t}\n\n");
		}

		$bestand->addContent("\n"
				.	"\t\t// uit de DB\n"
				.	"\t\t\$query = \$WSW4DB->q('DELETE FROM `$classname`'\n");
		$bestand->addContent($cls->genSQLWhere(2));
		$bestand->addContent("\t\t          .' LIMIT 1'\n");
		$bestand->addContent($cls->genSQLWhereArgs(2));
		$bestand->addContent("\t\t          );\n");

		if($cls->IDsource)
		{
			$bestand->addContent("\n"
					.	"\t\t\$queryarray[] = parent::verwijderen(true);\n"
					);
		}


		$bestand->addContent("\n"
			.       "\t\t// Verzamel alle queries in 1 array\n"
			.       "\t\t\$finalqueries = array();\n"
			.       "\t\t\$it = new RecursiveIteratorIterator(new RecursiveArrayIterator(\$queryarray));\n"
			.       "\t\tforeach(\$it as \$singlequery)\n"
			.       "\t\t{\n"
			.       "\t\t\t\$finalqueries[] = \$singlequery;\n"
			.       "\t\t}\n"
			.       "\t\t\$finalqueries[] = \$query;\n"
		);

		$bestand->addContent("\n"
			.       "\t\t// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql\n"
			.       "\t\tif(!\$foreigncall)\n"
			.       "\t\t{\n"
			.       "\t\t\t\$queriesgelukt = true;\n"
			.       "\t\t\t\$output = array();\n"
			.       "\t\t\tforeach(\$finalqueries as \$singlequery)\n"
			.       "\t\t\t{\n"
			.       "\t\t\t\t\$queriesgelukt &= \$singlequery;\n"
			.       "\t\t\t\tif(\$queriesgelukt == false) {\n"
			.       "\t\t\t\t\t\$output[] = \$singlequery;\n"
			.       "\t\t\t\t\tbreak;\n"
			.       "\t\t\t\t}\n"
			.       "\t\t\t}\n"
			.       "\t\t\tif(\$queriesgelukt)\n"
			.       "\t\t\t{\n"
			.       "\t\t\t\t\$WSW4DB->q('COMMIT');\n"
			.       "\t\t\t}\n"
			.       "\t\t\telse\n"
			.       "\t\t\t{\n"
			.       "\t\t\t\t\$WSW4DB->q('ROLLBACK');\n"
			.       "\t\t\t\treturn \$output;\n"
			.       "\t\t\t}\n"
			.       "\t\t}\n"
		);

		if(!$cls->IDsource)
		{
			$bestand->addContent("\n"
					.	"\t\t\$this->inDB = False;\n"
					.	"\n"
					.	"\t\t// unset IDs\n"
					);
			foreach($cls->IDs as $id)
			{
				$bestand->addContent("\t\t\$this->" . $id->name . " = NULL;\n");
			}
		}

		$bestand->addContent("\n"
				  . "\t\tif(\$foreigncall)\n"
				  . "\t\t\treturn(\$finalqueries);\n"
			  	  . "\t\telse\n"
			  	  . "\t\t\treturn NULL;\n");
		$bestand->addContent("\t}\n");
	}

	// UitDB haalt een LAZY attribuut op uit de DB.
	private function codeUitDB(Bestand $bestand, $cls)
	{
		$classname = $cls->real->name;

		$bestand->addContent("\t\tglobal \$WSW4DB;\n\n");

		$ts = array();
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			$ts[$var->annotation['LAZY']][] = $var->name;
		}

		if(count($ts) > 1 || $cls->IDsource)
		{
			ksort($ts);
			$bestand->addContent("\t\tswitch(\$veld) {\n"
					);
			foreach($ts as $l => $vs)
			{
				if(count($vs) == 0)
					continue;

				foreach($vs as $v) {
					$bestand->addContent("\t\tcase '$v':\n");
				}
				if($l)	// == LAZY, dus uitDB mag
					$bestand->addContent("\t\t\tbreak;\n");
				else
					$bestand->addContent("\t\t\tthrow new BadMethodCallException("
								.	"\"veld '\$veld' is niet LAZY\");\n");
			}
			$bestand->addContent("\t\tdefault:\n"
					.	( $cls->IDsource
					?	"\t\t\treturn parent::uitDB("
						.	"\$veld, \$lang);\n"
					:	"\t\t\tthrow new BadMethodCallException("
						.	"\"onbekend veld '\$veld'\");\n")
					.	"\t\t}\n"
					.	"\n");
		}

		$bestand->addContent("\t\tif(isset(\$lang))\n"
				.	"\t\t\t\$veld .= '_' . strtoupper(\$lang);\n"
				.	"\n"
				.	"\t\treturn \$WSW4DB->q('VALUE SELECT `%l`'\n"
				.	"\t\t                 .' FROM `$classname`'\n"
				);
		$bestand->addContent($cls->genSQLWhere(2));
		$bestand->addContent("\t\t                 , \$veld\n");
		$bestand->addContent($cls->genSQLWhereArgs(2));
		$bestand->addContent("\t\t                 );\n"
				.	"\t}\n"
				);
	}

	// sla wijziging op in de database
	private function codeNaarDB(Bestand $bestand, $cls)
	{
		// TODO IDs weigeren te updaten

		$classname = $cls->real->name;

		$bestand->addContent("\t\tglobal \$WSW4DB;\n"
				.	"\n");

		$ts = array();
		foreach($cls->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if($var->isForeignObject()) continue;
			if($var->type == 'object') continue;
			$type = $var->getLibDBType();	// wordt gebruikt bij count($ts)==1
			$ts[$type][] = $var->name;
		}

		if(count($ts) > 1 || $cls->IDsource)
		{
			ksort($ts);
			$bestand->addContent("\t\tswitch(\$veld) {\n");
			foreach($ts as $t => $vs)
			{
				foreach($vs as $v)
				{
					$bestand->addContent("\t\tcase '$v':\n");
				}
				$bestand->addContent("\t\t\t\$type = '%$t';\n"
						.	"\t\t\tbreak;\n");
			}
			$bestand->addContent("\t\tdefault:\n"
					.	( $cls->IDsource
					?	"\t\t\treturn parent::naarDB("
						.	"\$veld, \$waarde, \$lang);\n"
					:	"\t\t\tthrow new BadMethodCallException("
						.	"\"onbekend veld '\$veld'\");\n")
					.	"\t\t}\n"
					.	"\n");
		}

		$where = $cls->genSQLWhere(2);
		$val = $cls->genSQLWhereArgs(2);

		$bestand->addContent("\t\tif(isset(\$lang))\n"
				.	"\t\t\t\$veld .= '_' . strtoupper(\$lang);\n"
				.	"\n"
				.	"\t\t\$WSW4DB->q('UPDATE `$classname`'\n"
				.	"\t\t          .\" SET `%l` = "
							. (count($ts) > 1 ? '$type' : "%$type") . "\"\n"
				.	(!$cls->IDsource
					? "\t\t          .   ', `gewijzigdWanneer` = %s'\n"
					 ."\t\t          .   ', `gewijzigdWie` = %i'\n"
					: "")
				.	$where
				.	"\t\t          , \$veld\n"
				.	"\t\t          , \$waarde\n"
				.	(!$cls->IDsource
					? "\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
					 ."\t\t          , \$this->gewijzigdWie\n"
					: "")
				.	$val
				.	"\t\t          );\n"
				.	"\t}\n"
				);

	}
	private function codeGewijzigd(Bestand $bestand, $cls)
	{
		$this->codeMethodHeader($bestand);

		if($cls->getPackage()->name != 'Basis'
		&& $cls->inherits->getPackage()->name == 'Basis')
		{
			$bestand->addContent("\t\tparent::gewijzigd(False);\n"
					.	"\n"
					.	"\t\tif(\$updatedb)\n"
					.	"\t\t{\n"
					.	"\t\t\tglobal \$WSW4DB;\n"
					);
			$where = $cls->genSQLWhere(3);
			$val = $cls->genSQLWhereArgs(3);
			$bestand->addContent("\n"
					.	"\t\t\t\$WSW4DB->q('UPDATE LOW_PRIORITY "
						.				"`" . $cls->real->name . "`'\n"
					.	"\t\t\t          .' SET `gewijzigdWanneer` = %s'\n"
					.	"\t\t\t          .   ', `gewijzigdWie` = %i'\n"
					.	$where
					.	"\t\t\t          , \$this->gewijzigdWanneer->strftime('%F %T')\n"
					.	"\t\t\t          , \$this->gewijzigdWie\n"
					.	$val
					.	"\t\t\t          );\n"
					.	"\t\t}\n"
					.	"\t}\n"
					);
			return;
		}
		$bestand->addContent("\t\tparent::gewijzigd(\$updatedb);\n"
				.	"\t}\n"
				);
	}
	private function codeVelden(Bestand $bestand, $cls)
	{
		$localvelden = $this->name.$cls->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Aanvulling op parent::velden(welke).")
			->addSee($localvelden);

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function {$this->name}(\$welke = NULL)\n"
				.	"\t{\n"
				.	"\t\tstatic \$veldenCache = array();\n"
				.	"\n"
				.	"\t\tif(!isset(\$veldenCache[\$welke])) {\n"
				.	"\t\t\t\$veldenCache[\$welke] =\n"
				.	"\t\t\t\t\tarray_merge( parent::velden(\$welke)\n"
				.	"\t\t\t\t\t           , static::$localvelden(\$welke)\n"
				.	"\t\t\t\t\t           );\n"
				.	"\t\t}\n"
				.	"\n"
				.	"\t\treturn \$veldenCache[\$welke];\n"
				.	"\t}\n");

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geef velden die bij deze klasse horen.")
			->addParam("welke", 'string|null', "String die een subset van velden beschrijft, of NULL voor alle velden.")
			->addSee("velden")
			->setReturn('array', "Een array met velden.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tprotected static function $localvelden(\$welke = NULL)\n"
				.	"\t{\n"
				.	"\t\t\$velden = array();\n"
				.	"\t\tswitch(\$welke) {\n"
				.	"\t\tcase 'set':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->static
			|| $var->visibility == 'implementation'
			|| $var->visibility == 'private'
			|| $var->visibility == 'protected'
			|| $var->isForeignKey()
			|| $var->annotation['PRIMARY']
			|| $var->annotation['PSEUDO'])
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tcase 'lang':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->static
			|| $var->visibility == 'implementation'
			|| $var->visibility == 'private'
			|| $var->visibility == 'protected'
			|| $var->isForeignKey()
			||!$var->annotation['LANG'])
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tcase 'lazy':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->static
			||!$var->annotation['LAZY']
			|| $this->visibility == 'implementation')
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tcase 'verplicht':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->static
			|| $var->visibility == 'implementation'
			|| $var->visibility == 'private'
			|| $var->visibility == 'protected'
			|| $var->type == 'bool'
			|| $var->type == 'enum'
			|| $var->type == 'int'
			|| $var->type == 'bigint'
			|| $var->annotation['NULL']
			|| $var->annotation['PSEUDO'])
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tcase 'viewInfo':\n"
				.	"\t\tcase 'viewWijzig':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->static
			|| $this->visibility == 'implementation'
			|| $var->isForeignKey()
			|| $var->annotation['PRIMARY'])
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tcase 'get':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->isForeign() && !$var->isForeignObject()) {
				continue;
			}

			if($var->static
			|| $this->visibility == 'implementation'
			|| $var->isForeignKey()
			|| $var->annotation['PRIMARY'])
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\tcase 'primary':\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->isForeign() && !$var->isForeignObject()) {
				$bestand->addContent("\t\t\t\$velden[] = '".$var->name."';\n");
				continue;
			}
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tcase 'verzamelingen':\n"
				);

		foreach($cls->foreignrelations as $c => $isnull)
			$bestand->addContent("\t\t\t\$velden[] = '".ucfirst($c)."Verzameling';\n");

		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\tdefault:\n"
				);
		foreach($cls->vars as $var)
		{
			if($var->static
			|| $this->visibility == 'implementation'
			|| $var->isForeignKey()
			|| $var->annotation['PRIMARY'])
				continue;

			$bestand->addContent("\t\t\t\$velden[] = '".$var->upper()."';\n");
		}
		$bestand->addContent("\t\t\tbreak;\n"
				.	"\t\t}\n"
				.	"\t\treturn \$velden;\n"
				.	"\t}\n"
				);
	}
	private function codeGetVeldType(Bestand $bestand, $cls)
	{
		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft van een veld van {$cls->real->name} terug wat het type is.")
			->addParam("field", 'string', "Het veld waarvan het type teruggegeven moet worden.")
			->setReturn('string|null', "Het type als string.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tstatic public function getVeldType(\$field)\n"
				.	"\t{\n"
				.	"\t\tswitch(strtolower(\$field))\n"
				.	"\t\t{\n");

		foreach($cls->vars as $var)
		{
			$bestand->addContent("\t\tcase '".strtolower($var->name)."':\n"
					.	"\t\t\treturn '".$var->type."';\n");
		}

		$bestand->addContent("\t\tdefault:\n"
				.	"\t\t\treturn NULL;\n"
				.	"\t\t}\n"
				.	"\t}\n");
	}
	function codeGetBekijkbareVars(Bestand $bestand, $cls)
	{
		$bestand->addContent("\tpublic function getBekijkbareVars(\$vars = NULL)\n"
				.	"\t{\n"
				.	"\t\tif(!\$this->magBekijken())\n"
				.	"\t\t\treturn array();\n"
				.	"\n"
				.	"\t\tif(is_null(\$vars) || !is_array(\$vars))\n"
				.	"\t\t\t\$vars = array();\n"
				.	"\n"
				.	"\t\tif(!isset(\$vars['vars']))\n"
				.	"\t\t\t\$vars['vars'] = array();\n"
				.	"\t\t\$vars['vars'] += static::velden('get');\n"
				.	"\t\tif(!isset(\$vars['verzamelingen']))\n"
				.	"\t\t\t\$vars['verzamelingen'] = array();\n"
				.	"\t\t\$vars['verzamelingen'] += static::velden('verzamelingen');\n"
				.	"\n"
				.	"\t\treturn parent::getBekijkbareVars(\$vars);\n"
				.	"\t}\n");
	}
	function codeGetWijzigbareVars(Bestand $bestand, $cls)
	{
		$bestand->addContent("\tpublic function getWijzigbareVars(\$vars = NULL)\n"
				.	"\t{\n"
				.	"\t\tif(!\$this->magWijzigen())\n"
				.	"\t\t\treturn array();\n"
				.	"\n"
				.	"\t\tif(is_null(\$vars) || !is_array(\$vars))\n"
				.	"\t\t\t\$vars = array();\n"
				.	"\n"
				.	"\t\t\$vars += static::velden('set');\n"
				.	"\t\treturn parent::getWijzigbareVars(\$vars);\n"
				.	"\t}\n");
	}

	/**
	 * @brief Genereer code om een lijst van dependencies te maken.
	 *
	 * @param fp De filepointer waar de dependencies op moeten komen.
	 * @param cls De klasse waarop deze methode moet komen.
	 * @return void
	 */
	private function codeGetDependencies(Bestand $bestand, $cls)
	{
		global $objects;

		// Poep eerst de methodeheader uit...
		$this->codeMethodHeader($bestand);

		// ... en dan de body:
		// We gaan ervan uit dat het eind van de recursie
		// (i.e. een lege lijst) geïmplementeerd is in DBObject.
		$bestand->addContent(
				"\t\t// Maak een array klassenaam => dependencyverzameling.\n"
			.	"\t\t\$dependencies = parent::getDependencies();\n");

		// Zoek alle dependencies uit de dia op,
		// en stop die in een mooie array.
		// TODO: dit lijkt best wel op codeVerwijderen,
		// kan die niet op basis van deze methode in de niet-generated code?
		foreach ($cls->foreignrelations as $depNaam => $magNull)
		{
			// De (gegenereerde editie van de) klasse die op ons dependt.
			$dependencyGenerated = $objects['class'][$depNaam."_Generated"];
			foreach($dependencyGenerated->vars as $foreignKey)
			{
				// We willen wel dat de foreign key naar deze klasse precies wijst,
				// niet naar een parent / childklasse.
				if ($foreignKey->isForeignObject() && $foreignKey->foreign->name == $cls->real->name)
				{
					// De naam van de foreign key.
					$foreignKeyNaam = ucfirst($foreignKey->name);
					// De naam van de verzameling gerelateerde objecten.
					// (Dit willen we uniek hebben, dus vandaar klasse en foreign key).
					$verzamelingNaam = '$' . $depNaam . "From" . $foreignKeyNaam . "Verz";

					$bestand->addContent("\n"
						.	"\t\t// Verzamel alle " . $depNaam . "-objecten die op dit object dependen,\n"
						.	"\t\t// en stop die in onze returnwaarde.\n"
						.	"\t\t" . $verzamelingNaam." = " . $depNaam . "Verzameling::from" . $foreignKeyNaam . "(\$this);\n"
						.	"\t\t\$dependencies['" . $depNaam . "'] = " . $verzamelingNaam . ";\n"
					);
				}
			}
		}

		// Array gevuld dus returnen die hap!
		$bestand->addContent("\n"
			.	"\t\treturn \$dependencies;\n"
		);
		$bestand->addContent("\t}\n");
	}
}
