<?php

namespace Generator\WSW4objects;

/**
 * Class WSW4relation
 * @package Generator\WSW4objects
 */
final class WSW4relation
{
	// LET OP: dit is dus niet iets als een Deelnemer of een CommissieLid
	// maar wordt gebruikt om dingen als inheritance pijltjes van de .dia
	// te modeleren.

	/**
	 * @var string
	 */
	private $name;
	/**
	 * @var WSW4class
	 */
	private $parent;
	/**
	 * @var WSW4class
	 */
	private $child;

	/**
	 * WSW4relation constructor.
	 * @param string $name
	 * @param WSW4class $parent
	 * @param WSW4class $child
	 */
	public function __construct(string $name, WSW4class $parent, WSW4class $child)
	{
		$this->name = $name;
		$this->parent = $parent;
		$this->child = $child;
	}

	/**
	 * @return string
	 */
	public function name(): string
	{
		return $this->name;
	}

	/**
	 * @return WSW4class
	 */
	public function parent(): WSW4class
	{
		return $this->parent;
	}

	/**
	 * @return WSW4class
	 */
	public function child(): WSW4class
	{
		return $this->child;
	}

	/**
	 * @param array $objects
	 */
	public function check(array $objects)
	{
		if (!isset($this->parent)) {
			error($this, "parent not set");
		}
		if (!isset($objects['class'][$this->parent->name])) {
			error($this, "parent not a valid class object");
		}
		if (!isset($this->child)) {
			error($this, "child not set");
		}
		if (!isset($objects['class'][$this->child->name])) {
			error($this, "child not a valid class object");
		}
	}
}
