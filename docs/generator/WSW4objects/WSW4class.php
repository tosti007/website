<?php

namespace Generator\WSW4objects;

use DocCommentTemplate;
use Generator\Bestand;

class WSW4class
{
	public $id;
	public $name;
	public $packageId;
	public $comment;
	public $abstract	= false;
	public $generated	= false;
	public $view        = false;
	public $verzameling = false;
	public $api         = false;
	public $query		= false;
	public $real;
	public $core;

	public $annotation	= array();

	public $inherits;
	public $implements	= array();
	/** @var WSW4var[] */
	public $vars		= array();
	/**
	 * @var WSW4method[]
	 */
	public $methods		= array();

	public $foreignrelations = array();

	/** @var WSW4var[] */
	public $IDs			= array();
	public $IDsource	= False;
	public $IDchildren	= array();

	function __construct($id)
	{
		$this->id = $id;
		$this->packageId = '_ROOT';

		$this->annotation['AUTH_GET'] = 'god';
		$this->annotation['AUTH_SET'] = 'god';
	}
	public function setName($name)
	{
		if(empty($name)) return;
		$this->name = $name;
	}
	public function setPackageId($packageId)
	{
		if(empty($packageId)) return;
		$this->packageId = $packageId;
	}
	public function setComment($comment)
	{
		$this->comment = $comment;

		// Annotations in comments parsen. Enkel de laatste regel van de
		// comments wordt bekeken en alleen als deze begint met een
		// apenstaartje
		$comments_arr = explode("\n", $comment);
		$lastline = array_pop($comments_arr);
		if (substr($lastline, 0, 1) === "@")
		{
			// Parse annotations!
			$lastline = substr($lastline, 1);
			$keywords = explode(',',$lastline);
			foreach($keywords as $keyword)
			{
				$keyword = trim($keyword);
				$key = explode(':', $keyword);
				switch(strtoupper($key[0]))
				{
					case 'AUTH_GET':
						if (!isset($key[1]) || count($key) > 2)
							error($this, 'De annotatie AUTH_GET neemt precies 1 parameter [level]');
						$this->annotation['AUTH_GET'] = $key[1];
						break;
					case 'AUTH_SET':
						if (!isset($key[1]) || count($key) > 2)
							error($this, 'De annotatie AUTH_SET neemt precies 1 parameter [level]');
						$this->annotation['AUTH_SET'] = $key[1];
						break;

					default:
						error($this, 'Onbekende annotation: ' . $key[0]);
						break;
				}
			}
		} // else: laatste regel van commentaar start niet met '@' => geen annotations
	}
	public function setAbstract($abstract)
	{
		$this->abstract = (bool)$abstract;
	}
	public function addVar($var)
	{
		$this->vars[] = $var;
		$var->class = $this;
	}
	public function addMethod($method)
	{
		$this->methods[] = $method;
	}
	public function getPackage()
	{
		global $objects;
		return $objects['id'][$this->packageId];
	}

	public function getBasis()
	{
		if($this->getPackage()->name == 'Basis')
			return $this->name;

		return $this->inherits->getBasis();
	}

	/**
	 * @brief Geef een string met een WHERE-clausule genereert.
	 *
	 * De resulterende query selecteert op basis van de primary key van de tabel.
	 *
	 * @see genSQLWhereArgs
	 *
	 * @param indentLevel Het aantal tabs om mee te indenteren.
	 *
	 * @return Een string met een WHERE-clausule om in een query te stoppen.
	 */
	public function genSQLWhere($indentLevel)
	{
		$result = "";
		$indent = str_repeat("\t", $indentLevel);

		$first = true;
		foreach ($this->IDs as $id)
		{
			if ($id->type == 'foreign')
			{
				continue;
			}
			$result .= $indent . "          ." . ($first ? "' WHERE" : "  ' AND")
				. " `" . $id->nameForDB . "` = %".$id->getLibDBType()."'\n";
			$first = false;
		}
		return $result;
	}

	/**
	 * @brief Geef een string met code voor argumenten voor de WHERE-clausule.
	 *
	 * @see genSQLWhere
	 *
	 * @param indentLevel Het aantal tabs om mee te indenteren.
	 *
	 * @return Een string met code voor functieargumenten voor `dbpdo::q`.
	 */
	public function genSQLWhereArgs($indentLevel)
	{
		$result = "";
		$indent = str_repeat("\t", $indentLevel);

		foreach ($this->IDs as $id)
		{
			if ($id->isForeignObject())
			{
				continue;
			}
			$result .= $indent . "          , \$this->" . $id->name . "\n";
		}

		return $result;
	}

	public function check ($objects)
	{
		if(isset($this->packageId))
		{
			if(!isset($objects['id'][$this->packageId]))
				error($this, "package not a valid id");
		}
		foreach($this->vars as $v) {
			$v->check($objects);
		}
		if(!$this->abstract) {
			foreach($this->methods as $m) {
				if($m->abstract)
					error($this, 'should not have abstract method(s)');
			}
		}
	}

	/**
	 * Deze methode zegt met welk php bestand deze klasse verbonden is
	 */
	public function path()
	{
		// Krijgt in welke map alles zich bevindt
		$path = $this->getPackage()->path();

		// Maak onderscheid tussen verschillende aspecten
		if ($this->getPackage()->name != 'Basis')
			if ($this->view)
				$path .= 'View/';
			elseif ($this->verzameling)
				$path .= 'Verzameling/';
			elseif ($this->api)
				$path .= 'Api/';
			elseif ($this->query)
				$path .= 'Query/';
			else
				$path .= 'DBObject/';

		// Gaat dit om een gegenereerde?
		if($this->generated)
			$path .= 'Generated/'.$this->real->name.'.gen.php';
		else
			if ($this->view)
				$path .= $this->real->name.'.view.php';
			elseif ($this->api)
				$path .= $this->name.'.api.php';
			elseif ($this->query)
				$path .= $this->real->name.'.query.php';
			elseif ($this->abstract)
				$path .= $this->name.'.abs.php';
			else
				$path .= $this->name.'.cls.php';

		// Returneer het pas
		return $path;
	}

	public function generateDBCreate(Bestand $bestand)
	{
		$IDnames = array();

		$bestand->addContent("CREATE TABLE `".$this->name."`\n");

		$f = True;
		foreach($this->IDs as $v)
		{
			if($v->isForeignObject()) continue;

			// TODO type, ISNULL?, autoincrement, ...
			$bestand->addContent(($f?"(":",")." `".$v->name . "` "
					. $v->getSQLType() . " NOT NULL"
					.	(count($this->IDs) == 1 && !$this->IDsource && $v->type == 'int'
						? " AUTO_INCREMENT" : "")
					. "\n"
					);
			$IDnames[] = $v->name;
			$f = False;
		}
		if($this->IDchildren)
		{
			// We moeten de overerving aanmaken via NULL,
			// want we checken dat de naam niet gereserveerd is in de WSW4Var-constructor.
			// TODO SOEPMES: doe dit bij het parsen van de dia...
			$oo = new WSW4Var(NULL);
			$oo->name = 'overerving';
			$oo->nameForDB = 'overerving';
			$oo->setType('enum');
			$oo->default = "'$this->name'";
			$oo->annotation['ENUM'] = array();
			$oo->annotation['ENUM'][] = $this->name;
			foreach($this->IDchildren as $c)
			{
				$oo->annotation['ENUM'][] = $c->name;
			}
			$oo->generateDBCreate($bestand);
		}

		foreach($this->inherits->vars as $v)
		{
			if(isset($this->IDs[$v->name]))
				continue;
			if($v->type == 'object' || $v->annotation['PSEUDO'])
				continue;

			$v->generateDBCreate($bestand);
		}

		$IDstr = '`'.implode('`, `', $IDnames).'`';
		if(!$this->IDsource)
		{
			// TODO overnemen uit Entiteit... ?
			$bestand->addContent(", `gewijzigdWanneer` datetime DEFAULT NULL\n"
					.	", `gewijzigdWie` int(11) DEFAULT NULL\n"
					);
		}
		$bestand->addContent(", PRIMARY KEY ($IDstr)\n");

		if($this->IDsource) {
			$bestand->addContent(", CONSTRAINT `".$this->name."_fk`\n"
					.	"    FOREIGN KEY ($IDstr)\n"
					.	"    REFERENCES `".$this->IDsource->name."` ($IDstr)\n"
					);
		}
		foreach($this->inherits->vars as $v)
		{
			if(!$v->isForeignObject() || $v->annotation['PSEUDO'])
				continue;

			$bestand->addContent(", CONSTRAINT `".$this->name."_".$v->upper()."`\n"
					.	"    FOREIGN KEY ");
			//If our foreign keys have the same substr we can't let them have the same name
			$foreignforeign = array();
			$first = true;
			foreach($v->foreign->IDs as $n => $id)
			{
				if($id->isForeignObject()) continue;

				$string = ($first ? "(" : "`, ")."`".$v->name."_";
				if($id->isForeign()) {
					$substr = substr($id->name, strrpos($id->name, "_") + 1);
					if(@$foreignforeign[$substr]){ //We have already used this substr
						$string .= $substr . $foreignforeign[$substr];
						$foreignforeign[$substr] = $foreignforeign[$substr] + 1;
					} else {
						$string .= $substr;
						$foreignforeign[$substr] = 2;
					}
				} else {
					$string .= $id->name;
				}

				//Write our constructed string to file.
				$bestand->addContent($string );
				$first = false;
			}
			$bestand->addContent("`)\n"
					.	"    REFERENCES `".$v->foreign->name."` "
					);
			$first = true;
			foreach($v->foreign->IDs as $n => $id)
			{
				if($id->isForeignObject()) continue;
				$bestand->addContent(($first ? "(" : ", ")."`".$n."`");
				$first = false;
			}
			$bestand->addContent(")\n");
		}
		$bestand->addContent(") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n");
	}

	/**
	 * Wordt voor ieder object ongeacht wat het is aangeroepen (dwz, ook views,
	 * verzamelingen, etc), afhankelijk of het generated is wordt er een nieuw
	 * bestand geforceerd geschreven of skippen we als er al een bestand
	 * bestaat
	 */
	public function generateCode ()
	{
		// Open het bestand om te gaan schrijven
		$bestand = new Bestand($this->path());

		// Als we geen geldige pointer terug krijgen betekend dat we niet
		// geforceerd hebben om een nieuw bestand te maken (dus niet
		// generated) en er al een bestand bestaat, dus skip dit dan
		if ($bestand->fileExists() && !$this->generated) return;

		// We maken een php-bestand
		$bestand->addContent("<?\n");

		// Is er in de dia commentaar meegegeven? Zo ja dan zetten we dat ook
		// in de code. Vervang de @ door 'AT' omdat @ in comment een speciaal
		// doxygencommando wordt
		if (!empty($this->comment))
		{
			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief(str_replace("@", "'AT'", str_replace("\n", "\n * ", $this->comment)));
			$bestand->addContent($docCommentTemplate->generate());
		}

		// De klas definitie, zeg maar de bovenste regel
		if ($this->abstract)
			$bestand->addContent("abstract ");
		$bestand->addContent("class ".$this->name."\n");
		if ($this->inherits)
			$bestand->addContent("\textends ".$this->inherits->name."\n");
		foreach ($this->implements as $i)
			$bestand->addContent("\timplements ".$i->name."\n");
		$bestand->addContent("{\n");

		// Als we een view gaan maken, voer dan de view specifieke templates
		if ($this->view)
			$this->generateCodeForView($bestand);
		// Als we een api gaan maken, voer dan de view specifieke templates
		elseif ($this->api)
			$this->generateCodeForApi($bestand);
		elseif ($this->query)
			$this->generateCodeForQuery($bestand);
		// Geen view of api? Voer dan de standaard code uit!
		else
			$this->generateCodeForDefault($bestand);

		// Sluit het bestand
		$bestand->addContent("}\n");
		$bestand->write($this->generated);
	}

	/**
	 * Deze methode schrijft naar $fp specifieke code voor view-objecten
	 * @param Bestand $bestand
	 */
	public function generateCodeForView (Bestand $bestand)
	{
		// Alleen als we echt een view zijn
		if (!$this->view)
			return;

		// De defaultWaarde-functie voor het object
		$this->generateCodeDefaultWaarde($bestand);

		// Per variabele willen we deze vier methoden
		foreach ($this->vars as $var)
		{
			$var->generateCodeToonLabel($bestand);
			$var->generateCodeToonWaarde($bestand);
			$var->generateCodeToonForm($bestand);
			$var->generateCodeToonOpmerking($bestand);
		}
	}

	/**
	 * Deze methode maakt de functie defaultWaarde$obj.
	 * @param Bestand $bestand
	 */
	public function generateCodeDefaultWaarde(Bestand $bestand)
	{
		$name = $this->name;
		if($this->real->real)
			$className = $this->real->real->name;
		else if($this->real)
			$className = $this->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft de defaultWaarde van het object terug. "
				.	"Er wordt aangeraden deze functie te overschrijven in {$className}View.")
			->addParam('obj', $className, "Het {$className}-object waarvan de waarde kregen moet worden.")
			->setReturn('string', "Een Html-veilige string die de waarde van het object representeert.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.	"\tpublic static function defaultWaarde".$className
				.	"($className \$obj)\n"
				.	"\t{\n"
				.	"\t\treturn self::defaultWaarde(\$obj);\n"
				.	"\t}\n\n");
	}

	public function generateCodeForQuery(Bestand $bestand)
	{
		if(!$this->query)
			return;

		if($this->generated)
		{
			$this->generateCodeConstructor($bestand);
			$this->generateCodeTable($bestand);
			$this->generateCodeVerzamel($bestand);
			$this->generateCodeGeef($bestand);
			$this->generateCodeCheckField($bestand);
			$this->generateCodeAutomaticJoin($bestand);
			$this->generateCodeGetEquivalentTables($bestand);
			$this->generateCodeGetJoinKeys($bestand);
			$this->generateCodeGetPrimaries($bestand);
			$this->generateCodeCheckProp($bestand);
		}
	}

	public function generateCodeTable(Bestand $bestand)
	{
		$className = $this->real->name;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Maakt een {$this->real->name} wat meteen gebruikt "
				.   "kan worden om methoden op toe te passen. We maken hier een nieuw "
				.   "{$this->real->name}-object aan om er zeker van te zijn dat we alle "
				.   "eigenschappen daarvan ook meenemen.")
			->setReturn($this->real->name, "Het {$this->real->name}-object waarvan meteen de db-table "
				.   "van het {$this->real->real->name}-object als table geset is.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function table()\n"
				.   "\t{\n"
				.   "\t\t\$query = new $className();\n"
				.   "\n");

		$dbTableName = $this->real->real->name;

		$bestand->addContent("\t\t\$query->tables('$dbTableName');\n"
				.   "\n"
				.   "\t\treturn \$query;\n"
				.   "\t}\n\n");
	}

	public function generateCodeVerzamel(Bestand $bestand)
	{
		$cls = $this->real->real;

		$ids = array();
		foreach($cls->IDs as $id)
		{
			if($id->isForeignObject()) continue;
			$ids[$id->name] = $id;
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Maakt een {$this->real->real->name}Verzameling aan van de objecten "
				.   "die geselecteerd worden door de {$this->real->name} uit te voeren.")
			->addParam('object', 'string', "De naam van het object om te verzamelen.")
			->addParam('distinct', 'bool', "Een bool of alle elementen distinct moeten zijn.")
			->setReturn($this->real->real->name . 'Verzameling', "Een {$this->real->real->name}Verzameling verkregen door de query");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tpublic function verzamel(\$object = '" . $this->real->real->name . "'"
				.   ", \$distinct = false)\n"
				.   "\t{\n"
				.   "\t\treturn parent::verzamel(\$object, \$distinct);\n"
				.   "\t}\n\n");
	}

	public function generateCodeGeef(Bestand $bestand)
	{
		$cls = $this->real->real;

		$ids = array();
		foreach($cls->IDs as $id)
		{
			if($id->isForeignObject()) continue;
			$ids[$id->name] = $id;
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft een {$this->real->real->name} die "
				. "geslecteeerd wordt door de {$this->real->name} uit te voeren.")
			->addParam('object', 'string', "De naam van het object om te verzamelen.")
			->setReturn($this->real->real->name . 'Verzameling', "Een {$this->real->real->name}Verzameling verkregen door de query.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tpublic function geef(\$object = '" . $this->real->real->name . "')\n"
				.   "\t{\n"
				.   "\t\treturn parent::geef(\$object);\n"
				.   "\t}\n\n");
	}

	public function generateCodeCheckField(Bestand $bestand)
	{
		$cls = $this->real->real;

		$select_vars = array();

		if($cls->IDchildren)
			$select_vars[] = 'overerving';

		foreach($cls->IDs as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if($var->type == 'object') continue;
			if($var->isForeignObject()) continue;
			$select_vars[] = $var->nameForDB;
		}

		foreach($cls->inherits->vars as $key => $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if($var->type == 'object') continue;
			if($var->isForeignObject()) continue;
			if(in_array($var->name, $select_vars)) continue;
			if($var->annotation['LAZY']) continue;
			if($var->annotation['LANG']) {
				$select_vars[] = $var->nameForDB.'_NL';
				$select_vars[] = $var->nameForDB.'_EN';
				continue;
			}
			$select_vars[] = $var->nameForDB;
		}

		if(!$cls->inherits->IDsource)
		{
			$select_vars[] = 'gewijzigdWanneer';
			$select_vars[] = 'gewijzigdWie';
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Checkt of een kolom wel voorkomt in de db-table van {$this->real->real->name}.")
			->addParam('field', 'string', "De kolom om te checken.")
			->setReturn('bool', "Een bool of de kolom wel voorkomt.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function checkField(\$field)\n"
				.   "\t{\n"
				.   "\t\t\$varArray = array(\n\t\t\t'"
				.   implode("',\n\t\t\t'", $select_vars)
				.   "'\n\t\t);\n"
				.   "\n"
				.   "\t\tif(in_array(\$field, \$varArray))\n"
				.   "\t\t\treturn '" . $cls->name . "';\n"
				.   "\t\tif(\$ret = parent::checkField(\$field))\n"
				.   "\t\t\treturn \$ret;\n"
				.   "\t\treturn False;\n"
				.   "\t}\n\n");
	}

	public function generateCodeAutomaticJoin(Bestand $bestand)
	{
		$cls = $this->real->real;

		$joins = array();

		if($cls->inherits->IDsource)
		{
			$nxt = $cls->inherits->inherits->inherits;
			$joins[$nxt->real->name] = array();

			foreach($nxt->IDs as $key => $id)
			{
				$joins[$nxt->real->name][$nxt->real->name.".".$id->name]
					= $cls->name.".".$cls->IDs[$key]->name;
			}
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Haalt de sub-klasse op met de keys hoe te "
				. "joinen als {$cls->name} een extended klasse is.")
			->setReturn('array|false', "Een array met de tabel om automatisch te joinen.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function automaticJoin()\n"
				.   "\t{\n");

		if(sizeof($joins) != 0)
		{
			$keys = reset($joins);
			$bestand->addContent("\t\t\$autoJoins = array('".key($joins)."' => array(\n");

			$firstInner = true;
			foreach($keys as $key => $id)
			{
				if(!$firstInner)
					$bestand->addContent(",\n");

				$fistInner = false;
				$bestand->addContent("\t\t\t\t'".$key."' => '".$id."'");
			}

			$bestand->addContent("\n\t\t\t)\n"
					.   "\t\t);\n");
		}
		else
		{
			$bestand->addContent("\t\t\$autoJoins = False;\n");
		}

		$bestand->addContent("\t\treturn \$autoJoins;\n"
				.   "\t}\n\n");
	}

	public function generateCodeGetEquivalentTables(Bestand $bestand)
	{
		$cls = $this->real->real;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft een array terug met de klassen waarvan {$cls->name} een extensie is. "
				.   "Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden "
				.   "als het in meerder tabellen voor komt.")
			->setReturn('array', "Een array met tabelnamen.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function getEquivalentTables()\n"
				.   "\t{\n"
				.   "\t\treturn array_merge(parent::getEquivalentTables(), "
				.   "array('".$cls->name."'));\n"
				.   "\t}\n\n");
	}

	public function generateCodeGetJoinKeys(Bestand $bestand)
	{
		$cls = $this->real->real;

		$select_vars = array();
		for($nxt = $cls; $nxt; $nxt = $nxt->inherits)
		{
			if(!$nxt->generated)
				continue;

			$currForeignObject = null;
			$currForeignName = '';
			$counter = 0;

			foreach($nxt->vars as $var)
			{
				if($var->isForeignObject())
				{
					$counter = 0;
					$currForeignObject = $var->foreign;
					$currForeignName = $var->name;

					if(!isset($select_vars[$currForeignObject->name]))
						$select_vars[$currForeignObject->name] = array();

					$select_vars[$currForeignObject->name][$currForeignName] = array();
				}
				else if($var->isForeign())
				{
					$keys = array_keys($currForeignObject->IDs);
					do {
						$key = $keys[$counter++];
					} while($currForeignObject->IDs[$key]->isForeignObject());

					$select_vars[$currForeignObject->name][$currForeignName][$var->nameForDB] = $key;
				}
			}

			if(!$nxt->IDsource)
				break;
		}

		if($cls->inherits->IDsource)
		{
			$nxt = $cls->inherits->inherits->inherits;
			$select_vars[$nxt->real->name] = array();
			$select_vars[$nxt->real->name][$nxt->real->name] = array();

			foreach($nxt->IDs as $key => $id)
			{
				$select_vars[$nxt->real->name][$nxt->real->name][$nxt->real->name.".".$id->name]
					= $cls->name.".".$cls->IDs[$key]->name;
			}
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft de velden die nodig zijn om met een ander object te joinen.")
			->addParam('table', 'string', "De table-naam om op te joinen.")
			->setReturn('array|false', "False als er niet gejoined mag worden met de opgegeven table, "
				.   "anders wordt er een array teruggegeven met als key de tablenaam om op te "
				.   "joinen en als value een array met als key-value pairs de velden om mee te joinen.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function getJoinKeys(\$table)\n"
				.   "\t{\n");
		if(sizeof($select_vars) > 0)
		{
			$bestand->addContent("\t\t\$tableArray = array(\n");

			$first = true;
			foreach($select_vars as $key => $ids)
			{
				if(!$first)
					$bestand->addContent(",\n");
				$bestand->addContent("\t\t\t'$key' => array(\n");

				if(sizeof($ids) == 1)
				{
					$firstInner = true;
					foreach(reset($ids) as $idKey => $id)
					{
						if(!$firstInner)
							$bestand->addContent(",\n");

						$bestand->addContent("\t\t\t\t'$idKey' => '$id'");

						$firstInner = false;
					}
				}
				else
				{
					$firstInner = true;
					foreach($ids as $idKey => $id)
					{
						if(!$firstInner)
							$bestand->addContent(",\n");

						$bestand->addContent("\t\t\t\t'$idKey' => array(\n");

						$firstInner2 = true;
						foreach($id as $innerIdKey => $innerId)
						{
							if(!$firstInner2)
								$bestand->addContent(",\n");

							$bestand->addContent("\t\t\t\t\t'$innerIdKey' => '$innerId'");

							$firstInner2 = false;
						}

						$bestand->addContent("\n\t\t\t\t)");

						$firstInner = false;
					}
				}

				$bestand->addContent("\n\t\t\t)");
				$first = false;
			}

			$bestand->addContent("\n\t\t);\n"
					.   "\n"
					.   "\t\tif(array_key_exists(\$table, \$tableArray))\n"
					.   "\t\t\treturn \$tableArray[\$table];\n\n");
		}

		$bestand->addContent("\t\treturn False;\n"
				.   "\t}\n\n");
	}

	public function generateCodeGetPrimaries(Bestand $bestand)
	{
		$cls = $this->real->real;

		$ids = array();
		$names = array();
		foreach($cls->IDs as $id)
		{
			if($id->isForeignObject()) continue;
			$ids[$id->name] = $id;
			$names[] = $this->real->real->name . '.' . $id->nameForDB;
		}

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft alle primary keys van {$this->real->real->name}.")
			->setReturn('array', "Een array met alle primary keys.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function getPrimaries()\n"
				.   "\t{\n"
				.   "\t\treturn array(\n"
				.   "\t\t\t'" . implode("',\n\t\t\t'", $names) . "'\n"
				.   "\t\t);\n"
				.   "\t}\n\n");
	}

	public function generateCodeCheckProp(Bestand $bestand)
	{
		$cls = $this->real->real;

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Checkt of een bepaalde property in een whereProp gebruikt kan "
				.   "worden en geeft meteen de fields, types en values voor de where terug.")
			->addParam('prop', 'string', "De property om te checken.")
			->addParam('value', 'mixed', "De waarde waarmee de prop vergeleken moet worden.")
			->setReturn('array', "Een array met de fields, values en types in die volgorde.");

		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function checkProp(\$prop, \$value = NULL)\n"
				.   "\t{\n"
				.   "\t\tswitch(strtolower(\$prop))\n"
				.   "\t\t{\n");

		foreach($cls->inherits->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;
			if($var->type == 'object') continue;
			if($var->isForeign() && !$var->isForeignObject()) continue;
			if($var->annotation['LAZY']) continue;

			$can_null = $var->annotation['NULL'] || $var->annotation['LEGACY_NULL'];

			$name = $var->name;

			if($var->annotation['LANG']) {
				$bestand->addContent("\t\tcase '".strtolower($name)."_nl':\n"
						.   "\t\t\t\$type = 'string';\n"
						.   "\t\t\t\$field = '".$name."_NL';\n"
						.   "\t\t\tbreak;\n"
						.   "\t\tcase '".strtolower($name)."_en':\n"
						.   "\t\t\t\$type = 'string';\n"
						.   "\t\t\t\$field = '".$name."_EN';\n"
						.   "\t\t\tbreak;\n");
				continue;
			}

			$bestand->addContent("\t\tcase '".strtolower($name)."':\n");

			if($var->isForeign())
			{
				$bestand->addContent("\t\t\tif(is_array(\$value))\n"
						.   "\t\t\t{\n"
						.   "\t\t\t\tif(count(\$value) != count(array_filter(\$value, function(\$x) { return \$x instanceof ".$var->foreign->name."; })))\n"
						.   "\t\t\t\t\tuser_error('Alle waarden in de array moeten van type ".$var->foreign->name." zijn', E_USER_ERROR);\n"
						.   "\t\t\t}\n"
						.   "\t\t\telse if(!(\$value instanceof ".$var->foreign->name.")"
						.   " && !(\$value instanceof ".$var->foreign->name."Verzameling)"
						.   ($can_null ? " && !is_null(\$value)" : ''). ")\n"
						.   "\t\t\t\tuser_error('Value moet van type ".$var->foreign->name
						.   ($can_null ? " of NULL" : "") . " zijn', E_USER_ERROR);\n");
				$fieldsStr = '';
				$valuesStr = '';
				$typesStr = '';
				$first = true;
				foreach($cls->inherits->vars as $v)
				{
					if($v->isForeignObject() && $name == $v->name)
					{
						$firstInner = true;
						foreach($v->foreign->IDs as $id)
						{
							if($id->isForeignObject()) continue;

							if(!$firstInner)
								$valuesStr .= ",\n";

							$firstInner = false;
							$expl = explode('_', $id->name);

							if(sizeof($var->foreign->IDs) == 1)
							{
								$valuesStr .= "\$value->get";
							}
							else
							{
								$valuesStr .= "\t\t\t\t\$value->get";
							}

							foreach($expl as $e)
								$valuesStr .= ucfirst($e);

							$valuesStr .= '()';
						}
					}
					else if($v->isForeign() && !$v->isForeignObject()
						&& substr($v->name, 0, strlen($name)) == $name
						&& $v->name[strlen($name)] == '_')
					{
						if(!$first)
						{
							$fieldsStr .= ",\n";
							$typesStr .= ",\n";
						}

						$first = false;

						$type = $v->type;
						if($v->type == 'enum')
							$type = 'string';

						if(sizeof($var->foreign->IDs) == 1)
						{
							$fieldsStr .= "'".$v->nameForDB."'";
							$typesStr .= "'".$type."'";
						}
						else
						{
							$fieldsStr .= "\t\t\t\t'".$v->nameForDB."'";
							$typesStr .= "\t\t\t\t'".$type."'";
						}
					}
				}
				if(sizeof($var->foreign->IDs) == 1)
				{
					$bestand->addContent("\t\t\t\$field = $fieldsStr;\n"
							. "\t\t\tif(is_array(\$value) || \$value instanceof ".$var->foreign->name."Verzameling)\n"
							. "\t\t\t{\n"
							. "\t\t\t\t\$new_value = array();\n"
							. "\t\t\t\tforeach(\$value as \$v)\n"
							. "\t\t\t\t\t\$new_value[] = ".str_replace('$value', '$v', $valuesStr).";\n"
							. "\t\t\t\t\$value = \$new_value;\n"
							. "\t\t\t}\n"
					);
					if($can_null)
					{
						$bestand->addContent("\t\t\telse if(!is_null(\$value))\n"
								. "\t\t\t\t\$value = $valuesStr;\n");
					}
					else
					{
						$bestand->addContent("\t\t\telse \$value = $valuesStr;\n");
					}
					$bestand->addContent("\t\t\t\$type = $typesStr;\n");
				}
				else
				{
					$bestand->addContent("\t\t\t\$field = array(\n"
							.   $fieldsStr
							.   "\n\t\t\t);\n"
							.   "\t\t\tif(is_array(\$value) || \$value instanceof ".$var->foreign->name."Verzameling)\n"
							.   "\t\t\t{\n"
							.   "\t\t\t\tforeach(\$value as \$k => \$v)\n"
							.   "\t\t\t\t\t\$value[\$k] = array(\n"
							.   str_replace('$value', '$v', $valuesStr)
							.   "\n\t\t\t\t\t);\n"
							.   "\t\t\t\tif(\$value instanceof ".$var->foreign->name."Verzameling)\n"
							.   "\t\t\t\t\t\$value = \$value->getVerzameling();\n"
							.   "\t\t\t}\n");
					if($can_null)
					{
						$bestand->addContent("\t\t\telse if(!is_null(\$value))\n"
								. "\t\t\t\t\$value = array(\n"
								. $valuesStr
								. "\n\t\t\t\t);\n");
					}
					else
					{
						$bestand->addContent("\t\t\telse\n"
							.   "\t\t\t\t\$value = array(\n"
							.   $valuesStr
							.   "\n\t\t\t\t);\n");
					}
					$bestand->addContent("\t\t\t\$type = array(\n"
							.   $typesStr
							.   "\t\t\t\t);\n");
				}

				$bestand->addContent("\t\t\tbreak;\n");

				continue;
			}

			switch($var->type)
			{
			case 'bool':
			case 'int':
			case 'bigint':
				$bestand->addContent("\t\t\t\$type = 'int';\n"
					.   "\t\t\t\$field = '".$name."';\n");
				break;
			case 'money':
			case 'float':
				$bestand->addContent("\t\t\t\$type = 'float';\n"
					.   "\t\t\t\$field = '".$name."';\n");
				break;
			case 'string':
			case 'text':
			case 'html':
			case 'url':
				$bestand->addContent("\t\t\t\$type = 'string';\n"
					.   "\t\t\t\$field = '".$name."';\n");
				break;
			case 'date':
			case 'datetime':
				$bestand->addContent("\t\t\tif(is_array(\$value))\n"
					.   "\t\t\t{\n"
					.   "\t\t\t\tforeach(\$value as \$k => \$v)\n"
					.   "\t\t\t\t{\n"
					.   "\t\t\t\t\tif(!(\$value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck(\$value))\n"
					.   "\t\t\t\t\t\tuser_error(\$value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);\n"
					.   "\t\t\t\t}\n"
					.   "\t\t\t}\n"
					.   "\t\t\telse if(!(\$value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck(\$value)"
					.   ($can_null ? " && !is_null(\$value)": "") . ")\n"
					.   "\t\t\t\tuser_error(\$value . ' moet een DateTimeLocale zijn of een correcte sql-date functie"
					.   ($can_null ? " of NULL" : "") . "', E_USER_ERROR);\n"
					.   "\t\t\tif(\$value instanceof DateTimeLocale)\n"
					.   "\t\t\t{\n"
					.   "\t\t\t\t\$type = 'string';\n"
					.   "\t\t\t\t\$value = \$value->strftime('%F %T');\n"
					.   "\t\t\t}\n"
					.   "\t\t\telse if(is_array(\$value))\n"
					.   "\t\t\t{\n"
					.   "\t\t\t\t\$type = 'string';\n"
					.   "\t\t\t\tforeach(\$value as \$k => \$v)\n"
					.   "\t\t\t\t\t\$value[\$k] = \$v->strftime('%F %T');\n"
					.   "\t\t\t}\n"
					.   "\t\t\telse\n"
					.   "\t\t\t{\n"
					.   "\t\t\t\t\$type = 'function';\n"
					.   "\t\t\t\t\$value = QueryBuilder::sqlDateFunctionCheck(\$value);\n"
					.   "\t\t\t}\n"
					.   "\t\t\t\$field = '".$name."';\n");
				break;
			case 'enum':
				$bestand->addContent("\t\t\tif(is_array(\$value))\n"
					.   "\t\t\t{\n"
					.   "\t\t\t\tforeach(\$value as \$v)\n"
					.   "\t\t\t\t{\n"
					.   "\t\t\t\t\tif(!in_array(\$v, ".$cls->name."::enums".ucfirst($name)."()))\n"
					.   "\t\t\t\t\t\tuser_error(\$value . ' is niet een geldige waarde voor ".$name."', E_USER_ERROR);\n"
					.   "\t\t\t\t}\n"
					.   "\t\t\t}\n"
					.   "\t\t\telse if(!in_array(\$value, ".$cls->name
					.   "::enums".ucfirst($name)."())"
					.   ($can_null ? " && !is_null(\$value)": "") . ")\n"
					.   "\t\t\t\tuser_error(\$value . ' is niet een geldige waarde voor ".$name."', E_USER_ERROR);\n"
					.   "\t\t\t\$type = 'string';\n"
					.   "\t\t\t\$field = '".$name."';\n");
				break;
			}

			$bestand->addContent("\t\t\tbreak;\n");
		}

		$bestand->addContent("\t\tdefault:\n"
				.   "\t\t\treturn parent::checkProp(\$prop, \$value);\n"
				.   "\t\t\tbreak;\n"
				.   "\t\t}\n"
				.   "\n"
				.   "\t\tif(is_array(\$field)) {\n"
				.   "\t\t\tforeach(\$field as \$k => \$v) {\n"
				.   "\t\t\t\t\$field[\$k] = '{$cls->name}.'.\$v;\n"
				.   "\t\t\t}\n"
				.   "\t\t} else {\n"
				.   "\t\t\t\$field = '{$cls->name}.'.\$field;\n"
				.   "\t\t}\n"
				.   "\n"
				.   "\t\treturn array(\$field, \$value, \$type);\n"
				.   "\t}\n\n");

	}

	/**
	 * Deze methode schrijft naar $fp specifieke code voor api-objecten
	 * @param Bestand $bestand
	 */
	public function generateCodeForApi (Bestand $bestand)
	{
		// Alleen api-aspecten mogen deze code
		if (!$this->api)
			return;

		// Als dit het gegenereerde object is, maak dan de select methode etc
		if ($this->generated)
		{
			$this->generateCodeRestGET($bestand);
		}
	}

	/**
	 * Deze methode schrijft naar $fp specifieke code voor de get methode
	 * van de api-bestanden
	 * @param Bestand $bestand
	 */
	public function generateCodeRestGET (Bestand $bestand)
	{
		// $obj is de naam van het oorspronkelijke object, bijv Lid
		$obj = $this->core->name;
		// $objverz is het aspect verzameling, bijv LidVerzameling
		$objverz = $obj . 'Verzameling';

		// $auth zijn de regels met login gegevens
		$auth = '';

		// Controleer of er een level nodig is
		if (isset($this->annotation['AUTH_GET']))
			$auth = "\t\tRequest::getInstance()->requireAuth('"
				. $this->annotation['AUTH_GET']."');\n\n";

		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("Geeft een array van het object terug afhankelijk van de parameters. "
				. "Als er geen object wordt gevonden dan wordt er een 404 exception gegooid.");

		// Schrijf de methode
		$bestand->addContent($docCommentTemplate->generate(1)
				.   "\tstatic public function get (\$id, \$params)\n"
				.	"\t{\n"
				.	$auth
				.	"\t\t//Als er geen specifieke velden gevraagd zijn pak ze dan allemaal\n"
				.	"\t\t\$geldigeVelden = $obj::velden('get');\n"
				.	"\t\tif (count(\$params['velden']) == 0)\n"
				.	"\t\t\t\$params['velden'] = \$geldigeVelden;\n"
				.	"\n"
				.	"\t\t\$velden = array_intersect(\$params['velden'], \$geldigeVelden);\n"
				.	"\t\tif (count(\$velden) == 0)\n"
				.	"\t\t\tthrow new Exception('Geen geldige velden geselecteerd', 400);\n"
				.	"\n"
				.	"\t\tif (!is_null(\$id))\n"
				.	"\t\t{\n"
				.	"\t\t\tif(!\$object = $obj::geef(\$id))\n"
				.	"\t\t\t\tthrow new Exception('Object niet gevonden', 404);\n"
				.	"\t\t\t\$verzameling = new $objverz();\n"
				.	"\t\t\t\$verzameling->voegtoe(\$object);\n"
				.	"\t\t}\n"
				.	"\t\telse\n"
				.	"\t\t{\n"
				.	"\t\t\tforeach (\$params['constraints'] as \$constraint)\n"
				.	"\t\t\t\tif (!in_array(\$constraint->getVeld(), \$geldigeVelden))\n"
				.	"\t\t\t\t\tthrow new Exception('Ongeldige constraint gevonden', 400);\n"
				.	"\n"
				.	"\t\t\tif (\$params['order']['veld'] != null && !in_array(\$params['order']['veld'], \$geldigeVelden))\n"
				.	"\t\t\t\tthrow new Exception('Ongeldig veld om op te sorteren', 400);\n"
				.	"\n"
				.	"\t\t\t\$verzameling = $objverz::select(\$params['constraints'], \$params['order']['veld'], \$params['order']['hoe'], 20, \$params['offset']);\n"
				.	"\t\t}\n"
				.	"\n"
				.	"\t\t\$verzameling->filterBekijken();\n"
				.	"\t\tif (\$verzameling->aantal() == 0)\n"
				.	"\t\t\tthrow new Exception('Geen zichtbare objecten gevonden', 404);\n"
				.	"\n"
				.	"\t\t//Maak een 2D array die de verzameling representeerd\n"
				.	"\t\t\$return = array();\n"
				.	"\t\tforeach (\$verzameling as \$obj)\n"
				.	"\t\t{\n"
                .   "\t\t\t\$returnarray = array();\n"
                .   "\t\t\t\$returnarray[\"ID\"] = \$obj->geefID();\n"
				.	"\t\t\tforeach (\$velden as \$veld)\n"
                .	"\t\t\t\t\$returnarray[\$veld] = call_user_func(array(\$obj, 'get'.\$veld));\n"
                .   "\t\t\t\$return[] = \$returnarray;\n"
				.	"\t\t}\n"
				.	"\n"
				.	"\t\treturn \$return;\n"
				.	"\t}\n"
				.	"\n");
	}

	/**
	 * Deze methode schrijft naar $fp specifieke code voor standaard-objecten,
	 * dwz geen view- of api-objecten maar wel ook verzameling-objecten
	 * @param Bestand $bestand
	 */
	public function generateCodeForDefault (Bestand $bestand)
	{
		// Alleen als we echt geen view of api zijn
		if ($this->view || $this->api)
			return;

		// Eerst alle variabelen definieren in de header
		foreach ($this->vars as $var)
			$var->generateCode($bestand);

		// Alle foreign-relations ook in de header definieren
		if(!$this->verzameling && sizeof($this->foreignrelations) != 0)
		{
			$bestand->addContent("\t/** Verzamelingen **/\n");
			foreach($this->foreignrelations as $c => $isnull)
				$bestand->addContent("\tprotected \$".lcfirst($c)."Verzameling;\n");
		}

		// Vervolgens de constructor
		$this->generateCodeConstructor($bestand);

		if(!$this->verzameling)
			$this->generateCodeConstructorVolgorde($bestand);

		// Als we een verzameling zijn, maak dan alle select en to* methoden
		if ($this->verzameling)
		{
			$this->generateCodeVerzamelingForeigns($bestand); // waarschuwing: lelijke methode
		}

		// Per variabele willen we deze methoden
		foreach($this->vars as $var)
		{
			$var->generateCodeGetEnum($bestand);
			$var->generateCodeGetter($bestand); // waarschuwing: lelijke methode!
			$var->generateCodeSetter($bestand); // waarschuwing: lelijke methode!
			$var->generateCodeConstraint($bestand);
		}

		// We willen ook getters voor de foreign-verzamelingen
		$this->generateCodeForeignGetters($bestand);

		// Als laatste willen we ook alle abstracte methoden die in de dia stonden
		// plus de basis opslaan() enzo
		// let op: dit moet alleen gebeuren als we een generated object zijn om
		// conflicten met opslaan() enzo te vermijden
		if ($this->generated)
			foreach ($this->methods as $method)
				$method->generateCode($bestand, $this);
	}

	/**
	 * Deze methode geeft de parameters die nodig zijn voor de constructorfunctie
	 */
	public function getConstructorParameters()
	{
		$paramarr = array(
			false, // does this class have a foreign key?
			0, // 'l1': number of parameters
			array() // the parameters
		);
		// Als deze klasse een uitbreiding is halen we eerst alle params van de parent op
		if($this->inherits)
			$paramarr = $this->inherits->getConstructorParameters();

		// We zoeken alle private vars. PRIMARY's zijn ook private, dus die nemen we ook mee
		foreach($this->vars as $n => $id) {
			// We willen niet de primary increment int in de constructor hebben
			if (array_key_exists($id->name, $this->IDs) && $id->type == 'int' && !$id->isForeign()) continue;

			// Neem alleen de privates en primary's
			if ($id->visibility !== 'private' && !$id->annotation['PRIMARY']) continue;

			// Neem niks met NO_CONSTRUCTOR
			if ($id->annotation['NO_CONSTRUCTOR']) continue;

			// Sta alleen als niet foreign types objecten toe
			if ($id->isForeign() && !$id->isForeignObject()) continue;

			$paramarr[0] |= $id->isForeign();
			$paramarr[2][$id->name] = array(chr(97 + $paramarr[1]++), $id);
		}
		return $paramarr;
	}

	/**
	 * Deze methode schrijft naar $fp de constructor-template
	 * @param Bestand $bestand
	 */
	public function generateCodeConstructor(Bestand $bestand)
	{
		// Geen views
		if($this->view || $this->name == 'View_Generated')
			return;

		// Haal de constructorparameters op
		$paramarr = $this->getConstructorParameters();

		$hasForeign = $paramarr[0];
		$l1 = $paramarr[1];
		$constr = $paramarr[2];

		// Als we een extensie zijn, haal dan ook de parameters van de parent op
		// zodat we die door kunnen geven aan de parent-constructor
		if($this->inherits)
		{
			$paramarr = $this->inherits->getConstructorParameters();
			$l1in = $paramarr[1];
			$constr_inherit = $paramarr[2];
		}

		// Genereer de docs van de constructor
		$docCommentTemplate = new DocCommentTemplate();
		$docCommentTemplate->setBrief("De constructor van de {$this->name}-klasse.");

		if($l1 > 0)
		{
			$bestand->addContent("\t/**\n");

			foreach($constr as $n => $val)
			{
				$l = $val[0];
				$var = $val[1];

				if(is_null($l))
					continue;

				$parameterName = $l;

				$beschrijving = ucfirst($var->name) . " (";

				if($var->isForeignObject())
				{
					$beschrijving .= "{$var->foreign->name} OR Array(";

					$first = true;
					foreach($var->foreign->IDs as $n => $l)
					{
						if($l->isForeignObject())
							continue;

						if($first) {
							$first = false;
						} else {
							$beschrijving .= ", ";
						}

						$exp = explode('_', $n);

						$beschrijving .= lcfirst($var->foreign->name).'_'.lcfirst(end($exp));
					}
					$beschrijving .= ")";

					if(count($var->foreign->IDs) == 1)
					{
						$beschrijving .= " OR ";

						reset($var->foreign->IDs);
						$exp = explode('_', key($var->foreign->IDs));

						$beschrijving .= lcfirst($var->foreign->name).'_'.lcfirst(end($exp));
					}
				}
				else
					$beschrijving .= $var->type;

				$beschrijving .= ")";

				// TODO: kunnen we het type ook in PHPDoc-syntax tonen?
				$docCommentTemplate->addParam($parameterName, 'mixed', $beschrijving);
			}
		}

		$bestand->addContent($docCommentTemplate->generate(1)
			. "\tpublic function __construct(");

		// Voeg parameters toe aan de constructor (als we die hebben)
		$first = true;

		foreach($constr as $n => $val)
		{
			$l = $val[0];
			$var = $val[1];

			if(is_null($l))
				continue;

			if(!$first)
				$bestand->addContent(",\n\t\t\t");
			else
				$first = false;

			// Voeg ook de default-waarde toe in de constructor
			if($var->type === 'date' || $var->type === 'datetime')
				$bestand->addContent("\$$l = NULL");
			else
				$bestand->addContent("\$$l = ".$var->default);
		}

		$bestand->addContent(")\n\t{\n");

		// Als we een extensie zijn, roep dan als eerste de parent-constructor aan
		if($this->inherits)
		{
			// in de .cls.php moet de parent-constructor met de
			// juiste argumenten aangeroepen worden, haal daarom de
			// argumenten op en stop in de parent-construtctor
			if($l1in > 0)
			{
				$bestand->addContent("\t\tparent::__construct("
					.	'$'.implode(', $', range('a', chr(96 + $l1in)))
					.	"); "
					.	"// ".$this->inherits->name."\n");
			}
			else
			{
				$bestand->addContent("\t\tparent::__construct(); "
					.       "// ".$this->inherits->name."\n");
			}
		}

		foreach($constr as $n => $val)
		{
			$l = $val[0];
			$var = $val[1];

			if(!in_array($var, $this->vars))
				continue;

			$bestand->addContent("\n");

			switch($var->type)
			{
			case 'int':
			case 'bool':
				$bestand->addContent("\t\t\$$l = ($var->type)\$$l;\n");
				break;
			case 'bigint':
				$bestand->addContent("\t\t\$$l = (int)\$$l;\n");
				break;
			case 'money':
				$bestand->addContent("\t\t\$$l = (float)\$$l;\n");
				break;
			case 'text':
			case 'html':
			case 'string':
			case 'url':
				$bestand->addContent("\t\t\$$l = trim(\$$l);\n");
				if(!$var->annotation['NULL'])
					$bestand->addContent("\t\tif(is_null(\$$l))\n"
							.	"\t\t\t\$$l = \"\";\n");
				break;
			case 'date':
			case 'datetime':
				$bestand->addContent("\t\tif(!\$$l instanceof DateTimeLocale)\n"
						.	"\t\t{\n"
						.	"\t\t\tif(is_null(\$$l))\n"
						.	"\t\t\t\t\$$l = new DateTimeLocale();\n"
						.	"\t\t\telse\n"
						.	"\t\t\t{\n"
						.	"\t\t\t\ttry {\n"
						.	"\t\t\t\t\t\$$l = new DateTimeLocale(\$$l);\n"
						.	"\t\t\t\t} catch(Exception \$e) {\n"
						.	"\t\t\t\t\tthrow new BadMethodCallException();\n"
						.	"\t\t\t\t}\n"
						.	"\t\t\t}\n"
						.	"\t\t}\n");
				break;
			case 'enum':
				$bestand->addContent("\t\t\$$l = strtoupper(trim(\$$l));\n"
						.	"\t\tif(!in_array(\$$l, static::enums".ucfirst($var->name)."()))\n"
						.	"\t\t\tthrow new BadMethodCallException();\n");
				break;
			case 'foreign':
				if(!$var->isForeignObject())
					break;

				$bestand->addContent("\t\tif(is_array(\$$l)");
				$i = 0;
				foreach($var->foreign->IDs as $nn => $ll)
				{
					$bestand->addContent(" && ");
					$bestand->addContent("is_null(\$$l"."[".($i++)."])");
				}

				$bestand->addContent(")\n"
						.	"\t\t\t\$$l = NULL;\n\n");

				$bestand->addContent("\t\tif(\$$l instanceof ".$var->foreign->name.")\n"
						.	"\t\t{\n"
						.	"\t\t\t\$this->$n = \$$l;\n");

				foreach($var->foreign->IDs as $nn => $ll)
				{
					if($ll->isForeignObject())
						continue;

					$exp = explode('_', $nn);
					$getter = 'get';
					foreach($exp as $e)
						$getter .= ucfirst($e);

					$bestand->addContent("\t\t\t\$this->".$var->name."_".lcfirst(end($exp))." = \$".$l."->$getter();\n");
				}

				$bestand->addContent("\t\t}\n"
						.	"\t\telse if(is_array(\$$l))\n"
						.	"\t\t{\n"
						.	"\t\t\t\$this->$n = NULL;\n");

				$i = 0;
				foreach($var->foreign->IDs as $nn => $ll)
				{
					if($ll->isForeignObject())
						continue;

					$exp = explode('_', $nn);

					$bestand->addContent("\t\t\t\$this->".$var->name."_".lcfirst(end($exp))." = (".$ll->type.")\$".$l."[".($i++)."];\n");
				}

				if(count($var->foreign->IDs) == 1)
				{
					$bestand->addContent("\t\t}\n"
							.	"\t\telse if(isset(\$$l))\n"
							.	"\t\t{\n"
							.	"\t\t\t\$this->$n = NULL;\n");

					reset($var->foreign->IDs);
					$exp = explode('_', key($var->foreign->IDs));

					$bestand->addContent("\t\t\t\$this->".$var->name."_".lcfirst(end($exp))." = (".$ll->type.")\$".$l.";\n");
				}

				if($var->annotation['NULL'])
				{
					$bestand->addContent("\t\t}\n"
							.	"\t\telse\n"
							.	"\t\t{\n"
							.	"\t\t\t\$this->$n = NULL;\n");
					foreach($var->foreign->IDs as $nn => $ll)
					{
						if($ll->isForeignObject())
							continue;

						$exp = explode('_', $nn);

						$bestand->addContent("\t\t\t\$this->".$var->name."_".lcfirst(end($exp))." = NULL;\n");
					}
					$bestand->addContent("\t\t}\n");
				}
				else
				{
					$bestand->addContent("\t\t}\n"
							.	"\t\telse\n"
							.	"\t\t{\n"
							.	"\t\t\tthrow new BadMethodCallException();\n"
							.	"\t\t}\n");
				}
			default:
				break;
			}
			if ($var->type == 'html')
			{
				$bestand->addContent("\t\tif (!is_null(\$$l))\n"
					. "\t\t\t\$$l = Purifier::Purify(\$$l);\n");
			}

			if($var->type !== 'foreign')
				$bestand->addContent("\t\t\$this->$n = \$$l;\n");
		}

		$first = true;
		foreach($this->IDs as $n => $id)
		{
			if($id->type != 'int' || $id->isForeign()) continue;
			$bestand->addContent(($first ? "\n" : "")
					.	"\t\t\$this->$n = NULL;\n");
			$first = false;
		}

		$not_using_foreign = false;

		foreach($this->vars as $var)
		{
			if ($var->annotation['PSEUDO'])
				continue;

			if(isset($this->IDs[$var->name]))
				continue;

			if(!$var->annotation['PRIMARY'])
				$not_using_foreign = false;

			if ($var->isForeignObject()) {
				$not_using_foreign = $var->visibility === 'private';
			}

			if($var->visibility === 'private' && !$var->annotation['NO_CONSTRUCTOR']
					&& (!$var->isForeign() || $not_using_foreign))
				continue;

			$first = $var->generateCodeDefault($bestand, $first);
		}

		// Initialiseer de foreign-verzamelingen op NULL
		foreach($this->foreignrelations as $c => $isnull)
			$bestand->addContent("\t\t\$this->".lcfirst($c)."Verzameling = NULL;\n");

		$bestand->addContent("\t}\n");
	}

	/**
	 * Deze methode schrijft naar $fp de constructorVolgorde-template
	 * @param Bestand $bestand
	 */
	public function generateCodeConstructorVolgorde(Bestand $bestand)
	{
		// Geen views
		if($this->view || $this->name == 'View_Generated')
			return;

		// Haal de constructorparameters op
		$paramarr = $this->getConstructorParameters();
		$hasForeign = $paramarr[0];
		$l1 = $paramarr[1];
		$constr = $paramarr[2];

		$bestand->addContent("\t/*** CONSTRUCTOR_VOLGORDE ***/\n"
				.	"\tstatic public function constructorVolgorde()\n"
				.	"\t{\n");

		if($l1 > 0)
		{
			$bestand->addContent("\t\t\$volgorde = array();\n");

			foreach($constr as $n => $val)
			{
				$l = $val[0];
				$var = $val[1];

				if(is_null($l))
					continue;

				$bestand->addContent("\t\t\$volgorde[] = '".ucfirst($var->name)."';\n");
			}

			$bestand->addContent("\t\treturn \$volgorde;\n");
		}
		else
		{
			$bestand->addContent("\t\treturn false;\n");
		}

		$bestand->addContent("\t}\n");
	}

	/**
	 * Deze methode schrijft naar $fp, mits dit object een verzameling is, de
	 * select-methode
	 */
	public function generateCodeVerzamelingSelect ($fp)
	{
		$obj = $this->core->name;

		$ids = array();
		foreach ($this->core->IDs as $id)
			if ($id->type != 'foreign')
				$ids[] = "`" . $id->name . "`";
		$idstring = implode(", ", $ids);

		$type = 'COLUMN';
		if (count($ids) > 1)
			$type = 'TABLE';

		$joins = '';
		$cls = $this->core;
		while ($cls = $cls->inherits)
		{
			if ($cls->getPackage()->name == 'Basis')
				break;
			if ($cls->generated)
				continue;

			$name = $cls->name;
			$joins .= "\t\t\t. 'LEFT JOIN $name USING ($idstring) '\n";
		}

		fwrite($fp, "\tstatic public function select (ConstraintVerzameling \$constraints = null, \$order = null, \$orderhow = null, \$limit = null, \$offset = null)\n"
				.	"\t{\n"
				.	"\t\tglobal \$WSW4DB;\n"
				.	"\n"
				.	"\t\tif (!is_null(\$order) && (!in_array(\$order, $obj::velden('get')) || !in_array(\$orderhow, array('ASC', 'DESC'))))\n"
				.	"\t\t\tuser_error('Ongeldige select query op $obj', E_USER_ERROR);\n"
				.	"\n"
				.	"\t\t\$query = '$type SELECT $idstring FROM $obj '\n"
				. $joins
				.	"\t\t\t. \$constraints->sqlWhere() . ' '\n"
				.	"\t\t\t. ((!is_null(\$order)) ? 'ORDER BY `'.\$order.'` '.\$orderhow.' ' : '')\n"
				.	"\t\t\t. ((!is_null(\$limit)) ? 'LIMIT %i '\n"
				.	"\t\t\t\t. ((!is_null(\$offset)) ? 'OFFSET %i' : '%_' ) : '%_ %_');\n"
				.	"\n"
				.	"\t\t\$params = \$constraints->sqlValues();\n"
				.	"\t\t\$params[] = \$limit;\n"
				.	"\t\t\$params[] = \$offset;\n"
				.	"\t\tarray_unshift(\$params, \$query);\n"
				.	"\n"
				.	"\t\t\$ids = call_user_func_array(array(\$WSW4DB, 'q'), \$params);\n"
				.	"\n"
				.	"\t\treturn self::verzamel(\$ids);\n"
				.	"\t}\n"
				.	"\n");
	}

	/**
	 * Deze methode schrijft naar $fp, mits dit object een verzameling is, alle
	 * to* methoden; deze methode heeft de $objects array nodig om te navigeren
	 * naar welke verzamelingen het allemaal gecast kan worden
	 * todo dit is een bijzonder lelijke methode, herschrijven is waarschijnlijk de moeite waard
	 * @param Bestand $bestand
	 */
	public function generateCodeVerzamelingForeigns (Bestand $bestand)
	{
		$objects = getObjects();

		if (!$this->verzameling)
			return;

		$verzClassName = substr($this->name, 0, -10);
		$className = substr($verzClassName, 0, -11);
		$name = $className . '_Generated';
		if(!isset($objects['class'][$name])
		|| substr($this->name, -21) != 'Verzameling_Generated')
			return;

		$fks = array();
		$cls = $objects['class'][$name];
		foreach($cls->vars as $var)
		{
			if($var->isForeignObject())
				$fks[] = $var;
		}

		if(count($fks) == 0) return;

		foreach($fks as $var)
		{
			$fverz = $var->foreign->name."Verzameling";
			// TODO zorgen dat de bijbehorende Verzameling altijd bestaat
			if(!isset($objects['class'][$fverz]))
				continue;

			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief("Maak van deze {$verzClassName} een {$fverz}.")
				->setReturn($fverz, "Een {$fverz} die elementen bevat die via foreign "
					. "keys corresponderen aan de elementen in deze {$verzClassName}.");

			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tpublic function to".$var->upper()."Verzameling()\n"
					.	"\t{\n"
					.	"\t\tif(\$this->aantal() == 0)\n"
					.	"\t\t\treturn new $fverz();\n"
					.	"\n"
					.	"\t\t\$origPositie = \$this->positie;\n"
					.	"\t\t\$foreignkeys = array();\n"
					.	"\t\tforeach(\$this as \$obj)\n"
					.	"\t\t{\n"
					);
			$first2 = True;
			if($var->annotation['NULL']) {
				foreach($var->foreign->IDs as $id) {
					if($id->isForeignObject()) continue;
					$n = $id->upper();
					if($id->isForeign()) $n = substr($n, strrpos($n, '_')+1);
					$bestand->addContent(($first2
							?	"\t\t\tif(is_null("
							:	") || is_null(")
							.	"\$obj->get".$var->upper().$n."()"
							);
					$first2 = false;
				}
				$bestand->addContent(")) {\n"
						.	"\t\t\t\tcontinue;\n"
						.	"\t\t\t}\n"
						.	"\n"
						);
			}
			$first2 = True;
			foreach($var->foreign->IDs as $id) {
				if($id->isForeignObject()) continue;
				$n = $id->upper();
				if($id->isForeign()) $n = substr($n, strrpos($n, '_')+1);
				$bestand->addContent(($first2
						?	"\t\t\t\$foreignkeys[] = array("
						:	"\t\t\t                      ,")
						.	"\$obj->get".$var->upper().$n."()\n"
						);
				$first2 = false;
			}
			$bestand->addContent("\t\t\t                      );\n"
					.	"\t\t}\n"
					.	"\t\t\$this->positie = \$origPositie;\n"
					.	"\t\treturn $fverz::verzamel(\$foreignkeys);\n"
					.	"\t}\n"
					);
		}

		$nvcls = $objects['class'][substr($this->name, 0, -21)];

		foreach($fks as $var)
		{
			$fverz = explode('_',$this->name);
			$fverz = $fverz[0];
			// TODO zorgen dat de bijbehorende Verzameling altijd bestaat
			if(!isset($objects['class'][$fverz]))
				continue;

			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief("Maak een {$verzClassName} van {$var->upper()}.")
				->setReturn($fverz, "Een {$fverz} die elementen bevat die bij de {$var->upper()} hoort.");

			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tstatic public function from".$var->upper()."(\$$var->name)\n"
					.	"\t{\n"
					.	"\t\tif(!isset(\$".$var->name."))\n"
					.	"\t\t\treturn new ". $fverz."();\n"
					.	"\n"
					.	"\t\treturn ".$className."Query::table()\n"
					.	"\t\t\t->whereProp('".ucfirst($var->upper())."', \$$var->name)\n"
					.	"\t\t\t->verzamel();\n"
					.	"\t}\n"
			);
		}
	}

	private function generateCodeForeignGetters(Bestand $bestand)
	{
		global $objects;

		foreach($this->foreignrelations as $c => $isnull)
		{
			$aantalForeign = 0;
			$foreignNaam = '';
			$childclass = $objects['class'][$c."_Generated"];

			foreach($childclass->vars as $id)
				if($id->isForeignObject() && $id->foreign->name == $this->real->name)
				{
					$foreignNaam = $id->name;
					$aantalForeign++;
				}

			$ucNaam = ucfirst($c)."Verzameling";
			$lcNaam = lcfirst($ucNaam);

			$docCommentTemplate = new DocCommentTemplate();
			$docCommentTemplate->setBrief("Returneert de {$ucNaam} die hoort bij dit object.");

			$bestand->addContent($docCommentTemplate->generate(1)
					.	"\tpublic function get$ucNaam()\n"
					.	"\t{\n"
					.	"\t\tif(!\$this->$lcNaam instanceof $ucNaam)\n");
			if($aantalForeign != 1)
			{
				$bestand->addContent("\t\t{\n"
						.	"\t\t\t\$this->$lcNaam = new $ucNaam();\n");

				foreach($childclass->vars as $id)
					if($id->isForeignObject() && $id->foreign->name == $this->real->name)
						$bestand->addContent("\t\t\t\$this->".$lcNaam."->union($ucNaam::from".ucfirst($id->name)."(\$this));\n");

				$bestand->addContent("\t\t}\n");
			}
			else
			{
				$bestand->addContent("\t\t\t\$this->$lcNaam = $ucNaam::from".ucfirst($foreignNaam)."(\$this);\n");
			}

			$bestand->addContent("\t\treturn \$this->$lcNaam;\n"
					.	"\t}\n");
		}
	}
}
