<?php

namespace Generator\WSW4objects;

class WSW4package
{
	public $id;
	public $name;
	public $parentId;

	function __construct($id)
	{
		$this->id = $id;
		$this->name = $id;

		if($this->id != '_ROOT')
			$this->parentId = '_ROOT';
	}
	public function setName($name)
	{
		if(empty($name)) return;
		$this->name = $name;
	}
	public function setParentId($parentId)
	{
		if(empty($parentId)) return;
		$this->parentId = $parentId;
	}
	public function getParent()
	{
		global $objects;
		return $objects['id'][$this->parentId];
	}

	public function check ($objects)
	{
		if(isset($this->parentId)
		&& !isset($objects['id'][$this->parentId]))
			error($this, "1 parent not a valid object");
	}
	public function path()
	{
		global $objects;

		if($this->parentId)
			$path = $objects['id'][$this->parentId]->path();
		else
			$path = '';

		return $path . $this->name . '/';
	}
}
