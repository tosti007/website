<?php

/**
 * @brief Is een template voor een docComment voor een methode.
 */
final class DocCommentTemplate
{
	/**
	 * Een constante die de lengte aangeeft waarover regels moeten worden afgebroken.
	 */
	const WRAP_LENGTH = 80;
	/**
	 * De brief beschrijving voor in het DocCommentBlok.
	 * @var string
	 */
	private $brief;

	/**
	 * De beschrijvingen van de params voor in het DocCommentBlok.
	 * Let op: de namen van de params zijn de indices van de array.
	 * @var string[]
	 */
	private $params;
	/**
	 * De types van de params voor in het DocCommentBlok.
	 * Let op: de namen van de params zijn de indices van de array.
	 * @var string[]
	 */
	private $paramTypes;

	/**
	 * De return beschrijving voor in het DocCommentBlok.
	 * @var string|NULL
	 */
	private $return;

	/**
	 * De sees, AKA verwijzingen, voor in het DocCommentBlok.
	 * @var array
	 */
	private $sees;

	public function __construct()
	{
		$this->params = [];
		$this->paramTypes = [];
		$this->sees = [];
	}

	/**
	 * @brief Geef de brief van het object een waarde.
	 *
	 * @param string $brief De brief beschrijving.
	 *
	 * @return $this
	 */
	public function setBrief($brief)
	{
		$this->brief = $brief;
		return $this;
	}

	/**
	 * @brief Voegt een beschrijving van een parameter toe
	 *
	 * @param string $param De naam van de parameter.
	 * @param string $type Het type van de parameter.
	 * @param string $description De beschrijving van de parameter.
	 *
	 * @return $this.
	 */
	public function addParam($param, $type, $description)
	{
		$this->params[$param] = $description;
		$this->paramTypes[$param] = $type;
		return $this;
	}

	/**
	 * @brief Geeft de return van het object een waarde.
	 *
	 * @param string $type Het returntype.
	 * @param string $return De return beschrijving.
	 *
	 * @return $this.
	 */
	public function setReturn($type, $return)
	{
		$this->return = $type . "\n" . $return;
		return $this;
	}

	/**
	 * @brief Voegt een see toe.
	 *
	 * @param string $see De see om toe te voegen.
	 *
	 * @return $this.
	 */
	public function addSee($see)
	{
		$this->sees[] = $see;
		return $this;
	}

	/**
	 * @brief Genereert het DocCommentBlok.
	 *
	 * @param int $tabsAmount De hoeveelheid tabs die aan het begin van elke
	 * regel van het blok moeten komen.
	 *
	 * @return string
	 * Het blok als een string.
	 */
	public function generate($tabsAmount = 0)
	{
		// Een docblok moet altijd een brief hebben!
		if(!$this->brief) {
			error($this, "Brief mag niet null zijn");
		}

		// Wrap de brief met de juste lengte en zet een '*' aan het begin van
		// elke regel. De '/^/m' in de preg betekent: zoek elk begin van elke regel.
		$brief = preg_replace('/^/m', " * ", wordwrap("@brief {$this->brief}", self::WRAP_LENGTH));

		// Wrap de sees met de juste lengte en zet een '*' aan het begin van
		// elke regel. Plak daarnaa alles sees aan elkaar. De '/^/m' in de
		// preg betekent: zoek elk begin van elke regel.
		$sees = implode(PHP_EOL, array_map(function ($see) {
			return preg_replace('/^/m', " * ", wordwrap("@see {$see}", self::WRAP_LENGTH));
		}, $this->sees));

		// Wrap de params met de juste lengte en zet een '*' aan het begin van
		// elke regel. Plak daarnaa alles params aan elkaar. De '/^/m' in de
		// preg betekent: zoek elk begin van elke regel.
		$params = implode(PHP_EOL, array_map(function ($name, $param, $paramType) {
			return preg_replace('/^/m', " * ", wordwrap(
				"@param {$paramType} \${$name} {$param}",
				self::WRAP_LENGTH)
			);
		}, array_keys($this->params), $this->params, $this->paramTypes));

		// Wrap de return met de juste lengte en zet een '*' aan het begin van
		// elke regel. De '/^/m' in de preg betekent: zoek elk begin van elke regel.
		$return = "";
		if($this->return) {
			$return = preg_replace('/^/m', " * ", wordwrap("@return {$this->return}", self::WRAP_LENGTH));
		}

		// Bouw het blok op. Tussen twee verschillende onderdelen van het blok
		// moet altijd een regel met alleen een '*' staan.
		$docComment = "/**" . PHP_EOL
					. $brief . PHP_EOL
					. (!empty($sees) ? " *" . PHP_EOL . $sees . PHP_EOL : "")
					. (!empty($params) ? " *" . PHP_EOL . $params . PHP_EOL : "")
					. (!empty($return) ? " *" . PHP_EOL . $return . PHP_EOL : "")
					. " */" . PHP_EOL;

		// Prefix hier elke regel met het aantal gewenste tabs.
		return preg_replace('/^/m', str_repeat("\t", $tabsAmount), $docComment);
	}
}
