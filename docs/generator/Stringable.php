<?php

interface Stringable
{
	public function __toString();
}
