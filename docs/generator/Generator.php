<?php

namespace Generator;

use Generator\WSW4objects\WSW4class;

class Generator
{
	/**
	 * @var \DOMDocument
	 */
	private $doc;

	/**
	 * Generator constructor.
	 * @param \DOMDocument $doc
	 */
	public function __construct(\DOMDocument $doc)
	{
		$this->doc = $doc;
	}

	public function generate($type = 'alle')
	{
		// We moeten hier objects nog global zetten omdat het nog op veel plekken als global gebruikt wordt.
		global $objects;
		// Laad alle objecten uit de dia en zet ze om in WSW4objecten
		// deze methode controleert ook gelijk de geldigheid van de geladen dia
		$objects = parseObjects($this->doc);

		/*	Na deze aanroep ziet $objects er ongeveer zo uit:
				$objects				= array();
				$objects['id']			= array();
				$objects['class']		= array();
				$objects['implements']	= array();
				$objects['inherits']	= array();
				$objects['package']		= array();

			De doelstelling van de komende paar regels is om alle objecten die
			geschreven moeten worden (dus alle objecten in de dia, plus verzamelingen,
			views en generated) in deze array te krijgen op zo'n manier dat alle ids
			en kind-ouder-relaties goed staan
		*/

		// Zoek alle primaire sleutels
		$this->findClassIDs();

		// Controleer of alle niet-basis objecten nu een primaire sleutel hebben
		$this->checkClassIDs();

		// Zorg ervoor dat alle foreign-object-variabelen goed gaan
		$this->updateForeignVars();

		// Haal alle reverse foreing vars op
		$this->getReverseForeignVars();

		// Maak dat alles een kind is, mochten ze dat niet al zijn dan van Entiteit
		$this->implicitInheritBasis();

		// Zorg ervoor dat alle abstracte methoden van de ouders naar de kinderen
		// worden gekopieerd
		$this->inheritVirtuals();

		// Maak alle afgeleide verzameling-klassen (bijv LidVerzameling)
		$this->createVerzamelingen();

		// Maak alle afgeleiden view-klassen (bijv LidView)
		$this->createViewLayer();

		// Maak alle afgeleiden query-klassen (bijv LidQuery)
		$this->createQueryLayer();

		// De hoofd gegenereerde abstracte klassen (bijv Lid_Generated)
		$this->createGeneratedLayer();

		// De api wordt op dit moment niet meer ondersteund. Mocht in de toekomst
		// de api wel weer worden gebruikt, dan is die makkelijk weer terug te
		// halen
		/*
		// De api-klassen
		createAPILayer();*/

		// Sorteer even alles mooi, dit heeft verder geen functie
		ksort($objects['class']);
		ksort($objects['package']);

		// Maak de code van alle klassen
		$this->generateCode($type);

		// Maak init.php
		$this->generateInit();

		// Maak includes-generated.php
		$this->generateIncludesGenerated();

		// Maak database.sql
		$this->generateDBCreate();

	}

	/**
	 * Deze functie haalt uit alle objecten in $objects de primaire sleutels
	 * en updatet de array
	 */
	private function findClassIDs ()
	{
		global $objects;

		// Zoek alle primaire sleutels
		foreach ($objects['class'] as $cls)
		{
			$nxt = $cls;
			while ($nxt)
			{
				// Kijken of er attributes zijn met annotation PRIMARY
				foreach ($nxt->vars as $v)
					if ($v->annotation['PRIMARY'])
						$cls->IDs[$v->name] = $v;

				if (!empty($cls->IDs))
				{
					if($cls != $nxt) {
						// even onthouden waar we IDs aan het uitwisselen zijn
						$cls->IDsource = $nxt;
						$nxt->IDchildren[] = $cls;
					}
					break;
				}

				// Niks gevonden, we gaan verder zoeken bij de ouder van het object
				$nxt = $nxt->inherits;
			}
		}
	}

	/**
	 * Deze functie controleert van alle objecten in $objects die niet basis zijn
	 * of ze een primaire sleutel hebben (let op dat als er in $objecten al de
	 * de verzameling-objecten zitten dit fout gaat, die hebben per definitie geen
	 * primaire sleutel)
	 */
	private function checkClassIDs ()
	{
		global $objects;

		foreach ($objects['class'] as $cls)
			if ($cls->getPackage()->name != 'Basis' && count($cls->IDs) < 1)
				error($cls, 'heeft geen primaire sleutel');
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects alle variabelen met
	 * object-types en updatet de objecten zodat alle primaire sleutels van
	 * de vreemde object-type ook in het oorspronkelijke object zitten
	 */
	private function updateForeignVars ()
	{
		global $objects;

		// Ga alle variabelen in alle objecten na om objecttypen van variabelen ook
		// goed te zetten
		foreach($objects['class'] as $cls)
		{
			// Check of er foreign keys zitten in de $cls
			$k = 0;
			foreach($cls->vars as $v)
			{
				// Onthoud de huidige index van de variabele, anders verandert de volgorde van
				// alle variabelen in de klasse
				$k++;

				// We hoeven alleen foreign keys aan te pakken.
				if (!$v->isForeign())
					continue;

				// We willen dat de foreign keys unieke korte namen hebben,
				// dus we moeten een tellertje ophogen als we die naam al hebben gezien.
				$foreignAantal = [];

				$l = 0;
				foreach($v->foreign->IDs as $id)
				{
					// Skip alle sleutels die objecten zijn
					// geen nood dat we nu sleutels skippen!
					// Het is namelijk zo dat automatisch de primaire sleutels van
					// de sleutels ook in het rijtje staan, dus het kan nooit zo zijn
					// dat dit voor een v altijd true geeft.
					if ($id->isForeignObject())
						continue;

					// We gaan een nieuwe variabele toevoegen, deze representeert de unieke
					// sleutel van het object wat in v zit
					$v2 = clone $id;

					// Geef in de originele variabele aan dat we deze subkey hebben.
					$v->subKeys[] = $v2;

					// Als v2->foreign geset (bool) is, dwz de index id van het vreemde object
					// correspondeerd met een vreemd object, dan heeft het een samengestelde
					// naam, wij willen alleen het laatste segment
					if ($v2->isForeign())
					{
						// We hebben objecten al geskipt, dus het moet een key zijn.
						assert($v2->isForeignKey());

						// Haal de laatste mogelijke naam van de index die we hebben
						// dwz de hoogste mogelijke index, dus activiteit_activiteitID wordt
						// activiteitID
						$korteNaam = substr($v2->name, strrpos($v2->name, '_') + 1);

						// Hebben we deze naam al eens gebruikt?
						// Zo ja, dan plakken we er een getal achter om het uniek te maken
						// Dit is het geval als klasse A B bevat, en B heeft als primaire
						// sleutel een gecombineerde van twee keer C
						if (@$foreignAantal[$korteNaam])
						{
							$foreignAantal[$korteNaam]++;
							$korteNaam = $korteNaam . $foreignAantal[$korteNaam];
						}
						else
						{
							$foreignAantal[$korteNaam] = 1;
						}
					}
					else
					{
						$korteNaam = $v2->name;
					}

					// De nieuwe variabele wordt een samengestelde naam en er wordt genoteerd
					// dat de sleutel correspondeert met een vreemd object
					$v2->nameForDB = $v->name."_".$v2->name;
					$v2->name = $v->name."_".$korteNaam;
					$v2->setForeignKey();
					$v2->class = $cls;

					if ($v->annotation['NULL'])
					{
						$v2->annotation['NULL'] = TRUE;
						$v2->default = 'NULL';
					}

					// Voeg de nieuwe variabele toe
					// Maar controleer eerst of we met een primaire sleutel te maken hebben!
					if (isset($cls->IDs[$v->name]))
					{
						// lookup index van $v->name
						$idsVoorNaam = array_keys(array_keys($cls->IDs), $v->name);
						$l2 = array_shift($idsVoorNaam);
						$l++;
						$cls->IDs = array_insert($cls->IDs, $l+$l2, $v2, $v2->name);
					}
					$cls->vars = array_insert($cls->vars, $k++, $v2);
				}
			}
		}
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects alle variabelen met
	 * object-types en updatet de objecten zodat alle primaire sleutels van
	 * de vreemde object-type ook in het oorspronkelijke object zitten
	 */
	private function getReverseForeignVars ()
	{
		global $objects;

		foreach($objects['class'] as $cls) {
			// check of er foreign keys zitten in de $cls
			foreach($cls->vars as $v)
			{
				if(!$v->isForeign() || $v->type != "foreign") continue;

				if(!array_key_exists($cls->name,$objects['class'][$v->foreign->name]->foreignrelations)) {
					if(strstr($v->comment,'@NULL')) {
						$objects['class'][$v->foreign->name]->foreignrelations[$cls->name] = true;
					} else {
						$objects['class'][$v->foreign->name]->foreignrelations[$cls->name] = false;
					}
				}
			}
		}
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects en zorgt ervoor dat ze een
	 * kind zijn van iets, mochten ze dat niet al zijn dan worden ze een kind van
	 * het object Entiteit
	 */
	private function implicitInheritBasis ()
	{
		global $objects;

		foreach ($objects['class'] as $cls)
		{
			// is een basis klasse
			if ($cls->getPackage()->name == 'Basis')
				continue;

			// heeft al expliciete inheritance
			if ($cls->inherits)
				continue;

			// Is het een verzameling?
			if (substr($cls->name, -11) == 'Verzameling')
			{
				$cls->inherits = $objects['class']['Verzameling'];
				continue;
			}

			if(substr($cls->name, -5) == 'Query')
			{
				$cls->inherits = $objects['class']['QueryObject'];
				continue;
			}

			// Nog geen erving, laat de ouder het object Entiteit zijn
			$cls->inherits = $objects['class']['Entiteit'];
		}
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects de ouders op, checkt of ze
	 * virtuele methodes hebben, en zo ja, maakt een kopie daarvan in het huidige
	 * object
	 */
	private function inheritVirtuals()
	{
		global $objects;

		foreach($objects['class'] as $cls)
		{
			// Ga een-voor-een alle ouders af en maak gebruik dat in php ieder
			// object max een ouder heeft
			$fnd = $cls;
			while ($fnd = $fnd->inherits)
			{
				foreach ($fnd->methods as $m)
				{
					// Controleer of de gevonden methode echt virtueel is en niet
					// een abstracte methode die eerder is gekopieerd (dan zouden
					// we kopieen van kopieen krijgen)
					// Let op: De generated variabele betekent niet of de code
					// automatisch gegenereerd kan worden
					if ($m->virtual && !$m->generated)
					{
						// Kloon de methode en noteer gelijk dat dit niet het
						// orgineel is
						$m2 = clone $m;
						$m2->generated = True;
						$m2->abstract = False;
						$m2->param = $m->param;
						$cls->addMethod($m2);

						// Als dit in het basis-pakket gebeurd, dan moet het ook
						// worden geimplementeerd door de programmeur (we zitten
						// immers in het hoogste niveau, er is geen andere klasse
						// hierboven die het voor ons kan doen)
						if ($cls->getPackage()->name == 'Basis')
							$cls->setAbstract(true);
					}
				}
			}
		}
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects op, checkt of ze al een
	 * query hebben, zo niet maak er een
	 */
	private function createQueryLayer()
	{
		global $objects;

		// Hier krijgt elke gewone klasse een query (als die er nog niet was)
		foreach ($objects['class'] as $cls)
		{
			// Basis klassen willen we niet
			if ($cls->view || $cls->verzameling || $cls->getPackage()->name == 'Basis')
				continue;

			// Controleer of de query nog niet bestaat
			$name = $cls->name.'Query';
			if (!isset($objects['class'][$name]))
			{
				// Maak de nieuwe query
				$query = new WSW4class('Q'.$cls->id);
				$query->name = $name;
				$query->packageId = $cls->packageId;
				$query->core = $cls->core;
				$query->real = $cls;
				$query->query = true;

				// Vul de overerving even met tijdelijke waarden
				// deze gaan we later goed zetten zodra we zeker
				// weten dat alle objecten gemaakt zijn
				if ($cls->inherits->getPackage()->name == 'Basis')
					$query->inherits = $objects['class']['Query'];
				else
					$query->inherits = 'Q'.$cls->inherits->id;

				// Bewaar de nieuwe query
				$objects['id'][$query->id] = $query;
				$objects['class'][$query->name] = $query;
			}
		}

		// Zet de overerving goed
		foreach($objects['class'] as $cls)
			if (!is_null($cls->inherits) && !is_object($cls->inherits))
				$cls->inherits = $objects['id'][$cls->inherits];
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects op, checkt of ze al een
	 * verzameling hebben, zo niet maak er een
	 */
	private function createVerzamelingen()
	{
		global $objects;

		// Hier krijgt elke gewone klasse een verzameling (als die er nog niet was)
		foreach ($objects['class'] as $cls)
		{
			// Basis klassen willen we niet
			if ($cls->getPackage()->name == 'Basis')
				continue;

			// Controleer of de verzameling nog niet bestaat
			$name = $cls->name.'Verzameling';
			if (!isset($objects['class'][$name]))
			{
				// Maak de nieuwe verzameling
				$verz = new WSW4class('C'.$cls->id);
				$verz->name = $name;
				$verz->packageId = $cls->packageId;
				$verz->core = $cls->core;
				$verz->verzameling = true;

				// Vul de overerving even met tijdelijke waarden
				// deze gaan we later goed zetten zodra we zeker
				// weten dat alle objecten gemaakt zijn
				if ($cls->inherits->getPackage()->name == 'Basis')
					$verz->inherits = $objects['class']['Verzameling'];
				else
					$verz->inherits = 'C'.$cls->inherits->id;

				// Bewaar de nieuwe verzameling
				$objects['id'][$verz->id] = $verz;
				$objects['class'][$verz->name] = $verz;
			}
		}

		// Zet de overerving goed
		foreach($objects['class'] as $cls)
			if (!is_null($cls->inherits) && !is_object($cls->inherits))
				$cls->inherits = $objects['id'][$cls->inherits];
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects op, en maak een bijbehorend
	 * view-object
	 */
	private function createViewLayer()
	{
		global $objects;

		// Hier krijgt elke gewone klassen een eigen view klasse.
		foreach($objects['class'] as $cls)
		{
			// Skip alles wat basis is
			if ($cls->getPackage()->name == 'Basis')
				continue;

			// Maak de nieuwe view aan en zet gelijk alle vars goed
			$view = new WSW4class('V'.$cls->id);
			$view->generated = False;
			$view->packageId = $cls->packageId;
			$view->setName($cls->name.'View');
			$view->setAbstract(True);
			$view->real = $cls;
			$view->core = $cls->core;
			$view->view = True;

			// Vertel voor welke variabelen we straks allemaal code moeten schrijven
			// (dit worden de label*, form*, waarde* en opmerking* methoden)
			foreach($cls->vars as $k => $v)
			{
				if ($v->static)
					continue;

				$v2 = clone $v;
				$view->addVar($v2);
			}

			// Vul de overerving even met tijdelijke waarden
			// deze gaan we later goed zetten zodra we zeker
			// weten dat alle objecten gemaakt zijn
			if ($cls->inherits->getPackage()->name == 'Basis')
				$view->inherits = $objects['class']['View'];
			else
				$view->inherits	= 'V'.$cls->inherits->id;

			// Voeg de nieuwe view toe aan alle andere objecten
			$objects['id'][$view->id] = $view;
			$objects['class'][$view->name] = $view;
		}

		// Zet de overerving goed
		foreach($objects['class'] as $cls)
			if (!is_null($cls->inherits) && !is_object($cls->inherits))
				$cls->inherits = $objects['id'][$cls->inherits];
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects op, en maak een bijbehorend
	 * abstract-gegenereerde object (bijv Lid_Generated)
	 */
	private function createGeneratedLayer()
	{
		global $objects;

		//  Hier krijgt elke gewone klassen een abstracte klasse als super.
		//  In de abstracte super klasse komen de variabelen (met hun getters/
		//  setters) en de abstracte varianten van de methoden die de klasse
		//  behoord te hebben.
		foreach ($objects['class'] as $cls)
		{
			// Kloon de huidige klasse en vul alle variabelen in
			$gen = clone $cls;
			$gen->generated = True;
			$gen->id = 'G'.$cls->id;
			$gen->setName($cls->name.'_Generated');
			$gen->setAbstract(True);
			$gen->methods = array();
			$gen->vars = array();
			$gen->inherits = $cls->inherits;
			$gen->implements = array();
			$gen->core = $cls->core;

			// Zeg tegen de oorspronkelijke klasse dat dit zijn ouder wordt
			// en zeg tegen de nieuwe klasse dat hij slechts de abstracte
			// versie is van de klasse
			$cls->inherits = $gen;
			$gen->real = $cls;

			// Verplaats alle variabelen van het oorspronkelijke object naar
			// het nieuwe object
			foreach ($cls->vars as $k => $v)
			{
				// Sla de statische dingen over
				if($v->static)
					continue;

				// Verplaats die variabele
				unset($cls->vars[$k]);
				$gen->addVar($v);
			}

			// Verplaats alle methoden van het oorspronkelijke object naar
			// het nieuwe object
			foreach ($cls->methods as $k => $m)
			{
				// Sla de statische en virtuele dingen over
				if($m->static && !$m->virtual)
					continue;

				// Maak de nieuwe methode die we later gaan plaatsen
				$m2 = clone $m;

				// Als de methode abstract is, en de klasse niet, maak dan de
				// orginele methode niet abstract; dit is veilig omdat cls de
				// uiteindelijk implementatie van een object bevat
				if ($m->abstract && !$cls->abstract)
					$m->setAbstract(False);

				// Als de methode virtueel is dan is er een implementatie voor
				// handen, anders niet; dus als de oorspronkelijke methode niet
				// virtueel is, dan moet de programmeur dat zelf doen en is de
				// nieuwe methode abstract
				if (!$m->virtual)
					$m2->setAbstract(True);

				// Voeg de nieuwe methode toe
				$gen->addMethod($m2);
			}

			// En voeg het nieuwe object toe
			$objects['id'][$gen->id] = $gen;
			$objects['class'][$gen->name] = $gen;
		}
	}

	/**
	 * Deze functie haalt uit $objects alle objecten die in aanmerkingen komen voor
	 * een api (dat zijn de niet views, verzamelingen of andere basis-objecten)
	 * en maakt de bijbehorende objecten
	 */
	private function createAPILayer ()
	{
		global $objects;

		// Loop alle objecten langs die we al zover hebben verzameld
		foreach ($objects['class'] as $cls)
		{
			// We willen geen api van views of verzamelingen of dingen uit de basis
			if ($cls->view || $cls->verzameling || $cls->getPackage()->name == 'Basis')
				continue;

			// Maak het api-object aan
			$m = new WSW4class('A' . $cls->id);
			$m->name = $cls->name . 'API';
			$m->packageId = $cls->packageId;
			$m->generated = $cls->generated;
			$m->core = $cls->core;
			$m->annotation = $cls->annotation;
			$m->api = true;
			$m->abstract = true;

			// Als dit niet een generated-object is, zet dan de parent goed
			if (!$cls->generated)
				$m->inherits = 'A' . $cls->inherits->id;

			// Als dit een generated-object is, zet de real goed
			if ($cls->generated)
				$m->real = 'A' . $cls->real->id;

			// Bewaar de nieuwe verzameling
			$objects['id'][$m->id] = $m;
			$objects['class'][$m->name] = $m;
		}

		// Zet de overerving plus reals goed
		foreach($objects['class'] as $cls)
		{
			if (!is_null($cls->inherits) && !is_object($cls->inherits))
				$cls->inherits = $objects['id'][$cls->inherits];
			if (is_string($cls->real))
				$cls->real = $objects['id'][$cls->real];
		}
	}

	/**
	 * Deze functie haalt uit alle objecten in $objects op en vertelt ze dat ze
	 * eens code moeten gaan maken
	 * @param string $type Welke soort klassen gegenereerd moeten worden.
	 */
	private function generateCode ($type = 'alle')
	{
		global $objects;

		foreach($objects['class'] as $cls) {
			if($type !== 'alle' && strpos(strtolower($cls->name), strtolower($type)) === false)
				continue;
			echo "Generating $cls->name...\n";
			$cls->generateCode();
		}
	}

	/**
	 * Deze functie maakt init.php, het hart van het systeem
	 */
	private function generateInit ()
	{
		// Kopieer de inhoud van dit bestand van de template
		$content = file_get_contents('generator/templates/init.php');

		$bestand = new Bestand('WhosWho4/init.php');
		$bestand->addContent($content);
		$bestand->write(true);
	}

	/**
	 * Deze functie maakt includes-generated.php
	 */
	private function generateIncludesGenerated ()
	{
		global $objects;

		$bestand = new Bestand('WhosWho4/includes-generated.php');

		// Het wordt een uitvoerbaar php-bestand
		$bestand->addContent("<?\n"
			.	"function wsw4_autoload(\$class)\n"
			.	"{\n"
			.	"\tswitch(\$class)\n"
			.	"\t{\n");

		// Werk alle objecten per pakket af
		foreach($objects['package'] as $pkg)
		{
			foreach($objects['class'] as $cls)
			{
				// Hoort dit object wel bij het pakket wat we nu aan het maken zijn
				if($pkg->id != $cls->packageId)
					continue;

				$bestand->addContent("\t\tcase '".$cls->name."':\n"
					.	"\t\t\trequire_once('".$cls->path()."');\n"
					.	"\t\t\tbreak;\n");
			}
			$bestand->addContent("\n");
		}
		$bestand->addContent("\t}\n"
			.	"}\n"
			.	"spl_autoload_register('wsw4_autoload');");

		$bestand->write(true);
	}

	/**
	 * Deze functie maakt database.sql
	 */
	private function generateDBCreate()
	{
		global $objects;

		$bestand = new Bestand('WhosWho4/database.sql');

		$bestand->addContent("/* WhosWho4 DB Table definities */\n"
			.	"/* Vergeet de DB niet aan te passen! */\n"
		);

		foreach ($objects['class'] as $cls)
		{
			/** @var WSW4class $cls */
			// Abstracte objecten en objecten zonder primary komen niet in de
			// database (niet abstracte objecten zonder primaire sleutels zijn
			// bijvoorbeeld verzamelingen)
			if ($cls->abstract || count($cls->IDs) == 0)
				continue;

			$bestand->addContent("\n");

			$cls->generateDBCreate($bestand);
		}

		$bestand->write(true);
	}
}