<?php

namespace Generator\Codes;

use Generator\Codes\KlasseCode;

final class BestandCode implements Stringable
{
	private $namespace;
	private $klassen;

	public function __construct($namespace = null)
	{
		$this->namespace = $namespace;
		$klassen = [];
	}

	public function addKlasse(KlasseCode $klasse)
	{
		if(sizeof($this->klassen) > 0) {
			trigger_error("Het wordt afgeraden om meer dan 1 klasse per bestand te doen", E_WARNING);
		}

		$this->klassen[] = $klasse;
	}

	public function __toString()
	{
		$str = "<?\n"
			.	"\n";

		if($this->namespace) {
			$str .= "namespace $this->namespace;\n"
			.	"\n";
		}

		$str .= implode("\n", array_map(function($cls) { return $cls->__toString(); }, $this->klassen));

		return $str;
	}
}
