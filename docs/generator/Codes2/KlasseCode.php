<?php

namespace Generator\Codes;

use Generator\Codes\Blok;

final class KlasseCode implements Stringable, BlokGebruiker
{
	private $naam;
	private $abstract;
	private $final;
	private $blok;
	private $extend;
	private $implements;

	public function __construct($naam)
	{
		$this->naam = $naam;
		$this->abstract = false;
		$this->final = false;
		$this->blok = new Blok(true);
		$this->extend = false;
		$this->implements = [];
	}

	public function blok()
	{
		return $this->blok;
	}

	public function addContent($content)
	{
		$this->blok()->addContent($content);
		return $this;
	}

	public function setAbstract()
	{
		if($this->final) {
			throw new \Exception('Final klasse mag niet abstract gemaakt worden');
		}

		$this->abstract = true;
		return $this;
	}

	public function setFinal()
	{
		if($this->abstract) {
			throw new \Exception('Abstract klasse mag niet final gemaakt worden');
		}

		$this->final = true;
		return $this;
	}

	public function extendVan($extendKlasse)
	{
		$this->extend = $extendKlasse;
		return $this;
	}

	public function implementeerd($interface)
	{
		$this->implements[] = $interface;
	}

	public function __toString()
	{
		$str = "";

		if($this->abstract) {
			$str .= "abstract ";
		}

		if($this->final) {
			$str .= "final ";
		}

		$str .= "class $this->naam" . PHP_EOL;

		if($this->extend) {
			$str .= "\textends {$this->extend}" . PHP_EOL;
		}

		foreach($this->implements as $i) {
			$str .= "\timplements $i" . PHP_EOL;
		}

		$str .= $this->blok->__toString();

		return $str;
	}
}
