<?php

namespace Generator\Codes;

final class FunctieCode implements Stringable, BlokGebruiker
{
	private $parameters;
	private $blok;
	private $naam;
	private $public;
	private $protected;
	private $private;
	private $static;
	private $abstract;
	private $final;

	public function __construct($naam)
	{
		$this->naam = $naam;
		$this->parameters = [];
		$this->blok = new Blok(true);
		$this->public = false;
		$this->protected = false;
		$this->private = false;
		$this->static = false;
		$this->abstract = false;
		$this->final = false;
	}

	public function setPublic()
	{
		if($this->private || $this->protected) {
			throw new \Exception("Private/protected methode mag niet public gemaakt worden");
		}

		$this->public = true;
		return $this;
	}

	public function setPrivate()
	{
		if($this->public || $this->protected) {
			throw new \Exception("Public/protected methode mag niet private gemaakt worden");
		}

		$this->private = true;
		return $this;
	}

	public function setProtected()
	{
		if($this->public || $this->private) {
			throw new \Exception("Public/private methode mag niet protected gemaakt worden");
		}

		$this->protected = true;
		return $this;
	}

	public function setStatic()
	{
		$this->static = true;
		return $this;
	}

	public function setFinal()
	{
		if($this->abstract) {
			throw new \Exception("Abstract methode mag niet final gemaakt worden");
		}

		$this->final = true;
		return $this;
	}

	public function setAbstract()
	{
		if($this->final) {
			throw new \Exception("Final methode mag niet abstract gemaakt worden");
		}

		$this->abstract = true;
		return $this;
	}

	public function blok()
	{
		return $this->blok;
	}

	public function addContent($content)
	{
		$this->blok->addContent($content);
		return $this;
	}

	public function addParameter(ParameterCode $parameter)
	{
		$this->parameters[] = $parameter;
		return $this;
	}

	public function __toString()
	{
		$str = "";

		if($this->abstract) {
			$str .= "abstract ";
		}

		if($this->final) {
			$str .= "final ";
		}

		if($this->public) {
			$str .= "public ";
		}

		if($this->protected) {
			$str .= "protected ";
		}

		if($this->private) {
			$str .= "private ";
		}

		if(!$this->private && !$this->protected && !$this->public) {
			$str .= "public ";
		}

		if($this->static) {
			$str .= "static ";
		}

		$str .= "function {$this->naam}(";

		$parameters = array_map(function($parameter) {
			return $parameter->__toString();
		}, $this->parameters);
		$str .= implode(",\n\t\t", $parameters);

		$str .= ")\n";

		$str .= $this->blok->__toString();

		return $str;
	}
}
