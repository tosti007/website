<?php

namespace Generator\Codes;

final class IfCode implements Stringable, BlokGebruiker
{
	private $conditie;
	private $blok;
	private $elses;

	public function __construct($conditie)
	{
		$this->conditie = $conditie;
		$this->blok = new Blok(false);
		$this->elses = [];
	}

	public function blok()
	{
		return $this->blok;
	}

	public function addContent($content)
	{
		$this->blok->addContent($content);
		return $this;
	}

	public function addElse(ElseCode $else)
	{
		$this->elses[] = $else;
		return $this;
	}

	public function __toString()
	{
		$str = "if({$this->conditie}) "
			. $this->blok->__toString();

		foreach($this->elses as $else) {
			$str .= " {$else->__toString()}";
		}

		$str .= PHP_EOL;

		return $str;
	}
}
