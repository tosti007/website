<?php

namespace Generator\Codes;

final class Blok implements Stringable
{
	private $content;
	private $newlineAtEnd;

	public function __construct($newlineAtEnd = true)
	{
		$this->content = [];
		$this->newlineAtEnd = $newlineAtEnd;
	}

	public function addContent($content)
	{
		$this->content[] = $content;
	}

	public function __toString()
	{
		$contents = array_map(function($content) {
			if($content instanceof Stringable) {
				$content = $content->__toString();
			}

			/*$content = explode("\n", $content);

			foreach($content as $i => $cont) {
				if(ctype_space($cont) || empty($cont)) {
					$content[$i] = "";
					continue;
				}

				$content[$i] = "\t" . $cont;
			}
			return implode("\n", $content);*/
			return preg_replace('/^/m', "\t", $content);
		}, $this->content);
		$str = "{" . PHP_EOL
			.	implode("", $contents)
			.	"}";

		if($this->newlineAtEnd) {
			$str .= PHP_EOL;
		}

		return $str;
	}
}
