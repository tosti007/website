<?php

namespace Generator\Codes;

final class TryCode implements Stringable, BlokGebruiker
{
	private $blok;

	public function __construct()
	{
		$this->blok = new Blok(false);
	}

	public function blok()
	{
		return $this->blok;
	}

	public function addContent($content)
	{
		$this->blok->addContent($content);
		return $this;
	}

	public function __toString()
	{
		return "try " . $this->blok->__toString();
	}
}
