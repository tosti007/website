<?php

namespace Generator\Codes;

final class PropertyCode implements Stringable
{
	private $naam;
	private $comment;
	private $defaultWaarde;
	private $static;
	private $private;
	private $protected;
	private $public;

	public function __construct($naam)
	{
		$this->naam = $naam;
		$this->comment = null;
		$this->defaultWaarde = null;
		$this->static = false;
		$this->private = false;
		$this->protected = false;
		$this->public = false;
	}

	public function setComment($comment)
	{
		$this->comment = $comment;
		return $this;
	}

	public function setDefaultWaarde($waarde)
	{
		$this->defaultWaarde = $waarde;
		return $this;
	}

	public function setPublic()
	{
		if($this->private || $this->protected) {
			throw new \Exception("Private/protected property mag niet public gemaakt worden");
		}

		$this->public = true;
		return $this;
	}

	public function setPrivate()
	{
		if($this->public || $this->protected) {
			throw new \Exception("Public/protected property mag niet private gemaakt worden");
		}

		$this->private = true;
		return $this;
	}

	public function setProtected()
	{
		if($this->public || $this->private) {
			throw new \Exception("Public/private property mag niet protected gemaakt worden");
		}

		$this->protected = true;
		return $this;
	}

	public function setZichtbaarheid($zichtbaarheid)
	{
		switch($zichtbaarheid) {
		case 'private':
			$propertyCode->setPrivate();
			break;
		case 'protected':
			$propertyCode->setProtected();
			break;
		default:
		case 'public':
			$propertyCode->setPublic();
			break;
		}

		return $this;
	}

	public function setStatic()
	{
		$this->static = true;
		return $this;
	}

	public function __toString()
	{
		$str = "";

		if($this->public) {
			$str .= "public ";
		}

		if($this->protected) {
			$str .= "protected ";
		}

		if($this->private) {
			$str .= "private ";
		}

		if(!$this->private && !$this->protected && !$this->public) {
			$str .= "public ";
		}

		if($this->static) {
			$str .= "static ";
		}

		$str .= "\${$this->naam}";

		if($this->static && $this->defaultWaarde) {
			$str .= " = {$this->defaultWaarde}";
		}

		$str .= ";";

		if($this->comment) {
			$tabsAmount = max(10 - (strlen("\t{$str}") / 4), 0);
			$str .= str_repeat("\t", $tabsAmount);
			$str .= "/**{$this->comment}*/";
		}

		$str .= "\n";

		return $str;
	}
}
