<?php

namespace Generator\Codes;

final class TryCatchCode implements Stringable
{
	private $try;
	private $catch;

	public function __construct(TryCode $try, CatchCode $catch)
	{
		$this->try = $try;
		$this->catch = $catch;
	}

	public function __toString()
	{
		return $this->try->__toString() . " " . $this->catch->__toString();
	}
}
