<?
/**
 * Dit bestand wordt door `dia2php` gekopieerd vanuit
 * generator/templates/init.php
 *
 * Het doel is om vanuit hier alle objectbestanden in te laden en gelijk te
 * timen hoelang PHP daar over doet
 */

$tm = new ProfilerTimerMark('WSW4/includes-generated.php');
Profiler::getSingleton()->addTimerMark($tm);
require_once('WhosWho4/includes-generated.php');
$tm->markEnd();

$tm = new ProfilerTimerMark('WSW4/includes-nongenerated.php');
Profiler::getSingleton()->addTimerMark($tm);
require_once('WhosWho4/includes-nongenerated.php');
$tm->markEnd();
