<?php

final class ParameterCode implements Stringable
{
	private $naam;
	private $defaultWaarde;

	public function __construct($naam, $defaultWaarde = null)
	{
		$this->naam = $naam;
		$this->defaultWaarde = $defaultWaarde;
	}

	public function __toString()
	{
		$parameterStr = "\${$this->naam}";

		if($this->defaultWaarde) {
			$parameterStr .= " = {$this->defaultWaarde}";
		}

		return $parameterStr;
	}
}
