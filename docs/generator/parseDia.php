<?php

namespace Generator;

use Generator\WSW4objects\WSW4class;
use Generator\WSW4objects\WSW4method;
use Generator\WSW4objects\WSW4package;
use Generator\WSW4objects\WSW4param;
use Generator\WSW4objects\WSW4relation;
use Generator\WSW4objects\WSW4var;

function parseObjects($xml)
{
	// Laad de xml-structuur van de dia
	$objs = $xml->getElementsByTagName('object');

	// Dit is de structuur van wat we terug sturen,
	// is belangrijk!
	$objects				= array();
	$objects['id']			= array();
	$objects['class']		= array();
	$objects['implements']	= array();
	$objects['inherits']	= array();
	$objects['package']		= array();

	// De hoofdroot
	$root = new WSW4package('_ROOT');
	$root->setName('WhosWho4');
	$objects['id']['_ROOT']      = $root;
	$objects['package']['WhosWho4'] = $root;

	// Lus door het xml bestand om alle objecten te vinden
	foreach($objs as $obj)
	{
		switch($obj->getAttribute('type')) {
		case 'UML - Class':
			$c = parseClass($obj);
			$objects['class'][$c->name] = $c;
			$objects['id'][$c->id] = $c;
			break;
		case 'UML - LargePackage':
			$p = parsePackage($obj);
			$objects['package'][$p->name] = $p;
			$objects['id'][$p->id] = $p;
			break;
		}
	}

	// Ga alle relaties na, dit moet nadat alle klassen gemaakt zijn zodat relations die kunnen gebruiken.
	foreach($objs as $obj)
	{
		switch($obj->getAttribute('type')) {
			case 'UML - Generalization':
				$relation = parseRelation($objects, $obj);
				$relation->check($objects);
				$c = $relation->child();
				$p = $relation->parent();
				$c->inherits = $p;
				break;
			case 'UML - Realizes':
				$relation = parseRelation($objects, $obj);
				$relation->check($objects);
				$p = $relation->parent();
				$c = $relation->child();
				$c->implements[] = $p;
				break;
		}
	}

	// Controleer of alle objecten geldig zijn
	foreach($objects['class'] as $cls)
		$cls->check($objects);
	foreach($objects['package'] as $pkg)
		$pkg->check($objects);

	return $objects;
}


function parseClass($obj)
{
	$c = new WSW4class($obj->getAttribute('id'));
	$c->setName(xmlAttrString($obj, 'name'));
	$c->setPackageId(xmlChildNode($obj));
	$c->setComment(xmlAttrString($obj, 'comment'));
	$c->setAbstract(xmlAttrBool($obj, 'abstract'));

	$c->core = $c;

	$comps = $obj->getElementsByTagName('composite');
	foreach($comps as $comp) {
		switch($comp->getAttribute('type')) {
		case 'umlattribute':
			$c->addVar(parseClassVar($comp));
			break;
		case 'umloperation':
			$c->addMethod(parseClassMethods($comp));
			break;
		}
	}
	return $c;
}
function parseClassVar($var)
{
	$v = new WSW4var(xmlAttrString($var, 'name'));
	$v->setType(xmlAttrString($var, 'type'));
	$v->setComment(xmlAttrString($var, 'comment'));
	$v->setDefault(xmlAttrString($var, 'value'));		// moet na Type/Comment
	$v->setStatic(xmlAttrBool($var, 'class_scope'));
	$v->setVisibility(xmlAttrVis($var));
	return $v;
}
function parseClassMethods($obj)
{
	$m = new WSW4method(xmlAttrString($obj, 'name'));
	$m->setComment(xmlAttrString($obj, 'comment'));
	$m->setAbstract(xmlAttrBool($obj, 'abstract'));
	$m->setStatic(xmlAttrBool($obj, 'class_scope'));
	$m->setVisibility(xmlAttrVis($obj));
	$m->setVirtual(xmlAttrVirtual($obj));

	$params = $obj->getElementsByTagName('composite');
	foreach($params as $param) {
		if($param->getAttribute('type') != 'umlparameter')
			continue;

#		$p['type']		= xmlAttrString($param, 'type');
#		$p['comment']	= xmlAttrString($param, 'comment');

		$p = new WSW4param(xmlAttrString($param, 'name'), xmlAttrString($param, 'value'));
		$m->addParam($p);
	}

	return $m;
}

/**
 * @param array $objects De lijst van tot nu toe aangemaakte WSW4 classes en packages.
 * @param mixed $obj Het XML object om de data van de relatie uit te lezen.
 * @return WSW4relation
 */
function parseRelation($objects, $obj)
{
	$name = xmlAttrString($obj, 'name');

	$cs = $obj->getElementsByTagName('connection');
	$parentId = null;
	$childId = null;
	foreach($cs as $c) {
		if($c->getAttribute('handle') == 0) {
			$parentId = $c->getAttribute('to');
		} else {
			$childId = $c->getAttribute('to');
		}
	}

	assert(!is_null($parentId));
	assert(!is_null($childId));

	$parent = $objects['id'][$parentId];
	$child = $objects['id'][$childId];

	return new WSW4relation($name, $parent, $child);
}
function parsePackage($obj)
{
	$p = new WSW4package($obj->getAttribute('id'));
	$p->setName(xmlAttrString($obj, 'name'));
	$p->setParentId(xmlChildNode($obj));
	return $p;
}

/** helper functies voor het verwerken van XML elementen **/
function xmlAttrBool($obj, $name)
{
	$childs = $obj->getElementsByTagName('attribute');
	foreach($childs as $child) {
		if($child->getAttribute('name') != $name)
			continue;
		$subchilds = $child->getElementsByTagName('boolean');
		foreach($subchilds as $subchild)
			return ($subchild->getAttribute('val') == 'true');
	}

}
function xmlAttrEnum($obj, $name)
{
	$childs = $obj->getElementsByTagName('attribute');
	foreach($childs as $child) {
		if($child->getAttribute('name') != $name)
			continue;
		$subchilds = $child->getElementsByTagName('enum');
		foreach($subchilds as $subchild)
			return (int)$subchild->getAttribute('val');
	}

}
function xmlAttrString($obj, $name)
{
	$childs = $obj->getElementsByTagName('attribute');
	foreach($childs as $child) {
		if($child->getAttribute('name') != $name)
			continue;
		$subchilds = $child->getElementsByTagName('string');
		foreach($subchilds as $subchild)
			return substr($subchild->nodeValue, 1, -1);
	}
}
function xmlAttrVis($obj)
{
	switch(xmlAttrEnum($obj, 'visibility')) {
	case 0:	return 'public';
	case 1:	return 'private';
	case 2:	return 'protected';
	case 3:	return 'implementation';
	}
}
function xmlAttrVirtual($obj)
{
	switch(xmlAttrEnum($obj, 'inheritance_type')) {
	case 1:		return True;
	default:	return False;
	}
}
function xmlChildNode($obj)
{
	$cns = $obj->getElementsByTagName('childnode');
	foreach($cns as $cn)
		return $cn->getAttribute('parent');
	return null;
}
