<?php

final class CodeBlok implements Stringable
{
	private $content;

	public function __construct()
	{
		$this->content = [];
	}

	public function addContent($content)
	{
		$this->content[] = $content;
	}

	public function __toString()
	{
		$contents = array_map(function($content) {
			return preg_replace('/^/m', "\t", $content);
		}, $this->content);
		return "{" . PHP_EOL
			.	implode("", $contents)
			.	"}" . PHP_EOL;
	}
}
