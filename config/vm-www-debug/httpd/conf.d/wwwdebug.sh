#!/bin/sh

exec > $1

echo "# Generated by wwwdebug.sh `date`"
echo
echo "NameVirtualHost *:80"
echo

while read login vhost root; do
	sed "s/LOGIN/$login/g;s/VHOST/$vhost/g" <<-CONFIG
	# DEBUG config voor LOGIN op VHOST-debug.a-eskwadraat.nl
	CONFIG

	# Ja, we mixen tabs en spaces, maar tabs verdwijnen gewoon
	sed "s/LOGIN/$login/g;s/VHOST/$vhost/g;s|\\(/\\)\\?ROOT|$root|g" <<-CONFIG
	<VirtualHost *:80>
	ServerAdmin LOGIN@A-Eskwadraat.nl
	DocumentRoot ROOT/LOGIN/wwwdebug/www
	ServerName  VHOST-debug.A-Eskwadraat.nl
	<Directory "ROOT/LOGIN/wwwdebug/www">
	    AuthType Basic
	    AuthName "Deze debug-pagina is alleen voor de WebCie"
	    AuthUserFile /etc/httpd/conf.d/htpasswd
	    AuthGroupFile /etc/httpd/conf.d/htgroups
	    Require group www
	    AllowOverride All
	    Options Indexes FollowSymLinks -ExecCGI
	    Order Allow,Deny
	    Allow from all
	    <IfModule mod_authz_core.c>
	        # Apache 2.4
	        Require all granted
	    </IfModule>
	    <IfModule !mod_authz_core.c>
	        # Apache 2.2
	        Order Deny,Allow
	        Allow from All
	    </IfModule>
	</Directory>

	<Location /space/auth/systeem.php>
	    InterceptFormPAMService httpd_systeemlogin
	    InterceptFormLogin loginnaam
	    InterceptFormPassword password
	    InterceptFormPasswordRedact on
	    LookupUserAttr memberId REMOTE_USER_MEMBERID
	</Location>

	ProxyPass /javascript !
	ProxyPass /Layout !
	ProxyPass /Minified !
	ProxyPassMatch ^/(.*?)$ fcgi://127.0.0.1:9000/ROOT/LOGIN/wwwdebug/www/space/space.php\$0

	php_value session.cookie_domain "VHOST-debug.a-eskwadraat.nl"
	Header always set X-Frame-Options SAMEORIGIN

	AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript
	BrowserMatch ^Mozilla/4 gzip-only-text/html
	BrowserMatch ^Mozilla/4\.0[678] no-gzip
	BrowserMatch \bMSIE !no-gzip !gzip-only-text/html

	CustomLog /var/log/httpd/debug-VHOST-access.log combined
	</VirtualHost>
	CONFIG

	sed "s/LOGIN/$login/g;s/VHOST/$vhost/g;s|\\(/\\)\\?ROOT|$root|g" <<-CONFIG
	# Documentatie voor de debugsite
	<VirtualHost *:80>
	    ServerAdmin LOGIN@A-Eskwadraat.nl
	    DocumentRoot ROOT/LOGIN/wwwdebug/docs-generated
	    ServerName VHOST-docs-debug.A-Eskwadraat.nl
	    ServerAlias VHOST-docs-debug.A-Eskwadraat.nl

	    CustomLog /var/log/httpd/debug-VHOST-access.log combined
	</VirtualHost>
CONFIG

	sed "s/LOGIN/$login/g;s/VHOST/$vhost/g" <<-CONFIG
	# EIND LOGIN
CONFIG

done

echo
echo "# vim: syntax=apache"




# Extra regels zodat vi niet de vimregel hierboven leest bij het editen van
# wwwdebug.sh
