package nl.a_eskwadraat.a_appkwadraatnl;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.view.WindowManager;
import android.view.Window;
import android.content.Intent;
import android.net.Uri;
import android.view.KeyEvent;
import android.webkit.CookieManager;
import android.app.DownloadManager
import android.webkit.DownloadListener

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private WebView mWebView;

    @Override
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    final AppCompatActivity activity = this;

    private boolean isSplashOn = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(nl.a_eskwadraat.a_appkwadraatnl.R.layout.activity_main);

        if (savedInstanceState != null)
            ((WebView)findViewById(nl.a_eskwadraat.a_appkwadraatnl.R.id.activity_main_webview)).restoreState(savedInstanceState);

        mWebView = (WebView) findViewById(nl.a_eskwadraat.a_appkwadraatnl.R.id.activity_main_webview);

        CookieManager.getInstance().acceptCookie();
        CookieManager.getInstance().setAcceptCookie(true);

        mWebView.setBackgroundColor(0);
        mWebView.setBackgroundResource(nl.a_eskwadraat.a_appkwadraatnl.R.drawable.splash);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setAllowFileAccess(true);

		mWebView.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
				DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

				request.setMimeType(mimeType);

				String cookies = CookieManager.getInstance().getCookie(url);
				request.addRequestHeader("cookie", cookies);

				request.addRequestHeader("User-Agent", userAgent);
				request.setDescription("Downloading file...");
				request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
				request.allowScanningByMediaScanner();
				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
				request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
				DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
				dm.enqueue(request);
			}
		});

        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.setScrollbarFadingEnabled(false);

        mWebView.setWebViewClient(new MyWebClient());
        if (savedInstanceState == null) {
            mWebView.loadUrl("https://www.a-eskwadraat.nl/Home");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }

    public class MyWebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            if(url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);

                return true;
            }

            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if(!isSplashOn) {
                mWebView.setBackgroundDrawable(null);
                mWebView.setBackgroundColor(0);

                isSplashOn = true;
            }

            super.onPageFinished(view, url);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if(mWebView.canGoBack()) {
                mWebView.goBack();

                return true;
            }else {
                activity.finish();
            }
        }

        return super.onKeyDown(keyCode, event);
    }
}
