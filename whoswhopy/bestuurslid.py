"""whoswhopy.bestuurslid - Definieer hoe bestuursleden geconfigureerd staan."""

class BestuursLid:
    """Klasse om configuratiedata over bestuursleden op te slaan.

    Nadat de database beschikbaar is, kunnen we hier Lid-objecten van maken.
    """

    def __init__(self, contactid, functie, email_prefix, boeken=False):
        self.contactid = contactid
        self.functie = functie
        self.email_prefix = email_prefix
        self.boeken = boeken

    @property
    def emailadres(self):
        """Geef het volledige e-mailadres van dit bestuurslid.

        Zie ook fancy_email als je het mooi wilt formatteren.
        """
        return "{}@a-eskwadraat.nl".format(self.email_prefix)

    @property
    def fancy_email(self):
        """Geef een afzender/ontvanger van e-mails. Bevat ook functienaam.

        Zie ook emailadres als je alleen het adres wilt.
        """
        # Merk op dat we geen half kastlijntje gebruiken,
        # omdat veel e-mailproviders die niet lusten.
        return "{} A-Eskwadraat <{}>".format(self.functie, self.emailadres)
