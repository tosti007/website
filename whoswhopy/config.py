"""whoswhopy.config - Functies om de configuratie uit te lezen.

Bij het importeren van dit bestand wordt de configuratie al ingelezen,
dus we ondersteunen (nog) niet dat configuratie wordt aangepast
terwijl een proces al runt.
Inlezen kan ook een exceptie gooien als het configuratiebestand foute syntax heeft.

Deze module bevat functies om specifieke configwaarden uit te lezen.
Je kan voor generieke stringwaardige configwaarden de functie geef gebruiken,
of geef_verplicht als je geen defaultwaarde accepteert.
Voer je iets van parsen uit op het resultaat,
dan is het een mooi idee om in deze module een functie te maken die dat doet,
in plaats van op de plek dat je de geconfigureerde waarde nodig hebt.
"""

from camel import Camel, CamelRegistry
import configparser
import re

from whoswhopy.bestuurslid import BestuursLid
from whoswhopy.webcielid import WebCieLid

class ConfigOngeldigError(Exception):
    """Wordt gegooid als de syntax/inhoud van de configuratie niet klopt."""
    pass

def geef(naam, default=None):
    """Geef de configuratiestring met gegeven naam.

    Voer je iets van parsen uit op het resultaat,
    dan is het een mooi idee om in deze module een functie te maken die dat doet,
    in plaats van op de plek dat je de geconfigureerde waarde nodig hebt.

    Indien er niets is met de gegeven naam, geven we de default.

    Zie ook geef_verplicht als de waarde moet bestaan.
    """
    # Lieve toekomstige maintainer:
    # Ga je de inhoud hier aanpassen, let dan op dat geef_verplicht hetzelfde doet.
    # (Denk dus na of je misschien de een de ander wil laten aanroepen,
    # in plaats van codedupliatie.)
    return _config.get(naam, default)

def geef_verplicht(naam):
    """Geef de configuratiestring met gegeven naam.

    Voer je iets van parsen uit op het resultaat,
    dan is het een mooi idee om in deze module een functie te maken die dat doet,
    in plaats van op de plek dat je de geconfigureerde waarde nodig hebt.

    Indien er niets is met de gegeven naam, gooien we een ConfigOngeldigError.

    Zie ook geef als je een defaultwaarde wilt returnen.
    """
    # Lieve toekomstige maintainer:
    # Ga je de inhoud hier aanpassen, let dan op dat geef hetzelfde doet.
    # (Denk dus na of je misschien de een de ander wil laten aanroepen,
    # in plaats van codedupliatie.)
    try:
        return _config[naam]
    except KeyError:
        raise ConfigOngeldigError("Geconfigureerde waarde voor `{}' ontbreekt".format(naam))

def _geef_bool(naam):
    """Geef de configuratiewaarde met gegeven naam als een bool.

    Returnwaarde is een boolean.
    Gooit een exceptie als de geconfigureerde waarde niet 'true' of 'false' is.
    """
    waarde_str = geef(naam)
    if waarde_str == 'true':
        return True
    elif waarde_str == 'false':
        return False
    else:
        raise ConfigOngeldigError("Geconfigureerde waarde voor `{}' moet 'true' of 'false' zijn, niet {}".format(naam, waarde_str))

def debug():
    """Geef of we in debugmodus zitten.

    Returnwaarde is een boolean.
    Gooit een exceptie als de geconfigureerde waarde niet 'true' of 'false' is.
    """
    return _geef_bool('debug')

def site_wide_salt():
    """Geef de sitewide salt.

    Dit is een magische bytestring die we (naast een specifieke salt)
    bij elke wachtwoordhash stoppen.
    """
    return geef_verplicht('SITE_WIDE_SALT').encode('ascii')

# Registreer de loaders om configuratie uit YAML te parsen.
config_registry = CamelRegistry()

@config_registry.loader('bestuurslid', version=1)
def _load_bestuurslid(data, version):
    return BestuursLid(data['contactid'], data['functie'], data['email'], data.get('boeken', False))

@config_registry.loader('webcielid', version=1)
def _load_webcielid(data, version):
    return WebCieLid(data['contactid'], data['login'], data['debugsite'], data['mountpoint'])

def _lees_config():
    """Lees configuratie uit verscheidene configuratiebestanden.

    Deze functie wordt aangeroepen bij het importeren van de module.
    We gaan er niet van uit dat het nog een andere keer aangeroepen wordt.

    Resultaat is een dict van de vorm {naam: waarde}
    """

    # Initialiseer de configuratie met processpecifieke waarden.
    config = {}

    # Helaas staat de meeste configuratie in bash, dus we moeten het zelf parsen.
    # TODO: dit gaat uit de environmentvariabelen gelezen worden.
    with open('www/install.config', 'r') as config_bestand:
        for regel in config_bestand:
            if regel[0] == '#':
                continue
            delen = regel[:-1].split('=', 1) # split op 1 keer '=' en gooi de '\n' weg
            if len(delen) != 2:
                raise ConfigOngeldigError("Configuratiebestand heeft foute syntax:\n`{}'".format(regel))
            config[delen[0]] = delen[1]
    # Er staan ook nog wat geheime dingen in een php-bestandje, parse die ook.
    define_regex = re.compile(r'define\("([^"]+)",\s*"([^"]+)"\);')
    with open('www/secret.php', 'r') as config_bestand:
        for regel in config_bestand:
            match = define_regex.match(regel)
            if match is None:
                continue
            config[match.group(1)] = match.group(2)
    # Lees de mooie configuratie in, over de lelijke config heen.
    config_bestand = configparser.ConfigParser()
    config_bestand.read('config/active.ini')
    config.update(config_bestand['whoswhopy'])

    # Dit is daadwerkelijk configuratie die we willen inlezen,
    # omdat het niet kan via env-vars of het Register.
    config_lezer = Camel([config_registry])
    # We kunnen dit pad eventueel ook uit een environmentvariabele uitlezen.
    with open('config.yaml') as config_yaml:
        config.update(config_lezer.load(config_yaml.read()))

    return config

# Zorg ervoor dat de configuratie is ingelezen.
_config = _lees_config()
