"""whoswhopy.webcielid - Definieer hoe leden der WebCie geconfigureerd staan."""

class WebCieLid:
    """Klasse om configuratiedata over webcieleden op te slaan."""

    def __init__(self, contactid, login, debugsite, mountpoint):
        self.contactid = contactid
        self.login = login
        self.debugsite = debugsite
        self.mountpoint = mountpoint
