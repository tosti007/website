"""whoswhopy.wachtwoord - Functies om wachtwoorden en salten enzo af te handelen.

De implementatie zou compatibel moeten zijn met BCryptHandler in PHP.
"""

import bcrypt

import whoswhopy.config as config

# Het aantal rondes.
# Zou vrij instelbaar moeten zijn.
_rounds = 12

def genereer(tekst_klaar, salt=None):
    """Geef de gehashte+gesalte versie van de gegeven klare tekst.

    Deze functie zou cryptografisch veilige salts moeten gebruiken.

    De salt wordt voor de klare tekst geplakt bij hashen.
    Defaultwaarde van de salt is config.side_wide_salt().
    (Let op dat je hetzelfde meegeeft bij verifieer.)

    Argumenten moeten bytes of str zijn.
    Ze mogen niet '\x00' (NUL) bevatten.
    Indien ze str zijn, worden ze in utf-8 geëncodeerd.
    """
    if salt is None:
        salt = config.site_wide_salt()

    if isinstance(tekst_klaar, str):
        tekst_klaar = tekst_klaar.encode('utf-8')
    if isinstance(salt, str):
        salt = salt.encode('utf-8')

    return bcrypt.hashpw(
        salt + tekst_klaar,
        bcrypt.gensalt(rounds=_rounds)
    )

def verifieer(tekst_klaar, tekst_hash, salt=None):
    """Controleer dat de klare tekst overeenkomt met de gegeven hash.

    Deze functie zou cryptografisch veilig moeten zijn.

    De salt wordt voor de klare tekst geplakt bij hashen.
    Defaultwaarde van de salt is config.side_wide_salt().
    (Let op dat je hetzelfde meegeeft bij verifieer.)

    Argumenten moeten bytes of str zijn.
    Ze mogen niet '\x00' (NUL) bevatten.
    Indien ze str zijn, worden ze in utf-8 geëncodeerd.
    """
    if salt is None:
        salt = config.site_wide_salt()

    if isinstance(tekst_klaar, str):
        tekst_klaar = tekst_klaar.encode('utf-8')
    if isinstance(tekst_hash, str):
        tekst_hash = tekst_hash.encode('utf-8')
    if isinstance(salt, str):
        salt = salt.encode('utf-8')

    return bcrypt.checkpw(
        salt + tekst_klaar,
        tekst_hash
    )
