from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
from whoswhopy.gen.Persoon import Persoon
class CommissieLid(Entiteit):
    __tablename__ = "CommissieLid"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    commissie_commissieID = Column(Integer, primary_key=True, nullable=False)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    datumBegin = Column(Date, nullable=True)
    datumEind = Column(Date, nullable=True)
    spreuk = Column(String(255), nullable=True)
    spreukZichtbaar = Column(Boolean, nullable=False)
    functie = Column(Enum("OVERIG", "VOORZ", "SECR", "PENNY", "PR", "SPONS", "BESTUUR", "SFEER"), nullable=False)
    functieNaam = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "CommissieLid"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="CommissieLid_Persoon"), ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="CommissieLid_Commissie"),)
