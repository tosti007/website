from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class CommissieCategorie(Entiteit):
    __tablename__ = "CommissieCategorie"
    categorieID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "CommissieCategorie"}
