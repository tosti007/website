from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class TinyUrl(Entiteit):
    __tablename__ = "TinyUrl"
    id = Column(Integer, primary_key=True, nullable=False)
    tinyUrl = Column(String(255), nullable=False)
    outgoingUrl = Column(String(511), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "TinyUrl"}
