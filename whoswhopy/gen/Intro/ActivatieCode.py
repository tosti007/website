from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid
class ActivatieCode(Entiteit):
    __tablename__ = "ActivatieCode"
    lid_contactID = Column(Integer, primary_key=True, nullable=False)
    lid = relationship("Lid", foreign_keys=[lid_contactID])
    code = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ActivatieCode"}
    __table_args__ = (ForeignKeyConstraint(["lid_contactID"], ["Lid.contactID"], name="ActivatieCode_Lid"),)
