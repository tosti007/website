from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.iDeal import iDeal
from whoswhopy.gen.Lid import Lid
class LidmaatschapsBetaling(Entiteit):
    __tablename__ = "LidmaatschapsBetaling"
    lid_contactID = Column(Integer, primary_key=True, nullable=False)
    lid = relationship("Lid", foreign_keys=[lid_contactID])
    soort = Column(Enum("BOEKVERKOOP", "MACHTIGING", "IDEAL"), nullable=False)
    rekeningnummer = Column(String(255), nullable=True)
    rekeninghouder = Column(String(255), nullable=True)
    ideal_transactieID = Column(Integer, nullable=True)
    ideal = relationship("iDeal", foreign_keys=[ideal_transactieID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "LidmaatschapsBetaling"}
    __table_args__ = (ForeignKeyConstraint(["lid_contactID"], ["Lid.contactID"], name="LidmaatschapsBetaling_Lid"), ForeignKeyConstraint(["ideal_transactieID"], ["iDeal.transactieID"], name="LidmaatschapsBetaling_Ideal"),)
