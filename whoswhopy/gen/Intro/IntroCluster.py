from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class IntroCluster(Entiteit):
    __tablename__ = "IntroCluster"
    clusterID = Column(Integer, primary_key=True, nullable=False)
    jaar = Column(Integer, nullable=False)
    naam = Column(String(255), nullable=False)
    persoon_contactID = Column(Integer, nullable=True)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "IntroCluster"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="IntroCluster_Persoon"),)
