from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid
class IntroDeelnemer(Entiteit):
    __tablename__ = "IntroDeelnemer"
    lid_contactID = Column(Integer, primary_key=True, nullable=False)
    lid = relationship("Lid", foreign_keys=[lid_contactID])
    betaald = Column(Enum("NEE", "BANK", "PIN", "KAS", "IDEAL", "ONBEKEND"), nullable=False)
    kamp = Column(Boolean, nullable=False)
    vega = Column(Boolean, nullable=False)
    allergie = Column(Boolean, nullable=False)
    aanwezig = Column(Boolean, nullable=False)
    allergieen = Column(Text, nullable=True)
    introOpmerkingen = Column(Text, nullable=True)
    voedselBeperking = Column(Text, nullable=True)
    magischeCode = Column(Text, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "IntroDeelnemer"}
    __table_args__ = (ForeignKeyConstraint(["lid_contactID"], ["Lid.contactID"], name="IntroDeelnemer_Lid"),)
