from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Intro.IntroGroep import IntroGroep
from whoswhopy.gen.Lid import Lid
class Mentor(Entiteit):
    __tablename__ = "Mentor"
    lid_contactID = Column(Integer, primary_key=True, nullable=False)
    lid = relationship("Lid", foreign_keys=[lid_contactID])
    groep_groepID = Column(Integer, primary_key=True, nullable=False)
    groep = relationship("IntroGroep", foreign_keys=[groep_groepID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Mentor"}
    __table_args__ = (ForeignKeyConstraint(["lid_contactID"], ["Lid.contactID"], name="Mentor_Lid"), ForeignKeyConstraint(["groep_groepID"], ["IntroGroep.groepID"], name="Mentor_Groep"),)
