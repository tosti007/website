from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Intro.IntroCluster import IntroCluster
class IntroGroep(Entiteit):
    __tablename__ = "IntroGroep"
    groepID = Column(Integer, primary_key=True, nullable=False)
    cluster_clusterID = Column(Integer, nullable=True)
    cluster = relationship("IntroCluster", foreign_keys=[cluster_clusterID])
    naam = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "IntroGroep"}
    __table_args__ = (ForeignKeyConstraint(["cluster_clusterID"], ["IntroCluster.clusterID"], name="IntroGroep_Cluster"),)
