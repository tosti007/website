from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from whoswhopy.gen.Boekweb2.Artikel import Artikel
class DibsProduct(Artikel):
    __tablename__ = "DibsProduct"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    kudos = Column(Integer, nullable=False)
    __mapper_args__ = {'polymorphic_identity': "DibsProduct", 'inherit_condition': artikelID == Artikel.artikelID}
    __table_args__ = (ForeignKeyConstraint(["artikelID"], ["Artikel.artikelID"], name="DibsProduct_ArtikelID"),)
