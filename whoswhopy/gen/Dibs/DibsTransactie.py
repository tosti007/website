from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Transactie import Transactie
from whoswhopy.gen.Dibs.Pas import Pas
class DibsTransactie(Transactie):
    __tablename__ = "DibsTransactie"
    transactieID = Column(Integer, primary_key=True, nullable=False)
    bron = Column(Enum("WWW", "TABLET", "BVK", "IDEAL"), nullable=False)
    pas_pasID = Column(Integer, nullable=True)
    pas = relationship("Pas", foreign_keys=[pas_pasID])
    __mapper_args__ = {'polymorphic_identity': "DibsTransactie", 'inherit_condition': transactieID == Transactie.transactieID}
    __table_args__ = (ForeignKeyConstraint(["pas_pasID"], ["Pas.pasID"], name="DibsTransactie_Pas"), ForeignKeyConstraint(["transactieID"], ["Transactie.transactieID"], name="DibsTransactie_TransactieID"),)
