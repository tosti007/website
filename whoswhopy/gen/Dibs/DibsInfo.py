from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Dibs.DibsTransactie import DibsTransactie
from whoswhopy.gen.Persoon import Persoon
class DibsInfo(Entiteit):
    __tablename__ = "DibsInfo"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    kudos = Column(Integer, nullable=False)
    laatste_transactieID = Column(Integer, nullable=True)
    laatste = relationship("DibsTransactie", foreign_keys=[laatste_transactieID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "DibsInfo"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="DibsInfo_Persoon"), ForeignKeyConstraint(["laatste_transactieID"], ["DibsTransactie.transactieID"], name="DibsInfo_Laatste"),)
