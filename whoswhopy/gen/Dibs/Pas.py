from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class Pas(Entiteit):
    __tablename__ = "Pas"
    pasID = Column(Integer, primary_key=True, nullable=False)
    rfidTag = Column(String(255), nullable=False)
    persoon_contactID = Column(Integer, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    actief = Column(Boolean, server_default="True", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Pas"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Pas_Persoon"),)
