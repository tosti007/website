from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from whoswhopy.gen.Boekweb2.Artikel import Artikel
class ColaProduct(Artikel):
    __tablename__ = "ColaProduct"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    volgorde = Column(Integer, nullable=False)
    __mapper_args__ = {'polymorphic_identity': "ColaProduct", 'inherit_condition': artikelID == Artikel.artikelID}
    __table_args__ = (ForeignKeyConstraint(["artikelID"], ["Artikel.artikelID"], name="ColaProduct_ArtikelID"),)
