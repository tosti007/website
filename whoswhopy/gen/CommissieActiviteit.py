from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
class CommissieActiviteit(Entiteit):
    __tablename__ = "CommissieActiviteit"
    commissie_commissieID = Column(Integer, primary_key=True, nullable=False)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    activiteit_activiteitID = Column(Integer, primary_key=True, nullable=False)
    activiteit = relationship("Activiteit", foreign_keys=[activiteit_activiteitID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "CommissieActiviteit"}
    __table_args__ = (ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="CommissieActiviteit_Commissie"), ForeignKeyConstraint(["activiteit_activiteitID"], ["Activiteit.activiteitID"], name="CommissieActiviteit_Activiteit"),)
