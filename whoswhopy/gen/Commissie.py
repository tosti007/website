from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.CommissieCategorie import CommissieCategorie
class Commissie(Entiteit):
    __tablename__ = "Commissie"
    commissieID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    soort = Column(Enum("CIE", "GROEP", "DISPUUT"), nullable=False)
    categorie_categorieID = Column(Integer, nullable=True)
    categorie = relationship("CommissieCategorie", foreign_keys=[categorie_categorieID])
    thema = Column(String(255), nullable=True)
    omschrijving = Column(String(255), nullable=True)
    datumBegin = Column(Date, nullable=True)
    datumEind = Column(Date, nullable=True)
    login = Column(String(255), nullable=False)
    email = Column(String(255), nullable=True)
    homepage = Column(String(511), nullable=True)
    emailZichtbaar = Column(Boolean, server_default="true", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Commissie"}
    __table_args__ = (ForeignKeyConstraint(["categorie_categorieID"], ["CommissieCategorie.categorieID"], name="Commissie_Categorie"),)
