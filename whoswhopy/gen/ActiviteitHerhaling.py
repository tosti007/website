from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
class ActiviteitHerhaling(Activiteit):
    __tablename__ = "ActiviteitHerhaling"
    activiteitID = Column(Integer, primary_key=True, nullable=False)
    parent_activiteitID = Column(Integer, nullable=False)
    parent = relationship("Activiteit", foreign_keys=[parent_activiteitID])
    __mapper_args__ = {'polymorphic_identity': "ActiviteitHerhaling", 'inherit_condition': activiteitID == Activiteit.activiteitID}
    __table_args__ = (ForeignKeyConstraint(["parent_activiteitID"], ["Activiteit.activiteitID"], name="ActiviteitHerhaling_Parent"), ForeignKeyConstraint(["activiteitID"], ["Activiteit.activiteitID"], name="ActiviteitHerhaling_ActiviteitID"),)
