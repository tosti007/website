from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Vak import Vak
class Tentamen(Entiteit):
    __tablename__ = "Tentamen"
    id = Column(Integer, primary_key=True, nullable=False)
    vak_vakID = Column(Integer, nullable=False)
    vak = relationship("Vak", foreign_keys=[vak_vakID])
    datum = Column(Date, nullable=False)
    naam = Column(String(255), nullable=True)
    oud = Column(Boolean, server_default="false", nullable=False)
    tentamen = Column(Boolean, nullable=False)
    opmerking = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Tentamen"}
    __table_args__ = (ForeignKeyConstraint(["vak_vakID"], ["Vak.vakID"], name="Tentamen_Vak"),)
