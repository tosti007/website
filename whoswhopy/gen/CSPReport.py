from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid
class CSPReport(Entiteit):
    __tablename__ = "CSPReport"
    id = Column(Integer, primary_key=True, nullable=False)
    ingelogdLid_contactID = Column(Integer, nullable=True)
    ingelogdLid = relationship("Lid", foreign_keys=[ingelogdLid_contactID])
    ip = Column(String(255), nullable=False)
    documentUri = Column(String(255), nullable=False)
    referrer = Column(String(255), nullable=True)
    blockedUri = Column(String(255), nullable=False)
    violatedDirective = Column(String(255), nullable=False)
    originalPolicy = Column(String(255), nullable=False)
    userAgent = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "CSPReport"}
    __table_args__ = (ForeignKeyConstraint(["ingelogdLid_contactID"], ["Lid.contactID"], name="CSPReport_IngelogdLid"),)
