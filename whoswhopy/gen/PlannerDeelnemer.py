from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
from whoswhopy.gen.PlannerData import PlannerData
class PlannerDeelnemer(Entiteit):
    __tablename__ = "PlannerDeelnemer"
    plannerData_plannerDataID = Column(Integer, primary_key=True, nullable=False)
    plannerData = relationship("PlannerData", foreign_keys=[plannerData_plannerDataID])
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    antwoord = Column(Enum("ONBEKEND", "LIEVER_NIET", "NEE", "JA"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "PlannerDeelnemer"}
    __table_args__ = (ForeignKeyConstraint(["plannerData_plannerDataID"], ["PlannerData.plannerDataID"], name="PlannerDeelnemer_PlannerData"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="PlannerDeelnemer_Persoon"),)
