from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
class Mededeling(Entiteit):
    __tablename__ = "Mededeling"
    mededelingID = Column(Integer, primary_key=True, nullable=False)
    commissie_commissieID = Column(Integer, nullable=True)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    doelgroep = Column(Enum("ALLEN", "LEDEN", "ACTIEF"), nullable=False)
    prioriteit = Column(Enum("LAAG", "NORMAAL", "HOOG"), nullable=False)
    datumBegin = Column(Date, nullable=False)
    datumEind = Column(Date, nullable=False)
    url = Column(String(511), nullable=True)
    omschrijving_NL = Column(String(255), nullable=True)
    omschrijving_EN = Column(String(255), nullable=True)
    mededeling_NL = Column(Text, nullable=True)
    mededeling_EN = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Mededeling"}
    __table_args__ = (ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="Mededeling_Commissie"),)
