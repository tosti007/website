from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.ActiviteitVraag import ActiviteitVraag
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Deelnemer import Deelnemer
class DeelnemerAntwoord(Entiteit):
    __tablename__ = "DeelnemerAntwoord"
    deelnemer_persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    deelnemer_activiteit_activiteitID = Column(Integer, primary_key=True, nullable=False)
    deelnemer = relationship("Deelnemer", foreign_keys=[deelnemer_persoon_contactID, deelnemer_activiteit_activiteitID])
    vraag_activiteit_activiteitID = Column(Integer, primary_key=True, nullable=False)
    vraag_vraagID = Column(Integer, primary_key=True, nullable=False)
    vraag = relationship("ActiviteitVraag", foreign_keys=[vraag_activiteit_activiteitID, vraag_vraagID])
    antwoord = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "DeelnemerAntwoord"}
    __table_args__ = (ForeignKeyConstraint(["deelnemer_persoon_contactID", "deelnemer_activiteit_activiteitID"], ["Deelnemer.persoon_contactID", "Deelnemer.activiteit_activiteitID"], name="DeelnemerAntwoord_Deelnemer"), ForeignKeyConstraint(["vraag_activiteit_activiteitID", "vraag_vraagID"], ["ActiviteitVraag.activiteit_activiteitID", "ActiviteitVraag.vraagID"], name="DeelnemerAntwoord_Vraag"),)
