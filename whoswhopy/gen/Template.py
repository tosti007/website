from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Template(Entiteit):
    __tablename__ = "Template"
    id = Column(Integer, primary_key=True, nullable=False)
    label = Column(String(255), nullable=False)
    beschrijving = Column(String(255), nullable=False)
    waarde = Column(Text, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Template"}
