from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Spookweb2.Contractonderdeel import Contractonderdeel
class Vacature(Entiteit):
    __tablename__ = "Vacature"
    id = Column(Integer, primary_key=True, nullable=False)
    contractonderdeel_id = Column(Integer, nullable=True)
    contractonderdeel = relationship("Contractonderdeel", foreign_keys=[contractonderdeel_id])
    datumVerloop = Column(Date, nullable=False)
    datumPlaatsing = Column(Date, nullable=False)
    type = Column(Enum("BIJBAAN", "PROMOTIEPLAATS", "STAGEPLEK", "STARTERSFUNCTIE", "AFSTUDEEROPDRACHT", "WERKSTUDENTSCHAP"), nullable=False)
    ica = Column(Boolean, nullable=False)
    iku = Column(Boolean, nullable=False)
    na = Column(Boolean, nullable=False)
    wis = Column(Boolean, nullable=False)
    titel_NL = Column(String(255), nullable=False)
    titel_EN = Column(String(255), nullable=False)
    inleiding_NL = Column(Text, nullable=True)
    inleiding_EN = Column(Text, nullable=True)
    inhoud_NL = Column(Text, nullable=True)
    inhoud_EN = Column(Text, nullable=True)
    views = Column(Integer, server_default="0", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Vacature"}
    __table_args__ = (ForeignKeyConstraint(["contractonderdeel_id"], ["Contractonderdeel.id"], name="Vacature_Contractonderdeel"),)
