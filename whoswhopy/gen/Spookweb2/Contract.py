from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Spookweb2.Bedrijf import Bedrijf
class Contract(Entiteit):
    __tablename__ = "Contract"
    contractID = Column(Integer, primary_key=True, nullable=False)
    bedrijf_contactID = Column(Integer, nullable=False)
    bedrijf = relationship("Bedrijf", foreign_keys=[bedrijf_contactID])
    korting = Column(String(255), nullable=True)
    status = Column(Enum("GEFACTUREERD", "MISLUKT", "OPGESTUURD", "RETOUR", "VOORSTEL"), nullable=False)
    ingangsdatum = Column(Date, nullable=False)
    opmerking = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Contract"}
    __table_args__ = (ForeignKeyConstraint(["bedrijf_contactID"], ["Bedrijf.contactID"], name="Contract_Bedrijf"),)
