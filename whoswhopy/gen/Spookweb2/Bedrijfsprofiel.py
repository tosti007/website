from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Spookweb2.Contractonderdeel import Contractonderdeel
class Bedrijfsprofiel(Entiteit):
    __tablename__ = "Bedrijfsprofiel"
    id = Column(Integer, primary_key=True, nullable=False)
    contractonderdeel_id = Column(Integer, nullable=True)
    contractonderdeel = relationship("Contractonderdeel", foreign_keys=[contractonderdeel_id])
    datumVerloop = Column(Date, nullable=False)
    inhoud_NL = Column(Text, nullable=True)
    inhoud_EN = Column(Text, nullable=True)
    url = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Bedrijfsprofiel"}
    __table_args__ = (ForeignKeyConstraint(["contractonderdeel_id"], ["Contractonderdeel.id"], name="Bedrijfsprofiel_Contractonderdeel"),)
