from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
from whoswhopy.gen.ContactPersoon import ContactPersoon
from whoswhopy.gen.Spookweb2.Bedrijf import Bedrijf
class Interactie(Entiteit):
    __tablename__ = "Interactie"
    id = Column(Integer, primary_key=True, nullable=False)
    spook_persoon_contactID = Column(Integer, nullable=False)
    spook_organisatie_contactID = Column(Integer, nullable=False)
    spook_type = Column(Enum("NORMAAL", "SPOOK", "BEDRIJFSCONTACT", "OVERIGSPOOKWEB", "ALUMNUS"), nullable=False)
    spook = relationship("ContactPersoon", foreign_keys=[spook_persoon_contactID, spook_organisatie_contactID, spook_type])
    bedrijf_contactID = Column(Integer, nullable=False)
    bedrijf = relationship("Bedrijf", foreign_keys=[bedrijf_contactID])
    bedrijfPersoon_persoon_contactID = Column(Integer, nullable=True)
    bedrijfPersoon_organisatie_contactID = Column(Integer, nullable=True)
    bedrijfPersoon_type = Column(Enum("NORMAAL", "SPOOK", "BEDRIJFSCONTACT", "OVERIGSPOOKWEB", "ALUMNUS"), nullable=True)
    bedrijfPersoon = relationship("ContactPersoon", foreign_keys=[bedrijfPersoon_persoon_contactID, bedrijfPersoon_organisatie_contactID, bedrijfPersoon_type])
    commissie_commissieID = Column(Integer, nullable=True)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    datum = Column(DateTime, nullable=False)
    inhoud = Column(Text, nullable=False)
    medium = Column(Enum("EMAIL", "FAX", "GESPREK", "NOTITIE", "ONBEKEND", "OVERIG", "POST", "TELEFOON"), nullable=False)
    todo = Column(Boolean, server_default="false", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Interactie"}
    __table_args__ = (ForeignKeyConstraint(["spook_persoon_contactID", "spook_organisatie_contactID", "spook_type"], ["ContactPersoon.persoon_contactID", "ContactPersoon.organisatie_contactID", "ContactPersoon.type"], name="Interactie_Spook"), ForeignKeyConstraint(["bedrijf_contactID"], ["Bedrijf.contactID"], name="Interactie_Bedrijf"), ForeignKeyConstraint(["bedrijfPersoon_persoon_contactID", "bedrijfPersoon_organisatie_contactID", "bedrijfPersoon_type"], ["ContactPersoon.persoon_contactID", "ContactPersoon.organisatie_contactID", "ContactPersoon.type"], name="Interactie_BedrijfPersoon"), ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="Interactie_Commissie"),)
