from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from whoswhopy.gen.Contact import Contact
from whoswhopy.gen.Organisatie import Organisatie
class Bedrijf(Organisatie):
    __tablename__ = "Bedrijf"
    contactID = Column(Integer, primary_key=True, nullable=False)
    status = Column(Enum("AFGEWEZEN", "HUIDIG", "KOSTENLOOS", "OUD", "POTENTIEEL", "OVERGENOME", "OPGEHEVEN", "OVERIG"), server_default="POTENTIEEL", nullable=False)
    type = Column(Enum("ICT", "CONSULTANCY", "FINANCIEEL", "ONDERZOEK", "OVERIG"), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Bedrijf", 'inherit_condition': contactID == Contact.contactID}
    __table_args__ = (ForeignKeyConstraint(["contactID"], ["Contact.contactID"], name="Bedrijf_ContactID"),)
