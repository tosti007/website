from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Spookweb2.Bedrijf import Bedrijf
from whoswhopy.gen.Studie import Studie
class BedrijfStudie(Entiteit):
    __tablename__ = "BedrijfStudie"
    Studie_studieID = Column(Integer, primary_key=True, nullable=False)
    Studie = relationship("Studie", foreign_keys=[Studie_studieID])
    Bedrijf_contactID = Column(Integer, primary_key=True, nullable=False)
    Bedrijf = relationship("Bedrijf", foreign_keys=[Bedrijf_contactID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "BedrijfStudie"}
    __table_args__ = (ForeignKeyConstraint(["Studie_studieID"], ["Studie.studieID"], name="BedrijfStudie_Studie"), ForeignKeyConstraint(["Bedrijf_contactID"], ["Bedrijf.contactID"], name="BedrijfStudie_Bedrijf"),)
