from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
from whoswhopy.gen.Persoon import Persoon
from whoswhopy.gen.Spookweb2.Contract import Contract
class Contractonderdeel(Entiteit):
    __tablename__ = "Contractonderdeel"
    id = Column(Integer, primary_key=True, nullable=False)
    contract_contractID = Column(Integer, nullable=False)
    contract = relationship("Contract", foreign_keys=[contract_contractID])
    soort = Column(Enum("VAKID", "VACATURE", "MAILING", "PROFIEL", "LEZING", "OVERIG"), server_default="OVERIG", nullable=False)
    bedrag = Column(Integer, nullable=False)
    uitvoerDatum = Column(Date, nullable=True)
    omschrijving = Column(Text, nullable=True)
    uitgevoerd = Column(String(255), nullable=True)
    verantwoordelijke_contactID = Column(Integer, nullable=True)
    verantwoordelijke = relationship("Persoon", foreign_keys=[verantwoordelijke_contactID])
    gefactureerd = Column(Boolean, server_default="false", nullable=False)
    commissie_commissieID = Column(Integer, nullable=True)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    latexString = Column(Text, nullable=True)
    annulering = Column(Boolean, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Contractonderdeel"}
    __table_args__ = (ForeignKeyConstraint(["contract_contractID"], ["Contract.contractID"], name="Contractonderdeel_Contract"), ForeignKeyConstraint(["verantwoordelijke_contactID"], ["Persoon.contactID"], name="Contractonderdeel_Verantwoordelijke"), ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="Contractonderdeel_Commissie"),)
