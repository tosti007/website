from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Spookweb2.Vacature import Vacature
from whoswhopy.gen.Studie import Studie
class VacatureStudie(Entiteit):
    __tablename__ = "VacatureStudie"
    Studie_studieID = Column(Integer, primary_key=True, nullable=False)
    Studie = relationship("Studie", foreign_keys=[Studie_studieID])
    Vacature_id = Column(Integer, primary_key=True, nullable=False)
    Vacature = relationship("Vacature", foreign_keys=[Vacature_id])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "VacatureStudie"}
    __table_args__ = (ForeignKeyConstraint(["Studie_studieID"], ["Studie.studieID"], name="VacatureStudie_Studie"), ForeignKeyConstraint(["Vacature_id"], ["Vacature.id"], name="VacatureStudie_Vacature"),)
