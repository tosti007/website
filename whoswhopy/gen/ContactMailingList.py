from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Contact import Contact
from whoswhopy.gen.Mailings.MailingList import MailingList
class ContactMailingList(Entiteit):
    __tablename__ = "ContactMailingList"
    contact_contactID = Column(Integer, primary_key=True, nullable=False)
    contact = relationship("Contact", foreign_keys=[contact_contactID])
    mailingList_mailingListID = Column(Integer, primary_key=True, nullable=False)
    mailingList = relationship("MailingList", foreign_keys=[mailingList_mailingListID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ContactMailingList"}
    __table_args__ = (ForeignKeyConstraint(["contact_contactID"], ["Contact.contactID"], name="ContactMailingList_Contact"), ForeignKeyConstraint(["mailingList_mailingListID"], ["MailingList.mailingListID"], name="ContactMailingList_MailingList"),)
