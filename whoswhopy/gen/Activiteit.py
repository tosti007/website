from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Activiteit(Entiteit):
    __tablename__ = "Activiteit"
    activiteitID = Column(Integer, primary_key=True, nullable=False)
    momentBegin = Column(DateTime, nullable=False)
    momentEind = Column(DateTime, nullable=False)
    toegang = Column(Enum("PUBLIEK", "PRIVE"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    overerving = Column(String(255), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Activiteit", 'polymorphic_on': overerving}
