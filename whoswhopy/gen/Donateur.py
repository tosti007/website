from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class Donateur(Entiteit):
    __tablename__ = "Donateur"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    anderPersoon_contactID = Column(Integer, nullable=True)
    anderPersoon = relationship("Persoon", foreign_keys=[anderPersoon_contactID])
    aanhef = Column(String(255), nullable=True)
    jaarBegin = Column(Integer, nullable=False)
    jaarEind = Column(Integer, nullable=True)
    donatie = Column(Numeric(8, 2), nullable=False)
    machtiging = Column(Boolean, nullable=False)
    machtigingOud = Column(Boolean, nullable=False)
    almanak = Column(Boolean, nullable=False)
    avNotulen = Column(Enum("N", "POST", "EMAIL", "POST_EMAIL"), nullable=False)
    jaarverslag = Column(Boolean, nullable=False)
    studiereis = Column(Boolean, nullable=False)
    symposium = Column(Boolean, nullable=False)
    vakid = Column(Boolean, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Donateur"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Donateur_Persoon"), ForeignKeyConstraint(["anderPersoon_contactID"], ["Persoon.contactID"], name="Donateur_AnderPersoon"),)
