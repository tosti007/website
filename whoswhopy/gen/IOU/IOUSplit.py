from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.IOU.IOUBon import IOUBon
from whoswhopy.gen.Persoon import Persoon
class IOUSplit(Entiteit):
    __tablename__ = "IOUSplit"
    bon_bonID = Column(Integer, primary_key=True, nullable=False)
    bon = relationship("IOUBon", foreign_keys=[bon_bonID])
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    moetBetalen = Column(Numeric(8, 2), nullable=False)
    heeftBetaald = Column(Numeric(8, 2), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "IOUSplit"}
    __table_args__ = (ForeignKeyConstraint(["bon_bonID"], ["IOUBon.bonID"], name="IOUSplit_Bon"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="IOUSplit_Persoon"),)
