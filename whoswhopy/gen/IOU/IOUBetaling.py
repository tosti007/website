from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.IOU.IOUBon import IOUBon
from whoswhopy.gen.Persoon import Persoon
class IOUBetaling(Entiteit):
    __tablename__ = "IOUBetaling"
    betalingID = Column(Integer, primary_key=True, nullable=False)
    bon_bonID = Column(Integer, nullable=True)
    bon = relationship("IOUBon", foreign_keys=[bon_bonID])
    van_contactID = Column(Integer, nullable=False)
    van = relationship("Persoon", foreign_keys=[van_contactID])
    naar_contactID = Column(Integer, nullable=False)
    naar = relationship("Persoon", foreign_keys=[naar_contactID])
    bedrag = Column(Numeric(8, 2), nullable=False)
    omschrijving = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "IOUBetaling"}
    __table_args__ = (ForeignKeyConstraint(["bon_bonID"], ["IOUBon.bonID"], name="IOUBetaling_Bon"), ForeignKeyConstraint(["van_contactID"], ["Persoon.contactID"], name="IOUBetaling_Van"), ForeignKeyConstraint(["naar_contactID"], ["Persoon.contactID"], name="IOUBetaling_Naar"),)
