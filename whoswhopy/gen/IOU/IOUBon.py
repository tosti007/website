from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class IOUBon(Entiteit):
    __tablename__ = "IOUBon"
    bonID = Column(Integer, primary_key=True, nullable=False)
    persoon_contactID = Column(Integer, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    omschrijving = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "IOUBon"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="IOUBon_Persoon"),)
