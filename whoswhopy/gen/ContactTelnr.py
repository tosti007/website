from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Contact import Contact
class ContactTelnr(Entiteit):
    __tablename__ = "ContactTelnr"
    telnrID = Column(Integer, primary_key=True, nullable=False)
    contact_contactID = Column(Integer, nullable=False)
    contact = relationship("Contact", foreign_keys=[contact_contactID])
    telefoonnummer = Column(String(255), nullable=False)
    soort = Column(Enum("THUIS", "MOBIEL", "WERK", "OUDERS", "RECEPTIE", "FAX"), nullable=False)
    voorkeur = Column(Boolean, server_default="False", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ContactTelnr"}
    __table_args__ = (ForeignKeyConstraint(["contact_contactID"], ["Contact.contactID"], name="ContactTelnr_Contact"),)
