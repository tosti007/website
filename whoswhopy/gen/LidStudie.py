from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Intro.IntroGroep import IntroGroep
from whoswhopy.gen.Lid import Lid
from whoswhopy.gen.Studie import Studie
class LidStudie(Entiteit):
    __tablename__ = "LidStudie"
    lidStudieID = Column(Integer, primary_key=True, nullable=False)
    studie_studieID = Column(Integer, nullable=False)
    studie = relationship("Studie", foreign_keys=[studie_studieID])
    lid_contactID = Column(Integer, nullable=False)
    lid = relationship("Lid", foreign_keys=[lid_contactID])
    datumBegin = Column(Date, nullable=False)
    datumEind = Column(Date, nullable=True)
    status = Column(Enum("STUDEREND", "ALUMNUS", "GESTOPT"), nullable=False)
    groep_groepID = Column(Integer, nullable=True)
    groep = relationship("IntroGroep", foreign_keys=[groep_groepID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "LidStudie"}
    __table_args__ = (ForeignKeyConstraint(["studie_studieID"], ["Studie.studieID"], name="LidStudie_Studie"), ForeignKeyConstraint(["lid_contactID"], ["Lid.contactID"], name="LidStudie_Lid"), ForeignKeyConstraint(["groep_groepID"], ["IntroGroep.groepID"], name="LidStudie_Groep"),)
