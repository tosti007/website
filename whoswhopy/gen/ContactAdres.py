from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Contact import Contact
class ContactAdres(Entiteit):
    __tablename__ = "ContactAdres"
    adresID = Column(Integer, primary_key=True, nullable=False)
    contact_contactID = Column(Integer, nullable=False)
    contact = relationship("Contact", foreign_keys=[contact_contactID])
    soort = Column(Enum("THUIS", "OUDERS", "WERK", "POST", "BEZOEK", "FACTUUR"), nullable=False)
    straat1 = Column(String(255), nullable=True)
    straat2 = Column(String(255), nullable=True)
    huisnummer = Column(String(255), nullable=True)
    postcode = Column(String(255), nullable=True)
    woonplaats = Column(String(255), nullable=True)
    land = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ContactAdres"}
    __table_args__ = (ForeignKeyConstraint(["contact_contactID"], ["Contact.contactID"], name="ContactAdres_Contact"),)
