from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Studie(Entiteit):
    __tablename__ = "Studie"
    studieID = Column(Integer, primary_key=True, nullable=False)
    naam_NL = Column(String(255), nullable=False)
    naam_EN = Column(String(255), nullable=False)
    soort = Column(Enum("GT", "IC", "IK", "NA", "WI", "WIT", "OVERIG"), nullable=False)
    fase = Column(Enum("BA", "MA"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Studie"}
