from sqlalchemy import BigInteger
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid
class PapierMolen(Entiteit):
    __tablename__ = "PapierMolen"
    papierMolenID = Column(Integer, primary_key=True, nullable=False)
    geplaatst = Column(DateTime, nullable=False)
    lid_contactID = Column(Integer, nullable=False)
    lid = relationship("Lid", foreign_keys=[lid_contactID])
    ean = Column(BigInteger, nullable=False)
    auteur = Column(String(255), nullable=True)
    titel = Column(String(255), nullable=True)
    druk = Column(String(255), nullable=True)
    prijs = Column(Numeric(8, 2), nullable=False)
    opmerking = Column(String(255), nullable=True)
    studie = Column(Enum("WI", "NA", "IC", "GT", "IK"), nullable=False)
    verloopdatum = Column(Date, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "PapierMolen"}
    __table_args__ = (ForeignKeyConstraint(["lid_contactID"], ["Lid.contactID"], name="PapierMolen_Lid"),)
