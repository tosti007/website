from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Planner import Planner
class PlannerData(Entiteit):
    __tablename__ = "PlannerData"
    plannerDataID = Column(Integer, primary_key=True, nullable=False)
    planner_plannerID = Column(Integer, nullable=False)
    planner = relationship("Planner", foreign_keys=[planner_plannerID])
    momentBegin = Column(DateTime, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "PlannerData"}
    __table_args__ = (ForeignKeyConstraint(["planner_plannerID"], ["Planner.plannerID"], name="PlannerData_Planner"),)
