from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from whoswhopy.gen.Basis.Entiteit import Entiteit
class MailingTemplate(Entiteit):
    __tablename__ = "MailingTemplate"
    mailingTemplateID = Column(Integer, primary_key=True, nullable=False)
    omschrijving = Column(Text, nullable=False)
    standaardPreviewers = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "MailingTemplate"}
