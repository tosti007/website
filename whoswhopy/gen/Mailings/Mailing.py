from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
class Mailing(Entiteit):
    __tablename__ = "Mailing"
    mailingID = Column(Integer, primary_key=True, nullable=False)
    commissie_commissieID = Column(Integer, nullable=True)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    omschrijving = Column(String(255), nullable=False)
    momentIngepland = Column(DateTime, nullable=True)
    momentVerwerkt = Column(DateTime, nullable=True)
    mailHeaders = Column(Text, nullable=True)
    mailSubject = Column(String(255), nullable=False)
    mailFrom = Column(String(255), nullable=False)
    isDefinitief = Column(Boolean, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Mailing"}
    __table_args__ = (ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="Mailing_Commissie"),)
