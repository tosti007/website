from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Mailings.Mailing import Mailing
from whoswhopy.gen.TinyUrl import TinyUrl
class TinyMailingUrl(Entiteit):
    __tablename__ = "TinyMailingUrl"
    mailing_mailingID = Column(Integer, primary_key=True, nullable=False)
    mailing = relationship("Mailing", foreign_keys=[mailing_mailingID])
    tinyUrl_id = Column(Integer, primary_key=True, nullable=False)
    tinyUrl = relationship("TinyUrl", foreign_keys=[tinyUrl_id])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "TinyMailingUrl"}
    __table_args__ = (ForeignKeyConstraint(["mailing_mailingID"], ["Mailing.mailingID"], name="TinyMailingUrl_Mailing"), ForeignKeyConstraint(["tinyUrl_id"], ["TinyUrl.id"], name="TinyMailingUrl_TinyUrl"),)
