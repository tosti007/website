from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from whoswhopy.gen.Basis.Entiteit import Entiteit
class MailingList(Entiteit):
    __tablename__ = "MailingList"
    mailingListID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    omschrijving = Column(Text, nullable=False)
    subjectPrefix = Column(String(255), nullable=True)
    bodyPrefix = Column(Text, nullable=True)
    bodySuffix = Column(Text, nullable=True)
    verborgen = Column(Boolean, server_default="False", nullable=False)
    query = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "MailingList"}
