from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Contact import Contact
from whoswhopy.gen.Mailings.Mailing import Mailing
class MailingBounce(Entiteit):
    __tablename__ = "MailingBounce"
    mailingBounceID = Column(Integer, primary_key=True, nullable=False)
    mailing_mailingID = Column(Integer, nullable=True)
    mailing = relationship("Mailing", foreign_keys=[mailing_mailingID])
    contact_contactID = Column(Integer, nullable=False)
    contact = relationship("Contact", foreign_keys=[contact_contactID])
    bounceContent = Column(Text, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "MailingBounce"}
    __table_args__ = (ForeignKeyConstraint(["mailing_mailingID"], ["Mailing.mailingID"], name="MailingBounce_Mailing"), ForeignKeyConstraint(["contact_contactID"], ["Contact.contactID"], name="MailingBounce_Contact"),)
