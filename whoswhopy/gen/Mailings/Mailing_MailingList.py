from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Mailings.Mailing import Mailing
from whoswhopy.gen.Mailings.MailingList import MailingList
class Mailing_MailingList(Entiteit):
    __tablename__ = "Mailing_MailingList"
    mailing_mailingID = Column(Integer, primary_key=True, nullable=False)
    mailing = relationship("Mailing", foreign_keys=[mailing_mailingID])
    mailingList_mailingListID = Column(Integer, primary_key=True, nullable=False)
    mailingList = relationship("MailingList", foreign_keys=[mailingList_mailingListID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Mailing_MailingList"}
    __table_args__ = (ForeignKeyConstraint(["mailing_mailingID"], ["Mailing.mailingID"], name="Mailing_MailingList_Mailing"), ForeignKeyConstraint(["mailingList_mailingListID"], ["MailingList.mailingListID"], name="Mailing_MailingList_MailingList"),)
