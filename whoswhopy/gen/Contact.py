from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Contact(Entiteit):
    __tablename__ = "Contact"
    contactID = Column(Integer, primary_key=True, nullable=False)
    email = Column(String(255), nullable=True)
    homepage = Column(String(511), nullable=True)
    vakidOpsturen = Column(Boolean, server_default="false", nullable=False)
    aes2rootsOpsturen = Column(Boolean, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    overerving = Column(String(255), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Contact", 'polymorphic_on': overerving}
