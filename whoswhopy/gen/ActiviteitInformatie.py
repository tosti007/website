from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
from whoswhopy.gen.ActiviteitCategorie import ActiviteitCategorie
class ActiviteitInformatie(Activiteit):
    __tablename__ = "ActiviteitInformatie"
    activiteitID = Column(Integer, primary_key=True, nullable=False)
    titel_NL = Column(String(255), nullable=True)
    titel_EN = Column(String(255), nullable=True)
    werftekst_NL = Column(Text, nullable=False)
    werftekst_EN = Column(Text, nullable=False)
    homepage = Column(String(511), nullable=True)
    locatie_NL = Column(String(255), nullable=True)
    locatie_EN = Column(String(255), nullable=True)
    prijs_NL = Column(String(255), nullable=True)
    prijs_EN = Column(String(255), nullable=True)
    inschrijfbaar = Column(Boolean, server_default="False", nullable=False)
    stuurMail = Column(Boolean, server_default="True", nullable=False)
    datumInschrijvenMax = Column(Date, nullable=True)
    datumUitschrijven = Column(Date, nullable=True)
    maxDeelnemers = Column(Integer, nullable=True)
    actsoort = Column(Enum("AES2", "UU", "LAND", "ZUS", ".COM", "EXT"), nullable=False)
    onderwijs = Column(Enum("N", "B", "J"), nullable=False)
    aantalComputers = Column(Integer, server_default="0", nullable=False)
    categorie_actCategorieID = Column(Integer, nullable=True)
    categorie = relationship("ActiviteitCategorie", foreign_keys=[categorie_actCategorieID])
    __mapper_args__ = {'polymorphic_identity': "ActiviteitInformatie", 'inherit_condition': activiteitID == Activiteit.activiteitID}
    __table_args__ = (ForeignKeyConstraint(["categorie_actCategorieID"], ["ActiviteitCategorie.actCategorieID"], name="ActiviteitInformatie_Categorie"), ForeignKeyConstraint(["activiteitID"], ["Activiteit.activiteitID"], name="ActiviteitInformatie_ActiviteitID"),)
