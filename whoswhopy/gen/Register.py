from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Register(Entiteit):
    __tablename__ = "Register"
    id = Column(Integer, primary_key=True, nullable=False)
    label = Column(String(255), nullable=False)
    beschrijving = Column(String(255), nullable=False)
    waarde = Column(String(255), nullable=True)
    type = Column(Enum("STRING", "INT", "BOOLEAN", "FLOAT"), nullable=False)
    auth = Column(Enum("BESTUUR", "GOD", "INGELOGD"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Register"}
