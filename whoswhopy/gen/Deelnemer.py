from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class Deelnemer(Entiteit):
    __tablename__ = "Deelnemer"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    activiteit_activiteitID = Column(Integer, primary_key=True, nullable=False)
    activiteit = relationship("Activiteit", foreign_keys=[activiteit_activiteitID])
    momentInschrijven = Column(DateTime, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Deelnemer"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Deelnemer_Persoon"), ForeignKeyConstraint(["activiteit_activiteitID"], ["Activiteit.activiteitID"], name="Deelnemer_Activiteit"),)
