from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Artikel import Artikel
from whoswhopy.gen.Persoon import Persoon
class Bestelling(Entiteit):
    __tablename__ = "Bestelling"
    bestellingID = Column(Integer, primary_key=True, nullable=False)
    persoon_contactID = Column(Integer, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    artikel_artikelID = Column(Integer, nullable=False)
    artikel = relationship("Artikel", foreign_keys=[artikel_artikelID])
    datumGeplaatst = Column(Date, nullable=False)
    status = Column(Enum("OPEN", "GERESERVEERD", "GESLOTEN", "VERLOPEN"), server_default="open", nullable=False)
    aantal = Column(Integer, nullable=False)
    vervalDatum = Column(Date, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Bestelling"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Bestelling_Persoon"), ForeignKeyConstraint(["artikel_artikelID"], ["Artikel.artikelID"], name="Bestelling_Artikel"),)
