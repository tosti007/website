from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Voorraad import Voorraad
from whoswhopy.gen.Boekweb2.VoorraadMutatie import VoorraadMutatie
class Verplaatsing(VoorraadMutatie):
    __tablename__ = "Verplaatsing"
    voorraadMutatieID = Column(Integer, primary_key=True, nullable=False)
    tegenVoorraad_voorraadID = Column(Integer, nullable=False)
    tegenVoorraad = relationship("Voorraad", foreign_keys=[tegenVoorraad_voorraadID])
    __mapper_args__ = {'polymorphic_identity': "Verplaatsing", 'inherit_condition': voorraadMutatieID == VoorraadMutatie.voorraadMutatieID}
    __table_args__ = (ForeignKeyConstraint(["tegenVoorraad_voorraadID"], ["Voorraad.voorraadID"], name="Verplaatsing_TegenVoorraad"), ForeignKeyConstraint(["voorraadMutatieID"], ["VoorraadMutatie.voorraadMutatieID"], name="Verplaatsing_VoorraadMutatieID"),)
