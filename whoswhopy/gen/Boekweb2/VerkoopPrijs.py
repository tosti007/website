from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Voorraad import Voorraad
class VerkoopPrijs(Entiteit):
    __tablename__ = "VerkoopPrijs"
    voorraad_voorraadID = Column(Integer, primary_key=True, nullable=False)
    voorraad = relationship("Voorraad", foreign_keys=[voorraad_voorraadID])
    wanneer = Column(DateTime, primary_key=True, nullable=False)
    prijs = Column(Numeric(8, 2), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "VerkoopPrijs"}
    __table_args__ = (ForeignKeyConstraint(["voorraad_voorraadID"], ["Voorraad.voorraadID"], name="VerkoopPrijs_Voorraad"),)
