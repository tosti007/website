from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from whoswhopy.gen.Boekweb2.Transactie import Transactie
class Pin(Transactie):
    __tablename__ = "Pin"
    transactieID = Column(Integer, primary_key=True, nullable=False)
    rekening = Column(Enum("507122690"), nullable=False)
    apparaat = Column(Enum("SEPAY", "XENTISSIMO"), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Pin", 'inherit_condition': transactieID == Transactie.transactieID}
    __table_args__ = (ForeignKeyConstraint(["transactieID"], ["Transactie.transactieID"], name="Pin_TransactieID"),)
