from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Transactie import Transactie
class Schuld(Transactie):
    __tablename__ = "Schuld"
    transactieID = Column(Integer, primary_key=True, nullable=False)
    afgeschreven = Column(DateTime, nullable=True)
    transactie_transactieID = Column(Integer, nullable=True)
    transactie = relationship("Transactie", foreign_keys=[transactie_transactieID])
    __mapper_args__ = {'polymorphic_identity': "Schuld", 'inherit_condition': transactieID == Transactie.transactieID}
    __table_args__ = (ForeignKeyConstraint(["transactie_transactieID"], ["Transactie.transactieID"], name="Schuld_Transactie"), ForeignKeyConstraint(["transactieID"], ["Transactie.transactieID"], name="Schuld_TransactieID"),)
