from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Leverancier import Leverancier
from whoswhopy.gen.Boekweb2.Order import Order
from whoswhopy.gen.Boekweb2.VoorraadMutatie import VoorraadMutatie
class LeverancierRetour(VoorraadMutatie):
    __tablename__ = "LeverancierRetour"
    voorraadMutatieID = Column(Integer, primary_key=True, nullable=False)
    order_orderID = Column(Integer, nullable=True)
    order = relationship("Order", foreign_keys=[order_orderID])
    leverancier_contactID = Column(Integer, nullable=False)
    leverancier = relationship("Leverancier", foreign_keys=[leverancier_contactID])
    __mapper_args__ = {'polymorphic_identity': "LeverancierRetour", 'inherit_condition': voorraadMutatieID == VoorraadMutatie.voorraadMutatieID}
    __table_args__ = (ForeignKeyConstraint(["order_orderID"], ["Order.orderID"], name="LeverancierRetour_Order"), ForeignKeyConstraint(["leverancier_contactID"], ["Leverancier.contactID"], name="LeverancierRetour_Leverancier"), ForeignKeyConstraint(["voorraadMutatieID"], ["VoorraadMutatie.voorraadMutatieID"], name="LeverancierRetour_VoorraadMutatieID"),)
