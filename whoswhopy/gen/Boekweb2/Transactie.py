from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Contact import Contact
class Transactie(Entiteit):
    __tablename__ = "Transactie"
    transactieID = Column(Integer, primary_key=True, nullable=False)
    rubriek = Column(Enum("BOEKWEB", "COLAKAS", "WWW"), nullable=False)
    soort = Column(Enum("VERKOOP", "DONATIE", "BETALING", "AFSCHRIJVING"), nullable=False)
    contact_contactID = Column(Integer, nullable=True)
    contact = relationship("Contact", foreign_keys=[contact_contactID])
    bedrag = Column(Numeric(8, 2), nullable=True)
    uitleg = Column(String(255), nullable=False)
    wanneer = Column(DateTime, nullable=False)
    status = Column(Enum("INACTIVE", "OPEN", "SUCCESS", "FAILURE", "CANCELLED", "EXPIRED", "OPENPOGING2", "OPENPOGING3", "OPENPOGING4", "OPENPOGING5", "BANKFAILURE"), server_default="SUCCESS", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    overerving = Column(String(255), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Transactie", 'polymorphic_on': overerving}
    __table_args__ = (ForeignKeyConstraint(["contact_contactID"], ["Contact.contactID"], name="Transactie_Contact"),)
