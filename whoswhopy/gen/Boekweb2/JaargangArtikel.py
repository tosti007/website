from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Artikel import Artikel
from whoswhopy.gen.Boekweb2.Jaargang import Jaargang
class JaargangArtikel(Entiteit):
    __tablename__ = "JaargangArtikel"
    jaargang_jaargangID = Column(Integer, primary_key=True, nullable=False)
    jaargang = relationship("Jaargang", foreign_keys=[jaargang_jaargangID])
    artikel_artikelID = Column(Integer, primary_key=True, nullable=False)
    artikel = relationship("Artikel", foreign_keys=[artikel_artikelID])
    schattingNodig = Column(Integer, nullable=True)
    opmerking = Column(Text, nullable=True)
    verplicht = Column(Boolean, server_default="true", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "JaargangArtikel"}
    __table_args__ = (ForeignKeyConstraint(["jaargang_jaargangID"], ["Jaargang.jaargangID"], name="JaargangArtikel_Jaargang"), ForeignKeyConstraint(["artikel_artikelID"], ["Artikel.artikelID"], name="JaargangArtikel_Artikel"),)
