from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Voorraad import Voorraad
class VoorraadMutatie(Entiteit):
    __tablename__ = "VoorraadMutatie"
    voorraadMutatieID = Column(Integer, primary_key=True, nullable=False)
    voorraad_voorraadID = Column(Integer, nullable=False)
    voorraad = relationship("Voorraad", foreign_keys=[voorraad_voorraadID])
    wanneer = Column(DateTime, nullable=False)
    aantal = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    overerving = Column(String(255), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "VoorraadMutatie", 'polymorphic_on': overerving}
    __table_args__ = (ForeignKeyConstraint(["voorraad_voorraadID"], ["Voorraad.voorraadID"], name="VoorraadMutatie_Voorraad"),)
