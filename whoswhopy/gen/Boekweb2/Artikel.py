from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Artikel(Entiteit):
    __tablename__ = "Artikel"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    overerving = Column(String(255), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Artikel", 'polymorphic_on': overerving}
