from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.ActiviteitVraag import ActiviteitVraag
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.iDeal import iDeal
class iDealAntwoord(Entiteit):
    __tablename__ = "iDealAntwoord"
    ideal_transactieID = Column(Integer, primary_key=True, nullable=False)
    ideal = relationship("iDeal", foreign_keys=[ideal_transactieID])
    vraag_activiteit_activiteitID = Column(Integer, primary_key=True, nullable=False)
    vraag_vraagID = Column(Integer, primary_key=True, nullable=False)
    vraag = relationship("ActiviteitVraag", foreign_keys=[vraag_activiteit_activiteitID, vraag_vraagID])
    antwoord = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "iDealAntwoord"}
    __table_args__ = (ForeignKeyConstraint(["ideal_transactieID"], ["iDeal.transactieID"], name="iDealAntwoord_Ideal"), ForeignKeyConstraint(["vraag_activiteit_activiteitID", "vraag_vraagID"], ["ActiviteitVraag.activiteit_activiteitID", "ActiviteitVraag.vraagID"], name="iDealAntwoord_Vraag"),)
