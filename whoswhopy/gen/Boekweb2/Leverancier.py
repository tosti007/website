from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Contact import Contact
class Leverancier(Contact):
    __tablename__ = "Leverancier"
    contactID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    dictaten = Column(Boolean, nullable=False)
    dictatenIntern = Column(Boolean, server_default="false", nullable=False)
    dibsproducten = Column(Boolean, server_default="false", nullable=False)
    colaproducten = Column(Boolean, server_default="false", nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Leverancier", 'inherit_condition': contactID == Contact.contactID}
    __table_args__ = (ForeignKeyConstraint(["contactID"], ["Contact.contactID"], name="Leverancier_ContactID"),)
