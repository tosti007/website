from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Leverancier import Leverancier
from whoswhopy.gen.Boekweb2.Order import Order
from whoswhopy.gen.Boekweb2.VoorraadMutatie import VoorraadMutatie
class Levering(VoorraadMutatie):
    __tablename__ = "Levering"
    voorraadMutatieID = Column(Integer, primary_key=True, nullable=False)
    order_orderID = Column(Integer, nullable=True)
    order = relationship("Order", foreign_keys=[order_orderID])
    leverancier_contactID = Column(Integer, nullable=False)
    leverancier = relationship("Leverancier", foreign_keys=[leverancier_contactID])
    __mapper_args__ = {'polymorphic_identity': "Levering", 'inherit_condition': voorraadMutatieID == VoorraadMutatie.voorraadMutatieID}
    __table_args__ = (ForeignKeyConstraint(["order_orderID"], ["Order.orderID"], name="Levering_Order"), ForeignKeyConstraint(["leverancier_contactID"], ["Leverancier.contactID"], name="Levering_Leverancier"), ForeignKeyConstraint(["voorraadMutatieID"], ["VoorraadMutatie.voorraadMutatieID"], name="Levering_VoorraadMutatieID"),)
