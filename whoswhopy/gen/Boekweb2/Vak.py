from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Vak(Entiteit):
    __tablename__ = "Vak"
    vakID = Column(Integer, primary_key=True, nullable=False)
    code = Column(String(255), nullable=False)
    naam = Column(String(255), nullable=True)
    departement = Column(Enum("NA", "WI", "IC", "OV"), nullable=False)
    opmerking = Column(String(255), nullable=True)
    inTentamenLijsten = Column(Boolean, server_default="true", nullable=False)
    tentamensOutdated = Column(Boolean, server_default="false", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Vak"}
