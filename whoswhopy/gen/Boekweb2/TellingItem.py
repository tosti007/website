from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Telling import Telling
from whoswhopy.gen.Boekweb2.Voorraad import Voorraad
class TellingItem(Entiteit):
    __tablename__ = "TellingItem"
    telling_tellingID = Column(Integer, primary_key=True, nullable=False)
    telling = relationship("Telling", foreign_keys=[telling_tellingID])
    voorraad_voorraadID = Column(Integer, primary_key=True, nullable=False)
    voorraad = relationship("Voorraad", foreign_keys=[voorraad_voorraadID])
    aantal = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "TellingItem"}
    __table_args__ = (ForeignKeyConstraint(["telling_tellingID"], ["Telling.tellingID"], name="TellingItem_Telling"), ForeignKeyConstraint(["voorraad_voorraadID"], ["Voorraad.voorraadID"], name="TellingItem_Voorraad"),)
