from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Artikel import Artikel
from whoswhopy.gen.Boekweb2.Leverancier import Leverancier
class Order(Entiteit):
    __tablename__ = "Order"
    orderID = Column(Integer, primary_key=True, nullable=False)
    artikel_artikelID = Column(Integer, nullable=False)
    artikel = relationship("Artikel", foreign_keys=[artikel_artikelID])
    leverancier_contactID = Column(Integer, nullable=False)
    leverancier = relationship("Leverancier", foreign_keys=[leverancier_contactID])
    waardePerStuk = Column(Numeric(8, 2), nullable=True)
    datumGeplaatst = Column(Date, nullable=False)
    geannuleerd = Column(Boolean, server_default="false", nullable=False)
    aantal = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    overerving = Column(String(255), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Order", 'polymorphic_on': overerving}
    __table_args__ = (ForeignKeyConstraint(["artikel_artikelID"], ["Artikel.artikelID"], name="Order_Artikel"), ForeignKeyConstraint(["leverancier_contactID"], ["Leverancier.contactID"], name="Order_Leverancier"),)
