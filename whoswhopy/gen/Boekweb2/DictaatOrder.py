from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from whoswhopy.gen.Boekweb2.Order import Order
class DictaatOrder(Order):
    __tablename__ = "DictaatOrder"
    orderID = Column(Integer, primary_key=True, nullable=False)
    kaftPrijs = Column(Numeric(8, 2), nullable=False)
    marge = Column(Numeric(8, 2), nullable=False)
    factuurverschil = Column(Numeric(8, 2), nullable=False)
    __mapper_args__ = {'polymorphic_identity': "DictaatOrder", 'inherit_condition': orderID == Order.orderID}
    __table_args__ = (ForeignKeyConstraint(["orderID"], ["Order.orderID"], name="DictaatOrder_OrderID"),)
