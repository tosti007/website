from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Boekweb2.Artikel import Artikel
class Boek(Artikel):
    __tablename__ = "Boek"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    isbn = Column(String(255), nullable=False)
    auteur = Column(String(255), nullable=True)
    druk = Column(String(255), nullable=True)
    uitgever = Column(String(255), nullable=True)
    __mapper_args__ = {'polymorphic_identity': "Boek", 'inherit_condition': artikelID == Artikel.artikelID}
    __table_args__ = (ForeignKeyConstraint(["artikelID"], ["Artikel.artikelID"], name="Boek_ArtikelID"),)
