from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Vak import Vak
from whoswhopy.gen.Persoon import Persoon
class Jaargang(Entiteit):
    __tablename__ = "Jaargang"
    jaargangID = Column(Integer, primary_key=True, nullable=False)
    vak_vakID = Column(Integer, nullable=False)
    vak = relationship("Vak", foreign_keys=[vak_vakID])
    contactPersoon_contactID = Column(Integer, nullable=False)
    contactPersoon = relationship("Persoon", foreign_keys=[contactPersoon_contactID])
    datumBegin = Column(Date, nullable=False)
    datumEinde = Column(Date, nullable=False)
    inschrijvingen = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Jaargang"}
    __table_args__ = (ForeignKeyConstraint(["vak_vakID"], ["Vak.vakID"], name="Jaargang_Vak"), ForeignKeyConstraint(["contactPersoon_contactID"], ["Persoon.contactID"], name="Jaargang_ContactPersoon"),)
