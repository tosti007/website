from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
from whoswhopy.gen.Boekweb2.Artikel import Artikel
class iDealKaartje(Artikel):
    __tablename__ = "iDealKaartje"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    activiteit_activiteitID = Column(Integer, nullable=False)
    activiteit = relationship("Activiteit", foreign_keys=[activiteit_activiteitID])
    autoInschrijven = Column(Boolean, server_default="false", nullable=False)
    returnURL = Column(String(255), nullable=True)
    externVerkrijgbaar = Column(Boolean, server_default="false", nullable=False)
    digitaalVerkrijgbaar = Column(Enum("NAMENLIJST", "CODE", "PERSOONLIJK"), server_default="NAMENLIJST", nullable=False)
    magScannen = Column(Boolean, server_default="false", nullable=False)
    __mapper_args__ = {'polymorphic_identity': "iDealKaartje", 'inherit_condition': artikelID == Artikel.artikelID}
    __table_args__ = (ForeignKeyConstraint(["activiteit_activiteitID"], ["Activiteit.activiteitID"], name="iDealKaartje_Activiteit"), ForeignKeyConstraint(["artikelID"], ["Artikel.artikelID"], name="iDealKaartje_ArtikelID"),)
