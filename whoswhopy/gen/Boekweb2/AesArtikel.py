from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Artikel import Artikel
from whoswhopy.gen.Commissie import Commissie
class AesArtikel(Artikel):
    __tablename__ = "AesArtikel"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    commissie_commissieID = Column(Integer, nullable=False)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    __mapper_args__ = {'polymorphic_identity': "AesArtikel", 'inherit_condition': artikelID == Artikel.artikelID}
    __table_args__ = (ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="AesArtikel_Commissie"), ForeignKeyConstraint(["artikelID"], ["Artikel.artikelID"], name="AesArtikel_ArtikelID"),)
