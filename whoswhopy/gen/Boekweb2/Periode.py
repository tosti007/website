from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Integer
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Periode(Entiteit):
    __tablename__ = "Periode"
    periodeID = Column(Integer, primary_key=True, nullable=False)
    datumBegin = Column(Date, nullable=False)
    collegejaar = Column(Integer, nullable=False)
    periodeNummer = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Periode"}
