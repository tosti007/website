from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Transactie import Transactie
from whoswhopy.gen.Boekweb2.VoorraadMutatie import VoorraadMutatie
class Verkoop(VoorraadMutatie):
    __tablename__ = "Verkoop"
    voorraadMutatieID = Column(Integer, primary_key=True, nullable=False)
    transactie_transactieID = Column(Integer, nullable=False)
    transactie = relationship("Transactie", foreign_keys=[transactie_transactieID])
    __mapper_args__ = {'polymorphic_identity': "Verkoop", 'inherit_condition': voorraadMutatieID == VoorraadMutatie.voorraadMutatieID}
    __table_args__ = (ForeignKeyConstraint(["transactie_transactieID"], ["Transactie.transactieID"], name="Verkoop_Transactie"), ForeignKeyConstraint(["voorraadMutatieID"], ["VoorraadMutatie.voorraadMutatieID"], name="Verkoop_VoorraadMutatieID"),)
