from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Boekweb2.Transactie import Transactie
class Giro(Transactie):
    __tablename__ = "Giro"
    transactieID = Column(Integer, primary_key=True, nullable=False)
    rekening = Column(String(255), nullable=True)
    tegenrekening = Column(String(255), nullable=True)
    __mapper_args__ = {'polymorphic_identity': "Giro", 'inherit_condition': transactieID == Transactie.transactieID}
    __table_args__ = (ForeignKeyConstraint(["transactieID"], ["Transactie.transactieID"], name="Giro_TransactieID"),)
