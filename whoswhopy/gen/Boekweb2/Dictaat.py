from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Artikel import Artikel
from whoswhopy.gen.Persoon import Persoon
class Dictaat(Artikel):
    __tablename__ = "Dictaat"
    artikelID = Column(Integer, primary_key=True, nullable=False)
    auteur_contactID = Column(Integer, nullable=False)
    auteur = relationship("Persoon", foreign_keys=[auteur_contactID])
    uitgaveJaar = Column(Integer, nullable=False)
    digitaalVerspreiden = Column(Boolean, nullable=False)
    beginGebruik = Column(Date, nullable=False)
    eindeGebruik = Column(Date, nullable=False)
    copyrightOpmerking = Column(Text, nullable=True)
    __mapper_args__ = {'polymorphic_identity': "Dictaat", 'inherit_condition': artikelID == Artikel.artikelID}
    __table_args__ = (ForeignKeyConstraint(["auteur_contactID"], ["Persoon.contactID"], name="Dictaat_Auteur"), ForeignKeyConstraint(["artikelID"], ["Artikel.artikelID"], name="Dictaat_ArtikelID"),)
