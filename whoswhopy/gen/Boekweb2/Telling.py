from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid
class Telling(Entiteit):
    __tablename__ = "Telling"
    tellingID = Column(Integer, primary_key=True, nullable=False)
    wanneer = Column(DateTime, nullable=False)
    teller_contactID = Column(Integer, nullable=False)
    teller = relationship("Lid", foreign_keys=[teller_contactID])
    Opmerking = Column(Text, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Telling"}
    __table_args__ = (ForeignKeyConstraint(["teller_contactID"], ["Lid.contactID"], name="Telling_Teller"),)
