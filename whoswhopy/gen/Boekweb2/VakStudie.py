from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Vak import Vak
from whoswhopy.gen.Studie import Studie
class VakStudie(Entiteit):
    __tablename__ = "VakStudie"
    vak_vakID = Column(Integer, primary_key=True, nullable=False)
    vak = relationship("Vak", foreign_keys=[vak_vakID])
    studie_studieID = Column(Integer, primary_key=True, nullable=False)
    studie = relationship("Studie", foreign_keys=[studie_studieID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "VakStudie"}
    __table_args__ = (ForeignKeyConstraint(["vak_vakID"], ["Vak.vakID"], name="VakStudie_Vak"), ForeignKeyConstraint(["studie_studieID"], ["Studie.studieID"], name="VakStudie_Studie"),)
