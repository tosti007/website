from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Boekweb2.Giro import Giro
from whoswhopy.gen.Boekweb2.Transactie import Transactie
from whoswhopy.gen.Boekweb2.Voorraad import Voorraad
class iDeal(Giro):
    __tablename__ = "iDeal"
    transactieID = Column(Integer, primary_key=True, nullable=False)
    titel = Column(String(255), nullable=False)
    voorraad_voorraadID = Column(Integer, nullable=False)
    voorraad = relationship("Voorraad", foreign_keys=[voorraad_voorraadID])
    externeID = Column(String(255), nullable=True)
    klantNaam = Column(String(255), nullable=True)
    klantStad = Column(String(255), nullable=True)
    expire = Column(DateTime, nullable=False)
    entranceCode = Column(String(255), nullable=False)
    magOpvragenTijdstip = Column(DateTime, nullable=False)
    emailadres = Column(String(255), nullable=True)
    returnURL = Column(String(255), nullable=True)
    __mapper_args__ = {'polymorphic_identity': "iDeal", 'inherit_condition': transactieID == Transactie.transactieID}
    __table_args__ = (ForeignKeyConstraint(["voorraad_voorraadID"], ["Voorraad.voorraadID"], name="iDeal_Voorraad"), ForeignKeyConstraint(["transactieID"], ["Transactie.transactieID"], name="iDeal_TransactieID"),)
