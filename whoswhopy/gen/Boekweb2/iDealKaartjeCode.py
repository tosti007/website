from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Transactie import Transactie
from whoswhopy.gen.Boekweb2.iDealKaartje import iDealKaartje
class iDealKaartjeCode(Entiteit):
    __tablename__ = "iDealKaartjeCode"
    id = Column(Integer, primary_key=True, nullable=False)
    kaartje_artikelID = Column(Integer, nullable=False)
    kaartje = relationship("iDealKaartje", foreign_keys=[kaartje_artikelID])
    code = Column(String(255), nullable=False)
    transactie_transactieID = Column(Integer, nullable=True)
    transactie = relationship("Transactie", foreign_keys=[transactie_transactieID])
    gescand = Column(Boolean, server_default="False", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "iDealKaartjeCode"}
    __table_args__ = (ForeignKeyConstraint(["kaartje_artikelID"], ["iDealKaartje.artikelID"], name="iDealKaartjeCode_Kaartje"), ForeignKeyConstraint(["transactie_transactieID"], ["Transactie.transactieID"], name="iDealKaartjeCode_Transactie"),)
