from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Boekweb2.Artikel import Artikel
class Voorraad(Entiteit):
    __tablename__ = "Voorraad"
    voorraadID = Column(Integer, primary_key=True, nullable=False)
    artikel_artikelID = Column(Integer, nullable=False)
    artikel = relationship("Artikel", foreign_keys=[artikel_artikelID])
    locatie = Column(Enum("BW1", "MAGAZIJN", "BOEKENHOK", "EJBV", "VIRTUEEL"), nullable=False)
    waardePerStuk = Column(Numeric(8, 2), nullable=False)
    btw = Column(Enum("21", "6", "0", "NULL"), server_default="NULL", nullable=False)
    omschrijving = Column(Text, nullable=True)
    verkoopbaar = Column(Boolean, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Voorraad"}
    __table_args__ = (ForeignKeyConstraint(["artikel_artikelID"], ["Artikel.artikelID"], name="Voorraad_Artikel"),)
