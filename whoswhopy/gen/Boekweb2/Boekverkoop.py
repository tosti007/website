from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Numeric
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Lid import Lid
class Boekverkoop(Entiteit):
    __tablename__ = "Boekverkoop"
    verkoopdag = Column(Date, primary_key=True, nullable=False)
    volgnummer = Column(Integer, primary_key=True, nullable=False)
    opener_contactID = Column(Integer, nullable=False)
    opener = relationship("Lid", foreign_keys=[opener_contactID])
    open = Column(DateTime, nullable=False)
    sluiter_contactID = Column(Integer, nullable=True)
    sluiter = relationship("Lid", foreign_keys=[sluiter_contactID])
    pin1 = Column(Numeric(8, 2), server_default="0", nullable=False)
    pin2 = Column(Numeric(8, 2), server_default="0", nullable=False)
    kasverschil = Column(Numeric(8, 2), server_default="0", nullable=False)
    gesloten = Column(DateTime, nullable=True)
    opmerking = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Boekverkoop"}
    __table_args__ = (ForeignKeyConstraint(["opener_contactID"], ["Lid.contactID"], name="Boekverkoop_Opener"), ForeignKeyConstraint(["sluiter_contactID"], ["Lid.contactID"], name="Boekverkoop_Sluiter"),)
