from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class ActiviteitCategorie(Entiteit):
    __tablename__ = "ActiviteitCategorie"
    actCategorieID = Column(Integer, primary_key=True, nullable=False)
    titel_NL = Column(String(255), nullable=False)
    titel_EN = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ActiviteitCategorie"}
