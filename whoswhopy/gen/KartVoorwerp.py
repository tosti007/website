from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
from whoswhopy.gen.Fotoweb.Media import Media
from whoswhopy.gen.Persoon import Persoon
from whoswhopy.gen.PersoonKart import PersoonKart
class KartVoorwerp(Entiteit):
    __tablename__ = "KartVoorwerp"
    kartVoorwerpID = Column(Integer, primary_key=True, nullable=False)
    persoonKart_persoon_contactID = Column(Integer, nullable=False)
    persoonKart_naam = Column(String(255), nullable=False)
    persoonKart = relationship("PersoonKart", foreign_keys=[persoonKart_persoon_contactID, persoonKart_naam])
    persoon_contactID = Column(Integer, nullable=True)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    commissie_commissieID = Column(Integer, nullable=True)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    media_mediaID = Column(Integer, nullable=True)
    media = relationship("Media", foreign_keys=[media_mediaID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "KartVoorwerp"}
    __table_args__ = (ForeignKeyConstraint(["persoonKart_persoon_contactID", "persoonKart_naam"], ["PersoonKart.persoon_contactID", "PersoonKart.naam"], name="KartVoorwerp_PersoonKart"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="KartVoorwerp_Persoon"), ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="KartVoorwerp_Commissie"), ForeignKeyConstraint(["media_mediaID"], ["Media.mediaID"], name="KartVoorwerp_Media"),)
