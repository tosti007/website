from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class Wachtwoord(Entiteit):
    __tablename__ = "Wachtwoord"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    login = Column(String(255), nullable=False)
    wachtwoord = Column(String(255), nullable=True)
    laatsteLogin = Column(Date, nullable=True)
    magic = Column(String(255), nullable=True)
    magicExpire = Column(DateTime, nullable=True)
    crypthash = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Wachtwoord"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Wachtwoord_Persoon"),)
