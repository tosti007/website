from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Contact import Contact
from whoswhopy.gen.Persoon import Persoon
class Lid(Persoon):
    __tablename__ = "Lid"
    contactID = Column(Integer, primary_key=True, nullable=False)
    lidToestand = Column(Enum("BIJNALID", "LID", "OUDLID", "BAL", "LIDVANVERDIENSTE", "ERELID", "GESCHORST", "BEESTJE", "INGESCHREVEN"), nullable=False)
    lidMaster = Column(Boolean, nullable=False)
    lidExchange = Column(Boolean, nullable=False)
    lidVan = Column(Date, nullable=True)
    lidTot = Column(Date, nullable=True)
    studentnr = Column(String(255), nullable=True)
    opzoekbaar = Column(Enum("A", "J", "N", "CIE"), server_default="N", nullable=False)
    inAlmanak = Column(Boolean, server_default="true", nullable=False)
    planetRSS = Column(String(255), nullable=True)
    __mapper_args__ = {'polymorphic_identity': "Lid", 'inherit_condition': contactID == Contact.contactID}
    __table_args__ = (ForeignKeyConstraint(["contactID"], ["Contact.contactID"], name="Lid_ContactID"),)
