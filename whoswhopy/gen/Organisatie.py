from sqlalchemy import Column
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Contact import Contact
class Organisatie(Contact):
    __tablename__ = "Organisatie"
    contactID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    soort = Column(Enum("ZUS_UU", "ZUS_NL", "OG_MED", "UU", "VAKGROEP", "BEDRIJF", "OVERIG"), nullable=False)
    omschrijving = Column(String(255), nullable=True)
    logo = Column(String(255), nullable=True)
    __mapper_args__ = {'polymorphic_identity': "Organisatie", 'inherit_condition': contactID == Contact.contactID}
    __table_args__ = (ForeignKeyConstraint(["contactID"], ["Contact.contactID"], name="Organisatie_ContactID"),)
