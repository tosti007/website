from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class DocuCategorie(Entiteit):
    __tablename__ = "DocuCategorie"
    id = Column(Integer, primary_key=True, nullable=False)
    omschrijving = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "DocuCategorie"}
