from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
from whoswhopy.gen.Basis.Entiteit import Entiteit
class ActiviteitVraag(Entiteit):
    __tablename__ = "ActiviteitVraag"
    activiteit_activiteitID = Column(Integer, primary_key=True, nullable=False)
    activiteit = relationship("Activiteit", foreign_keys=[activiteit_activiteitID])
    vraagID = Column(Integer, primary_key=True, nullable=False)
    type = Column(Enum("STRING", "ENUM"), nullable=False)
    vraag_NL = Column(String(255), nullable=True)
    vraag_EN = Column(String(255), nullable=True)
    opties = Column(Text, nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ActiviteitVraag"}
    __table_args__ = (ForeignKeyConstraint(["activiteit_activiteitID"], ["Activiteit.activiteitID"], name="ActiviteitVraag_Activiteit"),)
