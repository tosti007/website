from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
class BugCategorie(Entiteit):
    __tablename__ = "BugCategorie"
    bugCategorieID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    commissie_commissieID = Column(Integer, nullable=False)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    actief = Column(Boolean, server_default="1", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "BugCategorie"}
    __table_args__ = (ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="BugCategorie_Commissie"),)
