from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Bugweb.BugCategorie import BugCategorie
from whoswhopy.gen.Persoon import Persoon
class Bug(Entiteit):
    __tablename__ = "Bug"
    bugID = Column(Integer, primary_key=True, nullable=False)
    melder_contactID = Column(Integer, nullable=True)
    melder = relationship("Persoon", foreign_keys=[melder_contactID])
    melderEmail = Column(String(255), nullable=True)
    titel = Column(String(255), nullable=False)
    categorie_bugCategorieID = Column(Integer, nullable=False)
    categorie = relationship("BugCategorie", foreign_keys=[categorie_bugCategorieID])
    status = Column(Enum("OPEN", "BEZIG", "OPGELOST", "GECOMMIT", "UITGESTELD", "GESLOTEN", "BOGUS", "DUBBEL", "WANNAHAVE", "FEEDBACK", "WONTFIX"), nullable=False)
    prioriteit = Column(Enum("ONBENULLIG", "LAAG", "GEMIDDELD", "HOOG", "URGENT"), nullable=False)
    niveau = Column(Enum("OPEN", "INGELOGD", "CIE"), nullable=False)
    moment = Column(DateTime, nullable=False)
    type = Column(String(255), nullable=False)
    level = Column(Enum("BEGINNER", "GEMIDDELD", "EXPERT"), server_default="GEMIDDELD", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Bug"}
    __table_args__ = (ForeignKeyConstraint(["melder_contactID"], ["Persoon.contactID"], name="Bug_Melder"), ForeignKeyConstraint(["categorie_bugCategorieID"], ["BugCategorie.bugCategorieID"], name="Bug_Categorie"),)
