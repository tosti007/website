from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Bugweb.Bug import Bug
from whoswhopy.gen.Bugweb.BugCategorie import BugCategorie
from whoswhopy.gen.Persoon import Persoon
class BugBericht(Entiteit):
    __tablename__ = "BugBericht"
    bugBerichtID = Column(Integer, primary_key=True, nullable=False)
    bug_bugID = Column(Integer, nullable=False)
    bug = relationship("Bug", foreign_keys=[bug_bugID])
    melder_contactID = Column(Integer, nullable=True)
    melder = relationship("Persoon", foreign_keys=[melder_contactID])
    melderEmail = Column(String(255), nullable=True)
    titel = Column(String(255), nullable=True)
    categorie_bugCategorieID = Column(Integer, nullable=True)
    categorie = relationship("BugCategorie", foreign_keys=[categorie_bugCategorieID])
    status = Column(Enum("OPEN", "BEZIG", "OPGELOST", "GECOMMIT", "UITGESTELD", "GESLOTEN", "BOGUS", "DUBBEL", "WANNAHAVE", "FEEDBACK", "WONTFIX"), nullable=False)
    prioriteit = Column(Enum("ONBENULLIG", "LAAG", "GEMIDDELD", "HOOG", "URGENT"), nullable=False)
    niveau = Column(Enum("OPEN", "INGELOGD", "CIE"), nullable=False)
    bericht = Column(Text, nullable=False)
    moment = Column(DateTime, nullable=False)
    revisie = Column(Integer, nullable=True)
    level = Column(Enum("BEGINNER", "GEMIDDELD", "EXPERT"), server_default="GEMIDDELD", nullable=False)
    commit = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "BugBericht"}
    __table_args__ = (ForeignKeyConstraint(["bug_bugID"], ["Bug.bugID"], name="BugBericht_Bug"), ForeignKeyConstraint(["melder_contactID"], ["Persoon.contactID"], name="BugBericht_Melder"), ForeignKeyConstraint(["categorie_bugCategorieID"], ["BugCategorie.bugCategorieID"], name="BugBericht_Categorie"),)
