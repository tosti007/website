from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Bugweb.Bug import Bug
from whoswhopy.gen.Persoon import Persoon
class BugVolger(Entiteit):
    __tablename__ = "BugVolger"
    bug_bugID = Column(Integer, primary_key=True, nullable=False)
    bug = relationship("Bug", foreign_keys=[bug_bugID])
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "BugVolger"}
    __table_args__ = (ForeignKeyConstraint(["bug_bugID"], ["Bug.bugID"], name="BugVolger_Bug"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="BugVolger_Persoon"),)
