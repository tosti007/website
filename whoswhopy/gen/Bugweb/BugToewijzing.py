from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Bugweb.Bug import Bug
from whoswhopy.gen.Bugweb.BugBericht import BugBericht
from whoswhopy.gen.Persoon import Persoon
class BugToewijzing(Entiteit):
    __tablename__ = "BugToewijzing"
    bugToewijzingID = Column(Integer, primary_key=True, nullable=False)
    bug_bugID = Column(Integer, nullable=False)
    bug = relationship("Bug", foreign_keys=[bug_bugID])
    persoon_contactID = Column(Integer, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    begin_bugBerichtID = Column(Integer, nullable=True)
    begin = relationship("BugBericht", foreign_keys=[begin_bugBerichtID])
    eind_bugBerichtID = Column(Integer, nullable=True)
    eind = relationship("BugBericht", foreign_keys=[eind_bugBerichtID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "BugToewijzing"}
    __table_args__ = (ForeignKeyConstraint(["bug_bugID"], ["Bug.bugID"], name="BugToewijzing_Bug"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="BugToewijzing_Persoon"), ForeignKeyConstraint(["begin_bugBerichtID"], ["BugBericht.bugBerichtID"], name="BugToewijzing_Begin"), ForeignKeyConstraint(["eind_bugBerichtID"], ["BugBericht.bugBerichtID"], name="BugToewijzing_Eind"),)
