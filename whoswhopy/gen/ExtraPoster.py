from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class ExtraPoster(Entiteit):
    __tablename__ = "ExtraPoster"
    extraPosterID = Column(Integer, primary_key=True, nullable=False)
    naam = Column(String(255), nullable=False)
    beschrijving = Column(String(255), nullable=False)
    zichtbaar = Column(Boolean, server_default="1", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ExtraPoster"}
