from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
from whoswhopy.gen.Tentamen import Tentamen
class TentamenUitwerking(Entiteit):
    __tablename__ = "TentamenUitwerking"
    uitwerkingID = Column(Integer, primary_key=True, nullable=False)
    tentamen_id = Column(Integer, nullable=False)
    tentamen = relationship("Tentamen", foreign_keys=[tentamen_id])
    uploader_contactID = Column(Integer, nullable=False)
    uploader = relationship("Persoon", foreign_keys=[uploader_contactID])
    wanneer = Column(DateTime, nullable=False)
    opmerking = Column(Text, nullable=True)
    gecontroleerd = Column(Boolean, server_default="false", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "TentamenUitwerking"}
    __table_args__ = (ForeignKeyConstraint(["tentamen_id"], ["Tentamen.id"], name="TentamenUitwerking_Tentamen"), ForeignKeyConstraint(["uploader_contactID"], ["Persoon.contactID"], name="TentamenUitwerking_Uploader"),)
