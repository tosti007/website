from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Organisatie import Organisatie
from whoswhopy.gen.Persoon import Persoon
class ContactPersoon(Entiteit):
    __tablename__ = "ContactPersoon"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    organisatie_contactID = Column(Integer, primary_key=True, nullable=False)
    organisatie = relationship("Organisatie", foreign_keys=[organisatie_contactID])
    type = Column(Enum("NORMAAL", "SPOOK", "BEDRIJFSCONTACT", "OVERIGSPOOKWEB", "ALUMNUS"), primary_key=True, server_default="NORMAAL", nullable=False)
    Functie = Column(String(255), nullable=True)
    beginDatum = Column(Date, nullable=True)
    eindDatum = Column(Date, nullable=True)
    opmerking = Column(String(255), nullable=True)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ContactPersoon"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="ContactPersoon_Persoon"), ForeignKeyConstraint(["organisatie_contactID"], ["Organisatie.contactID"], name="ContactPersoon_Organisatie"),)
