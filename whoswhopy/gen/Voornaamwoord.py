from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Voornaamwoord(Entiteit):
    __tablename__ = "Voornaamwoord"
    woordID = Column(Integer, primary_key=True, nullable=False)
    soort_NL = Column(String(255), nullable=False)
    soort_EN = Column(String(255), nullable=False)
    onderwerp_NL = Column(String(255), nullable=False)
    onderwerp_EN = Column(String(255), nullable=False)
    voorwerp_NL = Column(String(255), nullable=False)
    voorwerp_EN = Column(String(255), nullable=False)
    bezittelijk_NL = Column(String(255), nullable=False)
    bezittelijk_EN = Column(String(255), nullable=False)
    wederkerend_NL = Column(String(255), nullable=False)
    wederkerend_EN = Column(String(255), nullable=False)
    ouder_NL = Column(String(255), nullable=False)
    ouder_EN = Column(String(255), nullable=False)
    kind_NL = Column(String(255), nullable=False)
    kind_EN = Column(String(255), nullable=False)
    brusje_NL = Column(String(255), nullable=False)
    brusje_EN = Column(String(255), nullable=False)
    kozijn_NL = Column(String(255), nullable=False)
    kozijn_EN = Column(String(255), nullable=False)
    kindGrootouder_NL = Column(String(255), nullable=False)
    kindGrootouder_EN = Column(String(255), nullable=False)
    kleinkindOuder_NL = Column(String(255), nullable=False)
    kleinkindOuder_EN = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Voornaamwoord"}
