from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.TinyUrl import TinyUrl
class TinyUrlHit(Entiteit):
    __tablename__ = "TinyUrlHit"
    id = Column(Integer, primary_key=True, nullable=False)
    tinyUrl_id = Column(Integer, nullable=False)
    tinyUrl = relationship("TinyUrl", foreign_keys=[tinyUrl_id])
    host = Column(String(255), nullable=True)
    hitTime = Column(DateTime, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "TinyUrlHit"}
    __table_args__ = (ForeignKeyConstraint(["tinyUrl_id"], ["TinyUrl.id"], name="TinyUrlHit_TinyUrl"),)
