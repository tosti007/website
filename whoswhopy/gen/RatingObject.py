from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from whoswhopy.gen.Basis.Entiteit import Entiteit
class RatingObject(Entiteit):
    __tablename__ = "RatingObject"
    ratingObjectID = Column(Integer, primary_key=True, nullable=False)
    rating = Column(Integer, server_default="50", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "RatingObject"}
