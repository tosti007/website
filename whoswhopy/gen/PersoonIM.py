from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class PersoonIM(Entiteit):
    __tablename__ = "PersoonIM"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    soort = Column(Enum("ICQ", "MSN", "JABBER", "AOL", "YAHOO!"), primary_key=True, nullable=False)
    accountName = Column(String(255), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "PersoonIM"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="PersoonIM_Persoon"),)
