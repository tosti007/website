from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class Planner(Entiteit):
    __tablename__ = "Planner"
    plannerID = Column(Integer, primary_key=True, nullable=False)
    persoon_contactID = Column(Integer, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    titel = Column(String(255), nullable=False)
    omschrijving = Column(Text, nullable=False)
    mail = Column(Boolean, server_default="False", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Planner"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Planner_Persoon"),)
