from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
from whoswhopy.gen.RatingObject import RatingObject
class PersoonRating(Entiteit):
    __tablename__ = "PersoonRating"
    ratingObject_ratingObjectID = Column(Integer, primary_key=True, nullable=False)
    ratingObject = relationship("RatingObject", foreign_keys=[ratingObject_ratingObjectID])
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    rating = Column(Enum("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "PersoonRating"}
    __table_args__ = (ForeignKeyConstraint(["ratingObject_ratingObjectID"], ["RatingObject.ratingObjectID"], name="PersoonRating_RatingObject"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="PersoonRating_Persoon"),)
