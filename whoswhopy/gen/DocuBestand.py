from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.DocuCategorie import DocuCategorie
class DocuBestand(Entiteit):
    __tablename__ = "DocuBestand"
    id = Column(Integer, primary_key=True, nullable=False)
    titel = Column(String(255), nullable=False)
    extensie = Column(String(255), nullable=False)
    categorie_id = Column(Integer, nullable=False)
    categorie = relationship("DocuCategorie", foreign_keys=[categorie_id])
    datum = Column(DateTime, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "DocuBestand"}
    __table_args__ = (ForeignKeyConstraint(["categorie_id"], ["DocuCategorie.id"], name="DocuBestand_Categorie"),)
