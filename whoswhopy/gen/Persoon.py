from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Contact import Contact
from whoswhopy.gen.Voornaamwoord import Voornaamwoord
class Persoon(Contact):
    __tablename__ = "Persoon"
    contactID = Column(Integer, primary_key=True, nullable=False)
    voornaam = Column(String(255), nullable=True)
    bijnaam = Column(String(255), nullable=True)
    voorletters = Column(String(255), nullable=True)
    geboortenamen = Column(String(255), nullable=True)
    tussenvoegsels = Column(String(255), nullable=True)
    achternaam = Column(String(255), nullable=False)
    titelsPrefix = Column(String(255), nullable=True)
    titelsPostfix = Column(String(255), nullable=True)
    geslacht = Column(Enum("M", "V", "?"), nullable=True)
    datumGeboorte = Column(Date, nullable=True)
    overleden = Column(Boolean, server_default="false", nullable=False)
    GPGkey = Column(String(255), nullable=True)
    Rekeningnummer = Column(String(255), nullable=True)
    opmerkingen = Column(Text, nullable=True)
    voornaamwoord_woordID = Column(Integer, nullable=False)
    voornaamwoord = relationship("Voornaamwoord", foreign_keys=[voornaamwoord_woordID])
    voornaamwoordZichtbaar = Column(Boolean, server_default="true", nullable=False)
    __mapper_args__ = {'polymorphic_identity': "Persoon", 'inherit_condition': contactID == Contact.contactID}
    __table_args__ = (ForeignKeyConstraint(["voornaamwoord_woordID"], ["Voornaamwoord.woordID"], name="Persoon_Voornaamwoord"), ForeignKeyConstraint(["contactID"], ["Contact.contactID"], name="Persoon_ContactID"),)
