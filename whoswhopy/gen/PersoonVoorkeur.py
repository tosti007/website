from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Bugweb.BugCategorie import BugCategorie
from whoswhopy.gen.Persoon import Persoon
class PersoonVoorkeur(Entiteit):
    __tablename__ = "PersoonVoorkeur"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    taal = Column(Enum("GEEN", "NL", "EN"), server_default="GEEN", nullable=False)
    bugSortering = Column(Enum("ASC", "DESC"), server_default="ASC", nullable=False)
    favoBugCategorie_bugCategorieID = Column(Integer, nullable=True)
    favoBugCategorie = relationship("BugCategorie", foreign_keys=[favoBugCategorie_bugCategorieID])
    tourAfgemaakt = Column(Boolean, server_default="false", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "PersoonVoorkeur"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="PersoonVoorkeur_Persoon"), ForeignKeyConstraint(["favoBugCategorie_bugCategorieID"], ["BugCategorie.bugCategorieID"], name="PersoonVoorkeur_FavoBugCategorie"),)
