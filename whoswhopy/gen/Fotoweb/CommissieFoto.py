from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
from whoswhopy.gen.Fotoweb.Media import Media
class CommissieFoto(Entiteit):
    __tablename__ = "CommissieFoto"
    commissie_commissieID = Column(Integer, primary_key=True, nullable=False)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    media_mediaID = Column(Integer, primary_key=True, nullable=False)
    media = relationship("Media", foreign_keys=[media_mediaID])
    status = Column(Enum("OUD", "HUIDIG"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "CommissieFoto"}
    __table_args__ = (ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="CommissieFoto_Commissie"), ForeignKeyConstraint(["media_mediaID"], ["Media.mediaID"], name="CommissieFoto_Media"),)
