from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy.orm import relationship
from whoswhopy.gen.Activiteit import Activiteit
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Persoon import Persoon
class Media(Entiteit):
    __tablename__ = "Media"
    mediaID = Column(Integer, primary_key=True, nullable=False)
    activiteit_activiteitID = Column(Integer, nullable=True)
    activiteit = relationship("Activiteit", foreign_keys=[activiteit_activiteitID])
    uploader_contactID = Column(Integer, nullable=True)
    uploader = relationship("Persoon", foreign_keys=[uploader_contactID])
    fotograaf_contactID = Column(Integer, nullable=True)
    fotograaf = relationship("Persoon", foreign_keys=[fotograaf_contactID])
    soort = Column(Enum("FOTO", "FILM"), nullable=False)
    gemaakt = Column(DateTime, nullable=True)
    licence = Column(Enum("CC3.0-BY-NC-SA", "CC3.0-BY-NC-SA-AES2", "CC3.0-BY-NC", "CC3.0-BY-ND-AES2"), nullable=False)
    premium = Column(Boolean, server_default="false", nullable=False)
    views = Column(Integer, nullable=False)
    lock = Column(Boolean, server_default="false", nullable=False)
    rating = Column(Integer, server_default="50", nullable=False)
    omschrijving = Column(Text, nullable=True)
    breedte = Column(Integer, nullable=False)
    hoogte = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Media"}
    __table_args__ = (ForeignKeyConstraint(["activiteit_activiteitID"], ["Activiteit.activiteitID"], name="Media_Activiteit"), ForeignKeyConstraint(["uploader_contactID"], ["Persoon.contactID"], name="Media_Uploader"), ForeignKeyConstraint(["fotograaf_contactID"], ["Persoon.contactID"], name="Media_Fotograaf"),)
