from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Fotoweb.Tag import Tag
class TagArea(Entiteit):
    __tablename__ = "TagArea"
    tag_tagID = Column(Integer, primary_key=True, nullable=False)
    tag = relationship("Tag", foreign_keys=[tag_tagID])
    beginPixelX = Column(Integer, nullable=False)
    eindPixelX = Column(Integer, nullable=False)
    beginPixelY = Column(Integer, nullable=False)
    eindPixelY = Column(Integer, nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "TagArea"}
    __table_args__ = (ForeignKeyConstraint(["tag_tagID"], ["Tag.tagID"], name="TagArea_Tag"),)
