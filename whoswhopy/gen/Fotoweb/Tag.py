from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Commissie import Commissie
from whoswhopy.gen.Fotoweb.Collectie import Collectie
from whoswhopy.gen.Fotoweb.Media import Media
from whoswhopy.gen.Intro.IntroGroep import IntroGroep
from whoswhopy.gen.Persoon import Persoon
class Tag(Entiteit):
    __tablename__ = "Tag"
    tagID = Column(Integer, primary_key=True, nullable=False)
    media_mediaID = Column(Integer, nullable=False)
    media = relationship("Media", foreign_keys=[media_mediaID])
    persoon_contactID = Column(Integer, nullable=True)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    commissie_commissieID = Column(Integer, nullable=True)
    commissie = relationship("Commissie", foreign_keys=[commissie_commissieID])
    introgroep_groepID = Column(Integer, nullable=True)
    introgroep = relationship("IntroGroep", foreign_keys=[introgroep_groepID])
    collectie_collectieID = Column(Integer, nullable=True)
    collectie = relationship("Collectie", foreign_keys=[collectie_collectieID])
    tagger_contactID = Column(Integer, nullable=False)
    tagger = relationship("Persoon", foreign_keys=[tagger_contactID])
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Tag"}
    __table_args__ = (ForeignKeyConstraint(["media_mediaID"], ["Media.mediaID"], name="Tag_Media"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Tag_Persoon"), ForeignKeyConstraint(["commissie_commissieID"], ["Commissie.commissieID"], name="Tag_Commissie"), ForeignKeyConstraint(["introgroep_groepID"], ["IntroGroep.groepID"], name="Tag_Introgroep"), ForeignKeyConstraint(["collectie_collectieID"], ["Collectie.collectieID"], name="Tag_Collectie"), ForeignKeyConstraint(["tagger_contactID"], ["Persoon.contactID"], name="Tag_Tagger"),)
