from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
class Collectie(Entiteit):
    __tablename__ = "Collectie"
    collectieID = Column(Integer, primary_key=True, nullable=False)
    parentCollectie_collectieID = Column(Integer, nullable=True)
    parentCollectie = relationship("Collectie", foreign_keys=[parentCollectie_collectieID])
    naam = Column(String(255), nullable=False)
    omschrijving = Column(String(255), nullable=True)
    actief = Column(Boolean, server_default="true", nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Collectie"}
    __table_args__ = (ForeignKeyConstraint(["parentCollectie_collectieID"], ["Collectie.collectieID"], name="Collectie_ParentCollectie"),)
