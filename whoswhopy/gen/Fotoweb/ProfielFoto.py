from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Fotoweb.Media import Media
from whoswhopy.gen.Persoon import Persoon
class ProfielFoto(Entiteit):
    __tablename__ = "ProfielFoto"
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    media_mediaID = Column(Integer, primary_key=True, nullable=False)
    media = relationship("Media", foreign_keys=[media_mediaID])
    status = Column(Enum("OUD", "HUIDIG", "NIEUW"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "ProfielFoto"}
    __table_args__ = (ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="ProfielFoto_Persoon"), ForeignKeyConstraint(["media_mediaID"], ["Media.mediaID"], name="ProfielFoto_Media"),)
