from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Enum
from sqlalchemy import ForeignKey
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from whoswhopy.gen.Basis.Entiteit import Entiteit
from whoswhopy.gen.Fotoweb.Media import Media
from whoswhopy.gen.Persoon import Persoon
class Rating(Entiteit):
    __tablename__ = "Rating"
    media_mediaID = Column(Integer, primary_key=True, nullable=False)
    media = relationship("Media", foreign_keys=[media_mediaID])
    persoon_contactID = Column(Integer, primary_key=True, nullable=False)
    persoon = relationship("Persoon", foreign_keys=[persoon_contactID])
    rating = Column(Enum("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), nullable=False)
    gewijzigdWie = Column(Integer)
    gewijzigdWanneer = Column(DateTime)
    __mapper_args__ = {'polymorphic_identity': "Rating"}
    __table_args__ = (ForeignKeyConstraint(["media_mediaID"], ["Media.mediaID"], name="Rating_Media"), ForeignKeyConstraint(["persoon_contactID"], ["Persoon.contactID"], name="Rating_Persoon"),)
