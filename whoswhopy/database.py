"""whoswhopy.database - Maak een databaseverbinding."""

import sqlalchemy
import sqlalchemy.orm

import whoswhopy.auth as auth
import whoswhopy.config as config

# Maak verbinding met de WhosWho4-database.
_db_user, _db_pass = auth.geef_db_auth()
wsw4db_engine = sqlalchemy.create_engine(config.geef('whoswho4_db_uri').format(
    username=_db_user,
    password=_db_pass,
    whoswho4_db=config.geef('whoswho4_db'),
))
# Maak een sessie en begin die meteen.
wsw4db = sqlalchemy.orm.sessionmaker(bind=wsw4db_engine)()
