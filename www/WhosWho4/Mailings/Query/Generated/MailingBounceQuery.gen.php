<?
abstract class MailingBounceQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de MailingBounceQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een MailingBounceQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw MailingBounceQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return MailingBounceQuery
	 * Het MailingBounceQuery-object waarvan meteen de db-table van het
	 * MailingBounce-object als table geset is.
	 */
	static public function table()
	{
		$query = new MailingBounceQuery();

		$query->tables('MailingBounce');

		return $query;
	}

	/**
	 * @brief Maakt een MailingBounceVerzameling aan van de objecten die geselecteerd
	 * worden door de MailingBounceQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return MailingBounceVerzameling
	 * Een MailingBounceVerzameling verkregen door de query
	 */
	public function verzamel($object = 'MailingBounce', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een MailingBounce-iterator aan van de objecten die geselecteerd
	 * worden door de MailingBounceQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een MailingBounceVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'MailingBounce', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een MailingBounce die geslecteeerd wordt door de MailingBounceQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return MailingBounceVerzameling
	 * Een MailingBounceVerzameling verkregen door de query.
	 */
	public function geef($object = 'MailingBounce')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van MailingBounce.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'mailingBounceID',
			'mailing_mailingID',
			'contact_contactID',
			'bounceContent',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'MailingBounce';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als MailingBounce een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan MailingBounce een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('MailingBounce'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Mailing' => array(
				'mailing_mailingID' => 'mailingID'
			),
			'Contact' => array(
				'contact_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van MailingBounce.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'MailingBounce.mailingBounceID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'mailingbounceid':
			$type = 'int';
			$field = 'mailingBounceID';
			break;
		case 'mailing':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Mailing; })))
					user_error('Alle waarden in de array moeten van type Mailing zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Mailing) && !($value instanceof MailingVerzameling) && !is_null($value))
				user_error('Value moet van type Mailing of NULL zijn', E_USER_ERROR);
			$field = 'mailing_mailingID';
			if(is_array($value) || $value instanceof MailingVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMailingID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getMailingID();
			$type = 'int';
			break;
		case 'contact':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Contact; })))
					user_error('Alle waarden in de array moeten van type Contact zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Contact) && !($value instanceof ContactVerzameling))
				user_error('Value moet van type Contact zijn', E_USER_ERROR);
			$field = 'contact_contactID';
			if(is_array($value) || $value instanceof ContactVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'bouncecontent':
			$type = 'string';
			$field = 'bounceContent';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'MailingBounce.'.$v;
			}
		} else {
			$field = 'MailingBounce.'.$field;
		}

		return array($field, $value, $type);
	}

}
