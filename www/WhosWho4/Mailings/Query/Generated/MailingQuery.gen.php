<?
abstract class MailingQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de MailingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een MailingQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw MailingQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return MailingQuery
	 * Het MailingQuery-object waarvan meteen de db-table van het Mailing-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new MailingQuery();

		$query->tables('Mailing');

		return $query;
	}

	/**
	 * @brief Maakt een MailingVerzameling aan van de objecten die geselecteerd worden
	 * door de MailingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return MailingVerzameling
	 * Een MailingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Mailing', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Mailing-iterator aan van de objecten die geselecteerd worden
	 * door de MailingQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een MailingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Mailing', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Mailing die geslecteeerd wordt door de MailingQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return MailingVerzameling
	 * Een MailingVerzameling verkregen door de query.
	 */
	public function geef($object = 'Mailing')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Mailing.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'mailingID',
			'commissie_commissieID',
			'omschrijving',
			'momentIngepland',
			'momentVerwerkt',
			'mailHeaders',
			'mailSubject',
			'mailFrom',
			'isDefinitief',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Mailing';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Mailing een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Mailing een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Mailing'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Mailing.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Mailing.mailingID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'mailingid':
			$type = 'int';
			$field = 'mailingID';
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling) && !is_null($value))
				user_error('Value moet van type Commissie of NULL zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'momentingepland':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'momentIngepland';
			break;
		case 'momentverwerkt':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'momentVerwerkt';
			break;
		case 'mailheaders':
			$type = 'string';
			$field = 'mailHeaders';
			break;
		case 'mailsubject':
			$type = 'string';
			$field = 'mailSubject';
			break;
		case 'mailfrom':
			$type = 'string';
			$field = 'mailFrom';
			break;
		case 'isdefinitief':
			$type = 'int';
			$field = 'isDefinitief';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Mailing.'.$v;
			}
		} else {
			$field = 'Mailing.'.$field;
		}

		return array($field, $value, $type);
	}

}
