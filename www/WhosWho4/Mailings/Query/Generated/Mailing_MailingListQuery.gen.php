<?
abstract class Mailing_MailingListQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de Mailing_MailingListQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een Mailing_MailingListQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw
	 * Mailing_MailingListQuery-object aan om er zeker van te zijn dat we alle
	 * eigenschappen daarvan ook meenemen.
	 *
	 * @return Mailing_MailingListQuery
	 * Het Mailing_MailingListQuery-object waarvan meteen de db-table van het
	 * Mailing_MailingList-object als table geset is.
	 */
	static public function table()
	{
		$query = new Mailing_MailingListQuery();

		$query->tables('Mailing_MailingList');

		return $query;
	}

	/**
	 * @brief Maakt een Mailing_MailingListVerzameling aan van de objecten die
	 * geselecteerd worden door de Mailing_MailingListQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Mailing_MailingListVerzameling
	 * Een Mailing_MailingListVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Mailing_MailingList', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Mailing_MailingList-iterator aan van de objecten die
	 * geselecteerd worden door de Mailing_MailingListQuery uit te voeren met een SQL
	 * cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een Mailing_MailingListVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Mailing_MailingList', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Mailing_MailingList die geslecteeerd wordt door de
	 * Mailing_MailingListQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return Mailing_MailingListVerzameling
	 * Een Mailing_MailingListVerzameling verkregen door de query.
	 */
	public function geef($object = 'Mailing_MailingList')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Mailing_MailingList.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'mailing_mailingID',
			'mailingList_mailingListID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Mailing_MailingList';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Mailing_MailingList
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Mailing_MailingList een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Mailing_MailingList'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Mailing' => array(
				'mailing_mailingID' => 'mailingID'
			),
			'MailingList' => array(
				'mailingList_mailingListID' => 'mailingListID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Mailing_MailingList.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Mailing_MailingList.mailing_mailingID',
			'Mailing_MailingList.mailingList_mailingListID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'mailing':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Mailing; })))
					user_error('Alle waarden in de array moeten van type Mailing zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Mailing) && !($value instanceof MailingVerzameling))
				user_error('Value moet van type Mailing zijn', E_USER_ERROR);
			$field = 'mailing_mailingID';
			if(is_array($value) || $value instanceof MailingVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMailingID();
				$value = $new_value;
			}
			else $value = $value->getMailingID();
			$type = 'int';
			break;
		case 'mailinglist':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof MailingList; })))
					user_error('Alle waarden in de array moeten van type MailingList zijn', E_USER_ERROR);
			}
			else if(!($value instanceof MailingList) && !($value instanceof MailingListVerzameling))
				user_error('Value moet van type MailingList zijn', E_USER_ERROR);
			$field = 'mailingList_mailingListID';
			if(is_array($value) || $value instanceof MailingListVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMailingListID();
				$value = $new_value;
			}
			else $value = $value->getMailingListID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Mailing_MailingList.'.$v;
			}
		} else {
			$field = 'Mailing_MailingList.'.$field;
		}

		return array($field, $value, $type);
	}

}
