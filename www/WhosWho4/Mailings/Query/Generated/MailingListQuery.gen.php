<?
abstract class MailingListQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de MailingListQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een MailingListQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw MailingListQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return MailingListQuery
	 * Het MailingListQuery-object waarvan meteen de db-table van het
	 * MailingList-object als table geset is.
	 */
	static public function table()
	{
		$query = new MailingListQuery();

		$query->tables('MailingList');

		return $query;
	}

	/**
	 * @brief Maakt een MailingListVerzameling aan van de objecten die geselecteerd
	 * worden door de MailingListQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return MailingListVerzameling
	 * Een MailingListVerzameling verkregen door de query
	 */
	public function verzamel($object = 'MailingList', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een MailingList-iterator aan van de objecten die geselecteerd
	 * worden door de MailingListQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een MailingListVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'MailingList', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een MailingList die geslecteeerd wordt door de MailingListQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return MailingListVerzameling
	 * Een MailingListVerzameling verkregen door de query.
	 */
	public function geef($object = 'MailingList')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van MailingList.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'mailingListID',
			'naam',
			'omschrijving',
			'subjectPrefix',
			'bodyPrefix',
			'bodySuffix',
			'verborgen',
			'query',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'MailingList';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als MailingList een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan MailingList een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('MailingList'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van MailingList.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'MailingList.mailingListID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'mailinglistid':
			$type = 'int';
			$field = 'mailingListID';
			break;
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'subjectprefix':
			$type = 'string';
			$field = 'subjectPrefix';
			break;
		case 'bodyprefix':
			$type = 'string';
			$field = 'bodyPrefix';
			break;
		case 'bodysuffix':
			$type = 'string';
			$field = 'bodySuffix';
			break;
		case 'verborgen':
			$type = 'int';
			$field = 'verborgen';
			break;
		case 'query':
			$type = 'string';
			$field = 'query';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'MailingList.'.$v;
			}
		} else {
			$field = 'MailingList.'.$field;
		}

		return array($field, $value, $type);
	}

}
