<?

/***
 * $Id$
 */
class MailingList
	extends MailingList_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * De functie voor het mogen verwijderen van een object
	 */
	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}

	/**
	 * Functie de een url naar de pagina van het object geeft
	 */
	public function url()
	{
		return MAILINGWEB . "Mailinglists/" . $this->geefID() .  "/";
	}

	/**
	 * Geeft alle ids van de geabonneerde leden terug
	 */
	public function abonneeLidnrs()
	{
		global $WSW4DB;

		if(!$this->getQuery()){
			//Haal alle ID's op van mensen die de mailing moeten ontvangen niet Bijna-Lid zijn
			$ids = $WSW4DB->q('COLUMN SELECT `ContactMailingList`.`contact_contactID`'
				                       . ' FROM `ContactMailingList`, `Contact`, `Lid`'
				                       . ' WHERE `ContactMailingList`.`contact_contactID`=`Contact`.`contactID` AND'
									   . ' `Contact`.`contactID` = `Lid`.`contactID` AND'
									   . ' `Lid`.`lidToestand` <> "BIJNALID" AND'
									   . ' `mailingList_mailingListID` = %i', $this->geefID());
		}
		else
			$ids = $WSW4DB->q($this->getQuery());

		return $ids;
	}

	/** METHODEN **/
	/**
	 * Retourneert een PersoonVerzameling met alle mensen die op deze mailinglist staan.
	 */
	public function abonnees()
	{
		if ($this->getMailingListID() === null) return new PersoonVerzameling();

		return ContactVerzameling::mailingListAbonnees($this);
	}

	/**
		Zeker weten dat we geen \\r\\n doen, dat wordt door sommige browsers
		gebruikt als newline en komt dus in de database terecht
	**/

	/**
	 * Haal de bodysuffix op van de mailing
	 */
	public function getBodySuffix(){
		return str_replace("\r\n", "\n", parent::getBodySuffix());
	}

	/**
	 * Haal de bodyprefix op van de mailing
	 */
	public function getBodyPrefix(){
		return str_replace("\r\n", "\n", parent::getBodyPrefix());
	}

	public function getMailings()
	{
		global $WSW4DB;
		if ($this->getMailingListID() === null) return new MailingVerzameling();

		$ids = $WSW4DB->q('COLUMN SELECT `Mailing_MailingList`.`mailing_mailingID`'
			. ' FROM `Mailing_MailingList`'
			. ' JOIN `Mailing` ON `Mailing`.`mailingID` = `Mailing_MailingList`.`mailing_mailingID`'
			. ' WHERE `Mailing_MailingList`.`mailingList_mailingListID` = %i'
			. ' ORDER BY `Mailing`.`momentIngepland` DESC', $this->geefID());

		return MailingVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
