<?
/**
 * @brief Bevat informatie over een bounce van een mailing, bijvoorbeeld als een
 * van de abonnees op de mailinglijst een incorrect e-mailadres heeft.
 */
abstract class MailingBounce_Generated
	extends Entiteit
{
	protected $mailingBounceID;			/**< \brief PRIMARY */
	protected $mailing;					/**< \brief NULL */
	protected $mailing_mailingID;		/**< \brief PRIMARY */
	protected $contact;
	protected $contact_contactID;		/**< \brief PRIMARY */
	protected $bounceContent;			/**< \brief Content van de bounce */
	/**
	/**
	 * @brief De constructor van de MailingBounce_Generated-klasse.
	 *
	 * @param mixed $a Mailing (Mailing OR Array(mailing_mailingID) OR
	 * mailing_mailingID)
	 * @param mixed $b Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Mailing)
		{
			$this->mailing = $a;
			$this->mailing_mailingID = $a->getMailingID();
		}
		else if(is_array($a))
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = (int)$a;
		}
		else
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = NULL;
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Contact)
		{
			$this->contact = $b;
			$this->contact_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->mailingBounceID = NULL;
		$this->bounceContent = '';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Mailing';
		$volgorde[] = 'Contact';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld mailingBounceID.
	 *
	 * @return int
	 * De waarde van het veld mailingBounceID.
	 */
	public function getMailingBounceID()
	{
		return $this->mailingBounceID;
	}
	/**
	 * @brief Geef de waarde van het veld mailing.
	 *
	 * @return Mailing
	 * De waarde van het veld mailing.
	 */
	public function getMailing()
	{
		if(!isset($this->mailing)
		 && isset($this->mailing_mailingID)
		 ) {
			$this->mailing = Mailing::geef
					( $this->mailing_mailingID
					);
		}
		return $this->mailing;
	}
	/**
	 * @brief Geef de waarde van het veld mailing_mailingID.
	 *
	 * @return int
	 * De waarde van het veld mailing_mailingID.
	 */
	public function getMailingMailingID()
	{
		if (is_null($this->mailing_mailingID) && isset($this->mailing)) {
			$this->mailing_mailingID = $this->mailing->getMailingID();
		}
		return $this->mailing_mailingID;
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @return Contact
	 * De waarde van het veld contact.
	 */
	public function getContact()
	{
		if(!isset($this->contact)
		 && isset($this->contact_contactID)
		 ) {
			$this->contact = Contact::geef
					( $this->contact_contactID
					);
		}
		return $this->contact;
	}
	/**
	 * @brief Geef de waarde van het veld contact_contactID.
	 *
	 * @return int
	 * De waarde van het veld contact_contactID.
	 */
	public function getContactContactID()
	{
		if (is_null($this->contact_contactID) && isset($this->contact)) {
			$this->contact_contactID = $this->contact->getContactID();
		}
		return $this->contact_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld bounceContent.
	 *
	 * @return string
	 * De waarde van het veld bounceContent.
	 */
	public function getBounceContent()
	{
		return $this->bounceContent;
	}
	/**
	 * @brief Stel de waarde van het veld bounceContent in.
	 *
	 * @param mixed $newBounceContent De nieuwe waarde.
	 *
	 * @return MailingBounce
	 * Dit MailingBounce-object.
	 */
	public function setBounceContent($newBounceContent)
	{
		unset($this->errors['BounceContent']);
		if(!is_null($newBounceContent))
			$newBounceContent = trim($newBounceContent);
		if($newBounceContent === "")
			$newBounceContent = NULL;
		if($this->bounceContent === $newBounceContent)
			return $this;

		$this->bounceContent = $newBounceContent;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bounceContent geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bounceContent geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBounceContent()
	{
		if (array_key_exists('BounceContent', $this->errors))
			return $this->errors['BounceContent'];
		$waarde = $this->getBounceContent();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return MailingBounce::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van MailingBounce.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return MailingBounce::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getMailingBounceID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return MailingBounce|false
	 * Een MailingBounce-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$mailingBounceID = (int)$a[0];
		}
		else if(isset($a))
		{
			$mailingBounceID = (int)$a;
		}

		if(is_null($mailingBounceID))
			throw new BadMethodCallException();

		static::cache(array( array($mailingBounceID) ));
		return Entiteit::geefCache(array($mailingBounceID), 'MailingBounce');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'MailingBounce');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('MailingBounce::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `MailingBounce`.`mailingBounceID`'
		                 .     ', `MailingBounce`.`mailing_mailingID`'
		                 .     ', `MailingBounce`.`contact_contactID`'
		                 .     ', `MailingBounce`.`bounceContent`'
		                 .     ', `MailingBounce`.`gewijzigdWanneer`'
		                 .     ', `MailingBounce`.`gewijzigdWie`'
		                 .' FROM `MailingBounce`'
		                 .' WHERE (`mailingBounceID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mailingBounceID']);

			$obj = new MailingBounce(array($row['mailing_mailingID']), array($row['contact_contactID']));

			$obj->inDB = True;

			$obj->mailingBounceID  = (int) $row['mailingBounceID'];
			$obj->bounceContent  = trim($row['bounceContent']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'MailingBounce')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getMailingMailingID();
		$this->getContactContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->mailingBounceID =
			$WSW4DB->q('RETURNID INSERT INTO `MailingBounce`'
			          . ' (`mailing_mailingID`, `contact_contactID`, `bounceContent`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i)'
			          , $this->mailing_mailingID
			          , $this->contact_contactID
			          , $this->bounceContent
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'MailingBounce')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `MailingBounce`'
			          .' SET `mailing_mailingID` = %i'
			          .   ', `contact_contactID` = %i'
			          .   ', `bounceContent` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailingBounceID` = %i'
			          , $this->mailing_mailingID
			          , $this->contact_contactID
			          , $this->bounceContent
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailingBounceID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMailingBounce
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMailingBounce($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMailingBounce($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'BounceContent';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'BounceContent';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Mailing';
			$velden[] = 'Contact';
			$velden[] = 'BounceContent';
			break;
		case 'get':
			$velden[] = 'Mailing';
			$velden[] = 'Contact';
			$velden[] = 'BounceContent';
		case 'primary':
			$velden[] = 'mailing_mailingID';
			$velden[] = 'contact_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Mailing';
			$velden[] = 'Contact';
			$velden[] = 'BounceContent';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `MailingBounce`'
		          .' WHERE `mailingBounceID` = %i'
		          .' LIMIT 1'
		          , $this->mailingBounceID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mailingBounceID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `MailingBounce`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailingBounceID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailingBounceID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van MailingBounce terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mailingbounceid':
			return 'int';
		case 'mailing':
			return 'foreign';
		case 'mailing_mailingid':
			return 'int';
		case 'contact':
			return 'foreign';
		case 'contact_contactid':
			return 'int';
		case 'bouncecontent':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'mailingBounceID':
		case 'mailing_mailingID':
		case 'contact_contactID':
			$type = '%i';
			break;
		case 'bounceContent':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `MailingBounce`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mailingBounceID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mailingBounceID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `MailingBounce`'
		          .' WHERE `mailingBounceID` = %i'
		                 , $veld
		          , $this->mailingBounceID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'MailingBounce');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
