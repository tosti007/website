<?
/**
 * @brief Linkt een TinyUrl aan een Mailing, zo kan je het totaal aantal hits van
 * een mailing tracken of van een mailing een mooi overzicht krijgen.
 */
abstract class TinyMailingUrl_Generated
	extends Entiteit
{
	protected $mailing;					/**< \brief PRIMARY */
	protected $mailing_mailingID;		/**< \brief PRIMARY */
	protected $tinyUrl;					/**< \brief PRIMARY */
	protected $tinyUrl_id;				/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de TinyMailingUrl_Generated-klasse.
	 *
	 * @param mixed $a Mailing (Mailing OR Array(mailing_mailingID) OR
	 * mailing_mailingID)
	 * @param mixed $b TinyUrl (TinyUrl OR Array(tinyUrl_id) OR tinyUrl_id)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Mailing)
		{
			$this->mailing = $a;
			$this->mailing_mailingID = $a->getMailingID();
		}
		else if(is_array($a))
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof TinyUrl)
		{
			$this->tinyUrl = $b;
			$this->tinyUrl_id = $b->getId();
		}
		else if(is_array($b))
		{
			$this->tinyUrl = NULL;
			$this->tinyUrl_id = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->tinyUrl = NULL;
			$this->tinyUrl_id = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Mailing';
		$volgorde[] = 'TinyUrl';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld mailing.
	 *
	 * @return Mailing
	 * De waarde van het veld mailing.
	 */
	public function getMailing()
	{
		if(!isset($this->mailing)
		 && isset($this->mailing_mailingID)
		 ) {
			$this->mailing = Mailing::geef
					( $this->mailing_mailingID
					);
		}
		return $this->mailing;
	}
	/**
	 * @brief Geef de waarde van het veld mailing_mailingID.
	 *
	 * @return int
	 * De waarde van het veld mailing_mailingID.
	 */
	public function getMailingMailingID()
	{
		if (is_null($this->mailing_mailingID) && isset($this->mailing)) {
			$this->mailing_mailingID = $this->mailing->getMailingID();
		}
		return $this->mailing_mailingID;
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl.
	 *
	 * @return TinyUrl
	 * De waarde van het veld tinyUrl.
	 */
	public function getTinyUrl()
	{
		if(!isset($this->tinyUrl)
		 && isset($this->tinyUrl_id)
		 ) {
			$this->tinyUrl = TinyUrl::geef
					( $this->tinyUrl_id
					);
		}
		return $this->tinyUrl;
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl_id.
	 *
	 * @return int
	 * De waarde van het veld tinyUrl_id.
	 */
	public function getTinyUrlId()
	{
		if (is_null($this->tinyUrl_id) && isset($this->tinyUrl)) {
			$this->tinyUrl_id = $this->tinyUrl->getId();
		}
		return $this->tinyUrl_id;
	}
	/**
	 * @brief Stel de waarde van het veld tinyUrl_id in.
	 *
	 * @param mixed $newTinyUrl_id De nieuwe waarde.
	 *
	 * @return TinyMailingUrl
	 * Dit TinyMailingUrl-object.
	 */
	public function setTinyUrl_id($newTinyUrl_id)
	{
		unset($this->errors['TinyUrl_id']);
		if(!is_null($newTinyUrl_id))
			$newTinyUrl_id = (int)$newTinyUrl_id;
		if($this->tinyUrl_id === $newTinyUrl_id)
			return $this;

		$this->tinyUrl_id = $newTinyUrl_id;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return TinyMailingUrl::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van TinyMailingUrl.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return TinyMailingUrl::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getMailingMailingID()
		                      , $this->getTinyUrlId()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return TinyMailingUrl|false
	 * Een TinyMailingUrl-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$mailing_mailingID = (int)$a[0];
			$tinyUrl_id = (int)$a[1];
		}
		else if($a instanceof Mailing
		     && $b instanceof TinyUrl)
		{
			$mailing_mailingID = $a->getMailingID();
			$tinyUrl_id = $b->getId();
		}
		else if(isset($a)
		     && isset($b))
		{
			$mailing_mailingID = (int)$a;
			$tinyUrl_id = (int)$b;
		}

		if(is_null($mailing_mailingID)
		|| is_null($tinyUrl_id))
			throw new BadMethodCallException();

		static::cache(array( array($mailing_mailingID, $tinyUrl_id) ));
		return Entiteit::geefCache(array($mailing_mailingID, $tinyUrl_id), 'TinyMailingUrl');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'TinyMailingUrl');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('TinyMailingUrl::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `TinyMailingUrl`.`mailing_mailingID`'
		                 .     ', `TinyMailingUrl`.`tinyUrl_id`'
		                 .     ', `TinyMailingUrl`.`gewijzigdWanneer`'
		                 .     ', `TinyMailingUrl`.`gewijzigdWie`'
		                 .' FROM `TinyMailingUrl`'
		                 .' WHERE (`mailing_mailingID`, `tinyUrl_id`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mailing_mailingID']
			                  ,$row['tinyUrl_id']);

			$obj = new TinyMailingUrl(array($row['mailing_mailingID']), array($row['tinyUrl_id']));

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'TinyMailingUrl')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getMailing();
		if(!$rel->getInDB())
			throw new LogicException('foreign Mailing is not in DB');
		$this->mailing_mailingID = $rel->getMailingID();

		$rel = $this->getTinyUrl();
		if(!$rel->getInDB())
			throw new LogicException('foreign TinyUrl is not in DB');
		$this->tinyUrl_id = $rel->getId();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `TinyMailingUrl`'
			          . ' (`mailing_mailingID`, `tinyUrl_id`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i)'
			          , $this->mailing_mailingID
			          , $this->tinyUrl_id
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'TinyMailingUrl')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `TinyMailingUrl`'
			.' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailing_mailingID` = %i'
			          .  ' AND `tinyUrl_id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailing_mailingID
			          , $this->tinyUrl_id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTinyMailingUrl
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTinyMailingUrl($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTinyMailingUrl($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			break;
		case 'get':
		case 'primary':
			$velden[] = 'mailing_mailingID';
			$velden[] = 'tinyUrl_id';
			break;
		case 'verzamelingen':
			break;
		default:
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `TinyMailingUrl`'
		          .' WHERE `mailing_mailingID` = %i'
		          .  ' AND `tinyUrl_id` = %i'
		          .' LIMIT 1'
		          , $this->mailing_mailingID
		          , $this->tinyUrl_id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mailing = NULL;
		$this->mailing_mailingID = NULL;
		$this->tinyUrl = NULL;
		$this->tinyUrl_id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `TinyMailingUrl`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailing_mailingID` = %i'
			          .  ' AND `tinyUrl_id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailing_mailingID
			          , $this->tinyUrl_id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van TinyMailingUrl terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mailing':
			return 'foreign';
		case 'mailing_mailingid':
			return 'int';
		case 'tinyurl':
			return 'foreign';
		case 'tinyurl_id':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `TinyMailingUrl`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mailing_mailingID` = %i'
		          .  ' AND `tinyUrl_id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mailing_mailingID
		          , $this->tinyUrl_id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `TinyMailingUrl`'
		          .' WHERE `mailing_mailingID` = %i'
		          .  ' AND `tinyUrl_id` = %i'
		                 , $veld
		          , $this->mailing_mailingID
		          , $this->tinyUrl_id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'TinyMailingUrl');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
