<?
/**
 * @brief 'AT'AUTH_GET:mailings,AUTH_SET:bestuur
 */
abstract class MailingList_Generated
	extends Entiteit
{
	protected $mailingListID;			/**< \brief Private key die deze lijst identificeert en gebruikt wordt om naar deze lijst te verwijzen bij het coden van queries. PRIMARY */
	protected $naam;					/**< \brief Korte naam van deze lijst. */
	protected $omschrijving;			/**< \brief Uitgebreide omschrijving van deze lijst. */
	protected $subjectPrefix;			/**< \brief Prefix die voor het subject van een mailing gezet wordt. NULL */
	protected $bodyPrefix;				/**< \brief Prefix voor mail body NULL */
	protected $bodySuffix;				/**< \brief Suffix voor mail body. NULL */
	protected $verborgen;
	protected $query;					/**< \brief NULL */
	/** Verzamelingen **/
	protected $mailing_MailingListVerzameling;
	protected $contactMailingListVerzameling;
	/**
	 * @brief De constructor van de MailingList_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->mailingListID = NULL;
		$this->naam = '';
		$this->omschrijving = '';
		$this->subjectPrefix = NULL;
		$this->bodyPrefix = NULL;
		$this->bodySuffix = NULL;
		$this->verborgen = False;
		$this->query = NULL;
		$this->mailing_MailingListVerzameling = NULL;
		$this->contactMailingListVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld mailingListID.
	 *
	 * @return int
	 * De waarde van het veld mailingListID.
	 */
	public function getMailingListID()
	{
		return $this->mailingListID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld subjectPrefix.
	 *
	 * @return string
	 * De waarde van het veld subjectPrefix.
	 */
	public function getSubjectPrefix()
	{
		return $this->subjectPrefix;
	}
	/**
	 * @brief Stel de waarde van het veld subjectPrefix in.
	 *
	 * @param mixed $newSubjectPrefix De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setSubjectPrefix($newSubjectPrefix)
	{
		unset($this->errors['SubjectPrefix']);
		if(!is_null($newSubjectPrefix))
			$newSubjectPrefix = trim($newSubjectPrefix);
		if($newSubjectPrefix === "")
			$newSubjectPrefix = NULL;
		if($this->subjectPrefix === $newSubjectPrefix)
			return $this;

		$this->subjectPrefix = $newSubjectPrefix;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld subjectPrefix geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld subjectPrefix geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkSubjectPrefix()
	{
		if (array_key_exists('SubjectPrefix', $this->errors))
			return $this->errors['SubjectPrefix'];
		$waarde = $this->getSubjectPrefix();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bodyPrefix.
	 *
	 * @return string
	 * De waarde van het veld bodyPrefix.
	 */
	public function getBodyPrefix()
	{
		return $this->bodyPrefix;
	}
	/**
	 * @brief Stel de waarde van het veld bodyPrefix in.
	 *
	 * @param mixed $newBodyPrefix De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setBodyPrefix($newBodyPrefix)
	{
		unset($this->errors['BodyPrefix']);
		if(!is_null($newBodyPrefix))
			$newBodyPrefix = trim($newBodyPrefix);
		if($newBodyPrefix === "")
			$newBodyPrefix = NULL;
		if($this->bodyPrefix === $newBodyPrefix)
			return $this;

		$this->bodyPrefix = $newBodyPrefix;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bodyPrefix geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bodyPrefix geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBodyPrefix()
	{
		if (array_key_exists('BodyPrefix', $this->errors))
			return $this->errors['BodyPrefix'];
		$waarde = $this->getBodyPrefix();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bodySuffix.
	 *
	 * @return string
	 * De waarde van het veld bodySuffix.
	 */
	public function getBodySuffix()
	{
		return $this->bodySuffix;
	}
	/**
	 * @brief Stel de waarde van het veld bodySuffix in.
	 *
	 * @param mixed $newBodySuffix De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setBodySuffix($newBodySuffix)
	{
		unset($this->errors['BodySuffix']);
		if(!is_null($newBodySuffix))
			$newBodySuffix = trim($newBodySuffix);
		if($newBodySuffix === "")
			$newBodySuffix = NULL;
		if($this->bodySuffix === $newBodySuffix)
			return $this;

		$this->bodySuffix = $newBodySuffix;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bodySuffix geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bodySuffix geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBodySuffix()
	{
		if (array_key_exists('BodySuffix', $this->errors))
			return $this->errors['BodySuffix'];
		$waarde = $this->getBodySuffix();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld verborgen.
	 *
	 * @return bool
	 * De waarde van het veld verborgen.
	 */
	public function getVerborgen()
	{
		return $this->verborgen;
	}
	/**
	 * @brief Stel de waarde van het veld verborgen in.
	 *
	 * @param mixed $newVerborgen De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setVerborgen($newVerborgen)
	{
		unset($this->errors['Verborgen']);
		if(!is_null($newVerborgen))
			$newVerborgen = (bool)$newVerborgen;
		if($this->verborgen === $newVerborgen)
			return $this;

		$this->verborgen = $newVerborgen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld verborgen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld verborgen geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkVerborgen()
	{
		if (array_key_exists('Verborgen', $this->errors))
			return $this->errors['Verborgen'];
		$waarde = $this->getVerborgen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld query.
	 *
	 * @return string
	 * De waarde van het veld query.
	 */
	public function getQuery()
	{
		return $this->query;
	}
	/**
	 * @brief Stel de waarde van het veld query in.
	 *
	 * @param mixed $newQuery De nieuwe waarde.
	 *
	 * @return MailingList
	 * Dit MailingList-object.
	 */
	public function setQuery($newQuery)
	{
		unset($this->errors['Query']);
		if(!is_null($newQuery))
			$newQuery = trim($newQuery);
		if($newQuery === "")
			$newQuery = NULL;
		if($this->query === $newQuery)
			return $this;

		$this->query = $newQuery;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld query geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld query geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkQuery()
	{
		if (array_key_exists('Query', $this->errors))
			return $this->errors['Query'];
		$waarde = $this->getQuery();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de Mailing_MailingListVerzameling die hoort bij dit object.
	 */
	public function getMailing_MailingListVerzameling()
	{
		if(!$this->mailing_MailingListVerzameling instanceof Mailing_MailingListVerzameling)
			$this->mailing_MailingListVerzameling = Mailing_MailingListVerzameling::fromMailingList($this);
		return $this->mailing_MailingListVerzameling;
	}
	/**
	 * @brief Returneert de ContactMailingListVerzameling die hoort bij dit object.
	 */
	public function getContactMailingListVerzameling()
	{
		if(!$this->contactMailingListVerzameling instanceof ContactMailingListVerzameling)
			$this->contactMailingListVerzameling = ContactMailingListVerzameling::fromMailingList($this);
		return $this->contactMailingListVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('mailings');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return MailingList::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van MailingList.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return MailingList::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getMailingListID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return MailingList|false
	 * Een MailingList-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$mailingListID = (int)$a[0];
		}
		else if(isset($a))
		{
			$mailingListID = (int)$a;
		}

		if(is_null($mailingListID))
			throw new BadMethodCallException();

		static::cache(array( array($mailingListID) ));
		return Entiteit::geefCache(array($mailingListID), 'MailingList');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'MailingList');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('MailingList::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `MailingList`.`mailingListID`'
		                 .     ', `MailingList`.`naam`'
		                 .     ', `MailingList`.`omschrijving`'
		                 .     ', `MailingList`.`subjectPrefix`'
		                 .     ', `MailingList`.`bodyPrefix`'
		                 .     ', `MailingList`.`bodySuffix`'
		                 .     ', `MailingList`.`verborgen`'
		                 .     ', `MailingList`.`query`'
		                 .     ', `MailingList`.`gewijzigdWanneer`'
		                 .     ', `MailingList`.`gewijzigdWie`'
		                 .' FROM `MailingList`'
		                 .' WHERE (`mailingListID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mailingListID']);

			$obj = new MailingList();

			$obj->inDB = True;

			$obj->mailingListID  = (int) $row['mailingListID'];
			$obj->naam  = trim($row['naam']);
			$obj->omschrijving  = trim($row['omschrijving']);
			$obj->subjectPrefix  = (is_null($row['subjectPrefix'])) ? null : trim($row['subjectPrefix']);
			$obj->bodyPrefix  = (is_null($row['bodyPrefix'])) ? null : trim($row['bodyPrefix']);
			$obj->bodySuffix  = (is_null($row['bodySuffix'])) ? null : trim($row['bodySuffix']);
			$obj->verborgen  = (bool) $row['verborgen'];
			$obj->query  = (is_null($row['query'])) ? null : trim($row['query']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'MailingList')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->mailingListID =
			$WSW4DB->q('RETURNID INSERT INTO `MailingList`'
			          . ' (`naam`, `omschrijving`, `subjectPrefix`, `bodyPrefix`, `bodySuffix`, `verborgen`, `query`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %s, %s, %i, %s, %s, %i)'
			          , $this->naam
			          , $this->omschrijving
			          , $this->subjectPrefix
			          , $this->bodyPrefix
			          , $this->bodySuffix
			          , $this->verborgen
			          , $this->query
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'MailingList')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `MailingList`'
			          .' SET `naam` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `subjectPrefix` = %s'
			          .   ', `bodyPrefix` = %s'
			          .   ', `bodySuffix` = %s'
			          .   ', `verborgen` = %i'
			          .   ', `query` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailingListID` = %i'
			          , $this->naam
			          , $this->omschrijving
			          , $this->subjectPrefix
			          , $this->bodyPrefix
			          , $this->bodySuffix
			          , $this->verborgen
			          , $this->query
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailingListID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMailingList
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMailingList($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMailingList($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'SubjectPrefix';
			$velden[] = 'BodyPrefix';
			$velden[] = 'BodySuffix';
			$velden[] = 'Verborgen';
			$velden[] = 'Query';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'SubjectPrefix';
			$velden[] = 'BodyPrefix';
			$velden[] = 'BodySuffix';
			$velden[] = 'Verborgen';
			$velden[] = 'Query';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'SubjectPrefix';
			$velden[] = 'BodyPrefix';
			$velden[] = 'BodySuffix';
			$velden[] = 'Verborgen';
			$velden[] = 'Query';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'Mailing_MailingListVerzameling';
			$velden[] = 'ContactMailingListVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'SubjectPrefix';
			$velden[] = 'BodyPrefix';
			$velden[] = 'BodySuffix';
			$velden[] = 'Verborgen';
			$velden[] = 'Query';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Mailing_MailingList-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$Mailing_MailingListFromMailingListVerz = Mailing_MailingListVerzameling::fromMailingList($this);
		$returnValue = $Mailing_MailingListFromMailingListVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Mailing_MailingList met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ContactMailingList-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContactMailingListFromMailingListVerz = ContactMailingListVerzameling::fromMailingList($this);
		$returnValue = $ContactMailingListFromMailingListVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ContactMailingList met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $Mailing_MailingListFromMailingListVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ContactMailingListFromMailingListVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `MailingList`'
		          .' WHERE `mailingListID` = %i'
		          .' LIMIT 1'
		          , $this->mailingListID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mailingListID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `MailingList`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailingListID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailingListID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van MailingList terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mailinglistid':
			return 'int';
		case 'naam':
			return 'string';
		case 'omschrijving':
			return 'text';
		case 'subjectprefix':
			return 'string';
		case 'bodyprefix':
			return 'text';
		case 'bodysuffix':
			return 'text';
		case 'verborgen':
			return 'bool';
		case 'query':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'mailingListID':
		case 'verborgen':
			$type = '%i';
			break;
		case 'naam':
		case 'omschrijving':
		case 'subjectPrefix':
		case 'bodyPrefix':
		case 'bodySuffix':
		case 'query':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `MailingList`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mailingListID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mailingListID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `MailingList`'
		          .' WHERE `mailingListID` = %i'
		                 , $veld
		          , $this->mailingListID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'MailingList');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Mailing_MailingList-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$Mailing_MailingListFromMailingListVerz = Mailing_MailingListVerzameling::fromMailingList($this);
		$dependencies['Mailing_MailingList'] = $Mailing_MailingListFromMailingListVerz;

		// Verzamel alle ContactMailingList-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContactMailingListFromMailingListVerz = ContactMailingListVerzameling::fromMailingList($this);
		$dependencies['ContactMailingList'] = $ContactMailingListFromMailingListVerz;

		return $dependencies;
	}
}
