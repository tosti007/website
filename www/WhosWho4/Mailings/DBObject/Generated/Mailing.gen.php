<?
/**
 * @brief 'AT'AUTH_GET:mailings
 */
abstract class Mailing_Generated
	extends Entiteit
{
	protected $mailingID;				/**< \brief PRIMARY */
	protected $commissie;				/**< \brief Optionele commissie waaraan deze mailing gekoppeld is. Met name nuttig voor SpoCie-mailings. NULL */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $omschrijving;			/**< \brief Omschrijving van de mailing, bijv. 'Activiteitenmailing maandag 16 november'. */
	protected $momentIngepland;			/**< \brief Moment waarvoor deze mailing gepland staat om verzonden te worden. NULL */
	protected $momentVerwerkt;			/**< \brief Moment waarop deze mailing verwerkt en verzonden is naar de lijsten. NULL */
	protected $mailHeaders;				/**< \brief Headers van de te verzenden mail (bijv. subject) NULL */
	protected $mailSubject;				/**< \brief Onderwerp van de mailing */
	protected $mailFrom;				/**< \brief Afzender (From-header) van de mail */
	protected $isDefinitief;			/**< \brief Is de mailing defintief, m.a.w.: kan deze verzonden worden? Default natuurlijk nee (False). */
	/** Verzamelingen **/
	protected $mailingBounceVerzameling;
	protected $mailing_MailingListVerzameling;
	protected $tinyMailingUrlVerzameling;
	/**
	 * @brief De constructor van de Mailing_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->mailingID = NULL;
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->omschrijving = '';
		$this->momentIngepland = new DateTimeLocale(NULL);
		$this->momentVerwerkt = new DateTimeLocale(NULL);
		$this->mailHeaders = NULL;
		$this->mailSubject = '';
		$this->mailFrom = '';
		$this->isDefinitief = False;
		$this->mailingBounceVerzameling = NULL;
		$this->mailing_MailingListVerzameling = NULL;
		$this->tinyMailingUrlVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld mailingID.
	 *
	 * @return int
	 * De waarde van het veld mailingID.
	 */
	public function getMailingID()
	{
		return $this->mailingID;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_commissieID) || $new_commissieID == 0)) {
			if($this->commissie == NULL && $this->commissie_commissieID == NULL)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld momentIngepland.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld momentIngepland.
	 */
	public function getMomentIngepland()
	{
		return $this->momentIngepland;
	}
	/**
	 * @brief Stel de waarde van het veld momentIngepland in.
	 *
	 * @param mixed $newMomentIngepland De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setMomentIngepland($newMomentIngepland)
	{
		unset($this->errors['MomentIngepland']);
		if(!$newMomentIngepland instanceof DateTimeLocale) {
			try {
				$newMomentIngepland = new DateTimeLocale($newMomentIngepland);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->momentIngepland->strftime('%F %T') == $newMomentIngepland->strftime('%F %T'))
			return $this;

		$this->momentIngepland = $newMomentIngepland;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld momentIngepland geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld momentIngepland geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMomentIngepland()
	{
		if (array_key_exists('MomentIngepland', $this->errors))
			return $this->errors['MomentIngepland'];
		$waarde = $this->getMomentIngepland();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld momentVerwerkt.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld momentVerwerkt.
	 */
	public function getMomentVerwerkt()
	{
		return $this->momentVerwerkt;
	}
	/**
	 * @brief Stel de waarde van het veld momentVerwerkt in.
	 *
	 * @param mixed $newMomentVerwerkt De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setMomentVerwerkt($newMomentVerwerkt)
	{
		unset($this->errors['MomentVerwerkt']);
		if(!$newMomentVerwerkt instanceof DateTimeLocale) {
			try {
				$newMomentVerwerkt = new DateTimeLocale($newMomentVerwerkt);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->momentVerwerkt->strftime('%F %T') == $newMomentVerwerkt->strftime('%F %T'))
			return $this;

		$this->momentVerwerkt = $newMomentVerwerkt;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld momentVerwerkt geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld momentVerwerkt geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMomentVerwerkt()
	{
		if (array_key_exists('MomentVerwerkt', $this->errors))
			return $this->errors['MomentVerwerkt'];
		$waarde = $this->getMomentVerwerkt();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld mailHeaders.
	 *
	 * @return string
	 * De waarde van het veld mailHeaders.
	 */
	public function getMailHeaders()
	{
		return $this->mailHeaders;
	}
	/**
	 * @brief Stel de waarde van het veld mailHeaders in.
	 *
	 * @param mixed $newMailHeaders De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setMailHeaders($newMailHeaders)
	{
		unset($this->errors['MailHeaders']);
		if(!is_null($newMailHeaders))
			$newMailHeaders = trim($newMailHeaders);
		if($newMailHeaders === "")
			$newMailHeaders = NULL;
		if($this->mailHeaders === $newMailHeaders)
			return $this;

		$this->mailHeaders = $newMailHeaders;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld mailHeaders geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld mailHeaders geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMailHeaders()
	{
		if (array_key_exists('MailHeaders', $this->errors))
			return $this->errors['MailHeaders'];
		$waarde = $this->getMailHeaders();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld mailSubject.
	 *
	 * @return string
	 * De waarde van het veld mailSubject.
	 */
	public function getMailSubject()
	{
		return $this->mailSubject;
	}
	/**
	 * @brief Stel de waarde van het veld mailSubject in.
	 *
	 * @param mixed $newMailSubject De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setMailSubject($newMailSubject)
	{
		unset($this->errors['MailSubject']);
		if(!is_null($newMailSubject))
			$newMailSubject = trim($newMailSubject);
		if($newMailSubject === "")
			$newMailSubject = NULL;
		if($this->mailSubject === $newMailSubject)
			return $this;

		$this->mailSubject = $newMailSubject;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld mailSubject geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld mailSubject geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMailSubject()
	{
		if (array_key_exists('MailSubject', $this->errors))
			return $this->errors['MailSubject'];
		$waarde = $this->getMailSubject();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld mailFrom.
	 *
	 * @return string
	 * De waarde van het veld mailFrom.
	 */
	public function getMailFrom()
	{
		return $this->mailFrom;
	}
	/**
	 * @brief Stel de waarde van het veld mailFrom in.
	 *
	 * @param mixed $newMailFrom De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setMailFrom($newMailFrom)
	{
		unset($this->errors['MailFrom']);
		if(!is_null($newMailFrom))
			$newMailFrom = trim($newMailFrom);
		if($newMailFrom === "")
			$newMailFrom = NULL;
		if($this->mailFrom === $newMailFrom)
			return $this;

		$this->mailFrom = $newMailFrom;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld mailFrom geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld mailFrom geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkMailFrom()
	{
		if (array_key_exists('MailFrom', $this->errors))
			return $this->errors['MailFrom'];
		$waarde = $this->getMailFrom();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld isDefinitief.
	 *
	 * @return bool
	 * De waarde van het veld isDefinitief.
	 */
	public function getIsDefinitief()
	{
		return $this->isDefinitief;
	}
	/**
	 * @brief Stel de waarde van het veld isDefinitief in.
	 *
	 * @param mixed $newIsDefinitief De nieuwe waarde.
	 *
	 * @return Mailing
	 * Dit Mailing-object.
	 */
	public function setIsDefinitief($newIsDefinitief)
	{
		unset($this->errors['IsDefinitief']);
		if(!is_null($newIsDefinitief))
			$newIsDefinitief = (bool)$newIsDefinitief;
		if($this->isDefinitief === $newIsDefinitief)
			return $this;

		$this->isDefinitief = $newIsDefinitief;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld isDefinitief geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld isDefinitief geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkIsDefinitief()
	{
		if (array_key_exists('IsDefinitief', $this->errors))
			return $this->errors['IsDefinitief'];
		$waarde = $this->getIsDefinitief();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de MailingBounceVerzameling die hoort bij dit object.
	 */
	public function getMailingBounceVerzameling()
	{
		if(!$this->mailingBounceVerzameling instanceof MailingBounceVerzameling)
			$this->mailingBounceVerzameling = MailingBounceVerzameling::fromMailing($this);
		return $this->mailingBounceVerzameling;
	}
	/**
	 * @brief Returneert de Mailing_MailingListVerzameling die hoort bij dit object.
	 */
	public function getMailing_MailingListVerzameling()
	{
		if(!$this->mailing_MailingListVerzameling instanceof Mailing_MailingListVerzameling)
			$this->mailing_MailingListVerzameling = Mailing_MailingListVerzameling::fromMailing($this);
		return $this->mailing_MailingListVerzameling;
	}
	/**
	 * @brief Returneert de TinyMailingUrlVerzameling die hoort bij dit object.
	 */
	public function getTinyMailingUrlVerzameling()
	{
		if(!$this->tinyMailingUrlVerzameling instanceof TinyMailingUrlVerzameling)
			$this->tinyMailingUrlVerzameling = TinyMailingUrlVerzameling::fromMailing($this);
		return $this->tinyMailingUrlVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('mailings');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Mailing::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Mailing.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Mailing::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getMailingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Mailing|false
	 * Een Mailing-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$mailingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$mailingID = (int)$a;
		}

		if(is_null($mailingID))
			throw new BadMethodCallException();

		static::cache(array( array($mailingID) ));
		return Entiteit::geefCache(array($mailingID), 'Mailing');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Mailing');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Mailing::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Mailing`.`mailingID`'
		                 .     ', `Mailing`.`commissie_commissieID`'
		                 .     ', `Mailing`.`omschrijving`'
		                 .     ', `Mailing`.`momentIngepland`'
		                 .     ', `Mailing`.`momentVerwerkt`'
		                 .     ', `Mailing`.`mailHeaders`'
		                 .     ', `Mailing`.`mailSubject`'
		                 .     ', `Mailing`.`mailFrom`'
		                 .     ', `Mailing`.`isDefinitief`'
		                 .     ', `Mailing`.`gewijzigdWanneer`'
		                 .     ', `Mailing`.`gewijzigdWie`'
		                 .' FROM `Mailing`'
		                 .' WHERE (`mailingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mailingID']);

			$obj = new Mailing();

			$obj->inDB = True;

			$obj->mailingID  = (int) $row['mailingID'];
			$obj->commissie_commissieID  = (is_null($row['commissie_commissieID'])) ? null : (int) $row['commissie_commissieID'];
			$obj->omschrijving  = trim($row['omschrijving']);
			$obj->momentIngepland  = new DateTimeLocale($row['momentIngepland']);
			$obj->momentVerwerkt  = new DateTimeLocale($row['momentVerwerkt']);
			$obj->mailHeaders  = (is_null($row['mailHeaders'])) ? null : trim($row['mailHeaders']);
			$obj->mailSubject  = trim($row['mailSubject']);
			$obj->mailFrom  = trim($row['mailFrom']);
			$obj->isDefinitief  = (bool) $row['isDefinitief'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Mailing')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getCommissieCommissieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->mailingID =
			$WSW4DB->q('RETURNID INSERT INTO `Mailing`'
			          . ' (`commissie_commissieID`, `omschrijving`, `momentIngepland`, `momentVerwerkt`, `mailHeaders`, `mailSubject`, `mailFrom`, `isDefinitief`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %s, %i, %s, %i)'
			          , $this->commissie_commissieID
			          , $this->omschrijving
			          , (!is_null($this->momentIngepland))?$this->momentIngepland->strftime('%F %T'):null
			          , (!is_null($this->momentVerwerkt))?$this->momentVerwerkt->strftime('%F %T'):null
			          , $this->mailHeaders
			          , $this->mailSubject
			          , $this->mailFrom
			          , $this->isDefinitief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Mailing')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Mailing`'
			          .' SET `commissie_commissieID` = %i'
			          .   ', `omschrijving` = %s'
			          .   ', `momentIngepland` = %s'
			          .   ', `momentVerwerkt` = %s'
			          .   ', `mailHeaders` = %s'
			          .   ', `mailSubject` = %s'
			          .   ', `mailFrom` = %s'
			          .   ', `isDefinitief` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailingID` = %i'
			          , $this->commissie_commissieID
			          , $this->omschrijving
			          , (!is_null($this->momentIngepland))?$this->momentIngepland->strftime('%F %T'):null
			          , (!is_null($this->momentVerwerkt))?$this->momentVerwerkt->strftime('%F %T'):null
			          , $this->mailHeaders
			          , $this->mailSubject
			          , $this->mailFrom
			          , $this->isDefinitief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMailing
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMailing($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMailing($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Commissie';
			$velden[] = 'Omschrijving';
			$velden[] = 'MomentIngepland';
			$velden[] = 'MomentVerwerkt';
			$velden[] = 'MailHeaders';
			$velden[] = 'MailSubject';
			$velden[] = 'MailFrom';
			$velden[] = 'IsDefinitief';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Omschrijving';
			$velden[] = 'MailSubject';
			$velden[] = 'MailFrom';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Commissie';
			$velden[] = 'Omschrijving';
			$velden[] = 'MomentIngepland';
			$velden[] = 'MomentVerwerkt';
			$velden[] = 'MailHeaders';
			$velden[] = 'MailSubject';
			$velden[] = 'MailFrom';
			$velden[] = 'IsDefinitief';
			break;
		case 'get':
			$velden[] = 'Commissie';
			$velden[] = 'Omschrijving';
			$velden[] = 'MomentIngepland';
			$velden[] = 'MomentVerwerkt';
			$velden[] = 'MailHeaders';
			$velden[] = 'MailSubject';
			$velden[] = 'MailFrom';
			$velden[] = 'IsDefinitief';
		case 'primary':
			$velden[] = 'commissie_commissieID';
			break;
		case 'verzamelingen':
			$velden[] = 'MailingBounceVerzameling';
			$velden[] = 'Mailing_MailingListVerzameling';
			$velden[] = 'TinyMailingUrlVerzameling';
			break;
		default:
			$velden[] = 'Commissie';
			$velden[] = 'Omschrijving';
			$velden[] = 'MomentIngepland';
			$velden[] = 'MomentVerwerkt';
			$velden[] = 'MailHeaders';
			$velden[] = 'MailSubject';
			$velden[] = 'MailFrom';
			$velden[] = 'IsDefinitief';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle MailingBounce-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MailingBounceFromMailingVerz = MailingBounceVerzameling::fromMailing($this);
		$returnValue = $MailingBounceFromMailingVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object MailingBounce met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Mailing_MailingList-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$Mailing_MailingListFromMailingVerz = Mailing_MailingListVerzameling::fromMailing($this);
		$returnValue = $Mailing_MailingListFromMailingVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Mailing_MailingList met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle TinyMailingUrl-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TinyMailingUrlFromMailingVerz = TinyMailingUrlVerzameling::fromMailing($this);
		$returnValue = $TinyMailingUrlFromMailingVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TinyMailingUrl met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $MailingBounceFromMailingVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $Mailing_MailingListFromMailingVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TinyMailingUrlFromMailingVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Mailing`'
		          .' WHERE `mailingID` = %i'
		          .' LIMIT 1'
		          , $this->mailingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mailingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Mailing`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Mailing terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mailingid':
			return 'int';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'omschrijving':
			return 'string';
		case 'momentingepland':
			return 'datetime';
		case 'momentverwerkt':
			return 'datetime';
		case 'mailheaders':
			return 'text';
		case 'mailsubject':
			return 'string';
		case 'mailfrom':
			return 'string';
		case 'isdefinitief':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'mailingID':
		case 'commissie_commissieID':
		case 'isDefinitief':
			$type = '%i';
			break;
		case 'omschrijving':
		case 'momentIngepland':
		case 'momentVerwerkt':
		case 'mailHeaders':
		case 'mailSubject':
		case 'mailFrom':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Mailing`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mailingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mailingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Mailing`'
		          .' WHERE `mailingID` = %i'
		                 , $veld
		          , $this->mailingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Mailing');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle MailingBounce-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MailingBounceFromMailingVerz = MailingBounceVerzameling::fromMailing($this);
		$dependencies['MailingBounce'] = $MailingBounceFromMailingVerz;

		// Verzamel alle Mailing_MailingList-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$Mailing_MailingListFromMailingVerz = Mailing_MailingListVerzameling::fromMailing($this);
		$dependencies['Mailing_MailingList'] = $Mailing_MailingListFromMailingVerz;

		// Verzamel alle TinyMailingUrl-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TinyMailingUrlFromMailingVerz = TinyMailingUrlVerzameling::fromMailing($this);
		$dependencies['TinyMailingUrl'] = $TinyMailingUrlFromMailingVerz;

		return $dependencies;
	}
}
