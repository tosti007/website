<?
/**
 * @brief Koppeling Mailings-Mailinglists
 */
abstract class Mailing_MailingList_Generated
	extends Entiteit
{
	protected $mailing;					/**< \brief PRIMARY */
	protected $mailing_mailingID;		/**< \brief PRIMARY */
	protected $mailingList;				/**< \brief PRIMARY */
	protected $mailingList_mailingListID;/**< \brief Private key die deze lijst identificeert en gebruikt wordt om naar deze lijst te verwijzen bij het coden van queries. PRIMARY */
	/**
	/**
	 * @brief De constructor van de Mailing_MailingList_Generated-klasse.
	 *
	 * @param mixed $a Mailing (Mailing OR Array(mailing_mailingID) OR
	 * mailing_mailingID)
	 * @param mixed $b MailingList (MailingList OR Array(mailingList_mailingListID) OR
	 * mailingList_mailingListID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Mailing)
		{
			$this->mailing = $a;
			$this->mailing_mailingID = $a->getMailingID();
		}
		else if(is_array($a))
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->mailing = NULL;
			$this->mailing_mailingID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof MailingList)
		{
			$this->mailingList = $b;
			$this->mailingList_mailingListID = $b->getMailingListID();
		}
		else if(is_array($b))
		{
			$this->mailingList = NULL;
			$this->mailingList_mailingListID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->mailingList = NULL;
			$this->mailingList_mailingListID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Mailing';
		$volgorde[] = 'MailingList';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld mailing.
	 *
	 * @return Mailing
	 * De waarde van het veld mailing.
	 */
	public function getMailing()
	{
		if(!isset($this->mailing)
		 && isset($this->mailing_mailingID)
		 ) {
			$this->mailing = Mailing::geef
					( $this->mailing_mailingID
					);
		}
		return $this->mailing;
	}
	/**
	 * @brief Geef de waarde van het veld mailing_mailingID.
	 *
	 * @return int
	 * De waarde van het veld mailing_mailingID.
	 */
	public function getMailingMailingID()
	{
		if (is_null($this->mailing_mailingID) && isset($this->mailing)) {
			$this->mailing_mailingID = $this->mailing->getMailingID();
		}
		return $this->mailing_mailingID;
	}
	/**
	 * @brief Geef de waarde van het veld mailingList.
	 *
	 * @return MailingList
	 * De waarde van het veld mailingList.
	 */
	public function getMailingList()
	{
		if(!isset($this->mailingList)
		 && isset($this->mailingList_mailingListID)
		 ) {
			$this->mailingList = MailingList::geef
					( $this->mailingList_mailingListID
					);
		}
		return $this->mailingList;
	}
	/**
	 * @brief Geef de waarde van het veld mailingList_mailingListID.
	 *
	 * @return int
	 * De waarde van het veld mailingList_mailingListID.
	 */
	public function getMailingListMailingListID()
	{
		if (is_null($this->mailingList_mailingListID) && isset($this->mailingList)) {
			$this->mailingList_mailingListID = $this->mailingList->getMailingListID();
		}
		return $this->mailingList_mailingListID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Mailing_MailingList::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Mailing_MailingList.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Mailing_MailingList::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getMailingMailingID()
		                      , $this->getMailingListMailingListID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return Mailing_MailingList|false
	 * Een Mailing_MailingList-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$mailing_mailingID = (int)$a[0];
			$mailingList_mailingListID = (int)$a[1];
		}
		else if($a instanceof Mailing
		     && $b instanceof MailingList)
		{
			$mailing_mailingID = $a->getMailingID();
			$mailingList_mailingListID = $b->getMailingListID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$mailing_mailingID = (int)$a;
			$mailingList_mailingListID = (int)$b;
		}

		if(is_null($mailing_mailingID)
		|| is_null($mailingList_mailingListID))
			throw new BadMethodCallException();

		static::cache(array( array($mailing_mailingID, $mailingList_mailingListID) ));
		return Entiteit::geefCache(array($mailing_mailingID, $mailingList_mailingListID), 'Mailing_MailingList');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Mailing_MailingList');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Mailing_MailingList::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Mailing_MailingList`.`mailing_mailingID`'
		                 .     ', `Mailing_MailingList`.`mailingList_mailingListID`'
		                 .     ', `Mailing_MailingList`.`gewijzigdWanneer`'
		                 .     ', `Mailing_MailingList`.`gewijzigdWie`'
		                 .' FROM `Mailing_MailingList`'
		                 .' WHERE (`mailing_mailingID`, `mailingList_mailingListID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mailing_mailingID']
			                  ,$row['mailingList_mailingListID']);

			$obj = new Mailing_MailingList(array($row['mailing_mailingID']), array($row['mailingList_mailingListID']));

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Mailing_MailingList')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getMailing();
		if(!$rel->getInDB())
			throw new LogicException('foreign Mailing is not in DB');
		$this->mailing_mailingID = $rel->getMailingID();

		$rel = $this->getMailingList();
		if(!$rel->getInDB())
			throw new LogicException('foreign MailingList is not in DB');
		$this->mailingList_mailingListID = $rel->getMailingListID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Mailing_MailingList`'
			          . ' (`mailing_mailingID`, `mailingList_mailingListID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i)'
			          , $this->mailing_mailingID
			          , $this->mailingList_mailingListID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Mailing_MailingList')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Mailing_MailingList`'
			.' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailing_mailingID` = %i'
			          .  ' AND `mailingList_mailingListID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailing_mailingID
			          , $this->mailingList_mailingListID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMailing_MailingList
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMailing_MailingList($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMailing_MailingList($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			break;
		case 'get':
		case 'primary':
			$velden[] = 'mailing_mailingID';
			$velden[] = 'mailingList_mailingListID';
			break;
		case 'verzamelingen':
			break;
		default:
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Mailing_MailingList`'
		          .' WHERE `mailing_mailingID` = %i'
		          .  ' AND `mailingList_mailingListID` = %i'
		          .' LIMIT 1'
		          , $this->mailing_mailingID
		          , $this->mailingList_mailingListID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mailing = NULL;
		$this->mailing_mailingID = NULL;
		$this->mailingList = NULL;
		$this->mailingList_mailingListID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Mailing_MailingList`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mailing_mailingID` = %i'
			          .  ' AND `mailingList_mailingListID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mailing_mailingID
			          , $this->mailingList_mailingListID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Mailing_MailingList terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mailing':
			return 'foreign';
		case 'mailing_mailingid':
			return 'int';
		case 'mailinglist':
			return 'foreign';
		case 'mailinglist_mailinglistid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Mailing_MailingList`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mailing_mailingID` = %i'
		          .  ' AND `mailingList_mailingListID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mailing_mailingID
		          , $this->mailingList_mailingListID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Mailing_MailingList`'
		          .' WHERE `mailing_mailingID` = %i'
		          .  ' AND `mailingList_mailingListID` = %i'
		                 , $veld
		          , $this->mailing_mailingID
		          , $this->mailingList_mailingListID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Mailing_MailingList');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
