<?

/***
 * $Id$
 * 
 * Bevat informatie over een bounce van een mailing, bijvoorbeeld als een van de abonnees op de mailinglijst een incorrect e-mailadres heeft.
 */
class MailingBounce
	extends MailingBounce_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a = NULL, $b)
	{
		parent::__construct($a, $b);
	}

	public function magVerwijderen()
	{
		if(hasAuth('bestuur')) {
			return true;
		}

		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
