<?

/***
 * $Id$
 * 
 * Template voor een Mailing. Bijvoorbeeld: activiteitenmail, boekenmail.
 */
class MailingTemplate
	extends MailingTemplate_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	/**
	 * Retourneert een lijst van mailinglists waar dit template normaal heen gestuurd wordt.
	 */
	public function mailinglists()
	{
		user_error('501 Not Implemented', E_USER_ERROR);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
