<?
/**
 * $Id$
 *
 *  Linkt een TinyUrl aan een Mailing, zo kan je het totaal aantal hits van een mailing tracken of van een mailing een mooi overzicht krijgen.
 */
class TinyMailingUrl
	extends TinyMailingUrl_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // TinyMailingUrl_Generated
	}

	public function magVerwijderen()
	{
		return $this->getMailing()->magVerwijderen();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
