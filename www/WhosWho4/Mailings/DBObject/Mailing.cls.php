<?

/***
 * $Id$
 * 
 * Klasse die een enkele mailing naar een (of meerdere) lijsten representeert. Bijvoorbeeld een activiteitenmailing.
 */
class Mailing
	extends Mailing_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	/**
	 * De functie voor het mogen verwijderen van een object
	 */
	public function magVerwijderen()
	{
		if($this->isVerzonden())
			return false;
		return (hasAuth('bestuur') || hasAuth('mailings'));
	}

	/**
	 * De functie voor het mogen wijzigen van een object
	 */
	public function magWijzigen()
	{
		if($this->isVerzonden())
			return false;
		return (hasAuth('bestuur') || hasAuth('mailings'));
	}

	/**
	 * De functie de de url van het object terug geeft
	 */
	public function url()
	{
		return MAILINGWEB . "Mailings/" . $this->geefID() . "/";
	}

	/**
	 * Functie die een verzameling van alle mailinglists van de mailing geeft
	 */
	public function getMailingLists()
	{
		return MailingListVerzameling::mailing($this);
	}

	/**
		Stelt headers en body in op basis van de string $mail die volledige
		headers en body bevat. Er wordt bekeken welke headers bewaard moeten
		blijven (bijv. 'Content-Type: ...') en welke headers er niet moeten
		zijn (bijv. 'Received: ...').
	**/
	public function parseExternalMailInput($mail, $setsubject = true){
		// Headers die behouden moeten blijven
		$copy_headers = array("Content-Type", "MIME-Version");

		$headers = array();
		$body = array();
		$subject = "";
		$parsing_headers = true;
		$last_headertype = null;
		$mail_lines = explode("\n", $mail);
		$koppeling = null;

		foreach ($mail_lines as $line){
			if ($parsing_headers && empty($line)){
				// End of headers
				$parsing_headers = false;
				continue;
			}

			if ($parsing_headers){
				// Headers parsen

				// Sommige headers (zoals Content-Type) vallen over meerdere
				// regels, daar moet rekening mee gehouden worden.
				$colonpos = strpos($line, ": ");
				if (is_int($colonpos)){
					// Reguliere header, regel bevat ": "
					$curr_headertype = substr($line, 0, $colonpos);
					$curr_headervalue = trim(substr($line, $colonpos + 1));
					$last_headertype = $curr_headertype;
				} else {
					// Geen reguliere header, blijkbaar bevat deze regel de
					// rest van de header van de vorige regel
					$curr_headertype = $last_headertype;
				}

				if ($curr_headertype == "Subject"){
					// Subject is speciale header, die wordt los opgeslagen
					$subject = $curr_headervalue;
				} elseif ($curr_headertype == "X-MailingWeb-MailingListID"){
					// Mailinglist koppelen
					$koppeling = $curr_headervalue;
				} elseif ($curr_headertype == "X-MailingWeb-Schedule"){
					// Schedule instellen
					$this->setMomentIngepland($curr_headervalue);
				} elseif (in_array($curr_headertype, $copy_headers)){
					// Deze header moet opgeslagen worden
					$headers[] = rtrim($line);
				} // else: header skippen, irrelevant
			} else {
				// Body
				$body[] = rtrim($line);
			}
		}

        if($setsubject)
    		$this->setMailSubject($subject);
		$this->setMailHeaders(implode("\n", $headers));
		$this->opslaan();

		if($koppeling) {
			$koppeling = new Mailing_MailingList($this->getMailingID(), $curr_headervalue);
			$koppeling->opslaan();
		}

		$this->setMailBody(implode("\n", $body));

		return false;
	}

	/**
		Haalt body en headers uit opgegeven EML-file en haalt het resultaat
		door de functie parseExternalMailInput($mail).
	**/
	public function parseEML($filename){
		// TODO error handling bij lezen van file
		return $this->parseExternalMailInput(file_get_contents($filename));
	}

	/**
		Slaat de body van een mail op. Gebeurt niet in database, maar gaat naar
		een file op het filesystem i.v.m. binary data die we niet in MySQL
		willen hebben.

		Merk op dat dit object dus eerst in de database opgeslagen moet zijn
		(het MailingID is nodig) alvorens de body text ingesteld kan worden.
		Als de mailing nog niet in de DB is opgeslagen, wordt er een exception
		gegooid.
	**/
	public function setMailBody($text){
		$path = $this->getMailBodyPath();
		if ($path === null){
			// Deze mailing zit nog niet in de database, heeft dus nog geen ID
			// en geen mailbody path
			throw new Exception("De mail body kan niet opgeslagen worden zolang deze mailing nog niet in de database opgeslagen is!");
		}

		$fp = fopen($this->getMailBodyPath(true), "w+");
		fwrite($fp, $text);
		fclose($fp);

		chmod($this->getMailBodyPath(true), 0664);
	}

	/**
		 Retourneert het pad naar de file met de body van deze mailing. Ook als
		de mailbody niet bestaat wordt er een pad geretourneerd. Als je op de debug
		zit wordt er een speciaal ander pad teruggegeven

		Deze functie retourneert null als dit Mailing-object nog niet in de
		database is opgeslagen en derhalve nog geen geldig MailingID heeft.
	**/
	public function getMailBodyPath($force_debug = false)
	{
		$id = $this->getMailingID();
		if (!is_numeric($id) || $id <= 0)
			return null;
		if(file_exists("/srv/mailing_files/$id-body") && (DEBUG && !$force_debug))
			return "/srv/mailing_files/$id-body";
		if(DEBUG)
			return "/srv/mailing_files/$id-body-debug";
		return "/srv/mailing_files/$id-body";
	}

	/**
		 Retourneert het pad naar de file met daarin de integrale mailing
		(inclusief headers), zoals deze is verzonden. Ook als de mailing nog
		niet is verzonden, wordt er een pad geretourneerd!

		Deze functie retourneert null als dit Mailing-object nog niet in de
		database is opgeslagen en derhalve nog geen geldig MailingID heeft.
	**/
	public function getMailSentPath(){
		$id = $this->getMailingID();
		if (!is_numeric($id) || $id <= 0) return null;
		return "/srv/mailing_files/$id-sent";
	}

	/**
		Retourneert de body van de mailing als string. Als er geen body is
		(file not found), dan retourneert de functie null. Deze functie voegt
		geen prefix of suffix toe aan de body!
	**/
	public function getMailBody(){
		$path = $this->getMailBodyPath();
		if ($path === null)
			return null;
		if (file_exists($path))
			return str_replace("\r\n", "\n", file_get_contents($path));
		return null;
	}

	public function composeFinalMailSubject($suffix = null){
		$subject = $this->getMailSubject();
		if ($this->getMailingLists()->aantal() == 1){
			$mailinglist = $this->getMailingLists()->first();
			$subject_prefix = $mailinglist->getSubjectPrefix();

			if ($subject_prefix) $subject = $subject_prefix . ' ' . $subject;
		}

		if ($suffix) $subject .= ' ' . $suffix;
		return $subject;
	}

	/**
		Bepaalt a.h.v. de Content-Type header of dit een plaintext mailing is.
	**/
	public function isPlainTextMailing(){
		$ctype = $this->getSingletonHeaderValue("Content-type");
		$is_plaintext = (substr($ctype, 0, 10) == "text/plain");
		return $is_plaintext;
	}

	/**
		Stelt de body van de mail samen, zoals deze er in de definitieve
		mailing uit zou moeten zien. Dat is dus eventueel inclusief prefix en
		suffix.
	**/
	public function composeFinalMailBody()
	{
		// Headers en body als string, exact zoals ze zijn binnengekomen
		$headers = $this->getMailHeaders();
		$body = $this->getMailBody();
		$merged = array_merge(
			explode("\n", $headers),
			array("\n"),
			explode("\n", $body)
		);
		$mailparser = new MailParser($merged);
		$mimeblock = $mailparser->getMIMEPart();

		// alleen als we geen ambiguiteit hebben, voegen we pre- en suffixen toe
		if ($this->getMailingLists()->aantal() != 1) {
			return wordwrap($this->getMailBody());
		}

		$mailinglist = $this->getMailingLists()->first();
		$bprefix = $mailinglist->getBodyPrefix();
		$bsuffix = $mailinglist->getBodySuffix();

		// als we geen prefix en geen suffix hebben, zijn we ook meteen klaar
		if (!$bprefix && !$bsuffix) {
			return wordwrap($this->getMailBody());
		}

		// Prefix en/of suffix invoegen
		if ($mimeblock ===  null){
			// Gewoon een simpele mail, geen multipart/MIME
			$res = "";

			if ($bprefix) $res .= $bprefix . "\n\n";

			$res .= wordwrap($this->getMailBody());

			if ($bsuffix) $res .= "\n\n" . $bsuffix;

			return $res;
		} else {
			// Moeilijker: in een multipart/MIME mail de prefix toevoegen...

			// Voor nu geldt: /alle/ MIME parts opvragen, prefix en
			// suffix toevoegen aan alle HTML en plaintext parts.
			// TODO: slimmer maken: het zou namelijk kunnen zijn dat er
			// een onschuldig HTML of plaintext attachment bij de mail
			// zit, waar geen prefix en suffix in moeten verschijnen.

			$mimeparts = $mimeblock->getParts(true);
			foreach($mimeparts as $mimepart){
				$contenttype = $mimepart->getContentType();
				if ($contenttype == "text/html"){
					// HTML mime part, prefix toevoegen direct na opening <body>

					// Prefix toevoegen na begin mail
					$body = implode("\n", $mimepart->getBody());
					$body = preg_replace(
							'/<body([^>]*)>/i',
							'<body$1><pre>' . $bprefix . '</pre>', 
							$body, 1
							);

					// Suffix toevoegen direct voor einde </body>
					$body = preg_replace(
							'/<\/body([^>]*)>/i',
							'<pre>' . $bsuffix . '</pre></body$1>', 
							$body, 1
							);

					$mimepart->setBody(explode("\n", $body));
				} elseif ($mimepart->getContentType() == "text/plain"){
					// Gewoon prefix en suffix toevoegen boven en onderaan MIME part
					if ($bprefix) $bprefix .= "\n\n";
					if ($bsuffix) $bsuffix = "\n\n$bsuffix";

					$body = array_merge(
							explode("\n", $bprefix),
							$mimepart->getBody(),
							explode("\n", $bsuffix)
							);

					$mimepart->setBody($body);
				} // else: content-type niet text/plain of text/html
			}

			// De MIME-datastructuur heeft nu allerlei toevoegingen
			// gekregen, dat moet nu omgezet worden naar een degelijke
			// string
			return $mimeblock->toString(true);
		}
	}

	/**
		Stelt de headers van de mail samen, zoals deze er in de definitieve
		mailing uit zouden zien. 
	**/
	public function composeFinalMailHeaders($previewing = false)
	{
		$headers = $this->getMailHeaders() . "\nX-MailingWeb-ID: " . $this->getMailingID();
		$headers_arr = explode("\n", $headers);
		$headers_arr[] = "From: " . $this->getMailFrom();
		$headers_arr[] = "To: Leden A-Eskwadraat <activiteiten@A-Eskwadraat.nl>";

		if ($previewing){
			$headers_arr[] = "Subject: " . $this->composeFinalMailSubject('[PREVIEW]');
		} else {
			$headers_arr[] = "Precedence: list";
			$headers_arr[] = "List-help: <http://www.A-Eskwadraat.nl/contribucie>, <mailto:activiteiten@A-Eskwadraat.nl>";
			$headers_arr[] = "List-Unsubscribe: <http://www.A-Eskwadraat.nl/contribucie>, <mailto:activiteiten@A-Eskwadraat.nl>";
			$headers_arr[] = "Subject: " . $this->composeFinalMailSubject();
		}
		return implode("\n", $headers_arr);
	}

	/**
		Retourneert alle headers van een bepaald type.
	**/
	public function getHeadersByType($type){
		$headers_array = explode("\n", $this->getMailHeaders());
		$res = array();
		foreach($headers_array as $header){
			if (strtolower(substr($header, 0, strlen($type . ":"))) === strtolower("$type:")) $res[] = $header;
		}
		return $res;
	}

	/**
		Retourneert de value van een singleton-header (i.e. een header die
		hooguit 1x mag voorkomen, zoals 'Sender' of 'Subject')
	**/
	public function getSingletonHeaderValue($type){
		$headers = $this->getHeadersByType($type);
		if (sizeof($headers) == 0) return null;
		return Mailing::trimHeaderType(array_shift($headers));
	}

	/**
		"Subject: bladiebla" => "bladiebla"
	**/
	private static function trimHeaderType($input){
		$colonpos = strpos($input, ":");
		if (!$colonpos) return "";
		return trim(substr($input, $colonpos + 1));
	}

	/**
	 * Geeft een bool die zegt op de mailing verzonden is of niet
	 */
	public function isVerzonden(){
		return $this->getMomentVerwerkt()->hasTime();
	}

    public function mailingErrorHandler($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno)) {
            // This error code is not included in error_reporting
            return;
        }

        switch ($errno) {
        case E_USER_ERROR:
            echo $errstr;
            break;

        default:
            echo "Unknown error type: [$errno] $errstr<br />\n";
            break;
        }

        /* Don't execute PHP internal error handler */
        return true;
    }

	/**
		Hier wordt de mailing daadwerkelijk verstuurd.

		Deze functie wordt enkel aangeroepen door Cron, niet via een
		browser-sessie. In het ongelukkige geval dat het toch vanuit een
		browsersessie gebeurt, dan doet deze functie een user_error

		Deze functie wordt ook gebruikt om previews te versturen, in dat geval
		moet er een array $preview_recipients meegegeven worden. Uiteraard kan
		dat wel vanuit een browsersessie. In dat geval mag $preview_recipients
		een reguliere array zijn met e-mailadressen.

		Deze functie gooit een Exception met een foutmelding als er iets mis
		is.
	**/
	public function verstuurMailing()
	{
		global $request;

		// Enkel gods mogen vanuit een webbrowser een volledige mailing
		// versturen...
		$is_websessie = $request->server->get("SERVER_NAME");
		if($is_websessie && !hasAuth("god"))
			user_error("Mailing->verstuurMailing() kan alleen aangeroepen worden in Commandline mode!", E_USER_ERROR);

		// Checken of deze mailing soms al verzonden is
		if($this->isVerzonden())
			user_error("Deze mailing is reeds verzonden, dat kan niet nogmaals gebeuren!", E_USER_ERROR);

		// Per abonnee versturen, opdat bounces goed geregeld zijn.
		// TODO: bounces iets slimmer maken, waardoor ze te koppelen zijn aan
		// de verstuurde mailing, opdat we kunnen bepalen voor hoeveel abonnees
		// een mailing gebounced heeft. Nu weten we alleen maar dat er /iets/
		// gebounced heeft, maar wat?

		// Verzendmoment instellen om race conditions tegen te gaan
		$this->setMomentVerwerkt(date('Y-m-d H:i:s'));
		$this->opslaan();

		// Vangt genante momenten af dat je continu dezelfde mailing gaat versturen
		if (!$this->isVerzonden()) {
			user_error(_("Maling kan niet op 'al verzonden' gezet worden!"), E_USER_ERROR);
		}

		$body = $this->composeFinalMailBody();

		$mailingid = $this->getMailingID();
		$headers = $this->getMailHeaders() . "\nX-MailingWeb-ID: " . $this->getMailingID();
		$from = $this->getMailFrom();
		$subject = $this->composeFinalMailSubject();

		// Recipient-array ophalen uit mailinglist. Formaat van deze array:
		// array(
		//		4042 => 'sjeik@a-eskwadraat.nl',
		//		4624 => 'arnoud@a-eskwadraat.nl
		// )

		// TODO: voor WhosWho4 zou dit moeten werken:
		//$recipients = $this->getMailingLists()->abonneesAsRecipientArray();

		// Maar zolang WSW4 nog niet definitief in gebruik is (en de
		// database nog niet met betrouwbare data gevuld is), wordt
		// aangenomen dat de queries in $MAILINGLIST_QUERIES nog in 'oude
		// stijl' WSW zijn.

		$mailinglists = $this->getMailingLists();
		if ($mailinglists->aantal() == 0){
			throw new Exception("Er zijn geen mailinglists aan deze mailing gekoppeld!");
		}

		$recipients = $this->getMailingLists()->abonneesAsRecipientArray();

        // Zet de errorhandler op custom, zodat als er een fout komt met sendmail
        // de rest van de mailings wel verstuurd worden
        set_error_handler(array($this, 'mailingErrorHandler'));
		// Mailing per recipient aan Exim voeren. Merk op dat de mail niet
		// direct door Exim geprocessed wordt, dat gebeurd pas later
		$sendcount = 0;
		$noemailcount = 0;
		foreach($recipients as $contactid => $r) {
			echo "Verzenden aan $r... ";
			flush();
			if(!$r) {
				// Lid heeft geen email!
				echo("$contactid heeft geen email! Niet verstuurd.");
				if ($is_websessie) echo '<br>';
				else echo "\n";
				$noemailcount++;
				continue;
			}

			sendmail($from, $r, $subject, $body, $from, $headers, "bounce-$contactid");
			echo 'verzonden!';
			$sendcount++;

			if ($is_websessie) echo '<br>';
			else echo "\n";
		}
        restore_error_handler();

		// Mail is echt verzonden!
		if ($is_websessie) echo "<br><br>";
		else echo "\n\n";

		echo "Er zijn in totaal $sendcount berichten verstuurd.";
		echo "Er zijn in totaal $noemailcount berichten niet verstuurd omdat de leden geen email hebben.";

		// Mail opslaan exact zoals deze is verzonden, zodat deze in
		// MailingWeb integraal opgevraagd kan worden. Merk op dat een
		// deel van een mailing dynamisch wordt gegenereerd op het
		// moment van verzenden, zoals een prefix van het subject. Deze
		// opgeslagen variant van de mailing zal natuurlijk niet
		// wijzigen op het moment dat het verantwoordelijke bestuurslid
		// besluit een andere prefix in te voeren!
		if(!DEBUG) {
			$fp = fopen($this->getMailSentPath(), 'w+');
			fwrite($fp, $headers."\n\n".$body);
			fclose($fp);
		}
	}

	public function verstuurMailingPreview($reciepients)
	{
		// Enkel gods, bestuur en spocieleden mogen volledige mailingpreview
		// versturen...
		if(!hasAuth("mailings"))
			user_error("Mailing->verstuurMailingPreview() kan alleen door goden, bestuur en spoken gedaan worden!", E_USER_ERROR);

		// Als er geen mailinglist is gaat de preview mis, dus dat mag niet
		if(!$this->getMailingLists())
			user_error("Er moet een mailinglist aan de mailing gekoppeld zijn!", E_USER_ERROR);
	
		// Checken of deze mailing soms al verzonden is
		if($this->getMomentVerwerkt()->hastime() && $this->getIsDefinitief())
			user_error("Deze mailing is reeds verzonden, dat kan niet nogmaals gebeuren!", E_USER_ERROR);
		
		$body = $this->composeFinalMailBody();

		$headers = $this->getMailHeaders() . "\nX-MailingWeb-ID: " . $this->getMailingID();
		$from = $this->getMailFrom();
		$subject = $this->composeFinalMailSubject('[PREVIEW]');

        set_error_handler(array($this, 'mailingErrorHandler'));
		foreach($reciepients as $r) {
			sendmail($from, $r, $subject, $body, '', $headers);
        }
        restore_error_handler();
	}

	/**
		Zorgt ervoor dat de gegeven MailingLists aan deze Mailing zijn gekoppeld.
		Returnt of het succesvol gebeurd is
	**/
	public function setMailingLists(MailingListVerzameling $mailinglists){
		// zoek de koppelingen die weg mogen
		$koppelingen = Mailing_MailingListVerzameling::fromMailing($this);
		if (!$koppelingen->magVerwijderen()) {
			return false;
		}
		$returnVal = $koppelingen->verwijderen();
		if(!is_null($returnVal)) {
			user_error($returnVal, E_USER_ERROR);
		}

		foreach ($mailinglists as $mailinglist){
			// voeg de nieuwe koppelingen toe
			$koppeling = new Mailing_MailingList($this, $mailinglist);
			$koppeling->opslaan();
		}
		return true;
	}

	/**
		Retourneert een leeg (en niet-opgeslagen) Mailing-object met headers
		voor een plaintext-mailing.
	**/
	public static function constructNewPlaintextMailing(){
		$mailing = new Mailing();
		$mailing->setMailHeaders("MIME-Version: 1.0\nContent-type: text/plain; charset=utf-8");
		return $mailing;
	}

	//Geef een array met alle filenames van attachments en inline bestanden.
	public function getFileNames()
	{
		//Als de mailing nog niet opgeslagen is of geen body heeft zijn er sowieso geen gekoppelde bestanden
		if(!is_file($this->getMailBodyPath()))
			return null;

		//Haal de mailingbody op (we doen dus niets met plaintext mailings op 't moment).
		$fp = fopen($this->getMailBodyPath(), "r");
		if(filesize($this->getMailBodyPath()) == 0) {
			$text = '';
		} else {
			$text = fread($fp, filesize($this->getMailBodyPath()));
		}
		fclose($fp);

		preg_match_all('/filename="[^"]*"/' , $text, $filenames);

		if(count($filenames) == 0)
			return null;

		$realFilenames = array();
		foreach($filenames[0] as $id => $filename)
		{
			preg_match('/"([^"]*)"/' , $filename, $realFilenames[]);
		}

		return $realFilenames;
	}

	public function verwijderen($foreigncall = NULL)
	{
		if(!DEBUG && !DEMO) {
			$path = $this->getMailBodyPath();
			if(file_exists($path)) {
				unlink($path);
			}
		}

		return parent::verwijderen($foreigncall);
	}

	public function koppelTinyUrls()
	{
		$path = $this->getMailBodyPath();
		if(file_exists($path) && filesize($path) != 0) {
			$fp = fopen($path, "r");
			$text = fread($fp, filesize($path));
			fclose($fp);

			//En match op href=, zowel met "" (double quotes) als '' (singel quotes), nare
			// is bv dat href='a.bc/bobjan"wit"baard' een valide url is. Beide quotes samen
			// in een regex is naar omdat er ook nog onderlinge afhankelijkeheden zijn.
			preg_match_all('/href="(?!mailto)[^"]*"/' , $text, $urlsdq);
			preg_match_all("/href='(?!mailto)[^']*'/" , $text, $urlssq);

			//For sake of gemak, haal een niveau weg.
			$urlsdq = $urlsdq[0];
			$urlssq = $urlssq[0];

			//Haal alle dubbele urls er uit. Later zal str_replace *alle* occurences toch vervangen.
			$urlsdq = array_unique($urlsdq);
			$urlssq = array_unique($urlssq);

			//Haal de tinyurls bij deze mailing op
			$tmurls = TinyUrlVerzameling::vanMailing($this);
			$tmurlsStrings = array();
			foreach($tmurls as $tmurl) {
				$tmurlsStrings[] = $tmurl->getOutgoingUrl();
			}

			//Ga voor iedere url kijken of we wat moeten doen, zo ja, maak een TinyUrl,
			// link deze met een TinyMailingUrl en str_replace de url in de mailing.
			foreach($urlsdq as $id => $dqurl)
			{
				preg_match('/"([^"]*)"/' , $dqurl, $displayUrl);
				if(in_array($displayUrl[1], $tmurlsStrings)) {
					continue;
				}
				$turl = new TinyUrl();
				$turl->setTinyUrl(TinyUrl::generateTinyUrl());
				$turl->setOutgoingUrl($displayUrl[1]);
				$tmurl = new TinyMailingUrl($this, $turl);
				$turl->opslaan();
				$tmurl->opslaan();
				$text = str_replace($dqurl, "href=\"http://a-es2.nl/t/".$turl->getTinyUrl()."\"", $text);
			}

			foreach($urlssq as $id => $squrl)
			{
				preg_match("/'([^']*)'/" , $squrl, $displayUrl);
				if(in_array($displayUrl[1], $tmurlsStrings)) {
					continue;
				}
				$turl = new TinyUrl();
				$turl->setTinyUrl(TinyUrl::generateTinyUrl());
				$turl->setOutgoingUrl($displayUrl[1]);
				$tmurl = new TinyMailingUrl($this, $turl);
				$turl->opslaan();
				$tmurl->opslaan();
				$text = str_replace($squrl, "href=\"http://a-es2.nl/t/".$turl->getTinyUrl()."\"", $text);
			}

			$this->setMailBody($text);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
