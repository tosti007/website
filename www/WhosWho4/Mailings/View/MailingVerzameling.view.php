<?

/**
 * $Id$
 */
abstract class MailingVerzamelingView
	extends MailingVerzamelingView_Generated
{
	/**
		Retourneert een HTML tabel met daarin informatie over de meegegeven
		mailings
	**/
	static public function makeMailingTable(MailingVerzameling $mailings, $columns = null)
	{
		$table = new HtmlTable();
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$row->addHeader(_("Omschrijving"));
		$row->addHeader(_("Onderwerp"));
		$row->addHeader(_("Commissie"));
		$row->addHeader(_("Ingepland voor"));
		$row->addHeader(_("Verzonden op"));
		$row->addHeader(_("Mailing definitief"));
		$row->addHeader(_("Mailinglijst"));

		foreach($mailings as $m) {
			$tbody->add($row = new HtmlTableRow());
			$row->addData(new HtmlAnchor($m->url(), MailingView::waardeOmschrijving($m)));
			$row->addData(MailingView::waardeMailSubject($m));
			$row->addData(MailingView::waardeCommissie($m));
			$row->addData(MailingView::waardeMomentIngepland($m));
			$row->addData(MailingView::waardeMomentVerwerkt($m));
			$row->addData(MailingView::waardeIsDefinitief($m));
			$row->addData(MailingView::waardeMailingLists($m));
		}

		return $table;
	}

	/**
	 * Maakt de view voor de home van mailingweb
	 */
	static public function home()
	{
		$page = Page::getInstance()->start('MailingWeb');

		$page->add($res = new HtmlDiv());
		$res->add(new HtmlDiv(sprintf('Je kunt een nieuwe mailing aanmaken in MailingWeb door deze ' .
			'simpelweg te sturen naar %s of %s. Als je een nieuwe ' . 
			'(simpele %s) mailing wil aanmaken, %s.'
			, new HtmlAnchor('mailto:mailings@a-eskwadraat.nl', 'mailings@a-eskwadraat.nl')
			, new HtmlAnchor('mailto:spocie-mailings@a-eskwadraat.nl', 'spocie-mailings@.a-eskwadraat.nl')
			, new HtmlEmphasis(_('plaintext'))
			, new HtmlAnchor(MAILINGWEB . 'Mailings/Nieuw', _("kun je ook hier klikken"))), 'bs-callout bs-callout-info'));
		$res->add(self::makeMailingsOverzicht());

		$page->end();
	}

	/**
	 * Maakt een overzicht van de laatste x mailings
	 */
	static public function makeMailingsOverzicht(){
		$res = new HtmlDiv();

		$mailings = MailingVerzameling::onverzondenMailings();
		$res->add(new HtmlHeader(3, _("De volgende mailings zijn wel beschikbaar maar zijn (nog) niet verzonden")));
		if($mailings->aantal() == 0) {
			$res->add(new HtmlEmphasis(_("Er zijn geen mailings")));
		} else {
			$res->add(MailingVerzamelingView::makeMailingTable($mailings));
		}

		$mailings = MailingVerzameling::teVerzendenMailings();
		$res->add(new HtmlHeader(3, _("De volgende mailings staan op het punt verzonden te worden")));
		if($mailings->aantal() == 0) {
			$res->add(new HtmlEmphasis(_("Er zijn geen mailings")));
		} else {
			$res->add(MailingVerzamelingView::makeMailingTable($mailings));
		}

		$mailings = MailingVerzameling::verzondenMailings(10);
		$res->add(new HtmlHeader(3, _("Dit zijn de tien laatst verzonden mailings")));
		if($mailings->aantal() == 0) {
			$res->add(new HtmlEmphasis(_("Er zijn geen mailings")));
		} else {
			$res->add(MailingVerzamelingView::makeMailingTable($mailings));
		}

		return $res;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
