<?

/**
 * $Id$
 */
abstract class MailingView
	extends MailingView_Generated
{
	static public function labelMailingLists(Mailing $mailing)
	{
		return _("Mailinglijsten");
	}

	static public function labelGekoppeldeUrls(Mailing $mailing)
	{
		return _("Gekoppelde Url's");
	}

	static public function labelBestanden(Mailing $mailing)
	{
		return _("Bestanden");
	}

	static public function labelMailFrom(Mailing $mailing)
	{
		return _("Afzender");
	}

	static public function labelMailSubject(Mailing $mailing)
	{
		return _("Onderwerp");
	}

	static public function waardeMailingLists(Mailing $mailing)
	{
		$lists = $mailing->getMailingLists();
		$first = true;
		$return = '';
		foreach($lists as $list) {
			if(!$first) {
				$return .= ', ';
			}
			$return .= new HtmlAnchor($list->url(), MailingListView::waardeNaam($list));
			$first = false;
		};

		return $return;
	}

	static public function waardeMomentIngepland(Mailing $mailing)
	{
		if($mailing->getMomentIngepland()->hasTime()) {
			return parent::waardeMomentIngepland($mailing);
		}
		return _("Nog niet ingepland");
	}

	static public function waardeMomentVerwerkt(Mailing $mailing)
	{
		if($mailing->getMomentVerwerkt()->hasTime()) {
			return parent::waardeMomentVerwerkt($mailing);
		}
		return _("Nog niet verzonden");
	}

	static public function waardeGekoppeldeUrls(Mailing $mailing)
	{
		$return = '';

		$tmurls = TinyUrlVerzameling::vanMailing($mailing);

		if($tmurls->aantal() > 0){
			$return = sprintf(_("Er zijn %s url(s) aan deze mailing gekoppeld met in totaal %s hits.")
				, new HtmlAnchor($mailing->url() ."Urls", $tmurls->aantal())
				, $tmurls->totaalHits());
		} else {
			$return = sprintf(_("Er zijn nog geen TinyUrls aan deze mailing %s")
				, new HtmlAnchor($mailing->url() ."Urls", _("gekoppeld")));
		}

		return $return;
	}

	static public function waardeBestanden(Mailing $mailing)
	{
		$return = '';

		if($mailing->getFileNames())
		{
			$aantal = count($mailing->getFileNames());
			$return = sprintf(_("Er zijn %s bestanden gevonden, bekijk het %s")
				, $aantal
				, new HtmlAnchor($mailing->url() . "Attachments", "lijstje"));
		} else {
			$return = _("Er zijn geen bestanden gevonden, maar ze kunnen er wel zijn.");
		}

		return $return;
	}

	static public function waardeMailSubject(Mailing $mailing)
	{
		return mb_decode_mimeheader(parent::waardeMailSubject($mailing));
	}

	static public function standaardPlainTextHeaders()
	{
		return "MIME-Version: 1.0; Content-type: text/plain; charset=utf-8";
	}

	/**
	 * Geef een lijst met from-adressen waar de gebruiker uit kan kiezen.
	 * (is nog niet ge-escaped!)
	 */
	static public function mailFromArray()
	{
		global $BESTUUR;

		// Lijst met from-adressen opstellen
		$from = array();

		if (hasAuth('bestuur')){
			// voeg alle bestuursfuncties toe
			foreach ($BESTUUR as $functie => $lid) {
				$from[] = $lid->getEmail();
			}

			// Arbeidsmarktorientatie mag alleen vanuit het bestuur gestuurd worden.
			$from[] = '"Arbeidsmarktorientatie" <Arbeidsmarktorientatie@A-Eskwadraat.nl>';
		}

		// Commissies van deze persoon opzoeken
		foreach (CommissieVerzameling::vanPersoon(Persoon::getIngelogd()) as $cie)
			$from[] = '"'.$cie->getNaam().'" <'.$cie->getEmail().'>';

		return $from;
	}

	/**
	 * Maakt de view voor het importeren van een extern bestand
	 */
	public static function makeEMLImportInterface(Mailing $mailing)
	{
		$page = Page::getInstance()->start("E-mail importeren");

		$page->add(new HtmlSpan('Hieronder kun je een e-mail importeren om te versturen als ' .
			'mailing naar een grote groep mensen. De te importeren e-mail moet ' .
			'plaintext zijn opgeslagen op een door de website leesbare locatie, ' .
			'bijvoorbeeld op /scratch. Hieronder kun je de locatie van het ' .
			'e-mailbestand opgeven.'));

		$page->add(new HtmlBreak());
		$page->add(new HtmlBreak());

		$page->add($form = HtmlForm::named('importeren'));
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeText('eml_path', tryPar('eml_path')));
		$form->add(HtmlInput::makeSubmitButton(_("Importeer!")));

		if ($mailing->getMailBody() != ""){
			$page->add(new HtmlSpan('Let op! De bestaande tekst van je mailing wordt vervangen ' .
				'door de tekst uit het te importeren bestand!', 'text-danger strongtext'));
		}

		$page->add(new HtmlBreak());

		$page->add(new HtmlSpan(sprintf(_("Klik %s om terug naar de mailing te gaan."),
				new HtmlAnchor($mailing->url(), _("hier")))));

		$page->end();
	}

	/**
	 * Maakt de view voor het versturen van een preview
	 */
	public static function makePreviewInterface(Mailing $mailing)
	{
		$page = Page::getInstance()->start(sprintf(_("Preview van mailing %s versturen"), $mailing->geefID()));

		$page->add($res = new HtmlDiv());

		$res->add(sprintf(_('Je staat op het punt om een preview te versturen van ' .
			'de mailing "%s". Hieronder kun ' .
			'je aangeven naar welke adressen je de preview wil versturen. ' .
			'Als je meerdere e-mailadressen wil gebruiken, dan dien je deze ' .
			'te scheiden met een komma.'), MailingView::waardeOmschrijving($mailing)));

		$res->add(new HtmlBreak());
		$res->add(new HtmlBreak());

		$res->add($form = HtmlForm::named('preview'));
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeText('preview_recipients'));
		$form->add(HtmlInput::makeSubmitButton(_("Preview versturen!")));

		$res->add(new HtmlBreak());

		$res->add(sprintf(_('Als je bij nader inzien toch liever geen preview wil versturen, '
			. 'klik dan %s om terug te gaan naar het detailoverzicht van deze mailing')
			, new HtmlAnchor($mailing->url(), _('hier'))));

		$page->end();
	}

	/**
		Maakt interface om details van een mailing te bekijken en/of bewerken.
		De boolean $editable geeft aan of velden door forms vervangen moeten
		worden om ze editable te maken. Eventuele wijzigingen worden ook door
		deze methode in het Mailing-object doorgevoerd.
	**/
	public static function makeMailingDetailsInterface($mailing, $editable = false)
	{
		$page = Page::getInstance()->start(sprintf(_("Details van mailing %s"), $mailing->geefID()));

		if ($mailing === null){
			// Nieuwe mailing
			$mailing = Mailing::constructNewPlaintextMailing();
		} elseif (!$mailing instanceof Mailing){
			// Error!
			user_error("Geen Mailing-object opgegeven bij MailingView::makeMailingDetailsInterface(...)!", E_USER_ERROR);
		}

		if ($mailing->isVerzonden() && $editable){
			// Mailing is al verwerkt, hier valt niets aan te bewerken!
			user_error("Deze mailing is al verwerkt (en dus verzonden), je kunt deze niet bewerken!", E_USER_ERROR);
		}

		$page->add($res = new HtmlDiv());

		$res->add(MailingView::makeMailingInternalsTable($mailing, $editable));
		$res->add(MailingView::makeMailingExternalsTable($mailing, $editable));

		$page->end();
	}

		/**
		Maakt een tabel met interne details (omschrijving, commissie, etc.) van
		deze mailing. Boolean $editable geeft aan of de velden bewerkbaar
		moeten zijn.
		**/
	public static function makeMailingInternalsTable(Mailing $mailing, $editable)
	{
		$res = new HtmlDiv();

		$res->add(new HtmlHeader(3, _("Interne informatie")));

		$res->add($table = new HtmlTable());

		// Omschrijving
		$row = $table->addRow();
		$row->addData(MailingView::labelOmschrijving($mailing));
		$row->addData(MailingView::waardeOmschrijving($mailing));

		// Commissie
		$row = $table->addRow();
		$row->addData(MailingView::labelCommissie($mailing));
		$row->addData(MailingView::waardeCommissie($mailing));

		// Mailinglists	
		// Lijst van mailinglist maken
		$row = $table->addRow();
		$row->addData(MailingView::labelMailingLists($mailing));
		$row->addData(MailingView::waardeMailingLists($mailing));
		if ($mailing->getMailingLists()->aantal() > 1){
			$res->add(new HtmlEmphasis('Let op: je hebt meerdere mailinglists ' .
				'geselecteerd om naar toe te mailen. Je dient zelf ' .
				'eventueel bij &quot;Onderwerp&quot; een prefix ' .
				'toe te voegen. Voorbeeld: ' .
				'&quot;[A-Eskwadraat activiteiten] (...)&quot'));
		}

		// Definitief, ja of nee?
		$row = $table->addRow();
		$row->addData(MailingView::labelIsDefinitief($mailing));
		$row->addData(MailingView::waardeIsDefinitief($mailing));

		// Ingeplande verzenddatum
		$row = $table->addRow();
		$row->addData(MailingView::labelMomentIngepland($mailing));
		$row->addData(MailingView::waardeMomentIngepland($mailing));

		// Datum waarop de mailing is verzonden (sowieso niet editable)
		$row = $table->addRow();
		$row->addData(MailingView::labelMomentVerwerkt($mailing));
		$row->addData(MailingView::waardeMomentVerwerkt($mailing));

		// Gekoppelde urls
		$row = $table->addRow();
		$row->addData(MailingView::labelGekoppeldeUrls($mailing));
		$row->addData(MailingView::waardeGekoppeldeUrls($mailing));

		//Automatische gevonden bestanden
		$row = $table->addRow();
		$row->addData(MailingView::labelBestanden($mailing));
		$row->addData(MailingView::waardeBestanden($mailing));

		return $res;
	}

	/**
		Externe details (afzender, onderwerp, mail headers, mail body)
	**/
	public static function makeMailingExternalsTable(Mailing $mailing, $editable)
	{
		$res = new HtmlDiv();

		$res->add(new HtmlHeader(3, _("Inhoudelijke informatie")));

		$res->add($table = new HtmlTable());

		if ($mailing->isVerzonden()) $editable = false;

		// Afzender
		$row = $table->addRow();
		$row->addData(MailingView::labelMailFrom($mailing));
		$row->addData(MailingView::waardeMailFrom($mailing));

		$mailinglists = $mailing->getMailingLists();

		// Onderwerp
		$subjectprefix = false;
		if ($mailinglists->aantal() == 1){
			// Prefix/suffix toevoegen
			$mailinglist = $mailinglists->first();
			if ($mailinglist->getSubjectPrefix()){
				$subjectprefix = new HtmlEmphasis(MailingListView::waardeSubjectPrefix($mailinglist));
			}
		} // else: 0 of > 1 mailinglists gekoppeld: geen prefix te bepalen

		$row = $table->addRow();
		$row->addData(MailingView::labelMailSubject($mailing));
		if($subjectprefix) {
			$row->addData(MailingView::waardeMailSubject($mailing) . " " . $subjectprefix);
		} else {
			$row->addData(MailingView::waardeMailSubject($mailing));
		}
		if ($subjectprefix){
			$res->add(new HtmlEmphasis(sprintf("De prefix '%s' wordt "
				. "automatisch toegevoegd voor mailinglist '%s'"
				, trim($subjectprefix)
				, MailingListView::waardeNaam($mailinglist))));
		} elseif ($mailinglists->aantal() == 0){
			$res->add(new HtmlEmphasis("Let op: je hebt nog geen mailinglist(s) voor deze "
				. "mailing geselecteerd. Door een mailinglist te selecteren "
				. "kan er automatisch een prefix toegevoegd worden aan het "
				. "onderwerp"));
		} elseif ($mailinglists->aantal() > 1){
			$res->add(new HtmlEmphasis("Let op: je hebt meerdere mailinglist(s) voor deze "
				. "mailing geselecteerd, hierdoor kan er geen eenduidige prefix "
				. "worden toegevoegd aan het onderwerp van de mailing. Je dient "
				. "zelf een prefix toe te voegen."));
		}

		// Headers (zijn nooit editable!)
		$res->add(new HtmlHeader(4, _("Mailing headers")));
		$res->add(new HtmlPre(MailingView::waardeMailHeaders($mailing)));

		// Body (enkel editable als het een plaintext mailing is)
		$body_editable = $mailing->isPlainTextMailing();

		$res->add(new HtmlHeader(4, _("Mailing body")));

		$res->add($pre = new HtmlPre(htmlspecialchars($mailing->composeFinalMailBody())));
		$pre->setCssStyle("max-height: 300px;");

		// Mogelijke acties (alleen tonen als de mailing niet bewerkt wordt en
		// tevens nog niet verzonden is)
		if (!$mailing->isVerzonden()) {
			$res->add(HtmlAnchor::button($mailing->url() . 'Preview', _('Preview versturen')));
			$res->add(HtmlAnchor::button($mailing->url() . 'ImportEML', _('Mail importeren uit bestand')));
		}

		return $res;
	}

	static function printSentMailing(Mailing $mailing, $type = "plaintext"){
		$filename = $mailing->getMailSentPath();
		if (file_exists($filename)){
			$parser = MailParser::parseFile($filename);
			if ($type === "plaintext"){
				$lines = $parser->getPlainText();
				if ($lines === false){
					// Mail bevat geen plaintext part
					spaceHTTP(404);
				} else {
					header("Content-Type: text/plain");
				}
			} elseif ($type === "html"){
				$lines = $parser->getHTML(new HtmlAnchor($mailing->url() . "/Sent?type=MIME"));
				if ($lines === false){
					// Geen HTML part
					spaceHTTP(404);
				} else {
					header("Content-Type: text/html");
				}
			} else {
				spaceHTTP(404);
			}

			echo implode("\n", $lines);
		} else {
			// File does not exist
			spaceHTTP(404);
		}
	}

	static function printMIMEContentBlock(Mailing $mailing, $contentIDHash){
		$filename = $mailing->getMailSentPath();
		$parser = MailParser::parseFile($filename);
		$part = $parser->getMIMEPartByContentIDHash($contentIDHash);

		if ($part === false){
			spaceHTTP(404);
		} else {
			if ($part->getHeaderValue("Content-Transfer-Encoding") == "base64"){
				if ($part->getHeaderValue("Content-Type")){
					header("Content-Type: " . $part->getHeaderValue("Content-Type"));
				}
				echo base64_decode(implode("\n", $part->getBody()));
			} else {
				// Cannot decode
				echo "MIME block not encoded using base64 encoding, sorry.";
			}
		}
	}

	/**
		 Maakt webinterface voor het tonen van een reeds verzonden mailing
		TODO: deze pagina moet nog gefixt worden GEEN IFRAME!
	**/
	public static function makeSentMailingInterface(Mailing $mailing){
		$filename = $mailing->getMailSentPath();
		$parser = MailParser::parseFile($filename);
		if (!$parser){
			return "Deze mailing is helaas niet te vinden in het archief!";
		}

		if ($parser->hasHtml()){
			$url = "/Sent?type=html";
		} else {
			$url = "/Sent?type=plaintext";
		}

		$res = new HtmlDiv();
		$table = new HtmlTable();
		$table->add(new HtmlTableRow(array(
			$lblSubj = new HtmlTableDataCell("Onderwerp:"),
			new HtmlTableDataCell($parser->getHeaderValue("Subject"))
		)));
		$table->add(new HtmlTableRow(array(
			$lblDate = new HtmlTableDataCell("Datum:"),
			new HtmlTableDataCell($parser->getHeaderValue("Date"))
		)));
		$lblSubj->setCssStyle("font-weight: bold; padding-right: 20px");
		$lblDate->setCssStyle("font-weight: bold; padding-right: 20px");
		$res->add($table);
		$res->add("<hr>");

		// Stukje javascript om IFrame de grootte van de content te laten hebben
		$script = HtmlScript::makeJavascript('
function autoIframe(frameId){
		frame = document.getElementById(frameId);
		innerDoc = (frame.contentDocument) ? frame.contentDocument : frame.contentWindow.document;
		objToResize = (frame.style) ? frame.style : frame;
		newHeight = innerDoc.body.scrollHeight + 50;
		objToResize.height = newHeight;
}');
		$res->add($script);

		$iframe = new HtmlIFrame($mailing->makeLink(false, $url));
		$iframe->setId("iframe-mailing");
		$iframe->setAttribute("onload", 'window.parent.autoIframe("iframe-mailing");');
		$iframe->setCssStyle("width: 100%;");
		$res->add($iframe);
		return $res->makeHtml();
	}

	/**
	 * Maakt het form voor het wijzigen van een mailing en voor een nieuwe
	 * mailing maken
	 */
	static public function wijzigForm(Mailing $mailing, $nieuw = false, $show_error)
	{
		$page = Page::getInstance();

		if($nieuw) {
			$page->start(_("Nieuwe mailing aanmaken"));

			$page->add($form = HtmlForm::named('MailingToevoegen'));
		} else {
			$page->start(sprintf(_("Wijzig mailing %s"), $mailing->geefID()));

			$page->add($form = HtmlForm::named('MailingWijzigen'));
		}

		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($mailing, 'omschrijving', $show_error))
			->add(self::wijzigTR($mailing, 'commissie', $show_error))
			->add(self::wijzigTR($mailing, 'mailinglists', $show_error))
			->add(self::wijzigTR($mailing, 'isDefinitief', $show_error))
			->add(self::wijzigTR($mailing, 'momentIngepland', $show_error))
			->add(self::wijzigTR($mailing, 'mailFrom', $show_error))
			->add(self::wijzigTR($mailing, 'mailSubject', $show_error));

		$form->add(new HtmlHeader(4, _("Headers")));
		$form->add(new HtmlSpan(_("De headers van een mailing kunnen niet bewerkt worden"), 'text-danger strongtext'));
		if($nieuw) {
			$form->add(new HtmlPre(MailingView::standaardPlainTextHeaders()));
			$form->add(HtmlInput::makeHidden('Mailing[MailHeaders]', MailingView::standaardPlainTextHeaders()));
		} else {
			$form->add(new HtmlPre(MailingView::waardeMailHeaders($mailing)));
			$form->add(HtmlInput::makeHidden('Mailing[MailHeaders]', $mailing->getMailHeaders()));
		}

		$form->add(new HtmlHeader(4, _("MailBody")));
		$mailBody = tryPar('Mailing[MailBody]', $mailing->getMailBody());
		$form->add(HtmlTextarea::withContent(htmlspecialchars($mailBody), 'Mailing[MailBody]', null, 15));

		$form->add(new HtmlBreak());
		$form->add(new HtmlBreak());

		if($nieuw) {
			$form->add(HtmlInput::makeSubmitButton("Voer in"));
		} else {
			$form->add(HtmlInput::makeSubmitButton("Wijzig"));
		}

		$page->end();
	}

	/**
	 * De functie om een form van de wijzigForm te processen
	 */
	static public function processNieuwForm(Mailing $mailing)
	{
		if(!$data = tryPar(self::formobj($mailing), NULL))
			return false;

		self::processForm($mailing);

		$from = MailingView::mailFromArray();
		$mailing->setMailFrom($from[tryPar('Mailing[MailFrom]', NULL)]);
	}

	/**
	 * Geeft de view voor het tonen van de tinyurlkoppelingen aan de mailing
	 */
	public static function Urls(Mailing $mailing)
	{
		$page = Page::getInstance()->start(_('MailingUrls van mailing'));

		//Haal de mailingbody op (we doen dus niets met plaintext mailings op 't moment).
		$path = $mailing->getMailBodyPath();
		if(file_exists($path) && filesize($path) != 0) {
			$fp = fopen($path, "r");
			$text = fread($fp, filesize($path));
			fclose($fp);

			//En match op href=, zowel met "" (double quotes) als '' (singel quotes), nare
			// is bv dat href='a.bc/bobjan"wit"baard' een valide url is. Beide quotes samen
			// in een regex is naar omdat er ook nog onderlinge afhankelijkeheden zijn.
			preg_match_all('/href="[^"]*"/' , $text, $urlsdq);
			preg_match_all("/href='[^']*'/" , $text, $urlssq);

			//For sake of gemak, haal een niveau weg.
			$urlsdq = $urlsdq[0];
			$urlssq = $urlssq[0];

			//Haal alle dubbele urls er uit. Later zal str_replace *alle* occurences toch vervangen.
			$urlsdq = array_unique($urlsdq);
			$urlssq = array_unique($urlssq);

			$tmurls = TinyUrlVerzameling::vanMailing($mailing);
			$tmurlsStrings = array();
			foreach($tmurls as $tmurl) {
				$tmurlsStrings[] = $tmurl->getOutgoingUrl();
			}

			$urlsdqStrings = array();
			foreach($urlsdq as $i => $dqurl) {
				preg_match('/"([^"]*)"/' , $dqurl, $displayUrl);
				$urlsdqStrings[$i] = $displayUrl[1];
			}
			$urlssqStrings = array();
			foreach($urlssq as $i => $squrl) {
				preg_match("/'([^']*)'/" , $squrl, $displayUrl);
				$urlssqStrings[$i] = $displayUrl[1];
			}

			//Verwerk de geslecteerde urls.
			if (Token::processNamedForm() == "urlsConvert")
			{
				//Ga voor iedere url kijken of we wat moeten doen, zo ja, maak een TinyUrl,
				// link deze met een TinyMailingUrl en str_replace de url in de mailing.
				foreach($urlsdq as $id => $dqurl)
				{
					if(trypar("Dq_$id") == 'on') {
						preg_match('/"([^"]*)"/' , $dqurl, $displayUrl);
						$turl = new TinyUrl();
						$turl->setTinyUrl(TinyUrl::generateTinyUrl());
						$turl->setOutgoingUrl($displayUrl[1]);
						$tmurl = new TinyMailingUrl($mailing, $turl);
						$turl->opslaan();
						$tmurl->opslaan();
						$text = str_replace($dqurl, "href=\"http://a-es2.nl/t/".$turl->getTinyUrl()."\"", $text);
						$page->add($displayUrl[1] . _(" is geconverteerd naar: ") . "http://a-es2.nl/t/".$turl->getTinyUrl()
							. new HtmlBreak());
					}
				}

				foreach($urlssq as $id => $squrl)
				{
					if(trypar("Sq_$id") == 'on') {
						preg_match("/'([^']*)'/" , $squrl, $displayUrl);
						$turl = new TinyUrl();
						$turl->setTinyUrl(TinyUrl::generateTinyUrl());
						$turl->setOutgoingUrl($displayUrl[1]);
						$tmurl = new TinyMailingUrl($mailing, $turl);
						$turl->opslaan();
						$tmurl->opslaan();
						$text = str_replace($squrl, "href=\"http://a-es2.nl/t/".$turl->getTinyUrl()."\"", $text);
						$page->add($displayUrl[1] . _(" is geconverteerd naar ") . "http://a-es2.nl/t/".$turl->getTinyUrl()
							. new HtmlBreak());
					}
				}

				$mailing->setMailBody($text);
			} elseif(count(array_merge($urlsdqStrings, $urlssqStrings)) > 0) {

				//Laat zien wat er nu al gelinkt is.
				if($tmurls->aantal() > 0){
					$tmurlsDiv = new HtmlDiv();
					$tmurlsDiv->add(new HtmlHeader(3, _("Gekoppelde urls")))
						->add(new HtmlParagraph(_("Hieronder staan alle TinyUrls die aan deze mailing gekoppeld zijn.")))
						->add(TinyUrlVerzamelingView::lijst($tmurls));
					$page->add($tmurlsDiv);
				}

				if(count(array_diff(array_merge($urlsdqStrings, $urlssqStrings), $tmurlsStrings)) > 0) {
					/**
					 *	Form om de urls te bekijken die je kan selecteren.
					 **/
					$div = new HtmlDiv();
					$div->add(new HtmlHeader(3, _("Aanpasbare urls")))
						->add($par = new HtmlParagraph( _("De onderstaande urls kunnen automagische worden vervangen door TinyUrls. 
							Als je urls in de mailing wil vervangen die hier niet tussen staan dan kan je dit handmatig 
							doen voor dat de mailing naar MailingWeb wordt gestuurd door er zelf TinyUrls in te zetten.")))
						->add(new HtmlParagraph(_("Selecteer de URLS die je wil converteren naar TinyUrls.")));

						if(!hasAuth("bestuur"))
							$par->add(" ". _("Vraag de ComExt of een ander tof bestuurslid om een TinyUrl voor je te maken."));

					$form = HtmlForm::named("urlsConvert");
					$form->addClass('form-inline');
					$table = new HtmlTable();
					$trow = new HtmlTableRow();
					$trow->addHeader("X");
					$trow->addHeader(_("Link"));
					$table->add($trow);

					foreach($urlsdq as $id => $dqurl)
					{
						preg_match('/"([^"]*)"/' , $dqurl, $displayUrl);
						if(!in_array($displayUrl[1], array_diff($urlsdqStrings, $tmurlsStrings)))
							continue;
						$trow = new HtmlTableRow();
						$trow->addData(HtmlInput::makeCheckbox("Dq_$id", null, "[Dq][$id]"));
						$trow->addData($displayUrl[1]);
						$table->add($trow);
					}

					foreach($urlssq as $id => $squrl)
					{
						preg_match("/'([^']*)'/" , $squrl, $displayUrl);
						if(!in_array($displayUrl[1], array_diff($urlssqStrings, $tmurlsStrings)))
							continue;
						$trow = new HtmlTableRow();
						$trow->addData(HtmlInput::makeCheckbox("Sq_$id", null, "[Sq][$id]"));
						$trow->addData($displayUrl[1]);
						$table->add($trow);
					}

					$form->add($table);
					if($mailing->isVerzonden()) $form->add(_("Let op: deze mailing is al verzonden!"));
					$form->add(HtmlInput::makeSubmitButton(_("Converteren")));
					$div->add($form);

					$page->add($div);
				}
			} else {
				//Er is niets om te doen! nooit!
				$div = new HtmlDiv();
				$div->add(new HtmlHeader(3, _("Aanpasbare urls")))
					->add($par = new HtmlParagraph(_("Er zijn helaas geen automatische aanpasbare urls in de deze mailing
						 gevonden. Mochten er toch urls in deze mailing zitten die je wilt aanpassen dan moet je dit
						 handmatig doen voordat je de mailing naar MailingWeb stuurt door er zelf TinyUrls in te
						 zetten.")));
				$page->add($div);
				if(!hasAuth("bestuur"))
					$par->add(" ". _("Vraag de ComExt of een ander tof bestuurslid om een TinyUrl voor je te maken."));
			}

			$page->add(HtmlAnchor::button($mailing->url(), sprintf(_("Terug naar de %s"), _("details pagina"))));
		} else {
			$page->add(_("Deze mailing heeft geen body of een lege body"));
		}

		$page->end();
	}

	/**
	 * Geeft de view voor het weergeven van de attachments van de mailing
	 */
	static public function attachments(Mailing $mailing)
	{
		$page = Page::getInstance()->start('MailingWeb: Attachements');

		$realFilenames = $mailing->getFileNames();

		$page->add( new HtmlHeader(3, _("Gevonden bestanden")));

		if(count($realFilenames) > 0 )
		{
			$table = new HtmlTable();

			$trow = new HtmlTableRow();
			$trow->addHeader(_("Bestandsnaam"));
			$table->add($trow);

			foreach($realFilenames as $id => $filename){
				$trow = new HtmlTableRow();
				$trow->addData($filename[1]);
				$table->add($trow);
			}

			$page->add(new HtmlParagraph(_("De volgende bestanden zijn automatisch gevonden in de mail, dit zijn zowel attachments als inline bestanden. Doordat je soms vieze truukjes kan uithalen in emails is het mogelijk dat er stiekem nog meer in zitten...")))
				->add($table);
		} else {
			$page->add(new HtmlParagraph(_("Er zijn geen bestanden gevonden, maar doordat je soms vieze truukjes kan uithalen in emails is het mogelijk dat er stiekem nog meer in zitten...")));
		}

		$page->end();
	}

	static public function formCommissie(Mailing $mailing, $include_id = false)
	{
		// Lijst (single-select) commissies tonen
		$cies = array();
		$ciesVerzameling = CommissieVerzameling::huidige('ALLE');
		foreach ($ciesVerzameling as $cie)
			$cies[$cie->geefID()] = $cie->getNaam();

		$select = HtmlSelectbox::fromArray('Mailing[Commissie]', $cies,
			tryPar('Mailing[Commissie]', $mailing->getCommissieCommissieID()), 1);

		return $select;
	}

	static public function formMailingLists(Mailing $mailing, $include_id = false)
	{
		$mailinglists = MailingListVerzameling::beschikbaar();
		$mailinglists_arr = array();
		foreach ($mailinglists as $mailinglist) {
			$mailinglists_arr[$mailinglist->getMailingListId()] = $mailinglist->getNaam();
		}

		$lists = $mailing->getMailingLists();
		$ids = array();
		foreach($lists as $list) {
			$ids[] = $list->getMailingListId();
		}
		$select = HtmlSelectbox::fromArray('Lists', $mailinglists_arr,
			tryPar('Lists', $ids), 3);
		$select->setMultiple();

		return $select;
	}

	static public function formMailFrom(Mailing $mailing, $include_id = false)
	{
		$from = array_map("htmlspecialchars", MailingView::mailFromArray());

		$select = HtmlSelectbox::fromArray('Mailing[MailFrom]', $from,
			array_search(tryPar('Mailing[MailFrom]', $mailing->getMailFrom()), MailingView::mailFromArray())
			, 1);

		return $select;
	}

	static public function opmerkingMailingLists()
	{
		return _("Als je meerdere mailinglists selecteert, \n"
			. "moet je hieronder bij \"Onderwerp\" zelf een prefix toevoegen. \n"
			. "Voorbeeld: \"[A-Eskwadraat activiteiten] (...)\"");
	}

	static public function opmerkingMailSubject()
	{
		return _("Door een mailinglist te selecteren kan er automatisch een prefix toegevoegd worden aan het onderwerp");
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
