<?

/**
 * $Id$
 */
abstract class MailingListVerzamelingView
	extends MailingListVerzamelingView_Generated
{
	/**
	 * Geeft de view voor het overzicht van alle mailinglists
	 */
	static public function mailingListsOverzicht()
	{
		$mlists = MailingListVerzameling::alle();

		$page = Page::getInstance()->start('Mailinglistoverzicht');

		$page->add(self::mailingListTable($mlists));

		if(hasAuth('bestuur')) {
			$page->add(HtmlAnchor::button(MAILINGWEB . "MailingLists/Nieuw",
				_("Maak nieuwe mailinglist aan")));
		}

		$page->end();
	}

	/**
		Retourneert een HTML tabel met daarin informatie over de meegegeven
		mailinglists
	**/
	static public function mailingListTable(MailingListVerzameling $mailinglists, $uitgebreid = false)
	{
		$table = new HtmlTable();
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		// Column headers
		$thead->add($row = new HtmlTableRow());
		$row->addHeader(_("Naam"));
		$row->addHeader(_("Omschrijving"));
		$row->addHeader(_("Prefix onderwerp"));
		if($uitgebreid) {
			$row->addHeader(_("Prefix mail body"));
			$row->addHeader(_("Suffix mail body"));
		}

		foreach ($mailinglists as $mailinglist){
			$tbody->add($row = new HtmlTableRow());
			$row->addData(MailingListView::makeLink($mailinglist));
			$row->addData(MailingListView::waardeOmschrijving($mailinglist));
			$row->addData(MailingListView::waardeSubjectPrefix($mailinglist));
			if($uitgebreid) {
				$row->addData(MailingListView::waardeBodyPrefix($mailinglist));
				$row->addData(MailingListView::waardeBodySuffix($mailinglist));
			}
		}

		return $table;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
