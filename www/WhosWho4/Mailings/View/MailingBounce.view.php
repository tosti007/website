<?

/**
 * $Id$
 */
abstract class MailingBounceView
	extends MailingBounceView_Generated
{

	//Voor stand-alone view van een bounce.
	public function pagina()
	{
		$page = Page::getInstance();
		$page->start('Bounce: ' . $this->getBounceID);

		$page->add(new HtmlParagraph(_('Hieronder de inhoud van de bounce;')));
		$page->add(new HtmlPre($this->getBounceContent()));

		$page->end();
	}

	//Geef een regel met de meeste informatie over de bounce
	static public function lineDescription($bounce)
	{
		$string = '';
		$string .= $bounce->getMailingBounceID() . ': '
				. $bounce->getGewijzigdWanneer()->strftime('%Y-%m-%d %H:%M:%S');
		$mailing = $bounce->getMailing();
		if($mailing)
			$string .= ' (mailing ' . $mailing->getMailingID() . ': ' . $mailing->getMailSubject() . ')';

		return $string;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
