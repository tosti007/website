<?
abstract class MailingListView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingListView.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingList(MailingList $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mailingListID.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailingListID labelt.
	 */
	public static function labelMailingListID(MailingList $obj)
	{
		return 'MailingListID';
	}
	/**
	 * @brief Geef de waarde van het veld mailingListID.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailingListID van het object
	 * obj representeert.
	 */
	public static function waardeMailingListID(MailingList $obj)
	{
		return static::defaultWaardeInt($obj, 'MailingListID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailingListID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailingListID representeert.
	 */
	public static function opmerkingMailingListID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(MailingList $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(MailingList $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(MailingList $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(MailingList $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(MailingList $obj)
	{
		return static::defaultWaardeText($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(MailingList $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld subjectPrefix.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld subjectPrefix labelt.
	 */
	public static function labelSubjectPrefix(MailingList $obj)
	{
		return 'SubjectPrefix';
	}
	/**
	 * @brief Geef de waarde van het veld subjectPrefix.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld subjectPrefix van het object
	 * obj representeert.
	 */
	public static function waardeSubjectPrefix(MailingList $obj)
	{
		return static::defaultWaardeString($obj, 'SubjectPrefix');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld subjectPrefix.
	 *
	 * @see genericFormsubjectPrefix
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld subjectPrefix staat en kan
	 * worden bewerkt. Indien subjectPrefix read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formSubjectPrefix(MailingList $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'SubjectPrefix', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld subjectPrefix. In
	 * tegenstelling tot formsubjectPrefix moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formsubjectPrefix
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld subjectPrefix staat en kan
	 * worden bewerkt. Indien subjectPrefix read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormSubjectPrefix($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'SubjectPrefix', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * subjectPrefix bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld subjectPrefix representeert.
	 */
	public static function opmerkingSubjectPrefix()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bodyPrefix.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bodyPrefix labelt.
	 */
	public static function labelBodyPrefix(MailingList $obj)
	{
		return 'BodyPrefix';
	}
	/**
	 * @brief Geef de waarde van het veld bodyPrefix.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bodyPrefix van het object obj
	 * representeert.
	 */
	public static function waardeBodyPrefix(MailingList $obj)
	{
		return static::defaultWaardeText($obj, 'BodyPrefix');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bodyPrefix.
	 *
	 * @see genericFormbodyPrefix
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bodyPrefix staat en kan
	 * worden bewerkt. Indien bodyPrefix read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBodyPrefix(MailingList $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'BodyPrefix', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bodyPrefix. In
	 * tegenstelling tot formbodyPrefix moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbodyPrefix
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bodyPrefix staat en kan
	 * worden bewerkt. Indien bodyPrefix read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBodyPrefix($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'BodyPrefix', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bodyPrefix bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bodyPrefix representeert.
	 */
	public static function opmerkingBodyPrefix()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bodySuffix.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bodySuffix labelt.
	 */
	public static function labelBodySuffix(MailingList $obj)
	{
		return 'BodySuffix';
	}
	/**
	 * @brief Geef de waarde van het veld bodySuffix.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bodySuffix van het object obj
	 * representeert.
	 */
	public static function waardeBodySuffix(MailingList $obj)
	{
		return static::defaultWaardeText($obj, 'BodySuffix');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bodySuffix.
	 *
	 * @see genericFormbodySuffix
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bodySuffix staat en kan
	 * worden bewerkt. Indien bodySuffix read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBodySuffix(MailingList $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'BodySuffix', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bodySuffix. In
	 * tegenstelling tot formbodySuffix moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbodySuffix
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bodySuffix staat en kan
	 * worden bewerkt. Indien bodySuffix read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBodySuffix($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'BodySuffix', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bodySuffix bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bodySuffix representeert.
	 */
	public static function opmerkingBodySuffix()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld verborgen.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld verborgen labelt.
	 */
	public static function labelVerborgen(MailingList $obj)
	{
		return 'Verborgen';
	}
	/**
	 * @brief Geef de waarde van het veld verborgen.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld verborgen van het object obj
	 * representeert.
	 */
	public static function waardeVerborgen(MailingList $obj)
	{
		return static::defaultWaardeBool($obj, 'Verborgen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld verborgen.
	 *
	 * @see genericFormverborgen
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verborgen staat en kan
	 * worden bewerkt. Indien verborgen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVerborgen(MailingList $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Verborgen', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld verborgen. In
	 * tegenstelling tot formverborgen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formverborgen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verborgen staat en kan
	 * worden bewerkt. Indien verborgen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVerborgen($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Verborgen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * verborgen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld verborgen representeert.
	 */
	public static function opmerkingVerborgen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld query.
	 *
	 * @param MailingList $obj Het MailingList-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld query labelt.
	 */
	public static function labelQuery(MailingList $obj)
	{
		return 'Query';
	}
	/**
	 * @brief Geef de waarde van het veld query.
	 *
	 * @param MailingList $obj Het MailingList-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld query van het object obj
	 * representeert.
	 */
	public static function waardeQuery(MailingList $obj)
	{
		return static::defaultWaardeText($obj, 'Query');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld query.
	 *
	 * @see genericFormquery
	 *
	 * @param MailingList $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld query staat en kan worden
	 * bewerkt. Indien query read-only is betreft het een statisch html-element.
	 */
	public static function formQuery(MailingList $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Query', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld query. In
	 * tegenstelling tot formquery moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formquery
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld query staat en kan worden
	 * bewerkt. Indien query read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormQuery($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Query', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld query
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld query representeert.
	 */
	public static function opmerkingQuery()
	{
		return NULL;
	}
}
