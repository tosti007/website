<?
abstract class MailingBounceView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingBounceView.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingBounce(MailingBounce $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mailingBounceID.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld mailingBounceID labelt.
	 */
	public static function labelMailingBounceID(MailingBounce $obj)
	{
		return 'MailingBounceID';
	}
	/**
	 * @brief Geef de waarde van het veld mailingBounceID.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailingBounceID van het
	 * object obj representeert.
	 */
	public static function waardeMailingBounceID(MailingBounce $obj)
	{
		return static::defaultWaardeInt($obj, 'MailingBounceID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailingBounceID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailingBounceID representeert.
	 */
	public static function opmerkingMailingBounceID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mailing.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld mailing labelt.
	 */
	public static function labelMailing(MailingBounce $obj)
	{
		return 'Mailing';
	}
	/**
	 * @brief Geef de waarde van het veld mailing.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailing van het object obj
	 * representeert.
	 */
	public static function waardeMailing(MailingBounce $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMailing())
			return NULL;
		return MailingView::defaultWaardeMailing($obj->getMailing());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld mailing.
	 *
	 * @see genericFormmailing
	 *
	 * @param MailingBounce $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailing staat en kan
	 * worden bewerkt. Indien mailing read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMailing(MailingBounce $obj, $include_id = false)
	{
		return static::waardeMailing($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld mailing. In
	 * tegenstelling tot formmailing moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmailing
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailing staat en kan
	 * worden bewerkt. Indien mailing read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMailing($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld mailing
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailing representeert.
	 */
	public static function opmerkingMailing()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contact.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld contact labelt.
	 */
	public static function labelContact(MailingBounce $obj)
	{
		return 'Contact';
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contact van het object obj
	 * representeert.
	 */
	public static function waardeContact(MailingBounce $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContact())
			return NULL;
		return ContactView::defaultWaardeContact($obj->getContact());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contact.
	 *
	 * @see genericFormcontact
	 *
	 * @param MailingBounce $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formContact(MailingBounce $obj, $include_id = false)
	{
		return static::waardeContact($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contact. In
	 * tegenstelling tot formcontact moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formcontact
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormContact($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld contact
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contact representeert.
	 */
	public static function opmerkingContact()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bounceContent.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld bounceContent labelt.
	 */
	public static function labelBounceContent(MailingBounce $obj)
	{
		return 'BounceContent';
	}
	/**
	 * @brief Geef de waarde van het veld bounceContent.
	 *
	 * @param MailingBounce $obj Het MailingBounce-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bounceContent van het object
	 * obj representeert.
	 */
	public static function waardeBounceContent(MailingBounce $obj)
	{
		return static::defaultWaardeText($obj, 'BounceContent');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bounceContent.
	 *
	 * @see genericFormbounceContent
	 *
	 * @param MailingBounce $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bounceContent staat en kan
	 * worden bewerkt. Indien bounceContent read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBounceContent(MailingBounce $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'BounceContent', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bounceContent. In
	 * tegenstelling tot formbounceContent moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbounceContent
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bounceContent staat en kan
	 * worden bewerkt. Indien bounceContent read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBounceContent($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'BounceContent', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bounceContent bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bounceContent representeert.
	 */
	public static function opmerkingBounceContent()
	{
		return NULL;
	}
}
