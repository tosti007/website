<?
abstract class MailingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingView.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailing(Mailing $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mailingID.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailingID labelt.
	 */
	public static function labelMailingID(Mailing $obj)
	{
		return 'MailingID';
	}
	/**
	 * @brief Geef de waarde van het veld mailingID.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailingID van het object obj
	 * representeert.
	 */
	public static function waardeMailingID(Mailing $obj)
	{
		return static::defaultWaardeInt($obj, 'MailingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailingID representeert.
	 */
	public static function opmerkingMailingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(Mailing $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(Mailing $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(Mailing $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Mailing $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Mailing $obj)
	{
		return static::defaultWaardeString($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Mailing $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld momentIngepland.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld momentIngepland labelt.
	 */
	public static function labelMomentIngepland(Mailing $obj)
	{
		return 'MomentIngepland';
	}
	/**
	 * @brief Geef de waarde van het veld momentIngepland.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld momentIngepland van het
	 * object obj representeert.
	 */
	public static function waardeMomentIngepland(Mailing $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MomentIngepland');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld momentIngepland.
	 *
	 * @see genericFormmomentIngepland
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentIngepland staat en
	 * kan worden bewerkt. Indien momentIngepland read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMomentIngepland(Mailing $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MomentIngepland', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld momentIngepland. In
	 * tegenstelling tot formmomentIngepland moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formmomentIngepland
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentIngepland staat en
	 * kan worden bewerkt. Indien momentIngepland read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormMomentIngepland($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MomentIngepland');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * momentIngepland bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld momentIngepland representeert.
	 */
	public static function opmerkingMomentIngepland()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld momentVerwerkt.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld momentVerwerkt labelt.
	 */
	public static function labelMomentVerwerkt(Mailing $obj)
	{
		return 'MomentVerwerkt';
	}
	/**
	 * @brief Geef de waarde van het veld momentVerwerkt.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld momentVerwerkt van het object
	 * obj representeert.
	 */
	public static function waardeMomentVerwerkt(Mailing $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MomentVerwerkt');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld momentVerwerkt.
	 *
	 * @see genericFormmomentVerwerkt
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentVerwerkt staat en
	 * kan worden bewerkt. Indien momentVerwerkt read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMomentVerwerkt(Mailing $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MomentVerwerkt', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld momentVerwerkt. In
	 * tegenstelling tot formmomentVerwerkt moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmomentVerwerkt
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentVerwerkt staat en
	 * kan worden bewerkt. Indien momentVerwerkt read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMomentVerwerkt($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MomentVerwerkt');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * momentVerwerkt bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld momentVerwerkt representeert.
	 */
	public static function opmerkingMomentVerwerkt()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mailHeaders.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailHeaders labelt.
	 */
	public static function labelMailHeaders(Mailing $obj)
	{
		return 'MailHeaders';
	}
	/**
	 * @brief Geef de waarde van het veld mailHeaders.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailHeaders van het object
	 * obj representeert.
	 */
	public static function waardeMailHeaders(Mailing $obj)
	{
		return static::defaultWaardeText($obj, 'MailHeaders');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld mailHeaders.
	 *
	 * @see genericFormmailHeaders
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailHeaders staat en kan
	 * worden bewerkt. Indien mailHeaders read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMailHeaders(Mailing $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'MailHeaders', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld mailHeaders. In
	 * tegenstelling tot formmailHeaders moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmailHeaders
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailHeaders staat en kan
	 * worden bewerkt. Indien mailHeaders read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMailHeaders($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'MailHeaders', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailHeaders bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailHeaders representeert.
	 */
	public static function opmerkingMailHeaders()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mailSubject.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailSubject labelt.
	 */
	public static function labelMailSubject(Mailing $obj)
	{
		return 'MailSubject';
	}
	/**
	 * @brief Geef de waarde van het veld mailSubject.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailSubject van het object
	 * obj representeert.
	 */
	public static function waardeMailSubject(Mailing $obj)
	{
		return static::defaultWaardeString($obj, 'MailSubject');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld mailSubject.
	 *
	 * @see genericFormmailSubject
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailSubject staat en kan
	 * worden bewerkt. Indien mailSubject read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMailSubject(Mailing $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'MailSubject', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld mailSubject. In
	 * tegenstelling tot formmailSubject moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmailSubject
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailSubject staat en kan
	 * worden bewerkt. Indien mailSubject read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMailSubject($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'MailSubject', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailSubject bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailSubject representeert.
	 */
	public static function opmerkingMailSubject()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mailFrom.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailFrom labelt.
	 */
	public static function labelMailFrom(Mailing $obj)
	{
		return 'MailFrom';
	}
	/**
	 * @brief Geef de waarde van het veld mailFrom.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailFrom van het object obj
	 * representeert.
	 */
	public static function waardeMailFrom(Mailing $obj)
	{
		return static::defaultWaardeString($obj, 'MailFrom');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld mailFrom.
	 *
	 * @see genericFormmailFrom
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailFrom staat en kan
	 * worden bewerkt. Indien mailFrom read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMailFrom(Mailing $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'MailFrom', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld mailFrom. In
	 * tegenstelling tot formmailFrom moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmailFrom
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld mailFrom staat en kan
	 * worden bewerkt. Indien mailFrom read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMailFrom($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'MailFrom', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailFrom bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailFrom representeert.
	 */
	public static function opmerkingMailFrom()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld isDefinitief.
	 *
	 * @param Mailing $obj Het Mailing-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld isDefinitief labelt.
	 */
	public static function labelIsDefinitief(Mailing $obj)
	{
		return 'IsDefinitief';
	}
	/**
	 * @brief Geef de waarde van het veld isDefinitief.
	 *
	 * @param Mailing $obj Het Mailing-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld isDefinitief van het object
	 * obj representeert.
	 */
	public static function waardeIsDefinitief(Mailing $obj)
	{
		return static::defaultWaardeBool($obj, 'IsDefinitief');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld isDefinitief.
	 *
	 * @see genericFormisDefinitief
	 *
	 * @param Mailing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld isDefinitief staat en kan
	 * worden bewerkt. Indien isDefinitief read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formIsDefinitief(Mailing $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'IsDefinitief', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld isDefinitief. In
	 * tegenstelling tot formisDefinitief moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formisDefinitief
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld isDefinitief staat en kan
	 * worden bewerkt. Indien isDefinitief read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormIsDefinitief($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'IsDefinitief');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * isDefinitief bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld isDefinitief representeert.
	 */
	public static function opmerkingIsDefinitief()
	{
		return NULL;
	}
}
