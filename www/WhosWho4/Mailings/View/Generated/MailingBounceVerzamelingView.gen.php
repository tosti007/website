<?
abstract class MailingBounceVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MailingBounceVerzamelingView.
	 *
	 * @param MailingBounceVerzameling $obj Het MailingBounceVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMailingBounceVerzameling(MailingBounceVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
