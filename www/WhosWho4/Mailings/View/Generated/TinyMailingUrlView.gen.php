<?
abstract class TinyMailingUrlView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TinyMailingUrlView.
	 *
	 * @param TinyMailingUrl $obj Het TinyMailingUrl-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTinyMailingUrl(TinyMailingUrl $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld mailing.
	 *
	 * @param TinyMailingUrl $obj Het TinyMailingUrl-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld mailing labelt.
	 */
	public static function labelMailing(TinyMailingUrl $obj)
	{
		return 'Mailing';
	}
	/**
	 * @brief Geef de waarde van het veld mailing.
	 *
	 * @param TinyMailingUrl $obj Het TinyMailingUrl-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailing van het object obj
	 * representeert.
	 */
	public static function waardeMailing(TinyMailingUrl $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMailing())
			return NULL;
		return MailingView::defaultWaardeMailing($obj->getMailing());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld mailing
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailing representeert.
	 */
	public static function opmerkingMailing()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tinyUrl.
	 *
	 * @param TinyMailingUrl $obj Het TinyMailingUrl-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld tinyUrl labelt.
	 */
	public static function labelTinyUrl(TinyMailingUrl $obj)
	{
		return 'TinyUrl';
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl.
	 *
	 * @param TinyMailingUrl $obj Het TinyMailingUrl-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tinyUrl van het object obj
	 * representeert.
	 */
	public static function waardeTinyUrl(TinyMailingUrl $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTinyUrl())
			return NULL;
		return TinyUrlView::defaultWaardeTinyUrl($obj->getTinyUrl());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld tinyUrl
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tinyUrl representeert.
	 */
	public static function opmerkingTinyUrl()
	{
		return NULL;
	}
}
