<?php

global $vertaal, $HTMLTemplate, $HTMLItemsTemplate, $plainTemplate, $plainItemsTemplate;

/////////Hier komt de opmaak!

$HTMLTemplate = "
<div style=\"max-width: 600pt; margin-left: auto; margin-right: auto; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.42857143; color: #333; text-align: justify;\">
	<div style='border-radius: 0px; background-image: -webkit-linear-gradient(top,#3c3c3c 0,#222 100%); background-image: -o-linear-gradient(top,#3c3c3c 0,#222 100%); background-image: -webkit-gradient(linear,left top,left bottom,from(#3c3c3c),to(#222)); background-image: linear-gradient(to bottom,#3c3c3c 0,#222 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#ff3c3c3c\", endColorstr=\"#ff222222\", GradientType=0); filter: progid:DXImageTransform.Microsoft.gradient(enabled=false); background-repeat: repeat-x; background-color: #222; border-color: #080808; position: relative; min-height: 50px; margin-bottom: 20px; border: 1px solid transparent; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;'>
		<div style='padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto; max-width: 1170px;'>
			<div style='margin-right: 0; margin-left: 0; float: left; max-height: 52px;'>
				<span><a id='aeslogo' href='" . HTTPS_ROOT . "' style='float: left; height: 50px; padding: 5px 15px; font-size: 18px; line-height: 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;'>
					<img src='cid:aesimage' style='width: 65px; height: auto; margin-top: -10px; vertical-align: middle; border: 0;'></a></span>
				<div style='color: white; margin-left: -15px; text-shadow: 0 -1px 0 rgba(0,0,0,.25); float: left; max-height: 50px; padding: 15px 15px; font-size: 18px; line-height: 20px; word-wrap: break-word; text-decoration: none; background: 0 0;'>
						--vertaal:titelHTML-- 
				</div>
			</div>
			<div style='margin-right: 0; margin-left: 0; width: auto; border-top: 0; -webkit-box-shadow: none; box-shadow: none; padding-right: 15px; padding-left: 15px; overflow-x: visible; -webkit-overflow-scrolling: touch; border-top: 1px solid transparent; -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1); box-shadow: inset 0 1px 0 rgba(255,255,255,.1);'>
				<ul style='float: right; margin: 0; padding-left: 0; list-style: none; display: block;'>
					<li style='float: left; position: relative; display: block;'><a style='line-height: 20px; position: relative; display: block; padding: 15px 15px;' href='https://www.facebook.com/aeskwadraat'><img src='cid:facebookimage' alt='Facebook' style='height: 18px; max-width: 18px; vertical-align: middle; border: 0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.25); line-height: 20px;'></a></li>
					<li style='float: left; position: relative; display: block;'><a style='line-height: 20px; position: relative; display: block; padding: 15px 15px;' href='https://twitter.com/aeskwadraat'><img src='cid:twitterimage' alt='Twitter' style='height: 18px; max-width: 18px; vertical-align: middle; border: 0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.25); line-height: 20px;'></a></li>
					<li style='float: left; position: relative; display: block;'><a style='line-height: 20px; position: relative; display: block; padding: 15px 15px;' href='https://www.linkedin.com/groups?gid=55143'><img src='cid:linkedinimage' alt='LinkedIn' style='height: 18px; max-width: 18px; vertical-align: middle; border: 0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.25); line-height: 20px;'></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div style=\"max-width: 550pt; margin-left: auto; margin-right: auto; padding-right: 10px; padding-left: 10px;\">
		<p>--actmail:Geleuter--</p>
		<p style='padding-top: 10px;'><b>--actmail:LedenLoterijWinnaar--</b></p>
		<h2 style='margin: 10px 0 20px; border-bottom: 1px solid red; overflow-y: hidden; padding-bottom: 9px; font-size: 30px;font-family: inherit; font-weight: 500; color: inherit;'>
			<a href='" . HTTPS_ROOT . "/Activiteiten/Fotos' style='color: red; text-decoration: none;'>--vertaal:fotomaand-- ↪</a>
		</h2>
		--actmail:fotos--
		<h2 style='margin: 10px 0 20px; border-bottom: 1px solid red; overflow-y: hidden; padding-bottom: 9px; font-size: 30px;font-family: inherit; font-weight: 500; color: inherit;'>
			<a href='" . HTTPS_ROOT . "/Activiteiten' style='color: red; text-decoration: none;'>--vertaal:agenda-- ↪</a>
		</h2>
		--actmail:agenda--
		<i>--vertaal:agendaMeerInfoHTML--</i>
		--actmail:Items--
		<p style='padding-top: 15px; font-size:10pt;color:gray'>--vertaal:disclaimer--</p>
	</div>
	<div style='border-radius: 0px; margin-bottom: 0px; background-image: -webkit-linear-gradient(top,#3c3c3c 0,#222 100%); background-image: -o-linear-gradient(top,#3c3c3c 0,#222 100%); background-image: -webkit-gradient(linear,left top,left bottom,from(#3c3c3c),to(#222)); background-image: linear-gradient(to bottom,#3c3c3c 0,#222 100%); background-repeat: repeat-x; background-color: #222; border-color: #080808; position: relative; min-height: 50px; display: block;'>
		<div style='padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto; max-width: 1170px;'>
			<div style='margin-right: 0; margin-left: 0; float: left;'>
				<div style='color: white; margin-left: -15px; text-shadow: 0 -1px 0 rgba(0,0,0,.25); float: left; height: 50px; padding: 15px 15px; font-size: 18px; line-height: 20px; word-wrap: break-word; text-decoration: none; background: 0 0;'><a href='" . HTTPS_ROOT . "' style='color:white; text-decoration: none;'>--vertaal:vereniging--</div></a>
			</div>
			<div style='margin-right: 0; margin-left: 0; width: auto; border-top: 0; -webkit-box-shadow: none; box-shadow: none; padding-right: 15px; padding-left: 15px; overflow-x: visible; -webkit-overflow-scrolling: touch; border-top: 1px solid transparent; -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1); box-shadow: inset 0 1px 0 rgba(255,255,255,.1);'>
				<ul style='float: right; margin: 0; padding-left: 0; list-style: none; display: block;'>
					<li style='float: left; position: relative; display: block;'><a style='line-height: 20px; position: relative; display: block; padding: 15px 15px;' href='https://www.facebook.com/aeskwadraat'><img src='cid:facebookimage' alt='Facebook' style='height: 18px; max-width: 18px; vertical-align: middle; border: 0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.25); line-height: 20px;'></a></li>
					<li style='float: left; position: relative; display: block;'><a style='line-height: 20px; position: relative; display: block; padding: 15px 15px;' href='https://twitter.com/aeskwadraat'><img src='cid:twitterimage' alt='Twitter' style='height: 18px; max-width: 18px; vertical-align: middle; border: 0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.25); line-height: 20px;'></a></li>
					<li style='float: left; position: relative; display: block;'><a style='line-height: 20px; position: relative; display: block; padding: 15px 15px;' href='https://www.linkedin.com/groups?gid=55143'><img src='cid:linkedinimage' alt='LinkedIn' style='height: 18px; max-width: 18px; vertical-align: middle; border: 0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; color: #fff; text-shadow: 0 -1px 0 rgba(0,0,0,.25); line-height: 20px;'></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
";

$HTMLItemsTemplate = "
<a href='" . HTTPS_ROOT . "--actItem:Link--' style='color: red; text-decoration: none;'>
	<h2 style='margin: 10px 0 20px; border-bottom: 1px solid red; overflow-y: hidden; padding-bottom: 9px; font-size: 30px; font-weight: 500; color: inherit; padding-top: 15px;'>
		--actItem:Titel--
	</h2>
</a>
<div style='padding-left: 5px; padding-right: 5px;'>
<b>--actItem:WieWatWaar--</b>
<p>--actItem:Omschrijving--</p>
</div>\n";

$plainTemplate = "
--actmail:Geleuter--

--actmail:LedenLoterijWinnaar--

************************ --vertaal:agenda-- *********************************

--actmail:agenda--

--vertaal:agendaMeerInfoPlain--

          **************************************

--actmail:Items----vertaal:disclaimerPlain--
";

$plainItemsTemplate = "
> --actItem:Titel-- <
--actItem:WieWatWaar--

--actItem:Omschrijving--

          **************************************

";

///////////Hier komen de vertalingen!

$vertaal = array(
	'titelHTML' => array('nl' => 'Activiteiten A&ndash;Eskwadraat', 'en' => 'Activities A&ndash;Eskwadraat'),
	'agenda' => array('nl' => 'Agenda', 'en' => 'Calendar'),
	'agendaMeerInfoHTML' => array(
		'nl' => "<p>* meer informatie vind je hieronder, de rest staat op <a href='" . HTTPS_ROOT . "/Activiteiten'>www.A-Eskwadraat.nl/activiteiten</a></p>",
		'en' => "<p>* more information below<br />All activities can be found at <a href='" . HTTPS_ROOT . "/Activiteiten?setlanguage=en'>" . HTTPS_ROOT . "/Activiteiten?setlanguage=en</a></p>"
	),
	'agendaMeerInfoPlain' => array(
		'nl' => "* meer informatie vind je hieronder\nAlle activiteiten staan op " . HTTPS_ROOT . "/Activiteiten",
		'en' => "* more information below\nAll activities can be found at " . HTTPS_ROOT . "/Activiteiten?setlanguage=en"
	),
	'fotomaand' => array(
		'nl' => "Foto's van deze maand",
		'en' => "This month's pictures",
	),
	'disclaimer' => array(
		'nl' => 'Je ontvangt deze e-mail omdat je aangemeld bent voor de activiteiten-mailinglijst. Van deze lijst af? Ga naar <span style="white-space: nowrap;"><a href="' . HTTPS_ROOT . '/contribucie">www.A-Eskwadraat.nl/contribucie</a></span> en volg de instructies of stuur een mailtje naar activiteiten@a-eskwadraat.nl',
		'en' => 'You have recieved this e-mail because you signed up for the activities mailing list. Don\'t want to receive these mails? visit <span style="white-space: nowrap;"><a href="' . HTTPS_ROOT . '/contribucie">www.A-Eskwadraat.nl/contribucie</a></span> and follow the instructions to remove yourself from this mailing list. You can also send an e-mail to activiteiten@a-eskwadraat.nl'),
	'disclaimerPlain' => array(
		'nl' => 'Je ontvangt deze email omdat je aangemeld bent voor de activiteiten-mailinglijst. Van deze lijst af? Ga naar ' . HTTPS_ROOT . '/contribucie en volg de instructies of stuur een mailtje naar activiteiten@a-eskwadraat.nl',
		'en' => 'You recieve this email because you signed up for the activities mailing list. Don\'t want to receive these mails? Visit ' . HTTPS_ROOT . '/contribucie and follow the instructions to remove yourself from this mailing list. You can also send an e-mail to activiteiten@a-eskwadraat.nl'
	),
	'vereniging' => array(
		'nl' => 'Studievereniging A&ndash;Eskwadraat',
		'en' => 'Study Association A&ndash;Eskwadraat'
	),
);


abstract class Actmail_Controller
{

	static public function start()
	{
		$body = new HtmlDiv();
		$body->add(new HtmlHeader(2, "Activiteitmaakmolen: Genereer"));

		//Datum offset formulier
		$nextMonday = date("Y-m-d", strtotime("next monday"));
		$startDatum = tryPar('startDatum', $nextMonday);
		if (!preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $startDatum))
		{
			$body->add(new HtmlSpan("Let op: " . $startDatum . " is geen geldig datumformaat (YYYY-MM-DD)!", 'text-danger strongtext'));
			$startDatum = $nextMonday;
		}

		$body->add($form = new HtmlForm('get'));
		$form->addClass('form-inline');
		$form->add(new HtmlDiv(array(
			new HtmlSpan("Laat activiteiten zien vanaf "),
			HtmlInput::makeText('startDatum', $startDatum, 10),
			new HtmlSpan("(YYYY-MM-DD)"),
			HtmlInput::makeSubmitButton("Vernieuw")
		)));

		//Maak activiteitentabel
		$lijst = ActiviteitVerzameling::getActiviteiten(strtotime($startDatum));
		$activiteitTabel = ActiviteitVerzamelingView::lijstAgendaActMail($lijst);

		$body->add($form2 = new HtmlForm('post', 'reviseer'));
		$form2->add(HtmlInput::makeHidden('startDatum', $startDatum));
		$form2->add(HtmlInput::makeHidden('gegenereerd', 1));
		$form2->add($activiteitTabel);
		$form2->add(new HtmlDiv(HtmlInput::makeSubmitButton('Maak preview!')));

		$page = Page::getInstance()
			->start("ActiviteitenMailingMaakMolen", 'actmail')
			->add($body)
			->end();
	}

	static public function reviseer()
	{
		global $session, $request;

		if (!tryPar('gegenereerd') && !$session->get('actmailnl'))
		{
			Page::getInstance()
				->start("ActiviteitenMailingMaakMolen", 'actmail')
				->add(new HtmlParagraph("Vul eerst het formulier in!", 'text-danger strongtext'))
				->add(new HtmlAnchor('.', "Gaan met die banaan"))
				->end();
		}

		$body = new HtmlDiv();
		$body->setAttribute('style', 'width:1100px');
		$body->add(new HtmlHeader(2, "Activiteitmaakmolen: Reviseer"))
			->add(new HtmlParagraph("Met pseudocode! Gebruik de volgende syntax:\n<ul>\n<li>De secties <ol><li>--actmail:Geleuter--</li><li>--actmail:LedenLoterijWinnaar--</li><li>--actmail:Agenda--</li><li>--actmail:Activiteit--</li></ol></li>\n<li>Agendaitem: *? datum|titel</li>\n<li>&gt; ActiviteitTitel &lt;</li>\n<li>&gt;&gt; WieWatWaar &lt;&lt;</li>\n<li>&gt;&gt;&gt;Link&lt;&lt;&lt;</li></ul>"));

		//Indien net gegenereerd, parse
		if (tryPar('gegenereerd'))
		{
			$actTitelNL = ActiviteitVerzameling::verzamel(array_filter($request->request->get('actTitelNL')));
			$actTitelEN = ActiviteitVerzameling::verzamel(array_filter($request->request->get('actTitelEN')));
			$actTekstNL = array_filter($request->request->get('actTekstNL'));
			$actTekstEN = array_filter($request->request->get('actTekstEN'));
			$session->set('actmailnl', ActiviteitVerzamelingView::pseudoActMail($actTitelNL, $actTekstNL, 'nl'));
			$session->set('actmailen', ActiviteitVerzamelingView::pseudoActMail($actTitelEN, $actTekstEN, 'en'));
		}

		//Laat de gegenereerde mailing zien voor evt aanpassingen
		$form = new HtmlForm('post', 'preview', true);
		$body->add($form);
		$form->add(new HtmlHeader(3, "Foto's die er in moeten komen (max 5 in totaal!)"));
		$form->add(new HtmlParagraph("a-eskwadraat.nl/Activiteiten/ActiviteitTitel/*actid*/ActNaam/Fotos#*tsjak*&photoid=*fotoid**overigetsjak*"));
		$areaFoto = new HtmlTextarea('fotolinks');
		if (!$session->has('fotolinks'))
		{
			$session->set('fotolinks', 'feed me linkjes!');
		}
		$form->add($areaFoto->setAttribute('cols', 100)->setAttribute('rows', 5)->add($session->get('fotolinks')));

		$divNL = new HtmlDiv();
		$divEN = new HtmlDiv();

		$areaNL = new HtmlTextarea('verhaal-nl');
		$areaNL->setAttribute('cols', 72)->setAttribute('rows', 70)->setAttribute('style', 'float:left')->add($session->get('actmailnl'));
		$divNL->setAttribute('style', 'float:left')->add(new HtmlHeader(3, "Nederlands"), $areaNL, new HtmlBreak(), HtmlInput::makeSubmitButton('Test NL', 'TestNL'));
		$areaEN = new HtmlTextarea('verhaal-en');
		$areaEN->setAttribute('cols', 72)->setAttribute('rows', 70)->setAttribute('style', 'float:left')->add($session->get('actmailen'));
		$divEN->setAttribute('style', 'float:left')->add(new HtmlHeader(3, "Engels"), $areaEN, new HtmlBreak(), HtmlInput::makeSubmitButton('Test EN', 'TestEN'));

		$form->add($divNL, $divEN);

		$page = Page::getInstance()
			->start()
			->add($body);

		Page::getInstance()->end();
	}

	static public function preview()
	{
		global $session;

		$page = Page::getInstance()->start("ActiviteitenMailingMaakMolen: Preview");

		//Alles opslaan want we gebruiken het later weer
		$session->set('actmailnl', tryPar('verhaal-nl'));
		$session->set('actmailen', tryPar('verhaal-en'));
		$session->set('fotolinks', tryPar('fotolinks'));

		//welk knopje is gedrukt?
		if (tryPar('TestEN', false) === false)
		{
			$taal = 'nl';
			$mail = $session->get('actmailnl');
		}
		else
		{
			$taal = 'en';
			$mail = $session->get('actmailen');
		}

		$body = new HtmlDiv();

		//Parse de opgegeven fotolinks
		$fotoLijst = self::parseFotoLinks($session->get('fotolinks'));

		//Geen fouten:
		if ($fotoLijst !== false)
		{
			$resultaat = self::genereerMail($mail, $taal, $fotoLijst, true);

			//Laat de preview zien in de browser
			$body->add($prev = new HtmlDiv());
			$prev->add($resultaat['HTML'])
				->setAttribute('style', 'border:solid 1px black');
			$body->add(new HtmlBreak())
				->add($prevPlain = new HtmlTextarea('waddupbitches'));
			$prevPlain->setAttribute('readonly', null)
				->setAttribute('cols', 72)
				->setAttribute('rows', 20)
				->add($resultaat['Plain']);

			$body->add($form = new HtmlForm('post', 'verstuur'));
			$form->add(new HtmlDiv(HtmlInput::makeSubmitButton('Stuur naar mailingweb')))
				->add(HtmlInput::makeHidden('taal', $taal));
			$body->add(new HtmlDiv(new HtmlAnchor('reviseer', "Ga terug!")));
		}

		Page::getInstance()->add($body);
		Page::getInstance()->end();
	}

	public static function verstuur()
	{
		global $session;

		Page::getInstance()->start()->add(new HtmlHeader(1, "ActiviteitenMailingMaakMolen: Verstuur"));

		//Stuur de mailing door naar mailingweb

		//Is het vandaag zondag?
		if (date('N') == 7 && date('G') < 20)
		{
			$komendezondag = date("Y-m-d");
		}
		else
		{
			$komendezondag = date("Y-m-d", strtotime("next sunday"));
		}
		$verzendmoment = $komendezondag . " 20:00:00";

		//Zet de taal!
		$taal = requirePar('taal');
		if ($taal == 'nl')
		{
			$mailinglistid = 7;
		}
		if ($taal == 'en')
		{
			$mailinglistid = 8;
		}

		$fotoLijst = self::parseFotoLinks($session->get('fotolinks'));

		$HTMLenPlain = self::genereerMail($session->get("actmail$taal"), $taal, $fotoLijst);
		$hash = md5(date('r', time()));
		$multipartMail = self::maakMultipart($hash, $HTMLenPlain['Plain'], $HTMLenPlain['HTML']);

		sendmail(
			'Activiteiten A-Eskwadraat <activiteiten@a-eskwadraat.nl>', //From			
			'activiteiten-mailings@a-eskwadraat.nl', //To 
			"Activiteitenmail", //Subject
			$multipartMail,
			false, //Reply-to (zelfde als From)
			"Content-Type: multipart/related; boundary=\"RELATED-$hash\"; type=\"multipart/alternative\"\n"
		);
		/*$mailing = new Mailing();
		$mailing->setOmschrijving('Activiteitenmail');
		$mailing->setMailSubject('Activiteitenmail');
		$mailing->setMailFrom('A-Eskwadraat <actvitieiten@a-eskwadraat.nl>');
		$mailing->setCommissie(603);
		$mailing->opslaan();
		$mailing->parseExternalMailInput("Content-Type: multipart/related; boundary=\"RELATED-$hash\"; type=\"multipart/alternative\"\nX-MailingWeb-Schedule: $verzendmoment\nX-MailingWeb-MailingListID: $mailinglistid\n" . $multipartMail, false);*/

		Page::getInstance()->add(new HtmlParagraph("De Activiteitenmail $taal is gestuurd! Veel plezier ermee!"))
			->add(new HtmlAnchor('reviseer', "Ga terug"))
			->add(new HtmlPre(htmlspecialchars($multipartMail)))
			->end();
	}

	//Knip de opgegeven foto's in de relevante stukken voor in de actmail
	static private function parseFotoLinks($fotoString)
	{
		$fotos = explode("\n", $fotoString);
		$fotoLijst = Array();
		for ($i = 0; $i < sizeof($fotos); $i++)
		{
			if (!isset($fotos[$i]))
			{
				Page::getInstance()->add(new HtmlParagraph("Slechts $i van de 5 fotolinks opgegeven", 'text-danger strongtext'))
					->add(new HtmlAnchor('reviseer', "Ga terug"))
					->add(new HtmlSpan("(Niet op vorige drukken want dan ben je je tekst kwijt)"));
				return false;
			}
			elseif (!preg_match("|/Activiteiten/([a-zA-Z0-9_]+)/([0-9]+)/([a-z0-9.!@$^&*()_=,-]+)/Fotos/?\#.*&photoId=([0-9]+).*|i", $fotos[$i], $m))
			{
				Page::getInstance()->add(new HtmlParagraph("'{$fotos[$i]}' voldoet niet aan de eisen voor een fotolink!", 'text-danger strongtext'))
					->add(new HtmlAnchor('reviseer', "Ga terug"))
					->add(new HtmlSpan("(Niet op vorige drukken want dan ben je je tekst kwijt)"));
				return false;
			}
			else
			{
				$fotoLijst[] = Array('cie' => $m[1], 'actid' => $m[2], 'actnaam' => $m[3], 'foto' => $m[4]);
			}
		}
		return $fotoLijst;
	}

	//Maak de echte activiteitenmail (html en plain) van het bewerkformpje
	static private function genereerMail($tekst, $taal, $fotoLijst, $preview = false)
	{
		global $vertaal, $HTMLTemplate, $HTMLItemsTemplate, $plainTemplate, $plainItemsTemplate;

		//Parse de pseudocodemail -> breek hem in de relevante stukjes
		$split = preg_split('/\s*--actmail:([^-]*)--\s*/i', $tekst, NULL, PREG_SPLIT_DELIM_CAPTURE);
		$data = Array('Activiteit' => Array());
		for ($i = 0; $i < count($split); $i++)
		{
			if (empty($split[$i]))
			{
				continue;
			}
			if (!in_array($split[$i], Array('Geleuter', 'LedenLoterijWinnaar', 'Agenda', 'Activiteit')))
			{
				Page::getInstance()->add(new HtmlParagraph("Let op! {$split[$i]} geen geldige delimiter", 'text-danger strongtext'));
			}
			if ($split[$i] != 'Activiteit')
			{
				$data[$split[$i]] = $split[++$i];
			}
			else
			{
				$data['Activiteit'][] = $split[++$i];
			}
		}

		//Bouw de agenda op
		$agendaPlain = str_replace(array('|'), array("\t"), $data['Agenda']);
		$agendaItems = explode("\n", $data['Agenda']);
		$agendaTable = "<table style='width: 100%; max-width: 100%; margin-bottom: 20px; background-color: transparent; border-spacing: 0; border-collapse: collapse; display: table;'>\n";
		$i = 0;
		foreach ($agendaItems as $item)
		{
			if (!preg_match('/^(\*?)\s*([^\|]*)\|\s*(.*)$/', $item, $match))
			{
				continue;
			}
			$agendaTable .= "<tr>\n";
			if ($i % 2 == 0)
			{
				$td = "<td style='background-color: #f9f9f9; padding: 8px; line-height: 1.42857143; vertical-align: top; border-top: 1px solid #ddd;'>\n";
			}
			else
			{
				$td = "<td style='padding: 8px; line-height: 1.42857143; vertical-align: top; border-top: 1px solid #ddd;'>\n";
			}
			$agendaTable .= $td . "{$match[1]}\n</td>\n" . $td . "{$match[2]}\n</td>\n" . $td . "{$match[3]}\n</td>\n";
			$agendaTable .= "</tr>\n";
			$i++;
		}
		$agendaTable .= "</table>\n";
		$agendaHTML = $agendaTable;

		//Bouw de activiteiten op
		$activiteitHTML = '';
		$activiteitPlain = '';
		for ($i = 0; $i < count($data['Activiteit']); $i++)
		{
			// TODO SOEPMES wat is deze regex aan het doen
			if (!preg_match('/\s*>([^<]*)<\s*>>\s*([^<]*)<<\s*>>>\s*([^<]*)<<<\s*([^$]*)/i', $data['Activiteit'][$i], $activiteitInfo))
			{
				Page::getInstance()->add(new HtmlParagraph(new HtmlSpan("Let op! ", 'text-danger strongtext') . $data['Activiteit'][$i] . new HtmlSpan(" geen geldige activiteit", 'text-danger strongtext')));
				continue;
			}
			$activiteitHTML .= str_replace(
				Array('--actItem:Titel--', '--actItem:WieWatWaar--', '--actItem:Omschrijving--', '--actItem:Link--'),
				Array($activiteitInfo[1] . ' ↪', $activiteitInfo[2], self::prepareerTekst($activiteitInfo[4]), $activiteitInfo[3]),
				$HTMLItemsTemplate);
			$activiteitPlain .= str_replace(
				Array('--actItem:Titel--', '--actItem:WieWatWaar--', '--actItem:Omschrijving--'),
				Array($activiteitInfo[1], $activiteitInfo[2], self::HTML2Plain(self::prepareerTekst($activiteitInfo[4]))),
				$plainItemsTemplate);
		}

		$fotosHTML = "<div style='display: table; width: 100%'>\n";
		for ($i = 0; $i < count($fotoLijst); $i++)
		{
			$f = $fotoLijst[$i];
			$fotosHTML .= "<div style='width: 20%; float: left; position: relative; min-height: 1px; text-align: center;'>
	<a href='" . HTTPS_ROOT . "/Activiteiten/{$f['cie']}/{$f['actid']}/{$f['actnaam']}/Fotos#&photoId={$f['foto']}'>
		<img src='" . (($preview) ? (HTTPS_ROOT . "/FotoWeb/Media/{$f['foto']}/Thumbnail") : ("cid:foto" . $i)) . "' alt='Foto' style='border-right:3px; max-width: 100%; max-height: 90px;'>
	</a>
</div>\n";
		}
		$fotosHTML .= "</div>\n";

		$HTML = str_replace(
			Array('--vertaal:titelHTML--', '--vertaal:agenda--', '--vertaal:agendaMeerInfoHTML--', '--vertaal:fotomaand--', '--vertaal:disclaimer--', '--vertaal:vereniging--'),
			Array($vertaal['titelHTML'][$taal], $vertaal['agenda'][$taal], $vertaal['agendaMeerInfoHTML'][$taal], $vertaal['fotomaand'][$taal], $vertaal['disclaimer'][$taal], $vertaal['vereniging'][$taal]),
			$HTMLTemplate);
		$HTML = str_replace(
			Array('--actmail:Geleuter--', '--actmail:LedenLoterijWinnaar--', '--actmail:fotos--', '--actmail:agenda--', '--actmail:Items--'),
			Array(nl2br($data['Geleuter']), $data['LedenLoterijWinnaar'], $fotosHTML, $agendaHTML, $activiteitHTML),
			$HTML);
		$Plain = str_replace(
			Array('--vertaal:agenda--', '--vertaal:agendaMeerInfoPlain--', '--vertaal:disclaimerPlain--'),
			Array($vertaal['agenda'][$taal], $vertaal['agendaMeerInfoPlain'][$taal], $vertaal['disclaimerPlain'][$taal]),
			$plainTemplate);
		$Plain = str_replace(
			Array('--actmail:Geleuter--', '--actmail:LedenLoterijWinnaar--', '--actmail:agenda--', '--actmail:Items--'),
			Array($data['Geleuter'], $data['LedenLoterijWinnaar'], $agendaPlain, $activiteitPlain),
			$Plain);

		return Array('HTML' => $HTML, 'Plain' => $Plain);
	}

	static private function HTML2Plain($html)
	{
		return html_entity_decode(strip_tags($html));
	}

	static private function prepareerTekst($txt)
	{
		//Er moet geunwrapt worden, want bij alle activiteitomschrijvingen staan er newlines op willekeurige regellengtes
		//Lelijkste unwrap functie ooit, als er twee newlines zijn, vervang door #SOEPMES#
		$txt = preg_replace('/(\n\n|\r\n\r\n)/', '#SOEPMES#', $txt);
		//Haal alle newlines weg
		$txt = preg_replace('/[\r\n]+/', ' ', $txt);
		//Strip dubbele spaties
		$txt = preg_replace('/\s+/', ' ', $txt);
		//Vervang alle soepmessen door een dubbele newline
		$txt = str_replace('#SOEPMES#', "\n\n", $txt);
		//En nu maar hopen dat niemand SOEPMES in zijn activiteitomschrijving vermeldt :P

		return $txt;
	}

	static private function maakMultipart($hash, $verhaal, $htmlverhaal)
	{
		global $session;

		$logoStr = base64_encode(file_get_contents(FS_ROOT . "/Layout/Images/logo.png"));
		$facebookStr = base64_encode(file_get_contents(FS_ROOT . "/Layout/Images/f.png"));
		$twitterStr = base64_encode(file_get_contents(FS_ROOT . "/Layout/Images/t.png"));
		$linkedinStr = base64_encode(file_get_contents(FS_ROOT . "/Layout/Images/LI.png"));
		$fotoLijst = self::parseFotoLinks($session->get('fotolinks'));
		$txt = "
--RELATED-$hash
Content-Type: multipart/alternative; boundary=\"ALT-$hash\"

--ALT-$hash
Content-Type: text/plain; charset=\"utf-8\"
Content-Transfer-Encoding: 8bit

$verhaal

--ALT-$hash
Content-Type: text/html; charset=\"utf-8\"
Content-Transfer-Encoding: 8bit

$htmlverhaal

--ALT-$hash--

--RELATED-$hash
Content-Type: image/png
Content-ID: <aesimage>
Content-Transfer-Encoding: base64
Content-Disposition: inline; filename=\"aes.png\"

" . $logoStr . "

--RELATED-$hash
Content-Type: image/png
Content-ID: <facebookimage>
Content-Transfer-Encoding: base64
Content-Disposition: inline; filename=\"f.png\"

" . $facebookStr . "

--RELATED-$hash
Content-Type: image/png
Content-ID: <twitterimage>
Content-Transfer-Encoding: base64
Content-Disposition: inline; filename=\"t.png\"

" . $twitterStr . "

--RELATED-$hash
Content-Type: image/png
Content-ID: <linkedinimage>
Content-Transfer-Encoding: base64
Content-Disposition: inline; filename=\"l.png\"

" . $linkedinStr . "

";

		for ($i = 0; $i < count($fotoLijst); $i++)
		{
			$f = $fotoLijst[$i];
			$fotoObj = Media::geef($f['foto']);
			$loc = Media::geefAbsLoc('thumbnail', $fotoObj->geefID());
			$fotoCode = base64_encode(file_get_contents(FILESYSTEM_PREFIX . $loc));
			$txt .= "--RELATED-$hash
Content-Type: image/png
Content-ID: <foto$i>
Content-Transfer-Encoding: base64
Content-Disposition: inline; filename=\"foto$i.png\"

" . $fotoCode . "

";
		}

		$txt .= "--RELATED-$hash--";

		return $txt;
	}

}
