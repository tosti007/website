<?
/**
 * $Id$
 */
class TinyMailingUrlVerzameling
	extends TinyMailingUrlVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TinyMailingUrlVerzameling_Generated
	}

	public static function vanUrl(TinyUrl $turl)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `mailing_mailingID` FROM `TinyMailingUrl`
						WHERE `tinyUrl_id` = %i", $turl->geefID());

		//Maak propere ids.
		foreach ($ids as &$id)
			$id = array($id, $turl->geefID());

		return TinyMailingUrlVerzameling::verzamel($ids);
	}

	public static function vanMailing(Mailing $mailing)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `tinyUrl_id` FROM `TinyMailingUrl`
						WHERE `mailing_mailingID` = %i", $mailing->geefID());

		//Maak propere ids.
		foreach ($ids as &$id)
			$id = array($mailing->geefID(), $id);

		return TinyMailingUrlVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
