<?
abstract class MailingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de MailingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze MailingVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze MailingVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCommissieCommissieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een MailingVerzameling van Commissie.
	 *
	 * @return MailingVerzameling
	 * Een MailingVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new MailingVerzameling();

		return MailingQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
}
