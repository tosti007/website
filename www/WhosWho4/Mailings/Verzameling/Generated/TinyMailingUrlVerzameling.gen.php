<?
abstract class TinyMailingUrlVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TinyMailingUrlVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TinyMailingUrlVerzameling een MailingVerzameling.
	 *
	 * @return MailingVerzameling
	 * Een MailingVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TinyMailingUrlVerzameling.
	 */
	public function toMailingVerzameling()
	{
		if($this->aantal() == 0)
			return new MailingVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMailingMailingID()
			                      );
		}
		$this->positie = $origPositie;
		return MailingVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TinyMailingUrlVerzameling een TinyUrlVerzameling.
	 *
	 * @return TinyUrlVerzameling
	 * Een TinyUrlVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TinyMailingUrlVerzameling.
	 */
	public function toTinyUrlVerzameling()
	{
		if($this->aantal() == 0)
			return new TinyUrlVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTinyUrlId()
			                      );
		}
		$this->positie = $origPositie;
		return TinyUrlVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TinyMailingUrlVerzameling van Mailing.
	 *
	 * @return TinyMailingUrlVerzameling
	 * Een TinyMailingUrlVerzameling die elementen bevat die bij de Mailing hoort.
	 */
	static public function fromMailing($mailing)
	{
		if(!isset($mailing))
			return new TinyMailingUrlVerzameling();

		return TinyMailingUrlQuery::table()
			->whereProp('Mailing', $mailing)
			->verzamel();
	}
	/**
	 * @brief Maak een TinyMailingUrlVerzameling van TinyUrl.
	 *
	 * @return TinyMailingUrlVerzameling
	 * Een TinyMailingUrlVerzameling die elementen bevat die bij de TinyUrl hoort.
	 */
	static public function fromTinyUrl($tinyUrl)
	{
		if(!isset($tinyUrl))
			return new TinyMailingUrlVerzameling();

		return TinyMailingUrlQuery::table()
			->whereProp('TinyUrl', $tinyUrl)
			->verzamel();
	}
}
