<?
abstract class MailingBounceVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de MailingBounceVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze MailingBounceVerzameling een MailingVerzameling.
	 *
	 * @return MailingVerzameling
	 * Een MailingVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze MailingBounceVerzameling.
	 */
	public function toMailingVerzameling()
	{
		if($this->aantal() == 0)
			return new MailingVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getMailingMailingID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getMailingMailingID()
			                      );
		}
		$this->positie = $origPositie;
		return MailingVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze MailingBounceVerzameling een ContactVerzameling.
	 *
	 * @return ContactVerzameling
	 * Een ContactVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze MailingBounceVerzameling.
	 */
	public function toContactVerzameling()
	{
		if($this->aantal() == 0)
			return new ContactVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getContactContactID()
			                      );
		}
		$this->positie = $origPositie;
		return ContactVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een MailingBounceVerzameling van Mailing.
	 *
	 * @return MailingBounceVerzameling
	 * Een MailingBounceVerzameling die elementen bevat die bij de Mailing hoort.
	 */
	static public function fromMailing($mailing)
	{
		if(!isset($mailing))
			return new MailingBounceVerzameling();

		return MailingBounceQuery::table()
			->whereProp('Mailing', $mailing)
			->verzamel();
	}
	/**
	 * @brief Maak een MailingBounceVerzameling van Contact.
	 *
	 * @return MailingBounceVerzameling
	 * Een MailingBounceVerzameling die elementen bevat die bij de Contact hoort.
	 */
	static public function fromContact($contact)
	{
		if(!isset($contact))
			return new MailingBounceVerzameling();

		return MailingBounceQuery::table()
			->whereProp('Contact', $contact)
			->verzamel();
	}
}
