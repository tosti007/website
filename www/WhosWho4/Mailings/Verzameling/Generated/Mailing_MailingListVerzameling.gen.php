<?
abstract class Mailing_MailingListVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de Mailing_MailingListVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze Mailing_MailingListVerzameling een MailingVerzameling.
	 *
	 * @return MailingVerzameling
	 * Een MailingVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze Mailing_MailingListVerzameling.
	 */
	public function toMailingVerzameling()
	{
		if($this->aantal() == 0)
			return new MailingVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMailingMailingID()
			                      );
		}
		$this->positie = $origPositie;
		return MailingVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze Mailing_MailingListVerzameling een MailingListVerzameling.
	 *
	 * @return MailingListVerzameling
	 * Een MailingListVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze Mailing_MailingListVerzameling.
	 */
	public function toMailingListVerzameling()
	{
		if($this->aantal() == 0)
			return new MailingListVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMailingListMailingListID()
			                      );
		}
		$this->positie = $origPositie;
		return MailingListVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een Mailing_MailingListVerzameling van Mailing.
	 *
	 * @return Mailing
	 * Een Mailing die elementen bevat die bij de Mailing hoort.
	 */
	static public function fromMailing($mailing)
	{
		if(!isset($mailing))
			return new Mailing();

		return Mailing_MailingListQuery::table()
			->whereProp('Mailing', $mailing)
			->verzamel();
	}
	/**
	 * @brief Maak een Mailing_MailingListVerzameling van MailingList.
	 *
	 * @return Mailing
	 * Een Mailing die elementen bevat die bij de MailingList hoort.
	 */
	static public function fromMailingList($mailingList)
	{
		if(!isset($mailingList))
			return new Mailing();

		return Mailing_MailingListQuery::table()
			->whereProp('MailingList', $mailingList)
			->verzamel();
	}
}
