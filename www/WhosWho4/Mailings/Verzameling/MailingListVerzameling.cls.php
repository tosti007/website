<?

/***
 * $Id$
 * 
 * Representeert een lijst van MailingList-objecten
 */
class MailingListVerzameling
	extends MailingListVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	/**
	 * Retourneert een lijst met unieke Contact-objecten van deze
	 * mailinglijsten, geen dubbele dus.
	 */
	public function abonnees()
	{
		$contacten = new ContactVerzameling();
		foreach($this as $mailinglist)
		{
			//Dit kan efficienter, maar is minder complex omdat er maar één abonnees-functie is
			$contacten->union($mailinglist->abonnees());
		}
		return $contacten;
	}

	/**
		Retourneert alle abonnees als een recipient array met lidnr als
		arraykey, bijv.:
		array(
			4042 => sjeik@a-eskwadraat.nl,
			4624 => arnoud@a-eskwadraat.nl
		)
	**/
	public function abonneesAsRecipientArray()
	{
		$contacts = $this->abonnees();

		$ret = array();
		foreach ($contacts as $contact)
			$ret[$contact->geefID()] = $contact->getEmail();

		return $ret;
	}


	/**
		Retourneert een verzameling van MailingList-objecten die gekoppeld zijn
		aan een bepaalde mailing.
	**/
	public static function mailing(Mailing $mailing)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingList_mailingListID`'
						.' FROM `Mailing_MailingList`'
						.' WHERE `Mailing_MailingID` = %i'
						, $mailing->getMailingID()
						);
		return MailingListVerzameling::verzamel($ids);
	}

	/**
		Retourneert een verzameling van alle MailingList-objecten
	**/
	public static function alle()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingListID`'
						.' FROM `MailingList`'
						.' ORDER BY `naam`'
						);
		return MailingListVerzameling::verzamel($ids);
	}
	
	public static function beschikbaar()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingListID`'
						.' FROM `MailingList`'
						.' WHERE `verborgen`= 0'
						.' ORDER BY `naam`'
						);
		return MailingListVerzameling::verzamel($ids);
	}
	
	/**
		Geeft alle Lists die je mag zien terug, namelijk niet de testmailings als je geen god bent.
		Lelijke code he?
	**/
	public static function bereikbaar()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingListID`'
						.' FROM `MailingList`'
						.' WHERE `verborgen`= 0'
						.' AND `mailingListID`!= 13'
						.' AND `mailingListID`!= 14'
						.' ORDER BY `naam`'
						);
		return MailingListVerzameling::verzamel($ids);
	}
	
	public static function nietMeenemen()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingListID`'
						.' FROM `MailingList`'
						.' WHERE `verborgen`= 1 OR query IS NOT NULL'
						);
		return MailingListVerzameling::verzamel($ids);
	}
	
	public static function vanContact(Contact $con)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `mailingList_mailingListID`'
						. ' FROM `ContactMailingList` JOIN MailingList ON `mailingList_mailingListID`=`mailingListID`'
						. ' WHERE `contact_contactID` = %i'
						. ' AND `verborgen` = 0 AND `query` IS NULL', //Negeer alle verborgen mailinglijsten
						$con->geefID());
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
