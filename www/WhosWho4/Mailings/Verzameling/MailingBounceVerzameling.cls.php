<?

/**
 * $Id$
 */
class MailingBounceVerzameling
	extends MailingBounceVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // MailingBounceVerzameling_Generated
	}

	public static function vanContact($contact)
	{
		global $WSW4DB;

		$bounceIDs = $WSW4DB->q('COLUMN SELECT `mailingBounceID` 
							FROM `MailingBounce` 
							WHERE `contact_contactID` = %i',
							$contact->getContactID());

		$bounces = MailingBounceVerzameling::verzamel($bounceIDs);

		return $bounces;
	}

	public static function vanMailing(Mailing $mailing)
	{
		global $WSW4DB;

		$bounceIDs = $WSW4DB->q('COLUMN SELECT `mailingBounceID` 
							FROM `MailingBounce` 
							WHERE `mailing_mailingID` = %i',
							$mailing->getMailingID());

		$bounces = MailingBounceVerzameling::verzamel($bounceIDs);

		return $bounces;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
