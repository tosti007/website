<?

/**
 * @brief Wordt gegooid door een `DBObject` als die geen URL heeft.
 *
 * Denk hier bijvoorbeeld aan objecten die nog niet in de DB staan,
 * of objecten die geen wijzig/verwijder-optie hebben.
 */
class HeeftGeenURLException extends Exception { /* inherit alles */ }

/**
 *  Representeert een object uit de database
 */
abstract class DBObject
	extends DBObject_Generated
{

	/**  Cache voor alle objecten. */
	static protected $objcache = array();

	/**  FakeID voor het gebruik van een id als het object nog niet opgeslagen is
	 * FakeID's zijn handig voor als je meerdere objecten van dezelfde soort aan
	 * wilt maken met 1 formulier. Als de data nog niet goed is wil je wel dat
	 * als je het formulier opstuurt je de data die je al ingevuld hebt er nog
	 * staat
	 **/
	protected $fakeID = NULL;

	/**
	 *  Default constructor voor DBObject.
	 */
	public function __construct ()
	{
		parent::__construct();

		$this->errors = array();
	}

	/**
	 *  DBObject destructor.
	 *
	 * Wanneer een object verwijderd wordt dat al wel in de DB zit, aangepast 
	 * is en geldig is, wordt er automatisch een poging tot opslaan gedaan.
	 */
	public function __destruct ()
	{
		if($this->inDB && $this->dirty && $this->valid()) {
			user_error('Er wordt een vies object gedestruct dat voldoet aan'
					. ' alle constraints, er wordt een poging tot opslaan'
					. ' gedaan. (' . get_class($this) . ' #' . $this->geefID() . ')'
					, E_USER_WARNING);

			$this->opslaan();
		}
	}

	public function __toString ()
	{
		$id = (!is_null($this->geefID())) ? $this->geefID() : 'unitialized';
		return '(wsw4-object) ' . get_class() . ' #' . $id;
	}

	/**
	 *	@name Object caching
	 *@{
	 */
	/**
	 *	 Vist een Object uit de cache.
	 *
	 *	@param id    unieke identifier van het object
	 *	@param class klasse van het object
	 *	@return Een object uit het cache, of NULL als het gevraagde object
	 *	niet beschikbaar is.
	 */
	static protected function geefCache ($id, $class)
	{
		global $CACHING;

		if(!isset(self::$objcache[$class]))
			return NULL;

		if(is_array($id))
			$id = implode('_', $id);

		if(!isset(self::$objcache[$class][$id]))
			return NULL;

		return self::$objcache[$class][$id];
	}
	/**
	 *	 Filtert alle id's die al wel in de cache zitten eruit.
	 *
	 *	Deze functie retourneert dus alleen de ids over die \e NIET in de cache
	 *	zitten.  Hierdoor weten we voor welke objecten we in de database moeten
	 *	zoeken.
	 *
	 *	@param ids   ids van de objecten waarin we geïnteresseerd zijn
	 *	@param class klasse van de objecten
	 *	@return id's van objecten die \e NIET in het cache zitten
	 */
	static protected function nietInCache ($ids, $class)
	{
		global $CACHING;

		if(!isset(self::$objcache[$class])) {
			self::$objcache[$class] = array();
		}

		$todostmp = array();

		// filter wat we al hebben
		foreach($ids as $key => $id)
		{
			if(!is_array($id))
				$id = array($id);
			$idstr = implode('_', $id);

			if(!isset(self::$objcache[$class][$idstr])) {
				$todostmp[$key] = $id;
			}
		}

		$todos = array();

		foreach($todostmp as $key => $id)
		{
			if(!is_array($id))
				$id = array($id);
			$idstr = implode('_', $id);
			$todos[$key] = $id;
		}

		return $todos;
	}

	/**
	 *	 Stop een object in het cache.
	 *
	 *	@param id    het id van het object
	 *	@param obj   het object dat in cache geplaatst moet worden
	 *	@param class de class van het object dat in cache geplaatst moet worden
	 *	@param key   indien het een eigenschap is, de bijbehorende key
	 *
	 *	\note De reden voor de class parameter is zodat we bv een Lid object
	 *	ook onder Persoon en Contact in de cache kunnen stoppen.
	 */
	static protected function stopInCache ($id, $obj, $class, $key = NULL)
	{
		if(is_array($id))
			$id = implode('_', $id);

		if(!is_a($obj, $class))
			$obj = False;

		if(!$key) {
			self::$objcache[$class][$id] = $obj;
			return;
		}
		self::$objcache[$class][$id][$key] = $obj;
	}

	static public function invalidateCache()
	{
		self::$objcache = array();
	}

	/**
	 *	 Zet een ongelimiteerd aantal id's om naar een enkel id.
	 *
	 *	Deze functie accepteert een arbitrair (doch >= 1) aantal ids en voegt
	 *	deze samen tot een enkel id.  Deze functie wordt gebruikt zodat een
	 *	object een eenduididge identifier string heeft.
	 *
	 *	@return een string die alle ingevoerde ids bevat
	 *
	 *	\todo review het gebruik van deze functie...
	 */
	static public function naarID ()
	{
		$args = func_get_args();
		if(is_null($args))
			return NULL;
		if(count($args) == 1 && is_array($args[0]))
			$args = $args[0];
		foreach($args as $arg) {
			if(is_null($arg))
				return NULL;
		}
		return implode('_', $args);
	}

	/**
	 *	 Splitst een samengestelde id op in individuele ids.
	 *	Dit is de inverse van naarID.
	 *
	 *	FIXME (docs): Waarom bestaat dit???
	 *
	 *	@return een array van ids
	 *
	 *	@see naarID
	 */
	static protected function vanID ($idstr)
	{
		return explode('_', $idstr);
	}

	/**
	 *  Geef een array met errors die de velden van dit objecten hebben.
	 *
	 * @param welkeVelden string die de verzameling velden representeert die 
	 * op errors gecheckt worden.
	 * @return een associatieve array die namen van velden van dit object 
	 * afbeeldt op errors (strings), verkregen door check*-methodes aan te 
	 * roepen.
	 */
	public function getErrors ($welkeVelden = 'set')
	{
		$velden = static::velden($welkeVelden);

		foreach($velden as $veld) {
			if(isset($this->errors[$veld]))
				continue;

			$this->errors[$veld] = $this->{'check'.$veld}();
		}

		return parent::getErrors();
	}

	/**
	 *  override DBObject::gewijzigd()
	 */
	public function gewijzigd ($updatedb = False)
	{
		$this->dirty = True;

		if($updatedb)
			user_error('gewijzigd(True) aangeroepen', E_USER_WARNING);
	}

	/**
	 *  Bepaal of velden van dit object geldig zijn.
	 *
	 * @param welkeVelden de string die de verzameling velden representeert die 
	 * gebruikt worden.
	 * @return True indien de door welkeVelden bepaalde velden allemaal goed 
	 * door hun respectievelijke check*-methode heenkomen; False anders.
	 */
	public function valid ($welkeVelden = 'set')
	{
		$errors = $this->getErrors($welkeVelden);
		if (!is_null($errors))
			foreach($errors as $error)
			{
				if(is_string($error))
					return false;
				if(is_array($error))
					foreach ($error as $msg)
						if (is_string($msg))
							return false;
			}

		return true;
	}

	/**
	 *  Maak een array met veldnamen.
	 *
	 * @param view Een string die aangeeft om welke subset velden het gaat.
	 * @return Een array van strings. Elke string is een veldnaam van deze klasse.
	 */
	public static function velden ($view = NULL)
	{
		return array();
	}

	/**
	 *  Bekijk van alle velden of ze verschillen en geef een associatieve 
	 * array terug met de verschillen.
	 *
	 * @param object Een DBObject waarmee vergeleken wordt.
	 * @return Een associatieve array met als keys de namen van velden waarop 
	 * dit DBObject verschilt van object. De values van de array zijn arrays 
	 * met de waarden die het veld heeft in respectievelijk dit DBObject en 
	 * object.
	 */
	public function difference (DBObject $object)
	{
		if(get_class($object) != get_class($this))
			user_error("Je vergelijkt appels met peren: " . get_class($object) . " vs " . get_class($this), E_USER_ERROR);
		
		$diff = array();
		foreach(static::velden('set') as $veld)
		{
			$deze = call_user_func(array($this, 'get' . $veld));
			$andere = call_user_func(array($object, 'get' . $veld));
			
			if($deze != $andere)
				$diff[$veld] = array($deze, $andere);			
		}
		return $diff;
	}

	/**
	 *  Bekijk of we $obj->verwijderen() kunnen aanroepen.
	 *
	 * Classes kunnen dit overriden om het verwijderen te voorkomen, 
	 * je mag bijvoorbeeld alleen commissies verwijderen zonder leden en activiteiten
	 * i.e. die je per ongeluk hebt aangemaakt
	 *
	 * Dit staat dus los van magVerwijderen(), die checkt alleen autorisatie
	 *
	 * @return False als het object verwijderd kan worden; anders een string 
	 * met een foutmelding.
	 */	 
	public function checkVerwijderen ()
	{
		return false;
	}

	/**
	 *  Geeft het fakeID van een object terug
	 *
	 * @return Het fakeID van het object
	 */
	public function getFakeID()
	{
		return $this->fakeID;
	}

	/**
	 *  Set het fakeID van een object
	 *
	 * @param newFakeID Het nieuwe FakeID van het object
	 *
	 * @return Het object
	 */
	public function setFakeID($newFakeID)
	{
		$this->fakeID = $newFakeID;

		return $this;
	}

	/**
	 * @brief Geef de basis-URL van een object.
	 *
	 * De bedoeling van deze methode is dat een functie als `wijzigURL()`
	 * gewoon kan doen `return $this->url() . '/Wijzig';`.
	 *
	 * @return De URL naar de (variabele entry) die bij dit object hoort.
	 * Voor een Commissie bijvoorbeeld `'/Vereniging/Commissie/webcie'`.
	 * Indien geen URL bekend is, gooit `HeeftGeenURLException`.
	 *
	 * @see verwijderURL
	 * @see wijzigURL
	 */
	public function url()
	{
		throw new HeeftGeenURLException("$this heeft geen URL!");
	}

	/**
	 * @brief Geef de verwijder-URL van een object.
	 *
	 * Gaat ervan uit dat `$this->url() . '/Verwijder'` werkt.
	 * Override het in je kindklasse als dat niet klopt.
	 *
	 * @return De URL naar de verwijderpagina die bij dit object hoort.
	 * Indien geen URL bekend is, gooit `HeeftGeenURLException`.
	 */
	public function verwijderURL()
	{
		return $this->url() . '/Verwijder';
	}

	/**
	 * @brief Geef de wijzig-URL van een object.
	 *
	 * Gaat ervan uit dat `$this->url() . '/Wijzig'` werkt.
	 * Override het in je kindklasse als dat niet klopt.
	 *
	 * @return De URL naar de wijzigpagina die bij dit object hoort.
	 * Indien geen URL bekend is, gooit `HeeftGeenURLException`.
	 */
	public function wijzigURL()
	{
		return $this->url() . '/Wijzig';
	}

	/**
	 * @brief Geef een lijst van dependencies op een generiek DBObject (dus geen).
	 */
	public function getDependencies()
	{
		return [];
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
