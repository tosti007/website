<?
abstract class Query_Generated
	extends QueryBuilder
{
	protected $sql;
	protected $fetchType;				/**< \brief ENUM:MAYBEVALUE/MAYBETUPLE/COLUMN/TABLE/CURSOR */
	/**
	 * @brief De constructor van de Query_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // QueryBuilder
		$this->sql = '';
		$this->fetchType = 'TABLE';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld sql.
	 *
	 * @return string
	 * De waarde van het veld sql.
	 */
	public function getSql()
	{
		return $this->sql;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld fetchType.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld fetchType.
	 */
	static public function enumsFetchType()
	{
		static $vals = array('MAYBEVALUE','MAYBETUPLE','COLUMN','TABLE','CURSOR');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld fetchType.
	 *
	 * @return string
	 * De waarde van het veld fetchType.
	 */
	public function getFetchType()
	{
		return $this->fetchType;
	}
}
