<?
/**
 * @brief vergeet niet de 'implements Iterator' (is pre-defined in php)
 */
abstract class Verzameling_Generated
{
	protected $verzameling;
	protected $volgorde;
	protected $positie;
	/**
	 * @brief De constructor van de Verzameling_Generated-klasse.
	 */
	public function __construct()
	{
		$this->verzameling = array();
		$this->volgorde = array();
		$this->positie = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	abstract public function aantal();
	abstract public function bevat($id);
	abstract public function verwijder($id);
	abstract public function voegtoe($object);
	abstract public function union($verzameling);
	abstract public function intersection($verzameling);
	abstract public function complement($verzameling);
	abstract public function difference($verzameling);
	abstract public function magVerwijderen();
	abstract public function magWijzigen();
}
