<?
abstract class Entiteit_Generated
	extends DBObject
{
	protected $gewijzigdWanneer;		/**< \brief NULL,NO_CONSTRUCTOR */
	protected $gewijzigdWie;			/**< \brief NULL,NO_CONSTRUCTOR */
	/**
	 * @brief De constructor van de Entiteit_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // DBObject
		$this->gewijzigdWanneer = new DateTimeLocale(NULL);
		$this->gewijzigdWie = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld gewijzigdWanneer.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld gewijzigdWanneer.
	 */
	public function getGewijzigdWanneer()
	{
		return $this->gewijzigdWanneer;
	}
	/**
	 * @brief Geef de waarde van het veld gewijzigdWie.
	 *
	 * @return int
	 * De waarde van het veld gewijzigdWie.
	 */
	public function getGewijzigdWie()
	{
		return $this->gewijzigdWie;
	}
	/**
	 * @brief Controleer of de huidig ingelogde gebruiker dit object mag bekijken.
	 */
	abstract public function magBekijken();
	/**
	 * @brief Controleer of de huidig ingelogde gebruiker dit object mag verwijderen.
	 */
	abstract public function magVerwijderen();
	/**
	 * @brief Controleer of de huidig ingelogde gebruiker dit object mag wijzigen.
	 */
	abstract public function magWijzigen();
	abstract public function getBekijkbareVars();
	abstract public function getWijzigbareVars();
}
