<?
abstract class ConstraintVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de ConstraintVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
}
