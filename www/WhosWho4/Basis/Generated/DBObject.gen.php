<?
abstract class DBObject_Generated
{
	protected $inDB;
	protected $dirty;
	protected $errors;
	/**
	 * @brief De constructor van de DBObject_Generated-klasse.
	 */
	public function __construct()
	{
		$this->inDB = False;
		$this->dirty = True;
		$this->errors = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld inDB.
	 *
	 * @return bool
	 * De waarde van het veld inDB.
	 */
	public function getInDB()
	{
		return $this->inDB;
	}
	/**
	 * @brief Geef de waarde van het veld dirty.
	 *
	 * @return bool
	 * De waarde van het veld dirty.
	 */
	public function getDirty()
	{
		return $this->dirty;
	}
	/**
	 * @brief Geef de waarde van het veld errors.
	 *
	 * @return mixed
	 * De waarde van het veld errors.
	 */
	public function getErrors()
	{
		return $this->errors;
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	abstract public function geefID();
	/**
	 * @brief Sla het object op in de database.
	 */
	abstract public function opslaan();
	/**
	 * @brief Controleer of dit object geldig is.
	 */
	abstract public function valid();
	/**
	 * @brief Verwijder het object.
	 */
	abstract public function verwijderen();
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	abstract public function gewijzigd($updatedb = False);
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	abstract protected function naarDB($veld, $waarde, $lang);
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	abstract protected function uitDB($veld, $lang);
	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	abstract public function getDependencies();
}
