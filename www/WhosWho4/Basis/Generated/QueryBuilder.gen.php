<?
abstract class QueryBuilder_Generated
{
	protected $fetchType;				/**< \brief ENUM:MAYBEVALUE/MAYBETUPLE/COLUMN/TABLE */
	protected $statements;
	protected $bindings;
	protected $objects;
	protected $selects;
	/**
	 * @brief De constructor van de QueryBuilder_Generated-klasse.
	 */
	public function __construct()
	{
		$this->fetchType = 'TABLE';
		$this->statements = array();
		$this->bindings = array();
		$this->objects = array();
		$this->selects = array();
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld fetchType.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld fetchType.
	 */
	static public function enumsFetchType()
	{
		static $vals = array('MAYBEVALUE','MAYBETUPLE','COLUMN','TABLE');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld fetchType.
	 *
	 * @return string
	 * De waarde van het veld fetchType.
	 */
	public function getFetchType()
	{
		return $this->fetchType;
	}
	/**
	 * @brief Geef de waarde van het veld statements.
	 *
	 * @return mixed
	 * De waarde van het veld statements.
	 */
	public function getStatements()
	{
		return $this->statements;
	}
	/**
	 * @brief Geef de waarde van het veld bindings.
	 *
	 * @return mixed
	 * De waarde van het veld bindings.
	 */
	public function getBindings()
	{
		return $this->bindings;
	}
	/**
	 * @brief Geef de waarde van het veld objects.
	 *
	 * @return mixed
	 * De waarde van het veld objects.
	 */
	public function getObjects()
	{
		return $this->objects;
	}
	/**
	 * @brief Geef de waarde van het veld selects.
	 *
	 * @return mixed
	 * De waarde van het veld selects.
	 */
	public function getSelects()
	{
		return $this->selects;
	}
}
