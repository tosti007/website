<?
/**
 *  Basisklasse voor View-klassen.
 *
 * View-klassen zijn niet-instantieerbare klassen die statische methoden
 * bevatten ten behoeve van het weergeven van objecten van een bepaalde klasse.
 */
abstract class View
	extends View_Generated
{

	/**
	 *  Methode om op terug te vallen indien er geen formulieronderdeel
	 * beschikbaar is.
	 *
	 * @param obj Een object.
	 * @return Een statische veergave van het object.
	 * @see defaultWaarde
	 */
	static public function defaultForm ($obj)
	{
		return static::defaultWaarde($obj);
	}

	/**
	 *  Genereer een html-veilige string van een object.
	 *
	 * @param obj Een object.
	 * @return Een statische weergave van het object, bestaande uit diens
	 * klasse en ID gescheiden door een hekje #.
	 */
	static public function defaultWaarde ($obj)
	{
		if (is_null($obj))
			return null;
		if ($obj instanceof DBObject)
		{
			$desc = get_class($obj) . ' #' . $obj->geefID();

			// Probeer een URL ervan te maken.
			try
			{
				$url = $obj->url();
				return new HtmlAnchor($url, $desc);
			}
			catch (\HeeftGeenURLException $e)
			{
				return $desc;
			}
		}

		return get_class($obj);
	}

	/**
	 *  Geef wanneer een object voor het laatst gewijzigd is.
	 *
	 * @param obj Een object.
	 * @return Een DateTimeLocale die het moment representeert waarop een
	 * object voor het laatst gewijzigd is.
	 * *todo deze kan ook generated worden
	 */
	static public function waardeGewijzigdWanneer (Entiteit $obj)
	{
		return self::defaultWaardeDateTime($obj, 'GewijzigdWanneer');
	}

	/**
	 *  Geef wie een object voor het laatst gewijzigd heeft.
	 *
	 * @param obj Een object.
	 * @return De ID van de Persoon die obj als laatste gewijzigd heeft.
	 * *todo deze kan ook generated worden
	 */
	static public function waardeGewijzigdWie (Entiteit $obj)
	{
		return self::defaultWaardeText($obj, 'GewijzigdWie');
	}

	/**
	 *	@name default info view
	 *@{
	 */

	/**
	 *  Maak een tabel met labels en velden van een object.
	 *
	 * @param obj Een object.
	 * @param header Een bool of de header ook meegegeven moet worden
	 * @return Een tabel, met voor elk 'view'-veld van obj het label en de waarde van dat veld.
	 * @see infoTR
	 * @see velden
	 */
	public static function viewInfo($obj, $header = false)
	{
		//Zoek uit welke class we moeten gebruiken: pak de class van de view zonder view.
		$class = substr(get_called_class(), 0, -4);

		$tbl = new HtmlDiv(null, 'info-table');
		foreach($class::velden('viewInfo') as $veld)
		{
			$tbl->add(self::call('infoTR', $veld, $obj));
		}
		return new HtmlDiv(
			array(
				($header ? new HtmlDiv(new HtmlStrong(get_class($obj)), 'panel-heading') : ''),
				new HtmlDiv($tbl, 'panel-body')
			)
			, 'panel panel-default'
		);
	}

	/**
	 *   default implementatie van een HtmlTableRow met een label..() en
	 *  waarde...() erin.
	 *
	 *	Als je de row van een specifiek $veld anders wilt, doe dat dan in de
	 *	methode infoTR$veld().
	 *
	 *	@param obj Een object.
	 *	@param veld De naam van een veld van obj.
	 *	@return Een HtmlTableRow met het label en de waarde van veld.
	 */
	protected static function infoTR($obj, $veld)
	{
		$row = new HtmlDiv(null, 'infotr-row');
		$row->add(new HtmlLabel($veld, self::call('label',  $veld, $obj), 'col-sm-2 control-label'));
		$row->add(new HtmlDiv(self::call('waarde', $veld, $obj), 'infotr-veld col-sm-10'));
		return $row;
	}

	protected static function infoTRData($label, $data)
	{
		$row = new HtmlDiv(null, 'infotr-row');
		$row->add(new HtmlLabel('', $label, 'col-sm-2 control-label'));
		$row->add(new HtmlDiv($data, 'infotr-veld col-sm-10'));
		return $row;
	}
	//@}

	/**
	 *  Maak een tabelcel met een datum die goed gesorteerd wordt.
	 *
	 * @param date Een DateTimeLocale.
	 * @param format Het format waarin date wordt weergegeven, of NULL voor het
	 * default format.
	 * @return Een HtmlTableDataCell met date en een juiste sortering.
	 * @see outputDateTime
	 * @see HtmlTableDataCell::maakGesorteerde
	 */
	public static function dateTimeTD (DateTimeLocale $date, $format = NULL)
	{
		return HtmlTableDataCell::maakGesorteerde
			(self::outputDateTime($date, $format), $date->strftime('%s'));
	}

	/**
	 *	@name default info view
	 *@{
	 */
	public static function createForm ($obj, $show_error = true, $velden = 'viewWijzig')
	{
		$ret = array();
		$class = substr(get_called_class(), 0, -4);

		foreach($class::velden($velden) as $veld)
		{
			$ret[] = self::wijzigTR($obj, $veld, $show_error);
		}
		return $ret;
	}

	public static function processForm($obj, $velden = 'viewWijzig', $data = null, $include_id = false)
	{
		if (is_null($data))
			$data = tryPar(self::formobj($obj, $include_id), NULL);
		if(!$data)
			return False;

		//Ook hier gaan we er van uit dat we worden aangeroepen vanuit *View,
		// zodat we een classname overhouden.
		$class = substr(get_called_class(), 0, -4);

		foreach($class::velden($velden) as $veld)
		{
			if(!array_key_exists($veld, $data))
				continue;
			$val = $data[$veld];

			// Als er een DateTimeLocale gepost wordt,
			// moet het losse tijdsveld er aan geplakt worden
			if($val && self::callobj($obj, 'get', $veld) instanceof DateTimeLocale)
			{
				$val .= ' '.tryPar('Time'.self::formobj($obj, $include_id).'['.$veld.']','');
			}

			if(is_array($val))
			{
				foreach ($val as $lang => $value)
					self::callobj($obj, 'set', $veld, array($value, $lang));
				continue;
			}
			elseif(strlen($val) == 0)
				$val = NULL;

			self::callobj($obj, 'set', $veld, array($val));
		}

		return $obj->valid();
	}

	protected static function wijzigHead ($obj, $show_error = true)
	{
		$row = new HtmlTableRow();
		$row->addHeader(_('Wat'));
		$row->addHeader(_('Gegevens'));
		$row->addHeader(_('Opmerking'));

		if ($show_error && !$obj->valid())
			$row->addHeader(_('Foutmelding'));

		return $row;
	}

	/**
	 *	 intern: default implementatie van een HtmlTableRow met een
	 *	toonLabel...(), toonForm...() en toonOpmerking...() erin.
	 *
	 *	Als je de row van een specifieke $var anders wilt, doen dat dan in de
	 *	methode toonTRwijzig$var().
	 */
	protected static function wijzigTR ($obj, $veld, $show_error = true, $include_id = false)
	{
		$class = substr(get_called_class(), 0, -4);

		$verplicht = $obj == null ? false : in_array($veld, $class::velden('verplicht'));
		$multilang = $obj == null ? false : in_array($veld, $class::velden('lang'));
		$msg = '';
		if($show_error && !$obj->valid())
		{
			if(method_exists($obj, 'check' . $veld))
			{
				$msg = call_user_func(array($obj, 'check' . $veld));
			}
		}

		$tdiv = new HtmlDiv();

		$er_is_error = !(empty($msg) || (is_array($msg) && sizeof(array_filter($msg)) == 0));

		if($show_error)
		{
			if (!$er_is_error)
				$tdiv->addClass('form-group has-success has-feedback');
			else
				$tdiv->addClass('form-group has-error has-feedback');
		}
		else
			$tdiv->addClass('form-group');

		switch($class::getVeldType(strtolower($veld)))
		{
		case 'bool':
			$i = self::call('form', $veld, $obj, $include_id);
			$l = self::call('label', $veld, $obj) . ($verplicht ? new HtmlSpan('*', 'waarschuwing') : '');
			$tdiv->add(new HtmlDiv(new HtmlDiv(
				'<label>' . $i . $l . '</label>', 'checkbox')
				, 'col-sm-offset-2 col-sm-10'));
			break;
		case 'enum':
			$tdiv->add(new HtmlLabel($veld
				, self::call('label', $veld, $obj) . ($verplicht ? new HtmlSpan('*', 'waarschuwing') : '')
				, 'col-sm-2 control-label'));
			$tdiv->add(new HtmlDiv(self::call('form', $veld, $obj, $include_id), 'col-sm-10'));
			break;
		default:
			$tdiv->add(new HtmlLabel($veld
				, self::call('label', $veld, $obj) . ($verplicht ? new HtmlSpan('*', 'waarschuwing') : '')
				, 'col-sm-2 control-label'));
			$tdiv->add($cdiv = new HtmlDiv(self::call('form', $veld, $obj, $include_id), 'col-sm-10'));
			if($show_error)
			{
				if (!$er_is_error)
					$cdiv->add(HtmlSpan::fa('check', _('Goed'), 'fa-2x form-control-feedback'));
				else
					$cdiv->add(HtmlSpan::fa('times', _('Fout'), 'fa-2x form-control-feedback'));
			}
			break;
		}

		if ($show_error && $er_is_error)
		{
			// Vanwege historische redenen is het moeilijk kut om bij de velden zelf de errors neer te zetten,
			// daarom moeten we hier kijken wat voor een soort errors er gebeurd zijn
			// om ze nog min of meer zinnig weer te geven.
			if (is_array($msg)) {
				$error_format = "";
				foreach ($msg as $key => $value) {
					if ($value) {
						if ($error_format) {
							$error_format .= ", ";
						}
						$error_format .= strtoupper($key) . ": " . $value;
					}
				}
			} else {
				$error_format = $msg;
			}
			$tdiv->add(new HtmlSpan(
				sprintf(_('Error: %s[VOC: errorbericht bij veld]'), $error_format),
				'help-block col-sm-offset-2 col-sm-10'));
		}

		$opmerking = self::call('opmerking', $veld, $obj);
		if(!empty($opmerking))
		{
			$tdiv->add(new HtmlSpan(_('Opmerking: ') . $opmerking, 'help-block col-sm-offset-2 col-sm-10'));
		}

		return $tdiv;
	}

	/**
	 * @brief Maak een generiek form voor het verwijderen van een object
	 **/
	public static function verwijderForm($obj)
	{
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(2, sprintf(_('Verwijderen van %s'), strtolower(get_class($obj))) ))
			->add(self::viewInfo($obj));

		$div->add(new HtmlHeader(3, _('De volgende objecten worden meeverwijderd')))
			->add($ul = new HtmlList());
		foreach ($obj->getDependencies() as $klassenaam => $dependencyVerzameling)
		{
			foreach ($dependencyVerzameling as $dependency)
			{
				$viewClass = $dependency->getView();
				$defaultWaardeFunctie = "defaultWaarde" . get_class($dependency);
				$ul->add($li = new HtmlListItem($viewClass::$defaultWaardeFunctie($dependency)));
			}
		}

		//Mogen we deze wel weggooien?
		$error = $obj->checkVerwijderen();
		if(!$error)
		{
			$div->add($form = HtmlForm::named(get_class($obj) . 'Verwijderen'));
			$form->add(new HtmlParagraph(sprintf(_("Weet je zeker dat je dit/deze %s wilt verwijderen?"), strtolower(get_class($obj))) ));
			$form->add(HtmlInput::makeSubmitButton(_("Jazeker!")));
		}
		else
		{
			$div->add(new HtmlSpan(_("Helaas, deze mag je niet weggooien: ") . $error, "text-danger strongtext"));
		}

		return $div;
	}

	/**
	 *	  Helper die het overriden makkelijker moet maken.
	 *
	 *	Probeer eerste self::$method$veld($obj) aan te roepen, als die niet
	 *	bestaat doe dan de fallback naar de generiekere methode
	 *	self::$method($obj, $veld)
	 *
	 *	@param methode De naam van een methode.
	 *	@param veld De naam van een veld van obj.
	 *	@param obj Een object.
	 */
	protected static function call ($methode, $veld, $obj)
	{
		$class = get_called_class();

		$args = func_get_args();
		$methode = array_shift($args);
		$veld = array_shift($args);

		// de specifieke implementatie
		if(method_exists($class, $methode.$veld))
			return call_user_func_array(array($class, $methode.$veld), $args);

		// of anders een algemene fallback
		if(method_exists($class, $methode))
		{
			if(!is_object($obj))
				user_error("obj is geen object in aanroep van methode"
				. "$class::$methode (voor veld '$veld')"
				, E_USER_ERROR);

			$obj = array_shift($args);
			array_unshift($args, $obj, $veld);

			return call_user_func_array(array($class, $methode), $args);
		}

		// FAIL! geen methode gevonden
		user_error("onbekende methode $class::$methode (voor veld '$veld')"
			, E_USER_WARNING);
	}
	protected static function callobj($obj, $methode, $veld, $args = NULL)
	{
		if(is_null($args))
			$args = array();
		if(!is_array($args))
			$args = array($args);

		// de specifieke implementatie
		if(method_exists($obj, $methode.$veld))
			return call_user_func_array(array($obj, $methode.$veld), $args);

		// FAIL! geen methode gevonden
		user_error("object ".get_class($obj)." heeft geen methode '$methode$veld'", E_USER_WARNING);
	}

	protected static function outputDate($val)
	{
		return self::outputDateTime($val, '%e %b %Y');
	}
	protected static function outputDateTime($val, $format = NULL)
	{
		if( is_null($val) || ($val instanceof DateTimeLocale && !$val->hasTime()) ) {
			return "";
		} elseif( !($val instanceof DateTimeLocale)) {
			$val = new DateTimeLocale($val);
		}
		if (is_null($format))
			$format = '%a %e %b %H:%M %Y';
		return $val->strftime($format);
	}

	/**
	 *	  Helper die een table row met informatie over het object teruggeeft.
	 *
	 *	@param obj Een object.
	 *	@param velden De namen van de velden waarvan de waarden in de table row moet komen.
	 *
	 *	@return De table row met de data erin.
	 **/
	protected static function dataTR($obj, $velden)
	{
		$row = new HtmlTableRow();

		foreach($velden as $veld) {
			$row->addData(self::call('waarde', $veld, $obj));
		}

		return $row;
	}

	/**
	 *  	Functie die een objectverzameling omzet in een table met
	 *			per row een object met de gewenste data teruggeeft.
	 *
	 *	@param objVerzameling De verzameling van objecten
	 *	@param velden Een array van de namen van de velden waarvan je de data
	 *		   in de table wilt hebben staan
	 *  @param headers Zo niet leeg, vervangt veldnamen als tabelheaderregel.
	 *  @param tableclasses Een string met eventuele extra classes voor de table
	 *  @param immutable Een bool of de rows immutable aan de table moeten
	 *		   worden toegevoegd of niet.
	 *
	 *  @return Een table met daarin de gewenste data
	 **/
	protected static function makeTable(
		$objVerzameling,
		$velden = array(),
		$headers = array(),
		$tableclasses = '',
		$immutable = false
	) {
		$table = new HtmlTable(null, $tableclasses);
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$theadRow = $thead->addRow();
		if (empty($headers))
		{
			$theadRow->makeHeaderFromArray($velden);
		}
		else
		{
			$theadRow->makeHeaderFromArray($headers);
		}
		$theadRow->add(new HtmlTableHeaderCell()); // wijzig
		$theadRow->add(new HtmlTableHeaderCell()); // verwijder

		foreach ($objVerzameling as $obj)
		{
			$class = get_class($obj) . 'View';
			$row = $class::dataTR($obj, $velden);

			if ($obj->magWijzigen())
			{
				try
				{
					$wijzigKnop = HtmlAnchor::button($obj->wijzigURL(), _("Wijzig"));
				}
				catch (HeeftGeenURLException $e)
				{
					$wijzigKnop = '';
				}
			}
			else
			{
				$wijzigKnop = '';
			}
			$row->add(new HtmlTableDataCell($wijzigKnop));
			if ($obj->magVerwijderen())
			{
				try
				{
					$verwijderKnop = HtmlAnchor::button($obj->verwijderURL(), _("Verwijder"));
				}
				catch (HeeftGeenURLException $e)
				{
					$verwijderKnop = '';
				}
			}
			else
			{
				$verwijderKnop = '';
			}
			$row->add(new HtmlTableDataCell($verwijderKnop));

			if ($immutable)
			{
				$tbody->addImmutable($row);
			}
			else
			{
				$tbody->add($row);
			}
		}

		return $table;
	}

	/**
	 * @name defaultWaarde\<type\>
	 * Deze methoden worden gebruikt door de gegenereerde waarde$veld()
	 * methoden in de generated view klassen.
	 *@{
	 */

	/**
	 * Maak een waarde van een bool.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een string: 'ja' indien de bool True is; 'nee' indien deze False
	 * is. Beide strings zijn vertaalbaar.
	 */
	protected static function defaultWaardeBool ($obj, $veld)
	{
		$waarde = self::callobj($obj, 'get', $veld);
		return ($waarde ? _('ja') : _('nee'));
	}

	/**
	 * Maak een waarde van een datum.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een string die de datum representeert. Het standaardformaat
	 * hiervoor staat beschreven in outputDate().
	 * @see outputDate
	 */
	protected static function defaultWaardeDate($obj, $veld)
	{
		$waarde = self::callobj($obj, 'get', $veld);
		return self::outputDate($waarde);
	}

	/**
	 * Maak een waarde van een datum/tijd.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een string die de datum/tijd representeert. Het standaardformaat
	 * hiervoor staat beschreven in outputDateTime().
	 * @see outputDateTime
	 */
	protected static function defaultWaardeDatetime($obj, $veld)
	{
		$waarde = self::callobj($obj, 'get', $veld);
		return self::outputDateTime($waarde);
	}

	/**
	 * Maak een waarde van een enum.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een string die de enum representeert, verkregen door een aanroep
	 * aan de relevante labelenum-methode in de View van obj.
	 */
	protected static function defaultWaardeEnum($obj, $veld)
	{
		$waarde = self::callobj($obj, 'get', $veld);
		return self::call('labelenum', $veld, $waarde);
	}

	/**
	 * Maak een waarde van een int.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return De waarde van het veld, als int.
	 */
	protected static function defaultWaardeInt($obj, $veld)
	{
		return (int) self::callobj($obj, 'get', $veld);
	}

	/**
	 * Maak een waarde van een bedrag.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een string die de waarde van het veld als geldbedrag
	 * representeert.
	 * @see Money::addPrice
	 */
	protected static function defaultWaardeMoney($obj, $veld)
	{
		return Money::addPrice(self::callobj($obj, 'get', $veld));
	}

	/**
	 * Maak een waarde van een string.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return De waarde van het veld, ge-escaped met htmlspecialchars.
	 */
	protected static function defaultWaardeString($obj, $veld)
	{
		$val = self::callobj($obj, 'get', $veld);
		return htmlspecialchars($val);
	}

	/**
	 * Maak een waarde van een tekst.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return De waarde van het veld, ge-escaped met htmlspecialchars.
	 * Newlines in de tekst worden vervangen door <br>.
	 */
	protected static function defaultWaardeText($obj, $veld)
	{
		$val = self::callobj($obj, 'get', $veld);
		return nl2br(htmlspecialchars($val));
	}

	/**
	 * Maak een waarde van html.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return De waarde van het veld.
	 */
	protected static function defaultWaardeHtml ($obj, $veld)
	{
		return self::callobj($obj, 'get', $veld);
	}

	/**
	 * Maak een waarde van een URL.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een link naar de waarde van het veld, met dezelfde caption.
	 * Indien de URL met een / begint, wordt er nog 'www.A-Eskwadraat.nl'
	 * vooraan geplakt.
	 * @see HtmlAnchor
	 */
	protected static function defaultWaardeUrl($obj, $veld)
	{
		$caption = $url = self::callobj($obj, 'get', $veld);
		if($url[0] == '/')
			$caption = 'www.A-Eskwadraat.nl' . $caption;

		return new HtmlAnchor($url, $caption);
	}

	/**
	 * Maak een waarde van een collegejaar.
	 *
	 * Als de waarde van het veld bijvoorbeeld '2008' is, maakt deze methode er
	 * '2008-2009' van.
	 *
	 * @param obj Een object.
	 * @param veld De naam van een veld van obj, dat door een aanroep aan de
	 * methode get$veld wordt opgehaald uit het object.
	 * @return Een string die een representatie is van het collegejaar in het
	 * veld.
	 */
	protected static function defaultWaardeColjaar($obj, $veld)
	{
		$waarde = self::callobj($obj, 'get', $veld);
		return $waarde . '-' . ($waarde + 1);
	}
	//@}

	/**
	 *	@name defaultForm\<type\>
	 *	Deze methoden worden gebruikt door de gegenereerde form$veld() methoden
	 *	in de generated view klassen.
	 *@{
	 */
	protected static function formobj($obj, $include_id = false)
	{
		return ($obj instanceof DBObject || $obj instanceof Verzameling) ?
			get_class($obj) . (($include_id) ? '['.(($obj->geefID())?$obj->geefID():(($obj->getFakeID())?$obj->getFakeID():0)).']' : '')
			: (string) $obj;
	}
	protected static function formveld($obj, $veld, $lang = null, $include_id = false)
	{
		if (is_null($lang))
			return self::formobj($obj, $include_id).'['.$veld.']';
		else
			return self::formobj($obj, $include_id).'['.$veld.']['.$lang.']';
	}

	protected static function inputRowDiv($input, $name, $label, $opmerking = NULL)
	{
		$div = new HtmlDiv(null, 'form-group');
		$div->add(new HtmlLabel($name, $label, 'col-sm-2 control-label'));

		$div->add($idiv = new HtmlDiv(null, 'col-sm-10'));

		$idiv->add($input);

		if($opmerking)
			$idiv->add(new HtmlSpan(sprintf('%s: %s', _('Opmerking'), $opmerking)
			, 'help-block col-offset-sm-2 col-sm-10'));

		return $div;
	}

	protected static function defaultFormBool($obj, $veld, $include_id = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		return self::genericDefaultFormBool($name, $waarde, $veld);
	}

	protected static function genericDefaultFormBool($name, $waarde, $veld)
	{
		return HtmlInput::makeCheckbox($name, tryPar($name, $waarde));
	}

	protected static function makeFormBoolRow($name, $value = False, $label, $opmerking = null)
	{
		$div = new HtmlDiv(null, 'form-group');
		$div->add($idiv = new HtmlDiv(null, 'col-sm-offset-2 col-sm-10'));

		$idiv->add($ldiv = new HtmlDiv(null, 'checkbox'));
		$ldiv->add('<label>');

		$ldiv->add(self::genericDefaultFormBool($name, $value, ''));
		$ldiv->add($label);
		$ldiv->add('</label>');

		if($opmerking)
			$idiv->add(new HtmlSpan(sprintf('%s: %s', _('Opmerking'), $opmerking)
			, 'help-block col-offset-sm-2 col-sm-10'));

		return $div;
	}

	protected static function defaultFormDate($obj, $veld, $include_id = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		$input = self::genericDefaultFormDate($name, $waarde, $veld);
		$input->setPlaceholder(self::call('label', $veld, $obj));
		return $input;
	}

	protected static function genericDefaultFormDate($name, $waarde, $veld)
	{
		return HtmlInput::makeDate($name, tryPar($name, $waarde), 10);
	}

	protected static function makeFormDateRow($name, $waarde, $label, $opmerking = null)
	{
		return self::inputRowDiv(self::genericDefaultFormDate($name, $waarde, ''), $name, $label, $opmerking);
	}

	protected static function defaultFormDatetime($obj, $veld, $include_id = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		$input = self::genericDefaultFormDatetime($name, $waarde, $veld, in_array($veld, $obj::velden('verplicht')));
		$input->setPlaceholder(self::call('label', $veld, $obj));

		return $input;
	}

	protected static function genericDefaultFormDatetime($name, $waarde, $veld, $required=false)
	{
		return HtmlInput::makeDatetime($name, tryPar($name, $waarde), null, null, $required);
	}

	protected static function defaultFormEnum($obj, $veld, $include_id = false, $selected = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$enums = self::callobj($obj, 'enums', $veld);
		$waarde = self::callobj($obj, 'get', $veld);
		return self::genericDefaultFormEnum($name, $waarde, $veld, $enums, $selected);
	}

	protected static function genericDefaultFormEnum($name, $waarde, $veld, $enums, $selected=false)
	{
		$select = new HtmlSelectbox($name);
		if (!in_array($waarde, $enums))
		{
			$label = self::call('labelenum', $veld, $waarde);
			$select->addOption($waarde, $label);
		}
		foreach ($enums as $enum)
		{
			$label = self::call('labelenum', $veld, $enum);
			$select->addOption($enum, $label);
		}
		$select->select(tryPar($name, $waarde));
		if($selected)
			$select->select($selected);

		return $select;
	}

	protected static function makeFormEnumRow($name, $options, $waarde = array(), $label = null, $opmerking = null, $autoSubmit = false)
	{
		$i = HtmlSelectbox::fromArray($name, $options, $waarde);
		if($autoSubmit)
			$i->setAutoSubmit();
		return self::inputRowDiv($i, $name, $label, $opmerking);
	}

	protected static function defaultFormInt($obj, $veld, $min = null, $max = null, $include_id = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		$input = self::genericDefaultFormInt($name, $waarde, $veld, $min, $max);
		$input->setPlaceholder(self::call('label', $veld, $obj));
		return $input;
	}

	protected static function genericDefaultFormInt($name, $waarde, $veld, $min=null, $max=null) {
		return HtmlInput::makeNumber($name, tryPar($name, $waarde), $min, $max);
	}

	protected static function makeFormNumberRow($name, $waarde, $min = null, $max = null, $label = null, $opmerking = null)
	{
		return self::inputRowDiv(self::genericDefaultFormInt($name, $waarde, $min, $max)
			, $name, $label, $opmerking);
	}

	protected static function defaultFormMoney($obj, $veld, $min = null, $max = null, $include_id = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		$label = self::call('label', $veld, $obj);
		return self::genericDefaultFormMoney($name, $waarde, $veld, $min, $max, $label);
	}

	protected static function genericDefaultFormMoney($name, $waarde, $veld, $min=null, $max=null, $label=null) {
		$div = new HtmlDiv(null, 'input-group');

		$div->add(new HtmlDiv('€', 'input-group-addon'));

		$div->add($input = self::genericDefaultFormInt($name, $waarde, $veld, $min, $max));
		$input->setAttribute('step', '0.01');
		$input->setValue(tryPar($name, $waarde));

		if (!is_null($label)) {
			$input->setPlaceholder($label);
		}

		return $div;
	}

	protected static function makeFormMoneyRow($name, $waarde = null, $min = null, $max = null, $label = null, $opmerking = null)
	{
		return self::inputRowDiv(self::genericDefaultFormMoney($name, $waarde, '', $min, $max)
			, $name, $label, $opmerking);
	}

	protected static function defaultFormUrl($obj, $veld, $include_id = false)
	{
		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		$input = self::genericDefaultFormUrl($name, $waarde, $veld);
		$input->setPlaceholder(self::call('label', $veld, $obj));
		return $input;
	}

	protected static function genericDefaultFormUrl($name, $waarde, $veld) {
		return HtmlInput::makeUrl($name, tryPar($name, $waarde));
	}

	protected static function defaultFormString($obj, $veld, $lang = null, $include_id = false)
	{
		$name = self::formveld($obj, $veld, $lang, $include_id);
		$waarde = self::callobj($obj, 'get', $veld, $lang);

		$input = self::genericDefaultFormString($name, $waarde, $veld);

		if(is_null($lang))
			$input->setPlaceholder(self::call('label', $veld, $obj));
		else
		{
			switch($lang)
			{
			case 'nl':
				$image = new HtmlImage('/Layout/Images/Icons/nederlands.png', 'Nederlands');
				$placeHolder = self::call('label', $veld, $obj) . '-NL';
				break;
			case 'en':
				$image = new HtmlImage('/Layout/Images/Icons/english.png', 'English');
				$placeHolder = self::call('label', $veld, $obj) . '-EN';
				break;
			}

			$input->setPlaceholder($placeHolder);
			$input = new HtmlDiv(array(
				new HtmlDiv($image, 'input-group-addon')
				, $input)
				, 'input-group');
		}

		return $input;
	}

	protected static function genericDefaultFormString($name, $waarde, $veld)
	{
		return HtmlInput::makeText($name, tryPar($name, $waarde), min(60, max(30, strlen($waarde))));
	}

	protected static function makeFormStringRow($name, $waarde, $label, $opmerking = null)
	{
		return self::inputRowDiv(self::genericDefaultFormString($name, $waarde, '')
			, $name, $label, $opmerking);
	}

	protected static function genericDefaultFormPassword($name, $veld)
	{
		return HtmlInput::makePassword($name);
	}

	protected static function makeFormPasswordRow($name, $label, $opmerking = null)
	{
		return self::inputRowDiv(self::genericDefaultFormPassword($name, '')
			, $name, $label, $opmerking);
	}

	protected static function defaultFormText($obj, $veld, $lang = null, $include_id = false)
	{
		$name = self::formveld($obj, $veld, $lang, $include_id);
		$waarde = self::callobj($obj, 'get', $veld, $lang);

		$input = self::genericDefaultFormText($name, $waarde, $veld, $lang);

		if(is_null($lang))
			$input->setPlaceholder(self::call('label', $veld, $obj));
		else
		{
			switch($lang)
			{
			case 'nl':
				$image = new HtmlImage('/Layout/Images/Icons/nederlands.png', 'Nederlands');
				$placeHolder = self::call('label', $veld, $obj) . '-NL';
				break;
			case 'en':
				$image = new HtmlImage('/Layout/Images/Icons/english.png', 'English');
				$placeHolder = self::call('label', $veld, $obj) . '-EN';
				break;
			}

			$input->setPlaceholder($placeHolder);
			$input = new HtmlDiv(array(
				new HtmlDiv($image, 'input-group-addon')
				, $input)
				, 'input-group');
		}

		return $input;
	}

	protected static function genericDefaultFormText($name, $waarde, $veld, $lang=NULL)
	{
		$area = new HtmlTextarea($name);
		$area->add(tryPar($name, $waarde));
		$area->setAttribute('cols', 45);
		$area->setAttribute('rows', 7);
		return $area;
	}

	public static function makeFormTextRow($name, $waarde, $label = null, $opmerking = null)
	{
		return self::inputRowDiv(self::genericDefaultFormText($name, $waarde, '')
			, $name, $label, $opmerking);
	}

	protected static function defaultFormHtml ($obj, $veld, $lang = null, $include_id = false)
	{
		$name = self::formveld($obj, $veld, $lang, $include_id);
		$waarde = self::callobj($obj, 'get', $veld, $lang);

		$input = self::genericDefaultFormHtml($name, $waarde, $veld, $lang);

		if(is_null($lang))
			$input->setPlaceholder(self::call('label', $veld, $obj));
		else
		{
			switch ($lang)
			{
			case 'nl':
				$image = new HtmlImage('/Layout/Images/Icons/nederlands.png', 'Nederlands');
				$placeHolder = self::call('label', $veld, $obj) . '-NL';
				break;
			case 'en':
				$image = new HtmlImage('/Layout/Images/Icons/english.png', 'English');
				$placeHolder = self::call('label', $veld, $obj) . '-EN';
				break;
			}

			$input->setPlaceholder($placeHolder);
			$input = new HtmlDiv(array(
				new HtmlDiv($image, 'input-group-addon')
				, $input)
				, 'input-group');
		}

		return $input;
	}

	protected static function genericDefaultFormHtml($name, $waarde, $veld, $lang=NULL, $kanPromocieVertalen=false) {
		$area = new HtmlTextarea($name, null, $name);
		$area->setAttribute('data-ckeditor', 'true');
		//Voeg alleen kinderen aan de textarea toe als het zin heeft
		if(tryPar($name, $waarde))
			$area->add(tryPar($name, $waarde));
		return $area;
	}

	protected static function makeFormRadiosRow($name, $values, $selected = null)
	{
		$div = new HtmlDiv(null, 'form-group');

		$div->add($idiv = new HtmlDiv(null, 'col-sm-offset-2 col-sm-10'));

		foreach($values as $value => $caption)
		{
			if($selected == $value)
				$idiv->add($i = HtmlInput::makeRadio($name, array($value, $caption), true));
			else
				$idiv->add($i = HtmlInput::makeRadio($name, array($value, $caption)));

			$i->setCaption(null);
			$idiv->add($caption);
		}

		return $div;
	}

	protected static function makeFormFileRow($name, $size = null, $multiple = false, $label = null, $opmerking = null)
	{
		return self::inputRowDiv(HtmlInput::makeFile($name, $size, $multiple), $name, $label, $opmerking);
	}

	/**
	 * Een formulier om meerdere datetime's te selecteren
	 * let op: vanwege koppeling met javascript is het niet mogelijk om een variabele veldnaam te gebruiken
	 */
	protected static function defaultFormDatetimeVerzameling ($obj = null, $veld = null, $datetimes = array())
	{
		$div = new HtmlDiv();

		$div->add(new HtmlParagraph(_("Klik de gewenste data aan in de kalender. Gebruik de volgende tijd voor de afspraken: ")))
			->add($tijd = HtmlInput::makeText('TimeForm_defaultFormDatetime_toevoegen', '17:00', null, null, 'TimeForm_defaultFormDatetime_toevoegen'))
			->add(HtmlInput::makeDateUitgeklapt(null, 'Form_defaultFormDatetime_toevoegen'));
		$tijd->setAttribute('maxlength', 5)
			->setAttribute('size', 5);

		if (count($datetimes) == 0)
		{
			$div->add(new HtmlDiv(new HtmlParagraph(_("Selecteer data via bovenstaande tabel en tijdveld."), 'italic'), null,
				'Form_defaultFormDatetimeVerzameling_select'));
		}
		else
		{
			$div->add(new HtmlDiv($ul = new HtmlList(), null,
				'Form_defaultFormDatetimeVerzameling_select'));
			$ul->SetID('Form_defaultFormDatetimeVerzameling_select_lijst');

			$ret = array();
			foreach ($datetimes as $datetime)
			{
				if(!is_int($datetime))
					$datetime = strtotime($datetime);
				$ret[] = 'Form_defaultFormDatetimeVerzameling_selecteer("' . date('Y-m-d', $datetime) . '",null,null, "' . date('H:i', $datetime) .'");';
			}
			$str = implode(' ', $ret);

			$div->add(HtmlScript::makeJavascript('$(function () {'.$str.'});'));
		}


		Page::getInstance()->setPopup($popup = new HtmlDiv());

		return $div;
	}
	/**
	 * Een formulier om meerdere datetime's te selecteren, synoniem met defaultFormDatetimeVerzameling
	 * let op: vanwege koppeling met javascript is het niet mogelijk om een variabele veldnaam te gebruiken
	 */
	protected static function genericDefaultFormDatetimeVerzameling($naam, $waarde, $veld = null, $datetimes = array()) {
		return self::defaultFormDatetimeVerzameling(NULL, $veld, $datetimes);
	}

	/**
	 * Een formulier om een of meerdere commissies te selecteren
	 */
	static public function defaultFormCommissieVerzameling ($obj, $veld, $selected = array(), $multiple = false, $oud = false)
	{
		$name = self::formveld($obj, $veld);

		$extraOptions = array();
		//Nieuwsberichten van oude commissies mag je ook nog wijzigen als die commissie niet bij 'huidige' hoort
		if($oud == true && $obj instanceof Commissie)
		{
			$extraOptions[$obj->geefID()] = CommissieView::waardeNaam($obj);
		}
		return self::genericDefaultFormCommissieVerzameling($name, NULL, $selected, $multiple, $oud, $extraOptions);
	}
	static public function genericDefaultFormCommissieVerzameling($name, $waarde, $selected=array(), $multiple=false, $oud=false, $extraOptions=array()) {
		if($selected instanceof CommissieVerzameling)
			$selected = $selected->keys();

		if(hasAuth('promocie'))
		{
			$options = CommissieVerzameling::huidige("ALLE")->geefArrayString();
		}
		else
			$options = CommissieVerzameling::vanPersoon(Persoon::getIngelogd())->geefArrayString();

		// merk op dat + op numerieke keys hetzelfde doet als array_merge op niet-numerieke keys
		$sel = HtmlSelectbox::fromArray($name, $options + $extraOptions, $selected);
		if($multiple) {
			$sel->setMultiple();
		} else {
			$sel->setSize(0);
		}

		/*$sel = new HtmlDiv(array(
			new HtmlLabel($name, $multiple ? _('Commissies') : _('Commissie'), 'col-sm-2 control-label')
			, new HtmlDiv($sel, 'col-sm-10'))
		, 'form-group');*/
		return $sel;
	}
	
	static private function persoonVerzamelingFormHasDefaults($defaults)
	{
		return $defaults && $defaults->aantal() > 0;
	}
	
	static private function persoonVerzamelingFormGetDefaults($defaults)
	{
		if (!static::persoonVerzamelingFormHasDefaults($defaults)) {
			$defaults = new PersoonVerzameling();
			// laten we toch maar de kart ondersteunen
			if ($persoon = Persoon::getIngelogd()) {
				$defaults->voegToe($persoon);
			}
			$defaults->union(Kart::geef()->getAllePersonen());
		} else {
			// zorg ervoor dat het wel degelijk een PersoonVerzameling is ipv LidVerzameling
			$defaults = PersoonVerzameling::verzamel($defaults->keys());
		}
		$defaults->sorteer("mijEerst");
		return $defaults;
	}
	
	static private function persoonVerzamelingFormCreateSearchForm($div, $naam, $gemangeldeNaam, $single, $kartbutton)
	{
		$div->add($zoekDivje = new HtmlDiv());

		// zorgt ervoor dat het zoekicoontje rechts in het zoekveld terecht komt
		$zoekDivje->add($zoekerWrapper = new HtmlDiv(null, 'input-group'));
		$zoekerWrapper->add(new HtmlDiv(
			HtmlSpan::fa('search', _('Zoeken'), null, 'text-muted')
			, 'input-group-addon'));

		$zoekerWrapper->add($zoekVak = HtmlInput::makeSearch("persoonZoeker", "", "persoonZoeker", "PersoonVerzamelingZoekVak_".$gemangeldeNaam));
		// zorg ervoor dat we automatisch zoeken als we iets intypen
		// cast single naar int om minder gezeur met string<->boolcasting te hebben
		$zoekVak->setAttribute("oninput", "Form_defaultFormPersoonVerzameling_zoek('" . $naam . "', '" . $gemangeldeNaam . "', " . ((int)$single) . ")");

		if($kartbutton)
		{
			// knop om kart toe te voegen
			// dit is nuttig bijvoorbeeld voor deelnemers in te schrijven als ze bij de vorige aanwezig waren
			$zoekDivje->add($kartKnop = HtmlInput::makeButton("PersoonVerzameling_addKart", _("Laad de kart in"), "Form_defaultFormPersoonVerzameling_laadKart('" . $naam . "', '" . $gemangeldeNaam . "', " . ((int)$single) . ");"));
		}
	}
	
	static private function persoonVerzamelingFormCreateCheckAllBox($div)
	{
		// dit is een checkbox maar heeft geen relevante formulierwerking
		$div->add($selectAll = HtmlInput::makeCheckbox("PersoonVerzameling_selectAll_ignoreMe"));

		// activeer javascript!
		$selectAll->setId("PersoonVerzameling_selectAll");
		$selectAll->setAttribute("onchange", "Form_defaultFormPersoonVerzameling_selectAll();");
		$div->add(new HtmlEmphasis(_("Selecteer allemaal")));
	}
	
	static private function persoonVerzamelingFormFinalize($div, $defaults, $naam, $gemangeldeNaam, $single, $geselecteerd)
	{
		// een nette dropdown waar we alle resultaten ingooien
		$div->add($dropDown = new HtmlDiv());
		$dropDown->addClass("persoonDropdown");
		$dropDown->setId("PersoonVerzamelingDropDown_" . $gemangeldeNaam);

		foreach($defaults as $figuur) {
			$dropDown->add(PersoonView::selectieOptie($naam, $figuur, $single, $geselecteerd));
		}
	}

	/**
	 * Een formulieronderdeel om personen te selecteren
	 * @param defaults Personen die alvast in het lijstje staan (worden aangevuld met je kart)
	 * @param naam Naam van de checkboxen in het formulier (default: "PersoonVerzameling")
	 * @param single Willen we precies een lid terughebben? (default: nope)
	 * @param geselecteerd Selecteer alvast iedereen die in de defaults staat (default: false)
	 * @param zoekbaar Voeg een zoekvakje toe aan dit formulier (default: wel)
	 * @return Een divje met het gewenste formulier
	 */
	static public function defaultFormPersoonVerzameling ($defaults = null, $naam = null, $single = false, $geselecteerd = false, $zoekbaar = true) 
	{
		if (!$naam) $naam = "PersoonVerzameling";

		// mangel de naam zodat het een respectabele selector kan zijn
		$gemangeldeNaam = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $naam);

		if(!static::persoonVerzamelingFormHasDefaults($defaults))
			$geselecteerd = false;
			
		$defaults = static::persoonVerzamelingFormGetDefaults($defaults);

		$div = new HtmlDiv();

		// formuliertje waar alle AJAX vandaan komt
		if($zoekbaar)
			static::persoonVerzamelingFormCreateSearchForm($div, $naam, $gemangeldeNaam, $single, true);

		// laat mensen alle resultaten tegelijk aanvinken
		// (bijvoorbeeld cies in IOU, kartelementen)
		if (!$single)
			static::persoonVerzamelingFormCreateCheckAllBox($div);

		static::persoonVerzamelingFormFinalize($div, $defaults, $naam, $gemangeldeNaam, $single, $geselecteerd);

		return $div;
	}

	//@}

	/**
	 *  Schrijf een regel naar een CSV-bestand.
	 * Je kan aangeven welke velden in welke volgorde weggeschreven moeten worden.
	 * @param regel Een array key=>value waarvan sommige velden weggeschreven moeten worden.
	 * @param velden Een array met alle velden die weggeschreven moeten worden, in de juiste volgorde.
	 * @param file De handle van de file waarin alle velden terecht moeten komen.
	 * @return void
	 */
	static public function schrijfCSVRegel($regel, $velden, $file) {
		$output = array();
		foreach ($velden as $veld) {
			$output[] = $regel[$veld];
		}
		fputcsv($file, $output);
	}

	/**
	 * @brief Geef een zinnige stringvormige weergave van het object.
	 *
	 * Roept $classView::defaultWaarde$class($obj) aan voor DBObjecten.
	 * Voor DateTime-objecten geeft het de datum weer.
	 * Anders wordt de ingebouwde toString aangeroepen.
	 *
	 * @sa maybeDateTimeToString.
	 *
	 * @return Een string die het object weergeeft.
	 */
	static public function maakString($obj) {
		if ($obj instanceof DateTime) {
			// Support de plekken waar maybeDateTimeToString gebruikt werden.
			return $obj->format('Y-m-d');
		} elseif ($obj instanceof DBObject) {
			// Voor DBObjecten gebruiken we een defaultWaarde die erbij past.
			// Die staat in een of andere Viewklasse, dus hier gebeurt wat magie.

			// Zoek op welke Viewklasse hierbij hoort.
			$class = get_class($obj);
			$classView = $class . 'View';
			// Zoek op welke defaultWaardemethode hierbij hoort.
			$defaultWaardeClass = 'defaultWaarde' . $class;
			// Roep de juiste methode aan.
			return call_user_func(array($classView, $defaultWaardeClass), $obj);
		} else {
			return (string) $obj;
		}
	}

	/**
	 * @brief Maak een HTMLPage met wijzig- en verwijderknoppen.
	 *
	 * Maakt gebruik van `DBObject::wijzigURL` en `DBObject::verwijderURL`
	 * voor de wijzig- en verwijderlinks.
	 *
	 * @param obj Het object om te tonen.
	 * @param page De pagina om de knoppen op te zetten. Default: nieuwe pagina.
	 *
	 * @return Een HTMLPage met verwijder- en wijzigknoppen.
	 */
	static public function pageLinks(DBObject $obj, $page = null)
	{
		if (is_null($page))
		{
			$page = new HTMLPage();
		}

		if(!$obj->magBekijken())
		{
			return $page;
		}

		// Laatste aanpassing/aanpasser.
		$page->setLastMod($obj);

		// Wijzig- en verwijderknoppen.
		if ($obj->magWijzigen())
		{
			try {
				$page->setEditUrl($obj->wijzigURL());
			} catch (HeeftGeenURLException $e) { /* zet geen link neer */ }
		}
		if ($obj->magVerwijderen())
		{
			try {
				$page->setDeleteUrl($obj->verwijderURL());
			} catch (HeeftGeenURLException $e) { /* zet geen link neer */ }
		}

		return $page;
	}

	/**
	 * Maak een HTMLPagina om dit object te wijzigen.
	 *
	 * @param DBObject $obj Het object om te tonen.
	 * @param string $formnaam De naam van het wijzigform, voor Token::processNamedForm().
	 * @param bool $show_error Of fout ingevulde velden een foutmelding krijgen.
	 *
	 * @return HTMLPage
	 */
	public static function wijzigPagina(DBObject $obj, $formnaam, $show_error = false)
	{
		$page = static::pageLinks($obj);
		$page->start();
		$page->add($form = HtmlForm::named($formnaam));
		$form->add(static::createForm($obj, $show_error));
		$form->add(HtmlInput::makeSubmitButton(_("Wijzigen!")));
		return $page;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
