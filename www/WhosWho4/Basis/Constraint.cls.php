<?
class Constraint
	extends Constraint_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($rule = null, $throw = true)
	{
		parent::__construct(); // Constraint_Generated

		if (is_array($rule))
		{
			if (!isset($rule['veld']) || !isset($rule['waarde']) || !isset($rule['operatie']))
				if ($throw)
					throw new Exception('Constraint niet goed geformuleerd', 400);
				else
					user_error('Constraint niet goed geformuleerd', E_USER_ERROR);

			$this->setVeld($rule['veld'], $throw);
			$this->setWaarde($rule['waarde'], $throw);
			$this->setOperatie($rule['operatie'], $throw);
		}
	}

	public function setVeld ($veld, $throw = true)
	{
		if (strpbrk($veld, " `'\""))
			if ($throw)
				throw new Exception('Constraintveld niet goed geformuleerd', 400);
			else
				user_error('Constraintveld niet goed geformuleer', E_USER_ERROR);

		$this->veld = $veld;
	}

	public function setWaarde ($waarde, $throw = true)
	{
		$this->waarde = $waarde;
	}

	public function setOperatie ($operatie, $throw = true)
	{
		if (!in_array($operatie, array('=', '<', '>', '<=', '>=', 'LIKE')))
			if ($throw)
				throw new Exception('Constraintoperatie niet goed geformuleerd', 400);
			else
				user_error('Constraintoperatie niet goed geformuleer', E_USER_ERROR);

		$this->operatie = $operatie;
	}

	public function sqlWhere ()
	{
		return '`'.$this->getVeld().'` '.$this->getOperatie().' %s';
	}
}
