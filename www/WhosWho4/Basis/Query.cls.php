<?
class Query
	extends Query_Generated
{
	private $result; /** Hierin wordt het resultaat van de query opgeslagen **/

	private $isFalse = 0;

	/*** CONSTRUCTOR ***/
	public function __construct($objects = array(), $selects = array())
	{
		parent::__construct(); // Query_Generated

		$this->objects = $objects;
		$this->selects = $selects;
	}

	/**
	 * Hierin slaan we de tables op als we gaan joinen om ervoor te zorgen dat
	 * we niet gaan joined met tables die nog moeten komen
	 */
	private $alreadyJoined = array();

	public function setFalse()
	{
		$this->isFalse = 1;
		return $this;
	}

	/**
	 *  Maakt een sql-query-string van zichzelf
	 *
	 * @return $this
	 */
	public function buildQuery()
	{
		// Een sql-string mag niet niet vaker gemaakt worden
		if(!empty($this->sql))
			user_error('Deze Query is al een keer gebouwd en mag niet nog een '
				. 'keer gebouwd worden', E_USER_ERROR);

		// Er moet wel een FROM bij de query kunnen
		if(!$this->statementsHaveKey('tables'))
			user_error('Geen tables geset', E_USER_ERROR);

		$fetchType = $this->fetchType;
		if($this->statementsHaveKey('extraSelects'))
		{
			switch($fetchType)
			{
			case 'COLUMN':
				$fetchType = 'TABLE';
				break;
			case 'MAYBEVALUE':
				$fetchType = 'MAYBETUPLE';
				break;
			default:
				break;
			}
		}

		// Maak een string van de tables om te gebruiken in de FROM
		$tables = $this->buildTables();

		// Als er geen selects zijn, selecteren we gewoon alles
		if(!$this->statementsHaveKey('selects'))
			$this->statements['selects'][] = '*';

		// Maak een string van de selects om te gebruiken in de SELECT
		$selects = self::arrayStr($this->addTablesToFields($this->statements['selects']), ',');

		// Filter de extraselects die we al in de selects hebben
		if($this->statementsHaveKey('extraSelects'))
		{
			$extraSelects = $this->statements['extraSelects'];
			if($extraSelects)
			{
				foreach($extraSelects as $key => $extraSelect)
				{
					// Omdat elementen van selects ook een array kunnen zijn en we de key
					// moeten vergelijken, doen we hier een foreach ipv in_array
					foreach($this->statements['selects'] as $select)
					{
						if(is_array($select))
						{
							$field = key($select);
							if($field == $extraSelect)
								unset($this->statements['extraSelects'][$key]);
						}
						else if($select == $extraSelect)
							unset($this->statements['extraSelects'][$key]);
					}
				}
			}
		}

		$extraSelects = ($this->statementsHaveKey('extraSelects')
				&& count($this->statements['extraSelects']) != 0)
			? self::arrayStr($this->addTablesToFields($this->statements['extraSelects']), ',')
			: '';

		$selects = empty($extraSelects) ? $selects : $selects . ', ' . $extraSelects;

		// Bouw de WHERE-clausule
		if($this->isFalse)
		{
			$whereCriteria = 'WHERE %i = %i';
			$whereBindings = array(0, 1);
		}
		else
			list($whereCriteria, $whereBindings) = $this->buildCriteriaWithType('wheres', 'WHERE');

		// Bouw de HAVING-clausule
		list($havingCriteria, $havingBindings) = $this->buildCriteriaWithType('havings', 'HAVING');

		// Maak de GROUP BY-string
		$groupBys = ($this->statementsHaveKey('groupBys')
			? 'GROUP BY ' . self::arrayStr(
				$this->addTablesToFields($this->statements['groupBys'])
				, ', ')
			: '');

		// Maak een string met alle ORDER BY's
		$orderBys = '';
		if($this->statementsHaveKey('orderBys'))
		{
			foreach($this->statements['orderBys'] as $orderBy)
				$orderBys .= sprintf('%s %s, '
					, self::sanitize($this->addTablesToFields($orderBy['field']))
					, $orderBy['direction']);

			if($orderBys = trim($orderBys, ', '))
				$orderBys = 'ORDER BY ' . $orderBys;
		}

		// Maak de join-string
		$joins = $this->buildJoins();

		// Maak de limit-string
		$limit = ($this->statementsHaveKey('limit')
			? 'LIMIT ' . $this->statements['limit']
			: '');
		// Maak de offset-string
		$offset = ($this->statementsHaveKey('offset')
			? 'OFFSET ' . $this->statements['offset']
			: '');

		// Voeg alle string-stukken samen in een array
		$sqlArray = array(
			$fetchType,
			'SELECT' . (isset($this->statements['distinct']) ? ' DISTINCT' : ''),
			$selects,
			'FROM',
			$tables,
			$joins,
			$whereCriteria,
			$groupBys,
			$havingCriteria,
			$orderBys,
			$limit,
			$offset
		);

		// Voeg de bindings samen toe in een array
		$bindings = array_merge(
			$whereBindings,
			$havingBindings
		);

		// Maak de sql-string
		$this->sql = self::concatenateQuery($sqlArray);
		$this->arguments = $bindings;
		return $this;
	}

	/**
	 *  Voert de Query uit door eerst de sql-string op te bouwen en deze
	 *  daarna aan $WSW4DB->q() door te spelen samen met de bindings
	 *
	 * @return Het resultaat van de query
	 */
	public function get()
	{
		global $WSW4DB;

		$cachetm = null;

		$cachetm = new ProfilerTimerMark('Query::get()');
		Profiler::getSingleton()->addTimerMark($cachetm);

		if(!is_null($this->result))
			return $this->result;

		if(empty($sql))
			$this->buildQuery();

		$params = array_merge(array($this->sql), $this->arguments);

		$res = call_user_func_array(array($WSW4DB, 'q'), $params);

		if($this->statementsHaveKey('extraSelects'))
		{
			$extraSelects = $this->statements['extraSelects'];

			switch($this->fetchType)
			{
			case 'TABLE':
			case 'MAYBETUPLE':
				foreach($res as $key => $row)
					$res[$key] = array_slice($row, 0, sizeof($row) - 2*sizeof($extraSelects));
				break;
			case 'COLUMN':
				foreach($res as $key => $row)
					$res[$key] = $row[0];
				break;
			case 'MAYBEVALUE':
				$res = $res[0];
				break;
			}
		}

		$this->result = $res;

		if($cachetm)
			$cachetm->markEnd();

		return $res;
	}

	/**
	 *  Zet het fetch-type van de query, ie MAYBEVALUE, TABLE etc
	 *
	 * @param type Het nieuwe type
	 *
	 * @return $this
	 */
	public function setFetchType($type)
	{
		if(!in_array($type, self::enumsFetchType()))
		{
			user_error($type . ' is geen geldige fetch-type', E_USER_ERROR);
		}

		$this->fetchType = $type;

		return $this;
	}

	/**
	 *  Geeft het eerste resultaat van de query terug als dat bestaat,
	 *  anders geeft de methode het resultaat terug
	 *
	 * @return Het eerste resultaat
	 */
	public function first()
	{
		if(is_null($this->result))
			$this->get();

		if(is_array($this->result))
			return reset($this->result);

		return $this->result;
	}

	/**
	 *  Voert een aggregator op met de huidige query. Hiervoor worden alle
	 *  huidige selects weggehaald en de aggregator daarvoor in de plaats gezet
	 *
	 * @param aggregator De aggregator om uit te voeren
	 * @param field Welk(e) veld(en) op geaggregeerd wordt. Default: '*'.
	 *
	 * @return Het resultaat
	 */
	public function aggregate($aggregator, $field = '*')
	{
		if(!in_array(strtoupper($aggregator), self::$allowedAggregators))
			user_error('Aggregator ' . $aggregator . ' niet toegstaan', E_USER_ERROR);

		$this->result = null;
		$this->sql = null;

		$this->setFetchType('MAYBEVALUE');
		$this->removeSelects();

		// TODO: Hier nog een check toevoegen om te kijken of het field wel
		// in een van de tabellen staat
		$this->select($aggregator . "($field)");

		return $this->get();
	}

	/**
	 *  Telt het aantal resultaten wat met de huidige query gevonden wordt
	 *  Hiervoor zijn dus geen limits, offsets of orderBys nodig
	 *
	 * @return Het aantal resultaten
	 */
	public function count()
	{
		unset($this->statements['orderBys']);
		unset($this->statements['limit']);
		unset($this->statements['offset']);

		return $this->aggregate('COUNT');
	}

	public function sum($field)
	{
		unset($this->statements['orderBys']);
		unset($this->statements['limit']);
		unset($this->statements['offset']);

		return $this->aggregate('SUM', $field);
	}

	/**
	 * Stel de query in om een verzameling te kunnen krijgen.
	 *
	 * Hierna kun je Verzameling::verzamel($this->get()) aanroepen.
	 *
	 * @param string $object
	 * De objectnaam om de verzameling van te krijgen
	 * @param bool $distinct
	 * Zo ja, doen we ->selectDistinct ipv ->select om de keys te krijgen
	 *
	 * @return string[]
	 * De primary keys voor het verzamelen.
	 */
	private function prepareVerzamel($object, $distinct = false)
	{
		$this->removeSelects();
		if($this->statementsHaveKey('distinct'))
			unset($this->statements['distinct']);

		$canUse = false;
		foreach($this->objects as $class)
		{
			if($class === $object || is_subclass_of($object, $class))
				$canUse = true;
		}
		if(!$canUse)
			user_error($object . ' hoort niet bij de tabellen van de query', E_USER_ERROR);

		$queryClass = $object . 'Query';
		$primaries = $queryClass::getPrimaries();

		// We doen hier een call_user_func_array omdat we de array van primaries
		// als argumenten willen stoppen in $this->select()
		$selectFunc = 'select' . ($distinct ? 'Distinct' : '');
		call_user_func_array(array($this, $selectFunc), $primaries);

		if(sizeof($primaries) == 1)
		{
			$this->setFetchType('COLUMN');
		}
		else
		{
			$this->setFetchType('TABLE');
		}

		return $primaries;
	}

	/**
	 * Maakt een verzameling die verkregen is door de query uit te voeren
	 *
	 * @param string $object
	 * De objectnaam om de verzameling van te krijgen
	 * @param bool $distinct
	 * Zo ja, doen we ->selectDistinct ipv ->select om de keys te krijgen
	 *
	 * @return Verzameling
	 */
	public function verzamel($object, $distinct = false)
	{
		$primaries = $this->prepareVerzamel($object, $distinct);
		$ids = $this->get();

		// Als we een TABLE-select doen geet de query 2x te veel resultaten terug
		// Die moeten we er dus nog uit filteren
		if(sizeof($primaries) > 1)
		{
			foreach($ids as $key => $id)
			{
				foreach($ids[$key] as $k => $i)
				{
					if(!is_numeric($k))
						unset($ids[$key][$k]);
				}
			}
		}

		$verzameling = $object . 'Verzameling';

		return $verzameling::verzamel($ids);
	}

	/**
	 * Maakt een verzameling die verkregen is door de query uit te voeren
	 *
	 * @param string $object
	 * De objectnaam om de verzameling van te krijgen
	 * @param bool $distinct
	 * Zo ja, doen we ->selectDistinct ipv ->select om de keys te krijgen
	 *
	 * @return Generator
	 */
	public function verzamelIter($object, $distinct = false)
	{
		$this->prepareVerzamel($object, $distinct);
		$this->setFetchType('CURSOR');

		//$this->buildQuery();
		//var_dump($this->sql); die();

		$idCursor = $this->get();
		foreach ($idCursor as $row)
		{
			// pdo geeft een row weer als key-value associatieve array,
			// maar de geef-methode lust alleen numerieke keys,
			// dus we moeten even vertalen hier.
			$ids = [];
			$i = 0;
			foreach ($row as $k => $v)
			{
				if (!is_numeric($k))
				{
					$ids[$i] = $v;
				}
			}

			$item = $object::geef($ids);
			yield $item;
		}
	}

	public function geef($object)
	{
		$this->removeSelects();
		if($this->statementsHaveKey('distinct'))
			unset($this->statements['distinct']);

		$canUse = false;
		foreach($this->objects as $class)
		{
			if($class === $object || is_subclass_of($object, $class))
				$canUse = true;
		}
		if(!$canUse)
			user_error($object . ' hoort niet bij de tabellen van de query', E_USER_ERROR);

		$queryClass = $object . 'Query';
		$primaries = $queryClass::getPrimaries();

		// We doen hier een call_user_func_array omdat we de array van primaries
		// als argumenten willen stoppen in $this->select()
		$selectFunc = 'select';
		call_user_func_array(array($this, $selectFunc), $primaries);

		if(sizeof($primaries) == 1)
			$this->setFetchType('MAYBEVALUE');
		else
			$this->setFetchType('MAYBETUPLE');

		$ids = $this->get();

		if(is_null($ids))
			return NULL;

		// Als we een TUPLE-select doen geet de query 2x te veel resultaten terug
		// Die moeten we er dus nog uit filteren
		if(sizeof($primaries) > 1)
		{
			foreach($ids as $key => $id)
			{
				if(!is_numeric($key))
					unset($ids[$key]);
			}
		}

		return $object::geef($ids);
	}

	/**
	 *  Trimt alle strings in een array en plakt ze aan elkaar met een
	 *  whitespaces als delimiter
	 *
	 * @param pieces De array van strings
	 *
	 * @return Het resultaat als string
	 */
	static private function concatenateQuery(array $pieces)
	{
		$str = '';
		foreach ($pieces as $piece) {
			$str = trim($str) . ' ' . trim($piece);
		}
		return trim($str);
	}

	/**
	 *  Bouwt de tabellen voor in de FROM
	 *
	 * @return Een string met komma-gescheiden ge-escapte tabellen met alias
	 */
	private function buildTables()
	{
		$strArray = array();

		foreach($this->statements['tables'] as $table)
		{
			// We mogen er vanuit gaan dat elke table met een key in objects staat
			if(!$key = array_search($table, $this->objects))
				user_error($table . ' staat niet in de objects van deze query! ERROR!', E_USER_ERROR);

			// Voeg ook de alias toe
			$strArr[] = self::sanitize($table) . ' AS ' . self::sanitize($key);
			$this->alreadyJoined[] = $table;
		}

		return implode(',', $strArr);
	}

	/**
	 *  Maakt een criteria-sql-string met de bijbehorende bindings erbij.
	 *  Hiermee wordt oa de where-clause tot sql-string gemaakt
	 *
	 * @param key De statement om de string van te bouwen, bv 'where'
	 * @param type De sql-string van het type wat we bouwen, bv 'WHERE'
	 *
	 * @return Een array met als eerste waarde de sql-string en als tweede waarde
	 *  een array van waardes die gebind moeten worden bij het uitvoeren van de
	 *  sql-string
	 */
	private function buildCriteriaWithType($key, $type = '')
	{
		$criteria = '';
		$bindings = array();

		if(isset($this->statements[$key]))
		{
			list($criteria, $bindings) = $this->buildCriteria($key);

			if($criteria)
				$criteria = $type . ' ' . $criteria;
		}

		return array($criteria, $bindings);
	}

	/**
	 *  Geeft bij een type het placeholder-character terug zodat die te
	 *  gebruiken is in een sql-string
	 *
	 * @param type Het type als string
	 *
	 * @return Het placeholder-character
	 */
	static private function getPlaceholder($type)
	{
		switch($type)
		{
		case 'int':
			return 'i';
		case 'string':
			return 's';
		case 'float':
			return 'f';
		case 'literal':
			return 'l';
		case 'function':
			return 'F';
		case 'quoteString':
			return 'c';
		case 'nothing':
			return '_';
		default:
			user_error('Type ' . $type . ' bestaat niet!', E_USER_ERROR);
			break;
		}
	}

	/**
	 *  Maakt een criteria-string horend bij de statement $keyName met bindings
	 *
	 * @param keyName De naam van de statement als string
	 *
	 * @return Een array van een criteria-string en een array met binding values
	 */
	private function buildCriteria($keyName)
	{
		$statements = $this->statements[$keyName];
		$criteria = '';
		$bindings = array();

		foreach ($statements as $statement) {
			// $key is hier de gesanitizede kolom om iets mee te doen
			$key = self::sanitize($this->addTablesToFields($statement['key']));
			$value = $statement['value'];

			if (is_null($value) && $key instanceof Closure)
			{
				// We hebben te maken met een Closure, oftewel een subclausule
				// Die handelen we af door een nieuwe query te maken, de
				// Closure daarop uit te voeren en vervolgens daarvan de criteria-string
				// te maken
				$query = new Query($this->objects, $this->selects);

				// Voer de Closure uit
				$key($query);

				// Maak criteria-string, laat het tweede argument leeg, we willen
				// niet bv nog een WHERE in de subclausule
				list($whereCriteria, $whereBindings) = $query->buildCriteriaWithType($keyName);

				// Voeg alles toe aan de huidige criteria en bindings
				$bindings = array_merge($bindings, $whereBindings);
				$criteria .= sprintf('%s (%s) '
								, $statement['joiner']
								, trim($whereCriteria));
			}
			else if(is_array($statement['type']))
			{
				// Het type is een array, dus we hebben te maken met een WHERE IN
				// en dus is de value die erbij hoort een multidimensionale array
				$valuePlaceholder = '(%A{';

				foreach($statement['type'] as $type)
				{
					$valuePlaceholder .= self::getPlaceholder($type);
				}

				$valuePlaceholder .= '})';

				$bindings[] = $value;
				$criteria .= sprintf('%s %s %s %s '
									, $statement['joiner']
									, $key
									, $statement['operator']
									, $valuePlaceholder);
			}
			elseif (is_array($value))
			{
				// De value is een array, maar het type is dat niet, dus we hebben
				// een 1-dimensionale array als values
				$bindings[] = $value;
				$criteria .= sprintf('%s %s %s (%%A%s) '
									, $statement['joiner']
									, $key
									, $statement['operator']
									, self::getPlaceholder($statement['type']));
			}
			else if(is_null($value))
			{
				// Dit betekent dat we een NULL als operator hebben
				$criteria .= sprintf('%s %s %s '
									, $statement['joiner']
									, $key
									, $statement['operator']);
			}
			else
			{
				// Handel de rest van wheres af
				$bindings[] = $value;
				$criteria .= sprintf('%s %s %s %%%s '
									, $statement['joiner']
									, $key
									, $statement['operator']
									, self::getPlaceholder($statement['type']));
			}
		}

		// Haal alle AND's, OR's en whitespaces weg van het begin van de string
		// en haal alle whitespaces aan het eind weg.
		$criteria = preg_replace('/^(\s?AND ?|\s?OR ?)|\s$/i', '', $criteria);

		return array($criteria, $bindings);
	}

	/**
	 *  Maakt een JOIN-string voor in de query van de huidige join-statement
	 *
	 * @param addJoinStr Een bool of de string JOIN ... ON moet worden toegevoegd.
	 *  Voor bv meerdere join-criteria moet dit niet voor ieder criterium gebruikt worden
	 *
	 * @return Het join-gedeelte als sql-string voor in de query
	 */
	private function buildJoins($addJoinStr = true)
	{
		$criteria = '';
		$joinOnStr = '';

		if($this->statementsHaveKey('joins'))
		{
			// Als eerste groeperen we alle joins op dezelfde table bij elkaar
			// zodat we goed alle join-criterium per tabel bij elkaar kunnen stoppen
			$joins = array();

			foreach($this->statements['joins'] as $join)
			{
				// De key is hier de key van de objects, dus zo wordt de table
				// ge-aliast in de query
				$key = $join['table'];
				if(!array_key_exists($key, $joins))
				{
					$joins[$key] = array();
					$joins[$key]['type'] = $join['type'];
					$joins[$key]['ids'] = array();
				}

				$joins[$key]['ids'][] =
					array('field1' => $join['field1'], 'field2' => $join['field2']);
			}

			foreach($joins as $key => $join)
			{
				// Haal de echte tabelnaam op bij de key
				$table = $this->objects[$key];

				// Als we de JOIN ... ON moeteen toevoegen, doe dat dan met alias
				if($addJoinStr)
					$joinOnStr = sprintf('%s %s ON '
										, $join['type']
										, self::sanitize($table) . ' AS ' . self::sanitize($key));
				else // Als we geen addJoinStr hebben zitten we in een closure
					$joinOnStr = 'AND ';

				$criteriaPart = '';

				foreach($join['ids'] as $ids)
				{
					$field1 = $ids['field1'];
					$field2 = $ids['field2'];

					$this->alreadyJoined[] = $table;

					if(is_null($field2) && $field1 instanceof Closure)
					{
						// We hebben te maken met een Closure, dus meerdere
						// join-criteria. We maken hier een nieuwe Query aan
						// en voeren daar de Closure op uit om vervolgens er een
						// join-string van te maken
						$query = new Query($this->objects, $this->selects);

						// Voer de Closure uit en maak de JOIN-string
						$field1($query);
						$res = $query->buildJoins(false);

						$criteriaPart .= sprintf('AND (%s) ', $res);
					}
					else if(is_null($field2) || is_null($field1))
					{
						// field1 en field2 mogen niet leeg zijn!
						user_error('field1 of field2 is NULL', E_USER_ERROR);
					}
					else
					{
						$criteriaPart .= sprintf('AND %s = %s '
							, self::sanitize(self::addTablesToFields($field1))
							, self::sanitize(self::addTablesToFields($field2)));
					}
				}

				// Haal alle AND's, OR's en whitespaces weg van het begin van de string
				// en haal alle whitespaces aan het eind weg.
				$criteriaPart = preg_replace('/^(\s?AND ?|\s?OR ?)|\s$/i', '', $criteriaPart);
				$criteria .= ' ' . $joinOnStr . '(' . $criteriaPart . ')';
			}
		}

		// Haal alle AND's, OR's en whitespaces weg van het begin van de string
		// en haal alle whitespaces aan het eind weg.
		$criteria = preg_replace('/^(\s?AND ?|\s?OR ?)|\s$/i', '', $criteria);

		return $criteria;
	}

	/**
	 *  Sanitizet een array van strings en plakt ze vervolgens aan elkaar
	 *
	 * @param pieces De array van strings
	 * @param glue De 'lijm' om alles aan elkaar te plakken als string
	 * @param sanitize Bool of er gesanitized moet worden
	 *
	 * @return De getrimde en gelijmde string
	 */
	static private function arrayStr(array $pieces, $glue, $sanitize = true)
	{
		$str = '';

		foreach($pieces as $key => $piece)
		{
			if($sanitize)
				$piece = self::sanitize($piece);

			$str .= $piece . $glue;
		}

		return trim($str, $glue);
	}

	/**
	 *  Checkt of een string begint met een aggregator er returnt die aggregator
	 *
	 * @param field De string om op te checken
	 *
	 * @return Een aggregator als die gevonden is, anders een lege string
	 */
	static private function checkForAggregator($field)
	{
		$aggregator = '';

		if($pos = strpos($field, '('))
		{
			// We hebben te maken met een aggregator dus het moet met een ')' worden
			// afgesloten of met ') AS huppeldepup
			if(strpos($field, ')') !== strlen($field)-1
					&& !strpos($field, ') AS '))
				user_error('Er ontbreekt een \')\' aan het einde van ' . $field
					. ' of de AS is verkeerd', E_USER_ERROR);

			$aggregator = substr($field, 0, $pos);

			if(!in_array(strtoupper($aggregator), self::$allowedAggregators))
				user_error('Aggregator ' . $aggregator . ' niet toegestaan', E_USER_ERROR);
		}
		else if(strpos($field, ')'))
		{
			user_error('Er ontbreekt een \'(\' in ' . $field, E_USER_ERROR);
		}

		return $aggregator;
	}

	/**
	 *  Voegt backticks to bij een string. Als een string bestaat uit een
	 *  tabel-naam.kolom worden er om zowel de tabel-naam als de kolom backticks
	 *  gezet. Als een string al backticks heeft wordt er niets mee gedaan
	 *
	 * @param str De string
	 *
	 * @return De string met backticks.
	 */
	static private function sanitize($str)
	{
		if($str instanceof Closure)
			return $str;

		$als = null;
		$aggregator = self::checkForAggregator($str);
		$aggregatorPrefix = "";
		if(!empty($aggregator)) {
			if(strpos($str, ') AS ')) {
				$expl = explode(')', $str);
				$str = substr($expl[0], strlen($aggregator) + 1);
				$als = substr($expl[1], 4);
			} else {
				$str = substr($str, strlen($aggregator) + 1, strlen($str) - strlen($aggregator) - 2);
			}
			if (substr($str, 0, 9) === "DISTINCT ") {
				$aggregatorPrefix = "DISTINCT ";
				$str = substr($str, 9);
			}
		}

		$strArr = explode('.', $str, 2);

		foreach($strArr as $key => $subValue)
		{
			// Als de string al backticks heeft slaan we deze over
			if(substr($subValue, 0, 1) == '`' && substr($subValue, -1) == '`')
				continue;

			if(trim($subValue) == '*') {
				$strArr[$key] = '*';
				continue;
			}

			$strArr[$key] = '`' . $subValue . '`';
		}

		if (empty($aggregator)) {
			$str = implode('.', $strArr);
		} else {
			$str = $aggregator . '(' . $aggregatorPrefix . implode('.', $strArr) . ')';
		}
		if (!is_null($als))
			$str .= ' AS `' . $als . '`';
		return $str;
	}

	/**
	 *  Voegt tabel-namen toe aan een array van kolommen. De tabelnamen worden
	 *  gehaald uit de huidige objects. Als het niet kan wordt er een array gegooid
	 *
	 * @param fields De kolommen, is een array van kolommen of kan 1 enkele kolom zijn
	 *
	 * @return De kolommen met tabel-namen ervoor
	 */
	private function addTablesToFields($fields)
	{
		// Met een Closure kunnen we weinig, dus die slaan we over
		if($fields instanceof Closure)
			return $fields;

		$isArray = true;
		if(!is_array($fields))
		{
			$fields = array($fields);
			$isArray = false;
		}

		foreach($fields as $id => $field)
		{
			$als = null;

			if(is_array($field))
			{
				$als = key($field);
				$field = current($field);
			}

			$field = trim($field);

			$aggregator = self::checkForAggregator($field);
			$aggregatorPrefix = "";
			if(!empty($aggregator)) {
				$field = substr($field, strlen($aggregator)+1, strlen($field)-strlen($aggregator)-2);
				if (substr($field, 0, 9) === "DISTINCT ") {
					$aggregatorPrefix = "DISTINCT ";
					// do not update the aggregate field now...
					$field = substr($field, 9);
				}
			}

			// Een wildcard slaan we over, we gebruiken dan toch alles
			if($field == '*') {
				if (!empty($aggregator))
					$fields[$id] = sprintf('%s(%s*)', $aggregator, $aggregatorPrefix);
				else
					$fields[$id] = $aggregatorPrefix . '*';
				if (!is_null($als))
					$fields[$id] .= ' AS ' . $als;
				continue;
			}

			$strArr = explode('.', $field, 2);
			if(sizeof($strArr) == 2) {
				// We mogen er hier van uitgaan dat de strArr[0] óf een key (dus alias)
				// uit objects is, óf een tabelnaam
				if(!$key = array_search($strArr[0], $this->objects)) {
					if(!array_key_exists($strArr[0], $this->objects)) {
						user_error($strArr[0] . ' komt niet voor in de huidige tables van deze query', E_USER_ERROR);
					} else {
						// strArr[0] is dus een key
						$key = $strArr[0];
						$table = $this->objects[$key];
						$objectQuery = $table . 'Query';
					}
				} else {
					// strArr[0] is een tabelnaam
					$table = $strArr[0];
				}

				$objectQuery = $table . 'Query';
				// Als de kolom al een tabel-naam ervoor had staan checken we of
				// dat toegestaan is
				if(!($ret = $objectQuery::checkField($strArr[1])))
					user_error($strArr[1] . ' komt niet voor in ' . $table, E_USER_ERROR);

				// Voeg voor de zekerheid de juiste alias toe aan het veld
				$strArr[0] = array_search($ret, $this->objects);
				if(!empty($aggregator)) {
					$fields[$id] = sprintf('%s(%s%s)', $aggregator, $aggregatorPrefix, implode('.', $strArr));
				} else {
					$fields[$id] = $aggregatorPrefix . implode('.', $strArr);
				}

				if(!is_null($als))
					$fields[$id] .= ' AS ' . $als;

				continue;
			}

			$tableNames = array();
			// Voor alle tabellen kijken we of de kolom in die tabel voor komt.
			// Als het in meerdere tabellen voorkomt weten we niet wat we moeten
			// doen dus gooien we een error
			foreach($this->objects as $object) {
				$objectQuery = $object . 'Query';
				if($ret = $objectQuery::checkField($field))
					$tableNames[] = $ret;
			}

			// Het zou ook mogelijk zijn dat we met een AS keyword aan het werken zijn
			$select_keys = array();
			foreach($this->selects as $select) {
				if(!is_array($select))
					continue;

				$key = key($select);
				if($key == $field)
					$select_keys[] = '"AS ' . $key . '"';
			}

			if(count($select_keys) != 0)
				$tableNames += $select_keys;

			if(sizeof($tableNames) > 1) {
				$ok = false;

				foreach($tableNames as $tableName) {
					$tableQuery = $tableName . 'Query';
					$equivTables = $tableQuery::getEquivalentTables();
					if($equivTables === array_intersect($equivTables, $tableNames) && $tableNames === array_intersect($tableNames, $equivTables)) {
						$ok = true;
						break;
					}
				}

				// We willen alle kiezen uit tablenames die al voorgekomen zijn,
				// geen tablenames die nog gejoined moeten worden
				$tableNames = array_intersect($tableNames, $this->alreadyJoined);
				if(!$ok) {
					user_error($field . ' komt in meerdere van de tabellen '
						. implode(',', $this->objects) . ' voor', E_USER_ERROR);
				} else
					$tableNames = array(reset($tableNames));
			}

			if(sizeof($tableNames) == 0)
				user_error($field . ' komt in geen van de tabellen '
					. implode(',', $this->objects) . ' voor en ook niet als een "AS" bij de select'
					, E_USER_ERROR);

			$columnName = $field;
			// In het geval van een AS keyword moeten we er geen tablename voorzetten
			if(count($select_keys) == 0) {
				// Haal de eerste tabelnaam uit tableNames, het maakt toch niet uit
				// welke want hij komt kennelijk in allemaal voor
				$tableNames = reset($tableNames);

				// We mogen er hier vanuit gaan dat de tableNames een geldige tabelNaam
				// is uit objects dus we halen de alias ervan op
				$tableNames = array_search($tableNames, $this->objects);
				$columnName = $tableNames . '.' . $field;
			}

			if(!empty($aggregator))
				$fields[$id] = sprintf('%s(%s%s)', $aggregator, $aggregatorPrefix, $columnName);
			else
				$fields[$id] = $aggregatorPrefix . $columnName;

			if(!is_null($als))
				$fields[$id] .= ' AS '.$als;
		}

		if(!$isArray)
			return $fields[0];
		return $fields;
	}
}
