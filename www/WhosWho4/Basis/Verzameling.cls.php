<?
/**
 *  Representeert een verzameling van DBObject-instanties.
 *
 * Het betreft hier eigenlijk geen verzameling, maar een geordende lijst van
 * objecten.
 *
 * In principe dienen alle objecten in de Verzameling tot dezelfde klasse te
 * behoren, welke dan weer een subklasse van DBObject moet zijn.
 */
class Verzameling
	extends Verzameling_Generated
	implements Iterator, ArrayAccess
{

	/** Een teller voor objecten die nog geen ID hebben. */
	private $nullTeller = 2147483647;

	protected static $proxies = [
		'each',
	];

	/**
	 *  Default constructor. Maak een nieuwe, lege Verzameling aan.
	 */
	public function __construct ()
	{
		parent::__construct();
	}

	/**
	 *  Factory-methode: maak een nieuwe Verzameling van een lijst van
	 * ID's.
	 *
	 * @param ids Een array met ID's van objecten die in de nieuwe Verzameling
	 * komen.
	 * @return Verzameling Een Verzameling die alle objecten met de gegeven ID's bevat.
	 * Objecten die niet in de database gevonden worden, worden overgeslagen.
	 */
	static public function verzamel ($ids)
	{
		$class = self::contentType();
		$class::cache($ids);

		$v = new static();
		foreach($ids as $id)
		{
			$obj = $class::geef($id);
			if (!$obj instanceof $class)
				continue;

			// Objecten horen niet twee keer in een verzameling te komen
			if(!in_array($obj->geefID(), $v->volgorde)) {
				$v->volgorde[] = $obj->geefID();
			}

			$v->verzameling[$obj->geefID()] = $obj;
		}
		return $v;
	}

	/**
	 *  Bepaal het aantal objecten in deze Verzameling.
	 *
	 * @return Het aantal DBObject-instanties in de Verzameling.
	 */
	public function aantal ()
	{
		return count($this->verzameling);
	}

	/**
	 *  Bepaal of deze Verzameling een bepaalde DBObject bevat.
	 *
	 * @param id De ID waarnaar gezocht wordt.
	 * @return True indien er een DBObject met ID id in deze Verzameling zit;
	 * False anders.
	 */
	public function bevat ($id)
	{
		if (is_null($id))
			return false;
		if ($id instanceof DBObject)
			$id = $id->geefID();
		$class = self::contentType();
		$id = $class::naarID($id);
		return isset($this->verzameling[$id]);
	}

	/**
	 * Pas deze verzameling aan zodat het object er niet meer inzit.
	 *
	 * Het object moet wel element zijn van deze verzameling.
	 *
	 * @param DBObject|mixed $id Het DBObject of ID van de DBObject-instantie die verwijderd wordt.
	 */
	public function verwijder ($id)
	{
		if ($id instanceof DBObject)
		{
			$id = $id->geefID();
		}

		if (!$this->bevat($id))
		{
			user_error('Element bestaat niet: ' . $id, E_USER_ERROR);
		}

		// Verwijder id uit de iterator
		$key = array_search($id, $this->volgorde);
		$slice1 = array_slice($this->volgorde, 0, $key);
		$slice2 = array_slice($this->volgorde, $key + 1);
		$this->volgorde = array_merge($slice1, $slice2);

		// Gooi het element weg uit de verzameling.
		unset($this->verzameling[$id]);
	}

	/**
	 *  Voeg een object toe aan deze Verzameling.
	 *
	 * Als het object als in de Verzameling zit gebeurt er niets.
	 *
	 * Deze methode geeft een error indien de klasse van object niet hetzelfde
	 * is als de klasse van alle andere objecten in de Verzameling.
	 *
	 * @param object Een instantie van DBObject die aan deze Verzameling toe
	 * wordt gevoegd. De klasse van het object dient hetzelfde te zijn als de
	 * klasse van objecten die al in deze Verzameling zitten.
	 */
	public function voegtoe ($object)
	{
		if (!is_a($object, self::contentType()))
		{
			throw new TypeError('Type mismatch: deze verzameling kan alleen objecten van type "' . self::contentType() . '" bevatten, je probeert een object van type "' . get_class($object) . '" toe te voegen.', E_USER_ERROR);
		}

		if (method_exists($object, 'geefID') && !is_null($object->geefID()))
		{
			if($this->bevat($object->geefID()))
				return; // zit er al in, doe niets.

			$this->verzameling[$object->geefID()] = $object;
			$this->volgorde[] = $object->geefID();
		}
		else
		{
			$this->verzameling[$this->nullTeller] = $object;
			$this->volgorde[] = $this->nullTeller;

			$this->nullTeller--;
		}
	}

	/**
	 *  Voeg alle objecten in een andere Verzameling toe aan deze Verzameling.
	 *
	 * @param verzameling Een andere Verzameling, die objecten van hetzelfde
	 * type bevat als deze Verzameling.
	 */
	public function union ($verzameling)
	{
		if (is_null($verzameling) || $this === $verzameling)
			return;

		$this->rewind();
		$verzameling->rewind();

		if(!($verzameling instanceof $this)) {
			user_error('Type mismatch: cannot compute union of ' . get_class($verzameling) . ' and ' . get_class($this), E_USER_ERROR);
		}

		// + array operator merget en voorkomt dubbele KEYS (geen renumbering)
		$this->verzameling = $this->verzameling + $verzameling->verzameling;

		// array_unique voorkomt niet dubbele VALUES maar laat wel hiaten zitten in de array
		// waar foreach NIET tegen kan. array_values nummert ook de keys en maakt wel een nette array van unique
		// values zonder deze hiaten. Misleidende functienamen FTL. Merge renumbert keys
		$this->volgorde = array_values(array_unique(array_merge(
			$this->volgorde, $verzameling->volgorde)));
	}

	/**
	 *  Vind alle elementen die zowel in deze als in een andere
	 * Verzameling zitten.
	 *
	 * @param verzameling Een andere Verzameling, die objecten van hetzelfde
	 * type bevat als deze Verzameling.
	 */
	public function intersection ($verzameling)
	{
		$this->rewind();
		$verzameling->rewind();

		if(!($verzameling instanceof $this)) {
			user_error('Type mismatch: cannot compute intersection of ' . get_class($verzameling) . ' and ' . get_class($this), E_USER_ERROR);
		}

		$this->verzameling = array_intersect($this->verzameling, $verzameling->verzameling);

		//Pak de array values zodat deze weer gerenumbert wordt maar dezelfde volgorde behoudt
		$this->volgorde = array_values(array_intersect($this->volgorde, $verzameling->volgorde));
	}

	/**
	 *  Bereken het relatieve complement van een andere Verzameling ten
	 * opzichte van deze Verzameling.
	 *
	 * Effectief verwijderen we alle objecten uit deze Verzameling die in
	 * verzameling zitten.
	 *
	 * @param verzameling Een andere Verzameling, die objecten van hetzelfde
	 * type bevat als deze Verzameling.
	 */
	public function complement ($verzameling)
	{
		$this->rewind();
		$verzameling->rewind();

		if(!($verzameling instanceof $this)) {
			user_error('Type mismatch: cannot compute complement of ' . get_class($verzameling) . ' and ' . get_class($this), E_USER_ERROR);
		}

		$this->verzameling = array_diff($this->verzameling, $verzameling->verzameling);

		$this->volgorde = array_values(array_diff($this->volgorde, $verzameling->volgorde));
	}

	/**
	 *  Bereken het complement van de union en de intersection van deze
	 * en een andere Verzameling.
	 *
	 * Merk op dat deze methode symmetrisch is in deze Verzameling en verzameling.
	 *
	 * @param verzameling Een andere Verzameling, die objecten van hetzelfde
	 * type bevat als deze Verzameling.
	 */
	public function difference ($verzameling)
	{
		$this->rewind();
		$verzameling->rewind();

		if(!($verzameling instanceof $this)) {
			user_error('Type mismatch: cannot compute difference of ' . get_class($verzameling) . ' and ' . get_class($this), E_USER_ERROR);
		}

		$tempIntersect = clone $this; //Omdat $this bij de union wordt aangepast moeten we 'm even clonen.

		$tempIntersect->intersection($verzameling);

		$this->union($verzameling);

		$this->complement($tempIntersect);

		unset($tempIntersect);
	}

	/**
	 *  Verwijder alle objecten uit deze Verzameling die de huidig ingelogde gebruiker niet mag bekijken.
	 *
	 * @see Entiteit::magBekijken
	 */
	public function filterBekijken ()
	{
		$tobe = array();
		foreach ($this->volgorde as $i => $key)
		{
			if (!$this->verzameling[$key]->magBekijken())
			{
				$tobe[] = $i;
				unset($this->verzameling[$key]);
			}
		}
		foreach ($tobe as $i)
			unset($this->volgorde[$i]);
		$old = $this->volgorde;
		$this->volgorde = array();
		foreach ($old as $id)
			$this->volgorde[] = $id;
		$this->rewind();
		return $this;
	}

	/**
	 * Bepaal de klasse van de objecten in deze Verzameling.
	 *
	 * @return string
	 * Een string met de naam van de klasse waarvan deze Verzameling
	 * objecten bevat. Dit is altijd een subklasse van DBObject.
	 */
	private static function contentType ()
	{
		$verzamelingklassenaam = get_called_class();

		// In een pure Verzameling mag je elk DBObject stoppen.
		if ($verzamelingklassenaam == 'Verzameling')
		{
			return 'DBObject';
		}

		// Anders parsen we de klassenaam van de verzameling om aan de klassenaam te komen.
		// Merk op dat dit voor een pure Verzameling een lege string geeft,
		// niet DBObject.
		return substr($verzamelingklassenaam, 0, -11);
	}

	/**
	 *  Bepaal het eerste item in deze Verzameling.
	 *
	 * @return Het DBObject dat in deze Verzameling als eerste staat; of NULL
	 * als deze Verzameling leeg is.
	 */
	public function first ()
	{
		if ($this->aantal() == 0) return null;
		return $this->verzameling[$this->volgorde[0]];
	}

	/**
	 *  Bepaal het laatste item in deze Verzameling.
	 *
	 * @return Het DBObject dat in deze Verzameling als laatste staat; of NULL
	 * als deze Verzameling leeg is.
	 */
	public function last ()
	{
		if ($this->aantal() == 0) return null;
		return $this->verzameling[$this->volgorde[$this->aantal() - 1]];
	}

	/**
	 *  Sla alle objecten in deze Verzameling op.
	 *
	 * @see DBObject::opslaan
	 */
	public function opslaan ()
	{
		foreach($this->verzameling as $id => $obj)
		{
			if (is_null($obj->geefID()))
			{
				$obj->opslaan();
				$this->verwijder($id);
				$this->voegtoe($obj);
			}
			else
			{
				$obj->opslaan();
			}
		}
	}

	/**
	 *  Verwijder alle objecten in deze Verzameling uit de database.
	 *
	 * @see DBObject::verwijderen
	 *  verwijderen() op alle objecten in de verzameling toepassen.
	 */
	public function verwijderen($foreigncall = false)
	{
		if(!$foreigncall) {
			foreach($this->verzameling as $id => $obj)
			{
				$obj->verwijderen();
			}
		} else {
			$queryarray = array();
			foreach($this->verzameling as $id => $obj)
			{
				$queryarray[] = $obj->verwijderen(true);
			}
			return $queryarray;
		}
	}

	/**
	 * @name Iterator interface
	 *@{
	 */
	public function current ()
	{
		return $this->verzameling[$this->key()];
	}
	public function key ()
	{
		return $this->volgorde[$this->positie];
	}
	public function next ()
	{
		$this->positie++;
	}
	public function rewind ()
	{
		$this->positie = 0;
	}
	public function valid ()
	{
		return isset($this->volgorde[$this->positie]);
	}
	public function hasNext ()
	{
		return $this->positie < count($this->verzameling)-1;
	}
	//@}

	/**
	 * @name ArrayAccess interface
	 *@{
	 */
	public function offsetExists ($id)
	{
		return $this->bevat($id);
	}
	public function offsetGet ($id)
	{
		if($this->offsetExists($id))
			return $this->verzameling[$id];

		return NULL;
	}
	public function offsetSet ($id, $value)
	{
		// TODO auto generate override
		throw new Exception('not (yet?) implemented');
	}
	public function offsetUnset ($id)
	{
		if(!$this->offsetExists($id))
			return;
		$this->verwijder($id);
	}
	//@}

	/**
	 *  Construeer een string op basis van de elementen in deze
	 * Verzameling.
	 *
	 * De gecostrueerde string bevat representaties van de elementen,
	 * gescheiden door glue.
	 *
	 * Bijvoorbeeld: CommissieVerzameling->implode("getNaam", ", ")
	 * levert een kommagescheiden string op van namen van commissies in de
	 * verzameling
	 *
	 * @param func De naam van een member-functie van het type objecten in deze
	 * verzameling. De functie moet een string returnen.
	 * @param glue Een string waarmee de resultaten aan elkaar worden geplakt.
	 * @return Een door glue gescheiden string van door func gegenereerde
	 * representaties van de elementen in deze Verzameling.
	 */
	public function implode ($func, $glue)
	{
		$strings = array();
		foreach($this as $elem)
			$strings[] = call_user_func(array($elem, $func));
		return implode($glue, $strings);
	}

	/**
	 *  Filter op een bepaalde waarde voor een bepaalde functie en geef
	 * in een nieuwe Verzameling de subset terug die er aan voldoet.
	 *
	 * Merk op dat deze Verzameling dus niet wordt aangepast.
	 *
	 * @param func De naam van een member-functie van het type objecten in deze
	 * verzameling.
	 * @param value De waarde die wordt vergeleken met de uitkomsten van func.
	 * Indien gelijk (==) wordt het object meegenomen in de subset.
	 * @return Een nieuwe Verzameling die alleen elementen bevat waarvan het
	 * resultaat van func gelijk is aan value.
	 */
	public function filter ($func, $value)
	{
		$classname = get_called_class();
		$new = new $classname(); //Maak een lege nieuwe verzameling

		foreach($this as $elem){
			$elemValue = call_user_func(array($elem, $func));
			if($elemValue == $value){
				$new->voegtoe($elem);
			}
		}

		return $new;
	}

	/**
	 * Geef alle keys van deze Verzameling. Dit zijn normaliter de ID's van de
	 * objecten.
	 *
	 * @return Een array met de keys van de objecten in deze Verzameling, in
	 * dezelfde volgorde als waarin ze in de Verzameling zitten.
	 */
	public function keys ()
	{
		return array_values($this->volgorde);
	}

	/**
	 * Maak een array van strings van de objecten in deze Verzameling.
	 *
	 * @return Een associatieve array met voor elk object in deze Verzameling
	 * de ID van dit object als key en het object naar een string gecast
	 * (__tostring__) als value, in dezelfde volorde als waarin ze in deze
	 * Verzameling zitten.
	 */
	public function geefArrayString ()
	{
		$ret = array();
		foreach ($this->volgorde as $id)
			$ret[$id] = (string) $this->verzameling[$id];
		return $ret;
	}

	/**
	 * Verwijder het laatste object in deze Verzameling.
	 *
	 * @return Het laatste object in deze Verzameling (voordat deze verwijderd
	 * wordt).
	 */
	public function pop ()
	{
		$v = $this->last();
		$this->verwijder($v->geefID());
		return $v;
	}

	/**
	 *  Sorteer deze Verzameling.
	 *
	 * De parameter waarop geeft de naam van de functie die gebruikt wordt om
	 * de sortering te bepalen. Dit moet een member-functie zijn van de
	 * objecten in de Verzameling. Voorbeeld:
	 * @code public static sorteerOp$waarop($a, $b) \endcode
	 *
	 * @param waarop Naam van de functie waarmee gesorteerd wordt.
	 * @param reverse Indien True wordt er omgekeerd gesorteerd.
	 * @return Deze Verzameling.
	 */
	public function sorteer ($waarop, $reverse = False)
	{
		if(!method_exists($this, "sorteerOp$waarop"))
			throw new BadMethodCallException('Onbekende sortering: ' . $waarop . ' voor ' . get_class($this));

		$this->rewind();
		usort($this->volgorde, array($this, "sorteerOp$waarop"));
		if($reverse)
			$this->volgorde = array_reverse($this->volgorde);

		return $this;
	}

	/**
	 *  Shuffle de volgorde van de objecten in deze Verzameling.
	 *
	 * @return Deze Verzameling.
	 */
	public function shuffle ()
	{
		shuffle($this->volgorde);
		return $this;
	}

	/**
	 *  Draai de volgorde van de objecten in deze Verzameling om.
	 *
	 * @return Verzameling Deze Verzameling.
	 */
	public function reverse ()
	{
		$this->volgorde = array_reverse($this->volgorde);
		return $this;
	}

	/**
	 *  Controleer of alle objecten in deze Verzameling valid zijn.
	 *
	 * @return True indien voor elk DBObject in de Verzameling de aanroep
	 * valid() True geeft; False anders.
	 * @see DBObject::valid
	 */
	public function allValid ($velden = 'set')
	{
		foreach ($this as $obj)
			if (!$obj->valid($velden))
				return false;
		return true;
	}

	/**
	 *  Geef de errors van alle objecten in deze Verzameling.
	 *
	 * We stoppen per veld de errors in een array,
	 * in Haskelltermen: [Map k v] -> Map k [v].
	 *
	 * @return array
	 * Een array die een samenvoeging is van de resultaten van de aanroep getErrors() op elk DBObject in deze
	 *     Verzameling.
	 * @see DBObject::getErrors
	 */
	public function allErrors ()
	{
		$ret = [];
		foreach ($this as $obj)
		{
			/** @var DBObject $obj */
			foreach ($obj->getErrors() as $k => $v)
			{
				if ($v)
				{
					$ret[$k][] = $v;
				}
			}
		}
		return $ret;
	}

	/**
	 * Geeft een true of alle objecten verwijderd mogen worden
	 * of een array van id's als het verwijderen niet mag
	 */
	public function magVerwijderen()
	{
		$magVerwijderen = true;
		$idsArray = array();

		foreach($this as $obj)
		{
			$magVerwijderen &= $obj->magVerwijderen();
			if(!$obj->magVerwijderen()) {
				$idsArray[] = $obj->geefID();
			}
		}

		if($magVerwijderen)
			return true;
		else
			return $idsArray;
	}

	/**
	 * Geeft true als alle objecten gewijzigd mogen worden of anders een
	 * array met de ids van de objecten die dat niet mogen
	 */
	public function magWijzigen()
	{
		$magWijzigen = true;
		$idsArray = array();

		foreach($this as $obj)
		{
			$magWijzigen &= $obj->magWijzigen();
			if(!$obj->magWijzigen()) {
				$idsArray[] = $obj->geefID();
			}
		}

		if($magWijzigen)
			return true;
		else
			return $magWijzigen;
	}

	/**
	 * \brief Maakt een array van een verzameling dat vervolgens met json_encode in
	 *        een json-format gestopt kan worden. De rede waarom we hier geen
	 *        json_decode doen is omdat deze methode recursief aangeroepen kan
	 *        worden en we pas op het einde alles willen encoden.
	 *
	 * \param expand Een bool of alle objecten in de verzameling ge-expand moeten worden.
	 *        Niet-ge-expandede objecten krijgen alleen een href.
	 * \param wantedVars Een array met de variabele die van het object gevraagd
	 *        worden. In dat geval doet de rest van de vars er niet toe.
	 *
	 * \return Een array klaar om ge-encode te worden als json. Als er niks van
	 *         het object bekeken mag worden geven we een lege array terug.
	 */
	public function __json($expand = false, $wantedVars = NULL)
	{
		$verzameling = array();

		foreach($this as $object)
		{
			// Als we het object niet mogen bekijken slaan we m over
			if(count($object->getBekijkbareVars()) != 0)
			{
				if($expand)
				{
					$object_json = $object->__json($wantedVars, is_array($expand) ? $expand : NULL);
					if(count($object_json) != 0)
						$verzameling[] = $object_json;
				}
				else
					$verzameling[] = array('href' => $object->getApiUri());
			}
		}

		return $verzameling;
	}

	static public function geefAlleJson($class, $offset = 0, $limit = 20, $vars, $expand, $obj = null)
	{
		global $uriRef;
		$queryClass = $class . 'Query';

		$query = $queryClass::table();

		if($obj)
		{
			$obj_class = get_class($obj);
			$joinKeys = false;

			while(!$joinKeys && $obj_class)
			{
				$joinKeys = $queryClass::getJoinKeys($obj_class);
				$obj_class = get_parent_class($obj_class);
			}

			if(!$joinKeys)
				return false;

			if($joinKeys)
			{
				$ids = explode('_', $obj);

				if(count($ids) != count($joinKeys)) // Er is iets mis!
					return false;

				$field_names = explode('_', key($joinKeys));
				$name = ucfirst($field_names[0]);
				$query->whereProp($name, $obj);
			}
		}

		if(!$class::magKlasseBekijken())
		{
			$filter = $queryClass::filterMagBekijken();
			if($filter)
			{
				if(isset($filter['joins']) && is_array($filter['joins']))
					$joins = $filter['joins'];
				else
					$joins = array();

				if(isset($filter['wheres']) && is_array($filter['wheres']))
					$wheres = $filter['wheres'];
				else if(isset($filter['wheres']))
					$wheres = array($filter['wheres']);
				else
					$wheres = array();

				foreach($joins as $join)
				{
					if(is_array($join))
						call_user_func_array(array($query, 'join'), $join);
					else
						$query->join($join);
				}

				foreach($wheres as $where)
					$query->where($where);
			}
		}
		$verzameling = $query->limit($limit)
			->offset($offset)
			->verzamel($class, true);

		$verzameling->filterBekijken();

		$total = $query->count();

		$get_params = $request->query->all();

		if(isset($get_params['offset']))
			unset($get_params['offset']);
		if(isset($get_params['limit']))
			unset($get_params['limit']);

		$json_verz = array();
		$json_verz['href'] = $uriRef['request'] . '?' . http_build_query($get_params + array('offset' => $offset, 'limit' => $limit));
		$json_verz['offset'] = $offset;
		$json_verz['limit'] = $limit;
		$json_verz['prev'] = ($offset == 0) ? NULL : (API_BASE . $class . '?' . http_build_query($get_params + array('offset' => max($offset - $limit, 0), 'limit' => $limit)));
		$json_verz['next'] = $uriRef['request'] . '?' . http_build_query($get_params + array('offset' => $offset + $limit, 'limit' => $limit));

		$verzameling_json = $verzameling->__json($expand, $vars);

		$json_verz['size'] = count($verzameling_json);
		$json_verz['items'] = $verzameling_json;
		$json_verz['total'] = (int)$total;

		return $json_verz;
	}

	/**
	 * Geeft een willekeurig element uit de verzameling.
	 */
	public function willekeurig()
	{
		return $this->verzameling[array_rand($this->verzameling)];
	}

	public function getVerzameling()
	{
		return $this->verzameling;
	}

	public function each(callable $callback)
	{
		foreach($this->verzameling as $item)
		{
			if($callback($item) === false)
				break;
		}

		return $this;
	}

	public function __get($key)
	{
		if(!in_array($key, static::$proxies))
		{
			throw new Exception("Property $key is geen bestaande proxy");
		}

		return new HigherOrderVerzameling($this, $key);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
