<?php

use Space\Auth\Auth;

/**
 * Class Controller
 *
 * Abstracte klasse die je kan inheriten voor Controller-objecten.
 * Bevat methoden en velden die het makkelijker maken om Controllers te programmeren.
 *
 * Instances worden door Benamite aangemaakt voor elke vfsEntry die ermee te maken heeft,
 * dus je kan niet in een variabele entry een member instellen die een include uitleest.
 */
abstract class Controller
{
	/**
	 * @brief Alle entryNames van benamite als een array
	 * @var string
	 */
	protected $entryNames;

	/**
	 * @var Auth
	 * Het object met alle autorisatie-informatie.
	 */
	private $auth;

	/**
	 * Constructor voor de controller.
	 * Wordt aangeroepen door Space
	 *
	 * @param Auth $auth
	 * @param array $entryNames
	 */
	public function __construct(Auth $auth, $entryNames = [])
	{
		$this->auth = $auth;
		$this->entryNames = $entryNames;
	}

	/**
	 * @brief Checkt of de ingelogde user het correcte level heeft, anders wordt er een 403 gegooid
	 *
	 * @param string $level
	 * Het level om te controleren als string
	 *
	 * @throws HTTPStatusException
	 * @return void
	 */
	protected function requireAuth($level)
	{
		if (!$this->hasAuth($level))
		{
			$this->responseUitStatus(403);
		}
	}

	/**
	 * @brief Checkt of de ingelogde user het correcte level heeft
	 *
	 * @param string $level Het level om te controleren als string
	 *
	 * @return bool
	 * Een boolean of de user het level heeft
	 */
	protected function hasAuth($level)
	{
		return hasAuth($level);
	}

	/**
	 * @brief Checkt of de ingelogde user de rechten van een cie heeft
	 *
	 * @param Commissie $cie
	 * De commissie om voor te checken.
	 *
	 * @return bool
	 * Een boolean of de user de rechten heeft
	 */
	protected function hasCieAuth(Commissie $cie)
	{
		return $this->auth->hasCieAuth($cie->geefID());
	}

	/**
	 * @brief Haalt een parameter uit de request parameters. Werkt hetzelfde als tryPar. Voor nu.
	 *
	 * @param string $parameter
	 * naam van de variabele
	 * @param mixed $default
	 * return waarde als $var niet gevonden wordt
	 * @param string $method
	 * GET, POST of REQUEST
	 *
	 * @return mixed
	 */
	protected function getRequestParam($parameter, $default = null, $method = 'REQUEST')
	{
		return tryPar($parameter, $default, $method);
	}

	/**
	 * @brief Redirect naar een url
	 *
	 * @param string $url De url om naar te redirecten
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	protected function redirect($url)
	{
		return responseRedirect($url);
	}

	/**
	 * @brief Redirect naar een url met een melding op de pagina.
	 *
	 * @param string $url De url om naar te redirecten.
	 * @param string $melding De melding om weer te geven.
	 * @param string $soort Het soort melding om weer te geven.
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	protected function redirectMelding($url, $melding, $soort = 'succes')
	{
		return Page::responseRedirectMelding($url, $melding, $soort);
	}

	/**
	 * @brief Gooit een HTTPStatusException
	 *
	 * @param int $status
	 * Een valide http statuscode
	 *
	 * @throws HTTPStatusException Gooit de exceptie met de status
	 */
	protected function responseUitStatus($status)
	{
		throw new HTTPStatusException($status);
	}

	/**
	 * @brief Gooit een 403 error code voor als de pagina niet bekeken mag worden
	 *
	 * @throws HTTPStatusException Gooit de exceptie met de status
	 */
	protected function geenToegang()
	{
		$this->responseUitStatus(403);
	}

	/**
	 * @brief Maakt een PageResponse van een gemaakte Page
	 *
	 * @param Page $page Een page object
	 *
	 * @return PageResponse
	 * Een nieuw PageResponse-object
	 */
	protected function pageResponse(Page $page)
	{
		return new PageResponse($page);
	}

	/**
	 * @brief Geeft de entrynames property terug
	 *
	 * @return string[]
	 * De entryNames als array
	 */
	protected function getEntryNames()
	{
		return $this->entryNames;
	}

	/**
	 * @brief Geeft een specifieke entryName uit de entryNames terug
	 *
	 * @param int $index De index van de array die opgevraagd moet worden
	 *
	 * @return string
	 * De gevraagde entryName
	 */
	protected function getEntryName($index = 0)
	{
		return $this->entryNames[$index];
	}

	/**
	 * @brief Geeft de ingelogde Persoon
	 *
	 * @return Persoon
	 * De ingelogde persoon als Persoon-object
	 */
	protected function getIngelogd()
	{
		return Persoon::getIngelogd();
	}

	/**
	 * @brief Geeft het id van de ingelogde Persoon terug
	 *
	 * @return int Het id van de ingelogde Persoon
	 */
	protected function getIngelogdID()
	{
		return $this->getIngelogd()->geefID();
	}
}
