<?
class QueryBuilder
	extends QueryBuilder_Generated
{
	/** De backtick als sanitizer **/
	static public $sanitizer = '`';
	/** Alle toegestane operators in where-functies **/
	static public $allowedOperators = array(
		'=', '!=', '<', '<=', '>', '>=',
		'IN', 'NOT IN', 'LIKE', 'NOT LIKE'
	);
	/** Alle toegestane operators in whereNull-functies **/
	static public $allowedNullOperators = array(
		'IS NULL', 'IS NOT NULL'
	);
	/** Alle toegestane joiners **/
	static public $allowedJoiners = array(
		'AND', 'OR', 'AND NOT', 'OR NOT'
	);
	/** Alle toegestane aggregators **/
	static public $allowedAggregators = array(
		'COUNT', 'SUM', 'MAX', 'MIN', 'YEAR'
	);
	/** Alle toegestane where-functies **/
	static public $allowedWhereFunctions = array(
		'orWhere', 'whereNot', 'orWhereNot',
		'whereIn', 'orWhereIn', 'whereNotIn', 'orWhereNotIn',
		'having', 'orHaving', 'havingNot', 'orHavingNot',
		'havingIn', 'orHavingIn', 'havingNotIn', 'orHavingNotIn',
		'where'
	);
	/** Alle toegestane types voor variabele **/
	static public $allowedTypes = array(
		'closure', // Closure moet hier apart bij voor nested
		'int', 'string', 'float', 'partstring', 'literal', 'nothing', 'function'
	);

	/** Alle toegestane sql date-functions met hun argumenten-format **/
	static public $allowedDateFunctions = array(
		'SQL_NOW', 'SQL_CURDATE', 'SQL_CURTIME'
	);

	static public $dateTypes = array(
		'MICROSECOND',
		'SECOND',
		'MINUTE',
		'HOUR',
		'DAY',
		'WEEK',
		'MONTH',
		'QUARTER',
		'YEAR',
		'SECOND_MICROSECOND',
		'MINUTE_MICROSECOND',
		'MINUTE_SECOND',
		'HOUR_MICROSECOND',
		'HOUR_SECOND',
		'HOUR_MINUTE',
		'DAY_MICROSECOND',
		'DAY_SECOND',
		'DAY_MINUTE',
		'DAY_HOUR',
		'YEAR_MONTH'
	);

	/** Een counter hoeveel zelf-ge-aliaste tabellen er zijn **/
	private $tableCounter = 0;

	/** Een array met welke tables we niet hoeven te autojoinen **/
	private $autoJoinObjects = array();

	/*** CONSTRUCTOR ***/
	public function __construct($objects = array(), $selects = array())
	{
		parent::__construct(); // QueryBuilder_Generated

		$this->objects = $objects;
		$this->selects = $selects;
	}

	/**
	 *  de __call-functie vangt in dit geval alle functies af en is bedoeld
	 *  om het type by where functies te achterhalen, zo wordt bij whereInt where
	 *  aangeroepen met type int
	 *
	 * @param name De naam van de aangeroepen functie
	 * @param args De argumenten van de aangeroepen functie
	 *
	 * @return Geeft het resultaat terug van $this->name() toegepast op($name, $args)
	 *  waarbij het type gehaald wordt uit de functie naam, bv whereNotInt -> type = int
	 */
	public function __call($name, $args)
	{
		// Als het eerste argument een closure is hebben we te maken met nested
		// wheres, dus dit heeft geen type en gaat meteen door.
		if(!empty($args) && $args[0] instanceof Closure)
		{
			$args = array_merge(array('closure'), $args);
			return call_user_func_array(array($this, $name), $args);
		}

		$match = false;
		$func = '';
		$type = '';

		foreach(self::$allowedWhereFunctions as $functionName)
		{
			if(substr($name, 0, strlen($functionName)) == $functionName)
			{
				// Dit is een vieze lelijke hack die beter moet;
				// de functie whereIn matched ook voor whereInt, wat niet hetzelfde is
				// in het eerste geval is het type 't' wat niet is toegestaan
				if(substr($functionName, -2) == 'In'
						&& substr($name, strlen($functionName), 1) == 't')
					continue;

				$func = $functionName;
				$type = strtolower(substr($name, strlen($func)));

				$match = true;
				break;
			}
		}

		if(!$match)
		{
			user_error('Call to undefined method ' . get_class($this)
				. '::' . $name . '()', E_USER_ERROR);
		}

		// Als we te maken hebben met een whereInArray-functie moeten we alle
		// types voor in de type-array eruit halen
		if(substr(strtolower($func), -2) == 'in'
			&& substr(strtolower($type), 0, 5) == 'array')
		{
			$types = substr($type, 5);
			$type = array();

			// We weten niet hoeveel types er komen, dus een while
			while(strlen($types) != 0)
			{
				$foundType = false;
				// Loop hier over alle types omdat we niet weten welk type
				// we kunnen verwachten
				foreach(self::$allowedTypes as $allowedType)
				{
					if(substr($types, 0, strlen($allowedType)) == $allowedType)
					{
						$type[] = $allowedType;
						$types = substr($types, strLen($allowedType));
						$foundType = true;
						break;
					}
				}

				if(!$foundType)
					user_error('Type ' . $types . ' niet toegestaan', E_USER_ERROR);
			}

			if(empty($type))
				user_error('Geef een type mee voor whereInArray-methoden', E_USER_ERROR);
		}
		// Het type moet wel bestaan
		else if(empty($type))
			user_error('Functie \'' . $func . '\' moet een type hebben.', E_USER_ERROR);
		// Het type moet geldig zijn
		else if(!in_array($type, self::$allowedTypes))
			user_error('Type \'' . $type . '\' niet toegestaan.', E_USER_ERROR);

		$args = array_merge(array($type), $args);

		return call_user_func_array(array($this, $func), $args);
	}

	/**
	 *  Kijkt of een key bestaat in de statements
	 *
	 * @param key De key om te checken
	 *
	 * @return De bool of het bestaat
	 */
	protected function statementsHaveKey($key)
	{
		return array_key_exists($key, $this->statements);
	}

	/**
	 *  Voegt een statements toe aan een key, als de key nog niet bestaat
	 *  wordt die toegevoegd
	 *
	 * @param key De key om de statements aan toe te voegen als string
	 * @param statements De statements om toe te voegen, kan een array of een enkele
	 *  waarde zijn
	 *
	 * @return this
	 */
	public function addStatements($key, $statements)
	{
		if(!is_array($statements))
		{
			$statements = array($statements);
		}

		if(!array_key_exists($key, $this->statements))
		{
			$this->statements[$key] = $statements;
		}
		else
		{
			// array_diff werkt niet goed op multidimensionele arrays, dus daarom
			// staat ie nu uit
			//$statements = array_diff_recursive($statements, $this->statements[$key]);
			$this->statements[$key] = array_merge($this->statements[$key], $statements);
		}

		if($key == 'selects')
			$this->selects = array_merge($this->selects, $statements);

		return $this;
	}

	/**
	 *  Voegt een enkele statement aan de statements of overschrijft
	 *  de huidige statement-key
	 *
	 * @param key De key als string
	 * @param statement De statement om toe te voegen
	 *
	 * @return this
	 */
	public function addSingleStatement($key, $statement)
	{
		$this->statements[$key] = $statement;

		return $this;
	}

	/**
	 *  Voeg selectkolommen toe aan de select-statement. Meerdere kolommen
	 *  kunnen in 1 keer toegevoegd worden door ze als argumenten aan select toe
	 *  te voegen. Als selectDistinct() al een keer aangeroepen is mag select
	 *  niet meer aangeroepen worden
	 *
	 *  @return this
	 */
	public function select()
	{
		if($this->statementsHaveKey('distinct'))
			user_error('Distinct is al geset, je kan alleen nog '
				. ' distinct selects toevoegen met selectDistinct()', E_USER_ERROR);

		$selects = func_get_args();
		$this->addStatements('selects', $selects);

		return $this;
	}

	/**
	 *  Voeg selectkolommen toe aan de select-statement met het distinct keyword.
	 *  Meerdere kolommen kunnen in 1 keer toegevoegd worden door ze als argumenten
	 *  aan select toe te voegen. Als select() al een keer aangeroepen is mag
	 *  selectDistinct() niet meer aangeroepen worden
	 *
	 *  @return this
	 */
	public function selectDistinct()
	{
		if($this->statementsHaveKey('selects')
				&& !$this->statementsHaveKey('distinct'))
			user_error('Selects zijn al geset, je kan alleen nog '
				. 'maar selects toevoegen met select()', E_USER_ERROR);

		$selects = func_get_args();
		$this->addSingleStatement('distinct', true);
		$this->addStatements('selects', $selects);

		return $this;
	}

	/**
	 *  Haalt alle selects (en de distinct) weg van de huidige query
	 *
	 * @return this
	 */
	protected function removeSelects()
	{
		if($this->statementsHaveKey('selects'))
			unset($this->statements['selects']);

		if($this->statementsHaveKey('distinct'))
			unset($this->statements['distinct']);

		return $this;
	}

	/**
	 *  Voegt tables toe aan de FROM van de query. Meerdere tables kunnen
	 *  worden toegevoegd door ze als argumenten mee te geven.
	 *
	 * @return this
	 */
	public function tables()
	{
		$tables = func_get_args();

		$newTables = array();

		// Auto-join alle tabellen en voeg ze toe
		$autoKey = 0;
		foreach($tables as $table)
		{
			// Omdat bij addObjectTable de huidige table aan objects wordt
			// toegevoegd is niet te controleren in de auto-join of de table
			// all gejoind is of niet. Sla daarom hier de huidige tables op
			// waarvan we zeker zijn dat ze niet meer gejoind hoeven te worden
			$this->autoJoinObjects = $this->objects;

			$this->addObjectTable($table);

			if(is_array($table))
				$this->doAutoJoin(reset($table));
			else
				$this->doAutoJoin($table);
		}

		return $this;
	}

	/**
	 *  Voegt een tabel toe als object aan de lijst met objects. Stopt
	 *  ook meteen een alias erbij als die niet al door de user is meegegeven
	 *
	 * @param table De tabelnaam
	 * @param addToTables Een bool of we de tabel ook meteen aan de tables-statement
	 *  toe willen voegen
	 *
	 * @return De aliaskey van de tabel als string
	 */
	private function addObjectTable($table, $addToTables = true)
	{
		if(is_array($table))
		{
			// De gebruiker heeft een alias meegegeven
			if(sizeof($table) != 1)
				user_error('Table als array moet precies 1 value hebben', E_USER_ERROR);

			$key = key($table);
			$table = reset($table);
		}
		else
		{
			// Voeg zelf een alias toe
			$key = 'table' . $this->tableCounter;
			$this->tableCounter++;
		}

		if(array_key_exists($key, $this->objects))
			return $key;

		$newTable = array($key => $table);

		if($addToTables)
			$this->addStatements('tables', array_values($newTable));

		$this->objects = array_merge($this->objects, $newTable);

		return $key;
	}

	/**
	 *  Zet de GROUP BY van de query. Meerdere kolommen kunnen worden toegevoegd
	 *  door ze te als argumenten mee te geven
	 *
	 * @return this
	 */
	public function groupBy()
	{
		$fields = func_get_args();

		$this->addStatements('extraSelects', $fields);
		$this->addStatements('groupBys', $fields);

		return $this;
	}

	/**
	 *  Voegt een ORDER BY $fields $direction toe. Deze functie is private
	 *  omdat het toevoegen van ORDER BY buiten deze klasse met orderByAsc en
	 *  orderByDesc moet gebeuren.
	 *
	 * @param fields Een array van velden om met direction als ORDER BY toe te voegen
	 * @param direction De richting, is 'ASC' of 'DESC'
	 *
	 * @return $this
	 */
	private function orderBy($fields, $direction)
	{
		foreach($fields as $field)
		{
			$this->addStatements('orderBys',
				array(compact('field', 'direction')));
		}

		return $this;
	}

	/**
	 *  Voeg kolommen als ORDER BY ASC toe. Meerdere kolommen kunnen worden
	 *  toegevoegd door ze als argumenten mee te geven.
	 *
	 * @return $this
	 */
	public function orderByAsc()
	{
		$fields = func_get_args();
		return $this->orderBy($fields, 'ASC');
	}

	/**
	 *  Voeg kolommen als ORDER BY DESC toe. Meerdere kolommen kunnen worden
	 *  toegevoegd door ze als argumenten mee te geven.
	 *
	 * @return this
	 */
	public function orderByDesc()
	{
		$fields = func_get_args();
		return $this->orderBy($fields, 'DESC');
	}

	/**
	 *  Voegt een LIMIT aan de query toe
	 *
	 * @param lim De LIMIT als int
	 *
	 * @return $this
	 */
	public function limit($lim)
	{
		if(!is_int($lim))
			user_error('Limit moet een int als input hebben', E_USER_ERROR);

		$this->addSingleStatement('limit', $lim);

		return $this;
	}

	/**
	 *  Voegt een OFFSET toe aan de query
	 *
	 * @param offset De OFFSET als int
	 *
	 * @return this
	 */
	public function offset($offset)
	{
		if(!is_int($offset))
			user_error('Offset moet een int als input hebben', E_USER_ERROR);

		$this->addSingleStatement('offset', $offset);

		return $this;
	}

	/**
	 *  Join de query met een andere table. Dit is standaard een LEFT JOIN.
	 *  Controleer ook meteen hoe je met joinen als je zelf geen velden meegeeft
	 *
	 * @param table De table of de object-naam om op te joinen als string
	 * @param field1 De kolom die gelijk komt aan $field2 in de ON, moet NULL zijn als $table een object is
	 * @param field2 De kolom die gelijk komt aan $field1 in de ON, moet NULL zijn als $table een object is
	 * @param type De join-type, moet 'LEFT JOIN', 'RIGHT JOIN', 'INNER JOIN' of 'FULL JOIN' zijn
	 *
	 * @return this
	 */
	public function join($table, $field1 = NULL, $field2 = NULL, $type = 'LEFT JOIN')
	{
		// De gebruiker kan een alias voor table hebben meegegeven, dus dat moeten
		// we goed afvangen
		$tableToAdd = $table;

		if(is_array($table))
		{
			if(sizeof($table) != 1)
				user_error('Table als array moet precies 1 value hebben', E_USER_ERROR);

			$table = reset($table);
		}

		// Als allebei field1 en field2 null zijn is er dus een object-naam
		// meegegeven om op te joinen, doe dus controles en automagie
		if(is_null($field1) && is_null($field2))
		{
			$objects = $this->objects;
			$canJoin = false;
			$keys = null;

			// Ga voor alle huidige objecten die de query queried langs of op
			// dit object gejoind mag worden
			foreach($this->objects as $key => $object)
			{
				$objectQuery = $object . 'Query';
				$tableQuery = $table . 'Query';
				// Haal de joinKeys op van zowel het nieuwe object als het object
				// wat al bij de query hoort
				// TODO: getJoinKeys moet ook naar extended classes
				$keyArray = array($objectQuery::getJoinKeys($table), $tableQuery::getJoinKeys($object));
				foreach($keyArray as $keysTmp)
				{
					// Als $keysTmp === False
					if(!is_array($keysTmp))
						continue;

					$keys = $keysTmp;
					if($canJoin)
						user_error('Er zijn meerdere huidige tables in de query '
							. 'waarmee ' . $table . ' gejoind kan worden', E_USER_ERROR);
					// Met reset vragen we het eerste element van $keys op
					// TODO: iets als [veld] toevoegen om dit te voorkomen, bv ContactPersoon[spook] (uit Interactie)
					if(is_array(reset($keys)))
						user_error('Er zijn meerdere eigenschappen van '.$object
							. ' die ' . $table . ' als foreign hebben', E_USER_ERROR);
					$canJoin = true;
				}
			}

			if(!$canJoin)
				user_error('Er is geen manier om '.$table.' te joinen', E_USER_ERROR);

			if(sizeof($keys) == 1)
			{
				// reset zorgt er voor dat key de key van het eerste element
				// van $keys ophaalt
				reset($keys);
				$field1 = key($keys);
				$field2 = $keys[$field1];
			}
			else
			{
				// Er zijn meerdere kolommen om op te joinen, we voegen dus
				// een Closure toe met alle kolommen, Closures voor joins worden
				// standaard gelijmd door een AND
				$field1 = function($q) use ($tableToAdd, $type, $keys)
				{
					foreach($keys as $id => $key)
						$q->join($tableToAdd, $id, $key, $type);
				};
				$field2 = NULL;
			}
		}

		// Omdat bij addObjectTable de huidige table aan objects wordt
		// toegevoegd is niet te controleren in de auto-join of de table
		// all gejoind is of niet. Sla daarom hier de huidige tables op
		// waarvan we zeker zijn dat ze niet meer gejoind hoeven te worden
		$this->autoJoinObjects = $this->objects;

		// Voeg de table toe aan de lijst met objecten
		$table = $this->addObjectTable($tableToAdd, false);

		$this->addStatements('joins', array(compact('table', 'field1', 'field2', 'type')));

		// We doen een autoJoin pas nadat de huidige join toegevoegd is, anders
		// kan er in de verkeerde volgorde gejoind worden
		$this->doAutoJoin($tableToAdd);

		return $this;
	}

	/**
	 *  Doet een automatische join op de sub-klasse als de tabel een klasse
	 *  representeert die een extensie is
	 *
	 * @param table De tabelnaam
	 *
	 * @return this
	 */
	public function doAutoJoin($table)
	{
		if(is_array($table))
		{
			if(sizeof($table) != 1)
				user_error('Table als array moet precies 1 value hebben', E_USER_ERROR);

			$table = reset($table);
		}

		$tableQuery = $table . 'Query';

		if(in_array($table, $this->autoJoinObjects))
			return $this;

		$aJoin = $tableQuery::automaticJoin();

		if($aJoin)
		{
			// Het resultataat van automaticJoin is een array met als key
			// de tabelnaam en als value de manier om te joinen
			$keys = reset($aJoin);
			$joinTable = key($aJoin);

			if(sizeof($keys) > 1)
			{
				$closure = function($q) use ($joinTable, $keys) {
					foreach($keys as $key => $id)
						$q->join($joinTable, $key, $id);
				};
				$this->join($joinTable, $closure);
			}
			else
			{
				$id = reset($keys);
				$this->join($joinTable, key($keys), $id);
			}
		}

		return $this;
	}

	/**
	 *  Doe een RIGHT JOIN
	 *
	 * @param table De table of de object-naam om op te joinen als string
	 * @param field1 De kolom die gelijk komt aan $field2 in de ON, moet NULL zijn als $table een object is
	 * @param field2 De kolom die gelijk komt aan $field1 in de ON, moet NULL zijn als $table een object is
	 *
	 * @return this
	 */
	public function rightJoin($table, $field1 = NULL, $field2 = NULL)
	{
		return $this->join($table, $field1, $field2, 'RIGHT JOIN');
	}

	/**
	 *  Doe een INNER JOIN
	 *
	 * @param table De table of de object-naam om op te joinen als string
	 * @param field1 De kolom die gelijk komt aan $field2 in de ON, moet NULL zijn als $table een object is
	 * @param field2 De kolom die gelijk komt aan $field1 in de ON, moet NULL zijn als $table een object is
	 *
	 * @return this
	 */
	public function innerJoin($table, $field1 = NULL, $field2 = NULL)
	{
		return $this->join($table, $field1, $field2, 'INNER JOIN');
	}

	/**
	 *  Doe een FULL JOIN
	 *
	 * @param table De table of de object-naam om op te joinen als string
	 * @param field1 De kolom die gelijk komt aan $field2 in de ON, moet NULL zijn als $table een object is
	 * @param field2 De kolom die gelijk komt aan $field1 in de ON, moet NULL zijn als $table een object is
	 *
	 * @return this
	 */
	public function fullJoin($table, $field1 = NULL, $field2 = NULL)
	{
		return $this->join($table, $field1, $field2, 'FULL JOIN');
	}

	/**
	 *  Voeg een WHERE clausule toe aan de query. Deze functie is private en
	 *  hoort alleen aangeroepen te worden door __call en andere where-functies.
	 *  Deze functie wordt als basis voor alle andere where-functies gebruikt.
	 *
	 * @param type Het type van de value, ie 'int', 'string' etc.
	 * @param key De kolom waar de where aan moet voldoen
	 * @param operator De operator die gebruikt wordt in de where. Kan elke waarde
	 *  aannemen die gedefinieerd is in self::$allowedOperators. Voor de operator
	 *  kan ook de value ingevuld worden als de value ook NULL gelaten wordt. In
	 *  dat geval wordt de operator '=';
	 * @param value De waarde waarmee de kolom vergelijkt moet worden met de operator
	 * @param joiner Hoe de where aan andere where's geplakt moet worden. De verschillende
	 *  toegestane joiners staan in self::$allowedJoiners
	 * @param criteria Bij welke criteria het gestopt moet worden, standaard bij de wheres
	 *
	 * @return this
	 */
	private function where($type, $key, $operator = null, $value = null, $joiner = 'AND', $criteria = 'wheres')
	{
		if(is_null($value))
		{
			$value = $operator;
			$operator = '=';
		}

		if(!in_array($operator, self::$allowedOperators))
			user_error('Operator ' . $operator . ' is niet toegestaan', E_USER_ERROR);

		if(!in_array($joiner, self::$allowedJoiners))
			user_error('Joiner ' . $joiner . ' is niet toegestaan', E_USER_ERROR);

		if(is_array($type))
		{
			foreach($type as $t)
				if(!in_array($t, self::$allowedTypes))
					user_error('Type ' . $t . ' is niet toegestaan', E_USER_ERROR);
		}
		else
			if(!in_array($type, self::$allowedTypes))
				user_error('Type ' . $type . ' is niet toegestaan', E_USER_ERROR);

		$this->addStatements($criteria, array(compact('key', 'type', 'operator', 'value', 'joiner')));

		return $this;
	}

	/**
	 *  Voeg een OR WHERE toe aan de query. Voor de parameters, zie where
	 */
	private function orWhere($type, $key, $operator = null, $value = null)
	{
		return $this->where($type, $key, $operator, $value, 'OR');
	}

	/**
	 *  Voeg een WHERE NOT toe aan de query. Voor de parameters, zie where
	 */
	private function whereNot($type, $key, $operator = null, $value = null)
	{
		return $this->where($type, $key, $operator, $value, 'AND NOT');
	}

	/**
	 *  Voeg een OR WHERE NOT toe aan de query. Voor de parameters, zie where
	 */
	private function orWhereNot($type, $key, $operator = null, $value = null)
	{
		return $this->where($type, $key, $operator, $value, 'OR NOT');
	}

	/**
	 *  Voeg een WHERE LIKE toe aan de query. Voor de parameters, zie where
	 */
	public function whereLike($key, $value = null)
	{
		return $this->where('string', $key, 'LIKE', $value, 'AND');
	}

	/**
	 *  Voeg een OR WHERE LIKE toe aan de query. Voor de parameters, zie where
	 */
	public function orWhereLike($key, $value = null)
	{
		return $this->where('string', $key, 'LIKE', $value, 'OR');
	}

	/**
	 *  Voeg een WHERE NOT LIKE toe aan de query. Voor de parameters, zie where
	 */
	public function whereNotLike($key, $value = null)
	{
		return $this->where('string', $key, 'NOT LIKE', $value, 'AND');
	}

	/**
	 *  Voeg een OR WHERE NOT LIKE toe aan de query. Voor de parameters, zie where
	 */
	public function orWhereNotLike($key, $value = null)
	{
		return $this->where('string', $key, 'NOT LIKE', $value, 'OR');
	}

	/**
	 *  Voeg een WHERE IN toe aan de query. Voor de parameters, zie where
	 */
	private function whereIn($type, $key, $value = null)
	{
		return $this->where($type, $key, 'IN', $value, 'AND');
	}

	/**
	 *  Voeg een OR WHERE IN toe aan de query. Voor de parameters, zie where
	 */
	private function orWhereIn($type, $key, $value = null)
	{
		return $this->where($type, $key, 'IN', $value, 'OR');
	}

	/**
	 *  Voeg een WHERE NOT IN toe aan de query. Voor de parameters, zie where
	 */
	private function whereNotIn($type, $key, $value = null)
	{
		return $this->where($type, $key, 'NOT IN', $value, 'AND');
	}

	/**
	 *  Voeg een OR WHERE NOT IN toe aan de query. Voor de parameters, zie where
	 */
	private function orWhereNotIn($type, $key, $value = null)
	{
		return $this->where($type, $key, 'NOT IN', $value, 'OR');
	}

	/**
	 *  Voegt een WHERE ... IS NULL toe aan een query. Deze functie wordt
	 *  ook gebruikt als basis voor de andere whereNull-functies
	 *
	 * @param key De kolom waarop gecontroleerd wordt
	 * @param operator De operator, kan een van de operators va self::allowedNullOperators zijn
	 * @param joiner De joiner, zie ook $joiner in where
	 * @param criteria Bij welke criteria het gestopt moet worden, standaard bij de wheres
	 *
	 * @return this
	 */
	public function whereNull($key, $operator = 'IS NULL', $joiner = 'AND', $criteria = 'wheres')
	{
		if(!in_array($operator, self::$allowedNullOperators))
			user_error('Operator ' . $operator . ' is niet toegestaan', E_USER_ERROR);

		if(!in_array($joiner, self::$allowedJoiners))
			user_error('Joiner ' . $joiner . ' is niet toegestaan', E_USER_ERROR);

		$value = NULL;
		$type = NULL;
		$this->addStatements($criteria, array(compact('key', 'type', 'operator', 'value', 'joiner')));

		return $this;
	}

	/**
	 *  Voeg een OR WHERE ... IS NULL toe aan de query. Voor de parameters, zie whereNull
	 */
	public function orWhereNull($key)
	{
		return $this->whereNull($key, 'IS NULL', 'OR');
	}

	/**
	 *  Voeg een WHERE ... IS NOT NULL toe aan de query. Voor de parameters, zie whereNull
	 */
	public function whereNotNull($key)
	{
		return $this->whereNull($key, 'IS NOT NULL');
	}

	/**
	 *  Voeg een OR WHERE ... IS NOT NULL toe aan de query. Voor de parameters, zie whereNull
	 */
	public function orWhereNotNull($key)
	{
		return $this->whereNull($key, 'IS NOT NULL', 'OR');
	}

	private function having($type, $key, $operator = NULL, $value = NULL, $joiner = 'AND')
	{
		$this->addStatements('extraSelects', $key);
		return $this->where($type, $key, $operator, $value, $joiner, 'havings');
	}

	private function orHaving($type, $key, $operator = NULL, $value = NULL)
	{
		return $this->having($type, $key, $operator, $value, $joiner = 'OR');
	}

	private function havingNot($type, $key, $operator = NULL, $value = NULL)
	{
		return $this->having($type, $key, $operator, $value, $joiner = 'AND NOT');
	}

	private function orHavingNot($type, $key, $operator = NULL, $value = NULL)
	{
		return $this->having($type, $key, $operator, $value, $joiner = 'OR NOT');
	}

	private function havingIn($type, $key, $value)
	{
		return $this->having($type, $key, 'IN', $value, 'AND');
	}

	private function orHavingIn($type, $key, $value)
	{
		return $this->having($type, $key, 'IN', $value, 'OR');
	}

	private function havingNotIn($type, $key, $value)
	{
		return $this->having($key, 'NOT IN', $value, 'AND');
	}

	private function orHavingNotIn($type, $key, $value)
	{
		return $this->having($key, 'NOT IN', $value, 'OR');
	}

	public function havingLike($key, $value = null)
	{
		return $this->having('string', $key, 'LIKE', $value, 'AND');
	}

	public function orHavingLike($key, $value = null)
	{
		return $this->having('string', $key, 'LIKE', $value, 'OR');
	}

	public function havingNotLike($key, $value = null)
	{
		return $this->having('string', $key, 'NOT LIKE', $value, 'AND');
	}

	public function orHavingNotLike($key, $value = null)
	{
		return $this->having('string', $key, 'NOT LIKE', $value, 'OR');
	}

	public function havingNull($key, $operator = 'NULL', $joiner = 'AND')
	{
		return $this->whereNull($key, $operator, $joiner, 'havings');
	}

	public function orHavingNull($key)
	{
		return $this->havingNull($key, 'IS NULL', 'OR');
	}

	public function havingNotNull($key)
	{
		return $this->havingNull($key, 'IS NOT NULL');
	}

	public function orHavingNotNull($key)
	{
		return $this->havingNull($key, 'IS NOT NULL', 'OR');
	}

	/**
	 *  Voert een whereProp uit, controleert of de property gebruikt mag worden
	 *  en stopt meteen de juiste informatie in de where-functie
	 *
	 * @param prop De property voor de where
	 * @param operator De operator om in de where te gebruiken
	 * @param value De waarde waarmee de property vergeleken moet worden
	 * @param joiner De joiner waarmee deze where aan andere wheres geplakt moet worden
	 *
	 * @return $this
	 */
	public function whereProp($prop, $operator, $value = NULL, $joiner = 'AND')
	{
		if(is_null($value))
		{
			$value = $operator;
			$operator = '=';
		}

		$canAdd = false;

		$strArr = explode('.', $prop, 2);
		if(sizeof($strArr) == 2)
		{
			$table = $strArr[0];
			$prop = $strArr[1];

			if(!in_array($table, $this->objects))
			{
				if(!array_key_exists($table, $this->objects))
					user_error('Ik weet niet hoe ik ' .$table. ' moet verwerken', E_USER_ERROR);
				$table = $this->objects[$table];
			}

			$class = $table . 'Query';
			list($field, $val, $type) = $class::checkProp($prop, $value);
			if(!is_array($val) && substr($operator, -2) === 'IN')
				user_error('Ik heb een "IN" maar de value is geen array', E_USER_ERROR);
			if(is_array($val) && count($val) == 0)
				return $this;

			if($field)
				$canAdd = true;
		}
		else
		{
			foreach($this->objects as $object)
			{
				$class = $object . 'Query';
				list($field, $val, $type) = $class::checkProp($prop, $value);
				if($field)
				{
					if(!is_array($val) && substr($operator, -2) === 'IN')
						user_error('Ik heb een "IN" maar de value is geen array', E_USER_ERROR);
					if(is_array($val) && count($val) == 0)
						return $this;
					$canAdd = true;
					break;
				}
			}
		}

		if(!$canAdd)
			user_error('Ik weet niet hoe ik ' . $prop . ' in deze query moet gebruiken!', E_USER_ERROR);

		if(is_array($field))
		{
			$v = function($q) use ($field, $operator, $val, $type) {
				foreach($field as $id => $f)
				{
					$q->where($type[$id], $f, $operator, $val[$id]);
				}
			};

			return $this->where('closure', $v, NULL, NULL, $joiner);
		}
		else
		{
			return $this->where($type, $field, $operator, $val, $joiner);
		}
	}

	/**
	 *  Or-whereProp, zie ook whereProp en where
	 *
	 * @return $this
	 */
	public function orWhereProp($prop, $operator, $value = NULL)
	{
		return $this->whereProp($prop, $operator, $value, 'OR');
	}

	/**
	 *  WhereNotProp, zie ook whereProp en where
	 */
	public function whereNotProp($prop, $operator, $value = NULL)
	{
		return $this->whereProp($prop, $operator, $value, 'AND NOT');
	}

	/**
	 *  Or-whereNotProp, zie ook whereProp en where
	 */
	public function orWhereNotProp($prop, $operator, $value = NULL)
	{
		return $this->whereProp($prop, $operator, $value, 'OR NOT');
	}

	/**
	 *  WhereInProp, zie ook whereProp en where
	 */
	public function whereInProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'IN', $value, 'AND');
	}

	/**
	 *  Or-whereInProp, zie ook whereProp en where
	 */
	public function orWhereInProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'IN', $value, 'OR');
	}

	/**
	 *  WhereNotInProp, zie ook whereProp en where
	 */
	public function whereNotInProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'NOT IN', $value, 'AND');
	}

	/**
	 *  Or-whereNotInProp, zie ook whereProp en where
	 */
	public function orWhereNotInProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'NOT IN', $value, 'OR');
	}

	/**
	 *  WhereLikeProp, zie ook whereProp en where
	 */
	public function whereLikeProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'LIKE', $value, 'AND');
	}

	/**
	 *  Or-whereLikeProp, zie ook whereProp en where
	 */
	public function orWhereLikeProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'LIKE', $value, 'OR');
	}

	/**
	 *  WhereNotLikeProp, zie ook whereProp en where
	 */
	public function whereNotLikeProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'NOT LIKE', $value, 'AND');
	}

	/**
	 *  Or-whereNotLikeProp, zie ook whereProp en where
	 */
	public function orWhereNotLikeProp($prop, $value = NULL)
	{
		return $this->whereProp($prop, 'NOT LIKE', $value, 'OR');
	}

	public function havingProp($type, $key, $operator = NULL, $value = NULL, $joiner = 'AND')
	{
		if(is_null($value))
		{
			$value = $operator;
			$operator = '=';
		}

		$canAdd = false;
		foreach($this->objects as $object)
		{
			$class = $object . 'Query';
			list($field, $value, $type) = $class::checkProp($prop, $value);
			if($field)
			{
				$canAdd = true;
				break;
			}
		}

		if(!$canAdd)
			user_error('Ik weet niet hoe ik ' . $prop . ' in deze query moet gebruiken!', E_USER_ERROR);

		$this->addStatements('extraSelects', $field);

		return $this->whereProp($type, $key, $operator, $value, $joiner, 'havings');
	}

	public function orHavingProp($type, $key, $operator = NULL, $value = NULL)
	{
		return $this->havingProp($type, $key, $operator, $value, $joiner = 'OR');
	}

	public function havingNotProp($key, $operator = NULL, $value = NULL)
	{
		return $this->havingProp($key, $operator, $value, $joiner = 'AND NOT');
	}

	public function orHavingNotProp($key, $operator = NULL, $value = NULL)
	{
		return $this->havingProp($key, $operator, $value, $joiner = 'OR NOT');
	}

	public function havingInProp($key, $value)
	{
		return $this->havingProp($key, 'IN', $value, 'AND');
	}

	public function orHavingInProp($key, $value)
	{
		return $this->havingProp($key, 'IN', $value, 'OR');
	}

	public function havingNotInProp($key, $value)
	{
		return $this->havingProp($key, 'NOT IN', $value, 'AND');
	}

	public function orHavingNotInProp($key, $value)
	{
		return $this->havingProp($key, 'NOT IN', $value, 'OR');
	}

	public function havingLikeProp($key, $value = null)
	{
		return $this->havingProp($key, 'LIKE', $value, 'AND');
	}

	public function orHavingLikeProp($key, $value = null)
	{
		return $this->havingProp($key, 'LIKE', $value, 'OR');
	}

	public function havingNotLikeProp($key, $value = null)
	{
		return $this->havingProp($key, 'NOT LIKE', $value, 'AND');
	}

	public function orHavingNotLikeProp($key, $value = null)
	{
		return $this->havingProp($key, 'NOT LIKE', $value, 'OR');
	}

	/**
	 *  Voegt een WHERE ... IS NULL toe aan een query van een prop. Deze functie wordt
	 *  ook gebruikt als basis voor de andere wherePropNull-functies
	 *
	 * @param prop De prop waarop gecontroleerd wordt
	 * @param operator De operator, kan een van de operators va self::allowedNullOperators zijn
	 * @param joiner De joiner, zie ook $joiner in where
	 * @param criteria Bij welke criteria het gestopt moet worden, standaard bij de wheres
	 *
	 * @return this
	 */
	public function wherePropNull($prop, $operator = 'IS NULL', $joiner = 'AND', $criteria = 'wheres')
	{
		if(!in_array($operator, self::$allowedNullOperators))
			user_error('Operator ' . $operator . ' is niet toegestaan', E_USER_ERROR);

		if(!in_array($joiner, self::$allowedJoiners))
			user_error('Joiner ' . $joiner . ' is niet toegestaan', E_USER_ERROR);

		$canAdd = false;

		$strArr = explode('.', $prop, 2);
		if(sizeof($strArr) == 2)
		{
			$table = $strArr[0];
			$prop = $strArr[1];

			if(!in_array($table, $this->objects))
			{
				if(!array_key_exists($table, $this->objects))
					user_error('Ik weet niet hoe ik ' .$table. ' moet verwerken', E_USER_ERROR);
				$table = $this->objects[$table];
			}

			$class = $table . 'Query';
			list($field, $val, $type) = $class::checkProp($prop, $value);

			if($field)
				$canAdd = true;
		}
		else
		{
			foreach($this->objects as $object)
			{
				$class = $object . 'Query';
				list($field, $val, $type) = $class::checkProp($prop);
				if($field && is_null($val))
				{
					$canAdd = true;
					break;
				}
			}
		}

		if(!$canAdd)
			user_error('Ik weet niet hoe ik ' . $prop . ' in deze query moet gebruiken!', E_USER_ERROR);

		$key = $field;

		$value = NULL;
		$type = NULL;

		$this->addStatements($criteria, array(compact('key', 'type', 'operator', 'value', 'joiner')));

		return $this;
	}

	/**
	 *  Voeg een OR WHERE ... IS NULL toe aan de query. Voor de parameters, zie wherePropNull
	 */
	public function orWherePropNull($key)
	{
		return $this->wherePropNull($key, 'IS NULL', 'OR');
	}

	/**
	 *  Voeg een WHERE ... IS NOT NULL toe aan de query. Voor de parameters, zie wherePropNull
	 */
	public function wherePropNotNull($key)
	{
		return $this->wherePropNull($key, 'IS NOT NULL');
	}

	/**
	 *  Voeg een OR WHERE ... IS NOT NULL toe aan de query. Voor de parameters, zie wherePropNull
	 */
	public function orWherePropNotNull($key)
	{
		return $this->wherePropNull($key, 'IS NOT NULL', 'OR');
	}

	static public function getEquivalentTables()
	{
		return array();
	}

	static public function checkField($field)
	{
		return false;
	}

	static public function automaticJoin()
	{
		return False;
	}

	static public function checkProp($prop, $value)
	{
		return array(false, false, false);
	}

	static public function sqlDateFunctionCheck($value)
	{
		if(is_null($value))
		{
			return NULL;
		}

		$value = strtoupper($value);

		if(in_array($value, self::$allowedDateFunctions))
		{
			switch($value)
			{
			case 'SQL_NOW':
				return 'NOW()';
				break;
			case 'SQL_CURDATE':
				return 'CURDATE()';
				break;
			case 'SQL_CURTIME':
				return 'CURTIME()';
				break;
			}
		}

		return false;
	}

	static public function filterMagBekijken()
	{
		return false;
	}
}
