<?
/**
 * Php file met allemaal hulpfuncties die worden gebruikt in WSW4.
 **/

// Geeft de zelfde array als enumsFunctie van CommissieLid, maar nu met de functies
// volledig uitgeschreven.
function commissieFuncties()
{
	return [
		'OVERIG' => 'Overig',
		'VOORZ' => 'Voorzitter',
		'SECR' => 'Secretaris',
		'PENNY' => 'Penningmeester',
		'PR' => 'Promocommissaris',
		'SPONS' => 'Sponsorcommissaris',
		'BESTUUR' => 'Bestuurslid',
		'SFEER' => 'Sfeercommissaris',
	];
}

//Geef een lijst met links voor get parameters zodat je makkelijk je query in stukken kan knippen.
function limietLinks($aantal, $stapgrootte)
{
	global $request;

	if ($aantal / $stapgrootte < 1)
	{
		return new HtmlDiv();
	}

	$aantalStappen = floor($aantal / $stapgrootte);
	$remainder = $aantal % $stapgrootte;
	$getParams = $request->query->all();
	$limit = tryPar('limit', '0_' . $stapgrootte);
	$limitSplit = explode('_', $limit);

	$li = new HtmlList(null, null, 'pagination');
	$li->setNoDefaultClasses();

	$getParams['limit'] = ($limitSplit[0] - $stapgrootte) . "_" . $limitSplit[0];
	$item = new HtmlListItem(new HtmlAnchor('?' . http_build_query($getParams), "&laquo;"));
	if ($limit == '0_' . $stapgrootte)
	{
		$item->addClass('disabled');
	}
	$item->setNoDefaultClasses();
	$li->add($item);

	for ($i = 0; $i < $aantalStappen; $i++)
	{
		//Zorg er voor dat de regel na 25 entries wordt afgebroken. TODO: kan vast mooier in CSS.
		if (($i % 25) == 0 && $i > 0)
		{
			$resultArray[] = new HtmlBreak(1);
		}

		if ($i == 0 && $aantalStappen > 0)
		{
			//Zonder spatie in de string gaat het fout.. (" 0")?
			$getParams['limit'] = "0_" . ($i + 1) * $stapgrootte;
			$item = new HtmlListItem(new HtmlAnchor('?' . http_build_query($getParams), "0"));
		}
		else
		{
			$getParams['limit'] = $i * $stapgrootte . "_" . ($i + 1) * $stapgrootte;
			$item = new HtmlListItem(new HtmlAnchor('?' . http_build_query($getParams), $i * $stapgrootte));
		}
		$item->setNoDefaultClasses();
		if ($limit == $i * $stapgrootte . "_" . ($i + 1) * $stapgrootte)
		{
			$item->addClass('active');
		}
		$li->add($item);
	}

	//En plak ook de laatste paar er aan...
	if ($remainder > 0)
	{
		$getParams['limit'] = $aantalStappen * $stapgrootte . "_" . ($aantalStappen + 1) * $stapgrootte;
		$item = new HtmlListItem(new HtmlAnchor('?' . http_build_query($getParams), $aantalStappen * $stapgrootte));
		$item->setNoDefaultClasses();
		if ($limit == $aantalStappen * $stapgrootte . "_" . ($aantalStappen + 1) * $stapgrootte)
		{
			$item->addClass('active');
		}
		$li->add($item);
	}

	$getParams['limit'] = $limitSplit[1] . "_" . ($limitSplit[1] + $stapgrootte);
	$item = new HtmlListItem(new HtmlAnchor('?' . http_build_query($getParams), "&raquo;"));
	if ($limit == $aantalStappen * $stapgrootte . "_" . ($aantalStappen + 1) * $stapgrootte)
	{
		$item->addClass('disabled');
	}
	$item->setNoDefaultClasses();
	$li->add($item);

	return new HtmlDiv($li);
}

//Als je niet zeker weet of iets een datatime is en niet moeilijk dingen wil gaan uitzoeken.
function maybeDateTimeToString($var)
{
	if ($var instanceof DateTime)
	{
		return $var->format("d-m-Y");
	}
	return $var;
}

/**
 * Returnt de naam van een object, indien mogelijk, of NULL anders.
 *
 * De naam komt uit de bijbehorende View-klasse,
 * met een van de methodes:
 * * naam
 * * waardeNaam
 * * titel
 * * waardeTitel
 *
 * @param Entiteit $obj
 * Het object waarvan een naam wordt gegenereerd.
 *
 * @return string|null
 * De naam van het object, of NULL.
 */
function geefNaam($obj)
{
	//pak de bijpassende View klasse en gebruik de juiste naamfunctie.
	$klasse = get_class($obj) . 'View';
	if (method_exists($klasse, 'naam'))
	{
		return $klasse::naam($obj);
	}
	elseif (method_exists($klasse, 'waardeNaam'))
	{
		return $klasse::waardeNaam($obj);
	}
	elseif (method_exists($klasse, 'titel'))
	{
		return $klasse::titel($obj);
	}
	elseif (method_exists($klasse, 'waardeTitel'))
	{
		return $klasse::waardeTitel($obj);
	}

	return null;
}

/**
 * Retourneert het emailadres van een object, indien mogelijk, of NULL anders.
 *
 * Het adres komt uit de bijbehorende View-klasse,
 * met een van de methodes:
 * * waardeEmail
 *
 * @param Entiteit $obj
 * Het object waarvan een emailadres wordt gegenereerd.
 *
 * @return string|null
 * Het emailadres van het object, of NULL.
 */
function geefEmail($obj)
{
	//pak de bijpassende View klasse en gebruik de juiste emailfunctie.
	$klasse = get_class($obj) . 'View';
	if (method_exists($klasse, 'waardeEmail'))
	{
		return $klasse::waardeEmail($obj);
	}

	return null;
}

//Checkt of een CP een alumnus is, spaart veel static aanroepen uit.
function isAlumnus(ContactPersoon $persoon)
{
	return strtoupper($persoon->getType()) == "ALUMNUS";
}

/**
 *  Is dit een absolute URL?
 *
 * Bijvoorbeeld als je in Publisher zit en relatieve URL's voor plaatjes goed
 * wil zetten, maar niet mailto:bla@example.com wil slopen.
 *
 * @param url Een string met de url erin.
 *
 * @return TRUE als het absoluut is, FALSE als het relatief is.
 */
function isAbsoluteURL($url)
{
	// Lege strings hoeven zeker geen http://a-es2.nl/ ervoor te krijgen.
	// (overigens zou ik me moeten schamen als je dit ooit moet aanpassen
	// om een bug te fixen, want dit is uiteraard best wel een randgeval)
	if (!$url)
	{
		return true;
	}
	// De URL moet beginnen met een /
	if ($url[0] == '/')
	{
		return true;
	}
	// of met een protocolnaam
	return preg_match('/^[a-zA-Z0-9]+:/', $url);
}

function maakGrafiek($labels, $dataset)
{
	$colors = ['red', 'blue', 'green', 'purple', 'yellow', 'darkred', 'darkgreen', 'pink', 'teal', 'orange'];

	$id = uniqid();
	$str = '<canvas id="' . $id . '"></canvas>';

	$datasets = [];
	$i = 0;
	foreach ($dataset as $key => $data)
	{
		$datasets[] = ['label' => $key, 'borderWidth' => 3, 'pointRadius' => 5, 'data' => $data, 'borderColor' => $colors[$i % count($colors)], 'backgroundColor' => 'transparent'];
		$i++;
	}

	$data = ['labels' => $labels, 'datasets' => $datasets];

	$str .= HtmlScript::makeJavascript('
$(function() {
	var ctx = document.getElementById("' . $id . '").getContext("2d");
	var myLineChart = new Chart(ctx, {
		type: "line",
		data: ' . json_encode($data) . ',
	});
});
'
	);

	return $str;
}
