<?
class Voornaamwoord
	extends Voornaamwoord_Generated
{
	/** Verzamelingen **/
	protected $persoonVerzameling;
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Voornaamwoord_Generated

		$this->woordID = NULL;
		$this->persoonVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * \brief Returneert de PersoonVerzameling die hoort bij dit object
	 */
	public function getPersoonVerzameling()
	{
		if(!$this->persoonVerzameling instanceof PersoonVerzameling)
		{
			$this->persoonVerzameling = new PersoonVerzameling();
		}
		return $this->persoonVerzameling;
	}

	/**
	 * @brief Geef de vervoeging van dit voornaamwoord adhv een voorbeeldwoord.
	 *
	 * @param voorbeeld Een van 'jij', 'jou', 'jouw' of 'jezelf'.
	 * @param lang De taal om in te vervoegen. Default: getLang().
	 *
	 * @return Een string met de vervoeging.
	 */
	public function vervoeg($voorbeeld, $lang=NULL) {
		switch ($voorbeeld) {
		case 'jij':
			return $this->getOnderwerp($lang);
		case 'jou':
			return $this->getVoorwerp($lang);
		case 'jouw':
			return $this->getBezittelijk($lang);
		case 'jezelf':
			return $this->getWederkerend($lang);
		default:
			user_error("Onbekende vervoeging $voorbeeld", E_USER_ERROR);
		}
	}

	public function magVerwijderen() {
		return hasAuth('promocie'); // TODO: denk na wie mag verwijderen
	}

	/**
	 * @brief Doe een slag naar voornaamwoorden op basis van het geslacht.
	 * Het bestaan van deze functie is niet heel fijn,
	 * maar het is een hoop werk om de introsite de API te laten gebruiken
	 * om hun dropdown te laten vullen.
	 *
	 * @param geslacht Een van de waarden van Persoon::enumGeslacht.
	 * @return Een Voornaamwoord dat past bij het geslacht.
	 */
	public static function uitGeslacht($geslacht) {
		switch ($geslacht) {
			default:
				user_error("Onbekend geslacht $geslacht", E_USER_WARNING);
				// fallthrough
			case '':
			case '?':
				// TODO SOEPMES: is er iets beter dan hardcoden in dit geval?
				return Voornaamwoord::geef(0);
			case 'M':
				return Voornaamwoord::geef(1);
			case 'V':
				return Voornaamwoord::geef(2);
		}
	}

	/** Reset voornaamwoord naar nummer 0
	 *
	 * @inheritdoc
	 */
	public function verwijderen($foreigncall = null)
	{
		global $WSW4DB;

		if(!$this->inDB)
		{
			throw new LogicException('object is not inDB');
		}

		$magVerwijderen = $this->magVerwijderen();

		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		$WSW4DB->q('START TRANSACTION');

		$WSW4DB->q('UPDATE Persoon SET voornaamwoord_woordID = %i WHERE voornaamwoord_woordID = %i',0,$this->geefID());

		$WSW4DB->q('DELETE FROM `Voornaamwoord`'
				.' WHERE `woordID` = %i'
				.' LIMIT 1'
				, $this->woordID
			  );

		$WSW4DB->q('COMMIT');
	}
}
