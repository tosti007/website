<?

/***
 * $Id$
 */
class Wachtwoord
	extends Wachtwoord_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a, $b = null)
	{
		parent::__construct($a, $b);
	}

	static public function loginToContactID($login)
	{
		global $WSW4DB;
		$id = $WSW4DB->q('MAYBEVALUE SELECT `persoon_contactID`'
		                .' FROM `Wachtwoord`'
		                .' WHERE `login` = %s'
		                , $login
		                );
		return $id;
	}

	static public function geefLogin($login)
	{
		$id = self::loginToContactID($login);
		if(!$id) return NULL;

		return self::geef($id);
	}

	static public function geefMagic($magic)
	{
		global $WSW4DB;

		$id = $WSW4DB->q('MAYBEVALUE SELECT `persoon_contactID`'
		                .' FROM `Wachtwoord`'
		                .' WHERE `magic` = %s'
						.'   AND `magicExpire` > NOW()'
		                , $magic
		                );
		if(!$id) return NULL;

		return self::geef($id);
	}

	/***
	 *  LET OP: doet geen checks en gooit het gehashde & gesalte ww de db in.
	 */
	private function setMagic($generateNew)
	{
		if($generateNew) {
			$this->magic = randstr(32);
			$this->magicExpire = new DateTimeLocale('+1 hour');
		} else {
			$this->magic = NULL;
			$this->magicExpire = new DateTimeLocale(NULL);
		}
	}
	public function removeMagic()
	{
		$this->setMagic(false);
	}

	/** METHODEN **/
	public function magBekijken()
	{
		return $this->magWijzigen();
	}

	public function magWijzigen($viaMagic = false)
	{
		global $auth;

		if($auth->getLidnr() == $this->getPersoonContactID())
			return True;		// altijd je eigen wachtwoord wijzigen

		if(!hasAuth('bestuur') && !$viaMagic) //als er een magische code is ingevuld hoeft er niet op authorisatie gecheckt te worden
			return False;
		// else: level >= bestuur

		if($auth->isGodAchtig($this->getPersoonContactID()))
			return False;		// niet het ww van een (andere) GOD wijzigen

		if(hasAuth('god'))
			return True;
		// else: level == bestuur

		if($auth->isBestuur($this->getPersoonContactID()))
			return False;		// bestuur mag niet aan elkaars ww zitten

		return True;
	}

	public function magVerwijderen()
	{
		return hasAuth('bestuur') && $this->magWijzigen();
	}

	public function heeftWachtwoord()
	{
		//md5 bestaat en is geset, of cypthash bestaat en is geset
		return isset($this->wachtwoord) && $this->wachtwoord || isset($this->crypthash) && $this->crypthash;
	}

	public static function geefOfNieuw($contactID)
	{
		$ww = self::geef($contactID);
		if($ww)
			return $ww;

		$ww = new Wachtwoord($contactID);
		$ww->setLogin('lid'.$contactID);
		$ww->opslaan();

		return $ww;
	}

	/**
	 *	 Controleer of dat het wachtwoord goed is.
	 *	@param wachtwoord Het wachtwoord om te checken.
	 *	@return True bij goed, False bij fout.
	 */
	public function authenticate($wachtwoord)
	{
		//Help, we hebben nu twee methodes om in te loggen:
		//md5 (uitfaseren), en bcrypt
		global $WSW4DB;

		$login_ok = false;
		if($this->crypthash)
		{
			//BCrypt
			$bc = new BCryptHandler();
			$login_ok = $bc->verifieer(SITE_WIDE_SALT . $wachtwoord, $this->crypthash);
		}
		else {
			//MD5 (legacy)
			$p = $this->wachtwoord;
			$login_ok = (strlen($p) == 36 &&
						md5($wachtwoord . substr($p, 0, 4)) == substr($p, 4));

			//Overstappen naar BCrypt:
			if($login_ok)
			{
				$bc = new BCryptHandler();
				$crypthash = $bc->nieuweHash(SITE_WIDE_SALT . $wachtwoord);
				$this->crypthash = $crypthash;
				$this->wachtwoord = null;
				$this->opslaan();
			}
		}

		if($login_ok === true && $this->getPersoon() instanceof Persoon)
		{
			$WSW4DB->q('UPDATE LOW_PRIORITY `Wachtwoord`'
					. ' SET `laatsteLogin` = NOW()'
					. ' WHERE `persoon_contactID` = %i'
					, $this->getPersoonContactID()
					);
			return $this->getPersoonContactID();
		}

		//TODO Dit is het punt om php-side iets aan DOS-aanvallen op je login
		// veldje te doen Voor nu een kleine sleep waardoor het proces 10x zo
		// lang duurt om te doorlopen zonder dat een gebruiker er echt last van
		// heeft. Dit is niet een echte oplossing en iets slims moet nog worden
		// bedacht! (iets met apache?)
		sleep(1); 

		return false;
	}

	public function wijzigWachtwoord($nieuw, $viaMagic = false)
	{
		unset($this->errors['Wachtwoord']);
		if(!$this->magWijzigen($viaMagic)) {
			$this->errors['Wachtwoord'] = _('Onvoldoende rechten om wachtwoord te wijzigen.');
			return $this;
		}

		if(empty($nieuw)) {
			$this->wachtwoord = NULL;
		} else {
			$bc = new BCryptHandler();
			$this->crypthash = $bc->nieuweHash(SITE_WIDE_SALT . $nieuw);
			$this->wachtwoord = null;
			$this->dirty = true;
		}
		return $this;
	}

	public function wijzigMagicWachtwoord($magic, $wachtwoord)
	{
		unset($this->errors['Wachtwoord']);

		if(empty($this->magic)) {
			$this->errors['Wachtwoord'] = _('Geen verificatie magie.');
			return $this;
		}
		if($this->magic != $magic) {
			$this->errors['Wachtwoord'] = _('Verificatie magie fout.');
			return $this;
		}

		$this->wijzigWachtwoord($wachtwoord);
		return True;
	}

	public function checkWachtwoord()
	{
		if($ret = parent::checkWachtwoord())
			return $ret;

		if($this->getUnixLogin())
			return _('Website login is gekoppeld aan systeem login.');
		if(strlen($w1) < 8)
			return _('Wachtwoord moet tenminste 8 tekens zijn.');

		return true;
	}

	public function checkLogin()
	{
		if($ret = parent::checkLogin())
			return $ret;

		if(strlen($this->login) < 3)
			return _('Login moet tenminste 3 tekens zijn!');
		if(strlen($this->login) > 16)
			return _('Login mag maximaal 16 tekens zijn!');

		// login 'lid<lidnr>' reserveren
		if(substr($this->login, 0, 3) == 'lid'
		&& $this->getPersoonContactID() != (int)substr($this->login, 3))
			return sprintf(_("De login \"%s\" is gereserveerd."), $this->login);

		$id = self::loginToContactID($this->login);
		if(isset($id) && $id != $this->getPersoonContactID())
			return _('Login is al in gebruik, kies een andere.');

		return false;
	}

	public function opslaan($classname = 'Wachtwoord')
	{
		global $auth;

		if($auth->getLidnr() || !$this->getInDB()) //Als het object nog niet in de DB staat werkt naarDB niet, dus meteen opslaan
			return parent::opslaan($classname);
		//else: niet ingelogd, en 'gast' mag maar een paar velden aan passen

		if(!$this->valid())
			throw new LogicException('Not all constraints passed.');

		$this->naarDB('login', $this->login, NULL);
		$this->naarDB('wachtwoord', $this->wachtwoord, NULL);

		$this->naarDB('magic', $this->magic, NULL);
		$this->naarDB('magicExpire', $this->magicExpire->strftime('%F %T'), NULL);

		$this->naarDB('crypthash', $this->crypthash, NULL);

		$this->dirty = false;
	}

	public function sendMagicMail()
	{
		global $request, $BESTUUR;

		$lidnr = $this->getPersoonContactID();
		$email = $this->getPersoon()->getEmail();

		if(!$email)
		{
			sendmail(WWWEMAIL, $BESTUUR['secretaris']->getEmail()
			, "Lid $lidnr heeft nog geen e-mailadres"
			, "Er is geen e-mailadres bekend van (oud-)lid $lidnr, terwijl"
			. "dit (oud-)lid zojuist een login heeft proberen aan te vragen.\n"
			. "Kun je hier iets aan doen?\n"
			. "\n"
			. "(automatisch gemaild door de website)"
			);

			return False;
		}

		$this->setMagic(true);
		$this->opslaan();

		$ip = $request->server->get('REMOTE_ADDR');
		$ip = str_replace('.','a',$ip);
		$ip = base_convert($ip,11,36);
		$time = base_convert($this->gewijzigdWanneer->getTimestamp(), 10, 36);
		$text = _("Beste A–Eskwadrater,

Je hebt zojuist een nieuwe login aangevraagd voor de website van
A–Eskwadraat, of je was je wachtwoord vergeten.

Je magische code is: %s
Vul dit in (met kopieren en plakken), en je kunt een (nieuwe) loginnaam
en wachtwoord kiezen.

Als je de browser al afgesloten hebt, ga dan naar:
%s
Dit kan nog tot 1 uur na ontvangst van deze e-mail. Daarna zul je
opnieuw je login moeten aanvragen.

Als je geen login hebt aangevraagd, heeft iemand waarschijnlijk een
vergissing gemaakt. Je kunt dan dit e-mailtje gewoon negeren, maar wij
zouden het fijn vinden als je hem even doorstuurt naar %s,
dan kunnen wij eventueel maatregelen nemen. %s

Groeten,

De A–Eskwadraatwebsite");
		$email_bericht = sprintf($text
				, $this->magic
				, "http://www.a-eskwadraat.nl/Service/Intern/Logindata"
				, WWWEMAIL
				, "($ip/$time)"
				);

		sendmail($BESTUUR['secretaris']->getEmail()
			, $email, _("Nieuwe login")
			, $email_bericht, $BESTUUR['secretaris']->getEmail()
			, '', $BESTUUR['secretaris']->getEmail()
			);

		return True;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
