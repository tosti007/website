<?
/**
 * $Id$
 */
class PersoonVoorkeur
	extends PersoonVoorkeur_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a)
	{
		parent::__construct($a); // PersoonVoorkeur_Generated
	}

	/**
	 * Controleer of alle voorkeurswaarden de default waarde zijn.
	 *
	 * @return True als alles default is; False anders.
	 */
	public function isDefault ()
	{
		$defaults = new PersoonVoorkeur($this->getPersoon());

		if ($this->difference($defaults))
			return False;

		return True;
	}

	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}

	public function inverteerBugSortering ()
	{
		$oudeSortering = $this->getBugSortering();

		if ($oudeSortering == 'ASC')
		{
			$nieuweSortering = 'DESC';
		}
		else if ($oudeSortering == 'DESC')
		{
			$nieuweSortering = 'ASC';
		}
		else
		{
			user_error("Onbekende bugsortering $oudeSortering");
		}

		$this->setBugSortering($nieuweSortering);
	}

	/**
	 * Geef de voorkeuren van een persoon.
	 *
	 * @param persoon Een Persoon.
	 * @return Het PersoonVoorkeur-object uit de database dat bij persoon hoort, of eentje met default-waarden als die niet gevonden wordt.
	 */
	public static function vanPersoon (Persoon $persoon)
	{
		$voorkeur = self::geef($persoon->geefID());

		if ($voorkeur instanceof PersoonVoorkeur)
			return $voorkeur;

		return new PersoonVoorkeur($persoon);
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
