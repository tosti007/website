<?
class ActiviteitHerhaling
	extends ActiviteitHerhaling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($act = null)
	{
		parent::__construct($act); // ActiviteitHerhaling_Generated
	}

	public function magBekijken()
	{
		//Als de activiteit publiek is mag het altijd
		if ($this->getToegang() == 'PUBLIEK')
			return true;

		//En anders moet je de activiteit beheren, of er voor ingeschreven staan
		return $this->magWijzigen() || (Lid::getIngelogd() && $this->isDeelnemer(Lid::getIngelogd()));
	}

	/**
	 *  Promoveert een herhalende activiteit naar een activiteitinformatie
	 *
	 * @param newAct De nieuwe activiteit die de herhalende act wordt
	 * @param cieActs De CommissieActiviteitVerzameling waar de nieuwe CommissieActiviteiten in komen
	 * @param vragen De ActiviteitVraagVerzameling waar de nieuwe vragen in komen
	 *
	 * @return this
	 */
	public function promoveer(ActiviteitInformatie $newAct,
		CommissieActiviteitVerzameling $cieActs,
		ActiviteitVraagVerzameling $vragen)
	{
		// Erken de nieuwe activiteit als kind,
		// zodat de check$veld-methoden ermee rekening mee kunnen houden.
		$newAct->setPromotiePapa($this);

		// Haal alle commissies op, zodat deze gekoppeld kunnen worden aan de gepromoveerde activiteit
		$papaCies = $this->getActiviteitCommissies();
		foreach($papaCies as $cieAct)
		{
			$MijnCieAct = new CommissieActiviteit($cieAct->getCommissie(), $newAct);
			$cieActs->voegtoe($MijnCieAct);
		}

		// haal de vragen op
		$papaVragen = $this->getVragen();
		foreach($papaVragen as $vraag)
		{
			$newVraag = new ActiviteitVraag($newAct);
			$newVraag->setVraag($vraag->getVraag('nl'), 'nl');
			$newVraag->setVraag($vraag->getVraag('en'), 'en');
			$vragen->voegtoe($newVraag);
		}

		$info = $this->getInformatie();

		// Stel alle velden in
		$newAct->setTitel($info->getTitel('nl'), 'nl');
		$newAct->setTitel($info->getTitel('en'), 'en');
		$newAct->setWerftekst($info->getWerftekst('nl'), 'nl');
		$newAct->setWerftekst($info->getWerftekst('en'), 'en');
		$newAct->setLocatie($info->getLocatie('nl'), 'nl');
		$newAct->setLocatie($info->getLocatie('en'), 'en');
		$newAct->setPrijs($info->getPrijs('nl'), 'nl');
		$newAct->setPrijs($info->getPrijs('en'), 'en');
		$newAct->setHomepage($info->getHomepage());
		$newAct->setInschrijfbaar($info->getInschrijfbaar());
		$newAct->setDatumInschrijvenMax($info->getDatumInschrijvenMax());
		$newAct->setDatumUitschrijven($info->getDatumUitschrijven());
		$newAct->setMaxDeelnemers($info->getMaxDeelnemers());
		$newAct->setToegang($info->getToegang());
		$newAct->setActsoort($info->getActsoort());
		$newAct->setOnderwijs($info->getOnderwijs());

		$newAct->setMomentBegin($this->getMomentBegin());
		$newAct->setMomentEind($this->getMomentEind());
		$newAct->setAantalComputers($this->getAantalComputers());
		$newAct->setStuurMail($this->getStuurMail());
		$newAct->setCategorie($this->getCategorie());
		return $this;
	}

	public function getActsoort()
	{
		return $this->getInformatie()->getActsoort();
	}

	public function getInschrijfbaar()
	{
		return $this->getInformatie()->getInschrijfbaar();
	}

	public function getDatumUitschrijven()
	{
		return $this->getInformatie()->getDatumUitschrijven();
	}

	public function getDatumInschrijvenMax()
	{
		return $this->getInformatie()->getDatumInschrijvenMax();
	}

	public function getMaxDeelnemers()
	{
		return $this->getInformatie()->getMaxDeelnemers();
	}

	protected static function veldenActiviteitHerhaling($welke = NULL)
	{
		switch($welke) {
		case 'set':
			$velden = array('AantalComputers');
			break;
		default:
			$velden = parent::veldenActiviteitHerhaling($welke);
			break;
		}

		return $velden;
	}

	public function checkAantalComputers()
	{
		$waarde = $this->getAantalComputers();

		if($waarde < 0)
			return _('Er moeten minstens 0 computers worden gereserveerd');
		if (is_null($eind = $this->getMomentEind()))
			return _('Er is geen einddatum opgegeven.');
		if (is_null($begin = $this->getMomentBegin()))
			return _('Er is geen begindatum opgegeven.');
		if($waarde == 0)
			return false;

		$max = $this->getMaxAantalComputers($begin, $eind);

		if ($waarde > $max)
			return _('Er kunnen maximaal '.$max.' computers gereserveerd worden');

		return false;
	}

	public function getAantalComputers()
	{
		$act = $this->getInformatie();
		return $act->getAantalComputers();
	}

	public function getStuurMail()
	{
		return $this->getInformatie()->getStuurMail();
	}

	public function getCategorie()
	{
		return $this->getInformatie()->getCategorie();
	}
}
