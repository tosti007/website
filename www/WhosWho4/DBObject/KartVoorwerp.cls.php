<?
/**
 * $Id$
 */
class KartVoorwerp
	extends KartVoorwerp_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($persoonkart = null)
	{
		parent::__construct($persoonkart); // KartVoorwerp_Generated
	}

	public function magVerwijderen()
	{
		return Persoon::getIngelogd() == $this->getPersoonKart()->getPersoon();
	}

	public function magWijzigen()
	{
		return Persoon::getIngelogd() == $this->getPersoonKart()->getPersoon()
			|| hasAuth('bestuur');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
