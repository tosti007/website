<?

/***
 * $Id$
 */
class CommissieLid
	extends CommissieLid_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}

	/** METHODEN **/

	/**
	 *	 Is dit een actieve relatie?
	 *	@return True of False
	 */
	public function isActief()
	{
		if(time() < $this->getDatumBegin())
			return False;
		if(!$this->getDatumEind())
			return True;
		return (time() < $this->getDatumEind());
	}
	
	public function url()
	{
		$cie = $this->getCommissie();
		return $cie->systeemUrl() . '/Leden/' . $this->getPersoon()->geefID();
	}

	/**
	 * Returneert of  het ingelogd lid het recht heeft om dit commissieLid te wijzigen.
	 */
	public function magWijzigen ()
	{
		return CommissieLid::magKlasseWijzigen() || $this->getPersoon() === Persoon::getIngelogd();
	}
	public function magVerwijderen ()
	{
		return hasAuth('bestuur') || $this->getPersoon() === Persoon::getIngelogd();
}
	
	public function magBekijken()
	{
		if ($this->magWijzigen())
			return true;
		//Alleen lid objecten hebben een opzoekbaarheid, dus check hier even op.
		if($this->getPersoon() instanceof Lid) {
			return $this->getPersoon()->magbekijken();
		} else {
			return false;
		}
	}

	/*
	 *	Override omdat we meer velden willen hebben dan alleen uit het commissielidobject.
	 *	We willen ook namen en dingen uit de kart hebben.
	 */
	protected static function veldenCommissieLid($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
		case 'nieuwLid':
			// Voeg hier je kartselectie toe en een commissieselect
			$velden[] = "Persoon";
			$velden[] = "Commissie";
			$velden = array_merge($velden, parent::veldenCommissieLid($welke));
			break;
		default:
			return parent::veldenCommissieLid($welke);
		}

		return $velden;
	}

	public function checkDatumEind()
	{
		if ($ret = parent::checkDatumEind())
			return $ret;
		if (($this->getDatumEind()->hasTime()) && $this->getDatumEind() <= $this->getDatumBegin())
			return _('Einddatum moet later zijn dan begindatum');
	}

	public function checkPersoon()
	{
		if(is_null($this->persoon_contactID && is_null($this->persoon)))
			return _('Er moet een lid geset zijn');
		if(is_null($this->persoon) && !Persoon::geef($this->persoon_contactID))
			return _('Er is geen lid geset');
		if(is_null($this->persoon_contactID) && !$this->persoon->valid())
			return _('Het lid is niet geldig');

		return false;
	}

	public function checkCommissie()
	{
		if(is_null($this->commissie_commissieID && is_null($this->commissie)))
			return _('Er moet een cie geset zijn');
		if(is_null($this->commissie) && !Commissie::geef($this->commissie_commissieID))
			return _('Er is geen cie geset');
		if(is_null($this->commissie_commissieID) && !$this->commissie->valid())
			return _('De cie is niet geldig');

		return false;
	}

	public function voegOpSysteemToe()
	{
		$meldingen = array('fout' => array(), 'succes' => array(), 'waarschuwing' => array());

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s automatisch '
				. 'in de commissie op \'Het Systeem\' te stoppen! Dit moet handmatig gebeuren!'), $persoonLink);
			return $meldingen;
		}

		$commissie = $this->getCommissie();
		$lid = $this->getPersoon();
		$persoonLink = PersoonView::makeLink($lid);

		$uid = $conn->getUid($lid);

		if($uid)
		{
			$return = $conn->voegToeAanCie($uid, $commissie);

			if($return)
			{
				foreach($return as $code => $fout)
				{
					switch($code)
					{
					case '1':
						$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s '
							. 'toe te voegen op \'Het Systeem\', dit moet handmatig gebeuren!'),
							$persoonLink);
						break;
					case '2':
						$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s '
							. 'aan de forward toe te voegen op \'Het Systeem\', dit moet handmatig gebeuren!'),
							$persoonLink);
						break;
					case '3':
						$meldingen['fout'][] = sprintf(_('Het is niet gelukt om bij %s '
							. 'de cie te linken op \'Het Systeem\', dit moet handmatig gebeuren!'),
							$persoonLink);
						break;
					default:
						$meldingen['fout'][] = sprintf(_('Onbekende fout: <pre>%s</pre>'), htmlspecialchars(implode("\n", $fout)));
						break;
					}
				}
			}
			else
				$meldingen['succes'][] = sprintf(_('%s succesvol toegevoegd aan '
					. 'het de commissie op \'Het Systeem\'!'), $persoonLink);
		}
		else
		{
			$email = $this->getPersoon()->getEmail();

			if($email)
			{
				$return = $conn->voegEmailToeAanCie($email, $commissie);

				if($return)
				{
					$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s '
						. 'aan de forward toe te voegen op \'Het Systeem\', dit '
						. 'moet handmatig gebeuren! De fout is: <pre>%s</pre>'),
						$persoonLink,
						htmlspecialchars(implode("\n", $return[1])));
				}
				else
					$meldingen['waarschuwing'][] = sprintf(_('%s heeft nog geen systeemaccount, '
						. 'het email is op de forward geplaatst op \'Het Systeem\'.'), $persoonLink);
			}
			else
				$meldingen['fout'][] = sprintf(_('%s heeft nog geen systeemaccount, '
					. 'maar er is ook geen email gevonden!'), $persoonLink);
		}

		return $meldingen;
	}

	public function haalOpSysteemWeg()
	{
		$meldingen = array('fout' => array(), 'succes' => array(), 'waarschuwing' => array());

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s automatisch '
				. 'in de commissie op \'Het Systeem\' te stoppen! Dit moet handmatig gebeuren!'), $persoonLink);
			return $meldingen;
		}

		$commissie = $this->getCommissie();
		$lid = $this->getPersoon();
		$persoonLink = PersoonView::makeLink($lid);

		$uid = $conn->getUid($lid);

		if($uid)
		{
			$return = $conn->haalWegUitCie($uid, $commissie);

			if($return)
			{
				foreach($return as $code => $fout)
				{
					switch($code)
					{
					case '1':
						$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s '
							. 'weg te halen van \'Het Systeem\', dit moet handmatig gebeuren!'),
							$persoonLink);
						break;
					case '2':
						$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s '
							. 'van de forward te halen op \'Het Systeem\', dit moet handmatig gebeuren!'),
							$persoonLink);
						break;
					case '3':
						$meldingen['fout'][] = sprintf(_('Het is niet gelukt om bij %s '
							. 'de cie-link weg te halen op \'Het Systeem\', dit moet handmatig gebeuren!'),
							$persoonLink);
						break;
					default:
						$meldingen['fout'][] = sprintf(_('Onbekende fout: <pre>%s</pre>'), htmlspecialchars(implode("\n", $fout)));
						break;
					}
				}
			}
			else
				$meldingen['succes'][] = sprintf(_('%s succesvol weggehaald uit '
					. 'de commissie op \'Het Systeem\'!'), $persoonLink);
		}
		else
		{
			$email = $this->getPersoon()->getEmail();

			if($email)
			{
				$return = $conn->haalEmailWegUitCie($email, $commissie);

				if($return)
				{
					$meldingen['fout'][] = sprintf(_('Het is niet gelukt om %s '
						. 'van de forward af te halen op \'Het Systeem\', dit '
						. 'moet handmatig gebeuren! De fout is: <pre>%s</pre>'),
						$persoonLink,
						htmlspecialchars(implode("\n", $return[1])));
				}
				else
					$meldingen['waarschuwing'][] = sprintf(_('%s heeft nog geen systeemaccount, '
						. 'het email is van de forward verwijderd op \'Het Systeem\'.'), $persoonLink);
			}
			else
				$meldingen['fout'][] = sprintf(_('%s heeft nog geen systeemaccount, '
					. 'maar er is ook geen email gevonden!'), $persoonLink);
		}

		return $meldingen;
	}
}

