<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class ContactTelnr_Generated
	extends Entiteit
{
	protected $telnrID;					/**< \brief PRIMARY */
	protected $contact;
	protected $contact_contactID;		/**< \brief PRIMARY */
	protected $telefoonnummer;			/**< \brief Moet in de implementatie worden gecheckt op correctheid. */
	protected $soort;					/**< \brief ENUM:THUIS/MOBIEL/WERK/OUDERS/RECEPTIE/FAX */
	protected $voorkeur;				/**< \brief voorkeurs telnr (1 of geen op True) */
	/**
	/**
	 * @brief De constructor van de ContactTelnr_Generated-klasse.
	 *
	 * @param mixed $a Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Contact)
		{
			$this->contact = $a;
			$this->contact_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->telnrID = NULL;
		$this->telefoonnummer = '';
		$this->soort = 'THUIS';
		$this->voorkeur = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Contact';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld telnrID.
	 *
	 * @return int
	 * De waarde van het veld telnrID.
	 */
	public function getTelnrID()
	{
		return $this->telnrID;
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @return Contact
	 * De waarde van het veld contact.
	 */
	public function getContact()
	{
		if(!isset($this->contact)
		 && isset($this->contact_contactID)
		 ) {
			$this->contact = Contact::geef
					( $this->contact_contactID
					);
		}
		return $this->contact;
	}
	/**
	 * @brief Geef de waarde van het veld contact_contactID.
	 *
	 * @return int
	 * De waarde van het veld contact_contactID.
	 */
	public function getContactContactID()
	{
		if (is_null($this->contact_contactID) && isset($this->contact)) {
			$this->contact_contactID = $this->contact->getContactID();
		}
		return $this->contact_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld telefoonnummer.
	 *
	 * @return string
	 * De waarde van het veld telefoonnummer.
	 */
	public function getTelefoonnummer()
	{
		return $this->telefoonnummer;
	}
	/**
	 * @brief Stel de waarde van het veld telefoonnummer in.
	 *
	 * @param mixed $newTelefoonnummer De nieuwe waarde.
	 *
	 * @return ContactTelnr
	 * Dit ContactTelnr-object.
	 */
	public function setTelefoonnummer($newTelefoonnummer)
	{
		unset($this->errors['Telefoonnummer']);
		if(!is_null($newTelefoonnummer))
			$newTelefoonnummer = trim($newTelefoonnummer);
		if($newTelefoonnummer === "")
			$newTelefoonnummer = NULL;
		if($this->telefoonnummer === $newTelefoonnummer)
			return $this;

		$this->telefoonnummer = $newTelefoonnummer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld telefoonnummer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld telefoonnummer geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTelefoonnummer()
	{
		if (array_key_exists('Telefoonnummer', $this->errors))
			return $this->errors['Telefoonnummer'];
		$waarde = $this->getTelefoonnummer();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('THUIS','MOBIEL','WERK','OUDERS','RECEPTIE','FAX');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return ContactTelnr
	 * Dit ContactTelnr-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld voorkeur.
	 *
	 * @return bool
	 * De waarde van het veld voorkeur.
	 */
	public function getVoorkeur()
	{
		return $this->voorkeur;
	}
	/**
	 * @brief Stel de waarde van het veld voorkeur in.
	 *
	 * @param mixed $newVoorkeur De nieuwe waarde.
	 *
	 * @return ContactTelnr
	 * Dit ContactTelnr-object.
	 */
	public function setVoorkeur($newVoorkeur)
	{
		unset($this->errors['Voorkeur']);
		if(!is_null($newVoorkeur))
			$newVoorkeur = (bool)$newVoorkeur;
		if($this->voorkeur === $newVoorkeur)
			return $this;

		$this->voorkeur = $newVoorkeur;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voorkeur geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voorkeur geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkVoorkeur()
	{
		if (array_key_exists('Voorkeur', $this->errors))
			return $this->errors['Voorkeur'];
		$waarde = $this->getVoorkeur();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ContactTelnr::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ContactTelnr.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ContactTelnr::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTelnrID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return ContactTelnr|false
	 * Een ContactTelnr-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$telnrID = (int)$a[0];
		}
		else if(isset($a))
		{
			$telnrID = (int)$a;
		}

		if(is_null($telnrID))
			throw new BadMethodCallException();

		static::cache(array( array($telnrID) ));
		return Entiteit::geefCache(array($telnrID), 'ContactTelnr');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'ContactTelnr');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('ContactTelnr::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `ContactTelnr`.`telnrID`'
		                 .     ', `ContactTelnr`.`contact_contactID`'
		                 .     ', `ContactTelnr`.`telefoonnummer`'
		                 .     ', `ContactTelnr`.`soort`'
		                 .     ', `ContactTelnr`.`voorkeur`'
		                 .     ', `ContactTelnr`.`gewijzigdWanneer`'
		                 .     ', `ContactTelnr`.`gewijzigdWie`'
		                 .' FROM `ContactTelnr`'
		                 .' WHERE (`telnrID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['telnrID']);

			$obj = new ContactTelnr(array($row['contact_contactID']));

			$obj->inDB = True;

			$obj->telnrID  = (int) $row['telnrID'];
			$obj->telefoonnummer  = trim($row['telefoonnummer']);
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->voorkeur  = (bool) $row['voorkeur'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ContactTelnr')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getContactContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->telnrID =
			$WSW4DB->q('RETURNID INSERT INTO `ContactTelnr`'
			          . ' (`contact_contactID`, `telefoonnummer`, `soort`, `voorkeur`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i, %s, %i)'
			          , $this->contact_contactID
			          , $this->telefoonnummer
			          , $this->soort
			          , $this->voorkeur
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'ContactTelnr')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ContactTelnr`'
			          .' SET `contact_contactID` = %i'
			          .   ', `telefoonnummer` = %s'
			          .   ', `soort` = %s'
			          .   ', `voorkeur` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `telnrID` = %i'
			          , $this->contact_contactID
			          , $this->telefoonnummer
			          , $this->soort
			          , $this->voorkeur
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->telnrID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenContactTelnr
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenContactTelnr($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenContactTelnr($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Telefoonnummer';
			$velden[] = 'Soort';
			$velden[] = 'Voorkeur';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Telefoonnummer';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Contact';
			$velden[] = 'Telefoonnummer';
			$velden[] = 'Soort';
			$velden[] = 'Voorkeur';
			break;
		case 'get':
			$velden[] = 'Contact';
			$velden[] = 'Telefoonnummer';
			$velden[] = 'Soort';
			$velden[] = 'Voorkeur';
		case 'primary':
			$velden[] = 'contact_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Contact';
			$velden[] = 'Telefoonnummer';
			$velden[] = 'Soort';
			$velden[] = 'Voorkeur';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ContactTelnr`'
		          .' WHERE `telnrID` = %i'
		          .' LIMIT 1'
		          , $this->telnrID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->telnrID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `ContactTelnr`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `telnrID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->telnrID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van ContactTelnr terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'telnrid':
			return 'int';
		case 'contact':
			return 'foreign';
		case 'contact_contactid':
			return 'int';
		case 'telefoonnummer':
			return 'string';
		case 'soort':
			return 'enum';
		case 'voorkeur':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'telnrID':
		case 'contact_contactID':
		case 'voorkeur':
			$type = '%i';
			break;
		case 'telefoonnummer':
		case 'soort':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ContactTelnr`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `telnrID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->telnrID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ContactTelnr`'
		          .' WHERE `telnrID` = %i'
		                 , $veld
		          , $this->telnrID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'ContactTelnr');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
