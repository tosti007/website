<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class Deelnemer_Generated
	extends Entiteit
{
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $activiteit;				/**< \brief PRIMARY */
	protected $activiteit_activiteitID;	/**< \brief PRIMARY */
	protected $momentInschrijven;		/**< \brief LEGACY_NULL */
	/** Verzamelingen **/
	protected $deelnemerAntwoordVerzameling;
	/**
	/**
	 * @brief De constructor van de Deelnemer_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 * @param mixed $b Activiteit (Activiteit OR Array(activiteit_activiteitID) OR
	 * activiteit_activiteitID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Activiteit)
		{
			$this->activiteit = $b;
			$this->activiteit_activiteitID = $b->getActiviteitID();
		}
		else if(is_array($b))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->momentInschrijven = new DateTimeLocale();
		$this->deelnemerAntwoordVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		$volgorde[] = 'Activiteit';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @return Activiteit
	 * De waarde van het veld activiteit.
	 */
	public function getActiviteit()
	{
		if(!isset($this->activiteit)
		 && isset($this->activiteit_activiteitID)
		 ) {
			$this->activiteit = Activiteit::geef
					( $this->activiteit_activiteitID
					);
		}
		return $this->activiteit;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld activiteit_activiteitID.
	 */
	public function getActiviteitActiviteitID()
	{
		if (is_null($this->activiteit_activiteitID) && isset($this->activiteit)) {
			$this->activiteit_activiteitID = $this->activiteit->getActiviteitID();
		}
		return $this->activiteit_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld momentInschrijven.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld momentInschrijven.
	 */
	public function getMomentInschrijven()
	{
		return $this->momentInschrijven;
	}
	/**
	 * @brief Stel de waarde van het veld momentInschrijven in.
	 *
	 * @param mixed $newMomentInschrijven De nieuwe waarde.
	 *
	 * @return Deelnemer
	 * Dit Deelnemer-object.
	 */
	public function setMomentInschrijven($newMomentInschrijven)
	{
		unset($this->errors['MomentInschrijven']);
		if(!$newMomentInschrijven instanceof DateTimeLocale) {
			try {
				$newMomentInschrijven = new DateTimeLocale($newMomentInschrijven);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->momentInschrijven->strftime('%F %T') == $newMomentInschrijven->strftime('%F %T'))
			return $this;

		if(!$newMomentInschrijven->hasTime() && $this->momentInschrijven->hasTime())
			$this->errors['MomentInschrijven'] = _('dit is een verplicht veld');

		$this->momentInschrijven = $newMomentInschrijven;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld momentInschrijven geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld momentInschrijven geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMomentInschrijven()
	{
		if (array_key_exists('MomentInschrijven', $this->errors))
			return $this->errors['MomentInschrijven'];
		$waarde = $this->getMomentInschrijven();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de DeelnemerAntwoordVerzameling die hoort bij dit object.
	 */
	public function getDeelnemerAntwoordVerzameling()
	{
		if(!$this->deelnemerAntwoordVerzameling instanceof DeelnemerAntwoordVerzameling)
			$this->deelnemerAntwoordVerzameling = DeelnemerAntwoordVerzameling::fromDeelnemer($this);
		return $this->deelnemerAntwoordVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Deelnemer::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Deelnemer.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Deelnemer::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPersoonContactID()
		                      , $this->getActiviteitActiviteitID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return Deelnemer|false
	 * Een Deelnemer-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$persoon_contactID = (int)$a[0];
			$activiteit_activiteitID = (int)$a[1];
		}
		else if($a instanceof Persoon
		     && $b instanceof Activiteit)
		{
			$persoon_contactID = $a->getContactID();
			$activiteit_activiteitID = $b->getActiviteitID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$persoon_contactID = (int)$a;
			$activiteit_activiteitID = (int)$b;
		}

		if(is_null($persoon_contactID)
		|| is_null($activiteit_activiteitID))
			throw new BadMethodCallException();

		static::cache(array( array($persoon_contactID, $activiteit_activiteitID) ));
		return Entiteit::geefCache(array($persoon_contactID, $activiteit_activiteitID), 'Deelnemer');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Deelnemer');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Deelnemer::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Deelnemer`.`persoon_contactID`'
		                 .     ', `Deelnemer`.`activiteit_activiteitID`'
		                 .     ', `Deelnemer`.`momentInschrijven`'
		                 .     ', `Deelnemer`.`gewijzigdWanneer`'
		                 .     ', `Deelnemer`.`gewijzigdWie`'
		                 .' FROM `Deelnemer`'
		                 .' WHERE (`persoon_contactID`, `activiteit_activiteitID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['persoon_contactID']
			                  ,$row['activiteit_activiteitID']);

			$obj = new Deelnemer(array($row['persoon_contactID']), array($row['activiteit_activiteitID']));

			$obj->inDB = True;

			$obj->momentInschrijven  = new DateTimeLocale($row['momentInschrijven']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Deelnemer')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		$rel = $this->getActiviteit();
		if(!$rel->getInDB())
			throw new LogicException('foreign Activiteit is not in DB');
		$this->activiteit_activiteitID = $rel->getActiviteitID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Deelnemer`'
			          . ' (`persoon_contactID`, `activiteit_activiteitID`, `momentInschrijven`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i)'
			          , $this->persoon_contactID
			          , $this->activiteit_activiteitID
			          , $this->momentInschrijven->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Deelnemer')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Deelnemer`'
			          .' SET `momentInschrijven` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          .  ' AND `activiteit_activiteitID` = %i'
			          , $this->momentInschrijven->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          , $this->activiteit_activiteitID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDeelnemer
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDeelnemer($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDeelnemer($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'MomentInschrijven';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'MomentInschrijven';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'MomentInschrijven';
			break;
		case 'get':
			$velden[] = 'MomentInschrijven';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'activiteit_activiteitID';
			break;
		case 'verzamelingen':
			$velden[] = 'DeelnemerAntwoordVerzameling';
			break;
		default:
			$velden[] = 'MomentInschrijven';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle DeelnemerAntwoord-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DeelnemerAntwoordFromDeelnemerVerz = DeelnemerAntwoordVerzameling::fromDeelnemer($this);
		$returnValue = $DeelnemerAntwoordFromDeelnemerVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object DeelnemerAntwoord met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $DeelnemerAntwoordFromDeelnemerVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Deelnemer`'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `activiteit_activiteitID` = %i'
		          .' LIMIT 1'
		          , $this->persoon_contactID
		          , $this->activiteit_activiteitID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;
		$this->activiteit = NULL;
		$this->activiteit_activiteitID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Deelnemer`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          .  ' AND `activiteit_activiteitID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          , $this->activiteit_activiteitID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Deelnemer terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'activiteit':
			return 'foreign';
		case 'activiteit_activiteitid':
			return 'int';
		case 'momentinschrijven':
			return 'datetime';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'persoon_contactID':
		case 'activiteit_activiteitID':
			$type = '%i';
			break;
		case 'momentInschrijven':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Deelnemer`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `activiteit_activiteitID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->persoon_contactID
		          , $this->activiteit_activiteitID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Deelnemer`'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `activiteit_activiteitID` = %i'
		                 , $veld
		          , $this->persoon_contactID
		          , $this->activiteit_activiteitID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Deelnemer');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle DeelnemerAntwoord-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DeelnemerAntwoordFromDeelnemerVerz = DeelnemerAntwoordVerzameling::fromDeelnemer($this);
		$dependencies['DeelnemerAntwoord'] = $DeelnemerAntwoordFromDeelnemerVerz;

		return $dependencies;
	}
}
