<?
abstract class PersoonRating_Generated
	extends Entiteit
{
	protected $ratingObject;			/**< \brief PRIMARY */
	protected $ratingObject_ratingObjectID;/**< \brief PRIMARY */
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $rating;					/**< \brief ENUM:1/2/3/4/5/6/7/8/9/10 */
	/**
	/**
	 * @brief De constructor van de PersoonRating_Generated-klasse.
	 *
	 * @param mixed $a RatingObject (RatingObject OR Array(ratingObject_ratingObjectID)
	 * OR ratingObject_ratingObjectID)
	 * @param mixed $b Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof RatingObject)
		{
			$this->ratingObject = $a;
			$this->ratingObject_ratingObjectID = $a->getRatingObjectID();
		}
		else if(is_array($a))
		{
			$this->ratingObject = NULL;
			$this->ratingObject_ratingObjectID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->ratingObject = NULL;
			$this->ratingObject_ratingObjectID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->persoon = $b;
			$this->persoon_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->rating = '1';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'RatingObject';
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld ratingObject.
	 *
	 * @return RatingObject
	 * De waarde van het veld ratingObject.
	 */
	public function getRatingObject()
	{
		if(!isset($this->ratingObject)
		 && isset($this->ratingObject_ratingObjectID)
		 ) {
			$this->ratingObject = RatingObject::geef
					( $this->ratingObject_ratingObjectID
					);
		}
		return $this->ratingObject;
	}
	/**
	 * @brief Geef de waarde van het veld ratingObject_ratingObjectID.
	 *
	 * @return int
	 * De waarde van het veld ratingObject_ratingObjectID.
	 */
	public function getRatingObjectRatingObjectID()
	{
		if (is_null($this->ratingObject_ratingObjectID) && isset($this->ratingObject)) {
			$this->ratingObject_ratingObjectID = $this->ratingObject->getRatingObjectID();
		}
		return $this->ratingObject_ratingObjectID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld rating.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld rating.
	 */
	static public function enumsRating()
	{
		static $vals = array('1','2','3','4','5','6','7','8','9','10');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @return string
	 * De waarde van het veld rating.
	 */
	public function getRating()
	{
		return $this->rating;
	}
	/**
	 * @brief Stel de waarde van het veld rating in.
	 *
	 * @param mixed $newRating De nieuwe waarde.
	 *
	 * @return PersoonRating
	 * Dit PersoonRating-object.
	 */
	public function setRating($newRating)
	{
		unset($this->errors['Rating']);
		if(!is_null($newRating))
			$newRating = strtoupper(trim($newRating));
		if($newRating === "")
			$newRating = NULL;
		if($this->rating === $newRating)
			return $this;

		$this->rating = $newRating;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rating geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rating geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkRating()
	{
		if (array_key_exists('Rating', $this->errors))
			return $this->errors['Rating'];
		$waarde = $this->getRating();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsRating()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return PersoonRating::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van PersoonRating.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return PersoonRating::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getRatingObjectRatingObjectID()
		                      , $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return PersoonRating|false
	 * Een PersoonRating-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$ratingObject_ratingObjectID = (int)$a[0];
			$persoon_contactID = (int)$a[1];
		}
		else if($a instanceof RatingObject
		     && $b instanceof Persoon)
		{
			$ratingObject_ratingObjectID = $a->getRatingObjectID();
			$persoon_contactID = $b->getContactID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$ratingObject_ratingObjectID = (int)$a;
			$persoon_contactID = (int)$b;
		}

		if(is_null($ratingObject_ratingObjectID)
		|| is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($ratingObject_ratingObjectID, $persoon_contactID) ));
		return Entiteit::geefCache(array($ratingObject_ratingObjectID, $persoon_contactID), 'PersoonRating');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'PersoonRating');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('PersoonRating::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `PersoonRating`.`ratingObject_ratingObjectID`'
		                 .     ', `PersoonRating`.`persoon_contactID`'
		                 .     ', `PersoonRating`.`rating`'
		                 .     ', `PersoonRating`.`gewijzigdWanneer`'
		                 .     ', `PersoonRating`.`gewijzigdWie`'
		                 .' FROM `PersoonRating`'
		                 .' WHERE (`ratingObject_ratingObjectID`, `persoon_contactID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['ratingObject_ratingObjectID']
			                  ,$row['persoon_contactID']);

			$obj = new PersoonRating(array($row['ratingObject_ratingObjectID']), array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->rating  = strtoupper(trim($row['rating']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'PersoonRating')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getRatingObject();
		if(!$rel->getInDB())
			throw new LogicException('foreign RatingObject is not in DB');
		$this->ratingObject_ratingObjectID = $rel->getRatingObjectID();

		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `PersoonRating`'
			          . ' (`ratingObject_ratingObjectID`, `persoon_contactID`, `rating`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i)'
			          , $this->ratingObject_ratingObjectID
			          , $this->persoon_contactID
			          , $this->rating
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'PersoonRating')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `PersoonRating`'
			          .' SET `rating` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `ratingObject_ratingObjectID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->rating
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->ratingObject_ratingObjectID
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPersoonRating
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPersoonRating($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPersoonRating($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Rating';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Rating';
			break;
		case 'get':
			$velden[] = 'Rating';
		case 'primary':
			$velden[] = 'ratingObject_ratingObjectID';
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Rating';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `PersoonRating`'
		          .' WHERE `ratingObject_ratingObjectID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->ratingObject_ratingObjectID
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->ratingObject = NULL;
		$this->ratingObject_ratingObjectID = NULL;
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `PersoonRating`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `ratingObject_ratingObjectID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->ratingObject_ratingObjectID
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van PersoonRating terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'ratingobject':
			return 'foreign';
		case 'ratingobject_ratingobjectid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'rating':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'ratingObject_ratingObjectID':
		case 'persoon_contactID':
			$type = '%i';
			break;
		case 'rating':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `PersoonRating`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `ratingObject_ratingObjectID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->ratingObject_ratingObjectID
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `PersoonRating`'
		          .' WHERE `ratingObject_ratingObjectID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		                 , $veld
		          , $this->ratingObject_ratingObjectID
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'PersoonRating');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
