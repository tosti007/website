<?
abstract class DeelnemerAntwoord_Generated
	extends Entiteit
{
	protected $deelnemer;				/**< \brief PRIMARY */
	protected $deelnemer_contactID;		/**< \brief PRIMARY */
	protected $deelnemer_activiteitID;	/**< \brief PRIMARY */
	protected $vraag;					/**< \brief PRIMARY */
	protected $vraag_activiteitID;		/**< \brief PRIMARY */
	protected $vraag_vraagID;			/**< \brief PRIMARY */
	protected $antwoord;
	/**
	/**
	 * @brief De constructor van de DeelnemerAntwoord_Generated-klasse.
	 *
	 * @param mixed $a Deelnemer (Deelnemer OR Array(deelnemer_contactID,
	 * deelnemer_activiteitID))
	 * @param mixed $b Vraag (ActiviteitVraag OR Array(activiteitVraag_activiteitID,
	 * activiteitVraag_vraagID))
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]) && is_null($a[1]) && is_null($a[2]) && is_null($a[3]))
			$a = NULL;

		if($a instanceof Deelnemer)
		{
			$this->deelnemer = $a;
			$this->deelnemer_contactID = $a->getPersoonContactID();
			$this->deelnemer_activiteitID = $a->getActiviteitActiviteitID();
		}
		else if(is_array($a))
		{
			$this->deelnemer = NULL;
			$this->deelnemer_contactID = (int)$a[0];
			$this->deelnemer_activiteitID = (int)$a[1];
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]) && is_null($b[1]) && is_null($b[2]))
			$b = NULL;

		if($b instanceof ActiviteitVraag)
		{
			$this->vraag = $b;
			$this->vraag_activiteitID = $b->getActiviteitActiviteitID();
			$this->vraag_vraagID = $b->getVraagID();
		}
		else if(is_array($b))
		{
			$this->vraag = NULL;
			$this->vraag_activiteitID = (int)$b[0];
			$this->vraag_vraagID = (int)$b[1];
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->antwoord = '';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Deelnemer';
		$volgorde[] = 'Vraag';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld deelnemer.
	 *
	 * @return Deelnemer
	 * De waarde van het veld deelnemer.
	 */
	public function getDeelnemer()
	{
		if(!isset($this->deelnemer)
		 && isset($this->deelnemer_contactID)
		 && isset($this->deelnemer_activiteitID)
		 ) {
			$this->deelnemer = Deelnemer::geef
					( $this->deelnemer_contactID
					, $this->deelnemer_activiteitID
					);
		}
		return $this->deelnemer;
	}
	/**
	 * @brief Geef de waarde van het veld deelnemer_contactID.
	 *
	 * @return int
	 * De waarde van het veld deelnemer_contactID.
	 */
	public function getDeelnemerContactID()
	{
		if (is_null($this->deelnemer_contactID) && isset($this->deelnemer)) {
			$this->deelnemer_contactID = $this->deelnemer->getPersoonContactID();
		}
		return $this->deelnemer_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld deelnemer_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld deelnemer_activiteitID.
	 */
	public function getDeelnemerActiviteitID()
	{
		if (is_null($this->deelnemer_activiteitID) && isset($this->deelnemer)) {
			$this->deelnemer_activiteitID = $this->deelnemer->getActiviteitActiviteitID();
		}
		return $this->deelnemer_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld vraag.
	 *
	 * @return ActiviteitVraag
	 * De waarde van het veld vraag.
	 */
	public function getVraag()
	{
		if(!isset($this->vraag)
		 && isset($this->vraag_activiteitID)
		 && isset($this->vraag_vraagID)
		 ) {
			$this->vraag = ActiviteitVraag::geef
					( $this->vraag_activiteitID
					, $this->vraag_vraagID
					);
		}
		return $this->vraag;
	}
	/**
	 * @brief Geef de waarde van het veld vraag_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld vraag_activiteitID.
	 */
	public function getVraagActiviteitID()
	{
		if (is_null($this->vraag_activiteitID) && isset($this->vraag)) {
			$this->vraag_activiteitID = $this->vraag->getActiviteitActiviteitID();
		}
		return $this->vraag_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld vraag_vraagID.
	 *
	 * @return int
	 * De waarde van het veld vraag_vraagID.
	 */
	public function getVraagVraagID()
	{
		if (is_null($this->vraag_vraagID) && isset($this->vraag)) {
			$this->vraag_vraagID = $this->vraag->getVraagID();
		}
		return $this->vraag_vraagID;
	}
	/**
	 * @brief Geef de waarde van het veld antwoord.
	 *
	 * @return string
	 * De waarde van het veld antwoord.
	 */
	public function getAntwoord()
	{
		return $this->antwoord;
	}
	/**
	 * @brief Stel de waarde van het veld antwoord in.
	 *
	 * @param mixed $newAntwoord De nieuwe waarde.
	 *
	 * @return DeelnemerAntwoord
	 * Dit DeelnemerAntwoord-object.
	 */
	public function setAntwoord($newAntwoord)
	{
		unset($this->errors['Antwoord']);
		if(!is_null($newAntwoord))
			$newAntwoord = trim($newAntwoord);
		if($newAntwoord === "")
			$newAntwoord = NULL;
		if($this->antwoord === $newAntwoord)
			return $this;

		$this->antwoord = $newAntwoord;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld antwoord geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld antwoord geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAntwoord()
	{
		if (array_key_exists('Antwoord', $this->errors))
			return $this->errors['Antwoord'];
		$waarde = $this->getAntwoord();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return DeelnemerAntwoord::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van DeelnemerAntwoord.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return DeelnemerAntwoord::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getDeelnemerContactID()
		                      , $this->getDeelnemerActiviteitID()
		                      , $this->getVraagActiviteitID()
		                      , $this->getVraagVraagID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 * @param mixed $c Object of ID waarop gezocht moet worden.
	 * @param mixed $d Object of ID waarop gezocht moet worden.
	 *
	 * @return DeelnemerAntwoord|false
	 * Een DeelnemerAntwoord-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL, $c = NULL, $d = NULL)
	{
		if(is_null($a)
		&& is_null($b)
		&& is_null($c)
		&& is_null($d))
			return false;

		if(is_string($a)
		&& is_null($b)
		&& is_null($c)
		&& is_null($d))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& is_null($c)
		&& is_null($d)
		&& count($a) == 4)
		{
			$deelnemer_contactID = (int)$a[0];
			$deelnemer_activiteitID = (int)$a[1];
			$vraag_activiteitID = (int)$a[2];
			$vraag_vraagID = (int)$a[3];
		}
		else if($a instanceof Deelnemer
		     && $b instanceof ActiviteitVraag
		     && !isset($c)
		     && !isset($d))
		{
			$deelnemer_contactID = $a->getPersoonContactID();
			$deelnemer_activiteitID = $a->getActiviteitActiviteitID();
			$vraag_activiteitID = $b->getActiviteitActiviteitID();
			$vraag_vraagID = $b->getVraagID();
		}
		else if(isset($a)
		     && isset($b)
		     && isset($c)
		     && isset($d))
		{
			$deelnemer_contactID = (int)$a;
			$deelnemer_activiteitID = (int)$b;
			$vraag_activiteitID = (int)$c;
			$vraag_vraagID = (int)$d;
		}

		if(is_null($deelnemer_contactID)
		|| is_null($deelnemer_activiteitID)
		|| is_null($vraag_activiteitID)
		|| is_null($vraag_vraagID))
			throw new BadMethodCallException();

		static::cache(array( array($deelnemer_contactID, $deelnemer_activiteitID, $vraag_activiteitID, $vraag_vraagID) ));
		return Entiteit::geefCache(array($deelnemer_contactID, $deelnemer_activiteitID, $vraag_activiteitID, $vraag_vraagID), 'DeelnemerAntwoord');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'DeelnemerAntwoord');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('DeelnemerAntwoord::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `DeelnemerAntwoord`.`deelnemer_persoon_contactID`'
		                 .     ', `DeelnemerAntwoord`.`deelnemer_activiteit_activiteitID`'
		                 .     ', `DeelnemerAntwoord`.`vraag_activiteit_activiteitID`'
		                 .     ', `DeelnemerAntwoord`.`vraag_vraagID`'
		                 .     ', `DeelnemerAntwoord`.`antwoord`'
		                 .     ', `DeelnemerAntwoord`.`gewijzigdWanneer`'
		                 .     ', `DeelnemerAntwoord`.`gewijzigdWie`'
		                 .' FROM `DeelnemerAntwoord`'
		                 .' WHERE (`deelnemer_persoon_contactID`, `deelnemer_activiteit_activiteitID`, `vraag_activiteit_activiteitID`, `vraag_vraagID`)'
		                 .      ' IN (%A{iiii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['deelnemer_persoon_contactID']
			                  ,$row['deelnemer_activiteit_activiteitID']
			                  ,$row['vraag_activiteit_activiteitID']
			                  ,$row['vraag_vraagID']);

			$obj = new DeelnemerAntwoord(array($row['deelnemer_persoon_contactID'], $row['deelnemer_activiteit_activiteitID']), array($row['vraag_activiteit_activiteitID'], $row['vraag_vraagID']));

			$obj->inDB = True;

			$obj->antwoord  = trim($row['antwoord']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'DeelnemerAntwoord')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getDeelnemer();
		if(!$rel->getInDB())
			throw new LogicException('foreign Deelnemer is not in DB');
		$this->deelnemer_contactID = $rel->getPersoonContactID();
		$this->deelnemer_activiteitID = $rel->getActiviteitActiviteitID();

		$rel = $this->getVraag();
		if(!$rel->getInDB())
			throw new LogicException('foreign Vraag is not in DB');
		$this->vraag_activiteitID = $rel->getActiviteitActiviteitID();
		$this->vraag_vraagID = $rel->getVraagID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `DeelnemerAntwoord`'
			          . ' (`deelnemer_persoon_contactID`, `deelnemer_activiteit_activiteitID`, `vraag_activiteit_activiteitID`, `vraag_vraagID`, `antwoord`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %i, %s, %s, %i)'
			          , $this->deelnemer_contactID
			          , $this->deelnemer_activiteitID
			          , $this->vraag_activiteitID
			          , $this->vraag_vraagID
			          , $this->antwoord
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'DeelnemerAntwoord')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `DeelnemerAntwoord`'
			          .' SET `antwoord` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `deelnemer_persoon_contactID` = %i'
			          .  ' AND `deelnemer_activiteit_activiteitID` = %i'
			          .  ' AND `vraag_activiteit_activiteitID` = %i'
			          .  ' AND `vraag_vraagID` = %i'
			          , $this->antwoord
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->deelnemer_contactID
			          , $this->deelnemer_activiteitID
			          , $this->vraag_activiteitID
			          , $this->vraag_vraagID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDeelnemerAntwoord
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDeelnemerAntwoord($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDeelnemerAntwoord($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Antwoord';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Antwoord';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Antwoord';
			break;
		case 'get':
			$velden[] = 'Antwoord';
		case 'primary':
			$velden[] = 'deelnemer_contactID';
			$velden[] = 'deelnemer_activiteitID';
			$velden[] = 'vraag_activiteitID';
			$velden[] = 'vraag_vraagID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Antwoord';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `DeelnemerAntwoord`'
		          .' WHERE `deelnemer_persoon_contactID` = %i'
		          .  ' AND `deelnemer_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_vraagID` = %i'
		          .' LIMIT 1'
		          , $this->deelnemer_contactID
		          , $this->deelnemer_activiteitID
		          , $this->vraag_activiteitID
		          , $this->vraag_vraagID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->deelnemer = NULL;
		$this->deelnemer_contactID = NULL;
		$this->deelnemer_activiteitID = NULL;
		$this->vraag = NULL;
		$this->vraag_activiteitID = NULL;
		$this->vraag_vraagID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `DeelnemerAntwoord`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `deelnemer_persoon_contactID` = %i'
			          .  ' AND `deelnemer_activiteit_activiteitID` = %i'
			          .  ' AND `vraag_activiteit_activiteitID` = %i'
			          .  ' AND `vraag_vraagID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->deelnemer_contactID
			          , $this->deelnemer_activiteitID
			          , $this->vraag_activiteitID
			          , $this->vraag_vraagID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van DeelnemerAntwoord terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'deelnemer':
			return 'foreign';
		case 'deelnemer_contactid':
			return 'int';
		case 'deelnemer_activiteitid':
			return 'int';
		case 'vraag':
			return 'foreign';
		case 'vraag_activiteitid':
			return 'int';
		case 'vraag_vraagid':
			return 'int';
		case 'antwoord':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'deelnemer_contactID':
		case 'deelnemer_activiteitID':
		case 'vraag_activiteitID':
		case 'vraag_vraagID':
			$type = '%i';
			break;
		case 'antwoord':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `DeelnemerAntwoord`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `deelnemer_persoon_contactID` = %i'
		          .  ' AND `deelnemer_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_vraagID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->deelnemer_contactID
		          , $this->deelnemer_activiteitID
		          , $this->vraag_activiteitID
		          , $this->vraag_vraagID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `DeelnemerAntwoord`'
		          .' WHERE `deelnemer_persoon_contactID` = %i'
		          .  ' AND `deelnemer_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_vraagID` = %i'
		                 , $veld
		          , $this->deelnemer_contactID
		          , $this->deelnemer_activiteitID
		          , $this->vraag_activiteitID
		          , $this->vraag_vraagID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'DeelnemerAntwoord');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
