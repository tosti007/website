<?
abstract class Donateur_Generated
	extends Entiteit
{
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $anderPersoon;			/**< \brief NULL */
	protected $anderPersoon_contactID;	/**< \brief PRIMARY */
	protected $aanhef;					/**< \brief NULL */
	protected $jaarBegin;
	protected $jaarEind;				/**< \brief NULL */
	protected $donatie;
	protected $machtiging;
	protected $machtigingOud;
	protected $almanak;
	protected $avNotulen;				/**< \brief ENUM:N/POST/EMAIL/POST_EMAIL */
	protected $jaarverslag;
	protected $studiereis;
	protected $symposium;
	protected $vakid;
	/**
	/**
	 * @brief De constructor van de Donateur_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->anderPersoon = NULL;
		$this->anderPersoon_contactID = NULL;
		$this->aanhef = NULL;
		$this->jaarBegin = 0;
		$this->jaarEind = NULL;
		$this->donatie = 0;
		$this->machtiging = False;
		$this->machtigingOud = False;
		$this->almanak = False;
		$this->avNotulen = 'N';
		$this->jaarverslag = False;
		$this->studiereis = False;
		$this->symposium = False;
		$this->vakid = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld anderPersoon.
	 *
	 * @return Persoon
	 * De waarde van het veld anderPersoon.
	 */
	public function getAnderPersoon()
	{
		if(!isset($this->anderPersoon)
		 && isset($this->anderPersoon_contactID)
		 ) {
			$this->anderPersoon = Persoon::geef
					( $this->anderPersoon_contactID
					);
		}
		return $this->anderPersoon;
	}
	/**
	 * @brief Stel de waarde van het veld anderPersoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setAnderPersoon($new_contactID)
	{
		unset($this->errors['AnderPersoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->anderPersoon == $new_contactID
			&& $this->anderPersoon_contactID == $this->anderPersoon->getContactID())
				return $this;
			$this->anderPersoon = $new_contactID;
			$this->anderPersoon_contactID
					= $this->anderPersoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->anderPersoon == NULL && $this->anderPersoon_contactID == NULL)
				return $this;
			$this->anderPersoon = NULL;
			$this->anderPersoon_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->anderPersoon == NULL 
				&& $this->anderPersoon_contactID == (int)$new_contactID)
				return $this;
			$this->anderPersoon = NULL;
			$this->anderPersoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld anderPersoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld anderPersoon geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkAnderPersoon()
	{
		if (array_key_exists('AnderPersoon', $this->errors))
			return $this->errors['AnderPersoon'];
		$waarde1 = $this->getAnderPersoon();
		$waarde2 = $this->getAnderPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld anderPersoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld anderPersoon_contactID.
	 */
	public function getAnderPersoonContactID()
	{
		if (is_null($this->anderPersoon_contactID) && isset($this->anderPersoon)) {
			$this->anderPersoon_contactID = $this->anderPersoon->getContactID();
		}
		return $this->anderPersoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld aanhef.
	 *
	 * @return string
	 * De waarde van het veld aanhef.
	 */
	public function getAanhef()
	{
		return $this->aanhef;
	}
	/**
	 * @brief Stel de waarde van het veld aanhef in.
	 *
	 * @param mixed $newAanhef De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setAanhef($newAanhef)
	{
		unset($this->errors['Aanhef']);
		if(!is_null($newAanhef))
			$newAanhef = trim($newAanhef);
		if($newAanhef === "")
			$newAanhef = NULL;
		if($this->aanhef === $newAanhef)
			return $this;

		$this->aanhef = $newAanhef;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aanhef geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aanhef geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAanhef()
	{
		if (array_key_exists('Aanhef', $this->errors))
			return $this->errors['Aanhef'];
		$waarde = $this->getAanhef();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld jaarBegin.
	 *
	 * @return int
	 * De waarde van het veld jaarBegin.
	 */
	public function getJaarBegin()
	{
		return $this->jaarBegin;
	}
	/**
	 * @brief Stel de waarde van het veld jaarBegin in.
	 *
	 * @param mixed $newJaarBegin De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setJaarBegin($newJaarBegin)
	{
		unset($this->errors['JaarBegin']);
		if(!is_null($newJaarBegin))
			$newJaarBegin = (int)$newJaarBegin;
		if($this->jaarBegin === $newJaarBegin)
			return $this;

		$this->jaarBegin = $newJaarBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld jaarBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld jaarBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkJaarBegin()
	{
		if (array_key_exists('JaarBegin', $this->errors))
			return $this->errors['JaarBegin'];
		$waarde = $this->getJaarBegin();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld jaarEind.
	 *
	 * @return int
	 * De waarde van het veld jaarEind.
	 */
	public function getJaarEind()
	{
		return $this->jaarEind;
	}
	/**
	 * @brief Stel de waarde van het veld jaarEind in.
	 *
	 * @param mixed $newJaarEind De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setJaarEind($newJaarEind)
	{
		unset($this->errors['JaarEind']);
		if(!is_null($newJaarEind))
			$newJaarEind = (int)$newJaarEind;
		if($this->jaarEind === $newJaarEind)
			return $this;

		$this->jaarEind = $newJaarEind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld jaarEind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld jaarEind geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkJaarEind()
	{
		if (array_key_exists('JaarEind', $this->errors))
			return $this->errors['JaarEind'];
		$waarde = $this->getJaarEind();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld donatie.
	 *
	 * @return float
	 * De waarde van het veld donatie.
	 */
	public function getDonatie()
	{
		return $this->donatie;
	}
	/**
	 * @brief Stel de waarde van het veld donatie in.
	 *
	 * @param mixed $newDonatie De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setDonatie($newDonatie)
	{
		unset($this->errors['Donatie']);
		if(!is_null($newDonatie))
			$newDonatie = (float)$newDonatie;
		if($this->donatie === $newDonatie)
			return $this;

		$this->donatie = $newDonatie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld donatie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld donatie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDonatie()
	{
		if (array_key_exists('Donatie', $this->errors))
			return $this->errors['Donatie'];
		$waarde = $this->getDonatie();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld machtiging.
	 *
	 * @return bool
	 * De waarde van het veld machtiging.
	 */
	public function getMachtiging()
	{
		return $this->machtiging;
	}
	/**
	 * @brief Stel de waarde van het veld machtiging in.
	 *
	 * @param mixed $newMachtiging De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setMachtiging($newMachtiging)
	{
		unset($this->errors['Machtiging']);
		if(!is_null($newMachtiging))
			$newMachtiging = (bool)$newMachtiging;
		if($this->machtiging === $newMachtiging)
			return $this;

		$this->machtiging = $newMachtiging;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld machtiging geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld machtiging geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkMachtiging()
	{
		if (array_key_exists('Machtiging', $this->errors))
			return $this->errors['Machtiging'];
		$waarde = $this->getMachtiging();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld machtigingOud.
	 *
	 * @return bool
	 * De waarde van het veld machtigingOud.
	 */
	public function getMachtigingOud()
	{
		return $this->machtigingOud;
	}
	/**
	 * @brief Stel de waarde van het veld machtigingOud in.
	 *
	 * @param mixed $newMachtigingOud De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setMachtigingOud($newMachtigingOud)
	{
		unset($this->errors['MachtigingOud']);
		if(!is_null($newMachtigingOud))
			$newMachtigingOud = (bool)$newMachtigingOud;
		if($this->machtigingOud === $newMachtigingOud)
			return $this;

		$this->machtigingOud = $newMachtigingOud;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld machtigingOud geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld machtigingOud geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMachtigingOud()
	{
		if (array_key_exists('MachtigingOud', $this->errors))
			return $this->errors['MachtigingOud'];
		$waarde = $this->getMachtigingOud();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld almanak.
	 *
	 * @return bool
	 * De waarde van het veld almanak.
	 */
	public function getAlmanak()
	{
		return $this->almanak;
	}
	/**
	 * @brief Stel de waarde van het veld almanak in.
	 *
	 * @param mixed $newAlmanak De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setAlmanak($newAlmanak)
	{
		unset($this->errors['Almanak']);
		if(!is_null($newAlmanak))
			$newAlmanak = (bool)$newAlmanak;
		if($this->almanak === $newAlmanak)
			return $this;

		$this->almanak = $newAlmanak;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld almanak geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld almanak geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAlmanak()
	{
		if (array_key_exists('Almanak', $this->errors))
			return $this->errors['Almanak'];
		$waarde = $this->getAlmanak();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld avNotulen.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld avNotulen.
	 */
	static public function enumsAvNotulen()
	{
		static $vals = array('N','POST','EMAIL','POST_EMAIL');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld avNotulen.
	 *
	 * @return string
	 * De waarde van het veld avNotulen.
	 */
	public function getAvNotulen()
	{
		return $this->avNotulen;
	}
	/**
	 * @brief Stel de waarde van het veld avNotulen in.
	 *
	 * @param mixed $newAvNotulen De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setAvNotulen($newAvNotulen)
	{
		unset($this->errors['AvNotulen']);
		if(!is_null($newAvNotulen))
			$newAvNotulen = strtoupper(trim($newAvNotulen));
		if($newAvNotulen === "")
			$newAvNotulen = NULL;
		if($this->avNotulen === $newAvNotulen)
			return $this;

		$this->avNotulen = $newAvNotulen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld avNotulen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld avNotulen geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAvNotulen()
	{
		if (array_key_exists('AvNotulen', $this->errors))
			return $this->errors['AvNotulen'];
		$waarde = $this->getAvNotulen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsAvNotulen()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld jaarverslag.
	 *
	 * @return bool
	 * De waarde van het veld jaarverslag.
	 */
	public function getJaarverslag()
	{
		return $this->jaarverslag;
	}
	/**
	 * @brief Stel de waarde van het veld jaarverslag in.
	 *
	 * @param mixed $newJaarverslag De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setJaarverslag($newJaarverslag)
	{
		unset($this->errors['Jaarverslag']);
		if(!is_null($newJaarverslag))
			$newJaarverslag = (bool)$newJaarverslag;
		if($this->jaarverslag === $newJaarverslag)
			return $this;

		$this->jaarverslag = $newJaarverslag;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld jaarverslag geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld jaarverslag geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkJaarverslag()
	{
		if (array_key_exists('Jaarverslag', $this->errors))
			return $this->errors['Jaarverslag'];
		$waarde = $this->getJaarverslag();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld studiereis.
	 *
	 * @return bool
	 * De waarde van het veld studiereis.
	 */
	public function getStudiereis()
	{
		return $this->studiereis;
	}
	/**
	 * @brief Stel de waarde van het veld studiereis in.
	 *
	 * @param mixed $newStudiereis De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setStudiereis($newStudiereis)
	{
		unset($this->errors['Studiereis']);
		if(!is_null($newStudiereis))
			$newStudiereis = (bool)$newStudiereis;
		if($this->studiereis === $newStudiereis)
			return $this;

		$this->studiereis = $newStudiereis;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld studiereis geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld studiereis geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkStudiereis()
	{
		if (array_key_exists('Studiereis', $this->errors))
			return $this->errors['Studiereis'];
		$waarde = $this->getStudiereis();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld symposium.
	 *
	 * @return bool
	 * De waarde van het veld symposium.
	 */
	public function getSymposium()
	{
		return $this->symposium;
	}
	/**
	 * @brief Stel de waarde van het veld symposium in.
	 *
	 * @param mixed $newSymposium De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setSymposium($newSymposium)
	{
		unset($this->errors['Symposium']);
		if(!is_null($newSymposium))
			$newSymposium = (bool)$newSymposium;
		if($this->symposium === $newSymposium)
			return $this;

		$this->symposium = $newSymposium;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld symposium geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld symposium geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkSymposium()
	{
		if (array_key_exists('Symposium', $this->errors))
			return $this->errors['Symposium'];
		$waarde = $this->getSymposium();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld vakid.
	 *
	 * @return bool
	 * De waarde van het veld vakid.
	 */
	public function getVakid()
	{
		return $this->vakid;
	}
	/**
	 * @brief Stel de waarde van het veld vakid in.
	 *
	 * @param mixed $newVakid De nieuwe waarde.
	 *
	 * @return Donateur
	 * Dit Donateur-object.
	 */
	public function setVakid($newVakid)
	{
		unset($this->errors['Vakid']);
		if(!is_null($newVakid))
			$newVakid = (bool)$newVakid;
		if($this->vakid === $newVakid)
			return $this;

		$this->vakid = $newVakid;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld vakid geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld vakid geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkVakid()
	{
		if (array_key_exists('Vakid', $this->errors))
			return $this->errors['Vakid'];
		$waarde = $this->getVakid();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Donateur::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Donateur.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Donateur::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Donateur|false
	 * Een Donateur-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$persoon_contactID = (int)$a[0];
		}
		else if($a instanceof Persoon)
		{
			$persoon_contactID = $a->getContactID();
		}
		else if(isset($a))
		{
			$persoon_contactID = (int)$a;
		}

		if(is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($persoon_contactID) ));
		return Entiteit::geefCache(array($persoon_contactID), 'Donateur');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Donateur');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Donateur::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Donateur`.`persoon_contactID`'
		                 .     ', `Donateur`.`anderPersoon_contactID`'
		                 .     ', `Donateur`.`aanhef`'
		                 .     ', `Donateur`.`jaarBegin`'
		                 .     ', `Donateur`.`jaarEind`'
		                 .     ', `Donateur`.`donatie`'
		                 .     ', `Donateur`.`machtiging`'
		                 .     ', `Donateur`.`machtigingOud`'
		                 .     ', `Donateur`.`almanak`'
		                 .     ', `Donateur`.`avNotulen`'
		                 .     ', `Donateur`.`jaarverslag`'
		                 .     ', `Donateur`.`studiereis`'
		                 .     ', `Donateur`.`symposium`'
		                 .     ', `Donateur`.`vakid`'
		                 .     ', `Donateur`.`gewijzigdWanneer`'
		                 .     ', `Donateur`.`gewijzigdWie`'
		                 .' FROM `Donateur`'
		                 .' WHERE (`persoon_contactID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['persoon_contactID']);

			$obj = new Donateur(array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->anderPersoon_contactID  = (is_null($row['anderPersoon_contactID'])) ? null : (int) $row['anderPersoon_contactID'];
			$obj->aanhef  = (is_null($row['aanhef'])) ? null : trim($row['aanhef']);
			$obj->jaarBegin  = (int) $row['jaarBegin'];
			$obj->jaarEind  = (is_null($row['jaarEind'])) ? null : (int) $row['jaarEind'];
			$obj->donatie  = (float) $row['donatie'];
			$obj->machtiging  = (bool) $row['machtiging'];
			$obj->machtigingOud  = (bool) $row['machtigingOud'];
			$obj->almanak  = (bool) $row['almanak'];
			$obj->avNotulen  = strtoupper(trim($row['avNotulen']));
			$obj->jaarverslag  = (bool) $row['jaarverslag'];
			$obj->studiereis  = (bool) $row['studiereis'];
			$obj->symposium  = (bool) $row['symposium'];
			$obj->vakid  = (bool) $row['vakid'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Donateur')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;
		$this->getAnderPersoonContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Donateur`'
			          . ' (`persoon_contactID`, `anderPersoon_contactID`, `aanhef`, `jaarBegin`, `jaarEind`, `donatie`, `machtiging`, `machtigingOud`, `almanak`, `avNotulen`, `jaarverslag`, `studiereis`, `symposium`, `vakid`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i, %i, %f, %i, %i, %i, %s, %i, %i, %i, %i, %s, %i)'
			          , $this->persoon_contactID
			          , $this->anderPersoon_contactID
			          , $this->aanhef
			          , $this->jaarBegin
			          , $this->jaarEind
			          , $this->donatie
			          , $this->machtiging
			          , $this->machtigingOud
			          , $this->almanak
			          , $this->avNotulen
			          , $this->jaarverslag
			          , $this->studiereis
			          , $this->symposium
			          , $this->vakid
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Donateur')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Donateur`'
			          .' SET `anderPersoon_contactID` = %i'
			          .   ', `aanhef` = %s'
			          .   ', `jaarBegin` = %i'
			          .   ', `jaarEind` = %i'
			          .   ', `donatie` = %f'
			          .   ', `machtiging` = %i'
			          .   ', `machtigingOud` = %i'
			          .   ', `almanak` = %i'
			          .   ', `avNotulen` = %s'
			          .   ', `jaarverslag` = %i'
			          .   ', `studiereis` = %i'
			          .   ', `symposium` = %i'
			          .   ', `vakid` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          , $this->anderPersoon_contactID
			          , $this->aanhef
			          , $this->jaarBegin
			          , $this->jaarEind
			          , $this->donatie
			          , $this->machtiging
			          , $this->machtigingOud
			          , $this->almanak
			          , $this->avNotulen
			          , $this->jaarverslag
			          , $this->studiereis
			          , $this->symposium
			          , $this->vakid
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDonateur
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDonateur($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDonateur($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'AnderPersoon';
			$velden[] = 'Aanhef';
			$velden[] = 'JaarBegin';
			$velden[] = 'JaarEind';
			$velden[] = 'Donatie';
			$velden[] = 'Machtiging';
			$velden[] = 'MachtigingOud';
			$velden[] = 'Almanak';
			$velden[] = 'AvNotulen';
			$velden[] = 'Jaarverslag';
			$velden[] = 'Studiereis';
			$velden[] = 'Symposium';
			$velden[] = 'Vakid';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Donatie';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'AnderPersoon';
			$velden[] = 'Aanhef';
			$velden[] = 'JaarBegin';
			$velden[] = 'JaarEind';
			$velden[] = 'Donatie';
			$velden[] = 'Machtiging';
			$velden[] = 'MachtigingOud';
			$velden[] = 'Almanak';
			$velden[] = 'AvNotulen';
			$velden[] = 'Jaarverslag';
			$velden[] = 'Studiereis';
			$velden[] = 'Symposium';
			$velden[] = 'Vakid';
			break;
		case 'get':
			$velden[] = 'AnderPersoon';
			$velden[] = 'Aanhef';
			$velden[] = 'JaarBegin';
			$velden[] = 'JaarEind';
			$velden[] = 'Donatie';
			$velden[] = 'Machtiging';
			$velden[] = 'MachtigingOud';
			$velden[] = 'Almanak';
			$velden[] = 'AvNotulen';
			$velden[] = 'Jaarverslag';
			$velden[] = 'Studiereis';
			$velden[] = 'Symposium';
			$velden[] = 'Vakid';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'anderPersoon_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'AnderPersoon';
			$velden[] = 'Aanhef';
			$velden[] = 'JaarBegin';
			$velden[] = 'JaarEind';
			$velden[] = 'Donatie';
			$velden[] = 'Machtiging';
			$velden[] = 'MachtigingOud';
			$velden[] = 'Almanak';
			$velden[] = 'AvNotulen';
			$velden[] = 'Jaarverslag';
			$velden[] = 'Studiereis';
			$velden[] = 'Symposium';
			$velden[] = 'Vakid';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Donateur`'
		          .' WHERE `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Donateur`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Donateur terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'anderpersoon':
			return 'foreign';
		case 'anderpersoon_contactid':
			return 'int';
		case 'aanhef':
			return 'string';
		case 'jaarbegin':
			return 'int';
		case 'jaareind':
			return 'int';
		case 'donatie':
			return 'money';
		case 'machtiging':
			return 'bool';
		case 'machtigingoud':
			return 'bool';
		case 'almanak':
			return 'bool';
		case 'avnotulen':
			return 'enum';
		case 'jaarverslag':
			return 'bool';
		case 'studiereis':
			return 'bool';
		case 'symposium':
			return 'bool';
		case 'vakid':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'donatie':
			$type = '%f';
			break;
		case 'persoon_contactID':
		case 'anderPersoon_contactID':
		case 'jaarBegin':
		case 'jaarEind':
		case 'machtiging':
		case 'machtigingOud':
		case 'almanak':
		case 'jaarverslag':
		case 'studiereis':
		case 'symposium':
		case 'vakid':
			$type = '%i';
			break;
		case 'aanhef':
		case 'avNotulen':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Donateur`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Donateur`'
		          .' WHERE `persoon_contactID` = %i'
		                 , $veld
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Donateur');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
