<?
abstract class ActiviteitHerhaling_Generated
	extends Activiteit
{
	protected $parent;
	protected $parent_activiteitID;		/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de ActiviteitHerhaling_Generated-klasse.
	 *
	 * @param mixed $a Parent (Activiteit OR Array(activiteit_activiteitID) OR
	 * activiteit_activiteitID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Activiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Activiteit)
		{
			$this->parent = $a;
			$this->parent_activiteitID = $a->getActiviteitID();
		}
		else if(is_array($a))
		{
			$this->parent = NULL;
			$this->parent_activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->parent = NULL;
			$this->parent_activiteitID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->activiteitID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Parent';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld parent.
	 *
	 * @return Activiteit
	 * De waarde van het veld parent.
	 */
	public function getParent()
	{
		if(!isset($this->parent)
		 && isset($this->parent_activiteitID)
		 ) {
			$this->parent = Activiteit::geef
					( $this->parent_activiteitID
					);
		}
		return $this->parent;
	}
	/**
	 * @brief Geef de waarde van het veld parent_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld parent_activiteitID.
	 */
	public function getParentActiviteitID()
	{
		if (is_null($this->parent_activiteitID) && isset($this->parent)) {
			$this->parent_activiteitID = $this->parent->getActiviteitID();
		}
		return $this->parent_activiteitID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ActiviteitHerhaling::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ActiviteitHerhaling.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ActiviteitHerhaling::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getActiviteitID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return ActiviteitHerhaling|false
	 * Een ActiviteitHerhaling-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$activiteitID = (int)$a;
		}

		if(is_null($activiteitID))
			throw new BadMethodCallException();

		static::cache(array( array($activiteitID) ));
		return Entiteit::geefCache(array($activiteitID), 'ActiviteitHerhaling');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Activiteit::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('ActiviteitHerhaling::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `ActiviteitHerhaling`.`parent_activiteitID`'
			                 .     ', `Activiteit`.`activiteitID`'
			                 .     ', `Activiteit`.`momentBegin`'
			                 .     ', `Activiteit`.`momentEind`'
			                 .     ', `Activiteit`.`toegang`'
			                 .     ', `Activiteit`.`gewijzigdWanneer`'
			                 .     ', `Activiteit`.`gewijzigdWie`'
			                 .' FROM `ActiviteitHerhaling`'
			                 .' LEFT JOIN `Activiteit` USING (`activiteitID`)'
			                 .' WHERE (`activiteitID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['activiteitID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['ActiviteitHerhaling'][$id];
			} else {
				$obj = new ActiviteitHerhaling(array($row['parent_activiteitID']));
			}

			$obj->inDB = True;

			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Activiteit::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ActiviteitHerhaling')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getParentActiviteitID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `ActiviteitHerhaling`'
			          . ' (`activiteitID`, `parent_activiteitID`)'
			          . ' VALUES (%i, %i)'
			          , $this->activiteitID
			          , $this->parent_activiteitID
			          );

			if($classname == 'ActiviteitHerhaling')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ActiviteitHerhaling`'
			          .' SET `parent_activiteitID` = %i'
			          .' WHERE `activiteitID` = %i'
			          , $this->parent_activiteitID
			          , $this->activiteitID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenActiviteitHerhaling
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenActiviteitHerhaling($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenActiviteitHerhaling($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Parent';
			break;
		case 'get':
			$velden[] = 'Parent';
		case 'primary':
			$velden[] = 'parent_activiteitID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Parent';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ActiviteitHerhaling`'
		          .' WHERE `activiteitID` = %i'
		          .' LIMIT 1'
		          , $this->activiteitID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van ActiviteitHerhaling terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'parent':
			return 'foreign';
		case 'parent_activiteitid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'parent_activiteitID':
			$type = '%i';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ActiviteitHerhaling`'
		          ." SET `%l` = %i"
		          .' WHERE `activiteitID` = %i'
		          , $veld
		          , $waarde
		          , $this->activiteitID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'parent':
		case 'parent_activiteitID':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ActiviteitHerhaling`'
		          .' WHERE `activiteitID` = %i'
		                 , $veld
		          , $this->activiteitID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
