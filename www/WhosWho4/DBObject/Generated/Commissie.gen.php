<?
/**
 * @brief 'AT'AUTH_GET:gast,AUTH_SET:bestuur
 */
abstract class Commissie_Generated
	extends Entiteit
{
	protected $commissieID;				/**< \brief PRIMARY */
	protected $naam;
	protected $soort;					/**< \brief ENUM:CIE/GROEP/DISPUUT */
	protected $categorie;				/**< \brief NULL */
	protected $categorie_categorieID;	/**< \brief PRIMARY */
	protected $thema;					/**< \brief NULL */
	protected $omschrijving;			/**< \brief NULL */
	protected $datumBegin;				/**< \brief NULL */
	protected $datumEind;				/**< \brief NULL */
	protected $login;
	protected $email;					/**< \brief NULL */
	protected $homepage;				/**< \brief NULL */
	protected $emailZichtbaar;
	/** Verzamelingen **/
	protected $commissieLidVerzameling;
	protected $mededelingVerzameling;
	protected $mailingVerzameling;
	protected $commissieActiviteitVerzameling;
	protected $contractonderdeelVerzameling;
	protected $interactieVerzameling;
	protected $tagVerzameling;
	protected $bugCategorieVerzameling;
	protected $kartVoorwerpVerzameling;
	protected $aesArtikelVerzameling;
	protected $commissieFotoVerzameling;
	/**
	 * @brief De constructor van de Commissie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->commissieID = NULL;
		$this->naam = '';
		$this->soort = 'CIE';
		$this->categorie = NULL;
		$this->categorie_categorieID = NULL;
		$this->thema = NULL;
		$this->omschrijving = NULL;
		$this->datumBegin = new DateTimeLocale(NULL);
		$this->datumEind = new DateTimeLocale(NULL);
		$this->login = '';
		$this->email = NULL;
		$this->homepage = NULL;
		$this->emailZichtbaar = True;
		$this->commissieLidVerzameling = NULL;
		$this->mededelingVerzameling = NULL;
		$this->mailingVerzameling = NULL;
		$this->commissieActiviteitVerzameling = NULL;
		$this->contractonderdeelVerzameling = NULL;
		$this->interactieVerzameling = NULL;
		$this->tagVerzameling = NULL;
		$this->bugCategorieVerzameling = NULL;
		$this->kartVoorwerpVerzameling = NULL;
		$this->aesArtikelVerzameling = NULL;
		$this->commissieFotoVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissieID.
	 */
	public function getCommissieID()
	{
		return $this->commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('CIE','GROEP','DISPUUT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @return CommissieCategorie
	 * De waarde van het veld categorie.
	 */
	public function getCategorie()
	{
		if(!isset($this->categorie)
		 && isset($this->categorie_categorieID)
		 ) {
			$this->categorie = CommissieCategorie::geef
					( $this->categorie_categorieID
					);
		}
		return $this->categorie;
	}
	/**
	 * @brief Stel de waarde van het veld categorie in.
	 *
	 * @param mixed $new_categorieID De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setCategorie($new_categorieID)
	{
		unset($this->errors['Categorie']);
		if($new_categorieID instanceof CommissieCategorie
		) {
			if($this->categorie == $new_categorieID
			&& $this->categorie_categorieID == $this->categorie->getCategorieID())
				return $this;
			$this->categorie = $new_categorieID;
			$this->categorie_categorieID
					= $this->categorie->getCategorieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_categorieID) || $new_categorieID == 0)) {
			if($this->categorie == NULL && $this->categorie_categorieID == NULL)
				return $this;
			$this->categorie = NULL;
			$this->categorie_categorieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_categorieID)
		) {
			if($this->categorie == NULL 
				&& $this->categorie_categorieID == (int)$new_categorieID)
				return $this;
			$this->categorie = NULL;
			$this->categorie_categorieID
					= (int)$new_categorieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld categorie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld categorie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCategorie()
	{
		if (array_key_exists('Categorie', $this->errors))
			return $this->errors['Categorie'];
		$waarde1 = $this->getCategorie();
		$waarde2 = $this->getCategorieCategorieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie_categorieID.
	 *
	 * @return int
	 * De waarde van het veld categorie_categorieID.
	 */
	public function getCategorieCategorieID()
	{
		if (is_null($this->categorie_categorieID) && isset($this->categorie)) {
			$this->categorie_categorieID = $this->categorie->getCategorieID();
		}
		return $this->categorie_categorieID;
	}
	/**
	 * @brief Geef de waarde van het veld thema.
	 *
	 * @return string
	 * De waarde van het veld thema.
	 */
	public function getThema()
	{
		return $this->thema;
	}
	/**
	 * @brief Stel de waarde van het veld thema in.
	 *
	 * @param mixed $newThema De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setThema($newThema)
	{
		unset($this->errors['Thema']);
		if(!is_null($newThema))
			$newThema = trim($newThema);
		if($newThema === "")
			$newThema = NULL;
		if($this->thema === $newThema)
			return $this;

		$this->thema = $newThema;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld thema geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld thema geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkThema()
	{
		if (array_key_exists('Thema', $this->errors))
			return $this->errors['Thema'];
		$waarde = $this->getThema();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumBegin.
	 */
	public function getDatumBegin()
	{
		return $this->datumBegin;
	}
	/**
	 * @brief Stel de waarde van het veld datumBegin in.
	 *
	 * @param mixed $newDatumBegin De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setDatumBegin($newDatumBegin)
	{
		unset($this->errors['DatumBegin']);
		if(!$newDatumBegin instanceof DateTimeLocale) {
			try {
				$newDatumBegin = new DateTimeLocale($newDatumBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumBegin->strftime('%F %T') == $newDatumBegin->strftime('%F %T'))
			return $this;

		$this->datumBegin = $newDatumBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumBegin()
	{
		if (array_key_exists('DatumBegin', $this->errors))
			return $this->errors['DatumBegin'];
		$waarde = $this->getDatumBegin();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumEind.
	 */
	public function getDatumEind()
	{
		return $this->datumEind;
	}
	/**
	 * @brief Stel de waarde van het veld datumEind in.
	 *
	 * @param mixed $newDatumEind De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setDatumEind($newDatumEind)
	{
		unset($this->errors['DatumEind']);
		if(!$newDatumEind instanceof DateTimeLocale) {
			try {
				$newDatumEind = new DateTimeLocale($newDatumEind);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumEind->strftime('%F %T') == $newDatumEind->strftime('%F %T'))
			return $this;

		$this->datumEind = $newDatumEind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumEind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumEind geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumEind()
	{
		if (array_key_exists('DatumEind', $this->errors))
			return $this->errors['DatumEind'];
		$waarde = $this->getDatumEind();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld login.
	 *
	 * @return string
	 * De waarde van het veld login.
	 */
	public function getLogin()
	{
		return $this->login;
	}
	/**
	 * @brief Stel de waarde van het veld login in.
	 *
	 * @param mixed $newLogin De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setLogin($newLogin)
	{
		unset($this->errors['Login']);
		if(!is_null($newLogin))
			$newLogin = trim($newLogin);
		if($newLogin === "")
			$newLogin = NULL;
		if($this->login === $newLogin)
			return $this;

		$this->login = $newLogin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld login geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld login geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLogin()
	{
		if (array_key_exists('Login', $this->errors))
			return $this->errors['Login'];
		$waarde = $this->getLogin();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld email.
	 *
	 * @return string
	 * De waarde van het veld email.
	 */
	public function getEmail()
	{
		return $this->email;
	}
	/**
	 * @brief Stel de waarde van het veld email in.
	 *
	 * @param mixed $newEmail De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setEmail($newEmail)
	{
		unset($this->errors['Email']);
		if(!is_null($newEmail))
			$newEmail = trim($newEmail);
		if($newEmail === "")
			$newEmail = NULL;
		if($this->email === $newEmail)
			return $this;

		$this->email = $newEmail;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld email geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld email geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkEmail()
	{
		if (array_key_exists('Email', $this->errors))
			return $this->errors['Email'];
		$waarde = $this->getEmail();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld homepage.
	 *
	 * @return string
	 * De waarde van het veld homepage.
	 */
	public function getHomepage()
	{
		return $this->homepage;
	}
	/**
	 * @brief Stel de waarde van het veld homepage in.
	 *
	 * @param mixed $newHomepage De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setHomepage($newHomepage)
	{
		unset($this->errors['Homepage']);
		if(!is_null($newHomepage))
			$newHomepage = trim($newHomepage);
		if($newHomepage === "")
			$newHomepage = NULL;
		if($this->homepage === $newHomepage)
			return $this;

		$this->homepage = $newHomepage;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld homepage geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld homepage geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkHomepage()
	{
		if (array_key_exists('Homepage', $this->errors))
			return $this->errors['Homepage'];
		$waarde = $this->getHomepage();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld emailZichtbaar.
	 *
	 * @return bool
	 * De waarde van het veld emailZichtbaar.
	 */
	public function getEmailZichtbaar()
	{
		return $this->emailZichtbaar;
	}
	/**
	 * @brief Stel de waarde van het veld emailZichtbaar in.
	 *
	 * @param mixed $newEmailZichtbaar De nieuwe waarde.
	 *
	 * @return Commissie
	 * Dit Commissie-object.
	 */
	public function setEmailZichtbaar($newEmailZichtbaar)
	{
		unset($this->errors['EmailZichtbaar']);
		if(!is_null($newEmailZichtbaar))
			$newEmailZichtbaar = (bool)$newEmailZichtbaar;
		if($this->emailZichtbaar === $newEmailZichtbaar)
			return $this;

		$this->emailZichtbaar = $newEmailZichtbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld emailZichtbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld emailZichtbaar geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkEmailZichtbaar()
	{
		if (array_key_exists('EmailZichtbaar', $this->errors))
			return $this->errors['EmailZichtbaar'];
		$waarde = $this->getEmailZichtbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de CommissieLidVerzameling die hoort bij dit object.
	 */
	public function getCommissieLidVerzameling()
	{
		if(!$this->commissieLidVerzameling instanceof CommissieLidVerzameling)
			$this->commissieLidVerzameling = CommissieLidVerzameling::fromCommissie($this);
		return $this->commissieLidVerzameling;
	}
	/**
	 * @brief Returneert de MededelingVerzameling die hoort bij dit object.
	 */
	public function getMededelingVerzameling()
	{
		if(!$this->mededelingVerzameling instanceof MededelingVerzameling)
			$this->mededelingVerzameling = MededelingVerzameling::fromCommissie($this);
		return $this->mededelingVerzameling;
	}
	/**
	 * @brief Returneert de MailingVerzameling die hoort bij dit object.
	 */
	public function getMailingVerzameling()
	{
		if(!$this->mailingVerzameling instanceof MailingVerzameling)
			$this->mailingVerzameling = MailingVerzameling::fromCommissie($this);
		return $this->mailingVerzameling;
	}
	/**
	 * @brief Returneert de CommissieActiviteitVerzameling die hoort bij dit object.
	 */
	public function getCommissieActiviteitVerzameling()
	{
		if(!$this->commissieActiviteitVerzameling instanceof CommissieActiviteitVerzameling)
			$this->commissieActiviteitVerzameling = CommissieActiviteitVerzameling::fromCommissie($this);
		return $this->commissieActiviteitVerzameling;
	}
	/**
	 * @brief Returneert de ContractonderdeelVerzameling die hoort bij dit object.
	 */
	public function getContractonderdeelVerzameling()
	{
		if(!$this->contractonderdeelVerzameling instanceof ContractonderdeelVerzameling)
			$this->contractonderdeelVerzameling = ContractonderdeelVerzameling::fromCommissie($this);
		return $this->contractonderdeelVerzameling;
	}
	/**
	 * @brief Returneert de InteractieVerzameling die hoort bij dit object.
	 */
	public function getInteractieVerzameling()
	{
		if(!$this->interactieVerzameling instanceof InteractieVerzameling)
			$this->interactieVerzameling = InteractieVerzameling::fromCommissie($this);
		return $this->interactieVerzameling;
	}
	/**
	 * @brief Returneert de TagVerzameling die hoort bij dit object.
	 */
	public function getTagVerzameling()
	{
		if(!$this->tagVerzameling instanceof TagVerzameling)
			$this->tagVerzameling = TagVerzameling::fromCommissie($this);
		return $this->tagVerzameling;
	}
	/**
	 * @brief Returneert de BugCategorieVerzameling die hoort bij dit object.
	 */
	public function getBugCategorieVerzameling()
	{
		if(!$this->bugCategorieVerzameling instanceof BugCategorieVerzameling)
			$this->bugCategorieVerzameling = BugCategorieVerzameling::fromCommissie($this);
		return $this->bugCategorieVerzameling;
	}
	/**
	 * @brief Returneert de KartVoorwerpVerzameling die hoort bij dit object.
	 */
	public function getKartVoorwerpVerzameling()
	{
		if(!$this->kartVoorwerpVerzameling instanceof KartVoorwerpVerzameling)
			$this->kartVoorwerpVerzameling = KartVoorwerpVerzameling::fromCommissie($this);
		return $this->kartVoorwerpVerzameling;
	}
	/**
	 * @brief Returneert de AesArtikelVerzameling die hoort bij dit object.
	 */
	public function getAesArtikelVerzameling()
	{
		if(!$this->aesArtikelVerzameling instanceof AesArtikelVerzameling)
			$this->aesArtikelVerzameling = AesArtikelVerzameling::fromCommissie($this);
		return $this->aesArtikelVerzameling;
	}
	/**
	 * @brief Returneert de CommissieFotoVerzameling die hoort bij dit object.
	 */
	public function getCommissieFotoVerzameling()
	{
		if(!$this->commissieFotoVerzameling instanceof CommissieFotoVerzameling)
			$this->commissieFotoVerzameling = CommissieFotoVerzameling::fromCommissie($this);
		return $this->commissieFotoVerzameling;
	}
	abstract public function activiteiten();
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Commissie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Commissie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Commissie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getCommissieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Commissie|false
	 * Een Commissie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$commissieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$commissieID = (int)$a;
		}

		if(is_null($commissieID))
			throw new BadMethodCallException();

		static::cache(array( array($commissieID) ));
		return Entiteit::geefCache(array($commissieID), 'Commissie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Commissie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Commissie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Commissie`.`commissieID`'
		                 .     ', `Commissie`.`naam`'
		                 .     ', `Commissie`.`soort`'
		                 .     ', `Commissie`.`categorie_categorieID`'
		                 .     ', `Commissie`.`thema`'
		                 .     ', `Commissie`.`omschrijving`'
		                 .     ', `Commissie`.`datumBegin`'
		                 .     ', `Commissie`.`datumEind`'
		                 .     ', `Commissie`.`login`'
		                 .     ', `Commissie`.`email`'
		                 .     ', `Commissie`.`homepage`'
		                 .     ', `Commissie`.`emailZichtbaar`'
		                 .     ', `Commissie`.`gewijzigdWanneer`'
		                 .     ', `Commissie`.`gewijzigdWie`'
		                 .' FROM `Commissie`'
		                 .' WHERE (`commissieID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['commissieID']);

			$obj = new Commissie();

			$obj->inDB = True;

			$obj->commissieID  = (int) $row['commissieID'];
			$obj->naam  = trim($row['naam']);
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->categorie_categorieID  = (is_null($row['categorie_categorieID'])) ? null : (int) $row['categorie_categorieID'];
			$obj->thema  = (is_null($row['thema'])) ? null : trim($row['thema']);
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->datumBegin  = new DateTimeLocale($row['datumBegin']);
			$obj->datumEind  = new DateTimeLocale($row['datumEind']);
			$obj->login  = trim($row['login']);
			$obj->email  = (is_null($row['email'])) ? null : trim($row['email']);
			$obj->homepage = $row['homepage'];
			$obj->emailZichtbaar  = (bool) $row['emailZichtbaar'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Commissie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getCategorieCategorieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->commissieID =
			$WSW4DB->q('RETURNID INSERT INTO `Commissie`'
			          . ' (`naam`, `soort`, `categorie_categorieID`, `thema`, `omschrijving`, `datumBegin`, `datumEind`, `login`, `email`, `homepage`, `emailZichtbaar`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %i, %s, %s, %s, %s, %s, %s, %s, %i, %s, %i)'
			          , $this->naam
			          , $this->soort
			          , $this->categorie_categorieID
			          , $this->thema
			          , $this->omschrijving
			          , (!is_null($this->datumBegin))?$this->datumBegin->strftime('%F %T'):null
			          , (!is_null($this->datumEind))?$this->datumEind->strftime('%F %T'):null
			          , $this->login
			          , $this->email
			          , $this->homepage
			          , $this->emailZichtbaar
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Commissie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Commissie`'
			          .' SET `naam` = %s'
			          .   ', `soort` = %s'
			          .   ', `categorie_categorieID` = %i'
			          .   ', `thema` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `datumBegin` = %s'
			          .   ', `datumEind` = %s'
			          .   ', `login` = %s'
			          .   ', `email` = %s'
			          .   ', `homepage` = %s'
			          .   ', `emailZichtbaar` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `commissieID` = %i'
			          , $this->naam
			          , $this->soort
			          , $this->categorie_categorieID
			          , $this->thema
			          , $this->omschrijving
			          , (!is_null($this->datumBegin))?$this->datumBegin->strftime('%F %T'):null
			          , (!is_null($this->datumEind))?$this->datumEind->strftime('%F %T'):null
			          , $this->login
			          , $this->email
			          , $this->homepage
			          , $this->emailZichtbaar
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->commissieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenCommissie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenCommissie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenCommissie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Categorie';
			$velden[] = 'Thema';
			$velden[] = 'Omschrijving';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Login';
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'EmailZichtbaar';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			$velden[] = 'Login';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Categorie';
			$velden[] = 'Thema';
			$velden[] = 'Omschrijving';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Login';
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'EmailZichtbaar';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Categorie';
			$velden[] = 'Thema';
			$velden[] = 'Omschrijving';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Login';
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'EmailZichtbaar';
		case 'primary':
			$velden[] = 'categorie_categorieID';
			break;
		case 'verzamelingen':
			$velden[] = 'CommissieLidVerzameling';
			$velden[] = 'MededelingVerzameling';
			$velden[] = 'MailingVerzameling';
			$velden[] = 'CommissieActiviteitVerzameling';
			$velden[] = 'ContractonderdeelVerzameling';
			$velden[] = 'InteractieVerzameling';
			$velden[] = 'TagVerzameling';
			$velden[] = 'BugCategorieVerzameling';
			$velden[] = 'KartVoorwerpVerzameling';
			$velden[] = 'AesArtikelVerzameling';
			$velden[] = 'CommissieFotoVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Categorie';
			$velden[] = 'Thema';
			$velden[] = 'Omschrijving';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Login';
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'EmailZichtbaar';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle CommissieLid-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$CommissieLidFromCommissieVerz = CommissieLidVerzameling::fromCommissie($this);
		$returnValue = $CommissieLidFromCommissieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CommissieLid met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Mededeling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MededelingFromCommissieVerz = MededelingVerzameling::fromCommissie($this);
		$returnValue = $MededelingFromCommissieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Mededeling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Mailing-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$MailingFromCommissieVerz = MailingVerzameling::fromCommissie($this);
		$returnValue = $MailingFromCommissieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Mailing met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle CommissieActiviteit-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$CommissieActiviteitFromCommissieVerz = CommissieActiviteitVerzameling::fromCommissie($this);
		$returnValue = $CommissieActiviteitFromCommissieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CommissieActiviteit met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Contractonderdeel-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$ContractonderdeelFromCommissieVerz = ContractonderdeelVerzameling::fromCommissie($this);
		$returnValue = $ContractonderdeelFromCommissieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Contractonderdeel met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Interactie-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$InteractieFromCommissieVerz = InteractieVerzameling::fromCommissie($this);
		$returnValue = $InteractieFromCommissieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Interactie met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Tag-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$TagFromCommissieVerz = TagVerzameling::fromCommissie($this);
		$returnValue = $TagFromCommissieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tag met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugCategorie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugCategorieFromCommissieVerz = BugCategorieVerzameling::fromCommissie($this);
		$returnValue = $BugCategorieFromCommissieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugCategorie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle KartVoorwerp-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$KartVoorwerpFromCommissieVerz = KartVoorwerpVerzameling::fromCommissie($this);
		$returnValue = $KartVoorwerpFromCommissieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object KartVoorwerp met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle AesArtikel-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$AesArtikelFromCommissieVerz = AesArtikelVerzameling::fromCommissie($this);
		$returnValue = $AesArtikelFromCommissieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object AesArtikel met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle CommissieFoto-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$CommissieFotoFromCommissieVerz = CommissieFotoVerzameling::fromCommissie($this);
		$returnValue = $CommissieFotoFromCommissieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CommissieFoto met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($MailingFromCommissieVerz as $v)
		{
			$v->setCommissie(NULL);
		}
		$MailingFromCommissieVerz->opslaan();

		foreach($ContractonderdeelFromCommissieVerz as $v)
		{
			$v->setCommissie(NULL);
		}
		$ContractonderdeelFromCommissieVerz->opslaan();

		foreach($InteractieFromCommissieVerz as $v)
		{
			$v->setCommissie(NULL);
		}
		$InteractieFromCommissieVerz->opslaan();

		foreach($TagFromCommissieVerz as $v)
		{
			$v->setCommissie(NULL);
		}
		$TagFromCommissieVerz->opslaan();

		foreach($KartVoorwerpFromCommissieVerz as $v)
		{
			$v->setCommissie(NULL);
		}
		$KartVoorwerpFromCommissieVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $CommissieLidFromCommissieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $MededelingFromCommissieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $CommissieActiviteitFromCommissieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BugCategorieFromCommissieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $AesArtikelFromCommissieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $CommissieFotoFromCommissieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Commissie`'
		          .' WHERE `commissieID` = %i'
		          .' LIMIT 1'
		          , $this->commissieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->commissieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Commissie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `commissieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->commissieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Commissie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'commissieid':
			return 'int';
		case 'naam':
			return 'string';
		case 'soort':
			return 'enum';
		case 'categorie':
			return 'foreign';
		case 'categorie_categorieid':
			return 'int';
		case 'thema':
			return 'string';
		case 'omschrijving':
			return 'string';
		case 'datumbegin':
			return 'date';
		case 'datumeind':
			return 'date';
		case 'login':
			return 'string';
		case 'email':
			return 'string';
		case 'homepage':
			return 'url';
		case 'emailzichtbaar':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'commissieID':
		case 'categorie_categorieID':
		case 'emailZichtbaar':
			$type = '%i';
			break;
		case 'naam':
		case 'soort':
		case 'thema':
		case 'omschrijving':
		case 'datumBegin':
		case 'datumEind':
		case 'login':
		case 'email':
		case 'homepage':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Commissie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `commissieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->commissieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Commissie`'
		          .' WHERE `commissieID` = %i'
		                 , $veld
		          , $this->commissieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Commissie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle CommissieLid-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CommissieLidFromCommissieVerz = CommissieLidVerzameling::fromCommissie($this);
		$dependencies['CommissieLid'] = $CommissieLidFromCommissieVerz;

		// Verzamel alle Mededeling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MededelingFromCommissieVerz = MededelingVerzameling::fromCommissie($this);
		$dependencies['Mededeling'] = $MededelingFromCommissieVerz;

		// Verzamel alle Mailing-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MailingFromCommissieVerz = MailingVerzameling::fromCommissie($this);
		$dependencies['Mailing'] = $MailingFromCommissieVerz;

		// Verzamel alle CommissieActiviteit-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CommissieActiviteitFromCommissieVerz = CommissieActiviteitVerzameling::fromCommissie($this);
		$dependencies['CommissieActiviteit'] = $CommissieActiviteitFromCommissieVerz;

		// Verzamel alle Contractonderdeel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContractonderdeelFromCommissieVerz = ContractonderdeelVerzameling::fromCommissie($this);
		$dependencies['Contractonderdeel'] = $ContractonderdeelFromCommissieVerz;

		// Verzamel alle Interactie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$InteractieFromCommissieVerz = InteractieVerzameling::fromCommissie($this);
		$dependencies['Interactie'] = $InteractieFromCommissieVerz;

		// Verzamel alle Tag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagFromCommissieVerz = TagVerzameling::fromCommissie($this);
		$dependencies['Tag'] = $TagFromCommissieVerz;

		// Verzamel alle BugCategorie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugCategorieFromCommissieVerz = BugCategorieVerzameling::fromCommissie($this);
		$dependencies['BugCategorie'] = $BugCategorieFromCommissieVerz;

		// Verzamel alle KartVoorwerp-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$KartVoorwerpFromCommissieVerz = KartVoorwerpVerzameling::fromCommissie($this);
		$dependencies['KartVoorwerp'] = $KartVoorwerpFromCommissieVerz;

		// Verzamel alle AesArtikel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$AesArtikelFromCommissieVerz = AesArtikelVerzameling::fromCommissie($this);
		$dependencies['AesArtikel'] = $AesArtikelFromCommissieVerz;

		// Verzamel alle CommissieFoto-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CommissieFotoFromCommissieVerz = CommissieFotoVerzameling::fromCommissie($this);
		$dependencies['CommissieFoto'] = $CommissieFotoFromCommissieVerz;

		return $dependencies;
	}
}
