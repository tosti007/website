<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class DocuBestand_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $titel;
	protected $extensie;
	protected $categorie;
	protected $categorie_id;			/**< \brief PRIMARY */
	protected $datum;
	/**
	 * @brief De constructor van de DocuBestand_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->titel = '';
		$this->extensie = '';
		$this->categorie = NULL;
		$this->categorie_id = 0;
		$this->datum = new DateTimeLocale();
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 *
	 * @return DocuBestand
	 * Dit DocuBestand-object.
	 */
	public function setTitel($newTitel)
	{
		unset($this->errors['Titel']);
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel === $newTitel)
			return $this;

		$this->titel = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel()
	{
		if (array_key_exists('Titel', $this->errors))
			return $this->errors['Titel'];
		$waarde = $this->getTitel();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld extensie.
	 *
	 * @return string
	 * De waarde van het veld extensie.
	 */
	public function getExtensie()
	{
		return $this->extensie;
	}
	/**
	 * @brief Stel de waarde van het veld extensie in.
	 *
	 * @param mixed $newExtensie De nieuwe waarde.
	 *
	 * @return DocuBestand
	 * Dit DocuBestand-object.
	 */
	public function setExtensie($newExtensie)
	{
		unset($this->errors['Extensie']);
		if(!is_null($newExtensie))
			$newExtensie = trim($newExtensie);
		if($newExtensie === "")
			$newExtensie = NULL;
		if($this->extensie === $newExtensie)
			return $this;

		$this->extensie = $newExtensie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld extensie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld extensie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkExtensie()
	{
		if (array_key_exists('Extensie', $this->errors))
			return $this->errors['Extensie'];
		$waarde = $this->getExtensie();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @return DocuCategorie
	 * De waarde van het veld categorie.
	 */
	public function getCategorie()
	{
		if(!isset($this->categorie)
		 && isset($this->categorie_id)
		 ) {
			$this->categorie = DocuCategorie::geef
					( $this->categorie_id
					);
		}
		return $this->categorie;
	}
	/**
	 * @brief Stel de waarde van het veld categorie in.
	 *
	 * @param mixed $new_id De nieuwe waarde.
	 *
	 * @return DocuBestand
	 * Dit DocuBestand-object.
	 */
	public function setCategorie($new_id)
	{
		unset($this->errors['Categorie']);
		if($new_id instanceof DocuCategorie
		) {
			if($this->categorie == $new_id
			&& $this->categorie_id == $this->categorie->getId())
				return $this;
			$this->categorie = $new_id;
			$this->categorie_id
					= $this->categorie->getId();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_id)
		) {
			if($this->categorie == NULL 
				&& $this->categorie_id == (int)$new_id)
				return $this;
			$this->categorie = NULL;
			$this->categorie_id
					= (int)$new_id;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld categorie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld categorie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCategorie()
	{
		if (array_key_exists('Categorie', $this->errors))
			return $this->errors['Categorie'];
		$waarde1 = $this->getCategorie();
		$waarde2 = $this->getCategorieId();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie_id.
	 *
	 * @return int
	 * De waarde van het veld categorie_id.
	 */
	public function getCategorieId()
	{
		if (is_null($this->categorie_id) && isset($this->categorie)) {
			$this->categorie_id = $this->categorie->getId();
		}
		return $this->categorie_id;
	}
	/**
	 * @brief Geef de waarde van het veld datum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datum.
	 */
	public function getDatum()
	{
		return $this->datum;
	}
	/**
	 * @brief Stel de waarde van het veld datum in.
	 *
	 * @param mixed $newDatum De nieuwe waarde.
	 *
	 * @return DocuBestand
	 * Dit DocuBestand-object.
	 */
	public function setDatum($newDatum)
	{
		unset($this->errors['Datum']);
		if(!$newDatum instanceof DateTimeLocale) {
			try {
				$newDatum = new DateTimeLocale($newDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datum->strftime('%F %T') == $newDatum->strftime('%F %T'))
			return $this;

		$this->datum = $newDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datum geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkDatum()
	{
		if (array_key_exists('Datum', $this->errors))
			return $this->errors['Datum'];
		$waarde = $this->getDatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return DocuBestand::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van DocuBestand.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return DocuBestand::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return DocuBestand|false
	 * Een DocuBestand-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'DocuBestand');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'DocuBestand');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('DocuBestand::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `DocuBestand`.`id`'
		                 .     ', `DocuBestand`.`titel`'
		                 .     ', `DocuBestand`.`extensie`'
		                 .     ', `DocuBestand`.`categorie_id`'
		                 .     ', `DocuBestand`.`datum`'
		                 .     ', `DocuBestand`.`gewijzigdWanneer`'
		                 .     ', `DocuBestand`.`gewijzigdWie`'
		                 .' FROM `DocuBestand`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new DocuBestand();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->titel  = trim($row['titel']);
			$obj->extensie  = trim($row['extensie']);
			$obj->categorie_id  = (int) $row['categorie_id'];
			$obj->datum  = new DateTimeLocale($row['datum']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'DocuBestand')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getCategorieId();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `DocuBestand`'
			          . ' (`titel`, `extensie`, `categorie_id`, `datum`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %i, %s, %s, %i)'
			          , $this->titel
			          , $this->extensie
			          , $this->categorie_id
			          , $this->datum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'DocuBestand')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `DocuBestand`'
			          .' SET `titel` = %s'
			          .   ', `extensie` = %s'
			          .   ', `categorie_id` = %i'
			          .   ', `datum` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->titel
			          , $this->extensie
			          , $this->categorie_id
			          , $this->datum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDocuBestand
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDocuBestand($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDocuBestand($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Titel';
			$velden[] = 'Extensie';
			$velden[] = 'Categorie';
			$velden[] = 'Datum';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Titel';
			$velden[] = 'Extensie';
			$velden[] = 'Categorie';
			$velden[] = 'Datum';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Titel';
			$velden[] = 'Extensie';
			$velden[] = 'Categorie';
			$velden[] = 'Datum';
			break;
		case 'get':
			$velden[] = 'Titel';
			$velden[] = 'Extensie';
			$velden[] = 'Categorie';
			$velden[] = 'Datum';
		case 'primary':
			$velden[] = 'categorie_id';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Titel';
			$velden[] = 'Extensie';
			$velden[] = 'Categorie';
			$velden[] = 'Datum';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `DocuBestand`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `DocuBestand`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van DocuBestand terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'titel':
			return 'string';
		case 'extensie':
			return 'string';
		case 'categorie':
			return 'foreign';
		case 'categorie_id':
			return 'int';
		case 'datum':
			return 'datetime';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'categorie_id':
			$type = '%i';
			break;
		case 'titel':
		case 'extensie':
		case 'datum':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `DocuBestand`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `DocuBestand`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'DocuBestand');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
