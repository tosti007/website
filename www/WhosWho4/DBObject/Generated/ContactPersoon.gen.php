<?
/**
 * @brief 'AT'AUTH_SET:bestuur
 */
abstract class ContactPersoon_Generated
	extends Entiteit
{
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $organisatie;				/**< \brief PRIMARY */
	protected $organisatie_contactID;	/**< \brief PRIMARY */
	protected $type;					/**< \brief PRIMARY, ENUM:NORMAAL/SPOOK/BEDRIJFSCONTACT/OVERIGSPOOKWEB/ALUMNUS */
	protected $Functie;					/**< \brief NULL */
	protected $beginDatum;				/**< \brief NULL */
	protected $eindDatum;				/**< \brief NULL */
	protected $opmerking;				/**< \brief Voor bv info over meerdere periodes NULL */
	/** Verzamelingen **/
	protected $interactieVerzameling;
	/**
	/**
	 * @brief De constructor van de ContactPersoon_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 * @param mixed $b Organisatie (Organisatie OR Array(organisatie_contactID) OR
	 * organisatie_contactID)
	 * @param mixed $c Type (enum)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = 'NORMAAL')
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Organisatie)
		{
			$this->organisatie = $b;
			$this->organisatie_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->organisatie = NULL;
			$this->organisatie_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->organisatie = NULL;
			$this->organisatie_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$c = strtoupper(trim($c));
		if(!in_array($c, static::enumsType()))
			throw new BadMethodCallException();
		$this->type = $c;
		$this->Functie = NULL;
		$this->beginDatum = new DateTimeLocale(NULL);
		$this->eindDatum = new DateTimeLocale(NULL);
		$this->opmerking = NULL;
		$this->interactieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		$volgorde[] = 'Organisatie';
		$volgorde[] = 'Type';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld organisatie.
	 *
	 * @return Organisatie
	 * De waarde van het veld organisatie.
	 */
	public function getOrganisatie()
	{
		if(!isset($this->organisatie)
		 && isset($this->organisatie_contactID)
		 ) {
			$this->organisatie = Organisatie::geef
					( $this->organisatie_contactID
					);
		}
		return $this->organisatie;
	}
	/**
	 * @brief Geef de waarde van het veld organisatie_contactID.
	 *
	 * @return int
	 * De waarde van het veld organisatie_contactID.
	 */
	public function getOrganisatieContactID()
	{
		if (is_null($this->organisatie_contactID) && isset($this->organisatie)) {
			$this->organisatie_contactID = $this->organisatie->getContactID();
		}
		return $this->organisatie_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld type.
	 */
	static public function enumsType()
	{
		static $vals = array('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @return string
	 * De waarde van het veld type.
	 */
	public function getType()
	{
		return $this->type;
	}
	/**
	 * @brief Geef de waarde van het veld Functie.
	 *
	 * @return string
	 * De waarde van het veld Functie.
	 */
	public function getFunctie()
	{
		return $this->Functie;
	}
	/**
	 * @brief Stel de waarde van het veld Functie in.
	 *
	 * @param mixed $newFunctie De nieuwe waarde.
	 *
	 * @return ContactPersoon
	 * Dit ContactPersoon-object.
	 */
	public function setFunctie($newFunctie)
	{
		unset($this->errors['Functie']);
		if(!is_null($newFunctie))
			$newFunctie = trim($newFunctie);
		if($newFunctie === "")
			$newFunctie = NULL;
		if($this->Functie === $newFunctie)
			return $this;

		$this->Functie = $newFunctie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld Functie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld Functie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkFunctie()
	{
		if (array_key_exists('Functie', $this->errors))
			return $this->errors['Functie'];
		$waarde = $this->getFunctie();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld beginDatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld beginDatum.
	 */
	public function getBeginDatum()
	{
		return $this->beginDatum;
	}
	/**
	 * @brief Stel de waarde van het veld beginDatum in.
	 *
	 * @param mixed $newBeginDatum De nieuwe waarde.
	 *
	 * @return ContactPersoon
	 * Dit ContactPersoon-object.
	 */
	public function setBeginDatum($newBeginDatum)
	{
		unset($this->errors['BeginDatum']);
		if(!$newBeginDatum instanceof DateTimeLocale) {
			try {
				$newBeginDatum = new DateTimeLocale($newBeginDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->beginDatum->strftime('%F %T') == $newBeginDatum->strftime('%F %T'))
			return $this;

		$this->beginDatum = $newBeginDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld beginDatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld beginDatum geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBeginDatum()
	{
		if (array_key_exists('BeginDatum', $this->errors))
			return $this->errors['BeginDatum'];
		$waarde = $this->getBeginDatum();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld eindDatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld eindDatum.
	 */
	public function getEindDatum()
	{
		return $this->eindDatum;
	}
	/**
	 * @brief Stel de waarde van het veld eindDatum in.
	 *
	 * @param mixed $newEindDatum De nieuwe waarde.
	 *
	 * @return ContactPersoon
	 * Dit ContactPersoon-object.
	 */
	public function setEindDatum($newEindDatum)
	{
		unset($this->errors['EindDatum']);
		if(!$newEindDatum instanceof DateTimeLocale) {
			try {
				$newEindDatum = new DateTimeLocale($newEindDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->eindDatum->strftime('%F %T') == $newEindDatum->strftime('%F %T'))
			return $this;

		$this->eindDatum = $newEindDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld eindDatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld eindDatum geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkEindDatum()
	{
		if (array_key_exists('EindDatum', $this->errors))
			return $this->errors['EindDatum'];
		$waarde = $this->getEindDatum();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return ContactPersoon
	 * Dit ContactPersoon-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de InteractieVerzameling die hoort bij dit object.
	 */
	public function getInteractieVerzameling()
	{
		if(!$this->interactieVerzameling instanceof InteractieVerzameling)
		{
			$this->interactieVerzameling = new InteractieVerzameling();
			$this->interactieVerzameling->union(InteractieVerzameling::fromSpook($this));
			$this->interactieVerzameling->union(InteractieVerzameling::fromBedrijfPersoon($this));
		}
		return $this->interactieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ContactPersoon::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ContactPersoon.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ContactPersoon::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPersoonContactID()
		                      , $this->getOrganisatieContactID()
		                      , $this->getType()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 * @param mixed $c Object of ID waarop gezocht moet worden.
	 *
	 * @return ContactPersoon|false
	 * Een ContactPersoon-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL, $c = NULL)
	{
		if(is_null($a)
		&& is_null($b)
		&& is_null($c))
			return false;

		if(is_string($a)
		&& is_null($b)
		&& is_null($c))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& is_null($c)
		&& count($a) == 3)
		{
			$persoon_contactID = (int)$a[0];
			$organisatie_contactID = (int)$a[1];
			$type = $a[2];
		}
		else if($a instanceof Persoon
		     && $b instanceof Organisatie
		     && isset($c))
		{
			$persoon_contactID = $a->getContactID();
			$organisatie_contactID = $b->getContactID();
			$type = $c;
		}
		else if(isset($a)
		     && isset($b)
		     && isset($c))
		{
			$persoon_contactID = (int)$a;
			$organisatie_contactID = (int)$b;
			$type = $c;
		}

		if(is_null($persoon_contactID)
		|| is_null($organisatie_contactID)
		|| is_null($type))
			throw new BadMethodCallException();

		static::cache(array( array($persoon_contactID, $organisatie_contactID, $type) ));
		return Entiteit::geefCache(array($persoon_contactID, $organisatie_contactID, $type), 'ContactPersoon');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'ContactPersoon');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('ContactPersoon::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `ContactPersoon`.`persoon_contactID`'
		                 .     ', `ContactPersoon`.`organisatie_contactID`'
		                 .     ', `ContactPersoon`.`type`'
		                 .     ', `ContactPersoon`.`Functie`'
		                 .     ', `ContactPersoon`.`beginDatum`'
		                 .     ', `ContactPersoon`.`eindDatum`'
		                 .     ', `ContactPersoon`.`opmerking`'
		                 .     ', `ContactPersoon`.`gewijzigdWanneer`'
		                 .     ', `ContactPersoon`.`gewijzigdWie`'
		                 .' FROM `ContactPersoon`'
		                 .' WHERE (`persoon_contactID`, `organisatie_contactID`, `type`)'
		                 .      ' IN (%A{iis})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['persoon_contactID']
			                  ,$row['organisatie_contactID']
			                  ,$row['type']);

			$obj = new ContactPersoon(array($row['persoon_contactID']), array($row['organisatie_contactID']), $row['type']);

			$obj->inDB = True;

			$obj->Functie  = (is_null($row['Functie'])) ? null : trim($row['Functie']);
			$obj->beginDatum  = new DateTimeLocale($row['beginDatum']);
			$obj->eindDatum  = new DateTimeLocale($row['eindDatum']);
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ContactPersoon')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		$rel = $this->getOrganisatie();
		if(!$rel->getInDB())
			throw new LogicException('foreign Organisatie is not in DB');
		$this->organisatie_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `ContactPersoon`'
			          . ' (`persoon_contactID`, `organisatie_contactID`, `type`, `Functie`, `beginDatum`, `eindDatum`, `opmerking`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %s, %s, %s, %s, %i)'
			          , $this->persoon_contactID
			          , $this->organisatie_contactID
			          , $this->type
			          , $this->Functie
			          , (!is_null($this->beginDatum))?$this->beginDatum->strftime('%F %T'):null
			          , (!is_null($this->eindDatum))?$this->eindDatum->strftime('%F %T'):null
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'ContactPersoon')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ContactPersoon`'
			          .' SET `Functie` = %s'
			          .   ', `beginDatum` = %s'
			          .   ', `eindDatum` = %s'
			          .   ', `opmerking` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          .  ' AND `organisatie_contactID` = %i'
			          .  ' AND `type` = %s'
			          , $this->Functie
			          , (!is_null($this->beginDatum))?$this->beginDatum->strftime('%F %T'):null
			          , (!is_null($this->eindDatum))?$this->eindDatum->strftime('%F %T'):null
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          , $this->organisatie_contactID
			          , $this->type
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenContactPersoon
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenContactPersoon($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenContactPersoon($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Functie';
			$velden[] = 'BeginDatum';
			$velden[] = 'EindDatum';
			$velden[] = 'Opmerking';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Functie';
			$velden[] = 'BeginDatum';
			$velden[] = 'EindDatum';
			$velden[] = 'Opmerking';
			break;
		case 'get':
			$velden[] = 'Functie';
			$velden[] = 'BeginDatum';
			$velden[] = 'EindDatum';
			$velden[] = 'Opmerking';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'organisatie_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'InteractieVerzameling';
			break;
		default:
			$velden[] = 'Functie';
			$velden[] = 'BeginDatum';
			$velden[] = 'EindDatum';
			$velden[] = 'Opmerking';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Interactie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$InteractieFromSpookVerz = InteractieVerzameling::fromSpook($this);
		$returnValue = $InteractieFromSpookVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Interactie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Interactie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$InteractieFromBedrijfPersoonVerz = InteractieVerzameling::fromBedrijfPersoon($this);
		$returnValue = $InteractieFromBedrijfPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Interactie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $InteractieFromBedrijfPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ContactPersoon`'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `organisatie_contactID` = %i'
		          .  ' AND `type` = %s'
		          .' LIMIT 1'
		          , $this->persoon_contactID
		          , $this->organisatie_contactID
		          , $this->type
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;
		$this->organisatie = NULL;
		$this->organisatie_contactID = NULL;
		$this->type = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `ContactPersoon`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          .  ' AND `organisatie_contactID` = %i'
			          .  ' AND `type` = %s'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          , $this->organisatie_contactID
			          , $this->type
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van ContactPersoon terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'organisatie':
			return 'foreign';
		case 'organisatie_contactid':
			return 'int';
		case 'type':
			return 'enum';
		case 'functie':
			return 'string';
		case 'begindatum':
			return 'date';
		case 'einddatum':
			return 'date';
		case 'opmerking':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'persoon_contactID':
		case 'organisatie_contactID':
			$type = '%i';
			break;
		case 'type':
		case 'Functie':
		case 'beginDatum':
		case 'eindDatum':
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ContactPersoon`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `organisatie_contactID` = %i'
		          .  ' AND `type` = %s'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->persoon_contactID
		          , $this->organisatie_contactID
		          , $this->type
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ContactPersoon`'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `organisatie_contactID` = %i'
		          .  ' AND `type` = %s'
		                 , $veld
		          , $this->persoon_contactID
		          , $this->organisatie_contactID
		          , $this->type
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'ContactPersoon');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Interactie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$InteractieFromSpookVerz = InteractieVerzameling::fromSpook($this);
		$dependencies['Interactie'] = $InteractieFromSpookVerz;

		// Verzamel alle Interactie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$InteractieFromBedrijfPersoonVerz = InteractieVerzameling::fromBedrijfPersoon($this);
		$dependencies['Interactie'] = $InteractieFromBedrijfPersoonVerz;

		return $dependencies;
	}
}
