<?
/**
 * @brief 'AT'AUTH_GET:gast
 */
abstract class ActiviteitInformatie_Generated
	extends Activiteit
{
	protected $titel;					/**< \brief LANG,LEGACY_NULL */
	protected $werftekst;				/**< \brief LANG,LAZY */
	protected $homepage;				/**< \brief NULL */
	protected $locatie;					/**< \brief LANG,NULL */
	protected $prijs;					/**< \brief LANG,NULL */
	protected $inschrijfbaar;
	protected $stuurMail;
	protected $datumInschrijvenMax;		/**< \brief NULL */
	protected $datumUitschrijven;		/**< \brief NULL */
	protected $maxDeelnemers;			/**< \brief NULL */
	protected $actsoort;				/**< \brief ENUM:AES2/UU/LAND/ZUS/.COM/EXT */
	protected $onderwijs;				/**< \brief ENUM:N/B/J */
	protected $aantalComputers;
	protected $categorie;				/**< \brief LEGACY_NULL */
	protected $categorie_actCategorieID;/**< \brief PRIMARY */
	/**
	 * @brief De constructor van de ActiviteitInformatie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Activiteit

		$this->activiteitID = NULL;
		$this->titel = array('nl' => '', 'en' => '');
		$this->werftekst = array('nl' => '', 'en' => '');
		$this->homepage = NULL;
		$this->locatie = array('nl' => NULL, 'en' => NULL);
		$this->prijs = array('nl' => NULL, 'en' => NULL);
		$this->inschrijfbaar = False;
		$this->stuurMail = True;
		$this->datumInschrijvenMax = new DateTimeLocale(NULL);
		$this->datumUitschrijven = new DateTimeLocale(NULL);
		$this->maxDeelnemers = NULL;
		$this->actsoort = 'AES2';
		$this->onderwijs = 'N';
		$this->aantalComputers = 0;
		$this->categorie = NULL;
		$this->categorie_actCategorieID = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->titel[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setTitel($newTitel, $lang = NULL)
	{
		unset($this->errors['Titel']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel[$lang] === $newTitel)
			return $this;

		if(is_null($newTitel) && isset($this->titel[$lang]))
			$this->errors['Titel'] = _('dit is een verplicht veld');

		$this->titel[$lang] = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkTitel('nl');
			$ret['en'] = $this->checkTitel('en');
			return $ret;
		}

		if (array_key_exists('Titel', $this->errors))
			if (is_array($this->errors['Titel']) && array_key_exists($lang, $this->errors['Titel']))
				return $this->errors['Titel'][$lang];
		$waarde = $this->getTitel($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld werftekst.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld werftekst.
	 */
	public function getWerftekst($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->werftekst[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld werftekst in.
	 *
	 * @param mixed $newWerftekst De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setWerftekst($newWerftekst, $lang = NULL)
	{
		unset($this->errors['Werftekst']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newWerftekst))
			$newWerftekst = trim($newWerftekst);
		if($newWerftekst === "")
			$newWerftekst = NULL;
		if (!is_null($newWerftekst))
			$newWerftekst = Purifier::Purify($newWerftekst);
		if($this->werftekst[$lang] === $newWerftekst)
			return $this;

		$this->werftekst[$lang] = $newWerftekst;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld werftekst geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld werftekst geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkWerftekst($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkWerftekst('nl');
			$ret['en'] = $this->checkWerftekst('en');
			return $ret;
		}

		if (array_key_exists('Werftekst', $this->errors))
			if (is_array($this->errors['Werftekst']) && array_key_exists($lang, $this->errors['Werftekst']))
				return $this->errors['Werftekst'][$lang];
		$waarde = $this->getWerftekst($lang);
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld homepage.
	 *
	 * @return string
	 * De waarde van het veld homepage.
	 */
	public function getHomepage()
	{
		return $this->homepage;
	}
	/**
	 * @brief Stel de waarde van het veld homepage in.
	 *
	 * @param mixed $newHomepage De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setHomepage($newHomepage)
	{
		unset($this->errors['Homepage']);
		if(!is_null($newHomepage))
			$newHomepage = trim($newHomepage);
		if($newHomepage === "")
			$newHomepage = NULL;
		if($this->homepage === $newHomepage)
			return $this;

		$this->homepage = $newHomepage;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld homepage geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld homepage geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkHomepage()
	{
		if (array_key_exists('Homepage', $this->errors))
			return $this->errors['Homepage'];
		$waarde = $this->getHomepage();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld locatie.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld locatie.
	 */
	public function getLocatie($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->locatie[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld locatie in.
	 *
	 * @param mixed $newLocatie De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setLocatie($newLocatie, $lang = NULL)
	{
		unset($this->errors['Locatie']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newLocatie))
			$newLocatie = trim($newLocatie);
		if($newLocatie === "")
			$newLocatie = NULL;
		if($this->locatie[$lang] === $newLocatie)
			return $this;

		$this->locatie[$lang] = $newLocatie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld locatie geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld locatie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkLocatie($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkLocatie('nl');
			$ret['en'] = $this->checkLocatie('en');
			return $ret;
		}

		if (array_key_exists('Locatie', $this->errors))
			if (is_array($this->errors['Locatie']) && array_key_exists($lang, $this->errors['Locatie']))
				return $this->errors['Locatie'][$lang];
		$waarde = $this->getLocatie($lang);
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld prijs.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld prijs.
	 */
	public function getPrijs($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->prijs[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld prijs in.
	 *
	 * @param mixed $newPrijs De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setPrijs($newPrijs, $lang = NULL)
	{
		unset($this->errors['Prijs']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newPrijs))
			$newPrijs = trim($newPrijs);
		if($newPrijs === "")
			$newPrijs = NULL;
		if($this->prijs[$lang] === $newPrijs)
			return $this;

		$this->prijs[$lang] = $newPrijs;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld prijs geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld prijs geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkPrijs($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkPrijs('nl');
			$ret['en'] = $this->checkPrijs('en');
			return $ret;
		}

		if (array_key_exists('Prijs', $this->errors))
			if (is_array($this->errors['Prijs']) && array_key_exists($lang, $this->errors['Prijs']))
				return $this->errors['Prijs'][$lang];
		$waarde = $this->getPrijs($lang);
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inschrijfbaar.
	 *
	 * @return bool
	 * De waarde van het veld inschrijfbaar.
	 */
	public function getInschrijfbaar()
	{
		return $this->inschrijfbaar;
	}
	/**
	 * @brief Stel de waarde van het veld inschrijfbaar in.
	 *
	 * @param mixed $newInschrijfbaar De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setInschrijfbaar($newInschrijfbaar)
	{
		unset($this->errors['Inschrijfbaar']);
		if(!is_null($newInschrijfbaar))
			$newInschrijfbaar = (bool)$newInschrijfbaar;
		if($this->inschrijfbaar === $newInschrijfbaar)
			return $this;

		$this->inschrijfbaar = $newInschrijfbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inschrijfbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inschrijfbaar geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkInschrijfbaar()
	{
		if (array_key_exists('Inschrijfbaar', $this->errors))
			return $this->errors['Inschrijfbaar'];
		$waarde = $this->getInschrijfbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld stuurMail.
	 *
	 * @return bool
	 * De waarde van het veld stuurMail.
	 */
	public function getStuurMail()
	{
		return $this->stuurMail;
	}
	/**
	 * @brief Stel de waarde van het veld stuurMail in.
	 *
	 * @param mixed $newStuurMail De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setStuurMail($newStuurMail)
	{
		unset($this->errors['StuurMail']);
		if(!is_null($newStuurMail))
			$newStuurMail = (bool)$newStuurMail;
		if($this->stuurMail === $newStuurMail)
			return $this;

		$this->stuurMail = $newStuurMail;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld stuurMail geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld stuurMail geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkStuurMail()
	{
		if (array_key_exists('StuurMail', $this->errors))
			return $this->errors['StuurMail'];
		$waarde = $this->getStuurMail();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumInschrijvenMax.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumInschrijvenMax.
	 */
	public function getDatumInschrijvenMax()
	{
		return $this->datumInschrijvenMax;
	}
	/**
	 * @brief Stel de waarde van het veld datumInschrijvenMax in.
	 *
	 * @param mixed $newDatumInschrijvenMax De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setDatumInschrijvenMax($newDatumInschrijvenMax)
	{
		unset($this->errors['DatumInschrijvenMax']);
		if(!$newDatumInschrijvenMax instanceof DateTimeLocale) {
			try {
				$newDatumInschrijvenMax = new DateTimeLocale($newDatumInschrijvenMax);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumInschrijvenMax->strftime('%F %T') == $newDatumInschrijvenMax->strftime('%F %T'))
			return $this;

		$this->datumInschrijvenMax = $newDatumInschrijvenMax;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumInschrijvenMax geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumInschrijvenMax geldig is; anders
	 * een string met een foutmelding.
	 */
	public function checkDatumInschrijvenMax()
	{
		if (array_key_exists('DatumInschrijvenMax', $this->errors))
			return $this->errors['DatumInschrijvenMax'];
		$waarde = $this->getDatumInschrijvenMax();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumUitschrijven.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumUitschrijven.
	 */
	public function getDatumUitschrijven()
	{
		return $this->datumUitschrijven;
	}
	/**
	 * @brief Stel de waarde van het veld datumUitschrijven in.
	 *
	 * @param mixed $newDatumUitschrijven De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setDatumUitschrijven($newDatumUitschrijven)
	{
		unset($this->errors['DatumUitschrijven']);
		if(!$newDatumUitschrijven instanceof DateTimeLocale) {
			try {
				$newDatumUitschrijven = new DateTimeLocale($newDatumUitschrijven);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumUitschrijven->strftime('%F %T') == $newDatumUitschrijven->strftime('%F %T'))
			return $this;

		$this->datumUitschrijven = $newDatumUitschrijven;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumUitschrijven geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumUitschrijven geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDatumUitschrijven()
	{
		if (array_key_exists('DatumUitschrijven', $this->errors))
			return $this->errors['DatumUitschrijven'];
		$waarde = $this->getDatumUitschrijven();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld maxDeelnemers.
	 *
	 * @return int
	 * De waarde van het veld maxDeelnemers.
	 */
	public function getMaxDeelnemers()
	{
		return $this->maxDeelnemers;
	}
	/**
	 * @brief Stel de waarde van het veld maxDeelnemers in.
	 *
	 * @param mixed $newMaxDeelnemers De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setMaxDeelnemers($newMaxDeelnemers)
	{
		unset($this->errors['MaxDeelnemers']);
		if(!is_null($newMaxDeelnemers))
			$newMaxDeelnemers = (int)$newMaxDeelnemers;
		if($this->maxDeelnemers === $newMaxDeelnemers)
			return $this;

		$this->maxDeelnemers = $newMaxDeelnemers;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld maxDeelnemers geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld maxDeelnemers geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMaxDeelnemers()
	{
		if (array_key_exists('MaxDeelnemers', $this->errors))
			return $this->errors['MaxDeelnemers'];
		$waarde = $this->getMaxDeelnemers();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld actsoort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld actsoort.
	 */
	static public function enumsActsoort()
	{
		static $vals = array('AES2','UU','LAND','ZUS','.COM','EXT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld actsoort.
	 *
	 * @return string
	 * De waarde van het veld actsoort.
	 */
	public function getActsoort()
	{
		return $this->actsoort;
	}
	/**
	 * @brief Stel de waarde van het veld actsoort in.
	 *
	 * @param mixed $newActsoort De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setActsoort($newActsoort)
	{
		unset($this->errors['Actsoort']);
		if(!is_null($newActsoort))
			$newActsoort = strtoupper(trim($newActsoort));
		if($newActsoort === "")
			$newActsoort = NULL;
		if($this->actsoort === $newActsoort)
			return $this;

		$this->actsoort = $newActsoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld actsoort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld actsoort geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkActsoort()
	{
		if (array_key_exists('Actsoort', $this->errors))
			return $this->errors['Actsoort'];
		$waarde = $this->getActsoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsActsoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld onderwijs.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld onderwijs.
	 */
	static public function enumsOnderwijs()
	{
		static $vals = array('N','B','J');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld onderwijs.
	 *
	 * @return string
	 * De waarde van het veld onderwijs.
	 */
	public function getOnderwijs()
	{
		return $this->onderwijs;
	}
	/**
	 * @brief Stel de waarde van het veld onderwijs in.
	 *
	 * @param mixed $newOnderwijs De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setOnderwijs($newOnderwijs)
	{
		unset($this->errors['Onderwijs']);
		if(!is_null($newOnderwijs))
			$newOnderwijs = strtoupper(trim($newOnderwijs));
		if($newOnderwijs === "")
			$newOnderwijs = NULL;
		if($this->onderwijs === $newOnderwijs)
			return $this;

		$this->onderwijs = $newOnderwijs;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld onderwijs geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld onderwijs geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOnderwijs()
	{
		if (array_key_exists('Onderwijs', $this->errors))
			return $this->errors['Onderwijs'];
		$waarde = $this->getOnderwijs();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsOnderwijs()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld aantalComputers.
	 *
	 * @return int
	 * De waarde van het veld aantalComputers.
	 */
	public function getAantalComputers()
	{
		return $this->aantalComputers;
	}
	/**
	 * @brief Stel de waarde van het veld aantalComputers in.
	 *
	 * @param mixed $newAantalComputers De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setAantalComputers($newAantalComputers)
	{
		unset($this->errors['AantalComputers']);
		if(!is_null($newAantalComputers))
			$newAantalComputers = (int)$newAantalComputers;
		if($this->aantalComputers === $newAantalComputers)
			return $this;

		$this->aantalComputers = $newAantalComputers;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aantalComputers geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aantalComputers geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkAantalComputers()
	{
		if (array_key_exists('AantalComputers', $this->errors))
			return $this->errors['AantalComputers'];
		$waarde = $this->getAantalComputers();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @return ActiviteitCategorie
	 * De waarde van het veld categorie.
	 */
	public function getCategorie()
	{
		if(!isset($this->categorie)
		 && isset($this->categorie_actCategorieID)
		 ) {
			$this->categorie = ActiviteitCategorie::geef
					( $this->categorie_actCategorieID
					);
		}
		return $this->categorie;
	}
	/**
	 * @brief Stel de waarde van het veld categorie in.
	 *
	 * @param mixed $new_actCategorieID De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setCategorie($new_actCategorieID)
	{
		unset($this->errors['Categorie']);
		if($new_actCategorieID instanceof ActiviteitCategorie
		) {
			if($this->categorie == $new_actCategorieID
			&& $this->categorie_actCategorieID == $this->categorie->getActCategorieID())
				return $this;
			$this->categorie = $new_actCategorieID;
			$this->categorie_actCategorieID
					= $this->categorie->getActCategorieID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_actCategorieID)
		) {
			if($this->categorie == NULL 
				&& $this->categorie_actCategorieID == (int)$new_actCategorieID)
				return $this;
			$this->categorie = NULL;
			$this->categorie_actCategorieID
					= (int)$new_actCategorieID;
			$this->gewijzigd();
			return $this;
		}
		if (isset($this->categorie)
		 || isset($this->categorie_actCategorieID))
		{
			$this->errors['Categorie'] = _('dit is een verplicht veld');
			$this->categorie = NULL;
			$this->categorie_actCategorieID = NULL;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld categorie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld categorie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCategorie()
	{
		if (array_key_exists('Categorie', $this->errors))
			return $this->errors['Categorie'];
		$waarde1 = $this->getCategorie();
		$waarde2 = $this->getCategorieActCategorieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie_actCategorieID.
	 *
	 * @return int
	 * De waarde van het veld categorie_actCategorieID.
	 */
	public function getCategorieActCategorieID()
	{
		if (is_null($this->categorie_actCategorieID) && isset($this->categorie)) {
			$this->categorie_actCategorieID = $this->categorie->getActCategorieID();
		}
		return $this->categorie_actCategorieID;
	}
	/**
	 * @brief Stel de waarde van het veld categorie_actCategorieID in.
	 *
	 * @param mixed $newCategorie_actCategorieID De nieuwe waarde.
	 *
	 * @return ActiviteitInformatie
	 * Dit ActiviteitInformatie-object.
	 */
	public function setCategorie_actCategorieID($newCategorie_actCategorieID)
	{
		unset($this->errors['Categorie_actCategorieID']);
		if(!is_null($newCategorie_actCategorieID))
			$newCategorie_actCategorieID = (int)$newCategorie_actCategorieID;
		if($this->categorie_actCategorieID === $newCategorie_actCategorieID)
			return $this;

		$this->categorie_actCategorieID = $newCategorie_actCategorieID;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ActiviteitInformatie::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ActiviteitInformatie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ActiviteitInformatie::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getActiviteitID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return ActiviteitInformatie|false
	 * Een ActiviteitInformatie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$activiteitID = (int)$a;
		}

		if(is_null($activiteitID))
			throw new BadMethodCallException();

		static::cache(array( array($activiteitID) ));
		return Entiteit::geefCache(array($activiteitID), 'ActiviteitInformatie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Activiteit::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('ActiviteitInformatie::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `ActiviteitInformatie`.`titel_NL`'
			                 .     ', `ActiviteitInformatie`.`titel_EN`'
			                 .     ', `ActiviteitInformatie`.`werftekst_NL`'
			                 .     ', `ActiviteitInformatie`.`werftekst_EN`'
			                 .     ', `ActiviteitInformatie`.`homepage`'
			                 .     ', `ActiviteitInformatie`.`locatie_NL`'
			                 .     ', `ActiviteitInformatie`.`locatie_EN`'
			                 .     ', `ActiviteitInformatie`.`prijs_NL`'
			                 .     ', `ActiviteitInformatie`.`prijs_EN`'
			                 .     ', `ActiviteitInformatie`.`inschrijfbaar`'
			                 .     ', `ActiviteitInformatie`.`stuurMail`'
			                 .     ', `ActiviteitInformatie`.`datumInschrijvenMax`'
			                 .     ', `ActiviteitInformatie`.`datumUitschrijven`'
			                 .     ', `ActiviteitInformatie`.`maxDeelnemers`'
			                 .     ', `ActiviteitInformatie`.`actsoort`'
			                 .     ', `ActiviteitInformatie`.`onderwijs`'
			                 .     ', `ActiviteitInformatie`.`aantalComputers`'
			                 .     ', `ActiviteitInformatie`.`categorie_actCategorieID`'
			                 .     ', `Activiteit`.`activiteitID`'
			                 .     ', `Activiteit`.`momentBegin`'
			                 .     ', `Activiteit`.`momentEind`'
			                 .     ', `Activiteit`.`toegang`'
			                 .     ', `Activiteit`.`gewijzigdWanneer`'
			                 .     ', `Activiteit`.`gewijzigdWie`'
			                 .' FROM `ActiviteitInformatie`'
			                 .' LEFT JOIN `Activiteit` USING (`activiteitID`)'
			                 .' WHERE (`activiteitID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['activiteitID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['ActiviteitInformatie'][$id];
			} else {
				$obj = new ActiviteitInformatie();
			}

			$obj->inDB = True;

			$obj->titel['nl'] = $row['titel_NL'];
			$obj->titel['en'] = $row['titel_EN'];
			$obj->werftekst['nl'] = $row['werftekst_NL'];
			$obj->werftekst['en'] = $row['werftekst_EN'];
			$obj->homepage = $row['homepage'];
			$obj->locatie['nl'] = $row['locatie_NL'];
			$obj->locatie['en'] = $row['locatie_EN'];
			$obj->prijs['nl'] = $row['prijs_NL'];
			$obj->prijs['en'] = $row['prijs_EN'];
			$obj->inschrijfbaar  = (bool) $row['inschrijfbaar'];
			$obj->stuurMail  = (bool) $row['stuurMail'];
			$obj->datumInschrijvenMax  = new DateTimeLocale($row['datumInschrijvenMax']);
			$obj->datumUitschrijven  = new DateTimeLocale($row['datumUitschrijven']);
			$obj->maxDeelnemers  = (is_null($row['maxDeelnemers'])) ? null : (int) $row['maxDeelnemers'];
			$obj->actsoort  = strtoupper(trim($row['actsoort']));
			$obj->onderwijs  = strtoupper(trim($row['onderwijs']));
			$obj->aantalComputers  = (int) $row['aantalComputers'];
			$obj->categorie_actCategorieID  = (int) $row['categorie_actCategorieID'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Activiteit::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ActiviteitInformatie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getCategorieActCategorieID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `ActiviteitInformatie`'
			          . ' (`activiteitID`, `titel_NL`, `titel_EN`, `werftekst_NL`, `werftekst_EN`, `homepage`, `locatie_NL`, `locatie_EN`, `prijs_NL`, `prijs_EN`, `inschrijfbaar`, `stuurMail`, `datumInschrijvenMax`, `datumUitschrijven`, `maxDeelnemers`, `actsoort`, `onderwijs`, `aantalComputers`, `categorie_actCategorieID`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %s, %s, %s, %s, %i, %i, %s, %s, %i, %s, %s, %i, %i)'
			          , $this->activiteitID
			          , $this->titel['nl']
			          , $this->titel['en']
			          , $this->werftekst['nl']
			          , $this->werftekst['en']
			          , $this->homepage
			          , $this->locatie['nl']
			          , $this->locatie['en']
			          , $this->prijs['nl']
			          , $this->prijs['en']
			          , $this->inschrijfbaar
			          , $this->stuurMail
			          , (!is_null($this->datumInschrijvenMax))?$this->datumInschrijvenMax->strftime('%F %T'):null
			          , (!is_null($this->datumUitschrijven))?$this->datumUitschrijven->strftime('%F %T'):null
			          , $this->maxDeelnemers
			          , $this->actsoort
			          , $this->onderwijs
			          , $this->aantalComputers
			          , $this->categorie_actCategorieID
			          );

			if($classname == 'ActiviteitInformatie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ActiviteitInformatie`'
			          .' SET `titel_NL` = %s'
			          .   ', `titel_EN` = %s'
			          .   ', `werftekst_NL` = %s'
			          .   ', `werftekst_EN` = %s'
			          .   ', `homepage` = %s'
			          .   ', `locatie_NL` = %s'
			          .   ', `locatie_EN` = %s'
			          .   ', `prijs_NL` = %s'
			          .   ', `prijs_EN` = %s'
			          .   ', `inschrijfbaar` = %i'
			          .   ', `stuurMail` = %i'
			          .   ', `datumInschrijvenMax` = %s'
			          .   ', `datumUitschrijven` = %s'
			          .   ', `maxDeelnemers` = %i'
			          .   ', `actsoort` = %s'
			          .   ', `onderwijs` = %s'
			          .   ', `aantalComputers` = %i'
			          .   ', `categorie_actCategorieID` = %i'
			          .' WHERE `activiteitID` = %i'
			          , $this->titel['nl']
			          , $this->titel['en']
			          , $this->werftekst['nl']
			          , $this->werftekst['en']
			          , $this->homepage
			          , $this->locatie['nl']
			          , $this->locatie['en']
			          , $this->prijs['nl']
			          , $this->prijs['en']
			          , $this->inschrijfbaar
			          , $this->stuurMail
			          , (!is_null($this->datumInschrijvenMax))?$this->datumInschrijvenMax->strftime('%F %T'):null
			          , (!is_null($this->datumUitschrijven))?$this->datumUitschrijven->strftime('%F %T'):null
			          , $this->maxDeelnemers
			          , $this->actsoort
			          , $this->onderwijs
			          , $this->aantalComputers
			          , $this->categorie_actCategorieID
			          , $this->activiteitID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenActiviteitInformatie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenActiviteitInformatie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenActiviteitInformatie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Titel';
			$velden[] = 'Werftekst';
			$velden[] = 'Homepage';
			$velden[] = 'Locatie';
			$velden[] = 'Prijs';
			$velden[] = 'Inschrijfbaar';
			$velden[] = 'StuurMail';
			$velden[] = 'DatumInschrijvenMax';
			$velden[] = 'DatumUitschrijven';
			$velden[] = 'MaxDeelnemers';
			$velden[] = 'Actsoort';
			$velden[] = 'Onderwijs';
			$velden[] = 'AantalComputers';
			$velden[] = 'Categorie';
			break;
		case 'lang':
			$velden[] = 'Titel';
			$velden[] = 'Werftekst';
			$velden[] = 'Locatie';
			$velden[] = 'Prijs';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Titel';
			$velden[] = 'Werftekst';
			$velden[] = 'Categorie';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Titel';
			$velden[] = 'Werftekst';
			$velden[] = 'Homepage';
			$velden[] = 'Locatie';
			$velden[] = 'Prijs';
			$velden[] = 'Inschrijfbaar';
			$velden[] = 'StuurMail';
			$velden[] = 'DatumInschrijvenMax';
			$velden[] = 'DatumUitschrijven';
			$velden[] = 'MaxDeelnemers';
			$velden[] = 'Actsoort';
			$velden[] = 'Onderwijs';
			$velden[] = 'AantalComputers';
			$velden[] = 'Categorie';
			break;
		case 'get':
			$velden[] = 'Titel';
			$velden[] = 'Werftekst';
			$velden[] = 'Homepage';
			$velden[] = 'Locatie';
			$velden[] = 'Prijs';
			$velden[] = 'Inschrijfbaar';
			$velden[] = 'StuurMail';
			$velden[] = 'DatumInschrijvenMax';
			$velden[] = 'DatumUitschrijven';
			$velden[] = 'MaxDeelnemers';
			$velden[] = 'Actsoort';
			$velden[] = 'Onderwijs';
			$velden[] = 'AantalComputers';
			$velden[] = 'Categorie';
		case 'primary':
			$velden[] = 'categorie_actCategorieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Titel';
			$velden[] = 'Werftekst';
			$velden[] = 'Homepage';
			$velden[] = 'Locatie';
			$velden[] = 'Prijs';
			$velden[] = 'Inschrijfbaar';
			$velden[] = 'StuurMail';
			$velden[] = 'DatumInschrijvenMax';
			$velden[] = 'DatumUitschrijven';
			$velden[] = 'MaxDeelnemers';
			$velden[] = 'Actsoort';
			$velden[] = 'Onderwijs';
			$velden[] = 'AantalComputers';
			$velden[] = 'Categorie';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ActiviteitInformatie`'
		          .' WHERE `activiteitID` = %i'
		          .' LIMIT 1'
		          , $this->activiteitID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van ActiviteitInformatie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'titel':
			return 'string';
		case 'werftekst':
			return 'html';
		case 'homepage':
			return 'url';
		case 'locatie':
			return 'string';
		case 'prijs':
			return 'string';
		case 'inschrijfbaar':
			return 'bool';
		case 'stuurmail':
			return 'bool';
		case 'datuminschrijvenmax':
			return 'date';
		case 'datumuitschrijven':
			return 'date';
		case 'maxdeelnemers':
			return 'int';
		case 'actsoort':
			return 'enum';
		case 'onderwijs':
			return 'enum';
		case 'aantalcomputers':
			return 'int';
		case 'categorie':
			return 'foreign';
		case 'categorie_actcategorieid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'inschrijfbaar':
		case 'stuurMail':
		case 'maxDeelnemers':
		case 'aantalComputers':
		case 'categorie_actCategorieID':
			$type = '%i';
			break;
		case 'titel':
		case 'werftekst':
		case 'homepage':
		case 'locatie':
		case 'prijs':
		case 'datumInschrijvenMax':
		case 'datumUitschrijven':
		case 'actsoort':
		case 'onderwijs':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ActiviteitInformatie`'
		          ." SET `%l` = $type"
		          .' WHERE `activiteitID` = %i'
		          , $veld
		          , $waarde
		          , $this->activiteitID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'titel':
		case 'werftekst':
		case 'homepage':
		case 'locatie':
		case 'prijs':
		case 'inschrijfbaar':
		case 'stuurMail':
		case 'datumInschrijvenMax':
		case 'datumUitschrijven':
		case 'maxDeelnemers':
		case 'actsoort':
		case 'onderwijs':
		case 'aantalComputers':
		case 'categorie':
		case 'categorie_actCategorieID':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ActiviteitInformatie`'
		          .' WHERE `activiteitID` = %i'
		                 , $veld
		          , $this->activiteitID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
