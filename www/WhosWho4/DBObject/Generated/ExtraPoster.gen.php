<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class ExtraPoster_Generated
	extends Entiteit
{
	protected $extraPosterID;			/**< \brief PRIMARY */
	protected $naam;
	protected $beschrijving;
	protected $zichtbaar;
	/**
	 * @brief De constructor van de ExtraPoster_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->extraPosterID = NULL;
		$this->naam = '';
		$this->beschrijving = '';
		$this->zichtbaar = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld extraPosterID.
	 *
	 * @return int
	 * De waarde van het veld extraPosterID.
	 */
	public function getExtraPosterID()
	{
		return $this->extraPosterID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return ExtraPoster
	 * Dit ExtraPoster-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld beschrijving.
	 *
	 * @return string
	 * De waarde van het veld beschrijving.
	 */
	public function getBeschrijving()
	{
		return $this->beschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld beschrijving in.
	 *
	 * @param mixed $newBeschrijving De nieuwe waarde.
	 *
	 * @return ExtraPoster
	 * Dit ExtraPoster-object.
	 */
	public function setBeschrijving($newBeschrijving)
	{
		unset($this->errors['Beschrijving']);
		if(!is_null($newBeschrijving))
			$newBeschrijving = trim($newBeschrijving);
		if($newBeschrijving === "")
			$newBeschrijving = NULL;
		if($this->beschrijving === $newBeschrijving)
			return $this;

		$this->beschrijving = $newBeschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld beschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld beschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBeschrijving()
	{
		if (array_key_exists('Beschrijving', $this->errors))
			return $this->errors['Beschrijving'];
		$waarde = $this->getBeschrijving();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld zichtbaar.
	 *
	 * @return bool
	 * De waarde van het veld zichtbaar.
	 */
	public function getZichtbaar()
	{
		return $this->zichtbaar;
	}
	/**
	 * @brief Stel de waarde van het veld zichtbaar in.
	 *
	 * @param mixed $newZichtbaar De nieuwe waarde.
	 *
	 * @return ExtraPoster
	 * Dit ExtraPoster-object.
	 */
	public function setZichtbaar($newZichtbaar)
	{
		unset($this->errors['Zichtbaar']);
		if(!is_null($newZichtbaar))
			$newZichtbaar = (bool)$newZichtbaar;
		if($this->zichtbaar === $newZichtbaar)
			return $this;

		$this->zichtbaar = $newZichtbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld zichtbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld zichtbaar geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkZichtbaar()
	{
		if (array_key_exists('Zichtbaar', $this->errors))
			return $this->errors['Zichtbaar'];
		$waarde = $this->getZichtbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ExtraPoster::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ExtraPoster.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ExtraPoster::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getExtraPosterID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return ExtraPoster|false
	 * Een ExtraPoster-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$extraPosterID = (int)$a[0];
		}
		else if(isset($a))
		{
			$extraPosterID = (int)$a;
		}

		if(is_null($extraPosterID))
			throw new BadMethodCallException();

		static::cache(array( array($extraPosterID) ));
		return Entiteit::geefCache(array($extraPosterID), 'ExtraPoster');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'ExtraPoster');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('ExtraPoster::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `ExtraPoster`.`extraPosterID`'
		                 .     ', `ExtraPoster`.`naam`'
		                 .     ', `ExtraPoster`.`beschrijving`'
		                 .     ', `ExtraPoster`.`zichtbaar`'
		                 .     ', `ExtraPoster`.`gewijzigdWanneer`'
		                 .     ', `ExtraPoster`.`gewijzigdWie`'
		                 .' FROM `ExtraPoster`'
		                 .' WHERE (`extraPosterID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['extraPosterID']);

			$obj = new ExtraPoster();

			$obj->inDB = True;

			$obj->extraPosterID  = (int) $row['extraPosterID'];
			$obj->naam  = trim($row['naam']);
			$obj->beschrijving  = trim($row['beschrijving']);
			$obj->zichtbaar  = (bool) $row['zichtbaar'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ExtraPoster')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->extraPosterID =
			$WSW4DB->q('RETURNID INSERT INTO `ExtraPoster`'
			          . ' (`naam`, `beschrijving`, `zichtbaar`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %i, %s, %i)'
			          , $this->naam
			          , $this->beschrijving
			          , $this->zichtbaar
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'ExtraPoster')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ExtraPoster`'
			          .' SET `naam` = %s'
			          .   ', `beschrijving` = %s'
			          .   ', `zichtbaar` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `extraPosterID` = %i'
			          , $this->naam
			          , $this->beschrijving
			          , $this->zichtbaar
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->extraPosterID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenExtraPoster
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenExtraPoster($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenExtraPoster($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Beschrijving';
			$velden[] = 'Zichtbaar';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			$velden[] = 'Beschrijving';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Beschrijving';
			$velden[] = 'Zichtbaar';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Beschrijving';
			$velden[] = 'Zichtbaar';
		case 'primary':
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Beschrijving';
			$velden[] = 'Zichtbaar';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ExtraPoster`'
		          .' WHERE `extraPosterID` = %i'
		          .' LIMIT 1'
		          , $this->extraPosterID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->extraPosterID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `ExtraPoster`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `extraPosterID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->extraPosterID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van ExtraPoster terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'extraposterid':
			return 'int';
		case 'naam':
			return 'string';
		case 'beschrijving':
			return 'string';
		case 'zichtbaar':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'extraPosterID':
		case 'zichtbaar':
			$type = '%i';
			break;
		case 'naam':
		case 'beschrijving':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ExtraPoster`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `extraPosterID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->extraPosterID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ExtraPoster`'
		          .' WHERE `extraPosterID` = %i'
		                 , $veld
		          , $this->extraPosterID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'ExtraPoster');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
