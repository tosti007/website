<?
/**
 * @brief 'AT'AUTH_GET:gast
 */
abstract class Tentamen_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $vak;
	protected $vak_vakID;				/**< \brief PRIMARY */
	protected $datum;
	protected $naam;					/**< \brief NULL */
	protected $oud;
	protected $tentamen;
	protected $opmerking;				/**< \brief NULL */
	/** Verzamelingen **/
	protected $tentamenUitwerkingVerzameling;
	/**
	/**
	 * @brief De constructor van de Tentamen_Generated-klasse.
	 *
	 * @param mixed $a Vak (Vak OR Array(vak_vakID) OR vak_vakID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Vak)
		{
			$this->vak = $a;
			$this->vak_vakID = $a->getVakID();
		}
		else if(is_array($a))
		{
			$this->vak = NULL;
			$this->vak_vakID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->vak = NULL;
			$this->vak_vakID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->id = NULL;
		$this->datum = new DateTimeLocale();
		$this->naam = NULL;
		$this->oud = False;
		$this->tentamen = False;
		$this->opmerking = NULL;
		$this->tentamenUitwerkingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Vak';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld vak.
	 *
	 * @return Vak
	 * De waarde van het veld vak.
	 */
	public function getVak()
	{
		if(!isset($this->vak)
		 && isset($this->vak_vakID)
		 ) {
			$this->vak = Vak::geef
					( $this->vak_vakID
					);
		}
		return $this->vak;
	}
	/**
	 * @brief Geef de waarde van het veld vak_vakID.
	 *
	 * @return int
	 * De waarde van het veld vak_vakID.
	 */
	public function getVakVakID()
	{
		if (is_null($this->vak_vakID) && isset($this->vak)) {
			$this->vak_vakID = $this->vak->getVakID();
		}
		return $this->vak_vakID;
	}
	/**
	 * @brief Geef de waarde van het veld datum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datum.
	 */
	public function getDatum()
	{
		return $this->datum;
	}
	/**
	 * @brief Stel de waarde van het veld datum in.
	 *
	 * @param mixed $newDatum De nieuwe waarde.
	 *
	 * @return Tentamen
	 * Dit Tentamen-object.
	 */
	public function setDatum($newDatum)
	{
		unset($this->errors['Datum']);
		if(!$newDatum instanceof DateTimeLocale) {
			try {
				$newDatum = new DateTimeLocale($newDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datum->strftime('%F %T') == $newDatum->strftime('%F %T'))
			return $this;

		$this->datum = $newDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datum geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkDatum()
	{
		if (array_key_exists('Datum', $this->errors))
			return $this->errors['Datum'];
		$waarde = $this->getDatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Tentamen
	 * Dit Tentamen-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld oud.
	 *
	 * @return bool
	 * De waarde van het veld oud.
	 */
	public function getOud()
	{
		return $this->oud;
	}
	/**
	 * @brief Stel de waarde van het veld oud in.
	 *
	 * @param mixed $newOud De nieuwe waarde.
	 *
	 * @return Tentamen
	 * Dit Tentamen-object.
	 */
	public function setOud($newOud)
	{
		unset($this->errors['Oud']);
		if(!is_null($newOud))
			$newOud = (bool)$newOud;
		if($this->oud === $newOud)
			return $this;

		$this->oud = $newOud;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld oud geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld oud geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkOud()
	{
		if (array_key_exists('Oud', $this->errors))
			return $this->errors['Oud'];
		$waarde = $this->getOud();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tentamen.
	 *
	 * @return bool
	 * De waarde van het veld tentamen.
	 */
	public function getTentamen()
	{
		return $this->tentamen;
	}
	/**
	 * @brief Stel de waarde van het veld tentamen in.
	 *
	 * @param mixed $newTentamen De nieuwe waarde.
	 *
	 * @return Tentamen
	 * Dit Tentamen-object.
	 */
	public function setTentamen($newTentamen)
	{
		unset($this->errors['Tentamen']);
		if(!is_null($newTentamen))
			$newTentamen = (bool)$newTentamen;
		if($this->tentamen === $newTentamen)
			return $this;

		$this->tentamen = $newTentamen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tentamen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tentamen geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkTentamen()
	{
		if (array_key_exists('Tentamen', $this->errors))
			return $this->errors['Tentamen'];
		$waarde = $this->getTentamen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return Tentamen
	 * Dit Tentamen-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de TentamenUitwerkingVerzameling die hoort bij dit object.
	 */
	public function getTentamenUitwerkingVerzameling()
	{
		if(!$this->tentamenUitwerkingVerzameling instanceof TentamenUitwerkingVerzameling)
			$this->tentamenUitwerkingVerzameling = TentamenUitwerkingVerzameling::fromTentamen($this);
		return $this->tentamenUitwerkingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Tentamen::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Tentamen.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Tentamen::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Tentamen|false
	 * Een Tentamen-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'Tentamen');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Tentamen');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Tentamen::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Tentamen`.`id`'
		                 .     ', `Tentamen`.`vak_vakID`'
		                 .     ', `Tentamen`.`datum`'
		                 .     ', `Tentamen`.`naam`'
		                 .     ', `Tentamen`.`oud`'
		                 .     ', `Tentamen`.`tentamen`'
		                 .     ', `Tentamen`.`opmerking`'
		                 .     ', `Tentamen`.`gewijzigdWanneer`'
		                 .     ', `Tentamen`.`gewijzigdWie`'
		                 .' FROM `Tentamen`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new Tentamen(array($row['vak_vakID']));

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->datum  = new DateTimeLocale($row['datum']);
			$obj->naam  = (is_null($row['naam'])) ? null : trim($row['naam']);
			$obj->oud  = (bool) $row['oud'];
			$obj->tentamen  = (bool) $row['tentamen'];
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Tentamen')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getVakVakID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `Tentamen`'
			          . ' (`vak_vakID`, `datum`, `naam`, `oud`, `tentamen`, `opmerking`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i, %i, %s, %s, %i)'
			          , $this->vak_vakID
			          , $this->datum->strftime('%F %T')
			          , $this->naam
			          , $this->oud
			          , $this->tentamen
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Tentamen')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Tentamen`'
			          .' SET `vak_vakID` = %i'
			          .   ', `datum` = %s'
			          .   ', `naam` = %s'
			          .   ', `oud` = %i'
			          .   ', `tentamen` = %i'
			          .   ', `opmerking` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->vak_vakID
			          , $this->datum->strftime('%F %T')
			          , $this->naam
			          , $this->oud
			          , $this->tentamen
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTentamen
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTentamen($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTentamen($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Datum';
			$velden[] = 'Naam';
			$velden[] = 'Oud';
			$velden[] = 'Tentamen';
			$velden[] = 'Opmerking';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Datum';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Vak';
			$velden[] = 'Datum';
			$velden[] = 'Naam';
			$velden[] = 'Oud';
			$velden[] = 'Tentamen';
			$velden[] = 'Opmerking';
			break;
		case 'get':
			$velden[] = 'Vak';
			$velden[] = 'Datum';
			$velden[] = 'Naam';
			$velden[] = 'Oud';
			$velden[] = 'Tentamen';
			$velden[] = 'Opmerking';
		case 'primary':
			$velden[] = 'vak_vakID';
			break;
		case 'verzamelingen':
			$velden[] = 'TentamenUitwerkingVerzameling';
			break;
		default:
			$velden[] = 'Vak';
			$velden[] = 'Datum';
			$velden[] = 'Naam';
			$velden[] = 'Oud';
			$velden[] = 'Tentamen';
			$velden[] = 'Opmerking';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle TentamenUitwerking-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TentamenUitwerkingFromTentamenVerz = TentamenUitwerkingVerzameling::fromTentamen($this);
		$returnValue = $TentamenUitwerkingFromTentamenVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TentamenUitwerking met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $TentamenUitwerkingFromTentamenVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Tentamen`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Tentamen`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Tentamen terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'vak':
			return 'foreign';
		case 'vak_vakid':
			return 'int';
		case 'datum':
			return 'date';
		case 'naam':
			return 'string';
		case 'oud':
			return 'bool';
		case 'tentamen':
			return 'bool';
		case 'opmerking':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'vak_vakID':
		case 'oud':
		case 'tentamen':
			$type = '%i';
			break;
		case 'datum':
		case 'naam':
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Tentamen`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Tentamen`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Tentamen');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle TentamenUitwerking-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TentamenUitwerkingFromTentamenVerz = TentamenUitwerkingVerzameling::fromTentamen($this);
		$dependencies['TentamenUitwerking'] = $TentamenUitwerkingFromTentamenVerz;

		return $dependencies;
	}
}
