<?
/**
 * @brief 'AT'AUTH_GET:gast
 */
abstract class TentamenUitwerking_Generated
	extends Entiteit
{
	protected $uitwerkingID;			/**< \brief PRIMARY */
	protected $tentamen;
	protected $tentamen_id;				/**< \brief PRIMARY */
	protected $uploader;
	protected $uploader_contactID;		/**< \brief PRIMARY */
	protected $wanneer;
	protected $opmerking;				/**< \brief NULL */
	protected $gecontroleerd;			/**< \brief Geeft aan of deze of deze uitwerking door de tentamencomissie is gecontroleerd. */
	/**
	/**
	 * @brief De constructor van de TentamenUitwerking_Generated-klasse.
	 *
	 * @param mixed $a Tentamen (Tentamen OR Array(tentamen_id) OR tentamen_id)
	 * @param mixed $b Uploader (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 * @param mixed $c Wanneer (datetime)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Tentamen)
		{
			$this->tentamen = $a;
			$this->tentamen_id = $a->getId();
		}
		else if(is_array($a))
		{
			$this->tentamen = NULL;
			$this->tentamen_id = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->tentamen = NULL;
			$this->tentamen_id = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->uploader = $b;
			$this->uploader_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->uploader = NULL;
			$this->uploader_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->uploader = NULL;
			$this->uploader_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(!$c instanceof DateTimeLocale)
		{
			if(is_null($c))
				$c = new DateTimeLocale();
			else
			{
				try {
					$c = new DateTimeLocale($c);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->wanneer = $c;

		$this->uitwerkingID = NULL;
		$this->opmerking = NULL;
		$this->gecontroleerd = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Tentamen';
		$volgorde[] = 'Uploader';
		$volgorde[] = 'Wanneer';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld uitwerkingID.
	 *
	 * @return int
	 * De waarde van het veld uitwerkingID.
	 */
	public function getUitwerkingID()
	{
		return $this->uitwerkingID;
	}
	/**
	 * @brief Geef de waarde van het veld tentamen.
	 *
	 * @return Tentamen
	 * De waarde van het veld tentamen.
	 */
	public function getTentamen()
	{
		if(!isset($this->tentamen)
		 && isset($this->tentamen_id)
		 ) {
			$this->tentamen = Tentamen::geef
					( $this->tentamen_id
					);
		}
		return $this->tentamen;
	}
	/**
	 * @brief Geef de waarde van het veld tentamen_id.
	 *
	 * @return int
	 * De waarde van het veld tentamen_id.
	 */
	public function getTentamenId()
	{
		if (is_null($this->tentamen_id) && isset($this->tentamen)) {
			$this->tentamen_id = $this->tentamen->getId();
		}
		return $this->tentamen_id;
	}
	/**
	 * @brief Geef de waarde van het veld uploader.
	 *
	 * @return Persoon
	 * De waarde van het veld uploader.
	 */
	public function getUploader()
	{
		if(!isset($this->uploader)
		 && isset($this->uploader_contactID)
		 ) {
			$this->uploader = Persoon::geef
					( $this->uploader_contactID
					);
		}
		return $this->uploader;
	}
	/**
	 * @brief Geef de waarde van het veld uploader_contactID.
	 *
	 * @return int
	 * De waarde van het veld uploader_contactID.
	 */
	public function getUploaderContactID()
	{
		if (is_null($this->uploader_contactID) && isset($this->uploader)) {
			$this->uploader_contactID = $this->uploader->getContactID();
		}
		return $this->uploader_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld wanneer.
	 */
	public function getWanneer()
	{
		return $this->wanneer;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return TentamenUitwerking
	 * Dit TentamenUitwerking-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld gecontroleerd.
	 *
	 * @return bool
	 * De waarde van het veld gecontroleerd.
	 */
	public function getGecontroleerd()
	{
		return $this->gecontroleerd;
	}
	/**
	 * @brief Stel de waarde van het veld gecontroleerd in.
	 *
	 * @param mixed $newGecontroleerd De nieuwe waarde.
	 *
	 * @return TentamenUitwerking
	 * Dit TentamenUitwerking-object.
	 */
	public function setGecontroleerd($newGecontroleerd)
	{
		unset($this->errors['Gecontroleerd']);
		if(!is_null($newGecontroleerd))
			$newGecontroleerd = (bool)$newGecontroleerd;
		if($this->gecontroleerd === $newGecontroleerd)
			return $this;

		$this->gecontroleerd = $newGecontroleerd;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld gecontroleerd geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld gecontroleerd geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkGecontroleerd()
	{
		if (array_key_exists('Gecontroleerd', $this->errors))
			return $this->errors['Gecontroleerd'];
		$waarde = $this->getGecontroleerd();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return TentamenUitwerking::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van TentamenUitwerking.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return TentamenUitwerking::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getUitwerkingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return TentamenUitwerking|false
	 * Een TentamenUitwerking-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$uitwerkingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$uitwerkingID = (int)$a;
		}

		if(is_null($uitwerkingID))
			throw new BadMethodCallException();

		static::cache(array( array($uitwerkingID) ));
		return Entiteit::geefCache(array($uitwerkingID), 'TentamenUitwerking');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'TentamenUitwerking');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('TentamenUitwerking::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `TentamenUitwerking`.`uitwerkingID`'
		                 .     ', `TentamenUitwerking`.`tentamen_id`'
		                 .     ', `TentamenUitwerking`.`uploader_contactID`'
		                 .     ', `TentamenUitwerking`.`wanneer`'
		                 .     ', `TentamenUitwerking`.`opmerking`'
		                 .     ', `TentamenUitwerking`.`gecontroleerd`'
		                 .     ', `TentamenUitwerking`.`gewijzigdWanneer`'
		                 .     ', `TentamenUitwerking`.`gewijzigdWie`'
		                 .' FROM `TentamenUitwerking`'
		                 .' WHERE (`uitwerkingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['uitwerkingID']);

			$obj = new TentamenUitwerking(array($row['tentamen_id']), array($row['uploader_contactID']), $row['wanneer']);

			$obj->inDB = True;

			$obj->uitwerkingID  = (int) $row['uitwerkingID'];
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->gecontroleerd  = (bool) $row['gecontroleerd'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'TentamenUitwerking')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getTentamenId();
		$this->getUploaderContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->uitwerkingID =
			$WSW4DB->q('RETURNID INSERT INTO `TentamenUitwerking`'
			          . ' (`tentamen_id`, `uploader_contactID`, `wanneer`, `opmerking`, `gecontroleerd`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i, %s, %i)'
			          , $this->tentamen_id
			          , $this->uploader_contactID
			          , $this->wanneer->strftime('%F %T')
			          , $this->opmerking
			          , $this->gecontroleerd
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'TentamenUitwerking')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `TentamenUitwerking`'
			          .' SET `tentamen_id` = %i'
			          .   ', `uploader_contactID` = %i'
			          .   ', `wanneer` = %s'
			          .   ', `opmerking` = %s'
			          .   ', `gecontroleerd` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `uitwerkingID` = %i'
			          , $this->tentamen_id
			          , $this->uploader_contactID
			          , $this->wanneer->strftime('%F %T')
			          , $this->opmerking
			          , $this->gecontroleerd
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->uitwerkingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTentamenUitwerking
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTentamenUitwerking($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTentamenUitwerking($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Opmerking';
			$velden[] = 'Gecontroleerd';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Tentamen';
			$velden[] = 'Uploader';
			$velden[] = 'Wanneer';
			$velden[] = 'Opmerking';
			$velden[] = 'Gecontroleerd';
			break;
		case 'get':
			$velden[] = 'Tentamen';
			$velden[] = 'Uploader';
			$velden[] = 'Wanneer';
			$velden[] = 'Opmerking';
			$velden[] = 'Gecontroleerd';
		case 'primary':
			$velden[] = 'tentamen_id';
			$velden[] = 'uploader_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Tentamen';
			$velden[] = 'Uploader';
			$velden[] = 'Wanneer';
			$velden[] = 'Opmerking';
			$velden[] = 'Gecontroleerd';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `TentamenUitwerking`'
		          .' WHERE `uitwerkingID` = %i'
		          .' LIMIT 1'
		          , $this->uitwerkingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->uitwerkingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `TentamenUitwerking`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `uitwerkingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->uitwerkingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van TentamenUitwerking terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'uitwerkingid':
			return 'int';
		case 'tentamen':
			return 'foreign';
		case 'tentamen_id':
			return 'int';
		case 'uploader':
			return 'foreign';
		case 'uploader_contactid':
			return 'int';
		case 'wanneer':
			return 'datetime';
		case 'opmerking':
			return 'text';
		case 'gecontroleerd':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'uitwerkingID':
		case 'tentamen_id':
		case 'uploader_contactID':
		case 'gecontroleerd':
			$type = '%i';
			break;
		case 'wanneer':
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `TentamenUitwerking`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `uitwerkingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->uitwerkingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `TentamenUitwerking`'
		          .' WHERE `uitwerkingID` = %i'
		                 , $veld
		          , $this->uitwerkingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'TentamenUitwerking');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
