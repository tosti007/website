<?
/**
 * @brief 'AT'AUTH_GET:promocie,AUTH_SET:bestuur
 */
abstract class Activiteit_Generated
	extends Entiteit
{
	protected $activiteitID;			/**< \brief PRIMARY */
	protected $momentBegin;
	protected $momentEind;
	protected $toegang;					/**< \brief ENUM:PUBLIEK/PRIVE */
	/** Verzamelingen **/
	protected $deelnemerVerzameling;
	protected $activiteitVraagVerzameling;
	protected $commissieActiviteitVerzameling;
	protected $iDealKaartjeVerzameling;
	protected $mediaVerzameling;
	protected $activiteitHerhalingVerzameling;
	/**
	 * @brief De constructor van de Activiteit_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->activiteitID = NULL;
		$this->momentBegin = new DateTimeLocale();
		$this->momentEind = new DateTimeLocale();
		$this->toegang = 'PUBLIEK';
		$this->deelnemerVerzameling = NULL;
		$this->activiteitVraagVerzameling = NULL;
		$this->commissieActiviteitVerzameling = NULL;
		$this->iDealKaartjeVerzameling = NULL;
		$this->mediaVerzameling = NULL;
		$this->activiteitHerhalingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld activiteitID.
	 *
	 * @return int
	 * De waarde van het veld activiteitID.
	 */
	public function getActiviteitID()
	{
		return $this->activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld momentBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld momentBegin.
	 */
	public function getMomentBegin()
	{
		return $this->momentBegin;
	}
	/**
	 * @brief Stel de waarde van het veld momentBegin in.
	 *
	 * @param mixed $newMomentBegin De nieuwe waarde.
	 *
	 * @return Activiteit
	 * Dit Activiteit-object.
	 */
	public function setMomentBegin($newMomentBegin)
	{
		unset($this->errors['MomentBegin']);
		if(!$newMomentBegin instanceof DateTimeLocale) {
			try {
				$newMomentBegin = new DateTimeLocale($newMomentBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->momentBegin->strftime('%F %T') == $newMomentBegin->strftime('%F %T'))
			return $this;

		$this->momentBegin = $newMomentBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld momentBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld momentBegin geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMomentBegin()
	{
		if (array_key_exists('MomentBegin', $this->errors))
			return $this->errors['MomentBegin'];
		$waarde = $this->getMomentBegin();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld momentEind.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld momentEind.
	 */
	public function getMomentEind()
	{
		return $this->momentEind;
	}
	/**
	 * @brief Stel de waarde van het veld momentEind in.
	 *
	 * @param mixed $newMomentEind De nieuwe waarde.
	 *
	 * @return Activiteit
	 * Dit Activiteit-object.
	 */
	public function setMomentEind($newMomentEind)
	{
		unset($this->errors['MomentEind']);
		if(!$newMomentEind instanceof DateTimeLocale) {
			try {
				$newMomentEind = new DateTimeLocale($newMomentEind);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->momentEind->strftime('%F %T') == $newMomentEind->strftime('%F %T'))
			return $this;

		$this->momentEind = $newMomentEind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld momentEind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld momentEind geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkMomentEind()
	{
		if (array_key_exists('MomentEind', $this->errors))
			return $this->errors['MomentEind'];
		$waarde = $this->getMomentEind();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld toegang.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld toegang.
	 */
	static public function enumsToegang()
	{
		static $vals = array('PUBLIEK','PRIVE');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld toegang.
	 *
	 * @return string
	 * De waarde van het veld toegang.
	 */
	public function getToegang()
	{
		return $this->toegang;
	}
	/**
	 * @brief Stel de waarde van het veld toegang in.
	 *
	 * @param mixed $newToegang De nieuwe waarde.
	 *
	 * @return Activiteit
	 * Dit Activiteit-object.
	 */
	public function setToegang($newToegang)
	{
		unset($this->errors['Toegang']);
		if(!is_null($newToegang))
			$newToegang = strtoupper(trim($newToegang));
		if($newToegang === "")
			$newToegang = NULL;
		if($this->toegang === $newToegang)
			return $this;

		$this->toegang = $newToegang;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld toegang geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld toegang geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkToegang()
	{
		if (array_key_exists('Toegang', $this->errors))
			return $this->errors['Toegang'];
		$waarde = $this->getToegang();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsToegang()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Returneert de DeelnemerVerzameling die hoort bij dit object.
	 */
	public function getDeelnemerVerzameling()
	{
		if(!$this->deelnemerVerzameling instanceof DeelnemerVerzameling)
			$this->deelnemerVerzameling = DeelnemerVerzameling::fromActiviteit($this);
		return $this->deelnemerVerzameling;
	}
	/**
	 * @brief Returneert de ActiviteitVraagVerzameling die hoort bij dit object.
	 */
	public function getActiviteitVraagVerzameling()
	{
		if(!$this->activiteitVraagVerzameling instanceof ActiviteitVraagVerzameling)
			$this->activiteitVraagVerzameling = ActiviteitVraagVerzameling::fromActiviteit($this);
		return $this->activiteitVraagVerzameling;
	}
	/**
	 * @brief Returneert de CommissieActiviteitVerzameling die hoort bij dit object.
	 */
	public function getCommissieActiviteitVerzameling()
	{
		if(!$this->commissieActiviteitVerzameling instanceof CommissieActiviteitVerzameling)
			$this->commissieActiviteitVerzameling = CommissieActiviteitVerzameling::fromActiviteit($this);
		return $this->commissieActiviteitVerzameling;
	}
	/**
	 * @brief Returneert de IDealKaartjeVerzameling die hoort bij dit object.
	 */
	public function getIDealKaartjeVerzameling()
	{
		if(!$this->iDealKaartjeVerzameling instanceof IDealKaartjeVerzameling)
			$this->iDealKaartjeVerzameling = IDealKaartjeVerzameling::fromActiviteit($this);
		return $this->iDealKaartjeVerzameling;
	}
	/**
	 * @brief Returneert de MediaVerzameling die hoort bij dit object.
	 */
	public function getMediaVerzameling()
	{
		if(!$this->mediaVerzameling instanceof MediaVerzameling)
			$this->mediaVerzameling = MediaVerzameling::fromActiviteit($this);
		return $this->mediaVerzameling;
	}
	/**
	 * @brief Returneert de ActiviteitHerhalingVerzameling die hoort bij dit object.
	 */
	public function getActiviteitHerhalingVerzameling()
	{
		if(!$this->activiteitHerhalingVerzameling instanceof ActiviteitHerhalingVerzameling)
			$this->activiteitHerhalingVerzameling = ActiviteitHerhalingVerzameling::fromParent($this);
		return $this->activiteitHerhalingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('promocie');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Activiteit::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Activiteit.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array('ActiviteitInformatie',
			'ActiviteitHerhaling');
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Activiteit::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getActiviteitID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Activiteit|false
	 * Een Activiteit-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$activiteitID = (int)$a;
		}

		if(is_null($activiteitID))
			throw new BadMethodCallException();

		static::cache(array( array($activiteitID) ));
		return Entiteit::geefCache(array($activiteitID), 'Activiteit');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			$ids = Entiteit::nietInCache($ids, 'Activiteit');

			// is er nog iets te doen?
			if(empty($ids)) return;

			// zoek uit wat de klasse van een id is
			$res = $WSW4DB->q('TABLE SELECT `activiteitID`'
			                 .     ', `overerving`'
			                 .' FROM `Activiteit`'
			                 .' WHERE (`activiteitID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );

			$recur = array();
			foreach($res as $row)
			{
				$recur[$row['overerving']][]
				    = array($row['activiteitID']);
			}
			foreach($recur as $class => $obj)
			{
				$class::cache($obj, True);
			}

			return;
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Activiteit::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Activiteit`.`activiteitID`'
			                 .     ', `Activiteit`.`momentBegin`'
			                 .     ', `Activiteit`.`momentEind`'
			                 .     ', `Activiteit`.`toegang`'
			                 .     ', `Activiteit`.`gewijzigdWanneer`'
			                 .     ', `Activiteit`.`gewijzigdWie`'
			                 .' FROM `Activiteit`'
			                 .' WHERE (`activiteitID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['activiteitID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Activiteit'][$id];
			} else {
				$obj = new Activiteit();
			}

			$obj->inDB = True;

			$obj->activiteitID  = (int) $row['activiteitID'];
			$obj->momentBegin  = new DateTimeLocale($row['momentBegin']);
			$obj->momentEind  = new DateTimeLocale($row['momentEind']);
			$obj->toegang  = strtoupper(trim($row['toegang']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			if($recur === True)
				self::stopInCache($id, $obj);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Activiteit')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->activiteitID =
			$WSW4DB->q('RETURNID INSERT INTO `Activiteit`'
			          . ' (`overerving`, `momentBegin`, `momentEind`, `toegang`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %s, %s, %i)'
			          , $classname
			          , $this->momentBegin->strftime('%F %T')
			          , $this->momentEind->strftime('%F %T')
			          , $this->toegang
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Activiteit')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Activiteit`'
			          .' SET `momentBegin` = %s'
			          .   ', `momentEind` = %s'
			          .   ', `toegang` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `activiteitID` = %i'
			          , $this->momentBegin->strftime('%F %T')
			          , $this->momentEind->strftime('%F %T')
			          , $this->toegang
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->activiteitID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenActiviteit
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenActiviteit($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenActiviteit($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'MomentBegin';
			$velden[] = 'MomentEind';
			$velden[] = 'Toegang';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'MomentBegin';
			$velden[] = 'MomentEind';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'MomentBegin';
			$velden[] = 'MomentEind';
			$velden[] = 'Toegang';
			break;
		case 'get':
			$velden[] = 'MomentBegin';
			$velden[] = 'MomentEind';
			$velden[] = 'Toegang';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'DeelnemerVerzameling';
			$velden[] = 'ActiviteitVraagVerzameling';
			$velden[] = 'CommissieActiviteitVerzameling';
			$velden[] = 'IDealKaartjeVerzameling';
			$velden[] = 'MediaVerzameling';
			$velden[] = 'ActiviteitHerhalingVerzameling';
			break;
		default:
			$velden[] = 'MomentBegin';
			$velden[] = 'MomentEind';
			$velden[] = 'Toegang';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Deelnemer-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DeelnemerFromActiviteitVerz = DeelnemerVerzameling::fromActiviteit($this);
		$returnValue = $DeelnemerFromActiviteitVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Deelnemer met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ActiviteitVraag-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ActiviteitVraagFromActiviteitVerz = ActiviteitVraagVerzameling::fromActiviteit($this);
		$returnValue = $ActiviteitVraagFromActiviteitVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ActiviteitVraag met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle CommissieActiviteit-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$CommissieActiviteitFromActiviteitVerz = CommissieActiviteitVerzameling::fromActiviteit($this);
		$returnValue = $CommissieActiviteitFromActiviteitVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CommissieActiviteit met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle iDealKaartje-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$iDealKaartjeFromActiviteitVerz = iDealKaartjeVerzameling::fromActiviteit($this);
		$returnValue = $iDealKaartjeFromActiviteitVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object iDealKaartje met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Media-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MediaFromActiviteitVerz = MediaVerzameling::fromActiviteit($this);
		$returnValue = $MediaFromActiviteitVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Media met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ActiviteitHerhaling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ActiviteitHerhalingFromParentVerz = ActiviteitHerhalingVerzameling::fromParent($this);
		$returnValue = $ActiviteitHerhalingFromParentVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ActiviteitHerhaling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $DeelnemerFromActiviteitVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ActiviteitVraagFromActiviteitVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $CommissieActiviteitFromActiviteitVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $iDealKaartjeFromActiviteitVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $MediaFromActiviteitVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ActiviteitHerhalingFromParentVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Activiteit`'
		          .' WHERE `activiteitID` = %i'
		          .' LIMIT 1'
		          , $this->activiteitID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->activiteitID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Activiteit`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `activiteitID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->activiteitID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Activiteit terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'activiteitid':
			return 'int';
		case 'momentbegin':
			return 'datetime';
		case 'momenteind':
			return 'datetime';
		case 'toegang':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'activiteitID':
			$type = '%i';
			break;
		case 'momentBegin':
		case 'momentEind':
		case 'toegang':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Activiteit`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `activiteitID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->activiteitID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Activiteit`'
		          .' WHERE `activiteitID` = %i'
		                 , $veld
		          , $this->activiteitID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Activiteit');
		parent::stopInCache($id, $obj, 'ActiviteitInformatie');
		parent::stopInCache($id, $obj, 'ActiviteitHerhaling');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Deelnemer-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DeelnemerFromActiviteitVerz = DeelnemerVerzameling::fromActiviteit($this);
		$dependencies['Deelnemer'] = $DeelnemerFromActiviteitVerz;

		// Verzamel alle ActiviteitVraag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ActiviteitVraagFromActiviteitVerz = ActiviteitVraagVerzameling::fromActiviteit($this);
		$dependencies['ActiviteitVraag'] = $ActiviteitVraagFromActiviteitVerz;

		// Verzamel alle CommissieActiviteit-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CommissieActiviteitFromActiviteitVerz = CommissieActiviteitVerzameling::fromActiviteit($this);
		$dependencies['CommissieActiviteit'] = $CommissieActiviteitFromActiviteitVerz;

		// Verzamel alle iDealKaartje-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$iDealKaartjeFromActiviteitVerz = iDealKaartjeVerzameling::fromActiviteit($this);
		$dependencies['iDealKaartje'] = $iDealKaartjeFromActiviteitVerz;

		// Verzamel alle Media-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MediaFromActiviteitVerz = MediaVerzameling::fromActiviteit($this);
		$dependencies['Media'] = $MediaFromActiviteitVerz;

		// Verzamel alle ActiviteitHerhaling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ActiviteitHerhalingFromParentVerz = ActiviteitHerhalingVerzameling::fromParent($this);
		$dependencies['ActiviteitHerhaling'] = $ActiviteitHerhalingFromParentVerz;

		return $dependencies;
	}
}
