<?
/**
 * @brief 'AT'AUTH_GET:bestuur,AUTH_SET:bestuur
 */
abstract class Organisatie_Generated
	extends Contact
{
	protected $naam;
	protected $soort;					/**< \brief ENUM:ZUS_UU/ZUS_NL/OG_MED/UU/VAKGROEP/BEDRIJF/OVERIG */
	protected $omschrijving;			/**< \brief NULL */
	protected $logo;					/**< \brief NULL */
	/** Verzamelingen **/
	protected $contactPersoonVerzameling;
	/**
	 * @brief De constructor van de Organisatie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Contact

		$this->contactID = NULL;
		$this->naam = '';
		$this->soort = 'ZUS_UU';
		$this->omschrijving = NULL;
		$this->logo = NULL;
		$this->contactPersoonVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Organisatie
	 * Dit Organisatie-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('ZUS_UU','ZUS_NL','OG_MED','UU','VAKGROEP','BEDRIJF','OVERIG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return Organisatie
	 * Dit Organisatie-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Organisatie
	 * Dit Organisatie-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld logo.
	 *
	 * @return string
	 * De waarde van het veld logo.
	 */
	public function getLogo()
	{
		return $this->logo;
	}
	/**
	 * @brief Stel de waarde van het veld logo in.
	 *
	 * @param mixed $newLogo De nieuwe waarde.
	 *
	 * @return Organisatie
	 * Dit Organisatie-object.
	 */
	public function setLogo($newLogo)
	{
		unset($this->errors['Logo']);
		if(!is_null($newLogo))
			$newLogo = trim($newLogo);
		if($newLogo === "")
			$newLogo = NULL;
		if($this->logo === $newLogo)
			return $this;

		$this->logo = $newLogo;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld logo geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld logo geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLogo()
	{
		if (array_key_exists('Logo', $this->errors))
			return $this->errors['Logo'];
		$waarde = $this->getLogo();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de ContactPersoonVerzameling die hoort bij dit object.
	 */
	public function getContactPersoonVerzameling()
	{
		if(!$this->contactPersoonVerzameling instanceof ContactPersoonVerzameling)
			$this->contactPersoonVerzameling = ContactPersoonVerzameling::fromOrganisatie($this);
		return $this->contactPersoonVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Organisatie::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Organisatie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Organisatie::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContactID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Organisatie|false
	 * Een Organisatie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contactID = (int)$a;
		}

		if(is_null($contactID))
			throw new BadMethodCallException();

		static::cache(array( array($contactID) ));
		return Entiteit::geefCache(array($contactID), 'Organisatie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Contact::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Organisatie::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Organisatie`.`naam`'
			                 .     ', `Organisatie`.`soort`'
			                 .     ', `Organisatie`.`omschrijving`'
			                 .     ', `Organisatie`.`logo`'
			                 .     ', `Contact`.`contactID`'
			                 .     ', `Contact`.`email`'
			                 .     ', `Contact`.`homepage`'
			                 .     ', `Contact`.`vakidOpsturen`'
			                 .     ', `Contact`.`aes2rootsOpsturen`'
			                 .     ', `Contact`.`gewijzigdWanneer`'
			                 .     ', `Contact`.`gewijzigdWie`'
			                 .' FROM `Organisatie`'
			                 .' LEFT JOIN `Contact` USING (`contactID`)'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contactID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Organisatie'][$id];
			} else {
				$obj = new Organisatie();
			}

			$obj->inDB = True;

			$obj->naam  = trim($row['naam']);
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->logo  = (is_null($row['logo'])) ? null : trim($row['logo']);
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Contact::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Organisatie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Organisatie`'
			          . ' (`contactID`, `naam`, `soort`, `omschrijving`, `logo`)'
			          . ' VALUES (%i, %s, %s, %s, %s)'
			          , $this->contactID
			          , $this->naam
			          , $this->soort
			          , $this->omschrijving
			          , $this->logo
			          );

			if($classname == 'Organisatie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Organisatie`'
			          .' SET `naam` = %s'
			          .   ', `soort` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `logo` = %s'
			          .' WHERE `contactID` = %i'
			          , $this->naam
			          , $this->soort
			          , $this->omschrijving
			          , $this->logo
			          , $this->contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenOrganisatie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenOrganisatie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenOrganisatie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Omschrijving';
			$velden[] = 'Logo';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Omschrijving';
			$velden[] = 'Logo';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Omschrijving';
			$velden[] = 'Logo';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'ContactPersoonVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Omschrijving';
			$velden[] = 'Logo';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle ContactPersoon-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContactPersoonFromOrganisatieVerz = ContactPersoonVerzameling::fromOrganisatie($this);
		$returnValue = $ContactPersoonFromOrganisatieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ContactPersoon met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $ContactPersoonFromOrganisatieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Organisatie`'
		          .' WHERE `contactID` = %i'
		          .' LIMIT 1'
		          , $this->contactID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Organisatie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'naam':
			return 'string';
		case 'soort':
			return 'enum';
		case 'omschrijving':
			return 'string';
		case 'logo':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'naam':
		case 'soort':
		case 'omschrijving':
		case 'logo':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Organisatie`'
		          ." SET `%l` = %s"
		          .' WHERE `contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'naam':
		case 'soort':
		case 'omschrijving':
		case 'logo':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Organisatie`'
		          .' WHERE `contactID` = %i'
		                 , $veld
		          , $this->contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle ContactPersoon-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContactPersoonFromOrganisatieVerz = ContactPersoonVerzameling::fromOrganisatie($this);
		$dependencies['ContactPersoon'] = $ContactPersoonFromOrganisatieVerz;

		return $dependencies;
	}
}
