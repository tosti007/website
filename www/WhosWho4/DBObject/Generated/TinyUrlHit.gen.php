<?
/**
 * @brief Een hit op een TinyUrl
 */
abstract class TinyUrlHit_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $tinyUrl;
	protected $tinyUrl_id;				/**< \brief PRIMARY */
	protected $host;					/**< \brief NULL */
	protected $hitTime;
	/**
	 * @brief De constructor van de TinyUrlHit_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->tinyUrl = NULL;
		$this->tinyUrl_id = 0;
		$this->host = NULL;
		$this->hitTime = new DateTimeLocale();
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl.
	 *
	 * @return TinyUrl
	 * De waarde van het veld tinyUrl.
	 */
	public function getTinyUrl()
	{
		if(!isset($this->tinyUrl)
		 && isset($this->tinyUrl_id)
		 ) {
			$this->tinyUrl = TinyUrl::geef
					( $this->tinyUrl_id
					);
		}
		return $this->tinyUrl;
	}
	/**
	 * @brief Stel de waarde van het veld tinyUrl in.
	 *
	 * @param mixed $new_id De nieuwe waarde.
	 *
	 * @return TinyUrlHit
	 * Dit TinyUrlHit-object.
	 */
	public function setTinyUrl($new_id)
	{
		unset($this->errors['TinyUrl']);
		if($new_id instanceof TinyUrl
		) {
			if($this->tinyUrl == $new_id
			&& $this->tinyUrl_id == $this->tinyUrl->getId())
				return $this;
			$this->tinyUrl = $new_id;
			$this->tinyUrl_id
					= $this->tinyUrl->getId();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_id)
		) {
			if($this->tinyUrl == NULL 
				&& $this->tinyUrl_id == (int)$new_id)
				return $this;
			$this->tinyUrl = NULL;
			$this->tinyUrl_id
					= (int)$new_id;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tinyUrl geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tinyUrl geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkTinyUrl()
	{
		if (array_key_exists('TinyUrl', $this->errors))
			return $this->errors['TinyUrl'];
		$waarde1 = $this->getTinyUrl();
		$waarde2 = $this->getTinyUrlId();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl_id.
	 *
	 * @return int
	 * De waarde van het veld tinyUrl_id.
	 */
	public function getTinyUrlId()
	{
		if (is_null($this->tinyUrl_id) && isset($this->tinyUrl)) {
			$this->tinyUrl_id = $this->tinyUrl->getId();
		}
		return $this->tinyUrl_id;
	}
	/**
	 * @brief Stel de waarde van het veld tinyUrl_id in.
	 *
	 * @param mixed $newTinyUrl_id De nieuwe waarde.
	 *
	 * @return TinyUrlHit
	 * Dit TinyUrlHit-object.
	 */
	public function setTinyUrl_id($newTinyUrl_id)
	{
		unset($this->errors['TinyUrl_id']);
		if(!is_null($newTinyUrl_id))
			$newTinyUrl_id = (int)$newTinyUrl_id;
		if($this->tinyUrl_id === $newTinyUrl_id)
			return $this;

		$this->tinyUrl_id = $newTinyUrl_id;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Geef de waarde van het veld host.
	 *
	 * @return string
	 * De waarde van het veld host.
	 */
	public function getHost()
	{
		return $this->host;
	}
	/**
	 * @brief Stel de waarde van het veld host in.
	 *
	 * @param mixed $newHost De nieuwe waarde.
	 *
	 * @return TinyUrlHit
	 * Dit TinyUrlHit-object.
	 */
	public function setHost($newHost)
	{
		unset($this->errors['Host']);
		if(!is_null($newHost))
			$newHost = trim($newHost);
		if($newHost === "")
			$newHost = NULL;
		if($this->host === $newHost)
			return $this;

		$this->host = $newHost;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld host geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld host geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkHost()
	{
		if (array_key_exists('Host', $this->errors))
			return $this->errors['Host'];
		$waarde = $this->getHost();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld hitTime.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld hitTime.
	 */
	public function getHitTime()
	{
		return $this->hitTime;
	}
	/**
	 * @brief Stel de waarde van het veld hitTime in.
	 *
	 * @param mixed $newHitTime De nieuwe waarde.
	 *
	 * @return TinyUrlHit
	 * Dit TinyUrlHit-object.
	 */
	public function setHitTime($newHitTime)
	{
		unset($this->errors['HitTime']);
		if(!$newHitTime instanceof DateTimeLocale) {
			try {
				$newHitTime = new DateTimeLocale($newHitTime);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->hitTime->strftime('%F %T') == $newHitTime->strftime('%F %T'))
			return $this;

		$this->hitTime = $newHitTime;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld hitTime geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld hitTime geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkHitTime()
	{
		if (array_key_exists('HitTime', $this->errors))
			return $this->errors['HitTime'];
		$waarde = $this->getHitTime();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return TinyUrlHit::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van TinyUrlHit.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return TinyUrlHit::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return TinyUrlHit|false
	 * Een TinyUrlHit-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'TinyUrlHit');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'TinyUrlHit');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('TinyUrlHit::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `TinyUrlHit`.`id`'
		                 .     ', `TinyUrlHit`.`tinyUrl_id`'
		                 .     ', `TinyUrlHit`.`host`'
		                 .     ', `TinyUrlHit`.`hitTime`'
		                 .     ', `TinyUrlHit`.`gewijzigdWanneer`'
		                 .     ', `TinyUrlHit`.`gewijzigdWie`'
		                 .' FROM `TinyUrlHit`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new TinyUrlHit();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->tinyUrl_id  = (int) $row['tinyUrl_id'];
			$obj->host  = (is_null($row['host'])) ? null : trim($row['host']);
			$obj->hitTime  = new DateTimeLocale($row['hitTime']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'TinyUrlHit')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getTinyUrlId();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `TinyUrlHit`'
			          . ' (`tinyUrl_id`, `host`, `hitTime`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %i)'
			          , $this->tinyUrl_id
			          , $this->host
			          , $this->hitTime->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'TinyUrlHit')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `TinyUrlHit`'
			          .' SET `tinyUrl_id` = %i'
			          .   ', `host` = %s'
			          .   ', `hitTime` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->tinyUrl_id
			          , $this->host
			          , $this->hitTime->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTinyUrlHit
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTinyUrlHit($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTinyUrlHit($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'TinyUrl';
			$velden[] = 'Host';
			$velden[] = 'HitTime';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'TinyUrl';
			$velden[] = 'HitTime';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'TinyUrl';
			$velden[] = 'Host';
			$velden[] = 'HitTime';
			break;
		case 'get':
			$velden[] = 'TinyUrl';
			$velden[] = 'Host';
			$velden[] = 'HitTime';
		case 'primary':
			$velden[] = 'tinyUrl_id';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'TinyUrl';
			$velden[] = 'Host';
			$velden[] = 'HitTime';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `TinyUrlHit`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `TinyUrlHit`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van TinyUrlHit terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'tinyurl':
			return 'foreign';
		case 'tinyurl_id':
			return 'int';
		case 'host':
			return 'string';
		case 'hittime':
			return 'datetime';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'tinyUrl_id':
			$type = '%i';
			break;
		case 'host':
		case 'hitTime':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `TinyUrlHit`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `TinyUrlHit`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'TinyUrlHit');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
