<?
abstract class PersoonVoorkeur_Generated
	extends Entiteit
{
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $taal;					/**< \brief ENUM:GEEN/NL/EN */
	protected $bugSortering;			/**< \brief ENUM:ASC/DESC */
	protected $favoBugCategorie;		/**< \brief NULL */
	protected $favoBugCategorie_bugCategorieID;/**< \brief PRIMARY */
	protected $tourAfgemaakt;
	/**
	/**
	 * @brief De constructor van de PersoonVoorkeur_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->taal = 'GEEN';
		$this->bugSortering = 'ASC';
		$this->favoBugCategorie = NULL;
		$this->favoBugCategorie_bugCategorieID = NULL;
		$this->tourAfgemaakt = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld taal.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld taal.
	 */
	static public function enumsTaal()
	{
		static $vals = array('GEEN','NL','EN');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld taal.
	 *
	 * @return string
	 * De waarde van het veld taal.
	 */
	public function getTaal()
	{
		return $this->taal;
	}
	/**
	 * @brief Stel de waarde van het veld taal in.
	 *
	 * @param mixed $newTaal De nieuwe waarde.
	 *
	 * @return PersoonVoorkeur
	 * Dit PersoonVoorkeur-object.
	 */
	public function setTaal($newTaal)
	{
		unset($this->errors['Taal']);
		if(!is_null($newTaal))
			$newTaal = strtoupper(trim($newTaal));
		if($newTaal === "")
			$newTaal = NULL;
		if($this->taal === $newTaal)
			return $this;

		$this->taal = $newTaal;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld taal geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld taal geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTaal()
	{
		if (array_key_exists('Taal', $this->errors))
			return $this->errors['Taal'];
		$waarde = $this->getTaal();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsTaal()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld bugSortering.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld bugSortering.
	 */
	static public function enumsBugSortering()
	{
		static $vals = array('ASC','DESC');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld bugSortering.
	 *
	 * @return string
	 * De waarde van het veld bugSortering.
	 */
	public function getBugSortering()
	{
		return $this->bugSortering;
	}
	/**
	 * @brief Stel de waarde van het veld bugSortering in.
	 *
	 * @param mixed $newBugSortering De nieuwe waarde.
	 *
	 * @return PersoonVoorkeur
	 * Dit PersoonVoorkeur-object.
	 */
	public function setBugSortering($newBugSortering)
	{
		unset($this->errors['BugSortering']);
		if(!is_null($newBugSortering))
			$newBugSortering = strtoupper(trim($newBugSortering));
		if($newBugSortering === "")
			$newBugSortering = NULL;
		if($this->bugSortering === $newBugSortering)
			return $this;

		$this->bugSortering = $newBugSortering;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bugSortering geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bugSortering geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBugSortering()
	{
		if (array_key_exists('BugSortering', $this->errors))
			return $this->errors['BugSortering'];
		$waarde = $this->getBugSortering();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsBugSortering()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld favoBugCategorie.
	 *
	 * @return BugCategorie
	 * De waarde van het veld favoBugCategorie.
	 */
	public function getFavoBugCategorie()
	{
		if(!isset($this->favoBugCategorie)
		 && isset($this->favoBugCategorie_bugCategorieID)
		 ) {
			$this->favoBugCategorie = BugCategorie::geef
					( $this->favoBugCategorie_bugCategorieID
					);
		}
		return $this->favoBugCategorie;
	}
	/**
	 * @brief Stel de waarde van het veld favoBugCategorie in.
	 *
	 * @param mixed $new_bugCategorieID De nieuwe waarde.
	 *
	 * @return PersoonVoorkeur
	 * Dit PersoonVoorkeur-object.
	 */
	public function setFavoBugCategorie($new_bugCategorieID)
	{
		unset($this->errors['FavoBugCategorie']);
		if($new_bugCategorieID instanceof BugCategorie
		) {
			if($this->favoBugCategorie == $new_bugCategorieID
			&& $this->favoBugCategorie_bugCategorieID == $this->favoBugCategorie->getBugCategorieID())
				return $this;
			$this->favoBugCategorie = $new_bugCategorieID;
			$this->favoBugCategorie_bugCategorieID
					= $this->favoBugCategorie->getBugCategorieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_bugCategorieID) || $new_bugCategorieID == 0)) {
			if($this->favoBugCategorie == NULL && $this->favoBugCategorie_bugCategorieID == NULL)
				return $this;
			$this->favoBugCategorie = NULL;
			$this->favoBugCategorie_bugCategorieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_bugCategorieID)
		) {
			if($this->favoBugCategorie == NULL 
				&& $this->favoBugCategorie_bugCategorieID == (int)$new_bugCategorieID)
				return $this;
			$this->favoBugCategorie = NULL;
			$this->favoBugCategorie_bugCategorieID
					= (int)$new_bugCategorieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld favoBugCategorie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld favoBugCategorie geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkFavoBugCategorie()
	{
		if (array_key_exists('FavoBugCategorie', $this->errors))
			return $this->errors['FavoBugCategorie'];
		$waarde1 = $this->getFavoBugCategorie();
		$waarde2 = $this->getFavoBugCategorieBugCategorieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld favoBugCategorie_bugCategorieID.
	 *
	 * @return int
	 * De waarde van het veld favoBugCategorie_bugCategorieID.
	 */
	public function getFavoBugCategorieBugCategorieID()
	{
		if (is_null($this->favoBugCategorie_bugCategorieID) && isset($this->favoBugCategorie)) {
			$this->favoBugCategorie_bugCategorieID = $this->favoBugCategorie->getBugCategorieID();
		}
		return $this->favoBugCategorie_bugCategorieID;
	}
	/**
	 * @brief Geef de waarde van het veld tourAfgemaakt.
	 *
	 * @return bool
	 * De waarde van het veld tourAfgemaakt.
	 */
	public function getTourAfgemaakt()
	{
		return $this->tourAfgemaakt;
	}
	/**
	 * @brief Stel de waarde van het veld tourAfgemaakt in.
	 *
	 * @param mixed $newTourAfgemaakt De nieuwe waarde.
	 *
	 * @return PersoonVoorkeur
	 * Dit PersoonVoorkeur-object.
	 */
	public function setTourAfgemaakt($newTourAfgemaakt)
	{
		unset($this->errors['TourAfgemaakt']);
		if(!is_null($newTourAfgemaakt))
			$newTourAfgemaakt = (bool)$newTourAfgemaakt;
		if($this->tourAfgemaakt === $newTourAfgemaakt)
			return $this;

		$this->tourAfgemaakt = $newTourAfgemaakt;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tourAfgemaakt geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tourAfgemaakt geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTourAfgemaakt()
	{
		if (array_key_exists('TourAfgemaakt', $this->errors))
			return $this->errors['TourAfgemaakt'];
		$waarde = $this->getTourAfgemaakt();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return PersoonVoorkeur::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van PersoonVoorkeur.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return PersoonVoorkeur::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return PersoonVoorkeur|false
	 * Een PersoonVoorkeur-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$persoon_contactID = (int)$a[0];
		}
		else if($a instanceof Persoon)
		{
			$persoon_contactID = $a->getContactID();
		}
		else if(isset($a))
		{
			$persoon_contactID = (int)$a;
		}

		if(is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($persoon_contactID) ));
		return Entiteit::geefCache(array($persoon_contactID), 'PersoonVoorkeur');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'PersoonVoorkeur');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('PersoonVoorkeur::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `PersoonVoorkeur`.`persoon_contactID`'
		                 .     ', `PersoonVoorkeur`.`taal`'
		                 .     ', `PersoonVoorkeur`.`bugSortering`'
		                 .     ', `PersoonVoorkeur`.`favoBugCategorie_bugCategorieID`'
		                 .     ', `PersoonVoorkeur`.`tourAfgemaakt`'
		                 .     ', `PersoonVoorkeur`.`gewijzigdWanneer`'
		                 .     ', `PersoonVoorkeur`.`gewijzigdWie`'
		                 .' FROM `PersoonVoorkeur`'
		                 .' WHERE (`persoon_contactID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['persoon_contactID']);

			$obj = new PersoonVoorkeur(array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->taal  = strtoupper(trim($row['taal']));
			$obj->bugSortering  = strtoupper(trim($row['bugSortering']));
			$obj->favoBugCategorie_bugCategorieID  = (is_null($row['favoBugCategorie_bugCategorieID'])) ? null : (int) $row['favoBugCategorie_bugCategorieID'];
			$obj->tourAfgemaakt  = (bool) $row['tourAfgemaakt'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'PersoonVoorkeur')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;
		$this->getFavoBugCategorieBugCategorieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `PersoonVoorkeur`'
			          . ' (`persoon_contactID`, `taal`, `bugSortering`, `favoBugCategorie_bugCategorieID`, `tourAfgemaakt`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i, %i, %s, %i)'
			          , $this->persoon_contactID
			          , $this->taal
			          , $this->bugSortering
			          , $this->favoBugCategorie_bugCategorieID
			          , $this->tourAfgemaakt
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'PersoonVoorkeur')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `PersoonVoorkeur`'
			          .' SET `taal` = %s'
			          .   ', `bugSortering` = %s'
			          .   ', `favoBugCategorie_bugCategorieID` = %i'
			          .   ', `tourAfgemaakt` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          , $this->taal
			          , $this->bugSortering
			          , $this->favoBugCategorie_bugCategorieID
			          , $this->tourAfgemaakt
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPersoonVoorkeur
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPersoonVoorkeur($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPersoonVoorkeur($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Taal';
			$velden[] = 'BugSortering';
			$velden[] = 'FavoBugCategorie';
			$velden[] = 'TourAfgemaakt';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Taal';
			$velden[] = 'BugSortering';
			$velden[] = 'FavoBugCategorie';
			$velden[] = 'TourAfgemaakt';
			break;
		case 'get':
			$velden[] = 'Taal';
			$velden[] = 'BugSortering';
			$velden[] = 'FavoBugCategorie';
			$velden[] = 'TourAfgemaakt';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'favoBugCategorie_bugCategorieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Taal';
			$velden[] = 'BugSortering';
			$velden[] = 'FavoBugCategorie';
			$velden[] = 'TourAfgemaakt';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `PersoonVoorkeur`'
		          .' WHERE `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `PersoonVoorkeur`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van PersoonVoorkeur terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'taal':
			return 'enum';
		case 'bugsortering':
			return 'enum';
		case 'favobugcategorie':
			return 'foreign';
		case 'favobugcategorie_bugcategorieid':
			return 'int';
		case 'tourafgemaakt':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'persoon_contactID':
		case 'favoBugCategorie_bugCategorieID':
		case 'tourAfgemaakt':
			$type = '%i';
			break;
		case 'taal':
		case 'bugSortering':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `PersoonVoorkeur`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `PersoonVoorkeur`'
		          .' WHERE `persoon_contactID` = %i'
		                 , $veld
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'PersoonVoorkeur');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
