<?
abstract class ActiviteitVraag_Generated
	extends Entiteit
{
	protected $activiteit;				/**< \brief PRIMARY */
	protected $activiteit_activiteitID;	/**< \brief PRIMARY */
	protected $vraagID;					/**< \brief PRIMARY */
	protected $type;					/**< \brief ENUM:STRING/ENUM */
	protected $vraag;					/**< \brief LANG,NULL */
	protected $opties;					/**< \brief NULL */
	/** Verzamelingen **/
	protected $deelnemerAntwoordVerzameling;
	protected $iDealAntwoordVerzameling;
	/**
	/**
	 * @brief De constructor van de ActiviteitVraag_Generated-klasse.
	 *
	 * @param mixed $a Activiteit (Activiteit OR Array(activiteit_activiteitID) OR
	 * activiteit_activiteitID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Activiteit)
		{
			$this->activiteit = $a;
			$this->activiteit_activiteitID = $a->getActiviteitID();
		}
		else if(is_array($a))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->vraagID = NULL;
		$this->type = 'STRING';
		$this->vraag = array('nl' => NULL, 'en' => NULL);
		$this->opties = NULL;
		$this->deelnemerAntwoordVerzameling = NULL;
		$this->iDealAntwoordVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Activiteit';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @return Activiteit
	 * De waarde van het veld activiteit.
	 */
	public function getActiviteit()
	{
		if(!isset($this->activiteit)
		 && isset($this->activiteit_activiteitID)
		 ) {
			$this->activiteit = Activiteit::geef
					( $this->activiteit_activiteitID
					);
		}
		return $this->activiteit;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld activiteit_activiteitID.
	 */
	public function getActiviteitActiviteitID()
	{
		if (is_null($this->activiteit_activiteitID) && isset($this->activiteit)) {
			$this->activiteit_activiteitID = $this->activiteit->getActiviteitID();
		}
		return $this->activiteit_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld vraagID.
	 *
	 * @return int
	 * De waarde van het veld vraagID.
	 */
	public function getVraagID()
	{
		return $this->vraagID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld type.
	 */
	static public function enumsType()
	{
		static $vals = array('STRING','ENUM');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @return string
	 * De waarde van het veld type.
	 */
	public function getType()
	{
		return $this->type;
	}
	/**
	 * @brief Stel de waarde van het veld type in.
	 *
	 * @param mixed $newType De nieuwe waarde.
	 *
	 * @return ActiviteitVraag
	 * Dit ActiviteitVraag-object.
	 */
	public function setType($newType)
	{
		unset($this->errors['Type']);
		if(!is_null($newType))
			$newType = strtoupper(trim($newType));
		if($newType === "")
			$newType = NULL;
		if($this->type === $newType)
			return $this;

		$this->type = $newType;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld type geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld type geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkType()
	{
		if (array_key_exists('Type', $this->errors))
			return $this->errors['Type'];
		$waarde = $this->getType();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsType()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld vraag.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld vraag.
	 */
	public function getVraag($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->vraag[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld vraag in.
	 *
	 * @param mixed $newVraag De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return ActiviteitVraag
	 * Dit ActiviteitVraag-object.
	 */
	public function setVraag($newVraag, $lang = NULL)
	{
		unset($this->errors['Vraag']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newVraag))
			$newVraag = trim($newVraag);
		if($newVraag === "")
			$newVraag = NULL;
		if($this->vraag[$lang] === $newVraag)
			return $this;

		$this->vraag[$lang] = $newVraag;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld vraag geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld vraag geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkVraag($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkVraag('nl');
			$ret['en'] = $this->checkVraag('en');
			return $ret;
		}

		if (array_key_exists('Vraag', $this->errors))
			if (is_array($this->errors['Vraag']) && array_key_exists($lang, $this->errors['Vraag']))
				return $this->errors['Vraag'][$lang];
		$waarde = $this->getVraag($lang);
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opties.
	 *
	 * @return string
	 * De waarde van het veld opties.
	 */
	public function getOpties()
	{
		return $this->opties;
	}
	/**
	 * @brief Stel de waarde van het veld opties in.
	 *
	 * @param mixed $newOpties De nieuwe waarde.
	 *
	 * @return ActiviteitVraag
	 * Dit ActiviteitVraag-object.
	 */
	public function setOpties($newOpties)
	{
		unset($this->errors['Opties']);
		if(!is_null($newOpties))
			$newOpties = trim($newOpties);
		if($newOpties === "")
			$newOpties = NULL;
		if($this->opties === $newOpties)
			return $this;

		$this->opties = $newOpties;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opties geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opties geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkOpties()
	{
		if (array_key_exists('Opties', $this->errors))
			return $this->errors['Opties'];
		$waarde = $this->getOpties();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de DeelnemerAntwoordVerzameling die hoort bij dit object.
	 */
	public function getDeelnemerAntwoordVerzameling()
	{
		if(!$this->deelnemerAntwoordVerzameling instanceof DeelnemerAntwoordVerzameling)
			$this->deelnemerAntwoordVerzameling = DeelnemerAntwoordVerzameling::fromVraag($this);
		return $this->deelnemerAntwoordVerzameling;
	}
	/**
	 * @brief Returneert de IDealAntwoordVerzameling die hoort bij dit object.
	 */
	public function getIDealAntwoordVerzameling()
	{
		if(!$this->iDealAntwoordVerzameling instanceof IDealAntwoordVerzameling)
			$this->iDealAntwoordVerzameling = IDealAntwoordVerzameling::fromVraag($this);
		return $this->iDealAntwoordVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ActiviteitVraag::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ActiviteitVraag.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ActiviteitVraag::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getActiviteitActiviteitID()
		                      , $this->getVraagID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return ActiviteitVraag|false
	 * Een ActiviteitVraag-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$activiteit_activiteitID = (int)$a[0];
			$vraagID = (int)$a[1];
		}
		else if($a instanceof Activiteit
		     && isset($b))
		{
			$activiteit_activiteitID = $a->getActiviteitID();
			$vraagID = (int)$b;
		}
		else if(isset($a)
		     && isset($b))
		{
			$activiteit_activiteitID = (int)$a;
			$vraagID = (int)$b;
		}

		if(is_null($activiteit_activiteitID)
		|| is_null($vraagID))
			throw new BadMethodCallException();

		static::cache(array( array($activiteit_activiteitID, $vraagID) ));
		return Entiteit::geefCache(array($activiteit_activiteitID, $vraagID), 'ActiviteitVraag');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'ActiviteitVraag');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('ActiviteitVraag::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `ActiviteitVraag`.`activiteit_activiteitID`'
		                 .     ', `ActiviteitVraag`.`vraagID`'
		                 .     ', `ActiviteitVraag`.`type`'
		                 .     ', `ActiviteitVraag`.`vraag_NL`'
		                 .     ', `ActiviteitVraag`.`vraag_EN`'
		                 .     ', `ActiviteitVraag`.`opties`'
		                 .     ', `ActiviteitVraag`.`gewijzigdWanneer`'
		                 .     ', `ActiviteitVraag`.`gewijzigdWie`'
		                 .' FROM `ActiviteitVraag`'
		                 .' WHERE (`activiteit_activiteitID`, `vraagID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['activiteit_activiteitID']
			                  ,$row['vraagID']);

			$obj = new ActiviteitVraag(array($row['activiteit_activiteitID']));

			$obj->inDB = True;

			$obj->vraagID  = (int) $row['vraagID'];
			$obj->type  = strtoupper(trim($row['type']));
			$obj->vraag['nl'] = $row['vraag_NL'];
			$obj->vraag['en'] = $row['vraag_EN'];
			$obj->opties  = (is_null($row['opties'])) ? null : trim($row['opties']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ActiviteitVraag')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getActiviteit();
		if(!$rel->getInDB())
			throw new LogicException('foreign Activiteit is not in DB');
		$this->activiteit_activiteitID = $rel->getActiviteitID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `ActiviteitVraag`'
			          . ' (`activiteit_activiteitID`, `vraagID`, `type`, `vraag_NL`, `vraag_EN`, `opties`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %s, %s, %s, %i)'
			          , $this->activiteit_activiteitID
			          , $this->vraagID
			          , $this->type
			          , $this->vraag['nl']
			          , $this->vraag['en']
			          , $this->opties
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'ActiviteitVraag')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ActiviteitVraag`'
			          .' SET `type` = %s'
			          .   ', `vraag_NL` = %s'
			          .   ', `vraag_EN` = %s'
			          .   ', `opties` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `activiteit_activiteitID` = %i'
			          .  ' AND `vraagID` = %i'
			          , $this->type
			          , $this->vraag['nl']
			          , $this->vraag['en']
			          , $this->opties
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->activiteit_activiteitID
			          , $this->vraagID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenActiviteitVraag
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenActiviteitVraag($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenActiviteitVraag($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Type';
			$velden[] = 'Vraag';
			$velden[] = 'Opties';
			break;
		case 'lang':
			$velden[] = 'Vraag';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Type';
			$velden[] = 'Vraag';
			$velden[] = 'Opties';
			break;
		case 'get':
			$velden[] = 'Type';
			$velden[] = 'Vraag';
			$velden[] = 'Opties';
		case 'primary':
			$velden[] = 'activiteit_activiteitID';
			break;
		case 'verzamelingen':
			$velden[] = 'DeelnemerAntwoordVerzameling';
			$velden[] = 'IDealAntwoordVerzameling';
			break;
		default:
			$velden[] = 'Type';
			$velden[] = 'Vraag';
			$velden[] = 'Opties';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle DeelnemerAntwoord-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DeelnemerAntwoordFromVraagVerz = DeelnemerAntwoordVerzameling::fromVraag($this);
		$returnValue = $DeelnemerAntwoordFromVraagVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object DeelnemerAntwoord met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle iDealAntwoord-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$iDealAntwoordFromVraagVerz = iDealAntwoordVerzameling::fromVraag($this);
		$returnValue = $iDealAntwoordFromVraagVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object iDealAntwoord met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $DeelnemerAntwoordFromVraagVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $iDealAntwoordFromVraagVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ActiviteitVraag`'
		          .' WHERE `activiteit_activiteitID` = %i'
		          .  ' AND `vraagID` = %i'
		          .' LIMIT 1'
		          , $this->activiteit_activiteitID
		          , $this->vraagID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->activiteit = NULL;
		$this->activiteit_activiteitID = NULL;
		$this->vraagID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `ActiviteitVraag`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `activiteit_activiteitID` = %i'
			          .  ' AND `vraagID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->activiteit_activiteitID
			          , $this->vraagID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van ActiviteitVraag terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'activiteit':
			return 'foreign';
		case 'activiteit_activiteitid':
			return 'int';
		case 'vraagid':
			return 'int';
		case 'type':
			return 'enum';
		case 'vraag':
			return 'string';
		case 'opties':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'activiteit_activiteitID':
		case 'vraagID':
			$type = '%i';
			break;
		case 'type':
		case 'vraag':
		case 'opties':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ActiviteitVraag`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `activiteit_activiteitID` = %i'
		          .  ' AND `vraagID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->activiteit_activiteitID
		          , $this->vraagID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ActiviteitVraag`'
		          .' WHERE `activiteit_activiteitID` = %i'
		          .  ' AND `vraagID` = %i'
		                 , $veld
		          , $this->activiteit_activiteitID
		          , $this->vraagID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'ActiviteitVraag');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle DeelnemerAntwoord-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DeelnemerAntwoordFromVraagVerz = DeelnemerAntwoordVerzameling::fromVraag($this);
		$dependencies['DeelnemerAntwoord'] = $DeelnemerAntwoordFromVraagVerz;

		// Verzamel alle iDealAntwoord-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$iDealAntwoordFromVraagVerz = iDealAntwoordVerzameling::fromVraag($this);
		$dependencies['iDealAntwoord'] = $iDealAntwoordFromVraagVerz;

		return $dependencies;
	}
}
