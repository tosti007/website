<?
/**
 * @brief 'AT'AUTH_SET:bestuur
 *  * Een tinyurl die vanaf A-Es2.nl/t/*url* doorverwijst naar een externe url en
 * bijhoudt wie er langs is gekomen.
 */
abstract class TinyUrl_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $tinyUrl;					/**< \brief Moet case sensetive zijn!! (bv utf8_bin) */
	protected $outgoingUrl;
	/** Verzamelingen **/
	protected $tinyUrlHitVerzameling;
	protected $tinyMailingUrlVerzameling;
	/**
	 * @brief De constructor van de TinyUrl_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->tinyUrl = '';
		$this->outgoingUrl = '';
		$this->tinyUrlHitVerzameling = NULL;
		$this->tinyMailingUrlVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Stel de waarde van het veld id in.
	 *
	 * @param mixed $newId De nieuwe waarde.
	 *
	 * @return TinyUrl
	 * Dit TinyUrl-object.
	 */
	public function setId($newId)
	{
		unset($this->errors['Id']);
		if(!is_null($newId))
			$newId = (int)$newId;
		if($this->id === $newId)
			return $this;

		$this->id = $newId;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld id geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld id geldig is; anders een string met een
	 * foutmelding.
	 */
	public function checkId()
	{
		if (array_key_exists('Id', $this->errors))
			return $this->errors['Id'];
		$waarde = $this->getId();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl.
	 *
	 * @return string
	 * De waarde van het veld tinyUrl.
	 */
	public function getTinyUrl()
	{
		return $this->tinyUrl;
	}
	/**
	 * @brief Stel de waarde van het veld tinyUrl in.
	 *
	 * @param mixed $newTinyUrl De nieuwe waarde.
	 *
	 * @return TinyUrl
	 * Dit TinyUrl-object.
	 */
	public function setTinyUrl($newTinyUrl)
	{
		unset($this->errors['TinyUrl']);
		if(!is_null($newTinyUrl))
			$newTinyUrl = trim($newTinyUrl);
		if($newTinyUrl === "")
			$newTinyUrl = NULL;
		if($this->tinyUrl === $newTinyUrl)
			return $this;

		$this->tinyUrl = $newTinyUrl;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tinyUrl geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tinyUrl geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkTinyUrl()
	{
		if (array_key_exists('TinyUrl', $this->errors))
			return $this->errors['TinyUrl'];
		$waarde = $this->getTinyUrl();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld outgoingUrl.
	 *
	 * @return string
	 * De waarde van het veld outgoingUrl.
	 */
	public function getOutgoingUrl()
	{
		return $this->outgoingUrl;
	}
	/**
	 * @brief Stel de waarde van het veld outgoingUrl in.
	 *
	 * @param mixed $newOutgoingUrl De nieuwe waarde.
	 *
	 * @return TinyUrl
	 * Dit TinyUrl-object.
	 */
	public function setOutgoingUrl($newOutgoingUrl)
	{
		unset($this->errors['OutgoingUrl']);
		if(!is_null($newOutgoingUrl))
			$newOutgoingUrl = trim($newOutgoingUrl);
		if($newOutgoingUrl === "")
			$newOutgoingUrl = NULL;
		if($this->outgoingUrl === $newOutgoingUrl)
			return $this;

		$this->outgoingUrl = $newOutgoingUrl;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld outgoingUrl geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld outgoingUrl geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOutgoingUrl()
	{
		if (array_key_exists('OutgoingUrl', $this->errors))
			return $this->errors['OutgoingUrl'];
		$waarde = $this->getOutgoingUrl();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de TinyUrlHitVerzameling die hoort bij dit object.
	 */
	public function getTinyUrlHitVerzameling()
	{
		if(!$this->tinyUrlHitVerzameling instanceof TinyUrlHitVerzameling)
			$this->tinyUrlHitVerzameling = TinyUrlHitVerzameling::fromTinyUrl($this);
		return $this->tinyUrlHitVerzameling;
	}
	/**
	 * @brief Returneert de TinyMailingUrlVerzameling die hoort bij dit object.
	 */
	public function getTinyMailingUrlVerzameling()
	{
		if(!$this->tinyMailingUrlVerzameling instanceof TinyMailingUrlVerzameling)
			$this->tinyMailingUrlVerzameling = TinyMailingUrlVerzameling::fromTinyUrl($this);
		return $this->tinyMailingUrlVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return TinyUrl::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van TinyUrl.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return TinyUrl::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return TinyUrl|false
	 * Een TinyUrl-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'TinyUrl');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'TinyUrl');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('TinyUrl::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `TinyUrl`.`id`'
		                 .     ', `TinyUrl`.`tinyUrl`'
		                 .     ', `TinyUrl`.`outgoingUrl`'
		                 .     ', `TinyUrl`.`gewijzigdWanneer`'
		                 .     ', `TinyUrl`.`gewijzigdWie`'
		                 .' FROM `TinyUrl`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new TinyUrl();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->tinyUrl  = trim($row['tinyUrl']);
			$obj->outgoingUrl = $row['outgoingUrl'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'TinyUrl')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `TinyUrl`'
			          . ' (`tinyUrl`, `outgoingUrl`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %i)'
			          , $this->tinyUrl
			          , $this->outgoingUrl
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'TinyUrl')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `TinyUrl`'
			          .' SET `tinyUrl` = %s'
			          .   ', `outgoingUrl` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->tinyUrl
			          , $this->outgoingUrl
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTinyUrl
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTinyUrl($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTinyUrl($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
			break;
		case 'get':
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'TinyUrlHitVerzameling';
			$velden[] = 'TinyMailingUrlVerzameling';
			break;
		default:
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle TinyUrlHit-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TinyUrlHitFromTinyUrlVerz = TinyUrlHitVerzameling::fromTinyUrl($this);
		$returnValue = $TinyUrlHitFromTinyUrlVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TinyUrlHit met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle TinyMailingUrl-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TinyMailingUrlFromTinyUrlVerz = TinyMailingUrlVerzameling::fromTinyUrl($this);
		$returnValue = $TinyMailingUrlFromTinyUrlVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TinyMailingUrl met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $TinyUrlHitFromTinyUrlVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TinyMailingUrlFromTinyUrlVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `TinyUrl`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `TinyUrl`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van TinyUrl terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'tinyurl':
			return 'string';
		case 'outgoingurl':
			return 'url';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
			$type = '%i';
			break;
		case 'tinyUrl':
		case 'outgoingUrl':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `TinyUrl`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `TinyUrl`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'TinyUrl');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle TinyUrlHit-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TinyUrlHitFromTinyUrlVerz = TinyUrlHitVerzameling::fromTinyUrl($this);
		$dependencies['TinyUrlHit'] = $TinyUrlHitFromTinyUrlVerz;

		// Verzamel alle TinyMailingUrl-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TinyMailingUrlFromTinyUrlVerz = TinyMailingUrlVerzameling::fromTinyUrl($this);
		$dependencies['TinyMailingUrl'] = $TinyMailingUrlFromTinyUrlVerz;

		return $dependencies;
	}
}
