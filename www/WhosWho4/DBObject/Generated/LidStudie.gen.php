<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class LidStudie_Generated
	extends Entiteit
{
	protected $lidStudieID;				/**< \brief PRIMARY */
	protected $studie;
	protected $studie_studieID;			/**< \brief PRIMARY */
	protected $lid;
	protected $lid_contactID;			/**< \brief PRIMARY */
	protected $datumBegin;
	protected $datumEind;				/**< \brief NULL */
	protected $status;					/**< \brief ENUM:STUDEREND/ALUMNUS/GESTOPT */
	protected $groep;					/**< \brief NULL */
	protected $groep_groepID;			/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de LidStudie_Generated-klasse.
	 *
	 * @param mixed $a Lid (Lid OR Array(lid_contactID) OR lid_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Lid)
		{
			$this->lid = $a;
			$this->lid_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->lidStudieID = NULL;
		$this->studie = NULL;
		$this->studie_studieID = 0;
		$this->datumBegin = new DateTimeLocale();
		$this->datumEind = new DateTimeLocale(NULL);
		$this->status = 'STUDEREND';
		$this->groep = NULL;
		$this->groep_groepID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Lid';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld lidStudieID.
	 *
	 * @return int
	 * De waarde van het veld lidStudieID.
	 */
	public function getLidStudieID()
	{
		return $this->lidStudieID;
	}
	/**
	 * @brief Stel de waarde van het veld lidStudieID in.
	 *
	 * @param mixed $newLidStudieID De nieuwe waarde.
	 *
	 * @return LidStudie
	 * Dit LidStudie-object.
	 */
	public function setLidStudieID($newLidStudieID)
	{
		unset($this->errors['LidStudieID']);
		if(!is_null($newLidStudieID))
			$newLidStudieID = (int)$newLidStudieID;
		if($this->lidStudieID === $newLidStudieID)
			return $this;

		$this->lidStudieID = $newLidStudieID;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lidStudieID geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lidStudieID geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkLidStudieID()
	{
		if (array_key_exists('LidStudieID', $this->errors))
			return $this->errors['LidStudieID'];
		$waarde = $this->getLidStudieID();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld studie.
	 *
	 * @return Studie
	 * De waarde van het veld studie.
	 */
	public function getStudie()
	{
		if(!isset($this->studie)
		 && isset($this->studie_studieID)
		 ) {
			$this->studie = Studie::geef
					( $this->studie_studieID
					);
		}
		return $this->studie;
	}
	/**
	 * @brief Stel de waarde van het veld studie in.
	 *
	 * @param mixed $new_studieID De nieuwe waarde.
	 *
	 * @return LidStudie
	 * Dit LidStudie-object.
	 */
	public function setStudie($new_studieID)
	{
		unset($this->errors['Studie']);
		if($new_studieID instanceof Studie
		) {
			if($this->studie == $new_studieID
			&& $this->studie_studieID == $this->studie->getStudieID())
				return $this;
			$this->studie = $new_studieID;
			$this->studie_studieID
					= $this->studie->getStudieID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_studieID)
		) {
			if($this->studie == NULL 
				&& $this->studie_studieID == (int)$new_studieID)
				return $this;
			$this->studie = NULL;
			$this->studie_studieID
					= (int)$new_studieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld studie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld studie geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStudie()
	{
		if (array_key_exists('Studie', $this->errors))
			return $this->errors['Studie'];
		$waarde1 = $this->getStudie();
		$waarde2 = $this->getStudieStudieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld studie_studieID.
	 *
	 * @return int
	 * De waarde van het veld studie_studieID.
	 */
	public function getStudieStudieID()
	{
		if (is_null($this->studie_studieID) && isset($this->studie)) {
			$this->studie_studieID = $this->studie->getStudieID();
		}
		return $this->studie_studieID;
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @return Lid
	 * De waarde van het veld lid.
	 */
	public function getLid()
	{
		if(!isset($this->lid)
		 && isset($this->lid_contactID)
		 ) {
			$this->lid = Lid::geef
					( $this->lid_contactID
					);
		}
		return $this->lid;
	}
	/**
	 * @brief Geef de waarde van het veld lid_contactID.
	 *
	 * @return int
	 * De waarde van het veld lid_contactID.
	 */
	public function getLidContactID()
	{
		if (is_null($this->lid_contactID) && isset($this->lid)) {
			$this->lid_contactID = $this->lid->getContactID();
		}
		return $this->lid_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumBegin.
	 */
	public function getDatumBegin()
	{
		return $this->datumBegin;
	}
	/**
	 * @brief Stel de waarde van het veld datumBegin in.
	 *
	 * @param mixed $newDatumBegin De nieuwe waarde.
	 *
	 * @return LidStudie
	 * Dit LidStudie-object.
	 */
	public function setDatumBegin($newDatumBegin)
	{
		unset($this->errors['DatumBegin']);
		if(!$newDatumBegin instanceof DateTimeLocale) {
			try {
				$newDatumBegin = new DateTimeLocale($newDatumBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumBegin->strftime('%F %T') == $newDatumBegin->strftime('%F %T'))
			return $this;

		$this->datumBegin = $newDatumBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumBegin()
	{
		if (array_key_exists('DatumBegin', $this->errors))
			return $this->errors['DatumBegin'];
		$waarde = $this->getDatumBegin();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumEind.
	 */
	public function getDatumEind()
	{
		return $this->datumEind;
	}
	/**
	 * @brief Stel de waarde van het veld datumEind in.
	 *
	 * @param mixed $newDatumEind De nieuwe waarde.
	 *
	 * @return LidStudie
	 * Dit LidStudie-object.
	 */
	public function setDatumEind($newDatumEind)
	{
		unset($this->errors['DatumEind']);
		if(!$newDatumEind instanceof DateTimeLocale) {
			try {
				$newDatumEind = new DateTimeLocale($newDatumEind);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumEind->strftime('%F %T') == $newDatumEind->strftime('%F %T'))
			return $this;

		$this->datumEind = $newDatumEind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumEind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumEind geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumEind()
	{
		if (array_key_exists('DatumEind', $this->errors))
			return $this->errors['DatumEind'];
		$waarde = $this->getDatumEind();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('STUDEREND','ALUMNUS','GESTOPT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return LidStudie
	 * Dit LidStudie-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld groep.
	 *
	 * @return IntroGroep
	 * De waarde van het veld groep.
	 */
	public function getGroep()
	{
		if(!isset($this->groep)
		 && isset($this->groep_groepID)
		 ) {
			$this->groep = IntroGroep::geef
					( $this->groep_groepID
					);
		}
		return $this->groep;
	}
	/**
	 * @brief Stel de waarde van het veld groep in.
	 *
	 * @param mixed $new_groepID De nieuwe waarde.
	 *
	 * @return LidStudie
	 * Dit LidStudie-object.
	 */
	public function setGroep($new_groepID)
	{
		unset($this->errors['Groep']);
		if($new_groepID instanceof IntroGroep
		) {
			if($this->groep == $new_groepID
			&& $this->groep_groepID == $this->groep->getGroepID())
				return $this;
			$this->groep = $new_groepID;
			$this->groep_groepID
					= $this->groep->getGroepID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_groepID) || $new_groepID == 0)) {
			if($this->groep == NULL && $this->groep_groepID == NULL)
				return $this;
			$this->groep = NULL;
			$this->groep_groepID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_groepID)
		) {
			if($this->groep == NULL 
				&& $this->groep_groepID == (int)$new_groepID)
				return $this;
			$this->groep = NULL;
			$this->groep_groepID
					= (int)$new_groepID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld groep geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld groep geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkGroep()
	{
		if (array_key_exists('Groep', $this->errors))
			return $this->errors['Groep'];
		$waarde1 = $this->getGroep();
		$waarde2 = $this->getGroepGroepID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld groep_groepID.
	 *
	 * @return int
	 * De waarde van het veld groep_groepID.
	 */
	public function getGroepGroepID()
	{
		if (is_null($this->groep_groepID) && isset($this->groep)) {
			$this->groep_groepID = $this->groep->getGroepID();
		}
		return $this->groep_groepID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return LidStudie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van LidStudie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return LidStudie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getLidStudieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return LidStudie|false
	 * Een LidStudie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$lidStudieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$lidStudieID = (int)$a;
		}

		if(is_null($lidStudieID))
			throw new BadMethodCallException();

		static::cache(array( array($lidStudieID) ));
		return Entiteit::geefCache(array($lidStudieID), 'LidStudie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'LidStudie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('LidStudie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `LidStudie`.`lidStudieID`'
		                 .     ', `LidStudie`.`studie_studieID`'
		                 .     ', `LidStudie`.`lid_contactID`'
		                 .     ', `LidStudie`.`datumBegin`'
		                 .     ', `LidStudie`.`datumEind`'
		                 .     ', `LidStudie`.`status`'
		                 .     ', `LidStudie`.`groep_groepID`'
		                 .     ', `LidStudie`.`gewijzigdWanneer`'
		                 .     ', `LidStudie`.`gewijzigdWie`'
		                 .' FROM `LidStudie`'
		                 .' WHERE (`lidStudieID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['lidStudieID']);

			$obj = new LidStudie(array($row['lid_contactID']));

			$obj->inDB = True;

			$obj->lidStudieID  = (int) $row['lidStudieID'];
			$obj->studie_studieID  = (int) $row['studie_studieID'];
			$obj->datumBegin  = new DateTimeLocale($row['datumBegin']);
			$obj->datumEind  = new DateTimeLocale($row['datumEind']);
			$obj->status  = strtoupper(trim($row['status']));
			$obj->groep_groepID  = (is_null($row['groep_groepID'])) ? null : (int) $row['groep_groepID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'LidStudie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getStudieStudieID();
		$this->getLidContactID();
		$this->getGroepGroepID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->lidStudieID =
			$WSW4DB->q('RETURNID INSERT INTO `LidStudie`'
			          . ' (`studie_studieID`, `lid_contactID`, `datumBegin`, `datumEind`, `status`, `groep_groepID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %s, %i, %s, %i)'
			          , $this->studie_studieID
			          , $this->lid_contactID
			          , $this->datumBegin->strftime('%F %T')
			          , (!is_null($this->datumEind))?$this->datumEind->strftime('%F %T'):null
			          , $this->status
			          , $this->groep_groepID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'LidStudie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `LidStudie`'
			          .' SET `studie_studieID` = %i'
			          .   ', `lid_contactID` = %i'
			          .   ', `datumBegin` = %s'
			          .   ', `datumEind` = %s'
			          .   ', `status` = %s'
			          .   ', `groep_groepID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lidStudieID` = %i'
			          , $this->studie_studieID
			          , $this->lid_contactID
			          , $this->datumBegin->strftime('%F %T')
			          , (!is_null($this->datumEind))?$this->datumEind->strftime('%F %T'):null
			          , $this->status
			          , $this->groep_groepID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lidStudieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenLidStudie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenLidStudie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenLidStudie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Studie';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Status';
			$velden[] = 'Groep';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Studie';
			$velden[] = 'DatumBegin';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Studie';
			$velden[] = 'Lid';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Status';
			$velden[] = 'Groep';
			break;
		case 'get':
			$velden[] = 'Studie';
			$velden[] = 'Lid';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Status';
			$velden[] = 'Groep';
		case 'primary':
			$velden[] = 'studie_studieID';
			$velden[] = 'lid_contactID';
			$velden[] = 'groep_groepID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Studie';
			$velden[] = 'Lid';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Status';
			$velden[] = 'Groep';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `LidStudie`'
		          .' WHERE `lidStudieID` = %i'
		          .' LIMIT 1'
		          , $this->lidStudieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->lidStudieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `LidStudie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lidStudieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lidStudieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van LidStudie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'lidstudieid':
			return 'int';
		case 'studie':
			return 'foreign';
		case 'studie_studieid':
			return 'int';
		case 'lid':
			return 'foreign';
		case 'lid_contactid':
			return 'int';
		case 'datumbegin':
			return 'date';
		case 'datumeind':
			return 'date';
		case 'status':
			return 'enum';
		case 'groep':
			return 'foreign';
		case 'groep_groepid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'lidStudieID':
		case 'studie_studieID':
		case 'lid_contactID':
		case 'groep_groepID':
			$type = '%i';
			break;
		case 'datumBegin':
		case 'datumEind':
		case 'status':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `LidStudie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `lidStudieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->lidStudieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `LidStudie`'
		          .' WHERE `lidStudieID` = %i'
		                 , $veld
		          , $this->lidStudieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'LidStudie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
