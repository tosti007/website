<?
abstract class KartVoorwerp_Generated
	extends Entiteit
{
	protected $kartVoorwerpID;			/**< \brief PRIMARY */
	protected $persoonKart;
	protected $persoonKart_contactID;	/**< \brief PRIMARY */
	protected $persoonKart_naam;		/**< \brief PRIMARY */
	protected $persoon;					/**< \brief NULL */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $commissie;				/**< \brief NULL */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $media;					/**< \brief NULL */
	protected $media_mediaID;			/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de KartVoorwerp_Generated-klasse.
	 *
	 * @param mixed $a PersoonKart (PersoonKart OR Array(persoonKart_contactID,
	 * persoonKart_naam))
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]) && is_null($a[1]) && is_null($a[2]))
			$a = NULL;

		if($a instanceof PersoonKart)
		{
			$this->persoonKart = $a;
			$this->persoonKart_contactID = $a->getPersoonContactID();
			$this->persoonKart_naam = $a->getNaam();
		}
		else if(is_array($a))
		{
			$this->persoonKart = NULL;
			$this->persoonKart_contactID = (int)$a[0];
			$this->persoonKart_naam = (string)$a[1];
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->kartVoorwerpID = NULL;
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->media = NULL;
		$this->media_mediaID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'PersoonKart';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld kartVoorwerpID.
	 *
	 * @return int
	 * De waarde van het veld kartVoorwerpID.
	 */
	public function getKartVoorwerpID()
	{
		return $this->kartVoorwerpID;
	}
	/**
	 * @brief Geef de waarde van het veld persoonKart.
	 *
	 * @return PersoonKart
	 * De waarde van het veld persoonKart.
	 */
	public function getPersoonKart()
	{
		if(!isset($this->persoonKart)
		 && isset($this->persoonKart_contactID)
		 && isset($this->persoonKart_naam)
		 ) {
			$this->persoonKart = PersoonKart::geef
					( $this->persoonKart_contactID
					, $this->persoonKart_naam
					);
		}
		return $this->persoonKart;
	}
	/**
	 * @brief Geef de waarde van het veld persoonKart_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoonKart_contactID.
	 */
	public function getPersoonKartContactID()
	{
		if (is_null($this->persoonKart_contactID) && isset($this->persoonKart)) {
			$this->persoonKart_contactID = $this->persoonKart->getContactID();
		}
		return $this->persoonKart_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld persoonKart_naam.
	 *
	 * @return string
	 * De waarde van het veld persoonKart_naam.
	 */
	public function getPersoonKartNaam()
	{
		if (is_null($this->persoonKart_naam) && isset($this->persoonKart)) {
			$this->persoonKart_naam = $this->persoonKart->getNaam();
		}
		return $this->persoonKart_naam;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Stel de waarde van het veld persoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return KartVoorwerp
	 * Dit KartVoorwerp-object.
	 */
	public function setPersoon($new_contactID)
	{
		unset($this->errors['Persoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->persoon == $new_contactID
			&& $this->persoon_contactID == $this->persoon->getContactID())
				return $this;
			$this->persoon = $new_contactID;
			$this->persoon_contactID
					= $this->persoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->persoon == NULL && $this->persoon_contactID == NULL)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->persoon == NULL 
				&& $this->persoon_contactID == (int)$new_contactID)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld persoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld persoon geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPersoon()
	{
		if (array_key_exists('Persoon', $this->errors))
			return $this->errors['Persoon'];
		$waarde1 = $this->getPersoon();
		$waarde2 = $this->getPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return KartVoorwerp
	 * Dit KartVoorwerp-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_commissieID) || $new_commissieID == 0)) {
			if($this->commissie == NULL && $this->commissie_commissieID == NULL)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld media.
	 *
	 * @return Media
	 * De waarde van het veld media.
	 */
	public function getMedia()
	{
		if(!isset($this->media)
		 && isset($this->media_mediaID)
		 ) {
			$this->media = Media::geef
					( $this->media_mediaID
					);
		}
		return $this->media;
	}
	/**
	 * @brief Stel de waarde van het veld media in.
	 *
	 * @param mixed $new_mediaID De nieuwe waarde.
	 *
	 * @return KartVoorwerp
	 * Dit KartVoorwerp-object.
	 */
	public function setMedia($new_mediaID)
	{
		unset($this->errors['Media']);
		if($new_mediaID instanceof Media
		) {
			if($this->media == $new_mediaID
			&& $this->media_mediaID == $this->media->getMediaID())
				return $this;
			$this->media = $new_mediaID;
			$this->media_mediaID
					= $this->media->getMediaID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_mediaID) || $new_mediaID == 0)) {
			if($this->media == NULL && $this->media_mediaID == NULL)
				return $this;
			$this->media = NULL;
			$this->media_mediaID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_mediaID)
		) {
			if($this->media == NULL 
				&& $this->media_mediaID == (int)$new_mediaID)
				return $this;
			$this->media = NULL;
			$this->media_mediaID
					= (int)$new_mediaID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld media geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld media geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMedia()
	{
		if (array_key_exists('Media', $this->errors))
			return $this->errors['Media'];
		$waarde1 = $this->getMedia();
		$waarde2 = $this->getMediaMediaID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld media_mediaID.
	 *
	 * @return int
	 * De waarde van het veld media_mediaID.
	 */
	public function getMediaMediaID()
	{
		if (is_null($this->media_mediaID) && isset($this->media)) {
			$this->media_mediaID = $this->media->getMediaID();
		}
		return $this->media_mediaID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return KartVoorwerp::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van KartVoorwerp.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return KartVoorwerp::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getKartVoorwerpID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return KartVoorwerp|false
	 * Een KartVoorwerp-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$kartVoorwerpID = (int)$a[0];
		}
		else if(isset($a))
		{
			$kartVoorwerpID = (int)$a;
		}

		if(is_null($kartVoorwerpID))
			throw new BadMethodCallException();

		static::cache(array( array($kartVoorwerpID) ));
		return Entiteit::geefCache(array($kartVoorwerpID), 'KartVoorwerp');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'KartVoorwerp');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('KartVoorwerp::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `KartVoorwerp`.`kartVoorwerpID`'
		                 .     ', `KartVoorwerp`.`persoonKart_persoon_contactID`'
		                 .     ', `KartVoorwerp`.`persoonKart_naam`'
		                 .     ', `KartVoorwerp`.`persoon_contactID`'
		                 .     ', `KartVoorwerp`.`commissie_commissieID`'
		                 .     ', `KartVoorwerp`.`media_mediaID`'
		                 .     ', `KartVoorwerp`.`gewijzigdWanneer`'
		                 .     ', `KartVoorwerp`.`gewijzigdWie`'
		                 .' FROM `KartVoorwerp`'
		                 .' WHERE (`kartVoorwerpID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['kartVoorwerpID']);

			$obj = new KartVoorwerp(array($row['persoonKart_persoon_contactID'], $row['persoonKart_naam']));

			$obj->inDB = True;

			$obj->kartVoorwerpID  = (int) $row['kartVoorwerpID'];
			$obj->persoon_contactID  = (is_null($row['persoon_contactID'])) ? null : (int) $row['persoon_contactID'];
			$obj->commissie_commissieID  = (is_null($row['commissie_commissieID'])) ? null : (int) $row['commissie_commissieID'];
			$obj->media_mediaID  = (is_null($row['media_mediaID'])) ? null : (int) $row['media_mediaID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'KartVoorwerp')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getPersoonKartContactID();
		$this->getPersoonKartNaam();
		$this->getPersoonContactID();
		$this->getCommissieCommissieID();
		$this->getMediaMediaID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->kartVoorwerpID =
			$WSW4DB->q('RETURNID INSERT INTO `KartVoorwerp`'
			          . ' (`persoonKart_persoon_contactID`, `persoonKart_naam`, `persoon_contactID`, `commissie_commissieID`, `media_mediaID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %i, %i, %i, %s, %i)'
			          , $this->persoonKart_contactID
			          , $this->persoonKart_naam
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          , $this->media_mediaID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'KartVoorwerp')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `KartVoorwerp`'
			          .' SET `persoonKart_contactID` = %i'
			          .   ', `persoonKart_naam` = %s'
			          .   ', `persoon_contactID` = %i'
			          .   ', `commissie_commissieID` = %i'
			          .   ', `media_mediaID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `kartVoorwerpID` = %i'
			          , $this->persoonKart_contactID
			          , $this->persoonKart_naam
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          , $this->media_mediaID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->kartVoorwerpID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenKartVoorwerp
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenKartVoorwerp($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenKartVoorwerp($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Media';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'PersoonKart';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Media';
			break;
		case 'get':
			$velden[] = 'PersoonKart';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Media';
		case 'primary':
			$velden[] = 'persoonKart_contactID';
			$velden[] = 'persoonKart_naam';
			$velden[] = 'persoon_contactID';
			$velden[] = 'commissie_commissieID';
			$velden[] = 'media_mediaID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'PersoonKart';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Media';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `KartVoorwerp`'
		          .' WHERE `kartVoorwerpID` = %i'
		          .' LIMIT 1'
		          , $this->kartVoorwerpID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->kartVoorwerpID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `KartVoorwerp`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `kartVoorwerpID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->kartVoorwerpID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van KartVoorwerp terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'kartvoorwerpid':
			return 'int';
		case 'persoonkart':
			return 'foreign';
		case 'persoonkart_contactid':
			return 'int';
		case 'persoonkart_naam':
			return 'string';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'media':
			return 'foreign';
		case 'media_mediaid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'kartVoorwerpID':
		case 'persoonKart_contactID':
		case 'persoon_contactID':
		case 'commissie_commissieID':
		case 'media_mediaID':
			$type = '%i';
			break;
		case 'persoonKart_naam':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `KartVoorwerp`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `kartVoorwerpID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->kartVoorwerpID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `KartVoorwerp`'
		          .' WHERE `kartVoorwerpID` = %i'
		                 , $veld
		          , $this->kartVoorwerpID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'KartVoorwerp');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
