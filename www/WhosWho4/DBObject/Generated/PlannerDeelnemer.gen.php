<?
abstract class PlannerDeelnemer_Generated
	extends Entiteit
{
	protected $plannerData;				/**< \brief PRIMARY */
	protected $plannerData_plannerDataID;/**< \brief PRIMARY */
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $antwoord;				/**< \brief ENUM:ONBEKEND/LIEVER_NIET/NEE/JA */
	/**
	/**
	 * @brief De constructor van de PlannerDeelnemer_Generated-klasse.
	 *
	 * @param mixed $a PlannerData (PlannerData OR Array(plannerData_plannerDataID) OR
	 * plannerData_plannerDataID)
	 * @param mixed $b Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof PlannerData)
		{
			$this->plannerData = $a;
			$this->plannerData_plannerDataID = $a->getPlannerDataID();
		}
		else if(is_array($a))
		{
			$this->plannerData = NULL;
			$this->plannerData_plannerDataID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->plannerData = NULL;
			$this->plannerData_plannerDataID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->persoon = $b;
			$this->persoon_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->antwoord = 'ONBEKEND';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'PlannerData';
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld plannerData.
	 *
	 * @return PlannerData
	 * De waarde van het veld plannerData.
	 */
	public function getPlannerData()
	{
		if(!isset($this->plannerData)
		 && isset($this->plannerData_plannerDataID)
		 ) {
			$this->plannerData = PlannerData::geef
					( $this->plannerData_plannerDataID
					);
		}
		return $this->plannerData;
	}
	/**
	 * @brief Geef de waarde van het veld plannerData_plannerDataID.
	 *
	 * @return int
	 * De waarde van het veld plannerData_plannerDataID.
	 */
	public function getPlannerDataPlannerDataID()
	{
		if (is_null($this->plannerData_plannerDataID) && isset($this->plannerData)) {
			$this->plannerData_plannerDataID = $this->plannerData->getPlannerDataID();
		}
		return $this->plannerData_plannerDataID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld antwoord.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld antwoord.
	 */
	static public function enumsAntwoord()
	{
		static $vals = array('ONBEKEND','LIEVER_NIET','NEE','JA');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld antwoord.
	 *
	 * @return string
	 * De waarde van het veld antwoord.
	 */
	public function getAntwoord()
	{
		return $this->antwoord;
	}
	/**
	 * @brief Stel de waarde van het veld antwoord in.
	 *
	 * @param mixed $newAntwoord De nieuwe waarde.
	 *
	 * @return PlannerDeelnemer
	 * Dit PlannerDeelnemer-object.
	 */
	public function setAntwoord($newAntwoord)
	{
		unset($this->errors['Antwoord']);
		if(!is_null($newAntwoord))
			$newAntwoord = strtoupper(trim($newAntwoord));
		if($newAntwoord === "")
			$newAntwoord = NULL;
		if($this->antwoord === $newAntwoord)
			return $this;

		$this->antwoord = $newAntwoord;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld antwoord geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld antwoord geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAntwoord()
	{
		if (array_key_exists('Antwoord', $this->errors))
			return $this->errors['Antwoord'];
		$waarde = $this->getAntwoord();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsAntwoord()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return PlannerDeelnemer::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van PlannerDeelnemer.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return PlannerDeelnemer::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPlannerDataPlannerDataID()
		                      , $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return PlannerDeelnemer|false
	 * Een PlannerDeelnemer-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$plannerData_plannerDataID = (int)$a[0];
			$persoon_contactID = (int)$a[1];
		}
		else if($a instanceof PlannerData
		     && $b instanceof Persoon)
		{
			$plannerData_plannerDataID = $a->getPlannerDataID();
			$persoon_contactID = $b->getContactID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$plannerData_plannerDataID = (int)$a;
			$persoon_contactID = (int)$b;
		}

		if(is_null($plannerData_plannerDataID)
		|| is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($plannerData_plannerDataID, $persoon_contactID) ));
		return Entiteit::geefCache(array($plannerData_plannerDataID, $persoon_contactID), 'PlannerDeelnemer');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'PlannerDeelnemer');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('PlannerDeelnemer::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `PlannerDeelnemer`.`plannerData_plannerDataID`'
		                 .     ', `PlannerDeelnemer`.`persoon_contactID`'
		                 .     ', `PlannerDeelnemer`.`antwoord`'
		                 .     ', `PlannerDeelnemer`.`gewijzigdWanneer`'
		                 .     ', `PlannerDeelnemer`.`gewijzigdWie`'
		                 .' FROM `PlannerDeelnemer`'
		                 .' WHERE (`plannerData_plannerDataID`, `persoon_contactID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['plannerData_plannerDataID']
			                  ,$row['persoon_contactID']);

			$obj = new PlannerDeelnemer(array($row['plannerData_plannerDataID']), array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->antwoord  = strtoupper(trim($row['antwoord']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'PlannerDeelnemer')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPlannerData();
		if(!$rel->getInDB())
			throw new LogicException('foreign PlannerData is not in DB');
		$this->plannerData_plannerDataID = $rel->getPlannerDataID();

		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `PlannerDeelnemer`'
			          . ' (`plannerData_plannerDataID`, `persoon_contactID`, `antwoord`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i)'
			          , $this->plannerData_plannerDataID
			          , $this->persoon_contactID
			          , $this->antwoord
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'PlannerDeelnemer')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `PlannerDeelnemer`'
			          .' SET `antwoord` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `plannerData_plannerDataID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->antwoord
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->plannerData_plannerDataID
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPlannerDeelnemer
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPlannerDeelnemer($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPlannerDeelnemer($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Antwoord';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Antwoord';
			break;
		case 'get':
			$velden[] = 'Antwoord';
		case 'primary':
			$velden[] = 'plannerData_plannerDataID';
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Antwoord';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `PlannerDeelnemer`'
		          .' WHERE `plannerData_plannerDataID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->plannerData_plannerDataID
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->plannerData = NULL;
		$this->plannerData_plannerDataID = NULL;
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `PlannerDeelnemer`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `plannerData_plannerDataID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->plannerData_plannerDataID
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van PlannerDeelnemer terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'plannerdata':
			return 'foreign';
		case 'plannerdata_plannerdataid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'antwoord':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'plannerData_plannerDataID':
		case 'persoon_contactID':
			$type = '%i';
			break;
		case 'antwoord':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `PlannerDeelnemer`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `plannerData_plannerDataID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->plannerData_plannerDataID
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `PlannerDeelnemer`'
		          .' WHERE `plannerData_plannerDataID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		                 , $veld
		          , $this->plannerData_plannerDataID
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'PlannerDeelnemer');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
