<?
abstract class PapierMolen_Generated
	extends Entiteit
{
	protected $papierMolenID;			/**< \brief PRIMARY */
	protected $geplaatst;
	protected $lid;
	protected $lid_contactID;			/**< \brief PRIMARY */
	protected $ean;
	protected $auteur;					/**< \brief NULL */
	protected $titel;					/**< \brief NULL */
	protected $druk;					/**< \brief NULL */
	protected $prijs;
	protected $opmerking;				/**< \brief NULL */
	protected $studie;					/**< \brief ENUM:wi/na/ic/gt/ik */
	protected $verloopdatum;
	/**
	/**
	 * @brief De constructor van de PapierMolen_Generated-klasse.
	 *
	 * @param mixed $a Geplaatst (datetime)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(!$a instanceof DateTimeLocale)
		{
			if(is_null($a))
				$a = new DateTimeLocale();
			else
			{
				try {
					$a = new DateTimeLocale($a);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->geplaatst = $a;

		$this->papierMolenID = NULL;
		$this->lid = NULL;
		$this->lid_contactID = 0;
		$this->ean = 0;
		$this->auteur = NULL;
		$this->titel = NULL;
		$this->druk = NULL;
		$this->prijs = 0;
		$this->opmerking = NULL;
		$this->studie = 'WI';
		$this->verloopdatum = new DateTimeLocale();
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Geplaatst';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld papierMolenID.
	 *
	 * @return int
	 * De waarde van het veld papierMolenID.
	 */
	public function getPapierMolenID()
	{
		return $this->papierMolenID;
	}
	/**
	 * @brief Geef de waarde van het veld geplaatst.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld geplaatst.
	 */
	public function getGeplaatst()
	{
		return $this->geplaatst;
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @return Lid
	 * De waarde van het veld lid.
	 */
	public function getLid()
	{
		if(!isset($this->lid)
		 && isset($this->lid_contactID)
		 ) {
			$this->lid = Lid::geef
					( $this->lid_contactID
					);
		}
		return $this->lid;
	}
	/**
	 * @brief Stel de waarde van het veld lid in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setLid($new_contactID)
	{
		unset($this->errors['Lid']);
		if($new_contactID instanceof Lid
		) {
			if($this->lid == $new_contactID
			&& $this->lid_contactID == $this->lid->getContactID())
				return $this;
			$this->lid = $new_contactID;
			$this->lid_contactID
					= $this->lid->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->lid == NULL 
				&& $this->lid_contactID == (int)$new_contactID)
				return $this;
			$this->lid = NULL;
			$this->lid_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lid geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lid geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLid()
	{
		if (array_key_exists('Lid', $this->errors))
			return $this->errors['Lid'];
		$waarde1 = $this->getLid();
		$waarde2 = $this->getLidContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld lid_contactID.
	 *
	 * @return int
	 * De waarde van het veld lid_contactID.
	 */
	public function getLidContactID()
	{
		if (is_null($this->lid_contactID) && isset($this->lid)) {
			$this->lid_contactID = $this->lid->getContactID();
		}
		return $this->lid_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld ean.
	 *
	 * @return int
	 * De waarde van het veld ean.
	 */
	public function getEan()
	{
		return $this->ean;
	}
	/**
	 * @brief Stel de waarde van het veld ean in.
	 *
	 * @param mixed $newEan De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setEan($newEan)
	{
		unset($this->errors['Ean']);
		if(!is_null($newEan))
			$newEan = (int)$newEan;
		if($this->ean === $newEan)
			return $this;

		$this->ean = $newEan;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ean geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ean geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkEan()
	{
		if (array_key_exists('Ean', $this->errors))
			return $this->errors['Ean'];
		$waarde = $this->getEan();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld auteur.
	 *
	 * @return string
	 * De waarde van het veld auteur.
	 */
	public function getAuteur()
	{
		return $this->auteur;
	}
	/**
	 * @brief Stel de waarde van het veld auteur in.
	 *
	 * @param mixed $newAuteur De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setAuteur($newAuteur)
	{
		unset($this->errors['Auteur']);
		if(!is_null($newAuteur))
			$newAuteur = trim($newAuteur);
		if($newAuteur === "")
			$newAuteur = NULL;
		if($this->auteur === $newAuteur)
			return $this;

		$this->auteur = $newAuteur;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld auteur geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld auteur geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAuteur()
	{
		if (array_key_exists('Auteur', $this->errors))
			return $this->errors['Auteur'];
		$waarde = $this->getAuteur();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setTitel($newTitel)
	{
		unset($this->errors['Titel']);
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel === $newTitel)
			return $this;

		$this->titel = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel()
	{
		if (array_key_exists('Titel', $this->errors))
			return $this->errors['Titel'];
		$waarde = $this->getTitel();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld druk.
	 *
	 * @return string
	 * De waarde van het veld druk.
	 */
	public function getDruk()
	{
		return $this->druk;
	}
	/**
	 * @brief Stel de waarde van het veld druk in.
	 *
	 * @param mixed $newDruk De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setDruk($newDruk)
	{
		unset($this->errors['Druk']);
		if(!is_null($newDruk))
			$newDruk = trim($newDruk);
		if($newDruk === "")
			$newDruk = NULL;
		if($this->druk === $newDruk)
			return $this;

		$this->druk = $newDruk;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld druk geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld druk geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkDruk()
	{
		if (array_key_exists('Druk', $this->errors))
			return $this->errors['Druk'];
		$waarde = $this->getDruk();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld prijs.
	 *
	 * @return float
	 * De waarde van het veld prijs.
	 */
	public function getPrijs()
	{
		return $this->prijs;
	}
	/**
	 * @brief Stel de waarde van het veld prijs in.
	 *
	 * @param mixed $newPrijs De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setPrijs($newPrijs)
	{
		unset($this->errors['Prijs']);
		if(!is_null($newPrijs))
			$newPrijs = (float)$newPrijs;
		if($this->prijs === $newPrijs)
			return $this;

		$this->prijs = $newPrijs;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld prijs geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld prijs geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkPrijs()
	{
		if (array_key_exists('Prijs', $this->errors))
			return $this->errors['Prijs'];
		$waarde = $this->getPrijs();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld studie.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld studie.
	 */
	static public function enumsStudie()
	{
		static $vals = array('WI','NA','IC','GT','IK');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld studie.
	 *
	 * @return string
	 * De waarde van het veld studie.
	 */
	public function getStudie()
	{
		return $this->studie;
	}
	/**
	 * @brief Stel de waarde van het veld studie in.
	 *
	 * @param mixed $newStudie De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setStudie($newStudie)
	{
		unset($this->errors['Studie']);
		if(!is_null($newStudie))
			$newStudie = strtoupper(trim($newStudie));
		if($newStudie === "")
			$newStudie = NULL;
		if($this->studie === $newStudie)
			return $this;

		$this->studie = $newStudie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld studie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld studie geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStudie()
	{
		if (array_key_exists('Studie', $this->errors))
			return $this->errors['Studie'];
		$waarde = $this->getStudie();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStudie()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld verloopdatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld verloopdatum.
	 */
	public function getVerloopdatum()
	{
		return $this->verloopdatum;
	}
	/**
	 * @brief Stel de waarde van het veld verloopdatum in.
	 *
	 * @param mixed $newVerloopdatum De nieuwe waarde.
	 *
	 * @return PapierMolen
	 * Dit PapierMolen-object.
	 */
	public function setVerloopdatum($newVerloopdatum)
	{
		unset($this->errors['Verloopdatum']);
		if(!$newVerloopdatum instanceof DateTimeLocale) {
			try {
				$newVerloopdatum = new DateTimeLocale($newVerloopdatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->verloopdatum->strftime('%F %T') == $newVerloopdatum->strftime('%F %T'))
			return $this;

		$this->verloopdatum = $newVerloopdatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld verloopdatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld verloopdatum geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVerloopdatum()
	{
		if (array_key_exists('Verloopdatum', $this->errors))
			return $this->errors['Verloopdatum'];
		$waarde = $this->getVerloopdatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return PapierMolen::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van PapierMolen.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return PapierMolen::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getPapierMolenID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return PapierMolen|false
	 * Een PapierMolen-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$papierMolenID = (int)$a[0];
		}
		else if(isset($a))
		{
			$papierMolenID = (int)$a;
		}

		if(is_null($papierMolenID))
			throw new BadMethodCallException();

		static::cache(array( array($papierMolenID) ));
		return Entiteit::geefCache(array($papierMolenID), 'PapierMolen');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'PapierMolen');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('PapierMolen::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `PapierMolen`.`papierMolenID`'
		                 .     ', `PapierMolen`.`geplaatst`'
		                 .     ', `PapierMolen`.`lid_contactID`'
		                 .     ', `PapierMolen`.`ean`'
		                 .     ', `PapierMolen`.`auteur`'
		                 .     ', `PapierMolen`.`titel`'
		                 .     ', `PapierMolen`.`druk`'
		                 .     ', `PapierMolen`.`prijs`'
		                 .     ', `PapierMolen`.`opmerking`'
		                 .     ', `PapierMolen`.`studie`'
		                 .     ', `PapierMolen`.`verloopdatum`'
		                 .     ', `PapierMolen`.`gewijzigdWanneer`'
		                 .     ', `PapierMolen`.`gewijzigdWie`'
		                 .' FROM `PapierMolen`'
		                 .' WHERE (`papierMolenID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['papierMolenID']);

			$obj = new PapierMolen($row['geplaatst']);

			$obj->inDB = True;

			$obj->papierMolenID  = (int) $row['papierMolenID'];
			$obj->lid_contactID  = (int) $row['lid_contactID'];
			$obj->ean  = (int) $row['ean'];
			$obj->auteur  = (is_null($row['auteur'])) ? null : trim($row['auteur']);
			$obj->titel  = (is_null($row['titel'])) ? null : trim($row['titel']);
			$obj->druk  = (is_null($row['druk'])) ? null : trim($row['druk']);
			$obj->prijs  = (float) $row['prijs'];
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->studie  = strtoupper(trim($row['studie']));
			$obj->verloopdatum  = new DateTimeLocale($row['verloopdatum']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'PapierMolen')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getLidContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->papierMolenID =
			$WSW4DB->q('RETURNID INSERT INTO `PapierMolen`'
			          . ' (`geplaatst`, `lid_contactID`, `ean`, `auteur`, `titel`, `druk`, `prijs`, `opmerking`, `studie`, `verloopdatum`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %i, %s, %s, %s, %f, %s, %s, %s, %s, %i)'
			          , $this->geplaatst->strftime('%F %T')
			          , $this->lid_contactID
			          , $this->ean
			          , $this->auteur
			          , $this->titel
			          , $this->druk
			          , $this->prijs
			          , $this->opmerking
			          , $this->studie
			          , $this->verloopdatum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'PapierMolen')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `PapierMolen`'
			          .' SET `geplaatst` = %s'
			          .   ', `lid_contactID` = %i'
			          .   ', `ean` = %i'
			          .   ', `auteur` = %s'
			          .   ', `titel` = %s'
			          .   ', `druk` = %s'
			          .   ', `prijs` = %f'
			          .   ', `opmerking` = %s'
			          .   ', `studie` = %s'
			          .   ', `verloopdatum` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `papierMolenID` = %i'
			          , $this->geplaatst->strftime('%F %T')
			          , $this->lid_contactID
			          , $this->ean
			          , $this->auteur
			          , $this->titel
			          , $this->druk
			          , $this->prijs
			          , $this->opmerking
			          , $this->studie
			          , $this->verloopdatum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->papierMolenID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPapierMolen
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPapierMolen($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPapierMolen($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Lid';
			$velden[] = 'Ean';
			$velden[] = 'Auteur';
			$velden[] = 'Titel';
			$velden[] = 'Druk';
			$velden[] = 'Prijs';
			$velden[] = 'Opmerking';
			$velden[] = 'Studie';
			$velden[] = 'Verloopdatum';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Lid';
			$velden[] = 'Prijs';
			$velden[] = 'Verloopdatum';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Geplaatst';
			$velden[] = 'Lid';
			$velden[] = 'Ean';
			$velden[] = 'Auteur';
			$velden[] = 'Titel';
			$velden[] = 'Druk';
			$velden[] = 'Prijs';
			$velden[] = 'Opmerking';
			$velden[] = 'Studie';
			$velden[] = 'Verloopdatum';
			break;
		case 'get':
			$velden[] = 'Geplaatst';
			$velden[] = 'Lid';
			$velden[] = 'Ean';
			$velden[] = 'Auteur';
			$velden[] = 'Titel';
			$velden[] = 'Druk';
			$velden[] = 'Prijs';
			$velden[] = 'Opmerking';
			$velden[] = 'Studie';
			$velden[] = 'Verloopdatum';
		case 'primary':
			$velden[] = 'lid_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Geplaatst';
			$velden[] = 'Lid';
			$velden[] = 'Ean';
			$velden[] = 'Auteur';
			$velden[] = 'Titel';
			$velden[] = 'Druk';
			$velden[] = 'Prijs';
			$velden[] = 'Opmerking';
			$velden[] = 'Studie';
			$velden[] = 'Verloopdatum';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `PapierMolen`'
		          .' WHERE `papierMolenID` = %i'
		          .' LIMIT 1'
		          , $this->papierMolenID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->papierMolenID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `PapierMolen`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `papierMolenID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->papierMolenID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van PapierMolen terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'papiermolenid':
			return 'int';
		case 'geplaatst':
			return 'datetime';
		case 'lid':
			return 'foreign';
		case 'lid_contactid':
			return 'int';
		case 'ean':
			return 'bigint';
		case 'auteur':
			return 'string';
		case 'titel':
			return 'string';
		case 'druk':
			return 'string';
		case 'prijs':
			return 'money';
		case 'opmerking':
			return 'string';
		case 'studie':
			return 'enum';
		case 'verloopdatum':
			return 'date';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'prijs':
			$type = '%f';
			break;
		case 'papierMolenID':
		case 'lid_contactID':
		case 'ean':
			$type = '%i';
			break;
		case 'geplaatst':
		case 'auteur':
		case 'titel':
		case 'druk':
		case 'opmerking':
		case 'studie':
		case 'verloopdatum':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `PapierMolen`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `papierMolenID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->papierMolenID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `PapierMolen`'
		          .' WHERE `papierMolenID` = %i'
		                 , $veld
		          , $this->papierMolenID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'PapierMolen');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
