<?
/**
 * @brief 'AT'AUTH_GET:bestuur,AUTH_SET:bestuur
 */
abstract class Persoon_Generated
	extends Contact
{
	protected $voornaam;				/**< \brief LEGACY_NULL */
	protected $bijnaam;					/**< \brief was: nickname NULL */
	protected $voorletters;				/**< \brief Soms zijn deze er simpelweg niet, dus TODO iets met "soft constraints" want bij sommige type personen weet je dat deze er wel altijd is. NULL */
	protected $geboortenamen;			/**< \brief NULL,LAZY */
	protected $tussenvoegsels;			/**< \brief NULL */
	protected $achternaam;
	protected $titelsPrefix;			/**< \brief NULL */
	protected $titelsPostfix;			/**< \brief NULL */
	protected $geslacht;				/**< \brief ENUM:M/V/?,NULL,LAZY */
	protected $datumGeboorte;			/**< \brief NULL,LAZY */
	protected $overleden;
	protected $GPGkey;					/**< \brief NULL,LAZY */
	protected $Rekeningnummer;			/**< \brief NULL,LAZY */
	protected $opmerkingen;				/**< \brief NULL,LAZY,SETBESTUUR */
	protected $voornaamwoord;
	protected $voornaamwoord_woordID;	/**< \brief PRIMARY */
	protected $voornaamwoordZichtbaar;
	/** Verzamelingen **/
	protected $commissieLidVerzameling;
	protected $deelnemerVerzameling;
	protected $introClusterVerzameling;
	protected $donateurVerzameling;
	protected $persoonIMVerzameling;
	protected $wachtwoordVerzameling;
	protected $plannerVerzameling;
	protected $plannerDeelnemerVerzameling;
	protected $contactPersoonVerzameling;
	protected $contractonderdeelVerzameling;
	protected $pasVerzameling;
	protected $dibsInfoVerzameling;
	protected $dictaatVerzameling;
	protected $jaargangVerzameling;
	protected $iOUBonVerzameling;
	protected $iOUBetalingVerzameling;
	protected $iOUSplitVerzameling;
	protected $mediaVerzameling;
	protected $tagVerzameling;
	protected $ratingVerzameling;
	protected $bugVerzameling;
	protected $bugBerichtVerzameling;
	protected $bugToewijzingVerzameling;
	protected $bugVolgerVerzameling;
	protected $persoonKartVerzameling;
	protected $kartVoorwerpVerzameling;
	protected $persoonVoorkeurVerzameling;
	protected $bestellingVerzameling;
	protected $profielFotoVerzameling;
	protected $persoonBadgeVerzameling;
	protected $tentamenUitwerkingVerzameling;
	protected $persoonRatingVerzameling;
	/**
	 * @brief De constructor van de Persoon_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Contact

		$this->contactID = NULL;
		$this->voornaam = '';
		$this->bijnaam = NULL;
		$this->voorletters = NULL;
		$this->geboortenamen = NULL;
		$this->tussenvoegsels = NULL;
		$this->achternaam = '';
		$this->titelsPrefix = NULL;
		$this->titelsPostfix = NULL;
		$this->geslacht = NULL;
		$this->datumGeboorte = new DateTimeLocale(NULL);
		$this->overleden = False;
		$this->GPGkey = NULL;
		$this->Rekeningnummer = NULL;
		$this->opmerkingen = NULL;
		$this->voornaamwoord = NULL;
		$this->voornaamwoord_woordID = 0;
		$this->voornaamwoordZichtbaar = True;
		$this->commissieLidVerzameling = NULL;
		$this->deelnemerVerzameling = NULL;
		$this->introClusterVerzameling = NULL;
		$this->donateurVerzameling = NULL;
		$this->persoonIMVerzameling = NULL;
		$this->wachtwoordVerzameling = NULL;
		$this->plannerVerzameling = NULL;
		$this->plannerDeelnemerVerzameling = NULL;
		$this->contactPersoonVerzameling = NULL;
		$this->contractonderdeelVerzameling = NULL;
		$this->pasVerzameling = NULL;
		$this->dibsInfoVerzameling = NULL;
		$this->dictaatVerzameling = NULL;
		$this->jaargangVerzameling = NULL;
		$this->iOUBonVerzameling = NULL;
		$this->iOUBetalingVerzameling = NULL;
		$this->iOUSplitVerzameling = NULL;
		$this->mediaVerzameling = NULL;
		$this->tagVerzameling = NULL;
		$this->ratingVerzameling = NULL;
		$this->bugVerzameling = NULL;
		$this->bugBerichtVerzameling = NULL;
		$this->bugToewijzingVerzameling = NULL;
		$this->bugVolgerVerzameling = NULL;
		$this->persoonKartVerzameling = NULL;
		$this->kartVoorwerpVerzameling = NULL;
		$this->persoonVoorkeurVerzameling = NULL;
		$this->bestellingVerzameling = NULL;
		$this->profielFotoVerzameling = NULL;
		$this->persoonBadgeVerzameling = NULL;
		$this->tentamenUitwerkingVerzameling = NULL;
		$this->persoonRatingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld voornaam.
	 *
	 * @return string
	 * De waarde van het veld voornaam.
	 */
	public function getVoornaam()
	{
		return $this->voornaam;
	}
	/**
	 * @brief Stel de waarde van het veld voornaam in.
	 *
	 * @param mixed $newVoornaam De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setVoornaam($newVoornaam)
	{
		unset($this->errors['Voornaam']);
		if(!is_null($newVoornaam))
			$newVoornaam = trim($newVoornaam);
		if($newVoornaam === "")
			$newVoornaam = NULL;
		if($this->voornaam === $newVoornaam)
			return $this;

		if(is_null($newVoornaam) && isset($this->voornaam))
			$this->errors['Voornaam'] = _('dit is een verplicht veld');

		$this->voornaam = $newVoornaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voornaam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voornaam geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkVoornaam()
	{
		if (array_key_exists('Voornaam', $this->errors))
			return $this->errors['Voornaam'];
		$waarde = $this->getVoornaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bijnaam.
	 *
	 * @return string
	 * De waarde van het veld bijnaam.
	 */
	public function getBijnaam()
	{
		return $this->bijnaam;
	}
	/**
	 * @brief Stel de waarde van het veld bijnaam in.
	 *
	 * @param mixed $newBijnaam De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setBijnaam($newBijnaam)
	{
		unset($this->errors['Bijnaam']);
		if(!is_null($newBijnaam))
			$newBijnaam = trim($newBijnaam);
		if($newBijnaam === "")
			$newBijnaam = NULL;
		if($this->bijnaam === $newBijnaam)
			return $this;

		$this->bijnaam = $newBijnaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bijnaam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bijnaam geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBijnaam()
	{
		if (array_key_exists('Bijnaam', $this->errors))
			return $this->errors['Bijnaam'];
		$waarde = $this->getBijnaam();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld voorletters.
	 *
	 * @return string
	 * De waarde van het veld voorletters.
	 */
	public function getVoorletters()
	{
		return $this->voorletters;
	}
	/**
	 * @brief Stel de waarde van het veld voorletters in.
	 *
	 * @param mixed $newVoorletters De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setVoorletters($newVoorletters)
	{
		unset($this->errors['Voorletters']);
		if(!is_null($newVoorletters))
			$newVoorletters = trim($newVoorletters);
		if($newVoorletters === "")
			$newVoorletters = NULL;
		if($this->voorletters === $newVoorletters)
			return $this;

		$this->voorletters = $newVoorletters;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voorletters geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voorletters geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVoorletters()
	{
		if (array_key_exists('Voorletters', $this->errors))
			return $this->errors['Voorletters'];
		$waarde = $this->getVoorletters();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld geboortenamen.
	 *
	 * @return string
	 * De waarde van het veld geboortenamen.
	 */
	public function getGeboortenamen()
	{
		return $this->geboortenamen;
	}
	/**
	 * @brief Stel de waarde van het veld geboortenamen in.
	 *
	 * @param mixed $newGeboortenamen De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setGeboortenamen($newGeboortenamen)
	{
		unset($this->errors['Geboortenamen']);
		if(!is_null($newGeboortenamen))
			$newGeboortenamen = trim($newGeboortenamen);
		if($newGeboortenamen === "")
			$newGeboortenamen = NULL;
		if($this->geboortenamen === $newGeboortenamen)
			return $this;

		$this->geboortenamen = $newGeboortenamen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld geboortenamen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld geboortenamen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkGeboortenamen()
	{
		if (array_key_exists('Geboortenamen', $this->errors))
			return $this->errors['Geboortenamen'];
		$waarde = $this->getGeboortenamen();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tussenvoegsels.
	 *
	 * @return string
	 * De waarde van het veld tussenvoegsels.
	 */
	public function getTussenvoegsels()
	{
		return $this->tussenvoegsels;
	}
	/**
	 * @brief Stel de waarde van het veld tussenvoegsels in.
	 *
	 * @param mixed $newTussenvoegsels De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setTussenvoegsels($newTussenvoegsels)
	{
		unset($this->errors['Tussenvoegsels']);
		if(!is_null($newTussenvoegsels))
			$newTussenvoegsels = trim($newTussenvoegsels);
		if($newTussenvoegsels === "")
			$newTussenvoegsels = NULL;
		if($this->tussenvoegsels === $newTussenvoegsels)
			return $this;

		$this->tussenvoegsels = $newTussenvoegsels;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tussenvoegsels geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tussenvoegsels geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTussenvoegsels()
	{
		if (array_key_exists('Tussenvoegsels', $this->errors))
			return $this->errors['Tussenvoegsels'];
		$waarde = $this->getTussenvoegsels();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld achternaam.
	 *
	 * @return string
	 * De waarde van het veld achternaam.
	 */
	public function getAchternaam()
	{
		return $this->achternaam;
	}
	/**
	 * @brief Stel de waarde van het veld achternaam in.
	 *
	 * @param mixed $newAchternaam De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setAchternaam($newAchternaam)
	{
		unset($this->errors['Achternaam']);
		if(!is_null($newAchternaam))
			$newAchternaam = trim($newAchternaam);
		if($newAchternaam === "")
			$newAchternaam = NULL;
		if($this->achternaam === $newAchternaam)
			return $this;

		$this->achternaam = $newAchternaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld achternaam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld achternaam geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAchternaam()
	{
		if (array_key_exists('Achternaam', $this->errors))
			return $this->errors['Achternaam'];
		$waarde = $this->getAchternaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titelsPrefix.
	 *
	 * @return string
	 * De waarde van het veld titelsPrefix.
	 */
	public function getTitelsPrefix()
	{
		return $this->titelsPrefix;
	}
	/**
	 * @brief Stel de waarde van het veld titelsPrefix in.
	 *
	 * @param mixed $newTitelsPrefix De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setTitelsPrefix($newTitelsPrefix)
	{
		unset($this->errors['TitelsPrefix']);
		if(!is_null($newTitelsPrefix))
			$newTitelsPrefix = trim($newTitelsPrefix);
		if($newTitelsPrefix === "")
			$newTitelsPrefix = NULL;
		if($this->titelsPrefix === $newTitelsPrefix)
			return $this;

		$this->titelsPrefix = $newTitelsPrefix;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titelsPrefix geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titelsPrefix geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTitelsPrefix()
	{
		if (array_key_exists('TitelsPrefix', $this->errors))
			return $this->errors['TitelsPrefix'];
		$waarde = $this->getTitelsPrefix();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titelsPostfix.
	 *
	 * @return string
	 * De waarde van het veld titelsPostfix.
	 */
	public function getTitelsPostfix()
	{
		return $this->titelsPostfix;
	}
	/**
	 * @brief Stel de waarde van het veld titelsPostfix in.
	 *
	 * @param mixed $newTitelsPostfix De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setTitelsPostfix($newTitelsPostfix)
	{
		unset($this->errors['TitelsPostfix']);
		if(!is_null($newTitelsPostfix))
			$newTitelsPostfix = trim($newTitelsPostfix);
		if($newTitelsPostfix === "")
			$newTitelsPostfix = NULL;
		if($this->titelsPostfix === $newTitelsPostfix)
			return $this;

		$this->titelsPostfix = $newTitelsPostfix;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titelsPostfix geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titelsPostfix geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTitelsPostfix()
	{
		if (array_key_exists('TitelsPostfix', $this->errors))
			return $this->errors['TitelsPostfix'];
		$waarde = $this->getTitelsPostfix();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld geslacht.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld geslacht.
	 */
	static public function enumsGeslacht()
	{
		static $vals = array('M','V','?','');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld geslacht.
	 *
	 * @return string
	 * De waarde van het veld geslacht.
	 */
	public function getGeslacht()
	{
		return $this->geslacht;
	}
	/**
	 * @brief Stel de waarde van het veld geslacht in.
	 *
	 * @param mixed $newGeslacht De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setGeslacht($newGeslacht)
	{
		unset($this->errors['Geslacht']);
		if(!is_null($newGeslacht))
			$newGeslacht = strtoupper(trim($newGeslacht));
		if($newGeslacht === "")
			$newGeslacht = NULL;
		if($this->geslacht === $newGeslacht)
			return $this;

		$this->geslacht = $newGeslacht;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld geslacht geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld geslacht geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkGeslacht()
	{
		if (array_key_exists('Geslacht', $this->errors))
			return $this->errors['Geslacht'];
		$waarde = $this->getGeslacht();
		if (is_null($waarde))
			return False;

		if(!in_array($waarde, static::enumsGeslacht()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumGeboorte.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumGeboorte.
	 */
	public function getDatumGeboorte()
	{
		return $this->datumGeboorte;
	}
	/**
	 * @brief Stel de waarde van het veld datumGeboorte in.
	 *
	 * @param mixed $newDatumGeboorte De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setDatumGeboorte($newDatumGeboorte)
	{
		unset($this->errors['DatumGeboorte']);
		if(!$newDatumGeboorte instanceof DateTimeLocale) {
			try {
				$newDatumGeboorte = new DateTimeLocale($newDatumGeboorte);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumGeboorte->strftime('%F %T') == $newDatumGeboorte->strftime('%F %T'))
			return $this;

		$this->datumGeboorte = $newDatumGeboorte;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumGeboorte geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumGeboorte geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDatumGeboorte()
	{
		if (array_key_exists('DatumGeboorte', $this->errors))
			return $this->errors['DatumGeboorte'];
		$waarde = $this->getDatumGeboorte();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld overleden.
	 *
	 * @return bool
	 * De waarde van het veld overleden.
	 */
	public function getOverleden()
	{
		return $this->overleden;
	}
	/**
	 * @brief Stel de waarde van het veld overleden in.
	 *
	 * @param mixed $newOverleden De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setOverleden($newOverleden)
	{
		unset($this->errors['Overleden']);
		if(!is_null($newOverleden))
			$newOverleden = (bool)$newOverleden;
		if($this->overleden === $newOverleden)
			return $this;

		$this->overleden = $newOverleden;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld overleden geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld overleden geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOverleden()
	{
		if (array_key_exists('Overleden', $this->errors))
			return $this->errors['Overleden'];
		$waarde = $this->getOverleden();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld GPGkey.
	 *
	 * @return string
	 * De waarde van het veld GPGkey.
	 */
	public function getGPGkey()
	{
		return $this->GPGkey;
	}
	/**
	 * @brief Stel de waarde van het veld GPGkey in.
	 *
	 * @param mixed $newGPGkey De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setGPGkey($newGPGkey)
	{
		unset($this->errors['GPGkey']);
		if(!is_null($newGPGkey))
			$newGPGkey = trim($newGPGkey);
		if($newGPGkey === "")
			$newGPGkey = NULL;
		if($this->GPGkey === $newGPGkey)
			return $this;

		$this->GPGkey = $newGPGkey;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld GPGkey geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld GPGkey geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkGPGkey()
	{
		if (array_key_exists('GPGkey', $this->errors))
			return $this->errors['GPGkey'];
		$waarde = $this->getGPGkey();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld Rekeningnummer.
	 *
	 * @return string
	 * De waarde van het veld Rekeningnummer.
	 */
	public function getRekeningnummer()
	{
		return $this->Rekeningnummer;
	}
	/**
	 * @brief Stel de waarde van het veld Rekeningnummer in.
	 *
	 * @param mixed $newRekeningnummer De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setRekeningnummer($newRekeningnummer)
	{
		unset($this->errors['Rekeningnummer']);
		if(!is_null($newRekeningnummer))
			$newRekeningnummer = trim($newRekeningnummer);
		if($newRekeningnummer === "")
			$newRekeningnummer = NULL;
		if($this->Rekeningnummer === $newRekeningnummer)
			return $this;

		$this->Rekeningnummer = $newRekeningnummer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld Rekeningnummer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld Rekeningnummer geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkRekeningnummer()
	{
		if (array_key_exists('Rekeningnummer', $this->errors))
			return $this->errors['Rekeningnummer'];
		$waarde = $this->getRekeningnummer();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerkingen.
	 *
	 * @return string
	 * De waarde van het veld opmerkingen.
	 */
	public function getOpmerkingen()
	{
		return $this->opmerkingen;
	}
	/**
	 * @brief Stel de waarde van het veld opmerkingen in.
	 *
	 * @param mixed $newOpmerkingen De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setOpmerkingen($newOpmerkingen)
	{
		unset($this->errors['Opmerkingen']);
		if (!hasAuth('bestuur')) {
			trigger_error('Alleen >= bestuur mag dit wijzigen!', E_USER_WARNING);
			return $this;
		}
		if(!is_null($newOpmerkingen))
			$newOpmerkingen = trim($newOpmerkingen);
		if($newOpmerkingen === "")
			$newOpmerkingen = NULL;
		if($this->opmerkingen === $newOpmerkingen)
			return $this;

		$this->opmerkingen = $newOpmerkingen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerkingen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerkingen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOpmerkingen()
	{
		if (array_key_exists('Opmerkingen', $this->errors))
			return $this->errors['Opmerkingen'];
		$waarde = $this->getOpmerkingen();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		use_error('Implementeer mij', E_USER_ERROR);
	}
	/**
	 * @brief Geef de waarde van het veld voornaamwoord.
	 *
	 * @return Voornaamwoord
	 * De waarde van het veld voornaamwoord.
	 */
	public function getVoornaamwoord()
	{
		if(!isset($this->voornaamwoord)
		 && isset($this->voornaamwoord_woordID)
		 ) {
			$this->voornaamwoord = Voornaamwoord::geef
					( $this->voornaamwoord_woordID
					);
		}
		return $this->voornaamwoord;
	}
	/**
	 * @brief Stel de waarde van het veld voornaamwoord in.
	 *
	 * @param mixed $new_woordID De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setVoornaamwoord($new_woordID)
	{
		unset($this->errors['Voornaamwoord']);
		if($new_woordID instanceof Voornaamwoord
		) {
			if($this->voornaamwoord == $new_woordID
			&& $this->voornaamwoord_woordID == $this->voornaamwoord->getWoordID())
				return $this;
			$this->voornaamwoord = $new_woordID;
			$this->voornaamwoord_woordID
					= $this->voornaamwoord->getWoordID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_woordID)
		) {
			if($this->voornaamwoord == NULL 
				&& $this->voornaamwoord_woordID == (int)$new_woordID)
				return $this;
			$this->voornaamwoord = NULL;
			$this->voornaamwoord_woordID
					= (int)$new_woordID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voornaamwoord geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voornaamwoord geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVoornaamwoord()
	{
		if (array_key_exists('Voornaamwoord', $this->errors))
			return $this->errors['Voornaamwoord'];
		$waarde1 = $this->getVoornaamwoord();
		$waarde2 = $this->getVoornaamwoordWoordID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld voornaamwoord_woordID.
	 *
	 * @return int
	 * De waarde van het veld voornaamwoord_woordID.
	 */
	public function getVoornaamwoordWoordID()
	{
		if (is_null($this->voornaamwoord_woordID) && isset($this->voornaamwoord)) {
			$this->voornaamwoord_woordID = $this->voornaamwoord->getWoordID();
		}
		return $this->voornaamwoord_woordID;
	}
	/**
	 * @brief Geef de waarde van het veld voornaamwoordZichtbaar.
	 *
	 * @return bool
	 * De waarde van het veld voornaamwoordZichtbaar.
	 */
	public function getVoornaamwoordZichtbaar()
	{
		return $this->voornaamwoordZichtbaar;
	}
	/**
	 * @brief Stel de waarde van het veld voornaamwoordZichtbaar in.
	 *
	 * @param mixed $newVoornaamwoordZichtbaar De nieuwe waarde.
	 *
	 * @return Persoon
	 * Dit Persoon-object.
	 */
	public function setVoornaamwoordZichtbaar($newVoornaamwoordZichtbaar)
	{
		unset($this->errors['VoornaamwoordZichtbaar']);
		if(!is_null($newVoornaamwoordZichtbaar))
			$newVoornaamwoordZichtbaar = (bool)$newVoornaamwoordZichtbaar;
		if($this->voornaamwoordZichtbaar === $newVoornaamwoordZichtbaar)
			return $this;

		$this->voornaamwoordZichtbaar = $newVoornaamwoordZichtbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voornaamwoordZichtbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voornaamwoordZichtbaar geldig is;
	 * anders een string met een foutmelding.
	 */
	public function checkVoornaamwoordZichtbaar()
	{
		if (array_key_exists('VoornaamwoordZichtbaar', $this->errors))
			return $this->errors['VoornaamwoordZichtbaar'];
		$waarde = $this->getVoornaamwoordZichtbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de CommissieLidVerzameling die hoort bij dit object.
	 */
	public function getCommissieLidVerzameling()
	{
		if(!$this->commissieLidVerzameling instanceof CommissieLidVerzameling)
			$this->commissieLidVerzameling = CommissieLidVerzameling::fromPersoon($this);
		return $this->commissieLidVerzameling;
	}
	/**
	 * @brief Returneert de DeelnemerVerzameling die hoort bij dit object.
	 */
	public function getDeelnemerVerzameling()
	{
		if(!$this->deelnemerVerzameling instanceof DeelnemerVerzameling)
			$this->deelnemerVerzameling = DeelnemerVerzameling::fromPersoon($this);
		return $this->deelnemerVerzameling;
	}
	/**
	 * @brief Returneert de IntroClusterVerzameling die hoort bij dit object.
	 */
	public function getIntroClusterVerzameling()
	{
		if(!$this->introClusterVerzameling instanceof IntroClusterVerzameling)
			$this->introClusterVerzameling = IntroClusterVerzameling::fromPersoon($this);
		return $this->introClusterVerzameling;
	}
	/**
	 * @brief Returneert de DonateurVerzameling die hoort bij dit object.
	 */
	public function getDonateurVerzameling()
	{
		if(!$this->donateurVerzameling instanceof DonateurVerzameling)
		{
			$this->donateurVerzameling = new DonateurVerzameling();
			$this->donateurVerzameling->union(DonateurVerzameling::fromPersoon($this));
			$this->donateurVerzameling->union(DonateurVerzameling::fromAnderPersoon($this));
		}
		return $this->donateurVerzameling;
	}
	/**
	 * @brief Returneert de PersoonIMVerzameling die hoort bij dit object.
	 */
	public function getPersoonIMVerzameling()
	{
		if(!$this->persoonIMVerzameling instanceof PersoonIMVerzameling)
			$this->persoonIMVerzameling = PersoonIMVerzameling::fromPersoon($this);
		return $this->persoonIMVerzameling;
	}
	/**
	 * @brief Returneert de WachtwoordVerzameling die hoort bij dit object.
	 */
	public function getWachtwoordVerzameling()
	{
		if(!$this->wachtwoordVerzameling instanceof WachtwoordVerzameling)
			$this->wachtwoordVerzameling = WachtwoordVerzameling::fromPersoon($this);
		return $this->wachtwoordVerzameling;
	}
	/**
	 * @brief Returneert de PlannerVerzameling die hoort bij dit object.
	 */
	public function getPlannerVerzameling()
	{
		if(!$this->plannerVerzameling instanceof PlannerVerzameling)
			$this->plannerVerzameling = PlannerVerzameling::fromPersoon($this);
		return $this->plannerVerzameling;
	}
	/**
	 * @brief Returneert de PlannerDeelnemerVerzameling die hoort bij dit object.
	 */
	public function getPlannerDeelnemerVerzameling()
	{
		if(!$this->plannerDeelnemerVerzameling instanceof PlannerDeelnemerVerzameling)
			$this->plannerDeelnemerVerzameling = PlannerDeelnemerVerzameling::fromPersoon($this);
		return $this->plannerDeelnemerVerzameling;
	}
	/**
	 * @brief Returneert de ContactPersoonVerzameling die hoort bij dit object.
	 */
	public function getContactPersoonVerzameling()
	{
		if(!$this->contactPersoonVerzameling instanceof ContactPersoonVerzameling)
			$this->contactPersoonVerzameling = ContactPersoonVerzameling::fromPersoon($this);
		return $this->contactPersoonVerzameling;
	}
	/**
	 * @brief Returneert de ContractonderdeelVerzameling die hoort bij dit object.
	 */
	public function getContractonderdeelVerzameling()
	{
		if(!$this->contractonderdeelVerzameling instanceof ContractonderdeelVerzameling)
			$this->contractonderdeelVerzameling = ContractonderdeelVerzameling::fromVerantwoordelijke($this);
		return $this->contractonderdeelVerzameling;
	}
	/**
	 * @brief Returneert de PasVerzameling die hoort bij dit object.
	 */
	public function getPasVerzameling()
	{
		if(!$this->pasVerzameling instanceof PasVerzameling)
			$this->pasVerzameling = PasVerzameling::fromPersoon($this);
		return $this->pasVerzameling;
	}
	/**
	 * @brief Returneert de DibsInfoVerzameling die hoort bij dit object.
	 */
	public function getDibsInfoVerzameling()
	{
		if(!$this->dibsInfoVerzameling instanceof DibsInfoVerzameling)
			$this->dibsInfoVerzameling = DibsInfoVerzameling::fromPersoon($this);
		return $this->dibsInfoVerzameling;
	}
	/**
	 * @brief Returneert de DictaatVerzameling die hoort bij dit object.
	 */
	public function getDictaatVerzameling()
	{
		if(!$this->dictaatVerzameling instanceof DictaatVerzameling)
			$this->dictaatVerzameling = DictaatVerzameling::fromAuteur($this);
		return $this->dictaatVerzameling;
	}
	/**
	 * @brief Returneert de JaargangVerzameling die hoort bij dit object.
	 */
	public function getJaargangVerzameling()
	{
		if(!$this->jaargangVerzameling instanceof JaargangVerzameling)
			$this->jaargangVerzameling = JaargangVerzameling::fromContactPersoon($this);
		return $this->jaargangVerzameling;
	}
	/**
	 * @brief Returneert de IOUBonVerzameling die hoort bij dit object.
	 */
	public function getIOUBonVerzameling()
	{
		if(!$this->iOUBonVerzameling instanceof IOUBonVerzameling)
			$this->iOUBonVerzameling = IOUBonVerzameling::fromPersoon($this);
		return $this->iOUBonVerzameling;
	}
	/**
	 * @brief Returneert de IOUBetalingVerzameling die hoort bij dit object.
	 */
	public function getIOUBetalingVerzameling()
	{
		if(!$this->iOUBetalingVerzameling instanceof IOUBetalingVerzameling)
		{
			$this->iOUBetalingVerzameling = new IOUBetalingVerzameling();
			$this->iOUBetalingVerzameling->union(IOUBetalingVerzameling::fromVan($this));
			$this->iOUBetalingVerzameling->union(IOUBetalingVerzameling::fromNaar($this));
		}
		return $this->iOUBetalingVerzameling;
	}
	/**
	 * @brief Returneert de IOUSplitVerzameling die hoort bij dit object.
	 */
	public function getIOUSplitVerzameling()
	{
		if(!$this->iOUSplitVerzameling instanceof IOUSplitVerzameling)
			$this->iOUSplitVerzameling = IOUSplitVerzameling::fromPersoon($this);
		return $this->iOUSplitVerzameling;
	}
	/**
	 * @brief Returneert de MediaVerzameling die hoort bij dit object.
	 */
	public function getMediaVerzameling()
	{
		if(!$this->mediaVerzameling instanceof MediaVerzameling)
		{
			$this->mediaVerzameling = new MediaVerzameling();
			$this->mediaVerzameling->union(MediaVerzameling::fromUploader($this));
			$this->mediaVerzameling->union(MediaVerzameling::fromFotograaf($this));
		}
		return $this->mediaVerzameling;
	}
	/**
	 * @brief Returneert de TagVerzameling die hoort bij dit object.
	 */
	public function getTagVerzameling()
	{
		if(!$this->tagVerzameling instanceof TagVerzameling)
		{
			$this->tagVerzameling = new TagVerzameling();
			$this->tagVerzameling->union(TagVerzameling::fromPersoon($this));
			$this->tagVerzameling->union(TagVerzameling::fromTagger($this));
		}
		return $this->tagVerzameling;
	}
	/**
	 * @brief Returneert de RatingVerzameling die hoort bij dit object.
	 */
	public function getRatingVerzameling()
	{
		if(!$this->ratingVerzameling instanceof RatingVerzameling)
			$this->ratingVerzameling = RatingVerzameling::fromPersoon($this);
		return $this->ratingVerzameling;
	}
	/**
	 * @brief Returneert de BugVerzameling die hoort bij dit object.
	 */
	public function getBugVerzameling()
	{
		if(!$this->bugVerzameling instanceof BugVerzameling)
			$this->bugVerzameling = BugVerzameling::fromMelder($this);
		return $this->bugVerzameling;
	}
	/**
	 * @brief Returneert de BugBerichtVerzameling die hoort bij dit object.
	 */
	public function getBugBerichtVerzameling()
	{
		if(!$this->bugBerichtVerzameling instanceof BugBerichtVerzameling)
			$this->bugBerichtVerzameling = BugBerichtVerzameling::fromMelder($this);
		return $this->bugBerichtVerzameling;
	}
	/**
	 * @brief Returneert de BugToewijzingVerzameling die hoort bij dit object.
	 */
	public function getBugToewijzingVerzameling()
	{
		if(!$this->bugToewijzingVerzameling instanceof BugToewijzingVerzameling)
			$this->bugToewijzingVerzameling = BugToewijzingVerzameling::fromPersoon($this);
		return $this->bugToewijzingVerzameling;
	}
	/**
	 * @brief Returneert de BugVolgerVerzameling die hoort bij dit object.
	 */
	public function getBugVolgerVerzameling()
	{
		if(!$this->bugVolgerVerzameling instanceof BugVolgerVerzameling)
			$this->bugVolgerVerzameling = BugVolgerVerzameling::fromPersoon($this);
		return $this->bugVolgerVerzameling;
	}
	/**
	 * @brief Returneert de PersoonKartVerzameling die hoort bij dit object.
	 */
	public function getPersoonKartVerzameling()
	{
		if(!$this->persoonKartVerzameling instanceof PersoonKartVerzameling)
			$this->persoonKartVerzameling = PersoonKartVerzameling::fromPersoon($this);
		return $this->persoonKartVerzameling;
	}
	/**
	 * @brief Returneert de KartVoorwerpVerzameling die hoort bij dit object.
	 */
	public function getKartVoorwerpVerzameling()
	{
		if(!$this->kartVoorwerpVerzameling instanceof KartVoorwerpVerzameling)
			$this->kartVoorwerpVerzameling = KartVoorwerpVerzameling::fromPersoon($this);
		return $this->kartVoorwerpVerzameling;
	}
	/**
	 * @brief Returneert de PersoonVoorkeurVerzameling die hoort bij dit object.
	 */
	public function getPersoonVoorkeurVerzameling()
	{
		if(!$this->persoonVoorkeurVerzameling instanceof PersoonVoorkeurVerzameling)
			$this->persoonVoorkeurVerzameling = PersoonVoorkeurVerzameling::fromPersoon($this);
		return $this->persoonVoorkeurVerzameling;
	}
	/**
	 * @brief Returneert de BestellingVerzameling die hoort bij dit object.
	 */
	public function getBestellingVerzameling()
	{
		if(!$this->bestellingVerzameling instanceof BestellingVerzameling)
			$this->bestellingVerzameling = BestellingVerzameling::fromPersoon($this);
		return $this->bestellingVerzameling;
	}
	/**
	 * @brief Returneert de ProfielFotoVerzameling die hoort bij dit object.
	 */
	public function getProfielFotoVerzameling()
	{
		if(!$this->profielFotoVerzameling instanceof ProfielFotoVerzameling)
			$this->profielFotoVerzameling = ProfielFotoVerzameling::fromPersoon($this);
		return $this->profielFotoVerzameling;
	}
	/**
	 * @brief Returneert de PersoonBadgeVerzameling die hoort bij dit object.
	 */
	public function getPersoonBadgeVerzameling()
	{
		if(!$this->persoonBadgeVerzameling instanceof PersoonBadgeVerzameling)
			$this->persoonBadgeVerzameling = PersoonBadgeVerzameling::fromPersoon($this);
		return $this->persoonBadgeVerzameling;
	}
	/**
	 * @brief Returneert de TentamenUitwerkingVerzameling die hoort bij dit object.
	 */
	public function getTentamenUitwerkingVerzameling()
	{
		if(!$this->tentamenUitwerkingVerzameling instanceof TentamenUitwerkingVerzameling)
			$this->tentamenUitwerkingVerzameling = TentamenUitwerkingVerzameling::fromUploader($this);
		return $this->tentamenUitwerkingVerzameling;
	}
	/**
	 * @brief Returneert de PersoonRatingVerzameling die hoort bij dit object.
	 */
	public function getPersoonRatingVerzameling()
	{
		if(!$this->persoonRatingVerzameling instanceof PersoonRatingVerzameling)
			$this->persoonRatingVerzameling = PersoonRatingVerzameling::fromPersoon($this);
		return $this->persoonRatingVerzameling;
	}
	abstract public function mijnActiviteiten();
	abstract public function mijnCommissies();
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Persoon::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Persoon.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Persoon::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContactID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Persoon|false
	 * Een Persoon-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contactID = (int)$a;
		}

		if(is_null($contactID))
			throw new BadMethodCallException();

		static::cache(array( array($contactID) ));
		return Entiteit::geefCache(array($contactID), 'Persoon');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Contact::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Persoon::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Persoon`.`voornaam`'
			                 .     ', `Persoon`.`bijnaam`'
			                 .     ', `Persoon`.`voorletters`'
			                 .     ', `Persoon`.`geboortenamen`'
			                 .     ', `Persoon`.`tussenvoegsels`'
			                 .     ', `Persoon`.`achternaam`'
			                 .     ', `Persoon`.`titelsPrefix`'
			                 .     ', `Persoon`.`titelsPostfix`'
			                 .     ', `Persoon`.`geslacht`'
			                 .     ', `Persoon`.`datumGeboorte`'
			                 .     ', `Persoon`.`overleden`'
			                 .     ', `Persoon`.`GPGkey`'
			                 .     ', `Persoon`.`Rekeningnummer`'
			                 .     ', `Persoon`.`opmerkingen`'
			                 .     ', `Persoon`.`voornaamwoord_woordID`'
			                 .     ', `Persoon`.`voornaamwoordZichtbaar`'
			                 .     ', `Contact`.`contactID`'
			                 .     ', `Contact`.`email`'
			                 .     ', `Contact`.`homepage`'
			                 .     ', `Contact`.`vakidOpsturen`'
			                 .     ', `Contact`.`aes2rootsOpsturen`'
			                 .     ', `Contact`.`gewijzigdWanneer`'
			                 .     ', `Contact`.`gewijzigdWie`'
			                 .' FROM `Persoon`'
			                 .' LEFT JOIN `Contact` USING (`contactID`)'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contactID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Persoon'][$id];
			} else {
				$obj = new Persoon();
			}

			$obj->inDB = True;

			$obj->voornaam  = trim($row['voornaam']);
			$obj->bijnaam  = (is_null($row['bijnaam'])) ? null : trim($row['bijnaam']);
			$obj->voorletters  = (is_null($row['voorletters'])) ? null : trim($row['voorletters']);
			$obj->geboortenamen  = (is_null($row['geboortenamen'])) ? null : trim($row['geboortenamen']);
			$obj->tussenvoegsels  = (is_null($row['tussenvoegsels'])) ? null : trim($row['tussenvoegsels']);
			$obj->achternaam  = trim($row['achternaam']);
			$obj->titelsPrefix  = (is_null($row['titelsPrefix'])) ? null : trim($row['titelsPrefix']);
			$obj->titelsPostfix  = (is_null($row['titelsPostfix'])) ? null : trim($row['titelsPostfix']);
			$obj->geslacht  = (is_null($row['geslacht'])) ? null : strtoupper(trim($row['geslacht']));
			$obj->datumGeboorte  = new DateTimeLocale($row['datumGeboorte']);
			$obj->overleden  = (bool) $row['overleden'];
			$obj->GPGkey  = (is_null($row['GPGkey'])) ? null : trim($row['GPGkey']);
			$obj->Rekeningnummer  = (is_null($row['Rekeningnummer'])) ? null : trim($row['Rekeningnummer']);
			$obj->opmerkingen  = (is_null($row['opmerkingen'])) ? null : trim($row['opmerkingen']);
			$obj->voornaamwoord_woordID  = (int) $row['voornaamwoord_woordID'];
			$obj->voornaamwoordZichtbaar  = (bool) $row['voornaamwoordZichtbaar'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Contact::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Persoon')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getVoornaamwoordWoordID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Persoon`'
			          . ' (`contactID`, `voornaam`, `bijnaam`, `voorletters`, `geboortenamen`, `tussenvoegsels`, `achternaam`, `titelsPrefix`, `titelsPostfix`, `geslacht`, `datumGeboorte`, `overleden`, `GPGkey`, `Rekeningnummer`, `opmerkingen`, `voornaamwoord_woordID`, `voornaamwoordZichtbaar`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %i, %s, %s, %s, %i, %i)'
			          , $this->contactID
			          , $this->voornaam
			          , $this->bijnaam
			          , $this->voorletters
			          , $this->geboortenamen
			          , $this->tussenvoegsels
			          , $this->achternaam
			          , $this->titelsPrefix
			          , $this->titelsPostfix
			          , $this->geslacht
			          , (!is_null($this->datumGeboorte))?$this->datumGeboorte->strftime('%F %T'):null
			          , $this->overleden
			          , $this->GPGkey
			          , $this->Rekeningnummer
			          , $this->opmerkingen
			          , $this->voornaamwoord_woordID
			          , $this->voornaamwoordZichtbaar
			          );

			if($classname == 'Persoon')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Persoon`'
			          .' SET `voornaam` = %s'
			          .   ', `bijnaam` = %s'
			          .   ', `voorletters` = %s'
			          .   ', `geboortenamen` = %s'
			          .   ', `tussenvoegsels` = %s'
			          .   ', `achternaam` = %s'
			          .   ', `titelsPrefix` = %s'
			          .   ', `titelsPostfix` = %s'
			          .   ', `geslacht` = %s'
			          .   ', `datumGeboorte` = %s'
			          .   ', `overleden` = %i'
			          .   ', `GPGkey` = %s'
			          .   ', `Rekeningnummer` = %s'
			          .   ', `opmerkingen` = %s'
			          .   ', `voornaamwoord_woordID` = %i'
			          .   ', `voornaamwoordZichtbaar` = %i'
			          .' WHERE `contactID` = %i'
			          , $this->voornaam
			          , $this->bijnaam
			          , $this->voorletters
			          , $this->geboortenamen
			          , $this->tussenvoegsels
			          , $this->achternaam
			          , $this->titelsPrefix
			          , $this->titelsPostfix
			          , $this->geslacht
			          , (!is_null($this->datumGeboorte))?$this->datumGeboorte->strftime('%F %T'):null
			          , $this->overleden
			          , $this->GPGkey
			          , $this->Rekeningnummer
			          , $this->opmerkingen
			          , $this->voornaamwoord_woordID
			          , $this->voornaamwoordZichtbaar
			          , $this->contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPersoon
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPersoon($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPersoon($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Voornaam';
			$velden[] = 'Bijnaam';
			$velden[] = 'Voorletters';
			$velden[] = 'Geboortenamen';
			$velden[] = 'Tussenvoegsels';
			$velden[] = 'Achternaam';
			$velden[] = 'TitelsPrefix';
			$velden[] = 'TitelsPostfix';
			$velden[] = 'Geslacht';
			$velden[] = 'DatumGeboorte';
			$velden[] = 'Overleden';
			$velden[] = 'GPGkey';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Opmerkingen';
			$velden[] = 'Voornaamwoord';
			$velden[] = 'VoornaamwoordZichtbaar';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Voornaam';
			$velden[] = 'Achternaam';
			$velden[] = 'Voornaamwoord';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Voornaam';
			$velden[] = 'Bijnaam';
			$velden[] = 'Voorletters';
			$velden[] = 'Geboortenamen';
			$velden[] = 'Tussenvoegsels';
			$velden[] = 'Achternaam';
			$velden[] = 'TitelsPrefix';
			$velden[] = 'TitelsPostfix';
			$velden[] = 'Geslacht';
			$velden[] = 'DatumGeboorte';
			$velden[] = 'Overleden';
			$velden[] = 'GPGkey';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Opmerkingen';
			$velden[] = 'Naam';
			$velden[] = 'Voornaamwoord';
			$velden[] = 'VoornaamwoordZichtbaar';
			break;
		case 'get':
			$velden[] = 'Voornaam';
			$velden[] = 'Bijnaam';
			$velden[] = 'Voorletters';
			$velden[] = 'Geboortenamen';
			$velden[] = 'Tussenvoegsels';
			$velden[] = 'Achternaam';
			$velden[] = 'TitelsPrefix';
			$velden[] = 'TitelsPostfix';
			$velden[] = 'Geslacht';
			$velden[] = 'DatumGeboorte';
			$velden[] = 'Overleden';
			$velden[] = 'GPGkey';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Opmerkingen';
			$velden[] = 'Naam';
			$velden[] = 'Voornaamwoord';
			$velden[] = 'VoornaamwoordZichtbaar';
		case 'primary':
			$velden[] = 'voornaamwoord_woordID';
			break;
		case 'verzamelingen':
			$velden[] = 'CommissieLidVerzameling';
			$velden[] = 'DeelnemerVerzameling';
			$velden[] = 'IntroClusterVerzameling';
			$velden[] = 'DonateurVerzameling';
			$velden[] = 'PersoonIMVerzameling';
			$velden[] = 'WachtwoordVerzameling';
			$velden[] = 'PlannerVerzameling';
			$velden[] = 'PlannerDeelnemerVerzameling';
			$velden[] = 'ContactPersoonVerzameling';
			$velden[] = 'ContractonderdeelVerzameling';
			$velden[] = 'PasVerzameling';
			$velden[] = 'DibsInfoVerzameling';
			$velden[] = 'DictaatVerzameling';
			$velden[] = 'JaargangVerzameling';
			$velden[] = 'IOUBonVerzameling';
			$velden[] = 'IOUBetalingVerzameling';
			$velden[] = 'IOUSplitVerzameling';
			$velden[] = 'MediaVerzameling';
			$velden[] = 'TagVerzameling';
			$velden[] = 'RatingVerzameling';
			$velden[] = 'BugVerzameling';
			$velden[] = 'BugBerichtVerzameling';
			$velden[] = 'BugToewijzingVerzameling';
			$velden[] = 'BugVolgerVerzameling';
			$velden[] = 'PersoonKartVerzameling';
			$velden[] = 'KartVoorwerpVerzameling';
			$velden[] = 'PersoonVoorkeurVerzameling';
			$velden[] = 'BestellingVerzameling';
			$velden[] = 'ProfielFotoVerzameling';
			$velden[] = 'PersoonBadgeVerzameling';
			$velden[] = 'TentamenUitwerkingVerzameling';
			$velden[] = 'PersoonRatingVerzameling';
			break;
		default:
			$velden[] = 'Voornaam';
			$velden[] = 'Bijnaam';
			$velden[] = 'Voorletters';
			$velden[] = 'Geboortenamen';
			$velden[] = 'Tussenvoegsels';
			$velden[] = 'Achternaam';
			$velden[] = 'TitelsPrefix';
			$velden[] = 'TitelsPostfix';
			$velden[] = 'Geslacht';
			$velden[] = 'DatumGeboorte';
			$velden[] = 'Overleden';
			$velden[] = 'GPGkey';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Opmerkingen';
			$velden[] = 'Naam';
			$velden[] = 'Voornaamwoord';
			$velden[] = 'VoornaamwoordZichtbaar';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle CommissieLid-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$CommissieLidFromPersoonVerz = CommissieLidVerzameling::fromPersoon($this);
		$returnValue = $CommissieLidFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CommissieLid met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Deelnemer-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DeelnemerFromPersoonVerz = DeelnemerVerzameling::fromPersoon($this);
		$returnValue = $DeelnemerFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Deelnemer met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle IntroCluster-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$IntroClusterFromPersoonVerz = IntroClusterVerzameling::fromPersoon($this);
		$returnValue = $IntroClusterFromPersoonVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IntroCluster met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Donateur-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DonateurFromPersoonVerz = DonateurVerzameling::fromPersoon($this);
		$returnValue = $DonateurFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Donateur met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Donateur-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DonateurFromAnderPersoonVerz = DonateurVerzameling::fromAnderPersoon($this);
		$returnValue = $DonateurFromAnderPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Donateur met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PersoonIM-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonIMFromPersoonVerz = PersoonIMVerzameling::fromPersoon($this);
		$returnValue = $PersoonIMFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonIM met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Wachtwoord-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$WachtwoordFromPersoonVerz = WachtwoordVerzameling::fromPersoon($this);
		$returnValue = $WachtwoordFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Wachtwoord met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Planner-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PlannerFromPersoonVerz = PlannerVerzameling::fromPersoon($this);
		$returnValue = $PlannerFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Planner met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PlannerDeelnemer-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PlannerDeelnemerFromPersoonVerz = PlannerDeelnemerVerzameling::fromPersoon($this);
		$returnValue = $PlannerDeelnemerFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PlannerDeelnemer met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ContactPersoon-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContactPersoonFromPersoonVerz = ContactPersoonVerzameling::fromPersoon($this);
		$returnValue = $ContactPersoonFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ContactPersoon met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Contractonderdeel-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$ContractonderdeelFromVerantwoordelijkeVerz = ContractonderdeelVerzameling::fromVerantwoordelijke($this);
		$returnValue = $ContractonderdeelFromVerantwoordelijkeVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Contractonderdeel met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Pas-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PasFromPersoonVerz = PasVerzameling::fromPersoon($this);
		$returnValue = $PasFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Pas met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle DibsInfo-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DibsInfoFromPersoonVerz = DibsInfoVerzameling::fromPersoon($this);
		$returnValue = $DibsInfoFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object DibsInfo met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Dictaat-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DictaatFromAuteurVerz = DictaatVerzameling::fromAuteur($this);
		$returnValue = $DictaatFromAuteurVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Dictaat met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Jaargang-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$JaargangFromContactPersoonVerz = JaargangVerzameling::fromContactPersoon($this);
		$returnValue = $JaargangFromContactPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Jaargang met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle IOUBon-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$IOUBonFromPersoonVerz = IOUBonVerzameling::fromPersoon($this);
		$returnValue = $IOUBonFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IOUBon met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle IOUBetaling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$IOUBetalingFromVanVerz = IOUBetalingVerzameling::fromVan($this);
		$returnValue = $IOUBetalingFromVanVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IOUBetaling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle IOUBetaling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$IOUBetalingFromNaarVerz = IOUBetalingVerzameling::fromNaar($this);
		$returnValue = $IOUBetalingFromNaarVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IOUBetaling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle IOUSplit-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$IOUSplitFromPersoonVerz = IOUSplitVerzameling::fromPersoon($this);
		$returnValue = $IOUSplitFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IOUSplit met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Media-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MediaFromUploaderVerz = MediaVerzameling::fromUploader($this);
		$returnValue = $MediaFromUploaderVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Media met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Media-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$MediaFromFotograafVerz = MediaVerzameling::fromFotograaf($this);
		$returnValue = $MediaFromFotograafVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Media met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Tag-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$TagFromPersoonVerz = TagVerzameling::fromPersoon($this);
		$returnValue = $TagFromPersoonVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tag met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Tag-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$TagFromTaggerVerz = TagVerzameling::fromTagger($this);
		$returnValue = $TagFromTaggerVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tag met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Rating-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$RatingFromPersoonVerz = RatingVerzameling::fromPersoon($this);
		$returnValue = $RatingFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Rating met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Bug-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$BugFromMelderVerz = BugVerzameling::fromMelder($this);
		$returnValue = $BugFromMelderVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Bug met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugBericht-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$BugBerichtFromMelderVerz = BugBerichtVerzameling::fromMelder($this);
		$returnValue = $BugBerichtFromMelderVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugBericht met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugToewijzing-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugToewijzingFromPersoonVerz = BugToewijzingVerzameling::fromPersoon($this);
		$returnValue = $BugToewijzingFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugToewijzing met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugVolger-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugVolgerFromPersoonVerz = BugVolgerVerzameling::fromPersoon($this);
		$returnValue = $BugVolgerFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugVolger met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PersoonKart-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonKartFromPersoonVerz = PersoonKartVerzameling::fromPersoon($this);
		$returnValue = $PersoonKartFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonKart met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle KartVoorwerp-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$KartVoorwerpFromPersoonVerz = KartVoorwerpVerzameling::fromPersoon($this);
		$returnValue = $KartVoorwerpFromPersoonVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object KartVoorwerp met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PersoonVoorkeur-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonVoorkeurFromPersoonVerz = PersoonVoorkeurVerzameling::fromPersoon($this);
		$returnValue = $PersoonVoorkeurFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonVoorkeur met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Bestelling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BestellingFromPersoonVerz = BestellingVerzameling::fromPersoon($this);
		$returnValue = $BestellingFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Bestelling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ProfielFoto-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ProfielFotoFromPersoonVerz = ProfielFotoVerzameling::fromPersoon($this);
		$returnValue = $ProfielFotoFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ProfielFoto met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PersoonBadge-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonBadgeFromPersoonVerz = PersoonBadgeVerzameling::fromPersoon($this);
		$returnValue = $PersoonBadgeFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonBadge met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle TentamenUitwerking-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TentamenUitwerkingFromUploaderVerz = TentamenUitwerkingVerzameling::fromUploader($this);
		$returnValue = $TentamenUitwerkingFromUploaderVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TentamenUitwerking met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PersoonRating-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonRatingFromPersoonVerz = PersoonRatingVerzameling::fromPersoon($this);
		$returnValue = $PersoonRatingFromPersoonVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonRating met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($IntroClusterFromPersoonVerz as $v)
		{
			$v->setPersoon(NULL);
		}
		$IntroClusterFromPersoonVerz->opslaan();

		foreach($ContractonderdeelFromVerantwoordelijkeVerz as $v)
		{
			$v->setVerantwoordelijke(NULL);
		}
		$ContractonderdeelFromVerantwoordelijkeVerz->opslaan();

		foreach($MediaFromFotograafVerz as $v)
		{
			$v->setFotograaf(NULL);
		}
		$MediaFromFotograafVerz->opslaan();

		foreach($TagFromPersoonVerz as $v)
		{
			$v->setPersoon(NULL);
		}
		$TagFromPersoonVerz->opslaan();

		foreach($TagFromTaggerVerz as $v)
		{
			$v->setTagger(NULL);
		}
		$TagFromTaggerVerz->opslaan();

		foreach($BugFromMelderVerz as $v)
		{
			$v->setMelder(NULL);
		}
		$BugFromMelderVerz->opslaan();

		foreach($BugBerichtFromMelderVerz as $v)
		{
			$v->setMelder(NULL);
		}
		$BugBerichtFromMelderVerz->opslaan();

		foreach($KartVoorwerpFromPersoonVerz as $v)
		{
			$v->setPersoon(NULL);
		}
		$KartVoorwerpFromPersoonVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $CommissieLidFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $DeelnemerFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $DonateurFromAnderPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PersoonIMFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $WachtwoordFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PlannerFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PlannerDeelnemerFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ContactPersoonFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PasFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $DibsInfoFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $DictaatFromAuteurVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $JaargangFromContactPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $IOUBonFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $IOUBetalingFromNaarVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $IOUSplitFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $MediaFromUploaderVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $RatingFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BugToewijzingFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BugVolgerFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PersoonKartFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PersoonVoorkeurFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BestellingFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ProfielFotoFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PersoonBadgeFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TentamenUitwerkingFromUploaderVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PersoonRatingFromPersoonVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Persoon`'
		          .' WHERE `contactID` = %i'
		          .' LIMIT 1'
		          , $this->contactID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Persoon terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'voornaam':
			return 'string';
		case 'bijnaam':
			return 'string';
		case 'voorletters':
			return 'string';
		case 'geboortenamen':
			return 'string';
		case 'tussenvoegsels':
			return 'string';
		case 'achternaam':
			return 'string';
		case 'titelsprefix':
			return 'string';
		case 'titelspostfix':
			return 'string';
		case 'geslacht':
			return 'enum';
		case 'datumgeboorte':
			return 'date';
		case 'overleden':
			return 'bool';
		case 'gpgkey':
			return 'string';
		case 'rekeningnummer':
			return 'string';
		case 'opmerkingen':
			return 'text';
		case 'naam':
			return 'text';
		case 'voornaamwoord':
			return 'foreign';
		case 'voornaamwoord_woordid':
			return 'int';
		case 'voornaamwoordzichtbaar':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'overleden':
		case 'voornaamwoord_woordID':
		case 'voornaamwoordZichtbaar':
			$type = '%i';
			break;
		case 'voornaam':
		case 'bijnaam':
		case 'voorletters':
		case 'geboortenamen':
		case 'tussenvoegsels':
		case 'achternaam':
		case 'titelsPrefix':
		case 'titelsPostfix':
		case 'geslacht':
		case 'datumGeboorte':
		case 'GPGkey':
		case 'Rekeningnummer':
		case 'opmerkingen':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Persoon`'
		          ." SET `%l` = $type"
		          .' WHERE `contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'voornaam':
		case 'bijnaam':
		case 'voorletters':
		case 'geboortenamen':
		case 'tussenvoegsels':
		case 'achternaam':
		case 'titelsPrefix':
		case 'titelsPostfix':
		case 'geslacht':
		case 'datumGeboorte':
		case 'overleden':
		case 'GPGkey':
		case 'Rekeningnummer':
		case 'opmerkingen':
		case 'voornaamwoord':
		case 'voornaamwoord_woordID':
		case 'voornaamwoordZichtbaar':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Persoon`'
		          .' WHERE `contactID` = %i'
		                 , $veld
		          , $this->contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle CommissieLid-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CommissieLidFromPersoonVerz = CommissieLidVerzameling::fromPersoon($this);
		$dependencies['CommissieLid'] = $CommissieLidFromPersoonVerz;

		// Verzamel alle Deelnemer-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DeelnemerFromPersoonVerz = DeelnemerVerzameling::fromPersoon($this);
		$dependencies['Deelnemer'] = $DeelnemerFromPersoonVerz;

		// Verzamel alle IntroCluster-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IntroClusterFromPersoonVerz = IntroClusterVerzameling::fromPersoon($this);
		$dependencies['IntroCluster'] = $IntroClusterFromPersoonVerz;

		// Verzamel alle Donateur-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DonateurFromPersoonVerz = DonateurVerzameling::fromPersoon($this);
		$dependencies['Donateur'] = $DonateurFromPersoonVerz;

		// Verzamel alle Donateur-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DonateurFromAnderPersoonVerz = DonateurVerzameling::fromAnderPersoon($this);
		$dependencies['Donateur'] = $DonateurFromAnderPersoonVerz;

		// Verzamel alle PersoonIM-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonIMFromPersoonVerz = PersoonIMVerzameling::fromPersoon($this);
		$dependencies['PersoonIM'] = $PersoonIMFromPersoonVerz;

		// Verzamel alle Wachtwoord-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$WachtwoordFromPersoonVerz = WachtwoordVerzameling::fromPersoon($this);
		$dependencies['Wachtwoord'] = $WachtwoordFromPersoonVerz;

		// Verzamel alle Planner-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PlannerFromPersoonVerz = PlannerVerzameling::fromPersoon($this);
		$dependencies['Planner'] = $PlannerFromPersoonVerz;

		// Verzamel alle PlannerDeelnemer-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PlannerDeelnemerFromPersoonVerz = PlannerDeelnemerVerzameling::fromPersoon($this);
		$dependencies['PlannerDeelnemer'] = $PlannerDeelnemerFromPersoonVerz;

		// Verzamel alle ContactPersoon-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContactPersoonFromPersoonVerz = ContactPersoonVerzameling::fromPersoon($this);
		$dependencies['ContactPersoon'] = $ContactPersoonFromPersoonVerz;

		// Verzamel alle Contractonderdeel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContractonderdeelFromVerantwoordelijkeVerz = ContractonderdeelVerzameling::fromVerantwoordelijke($this);
		$dependencies['Contractonderdeel'] = $ContractonderdeelFromVerantwoordelijkeVerz;

		// Verzamel alle Pas-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PasFromPersoonVerz = PasVerzameling::fromPersoon($this);
		$dependencies['Pas'] = $PasFromPersoonVerz;

		// Verzamel alle DibsInfo-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DibsInfoFromPersoonVerz = DibsInfoVerzameling::fromPersoon($this);
		$dependencies['DibsInfo'] = $DibsInfoFromPersoonVerz;

		// Verzamel alle Dictaat-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DictaatFromAuteurVerz = DictaatVerzameling::fromAuteur($this);
		$dependencies['Dictaat'] = $DictaatFromAuteurVerz;

		// Verzamel alle Jaargang-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$JaargangFromContactPersoonVerz = JaargangVerzameling::fromContactPersoon($this);
		$dependencies['Jaargang'] = $JaargangFromContactPersoonVerz;

		// Verzamel alle IOUBon-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IOUBonFromPersoonVerz = IOUBonVerzameling::fromPersoon($this);
		$dependencies['IOUBon'] = $IOUBonFromPersoonVerz;

		// Verzamel alle IOUBetaling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IOUBetalingFromVanVerz = IOUBetalingVerzameling::fromVan($this);
		$dependencies['IOUBetaling'] = $IOUBetalingFromVanVerz;

		// Verzamel alle IOUBetaling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IOUBetalingFromNaarVerz = IOUBetalingVerzameling::fromNaar($this);
		$dependencies['IOUBetaling'] = $IOUBetalingFromNaarVerz;

		// Verzamel alle IOUSplit-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IOUSplitFromPersoonVerz = IOUSplitVerzameling::fromPersoon($this);
		$dependencies['IOUSplit'] = $IOUSplitFromPersoonVerz;

		// Verzamel alle Media-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MediaFromUploaderVerz = MediaVerzameling::fromUploader($this);
		$dependencies['Media'] = $MediaFromUploaderVerz;

		// Verzamel alle Media-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MediaFromFotograafVerz = MediaVerzameling::fromFotograaf($this);
		$dependencies['Media'] = $MediaFromFotograafVerz;

		// Verzamel alle Tag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagFromPersoonVerz = TagVerzameling::fromPersoon($this);
		$dependencies['Tag'] = $TagFromPersoonVerz;

		// Verzamel alle Tag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagFromTaggerVerz = TagVerzameling::fromTagger($this);
		$dependencies['Tag'] = $TagFromTaggerVerz;

		// Verzamel alle Rating-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$RatingFromPersoonVerz = RatingVerzameling::fromPersoon($this);
		$dependencies['Rating'] = $RatingFromPersoonVerz;

		// Verzamel alle Bug-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugFromMelderVerz = BugVerzameling::fromMelder($this);
		$dependencies['Bug'] = $BugFromMelderVerz;

		// Verzamel alle BugBericht-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugBerichtFromMelderVerz = BugBerichtVerzameling::fromMelder($this);
		$dependencies['BugBericht'] = $BugBerichtFromMelderVerz;

		// Verzamel alle BugToewijzing-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugToewijzingFromPersoonVerz = BugToewijzingVerzameling::fromPersoon($this);
		$dependencies['BugToewijzing'] = $BugToewijzingFromPersoonVerz;

		// Verzamel alle BugVolger-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugVolgerFromPersoonVerz = BugVolgerVerzameling::fromPersoon($this);
		$dependencies['BugVolger'] = $BugVolgerFromPersoonVerz;

		// Verzamel alle PersoonKart-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonKartFromPersoonVerz = PersoonKartVerzameling::fromPersoon($this);
		$dependencies['PersoonKart'] = $PersoonKartFromPersoonVerz;

		// Verzamel alle KartVoorwerp-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$KartVoorwerpFromPersoonVerz = KartVoorwerpVerzameling::fromPersoon($this);
		$dependencies['KartVoorwerp'] = $KartVoorwerpFromPersoonVerz;

		// Verzamel alle PersoonVoorkeur-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonVoorkeurFromPersoonVerz = PersoonVoorkeurVerzameling::fromPersoon($this);
		$dependencies['PersoonVoorkeur'] = $PersoonVoorkeurFromPersoonVerz;

		// Verzamel alle Bestelling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BestellingFromPersoonVerz = BestellingVerzameling::fromPersoon($this);
		$dependencies['Bestelling'] = $BestellingFromPersoonVerz;

		// Verzamel alle ProfielFoto-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ProfielFotoFromPersoonVerz = ProfielFotoVerzameling::fromPersoon($this);
		$dependencies['ProfielFoto'] = $ProfielFotoFromPersoonVerz;

		// Verzamel alle PersoonBadge-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonBadgeFromPersoonVerz = PersoonBadgeVerzameling::fromPersoon($this);
		$dependencies['PersoonBadge'] = $PersoonBadgeFromPersoonVerz;

		// Verzamel alle TentamenUitwerking-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TentamenUitwerkingFromUploaderVerz = TentamenUitwerkingVerzameling::fromUploader($this);
		$dependencies['TentamenUitwerking'] = $TentamenUitwerkingFromUploaderVerz;

		// Verzamel alle PersoonRating-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonRatingFromPersoonVerz = PersoonRatingVerzameling::fromPersoon($this);
		$dependencies['PersoonRating'] = $PersoonRatingFromPersoonVerz;

		return $dependencies;
	}
}
