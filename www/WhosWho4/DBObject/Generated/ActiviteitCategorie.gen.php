<?
/**
 * @brief 'AT'AUTH_GET:gast
 */
abstract class ActiviteitCategorie_Generated
	extends Entiteit
{
	protected $actCategorieID;			/**< \brief PRIMARY */
	protected $titel;					/**< \brief LANG */
	/** Verzamelingen **/
	protected $activiteitInformatieVerzameling;
	/**
	 * @brief De constructor van de ActiviteitCategorie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->actCategorieID = NULL;
		$this->titel = array('nl' => '', 'en' => '');
		$this->activiteitInformatieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld actCategorieID.
	 *
	 * @return int
	 * De waarde van het veld actCategorieID.
	 */
	public function getActCategorieID()
	{
		return $this->actCategorieID;
	}
	/**
	 * @brief Stel de waarde van het veld actCategorieID in.
	 *
	 * @param mixed $newActCategorieID De nieuwe waarde.
	 *
	 * @return ActiviteitCategorie
	 * Dit ActiviteitCategorie-object.
	 */
	public function setActCategorieID($newActCategorieID)
	{
		unset($this->errors['ActCategorieID']);
		if(!is_null($newActCategorieID))
			$newActCategorieID = (int)$newActCategorieID;
		if($this->actCategorieID === $newActCategorieID)
			return $this;

		$this->actCategorieID = $newActCategorieID;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld actCategorieID geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld actCategorieID geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkActCategorieID()
	{
		if (array_key_exists('ActCategorieID', $this->errors))
			return $this->errors['ActCategorieID'];
		$waarde = $this->getActCategorieID();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->titel[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return ActiviteitCategorie
	 * Dit ActiviteitCategorie-object.
	 */
	public function setTitel($newTitel, $lang = NULL)
	{
		unset($this->errors['Titel']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel[$lang] === $newTitel)
			return $this;

		$this->titel[$lang] = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkTitel('nl');
			$ret['en'] = $this->checkTitel('en');
			return $ret;
		}

		if (array_key_exists('Titel', $this->errors))
			if (is_array($this->errors['Titel']) && array_key_exists($lang, $this->errors['Titel']))
				return $this->errors['Titel'][$lang];
		$waarde = $this->getTitel($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de ActiviteitInformatieVerzameling die hoort bij dit object.
	 */
	public function getActiviteitInformatieVerzameling()
	{
		if(!$this->activiteitInformatieVerzameling instanceof ActiviteitInformatieVerzameling)
			$this->activiteitInformatieVerzameling = ActiviteitInformatieVerzameling::fromCategorie($this);
		return $this->activiteitInformatieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ActiviteitCategorie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ActiviteitCategorie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ActiviteitCategorie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getActCategorieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return ActiviteitCategorie|false
	 * Een ActiviteitCategorie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$actCategorieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$actCategorieID = (int)$a;
		}

		if(is_null($actCategorieID))
			throw new BadMethodCallException();

		static::cache(array( array($actCategorieID) ));
		return Entiteit::geefCache(array($actCategorieID), 'ActiviteitCategorie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'ActiviteitCategorie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('ActiviteitCategorie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `ActiviteitCategorie`.`actCategorieID`'
		                 .     ', `ActiviteitCategorie`.`titel_NL`'
		                 .     ', `ActiviteitCategorie`.`titel_EN`'
		                 .     ', `ActiviteitCategorie`.`gewijzigdWanneer`'
		                 .     ', `ActiviteitCategorie`.`gewijzigdWie`'
		                 .' FROM `ActiviteitCategorie`'
		                 .' WHERE (`actCategorieID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['actCategorieID']);

			$obj = new ActiviteitCategorie();

			$obj->inDB = True;

			$obj->actCategorieID  = (int) $row['actCategorieID'];
			$obj->titel['nl'] = $row['titel_NL'];
			$obj->titel['en'] = $row['titel_EN'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ActiviteitCategorie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->actCategorieID =
			$WSW4DB->q('RETURNID INSERT INTO `ActiviteitCategorie`'
			          . ' (`titel_NL`, `titel_EN`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %i)'
			          , $this->titel['nl']
			          , $this->titel['en']
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'ActiviteitCategorie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ActiviteitCategorie`'
			          .' SET `titel_NL` = %s'
			          .   ', `titel_EN` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `actCategorieID` = %i'
			          , $this->titel['nl']
			          , $this->titel['en']
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->actCategorieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenActiviteitCategorie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenActiviteitCategorie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenActiviteitCategorie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Titel';
			break;
		case 'lang':
			$velden[] = 'Titel';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Titel';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Titel';
			break;
		case 'get':
			$velden[] = 'Titel';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'ActiviteitInformatieVerzameling';
			break;
		default:
			$velden[] = 'Titel';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle ActiviteitInformatie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ActiviteitInformatieFromCategorieVerz = ActiviteitInformatieVerzameling::fromCategorie($this);
		$returnValue = $ActiviteitInformatieFromCategorieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ActiviteitInformatie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $ActiviteitInformatieFromCategorieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ActiviteitCategorie`'
		          .' WHERE `actCategorieID` = %i'
		          .' LIMIT 1'
		          , $this->actCategorieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->actCategorieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `ActiviteitCategorie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `actCategorieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->actCategorieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van ActiviteitCategorie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'actcategorieid':
			return 'int';
		case 'titel':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'actCategorieID':
			$type = '%i';
			break;
		case 'titel':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ActiviteitCategorie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `actCategorieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->actCategorieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ActiviteitCategorie`'
		          .' WHERE `actCategorieID` = %i'
		                 , $veld
		          , $this->actCategorieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'ActiviteitCategorie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle ActiviteitInformatie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ActiviteitInformatieFromCategorieVerz = ActiviteitInformatieVerzameling::fromCategorie($this);
		$dependencies['ActiviteitInformatie'] = $ActiviteitInformatieFromCategorieVerz;

		return $dependencies;
	}
}
