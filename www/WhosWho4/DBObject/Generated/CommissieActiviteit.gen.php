<?
/**
 * @brief 'AT'AUTH_GET:gast
 */
abstract class CommissieActiviteit_Generated
	extends Entiteit
{
	protected $commissie;				/**< \brief PRIMARY */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $activiteit;				/**< \brief PRIMARY */
	protected $activiteit_activiteitID;	/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de CommissieActiviteit_Generated-klasse.
	 *
	 * @param mixed $a Commissie (Commissie OR Array(commissie_commissieID) OR
	 * commissie_commissieID)
	 * @param mixed $b Activiteit (Activiteit OR Array(activiteit_activiteitID) OR
	 * activiteit_activiteitID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Commissie)
		{
			$this->commissie = $a;
			$this->commissie_commissieID = $a->getCommissieID();
		}
		else if(is_array($a))
		{
			$this->commissie = NULL;
			$this->commissie_commissieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->commissie = NULL;
			$this->commissie_commissieID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Activiteit)
		{
			$this->activiteit = $b;
			$this->activiteit_activiteitID = $b->getActiviteitID();
		}
		else if(is_array($b))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Commissie';
		$volgorde[] = 'Activiteit';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @return Activiteit
	 * De waarde van het veld activiteit.
	 */
	public function getActiviteit()
	{
		if(!isset($this->activiteit)
		 && isset($this->activiteit_activiteitID)
		 ) {
			$this->activiteit = Activiteit::geef
					( $this->activiteit_activiteitID
					);
		}
		return $this->activiteit;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld activiteit_activiteitID.
	 */
	public function getActiviteitActiviteitID()
	{
		if (is_null($this->activiteit_activiteitID) && isset($this->activiteit)) {
			$this->activiteit_activiteitID = $this->activiteit->getActiviteitID();
		}
		return $this->activiteit_activiteitID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return CommissieActiviteit::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van CommissieActiviteit.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return CommissieActiviteit::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getCommissieCommissieID()
		                      , $this->getActiviteitActiviteitID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return CommissieActiviteit|false
	 * Een CommissieActiviteit-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$commissie_commissieID = (int)$a[0];
			$activiteit_activiteitID = (int)$a[1];
		}
		else if($a instanceof Commissie
		     && $b instanceof Activiteit)
		{
			$commissie_commissieID = $a->getCommissieID();
			$activiteit_activiteitID = $b->getActiviteitID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$commissie_commissieID = (int)$a;
			$activiteit_activiteitID = (int)$b;
		}

		if(is_null($commissie_commissieID)
		|| is_null($activiteit_activiteitID))
			throw new BadMethodCallException();

		static::cache(array( array($commissie_commissieID, $activiteit_activiteitID) ));
		return Entiteit::geefCache(array($commissie_commissieID, $activiteit_activiteitID), 'CommissieActiviteit');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'CommissieActiviteit');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('CommissieActiviteit::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `CommissieActiviteit`.`commissie_commissieID`'
		                 .     ', `CommissieActiviteit`.`activiteit_activiteitID`'
		                 .     ', `CommissieActiviteit`.`gewijzigdWanneer`'
		                 .     ', `CommissieActiviteit`.`gewijzigdWie`'
		                 .' FROM `CommissieActiviteit`'
		                 .' WHERE (`commissie_commissieID`, `activiteit_activiteitID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['commissie_commissieID']
			                  ,$row['activiteit_activiteitID']);

			$obj = new CommissieActiviteit(array($row['commissie_commissieID']), array($row['activiteit_activiteitID']));

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'CommissieActiviteit')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getCommissie();
		if(!$rel->getInDB())
			throw new LogicException('foreign Commissie is not in DB');
		$this->commissie_commissieID = $rel->getCommissieID();

		$rel = $this->getActiviteit();
		if(!$rel->getInDB())
			throw new LogicException('foreign Activiteit is not in DB');
		$this->activiteit_activiteitID = $rel->getActiviteitID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `CommissieActiviteit`'
			          . ' (`commissie_commissieID`, `activiteit_activiteitID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i)'
			          , $this->commissie_commissieID
			          , $this->activiteit_activiteitID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'CommissieActiviteit')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `CommissieActiviteit`'
			.' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `commissie_commissieID` = %i'
			          .  ' AND `activiteit_activiteitID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->commissie_commissieID
			          , $this->activiteit_activiteitID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenCommissieActiviteit
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenCommissieActiviteit($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenCommissieActiviteit($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			break;
		case 'get':
		case 'primary':
			$velden[] = 'commissie_commissieID';
			$velden[] = 'activiteit_activiteitID';
			break;
		case 'verzamelingen':
			break;
		default:
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `CommissieActiviteit`'
		          .' WHERE `commissie_commissieID` = %i'
		          .  ' AND `activiteit_activiteitID` = %i'
		          .' LIMIT 1'
		          , $this->commissie_commissieID
		          , $this->activiteit_activiteitID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->activiteit = NULL;
		$this->activiteit_activiteitID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `CommissieActiviteit`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `commissie_commissieID` = %i'
			          .  ' AND `activiteit_activiteitID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->commissie_commissieID
			          , $this->activiteit_activiteitID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van CommissieActiviteit terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'activiteit':
			return 'foreign';
		case 'activiteit_activiteitid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `CommissieActiviteit`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `commissie_commissieID` = %i'
		          .  ' AND `activiteit_activiteitID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->commissie_commissieID
		          , $this->activiteit_activiteitID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `CommissieActiviteit`'
		          .' WHERE `commissie_commissieID` = %i'
		          .  ' AND `activiteit_activiteitID` = %i'
		                 , $veld
		          , $this->commissie_commissieID
		          , $this->activiteit_activiteitID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'CommissieActiviteit');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
