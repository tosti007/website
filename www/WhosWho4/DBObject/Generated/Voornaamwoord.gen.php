<?
abstract class Voornaamwoord_Generated
	extends Entiteit
{
	protected $woordID;					/**< \brief PRIMARY */
	protected $soort;					/**< \brief LANG */
	protected $onderwerp;				/**< \brief LANG */
	protected $voorwerp;				/**< \brief LANG */
	protected $bezittelijk;				/**< \brief LANG */
	protected $wederkerend;				/**< \brief LANG */
	protected $ouder;					/**< \brief LANG */
	protected $kind;					/**< \brief LANG */
	protected $brusje;					/**< \brief LANG */
	protected $kozijn;					/**< \brief LANG */
	protected $kindGrootouder;			/**< \brief LANG */
	protected $kleinkindOuder;			/**< \brief LANG */
	/** Verzamelingen **/
	protected $persoonVerzameling;
	/**
	 * @brief De constructor van de Voornaamwoord_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->woordID = NULL;
		$this->soort = array('nl' => '', 'en' => '');
		$this->onderwerp = array('nl' => '', 'en' => '');
		$this->voorwerp = array('nl' => '', 'en' => '');
		$this->bezittelijk = array('nl' => '', 'en' => '');
		$this->wederkerend = array('nl' => '', 'en' => '');
		$this->ouder = array('nl' => '', 'en' => '');
		$this->kind = array('nl' => '', 'en' => '');
		$this->brusje = array('nl' => '', 'en' => '');
		$this->kozijn = array('nl' => '', 'en' => '');
		$this->kindGrootouder = array('nl' => '', 'en' => '');
		$this->kleinkindOuder = array('nl' => '', 'en' => '');
		$this->persoonVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld woordID.
	 *
	 * @return int
	 * De waarde van het veld woordID.
	 */
	public function getWoordID()
	{
		return $this->woordID;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->soort[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setSoort($newSoort, $lang = NULL)
	{
		unset($this->errors['Soort']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newSoort))
			$newSoort = trim($newSoort);
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort[$lang] === $newSoort)
			return $this;

		$this->soort[$lang] = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkSoort('nl');
			$ret['en'] = $this->checkSoort('en');
			return $ret;
		}

		if (array_key_exists('Soort', $this->errors))
			if (is_array($this->errors['Soort']) && array_key_exists($lang, $this->errors['Soort']))
				return $this->errors['Soort'][$lang];
		$waarde = $this->getSoort($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld onderwerp.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld onderwerp.
	 */
	public function getOnderwerp($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->onderwerp[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld onderwerp in.
	 *
	 * @param mixed $newOnderwerp De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setOnderwerp($newOnderwerp, $lang = NULL)
	{
		unset($this->errors['Onderwerp']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newOnderwerp))
			$newOnderwerp = trim($newOnderwerp);
		if($newOnderwerp === "")
			$newOnderwerp = NULL;
		if($this->onderwerp[$lang] === $newOnderwerp)
			return $this;

		$this->onderwerp[$lang] = $newOnderwerp;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld onderwerp geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld onderwerp geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOnderwerp($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkOnderwerp('nl');
			$ret['en'] = $this->checkOnderwerp('en');
			return $ret;
		}

		if (array_key_exists('Onderwerp', $this->errors))
			if (is_array($this->errors['Onderwerp']) && array_key_exists($lang, $this->errors['Onderwerp']))
				return $this->errors['Onderwerp'][$lang];
		$waarde = $this->getOnderwerp($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld voorwerp.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld voorwerp.
	 */
	public function getVoorwerp($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->voorwerp[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld voorwerp in.
	 *
	 * @param mixed $newVoorwerp De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setVoorwerp($newVoorwerp, $lang = NULL)
	{
		unset($this->errors['Voorwerp']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newVoorwerp))
			$newVoorwerp = trim($newVoorwerp);
		if($newVoorwerp === "")
			$newVoorwerp = NULL;
		if($this->voorwerp[$lang] === $newVoorwerp)
			return $this;

		$this->voorwerp[$lang] = $newVoorwerp;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voorwerp geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voorwerp geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkVoorwerp($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkVoorwerp('nl');
			$ret['en'] = $this->checkVoorwerp('en');
			return $ret;
		}

		if (array_key_exists('Voorwerp', $this->errors))
			if (is_array($this->errors['Voorwerp']) && array_key_exists($lang, $this->errors['Voorwerp']))
				return $this->errors['Voorwerp'][$lang];
		$waarde = $this->getVoorwerp($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bezittelijk.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld bezittelijk.
	 */
	public function getBezittelijk($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->bezittelijk[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld bezittelijk in.
	 *
	 * @param mixed $newBezittelijk De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setBezittelijk($newBezittelijk, $lang = NULL)
	{
		unset($this->errors['Bezittelijk']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newBezittelijk))
			$newBezittelijk = trim($newBezittelijk);
		if($newBezittelijk === "")
			$newBezittelijk = NULL;
		if($this->bezittelijk[$lang] === $newBezittelijk)
			return $this;

		$this->bezittelijk[$lang] = $newBezittelijk;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bezittelijk geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bezittelijk geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBezittelijk($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkBezittelijk('nl');
			$ret['en'] = $this->checkBezittelijk('en');
			return $ret;
		}

		if (array_key_exists('Bezittelijk', $this->errors))
			if (is_array($this->errors['Bezittelijk']) && array_key_exists($lang, $this->errors['Bezittelijk']))
				return $this->errors['Bezittelijk'][$lang];
		$waarde = $this->getBezittelijk($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld wederkerend.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld wederkerend.
	 */
	public function getWederkerend($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->wederkerend[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld wederkerend in.
	 *
	 * @param mixed $newWederkerend De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setWederkerend($newWederkerend, $lang = NULL)
	{
		unset($this->errors['Wederkerend']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newWederkerend))
			$newWederkerend = trim($newWederkerend);
		if($newWederkerend === "")
			$newWederkerend = NULL;
		if($this->wederkerend[$lang] === $newWederkerend)
			return $this;

		$this->wederkerend[$lang] = $newWederkerend;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld wederkerend geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld wederkerend geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkWederkerend($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkWederkerend('nl');
			$ret['en'] = $this->checkWederkerend('en');
			return $ret;
		}

		if (array_key_exists('Wederkerend', $this->errors))
			if (is_array($this->errors['Wederkerend']) && array_key_exists($lang, $this->errors['Wederkerend']))
				return $this->errors['Wederkerend'][$lang];
		$waarde = $this->getWederkerend($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ouder.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld ouder.
	 */
	public function getOuder($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->ouder[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld ouder in.
	 *
	 * @param mixed $newOuder De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setOuder($newOuder, $lang = NULL)
	{
		unset($this->errors['Ouder']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newOuder))
			$newOuder = trim($newOuder);
		if($newOuder === "")
			$newOuder = NULL;
		if($this->ouder[$lang] === $newOuder)
			return $this;

		$this->ouder[$lang] = $newOuder;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ouder geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ouder geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkOuder($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkOuder('nl');
			$ret['en'] = $this->checkOuder('en');
			return $ret;
		}

		if (array_key_exists('Ouder', $this->errors))
			if (is_array($this->errors['Ouder']) && array_key_exists($lang, $this->errors['Ouder']))
				return $this->errors['Ouder'][$lang];
		$waarde = $this->getOuder($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld kind.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld kind.
	 */
	public function getKind($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->kind[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld kind in.
	 *
	 * @param mixed $newKind De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setKind($newKind, $lang = NULL)
	{
		unset($this->errors['Kind']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newKind))
			$newKind = trim($newKind);
		if($newKind === "")
			$newKind = NULL;
		if($this->kind[$lang] === $newKind)
			return $this;

		$this->kind[$lang] = $newKind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kind geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kind geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkKind($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkKind('nl');
			$ret['en'] = $this->checkKind('en');
			return $ret;
		}

		if (array_key_exists('Kind', $this->errors))
			if (is_array($this->errors['Kind']) && array_key_exists($lang, $this->errors['Kind']))
				return $this->errors['Kind'][$lang];
		$waarde = $this->getKind($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld brusje.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld brusje.
	 */
	public function getBrusje($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->brusje[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld brusje in.
	 *
	 * @param mixed $newBrusje De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setBrusje($newBrusje, $lang = NULL)
	{
		unset($this->errors['Brusje']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newBrusje))
			$newBrusje = trim($newBrusje);
		if($newBrusje === "")
			$newBrusje = NULL;
		if($this->brusje[$lang] === $newBrusje)
			return $this;

		$this->brusje[$lang] = $newBrusje;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld brusje geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld brusje geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkBrusje($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkBrusje('nl');
			$ret['en'] = $this->checkBrusje('en');
			return $ret;
		}

		if (array_key_exists('Brusje', $this->errors))
			if (is_array($this->errors['Brusje']) && array_key_exists($lang, $this->errors['Brusje']))
				return $this->errors['Brusje'][$lang];
		$waarde = $this->getBrusje($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld kozijn.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld kozijn.
	 */
	public function getKozijn($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->kozijn[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld kozijn in.
	 *
	 * @param mixed $newKozijn De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setKozijn($newKozijn, $lang = NULL)
	{
		unset($this->errors['Kozijn']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newKozijn))
			$newKozijn = trim($newKozijn);
		if($newKozijn === "")
			$newKozijn = NULL;
		if($this->kozijn[$lang] === $newKozijn)
			return $this;

		$this->kozijn[$lang] = $newKozijn;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kozijn geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kozijn geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkKozijn($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkKozijn('nl');
			$ret['en'] = $this->checkKozijn('en');
			return $ret;
		}

		if (array_key_exists('Kozijn', $this->errors))
			if (is_array($this->errors['Kozijn']) && array_key_exists($lang, $this->errors['Kozijn']))
				return $this->errors['Kozijn'][$lang];
		$waarde = $this->getKozijn($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld kindGrootouder.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld kindGrootouder.
	 */
	public function getKindGrootouder($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->kindGrootouder[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld kindGrootouder in.
	 *
	 * @param mixed $newKindGrootouder De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setKindGrootouder($newKindGrootouder, $lang = NULL)
	{
		unset($this->errors['KindGrootouder']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newKindGrootouder))
			$newKindGrootouder = trim($newKindGrootouder);
		if($newKindGrootouder === "")
			$newKindGrootouder = NULL;
		if($this->kindGrootouder[$lang] === $newKindGrootouder)
			return $this;

		$this->kindGrootouder[$lang] = $newKindGrootouder;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kindGrootouder geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kindGrootouder geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkKindGrootouder($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkKindGrootouder('nl');
			$ret['en'] = $this->checkKindGrootouder('en');
			return $ret;
		}

		if (array_key_exists('KindGrootouder', $this->errors))
			if (is_array($this->errors['KindGrootouder']) && array_key_exists($lang, $this->errors['KindGrootouder']))
				return $this->errors['KindGrootouder'][$lang];
		$waarde = $this->getKindGrootouder($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld kleinkindOuder.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld kleinkindOuder.
	 */
	public function getKleinkindOuder($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->kleinkindOuder[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld kleinkindOuder in.
	 *
	 * @param mixed $newKleinkindOuder De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Voornaamwoord
	 * Dit Voornaamwoord-object.
	 */
	public function setKleinkindOuder($newKleinkindOuder, $lang = NULL)
	{
		unset($this->errors['KleinkindOuder']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newKleinkindOuder))
			$newKleinkindOuder = trim($newKleinkindOuder);
		if($newKleinkindOuder === "")
			$newKleinkindOuder = NULL;
		if($this->kleinkindOuder[$lang] === $newKleinkindOuder)
			return $this;

		$this->kleinkindOuder[$lang] = $newKleinkindOuder;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kleinkindOuder geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kleinkindOuder geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkKleinkindOuder($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkKleinkindOuder('nl');
			$ret['en'] = $this->checkKleinkindOuder('en');
			return $ret;
		}

		if (array_key_exists('KleinkindOuder', $this->errors))
			if (is_array($this->errors['KleinkindOuder']) && array_key_exists($lang, $this->errors['KleinkindOuder']))
				return $this->errors['KleinkindOuder'][$lang];
		$waarde = $this->getKleinkindOuder($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de PersoonVerzameling die hoort bij dit object.
	 */
	public function getPersoonVerzameling()
	{
		if(!$this->persoonVerzameling instanceof PersoonVerzameling)
			$this->persoonVerzameling = PersoonVerzameling::fromVoornaamwoord($this);
		return $this->persoonVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Voornaamwoord::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Voornaamwoord.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Voornaamwoord::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getWoordID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Voornaamwoord|false
	 * Een Voornaamwoord-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$woordID = (int)$a[0];
		}
		else if(isset($a))
		{
			$woordID = (int)$a;
		}

		if(is_null($woordID))
			throw new BadMethodCallException();

		static::cache(array( array($woordID) ));
		return Entiteit::geefCache(array($woordID), 'Voornaamwoord');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Voornaamwoord');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Voornaamwoord::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Voornaamwoord`.`woordID`'
		                 .     ', `Voornaamwoord`.`soort_NL`'
		                 .     ', `Voornaamwoord`.`soort_EN`'
		                 .     ', `Voornaamwoord`.`onderwerp_NL`'
		                 .     ', `Voornaamwoord`.`onderwerp_EN`'
		                 .     ', `Voornaamwoord`.`voorwerp_NL`'
		                 .     ', `Voornaamwoord`.`voorwerp_EN`'
		                 .     ', `Voornaamwoord`.`bezittelijk_NL`'
		                 .     ', `Voornaamwoord`.`bezittelijk_EN`'
		                 .     ', `Voornaamwoord`.`wederkerend_NL`'
		                 .     ', `Voornaamwoord`.`wederkerend_EN`'
		                 .     ', `Voornaamwoord`.`ouder_NL`'
		                 .     ', `Voornaamwoord`.`ouder_EN`'
		                 .     ', `Voornaamwoord`.`kind_NL`'
		                 .     ', `Voornaamwoord`.`kind_EN`'
		                 .     ', `Voornaamwoord`.`brusje_NL`'
		                 .     ', `Voornaamwoord`.`brusje_EN`'
		                 .     ', `Voornaamwoord`.`kozijn_NL`'
		                 .     ', `Voornaamwoord`.`kozijn_EN`'
		                 .     ', `Voornaamwoord`.`kindGrootouder_NL`'
		                 .     ', `Voornaamwoord`.`kindGrootouder_EN`'
		                 .     ', `Voornaamwoord`.`kleinkindOuder_NL`'
		                 .     ', `Voornaamwoord`.`kleinkindOuder_EN`'
		                 .     ', `Voornaamwoord`.`gewijzigdWanneer`'
		                 .     ', `Voornaamwoord`.`gewijzigdWie`'
		                 .' FROM `Voornaamwoord`'
		                 .' WHERE (`woordID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['woordID']);

			$obj = new Voornaamwoord();

			$obj->inDB = True;

			$obj->woordID  = (int) $row['woordID'];
			$obj->soort['nl'] = $row['soort_NL'];
			$obj->soort['en'] = $row['soort_EN'];
			$obj->onderwerp['nl'] = $row['onderwerp_NL'];
			$obj->onderwerp['en'] = $row['onderwerp_EN'];
			$obj->voorwerp['nl'] = $row['voorwerp_NL'];
			$obj->voorwerp['en'] = $row['voorwerp_EN'];
			$obj->bezittelijk['nl'] = $row['bezittelijk_NL'];
			$obj->bezittelijk['en'] = $row['bezittelijk_EN'];
			$obj->wederkerend['nl'] = $row['wederkerend_NL'];
			$obj->wederkerend['en'] = $row['wederkerend_EN'];
			$obj->ouder['nl'] = $row['ouder_NL'];
			$obj->ouder['en'] = $row['ouder_EN'];
			$obj->kind['nl'] = $row['kind_NL'];
			$obj->kind['en'] = $row['kind_EN'];
			$obj->brusje['nl'] = $row['brusje_NL'];
			$obj->brusje['en'] = $row['brusje_EN'];
			$obj->kozijn['nl'] = $row['kozijn_NL'];
			$obj->kozijn['en'] = $row['kozijn_EN'];
			$obj->kindGrootouder['nl'] = $row['kindGrootouder_NL'];
			$obj->kindGrootouder['en'] = $row['kindGrootouder_EN'];
			$obj->kleinkindOuder['nl'] = $row['kleinkindOuder_NL'];
			$obj->kleinkindOuder['en'] = $row['kleinkindOuder_EN'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Voornaamwoord')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->woordID =
			$WSW4DB->q('RETURNID INSERT INTO `Voornaamwoord`'
			          . ' (`soort_NL`, `soort_EN`, `onderwerp_NL`, `onderwerp_EN`, `voorwerp_NL`, `voorwerp_EN`, `bezittelijk_NL`, `bezittelijk_EN`, `wederkerend_NL`, `wederkerend_EN`, `ouder_NL`, `ouder_EN`, `kind_NL`, `kind_EN`, `brusje_NL`, `brusje_EN`, `kozijn_NL`, `kozijn_EN`, `kindGrootouder_NL`, `kindGrootouder_EN`, `kleinkindOuder_NL`, `kleinkindOuder_EN`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %i)'
			          , $this->soort['nl']
			          , $this->soort['en']
			          , $this->onderwerp['nl']
			          , $this->onderwerp['en']
			          , $this->voorwerp['nl']
			          , $this->voorwerp['en']
			          , $this->bezittelijk['nl']
			          , $this->bezittelijk['en']
			          , $this->wederkerend['nl']
			          , $this->wederkerend['en']
			          , $this->ouder['nl']
			          , $this->ouder['en']
			          , $this->kind['nl']
			          , $this->kind['en']
			          , $this->brusje['nl']
			          , $this->brusje['en']
			          , $this->kozijn['nl']
			          , $this->kozijn['en']
			          , $this->kindGrootouder['nl']
			          , $this->kindGrootouder['en']
			          , $this->kleinkindOuder['nl']
			          , $this->kleinkindOuder['en']
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Voornaamwoord')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Voornaamwoord`'
			          .' SET `soort_NL` = %s'
			          .   ', `soort_EN` = %s'
			          .   ', `onderwerp_NL` = %s'
			          .   ', `onderwerp_EN` = %s'
			          .   ', `voorwerp_NL` = %s'
			          .   ', `voorwerp_EN` = %s'
			          .   ', `bezittelijk_NL` = %s'
			          .   ', `bezittelijk_EN` = %s'
			          .   ', `wederkerend_NL` = %s'
			          .   ', `wederkerend_EN` = %s'
			          .   ', `ouder_NL` = %s'
			          .   ', `ouder_EN` = %s'
			          .   ', `kind_NL` = %s'
			          .   ', `kind_EN` = %s'
			          .   ', `brusje_NL` = %s'
			          .   ', `brusje_EN` = %s'
			          .   ', `kozijn_NL` = %s'
			          .   ', `kozijn_EN` = %s'
			          .   ', `kindGrootouder_NL` = %s'
			          .   ', `kindGrootouder_EN` = %s'
			          .   ', `kleinkindOuder_NL` = %s'
			          .   ', `kleinkindOuder_EN` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `woordID` = %i'
			          , $this->soort['nl']
			          , $this->soort['en']
			          , $this->onderwerp['nl']
			          , $this->onderwerp['en']
			          , $this->voorwerp['nl']
			          , $this->voorwerp['en']
			          , $this->bezittelijk['nl']
			          , $this->bezittelijk['en']
			          , $this->wederkerend['nl']
			          , $this->wederkerend['en']
			          , $this->ouder['nl']
			          , $this->ouder['en']
			          , $this->kind['nl']
			          , $this->kind['en']
			          , $this->brusje['nl']
			          , $this->brusje['en']
			          , $this->kozijn['nl']
			          , $this->kozijn['en']
			          , $this->kindGrootouder['nl']
			          , $this->kindGrootouder['en']
			          , $this->kleinkindOuder['nl']
			          , $this->kleinkindOuder['en']
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->woordID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVoornaamwoord
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVoornaamwoord($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVoornaamwoord($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Soort';
			$velden[] = 'Onderwerp';
			$velden[] = 'Voorwerp';
			$velden[] = 'Bezittelijk';
			$velden[] = 'Wederkerend';
			$velden[] = 'Ouder';
			$velden[] = 'Kind';
			$velden[] = 'Brusje';
			$velden[] = 'Kozijn';
			$velden[] = 'KindGrootouder';
			$velden[] = 'KleinkindOuder';
			break;
		case 'lang':
			$velden[] = 'Soort';
			$velden[] = 'Onderwerp';
			$velden[] = 'Voorwerp';
			$velden[] = 'Bezittelijk';
			$velden[] = 'Wederkerend';
			$velden[] = 'Ouder';
			$velden[] = 'Kind';
			$velden[] = 'Brusje';
			$velden[] = 'Kozijn';
			$velden[] = 'KindGrootouder';
			$velden[] = 'KleinkindOuder';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Soort';
			$velden[] = 'Onderwerp';
			$velden[] = 'Voorwerp';
			$velden[] = 'Bezittelijk';
			$velden[] = 'Wederkerend';
			$velden[] = 'Ouder';
			$velden[] = 'Kind';
			$velden[] = 'Brusje';
			$velden[] = 'Kozijn';
			$velden[] = 'KindGrootouder';
			$velden[] = 'KleinkindOuder';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Soort';
			$velden[] = 'Onderwerp';
			$velden[] = 'Voorwerp';
			$velden[] = 'Bezittelijk';
			$velden[] = 'Wederkerend';
			$velden[] = 'Ouder';
			$velden[] = 'Kind';
			$velden[] = 'Brusje';
			$velden[] = 'Kozijn';
			$velden[] = 'KindGrootouder';
			$velden[] = 'KleinkindOuder';
			break;
		case 'get':
			$velden[] = 'Soort';
			$velden[] = 'Onderwerp';
			$velden[] = 'Voorwerp';
			$velden[] = 'Bezittelijk';
			$velden[] = 'Wederkerend';
			$velden[] = 'Ouder';
			$velden[] = 'Kind';
			$velden[] = 'Brusje';
			$velden[] = 'Kozijn';
			$velden[] = 'KindGrootouder';
			$velden[] = 'KleinkindOuder';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'PersoonVerzameling';
			break;
		default:
			$velden[] = 'Soort';
			$velden[] = 'Onderwerp';
			$velden[] = 'Voorwerp';
			$velden[] = 'Bezittelijk';
			$velden[] = 'Wederkerend';
			$velden[] = 'Ouder';
			$velden[] = 'Kind';
			$velden[] = 'Brusje';
			$velden[] = 'Kozijn';
			$velden[] = 'KindGrootouder';
			$velden[] = 'KleinkindOuder';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Persoon-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonFromVoornaamwoordVerz = PersoonVerzameling::fromVoornaamwoord($this);
		$returnValue = $PersoonFromVoornaamwoordVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Persoon met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $PersoonFromVoornaamwoordVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Voornaamwoord`'
		          .' WHERE `woordID` = %i'
		          .' LIMIT 1'
		          , $this->woordID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->woordID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Voornaamwoord`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `woordID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->woordID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Voornaamwoord terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'woordid':
			return 'int';
		case 'soort':
			return 'string';
		case 'onderwerp':
			return 'string';
		case 'voorwerp':
			return 'string';
		case 'bezittelijk':
			return 'string';
		case 'wederkerend':
			return 'string';
		case 'ouder':
			return 'string';
		case 'kind':
			return 'string';
		case 'brusje':
			return 'string';
		case 'kozijn':
			return 'string';
		case 'kindgrootouder':
			return 'string';
		case 'kleinkindouder':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'woordID':
			$type = '%i';
			break;
		case 'soort':
		case 'onderwerp':
		case 'voorwerp':
		case 'bezittelijk':
		case 'wederkerend':
		case 'ouder':
		case 'kind':
		case 'brusje':
		case 'kozijn':
		case 'kindGrootouder':
		case 'kleinkindOuder':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Voornaamwoord`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `woordID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->woordID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Voornaamwoord`'
		          .' WHERE `woordID` = %i'
		                 , $veld
		          , $this->woordID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Voornaamwoord');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Persoon-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonFromVoornaamwoordVerz = PersoonVerzameling::fromVoornaamwoord($this);
		$dependencies['Persoon'] = $PersoonFromVoornaamwoordVerz;

		return $dependencies;
	}
}
