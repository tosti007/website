<?
/**
 * @brief 'AT'AUTH_GET:bestuur,AUTH_SET:bestuur
 */
abstract class Planner_Generated
	extends Entiteit
{
	protected $plannerID;				/**< \brief PRIMARY */
	protected $persoon;					/**< \brief De eigenaar van de planner */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $titel;
	protected $omschrijving;			/**< \brief NULL */
	protected $mail;					/**< \brief Moet de eigenaar een mail ontvangen zodra er een wijziging is */
	/** Verzamelingen **/
	protected $plannerDataVerzameling;
	/**
	 * @brief De constructor van de Planner_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->plannerID = NULL;
		$this->persoon = NULL;
		$this->persoon_contactID = 0;
		$this->titel = '';
		$this->omschrijving = '';
		$this->mail = False;
		$this->plannerDataVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld plannerID.
	 *
	 * @return int
	 * De waarde van het veld plannerID.
	 */
	public function getPlannerID()
	{
		return $this->plannerID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Stel de waarde van het veld persoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Planner
	 * Dit Planner-object.
	 */
	public function setPersoon($new_contactID)
	{
		unset($this->errors['Persoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->persoon == $new_contactID
			&& $this->persoon_contactID == $this->persoon->getContactID())
				return $this;
			$this->persoon = $new_contactID;
			$this->persoon_contactID
					= $this->persoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->persoon == NULL 
				&& $this->persoon_contactID == (int)$new_contactID)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld persoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld persoon geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPersoon()
	{
		if (array_key_exists('Persoon', $this->errors))
			return $this->errors['Persoon'];
		$waarde1 = $this->getPersoon();
		$waarde2 = $this->getPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 *
	 * @return Planner
	 * Dit Planner-object.
	 */
	public function setTitel($newTitel)
	{
		unset($this->errors['Titel']);
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel === $newTitel)
			return $this;

		$this->titel = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel()
	{
		if (array_key_exists('Titel', $this->errors))
			return $this->errors['Titel'];
		$waarde = $this->getTitel();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Planner
	 * Dit Planner-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if (!is_null($newOmschrijving))
			$newOmschrijving = Purifier::Purify($newOmschrijving);
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld mail.
	 *
	 * @return bool
	 * De waarde van het veld mail.
	 */
	public function getMail()
	{
		return $this->mail;
	}
	/**
	 * @brief Stel de waarde van het veld mail in.
	 *
	 * @param mixed $newMail De nieuwe waarde.
	 *
	 * @return Planner
	 * Dit Planner-object.
	 */
	public function setMail($newMail)
	{
		unset($this->errors['Mail']);
		if(!is_null($newMail))
			$newMail = (bool)$newMail;
		if($this->mail === $newMail)
			return $this;

		$this->mail = $newMail;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld mail geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld mail geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMail()
	{
		if (array_key_exists('Mail', $this->errors))
			return $this->errors['Mail'];
		$waarde = $this->getMail();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de PlannerDataVerzameling die hoort bij dit object.
	 */
	public function getPlannerDataVerzameling()
	{
		if(!$this->plannerDataVerzameling instanceof PlannerDataVerzameling)
			$this->plannerDataVerzameling = PlannerDataVerzameling::fromPlanner($this);
		return $this->plannerDataVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Planner::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Planner.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Planner::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getPlannerID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Planner|false
	 * Een Planner-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$plannerID = (int)$a[0];
		}
		else if(isset($a))
		{
			$plannerID = (int)$a;
		}

		if(is_null($plannerID))
			throw new BadMethodCallException();

		static::cache(array( array($plannerID) ));
		return Entiteit::geefCache(array($plannerID), 'Planner');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Planner');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Planner::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Planner`.`plannerID`'
		                 .     ', `Planner`.`persoon_contactID`'
		                 .     ', `Planner`.`titel`'
		                 .     ', `Planner`.`omschrijving`'
		                 .     ', `Planner`.`mail`'
		                 .     ', `Planner`.`gewijzigdWanneer`'
		                 .     ', `Planner`.`gewijzigdWie`'
		                 .' FROM `Planner`'
		                 .' WHERE (`plannerID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['plannerID']);

			$obj = new Planner();

			$obj->inDB = True;

			$obj->plannerID  = (int) $row['plannerID'];
			$obj->persoon_contactID  = (int) $row['persoon_contactID'];
			$obj->titel  = trim($row['titel']);
			$obj->omschrijving = $row['omschrijving'];
			$obj->mail  = (bool) $row['mail'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Planner')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getPersoonContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->plannerID =
			$WSW4DB->q('RETURNID INSERT INTO `Planner`'
			          . ' (`persoon_contactID`, `titel`, `omschrijving`, `mail`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i, %s, %i)'
			          , $this->persoon_contactID
			          , $this->titel
			          , $this->omschrijving
			          , $this->mail
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Planner')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Planner`'
			          .' SET `persoon_contactID` = %i'
			          .   ', `titel` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `mail` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `plannerID` = %i'
			          , $this->persoon_contactID
			          , $this->titel
			          , $this->omschrijving
			          , $this->mail
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->plannerID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPlanner
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPlanner($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPlanner($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Persoon';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mail';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Persoon';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Persoon';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mail';
			break;
		case 'get':
			$velden[] = 'Persoon';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mail';
		case 'primary':
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'PlannerDataVerzameling';
			break;
		default:
			$velden[] = 'Persoon';
			$velden[] = 'Titel';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mail';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle PlannerData-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PlannerDataFromPlannerVerz = PlannerDataVerzameling::fromPlanner($this);
		$returnValue = $PlannerDataFromPlannerVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PlannerData met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $PlannerDataFromPlannerVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Planner`'
		          .' WHERE `plannerID` = %i'
		          .' LIMIT 1'
		          , $this->plannerID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->plannerID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Planner`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `plannerID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->plannerID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Planner terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'plannerid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'titel':
			return 'string';
		case 'omschrijving':
			return 'html';
		case 'mail':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'plannerID':
		case 'persoon_contactID':
		case 'mail':
			$type = '%i';
			break;
		case 'titel':
		case 'omschrijving':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Planner`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `plannerID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->plannerID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Planner`'
		          .' WHERE `plannerID` = %i'
		                 , $veld
		          , $this->plannerID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Planner');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle PlannerData-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PlannerDataFromPlannerVerz = PlannerDataVerzameling::fromPlanner($this);
		$dependencies['PlannerData'] = $PlannerDataFromPlannerVerz;

		return $dependencies;
	}
}
