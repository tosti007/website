<?
/**
 * @brief 'AT'AUTH_GET:bestuur,AUTH_SET:bestuur
 */
abstract class Contact_Generated
	extends Entiteit
{
	protected $contactID;				/**< \brief PRIMARY */
	protected $email;					/**< \brief NULL */
	protected $homepage;				/**< \brief NULL,LAZY */
	protected $vakidOpsturen;			/**< \brief LAZY Default is false omdat contacten niet alleen leden zijn en er toch ook niet-leden zijn die een vakid willen. */
	protected $aes2rootsOpsturen;		/**< \brief LAZY */
	/** Verzamelingen **/
	protected $contactAdresVerzameling;
	protected $mailingBounceVerzameling;
	protected $contactTelnrVerzameling;
	protected $contactMailingListVerzameling;
	protected $transactieVerzameling;
	/**
	 * @brief De constructor van de Contact_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->contactID = NULL;
		$this->email = NULL;
		$this->homepage = NULL;
		$this->vakidOpsturen = False;
		$this->aes2rootsOpsturen = False;
		$this->contactAdresVerzameling = NULL;
		$this->mailingBounceVerzameling = NULL;
		$this->contactTelnrVerzameling = NULL;
		$this->contactMailingListVerzameling = NULL;
		$this->transactieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld contactID.
	 *
	 * @return int
	 * De waarde van het veld contactID.
	 */
	public function getContactID()
	{
		return $this->contactID;
	}
	/**
	 * @brief Geef de waarde van het veld email.
	 *
	 * @return string
	 * De waarde van het veld email.
	 */
	public function getEmail()
	{
		return $this->email;
	}
	/**
	 * @brief Stel de waarde van het veld email in.
	 *
	 * @param mixed $newEmail De nieuwe waarde.
	 *
	 * @return Contact
	 * Dit Contact-object.
	 */
	public function setEmail($newEmail)
	{
		unset($this->errors['Email']);
		if(!is_null($newEmail))
			$newEmail = trim($newEmail);
		if($newEmail === "")
			$newEmail = NULL;
		if($this->email === $newEmail)
			return $this;

		$this->email = $newEmail;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld email geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld email geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkEmail()
	{
		if (array_key_exists('Email', $this->errors))
			return $this->errors['Email'];
		$waarde = $this->getEmail();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld homepage.
	 *
	 * @return string
	 * De waarde van het veld homepage.
	 */
	public function getHomepage()
	{
		return $this->homepage;
	}
	/**
	 * @brief Stel de waarde van het veld homepage in.
	 *
	 * @param mixed $newHomepage De nieuwe waarde.
	 *
	 * @return Contact
	 * Dit Contact-object.
	 */
	public function setHomepage($newHomepage)
	{
		unset($this->errors['Homepage']);
		if(!is_null($newHomepage))
			$newHomepage = trim($newHomepage);
		if($newHomepage === "")
			$newHomepage = NULL;
		if($this->homepage === $newHomepage)
			return $this;

		$this->homepage = $newHomepage;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld homepage geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld homepage geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkHomepage()
	{
		if (array_key_exists('Homepage', $this->errors))
			return $this->errors['Homepage'];
		$waarde = $this->getHomepage();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld vakidOpsturen.
	 *
	 * @return bool
	 * De waarde van het veld vakidOpsturen.
	 */
	public function getVakidOpsturen()
	{
		return $this->vakidOpsturen;
	}
	/**
	 * @brief Stel de waarde van het veld vakidOpsturen in.
	 *
	 * @param mixed $newVakidOpsturen De nieuwe waarde.
	 *
	 * @return Contact
	 * Dit Contact-object.
	 */
	public function setVakidOpsturen($newVakidOpsturen)
	{
		unset($this->errors['VakidOpsturen']);
		if(!is_null($newVakidOpsturen))
			$newVakidOpsturen = (bool)$newVakidOpsturen;
		if($this->vakidOpsturen === $newVakidOpsturen)
			return $this;

		$this->vakidOpsturen = $newVakidOpsturen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld vakidOpsturen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld vakidOpsturen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVakidOpsturen()
	{
		if (array_key_exists('VakidOpsturen', $this->errors))
			return $this->errors['VakidOpsturen'];
		$waarde = $this->getVakidOpsturen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld aes2rootsOpsturen.
	 *
	 * @return bool
	 * De waarde van het veld aes2rootsOpsturen.
	 */
	public function getAes2rootsOpsturen()
	{
		return $this->aes2rootsOpsturen;
	}
	/**
	 * @brief Stel de waarde van het veld aes2rootsOpsturen in.
	 *
	 * @param mixed $newAes2rootsOpsturen De nieuwe waarde.
	 *
	 * @return Contact
	 * Dit Contact-object.
	 */
	public function setAes2rootsOpsturen($newAes2rootsOpsturen)
	{
		unset($this->errors['Aes2rootsOpsturen']);
		if(!is_null($newAes2rootsOpsturen))
			$newAes2rootsOpsturen = (bool)$newAes2rootsOpsturen;
		if($this->aes2rootsOpsturen === $newAes2rootsOpsturen)
			return $this;

		$this->aes2rootsOpsturen = $newAes2rootsOpsturen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aes2rootsOpsturen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aes2rootsOpsturen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkAes2rootsOpsturen()
	{
		if (array_key_exists('Aes2rootsOpsturen', $this->errors))
			return $this->errors['Aes2rootsOpsturen'];
		$waarde = $this->getAes2rootsOpsturen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de ContactAdresVerzameling die hoort bij dit object.
	 */
	public function getContactAdresVerzameling()
	{
		if(!$this->contactAdresVerzameling instanceof ContactAdresVerzameling)
			$this->contactAdresVerzameling = ContactAdresVerzameling::fromContact($this);
		return $this->contactAdresVerzameling;
	}
	/**
	 * @brief Returneert de MailingBounceVerzameling die hoort bij dit object.
	 */
	public function getMailingBounceVerzameling()
	{
		if(!$this->mailingBounceVerzameling instanceof MailingBounceVerzameling)
			$this->mailingBounceVerzameling = MailingBounceVerzameling::fromContact($this);
		return $this->mailingBounceVerzameling;
	}
	/**
	 * @brief Returneert de ContactTelnrVerzameling die hoort bij dit object.
	 */
	public function getContactTelnrVerzameling()
	{
		if(!$this->contactTelnrVerzameling instanceof ContactTelnrVerzameling)
			$this->contactTelnrVerzameling = ContactTelnrVerzameling::fromContact($this);
		return $this->contactTelnrVerzameling;
	}
	/**
	 * @brief Returneert de ContactMailingListVerzameling die hoort bij dit object.
	 */
	public function getContactMailingListVerzameling()
	{
		if(!$this->contactMailingListVerzameling instanceof ContactMailingListVerzameling)
			$this->contactMailingListVerzameling = ContactMailingListVerzameling::fromContact($this);
		return $this->contactMailingListVerzameling;
	}
	/**
	 * @brief Returneert de TransactieVerzameling die hoort bij dit object.
	 */
	public function getTransactieVerzameling()
	{
		if(!$this->transactieVerzameling instanceof TransactieVerzameling)
			$this->transactieVerzameling = TransactieVerzameling::fromContact($this);
		return $this->transactieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Contact::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Contact.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array('Lid',
			'Persoon',
			'Organisatie',
			'Bedrijf',
			'Leverancier');
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Contact::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContactID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Contact|false
	 * Een Contact-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contactID = (int)$a;
		}

		if(is_null($contactID))
			throw new BadMethodCallException();

		static::cache(array( array($contactID) ));
		return Entiteit::geefCache(array($contactID), 'Contact');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			$ids = Entiteit::nietInCache($ids, 'Contact');

			// is er nog iets te doen?
			if(empty($ids)) return;

			// zoek uit wat de klasse van een id is
			$res = $WSW4DB->q('TABLE SELECT `contactID`'
			                 .     ', `overerving`'
			                 .' FROM `Contact`'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );

			$recur = array();
			foreach($res as $row)
			{
				$recur[$row['overerving']][]
				    = array($row['contactID']);
			}
			foreach($recur as $class => $obj)
			{
				$class::cache($obj, True);
			}

			return;
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Contact::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Contact`.`contactID`'
			                 .     ', `Contact`.`email`'
			                 .     ', `Contact`.`homepage`'
			                 .     ', `Contact`.`vakidOpsturen`'
			                 .     ', `Contact`.`aes2rootsOpsturen`'
			                 .     ', `Contact`.`gewijzigdWanneer`'
			                 .     ', `Contact`.`gewijzigdWie`'
			                 .' FROM `Contact`'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contactID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Contact'][$id];
			} else {
				$obj = new Contact();
			}

			$obj->inDB = True;

			$obj->contactID  = (int) $row['contactID'];
			$obj->email  = (is_null($row['email'])) ? null : trim($row['email']);
			$obj->homepage = $row['homepage'];
			$obj->vakidOpsturen  = (bool) $row['vakidOpsturen'];
			$obj->aes2rootsOpsturen  = (bool) $row['aes2rootsOpsturen'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			if($recur === True)
				self::stopInCache($id, $obj);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Contact')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->contactID =
			$WSW4DB->q('RETURNID INSERT INTO `Contact`'
			          . ' (`overerving`, `email`, `homepage`, `vakidOpsturen`, `aes2rootsOpsturen`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %i, %i, %s, %i)'
			          , $classname
			          , $this->email
			          , $this->homepage
			          , $this->vakidOpsturen
			          , $this->aes2rootsOpsturen
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Contact')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Contact`'
			          .' SET `email` = %s'
			          .   ', `homepage` = %s'
			          .   ', `vakidOpsturen` = %i'
			          .   ', `aes2rootsOpsturen` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `contactID` = %i'
			          , $this->email
			          , $this->homepage
			          , $this->vakidOpsturen
			          , $this->aes2rootsOpsturen
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenContact
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenContact($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenContact($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'VakidOpsturen';
			$velden[] = 'Aes2rootsOpsturen';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'VakidOpsturen';
			$velden[] = 'Aes2rootsOpsturen';
			break;
		case 'get':
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'VakidOpsturen';
			$velden[] = 'Aes2rootsOpsturen';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'ContactAdresVerzameling';
			$velden[] = 'MailingBounceVerzameling';
			$velden[] = 'ContactTelnrVerzameling';
			$velden[] = 'ContactMailingListVerzameling';
			$velden[] = 'TransactieVerzameling';
			break;
		default:
			$velden[] = 'Email';
			$velden[] = 'Homepage';
			$velden[] = 'VakidOpsturen';
			$velden[] = 'Aes2rootsOpsturen';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle ContactAdres-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContactAdresFromContactVerz = ContactAdresVerzameling::fromContact($this);
		$returnValue = $ContactAdresFromContactVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ContactAdres met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle MailingBounce-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MailingBounceFromContactVerz = MailingBounceVerzameling::fromContact($this);
		$returnValue = $MailingBounceFromContactVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object MailingBounce met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ContactTelnr-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContactTelnrFromContactVerz = ContactTelnrVerzameling::fromContact($this);
		$returnValue = $ContactTelnrFromContactVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ContactTelnr met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ContactMailingList-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContactMailingListFromContactVerz = ContactMailingListVerzameling::fromContact($this);
		$returnValue = $ContactMailingListFromContactVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ContactMailingList met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Transactie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TransactieFromContactVerz = TransactieVerzameling::fromContact($this);
		$returnValue = $TransactieFromContactVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Transactie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $ContactAdresFromContactVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $MailingBounceFromContactVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ContactTelnrFromContactVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ContactMailingListFromContactVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TransactieFromContactVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Contact`'
		          .' WHERE `contactID` = %i'
		          .' LIMIT 1'
		          , $this->contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Contact`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Contact terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'contactid':
			return 'int';
		case 'email':
			return 'string';
		case 'homepage':
			return 'url';
		case 'vakidopsturen':
			return 'bool';
		case 'aes2rootsopsturen':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'contactID':
		case 'vakidOpsturen':
		case 'aes2rootsOpsturen':
			$type = '%i';
			break;
		case 'email':
		case 'homepage':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Contact`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Contact`'
		          .' WHERE `contactID` = %i'
		                 , $veld
		          , $this->contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Contact');
		parent::stopInCache($id, $obj, 'Lid');
		parent::stopInCache($id, $obj, 'Persoon');
		parent::stopInCache($id, $obj, 'Organisatie');
		parent::stopInCache($id, $obj, 'Bedrijf');
		parent::stopInCache($id, $obj, 'Leverancier');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle ContactAdres-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContactAdresFromContactVerz = ContactAdresVerzameling::fromContact($this);
		$dependencies['ContactAdres'] = $ContactAdresFromContactVerz;

		// Verzamel alle MailingBounce-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MailingBounceFromContactVerz = MailingBounceVerzameling::fromContact($this);
		$dependencies['MailingBounce'] = $MailingBounceFromContactVerz;

		// Verzamel alle ContactTelnr-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContactTelnrFromContactVerz = ContactTelnrVerzameling::fromContact($this);
		$dependencies['ContactTelnr'] = $ContactTelnrFromContactVerz;

		// Verzamel alle ContactMailingList-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContactMailingListFromContactVerz = ContactMailingListVerzameling::fromContact($this);
		$dependencies['ContactMailingList'] = $ContactMailingListFromContactVerz;

		// Verzamel alle Transactie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TransactieFromContactVerz = TransactieVerzameling::fromContact($this);
		$dependencies['Transactie'] = $TransactieFromContactVerz;

		return $dependencies;
	}
}
