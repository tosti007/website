<?
abstract class Register_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $label;
	protected $beschrijving;
	protected $waarde;					/**< \brief NULL */
	protected $type;					/**< \brief ENUM:STRING/INT/BOOLEAN/FLOAT */
	protected $auth;					/**< \brief ENUM:BESTUUR/GOD/INGELOGD */
	/**
	 * @brief De constructor van de Register_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->label = '';
		$this->beschrijving = '';
		$this->waarde = NULL;
		$this->type = 'STRING';
		$this->auth = 'BESTUUR';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld label.
	 *
	 * @return string
	 * De waarde van het veld label.
	 */
	public function getLabel()
	{
		return $this->label;
	}
	/**
	 * @brief Stel de waarde van het veld label in.
	 *
	 * @param mixed $newLabel De nieuwe waarde.
	 *
	 * @return Register
	 * Dit Register-object.
	 */
	public function setLabel($newLabel)
	{
		unset($this->errors['Label']);
		if(!is_null($newLabel))
			$newLabel = trim($newLabel);
		if($newLabel === "")
			$newLabel = NULL;
		if($this->label === $newLabel)
			return $this;

		$this->label = $newLabel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld label geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld label geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLabel()
	{
		if (array_key_exists('Label', $this->errors))
			return $this->errors['Label'];
		$waarde = $this->getLabel();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld beschrijving.
	 *
	 * @return string
	 * De waarde van het veld beschrijving.
	 */
	public function getBeschrijving()
	{
		return $this->beschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld beschrijving in.
	 *
	 * @param mixed $newBeschrijving De nieuwe waarde.
	 *
	 * @return Register
	 * Dit Register-object.
	 */
	public function setBeschrijving($newBeschrijving)
	{
		unset($this->errors['Beschrijving']);
		if(!is_null($newBeschrijving))
			$newBeschrijving = trim($newBeschrijving);
		if($newBeschrijving === "")
			$newBeschrijving = NULL;
		if($this->beschrijving === $newBeschrijving)
			return $this;

		$this->beschrijving = $newBeschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld beschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld beschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBeschrijving()
	{
		if (array_key_exists('Beschrijving', $this->errors))
			return $this->errors['Beschrijving'];
		$waarde = $this->getBeschrijving();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld waarde.
	 *
	 * @return string
	 * De waarde van het veld waarde.
	 */
	public function getWaarde()
	{
		return $this->waarde;
	}
	/**
	 * @brief Stel de waarde van het veld waarde in.
	 *
	 * @param mixed $newWaarde De nieuwe waarde.
	 *
	 * @return Register
	 * Dit Register-object.
	 */
	public function setWaarde($newWaarde)
	{
		unset($this->errors['Waarde']);
		if(!is_null($newWaarde))
			$newWaarde = trim($newWaarde);
		if($newWaarde === "")
			$newWaarde = NULL;
		if($this->waarde === $newWaarde)
			return $this;

		$this->waarde = $newWaarde;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld waarde geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld waarde geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkWaarde()
	{
		if (array_key_exists('Waarde', $this->errors))
			return $this->errors['Waarde'];
		$waarde = $this->getWaarde();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld type.
	 */
	static public function enumsType()
	{
		static $vals = array('STRING','INT','BOOLEAN','FLOAT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @return string
	 * De waarde van het veld type.
	 */
	public function getType()
	{
		return $this->type;
	}
	/**
	 * @brief Stel de waarde van het veld type in.
	 *
	 * @param mixed $newType De nieuwe waarde.
	 *
	 * @return Register
	 * Dit Register-object.
	 */
	public function setType($newType)
	{
		unset($this->errors['Type']);
		if(!is_null($newType))
			$newType = strtoupper(trim($newType));
		if($newType === "")
			$newType = NULL;
		if($this->type === $newType)
			return $this;

		$this->type = $newType;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld type geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld type geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkType()
	{
		if (array_key_exists('Type', $this->errors))
			return $this->errors['Type'];
		$waarde = $this->getType();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsType()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld auth.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld auth.
	 */
	static public function enumsAuth()
	{
		static $vals = array('BESTUUR','GOD','INGELOGD');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld auth.
	 *
	 * @return string
	 * De waarde van het veld auth.
	 */
	public function getAuth()
	{
		return $this->auth;
	}
	/**
	 * @brief Stel de waarde van het veld auth in.
	 *
	 * @param mixed $newAuth De nieuwe waarde.
	 *
	 * @return Register
	 * Dit Register-object.
	 */
	public function setAuth($newAuth)
	{
		unset($this->errors['Auth']);
		if(!is_null($newAuth))
			$newAuth = strtoupper(trim($newAuth));
		if($newAuth === "")
			$newAuth = NULL;
		if($this->auth === $newAuth)
			return $this;

		$this->auth = $newAuth;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld auth geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld auth geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAuth()
	{
		if (array_key_exists('Auth', $this->errors))
			return $this->errors['Auth'];
		$waarde = $this->getAuth();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsAuth()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Register::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Register.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Register::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Register|false
	 * Een Register-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'Register');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Register');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Register::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Register`.`id`'
		                 .     ', `Register`.`label`'
		                 .     ', `Register`.`beschrijving`'
		                 .     ', `Register`.`waarde`'
		                 .     ', `Register`.`type`'
		                 .     ', `Register`.`auth`'
		                 .     ', `Register`.`gewijzigdWanneer`'
		                 .     ', `Register`.`gewijzigdWie`'
		                 .' FROM `Register`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new Register();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->label  = trim($row['label']);
			$obj->beschrijving  = trim($row['beschrijving']);
			$obj->waarde  = (is_null($row['waarde'])) ? null : trim($row['waarde']);
			$obj->type  = strtoupper(trim($row['type']));
			$obj->auth  = strtoupper(trim($row['auth']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Register')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `Register`'
			          . ' (`label`, `beschrijving`, `waarde`, `type`, `auth`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %s, %s, %s, %i)'
			          , $this->label
			          , $this->beschrijving
			          , $this->waarde
			          , $this->type
			          , $this->auth
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Register')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Register`'
			          .' SET `label` = %s'
			          .   ', `beschrijving` = %s'
			          .   ', `waarde` = %s'
			          .   ', `type` = %s'
			          .   ', `auth` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->label
			          , $this->beschrijving
			          , $this->waarde
			          , $this->type
			          , $this->auth
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenRegister
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenRegister($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenRegister($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Label';
			$velden[] = 'Beschrijving';
			$velden[] = 'Waarde';
			$velden[] = 'Type';
			$velden[] = 'Auth';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Label';
			$velden[] = 'Beschrijving';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Label';
			$velden[] = 'Beschrijving';
			$velden[] = 'Waarde';
			$velden[] = 'Type';
			$velden[] = 'Auth';
			break;
		case 'get':
			$velden[] = 'Label';
			$velden[] = 'Beschrijving';
			$velden[] = 'Waarde';
			$velden[] = 'Type';
			$velden[] = 'Auth';
		case 'primary':
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Label';
			$velden[] = 'Beschrijving';
			$velden[] = 'Waarde';
			$velden[] = 'Type';
			$velden[] = 'Auth';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Register`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Register`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Register terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'label':
			return 'string';
		case 'beschrijving':
			return 'string';
		case 'waarde':
			return 'string';
		case 'type':
			return 'enum';
		case 'auth':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
			$type = '%i';
			break;
		case 'label':
		case 'beschrijving':
		case 'waarde':
		case 'type':
		case 'auth':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Register`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Register`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Register');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
