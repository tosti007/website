<?
abstract class CSPReport_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $ingelogdLid;				/**< \brief NULL */
	protected $ingelogdLid_contactID;	/**< \brief PRIMARY */
	protected $ip;
	protected $documentUri;
	protected $referrer;				/**< \brief NULL */
	protected $blockedUri;
	protected $violatedDirective;
	protected $originalPolicy;
	protected $userAgent;				/**< \brief NULL */
	/**
	 * @brief De constructor van de CSPReport_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->ingelogdLid = NULL;
		$this->ingelogdLid_contactID = NULL;
		$this->ip = '';
		$this->documentUri = '';
		$this->referrer = NULL;
		$this->blockedUri = '';
		$this->violatedDirective = '';
		$this->originalPolicy = '';
		$this->userAgent = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Stel de waarde van het veld id in.
	 *
	 * @param mixed $newId De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setId($newId)
	{
		unset($this->errors['Id']);
		if(!is_null($newId))
			$newId = (int)$newId;
		if($this->id === $newId)
			return $this;

		$this->id = $newId;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld id geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld id geldig is; anders een string met een
	 * foutmelding.
	 */
	public function checkId()
	{
		if (array_key_exists('Id', $this->errors))
			return $this->errors['Id'];
		$waarde = $this->getId();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ingelogdLid.
	 *
	 * @return Lid
	 * De waarde van het veld ingelogdLid.
	 */
	public function getIngelogdLid()
	{
		if(!isset($this->ingelogdLid)
		 && isset($this->ingelogdLid_contactID)
		 ) {
			$this->ingelogdLid = Lid::geef
					( $this->ingelogdLid_contactID
					);
		}
		return $this->ingelogdLid;
	}
	/**
	 * @brief Stel de waarde van het veld ingelogdLid in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setIngelogdLid($new_contactID)
	{
		unset($this->errors['IngelogdLid']);
		if($new_contactID instanceof Lid
		) {
			if($this->ingelogdLid == $new_contactID
			&& $this->ingelogdLid_contactID == $this->ingelogdLid->getContactID())
				return $this;
			$this->ingelogdLid = $new_contactID;
			$this->ingelogdLid_contactID
					= $this->ingelogdLid->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->ingelogdLid == NULL && $this->ingelogdLid_contactID == NULL)
				return $this;
			$this->ingelogdLid = NULL;
			$this->ingelogdLid_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->ingelogdLid == NULL 
				&& $this->ingelogdLid_contactID == (int)$new_contactID)
				return $this;
			$this->ingelogdLid = NULL;
			$this->ingelogdLid_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ingelogdLid geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ingelogdLid geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkIngelogdLid()
	{
		if (array_key_exists('IngelogdLid', $this->errors))
			return $this->errors['IngelogdLid'];
		$waarde1 = $this->getIngelogdLid();
		$waarde2 = $this->getIngelogdLidContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ingelogdLid_contactID.
	 *
	 * @return int
	 * De waarde van het veld ingelogdLid_contactID.
	 */
	public function getIngelogdLidContactID()
	{
		if (is_null($this->ingelogdLid_contactID) && isset($this->ingelogdLid)) {
			$this->ingelogdLid_contactID = $this->ingelogdLid->getContactID();
		}
		return $this->ingelogdLid_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld ip.
	 *
	 * @return string
	 * De waarde van het veld ip.
	 */
	public function getIp()
	{
		return $this->ip;
	}
	/**
	 * @brief Stel de waarde van het veld ip in.
	 *
	 * @param mixed $newIp De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setIp($newIp)
	{
		unset($this->errors['Ip']);
		if(!is_null($newIp))
			$newIp = trim($newIp);
		if($newIp === "")
			$newIp = NULL;
		if($this->ip === $newIp)
			return $this;

		$this->ip = $newIp;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ip geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ip geldig is; anders een string met een
	 * foutmelding.
	 */
	public function checkIp()
	{
		if (array_key_exists('Ip', $this->errors))
			return $this->errors['Ip'];
		$waarde = $this->getIp();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld documentUri.
	 *
	 * @return string
	 * De waarde van het veld documentUri.
	 */
	public function getDocumentUri()
	{
		return $this->documentUri;
	}
	/**
	 * @brief Stel de waarde van het veld documentUri in.
	 *
	 * @param mixed $newDocumentUri De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setDocumentUri($newDocumentUri)
	{
		unset($this->errors['DocumentUri']);
		if(!is_null($newDocumentUri))
			$newDocumentUri = trim($newDocumentUri);
		if($newDocumentUri === "")
			$newDocumentUri = NULL;
		if($this->documentUri === $newDocumentUri)
			return $this;

		$this->documentUri = $newDocumentUri;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld documentUri geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld documentUri geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDocumentUri()
	{
		if (array_key_exists('DocumentUri', $this->errors))
			return $this->errors['DocumentUri'];
		$waarde = $this->getDocumentUri();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld referrer.
	 *
	 * @return string
	 * De waarde van het veld referrer.
	 */
	public function getReferrer()
	{
		return $this->referrer;
	}
	/**
	 * @brief Stel de waarde van het veld referrer in.
	 *
	 * @param mixed $newReferrer De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setReferrer($newReferrer)
	{
		unset($this->errors['Referrer']);
		if(!is_null($newReferrer))
			$newReferrer = trim($newReferrer);
		if($newReferrer === "")
			$newReferrer = NULL;
		if($this->referrer === $newReferrer)
			return $this;

		$this->referrer = $newReferrer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld referrer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld referrer geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkReferrer()
	{
		if (array_key_exists('Referrer', $this->errors))
			return $this->errors['Referrer'];
		$waarde = $this->getReferrer();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld blockedUri.
	 *
	 * @return string
	 * De waarde van het veld blockedUri.
	 */
	public function getBlockedUri()
	{
		return $this->blockedUri;
	}
	/**
	 * @brief Stel de waarde van het veld blockedUri in.
	 *
	 * @param mixed $newBlockedUri De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setBlockedUri($newBlockedUri)
	{
		unset($this->errors['BlockedUri']);
		if(!is_null($newBlockedUri))
			$newBlockedUri = trim($newBlockedUri);
		if($newBlockedUri === "")
			$newBlockedUri = NULL;
		if($this->blockedUri === $newBlockedUri)
			return $this;

		$this->blockedUri = $newBlockedUri;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld blockedUri geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld blockedUri geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBlockedUri()
	{
		if (array_key_exists('BlockedUri', $this->errors))
			return $this->errors['BlockedUri'];
		$waarde = $this->getBlockedUri();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld violatedDirective.
	 *
	 * @return string
	 * De waarde van het veld violatedDirective.
	 */
	public function getViolatedDirective()
	{
		return $this->violatedDirective;
	}
	/**
	 * @brief Stel de waarde van het veld violatedDirective in.
	 *
	 * @param mixed $newViolatedDirective De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setViolatedDirective($newViolatedDirective)
	{
		unset($this->errors['ViolatedDirective']);
		if(!is_null($newViolatedDirective))
			$newViolatedDirective = trim($newViolatedDirective);
		if($newViolatedDirective === "")
			$newViolatedDirective = NULL;
		if($this->violatedDirective === $newViolatedDirective)
			return $this;

		$this->violatedDirective = $newViolatedDirective;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld violatedDirective geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld violatedDirective geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkViolatedDirective()
	{
		if (array_key_exists('ViolatedDirective', $this->errors))
			return $this->errors['ViolatedDirective'];
		$waarde = $this->getViolatedDirective();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld originalPolicy.
	 *
	 * @return string
	 * De waarde van het veld originalPolicy.
	 */
	public function getOriginalPolicy()
	{
		return $this->originalPolicy;
	}
	/**
	 * @brief Stel de waarde van het veld originalPolicy in.
	 *
	 * @param mixed $newOriginalPolicy De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setOriginalPolicy($newOriginalPolicy)
	{
		unset($this->errors['OriginalPolicy']);
		if(!is_null($newOriginalPolicy))
			$newOriginalPolicy = trim($newOriginalPolicy);
		if($newOriginalPolicy === "")
			$newOriginalPolicy = NULL;
		if($this->originalPolicy === $newOriginalPolicy)
			return $this;

		$this->originalPolicy = $newOriginalPolicy;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld originalPolicy geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld originalPolicy geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOriginalPolicy()
	{
		if (array_key_exists('OriginalPolicy', $this->errors))
			return $this->errors['OriginalPolicy'];
		$waarde = $this->getOriginalPolicy();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld userAgent.
	 *
	 * @return string
	 * De waarde van het veld userAgent.
	 */
	public function getUserAgent()
	{
		return $this->userAgent;
	}
	/**
	 * @brief Stel de waarde van het veld userAgent in.
	 *
	 * @param mixed $newUserAgent De nieuwe waarde.
	 *
	 * @return CSPReport
	 * Dit CSPReport-object.
	 */
	public function setUserAgent($newUserAgent)
	{
		unset($this->errors['UserAgent']);
		if(!is_null($newUserAgent))
			$newUserAgent = trim($newUserAgent);
		if($newUserAgent === "")
			$newUserAgent = NULL;
		if($this->userAgent === $newUserAgent)
			return $this;

		$this->userAgent = $newUserAgent;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld userAgent geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld userAgent geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkUserAgent()
	{
		if (array_key_exists('UserAgent', $this->errors))
			return $this->errors['UserAgent'];
		$waarde = $this->getUserAgent();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return CSPReport::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van CSPReport.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return CSPReport::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return CSPReport|false
	 * Een CSPReport-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'CSPReport');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'CSPReport');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('CSPReport::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `CSPReport`.`id`'
		                 .     ', `CSPReport`.`ingelogdLid_contactID`'
		                 .     ', `CSPReport`.`ip`'
		                 .     ', `CSPReport`.`documentUri`'
		                 .     ', `CSPReport`.`referrer`'
		                 .     ', `CSPReport`.`blockedUri`'
		                 .     ', `CSPReport`.`violatedDirective`'
		                 .     ', `CSPReport`.`originalPolicy`'
		                 .     ', `CSPReport`.`userAgent`'
		                 .     ', `CSPReport`.`gewijzigdWanneer`'
		                 .     ', `CSPReport`.`gewijzigdWie`'
		                 .' FROM `CSPReport`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new CSPReport();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->ingelogdLid_contactID  = (is_null($row['ingelogdLid_contactID'])) ? null : (int) $row['ingelogdLid_contactID'];
			$obj->ip  = trim($row['ip']);
			$obj->documentUri  = trim($row['documentUri']);
			$obj->referrer  = (is_null($row['referrer'])) ? null : trim($row['referrer']);
			$obj->blockedUri  = trim($row['blockedUri']);
			$obj->violatedDirective  = trim($row['violatedDirective']);
			$obj->originalPolicy  = trim($row['originalPolicy']);
			$obj->userAgent  = (is_null($row['userAgent'])) ? null : trim($row['userAgent']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'CSPReport')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getIngelogdLidContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `CSPReport`'
			          . ' (`ingelogdLid_contactID`, `ip`, `documentUri`, `referrer`, `blockedUri`, `violatedDirective`, `originalPolicy`, `userAgent`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %s, %s, %s, %i)'
			          , $this->ingelogdLid_contactID
			          , $this->ip
			          , $this->documentUri
			          , $this->referrer
			          , $this->blockedUri
			          , $this->violatedDirective
			          , $this->originalPolicy
			          , $this->userAgent
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'CSPReport')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `CSPReport`'
			          .' SET `ingelogdLid_contactID` = %i'
			          .   ', `ip` = %s'
			          .   ', `documentUri` = %s'
			          .   ', `referrer` = %s'
			          .   ', `blockedUri` = %s'
			          .   ', `violatedDirective` = %s'
			          .   ', `originalPolicy` = %s'
			          .   ', `userAgent` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->ingelogdLid_contactID
			          , $this->ip
			          , $this->documentUri
			          , $this->referrer
			          , $this->blockedUri
			          , $this->violatedDirective
			          , $this->originalPolicy
			          , $this->userAgent
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenCSPReport
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenCSPReport($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenCSPReport($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'IngelogdLid';
			$velden[] = 'Ip';
			$velden[] = 'DocumentUri';
			$velden[] = 'Referrer';
			$velden[] = 'BlockedUri';
			$velden[] = 'ViolatedDirective';
			$velden[] = 'OriginalPolicy';
			$velden[] = 'UserAgent';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Ip';
			$velden[] = 'DocumentUri';
			$velden[] = 'BlockedUri';
			$velden[] = 'ViolatedDirective';
			$velden[] = 'OriginalPolicy';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'IngelogdLid';
			$velden[] = 'Ip';
			$velden[] = 'DocumentUri';
			$velden[] = 'Referrer';
			$velden[] = 'BlockedUri';
			$velden[] = 'ViolatedDirective';
			$velden[] = 'OriginalPolicy';
			$velden[] = 'UserAgent';
			break;
		case 'get':
			$velden[] = 'IngelogdLid';
			$velden[] = 'Ip';
			$velden[] = 'DocumentUri';
			$velden[] = 'Referrer';
			$velden[] = 'BlockedUri';
			$velden[] = 'ViolatedDirective';
			$velden[] = 'OriginalPolicy';
			$velden[] = 'UserAgent';
		case 'primary':
			$velden[] = 'ingelogdLid_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'IngelogdLid';
			$velden[] = 'Ip';
			$velden[] = 'DocumentUri';
			$velden[] = 'Referrer';
			$velden[] = 'BlockedUri';
			$velden[] = 'ViolatedDirective';
			$velden[] = 'OriginalPolicy';
			$velden[] = 'UserAgent';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `CSPReport`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `CSPReport`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van CSPReport terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'ingelogdlid':
			return 'foreign';
		case 'ingelogdlid_contactid':
			return 'int';
		case 'ip':
			return 'string';
		case 'documenturi':
			return 'string';
		case 'referrer':
			return 'string';
		case 'blockeduri':
			return 'string';
		case 'violateddirective':
			return 'string';
		case 'originalpolicy':
			return 'string';
		case 'useragent':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'ingelogdLid_contactID':
			$type = '%i';
			break;
		case 'ip':
		case 'documentUri':
		case 'referrer':
		case 'blockedUri':
		case 'violatedDirective':
		case 'originalPolicy':
		case 'userAgent':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `CSPReport`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `CSPReport`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'CSPReport');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
