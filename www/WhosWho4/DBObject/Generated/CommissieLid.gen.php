<?
/**
 * @brief 'AT'AUTH_GET:gast,AUTH_SET:bestuur
 */
abstract class CommissieLid_Generated
	extends Entiteit
{
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $commissie;				/**< \brief PRIMARY */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $datumBegin;				/**< \brief NULL */
	protected $datumEind;				/**< \brief NULL */
	protected $spreuk;					/**< \brief NULL */
	protected $spreukZichtbaar;
	protected $functie;					/**< \brief ENUM:OVERIG/VOORZ/SECR/PENNY/PR/SPONS/BESTUUR/SFEER */
	protected $functieNaam;				/**< \brief NULL */
	/**
	/**
	 * @brief De constructor van de CommissieLid_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 * @param mixed $b Commissie (Commissie OR Array(commissie_commissieID) OR
	 * commissie_commissieID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Commissie)
		{
			$this->commissie = $b;
			$this->commissie_commissieID = $b->getCommissieID();
		}
		else if(is_array($b))
		{
			$this->commissie = NULL;
			$this->commissie_commissieID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->commissie = NULL;
			$this->commissie_commissieID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->datumBegin = new DateTimeLocale(NULL);
		$this->datumEind = new DateTimeLocale(NULL);
		$this->spreuk = NULL;
		$this->spreukZichtbaar = False;
		$this->functie = 'OVERIG';
		$this->functieNaam = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		$volgorde[] = 'Commissie';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumBegin.
	 */
	public function getDatumBegin()
	{
		return $this->datumBegin;
	}
	/**
	 * @brief Stel de waarde van het veld datumBegin in.
	 *
	 * @param mixed $newDatumBegin De nieuwe waarde.
	 *
	 * @return CommissieLid
	 * Dit CommissieLid-object.
	 */
	public function setDatumBegin($newDatumBegin)
	{
		unset($this->errors['DatumBegin']);
		if(!$newDatumBegin instanceof DateTimeLocale) {
			try {
				$newDatumBegin = new DateTimeLocale($newDatumBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumBegin->strftime('%F %T') == $newDatumBegin->strftime('%F %T'))
			return $this;

		$this->datumBegin = $newDatumBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumBegin()
	{
		if (array_key_exists('DatumBegin', $this->errors))
			return $this->errors['DatumBegin'];
		$waarde = $this->getDatumBegin();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumEind.
	 */
	public function getDatumEind()
	{
		return $this->datumEind;
	}
	/**
	 * @brief Stel de waarde van het veld datumEind in.
	 *
	 * @param mixed $newDatumEind De nieuwe waarde.
	 *
	 * @return CommissieLid
	 * Dit CommissieLid-object.
	 */
	public function setDatumEind($newDatumEind)
	{
		unset($this->errors['DatumEind']);
		if(!$newDatumEind instanceof DateTimeLocale) {
			try {
				$newDatumEind = new DateTimeLocale($newDatumEind);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumEind->strftime('%F %T') == $newDatumEind->strftime('%F %T'))
			return $this;

		$this->datumEind = $newDatumEind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumEind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumEind geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumEind()
	{
		if (array_key_exists('DatumEind', $this->errors))
			return $this->errors['DatumEind'];
		$waarde = $this->getDatumEind();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld spreuk.
	 *
	 * @return string
	 * De waarde van het veld spreuk.
	 */
	public function getSpreuk()
	{
		return $this->spreuk;
	}
	/**
	 * @brief Stel de waarde van het veld spreuk in.
	 *
	 * @param mixed $newSpreuk De nieuwe waarde.
	 *
	 * @return CommissieLid
	 * Dit CommissieLid-object.
	 */
	public function setSpreuk($newSpreuk)
	{
		unset($this->errors['Spreuk']);
		if(!is_null($newSpreuk))
			$newSpreuk = trim($newSpreuk);
		if($newSpreuk === "")
			$newSpreuk = NULL;
		if($this->spreuk === $newSpreuk)
			return $this;

		$this->spreuk = $newSpreuk;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld spreuk geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld spreuk geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSpreuk()
	{
		if (array_key_exists('Spreuk', $this->errors))
			return $this->errors['Spreuk'];
		$waarde = $this->getSpreuk();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld spreukZichtbaar.
	 *
	 * @return bool
	 * De waarde van het veld spreukZichtbaar.
	 */
	public function getSpreukZichtbaar()
	{
		return $this->spreukZichtbaar;
	}
	/**
	 * @brief Stel de waarde van het veld spreukZichtbaar in.
	 *
	 * @param mixed $newSpreukZichtbaar De nieuwe waarde.
	 *
	 * @return CommissieLid
	 * Dit CommissieLid-object.
	 */
	public function setSpreukZichtbaar($newSpreukZichtbaar)
	{
		unset($this->errors['SpreukZichtbaar']);
		if(!is_null($newSpreukZichtbaar))
			$newSpreukZichtbaar = (bool)$newSpreukZichtbaar;
		if($this->spreukZichtbaar === $newSpreukZichtbaar)
			return $this;

		$this->spreukZichtbaar = $newSpreukZichtbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld spreukZichtbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld spreukZichtbaar geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkSpreukZichtbaar()
	{
		if (array_key_exists('SpreukZichtbaar', $this->errors))
			return $this->errors['SpreukZichtbaar'];
		$waarde = $this->getSpreukZichtbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld functie.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld functie.
	 */
	static public function enumsFunctie()
	{
		static $vals = array('OVERIG','VOORZ','SECR','PENNY','PR','SPONS','BESTUUR','SFEER');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld functie.
	 *
	 * @return string
	 * De waarde van het veld functie.
	 */
	public function getFunctie()
	{
		return $this->functie;
	}
	/**
	 * @brief Stel de waarde van het veld functie in.
	 *
	 * @param mixed $newFunctie De nieuwe waarde.
	 *
	 * @return CommissieLid
	 * Dit CommissieLid-object.
	 */
	public function setFunctie($newFunctie)
	{
		unset($this->errors['Functie']);
		if(!is_null($newFunctie))
			$newFunctie = strtoupper(trim($newFunctie));
		if($newFunctie === "")
			$newFunctie = NULL;
		if($this->functie === $newFunctie)
			return $this;

		$this->functie = $newFunctie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld functie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld functie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkFunctie()
	{
		if (array_key_exists('Functie', $this->errors))
			return $this->errors['Functie'];
		$waarde = $this->getFunctie();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsFunctie()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld functieNaam.
	 *
	 * @return string
	 * De waarde van het veld functieNaam.
	 */
	public function getFunctieNaam()
	{
		return $this->functieNaam;
	}
	/**
	 * @brief Stel de waarde van het veld functieNaam in.
	 *
	 * @param mixed $newFunctieNaam De nieuwe waarde.
	 *
	 * @return CommissieLid
	 * Dit CommissieLid-object.
	 */
	public function setFunctieNaam($newFunctieNaam)
	{
		unset($this->errors['FunctieNaam']);
		if(!is_null($newFunctieNaam))
			$newFunctieNaam = trim($newFunctieNaam);
		if($newFunctieNaam === "")
			$newFunctieNaam = NULL;
		if($this->functieNaam === $newFunctieNaam)
			return $this;

		$this->functieNaam = $newFunctieNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld functieNaam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld functieNaam geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkFunctieNaam()
	{
		if (array_key_exists('FunctieNaam', $this->errors))
			return $this->errors['FunctieNaam'];
		$waarde = $this->getFunctieNaam();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return CommissieLid::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van CommissieLid.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return CommissieLid::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPersoonContactID()
		                      , $this->getCommissieCommissieID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return CommissieLid|false
	 * Een CommissieLid-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$persoon_contactID = (int)$a[0];
			$commissie_commissieID = (int)$a[1];
		}
		else if($a instanceof Persoon
		     && $b instanceof Commissie)
		{
			$persoon_contactID = $a->getContactID();
			$commissie_commissieID = $b->getCommissieID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$persoon_contactID = (int)$a;
			$commissie_commissieID = (int)$b;
		}

		if(is_null($persoon_contactID)
		|| is_null($commissie_commissieID))
			throw new BadMethodCallException();

		static::cache(array( array($persoon_contactID, $commissie_commissieID) ));
		return Entiteit::geefCache(array($persoon_contactID, $commissie_commissieID), 'CommissieLid');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'CommissieLid');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('CommissieLid::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `CommissieLid`.`persoon_contactID`'
		                 .     ', `CommissieLid`.`commissie_commissieID`'
		                 .     ', `CommissieLid`.`datumBegin`'
		                 .     ', `CommissieLid`.`datumEind`'
		                 .     ', `CommissieLid`.`spreuk`'
		                 .     ', `CommissieLid`.`spreukZichtbaar`'
		                 .     ', `CommissieLid`.`functie`'
		                 .     ', `CommissieLid`.`functieNaam`'
		                 .     ', `CommissieLid`.`gewijzigdWanneer`'
		                 .     ', `CommissieLid`.`gewijzigdWie`'
		                 .' FROM `CommissieLid`'
		                 .' WHERE (`persoon_contactID`, `commissie_commissieID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['persoon_contactID']
			                  ,$row['commissie_commissieID']);

			$obj = new CommissieLid(array($row['persoon_contactID']), array($row['commissie_commissieID']));

			$obj->inDB = True;

			$obj->datumBegin  = new DateTimeLocale($row['datumBegin']);
			$obj->datumEind  = new DateTimeLocale($row['datumEind']);
			$obj->spreuk  = (is_null($row['spreuk'])) ? null : trim($row['spreuk']);
			$obj->spreukZichtbaar  = (bool) $row['spreukZichtbaar'];
			$obj->functie  = strtoupper(trim($row['functie']));
			$obj->functieNaam  = (is_null($row['functieNaam'])) ? null : trim($row['functieNaam']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'CommissieLid')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		$rel = $this->getCommissie();
		if(!$rel->getInDB())
			throw new LogicException('foreign Commissie is not in DB');
		$this->commissie_commissieID = $rel->getCommissieID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `CommissieLid`'
			          . ' (`persoon_contactID`, `commissie_commissieID`, `datumBegin`, `datumEind`, `spreuk`, `spreukZichtbaar`, `functie`, `functieNaam`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %s, %i, %s, %s, %s, %i)'
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          , (!is_null($this->datumBegin))?$this->datumBegin->strftime('%F %T'):null
			          , (!is_null($this->datumEind))?$this->datumEind->strftime('%F %T'):null
			          , $this->spreuk
			          , $this->spreukZichtbaar
			          , $this->functie
			          , $this->functieNaam
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'CommissieLid')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `CommissieLid`'
			          .' SET `datumBegin` = %s'
			          .   ', `datumEind` = %s'
			          .   ', `spreuk` = %s'
			          .   ', `spreukZichtbaar` = %i'
			          .   ', `functie` = %s'
			          .   ', `functieNaam` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          .  ' AND `commissie_commissieID` = %i'
			          , (!is_null($this->datumBegin))?$this->datumBegin->strftime('%F %T'):null
			          , (!is_null($this->datumEind))?$this->datumEind->strftime('%F %T'):null
			          , $this->spreuk
			          , $this->spreukZichtbaar
			          , $this->functie
			          , $this->functieNaam
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenCommissieLid
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenCommissieLid($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenCommissieLid($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Spreuk';
			$velden[] = 'SpreukZichtbaar';
			$velden[] = 'Functie';
			$velden[] = 'FunctieNaam';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Spreuk';
			$velden[] = 'SpreukZichtbaar';
			$velden[] = 'Functie';
			$velden[] = 'FunctieNaam';
			break;
		case 'get':
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Spreuk';
			$velden[] = 'SpreukZichtbaar';
			$velden[] = 'Functie';
			$velden[] = 'FunctieNaam';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'commissie_commissieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Spreuk';
			$velden[] = 'SpreukZichtbaar';
			$velden[] = 'Functie';
			$velden[] = 'FunctieNaam';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `CommissieLid`'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `commissie_commissieID` = %i'
		          .' LIMIT 1'
		          , $this->persoon_contactID
		          , $this->commissie_commissieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `CommissieLid`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          .  ' AND `commissie_commissieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van CommissieLid terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'datumbegin':
			return 'date';
		case 'datumeind':
			return 'date';
		case 'spreuk':
			return 'string';
		case 'spreukzichtbaar':
			return 'bool';
		case 'functie':
			return 'enum';
		case 'functienaam':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'persoon_contactID':
		case 'commissie_commissieID':
		case 'spreukZichtbaar':
			$type = '%i';
			break;
		case 'datumBegin':
		case 'datumEind':
		case 'spreuk':
		case 'functie':
		case 'functieNaam':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `CommissieLid`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `commissie_commissieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->persoon_contactID
		          , $this->commissie_commissieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `CommissieLid`'
		          .' WHERE `persoon_contactID` = %i'
		          .  ' AND `commissie_commissieID` = %i'
		                 , $veld
		          , $this->persoon_contactID
		          , $this->commissie_commissieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'CommissieLid');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
