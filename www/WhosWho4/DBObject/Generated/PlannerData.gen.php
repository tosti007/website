<?
abstract class PlannerData_Generated
	extends Entiteit
{
	protected $plannerDataID;			/**< \brief PRIMARY */
	protected $planner;
	protected $planner_plannerID;		/**< \brief PRIMARY */
	protected $momentBegin;
	/** Verzamelingen **/
	protected $plannerDeelnemerVerzameling;
	/**
	 * @brief De constructor van de PlannerData_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->plannerDataID = NULL;
		$this->planner = NULL;
		$this->planner_plannerID = 0;
		$this->momentBegin = new DateTimeLocale();
		$this->plannerDeelnemerVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld plannerDataID.
	 *
	 * @return int
	 * De waarde van het veld plannerDataID.
	 */
	public function getPlannerDataID()
	{
		return $this->plannerDataID;
	}
	/**
	 * @brief Geef de waarde van het veld planner.
	 *
	 * @return Planner
	 * De waarde van het veld planner.
	 */
	public function getPlanner()
	{
		if(!isset($this->planner)
		 && isset($this->planner_plannerID)
		 ) {
			$this->planner = Planner::geef
					( $this->planner_plannerID
					);
		}
		return $this->planner;
	}
	/**
	 * @brief Stel de waarde van het veld planner in.
	 *
	 * @param mixed $new_plannerID De nieuwe waarde.
	 *
	 * @return PlannerData
	 * Dit PlannerData-object.
	 */
	public function setPlanner($new_plannerID)
	{
		unset($this->errors['Planner']);
		if($new_plannerID instanceof Planner
		) {
			if($this->planner == $new_plannerID
			&& $this->planner_plannerID == $this->planner->getPlannerID())
				return $this;
			$this->planner = $new_plannerID;
			$this->planner_plannerID
					= $this->planner->getPlannerID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_plannerID)
		) {
			if($this->planner == NULL 
				&& $this->planner_plannerID == (int)$new_plannerID)
				return $this;
			$this->planner = NULL;
			$this->planner_plannerID
					= (int)$new_plannerID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld planner geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld planner geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPlanner()
	{
		if (array_key_exists('Planner', $this->errors))
			return $this->errors['Planner'];
		$waarde1 = $this->getPlanner();
		$waarde2 = $this->getPlannerPlannerID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld planner_plannerID.
	 *
	 * @return int
	 * De waarde van het veld planner_plannerID.
	 */
	public function getPlannerPlannerID()
	{
		if (is_null($this->planner_plannerID) && isset($this->planner)) {
			$this->planner_plannerID = $this->planner->getPlannerID();
		}
		return $this->planner_plannerID;
	}
	/**
	 * @brief Geef de waarde van het veld momentBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld momentBegin.
	 */
	public function getMomentBegin()
	{
		return $this->momentBegin;
	}
	/**
	 * @brief Stel de waarde van het veld momentBegin in.
	 *
	 * @param mixed $newMomentBegin De nieuwe waarde.
	 *
	 * @return PlannerData
	 * Dit PlannerData-object.
	 */
	public function setMomentBegin($newMomentBegin)
	{
		unset($this->errors['MomentBegin']);
		if(!$newMomentBegin instanceof DateTimeLocale) {
			try {
				$newMomentBegin = new DateTimeLocale($newMomentBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->momentBegin->strftime('%F %T') == $newMomentBegin->strftime('%F %T'))
			return $this;

		$this->momentBegin = $newMomentBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld momentBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld momentBegin geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMomentBegin()
	{
		if (array_key_exists('MomentBegin', $this->errors))
			return $this->errors['MomentBegin'];
		$waarde = $this->getMomentBegin();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de PlannerDeelnemerVerzameling die hoort bij dit object.
	 */
	public function getPlannerDeelnemerVerzameling()
	{
		if(!$this->plannerDeelnemerVerzameling instanceof PlannerDeelnemerVerzameling)
			$this->plannerDeelnemerVerzameling = PlannerDeelnemerVerzameling::fromPlannerData($this);
		return $this->plannerDeelnemerVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return PlannerData::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van PlannerData.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return PlannerData::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getPlannerDataID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return PlannerData|false
	 * Een PlannerData-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$plannerDataID = (int)$a[0];
		}
		else if(isset($a))
		{
			$plannerDataID = (int)$a;
		}

		if(is_null($plannerDataID))
			throw new BadMethodCallException();

		static::cache(array( array($plannerDataID) ));
		return Entiteit::geefCache(array($plannerDataID), 'PlannerData');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'PlannerData');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('PlannerData::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `PlannerData`.`plannerDataID`'
		                 .     ', `PlannerData`.`planner_plannerID`'
		                 .     ', `PlannerData`.`momentBegin`'
		                 .     ', `PlannerData`.`gewijzigdWanneer`'
		                 .     ', `PlannerData`.`gewijzigdWie`'
		                 .' FROM `PlannerData`'
		                 .' WHERE (`plannerDataID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['plannerDataID']);

			$obj = new PlannerData();

			$obj->inDB = True;

			$obj->plannerDataID  = (int) $row['plannerDataID'];
			$obj->planner_plannerID  = (int) $row['planner_plannerID'];
			$obj->momentBegin  = new DateTimeLocale($row['momentBegin']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'PlannerData')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getPlannerPlannerID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->plannerDataID =
			$WSW4DB->q('RETURNID INSERT INTO `PlannerData`'
			          . ' (`planner_plannerID`, `momentBegin`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i)'
			          , $this->planner_plannerID
			          , $this->momentBegin->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'PlannerData')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `PlannerData`'
			          .' SET `planner_plannerID` = %i'
			          .   ', `momentBegin` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `plannerDataID` = %i'
			          , $this->planner_plannerID
			          , $this->momentBegin->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->plannerDataID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPlannerData
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPlannerData($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPlannerData($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Planner';
			$velden[] = 'MomentBegin';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Planner';
			$velden[] = 'MomentBegin';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Planner';
			$velden[] = 'MomentBegin';
			break;
		case 'get':
			$velden[] = 'Planner';
			$velden[] = 'MomentBegin';
		case 'primary':
			$velden[] = 'planner_plannerID';
			break;
		case 'verzamelingen':
			$velden[] = 'PlannerDeelnemerVerzameling';
			break;
		default:
			$velden[] = 'Planner';
			$velden[] = 'MomentBegin';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle PlannerDeelnemer-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PlannerDeelnemerFromPlannerDataVerz = PlannerDeelnemerVerzameling::fromPlannerData($this);
		$returnValue = $PlannerDeelnemerFromPlannerDataVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PlannerDeelnemer met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $PlannerDeelnemerFromPlannerDataVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `PlannerData`'
		          .' WHERE `plannerDataID` = %i'
		          .' LIMIT 1'
		          , $this->plannerDataID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->plannerDataID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `PlannerData`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `plannerDataID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->plannerDataID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van PlannerData terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'plannerdataid':
			return 'int';
		case 'planner':
			return 'foreign';
		case 'planner_plannerid':
			return 'int';
		case 'momentbegin':
			return 'datetime';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'plannerDataID':
		case 'planner_plannerID':
			$type = '%i';
			break;
		case 'momentBegin':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `PlannerData`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `plannerDataID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->plannerDataID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `PlannerData`'
		          .' WHERE `plannerDataID` = %i'
		                 , $veld
		          , $this->plannerDataID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'PlannerData');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle PlannerDeelnemer-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PlannerDeelnemerFromPlannerDataVerz = PlannerDeelnemerVerzameling::fromPlannerData($this);
		$dependencies['PlannerDeelnemer'] = $PlannerDeelnemerFromPlannerDataVerz;

		return $dependencies;
	}
}
