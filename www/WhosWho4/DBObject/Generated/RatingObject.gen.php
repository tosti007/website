<?
abstract class RatingObject_Generated
	extends Entiteit
{
	protected $ratingObjectID;			/**< \brief PRIMARY */
	protected $rating;
	/** Verzamelingen **/
	protected $persoonRatingVerzameling;
	/**
	 * @brief De constructor van de RatingObject_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->ratingObjectID = NULL;
		$this->rating = 50;
		$this->persoonRatingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld ratingObjectID.
	 *
	 * @return int
	 * De waarde van het veld ratingObjectID.
	 */
	public function getRatingObjectID()
	{
		return $this->ratingObjectID;
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @return int
	 * De waarde van het veld rating.
	 */
	public function getRating()
	{
		return $this->rating;
	}
	/**
	 * @brief Stel de waarde van het veld rating in.
	 *
	 * @param mixed $newRating De nieuwe waarde.
	 *
	 * @return RatingObject
	 * Dit RatingObject-object.
	 */
	public function setRating($newRating)
	{
		unset($this->errors['Rating']);
		if(!is_null($newRating))
			$newRating = (int)$newRating;
		if($this->rating === $newRating)
			return $this;

		$this->rating = $newRating;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rating geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rating geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkRating()
	{
		if (array_key_exists('Rating', $this->errors))
			return $this->errors['Rating'];
		$waarde = $this->getRating();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de PersoonRatingVerzameling die hoort bij dit object.
	 */
	public function getPersoonRatingVerzameling()
	{
		if(!$this->persoonRatingVerzameling instanceof PersoonRatingVerzameling)
			$this->persoonRatingVerzameling = PersoonRatingVerzameling::fromRatingObject($this);
		return $this->persoonRatingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return RatingObject::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van RatingObject.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return RatingObject::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getRatingObjectID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return RatingObject|false
	 * Een RatingObject-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$ratingObjectID = (int)$a[0];
		}
		else if(isset($a))
		{
			$ratingObjectID = (int)$a;
		}

		if(is_null($ratingObjectID))
			throw new BadMethodCallException();

		static::cache(array( array($ratingObjectID) ));
		return Entiteit::geefCache(array($ratingObjectID), 'RatingObject');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'RatingObject');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('RatingObject::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `RatingObject`.`ratingObjectID`'
		                 .     ', `RatingObject`.`rating`'
		                 .     ', `RatingObject`.`gewijzigdWanneer`'
		                 .     ', `RatingObject`.`gewijzigdWie`'
		                 .' FROM `RatingObject`'
		                 .' WHERE (`ratingObjectID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['ratingObjectID']);

			$obj = new RatingObject();

			$obj->inDB = True;

			$obj->ratingObjectID  = (int) $row['ratingObjectID'];
			$obj->rating  = (int) $row['rating'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'RatingObject')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->ratingObjectID =
			$WSW4DB->q('RETURNID INSERT INTO `RatingObject`'
			          . ' (`rating`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %i)'
			          , $this->rating
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'RatingObject')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `RatingObject`'
			          .' SET `rating` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `ratingObjectID` = %i'
			          , $this->rating
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->ratingObjectID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenRatingObject
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenRatingObject($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenRatingObject($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Rating';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Rating';
			break;
		case 'get':
			$velden[] = 'Rating';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'PersoonRatingVerzameling';
			break;
		default:
			$velden[] = 'Rating';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle PersoonRating-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonRatingFromRatingObjectVerz = PersoonRatingVerzameling::fromRatingObject($this);
		$returnValue = $PersoonRatingFromRatingObjectVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonRating met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $PersoonRatingFromRatingObjectVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `RatingObject`'
		          .' WHERE `ratingObjectID` = %i'
		          .' LIMIT 1'
		          , $this->ratingObjectID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->ratingObjectID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `RatingObject`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `ratingObjectID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->ratingObjectID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van RatingObject terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'ratingobjectid':
			return 'int';
		case 'rating':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `RatingObject`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `ratingObjectID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->ratingObjectID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `RatingObject`'
		          .' WHERE `ratingObjectID` = %i'
		                 , $veld
		          , $this->ratingObjectID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'RatingObject');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle PersoonRating-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonRatingFromRatingObjectVerz = PersoonRatingVerzameling::fromRatingObject($this);
		$dependencies['PersoonRating'] = $PersoonRatingFromRatingObjectVerz;

		return $dependencies;
	}
}
