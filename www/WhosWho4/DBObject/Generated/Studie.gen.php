<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class Studie_Generated
	extends Entiteit
{
	protected $studieID;				/**< \brief PRIMARY */
	protected $naam;					/**< \brief LANG */
	protected $soort;					/**< \brief ENUM:GT/IC/IK/NA/WI/WIT/OVERIG */
	protected $fase;					/**< \brief ENUM:BA/MA */
	/** Verzamelingen **/
	protected $lidStudieVerzameling;
	protected $bedrijfStudieVerzameling;
	protected $vacatureStudieVerzameling;
	protected $vakStudieVerzameling;
	/**
	 * @brief De constructor van de Studie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->studieID = NULL;
		$this->naam = array('nl' => '', 'en' => '');
		$this->soort = 'GT';
		$this->fase = 'BA';
		$this->lidStudieVerzameling = NULL;
		$this->bedrijfStudieVerzameling = NULL;
		$this->vacatureStudieVerzameling = NULL;
		$this->vakStudieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld studieID.
	 *
	 * @return int
	 * De waarde van het veld studieID.
	 */
	public function getStudieID()
	{
		return $this->studieID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->naam[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Studie
	 * Dit Studie-object.
	 */
	public function setNaam($newNaam, $lang = NULL)
	{
		unset($this->errors['Naam']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam[$lang] === $newNaam)
			return $this;

		$this->naam[$lang] = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkNaam('nl');
			$ret['en'] = $this->checkNaam('en');
			return $ret;
		}

		if (array_key_exists('Naam', $this->errors))
			if (is_array($this->errors['Naam']) && array_key_exists($lang, $this->errors['Naam']))
				return $this->errors['Naam'][$lang];
		$waarde = $this->getNaam($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('GT','IC','IK','NA','WI','WIT','OVERIG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return Studie
	 * Dit Studie-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld fase.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld fase.
	 */
	static public function enumsFase()
	{
		static $vals = array('BA','MA');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld fase.
	 *
	 * @return string
	 * De waarde van het veld fase.
	 */
	public function getFase()
	{
		return $this->fase;
	}
	/**
	 * @brief Stel de waarde van het veld fase in.
	 *
	 * @param mixed $newFase De nieuwe waarde.
	 *
	 * @return Studie
	 * Dit Studie-object.
	 */
	public function setFase($newFase)
	{
		unset($this->errors['Fase']);
		if(!is_null($newFase))
			$newFase = strtoupper(trim($newFase));
		if($newFase === "")
			$newFase = NULL;
		if($this->fase === $newFase)
			return $this;

		$this->fase = $newFase;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld fase geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld fase geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkFase()
	{
		if (array_key_exists('Fase', $this->errors))
			return $this->errors['Fase'];
		$waarde = $this->getFase();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsFase()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Returneert de LidStudieVerzameling die hoort bij dit object.
	 */
	public function getLidStudieVerzameling()
	{
		if(!$this->lidStudieVerzameling instanceof LidStudieVerzameling)
			$this->lidStudieVerzameling = LidStudieVerzameling::fromStudie($this);
		return $this->lidStudieVerzameling;
	}
	/**
	 * @brief Returneert de BedrijfStudieVerzameling die hoort bij dit object.
	 */
	public function getBedrijfStudieVerzameling()
	{
		if(!$this->bedrijfStudieVerzameling instanceof BedrijfStudieVerzameling)
			$this->bedrijfStudieVerzameling = BedrijfStudieVerzameling::fromStudie($this);
		return $this->bedrijfStudieVerzameling;
	}
	/**
	 * @brief Returneert de VacatureStudieVerzameling die hoort bij dit object.
	 */
	public function getVacatureStudieVerzameling()
	{
		if(!$this->vacatureStudieVerzameling instanceof VacatureStudieVerzameling)
			$this->vacatureStudieVerzameling = VacatureStudieVerzameling::fromStudie($this);
		return $this->vacatureStudieVerzameling;
	}
	/**
	 * @brief Returneert de VakStudieVerzameling die hoort bij dit object.
	 */
	public function getVakStudieVerzameling()
	{
		if(!$this->vakStudieVerzameling instanceof VakStudieVerzameling)
			$this->vakStudieVerzameling = VakStudieVerzameling::fromStudie($this);
		return $this->vakStudieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Studie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Studie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Studie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getStudieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Studie|false
	 * Een Studie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$studieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$studieID = (int)$a;
		}

		if(is_null($studieID))
			throw new BadMethodCallException();

		static::cache(array( array($studieID) ));
		return Entiteit::geefCache(array($studieID), 'Studie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Studie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Studie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Studie`.`studieID`'
		                 .     ', `Studie`.`naam_NL`'
		                 .     ', `Studie`.`naam_EN`'
		                 .     ', `Studie`.`soort`'
		                 .     ', `Studie`.`fase`'
		                 .     ', `Studie`.`gewijzigdWanneer`'
		                 .     ', `Studie`.`gewijzigdWie`'
		                 .' FROM `Studie`'
		                 .' WHERE (`studieID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['studieID']);

			$obj = new Studie();

			$obj->inDB = True;

			$obj->studieID  = (int) $row['studieID'];
			$obj->naam['nl'] = $row['naam_NL'];
			$obj->naam['en'] = $row['naam_EN'];
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->fase  = strtoupper(trim($row['fase']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Studie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->studieID =
			$WSW4DB->q('RETURNID INSERT INTO `Studie`'
			          . ' (`naam_NL`, `naam_EN`, `soort`, `fase`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %s, %s, %i)'
			          , $this->naam['nl']
			          , $this->naam['en']
			          , $this->soort
			          , $this->fase
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Studie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Studie`'
			          .' SET `naam_NL` = %s'
			          .   ', `naam_EN` = %s'
			          .   ', `soort` = %s'
			          .   ', `fase` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `studieID` = %i'
			          , $this->naam['nl']
			          , $this->naam['en']
			          , $this->soort
			          , $this->fase
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->studieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenStudie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenStudie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenStudie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Fase';
			break;
		case 'lang':
			$velden[] = 'Naam';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Fase';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Fase';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'LidStudieVerzameling';
			$velden[] = 'BedrijfStudieVerzameling';
			$velden[] = 'VacatureStudieVerzameling';
			$velden[] = 'VakStudieVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Soort';
			$velden[] = 'Fase';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle LidStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$LidStudieFromStudieVerz = LidStudieVerzameling::fromStudie($this);
		$returnValue = $LidStudieFromStudieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LidStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BedrijfStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BedrijfStudieFromStudieVerz = BedrijfStudieVerzameling::fromStudie($this);
		$returnValue = $BedrijfStudieFromStudieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BedrijfStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle VacatureStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VacatureStudieFromStudieVerz = VacatureStudieVerzameling::fromStudie($this);
		$returnValue = $VacatureStudieFromStudieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object VacatureStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle VakStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VakStudieFromStudieVerz = VakStudieVerzameling::fromStudie($this);
		$returnValue = $VakStudieFromStudieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object VakStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $LidStudieFromStudieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BedrijfStudieFromStudieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $VacatureStudieFromStudieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $VakStudieFromStudieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Studie`'
		          .' WHERE `studieID` = %i'
		          .' LIMIT 1'
		          , $this->studieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->studieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Studie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `studieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->studieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Studie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'studieid':
			return 'int';
		case 'naam':
			return 'string';
		case 'soort':
			return 'enum';
		case 'fase':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'studieID':
			$type = '%i';
			break;
		case 'naam':
		case 'soort':
		case 'fase':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Studie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `studieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->studieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Studie`'
		          .' WHERE `studieID` = %i'
		                 , $veld
		          , $this->studieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Studie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle LidStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LidStudieFromStudieVerz = LidStudieVerzameling::fromStudie($this);
		$dependencies['LidStudie'] = $LidStudieFromStudieVerz;

		// Verzamel alle BedrijfStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BedrijfStudieFromStudieVerz = BedrijfStudieVerzameling::fromStudie($this);
		$dependencies['BedrijfStudie'] = $BedrijfStudieFromStudieVerz;

		// Verzamel alle VacatureStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VacatureStudieFromStudieVerz = VacatureStudieVerzameling::fromStudie($this);
		$dependencies['VacatureStudie'] = $VacatureStudieFromStudieVerz;

		// Verzamel alle VakStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VakStudieFromStudieVerz = VakStudieVerzameling::fromStudie($this);
		$dependencies['VakStudie'] = $VakStudieFromStudieVerz;

		return $dependencies;
	}
}
