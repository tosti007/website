<?
/**
 * @brief 'AT'AUTH_GET:ingelogd
 */
abstract class ContactAdres_Generated
	extends Entiteit
{
	protected $adresID;					/**< \brief PRIMARY */
	protected $contact;
	protected $contact_contactID;		/**< \brief PRIMARY */
	protected $soort;					/**< \brief ENUM:THUIS/OUDERS/WERK/POST/BEZOEK/FACTUUR */
	protected $straat1;					/**< \brief LEGACY_NULL */
	protected $straat2;					/**< \brief NULL */
	protected $huisnummer;				/**< \brief NULL */
	protected $postcode;				/**< \brief LEGACY_NULL */
	protected $woonplaats;				/**< \brief LEGACY_NULL */
	protected $land;					/**< \brief NULL */
	/**
	/**
	 * @brief De constructor van de ContactAdres_Generated-klasse.
	 *
	 * @param mixed $a Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Contact)
		{
			$this->contact = $a;
			$this->contact_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->adresID = NULL;
		$this->soort = 'THUIS';
		$this->straat1 = '';
		$this->straat2 = NULL;
		$this->huisnummer = NULL;
		$this->postcode = '';
		$this->woonplaats = '';
		$this->land = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Contact';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld adresID.
	 *
	 * @return int
	 * De waarde van het veld adresID.
	 */
	public function getAdresID()
	{
		return $this->adresID;
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @return Contact
	 * De waarde van het veld contact.
	 */
	public function getContact()
	{
		if(!isset($this->contact)
		 && isset($this->contact_contactID)
		 ) {
			$this->contact = Contact::geef
					( $this->contact_contactID
					);
		}
		return $this->contact;
	}
	/**
	 * @brief Geef de waarde van het veld contact_contactID.
	 *
	 * @return int
	 * De waarde van het veld contact_contactID.
	 */
	public function getContactContactID()
	{
		if (is_null($this->contact_contactID) && isset($this->contact)) {
			$this->contact_contactID = $this->contact->getContactID();
		}
		return $this->contact_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('THUIS','OUDERS','WERK','POST','BEZOEK','FACTUUR');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld straat1.
	 *
	 * @return string
	 * De waarde van het veld straat1.
	 */
	public function getStraat1()
	{
		return $this->straat1;
	}
	/**
	 * @brief Stel de waarde van het veld straat1 in.
	 *
	 * @param mixed $newStraat1 De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setStraat1($newStraat1)
	{
		unset($this->errors['Straat1']);
		if(!is_null($newStraat1))
			$newStraat1 = trim($newStraat1);
		if($newStraat1 === "")
			$newStraat1 = NULL;
		if($this->straat1 === $newStraat1)
			return $this;

		if(is_null($newStraat1) && isset($this->straat1))
			$this->errors['Straat1'] = _('dit is een verplicht veld');

		$this->straat1 = $newStraat1;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld straat1 geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld straat1 geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkStraat1()
	{
		if (array_key_exists('Straat1', $this->errors))
			return $this->errors['Straat1'];
		$waarde = $this->getStraat1();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld straat2.
	 *
	 * @return string
	 * De waarde van het veld straat2.
	 */
	public function getStraat2()
	{
		return $this->straat2;
	}
	/**
	 * @brief Stel de waarde van het veld straat2 in.
	 *
	 * @param mixed $newStraat2 De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setStraat2($newStraat2)
	{
		unset($this->errors['Straat2']);
		if(!is_null($newStraat2))
			$newStraat2 = trim($newStraat2);
		if($newStraat2 === "")
			$newStraat2 = NULL;
		if($this->straat2 === $newStraat2)
			return $this;

		$this->straat2 = $newStraat2;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld straat2 geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld straat2 geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkStraat2()
	{
		if (array_key_exists('Straat2', $this->errors))
			return $this->errors['Straat2'];
		$waarde = $this->getStraat2();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld huisnummer.
	 *
	 * @return string
	 * De waarde van het veld huisnummer.
	 */
	public function getHuisnummer()
	{
		return $this->huisnummer;
	}
	/**
	 * @brief Stel de waarde van het veld huisnummer in.
	 *
	 * @param mixed $newHuisnummer De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setHuisnummer($newHuisnummer)
	{
		unset($this->errors['Huisnummer']);
		if(!is_null($newHuisnummer))
			$newHuisnummer = trim($newHuisnummer);
		if($newHuisnummer === "")
			$newHuisnummer = NULL;
		if($this->huisnummer === $newHuisnummer)
			return $this;

		$this->huisnummer = $newHuisnummer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld huisnummer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld huisnummer geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkHuisnummer()
	{
		if (array_key_exists('Huisnummer', $this->errors))
			return $this->errors['Huisnummer'];
		$waarde = $this->getHuisnummer();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld postcode.
	 *
	 * @return string
	 * De waarde van het veld postcode.
	 */
	public function getPostcode()
	{
		return $this->postcode;
	}
	/**
	 * @brief Stel de waarde van het veld postcode in.
	 *
	 * @param mixed $newPostcode De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setPostcode($newPostcode)
	{
		unset($this->errors['Postcode']);
		if(!is_null($newPostcode))
			$newPostcode = trim($newPostcode);
		if($newPostcode === "")
			$newPostcode = NULL;
		if($this->postcode === $newPostcode)
			return $this;

		if(is_null($newPostcode) && isset($this->postcode))
			$this->errors['Postcode'] = _('dit is een verplicht veld');

		$this->postcode = $newPostcode;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld postcode geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld postcode geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPostcode()
	{
		if (array_key_exists('Postcode', $this->errors))
			return $this->errors['Postcode'];
		$waarde = $this->getPostcode();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld woonplaats.
	 *
	 * @return string
	 * De waarde van het veld woonplaats.
	 */
	public function getWoonplaats()
	{
		return $this->woonplaats;
	}
	/**
	 * @brief Stel de waarde van het veld woonplaats in.
	 *
	 * @param mixed $newWoonplaats De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setWoonplaats($newWoonplaats)
	{
		unset($this->errors['Woonplaats']);
		if(!is_null($newWoonplaats))
			$newWoonplaats = trim($newWoonplaats);
		if($newWoonplaats === "")
			$newWoonplaats = NULL;
		if($this->woonplaats === $newWoonplaats)
			return $this;

		if(is_null($newWoonplaats) && isset($this->woonplaats))
			$this->errors['Woonplaats'] = _('dit is een verplicht veld');

		$this->woonplaats = $newWoonplaats;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld woonplaats geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld woonplaats geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkWoonplaats()
	{
		if (array_key_exists('Woonplaats', $this->errors))
			return $this->errors['Woonplaats'];
		$waarde = $this->getWoonplaats();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld land.
	 *
	 * @return string
	 * De waarde van het veld land.
	 */
	public function getLand()
	{
		return $this->land;
	}
	/**
	 * @brief Stel de waarde van het veld land in.
	 *
	 * @param mixed $newLand De nieuwe waarde.
	 *
	 * @return ContactAdres
	 * Dit ContactAdres-object.
	 */
	public function setLand($newLand)
	{
		unset($this->errors['Land']);
		if(!is_null($newLand))
			$newLand = trim($newLand);
		if($newLand === "")
			$newLand = NULL;
		if($this->land === $newLand)
			return $this;

		$this->land = $newLand;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld land geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld land geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLand()
	{
		if (array_key_exists('Land', $this->errors))
			return $this->errors['Land'];
		$waarde = $this->getLand();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return ContactAdres::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van ContactAdres.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return ContactAdres::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getAdresID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return ContactAdres|false
	 * Een ContactAdres-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$adresID = (int)$a[0];
		}
		else if(isset($a))
		{
			$adresID = (int)$a;
		}

		if(is_null($adresID))
			throw new BadMethodCallException();

		static::cache(array( array($adresID) ));
		return Entiteit::geefCache(array($adresID), 'ContactAdres');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'ContactAdres');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('ContactAdres::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `ContactAdres`.`adresID`'
		                 .     ', `ContactAdres`.`contact_contactID`'
		                 .     ', `ContactAdres`.`soort`'
		                 .     ', `ContactAdres`.`straat1`'
		                 .     ', `ContactAdres`.`straat2`'
		                 .     ', `ContactAdres`.`huisnummer`'
		                 .     ', `ContactAdres`.`postcode`'
		                 .     ', `ContactAdres`.`woonplaats`'
		                 .     ', `ContactAdres`.`land`'
		                 .     ', `ContactAdres`.`gewijzigdWanneer`'
		                 .     ', `ContactAdres`.`gewijzigdWie`'
		                 .' FROM `ContactAdres`'
		                 .' WHERE (`adresID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['adresID']);

			$obj = new ContactAdres(array($row['contact_contactID']));

			$obj->inDB = True;

			$obj->adresID  = (int) $row['adresID'];
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->straat1  = trim($row['straat1']);
			$obj->straat2  = (is_null($row['straat2'])) ? null : trim($row['straat2']);
			$obj->huisnummer  = (is_null($row['huisnummer'])) ? null : trim($row['huisnummer']);
			$obj->postcode  = trim($row['postcode']);
			$obj->woonplaats  = trim($row['woonplaats']);
			$obj->land  = (is_null($row['land'])) ? null : trim($row['land']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'ContactAdres')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getContactContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->adresID =
			$WSW4DB->q('RETURNID INSERT INTO `ContactAdres`'
			          . ' (`contact_contactID`, `soort`, `straat1`, `straat2`, `huisnummer`, `postcode`, `woonplaats`, `land`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %s, %s, %s, %i)'
			          , $this->contact_contactID
			          , $this->soort
			          , $this->straat1
			          , $this->straat2
			          , $this->huisnummer
			          , $this->postcode
			          , $this->woonplaats
			          , $this->land
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'ContactAdres')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `ContactAdres`'
			          .' SET `contact_contactID` = %i'
			          .   ', `soort` = %s'
			          .   ', `straat1` = %s'
			          .   ', `straat2` = %s'
			          .   ', `huisnummer` = %s'
			          .   ', `postcode` = %s'
			          .   ', `woonplaats` = %s'
			          .   ', `land` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `adresID` = %i'
			          , $this->contact_contactID
			          , $this->soort
			          , $this->straat1
			          , $this->straat2
			          , $this->huisnummer
			          , $this->postcode
			          , $this->woonplaats
			          , $this->land
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->adresID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenContactAdres
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenContactAdres($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenContactAdres($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Soort';
			$velden[] = 'Straat1';
			$velden[] = 'Straat2';
			$velden[] = 'Huisnummer';
			$velden[] = 'Postcode';
			$velden[] = 'Woonplaats';
			$velden[] = 'Land';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Straat1';
			$velden[] = 'Postcode';
			$velden[] = 'Woonplaats';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Contact';
			$velden[] = 'Soort';
			$velden[] = 'Straat1';
			$velden[] = 'Straat2';
			$velden[] = 'Huisnummer';
			$velden[] = 'Postcode';
			$velden[] = 'Woonplaats';
			$velden[] = 'Land';
			break;
		case 'get':
			$velden[] = 'Contact';
			$velden[] = 'Soort';
			$velden[] = 'Straat1';
			$velden[] = 'Straat2';
			$velden[] = 'Huisnummer';
			$velden[] = 'Postcode';
			$velden[] = 'Woonplaats';
			$velden[] = 'Land';
		case 'primary':
			$velden[] = 'contact_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Contact';
			$velden[] = 'Soort';
			$velden[] = 'Straat1';
			$velden[] = 'Straat2';
			$velden[] = 'Huisnummer';
			$velden[] = 'Postcode';
			$velden[] = 'Woonplaats';
			$velden[] = 'Land';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `ContactAdres`'
		          .' WHERE `adresID` = %i'
		          .' LIMIT 1'
		          , $this->adresID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->adresID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `ContactAdres`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `adresID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->adresID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van ContactAdres terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'adresid':
			return 'int';
		case 'contact':
			return 'foreign';
		case 'contact_contactid':
			return 'int';
		case 'soort':
			return 'enum';
		case 'straat1':
			return 'string';
		case 'straat2':
			return 'string';
		case 'huisnummer':
			return 'string';
		case 'postcode':
			return 'string';
		case 'woonplaats':
			return 'string';
		case 'land':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'adresID':
		case 'contact_contactID':
			$type = '%i';
			break;
		case 'soort':
		case 'straat1':
		case 'straat2':
		case 'huisnummer':
		case 'postcode':
		case 'woonplaats':
		case 'land':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `ContactAdres`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `adresID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->adresID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `ContactAdres`'
		          .' WHERE `adresID` = %i'
		                 , $veld
		          , $this->adresID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'ContactAdres');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
