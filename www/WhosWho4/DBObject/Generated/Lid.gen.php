<?
/**
 * @brief 'AT'AUTH_GET:bestuur,AUTH_SET:bestuur
 */
abstract class Lid_Generated
	extends Persoon
{
	protected $lidToestand;				/**< \brief ENUM:BIJNALID/LID/OUDLID/BAL/LIDVANVERDIENSTE/ERELID/GESCHORST/BEESTJE/INGESCHREVEN */
	protected $lidMaster;				/**< \brief SETBESTUUR */
	protected $lidExchange;				/**< \brief SETBESTUUR */
	protected $lidVan;					/**< \brief LEGACY_NULL */
	protected $lidTot;					/**< \brief NULL,SETBESTUUR */
	protected $studentnr;				/**< \brief NULL,LAZY */
	protected $opzoekbaar;				/**< \brief ENUM:A/J/N/CIE */
	protected $inAlmanak;				/**< \brief LAZY */
	protected $planetRSS;				/**< \brief NULL,LAZY */
	/** Verzamelingen **/
	protected $introDeelnemerVerzameling;
	protected $lidStudieVerzameling;
	protected $mentorVerzameling;
	protected $cSPReportVerzameling;
	protected $boekverkoopVerzameling;
	protected $tellingVerzameling;
	protected $papierMolenVerzameling;
	protected $activatieCodeVerzameling;
	protected $lidmaatschapsBetalingVerzameling;
	/**
	 * @brief De constructor van de Lid_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Persoon

		$this->contactID = NULL;
		$this->lidToestand = 'BIJNALID';
		$this->lidMaster = False;
		$this->lidExchange = False;
		$this->lidVan = new DateTimeLocale();
		$this->lidTot = new DateTimeLocale(NULL);
		$this->studentnr = NULL;
		$this->opzoekbaar = 'N';
		$this->inAlmanak = True;
		$this->planetRSS = NULL;
		$this->introDeelnemerVerzameling = NULL;
		$this->lidStudieVerzameling = NULL;
		$this->mentorVerzameling = NULL;
		$this->cSPReportVerzameling = NULL;
		$this->boekverkoopVerzameling = NULL;
		$this->tellingVerzameling = NULL;
		$this->papierMolenVerzameling = NULL;
		$this->activatieCodeVerzameling = NULL;
		$this->lidmaatschapsBetalingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld lidToestand.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld lidToestand.
	 */
	static public function enumsLidToestand()
	{
		static $vals = array('BIJNALID','LID','OUDLID','BAL','LIDVANVERDIENSTE','ERELID','GESCHORST','BEESTJE','INGESCHREVEN');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld lidToestand.
	 *
	 * @return string
	 * De waarde van het veld lidToestand.
	 */
	public function getLidToestand()
	{
		return $this->lidToestand;
	}
	/**
	 * @brief Stel de waarde van het veld lidToestand in.
	 *
	 * @param mixed $newLidToestand De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setLidToestand($newLidToestand)
	{
		unset($this->errors['LidToestand']);
		if(!is_null($newLidToestand))
			$newLidToestand = strtoupper(trim($newLidToestand));
		if($newLidToestand === "")
			$newLidToestand = NULL;
		if($this->lidToestand === $newLidToestand)
			return $this;

		$this->lidToestand = $newLidToestand;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lidToestand geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lidToestand geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkLidToestand()
	{
		if (array_key_exists('LidToestand', $this->errors))
			return $this->errors['LidToestand'];
		$waarde = $this->getLidToestand();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsLidToestand()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld lidMaster.
	 *
	 * @return bool
	 * De waarde van het veld lidMaster.
	 */
	public function getLidMaster()
	{
		return $this->lidMaster;
	}
	/**
	 * @brief Stel de waarde van het veld lidMaster in.
	 *
	 * @param mixed $newLidMaster De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setLidMaster($newLidMaster)
	{
		unset($this->errors['LidMaster']);
		if (!hasAuth('bestuur')) {
			trigger_error('Alleen >= bestuur mag dit wijzigen!', E_USER_WARNING);
			return $this;
		}
		if(!is_null($newLidMaster))
			$newLidMaster = (bool)$newLidMaster;
		if($this->lidMaster === $newLidMaster)
			return $this;

		$this->lidMaster = $newLidMaster;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lidMaster geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lidMaster geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkLidMaster()
	{
		if (array_key_exists('LidMaster', $this->errors))
			return $this->errors['LidMaster'];
		$waarde = $this->getLidMaster();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld lidExchange.
	 *
	 * @return bool
	 * De waarde van het veld lidExchange.
	 */
	public function getLidExchange()
	{
		return $this->lidExchange;
	}
	/**
	 * @brief Stel de waarde van het veld lidExchange in.
	 *
	 * @param mixed $newLidExchange De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setLidExchange($newLidExchange)
	{
		unset($this->errors['LidExchange']);
		if (!hasAuth('bestuur')) {
			trigger_error('Alleen >= bestuur mag dit wijzigen!', E_USER_WARNING);
			return $this;
		}
		if(!is_null($newLidExchange))
			$newLidExchange = (bool)$newLidExchange;
		if($this->lidExchange === $newLidExchange)
			return $this;

		$this->lidExchange = $newLidExchange;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lidExchange geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lidExchange geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkLidExchange()
	{
		if (array_key_exists('LidExchange', $this->errors))
			return $this->errors['LidExchange'];
		$waarde = $this->getLidExchange();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld lidVan.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld lidVan.
	 */
	public function getLidVan()
	{
		return $this->lidVan;
	}
	/**
	 * @brief Stel de waarde van het veld lidVan in.
	 *
	 * @param mixed $newLidVan De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setLidVan($newLidVan)
	{
		unset($this->errors['LidVan']);
		if(!$newLidVan instanceof DateTimeLocale) {
			try {
				$newLidVan = new DateTimeLocale($newLidVan);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->lidVan->strftime('%F %T') == $newLidVan->strftime('%F %T'))
			return $this;

		if(!$newLidVan->hasTime() && $this->lidVan->hasTime())
			$this->errors['LidVan'] = _('dit is een verplicht veld');

		$this->lidVan = $newLidVan;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lidVan geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lidVan geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLidVan()
	{
		if (array_key_exists('LidVan', $this->errors))
			return $this->errors['LidVan'];
		$waarde = $this->getLidVan();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld lidTot.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld lidTot.
	 */
	public function getLidTot()
	{
		return $this->lidTot;
	}
	/**
	 * @brief Stel de waarde van het veld lidTot in.
	 *
	 * @param mixed $newLidTot De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setLidTot($newLidTot)
	{
		unset($this->errors['LidTot']);
		if (!hasAuth('bestuur')) {
			trigger_error('Alleen >= bestuur mag dit wijzigen!', E_USER_WARNING);
			return $this;
		}
		if(!$newLidTot instanceof DateTimeLocale) {
			try {
				$newLidTot = new DateTimeLocale($newLidTot);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->lidTot->strftime('%F %T') == $newLidTot->strftime('%F %T'))
			return $this;

		$this->lidTot = $newLidTot;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lidTot geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lidTot geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLidTot()
	{
		if (array_key_exists('LidTot', $this->errors))
			return $this->errors['LidTot'];
		$waarde = $this->getLidTot();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld studentnr.
	 *
	 * @return string
	 * De waarde van het veld studentnr.
	 */
	public function getStudentnr()
	{
		return $this->studentnr;
	}
	/**
	 * @brief Stel de waarde van het veld studentnr in.
	 *
	 * @param mixed $newStudentnr De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setStudentnr($newStudentnr)
	{
		unset($this->errors['Studentnr']);
		if(!is_null($newStudentnr))
			$newStudentnr = trim($newStudentnr);
		if($newStudentnr === "")
			$newStudentnr = NULL;
		if($this->studentnr === $newStudentnr)
			return $this;

		$this->studentnr = $newStudentnr;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld studentnr geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld studentnr geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkStudentnr()
	{
		if (array_key_exists('Studentnr', $this->errors))
			return $this->errors['Studentnr'];
		$waarde = $this->getStudentnr();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld opzoekbaar.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld opzoekbaar.
	 */
	static public function enumsOpzoekbaar()
	{
		static $vals = array('A','J','N','CIE');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld opzoekbaar.
	 *
	 * @return string
	 * De waarde van het veld opzoekbaar.
	 */
	public function getOpzoekbaar()
	{
		return $this->opzoekbaar;
	}
	/**
	 * @brief Stel de waarde van het veld opzoekbaar in.
	 *
	 * @param mixed $newOpzoekbaar De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setOpzoekbaar($newOpzoekbaar)
	{
		unset($this->errors['Opzoekbaar']);
		if(!is_null($newOpzoekbaar))
			$newOpzoekbaar = strtoupper(trim($newOpzoekbaar));
		if($newOpzoekbaar === "")
			$newOpzoekbaar = NULL;
		if($this->opzoekbaar === $newOpzoekbaar)
			return $this;

		$this->opzoekbaar = $newOpzoekbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opzoekbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opzoekbaar geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpzoekbaar()
	{
		if (array_key_exists('Opzoekbaar', $this->errors))
			return $this->errors['Opzoekbaar'];
		$waarde = $this->getOpzoekbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsOpzoekbaar()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inAlmanak.
	 *
	 * @return bool
	 * De waarde van het veld inAlmanak.
	 */
	public function getInAlmanak()
	{
		return $this->inAlmanak;
	}
	/**
	 * @brief Stel de waarde van het veld inAlmanak in.
	 *
	 * @param mixed $newInAlmanak De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setInAlmanak($newInAlmanak)
	{
		unset($this->errors['InAlmanak']);
		if(!is_null($newInAlmanak))
			$newInAlmanak = (bool)$newInAlmanak;
		if($this->inAlmanak === $newInAlmanak)
			return $this;

		$this->inAlmanak = $newInAlmanak;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inAlmanak geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inAlmanak geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkInAlmanak()
	{
		if (array_key_exists('InAlmanak', $this->errors))
			return $this->errors['InAlmanak'];
		$waarde = $this->getInAlmanak();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld planetRSS.
	 *
	 * @return string
	 * De waarde van het veld planetRSS.
	 */
	public function getPlanetRSS()
	{
		return $this->planetRSS;
	}
	/**
	 * @brief Stel de waarde van het veld planetRSS in.
	 *
	 * @param mixed $newPlanetRSS De nieuwe waarde.
	 *
	 * @return Lid
	 * Dit Lid-object.
	 */
	public function setPlanetRSS($newPlanetRSS)
	{
		unset($this->errors['PlanetRSS']);
		if(!is_null($newPlanetRSS))
			$newPlanetRSS = trim($newPlanetRSS);
		if($newPlanetRSS === "")
			$newPlanetRSS = NULL;
		if($this->planetRSS === $newPlanetRSS)
			return $this;

		$this->planetRSS = $newPlanetRSS;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld planetRSS geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld planetRSS geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPlanetRSS()
	{
		if (array_key_exists('PlanetRSS', $this->errors))
			return $this->errors['PlanetRSS'];
		$waarde = $this->getPlanetRSS();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de IntroDeelnemerVerzameling die hoort bij dit object.
	 */
	public function getIntroDeelnemerVerzameling()
	{
		if(!$this->introDeelnemerVerzameling instanceof IntroDeelnemerVerzameling)
			$this->introDeelnemerVerzameling = IntroDeelnemerVerzameling::fromLid($this);
		return $this->introDeelnemerVerzameling;
	}
	/**
	 * @brief Returneert de LidStudieVerzameling die hoort bij dit object.
	 */
	public function getLidStudieVerzameling()
	{
		if(!$this->lidStudieVerzameling instanceof LidStudieVerzameling)
			$this->lidStudieVerzameling = LidStudieVerzameling::fromLid($this);
		return $this->lidStudieVerzameling;
	}
	/**
	 * @brief Returneert de MentorVerzameling die hoort bij dit object.
	 */
	public function getMentorVerzameling()
	{
		if(!$this->mentorVerzameling instanceof MentorVerzameling)
			$this->mentorVerzameling = MentorVerzameling::fromLid($this);
		return $this->mentorVerzameling;
	}
	/**
	 * @brief Returneert de CSPReportVerzameling die hoort bij dit object.
	 */
	public function getCSPReportVerzameling()
	{
		if(!$this->cSPReportVerzameling instanceof CSPReportVerzameling)
			$this->cSPReportVerzameling = CSPReportVerzameling::fromIngelogdLid($this);
		return $this->cSPReportVerzameling;
	}
	/**
	 * @brief Returneert de BoekverkoopVerzameling die hoort bij dit object.
	 */
	public function getBoekverkoopVerzameling()
	{
		if(!$this->boekverkoopVerzameling instanceof BoekverkoopVerzameling)
		{
			$this->boekverkoopVerzameling = new BoekverkoopVerzameling();
			$this->boekverkoopVerzameling->union(BoekverkoopVerzameling::fromOpener($this));
			$this->boekverkoopVerzameling->union(BoekverkoopVerzameling::fromSluiter($this));
		}
		return $this->boekverkoopVerzameling;
	}
	/**
	 * @brief Returneert de TellingVerzameling die hoort bij dit object.
	 */
	public function getTellingVerzameling()
	{
		if(!$this->tellingVerzameling instanceof TellingVerzameling)
			$this->tellingVerzameling = TellingVerzameling::fromTeller($this);
		return $this->tellingVerzameling;
	}
	/**
	 * @brief Returneert de PapierMolenVerzameling die hoort bij dit object.
	 */
	public function getPapierMolenVerzameling()
	{
		if(!$this->papierMolenVerzameling instanceof PapierMolenVerzameling)
			$this->papierMolenVerzameling = PapierMolenVerzameling::fromLid($this);
		return $this->papierMolenVerzameling;
	}
	/**
	 * @brief Returneert de ActivatieCodeVerzameling die hoort bij dit object.
	 */
	public function getActivatieCodeVerzameling()
	{
		if(!$this->activatieCodeVerzameling instanceof ActivatieCodeVerzameling)
			$this->activatieCodeVerzameling = ActivatieCodeVerzameling::fromLid($this);
		return $this->activatieCodeVerzameling;
	}
	/**
	 * @brief Returneert de LidmaatschapsBetalingVerzameling die hoort bij dit object.
	 */
	public function getLidmaatschapsBetalingVerzameling()
	{
		if(!$this->lidmaatschapsBetalingVerzameling instanceof LidmaatschapsBetalingVerzameling)
			$this->lidmaatschapsBetalingVerzameling = LidmaatschapsBetalingVerzameling::fromLid($this);
		return $this->lidmaatschapsBetalingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Lid::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Lid.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Lid::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContactID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Lid|false
	 * Een Lid-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contactID = (int)$a;
		}

		if(is_null($contactID))
			throw new BadMethodCallException();

		static::cache(array( array($contactID) ));
		return Entiteit::geefCache(array($contactID), 'Lid');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Contact::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Lid::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Lid`.`lidToestand`'
			                 .     ', `Lid`.`lidMaster`'
			                 .     ', `Lid`.`lidExchange`'
			                 .     ', `Lid`.`lidVan`'
			                 .     ', `Lid`.`lidTot`'
			                 .     ', `Lid`.`studentnr`'
			                 .     ', `Lid`.`opzoekbaar`'
			                 .     ', `Lid`.`inAlmanak`'
			                 .     ', `Lid`.`planetRSS`'
			                 .     ', `Persoon`.`voornaam`'
			                 .     ', `Persoon`.`bijnaam`'
			                 .     ', `Persoon`.`voorletters`'
			                 .     ', `Persoon`.`geboortenamen`'
			                 .     ', `Persoon`.`tussenvoegsels`'
			                 .     ', `Persoon`.`achternaam`'
			                 .     ', `Persoon`.`titelsPrefix`'
			                 .     ', `Persoon`.`titelsPostfix`'
			                 .     ', `Persoon`.`geslacht`'
			                 .     ', `Persoon`.`datumGeboorte`'
			                 .     ', `Persoon`.`overleden`'
			                 .     ', `Persoon`.`GPGkey`'
			                 .     ', `Persoon`.`Rekeningnummer`'
			                 .     ', `Persoon`.`opmerkingen`'
			                 .     ', `Persoon`.`voornaamwoord_woordID`'
			                 .     ', `Persoon`.`voornaamwoordZichtbaar`'
			                 .     ', `Contact`.`contactID`'
			                 .     ', `Contact`.`email`'
			                 .     ', `Contact`.`homepage`'
			                 .     ', `Contact`.`vakidOpsturen`'
			                 .     ', `Contact`.`aes2rootsOpsturen`'
			                 .     ', `Contact`.`gewijzigdWanneer`'
			                 .     ', `Contact`.`gewijzigdWie`'
			                 .' FROM `Lid`'
			                 .' LEFT JOIN `Persoon` USING (`contactID`)'
			                 .' LEFT JOIN `Contact` USING (`contactID`)'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contactID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Lid'][$id];
			} else {
				$obj = new Lid();
			}

			$obj->inDB = True;

			$obj->lidToestand  = strtoupper(trim($row['lidToestand']));
			$obj->lidMaster  = (bool) $row['lidMaster'];
			$obj->lidExchange  = (bool) $row['lidExchange'];
			$obj->lidVan  = new DateTimeLocale($row['lidVan']);
			$obj->lidTot  = new DateTimeLocale($row['lidTot']);
			$obj->studentnr  = (is_null($row['studentnr'])) ? null : trim($row['studentnr']);
			$obj->opzoekbaar  = strtoupper(trim($row['opzoekbaar']));
			$obj->inAlmanak  = (bool) $row['inAlmanak'];
			$obj->planetRSS  = (is_null($row['planetRSS'])) ? null : trim($row['planetRSS']);
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Persoon::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Lid')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Lid`'
			          . ' (`contactID`, `lidToestand`, `lidMaster`, `lidExchange`, `lidVan`, `lidTot`, `studentnr`, `opzoekbaar`, `inAlmanak`, `planetRSS`)'
			          . ' VALUES (%i, %s, %i, %i, %s, %s, %s, %s, %i, %s)'
			          , $this->contactID
			          , $this->lidToestand
			          , $this->lidMaster
			          , $this->lidExchange
			          , $this->lidVan->strftime('%F %T')
			          , (!is_null($this->lidTot))?$this->lidTot->strftime('%F %T'):null
			          , $this->studentnr
			          , $this->opzoekbaar
			          , $this->inAlmanak
			          , $this->planetRSS
			          );

			if($classname == 'Lid')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Lid`'
			          .' SET `lidToestand` = %s'
			          .   ', `lidMaster` = %i'
			          .   ', `lidExchange` = %i'
			          .   ', `lidVan` = %s'
			          .   ', `lidTot` = %s'
			          .   ', `studentnr` = %s'
			          .   ', `opzoekbaar` = %s'
			          .   ', `inAlmanak` = %i'
			          .   ', `planetRSS` = %s'
			          .' WHERE `contactID` = %i'
			          , $this->lidToestand
			          , $this->lidMaster
			          , $this->lidExchange
			          , $this->lidVan->strftime('%F %T')
			          , (!is_null($this->lidTot))?$this->lidTot->strftime('%F %T'):null
			          , $this->studentnr
			          , $this->opzoekbaar
			          , $this->inAlmanak
			          , $this->planetRSS
			          , $this->contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenLid
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenLid($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenLid($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'LidToestand';
			$velden[] = 'LidMaster';
			$velden[] = 'LidExchange';
			$velden[] = 'LidVan';
			$velden[] = 'LidTot';
			$velden[] = 'Studentnr';
			$velden[] = 'Opzoekbaar';
			$velden[] = 'InAlmanak';
			$velden[] = 'PlanetRSS';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'LidVan';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'LidToestand';
			$velden[] = 'LidMaster';
			$velden[] = 'LidExchange';
			$velden[] = 'LidVan';
			$velden[] = 'LidTot';
			$velden[] = 'Studentnr';
			$velden[] = 'Opzoekbaar';
			$velden[] = 'InAlmanak';
			$velden[] = 'PlanetRSS';
			break;
		case 'get':
			$velden[] = 'LidToestand';
			$velden[] = 'LidMaster';
			$velden[] = 'LidExchange';
			$velden[] = 'LidVan';
			$velden[] = 'LidTot';
			$velden[] = 'Studentnr';
			$velden[] = 'Opzoekbaar';
			$velden[] = 'InAlmanak';
			$velden[] = 'PlanetRSS';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'IntroDeelnemerVerzameling';
			$velden[] = 'LidStudieVerzameling';
			$velden[] = 'MentorVerzameling';
			$velden[] = 'CSPReportVerzameling';
			$velden[] = 'BoekverkoopVerzameling';
			$velden[] = 'TellingVerzameling';
			$velden[] = 'PapierMolenVerzameling';
			$velden[] = 'ActivatieCodeVerzameling';
			$velden[] = 'LidmaatschapsBetalingVerzameling';
			break;
		default:
			$velden[] = 'LidToestand';
			$velden[] = 'LidMaster';
			$velden[] = 'LidExchange';
			$velden[] = 'LidVan';
			$velden[] = 'LidTot';
			$velden[] = 'Studentnr';
			$velden[] = 'Opzoekbaar';
			$velden[] = 'InAlmanak';
			$velden[] = 'PlanetRSS';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle IntroDeelnemer-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$IntroDeelnemerFromLidVerz = IntroDeelnemerVerzameling::fromLid($this);
		$returnValue = $IntroDeelnemerFromLidVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IntroDeelnemer met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle LidStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$LidStudieFromLidVerz = LidStudieVerzameling::fromLid($this);
		$returnValue = $LidStudieFromLidVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LidStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Mentor-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MentorFromLidVerz = MentorVerzameling::fromLid($this);
		$returnValue = $MentorFromLidVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Mentor met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle CSPReport-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$CSPReportFromIngelogdLidVerz = CSPReportVerzameling::fromIngelogdLid($this);
		$returnValue = $CSPReportFromIngelogdLidVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CSPReport met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Boekverkoop-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BoekverkoopFromOpenerVerz = BoekverkoopVerzameling::fromOpener($this);
		$returnValue = $BoekverkoopFromOpenerVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Boekverkoop met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Boekverkoop-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BoekverkoopFromSluiterVerz = BoekverkoopVerzameling::fromSluiter($this);
		$returnValue = $BoekverkoopFromSluiterVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Boekverkoop met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Telling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TellingFromTellerVerz = TellingVerzameling::fromTeller($this);
		$returnValue = $TellingFromTellerVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Telling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PapierMolen-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$PapierMolenFromLidVerz = PapierMolenVerzameling::fromLid($this);
		$returnValue = $PapierMolenFromLidVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PapierMolen met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ActivatieCode-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ActivatieCodeFromLidVerz = ActivatieCodeVerzameling::fromLid($this);
		$returnValue = $ActivatieCodeFromLidVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ActivatieCode met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle LidmaatschapsBetaling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$LidmaatschapsBetalingFromLidVerz = LidmaatschapsBetalingVerzameling::fromLid($this);
		$returnValue = $LidmaatschapsBetalingFromLidVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LidmaatschapsBetaling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($CSPReportFromIngelogdLidVerz as $v)
		{
			$v->setIngelogdLid(NULL);
		}
		$CSPReportFromIngelogdLidVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $IntroDeelnemerFromLidVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $LidStudieFromLidVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $MentorFromLidVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BoekverkoopFromSluiterVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TellingFromTellerVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $PapierMolenFromLidVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ActivatieCodeFromLidVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $LidmaatschapsBetalingFromLidVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Lid`'
		          .' WHERE `contactID` = %i'
		          .' LIMIT 1'
		          , $this->contactID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Lid terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'lidtoestand':
			return 'enum';
		case 'lidmaster':
			return 'bool';
		case 'lidexchange':
			return 'bool';
		case 'lidvan':
			return 'date';
		case 'lidtot':
			return 'date';
		case 'studentnr':
			return 'string';
		case 'opzoekbaar':
			return 'enum';
		case 'inalmanak':
			return 'bool';
		case 'planetrss':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'lidMaster':
		case 'lidExchange':
		case 'inAlmanak':
			$type = '%i';
			break;
		case 'lidToestand':
		case 'lidVan':
		case 'lidTot':
		case 'studentnr':
		case 'opzoekbaar':
		case 'planetRSS':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Lid`'
		          ." SET `%l` = $type"
		          .' WHERE `contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'lidToestand':
		case 'lidMaster':
		case 'lidExchange':
		case 'lidVan':
		case 'lidTot':
		case 'studentnr':
		case 'opzoekbaar':
		case 'inAlmanak':
		case 'planetRSS':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Lid`'
		          .' WHERE `contactID` = %i'
		                 , $veld
		          , $this->contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle IntroDeelnemer-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IntroDeelnemerFromLidVerz = IntroDeelnemerVerzameling::fromLid($this);
		$dependencies['IntroDeelnemer'] = $IntroDeelnemerFromLidVerz;

		// Verzamel alle LidStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LidStudieFromLidVerz = LidStudieVerzameling::fromLid($this);
		$dependencies['LidStudie'] = $LidStudieFromLidVerz;

		// Verzamel alle Mentor-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MentorFromLidVerz = MentorVerzameling::fromLid($this);
		$dependencies['Mentor'] = $MentorFromLidVerz;

		// Verzamel alle CSPReport-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CSPReportFromIngelogdLidVerz = CSPReportVerzameling::fromIngelogdLid($this);
		$dependencies['CSPReport'] = $CSPReportFromIngelogdLidVerz;

		// Verzamel alle Boekverkoop-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BoekverkoopFromOpenerVerz = BoekverkoopVerzameling::fromOpener($this);
		$dependencies['Boekverkoop'] = $BoekverkoopFromOpenerVerz;

		// Verzamel alle Boekverkoop-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BoekverkoopFromSluiterVerz = BoekverkoopVerzameling::fromSluiter($this);
		$dependencies['Boekverkoop'] = $BoekverkoopFromSluiterVerz;

		// Verzamel alle Telling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TellingFromTellerVerz = TellingVerzameling::fromTeller($this);
		$dependencies['Telling'] = $TellingFromTellerVerz;

		// Verzamel alle PapierMolen-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PapierMolenFromLidVerz = PapierMolenVerzameling::fromLid($this);
		$dependencies['PapierMolen'] = $PapierMolenFromLidVerz;

		// Verzamel alle ActivatieCode-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ActivatieCodeFromLidVerz = ActivatieCodeVerzameling::fromLid($this);
		$dependencies['ActivatieCode'] = $ActivatieCodeFromLidVerz;

		// Verzamel alle LidmaatschapsBetaling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LidmaatschapsBetalingFromLidVerz = LidmaatschapsBetalingVerzameling::fromLid($this);
		$dependencies['LidmaatschapsBetaling'] = $LidmaatschapsBetalingFromLidVerz;

		return $dependencies;
	}
}
