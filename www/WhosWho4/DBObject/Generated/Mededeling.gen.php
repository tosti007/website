<?
/**
 * @brief 'AT'AUTH_GET:gast,AUTH_SET:promocie
 */
abstract class Mededeling_Generated
	extends Entiteit
{
	protected $mededelingID;			/**< \brief PRIMARY */
	protected $commissie;				/**< \brief LEGACY_NULL */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $doelgroep;				/**< \brief ENUM:ALLEN/LEDEN/ACTIEF */
	protected $prioriteit;				/**< \brief ENUM:LAAG/NORMAAL/HOOG */
	protected $datumBegin;
	protected $datumEind;
	protected $url;						/**< \brief NULL */
	protected $omschrijving;			/**< \brief LANG,LEGACY_NULL */
	protected $mededeling;				/**< \brief LANG,LEGACY_NULL */
	/**
	 * @brief De constructor van de Mededeling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->mededelingID = NULL;
		$this->commissie = NULL;
		$this->commissie_commissieID = 0;
		$this->doelgroep = 'ALLEN';
		$this->prioriteit = 'LAAG';
		$this->datumBegin = new DateTimeLocale();
		$this->datumEind = new DateTimeLocale();
		$this->url = NULL;
		$this->omschrijving = array('nl' => '', 'en' => '');
		$this->mededeling = array('nl' => '', 'en' => '');
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld mededelingID.
	 *
	 * @return int
	 * De waarde van het veld mededelingID.
	 */
	public function getMededelingID()
	{
		return $this->mededelingID;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		if (isset($this->commissie)
		 || isset($this->commissie_commissieID))
		{
			$this->errors['Commissie'] = _('dit is een verplicht veld');
			$this->commissie = NULL;
			$this->commissie_commissieID = NULL;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld doelgroep.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld doelgroep.
	 */
	static public function enumsDoelgroep()
	{
		static $vals = array('ALLEN','LEDEN','ACTIEF');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld doelgroep.
	 *
	 * @return string
	 * De waarde van het veld doelgroep.
	 */
	public function getDoelgroep()
	{
		return $this->doelgroep;
	}
	/**
	 * @brief Stel de waarde van het veld doelgroep in.
	 *
	 * @param mixed $newDoelgroep De nieuwe waarde.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setDoelgroep($newDoelgroep)
	{
		unset($this->errors['Doelgroep']);
		if(!is_null($newDoelgroep))
			$newDoelgroep = strtoupper(trim($newDoelgroep));
		if($newDoelgroep === "")
			$newDoelgroep = NULL;
		if($this->doelgroep === $newDoelgroep)
			return $this;

		$this->doelgroep = $newDoelgroep;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld doelgroep geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld doelgroep geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDoelgroep()
	{
		if (array_key_exists('Doelgroep', $this->errors))
			return $this->errors['Doelgroep'];
		$waarde = $this->getDoelgroep();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsDoelgroep()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld prioriteit.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld prioriteit.
	 */
	static public function enumsPrioriteit()
	{
		static $vals = array('LAAG','NORMAAL','HOOG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld prioriteit.
	 *
	 * @return string
	 * De waarde van het veld prioriteit.
	 */
	public function getPrioriteit()
	{
		return $this->prioriteit;
	}
	/**
	 * @brief Stel de waarde van het veld prioriteit in.
	 *
	 * @param mixed $newPrioriteit De nieuwe waarde.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setPrioriteit($newPrioriteit)
	{
		unset($this->errors['Prioriteit']);
		if(!is_null($newPrioriteit))
			$newPrioriteit = strtoupper(trim($newPrioriteit));
		if($newPrioriteit === "")
			$newPrioriteit = NULL;
		if($this->prioriteit === $newPrioriteit)
			return $this;

		$this->prioriteit = $newPrioriteit;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld prioriteit geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld prioriteit geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPrioriteit()
	{
		if (array_key_exists('Prioriteit', $this->errors))
			return $this->errors['Prioriteit'];
		$waarde = $this->getPrioriteit();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsPrioriteit()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumBegin.
	 */
	public function getDatumBegin()
	{
		return $this->datumBegin;
	}
	/**
	 * @brief Stel de waarde van het veld datumBegin in.
	 *
	 * @param mixed $newDatumBegin De nieuwe waarde.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setDatumBegin($newDatumBegin)
	{
		unset($this->errors['DatumBegin']);
		if(!$newDatumBegin instanceof DateTimeLocale) {
			try {
				$newDatumBegin = new DateTimeLocale($newDatumBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumBegin->strftime('%F %T') == $newDatumBegin->strftime('%F %T'))
			return $this;

		$this->datumBegin = $newDatumBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumBegin()
	{
		if (array_key_exists('DatumBegin', $this->errors))
			return $this->errors['DatumBegin'];
		$waarde = $this->getDatumBegin();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumEind.
	 */
	public function getDatumEind()
	{
		return $this->datumEind;
	}
	/**
	 * @brief Stel de waarde van het veld datumEind in.
	 *
	 * @param mixed $newDatumEind De nieuwe waarde.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setDatumEind($newDatumEind)
	{
		unset($this->errors['DatumEind']);
		if(!$newDatumEind instanceof DateTimeLocale) {
			try {
				$newDatumEind = new DateTimeLocale($newDatumEind);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumEind->strftime('%F %T') == $newDatumEind->strftime('%F %T'))
			return $this;

		$this->datumEind = $newDatumEind;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumEind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumEind geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumEind()
	{
		if (array_key_exists('DatumEind', $this->errors))
			return $this->errors['DatumEind'];
		$waarde = $this->getDatumEind();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld url.
	 *
	 * @return string
	 * De waarde van het veld url.
	 */
	public function getUrl()
	{
		return $this->url;
	}
	/**
	 * @brief Stel de waarde van het veld url in.
	 *
	 * @param mixed $newUrl De nieuwe waarde.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setUrl($newUrl)
	{
		unset($this->errors['Url']);
		if(!is_null($newUrl))
			$newUrl = trim($newUrl);
		if($newUrl === "")
			$newUrl = NULL;
		if($this->url === $newUrl)
			return $this;

		$this->url = $newUrl;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld url geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld url geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkUrl()
	{
		if (array_key_exists('Url', $this->errors))
			return $this->errors['Url'];
		$waarde = $this->getUrl();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->omschrijving[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setOmschrijving($newOmschrijving, $lang = NULL)
	{
		unset($this->errors['Omschrijving']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving[$lang] === $newOmschrijving)
			return $this;

		if(is_null($newOmschrijving) && isset($this->omschrijving[$lang]))
			$this->errors['Omschrijving'] = _('dit is een verplicht veld');

		$this->omschrijving[$lang] = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkOmschrijving('nl');
			$ret['en'] = $this->checkOmschrijving('en');
			return $ret;
		}

		if (array_key_exists('Omschrijving', $this->errors))
			if (is_array($this->errors['Omschrijving']) && array_key_exists($lang, $this->errors['Omschrijving']))
				return $this->errors['Omschrijving'][$lang];
		$waarde = $this->getOmschrijving($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld mededeling.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld mededeling.
	 */
	public function getMededeling($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->mededeling[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld mededeling in.
	 *
	 * @param mixed $newMededeling De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Mededeling
	 * Dit Mededeling-object.
	 */
	public function setMededeling($newMededeling, $lang = NULL)
	{
		unset($this->errors['Mededeling']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newMededeling))
			$newMededeling = trim($newMededeling);
		if($newMededeling === "")
			$newMededeling = NULL;
		if (!is_null($newMededeling))
			$newMededeling = Purifier::Purify($newMededeling);
		if($this->mededeling[$lang] === $newMededeling)
			return $this;

		if(is_null($newMededeling) && isset($this->mededeling[$lang]))
			$this->errors['Mededeling'] = _('dit is een verplicht veld');

		$this->mededeling[$lang] = $newMededeling;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld mededeling geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld mededeling geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkMededeling($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkMededeling('nl');
			$ret['en'] = $this->checkMededeling('en');
			return $ret;
		}

		if (array_key_exists('Mededeling', $this->errors))
			if (is_array($this->errors['Mededeling']) && array_key_exists($lang, $this->errors['Mededeling']))
				return $this->errors['Mededeling'][$lang];
		$waarde = $this->getMededeling($lang);
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Mededeling::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Mededeling.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('promocie');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Mededeling::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getMededelingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Mededeling|false
	 * Een Mededeling-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$mededelingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$mededelingID = (int)$a;
		}

		if(is_null($mededelingID))
			throw new BadMethodCallException();

		static::cache(array( array($mededelingID) ));
		return Entiteit::geefCache(array($mededelingID), 'Mededeling');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Mededeling');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Mededeling::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Mededeling`.`mededelingID`'
		                 .     ', `Mededeling`.`commissie_commissieID`'
		                 .     ', `Mededeling`.`doelgroep`'
		                 .     ', `Mededeling`.`prioriteit`'
		                 .     ', `Mededeling`.`datumBegin`'
		                 .     ', `Mededeling`.`datumEind`'
		                 .     ', `Mededeling`.`url`'
		                 .     ', `Mededeling`.`omschrijving_NL`'
		                 .     ', `Mededeling`.`omschrijving_EN`'
		                 .     ', `Mededeling`.`mededeling_NL`'
		                 .     ', `Mededeling`.`mededeling_EN`'
		                 .     ', `Mededeling`.`gewijzigdWanneer`'
		                 .     ', `Mededeling`.`gewijzigdWie`'
		                 .' FROM `Mededeling`'
		                 .' WHERE (`mededelingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mededelingID']);

			$obj = new Mededeling();

			$obj->inDB = True;

			$obj->mededelingID  = (int) $row['mededelingID'];
			$obj->commissie_commissieID  = (int) $row['commissie_commissieID'];
			$obj->doelgroep  = strtoupper(trim($row['doelgroep']));
			$obj->prioriteit  = strtoupper(trim($row['prioriteit']));
			$obj->datumBegin  = new DateTimeLocale($row['datumBegin']);
			$obj->datumEind  = new DateTimeLocale($row['datumEind']);
			$obj->url = $row['url'];
			$obj->omschrijving['nl'] = $row['omschrijving_NL'];
			$obj->omschrijving['en'] = $row['omschrijving_EN'];
			$obj->mededeling['nl'] = $row['mededeling_NL'];
			$obj->mededeling['en'] = $row['mededeling_EN'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Mededeling')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getCommissieCommissieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->mededelingID =
			$WSW4DB->q('RETURNID INSERT INTO `Mededeling`'
			          . ' (`commissie_commissieID`, `doelgroep`, `prioriteit`, `datumBegin`, `datumEind`, `url`, `omschrijving_NL`, `omschrijving_EN`, `mededeling_NL`, `mededeling_EN`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %i)'
			          , $this->commissie_commissieID
			          , $this->doelgroep
			          , $this->prioriteit
			          , $this->datumBegin->strftime('%F %T')
			          , $this->datumEind->strftime('%F %T')
			          , $this->url
			          , $this->omschrijving['nl']
			          , $this->omschrijving['en']
			          , $this->mededeling['nl']
			          , $this->mededeling['en']
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Mededeling')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Mededeling`'
			          .' SET `commissie_commissieID` = %i'
			          .   ', `doelgroep` = %s'
			          .   ', `prioriteit` = %s'
			          .   ', `datumBegin` = %s'
			          .   ', `datumEind` = %s'
			          .   ', `url` = %s'
			          .   ', `omschrijving_NL` = %s'
			          .   ', `omschrijving_EN` = %s'
			          .   ', `mededeling_NL` = %s'
			          .   ', `mededeling_EN` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mededelingID` = %i'
			          , $this->commissie_commissieID
			          , $this->doelgroep
			          , $this->prioriteit
			          , $this->datumBegin->strftime('%F %T')
			          , $this->datumEind->strftime('%F %T')
			          , $this->url
			          , $this->omschrijving['nl']
			          , $this->omschrijving['en']
			          , $this->mededeling['nl']
			          , $this->mededeling['en']
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mededelingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMededeling
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMededeling($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMededeling($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Commissie';
			$velden[] = 'Doelgroep';
			$velden[] = 'Prioriteit';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Url';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mededeling';
			break;
		case 'lang':
			$velden[] = 'Omschrijving';
			$velden[] = 'Mededeling';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Commissie';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mededeling';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Commissie';
			$velden[] = 'Doelgroep';
			$velden[] = 'Prioriteit';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Url';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mededeling';
			break;
		case 'get':
			$velden[] = 'Commissie';
			$velden[] = 'Doelgroep';
			$velden[] = 'Prioriteit';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Url';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mededeling';
		case 'primary':
			$velden[] = 'commissie_commissieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Commissie';
			$velden[] = 'Doelgroep';
			$velden[] = 'Prioriteit';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEind';
			$velden[] = 'Url';
			$velden[] = 'Omschrijving';
			$velden[] = 'Mededeling';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Mededeling`'
		          .' WHERE `mededelingID` = %i'
		          .' LIMIT 1'
		          , $this->mededelingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mededelingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Mededeling`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mededelingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mededelingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Mededeling terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mededelingid':
			return 'int';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'doelgroep':
			return 'enum';
		case 'prioriteit':
			return 'enum';
		case 'datumbegin':
			return 'date';
		case 'datumeind':
			return 'date';
		case 'url':
			return 'url';
		case 'omschrijving':
			return 'string';
		case 'mededeling':
			return 'html';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'mededelingID':
		case 'commissie_commissieID':
			$type = '%i';
			break;
		case 'doelgroep':
		case 'prioriteit':
		case 'datumBegin':
		case 'datumEind':
		case 'url':
		case 'omschrijving':
		case 'mededeling':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Mededeling`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mededelingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mededelingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Mededeling`'
		          .' WHERE `mededelingID` = %i'
		                 , $veld
		          , $this->mededelingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Mededeling');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
