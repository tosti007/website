<?
/**
 * $Id$
 */
class Tentamen
	extends Tentamen_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($vak = null)
	{
		parent::__construct($vak); // Tentamen_Generated
	}

	/*
	 *  Geeft de url voor een tentamen terug
	 *
	 * @param extra Een extra stukje url voor achter de standaardurl
	 * @return Een urlstring voor een tentamen
	 */
	public function url($extra = null) {
		$vak = $this->getVak();
		$pre = $vak->url() . 'Tentamen/' . $this->geefID();
		if($extra)
			return $pre . '/' . $extra;
		return $pre;
	}

	/*
	 *  Geeft de url voor een uitwerking bij een tentamen
	 *
	 * @param extra Een extra stukje url voor achter de standaardurl
	 * @return Een urlstring voor een uitwerking
	 */
	public function uitwerkingUrl() {
		return $this->url('Uitwerking');
	}

	/*
	 *  Geeft de bestandslocatie van een tentamen op het fs
	 *
	 * @return De bestandslocatie van het tentamen als string
	 */
	public function getBestand()
	{
		return TENT_SRV . $this->geefID() . ".tent.pdf";
	}

	/*
	 *  Controleert of het tentamen gewijzigd mag worden
	 *
	 * @return Een bool met of het wijzigen mag
	 */
	public function magWijzigen()
	{
		return hasAuth('tbc');
	}

	/*
	 *  Controleert of het tentamen verwijderd mag worden
	 *
	 * @return Een bool met of het verwijderen mag
	 */
	public function magVerwijderen()
	{
		return hasAuth('tbc');
	}

	/*
	 *  Verwijdert een tentamen uit de db
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $filesystem;
		if($filesystem->has($this->getBestand()))
		{
			if (!DEBUG) {
				$filesystem->delete($this->getBestand());
			} else {
				Page::addMelding($this->getBestand() . " zou nu verwijderd worden.\n");
			}
		}

		return parent::verwijderen($foreigncall);
	}

	/*
	 *  Wijzigt een tentamen met de wijzigingen van het form
	 *
	 * @param files De $_FILES-variabele waarin de bestanden zijn geüpload
	 * @param vak Het vak waarbij het tentamen hoort
	 * @return Een HtmlSpan met errors als het misgegaan is
	 */
	public function wijzigTentamen($files, $vak)
	{
		global $filesystem;

		$msg = array();

		// De $_FILES-array kan zelf ook aangeven of er errors zijn
		// Error nr 4 is dat er geen bestand was geüpload
		if($files['file']['error'] && $files['file']['error'] != 4)
		{
			$msg[] = _('Er waren fouten met het uploaden');
			return $msg;
		}

		if($this->valid()) {
			$this->opslaan();
			if($file = $files['file']['name'] && $files['file']['error'] != 4) {
				$this->setTentamen(true);
				$this->opslaan();
				if(!DEBUG)
				{
					// Haal eerst het bestaande bestand weg als dat bestaat
					if($filesystem->has($this->getBestand()))
					{
						$filesystem->delete($this->getBestand());
					}
					$stream = fopen($files['file']['tmp_name'], 'r+');
					$filesystem->writeStream($this->getBestand(), $stream);
					fclose($stream);
				}
			}
		} else {
			$msg[] = _('Er waren fouten');
		}

		return $msg;
	}

	public function geefAantalUitwerkingen()
	{
		return TentamenUitwerkingQuery::table()
			->whereProp('Tentamen', $this)
			->count();
	}

	/*
	 *  Geeft het aantal uitwerkingen dat zichtbaar is voor de gebruiker.
	 *
	 */
	public function geefAantalUitwerkingenZichtbaar()
	{
		//Als je tbc bent, krijg je het totaal aantal uitwerkingen
		if(hasAuth('tbc'))
			return $this->geefAantalUitwerkingen();

		$uitwerkingen = TentamenUitwerkingVerzameling::geefVanTentamen($this);

		$aantal = $uitwerkingen->geefAantalZichtbaar();

		return $aantal;
	}


	/*
	 *  Geeft aan of het persoon dat is ingelogd al een uitwerking heeft geupload voor dit tentamen.
	 *
	 */
	public function ingelogdPersoonHeeftUitwerking()
	{
		return TentamenUitwerkingVerzameling::geefVanTentamen($this)->filter('getUploader', Persoon::getIngelogd())->aantal() > 0;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
