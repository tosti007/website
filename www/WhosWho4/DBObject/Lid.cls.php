<?

/***
 * $Id$
 */
class Lid
	extends Lid_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	public static function velden ($welke = NULL)
	{
		$velden = parent::velden($welke);

		// Strip studentnr eruit voor get-requests
		if ($welke == 'get' && !hasAuth('bestuur'))
			$velden = array_diff($velden, array('Studentnr'));

		return $velden;
	}
	
	public static function zoekOpEmail($email)
	{
		//Vrijwel identiek aan die van Contact, maar nu met overerving
		//Handig als je alleen leden wilt hebben, zoals bij het aanvragen van een login
		global $WSW4DB;

		$id = $WSW4DB->q('MAYBEVALUE SELECT `ContactID`'
		                .' FROM `Contact`'
		                .' WHERE `email` = %s AND `Overerving` = "Lid" LIMIT 1'
		                , $email
		                );
		if(!$id)
			return NULL;

		return self::geef($id);
	}

	/** METHODE **/

	/**
	 * Overschrijft de defaultopslaan zodat ook voorkeuren worden (mooi) meegenomen.
	 */
	public function opslaan ($classname = 'Lid')
	{
		parent::opslaan($classname);

		// We slaan hier voor een Lid de persoonvoorkeur op om te voorkomen
		// dat er dingen mis gaa (zie Persoon::opslaan)
		// Sla de voorkeur op, of gooi hem weg als alles default is
		$voorkeur = $this->getVoorkeur();
		if (!$voorkeur->isDefault())
		{
			$voorkeur->opslaan();
		}
		else if ($voorkeur->getInDB())
		{
			$returnVal = $voorkeur->verwijderen();
			if(!is_null($returnVal)) {
				user_error($returnVal, E_USER_ERROR);
			}
		}
	}

	/**
	 *  Als er mentoren zijn dan staan we het verwijderen niet toe.
	 **/
	public function checkVerwijderen()
	{
		if($error = parent::checkVerwijderen())
			return $error;
		if($this->mentorVan()->aantal() > 0)
			return _('Dit lid is mentor (geweest)');
		//TODO: meer checks
		return false;
	}

	public function checkLidTot ()
	{
		if ($ret = parent::checkLidTot())
			return $ret;
		if ($this->getLidTot()->hasTime() && $this->getLidTot() < $this->getLidVan())
			return _('een lid kan niet eerder lid-af dan lid zijn');
		return false;
	}

	public function checkLidVan ()
	{
		if ($ret = parent::checkLidVan())
			return $ret;
		if ($this->getDatumGeboorte()->hasTime() && $this->getLidVan() < $this->getDatumGeboorte())
			return _('een lid kan niet eerder lid dan geboren zijn');
		return false;
	}

	public function url ()
	{
		return '/Leden/' . $this->geefID();
	}

	public function magBekijken ()
	{
		if ($this->magWijzigen())
			return true;

		if ($this->isBijnaLid() && !hasAuth('intro'))
			return false;

		switch ($this->getOpzoekbaar())
		{
			case 'A':	return hasAuth('oud-lid');
			case 'J':	return hasAuth('lid') && $this->isHuidigLid();
			case 'N':	return false;
			case 'CIE':	return hasAuth('actief') && CommissieVerzameling::vanPersoon(Persoon::getIngelogd())->hasLid($this);
			default:	return false;
		}
	}

	/**
	 *  Geef een willekeurig Lid.
	 *
	 * Deze methode gebruikt aan de achterkant de MySQL-functie RAND().
	 *
	 * @return Een willekeurig bepaald Lid dat opzoekbaar is.
	 */
	public static function geefWillekeurig()
	{
		global $WSW4DB;

		//Grijp de identiteit van een random lid en geef het lid terug!
		do {
			$lidId = $WSW4DB->q('VALUE SELECT `Lid`.`contactID` FROM `Lid` WHERE ' . WSW4_WHERE_OPZOEKBAAR_LID . ' ORDER BY RAND() LIMIT 1');
			$lid=self::geef($lidId);
		} while (!$lid->magBekijken());
		return $lid;
	}

	public function getStudies ($huidige = false)
	{
		$studies = LidStudieVerzameling::vanLid($this);
		if ($huidige)
		{
			return $studies->huidige();
		}
		return $studies;
	}

	/**
		 Retourneert het Lid object behorend bij het ingelogde lid,
		of null als er niet ingelogd is

		Als je NIET perse een Lid object nodig hebt moet je Persoon::getIngelogd()
		aanroepen.
	**/
	public static function getIngelogdLid(){
		global $auth;
		$contactid = $auth->getLidnr();
		if ($contactid) return Lid::geef($contactid);
		return null;
	}

	/**
		 Retourneert het Lid object met studentnummer $studnr.
     **/
	public static function geefLedenMetStudentnummer($studnr)
	{
		if (!preg_match("/^[f0-9][0-9]{6}$/i" , $studnr)) {
			// geen geldig studentnummer
			return new LidVerzameling();
		}
		return LidQuery::table()->whereProp('studentnr', $studnr)->verzamel();
	}

	/**
		@see Contact::controleerInformatie
	**/
	public function controleerInformatie($positie = null){
		global $BESTUUR;
		global $auth;

		$problemen = parent::controleerInformatie($positie);
		if ($positie != 2 && $positie != 3){
			if ($auth->getLidnr() == $this->geefID()) $persoon = 2;
			else $persoon = 3;
		}

		if (!$this->getStudentnr() && $this->isHuidigLid()){
			$prob['title'] = _('Ongeldig of ontbrekend studentnummer');
			$prob['level'] = 'warning';

			if ($persoon == 2){
				$prob['message'] = sprintf(_('We hebben een ongeldig (of geen) studentnummer van jou.' .
					'Dit hebben we nodig om te kunnen controleren of je nog studeert. Gelieve je ' .
					'studentnummer in een e-mail te sturen aan %s, dan zorgen wij ervoor dat je ' .
					'persoonsgegevens worden bijgewerkt.'), $BESTUUR['secretaris']->getEmailAdres()
				);
			} else {
				$prob['message'] = _('Deze persoon heeft geen studentnummer opgegeven');
			}
			$problemen[] = $prob;
		}


		return $problemen;
	}

	/**
		 Bepaalt of de persoon beschreven in dit Lid object op dit moment
		een bijnalid is van de vereniging
	**/
	public function isBijnaLid()
	{
		return $this->getLidToestand() == 'BIJNALID';
	}

	/**
		 Bepaalt of de persoon beschreven in dit Lid object op dit moment
		lid is van de vereniging
	**/
	public function isHuidigLid()
	{
		return $this->getLidToestand() == 'LID' || $this->getLidToestand() == 'BAL' || $this->getLidToestand() == 'LIDVANVERDIENSTE' || $this->getLidToestand() == 'ERELID';
	}
	/**
		 Bepaalt of de persoon beschreven in dit Lid object op dit moment
		oud-lid is van de vereniging
	**/
	public function isOudLid()
	{
		return $this->getLidToestand() == 'OUDLID';
	}

	/**
		 Retourneert alle mentorgroepen waar deze persoon in gezeten
		heeft in een IntroGroepVerzameling
	**/
	public function mentorgroepen()
	{
		$studies = LidStudieVerzameling::vanLid($this);
		$groepids = array();
		foreach ($studies as $studie){
			if (is_numeric($studie->getGroepGroepID())) $groepids[] = $studie->getGroepGroepID();
		}
		$groepids = array_unique($groepids);
		return IntroGroepVerzameling::verzamel($groepids);
	}
	/**
		 Retourneert alle mentorgroepen waar deze persoon mentor van is 
		geweest in een IntroGroepVerzameling
	**/
	public function mentorVan()
	{
		global $WSW4DB;
		$groepIds = $WSW4DB->q("COLUMN SELECT `groep_groepID` FROM `Mentor` WHERE `lid_ContactID`=%i", $this->geefID());
		return IntroGroepVerzameling::verzamel($groepIds);
	}

	/**
	 *  Pasfoto is ok bevonden, dus doe het werk om alles te fixxen.
	 */

	public static function afwijzenUploadedFoto($profielFoto)
	{
		$media = $profielFoto->getMedia();
		$media->verwijderVanFileSystem();

		$returnVal = $profielFoto->verwijderen();

		if(!is_null($returnVal)) {
			user_error($returnVal, E_USER_ERROR);
		}

		$returnVal = $media->verwijderen();

		if(!is_null($returnVal)) {
			user_error($returnVal, E_USER_ERROR);
		}
	}

	public static function accepteerUploadedFoto($profielFoto)
	{
		$lid = $profielFoto->getPersoon();

		$curfoto = ProfielFoto::zoekHuidig($lid);

		if($curfoto) {
			$curfoto->setStatus('OUD');
			$curfoto->opslaan();
		}

		$profielFoto->setStatus('HUIDIG');
		$profielFoto->opslaan();

		$media = $profielFoto->getMedia();

		$tag = new Tag();
		$tag->setMedia($media);
		$tag->setPersoon($lid);
		$tag->setTagger($lid);
		$tag->opslaan();

		$media->setLock(true);
		$media->opslaan();
	}

	public function maakLidaf($date){

		$this->setLidTot($date);
		if($date < new DateTimeLocale("now")){ 
			$this->setLidToestand("OUDLID");
		} //Else doen we dit niet en moet de cronjob dit nog doen voor ons zodra het lid wel lid-af is

		//Zoek de standaard mailings op en gooi deze weg.
		$idarray = array(array($this->geefID(), 1), array($this->geefID(), 7),
			array($this->geefID(), 8), array($this->geefID(), 9));
		$mailings = ContactMailingListVerzameling::verzamel($idarray);
		$returnVal = $mailings->verwijderen();
		if(!is_null($returnVal)) {
			user_error($returnVal, E_USER_ERROR);
		}

		$this->setVakIdOpsturen(false);
		$this->opslaan();

		$mail = $this->getEmail();
		if (!empty($mail))
		{
			$this->stuurLidafMail();
		}
	}

	/**
	 * @brief Maak van een bijnalid (bijvoorbeeld een introdeelnemer) een volwaardig lid.
	 *
	 * Na afloop wordt het lid opgeslagen.
	 *
	 * @param toestand De nieuwe toestand van het lid. Dit wil je INGESCHREVEN maken als ze nog moeten betalen.
	 * @param date NULL voor nu, of de datum van lid worden.
	 */
	public function bijnaLidToLid($toestand = "LID", $date = NULL) {
		if ($date == NULL) {
			$date = new DateTimeLocale("now");
		}

		$this->setLidVan($date);
		$this->setLidToestand($toestand);

		$this->opslaan();
	}

	/**
	 * @brief stuurt een `uitzwaaimail` naar dit lid.
	 *
	 * Stuur een mail naar een lid dat lidaf geworden is, om dit aan ze
	 * duidelijk te maken en wat andere informatie onder de aandacht te
	 * brengen.
	 * 
	 */
	private function stuurLidafMail()
	{
		global $BESTUUR;

		$taal = $this->getVoorkeur()->getTaal();

		$uitzwaaimail = $taal == "EN" ? $this->getUitzwaaiMailEN(SECRNAME) : $this->getUitzwaaiMailNL(SECRNAME);

		$mail = new Email();
		$mail->setTo($this);
		$mail->setFrom($BESTUUR['secretaris']);
		$mail->setSubject($uitzwaaimail['subject']);

		// Voeg de HTML body toe
		$mail->setBody($uitzwaaimail['body'], true);

		if ($mail->valid()) {
			$mail->send();
		} else {
			user_error("Uitzwaaimail is geen geldige mail! Krijg nou wat?!", E_USER_ERROR);
		}
	}

	/**
	 *  Zo nu en dan worden personen alsnog lid, hiervoor moeten we wat extra
	 *  informatie aan de DB toevoegen en dingen van het object goed zetten.
	 **/
	public static function persoonToLid(Persoon $pers)
	{
		global $WSW4DB;

		$now = new DateTimeLocale();

		$WSW4DB->q('INSERT INTO `Lid`'
		          .' SET `contactID` = %i'
		          .   ', `lidToestand` = %s'
		          .   ', `lidMaster` = %i'
		          .   ', `lidVan` = %s'
		          .   ', `lidTot` = %s'
		          .   ', `studentnr` = %s'
		          .   ', `opzoekbaar` = %s'
		          .   ', `inAlmanak` = %i'
		          .   ', `planetRSS` = %s'
		          , $pers->contactID
		          , 'Lid'
		          , 0
		          , $now->strftime('%F %T')
		          , null
		          , null
		          , 'A'
		          , 0
		          , null
		          );

		//Pas de overerving aan
		$WSW4DB->q('UPDATE `Contact`'
		           .' SET `overerving` = "Lid"'
		           .   ', `gewijzigdWanneer` = %s'
		           .   ', `gewijzigdWie` = %i'
		           .' WHERE `contactID` = %i'
		           , $now->strftime('%F %T')
		           , Persoon::getIngelogd()->geefID()
		           , $pers->contactID
		           );

		//Stel de cache bij
		self::cache(array(array($pers->geefID())), true);

		//En geef het juiste object terug.
		return self::geef($pers->geefID());
	}

	/*
		Stel de default lidinstellingen in bij een lid:
		* Ontvang wel: vakid, actmailing NL, boekenmailen alle studenten, stage en banen mailing
		* Opzoekbaarheid: J
		* In almanak: Ja

	 */
	public function defaultLidInstelling()
	{
		$mailings = new ContactMailingListVerzameling();
		$mailings->voegtoe(new ContactMailingList($this->geefID(), 1)); //Stage en banen
		$mailings->voegtoe(new ContactMailingList($this->geefID(), 7)); //Activiteiten NL
		$mailings->voegtoe(new ContactMailingList($this->geefID(), 9)); //Boekenmail 

		$huidigeMailings = MailingListVerzameling::vanContact($this);
		$wilMailingKeys = array();
		foreach($huidigeMailings as $huidigeMailing)
			$wilMailingKeys[] = array($this->geefID(), $huidigeMailing->geefID());

		//Filter de mailings die je al hebt er uit als dat mogelijk is.
		if($wilMailingKeys)
			$mailings->complement(ContactMailingListVerzameling::verzamel($wilMailingKeys));

		$mailings->opslaan();

		//Als er geen formulier is gebruikt om dit in te stellen dan doen we de default.
		if(!trypar("Lid[InAlmanak]"))
			$this->setInAlmanak(true);
		if(!trypar("Lid[Opzoekbaar]"))
			$this->setOpzoekbaar('A');

		$this->setVakidOpsturen(true)
			->opslaan();
	}

	public function heeftDibs()
	{
		global $WSW4DB;

		$ret = $WSW4DB->q('MAYBEVALUE SELECT `persoon_contactID` FROM `DibsInfo` '
			. 'WHERE `persoon_contactID` = %i'
			, $this->geefID());

		return !is_null($ret);
	}

	/**
	 * @brief geeft de template voor de mail in het nederlands die afgestudeerden krijgen
	 * @param afzenderNaam de naam die de mail ondertekend
	 * @return array met subject en body van de uitzwaaimail
	 */
	private function getUitzwaaiMailNL($afzenderNaam)
	{
		// naam met aanspreekvorm van degene die uitgezwaaid wordt
		$naam = PersoonView::naam($this, WSW_NAME_VOORNAAM);
		// leesbare datum waarop het lid geen lid meer was
		$lidAfDatum = $this->getlidTot()->strftime('%Y-%m-%d');

		$actiefTekst = "";
		if ($this->isActief()) {
			$actiefTekst =
<<<HEREDOC
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Tot slot willen we je bedanken voor je inzet als actief lid tijdens je studie. De activiteiten van A&ndash;Eskwadraat worden mogelijk gemaakt door mensen zoals jij! Ik wens je veel succes in de toekomst en hopelijk tot ziens bij de volgende re&uuml;nie of een activiteit van de alumnivereniging!</p>	
HEREDOC;
		}

		$mailSubject = "Uitschrijving bij A–Eskwadraat";
		$mailBody = 
<<<HEREDOC
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
	<tr>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; ">&nbsp;</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		<td style="font-family: sans-serif;text-align:center; font-size: 14px; vertical-align: top; margin: 0 auto; max-width: 580px; padding: 0px; width: 580px;">
			<img alt="A-Eskwadraat logo" height="120px;" src="https://www.a-eskwadraat.nl/Vereniging/Commissies/promocie/aes2logo-rood.png"></img>
		</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 580px; width: 580px;">
			<div style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;">
				<table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
					<tr>
						<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
								<tr>
									<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Beste {$naam}</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
We hebben gezien dat je niet langer studeert aan de Universiteit Utrecht aan een van de bij ons aangesloten studierichtingen. Dit heeft tot gevolg dat je per {$lidAfDatum} geen lid meer bent van A&ndash;Eskwadraat.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
We hopen dat A&ndash;Eskwadraat een nuttige en aangename bijdrage heeft geleverd aan je studententijd! Graag horen we van je als je suggesties hebt over wat we beter hadden kunnen doen.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Vanuit A&ndash;Eskwadraat blijven we graag in contact met je door middel van ons Alumni magazine A&ndash;Eskwadraat Roots, wat ieder half jaar uitkomt. Hiermee houden we je op de hoogte van het laatste nieuws. Dit magazine kan je kosteloos ontvangen door je op te geven.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Daarnaast is er A.V. Roots, de alumnivereniging van A&ndash;Eskwadraat, de plek om in contact te blijven met studie en vakgenoten.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Opgeven voor het alumni magazine en de alumnivereniging kan via onderstaande pagina's.</p>

										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin-bottom: 15px; width: 100%;"><tbody><tr>
											<td style="font-family: sans-serif; font-size: 14px; margin-right:100px; vertical-align: top; background-color: #97001e; border-radius: 5px; text-align: center;"> 
												<a href="https://www.a-eskwadraat.nl/Leden/Alumni/" target="_blank" style="display: inline-block; color: #ffffff; background-color: #97001e; border: solid 1px #97001e; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #97001e; width: 100%;">Magazine</a>
											</td>
											<td>&nbsp;</td>
											<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #97001e; border-radius: 5px;  text-align: center;">
												<a href="https://avroots.nl" target="_blank" style="display: inline-block; color: #ffffff; background-color: #97001e; border: solid 1px #97001e; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #97001e; width: 100%;">Alumnivereniging</a>
											</td>
										</tr></tbody></table>
{$actiefTekst}
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Met vriendelijke groeten,<br />
<br />
{$afzenderNaam}<br />
Secretaris<br />
Namens het bestuur van Studievereniging A&ndash;Eskwadraat<br />
<br /></p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
P.S. Weten waar al je studiegenoten straks aan het werk gaan?<br />
Wordt ook lid van de <a href="https://www.linkedin.com/groups?gid=55143">LinkedIn groep</a> van A&ndash;Eskwadraat.					
										</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div style="clear: both; margin-top: 10px; text-align: center; width: 100%;">
					<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
						<tr>
							<td style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
								<span style="color: #999999; font-size: 12px; text-align: center;">
Je ontvangt deze e-mail omdat je lid bent (geweest) van A&ndash;Eskwadraat.<br/>
Dit betreft een eenmalige e-mail.</span>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	</tr>
</table>
HEREDOC;

		return array(
			'subject' => $mailSubject,
			'body' => $mailBody
		);
	}

	/**
	 * @brief geeft de template voor de mail in het engels die afgestudeerden krijgen
	 * @param afzenderNaam de naam die de mail ondertekend
	 * @return array met subject en body van de uitzwaaimail
	 */
	private function getUitzwaaiMailEN($afzenderNaam)
	{
		// naam met aanspreekvorm van degene die uitgezwaaid wordt
		$naam = PersoonView::naam($this, WSW_NAME_VOORNAAM);
		// leesbare datum waarop het lid geen lid meer was
		$lidAfDatum = $this->getlidTot()->strftime('%Y-%m-%d');

		$actiefTekst = "";
		if ($this->isActief()) {
			$actiefTekst =
<<<HEREDOC
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Finally we would like to thank you for your hard work as an active member of this association.  The activities organised by A&ndash;Eskwadraat are possible because of people like you!  I wish you lots of success and good fortune in the future and hope to see you again some day, for example at a reunion or an activity organised by the alumni association.</p>	
HEREDOC;
		}

		$mailSubject = "End of A–Eskwadraat membership";
		$mailBody = 
<<<HEREDOC
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
	<tr>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; ">&nbsp;</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		<td style="font-family: sans-serif;text-align:center; font-size: 14px; vertical-align: top; margin: 0 auto; max-width: 580px; padding: 0px; width: 580px;">
			<img alt="A-Eskwadraat logo" height="120px;" src="https://www.a-eskwadraat.nl/Vereniging/Commissies/promocie/aes2logo-rood.png"></img>
		</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	</tr>
	<tr>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 580px; width: 580px;">
			<div style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;">
				<table style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
					<tr>
						<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
							<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
								<tr>
									<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Dear {$naam}</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
We see that you are no longer enrolled in a studies at Utrecht University for one of our study programmes. This means that from {$lidAfDatum} you will no longer be a member of A&ndash;Eskwadraat.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
We hope that A&ndash;Eskwadraat has been a nice and useful part of your time as a student. If there is anything that could have been better, we would gladly receive some feedback!</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
A&ndash;Eskwadraat would like to stay in touch with you through our Alumni magazine the A&ndash;Eskwadraat Roots, which has a publication twice a year. Through this magazine we will keep you up-to-date with the latest news. You can receive this magazine for free by subscribing to it.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
There is also A.V. Roots, the assocation for alumni of A&ndash;Eskwadraat. This is the perfect place to stay in touch with friends, or build a network with other alumni.</p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Subscribing to the alumni magazine or the alumni association can be done by clicking the buttons below.</p>

										<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; margin-bottom: 15px; width: 100%;"><tbody><tr>
											<td style="font-family: sans-serif; font-size: 14px; margin-right:100px; vertical-align: top; background-color: #97001e; border-radius: 5px; text-align: center;"> 
												<a href="https://www.a-eskwadraat.nl/Leden/Alumni/" target="_blank" style="display: inline-block; color: #ffffff; background-color: #97001e; border: solid 1px #97001e; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #97001e; width: 100%;">Magazine</a>
											</td>
											<td>&nbsp;</td>
											<td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #97001e; border-radius: 5px;  text-align: center;">
												<a href="https://avroots.nl" target="_blank" style="display: inline-block; color: #ffffff; background-color: #97001e; border: solid 1px #97001e; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #97001e; width: 100%;">Alumni association</a>
											</td>
										</tr></tbody></table>
{$actiefTekst}
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
Kind regards,<br />
<br />
{$afzenderNaam}<br />
Secretary<br />
On behalf of the board of study association A&ndash;Eskwadraat<br />
<br /></p>
										<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
P.S. Would you like to know where your fellow students go to after their studies?<br />
Become a member of the <a href="https://www.linkedin.com/groups?gid=55143">LinkedIn group</a> of A&ndash;Eskwadraat.					
										</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div style="clear: both; margin-top: 10px; text-align: center; width: 100%;">
					<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
						<tr>
							<td style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
								<span style="color: #999999; font-size: 12px; text-align: center;">
You receive this email because you are/were a member of A&ndash;Eskwadraat.<br/>
This is not a recurring e-mail.</span>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</td>
		<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
	</tr>
</table>
HEREDOC;

		return array(
			'subject' => $mailSubject,
			'body' => $mailBody
		);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
