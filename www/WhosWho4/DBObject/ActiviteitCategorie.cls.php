<?
/**
 *
 *  'AT'AUTH_GET:gast
 */
class ActiviteitCategorie
	extends ActiviteitCategorie_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($titelNL = "", $titelEN = "")
	{
		parent::__construct(); // ActiviteitCategorie_Generated

		$this->setTitel($titelNL, "nl");
		$this->setTitel($titelEN, "en");

		$this->actCategorieID = NULL;
	}

	/**
	 *  Je mag een categorie alleen verwijderen als die niet meer gebruikt wordt in een activiteit ooit.
	 */
	public function magVerwijderen() {
		return hasAuth('bestuur') && $this->getActiviteitInformatieVerzameling()->aantal() == 0;
	}
}
