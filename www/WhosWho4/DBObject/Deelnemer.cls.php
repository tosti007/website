<?

/***
 * $Id$
 */
class Deelnemer
	extends Deelnemer_Generated
{
	//Wordt gebruikt bij het verwerken van antwoorden
	protected $antwoorden;

	/** CONSTRUCTOR **/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}

	/** 
	 *  /brief Bestuur, de persoon zelf en de verantwoordelijke voor de commissie
	 *   mogen deelenemer aanpasssen.
	 **/
	public function magWijzigen()
	{
		if($this->getPersoon()->magWijzigen() || $this->getActiviteit()->magWijzigen())
			return true;

		return false;
	}
	public function magVerwijderen()
	{
		if($this->getPersoon()->magWijzigen() || $this->getActiviteit()->magWijzigen())
			return true;

		return false;
	}

	public function url()
	{
		return $this->getActiviteit()->deelnemersURL() . '/' . $this->getPersoonContactID();
	}

	public function uitschrijfURL()
	{
		return $this->url() . "/Uitschrijven";
	}

	/**
	 *  Kan de gebruiker iets aanpassen aan deze deelnemer?
	 *
	 * Een activiteit zonder vragen heeft namelijk een lege wijzigpagina, dus
	 * dat is raar om als optie te geven.
	 * Ook als de deelnemer uberhaupt niet gewijzigd mag worden, geven we nee.
	 *
	 * @returns True als er iets te wijzigen valt, anders False.
	 */
	public function wijzigenHeeftZin()
	{
		if (!$this->magWijzigen()) {
			return FALSE;
		}
		return $this->getAntwoorden()->aantal() > 0;
	}

	/**
	 *  Geef de verzameling van antwoorden terug. Als deze er nog niet is haal deze dan op
	 *  of voeg de lege antwoorden toe die nog niet ingevuld zijn.
	 **/
	public function getAntwoorden()
	{
		if(!$this->antwoorden instanceof DeelnemerAntwoordVerzameling)
		{
			$act = $this->getActiviteit();
			$vragen = ActiviteitVraagVerzameling::vanActiviteit($act);
			$this->antwoorden = new DeelnemerAntwoordVerzameling();

			foreach($vragen as $vraag)
			{
				$antwoord = DeelnemerAntwoord::geef($this, $vraag);
				if(!$antwoord) {
					$antwoord = new DeelnemerAntwoord($this, $vraag);
				}
				$this->antwoorden->voegtoe($antwoord);
			}
		}
		return $this->antwoorden;
	}

	public function setAntwoorden(DeelnemerAntwoordVerzameling $antwoorden)
	{
		$this->antwoorden = $antwoorden;
	}

	public function opslaan($classname = 'Deelnemer')
	{
		/** Sla eerst de deelnemer zelf op **/
		parent::opslaan($classname);

		/** Verwerk antwoorden **/
		$antwoorden = $this->getAntwoorden();
		foreach($antwoorden as $antwoord)
		{
			// Als het veld 'antwoord' in een DeelnemerAntwoord de lege string is
			// gaat het mis met het opslaan van DeelnemerAntwoord, check daarom 
			// hier of het object valid is
			if($antwoord->valid())
				$antwoord->opslaan();
		}

	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
