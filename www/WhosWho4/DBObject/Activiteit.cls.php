<?
use Endroid\QrCode\QrCode;
// $Id$

/**
 *  Representeert een activiteit
 */
class Activiteit
	extends Activiteit_Generated
{
	public function __construct()
	{
		parent::__construct();
	}

	public function __toString ()
	{
		return ActiviteitView::titel($this);
	}

	/**
	 *  Controleer of mensen zich kunnen inschrijven/uitschrijven
	 *
	 * @return True als het kan, false als het niet kan
	 */
	public function inschrijfbaar ()
	{
		if (!$this->getInschrijfbaar())
			return false;

		if ($this->getMaxDeelnemers() > 0 && $this->deelnemers()->aantal() >= $this->getMaxDeelnemers())
			return false;

		// Het is raar dat je wil inschrijven nadat een activiteit al geeindigd is,
		//  dus laten we het niet toe. (Tenzij iemand met een usecase komt!)
		if (new DateTimeLocale() > $this->getMomentEind())
			return false;

		if ($this->getDatumInschrijvenMax()->hasTime()) {
			// We tellen een dag op bij de grens, zodat het tot en met de dag zelf kan
			// (het is namelijk populair om de grens op de dag van de activiteit te zetten)
			return new DateTimeLocale() < $this->getDatumInschrijvenMax()->add(new DateInterval("P1D"));
		} else {
			return true;
		}
	}

	/**
	 *  Controleert of een activiteit uitschrijfbaar is
	 *
	 * @return True als het kan, false als het niet kan
	 */
	public function uitschrijfbaar ()
	{
		if (!$this->getInschrijfbaar())
			return false;

		// Zelfde redenatie als inschrijfbaar(), doe het zo tenzij we een usecase hebben
		if (new DateTimeLocale() > $this->getMomentEind())
			return false;

		if ($this->getDatumUitschrijven()->hasTime()) {
			return new DateTimeLocale() < $this->getDatumUitschrijven()->add(new DateInterval("P1D"));
		} else {
			return true;
		}
	}

	/**
	 *  Geeft de titel van een activiteit terug
	 *
	 * @param lang De taal van de titel
	 *
	 * @return string De titel
	 */
	public function getTitel($lang = NULL)
	{
		switch(get_class($this))
		{
		case 'Activiteit':
			return _('Acitiviteit') . ' #' . $this->geefID();
			break;
		case 'ActiviteitInformatie':
			return $this->getTitel($lang);
			break;
		case 'ActiviteitHerhaling':
			return $this->getInformatie()->getTitel($lang);
			break;
		}
	}

	/**
	 *  Geeft alle CommissieActiviteiten van een activiteit
	 *
	 * @return CommissieActiviteitVerzameling De CommissieActiviteitVerzameling
	 */
	public function getActiviteitCommissies()
	{
		if($this instanceof ActiviteitHerhaling)
			return CommissieActiviteitVerzameling::vanActiviteit($this->getInformatie());
		return CommissieActiviteitVerzameling::vanActiviteit($this);
	}

	/**
	 *  Geeft alle Commissies van een activiteit
	 *
	 * @return CommissieVerzameling De CommissieVerzameling
	 */
	public function getCommissies()
	{
		if($this instanceof ActiviteitHerhaling)
			return CommissieVerzameling::vanActiviteit($this->getInformatie());
		return CommissieVerzameling::vanActiviteit($this);
	}

	/**
	 *  Geeft het aantal commissies terug wat aan de act gekoppeld is
	 *
	 * @return Het aantal commissies
	 */
	public function getAantalCommissies ()
	{
		return $this->getActiviteitCommissies()->aantal();
	}

	/**
	 *  Geeft alle ActiviteitVragen terug die bij de act horen
	 *
	 * @return ActiviteitVraagVerzameling De ActiviteitVraagVerzameling van de act
	 */
	public function getVragen()
	{
		if($this instanceof ActiviteitHerhaling)
			return ActiviteitVraagVerzameling::vanActiviteit($this->getInformatie());
		return ActiviteitVraagVerzameling::vanActiviteit($this);
	}

	public function getIdealKaartjes()
	{
		if($this instanceof ActiviteitHerhaling)
			return iDealKaartjeVerzameling::vanActiviteit($this->getInformatie());
		return iDealKaartjeVerzameling::vanActiviteit($this);
	}

	public function getIdealAntwoorden()
	{
		if($this instanceof ActiviteitHerhaling)
			return iDealAntwoordVerzameling::vanActiviteit($this->getInformatie());
		return iDealAntwoordVerzameling::vanActiviteit($this);
	}

	/**
	 *  Overschrijft de functie die het veld voor de eindtijd controleert.
	 */
	public function checkMomentEind()
	{
		if ($ret = parent::checkMomentEind())
			return $ret;
		if (($this->getMomentEind()->hasTime()) && $this->getMomentEind() <= $this->getMomentBegin())
			return _('Einddatum moet later zijn dan begindatum');
	}

	/**
	 *  Checkt of de act verwijderd mag worden
	 */
	public function checkVerwijderen()
	{
		if(MediaVerzameling::getTopratedByAct($this))
			return _("Er zijn foto's/video's gekoppeld aan deze activiteit");
		if($this->getMomentBegin() < new DateTimeLocale('NOW'))
			return _("Deze activiteit is al begonnen!");
		if($this->getIdealKaartjes()->aantal() != 0)
			return _('Deze activiteit heeft idealkaarjtes');
		return parent::checkVerwijderen();
	}

	/**
	 *  Retourneert alle deelnemers ingeschreven voor deze activiteit
	 *
	 * @return DeelnemerVerzameling Een DeelnemerVerzameling van Deelnemer objecten
	 **/
	public function deelnemers()
	{
		return DeelnemerVerzameling::vanActiviteit($this);
	}

	/**
	 *  Geeft een array van alle deelnemers met als key het lidnr en value de naam
	 *
	 * @return array met als key lidnr en als value naam
	 **/
	public function deelnemersArray()
	{
		$deelnemers = $this->deelnemers();
		$array = array();
		foreach($deelnemers as $deelnemer) {
			$array[$deelnemer->getPersoon()->getContactID()] = PersoonView::naam($deelnemer->getPersoon());
		}

		return $array;
	}

	/**
	 *  Checkt of een deelnemer is ingeschreven voor deze activiteit
	 *
	 * @return bool
	 **/
	public function isDeelnemer(Persoon $lid)
	{
		foreach($this->deelnemers() as $deelnemer)
			if($lid->getContactID() == $deelnemer->getPersoonContactID())
				return true;
		return false;
	}

	/**
	 *  Returneert een deelnemer aan de activiteit
	 *
	 * @return Deelnemer
	 **/
	public function deelnemer(Persoon $lid)
	{
		return Deelnemer::geef($lid, $this);
	}

	/**
	 *  Retourneert de URL waarop deze activiteit te vinden is
	 *
	 * @return string Een URL (string) waarop informatie over deze activiteit te
	 * vinden is
	 **/
	public function url()
	{
		/*
		//Als er meerdere cies zijn, plak ze aan elkaar
		 */
		$cies = $this->getCommissies();

		$cieURL = '';
		foreach($cies as $cie)
			$cieURL .= '_' . $cie->getLogin();
		$cieURL = trim($cieURL, '_');

		return '/Activiteiten/' . $cieURL . '/' . $this->geefID() . '/' . $this->getActURLActnaam();
	}

	/**
	 * @brief Geeft de URL naar de map waarin dingen als deelnemers bekijken/toevoegen/wijzigen gedaan kan worden.
	 */
	public function deelnemersURL()
	{
		return $this->url() . '/Deelnemers';
	}

	/**
	 *  Geeft een url naar de fotos die bij de activiteit horen
	 *
	 * @param withfotoid Bool of het fotoid in de url moet
	 *
	 * @return De url als string
	 */
	public function fotoUrl($withfotoid = false)
	{
		if($withfotoid) {
			$fotos = MediaVerzameling::geefSelectie('activiteit', $this);
			return $this->url() . '/Fotos#' . end($fotos[0]);
		} else {
			return $this->url() . '/Fotos';
		}
	}

	/**
	 *  Naam van de activiteit om te gebruiken in URLs
	 *
	 * @return De acturlnaam als string
	 */
	public function getActURLActnaam()
	{
		$act = $this->getInformatie();
		return preg_replace('/[^a-z0-9.!@$^&*()_=,-]/i', '', $act->getTitel());
	}

	/**
	 *  Returneert of de ingelogde persoon het recht heeft om altijd op deze activiteit in te schrijven.
	 */
	public function magAltijdInschrijven ()
	{
		global $auth;
		return Activiteit::magKlasseWijzigen() || $auth->hasCiesAuth($this->getCommissies());
	}
	
	/**
	 *  Returneert of de ingelogde persoon het recht heeft om deze activiteit te wijzigen.
	 */
	public function magWijzigen ()
	{
		global $auth;
		return Activiteit::magKlasseWijzigen() || $auth->hasCiesAuth($this->getCommissies()) || hasAuth('promocie');
	}

	/**
	 *  Geeft terug of het ingelogd persoon de act mag verwijderen
	 */
	public function magVerwijderen()
	{
		return $this->magWijzigen();
	}

	/**
	 *  Geeft terug of het ingelogd persoon de activiteit mag bekijken
	 */
	public function magBekijken ()
	{
		//Als de activiteit publiek is mag het altijd
		if ($this->getToegang() == 'PUBLIEK')
			return true;
		//En anders moet je de activiteit beheren, of er voor ingeschreven staan
		return Activiteit::magKlasseBekijken() || $this->magWijzigen() || Lid::getIngelogd() && $this->isDeelnemer(Lid::getIngelogd());
	}

	/**
	 *  Haalt het ActiviteitInformatie-object terug wat bij de act hoort.
	 * Bij ActiviteitInforamtieobjecten returnt de functie this, in het geval van
	 * herhalingen returnt hij de parent van de herhaling
	 *
	 * @return ActiviteitInformatie Het ActiviteitInformatie-object
	 */
	public function getInformatie()
	{
		switch(get_class($this))
		{
		case 'Activiteit':
			throw new BadFunctionCallException("Klasse Activiteit moet een extensie hebben!");
		case 'ActiviteitInformatie':
			return $this;
			break;
		case 'ActiviteitHerhaling':
			return $this->getParent()->getInformatie();
			break;
		}
	}

	/**
	 *  Een functie die controleert of een activiteit herhalingen heeft
	 *
	 * @return True als er herhalingen zijn, false anders
	 */
	public function hasHerhalingsChildren ()
	{
		global $WSW4DB;
		$count = $WSW4DB->q('VALUE SELECT COUNT(*)'
			.' FROM `ActiviteitHerhaling`'
			.' WHERE `parent_activiteitID` = %i',
			$this->geefID());
		return ($count > 0);
	}

	/**
	 *  Geeft alle herhalingsactiviteiten van een activiteit
	 *
	 * @return ActiviteitHerhalingVerzameling De ActiviteitVerzameling met herhalingen
	 */
	public function getHerhalingsChildren ()
	{
		return ActiviteitHerhalingVerzameling::geefHerhalingsChildren($this);
	}

	protected static function veldenActiviteit ($velden = null)
	{
		switch ($velden) {
		case 'nieuw':
			$ret = parent::veldenActiviteit('set');
			break;
		case 'datum':
			$ret = array('momentBegin', 'momentEind');
			break;
		default:
			$ret = parent::veldenActiviteit($velden);
		}
		return $ret;
	}

	/**
	 *  Geeft de eerste zoveel fotos van de act terug
	 *
	 * @param aantal Het aantal fotos wat je terug wilt
	 *
	 * @return De MediaVerzameling van fotos
	 */
	public function getRangedFotos($aantal = 6)
	{
		return MediaQuery::table()
			->whereProp('Activiteit', $this)
			->orderByDesc('rating')
			->limit($aantal)
			->verzamel();
	}

	/**
	 *  Controleert of een activiteit premium-fotos heeft
	 */
	public function heeftPremiumFotos() {
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `mediaID` '
			. 'FROM `Media` '
			. 'WHERE `premium` = 1 '
			. 'AND `activiteit_activiteitID` = %i'
			, $this->getActiviteitID());

		return sizeof($ids) != 0;
	}

	public function posterPath($usedExtensions = NULL, $onlyExisting = TRUE, $withLive = TRUE)
	{
		global $FW_EXTENSIONS_FOTO, $filesystem;

		if ($usedExtensions === NULL) $usedExtensions = $FW_EXTENSIONS_FOTO;

		$prefixes = array();

		if (DEBUG) $prefixes[] = 'debug-';

		if (!DEBUG || $withLive) $prefixes[] = '';

		foreach ($prefixes as $prefix) {
			foreach ($usedExtensions as $ext) {
				$path = ACTPOSTERS_SRV . $prefix . $this->activiteitID . '.' . $ext;
				if (!$onlyExisting || $filesystem->has($path)) {
					return $path; // Deze extensie wil werken :)
				}
			}
		}
		return NULL;
	}

	/**
	 *  Geeft de master-qr-code die bij een act hoort die eerst gescand moet
	 * worden voordat de huidige sessie mag scannen bij deze activiteit
	 *
	 * @return De master-qr-code als rauwe image-data
	 */
	public function getMasterQRCode()
	{
		$code = hash('sha512', QRCODE_SALT . $this->geefID());
		$qrCode = new QrCode($code);
		$qrCode->setSize(490);
		$qrCode->setMargin(60);
		$qrCode->writeFile("/tmp/qr-code.$code");

		$qrcode = imagecreatefrompng("/tmp/qr-code.$code");
		unlink("/tmp/qr-code.$code");

		$black = imagecolorallocate($qrcode, 0x00, 0x00, 0x00);
		imagefttext($qrcode, 16, 0, 5, 30, $black, LIBDIR . "/Consolas.ttf", $this->getTitel());

		ob_start();
		imagepng($qrcode);
		$image = ob_get_contents();
		ob_end_clean();

		return $image;
	}

	/**
	 *  Geeft het maximum aantal computers wat gereserveerd kan worden tussen begin en eind
	 *
	 * @param begin De begintijd
	 * @param eind De eindtijd
	 *
	 * @return De maximale hoeveelheid computers om te reserveren
	 */
	public function getMaxAantalComputers($begin, $eind)
	{
		global $WSW4DB;

		// We doen hier 2x een LEFT JOIN op ActiviteitInformatie zodat we ook
		// de info van de herhalingen krijgen
		$reserveringen = $WSW4DB->q('TABLE SELECT `info1`.`aantalComputers` AS `ac1`, `info2`.`aantalComputers` AS `ac2`, '
			. '`act`.`momentBegin`, `act`.`momentEind`, `act`.`activiteitID` '
			. 'FROM `Activiteit` AS `act` '
			. 'LEFT JOIN `ActiviteitInformatie` AS `info1` ON `info1`.`activiteitID` = `act`.`activiteitID` '
			. 'LEFT JOIN `ActiviteitHerhaling` AS `her` ON `her`.`activiteitID` = `act`.`activiteitID` '
			. 'LEFT JOIN `ActiviteitInformatie` AS `info2` ON `info2`.`activiteitID` = `her`.`parent_activiteitID` '
			. 'WHERE `act`.`momentBegin` <= %s '
			. 'AND `act`.`momentEind` >= %s '
			. ((is_null($this->geefID()))?' %_':' AND `act`.`activiteitID` != %i')
			, $eind->strftime("%F %T")
			, $begin->strftime("%F %T")
			, $this->geefID()
		);

		//Vervolgens gaan we voor elk van deze gevonden reserveringen overlappende reserveringen zoeken.
		//Het maximum wat we hieruit krijgen geeft dan aan wat het maximaal aantal computers is wat gereserveerd zal zijn
		//tijdens de reservering die we willen doen: precies wat we nodig hebben.
		$max = Register::getValue("maxComputers", 0);
		$maxcomps = 0;

		$reserveringen = array_filter($reserveringen, function($reservering) {
			return !$this->reserveringenCompatibel(Activiteit::geef($reservering[4]));
		});
		foreach ($reserveringen as $reservering)
		{
			$aantal = self::berekenOverlap($reserveringen, $reservering);
			if ($aantal > $maxcomps) {
				$maxcomps = $aantal;
			}
		}
		$max -= $maxcomps;

		return $max;
	}

	/**
	 * @brief Geef of de computerreserveringen van de activiteit tegelijk kunnen.
	 *
	 * Zo wil je bijvoorbeeld niet je eigen reserveringen tellen,
	 * of de reserveringen van een activiteit die je gaat vervangen.
	 *
	 * @param act De activiteit met gelijktijdige reserveringen.
	 * @returns Een bool of het compatibel is.
	 */
	public function reserveringenCompatibel(Activiteit $act) {
		return $act == $this;
	}

	/**
	 *  Submethode van checkAantalComputers. Berekent voor een bepaalde
	 * computerreservering overlappingen met een groep computerreserveringen.
	 */
	static public function berekenOverlap($reserveringen, $reservering)
	{
		$aantalIndex = (is_null($reservering[0])) ? 1 : 0;
		$aantalbegin = $reservering[$aantalIndex];
		$begin = $reservering[2];
		$eind = $reservering[3];
		//We berekenen hoeveel computers er op het begin-tijdstip van de betreffende reservering gereserveerd zijn.
		foreach($reserveringen as $reserv)
		{
			if ($reserv != $reservering && $reserv[2] < $begin && $reserv[3] > $begin)
			{
				$aantalbegin += (is_null($reserv[0])) ? $reserv[1] : $reserv[0];
			}
		}
		$aantaleind = $reservering[$aantalIndex];
		//Hetzelfde doen we voor het eind-tijdstip van de betreffende reservering.
		foreach($reserveringen as $reserv)
		{
			if ($reserv != $reservering && $reserv[2] < $eind && $reserv[3] > $eind)
			{
				$aantaleind += (is_null($reserv[0])) ? $reserv[1] : $reserv[0];
			}
		}
		$aantalzelfdetijd = -$reservering[$aantalIndex];
		//Met de vorige loops kunnen we geen reserveringen vinden die op precies hetzelfde tijdstip staan als de betreffende reservering.
		//Deze tellen we er ook nog bij op.
		foreach($reserveringen as $reserv)
		{
			if ($reserv[2] == $begin && $reserv[3] == $eind)
			{
				$aantalzelfdetijd += (is_null($reserv[0])) ? $reserv[1] : $reserv[0];
			}
		}
		if ($aantalbegin > $aantaleind)
			return $aantalbegin + $aantalzelfdetijd;
		return $aantaleind + $aantalzelfdetijd;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
