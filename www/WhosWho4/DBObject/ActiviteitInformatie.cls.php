<?
/**
 *
 *  'AT'AUTH_GET:gast
 */
class ActiviteitInformatie
	extends ActiviteitInformatie_Generated
{
	/**
	 * @brief De ActiviteitHerhaling waaruit deze activiteit is gepromoveerd.
	 *
	 * Tijdelijke waarde, hebben we nodig om bij geldigheidschecks te doen
	 * alsof de herhaling waaruit we gepromoveerd zijn, niet (meer) bestaat.
	 */
	protected $promotiePapa;

	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ActiviteitInformatie_Generated

		$this->activiteitID = NULL;
	}

	public function magBekijken()
	{
		//Als de activiteit publiek is mag het altijd
		if ($this->getToegang() == 'PUBLIEK')
			return true;

		//En anders moet je de activiteit beheren, of er voor ingeschreven staan
		return $this->magWijzigen() || (Lid::getIngelogd() && $this->isDeelnemer(Lid::getIngelogd()));
	}

	public function opslaan($classname = 'ActiviteitInformatie')
	{
		//Is het object nieuw en moet het door de VOC vertaald worden?
		/*if(stripos($this->getWerftekst('en'), "[Translation in progress]") !== false && !$this->getInDB() && $this->getToegang() == "PUBLIEK")
			$stuurCieMail = true;
		else
			$stuurCieMail = false;*/

		//Sla de aanpassingen van de activiteit nu pas op voor het geval je een nieuwe act maakt.
		parent::opslaan($classname);

		//Eerst opslaan, dan pas mailen anders hebben we geen URL
		/*if($stuurCieMail) {
			$cieSet = $this->getCommissies();
			foreach($cieSet as $cie)
			{
				$mail = "Lieve " . CommissieView::waardeNaam($cie) . ",\r\n\r\nEr is een nieuw item aangemaakt dat vertaald dient te worden:\r\n\r\n" . HTTPS_ROOT . $this->url() . "\r\n\r\nGroetjes van de WebCie";
				sendmail("www", $cie->getEmail(), "Nieuw item om te vertalen", $mail);
				Page::addMelding(_("Je hebt nog geen Engelse omschrijving gegeven voor het item! Vergeet niet om dit ook nog te doen!"));
			}
		}*/
	}

	/**
	 * @brief Stel de waarde van het veld promotiePapa in.
	 *
	 * Dit wil je eigenlijk alleen in ActiviteitHerhaling::promoveer gebruiken.
	 */
	public function setPromotiePapa(ActiviteitHerhaling $act) {
		$this->promotiePapa = $act;
	}

	/**
	 * @brief Geef of de computerreserveringen van de activiteit tegelijk kunnen.
	 *
	 * Zo wil je bijvoorbeeld niet je eigen reserveringen tellen,
	 * of de reserveringen van een activiteit die je gaat vervangen.
	 *
	 * Deze override checkt ook de waarde van promotiePapa.
	 *
	 * @param act De activiteit met gelijktijdige reserveringen.
	 * @returns Een bool of het compatibel is.
	 */
	public function reserveringenCompatibel(Activiteit $act) {
		return $act == $this || $act == $this->promotiePapa;
	}

	/**
	 *  Overschrijft de functie die het veld voor het aantal computers controleert.
	 */
	public function checkAantalComputers()
	{
		if ($ret = parent::checkAantalComputers())
			return $ret;

		$waarde = $this->getAantalComputers();
		if($waarde < 0)
			return _('Er moeten minstens 0 computers worden gereserveerd');
		//We queryen eerst alle reserveringen waarvan de tijdstippen overlappen met onze reservering.
		if (is_null($eind = $this->getMomentEind()))
			return _('Er is geen einddatum opgegeven.');
		if (is_null($begin = $this->getMomentBegin()))
			return _('Er is geen begindatum opgegeven.');
		if($waarde == 0)
			return false;

		$max = $this->getMaxAantalComputers($begin, $eind);

		if ($waarde > $max) {
			return sprintf(
				_('Er kunnen maximaal %d computers gereserveerd worden'),
				$max
			);
		}

		return false;
	}
}
