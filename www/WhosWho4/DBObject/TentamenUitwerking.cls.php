<?
/**
 *
 *  'AT'AUTH_GET:gast
 */
class TentamenUitwerking
	extends TentamenUitwerking_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($tentamen = null, $uploader = null, $wanneer = null)
	{
		parent::__construct($tentamen, $uploader, $wanneer); // TentamenUitwerking_Generated
	}

	public function getBestand()
	{
		return TENT_SRV . "uitwerkingen/" . $this->geefID() . ".pdf";
	}


	public function url($extra = null)
	{
		$tentamen = $this->getTentamen();
		$pre = $tentamen->url() . '/Uitwerkingen/' . $this->geefID();
		if($extra)
			return $pre . '/' . $extra;
		return $pre;
	}

	/*
	 *  Wijzigt het pdf bestand
	 *
	 * @param files De $_FILES-variabele waarin de bestanden zijn geüpload
	 * @return Een HtmlSpan met errors als het misgegaan is
	 */
	public function wijzigBestand($files, $msg)
	{
		global $filesystem;

		$msg = array();

		// De $_FILES-array kan zelf ook aangeven of er errors zijn
		// Error nr 4 is dat er geen bestand was geüpload
		if($files['file']['error'] && $files['file']['error'] != 4)
		{
			$msg[] = _('Er waren fouten met het uploaden');
			return $msg;
		}

		// Controleer even of deze tentamenuitwerking geldig is(anders kunnen we gem niet opslaan)
		if($this->valid())
		{
			// Check of onze geuploade uitwerking bestand goed is.
			if($file = $files['file']['name'] && $files['file']['error'] != 4)
			{
				// Check of we wel een pdf bestand aan het uploaden zijn(we willen alleen pdf's)
				$name = $files['file']['name'];
				$ext = pathinfo($name, PATHINFO_EXTENSION);
				if($ext == 'pdf')
				{
					// Sla de tentamenUitwerking op. Dit zorgt ervoor dat we een id hebben
					$this->opslaan();
					if(!DEBUG)
					{
						// Haal eerst het bestaande bestand weg als dat bestaat
						if($filesystem->has($this->getBestand()))
						{
							$filesystem->delete($this->getBestand());
						}
						$stream = fopen($files['file']['tmp_name'], 'r+');
						$filesystem->writeStream($this->getBestand(), $stream);
						fclose($stream);
					}
				}
				else
				{
					$msg[] = _('Dit bestand is niet van het type pdf');
				}
			}
			else
			{
				$msg[] = _('Geen (geldig) bestand geupload');
			}
		}
		else
		{
			$msg[] = _('Er waren fouten');
		}

		return $msg;
	}

	/*
	 *  Controleert of het tentamenuitwerking verwijderd mag worden
	 *
	 * @return Een bool met of het verwijderen mag
	 */
	public function magVerwijderen()
	{
		return hasAuth('tbc')
			|| $this->uploaderIsIngelogd();
	}

	/*
	 *  Verwijdert een tentamen uit de db
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $filesystem;

		if($filesystem->has($this->getBestand()))
		{
			if(!DEBUG)
			{
				$filesystem->delete($this->getBestand());
			}
			else
			{
				Page::addMelding($this->getBestand() . " zou nu verwijderd worden.\n");
			}
		}

		return parent::verwijderen($foreigncall);
	}

	public function uploaderIsIngelogd()
	{
		if($this->getUploader() == null)
			return false;
		return hasAuth('ingelogd') && $this->getUploader()->geefId() == Persoon::getIngelogd()->geefId();
	}

	public function isZichtbaar()
	{
		return hasAuth('tbc') || $this->gecontroleerd || $this->uploaderIsIngelogd();
	}
}
