<?

/***
 * $Id$
 */
class ActiviteitVraag
	extends ActiviteitVraag_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a)
	{
		parent::__construct($a);
	}

	public function magWijzigen()
	{
		$act = $this->getActiviteit();
		return $act->magWijzigen();
	}

	public function magVerwijderen()
	{
		return $this->magWijzigen();
	}

	public function opslaan($classname = 'ActiviteitVraag')
	{
		//Omdat het geen auto increment is in SQL moeten we het zelf doen als dit nodig is.
		if(!$this->inDB) {
			global $WSW4DB;

			$id = $WSW4DB->q("COLUMN SELECT MAX(vraagID) FROM ActiviteitVraag
							WHERE `activiteit_activiteitID` = %i", $this->getActiviteitActiviteitID());
			if($id)	$this->vraagID = $id[0] + 1;
			else $this->vraagID = 1;
		}
		parent::opslaan($classname);
	}

	public function setOpties($newOpties)
	{
		if(is_array($newOpties))
		{
			foreach($newOpties as $key => $optie)
			{
				if(empty($optie))
					unset($newOpties[$key]);
				else
					$newOpties[$key] = trim($optie);
			}

			if(count($newOpties) == 0)
				$newOpties = null;
			else
				$newOpties = json_encode($newOpties);
		}

		return parent::setOpties($newOpties);
	}

	public function getOpties()
	{
		$opties = parent::getOpties();

		if(is_null($opties))
			return array();
		else
			return json_decode($opties);
	}

	protected static function veldenActiviteitVraag($welke = NULL)
	{
		$velden = array();
		switch($welke)
		{
		case 'viewWijzig':
			$velden[] = 'Type';
			$velden[] = 'Vraag';
			break;
		default:
			$velden = parent::veldenActiviteitVraag($welke);
		}

		return $velden;
	}

	public function valid($welkeVelden = 'set')
	{
		$valid = parent::valid($welkeVelden);

		if($this->getType() == 'ENUM')
		{
			if(count($this->getOpties()) == 0)
			{
				$this->errors['Opties'] = _('Er moeten opties gegeven worden bij een meerkeuzevraag');
				$valid = false;
			}
		}

		return $valid;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
