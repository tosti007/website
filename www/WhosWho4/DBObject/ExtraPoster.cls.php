<?
class ExtraPoster
	extends ExtraPoster_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ExtraPoster_Generated

		$this->extraPosterID = NULL;
	}

	public function getURL()
	{
		$id = $this->getExtraPosterID();
		return '/Service/ExtraPoster/' . $id . '/';
	}

	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}

	public function posterPath($usedExtensions = NULL, $onlyExisting = TRUE, $withLive = FALSE)
	{
		global $FW_EXTENSIONS_FOTO, $filesystem;

		// we hebben geen voorkeur voor extensie: zoek alles
		if ($usedExtensions === NULL) $usedExtensions = $FW_EXTENSIONS_FOTO;

		$prefixes = array();
		if (DEBUG) $prefixes[] = 'debug-'; // kijk naar debug posters
		if (!DEBUG || $withLive) $prefixes[] = ''; // kijk naar live posters

		foreach ($prefixes as $prefix) {
			foreach ($usedExtensions as $ext) {
				$path = ACTPOSTERS_SRV . 'extra/' . $prefix . $this->getExtraPosterID() . '.' . $ext;
				if (!$onlyExisting || $filesystem->has($path)) {
					return $path; // Deze extensie wil werken :)
				}
			}
		}
		return NULL;
	}
}
