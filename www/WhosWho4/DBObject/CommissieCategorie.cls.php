<?

/***
 * $Id$
 */
class CommissieCategorie
	extends CommissieCategorie_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	public function commissies()
	{
		return CommissieVerzameling::metCategorie($this);
	}

	public function url()
	{
		return '/Vereniging/Commissies/Categorie/'.$this->geefID().'/';
	}

	public static function categorieArray()
	{
		global $WSW4DB;
		return $WSW4DB->q('KEYVALUETABLE SELECT `categorieID`, `naam`'
						. ' FROM `CommissieCategorie` ORDER BY `naam` ASC');
	}

	public function getCommissies($oud = false)
	{
		$query = CommissieQuery::table()
			->whereProp('Categorie', $this);
		if(!$oud)
		{
			$query->where(function($q) {
				$q->whereProp('datumEind', '>', 'SQL_NOW')
				  ->orWherePropNull('datumEind');
			});
		}
		return $query->verzamel();
	}

	public function magVerwijderen()
	{
		return $this->magWijzigen();
	}

	public function magWijzigen()
	{
		return hasAuth('bestuur');
	}

	public function checkVerwijderen()
	{
		if($this->getCommissies(true)->aantal() > 0) {
			return 'Er zijn (oude) commissies die deze categorie hebben';
		}

		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
