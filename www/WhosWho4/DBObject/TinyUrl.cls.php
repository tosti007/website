<?

/**
 * $Id$
 *
 *  Een tinyurl die vanaf a-es2.nl/t/*url* naar een andere url doorverwijst.
 */
class TinyUrl
	extends TinyUrl_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TinyUrl_Generated

		$this->id = NULL;
	}

	/**
	 *   Geeft een ID van een TinyUrl terug als die bestaat, anders
	 *   krijg je null terug.
	 *
	 *  @param url de url die je wilt verwerken. 
	 **/
	public static function tinyUrlByUrl($url)
	{
		global $WSW4DB;

		$id = $WSW4DB->q("MAYBEVALUE SELECT `id` FROM `TinyUrl`
						WHERE `TinyUrl` = %s", $url);

		return $id;
	}

	/**
	 *  Geef alleen het aantal hits terug, doe niets met objecten.
	 **/
	public function amountOfHits()
	{
		global $WSW4DB;
		$result = $WSW4DB->q("VALUE SELECT COUNT(`id`) FROM `TinyUrlHit`
							WHERE `tinyUrl_id` = %i", $this->geefID());
		return $result;
	}

	/**
	 *  Geef een array met het aantal hits op een bepaald moment.
	 *  Ook hier geen objecten omdat we aggregeren.
	 **/
	public function arrayOfHits($periode = "eersteweek")
	{
		global $WSW4DB;
		global $logger;

		$data = $WSW4DB->q("TABLE SELECT COUNT(`id`) as aantal, `tinyUrl_id`, DATE(`hitTime`) as date 
								FROM `TinyUrlHit` 
								WHERE `tinyUrl_id` = %i 
								GROUP BY DATE(`hitTime`)"
								,$this->geefID());

		$uurdata = $WSW4DB->q("TABLE SELECT COUNT(`id`) as aantal, `tinyUrl_id`, DATE(`hitTime`) as date,
								HOUR(`hitTime`) as uur
								FROM `TinyUrlHit` 
								WHERE `tinyUrl_id` = %i 
								GROUP BY DATE(`hitTime`), HOUR(`hitTime`)"
								,$this->geefID());

		switch($periode){
			case 'eersteweek':

				//Vul de resultaat array
				$res = array();
				foreach($data as $row)
				{
					$res[$row["date"]] = $row["aantal"];
				}

				//Maakt een array van een week vanaf de eerste datum
				$beginDate = new DateTimeLocale(key($res));

				$newres = array();
				$newres[$beginDate->format("Y-m-d")] = $res[$beginDate->format("Y-m-d")];
				$date = clone $beginDate;

				for($i = 0; $i < 14; $i++)
				{
					$newdate  = $date->modify("+1 day");
					$newres[$date->format("Y-m-d")] = array_key_exists($date->format("Y-m-d"), $res)?
														$res[$date->format("Y-m-d")]:"0";
				}
				return $newres;
				break;
			case 'eerstedag':
				//Maakt een array van de eerste 24 uur, per uur.

				//Vul de resultaat array, met EXIF tijdsformat
				$res = array();
				foreach($uurdata as $row)
				{
					$res[$row["date"]. " " .$row["uur"] . ":00:00"] = $row["aantal"];
				}

				$logger->debug('tinyurl hits: ' . print_r($res, true));

				//Pak het eerste moment
				$beginDateTime = new DateTimeLocale(key($res));

				$newres[$beginDateTime->format("Y-m-d H:i:s")] = $res[$beginDateTime->format("Y-m-d H:i:s")];
				$dateTime = clone $beginDateTime;

				for($i = 0; $i < 35; $i++)
				{
					$newdateTime  = $dateTime->modify("+1 hour");
					$newres[$dateTime->format("d - H:i:s")] = array_key_exists($dateTime->format("Y-m-d H:i:s"), $res)?
														$res[$dateTime->format("Y-m-d H:i:s")]:"0";
				}
				return $newres;
				break;
			default:

		}
			
		return $res;
	}

	public function url()
	{
		return "/Service/Intern/TinyUrl/" . $this->geefID();
	}

	/**
	 *   Maakt een random url string die nog niet bestaat. Er zijn met
	 *   een string lengte in totaal 380204032 opties, dus voorlopig wel even
	 *   genoeg om met random generation te doen ipv hashing.
	 **/
	public static function generateTinyUrl($length = 5)
	{
		$url = randstr(5);

		//Zolang we entries tegenkomen die bestaat maken we nieuwe.
		while(TinyUrl::TinyUrlByUrl($url)) {
			$url = randstr(5);
		}

		return $url;
	}

	/**
	 *   Breid de normale check in de generated class uit met een check op
	 *   dubbele TinyUrls. Door dat hier te doen hoeven we geen eigen processForm() te maken.  
	 **/
	public function checkTinyUrl()
	{
		//Kijk of de parent al een foutmelding terug geeft
		$parent = parent::checkTinyUrl();
		if($parent)
			return $parent;

		//Bestaat deze TinyUrl al?
		if(TinyUrl::TinyUrlByUrl($this->getTinyUrl()) && TinyUrl::TinyUrlByUrl($this->getTinyUrl()) != $this->geefID())
			return _("Deze TinyUrl bestaat helaas al");

		return false;
	}

	/**
	 *  Om hits netjes samen met de rest van het object te laten zien stoppen we
	 *   ze er gewoon bij! In de view staan de label en waarde.
	 **/
	protected static function veldenTinyUrl($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'viewWijzig':
		case 'get':
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
			$velden[] = 'Mailings';
			return $velden;
		case 'viewInfo':
			$velden[] = 'TinyUrl';
			$velden[] = 'OutgoingUrl';
			$velden[] = 'Hits';
			$velden[] = 'Mailings';
			return $velden;
		default:
			return parent::veldenTinyUrl($welke);
		}
	}

	/**
	 *  We haken de lijst met gekoppelede mailings netjes in het object door ook setters
	 *   en getters te maken.
	 **/

	//LET OP: deze lijst is voor het gemakt een string, zo is hij consistent met de setter.
	public function getMailings()
	{
		$mailings = TinyMailingUrlVerzameling::vanUrl($this)->toMailingVerzameling();	
		return implode($mailings->keys(), ", ");
	}

	//En deze wil dus een string met mailing id's er in.
	public function setMailings($newMailings)
	{
		$newArray = explode(",", $newMailings);

		$mailings = TinyMailingUrlVerzameling::vanUrl($this)->toMailingVerzameling();	
		$oldArray = $mailings->keys();

		//Haal eerst TinyMailingUrls weg die niet meer nodig zijn.
		$removeArray = array_diff($oldArray, $newArray);

		foreach($removeArray as $mailing)
		{
			$tmu = TinyMailingUrl::geef(Mailing::geef($mailing), $this);
			$returnVal = $tmu->verwijderen();
			if(!is_null($returnVal)) {
				user_error($returnVal, E_USER_ERROR);
			}
		}

		//En voeg vervolgens nieuwe toe.
		foreach($newArray as $mailing)
		{
			//Niet bestaande mailings worden niet gekoppeld.
			if(Mailing::geef($mailing)){
				if(TinyMailingUrl::geef(Mailing::geef($mailing), $this)){
					//Bestaat al, doe niets
				} else {
					$tmu = new TinyMailingUrl(Mailing::geef($mailing), $this);
					$tmu->opslaan();
				}
			}
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
