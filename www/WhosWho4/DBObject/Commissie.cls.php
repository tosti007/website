<?
class Commissie
	extends Commissie_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	public function __toString ()
	{
		return $this->getNaam();
	}

	/**
	 * @return geeft de commissie terug die het bestuur voorstelt
	 */
	public static function bestuur()
	{
		return Commissie::cieByLogin(BESTUURLOGINNAAM);
	}

	/**
	 * @brief Geeft null of de commissie kandidaatbestuur
	 * Als er nog geen kandidaatsbestuur is gevormd, zal deze functie null
	 * teruggeven.
	 * @return de commissie die het kandidaatbestuur voorstelt
	 */
	public static function kandidaatBestuur()
	{
		if (!defined('KANDIDAATBESTUURLOGINNAAM')) {
			// Er is nog geen kandidaatsbestuur bekend
			return null;
		}

		return Commissie::cieByLogin(KANDIDAATBESTUURLOGINNAAM);
	}

	/** METHODEN **/
	public function checkDatumEind()
	{
		if ($ret = parent::checkDatumEind())
			return $ret;

		if ($this->getDatumEind()->hasTime())
		{
			if ($this->getDatumEind() <= $this->getDatumBegin())
			{
				return _('Einddatum moet later zijn dan begindatum');
			}
			else if (($aantal = $this->getBugCategorieen()->aantal()) > 0)
			{
				return sprintf(_('Commissie heeft nog %d actieve bug-categorie&#235;n, deze moeten eerst worden gedeactiveerd'), $aantal);
			}
		}

		return False;
	}
	public function checkDatumBegin()
	{
		if(!$this->getDatumBegin()->hasTime())
		{
			return _("Commissie moet een begindatum hebben");
		}

		return False;
	}
	public function checkEmail()
	{
		$ret = parent::checkEmail();
		if($ret)
			return $ret;

		$mail = $this->getEmail();
		if(empty($mail))
			return _("Email mag niet leeg zijn.");

		return False;
	}
	public function checkLogin()
	{
		$ret = parent::checkLogin();
		if($ret)
			return $ret;

		$cie = Commissie::cieByLogin($this->getLogin());
		if($cie && $cie != $this)
			return _("Login is al gekoppeld aan een andere commissie.");

		//Haal deze check er uit aangezien de accountpolicy van sysop niet meer overeenkomt met hoe commissies
		// op de website staan. Ook is de check verder niet meer van belang, anders dan dat het wordt gebruikt
		// om de url van een commissie te maken.
		/*
		$posix = posix_getpwnam($this->getLogin());
		if(empty($posix) && !hasAuth('god')) {
			return sprintf(_("Login %s bestaat niet op het systeem."), $this->getLogin());
		}
		*/

		return False;
	}

	static public function cieByLogin($login)
	{
		global $WSW4DB;

		$id = $WSW4DB->q('MAYBEVALUE SELECT `CommissieID`'
						.'FROM `Commissie`'
						.'WHERE `login` = %s',$login);

		if(!$id)
			return null;
		return self::geef($id);
	}

	public function magWijzigen ()
	{
		return Commissie::magKlasseWijzigen() || $this->hasLid(Persoon::getIngelogd());
	}
	public function magVerwijderen()
	{
		return hasAuth('bestuur');
	}

	/**
	 *  Bepaal of deze Commissie actief is.
	 *
	 * @return True indien we tussen de begin- en eindatum van de Commissie 
	 * zitten, False anders.
	 */
	public function isActief ()
	{
		$nu = new DateTimeLocale();

		return $nu < $this->getDatumEind() && $nu > $this->getDatumBegin();
	}

	/***
	 *   geeft activiteiten van een cie terug.
	 *  \input Status mag huidig, oud of komend zijn
	 **/
	public function activiteiten($status = 'komend', $toegang = 'PUBLIEK')
	{
		return ActiviteitVerzameling::vanCommissie($this, $status, $toegang);
	}

	/***
	 *  /brief geeft activiteiten van een cie terug op loginlevel, dit om
	 *  codeduplicatie tegen te gaan.
	 **/

	public function activiteitenByLogin($status = 'komend')
	{
		if (hasAuth('bestuur') || $this->hasLid(Persoon::getIngelogd()))
			$toegang = 'alles';
		else
			$toegang = 'PUBLIEK';

		return self::activiteiten($status, $toegang);
	}

	public function leden ($status = 'huidige')
	{
		return PersoonVerzameling::vanCommissie($this, $status);
	}

	public function cieleden ($status = 'huidige')
	{
		return CommissieLidVerzameling::vanCommissie($this, $status);
	}

	public function hasLid (Persoon $lid = null)
	{
		if (!$lid instanceof Persoon)
			return false;

		$leden = $this->leden();
		return $leden->bevat($lid);
	}

	/**
	 * Zoek iedere BugCategorie die bij deze Commissie hoort.
	 *
	 * @param ookInactieve Indien True wordt ook iedere inactieve BugCategorie 
	 * opgezocht.
	 * @return Een BugCategorieVerzameling met elke BugCategorie die van deze 
	 * Commissie.
	 */
	public function getBugCategorieen ($ookInactieve = False)
	{
		return BugCategorieVerzameling::geefVanCommissie($this, $ookInactieve);
	}

	/**
	 * Bepaal het aantal inactieve BugCategorieen die bij deze Commissie horen.
	 *
	 * @return Het aantal inactieve categorieen.
	 */
	public function getAantalInactieveBugCategorieen ()
	{
		return BugCategorieVerzameling::geefAantalInactieveVanCommissie($this);
	}

	/**
	 * @brief Geef de URL van de hoofdpagina van de commissie.
	 *
	 * Indien deze niet geset is, geeft het de basis-URL,
	 * waarvandaan de variabele entry doorstuurt naar Publisher of de infopagina.
	 *
	 * Let op: dit doet wat anders dan DBObject::url ivm historische redenen...
	 */
	public function url()
	{
		if($this->getHomepage()) {
			return $this->getHomepage();
		}
		//Genereer hele url ipv a-es2 hardcoded
		return $this->systeemUrl();
	}

	/**
	 * @brief Geef de URL voor de verwijderpagina van deze commissie.
	 *
	 * @return Een string die de URL representeert.
	 */
	public function verwijderURL() {
		return $this->getURLBase('/Verwijder');
	}

	/**
	 * @brief Geef de URL voor de wijzigpagina van deze commissie.
	 *
	 * @return Een string die de URL representeert.
	 */
	public function wijzigURL() {
		return $this->getURLBase('/Wijzig');
	}

	/**
	 * @brief Geef de URL voor de foto's van deze commissie.
	 *
	 * @return Een string die de URL representeert.
	 */
	public function fotoUrl() {
		return $this->systeemUrl() . '/Fotos';
	}

	public function systeemUrl()
	{
		return '/Vereniging/Commissies/'.$this->getLogin();
	}

	public static function inputToCie($input)
	{
		if(is_numeric($input)) {
			return Commissie::geef($input);
		} else {
			return Commissie::cieByLogin($input);
		}
	}

	/**
	 *	 Retourneert de URL base voor deze Commissie, bijvoorbeeld:
	 *	/Vereniging/Commissies/sysop/Info
	 *
	 *	@param extrapath is bij default /Info, maar kan bijv. ook op /Wijzig
	 *	gezet worden
	 */
	public function getURLBase($extrapath = '/Info')
	{
		$ciehome = "/Vereniging/Commissies/";

		$res = $ciehome . rawurlencode($this->getLogin());
		if ($extrapath) $res .= $extrapath;

		return $res;
	}

	public static function cieSoorten()
	{
		trigger_error("Gebruik de standaardmethoden van de view", E_USER_ERROR);
		return array("CIE" => _("Commissie"),
					"GROEP" => _("Groep"),
					"DISPUUT" => _("Dispuut"));
	}
	public static function cieSoortenMeervoud()
	{
		trigger_error("Gebruik de standaardmethoden van de view", E_USER_ERROR);
		return array("CIE" => _("Commissies"),
					"GROEP" => _("Groepen"),
					"DISPUUT" => _("Disputen"));
	}
	
	public function checkVerwijderen()
	{
		if($error = parent::checkVerwijderen())
			return $error;

		//verwijderen mag alleen als er geen activiteiten zijn gekoppeld
		if($this->activiteiten('huidig')->aantal() 
			|| $this->activiteiten('oud')->aantal()
			|| $this->activiteiten('komend')->aantal()
		)
			return _("Deze commissie heeft activiteiten georganiseerd");
		
		//en er geen leden in hebben gezeten/zitten
		if($this->cieleden('huidige')->aantal() 
			|| $this->cieleden('oud')->aantal()
		)
			return _("Deze commissie heeft/had commissieleden");
		
		return false;
	}
	
	protected static function veldenCommissie($welke = NULL)
	{
		$velden = parent::veldenCommissie($welke);
		switch($welke) {
		case 'viewWijzig':
			if($search = array_search("CommissieID", $velden))
				unset($velden[$search]); //CommissieID willen we niet zien.
			if(!hasAuth('bestuur') && $search = array_search("Login", $velden)) {
				unset($velden[$search]);
			}
			break;
		case 'verplicht':
			$velden[] = 'Email';
		}
		return $velden;
	}

	public function maakBugGrafiek()
	{
		global $WSW4DB, $bugStatusSets;

		$cieNaam = CommissieView::waardeNaam($this);

		$allbugs = BugBerichtQuery::table()
			->setFetchType('TABLE')
			->select('Bug.bugID', 'BugBericht.moment', 'Bug.gewijzigdWanneer', 'BugBericht.status')
			->join('Bug')
			->join('BugCategorie', 'BugCategorie.bugCategorieID', 'Bug.categorie_bugCategorieID')
			->whereProp('Commissie', $this)
			->orderByAsc('BugBericht.moment')
			->get();

		$mindate = strtotime($allbugs[0]['moment']);
		$maxdate = 0; $counter = 1;

		while(!$maxdate)
		{
			$maxdate = strtotime($allbugs[count($allbugs)-$counter++]['moment']);
		}

		$dates = range($mindate, $maxdate, ($maxdate-$mindate)/49);

		$bugsOpenOnDate = Array();
		foreach($dates as $date)
			$bugsOpenOnDate[$date] = 0;

		$bugsGemeldOnDate = Array();
		foreach($dates as $date)
			$bugsGemeldOnDate[$date] = 0;

		$bugsGeslotenOnDate = Array();
		foreach($dates as $date)
			$bugsGeslotenOnDate[$date] = 0;

		foreach($allbugs as $key => $bug)
		{
			$status = $bug['status'];
			if(in_array($status, array_merge($bugStatusSets['GESLOTENS'], $bugStatusSets['OPGELOSTS'])))
			{
				$allbugs[$key]['status'] = 'GESLOTEN';
			}
			else if(in_array($status, $bugStatusSets['OPENS']))
			{
				$allbugs[$key]['status'] = 'OPEN';
			}
		}

		$bugs_array = array();

		foreach($allbugs as $bug)
		{
			$status = $bug['status'];
			$modified = strtotime($bug['moment']);

			if(!isset($bugs_array[$bug['bugID']]))
			{
				$open_add = (int)($status == 'OPEN');
				$closed_add = (int)($status == 'GESLOTEN');
				$add_add = $open_add;
			}
			else
			{
				$old_status = $bugs_array[$bug['bugID']];

				if($old_status == $status)
					continue;

				$open_add = -1 * (int)($old_status == 'OPEN');
				$closed_add = -1 * (int)($old_status == 'GESLOTEN');
				$open_add += (int)($status == 'OPEN');
				$closed_add += (int)($status == 'GESLOTEN');
				$add_add = 0;
			}

			$bugs_array[$bug['bugID']] = $status;

			foreach($dates as $date)
			{
				if($date < $modified)
					continue;

				$bugsGemeldOnDate[$date] += $add_add;
				$bugsGeslotenOnDate[$date] += $closed_add;

				if($bugsGeslotenOnDate[$date] == -1)
				{
					$bugsGeslotenOnDate[$date] = 0;
					$bugsGemeldOnDate[$date]++;
				}

				if($date >= $modified)
					break;
			}

			foreach($dates as $date)
			{
				if($date < $modified)
					continue;

				$bugsOpenOnDate[$date] += $open_add;
			}
		}

		//		$labels = array_map("dateMooi", array_keys($bugsOpenOnDate));
		$labels = array();
		foreach(array_keys($bugsOpenOnDate) as $key) {
			$key = new DateTimeLocale($key);
			$labels[] = $key->format("Y-m-d");
		}

		$dataset = array();
		$dataset['Open bugs'] = array_values($bugsOpenOnDate);
		$dataset['Bugs gemeld'] = array_values($bugsGemeldOnDate);
		$dataset['Bugs gesloten'] = array_values($bugsGeslotenOnDate);

		return maakGrafiek($labels, $dataset);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
