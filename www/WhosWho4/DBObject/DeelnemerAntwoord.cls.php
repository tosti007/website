<?

/***
 * $Id$
 */
class DeelnemerAntwoord
	extends DeelnemerAntwoord_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}

	public function magVerwijderen()
	{
		if($this->getDeelnemer()->getPersoon()->magWijzigen() || $this->getDeelnemer()->getActiviteit()->magWijzigen())
			return true;

		return false;
	}

	public function valid ($welkeVelden = 'set')
	{
		$bool = parent::valid($welkeVelden);

		if($this->getVraag()->getType() == 'ENUM'
			&& !$this->getAntwoord())
		{
			$this->errors['Antwoord'] = _('Selecteer een optie');
			$bool = false;
		}
		else
		{
			if($this->errors['Antwoord'])
				$bool = true;
			$this->errors['Antwoord'] = false;
		}

		return $bool;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
