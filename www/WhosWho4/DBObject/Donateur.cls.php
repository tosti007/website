<?

/***
 * $Id$
 */
class Donateur
	extends Donateur_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a)
	{
		parent::__construct($a);

		$this->jaarBegin = colJaar();
		$this->donatie = 15;
	}

	/**
	 * @brief Retourneert de URL waarop deze donateur te vinden is
	 *
	 * @returns Een URL (string) waarop informatie over deze donateur te vinden is
	 */
	public function url()
	{
		return '/Leden/Donateurs/' . $this->geefID();
	}

	/**
	 * Returneert of  het ingelogd lid het recht heeft om deze donateur te wijzigen.
	 */
	public function magWijzigen ()
	{
		return hasAuth("bestuur");
	}
	public function magVerwijderen ()
	{
		return hasAuth("bestuur");
	}

	public function totaalbedrag()
	{
		global $DONAKOSTEN;
		$totaal = $DONAKOSTEN["jaarverslag"] * $this->getJaarverslag() 
				+ $DONAKOSTEN["studiereis"] * $this->getStudiereis() 
				+ $DONAKOSTEN["almanak"] * $this->getAlmanak()
				+ $this->getDonatie();
		return $totaal;
	}
	
	public function setAnderPersoon($new)
	{
		$pre = $this->anderPersoon_contactID;

		parent::setAnderPersoon($new);

		if($this->inDB)
		{
			if(isset($pre) && $pre != $this->anderPersoon_contactID)
				Entiteit::stopInCache($pre, False, 'Donateur');
			if($this->anderPersoon_contactID)
				Entiteit::stopInCache($this->anderPersoon_contactID
						, $this, 'Donateur');
		}
	}
	public function setMachtigingOud($newMachtigingOud)
	{
		// mag niet meer gewijzigd worden
		return $this;
	}
	public function checkDonatie()
	{
		$check = parent::checkDonatie();
		if($check)
			return $check;

		if($this->getDonatie() < 0)
			return _('het bedrag mag niet negatief zijn');

		return false;
	}
	public function checkJaarEind()
	{
		$check = parent::checkJaarEind();
		if($check)
			return $check;

		if($this->getJaarEind() && $this->getJaarEind() < $this->getJaarBegin())
			return _('Eind jaar moet na begin jaar liggen');

		return false;
	}

	public static function cache($ids, $recur = False)
	{
		global $WSW4DB;

		$ids = Entiteit::nietInCache($ids, 'Donateur');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$ids2 = $WSW4DB->q('COLUMN SELECT `Donateur`.`persoon_contactID`'
						.	' FROM `Donateur`'
						.	' WHERE `persoon_contactID` IN (%A{i})'
						.	'    OR `anderPersoon_ContactID` in (%A{i})'
						, $ids, $ids
						);

		parent::cache($ids2, $recur);

		foreach($ids2 as $id)
		{
			$dona = Entiteit::geefCache($id, 'Donateur');
			if(!$dona)
				continue;

			$ander = $dona->getAnderPersoonContactID();
			if($ander != NULL) {
				Entiteit::stopInCache($ander, $dona, 'Donateur');
			}
		}
	}
	public function opslaan($classname = 'Donateur')
	{
		parent::opslaan($classname);

		// ook gelijk in de cache onder getAnderContactID
		if($this->anderPersoon_contactID)
			self::stopInCache( $this->anderPersoon_contactID
			                 , $this
			                 , 'Donateur'
			                 );
	}

	public static function statistieken()
	{
		global $WSW4DB;

		return $WSW4DB->q('TUPLE SELECT'
						. '  COUNT(`persoon_contactID`)       as huidig'
						. ', COUNT(`anderPersoon_ContactID`)  as samen'
						. ', ROUND(AVG(`donatie`), 2)         as gem'
						. ', SUM(`donatie`)                   as tot'
						. ', MAX(`donatie`)                   as max'
						. ', MIN(`donatie`)                   as min'
						.' FROM `Donateur`'
						.' WHERE (`jaarEind` IS NULL OR `jaarEind` > %i)'
						, colJaar()
						);
	}

	protected static function veldenDonateur($welke = NULL)
	{
		switch($welke) {
		case 'viewInfo':
			$output = parent::veldenDonateur($welke);
			array_unshift($output, 'Persoon');
			return $output;
		default:
			return parent::veldenDonateur($welke);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
