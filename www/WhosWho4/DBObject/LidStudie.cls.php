<?

/***
 * $Id$
 */
class LidStudie
	extends LidStudie_Generated
{
	/** CONSTRUCTOR **/
	public function __construct ($lid = null, $perSept = false)
	{
		parent::__construct($lid);

		if($perSept == true)
		{
			$this->setDatumBegin(Date('Y')."-09-01");
		}
	}

	public function magVerwijderen()
	{
		global $auth;
		// Intro-leden mogen ook lidstudies aanpassen,
		// voor het geval ze een deelnemer verwijderen
        // (en mollie ook).
		return hasAuth('intro') || $auth->getLevel() == 'mollie';
	}

	public function magWijzigen()
	{
		// Intro-leden mogen ook lidstudies aanpassen,
		// voor het geval ze een deelnemer verwijderen
		return hasAuth('intro');
	}

	public static function alleStudiesArray()
	{
		return array("IK" => _("Informatiekunde"),
					 "IC" => _("Informatica"),
					 "NA" => _("Natuurkunde"),
					 "WI" => _("Wiskunde"),
					 "IC IK" => _("INFO Informatica & Informatiekunde"),
					 "NA WI" => _("TWIN Natuurkunde & Wiskunde"),
					 "IC WI" => _("TWINFO Informatica & Wiskunde"),
					 "IC WI NA" => _("INTWIN Informatica, Wiskunde en Natuurkunde"));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
