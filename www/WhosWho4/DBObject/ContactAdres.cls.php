<?

/***
 * $Id$
 */
class ContactAdres
	extends ContactAdres_Generated
{
	/** CONSTRUCTOR **/
	public function __construct ($contact = null)
	{
		parent::__construct($contact);

		//Voor een bedrijf willen we een ander default soort adres.
		if($contact instanceof Organisatie)
		{
			$this->setSoort('BEZOEK');
		}
	}

	public function magVerwijderen()
	{
		global $auth;

		if($this->getContact()->getContactID() == $auth->getLidnr())
			return true;
		if(hasAuth('bestuur'))
			return true;
		// Een spook mag een contactadres van een bedrijf weggooien
		if(hasAuth('spocie') && ContactPersoon($this->getContact(), "bedrijfscontact"))
			return true;
        if($auth->getLevel() == 'mollie')
            return true;
		return false;
	}

	static public function geefBySoort(Contact $contact, $soort)
	{
		global $WSW4DB;
		$id = $WSW4DB->q('MAYBEVAlUE SELECT `adresID`'
						. ' FROM `ContactAdres`'
						. ' WHERE `contact_contactID` = %i'
						. ' AND `soort` = %s'
						. ' LIMIT 1',
						$contact->geefID(), $soort);

		if($id)	return self::geef($id);
	}

	static public function eersteBijContact (Contact $con)
	{
		global $WSW4DB;
		$id = $WSW4DB->q('MAYBEVAlUE SELECT `adresID`'
						. ' FROM `ContactAdres`'
						. ' WHERE `contact_contactID` = %i'
						. ' ORDER BY `soort`'
						. ' LIMIT 1',
						$con->geefID());

		if($id)	return self::geef($id);
	}

	static public function eersteBijContactAdresGesorteerd (Contact $con)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `adresID`'
						. ' FROM `ContactAdres`'
						. ' WHERE `contact_contactID` = %i'
						. ' LIMIT 1',
						$con->geefID());
		$verz = ContactAdresVerzameling::verzamel($ids);
		$verz->sorteer('soort');

		if($verz->aantal() != 0) {
			return $verz->first();
		}
	}

	/**
	 *  KiX = streepjescode van TNT Post voor onder/boven adressering.
	 */
	public function kixcode()
	{
		// kixcode berekenen
		$kix = '';
		$pc = $this->getPostcode();
		$hn = $this->getHuisnummer();
		if(!empty($pc) && !empty($hn)) {
			$pc = trim($pc); $hn = trim($hn);
			$pc = preg_replace('/\s/','', $pc);

			// alleen als de postcode een echte postcode is
			if(preg_match('/^\d\d\d\d\w\w$/', $pc)) {
				$kix .= $pc;
				// splits het huisnummer op in huisnummer plus evt toevoeging
				if(preg_match('/(\d+)(\D*.*)/', $hn, $hnparts)) {
					$kix .= $hnparts[1];
					// als er een toevoeging is, voeg die toe met 'X' als separator
					if(!empty($hnparts[2])) {
						$ext = preg_replace('/[^A-Za-z0-9]/','', $hnparts[2]);
						$kix .= 'X'. substr($ext, 0, 5);
					}
				}
			}
		}

		return strtoupper($kix);
	}

	public function getPostcodeCijfers()
	{
		$postcode = $this->getPostcode();

		$cijfers = '';
		for($i = 0; $i < strlen($postcode); $i++)
			{
				$char = $postcode[$i];
				if(is_numeric($char)) $cijfers .= $char;
			}
		return $cijfers;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
