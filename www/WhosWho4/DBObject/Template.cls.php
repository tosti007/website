<?php

/**
 * @brief Een klasse die Twig-templates laadt uit Template-objecten.
 */
class Twig_LoaderDBObject implements
		Twig_LoaderInterface,
		Twig_SourceContextLoaderInterface,
		Twig_ExistsLoaderInterface {

	/**
	 * @brief Zoek het object die hoort bij de gegeven templatenaam.
	 *
	 * @param name De naam van deze template.
	 * @returns Een Template als die bestaat.
	 * @throws Twig_Error_Loader als die niet bestaat.
	 */
	private function getObject($name) {
		$template = $this->getObjectOrNull($name);
		if (!$template) {
			throw new Twig_Error_Loader("Gevraagde template staat niet in de DB.");
		}
		return $template;
	}
	/**
	 * @brief Zoek de benamite-entry die behoort tot de gegeven templatenaam.
	 * Geeft NULL als het de entry niet goed is,
	 * maar dat wil je in de loader in principe liever niet.
	 *
	 * @see getObject
	 *
	 * @param name De naam van deze template.
	 * @returns Een Template-object als die bestaat, anders NULL.
	 */
	private function getObjectOrNull($name) {
		return Template::geefVanLabel($name);
	}

	/**
	 * Returns the source for a given template logical name.
	 *
	 * @param string $name The template logical name
	 *
	 * @return string The template source code
	 *
	 * @throws Twig_Error_Loader When $name is not found
	 */
	public function getSource($name) {
		$template = $this->getObject($name);
		$content = $template->getWaarde(getLang());
		return $content;
	}

	/**
	 * Returns the source context for a given template logical name.
	 *
	 * @param string $name The template logical name
	 *
	 * @return Twig_Source
	 *
	 * @throws Twig_Error_Loader When $name is not found
	 */
	public function getSourceContext($name) {
		$content = $this->getSource($name);
		return new Twig_Source($content, $name, $name);
	}

	/**
	 * Gets the cache key to use for the cache for a given template name.
	 *
	 * @param string $name The name of the template to load
	 *
	 * @return string The cache key
	 *
	 * @throws Twig_Error_Loader When $name is not found
	 */
	public function getCacheKey($name) {
		$template = $this->getObject($name);
		return $template->getId();
	}

	/**
	 * Returns true if the template is still fresh.
	 *
	 * @param string    $name The template name
	 * @param timestamp $time The last modification time of the cached template
	 *
	 * @return bool    true if the template is fresh, false otherwise
	 *
	 * @throws Twig_Error_Loader When $name is not found
	 */
	public function isFresh($name, $time) {
		$template = $this->getObject($name);
		$lastModified = $template->getGewijzigdWanneer();

		// Als we niet weten wanneer dit ding gewijzigd is,
		// gaan we ervan uit dat de cache ongeldig is.
		if (!$lastModified->hasTime())
		{
			$lastModified = $template->gewijzigd();
			$template->opslaan();
			return false;
		}
		return $lastModified->getTimestamp() <= $time;
	}

	/**
	 * Check if we have the source code of a template, given its name.
	 *
	 * @param string $name The name of the template to check if we can load
	 *
	 * @return bool    If the template source code is handled by this loader or not
	 */
	public function exists($name) {
		return !is_null($this->getObjectOrNull($name));
	}

	/**
	 * @brief Geef een HtmlAnchor om de template te wijzigen.
	 *
	 * @param template De naam van een template op de VFS.
	 * @param text De tekst waar je op kan klikken.
	 */
	public function wijzigLink($template, $text="Wijzig") {
		$template = $this->getObject($template);
		return new HtmlAnchor($template->publisherUrl(), $text);
	}
}
/**
 * @brief Templates die door de gebruikers kunnen worden aangepast.
 *
 * Dit lijkt een beetje op het Register:
 * de namen van templates zijn hardcoded, maar de inhoud staat in de db.
 */
class Template
	extends Template_Generated
{
	/** Verzamelingen **/
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Template_Generated

		$this->id = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}

	/**
	 * @brief De Twig_LoaderInterface die templates uit de databaas sjort.
	 * Wordt geïnstantieerd zodra die nodig is, in maakLoader.
	 *
	 * @see maakLoader
	 */
	private static $loader;

	/**
	 * @brief De Twig_Environment die templates invult.
	 * Wordt geïnstantieerd zodra die nodig is, in render.
	 *
	 * @see render
	 */
	private static $twig;

	/**
	 * @brief Als er nog geen loader bestaat, initialiseer een nieuwe.
	 * Dit is handig als je van plan bent vaak een loader te gebruiken.
	 */
	private static function maakLoader() {
		if (!self::$loader) {
			self::$loader = new Twig_LoaderDBObject();
		}
	}

	/**
	 * @brief Geef de Template met de gegeven label.
	 *
	 * @param label Een string, die gelijk moet zijn aan het Label-veld van een Template.
	 *
	 * @return Een Template-object, of null.
	 */
	public static function geefVanLabel($label)
	{
		return TemplateQuery::table()->whereProp('label', $label)->geef();
	}

	/**
	 * @brief Geef de letterlijke tekst van de template voordat er variabelen worden ingevuld.
	 *
	 * @param template De naam van een template op de VFS.
	 *
	 * @return Een string met de ongerenderde templatetekst.
	 */
	public static function geefOngerenderd($template) {
		self::maakLoader();

		return self::$loader->getSource($template);
	}

	/**
	 * @brief Render de template met gegeven naam en invulling van variabelen.
	 *
	 * Overigens is "vertolken" een mooie vertaling van "render" naar het Nederlands.
	 *
	 * @param template De naam van een template op de VFS.
	 * @param variables Een array variabelenaam => invulling.
	 */
	public static function render($template, $variables) {
		// maak statische instances aan indien nuttig
		self::maakLoader();
		if (!self::$twig) {
			// Cache geen dingen want dingen in je www-map schrijfbaar voor php maken
			// is echt een heel erg slecht idee
			self::$twig = new Twig_Environment(self::$loader, array());
		}

		// vertolk!
		return self::$twig->render($template, $variables);
	}

	/**
	 * @brief Controleer of de waarde van het veld label geldig is.
	 *
	 * @return False als de huidige waarde van het veld label geldig is;
	 * anders een string met een foutmelding.
	 */
	public function checkLabel()
	{
		if (!is_null(static::geefVanLabel($this->getLabel())))
		{
			return _('labels moeten uniek zijn');
		}

		return parent::checkLabel();
	}

	/**
	 * @brief Mag de huidige gebruiker dit object wijzigen?
	 *
	 * @return Een bool.
	 */
	public function magWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Mag de huidige gebruiker dit object verwijderen?
	 *
	 * @return Een bool.
	 */
	public function magVerwijderen()
	{
		return hasAuth('god');
	}

	/**
	 * @brief Geef de URL van de info-pagina van deze Template.
	 *
	 * @return Een string met de URL.
	 */
	public function url()
	{
		return TEMPLATE_BASE . $this->getID();
	}

	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * Override zodat bij wijzigen niet 'label' voorbijkomt.
	 *
	 * @param welke String die een subset van velden beschrijft, of NULL voor
	 * alle velden.
	 * @return Een array met velden.
	 * @see velden
	 */
	protected static function veldenTemplate($welke = NULL)
	{
		$velden = [];
		switch ($welke)
		{
			case 'viewWijzig':
				$velden[] = 'Beschrijving';
				$velden[] = 'Waarde';
				break;
			default:
				$velden = parent::veldenTemplate($welke);
		}
		return $velden;
	}
}
