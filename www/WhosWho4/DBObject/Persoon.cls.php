<?

/***
 * $Id$
 */
class Persoon
	extends Persoon_Generated
{
	/** Cache voor het PersoonVoorkeur-object dat bij deze Persoon hoort. **/
	protected $voorkeur;

	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/

	/**
	 * @brief Geef de basis-URL van een Persoon.
	 *
	 * De bedoeling van deze methode is dat een functie als `wijzigURL()`
	 * gewoon kan doen `return $this->url() . '/Wijzig';`.
	 *
	 * @return De URL naar de (variabele entry) die bij dit object hoort.
	 * Voor een Persoon bijvoorbeeld `'/Leden/8566'`.
	 * Indien geen URL bekend is, gooit `HeeftGeenURLException`.
	 *
	 * @see urlAchievements
	 * @see fotoUrl
	 * @see fotograafUrl
	 * @see urlIOU
	 * @see verwijderURL
	 * @see wijzigURL
	 */
	public function url()
	{
		return '/Leden/' . $this->getContactID();
	}

	public function urlAchievements()
	{
		return $this->url() . '/Achievements';
	}

	public function fotoUrl()
	{
		return $this->url() . '/Fotos';
	}

	public function urlIOU()
	{
		return IOUBASE . $this->getContactID();
	}

	public function fotograafUrl()
	{
		return '/FotoWeb/Fotografen/' . $this->getContactID();
	}

	public function magBekijken()
	{
		return $this->magWijzigen();
	}

	/**
	 * Returneert of de ingelogde persoon het recht heeft om deze persoon te wijzigen.
	 */
	public function magWijzigen ()
	{
		global $auth;
		//Spoken mogen bedrijfscontacten aanpassen, kijk eerst of het wel om een CP gaat.
        //De mollie webhook mag bijnaleden verwijderen.
		return $this === Persoon::getIngelogd()
			|| (hasAuth('spocie') && ContactPersoon::vanPersoon($this, "bedrijfscontact"))
            || ($auth->getLevel() == 'mollie' && $this->isBijnaLid())
			|| parent::magWijzigen();
	}
	public function magVerwijderen ()
	{
		return $this->magWijzigen();
	}

	/**
	 * Zoek de voorkeur van dit Persoon.
	 *
	 * @return Het gecachte PersoonVoorkeur-object als dat bestaat, anders een 
	 * nieuw object met de voorkeuren van dit Persoon.
	 */
	public function getVoorkeur ()
	{
		if (!($this->voorkeur instanceof PersoonVoorkeur))
			$this->voorkeur = PersoonVoorkeur::vanPersoon($this);

		return $this->voorkeur;
	}

	/**
	 * Zoek alle actieve commissies, disputen en groepen waar deze persoon in zit.
	 * Als afgelopenCies true is, wordt de einddatum van deze cies niet meegenomen
	 * (maar de einddatum van CommissieLid wel).
	 * @param alleenCies Zo ja, geef alleen commissies, geen disputen of groepen.
	 * @param afgelopenCies Zo ja, geef ook commissies wier einddatum al geweest is.
	 */
	public function getCommissies ($alleenCies = false, $afgelopenCies = false)
	{
		return CommissieVerzameling::vanPersoon($this, false, false, $alleenCies, $afgelopenCies);
	}

	public function getCommissieLids ($huidige = true)
	{
		return CommissieLidVerzameling::vanPersoon($this, $huidige);
	}

	public function getOudeCommissies ()
	{
		return CommissieVerzameling::vanPersoon($this, true);
	}

	public function getCommissiesMetBugs ()
	{
		return CommissieVerzameling::vanPersoonMetBugs($this);
	}

	public function getDeelgenomenActiviteiten ($publiek = true, $datum = null)
	{
		return ActiviteitVerzameling::vanDeelgenomenPersoon($this, $publiek, $datum);
	}

	public function getGemeldeBugs ($alleenOpen = False)
	{
		return BugVerzameling::geefGemeld($this, $alleenOpen);
	}

	public function getToegewezenBugs ($alleenOpen = False)
	{
		return BugVerzameling::geefToegewezen($this, $alleenOpen);
	}

	public function getGevolgdeBugs ($alleenOpen = False)
	{
		return BugVerzameling::geefGevolgd($this, $alleenOpen);
	}

	/**
	 * @brief Geeft terug of deze persoon kandidaatbestuurslid is
	 * Als er geen kb is, is deze persoon geen kb'er.
	 * @return bool of het een kb'er is
	 */
	public function isKandidaatBestuur()
	{
		$kb = Commissie::kandidaatBestuur();
		return !is_null($kb) && $kb->hasLid($this);
	}

	public function isActiefGeweest(){

		$cies = new CommissieVerzameling();
		$cies->union($this->getCommissies());
		$cies->union($this->getOudeCommissies());
		return $cies->aantal() > 0;

	}

	public function isActief($alleenCies = false){
		$cies = new CommissieVerzameling();
		$cies->union($this->getCommissies($alleenCies));
		return $cies->aantal() > 0;
	}

	/**
	 * @brief geeft aan of deze persoon sponsor commissaris is
	 * Een sponsorcommisaris is een functiebeschrijving in een commissie.
	 * Dit is degene die sponsors regelt voor een commissie.
	 * Daarom moet deze ook in spookweb dingen mogen zien.
	 */
	public function isSponsorCommisaris()
	{
		foreach(CommissieLidVerzameling::vanPersoon($this) as $cielid) {
			if($cielid->getFunctie() == 'SPONS') {
				return true;
			}
		}
		return false;
	}

	/**
	 * Retourneert het Persoon object behorend bij het ingelogde personage,
	 * of null als er niet ingelogd is
	 * Dit verandert afhankelijk van in wiens je gekropen bent!
	 * @see space/auth/Auth.php:getLidnr
	 *
	 * @return Persoon|null
	 */
	public static function getIngelogd()
	{
		global $auth;
		$contactid = $auth->getLidnr();
		if ($contactid) return Persoon::geef($contactid);
		return null;
	}

	/**
	 * Retourneert het Persoon object ook inlogde op de site tijdens het begin van de sessie
	 * of null als er niet ingelogd is
	 * Dit blijft hetzelfde gedurende de hele sessie.
	 * @see space/auth/autorisatie.php:getRealLidnr
	 */
	public static function getRealIngelogd()
	{
		global $auth;
		$contactid = $auth->getRealLidnr();
		if ($contactid) return Persoon::geef($contactid);
		return null;
	}

	static public function enumsGeslacht()
	{
		if(hasAuth('bestuur'))
			return parent::enumsGeslacht();

		static $vals = array('M','V');
		return $vals;
	}

	public function mijnActiviteiten()
	{
		user_error('501 Not Implemented', E_USER_ERROR);
	}
	public function mijnCommissies($huidige = True)
	{
		static $cies = array();
		if(!isset($cies[$huidige])) {
			$cies[$huidige]
				= CommissieLidVerzameling::vanPersoon($this->getContactID()
					, $huidige);
		}
		return $cies[$huidige];
	}

	public function getNaam ($type = WSW_NAME_DEFAULT)
	{
		return PersoonView::naam($this, $type);
	}

	public function controleerInformatie($positie = null){
		global $auth;
		$problemen = parent::controleerInformatie($positie);
		if ($positie != 2 && $positie != 3){
			if ($auth->getLidnr() == $this->geefID()) $persoon = 2;
			else $persoon = 3;
		}

		$link = '/Leden/' . $this->getContactID();
		$anchor = new HtmlAnchor($link, _("deze pagina"));

		return $problemen;
	}
	public function checkDatumGeboorte()
	{
		$error = parent::checkDatumGeboorte();
		if($error !== false)
			return $error;
		
		if($this->getDatumGeboorte() == null)
			return false;

		if($this->getDatumGeboorte()->hasTime() && $this->leeftijd() < 13)
			return _("Persoon moet 13 jaar of ouder zijn.");

		return false;
	}
	
	public static function velden($welke = NULL)
	{
		switch($welke) {
		case 'verzameling':
			$velden = array();
			$velden[] = 'Voornaam';
			$velden[] = 'Tussenvoegsels';
			$velden[] = 'Achternaam';
			$velden[] = 'Email';
			break;
		case 'get':
			$velden = parent::velden($welke);
			if (!hasAuth('bestuur'))
				$velden = array_diff($velden, array('Opmerkingen'));
			break;
		default:
			$velden = parent::velden($welke);
		}
		return $velden;
	}

	public function leeftijd()
	{
		//Er schijnt een bug te zijn in php dat het verschil in jaren tussen een datum '1 maart jaar' en '1 maart schrikkeljaar' precies 1 jaar te weinig is, vandaar dat er een if is die dit controleert
		$geboortedatum = $this->getDatumGeboorte();
		if(!$geboortedatum->hasTime()) {
			return null;
		}
		$vandaag = new DateTimeLocale('now');
		if(((int)$geboortedatum->format('L')) && ((int)$geboortedatum->format('m') == 3) && ((int)$geboortedatum->format('d') == 1)
			&& ((int)$vandaag->format('m') == 3) && ((int)$vandaag->format('d') == 1) && ((int)$vandaag->format('Y') % 4 ) != 0)
			return $geboortedatum->diff($vandaag)->format('%y') + 1;
		else
			return $geboortedatum->diff($vandaag)->format('%y');
	}

	/**
	 *  Verander een Lid object in een Persoon object en pas ook de db aan.
	 **/
	public static function lidToPersoon(Lid $lid)
	{
		global $WSW4DB;

		if(!$lid->inDB)
			throw new LogicException('object is not inDB');

		//Gooi een eventuele IntroDeelnemer weg
		if ($introdeelnemer = IntroDeelnemer::geef($lid))
		{
			$returnString = $introdeelnemer->verwijderen();
			if(!is_null($returnString)) {
				return $returnString;
			}
		}

		$now = new DateTimeLocale();

		//Pas de overerving aan
		$WSW4DB->q('UPDATE `Contact`'
		           .' SET `overerving` = "Persoon"'
		           .   ', `gewijzigdWanneer` = %s'
		           .   ', `gewijzigdWie` = %i'
		           .' WHERE `contactID` = %i'
		           , $now->strftime('%F %T')
		           , Persoon::getIngelogd()->geefID()
		           , $lid->contactID
		           );

		//Gooi de studies weg
		$WSW4DB->q('DELETE FROM `LidStudie`'
				  . ' WHERE `lid_contactID` = %i'
				  , $lid->contactID
				  );

		//Haal lidinfo uit de db
		$WSW4DB->q('DELETE FROM `Lid`'
		          .' WHERE `contactID` = %i'
		          , $lid->contactID
		          );

		//Stel de cache bij
		self::cache(array(array($lid->geefID())), true);

		//En geef het juiste object terug.
		return self::geef($lid->geefID());
	}

	public function opslaan ($classname = 'Persoon')
	{
		parent::opslaan($classname);

		// Als we bezig zijn met een lid slaan we de persoonvoorkeuren daar op
		// om te voorkomen dat als er een nieuw lid aangemaakt wordt
		// het Lid-object nog niet opgeslagen is (het persoon-object wel)
		// waardoor de voorkeur ook niet opgeslagen kan worden
		if(!($this instanceof Lid)) {
			// Sla de voorkeur op, of gooi hem weg als alles default is
			$voorkeur = $this->getVoorkeur();
			if (!$voorkeur->isDefault())
			{
				$voorkeur->opslaan();
			}
			else if ($voorkeur->getInDB())
			{
				$voorkeur->verwijderen();
			}
		}
	}

	public function checkVerwijderen()
	{
		$watkeweest = array();
		$redenen = array();

		if(CommissieLidVerzameling::vanPersoon($this)->aantal() > 0)
			$watkeweest[] = _("commissielid (geweest)");

		if(DeelnemerVerzameling::vanPersoon($this)->aantal() > 0)
			$watkeweest[] = _("deelnemer (geweest)");

		if(Donateur::geef($this))
			$watkeweest[] = _("donateur (geweest)");

		if(count($watkeweest) > 0)
			$redenen[] = sprintf(
				_("is %s[VOC: commissielid, deelnemer, donateur (geweest)]"),
				implode(", ", $watkeweest));

		if(TransactieVerzameling::getPersoonTransacties($this)->aantal() > 0)
			$redenen[] = _('[VOC: deze persoon ]is aan transacties gekoppeld');

		if(count($redenen) > 0)
			return sprintf(_('Deze persoon %s[VOC: redenen dat persoon niet verwijderd mag worden]!'), implode(_(" en "), $redenen));

		return false;
	}

	/**
	 *   Gegeven en persoon ga de naam analyseren en geef een array met een naam die ons meer aanstaat.
	 *  Ondersteunt:
	 *   mensen met alleen achternamen
	 *   geen of rare voorletters
	 *   tussenvoegsels in de achternaam
	 *    (inclusief tussenvoegsels die er al stonden)
	 *   klein-geletterde namen
	 *   mensen die lege velden niet leeglaten
	 *   voorletters (in de vorm van letter(s)-met-punt) in de achternaam
	 *   tweede namen in tussenvoegsel
	 *
	 *  @param persoon de persoon die we gaan analyseren.
	 **/
	public static function naamsuggestie($persoon)
	{
		// als een naamdeel er zo uitziet, dan zal het vermoedelijk een tussenvoegsel zijn
		// deze komen uit onze top-25 tussenvoegsels, vul aan indien nodig
		$tussenvoegsels = array(
			"van", "de", "der", "den", "ten", "'t", "dos", "a", "à", "al", "ben", "de",
			"du", "la", "el", "het", "in", "van't", "von", "der", "da", "op", "del",
		);

		// doe alleen een suggestie als er een verandering is
		$veranderd = false;
		
		//Kijk of het alleen een achternaam heeft
		if(!PersoonView::waardeVoornaam($persoon) && !PersoonView::waardeVoorletters($persoon)
			&& !PersoonView::waardeGeboortenamen($persoon) && !PersoonView::waardeTussenvoegsels($persoon)
			&& PersoonView::waardeAchternaam($persoon) ) {
			
			$nieuweNaam = array();
			$oudeNaam = PersoonView::waardeAchternaam($persoon);

			// Soms stoppen mensen e-mailadressen in achternamen, foei!
			if (strpos($oudeNaam, '@') !== FALSE) {
				Page::addMelding(_('Er is een emailadres in de achternaam! Foei!'), 'fout');
				return null;
			} else {
				$naamArray = explode(" ", $oudeNaam);

				// iemand waarvan we alleen een achternaam met tussenvoegsel weten,
				// krijgt die normaal als voornaam, pak dat hier aan
				if (in_array(strtolower($naamArray[0]), $tussenvoegsels)) {
					$nieuweNaam["voornaam"] = "";
					$nieuweNaam["voorletters"] = "";
					$nieuweNaam["tussenvoegsels"] = strtolower($nieuweNaam["voornaam"]);
					$nieuweNaam["achternaam"] = implode(" ", $naamArray);
				} else {
					// doe een wilde gok naar de naam
					$nieuweNaam['voornaam'] = array_shift($naamArray);
					$nieuweNaam['voorletters'] = $nieuweNaam['voornaam'][0];
					$nieuweNaam['achternaam'] = implode(" ", $naamArray);
					$nieuweNaam['tussenvoegsels'] = ""; // wordt later opgevuld
				}
				
				$veranderd = true;
			}
		} else {
			$nieuweNaam = array(
				"voornaam" => PersoonView::waardeVoornaam($persoon),
				"voorletters" => PersoonView::waardeVoorletters($persoon),
				"tussenvoegsels" => PersoonView::waardeTussenvoegsels($persoon),
				"achternaam" => PersoonView::waardeAchternaam($persoon)
			);
		}
		
		foreach (array("voornaam", "voorletters", "tussenvoegsels", "achternaam") as $naamdeel) {
			// haal strings die een leeg veld aangeven weg
			if ($nieuweNaam[$naamdeel] == "-" || $nieuweNaam[$naamdeel] == "_" ||
					$nieuweNaam[$naamdeel] == "." || $nieuweNaam[$naamdeel] == "?" ||
					$nieuweNaam[$naamdeel] == "- -") {
				$nieuweNaam[$naamdeel] = "";
				$veranderd = true;
			}
		}

		// Als iemand (met kleine letters!) een tussenvoegsel invult als voorletters,
		// zal dat toch wel een tussenvoegsel horen te zijn.
		if (!$nieuweNaam["tussenvoegsels"] &&
				in_array($nieuweNaam['voorletters'], $tussenvoegsels)) {
			$nieuweNaam['tussenvoegsels'] = $nieuweNaam['voorletters'];
			$nieuweNaam['voorletters'] = $nieuweNaam['voornaam'][0] . ".";
			$veranderd = true;
		}

		// Voorletters identiek aan voornaam lijkt me niet te kloppen,
		// tenzij de ouders van dit figuur wel heel creatief waren...
		// (negeer de gevallen waar ze allebei leeg zijn)
		if ($nieuweNaam['voornaam'] == $nieuweNaam['voorletters'] && $nieuweNaam['voornaam']) {
			$nieuweNaam['voorletters'] = $nieuweNaam['voornaam'][0] . ".";
			$veranderd = true;
		}

		$nieuweTussenvoegsels = explode(' ', $nieuweNaam["tussenvoegsels"]);
		// haal tweede namen uit tussenvoegsels
		foreach ($nieuweTussenvoegsels as $i => $tussenvoegsel) {
			unset($nieuweTussenvoegsels[$i]);

			if ($tussenvoegsel == "") continue;

			if ($tussenvoegsel != strtolower($tussenvoegsel)) {
				// hoofdletter in tussenvoegsel, dat is verdacht
				$tussenvoegsel = strtolower($tussenvoegsel);

				if (in_array($tussenvoegsel, $tussenvoegsels)) {
					// het zal wel een tussenvoegsel zijn
					$nieuweTussenvoegsels[$i] = $tussenvoegsel;
				} else {
					// een tweede naam, misschien? Die onthouden we niet
				}
				$veranderd = true;
			} else {
				$nieuweTussenvoegsels[$i] = $tussenvoegsel;
			}
		}
		ksort($nieuweTussenvoegsels); // blijkbaar komt index 1 soms voor index 0 terecht
		$nieuweNaam["tussenvoegsels"] = implode(" ", $nieuweTussenvoegsels);

		// haal populaire tussenvoegsels en voorletters uit de achternaam
		// om te voorkomen dat we voorletters dubbelop toevoegen, houd bij of ze er al waren
		$hadVoorletters = $nieuweNaam['voorletters'];
		$achternamen = explode(' ', $nieuweNaam["achternaam"]);
		for ($i = 0; $i < count($achternamen); $i++) {
			$achternaam = $achternamen[$i];
			unset($achternamen[$i]);
			
			// ziet er wel uit als een tussenvoegsel?
			if (in_array($achternaam, $tussenvoegsels)) {
				if ($nieuweNaam["tussenvoegsels"]) {
					// voor het geval dat het tussenvoegsel in de achternamen en in de tussenvoegsels staat
					if ($nieuweNaam["tussenvoegsels"] != $achternaam) {
						$nieuweNaam["tussenvoegsels"] .= ' ' . $achternaam;
					}
				} else {
					$nieuweNaam["tussenvoegsels"] = $achternaam;
				}
				$veranderd = true;
			} else if (substr($achternaam, -1) == '.') { // voorletter?
				// zorg ervoor dat we handmatig ingevoerde voorletters niet slopen
				if ($hadVoorletters) {
					// maar voorletters die er automatisch gegenereerd uitzien kunnen we maar beter aanpakken
					if ($nieuweNaam["voorletters"] == $nieuweNaam["voornaam"][0] . '.') {
						$nieuweNaam["voorletters"] = $achternaam;
						$veranderd = true;
					}
				} else {
					// zet het maar achteraan, dan houden we de volgorde
					$nieuweNaam["voorletters"] .= $achternaam;
					$veranderd = true;
				}
			} else {
				// het is geen tussenvoegsel, stop het terug tussen de achternamen
				$achternamen[$i] = $achternaam;
				break;
			}
		}
		// plak alle niet-tussenvoegselachtige dingen aan elkaar tot achternamen
		ksort($achternamen); // blijkbaar komt index 1 soms voor index 0 terecht
		$nieuweNaam["achternaam"] = implode(' ', $achternamen);
		
		// probeer kleine letters om te zetten naar hoofdletters
		// struikelt dus over achterblijvende tussenvoegsels in de achternaam
		if (strlen($nieuweNaam["voornaam"]) > 0 && ctype_lower($nieuweNaam["voornaam"][0])) {
			$nieuweNaam["voornaam"] = ucwords($nieuweNaam["voornaam"]);
			$veranderd = true;
		}
		
		// probeer voorletters om te zetten in het formaat:
		// A.B.Chr.D.
		if (strlen($nieuweNaam["voorletters"]) > 0) {
			$result = "";
			
			// check of de gebruiker competent genoeg is om een separator te gebruiken
			// probeer de voorletters op te breken op populaire separatorkeuzes
			$parts = preg_split('/[\.\s]\s*/', $nieuweNaam["voorletters"]);
			
			if (count($parts) <= 1) {
				// anders hopen we dat het niet een gevalletje AFTh -> A.F.Th. (van der Heijden) is
				// elke letter wordt een voorletter
				$parts = str_split($nieuweNaam["voorletters"]);
			}
			
			// zorg ervoor dat elk deel een hoofdletter ervoor heeft
			// en negeer lege string (bijv. aan het einde)
			foreach($parts as $part) {
				if ($part != "") {
					$result .= ucfirst($part) . '.';
				}
			}
			
			// verander alleen indien nodig
			if ($nieuweNaam["voorletters"] != $result) {
				$nieuweNaam["voorletters"] = $result;
				$veranderd = true;
			}
		} elseif($nieuweNaam["voornaam"]) {
			// maak een voorletter van de eerste letter van de voornaam
			$nieuweNaam["voorletters"] = strtoupper($nieuweNaam["voornaam"][0]) . ".";
			$veranderd = true;
		}
		
		// probeer hoofdletters te maken in de achternaam
		if (strlen($nieuweNaam["achternaam"]) > 0 && ctype_lower($nieuweNaam["achternaam"][0])) {
			$nieuweNaam["achternaam"] = ucwords($nieuweNaam["achternaam"]);
			$veranderd = true;
		}
		
		// alleen als we het konden aanpakken, is het nuttig om een verandering te suggereren
		if ($veranderd) {
			return $nieuweNaam;
		}
		
		return null;
	}

	public function getFotoNaam ()
	{
        return substr($this->geefID(),-2).'/lid'.$this->geefID().'.png';
	}

	public function getBadges()
	{
		$badges = PersoonBadge::geef($this);
		if(!$badges)
			$badges = new PersoonBadge($this);

		return $badges;
	}

	public function hasBadge($badgeNaam)
	{
		$badges = json_decode($this->getBadges()->getBadges());

		if(!empty($badges) && in_array($badgeNaam, $badges)) {
			return true;
		}

		return false;
	}

	public function mijnCiesGroepenDisputen($soort)
	{
		$namen = array('CIE' => _('Mijn Commissies')
			, 'GROEP' => _('Mijn Groepen')
			, 'DISPUUT' => _('Mijn Disputen'));

		$ciehome = '/Vereniging/Commissies/';

		$children = array();
		$mycies = Persoon::getIngelogd()->getCommissies();
		foreach ($mycies as $cie) {
			if ($cie->getSoort() != $soort) continue;
			$cutname = $cie->getNaam();
			$tooltip = ($cutname == $cie->getNaam() ? NULL : $cie->getNaam());
			$children[] = array('url' => $cie->getUrlBase(),
				'displayName' => $cutname,
				'tooltip' => $tooltip);
		}

		if ( hasAuth('actief') && count($children) > 0) {
			$menu = array('displayName' => $namen[$soort], 'children' => $children);
			return $menu;
		}
	}

	public function heeftFotos()
	{
		if (isPersoonBeperkt($this->getContactID())) return false;

		global $WSW4DB;

		$ids = $WSW4DB->q('MAYBEVALUE SELECT COUNT(`mediaID`) FROM `Media` '
			. 'LEFT JOIN `Tag` ON `Tag`.`media_mediaID` = `Media`.`mediaID` '
			. 'WHERE `Tag`.`persoon_contactID` = %i'
			, $this->geefID());

		return $ids > 0;
	}

	public function heeftPlanners()
	{
		return PlannerVerzameling::getOpenPlanners($this)->aantal() > 0;
	}

	/**
	 * @brief Genereert een lijst van alle ouders van deze persoon met als getal
	 * de hoeveelste generatie boven je het is
	 */
	public function ouderBoom()
	{
		$queue = [ [ 0, $this ] ];

		$had = [];

		while(count($queue) != 0) {
			$el = array_shift($queue);

			$persoon = $el[1];
			$count = $el[0];

			if(array_key_exists($persoon->geefID(), $had)) {
				continue;
			} else {
				$had[$persoon->geefID()] = $count;
			}

			$groepen = $persoon->mentorgroepen();

			foreach($groepen as $groep) {
				$mentoren = $groep->mentoren();

				foreach($mentoren as $mentor) {
					$queue[] = [ $count+1, $mentor ];
				}
			}
		}

		return $had;
	}

	/**
	 * @brief Geeft de benaming voor jouw relatie met deze persoon
	 * @return Een menselijk leesbare string voor de relatie, of False wanneer er geen naam bekend is.
	 */
	public function geefRelatieMet($persoon)
	{
		$boom1 = $persoon->ouderBoom();
		$boom2 = Persoon::getIngelogd()->ouderBoom();

		// Een super groot getal zodat we zeker weten dat alles kleiner is
		$maxSum = 100000;
		$up = 0; $down = 0;

		// Bereken de minimale afstand
		foreach($boom1 as $who => $count) {
			if(array_key_exists($who, $boom2)) {
				$sum = $count + $boom2[$who];

				if($sum < $maxSum) {
					$up = $boom2[$who];
					$down = $count;
					$maxSum = $sum;
				}
			}
		}

		if($up == 0 && $down == 0) {
			return False;
		}

		$genus = $persoon->getGeslacht();
		$voornaamwoord = $persoon->getVoornaamwoord();

		if($up == 1 && $down == 1) { // brussen
			return _('mentor') . $voornaamwoord->getBrusje();
		}

		if($up == 0) { // Nageslacht
			if($down == 1) {
				return _('mentor') . $voornaamwoord->getKind();
			} else {
				$klein_str = rtrim(str_repeat(_('achter-'), $down - 2), '-') . _('klein');
				return $klein_str . _('mentor') . $voornaamwoord->getKind();
			}
		}

		if($down == 0) { // Ouders
			switch($up) {
			case 1:
				$prefix = '';
				break;
			case 2:
				$prefix = _('groot');
				break;
			case 3:
				$prefix = _('overgroot');
				break;
			default:
				$prefix = rtrim(str_repeat(_('bet-'), $up - 3), '-') . _('overgroot');
				break;
			}
			return $prefix . _('mentor') . $voornaamwoord->getOuder();
		}

		if($up == $down) { // kozijnen
			$achter_str = rtrim(str_repeat(_('achter-'), $down - 2), '-');

			return $achter_str . _('mentor') . $voornaamwoord->getKozijn();
		}

		if($down < $up) { // ooms en tantes
			$prefix = str_repeat(_('achter-'), $down - 1);
			$prefix .= str_repeat(_('oud-'), $up - 2 - ($down - 1));
			$prefix = rtrim($prefix, '-');

			return $prefix . _('mentor') . $voornaamwoord->getKindGrootouder();
		}

		$eigenVoornaamwoord = $this->getVoornaamwoord();

		if($down > $up) {
			$prefix = str_repeat(_('achter-'), $up - 1);
			$prefix .= str_repeat(_('oud-'), $down - 2 - ($up - 1));
			$prefix = rtrim($prefix, '-');

			return $prefix . _('mentor') . sprintf($voornaamwoord->getKleinkindOuder(), $eigenVoornaamwoord->getKindGrootouder());
		}

		return False;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
