<?

/***
 * $Id$
 */
class CommissieActiviteit
	extends CommissieActiviteit_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b);
	}

	public function magVerwijderen()
	{
		return hasAuth('actief');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
