<?
class PersoonBadge
	extends PersoonBadge_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a)
	{
		parent::__construct($a); // PersoonBadge_Generated
	}

	public function magVerwijderen()
	{
		return hasAuth('bestuur') || Persoon::getIngelogd() == $this->getPersoon();
	}

	/*
	 *  Voegt een badge-string toe aan een PersoonBadge-object
	 *
	 * @param badge De naam van de badge om toe te voegen
	 * @param badge setlast Of de badge ook bij de last-array moet worden toegevoegd
	 */
	public function addBadge($badge, $setlast = true)
	{
		$badges_json_string = $this->getBadges();
		$last_json_string = $this->getLast();

		// vertaal de json-strings naar php-array's
		$badges = json_decode($badges_json_string);
		$last = json_decode($last_json_string);

		// als de badge al bij de persoon hoort hoeven we niets te doen
		if (!is_null($badges) && count($badges) != 0 && in_array($badge, $badges)) {
			return;
		}

		$badges[] = $badge;

		// zet de badge ook in last (als nodig)
		if($setlast) {
			if(is_null($last)){
				$last = array();
			}
			if(count($last) == 0 || (count($last) != 0 && !in_array($badge, $last))) {
				$last[] = $badge;
			}
		}

		// vertaal de arrays weer terug naar json
		$badges_json_string = json_encode($badges);
		$this->setBadges($badges_json_string);

		if (count($last) != 0) {
			$last_json_string = json_encode($last);
			$this->setLast($last_json_string);
		}

		$this->opslaan();
	}

	/*
	 *  Deze functie haalt een badge-string uit een PersoonBadge-object
	 *
	 * @param badge De badge om weg te halen
	 */
	public function removeBadge($badge)
	{
		$badges_json_string = $this->getBadges();
		$last_json_string = $this->getLast();

		// vertaal de json-strings naar php-array's
		$badges = json_decode($badges_json_string);
		$last = json_decode($last_json_string);

		// als de badge niet bij de persoon hoort hoeven we niets te doen
		if (count($badges) == 0 || !in_array($badge, $badges)) {
			return;
		}

		// vind de positie en unset het element van de array
		if (($key = array_search($badge, $badges)) !== false) {
			unset($badges[$key]);
			$badges = array_values($badges);
		}

		// haal de badge ook weg uit last
		if(!is_null($last)) {
			if (($key = array_search($badge, $last)) !== false) {
				unset($last[$key]);
				$last = array_values($last);
			}
		}

		// vertaal de arrays weer terug naar json
		$badges_json_string = json_encode($badges);
		$this->setBadges($badges_json_string);

		if(!empty($last)) {
			$last_json_string = json_encode($last);
			$this->setLast($last_json_string);
		}

		$this->opslaan();
	}

	/*
	 *  Checkt of een persoon al een badge heeft
	 *
	 * @param badge De badge om te checken
	 */
	public function bevatBadge($badge)
	{
		$badges_json_string = $this->getBadges();

		$badges = json_decode($badges_json_string);

		if(in_array($badge, $badges)) {
			return true;
		}

		return false;
	}

	/*
	 *  Checkt voor alle badges of de persoon ze heeft gehaald of niet
	 *
	 * @param pers De persoon om de padges voor te checken
	 */
	static public function checkAllBadges(Persoon $pers)
	{
		global $BADGES;

		foreach($BADGES['onetime'] as $key => $badge)
		{
			self::checkOneTimeBadge($key, $pers);
		}

		foreach($BADGES['bsgp'] as $key => $badge)
		{
			self::checkBSGPBadge($key, $pers);
		}
	}

	/**
	 *  Telt deze commissie mee voor een achievement
	 * i.e. heeft de commissie de categorie die nodig is voor
	 * de achievement
	 *
	 * @param cie De commissie om te checken
	 * @param catId Het Id van de categorie om te checken
	 *
	 * @return Een bool met of de cie mee telt
	 */
	static public function checkCie($cie, $catId) {
		$cat = $cie->getCategorie();
		if($cat) {
			return $cat->geefID() == $catId;
		}
		return false;
	}

	/*
	 *  Checkt een one-time badge voor een persoon
	 *
	 * @param badgeNaam De naam van de badge om te checken
	 * @param pers De persoon om de badge van te checken
	 * @param check Een bool die aangeeft of het alleen voor een check gebruikt wordt
	 *
	 * @return Een bool die false teruggeeft als de persoon niet de badge verdiet
	 * en true als check true is en als de persoon wel de badge verdient
	 */
	static public function checkOneTimeBadge($badgeNaam, Persoon $pers, $check = false)
	{
		global $request, $WSW4DB, $BADGES;

		$cies = new CommissieVerzameling();
		$cies->union($pers->getCommissies());
		$cies->union($pers->getOudeCommissies());

		// moeten we de badge geven als we door de switch heen zijn?
		$geefBadge = false;

		switch($badgeNaam) {
			case 'iba':
				foreach($cies as $cie) {
					if (self::checkCie($cie, 12)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'bestuur':
				foreach($cies as $cie) {
					if (self::checkCie($cie, 1)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'ec':
				foreach($cies as $cie) {
					if (self::checkCie($cie, 7)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'ar':
				foreach($cies as $cie) {
					if (self::checkCie($cie, 18)) {
						$geefBadge = true;
						break;
					}
					if (self::checkCie($cie, 14)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'almanak':
				foreach($cies as $cie) {
					if (self::checkCie($cie, 4)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'reis':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 6)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'intro':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 8)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'sympo':
				foreach($cies as $cie) {
					if (self::checkCie($cie, 9)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'ingelogd':
				$ww = Wachtwoord::geef($pers);
				$date = new DateTimeLocale();
				$geefBadge = $ww && $ww->getLaatsteLogin() < $date;
				break;
			case 'perry':
				$geefBadge = $request->query->get('perry') == "'daar!'"
					&& $pers == Persoon::getIngelogd();
				break;
			
			case 'feedback':
				$bugaantal = $WSW4DB->q('VALUE SELECT COUNT(`melder_contactID`) '
				. 'FROM `Bug` '
				. 'WHERE `melder_contactID` = %i '
				, $pers->geefID());
				$geefBadge = $bugaantal > 0;
				break;

			case 'nijntje':
				global $BWDB;
				$res = $BWDB->q("VALUE SELECT COUNT(`lidnr`) "
					. "FROM `verkoop` "
					. "WHERE `lidnr` = %i"
					, $pers->geefID());
				$geefBadge = $res > 0;
				break;
			case 'supermentor':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 19)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'kart':
				global $WSW4DB;
				$res = $WSW4DB->q("VALUE SELECT COUNT(`persoonKart_persoon_contactID`) "
					. "FROM `KartVoorwerp` "
					. "WHERE `persoonKart_persoon_contactID` = %i"
					, $pers->geefID());
				$geefBadge = $res > 0;
				break;
			case 'even':
				if($pers->geefID() % 2 == 0) {
					$geefBadge = true;
				}
				break;
			case 'oneven':
				if($pers->geefID() % 2 == 1) {
					$geefBadge = true;
				}
				break;
			case 'eureka':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 23)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'donateur':
				global $WSW4DB;
				$res = $WSW4DB->q("MAYBEVALUE SELECT `persoon_contactID` "
					. "FROM `Donateur` "
					. "WHERE `persoon_contactID` = %i"
					, $pers->geefID());
				$geefBadge = $res > 0;
				break;
			case 'medezeggenschap':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 24)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'bier':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 25)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'eten':
				foreach($cies as $cie) {
					if(self::checkCie($cie, 21)) {
						$geefBadge = true;
						break;
					}
					if(self::checkCie($cie, 26)) {
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'kamerdienst':
				foreach ($cies as $cie)
				{
					if ($cie->getLogin() == 'kamerdienst')
					{
						$geefBadge = true;
						break;
					}
				}
				break;
			case 'badges':
			case 'stamboom':
			case 'nolife':
			case 'tentamen':
			case 'docuweb':
			case 'jaloers':
			case 'creeper':
			case 'webcie':
			case 'almanakstukje':
			case 'historie':
			case 'hacker':
			case 'ibablog':
			case 'planner':
			case 'activiteit':
			case 'virus':
			case 'iba-activiteit':
			case 'dilbert':
			case 'vakidioot': // TODO: Mocht VakidiootWeb ooit nog gebeuren, dan kan dit via een mooie kwerrie
				break;
			default:
				if(array_key_exists($badgeNaam, $BADGES['onetime']))
					user_error("Badge " . $badgeNaam . " niet geconfigureerd!");
				else
					user_error("Badge " . $badgeNaam . " bestaat niet!");
				break;
		}

		if ($geefBadge) {
			if(!$check) {
				$persoonBadges = $pers->getBadges();
				$persoonBadges->addBadge($badgeNaam);
			} else {
				return true;
			}
		}

		return false;
	}

	/*
	 *  Checkt voor een badge alle bronze-silver-gold-platinum voor een persoon
	 *
	 * @param badgeNaam De naam van de badge om te checken
	 * @param pers De persoon om de badge van te checken
	 * @param setlast Een bool die aangeeft of last ook geset moet worden
	 */
	static public function checkBSGPBadge($badgeNaam, Persoon $pers, $setlast = true)
	{
		global $WSW4DB, $BADGES;

		$badgesBSGP = $BADGES['bsgp'];

		if(array_key_exists($badgeNaam, $badgesBSGP)) {
			$badge = $badgesBSGP[$badgeNaam];

			if($badge['multi'] == 'long') {
				$multiplier = array(1, 2, 5, 10);
			} elseif($badge['multi'] == 'short') {
				$multiplier = array(1, 2, 3, 4);
			}

			// Tel eerst de badges zodat we dit maar 1x hoeven te doen
			$res = PersoonBadge::badgeBSGPCount($badgeNaam, $pers);

			foreach($multiplier as $multi) {
				if($res >= $badge['value'] * $multi) {
					$persoonBadges = $pers->getBadges();
					$persoonBadges->addBadge($badgeNaam . '_' . ($badge['value'] * $multi), $setlast);
				}
			}
		} else {
			user_error("Badge " . $badgeNaam . " bestaat niet!");
		}
	}

	/*
	 *  Checkt een enkele bronze-silver-gold-platinum badge voor een persoon
	 *
	 * @param badgeNaam De naam van de badge om te checken
	 * @param count Welke van de vier badges het moet zijn
	 * @param pers De persoon om de badge van te checken
	 * @param check Een bool die aangeeft of het alleen voor een check gebruikt wordt
	 *
	 * @return Een bool die false teruggeeft als de persoon niet de badge verdiet
	 * en true als check true is en als de persoon wel de badge verdient
	 */
	static public function checkSingleBSGPBadge($badgeNaam, $count, Persoon $pers, $check = true)
	{
		global $WSW4DB, $BADGES;

		$badgesBSGP = $BADGES['bsgp'];

		if(array_key_exists($badgeNaam, $badgesBSGP)) {
			$badge = $badgesBSGP[$badgeNaam];

			if($badge['multi'] == 'long') {
				$multiplier = array(1, 2, 5, 10);
			} elseif($badge['multi'] == 'short') {
				$multiplier = array(1, 2, 3, 4);
			}

			$multi = $multiplier[$count];
			$res = PersoonBadge::badgeBSGPCount($badgeNaam, $pers);
			if($res >= $badge['value'] * $multi) {
				if(!$check) {
					$persoonBadges = $pers->getBadges();
					$persoonBadges->addBadge($badgeNaam . '_' . ($badge['value'] * $multi), $setlast);
				} else {
					return true;
				}
			}

			return false;
		} else {
			user_error("Badge " . $badgeNaam . " bestaat niet!");
		}
	}

	/*
	 *  Voert de tel-query uit die bij de bsgp-badges hoort voor een persoon
	 *
	 * @param badgeNaam De naam van de badge om de query van uit te voeren
	 * @param pers De persoon om van te tellen
	 *
	 * @return Het resultaat van de query
	 */
	static public function badgeBSGPCount($badgeNaam, Persoon $pers)
	{
		global $WSW4DB, $BADGES;

		$res = 0;

		switch($badgeNaam)
		{
		case 'bug_melder':
			return $WSW4DB->q('VALUE SELECT COUNT(`melder_contactID`) '
				. 'FROM `Bug` '
				. 'WHERE `melder_contactID` = %i '
				, $pers->geefID());
			break;
		case 'commissie_lid':
			return $WSW4DB->q('VALUE SELECT COUNT(`CommissieLid`.`persoon_contactID`) '
				. 'FROM `CommissieLid` '
				. 'LEFT JOIN `Commissie` '
				. 'ON `CommissieLid`.`commissie_commissieID` = `Commissie`.`commissieID` '
				. 'WHERE `CommissieLid`.`persoon_contactID` = %i '
				. 'AND `Commissie`.`soort` = \'CIE\' '
				, $pers->geefID());
			break;
		case 'groep_lid':
			return $WSW4DB->q('VALUE SELECT COUNT(`CommissieLid`.`persoon_contactID`) '
				. 'FROM `CommissieLid` '
				. 'LEFT JOIN `Commissie` '
				. 'ON `CommissieLid`.`commissie_commissieID` = `Commissie`.`commissieID` '
				. 'WHERE `CommissieLid`.`persoon_contactID` = %i '
				. 'AND `Commissie`.`soort` = \'GROEP\' '
				, $pers->geefID());
			break;
		case 'dispuut_lid':
			return $WSW4DB->q('VALUE SELECT COUNT(`CommissieLid`.`persoon_contactID`) '
				. 'FROM `CommissieLid` '
				. 'LEFT JOIN `Commissie` '
				. 'ON `CommissieLid`.`commissie_commissieID` = `Commissie`.`commissieID` '
				. 'WHERE `CommissieLid`.`persoon_contactID` = %i '
				. 'AND `Commissie`.`soort` = \'DISPUUT\' '
				, $pers->geefID());
			break;
		case 'dibstransactie':
			return $WSW4DB->q('VALUE SELECT COUNT(`contact_contactID`) '
				. 'FROM `Transactie` '
				. 'WHERE `contact_contactID` = %i '
				. 'AND `overerving` = \'DibsTransactie\' AND `bedrag` > 0 '
				, $pers->geefID());
			break;
		case 'tag_tagger':
			return $WSW4DB->q('VALUE SELECT COUNT(`tagger_contactID`) '
				. 'FROM `Tag` '
				. 'WHERE `tagger_contactID` = %i '
				. 'AND `collectie_collectieID` IS NULL '
				, $pers->geefID());
			break;
		case 'tag_getagd':
			return $WSW4DB->q('VALUE SELECT COUNT(`persoon_contactID`) '
				. 'FROM `Tag` '
				. 'WHERE `persoon_contactID` = %i '
				, $pers->geefID());
			break;
		case 'rating_persoon':
			return $WSW4DB->q('VALUE SELECT COUNT(`persoon_contactID`) '
				. 'FROM `Rating` '
				. 'WHERE `persoon_contactID` = %i '
				, $pers->geefID());
			break;
		case 'mentor_persoon':
			return $WSW4DB->q('VALUE SELECT COUNT(`lid_contactID`) '
				. 'FROM `Mentor` '
				. 'WHERE `lid_contactID` = %i '
				, $pers->geefID());
			break;
		case 'fotograaf_persoon':
			return $WSW4DB->q('VALUE SELECT COUNT(`fotograaf_contactID`) '
				. 'FROM `Media` '
				. 'WHERE `fotograaf_contactID` = %i '
				, $pers->geefID());
			break;
		case 'IOUBon_persoon':
			return $WSW4DB->q('VALUE SELECT COUNT(`T`.`gewijzigdWie`) '
				. 'FROM (SELECT `IOUBetaling`.`gewijzigdWie` '
				. ' FROM `IOUBetaling` '
				. ' LEFT JOIN `IOUBon` ON `IOUBon`.`bonID` = `IOUBetaling`.`bon_bonID` '
				. ' GROUP BY `IOUBetaling`.`bon_bonID`, '
				. ' IF(`IOUBetaling`.`bon_bonID` IS NULL, `IOUBetaling`.`betalingID`, 0)) as T '
				. 'WHERE `T`.`gewijzigdWie` = %i '
				, $pers->geefID());
			break;
		case 'iDealTransactie':
			return $WSW4DB->q('VALUE SELECT COUNT(`contact_contactID`) '
				. 'FROM `Transactie` '
				. 'WHERE `contact_contactID` = %i '
				. 'AND `overerving` = \'iDeal\' AND `status` = \'SUCCESS\' '
				, $pers->geefID());
			break;
		case 'activiteiten':
			return $WSW4DB->q('VALUE SELECT COUNT(`persoon_contactID`) '
				. 'FROM `Deelnemer` '
				. 'WHERE `persoon_contactID` = %i '
				, $pers->geefID());
			break;
		case 'uitwerking':
			return $WSW4DB->q('VALUE SELECT COUNT(`uploader_contactID`) '
				. 'FROM `TentamenUitwerking` '
				. 'WHERE `uploader_contactID` = %i '
				, $pers->geefID());
			break;
		case 'generatie':
			$lid = Lid::geef($pers->geefID());
			if (!$lid) {
				// personen hebben geen kindjes dus krijgen geen achievement
				return 0;
			}
			$groepen = $lid->mentorVan();
			if($groepen->aantal() == 0)
				return 0;
			$alBezocht = new IntroGroepVerzameling();

			$groepCounter = array();
			foreach($groepen as $groep)
				$groepCounter[$groep->geefID()] = 1;

			while ($groepen->aantal() > 0) {
				$groep = $groepen->first();
				if ($alBezocht->bevat($groep)) {
					$groepen->verwijder($groep);
					$groepen->rewind();
					continue;
				}
				$g2 = $groep->getMentorGroepenVanMentoren();
				$alBezocht->voegtoe($groep);
				$groepen->verwijder($groep);
				$groepen->union($g2);
				$groepen->rewind();
				foreach($g2 as $g)
				{
					if(!array_key_exists($g->geefID(), $groepCounter))
						$groepCounter[$g->geefID()] = 1 + $groepCounter[$groep->geefID()];
				}
			}

			return max($groepCounter);
		default:
			if(array_key_exists($badgeNaam, $BADGES['bsgp']))
				user_error("Badge " . $badgeNaam . " niet geconfigureerd!");
			else
				user_error("Badge " . $badgeNaam . " bestaat niet!");
			break;
		}
	}

	/**
	 * Geef het aantal achievements van deze persoon.
	 *
	 * @return int
	 */
	public function aantalBadges()
	{
		$badges = json_decode($this->getBadges());
		if (is_null($badges))
		{
			return 0;
		}
		else
		{
			return count($badges);
		}
	}
}
