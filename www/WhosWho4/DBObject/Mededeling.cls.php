<?

/***
 * $Id$
 */
class Mededeling
	extends Mededeling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	public function checkCommissie()
	{
		global $auth;

		if (!$this->getCommissie() instanceof Commissie)
			return _('dit is een verplicht veld');
		if (!$auth->hasCieAuth($this->getCommissie()->geefID()))
			return _('geef een geldige commissie');
		return parent::checkCommissie();
	}
	public function checkDatumEind()
	{
		if ($ret = parent::checkDatumEind())
			return $ret;
		if ($this->getDatumEind() < $this->getDatumBegin())
			return _('Einddatum moet later zijn dan begindatum');
	}
	public function setURL($url)
	{
		$url = preg_replace('@^(https?://)?(www.|secure.)?a-es(2|kwadraat).nl/?@', '/', $url);
		parent::setURL($url);
	}
	public function opslaan($classname = 'Mededeling')
	{
		//Is het object nieuw en moet het nog vertaald worden?
		if(stripos($this->getMededeling('en'), "[Translation in progress]") !== false && !$this->getInDB())
			$stuurMail = true;
		else
			$stuurMail = false;
		//Eerst opslaan, anders kunnen we geen URL maken
		parent::opslaan($classname);
		if($stuurMail)
		{
			if($cie = $this->getCommissie())
			{
				$naam = $cie;
				$mail = $cie->getEmail();
			}
			else
			{
				$pers = Persoon::geef($this->getGewijzigdWie());
				$naam = PersoonView::waardeNaam($pers);
				$mail = $pers->getEmail();
			}
			$mail = "Lieve " . $naam . ",\r\n\r\nEr is een nieuw item aangemaakt dat vertaald dient te worden:\r\n\r\n" . HTTPS_ROOT . $this->url() . "\r\n\r\nGroetjes van de WebCie";
			sendmail("www", $cie->getEmail(), "Nieuw item om te vertalen", $mail);
			Page::addMelding(_("Je hebt nog geen Engelse omschrijving gegeven voor het item! Vergeet niet om dit ook nog te doen!"));
		}
	}
	
	/**
	 * Returneert of  het ingelogd lid het recht heeft om deze mededeling te wijzigen.
	 */
	public function magWijzigen ()
	{
		$cie = $this->getCommissie();
		return Mededeling::magKlasseWijzigen() || ($cie && $cie->hasLid(Persoon::getIngelogd()));
	}
	public function magVerwijderen ()
	{
		$cie = $this->getCommissie();
		if ($cie && $cie->hasLid(Persoon::getIngelogd()))
			return true;
		return hasAuth('promocie');
	}

	public function magBekijken()
	{
		switch($this->getDoelgroep())
		{
				case 'ALLEN':
					return true;
				case 'LEDEN':
					return hasAuth('lid');
				case 'ACTIEF':
					return hasAuth('actief');
				default:
					return false;
		}
	}

	public function url()
	{
		return '/Vereniging/Nieuws/' . $this->geefID();
	}

	protected static function veldenMededeling ($velden = null)
	{
		switch ($velden) {
		case 'nieuw':
			$ret = parent::veldenMededeling('set');
			break;
		case 'viewInfo':
			$ret = array('Commissie', 'Doelgroep', 'Prioriteit', 'DatumBegin', 'DatumEind', 'Url');
			break;
		default:
			$ret = parent::veldenMededeling($velden);
		}
		return $ret;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
