<?
/**
 * $Id$
 */
class BugCategorieVerzameling
	extends BugCategorieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BugCategorieVerzameling_Generated
	}

	/**
	 *  Zoek alle Commissies op die een of meer BugCategorieen hebben.
	 *
	 * @return Een CommissieVerzameling.
	 */
	static public function geefAlleBugCommissies ()
	{
		return CommissieVerzameling::bugCommissies(False);
	}

	/**
	 *  Zoek alle Commissies op die een of meer actieve BugCategorieen hebben.
	 *
	 * @return Een CommissieVerzameling.
	 */
	static public function geefActieveBugCommissies ()
	{
		return CommissieVerzameling::bugCommissies(True);
	}

	/**
	 *  Zoek alle BugCategorieen op die bij een Commissie horen.
	 *
	 * @param commissie een Commissie.
	 * @param ookInactieve indien True, zoek ook BugCategorieen die niet actief zijn.
	 * @return een BugCategorieVerzameling met BugCategorieen die bij commissie horen.
	 */
	static public function geefVanCommissie (Commissie $commissie, $ookInactieve = False)
	{
		$query = BugCategorieQuery::table()
			->whereProp('Commissie', $commissie)
			->orderByAsc('naam');

		if(!$ookInactieve) {
			$query->whereProp('actief', true);
		}

		return $query->verzamel();
	}

	/**
	 * Bepaal het aantal inactieve BugCategorieen die bij een Commissie horen.
	 *
	 * @param commissie Een Commissie.
	 * @return Het aantal inactieve categorieen.
	 */
	static public function geefAantalInactieveVanCommissie (Commissie $commissie)
	{
		return BugCategorieQuery::table()
			->whereProp('Commissie', $commissie)
			->whereProp('actief', true)
			->count();
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
