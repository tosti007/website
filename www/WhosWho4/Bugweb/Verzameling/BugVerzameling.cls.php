<?
/**
 * $Id$
 */
class BugVerzameling
	extends BugVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BugVerzameling_Generated
	}

	public static function filterOpQuery($query) {
		$pers = Persoon::getIngelogd();
		if (!$pers)
			return $query->whereProp('Bug.niveau', 'OPEN');

		return $query->where(function($q) use ($pers) {
			$q->whereProp('Bug.niveau', 'OPEN')->orWhereProp('Bug.niveau', 'INGELOGD');
			if (hasAuth('bestuur')) {
				$q->orWhereProp('Bug.niveau', 'CIE');
			} else {
				$cies = CommissieVerzameling::vanPersoon($pers, false, true);
				if (!empty($cies)) {
					$q->orWhere(function($q2) use ($cies) {
						$q2->whereProp('Bug.niveau', 'CIE')->whereInProp('Commissie.commissieID', $cies);
					});
				}
			}
		});
	}

	static public function queryFilter()
	{
		$filter = ' AND (`Bug`.`niveau` = "OPEN" ';
		$cies = array();
		if($pers = Persoon::getIngelogd()) {
			$filter .= ' OR `Bug`.`niveau` = "INGELOGD" OR `Bug`.`melder_contactID` = "' . $pers->getContactID() . '"';
			if(hasAuth('bestuur')) {
				$filter .= ' OR `Bug`.`niveau` = "CIE" %_)';
			} else {
				$cies = CommissieVerzameling::vanPersoon($pers, false, true);
				if(sizeof($cies) != 0) {
					$filter .= ' OR (`Bug`.`niveau` = "CIE" AND `Commissie`.`commissieID` IN (%Ai))) ';
				} else {
					$filter .= ' %_) ';
				}
			}
		} else {
			$filter .= ' %_) ';
		}

		return $filter;
	}

	/**
	 *  Zoek Bug-objecten die aan bepaalde criteria voldoen.
	 *
	 * In het algemeen geldt dat elke parameter die NULL of leeg is niet wordt meegenomen in de zoekopdracht.
	 *
	 * @param zoekterm Een string waarop gezocht wordt in BugBerichten.
	 * @param categorieen Een BugCategorieVerzameling. Indien niet NULL wordt alleen op Bugs
	 * gezocht die bij deze categorieen horen.
	 * @param commissie Een Commissie. Indien niet NULL wordt alleen op Bugs 
	 * gezocht die bij een BugCategorie horen van deze Commissie. Indien 
	 * categorie niet NULL is wordt deze parameter genegeerd.
	 * @param status Een status of statusset. Indien niet NULL wordt alleen op 
	 * Bugs gezocht die deze status of een status in deze statusset hebben.
	 * @param prioriteiten Alle prioriteiten waarop gezocht wordt. Zie DBObject/Bug::prioriteiten
	 * @param offset De offset voor welke bugs opgehaald moeten worden uit de db
	 * @param tellen Een bool of er geteld moet worden hoeveel bugs het zijn
	 * ipv dat er een verzameling gereturnd wordt.
	 * @return Een BugVerzameling.
	 * @see Bug::geefStatusSet
	 */
	static public function zoeken ($zoekterm, BugCategorieVerzameling $categorieen = NULL, Commissie $commissie = NULL, $status = NULL, $prioriteiten = array(), $offset = null, $tellen = false)
	{
		// $categorieen overridet $commissie indien meegegeven
		$commissieID = ($commissie && (!$categorieen || $categorieen->aantal() == 0)) ? $commissie->geefID() : NULL;

		$statusSet = Bug::geefStatusSet($status);

        if($zoekterm && is_numeric($zoekterm))
            $zoektermid = (int)$zoekterm;
        else
            $zoektermid = null;

		$q = BugQuery::table()
			->join('BugBericht', 'BugBericht.bug_bugID', 'Bug.bugID')
			->join('BugCategorie', 'Bug.categorie_bugCategorieID', 'BugCategorie.bugCategorieID')
			->join('Commissie', 'Commissie.commissieID', 'BugCategorie.commissie_commissieID')
			->orderByDesc('BugBericht.bug_bugID');
		$q = self::filterOpQuery($q);
		if ($zoekterm) {
			$zoekterm = '%' . str_replace('%', '\%', $zoekterm) . '%';
			$q->where(function ($q2) use ($zoekterm, $zoektermid) {
				$q2->whereLikeProp('BugBericht.bericht', $zoekterm)
					->orWhereLikeProp('Bug.titel', $zoekterm)
					->orWhereProp('Bug.bugID', $zoektermid);
			});
		}
		if ($categorieen)
			$q->whereInProp('BugCategorie.bugCategorieID', $categorieen->keys());
		if ($commissieID)
			$q->whereProp('Commissie.commissieID', $commissieID);
		if ($statusSet)
			$q->whereInProp('Bug.status', $statusSet);
		if ($prioriteiten)
			$q->whereInProp('Bug.prioriteit', $prioriteiten);

		if ($tellen) {
			return $q->aggregate('COUNT', 'DISTINCT Bug.bugID');
		} else {
			$q->limit(BUGWEB_BUGS_BIJ_ZOEKEN);
			if ($offset) $q->offset($offset);
			return $q->verzamel();
		}
	}

	/**
	 *  Zoek Bugs die het laatst gemeld zijn.
	 *
	 * @param aantal Het maximale aantal Bugs.
	 * @param alleenOpen Indien True, zoek alleen open Bugs.
	 * @return Een BugVerzameling die de laatste aantal gemelde (al dan niet 
	 * open) Bugs bevat.
	 */
	static public function laatsteGemeld ($aantal = 20, $alleenOpen = False)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Bug`.`bugID`
			FROM `Bug`
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE 1=1 ' .
			($alleenOpen ? ' AND `Bug`.`status` IN (%As) ' : ' %_ ') . 
			self::queryFilter() . '
			ORDER BY `Bug`.`moment` DESC
			LIMIT %i',
			Bug::geefStatusSet('OPENS'),
			(Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()),
			$aantal);

		return self::verzamel($ids);
	}

	/**
	 *  Zoek Bugs die het laatst gewijzigd zijn.
	 *
	 * @param aantal Het maximale aantal Bugs.
	 * @param alleenOpen Indien True, zoek alleen open Bugs.
	 * @return Een BugVerzameling die de laatste aantal gewijzigde (al dan niet 
	 * open) Bugs bevat.
	 */
	static public function laatsteGewijzigd ($aantal = 20, $alleenOpen = False)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Bug`.`bugID`
			FROM `Bug`
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE 1=1 ' .
			($alleenOpen ? ' AND `Bug`.`status` IN (%As) ' : ' %_ ') . 
			self::queryFilter() . '
			ORDER BY `Bug`.`gewijzigdWanneer` DESC
			LIMIT %i',
			Bug::geefStatusSet('OPENS'), 
			(Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()),
			$aantal);

		return self::verzamel($ids);
	}

	/**
	 *  Zoek alle Bugs die in een bepaalde BugCategorie zitten.
	 *
	 * @param categorie een BugCategorie.
	 * @param alleenOpen indien True, zoek alleen open Bugs.
	 * @return een BugVerzameling die alle (al dan niet open) Bugs in categorie 
	 * bevat.
	 */
	static public function geefInCategorie (BugCategorie $categorie, $alleenOpen = False)
	{
		global $WSW4DB;
		$id = $categorie->geefID();

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Bug`.`bugID`
			FROM `Bug`
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE `Bug`.`categorie_bugCategorieID` = %i ' .
			self::queryFilter() .
			($alleenOpen ? ' AND `Bug`.`status` IN (%As) ' : ' %_ ') .
			' ORDER BY `Bug`.`gewijzigdWanneer` DESC',
			$id,
			(Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()),
			Bug::geefStatusSet('OPENS'));

		return self::verzamel($ids);
	}

	/**
	 *  Zoek alle Bugs die door een bepaalde Persoon gemeld zijn.
	 *
	 * @param persoon Een Persoon.
	 * @param alleenOpen Indien True, zoek alleen open Bugs.
	 * @return Een BugVerzameling die alle (al dan niet open) Bugs bevat die 
	 * door persoon gemeld zijn.
	 */
	static public function geefGemeld (Persoon $persoon, $alleenOpen = False)
	{
		global $WSW4DB;
		$id = $persoon->geefID();

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Bug`.`bugID`
			FROM `Bug`
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE `Bug`.`melder_contactID` = %i '
			. self::queryFilter()
			. ($alleenOpen ? ' AND `Bug`.`status` IN (%As) ' : ' %_ ') .
			' ORDER BY `Bug`.`gewijzigdWanneer` DESC',
			$id,
			(Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()),
			Bug::geefStatusSet('OPENS'));

		return self::verzamel($ids);
	}

	/**
	 *  Zoek alle Bugs die op dit moment aan een bepaalde Persoon 
	 * toegewezen zijn.
	 *
	 * @param persoon Een Persoon.
	 * @param alleenOpen Indien True, zoek alleen open Bugs.
	 * @return Een BugVerzameling die alle (al dan niet open) Bugs bevat die 
	 * aan persoon toegewezen zijn.
	 */
	static public function geefToegewezen (Persoon $persoon, $alleenOpen = False)
	{
		global $WSW4DB;
		$id = $persoon->geefID();

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `BugToewijzing`.`bug_bugID`
			FROM `BugToewijzing`
			JOIN `Bug` ON `BugToewijzing`.`bug_bugID` = `Bug`.`bugID`
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE `BugToewijzing`.`persoon_contactID` = %i '
			. self::queryFilter() . '
			AND `BugToewijzing`.`eind_bugBerichtID` IS NULL'
			. ($alleenOpen ? ' AND `Bug`.`status` IN (%As) ' : ' %_ ') . '
			ORDER BY `BugToewijzing`.`gewijzigdWanneer` DESC',
			$id,
			(Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()),
			Bug::geefStatusSet('OPENS'));

		return self::verzamel($ids);
	}

	/**
	 *  Zoek alle Bugs die door een bepaalde Persoon gevolgd worden.
	 *
	 * @param persoon Een Persoon.
	 * @param alleenOpen Indien True, zoek alleen open Bugs.
	 * @return Een BugVerzameling die alle (al dan niet open) Bugs bevat die 
	 * door persoon gevolgd worden.
	 */
	static public function geefGevolgd (Persoon $persoon, $alleenOpen = False)
	{
		global $WSW4DB;
		$id = $persoon->geefID();

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `BugVolger`.`bug_bugID`
			FROM `BugVolger`
			JOIN `Bug` ON `BugVolger`.`bug_bugID` = `Bug`.`bugID`
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE `BugVolger`.`persoon_contactID` = %i '
			. self::queryFilter()
			. ($alleenOpen ? ' AND `Bug`.`status` IN (%As) ' : ' %_ ') . '
			ORDER BY `BugVolger`.`gewijzigdWanneer` DESC',
			$id,
			(Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()),
			Bug::geefStatusSet('OPENS'));

		return self::verzamel($ids);
	}

    /**
     *  Geeft alle melders van bugs met hoeveel ze gemeld hebben
     */
	static public function geefMeldersMetAantal($amount = 10)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `melder_contactID`,COUNT(`bugID`) as aantal '
			.'FROM `Bug` '
			.'GROUP BY `melder_contactID`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id["melder_contactID"]] = $id["aantal"];
		}
		arsort($ret);
		$ret = array_slice($ret, 0, $amount, true);

		return $ret;
	}
	
	/**
	 *  Geeft alle statussen van bugs met hoeveel daarvan zijn
	 */
	static public function geefStatussenMetAantal()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `status`,COUNT(`bugID`) as aantal FROM `Bug` GROUP BY `status`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id["status"]] = $id["aantal"];
		}

		return $ret;
	}
	
	/**
	 *  Geeft alle prioriteiten van bugs met hoeveel daarvan zijn
	 */
	static public function geefPrioriteitenMetAantal()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `prioriteit`,COUNT(`bugID`) as aantal FROM `Bug` GROUP BY `prioriteit`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id["prioriteit"]] = $id["aantal"];
		}

		return $ret;
	}

	/**
	 *  Geeft de oudste bugs die nog steeds niet opgelost zijn
	 */
	static public function geefNogNietOpgelost($amount = 10)
	{
		global $WSW4DB, $bugStatusSets;

		$ids = $WSW4DB->q('COLUMN SELECT `bugID`
			FROM `Bug` 
			JOIN `BugCategorie` ON `Bug`.`categorie_bugCategorieID` = `BugCategorie`.`bugCategorieID`
			JOIN `Commissie` ON `BugCategorie`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE `status` IN (%As) '
			. self::queryFilter() . '
			ORDER BY `moment`'
			, Bug::geefStatusSet('OPENS')
			, (Persoon::getIngelogd() ? CommissieVerzameling::vanPersoon(Persoon::getIngelogd(), false, true) : array()));

		$ret = array_slice($ids, 0, $amount);

		return self::verzamel($ret);
	}

	/**
	 *  Geeft de bugs die het meest gevolgd worden
	 *
	 * @param amount De hoeveelheid bugs die gereturnd moet worden
	 * @return Een sorted array met als keys de bugids en als
	 * value door hoeveel personen de bug gevolgd wordt
	 */
	static public function geefGevolgdMetAantal($amount = 10)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `bug_bugID`,COUNT(`persoon_contactID`) as aantal '
			.'FROM `BugVolger` '
			.'GROUP BY `bug_bugID`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id['bug_bugID']] = $id['aantal'];
		}
		arsort($ret);
		$ret = array_slice($ret, 0, $amount, true);

		return $ret;
	}

	/**
	 *  Geeft de bugs waar het meest op gereageerd is
	 *
	 * @param amount De hoeveelheid bugs die gereturnd moet worden
	 * @return Een sorted array met als keys de bugids en als
	 * value hoevaak er een reactie bij de bug geplaatst is
	 */
	static public function geefGereageerdMetAantal($amount = 10)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `bug_bugID`,COUNT(`melder_contactID`) as aantal '
			.'FROM `BugBericht` '
			.'GROUP BY `bug_bugID`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id['bug_bugID']] = $id['aantal'];
		}
		arsort($ret);
		$ret = array_slice($ret, 0, $amount, true);

		return $ret;
	}

	/**
	 *  Vergelijk twee Bugs, geef -1 als de eerste eerder gemeld is, 0 als gelijk, 1 als later
	 */
	static public function sorteerOpLaatstGemeld($a, $b) {
		$a = Bug::geef($a)->getMoment();
		$b = Bug::geef($b)->getMoment();
		if ($a > $b) return -1;
		if ($a < $b) return 1;
		return 0;
	}
	/**
	 *  Vergelijk twee Bugs, geef -1 als de eerste eerder gewijzigd is, 0 als gelijk, 1 als later
	 */
	static public function sorteerOpLaatstGewijzigd($a, $b) {
		$a = Bug::geef($a)->getGewijzigdWanneer();
		$b = Bug::geef($b)->getGewijzigdWanneer();
		if ($a > $b) return -1;
		if ($a < $b) return 1;
		return 0;
	}
	/**
	 *  Vergelijk twee Bugs, geef -1 als de eerste urgenter is, 0 als gelijk, 1 als onbenulliger
	 */
	static public function sorteerOpPrioriteit($a, $b) {
		$a = Bug::geef($a)->getPrioriteitInt();
		$b = Bug::geef($b)->getPrioriteitInt();
		if ($a > $b) return -1;
		if ($a < $b) return 1;
		return 0;
	}

	static public function geefOpgelosteBugsVanPersoon($persoon)
	{
		return BugToewijzingQuery::table()
			->join('Bug')
			->whereProp('BugToewijzing.persoon', $persoon)
			->where(function($q) {
				$q->whereProp('Bug.status', 'OPGELOST')
				  ->orWhereProp('Bug.status', 'GECOMMIT');
			})
			->verzamel('Bug', true);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
