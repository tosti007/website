<?
abstract class BugVolgerVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BugVolgerVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BugVolgerVerzameling een BugVerzameling.
	 *
	 * @return BugVerzameling
	 * Een BugVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze BugVolgerVerzameling.
	 */
	public function toBugVerzameling()
	{
		if($this->aantal() == 0)
			return new BugVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBugBugID()
			                      );
		}
		$this->positie = $origPositie;
		return BugVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugVolgerVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BugVolgerVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BugVolgerVerzameling van Bug.
	 *
	 * @return BugVolgerVerzameling
	 * Een BugVolgerVerzameling die elementen bevat die bij de Bug hoort.
	 */
	static public function fromBug($bug)
	{
		if(!isset($bug))
			return new BugVolgerVerzameling();

		return BugVolgerQuery::table()
			->whereProp('Bug', $bug)
			->verzamel();
	}
	/**
	 * @brief Maak een BugVolgerVerzameling van Persoon.
	 *
	 * @return BugVolgerVerzameling
	 * Een BugVolgerVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new BugVolgerVerzameling();

		return BugVolgerQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
