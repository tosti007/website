<?
abstract class BugBerichtVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BugBerichtVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BugBerichtVerzameling een BugVerzameling.
	 *
	 * @return BugVerzameling
	 * Een BugVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze BugBerichtVerzameling.
	 */
	public function toBugVerzameling()
	{
		if($this->aantal() == 0)
			return new BugVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBugBugID()
			                      );
		}
		$this->positie = $origPositie;
		return BugVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugBerichtVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BugBerichtVerzameling.
	 */
	public function toMelderVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getMelderContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getMelderContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BugBerichtVerzameling een BugCategorieVerzameling.
	 *
	 * @return BugCategorieVerzameling
	 * Een BugCategorieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze BugBerichtVerzameling.
	 */
	public function toCategorieVerzameling()
	{
		if($this->aantal() == 0)
			return new BugCategorieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCategorieBugCategorieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCategorieBugCategorieID()
			                      );
		}
		$this->positie = $origPositie;
		return BugCategorieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BugBerichtVerzameling van Bug.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling die elementen bevat die bij de Bug hoort.
	 */
	static public function fromBug($bug)
	{
		if(!isset($bug))
			return new BugBerichtVerzameling();

		return BugBerichtQuery::table()
			->whereProp('Bug', $bug)
			->verzamel();
	}
	/**
	 * @brief Maak een BugBerichtVerzameling van Melder.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling die elementen bevat die bij de Melder hoort.
	 */
	static public function fromMelder($melder)
	{
		if(!isset($melder))
			return new BugBerichtVerzameling();

		return BugBerichtQuery::table()
			->whereProp('Melder', $melder)
			->verzamel();
	}
	/**
	 * @brief Maak een BugBerichtVerzameling van Categorie.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling die elementen bevat die bij de Categorie hoort.
	 */
	static public function fromCategorie($categorie)
	{
		if(!isset($categorie))
			return new BugBerichtVerzameling();

		return BugBerichtQuery::table()
			->whereProp('Categorie', $categorie)
			->verzamel();
	}
}
