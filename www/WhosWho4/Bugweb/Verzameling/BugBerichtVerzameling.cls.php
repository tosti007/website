<?
/**
 * $Id$
 */
class BugBerichtVerzameling
	extends BugBerichtVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BugBerichtVerzameling_Generated
	}

	/**
	 *  Zoek alle berichten bij een Bug.
	 *
	 * @param bug een Bug.
	 * @return een BugBerichtVerzameling met alle BugBerichten die bij bug horen.
	 */
	static public function geefVanBug (Bug $bug)
	{
		return BugBerichtQuery::table()
			->whereProp('Bug', $bug)
			->orderByAsc('moment')
			->verzamel();
	}

	/**
	 *  Zoek alle berichten bij een Bug, behalve de eerste.
	 *
	 * @param bug een Bug.
	 * @return een BugBerichtVerzameling met alle BugBerichten die bij bug 
	 * horen behalve chronologisch de eerste.
	 */
	static public function geefReacties (Bug $bug)
	{
		return BugBerichtQuery::table()
			->whereProp('Bug', $bug)
			->orderByAsc('moment')
			->offset(1)
			->limit(100000)
			->verzamel();
	}

	/**
	 *  Geeft alle commentators van bugs met hoeveel ze gereageerd hebben
	 *
	 * @param amount De hoeveelheid die teruggegeven moet worden
	 * @return Een sorted array met als keys het lidid en als value het aantal reacties
	 */
	static public function geefCommentatorsMetAantal($amount = 10)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `melder_contactID`,COUNT(`bugBerichtID`) as aantal '
			.'FROM `BugBericht` '
			.'GROUP BY `melder_contactID`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id["melder_contactID"]] = $id["aantal"];
		}
		arsort($ret);
		$ret = array_slice($ret, 0, $amount, true);

		return $ret;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
