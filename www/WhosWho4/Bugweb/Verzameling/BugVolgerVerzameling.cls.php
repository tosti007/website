<?
/**
 * $Id$
 */
class BugVolgerVerzameling
	extends BugVolgerVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BugVolgerVerzameling_Generated
	}

	/**
	 *  Functie om de top volgers met aantal te geven
	 */
	static public function geefVolgersMetAantal($amount = 10)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('TABLE SELECT `persoon_contactID`,COUNT(`bug_bugID`) as aantal '
			.'FROM `BugVolger` '
			.'GROUP BY `persoon_contactID`');

		$ret = array();
		foreach($ids as $id) {
			$ret[$id["persoon_contactID"]] = $id["aantal"];
		}
		arsort($ret);
		$ret = array_slice($ret, 0, $amount, true);

		return $ret;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
