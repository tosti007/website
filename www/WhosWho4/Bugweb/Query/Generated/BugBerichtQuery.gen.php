<?
abstract class BugBerichtQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de BugBerichtQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een BugBerichtQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw BugBerichtQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return BugBerichtQuery
	 * Het BugBerichtQuery-object waarvan meteen de db-table van het BugBericht-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new BugBerichtQuery();

		$query->tables('BugBericht');

		return $query;
	}

	/**
	 * @brief Maakt een BugBerichtVerzameling aan van de objecten die geselecteerd
	 * worden door de BugBerichtQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling verkregen door de query
	 */
	public function verzamel($object = 'BugBericht', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een BugBericht-iterator aan van de objecten die geselecteerd worden
	 * door de BugBerichtQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een BugBerichtVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'BugBericht', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een BugBericht die geslecteeerd wordt door de BugBerichtQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return BugBerichtVerzameling
	 * Een BugBerichtVerzameling verkregen door de query.
	 */
	public function geef($object = 'BugBericht')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van BugBericht.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'bugBerichtID',
			'bug_bugID',
			'melder_contactID',
			'melderEmail',
			'titel',
			'categorie_bugCategorieID',
			'status',
			'prioriteit',
			'niveau',
			'bericht',
			'moment',
			'revisie',
			'level',
			'commit',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'BugBericht';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als BugBericht een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan BugBericht een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('BugBericht'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Bug' => array(
				'bug_bugID' => 'bugID'
			),
			'Persoon' => array(
				'melder_contactID' => 'contactID'
			),
			'BugCategorie' => array(
				'categorie_bugCategorieID' => 'bugCategorieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van BugBericht.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'BugBericht.bugBerichtID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'bugberichtid':
			$type = 'int';
			$field = 'bugBerichtID';
			break;
		case 'bug':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Bug; })))
					user_error('Alle waarden in de array moeten van type Bug zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Bug) && !($value instanceof BugVerzameling))
				user_error('Value moet van type Bug zijn', E_USER_ERROR);
			$field = 'bug_bugID';
			if(is_array($value) || $value instanceof BugVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getBugID();
				$value = $new_value;
			}
			else $value = $value->getBugID();
			$type = 'int';
			break;
		case 'melder':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling) && !is_null($value))
				user_error('Value moet van type Persoon of NULL zijn', E_USER_ERROR);
			$field = 'melder_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'melderemail':
			$type = 'string';
			$field = 'melderEmail';
			break;
		case 'titel':
			$type = 'string';
			$field = 'titel';
			break;
		case 'categorie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof BugCategorie; })))
					user_error('Alle waarden in de array moeten van type BugCategorie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof BugCategorie) && !($value instanceof BugCategorieVerzameling) && !is_null($value))
				user_error('Value moet van type BugCategorie of NULL zijn', E_USER_ERROR);
			$field = 'categorie_bugCategorieID';
			if(is_array($value) || $value instanceof BugCategorieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getBugCategorieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getBugCategorieID();
			$type = 'int';
			break;
		case 'status':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, BugBericht::enumsStatus()))
						user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
				}
			}
			else if(!in_array($value, BugBericht::enumsStatus()))
				user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
			$type = 'string';
			$field = 'status';
			break;
		case 'prioriteit':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, BugBericht::enumsPrioriteit()))
						user_error($value . ' is niet een geldige waarde voor prioriteit', E_USER_ERROR);
				}
			}
			else if(!in_array($value, BugBericht::enumsPrioriteit()))
				user_error($value . ' is niet een geldige waarde voor prioriteit', E_USER_ERROR);
			$type = 'string';
			$field = 'prioriteit';
			break;
		case 'niveau':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, BugBericht::enumsNiveau()))
						user_error($value . ' is niet een geldige waarde voor niveau', E_USER_ERROR);
				}
			}
			else if(!in_array($value, BugBericht::enumsNiveau()))
				user_error($value . ' is niet een geldige waarde voor niveau', E_USER_ERROR);
			$type = 'string';
			$field = 'niveau';
			break;
		case 'bericht':
			$type = 'string';
			$field = 'bericht';
			break;
		case 'moment':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'moment';
			break;
		case 'revisie':
			$type = 'int';
			$field = 'revisie';
			break;
		case 'level':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, BugBericht::enumsLevel()))
						user_error($value . ' is niet een geldige waarde voor level', E_USER_ERROR);
				}
			}
			else if(!in_array($value, BugBericht::enumsLevel()))
				user_error($value . ' is niet een geldige waarde voor level', E_USER_ERROR);
			$type = 'string';
			$field = 'level';
			break;
		case 'commit':
			$type = 'string';
			$field = 'commit';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'BugBericht.'.$v;
			}
		} else {
			$field = 'BugBericht.'.$field;
		}

		return array($field, $value, $type);
	}

}
