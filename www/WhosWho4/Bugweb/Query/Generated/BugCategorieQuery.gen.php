<?
abstract class BugCategorieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de BugCategorieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een BugCategorieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw BugCategorieQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return BugCategorieQuery
	 * Het BugCategorieQuery-object waarvan meteen de db-table van het
	 * BugCategorie-object als table geset is.
	 */
	static public function table()
	{
		$query = new BugCategorieQuery();

		$query->tables('BugCategorie');

		return $query;
	}

	/**
	 * @brief Maakt een BugCategorieVerzameling aan van de objecten die geselecteerd
	 * worden door de BugCategorieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return BugCategorieVerzameling
	 * Een BugCategorieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'BugCategorie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een BugCategorie-iterator aan van de objecten die geselecteerd
	 * worden door de BugCategorieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een BugCategorieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'BugCategorie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een BugCategorie die geslecteeerd wordt door de BugCategorieQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return BugCategorieVerzameling
	 * Een BugCategorieVerzameling verkregen door de query.
	 */
	public function geef($object = 'BugCategorie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van BugCategorie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'bugCategorieID',
			'naam',
			'commissie_commissieID',
			'actief',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'BugCategorie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als BugCategorie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan BugCategorie een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('BugCategorie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van BugCategorie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'BugCategorie.bugCategorieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'bugcategorieid':
			$type = 'int';
			$field = 'bugCategorieID';
			break;
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling))
				user_error('Value moet van type Commissie zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else $value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'actief':
			$type = 'int';
			$field = 'actief';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'BugCategorie.'.$v;
			}
		} else {
			$field = 'BugCategorie.'.$field;
		}

		return array($field, $value, $type);
	}

}
