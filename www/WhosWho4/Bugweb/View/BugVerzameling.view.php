<?
/**
 * $Id$
 */
abstract class BugVerzamelingView
	extends BugVerzamelingView_Generated
{
	private static function addTab(&$navtabs, &$tabpanes, $id, $title, $body, $count = NULL, $isActive = false)
	{
		if ($count !== NULL) {
			$title = array($title, new HtmlSpan($count, 'badge'));
		}
		$a = new HtmlAnchor("#tab-$id", $title);
		$a->setAttributes(array(
			'role' => 'tab',
			'data-toggle' => 'tab'
		));
		$li = new HtmlListItem($a, $isActive ? 'active' : NULL, NULL, false);
		$li->setAttribute('role', 'presentation');
		$navtabs->add($li);

		$div = new HtmlDiv($body, 'tab-pane' . ($isActive ? ' active' : ''), "tab-$id");
		$div->setAttribute('role', 'tabpanel');
		$tabpanes->add($div);
	}

	/**
	 *  Maak een persoonlijke Bugweb-homepage.
	 *
	 * @param persoon De persoon voor wie pagina persoonlijk is.
	 * @param geselecteerd De BugCategorie die geselecteerd is.
	 * @return Een HtmlDiv met Bug-tabellen: eentje met de bugs in geselecteerd 
	 * (tenzij NULL), een met laatst gemelde bugs, een met laatst 
	 * becommentarieerde bgus, een met aan persoon toegewezen bugs, een met 
	 * door persoon gemelde bugs en een met door persoon gevolgde bugs.
	 */
	static public function toonInfoPagina (Persoon $persoon = NULL, BugCategorie $geselecteerd = NULL, Commissie $geselecteerdCie = NULL)
	{
		$div = new HtmlDiv(array(
			$navtabs = new HtmlList(false, null, 'nav nav-tabs'),
			$tabpanes = new HtmlDiv(null, 'tab-content')
		));
		$navtabs->setAttribute('role', 'tablist');

		$closed = tryPar('closed', false);

		$cieform = false;
		if ($persoon && hasAuth('actief')) {
			$ciebox = CommissieVerzamelingView::maakCommissieBugSelectBox('commissieID', $geselecteerdCie);
			if ($ciebox) {
				$ciebox->setAutoSubmit(true);
				$ciebox->addClass('form-control');
				$cieform = new HtmlForm('GET');
				$cieform->addClass('form-group');
				$cieform->add(new HtmlDiv(array(
					new HtmlLabel(null, _('Commissie'), 'input-group-addon'),
					$ciebox
				), 'input-group'));
				// als er geen form is, is de hidden input ook niet nodig,
				// want er is niet nog een mogelijkheid om het form op te sturen
				if($closed && !is_null($cieform)) {
					$cieform->add(HtmlInput::makeHidden('closed', $closed));
				}
			}
		}
		$cieselected = (bool)$geselecteerdCie;

		if($geselecteerdCie) {
			$title = _('Commissie');
			$body = [$cieform];
			$count = NULL;

			if($geselecteerdCie) {
				$ciebugs = BugVerzameling::zoeken('', null, $geselecteerdCie, 'OPENS');
				$title = sprintf(_("Commissie: %s"), CommissieView::waardeNaam($geselecteerdCie)); 
				$body[] = self::tabel($ciebugs);
				$count = $ciebugs->aantal();
			}

			self::addTab($navtabs, $tabpanes, 'cie', $title, $body, $count, $cieselected);
		}

		$catform = false;
		if ($persoon && hasAuth('actief')) {
			$catbox = BugCategorieVerzamelingView::maakCiesComboBox('categorieID',
					$geselecteerd, false, $persoon->getCommissies());
			if ($catbox) {
				$catbox->setAutoSubmit(true);
				$catbox->addClass('form-control');
				$catform = new HtmlForm('GET');
				$catform->addClass('form-group');
				$catform->add(new HtmlDiv(array(
					new HtmlLabel(null, _('Categorie'), 'input-group-addon'),
					$catbox,
					HtmlAnchor::button(BUGWEBBASE . '/Categorie/Nieuw',
							HtmlSpan::fa('plus', 'nieuw'),
							null, 'input-group-addon', null, 'default'),
				), 'input-group'));
				// als er geen form is, is de hidden input ook niet nodig,
				// want er is niet nog een mogelijkheid om het form op te sturen
				if($closed && !is_null($catform)) {
					$catform->add(HtmlInput::makeHidden('closed', $closed));
				}
			}
		}
		$catselected = $catform && $geselecteerd && !$cieselected;

		if ($catform) {
			$title = _('Categorie');
			$body = array($catform);
			$count = NULL;
			if ($geselecteerd) {
				$catbugs = $geselecteerd->getBugs(!$closed);
				$title = sprintf(_("Categorie: %s"), BugCategorieView::waardeNaam($geselecteerd)); 
				$body[] = self::tabel($catbugs);
				$count = $catbugs->aantal();
			}
			self::addTab($navtabs, $tabpanes, 'cat', $title, $body, $count, $catselected);
		}

		$laatstGemeldeBugs = BugVerzameling::laatsteGemeld(BUGWEB_AANTAL_GEMELD, !$closed);
		$isMeer = $laatstGemeldeBugs->aantal() == BUGWEB_AANTAL_GEMELD;
		self::addTab($navtabs, $tabpanes, 'laatstgemeld', 'Laatst gemeld',
				self::tabel($laatstGemeldeBugs, true, "LaatstGemeld"),
				$laatstGemeldeBugs->aantal() . ($isMeer ? '+' : ''), !$cieselected && !$catselected);

		$laatstGewijzigdeBugs = BugVerzameling::laatsteGewijzigd(BUGWEB_AANTAL_GEMELD, !$closed);
		$isMeer = $laatstGewijzigdeBugs->aantal() == BUGWEB_AANTAL_GEMELD;
		self::addTab($navtabs, $tabpanes, 'laatstgewijzigd', 'Laatst gewijzigd',
				self::tabel($laatstGewijzigdeBugs, true, "LaatstGewijzigd"),
				$laatstGewijzigdeBugs->aantal() . ($isMeer ? '+' : ''));

		if ($persoon) {
			$toegewezenBugs = $persoon->getToegewezenBugs(!$closed);
			if ($toegewezenBugs->aantal() > 0)
				self::addTab($navtabs, $tabpanes, 'toegewezen', _('Aan mij toegewezen'),
						self::tabel($toegewezenBugs, true), $toegewezenBugs->aantal());

			$gemeldeBugs = $persoon->getGemeldeBugs(!$closed);
			if ($gemeldeBugs->aantal() > 0)
				self::addTab($navtabs, $tabpanes, 'gemeld', _('Door mij gemeld'),
						self::tabel($gemeldeBugs, true), $gemeldeBugs->aantal());

			$gevolgdeBugs = $persoon->getGevolgdeBugs(!$closed);
			if ($gevolgdeBugs->aantal() > 0)
				self::addTab($navtabs, $tabpanes, 'gevolgd', _('Door mij gevolgd'),
						self::tabel($gevolgdeBugs, true), $gevolgdeBugs->aantal());
        }

		$actions = new HtmlDiv(array(
			new HtmlDiv(array(
				HtmlAnchor::button(BUGWEBBASE . '/Melden', array(
					HtmlSpan::fa('plus', 'nieuw'),
					'&nbsp;' . _("Bug melden")
				)),
				HtmlAnchor::button(BUGWEBBASE . '/Willekeurig', array(
					HtmlSpan::fa('bug', 'bug'),
					'&nbsp;' . _("Willekeurige bug")
				))
			), 'btn-group'),
			$zoekform = new HtmlForm('GET', BUGWEBBASE . '/Zoeken'),
			$closedform = new HtmlForm('GET')
		), 'form-inline');

		$zoekform->addClass('input-group');
		$zoekform->add(new HtmlDiv(array(
			HtmlInput::makeText('zoekterm', null, null, 'form-control', null, _('Snel zoeken...')),
			new HtmlSpan(new HtmlButton('submit', HtmlSpan::fa('search', _('Snel zoeken...'))), 'input-group-btn')
		)));
		$closedform->addClass('btn-group');
		$closedform->setAttribute('style', 'margin-left: 1em;');
		$closedform->add(array(
			new HtmlButton('submit', 'Alleen open bugs', 'closed', '', 'btn btn-default' . ($closed ? '' : ' active')),
			new HtmlButton('submit', 'Alle bugs', 'closed', '1', 'btn btn-default' . ($closed ? ' active' : ''))
		));

		return array($actions, new HtmlHR(), $div);
	}

	/**
	 * Maak een formulier waarmee Bugs gezocht kunnen worden.
	 *
	 * @param zoekterm De ingevulde zoekterm.
	 * @param categorieen De geselecteerde BugCategorieen.
	 * @param commissie De geselecteerde Commissie. Wordt genegeerd indien 
	 * categorie niet NULL is.
	 * @param status De geselecteerde status.
	 * @param prioriteit De geselecteerde prioriteit.
	 * @return Een HtmlForm waarmee op bugs gezocht kan worden en waar de 
	 * waarden van de parameters in ingevuld staan.
	 */
	public static function zoekForm ($zoekterm = '', BugCategorieVerzameling $categorieen, Commissie $commissie = NULL, $status = NULL, $prioriteit = NULL)
	{
		$form = new HtmlForm('GET');

		$form->add(new HtmlDiv(array(
			new HtmlLabel('zoekterm', _('Zoekterm:'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText('zoekterm', $zoekterm, null, 'form-control', null, 'Zoek op titel of inhoud...'), 'col-sm-10')
		), 'form-group'));

		$catbox = BugCategorieVerzamelingView::maakCiesComboBox('categorie', $categorieen, false);
		$catbox->setMultiple();
		$ciebox = CommissieVerzamelingView::maakCommissieSelectbox('commissie', CommissieVerzameling::BugCommissies(true), $commissie, true);
		$form->add(new HtmlDiv(array(
			new HtmlLabel('categorie', _('Categorie:'), 'col-sm-2 control-label'),
			new HtmlDiv($catbox, 'col-sm-4'),
			new HtmlLabel('commissie', _('Commissie:'), 'col-sm-2 control-label'),
			new HtmlDiv($ciebox, 'col-sm-4')
		), 'form-group'));
		$catbox->setid('categorie');
		$ciebox->setid('commissie');

		$form->add(new HtmlDiv(array(
			new HtmlLabel('status', _('Status:'), 'col-sm-2 control-label'),
			new HtmlDiv(self::maakstatusselectbox('status', $status), 'col-sm-4'),
			new HtmlLabel('prioriteit', _('Prioriteit:'), 'col-sm-2 control-label'),
			new HtmlDiv(self::maakPrioriteitSelectbox('prioriteit', $prioriteit), 'col-sm-4')
		), 'form-group'));
		$form->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeSubmitButton(_('Zoeken')), 'col-sm-12 text-center'), 'form-group'));
		return $form;
	}

	/*
	 * Maak een resizable tabel van een BugVerzameling.
	 *
	 * @param bugs De BugVerzameling waarvan een tabel gemaakt wordt.
	 * @return Een HtmlResizableDiv met HtmlTable met en rij voor elke Bug in 
	 * bugs, of een gewone HtmlDiv indien bugs minder dan 6 elementen bevat.
	 */
	static public function resizableTabel (BugVerzameling $bugs, $gesorteerdOp = "Prioriteit")
	{
		$div = $bugs->aantal() > 6 ? new HtmlResizableDiv("100%", "180px") : new HtmlDiv();
		$div->add(self::tabel($bugs, false, $gesorteerdOp));
		return $div;
	}

	/*
	 * Maak een tabel van een BugVerzameling.
	 *
	 * @param bugs De BugVerzameling waarvan een tabel gemaakt wordt.
	 * @return Een HtmlTable met en rij voor elke Bug in bugs.
	 */
	static public function tabel (BugVerzameling $bugs, $groot = False, $gesorteerdOp = "Prioriteit")
	{
		if ($bugs->aantal() == 0)
		{
			return new HtmlEmphasis(_('Er zijn geen bugs gevonden.'));
		}
		$bugs->sorteer($gesorteerdOp);

		$table = new HtmlTable(NULL, 'bugs sortable table-non-striped');
		$thead = $table->addHead();
		$row = $thead->addRow();
		$row->addData(BugView::labelBugID(), 'bugID');
		$row->addData('', 'prioriteit');
		$row->addData('Gewijzigd');
		$row->addData(BugView::labelStatus(), 'status');
		$row->addData(BugView::labelLevel());
		if ($groot)
		{
			$row->addData(BugView::labelCategorie(), 'categorie');
		}
		$row->addData(BugView::labelTitel(), 'titel');

		$tbody = $table->addBody();
		foreach ($bugs as $bug)
		{
			$tbody->addImmutable(BugView::toTableRow($bug, $groot));
		}

		return $table;
	}

	/**
	 *  Maak een formulieronderdeel waarmee een bug-statusset kan worden 
	 * geselecteerd.
	 *
	 * @param naam De naam van het formulieronderdeel.
	 * @param geselecteerd De naam van de geselecteerde bug-statusset. Een van 
	 * 'OPENS', 'NIETBEZIG', 'FEEDBACK', 'GESLOTENS', 'UITGESTELD', 'OPGELOSTS' en 'ALLE'.
	 * @return Een HtmlSelectbox met als opties alle bug-statussets.
	 */
	static public function maakStatusSelectbox ($naam, $geselecteerd = NULL)
	{
		$opts = array(
			'' => '&lt;alle statussen&gt;',
			'OPENS' => _('Open'),
			'NIETBEZIG' => _('Nog niet mee bezig'),
			'BEZIG' => _('Wordt aan gewerkt'),
			'FEEDBACK' => _('Meer info nodig'),
			'WANNAHAVE' => _('Feature request'),
			'GESLOTENS' => _('Gesloten'),
			'OPGELOSTS' => _('Opgelost'),
			'UITGESTELD' => _('Uitgesteld'),
			'ALLE' => _('Alle bugs')
		);

		return HtmlSelectbox::fromArray($naam, $opts, $geselecteerd);
	}

	/**
	 *  Maak een formulieronderdeel waarmee een prioriteit kan worden 
	 * geselecteerd.
	 *
	 * @param naam De naam van het formulieronderdeel.
	 * @param geselecteerd De naam van de geselecteerde prioriteit.
	 * @return Een HtmlSelectbox met voor elke prioriteit de opties 'gelijk 
	 * aan', 'minstens' en 'hoogstens' die prioriteit.
	 */
	static public function maakPrioriteitSelectbox ($naam, $geselecteerd = NULL)
	{
		$prios = Bug::enumsPrioriteit();

		$opts = array('' => '&lt;alle prioriteiten&gt;');
		$subopts = array();
		for ($i = 0; $i < count($prios); $i++)
		{
			$prio = $prios[$i];
			$subopts['= ' . ($i+1)] = BugView::labelEnumPrioriteit($prio);
		}
		$opts[_('Gelijk aan')] = $subopts;

		$subopts = array();
		for ($i = 1; $i < count($prios); $i++)
		{
			$prio = $prios[$i];
			$subopts['>= ' . ($i+1)] = BugView::labelEnumPrioriteit($prio);
		}
		$opts[_('Minstens')] = $subopts;

		$subopts = array();
		for ($i = 0; $i < count($prios) - 1; $i++)
		{
			$prio = $prios[$i];
			$subopts['<= ' . ($i+1)] = BugView::labelEnumPrioriteit($prio);
		}
		$opts[_('Hoogstens')] = $subopts;

		return HtmlSelectbox::fromArray($naam, $opts, $geselecteerd);
	}

	/**
	 * Geeft een RSS-bestand van de open bugs
	 */
	static public function rss ($bugs)
	{
		$page = Page::getInstance('rss')
			->start(_("Bugs A–Eskwadraat"), _("Alle open bugs"))
			->add(BugVerzamelingView::rssItem($bugs))
			->end();
	}

	/**
	 * Geeft een array van RSS-items terug
	 */
	static public function rssItem (BugVerzameling $bugs)
	{
		$output = array();
		foreach ($bugs as $bug)
			$output[] = BugView::rssItem($bug);
		return $output;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
