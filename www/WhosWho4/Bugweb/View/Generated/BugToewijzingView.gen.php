<?
abstract class BugToewijzingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugToewijzingView.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBugToewijzing(BugToewijzing $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bugToewijzingID.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld bugToewijzingID labelt.
	 */
	public static function labelBugToewijzingID(BugToewijzing $obj)
	{
		return 'BugToewijzingID';
	}
	/**
	 * @brief Geef de waarde van het veld bugToewijzingID.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bugToewijzingID van het
	 * object obj representeert.
	 */
	public static function waardeBugToewijzingID(BugToewijzing $obj)
	{
		return static::defaultWaardeInt($obj, 'BugToewijzingID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bugToewijzingID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bugToewijzingID representeert.
	 */
	public static function opmerkingBugToewijzingID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bug.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld bug labelt.
	 */
	public static function labelBug(BugToewijzing $obj)
	{
		return 'Bug';
	}
	/**
	 * @brief Geef de waarde van het veld bug.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bug van het object obj
	 * representeert.
	 */
	public static function waardeBug(BugToewijzing $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBug())
			return NULL;
		return BugView::defaultWaardeBug($obj->getBug());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bug.
	 *
	 * @see genericFormbug
	 *
	 * @param BugToewijzing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bug staat en kan worden
	 * bewerkt. Indien bug read-only is betreft het een statisch html-element.
	 */
	public static function formBug(BugToewijzing $obj, $include_id = false)
	{
		return static::waardeBug($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bug. In tegenstelling
	 * tot formbug moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formbug
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bug staat en kan worden
	 * bewerkt. Indien bug read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBug($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bug
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bug representeert.
	 */
	public static function opmerkingBug()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(BugToewijzing $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(BugToewijzing $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param BugToewijzing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(BugToewijzing $obj, $include_id = false)
	{
		return static::waardePersoon($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld begin.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld begin labelt.
	 */
	public static function labelBegin(BugToewijzing $obj)
	{
		return 'Begin';
	}
	/**
	 * @brief Geef de waarde van het veld begin.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld begin van het object obj
	 * representeert.
	 */
	public static function waardeBegin(BugToewijzing $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBegin())
			return NULL;
		return BugBerichtView::defaultWaardeBugBericht($obj->getBegin());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld begin.
	 *
	 * @see genericFormbegin
	 *
	 * @param BugToewijzing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld begin staat en kan worden
	 * bewerkt. Indien begin read-only is betreft het een statisch html-element.
	 */
	public static function formBegin(BugToewijzing $obj, $include_id = false)
	{
		return static::waardeBegin($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld begin. In
	 * tegenstelling tot formbegin moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld begin staat en kan worden
	 * bewerkt. Indien begin read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBegin($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld begin
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld begin representeert.
	 */
	public static function opmerkingBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld eind.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld eind labelt.
	 */
	public static function labelEind(BugToewijzing $obj)
	{
		return 'Eind';
	}
	/**
	 * @brief Geef de waarde van het veld eind.
	 *
	 * @param BugToewijzing $obj Het BugToewijzing-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld eind van het object obj
	 * representeert.
	 */
	public static function waardeEind(BugToewijzing $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getEind())
			return NULL;
		return BugBerichtView::defaultWaardeBugBericht($obj->getEind());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld eind.
	 *
	 * @see genericFormeind
	 *
	 * @param BugToewijzing $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eind staat en kan worden
	 * bewerkt. Indien eind read-only is betreft het een statisch html-element.
	 */
	public static function formEind(BugToewijzing $obj, $include_id = false)
	{
		return BugBerichtView::defaultForm($obj->getEind());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld eind. In tegenstelling
	 * tot formeind moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formeind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eind staat en kan worden
	 * bewerkt. Indien eind read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormEind($name, $waarde=NULL)
	{
		return BugBerichtView::genericDefaultForm('Eind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld eind
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld eind representeert.
	 */
	public static function opmerkingEind()
	{
		return NULL;
	}
}
