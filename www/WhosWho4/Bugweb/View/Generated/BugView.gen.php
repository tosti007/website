<?
abstract class BugView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugView.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBug(Bug $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bugID.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bugID labelt.
	 */
	public static function labelBugID(Bug $obj)
	{
		return 'BugID';
	}
	/**
	 * @brief Geef de waarde van het veld bugID.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bugID van het object obj
	 * representeert.
	 */
	public static function waardeBugID(Bug $obj)
	{
		return static::defaultWaardeInt($obj, 'BugID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bugID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bugID representeert.
	 */
	public static function opmerkingBugID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld melder.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld melder labelt.
	 */
	public static function labelMelder(Bug $obj)
	{
		return 'Melder';
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld melder van het object obj
	 * representeert.
	 */
	public static function waardeMelder(Bug $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMelder())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getMelder());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld melder.
	 *
	 * @see genericFormmelder
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is betreft het een statisch html-element.
	 */
	public static function formMelder(Bug $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getMelder());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld melder. In
	 * tegenstelling tot formmelder moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmelder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMelder($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Melder');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld melder
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld melder representeert.
	 */
	public static function opmerkingMelder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld melderEmail.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld melderEmail labelt.
	 */
	public static function labelMelderEmail(Bug $obj)
	{
		return 'MelderEmail';
	}
	/**
	 * @brief Geef de waarde van het veld melderEmail.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld melderEmail van het object
	 * obj representeert.
	 */
	public static function waardeMelderEmail(Bug $obj)
	{
		return static::defaultWaardeString($obj, 'MelderEmail');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld melderEmail.
	 *
	 * @see genericFormmelderEmail
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melderEmail staat en kan
	 * worden bewerkt. Indien melderEmail read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMelderEmail(Bug $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'MelderEmail', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld melderEmail. In
	 * tegenstelling tot formmelderEmail moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmelderEmail
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melderEmail staat en kan
	 * worden bewerkt. Indien melderEmail read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMelderEmail($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'MelderEmail', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * melderEmail bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld melderEmail representeert.
	 */
	public static function opmerkingMelderEmail()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(Bug $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(Bug $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(Bug $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Titel', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Titel', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld categorie.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld categorie labelt.
	 */
	public static function labelCategorie(Bug $obj)
	{
		return 'Categorie';
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld categorie van het object obj
	 * representeert.
	 */
	public static function waardeCategorie(Bug $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCategorie())
			return NULL;
		return BugCategorieView::defaultWaardeBugCategorie($obj->getCategorie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld categorie.
	 *
	 * @see genericFormcategorie
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCategorie(Bug $obj, $include_id = false)
	{
		return BugCategorieView::defaultForm($obj->getCategorie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld categorie. In
	 * tegenstelling tot formcategorie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcategorie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCategorie($name, $waarde=NULL)
	{
		return BugCategorieView::genericDefaultForm('Categorie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * categorie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld categorie representeert.
	 */
	public static function opmerkingCategorie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(Bug $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(Bug::enumsStatus() as $id)
			$soorten[$id] = BugView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(Bug $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(Bug $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', Bug::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld prioriteit.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld prioriteit labelt.
	 */
	public static function labelPrioriteit(Bug $obj)
	{
		return 'Prioriteit';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld prioriteit.
	 *
	 * @param string $value Een enum-waarde van het veld prioriteit.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumPrioriteit($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld prioriteit horen.
	 *
	 * @see labelenumPrioriteit
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld prioriteit
	 * representeren.
	 */
	public static function labelenumPrioriteitArray()
	{
		$soorten = array();
		foreach(Bug::enumsPrioriteit() as $id)
			$soorten[$id] = BugView::labelenumPrioriteit($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld prioriteit.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld prioriteit van het object obj
	 * representeert.
	 */
	public static function waardePrioriteit(Bug $obj)
	{
		return static::defaultWaardeEnum($obj, 'Prioriteit');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld prioriteit.
	 *
	 * @see genericFormprioriteit
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prioriteit staat en kan
	 * worden bewerkt. Indien prioriteit read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPrioriteit(Bug $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Prioriteit', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld prioriteit. In
	 * tegenstelling tot formprioriteit moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formprioriteit
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prioriteit staat en kan
	 * worden bewerkt. Indien prioriteit read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPrioriteit($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Prioriteit', Bug::enumsprioriteit());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * prioriteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld prioriteit representeert.
	 */
	public static function opmerkingPrioriteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld niveau.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld niveau labelt.
	 */
	public static function labelNiveau(Bug $obj)
	{
		return 'Niveau';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld niveau.
	 *
	 * @param string $value Een enum-waarde van het veld niveau.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumNiveau($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld niveau horen.
	 *
	 * @see labelenumNiveau
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld niveau
	 * representeren.
	 */
	public static function labelenumNiveauArray()
	{
		$soorten = array();
		foreach(Bug::enumsNiveau() as $id)
			$soorten[$id] = BugView::labelenumNiveau($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld niveau.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld niveau van het object obj
	 * representeert.
	 */
	public static function waardeNiveau(Bug $obj)
	{
		return static::defaultWaardeEnum($obj, 'Niveau');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld niveau.
	 *
	 * @see genericFormniveau
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld niveau staat en kan worden
	 * bewerkt. Indien niveau read-only is betreft het een statisch html-element.
	 */
	public static function formNiveau(Bug $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Niveau', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld niveau. In
	 * tegenstelling tot formniveau moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formniveau
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld niveau staat en kan worden
	 * bewerkt. Indien niveau read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNiveau($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Niveau', Bug::enumsniveau());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld niveau
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld niveau representeert.
	 */
	public static function opmerkingNiveau()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld moment.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld moment labelt.
	 */
	public static function labelMoment(Bug $obj)
	{
		return 'Moment';
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld moment van het object obj
	 * representeert.
	 */
	public static function waardeMoment(Bug $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Moment');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld moment.
	 *
	 * @see genericFormmoment
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is betreft het een statisch html-element.
	 */
	public static function formMoment(Bug $obj, $include_id = false)
	{
		return static::waardeMoment($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld moment. In
	 * tegenstelling tot formmoment moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmoment
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMoment($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld moment
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld moment representeert.
	 */
	public static function opmerkingMoment()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld type.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld type labelt.
	 */
	public static function labelType(Bug $obj)
	{
		return 'Type';
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld type van het object obj
	 * representeert.
	 */
	public static function waardeType(Bug $obj)
	{
		return static::defaultWaardeString($obj, 'Type');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld type.
	 *
	 * @see genericFormtype
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is betreft het een statisch html-element.
	 */
	public static function formType(Bug $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Type', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld type. In tegenstelling
	 * tot formtype moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtype
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormType($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Type', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld type
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld type representeert.
	 */
	public static function opmerkingType()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld level.
	 *
	 * @param Bug $obj Het Bug-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld level labelt.
	 */
	public static function labelLevel(Bug $obj)
	{
		return 'Level';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld level.
	 *
	 * @param string $value Een enum-waarde van het veld level.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumLevel($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld level horen.
	 *
	 * @see labelenumLevel
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld level
	 * representeren.
	 */
	public static function labelenumLevelArray()
	{
		$soorten = array();
		foreach(Bug::enumsLevel() as $id)
			$soorten[$id] = BugView::labelenumLevel($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld level.
	 *
	 * @param Bug $obj Het Bug-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld level van het object obj
	 * representeert.
	 */
	public static function waardeLevel(Bug $obj)
	{
		return static::defaultWaardeEnum($obj, 'Level');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld level.
	 *
	 * @see genericFormlevel
	 *
	 * @param Bug $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld level staat en kan worden
	 * bewerkt. Indien level read-only is betreft het een statisch html-element.
	 */
	public static function formLevel(Bug $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Level', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld level. In
	 * tegenstelling tot formlevel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlevel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld level staat en kan worden
	 * bewerkt. Indien level read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLevel($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Level', Bug::enumslevel());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld level
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld level representeert.
	 */
	public static function opmerkingLevel()
	{
		return NULL;
	}
}
