<?
abstract class BugCategorieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugCategorieView.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBugCategorie(BugCategorie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bugCategorieID.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld bugCategorieID labelt.
	 */
	public static function labelBugCategorieID(BugCategorie $obj)
	{
		return 'BugCategorieID';
	}
	/**
	 * @brief Geef de waarde van het veld bugCategorieID.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bugCategorieID van het object
	 * obj representeert.
	 */
	public static function waardeBugCategorieID(BugCategorie $obj)
	{
		return static::defaultWaardeInt($obj, 'BugCategorieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bugCategorieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bugCategorieID representeert.
	 */
	public static function opmerkingBugCategorieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(BugCategorie $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(BugCategorie $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param BugCategorie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(BugCategorie $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(BugCategorie $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(BugCategorie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param BugCategorie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(BugCategorie $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld actief.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld actief labelt.
	 */
	public static function labelActief(BugCategorie $obj)
	{
		return 'Actief';
	}
	/**
	 * @brief Geef de waarde van het veld actief.
	 *
	 * @param BugCategorie $obj Het BugCategorie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld actief van het object obj
	 * representeert.
	 */
	public static function waardeActief(BugCategorie $obj)
	{
		return static::defaultWaardeBool($obj, 'Actief');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld actief.
	 *
	 * @see genericFormactief
	 *
	 * @param BugCategorie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actief staat en kan worden
	 * bewerkt. Indien actief read-only is betreft het een statisch html-element.
	 */
	public static function formActief(BugCategorie $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Actief', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld actief. In
	 * tegenstelling tot formactief moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formactief
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actief staat en kan worden
	 * bewerkt. Indien actief read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormActief($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Actief');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld actief
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld actief representeert.
	 */
	public static function opmerkingActief()
	{
		return NULL;
	}
}
