<?
abstract class BugCategorieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugCategorieVerzamelingView.
	 *
	 * @param BugCategorieVerzameling $obj Het BugCategorieVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBugCategorieVerzameling(BugCategorieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
