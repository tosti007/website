<?
abstract class BugBerichtView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BugBerichtView.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBugBericht(BugBericht $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bugBerichtID.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bugBerichtID labelt.
	 */
	public static function labelBugBerichtID(BugBericht $obj)
	{
		return 'BugBerichtID';
	}
	/**
	 * @brief Geef de waarde van het veld bugBerichtID.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bugBerichtID van het object
	 * obj representeert.
	 */
	public static function waardeBugBerichtID(BugBericht $obj)
	{
		return static::defaultWaardeInt($obj, 'BugBerichtID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bugBerichtID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bugBerichtID representeert.
	 */
	public static function opmerkingBugBerichtID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bug.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bug labelt.
	 */
	public static function labelBug(BugBericht $obj)
	{
		return 'Bug';
	}
	/**
	 * @brief Geef de waarde van het veld bug.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bug van het object obj
	 * representeert.
	 */
	public static function waardeBug(BugBericht $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBug())
			return NULL;
		return BugView::defaultWaardeBug($obj->getBug());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bug.
	 *
	 * @see genericFormbug
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bug staat en kan worden
	 * bewerkt. Indien bug read-only is betreft het een statisch html-element.
	 */
	public static function formBug(BugBericht $obj, $include_id = false)
	{
		return static::waardeBug($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bug. In tegenstelling
	 * tot formbug moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formbug
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bug staat en kan worden
	 * bewerkt. Indien bug read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBug($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bug
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bug representeert.
	 */
	public static function opmerkingBug()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld melder.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld melder labelt.
	 */
	public static function labelMelder(BugBericht $obj)
	{
		return 'Melder';
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld melder van het object obj
	 * representeert.
	 */
	public static function waardeMelder(BugBericht $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMelder())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getMelder());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld melder.
	 *
	 * @see genericFormmelder
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is betreft het een statisch html-element.
	 */
	public static function formMelder(BugBericht $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getMelder());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld melder. In
	 * tegenstelling tot formmelder moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmelder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melder staat en kan worden
	 * bewerkt. Indien melder read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMelder($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Melder');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld melder
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld melder representeert.
	 */
	public static function opmerkingMelder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld melderEmail.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld melderEmail labelt.
	 */
	public static function labelMelderEmail(BugBericht $obj)
	{
		return 'MelderEmail';
	}
	/**
	 * @brief Geef de waarde van het veld melderEmail.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld melderEmail van het object
	 * obj representeert.
	 */
	public static function waardeMelderEmail(BugBericht $obj)
	{
		return static::defaultWaardeString($obj, 'MelderEmail');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld melderEmail.
	 *
	 * @see genericFormmelderEmail
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melderEmail staat en kan
	 * worden bewerkt. Indien melderEmail read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMelderEmail(BugBericht $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'MelderEmail', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld melderEmail. In
	 * tegenstelling tot formmelderEmail moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmelderEmail
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld melderEmail staat en kan
	 * worden bewerkt. Indien melderEmail read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMelderEmail($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'MelderEmail', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * melderEmail bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld melderEmail representeert.
	 */
	public static function opmerkingMelderEmail()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(BugBericht $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(BugBericht $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(BugBericht $obj, $include_id = false)
	{
		return static::waardeTitel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld categorie.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld categorie labelt.
	 */
	public static function labelCategorie(BugBericht $obj)
	{
		return 'Categorie';
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld categorie van het object obj
	 * representeert.
	 */
	public static function waardeCategorie(BugBericht $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCategorie())
			return NULL;
		return BugCategorieView::defaultWaardeBugCategorie($obj->getCategorie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld categorie.
	 *
	 * @see genericFormcategorie
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCategorie(BugBericht $obj, $include_id = false)
	{
		return static::waardeCategorie($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld categorie. In
	 * tegenstelling tot formcategorie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcategorie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCategorie($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * categorie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld categorie representeert.
	 */
	public static function opmerkingCategorie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(BugBericht $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(BugBericht::enumsStatus() as $id)
			$soorten[$id] = BugBerichtView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(BugBericht $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(BugBericht $obj, $include_id = false)
	{
		return static::waardeStatus($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld prioriteit.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld prioriteit labelt.
	 */
	public static function labelPrioriteit(BugBericht $obj)
	{
		return 'Prioriteit';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld prioriteit.
	 *
	 * @param string $value Een enum-waarde van het veld prioriteit.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumPrioriteit($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld prioriteit horen.
	 *
	 * @see labelenumPrioriteit
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld prioriteit
	 * representeren.
	 */
	public static function labelenumPrioriteitArray()
	{
		$soorten = array();
		foreach(BugBericht::enumsPrioriteit() as $id)
			$soorten[$id] = BugBerichtView::labelenumPrioriteit($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld prioriteit.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld prioriteit van het object obj
	 * representeert.
	 */
	public static function waardePrioriteit(BugBericht $obj)
	{
		return static::defaultWaardeEnum($obj, 'Prioriteit');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld prioriteit.
	 *
	 * @see genericFormprioriteit
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prioriteit staat en kan
	 * worden bewerkt. Indien prioriteit read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPrioriteit(BugBericht $obj, $include_id = false)
	{
		return static::waardePrioriteit($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld prioriteit. In
	 * tegenstelling tot formprioriteit moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formprioriteit
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prioriteit staat en kan
	 * worden bewerkt. Indien prioriteit read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPrioriteit($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * prioriteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld prioriteit representeert.
	 */
	public static function opmerkingPrioriteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld niveau.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld niveau labelt.
	 */
	public static function labelNiveau(BugBericht $obj)
	{
		return 'Niveau';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld niveau.
	 *
	 * @param string $value Een enum-waarde van het veld niveau.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumNiveau($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld niveau horen.
	 *
	 * @see labelenumNiveau
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld niveau
	 * representeren.
	 */
	public static function labelenumNiveauArray()
	{
		$soorten = array();
		foreach(BugBericht::enumsNiveau() as $id)
			$soorten[$id] = BugBerichtView::labelenumNiveau($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld niveau.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld niveau van het object obj
	 * representeert.
	 */
	public static function waardeNiveau(BugBericht $obj)
	{
		return static::defaultWaardeEnum($obj, 'Niveau');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld niveau.
	 *
	 * @see genericFormniveau
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld niveau staat en kan worden
	 * bewerkt. Indien niveau read-only is betreft het een statisch html-element.
	 */
	public static function formNiveau(BugBericht $obj, $include_id = false)
	{
		return static::waardeNiveau($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld niveau. In
	 * tegenstelling tot formniveau moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formniveau
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld niveau staat en kan worden
	 * bewerkt. Indien niveau read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNiveau($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld niveau
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld niveau representeert.
	 */
	public static function opmerkingNiveau()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bericht.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bericht labelt.
	 */
	public static function labelBericht(BugBericht $obj)
	{
		return 'Bericht';
	}
	/**
	 * @brief Geef de waarde van het veld bericht.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bericht van het object obj
	 * representeert.
	 */
	public static function waardeBericht(BugBericht $obj)
	{
		return static::defaultWaardeText($obj, 'Bericht');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bericht.
	 *
	 * @see genericFormbericht
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bericht staat en kan
	 * worden bewerkt. Indien bericht read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBericht(BugBericht $obj, $include_id = false)
	{
		return static::waardeBericht($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bericht. In
	 * tegenstelling tot formbericht moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbericht
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bericht staat en kan
	 * worden bewerkt. Indien bericht read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBericht($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bericht
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bericht representeert.
	 */
	public static function opmerkingBericht()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld moment.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld moment labelt.
	 */
	public static function labelMoment(BugBericht $obj)
	{
		return 'Moment';
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld moment van het object obj
	 * representeert.
	 */
	public static function waardeMoment(BugBericht $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Moment');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld moment.
	 *
	 * @see genericFormmoment
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is betreft het een statisch html-element.
	 */
	public static function formMoment(BugBericht $obj, $include_id = false)
	{
		return static::waardeMoment($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld moment. In
	 * tegenstelling tot formmoment moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmoment
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moment staat en kan worden
	 * bewerkt. Indien moment read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMoment($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld moment
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld moment representeert.
	 */
	public static function opmerkingMoment()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld revisie.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld revisie labelt.
	 */
	public static function labelRevisie(BugBericht $obj)
	{
		return 'Revisie';
	}
	/**
	 * @brief Geef de waarde van het veld revisie.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld revisie van het object obj
	 * representeert.
	 */
	public static function waardeRevisie(BugBericht $obj)
	{
		return static::defaultWaardeInt($obj, 'Revisie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld revisie.
	 *
	 * @see genericFormrevisie
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld revisie staat en kan
	 * worden bewerkt. Indien revisie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRevisie(BugBericht $obj, $include_id = false)
	{
		return static::waardeRevisie($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld revisie. In
	 * tegenstelling tot formrevisie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formrevisie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld revisie staat en kan
	 * worden bewerkt. Indien revisie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRevisie($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld revisie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld revisie representeert.
	 */
	public static function opmerkingRevisie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld level.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld level labelt.
	 */
	public static function labelLevel(BugBericht $obj)
	{
		return 'Level';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld level.
	 *
	 * @param string $value Een enum-waarde van het veld level.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumLevel($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld level horen.
	 *
	 * @see labelenumLevel
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld level
	 * representeren.
	 */
	public static function labelenumLevelArray()
	{
		$soorten = array();
		foreach(BugBericht::enumsLevel() as $id)
			$soorten[$id] = BugBerichtView::labelenumLevel($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld level.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld level van het object obj
	 * representeert.
	 */
	public static function waardeLevel(BugBericht $obj)
	{
		return static::defaultWaardeEnum($obj, 'Level');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld level.
	 *
	 * @see genericFormlevel
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld level staat en kan worden
	 * bewerkt. Indien level read-only is betreft het een statisch html-element.
	 */
	public static function formLevel(BugBericht $obj, $include_id = false)
	{
		return static::waardeLevel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld level. In
	 * tegenstelling tot formlevel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlevel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld level staat en kan worden
	 * bewerkt. Indien level read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLevel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld level
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld level representeert.
	 */
	public static function opmerkingLevel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commit.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld commit labelt.
	 */
	public static function labelCommit(BugBericht $obj)
	{
		return 'Commit';
	}
	/**
	 * @brief Geef de waarde van het veld commit.
	 *
	 * @param BugBericht $obj Het BugBericht-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commit van het object obj
	 * representeert.
	 */
	public static function waardeCommit(BugBericht $obj)
	{
		return static::defaultWaardeString($obj, 'Commit');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commit.
	 *
	 * @see genericFormcommit
	 *
	 * @param BugBericht $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commit staat en kan worden
	 * bewerkt. Indien commit read-only is betreft het een statisch html-element.
	 */
	public static function formCommit(BugBericht $obj, $include_id = false)
	{
		return static::waardeCommit($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commit. In
	 * tegenstelling tot formcommit moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formcommit
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commit staat en kan worden
	 * bewerkt. Indien commit read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormCommit($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld commit
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commit representeert.
	 */
	public static function opmerkingCommit()
	{
		return NULL;
	}
}
