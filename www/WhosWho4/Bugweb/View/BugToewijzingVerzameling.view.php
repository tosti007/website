<?
/**
 * $Id$
 */
abstract class BugToewijzingVerzamelingView
	extends BugToewijzingVerzamelingView_Generated
{
	static public function toString(BugToewijzingVerzameling $btv)
	{
		$pv = new PersoonVerzameling();
		foreach($btv as $bt) {
			$pv->voegtoe($bt->getPersoon());
		}
		if ($pv->aantal() > 0)
		{
			$pv->sorteer('Naam');
			$string = '';
			$first = true;
			foreach($pv as $pers) {
				if(!$first)
					$string .= ", ";
				$string .= PersoonView::naam($pers);
				$first = false;
			}
			return $string;
		}
		return _('Er is nog niemand aan deze bug toegewezen');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
