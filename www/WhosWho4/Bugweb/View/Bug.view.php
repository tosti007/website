<?
/**
 * $Id$
 */
abstract class BugView
	extends BugView_Generated
{

	/**
	 * Maak een visuele representatie van de huidige toestand en het eerste 
	 * bericht van een Bug.
	 *
	 * @param bug Een Bug.
	 * @return Een HtmlDiv die informatie over en het oorspronkelikje 
	 * BugBericht van bug bevat.
	 * @see BugBerichtVerzamelingView::toonBerichten
	 */
	public static function toon (Bug $bug)
	{
		$ingelogd = Persoon::getIngelogd();
		$reacties = $bug->getReacties();

		$div = new HtmlDiv(NULL, 'bugs');

		$pijltje = '';
		if ($ingelogd
			&& $ingelogd->getVoorkeur()->getBugSortering() == 'ASC'
			&& $reacties->aantal() > 0)
		{
			$pijltje = new HtmlAnchor('#laatste', '&#8595;');
			$pijltje->addClass('pijl');
			$pijltje .= '&nbsp;';
		}

		$div->add(new HtmlHeader(2, sprintf('%s%s: %s [%s]',
			$pijltje,
			self::waardeBugID($bug),
			self::waardeTitel($bug),
			self::waardeCategorie($bug))));

		if ($ingelogd)
		{
			$form = new HtmlForm('get', BUGWEBBASE . '/InverteerBugSortering');
			$form->add(HtmlInput::makeSubmitButton(_('Inverteer sortering')));
			$form->addClass('sorteringsbutton');
			$div->add($form);
		}

		if ($ingelogd)
		{
			if ($bug->isVolger(Persoon::getIngelogd()))
			{
				$div->add($form = new HtmlForm('get', 'Ontvolg'));
				$form->add(HtmlInput::makeSubmitButton(_('Volg deze bug niet'), ''));
			}
			else
			{
				$div->add($form = new HtmlForm('get', 'Volg'));
				$form->add(HtmlInput::makeSubmitButton(_('Volg deze bug'), ''));
			}
		}

		$div->add(new HtmlDiv(
			new HtmlStrong(_('Toegewezen aan:')) . ' ' . self::waardeToewijzingen($bug) .
			new HtmlBreak() .
			new HtmlStrong(self::labelStatus($bug) . ':') . ' ' . self::waardeStatus($bug) .
			new HtmlBreak() .
			new HtmlStrong(self::labelPrioriteit($bug) . ':') . ' ' . self::waardePrioriteit($bug) .
			new HtmlBreak() .
			new HtmlStrong(self::labelLevel($bug) . ':') . ' ' . self::waardeLevel($bug) .
			new HtmlBreak() .
			new HtmlStrong(self::labelNiveau($bug) . ':') . ' ' . self::waardeNiveau($bug)));

		$div->add(BugBerichtView::toonEersteBericht($bug));

		return $div;
	}

	/**
	 * Maak een visuele representatie van de reacties op een Bug.
	 * 
	 * @param bug Een Bug.
	 * @param omgekeerd Indien True, worden de BugBerichten in aflopende 
	 * volgorde weergegeven; anders worden ze in oplopende volgorde 
	 * weergegeven.
	 * @return Een HtmlDiv die de reacties op bug bevat.
	 */
	public static function toonReacties (Bug $bug, $omgekeerd = False)
	{
		$reacties = $bug->getReacties();

		$div = new HtmlDiv(NULL, 'bugs');
		$div->add(BugBerichtVerzamelingView::toonBerichten($reacties, $omgekeerd));

		return $div;
	}

	/**
	 * Maak een tabelrij van een Bug.
	 *
	 * @param bug Een Bug.
	 * @param groot Een bool om aan te geven of de categorie erbij moet
	 * @return Een HtmlTableRow die het nummer, de huidige prioriteit (als 
	 * afbeelding), de huidige status en de huidige titel van bug bevat.
	 */
	public static function toTableRow (Bug $bug, $groot = False)
	{
		$url = $bug->url();
		$openBugJs = "document.location='$url'";

		$row = new HtmlTableRow(NULL, self::waardeStatus($bug));
		$td = $row->addData(self::makeLink($bug));
		$row->add($td = HtmlTableDataCell::maakGesorteerde(
			self::waardePrioriteitImage($bug), $bug->getPrioriteitInt()));
		$td = $row->addData(self::getGewijzigdTijd($bug));
		$td = $row->addData(self::waardeStatus($bug));
		$row->add($td = HtmlTableDataCell::maakGesorteerde(
            self::waardeLevel($bug), $bug->getLevelInt()));

		if ($groot)
		{
			$td = $row->addData(self::waardeCategorie($bug));
		}

		$td = $row->addData(new HtmlAnchor($bug->url(), self::waardeTitel($bug)));

		$row->addClass(strtolower($bug->getStatus()));
		return $row;
	}

	/**
	 * Maak een formulier om een nieuwe Bug te melden.
	 *
	 * @param bug De nieuwe Bug.
	 * @param bericht Het bericht van de nieuwe Bug. Dit is een aparte 
	 * parameter omdat Bug geen veld bericht heeft (dat zit in BugBericht).
	 * @param toon_fouten Indien True, toon een kolom met foutmeldingen.
	 * @return Een HtmlForm met velden om een nieuwe Bug te melden, 
	 * vooringevuld met de informatie in bug en bericht.
	 */
	public static function nieuweBug (Bug $bug, $bericht, $toon_fouten)
	{
		$form = HtmlForm::named('NieuweBug');

		$cies = BugCategorieVerzameling::geefActieveBugCommissies();
		$cieLeden = [];

		foreach($cies as $cie) {
			$cieLeden[$cie->geefID()] = $cie->leden();
			$cieLeden[$cie->geefID()]->sorteer('Naam');
		}

		$catCieIds = [];
		foreach($cies as $cie) {
			foreach(BugCategorieVerzameling::geefVanCommissie($cie) as $cat) {
				$catCieIds[$cat->geefID()] = $cie->geefID();
			}
		}

		$form->add(HtmlScript::makeJavascript('var catCies = '.json_encode($catCieIds).';'));

		$form->add(self::wijzigTR($bug, 'Titel', $toon_fouten));
		$form->add($box = self::wijzigTR($bug, 'Categorie', $toon_fouten));
		$box->getChild(1)->getChild(0)->setAttribute('data-bind', 'value: categorie');
		$form->add(self::wijzigTR($bug, 'Status', $toon_fouten));
		$form->add(self::wijzigTR($bug, 'Prioriteit', $toon_fouten));
		$form->add(self::wijzigTR($bug, 'Niveau', $toon_fouten));
		$form->add(self::wijzigTR($bug, 'Level', $toon_fouten));
		$form->add(self::wijzigTR($bug, 'Bericht', $toon_fouten));

		/**
		 * Beste Webcie'er van de verre toekomst
		 * Omdat het eerste bericht van een bug een Bug-object is ipv een BugBericht-object kunnen hier geen toewijzigingen gedaan worden.
		 * Zonder de hele Bug-classe te refactoren is het dus niet mogelijk om hier een persoon te koppelen
		 * Veel succes als je het toch wil proberen!
		 */
		/*
		foreach($cies as $cie)
		{
			$cieL = PersoonVerzameling::getBugToewijzingen($bug);
			$cieL->sorteer('Naam');
			$cieL->union($cieLeden[$cie->geefID()]);
			 
			$box = PersoonVerzamelingView::htmlSelectBox(NULL, $cieL, NULL, NULL, true);
			$box->setAttribute('name', 'BugBericht[Toewijzingen][]');
			$box->setCaption(NULL);
			$box->setAttribute('data-bind', 'attr: { name: curCie() == '.$cie->geefID().' ? "BugBericht[Toewijzingen][]" : "null" }');

			$form->add($div = self::inputRowDiv($box, 'toewijzing', _('Toewijzen aan'), _('Optioneel')));
			$div->setAttribute('data-bind', 'visible: curCie() == '.$cie->geefID());
		}
		*/

		$form->add(HtmlInput::makeFormSubmitButton(_('Voer in')));
		return $form;
	}

	/**
	 * Geef de tijd waarop er voor het laatst iets aan de bug gewijzgd is
	 * of wanneer er voor het laatst iemand op de bug gereageerd heeft
	 *
	 * @param bug Een Bug.
	 * @return De tijd waarop de bug voor het laatst gewijzigd is
	 */
	static public function getGewijzigdTijd(Bug $bug)
	{
		$gewijzigdtijd = $bug->getMoment();

		$berichten = $bug->getBerichten();

		$laatstetijd = $berichten->last()->getMoment();

		if($gewijzigdtijd <= $laatstetijd)
			return $laatstetijd->format('Y-m-d H:i:s');
		else
			return $gewijzigdtijd->format('Y-m-d H:i:s');
	}

	/**
	 * Maak een link naar een Bug.
	 *
	 * @param bug Een Bug.
	 * @param titel (Optioneel) de tekst waar je op kan klikken. Per default het bugnummer.
	 * @return Een HtmlAnchor naar de url van bug met gegeven titel.
	 */
	public static function makeLink (Bug $bug, $titel = NULL)
	{
		if (!$titel) {
			$titel = self::waardeBugID($bug);
		}
		return new HtmlAnchor(HTTPS_ROOT . $bug->url(), $titel);
	}
	
	public static function waardeMelder(Bug $obj)
	{
		if (($melder = $obj->getMelder()) instanceof Persoon)
			return PersoonView::makeLink($melder);

		return static::waardeMelderEmail($obj);
	}
	public static function waardeCategorie(Bug $obj)
	{
		return BugCategorieView::makeLink($obj->getCategorie());
	}
	public static function labelEnumStatus($value)
	{
		switch ($value)
		{
		case 'OPEN':
			return _('Open');
		case 'BEZIG':
			return _('Wordt aan gewerkt');
		case 'OPGELOST':
			return _('Opgelost');
		case 'UITGESTELD':
			return _('Uitgesteld');
		case 'GESLOTEN':
			return _('Gesloten');
		case 'BOGUS':
			return _('Geen bug');
		case 'DUBBEL':
			return _('Dubbel');
		case 'WANNAHAVE':
			return _('Feature request');
		case 'FEEDBACK':
			return _('Meer info nodig');
		case 'GECOMMIT':
			return _('Fix gecommit');
		case 'WONTFIX':
			return _('Won\'t fix');
		}
	}
	public static function labelEnumPrioriteit($value)
	{
		switch ($value)
		{
		case 'URGENT':
			return _('Urgent');
		case 'HOOG':
			return _('Hoog');
		case 'GEMIDDELD':
			return _('Gemiddeld');
		case 'LAAG':
			return _('Laag');
		case 'ONBENULLIG':
			return _('Onbenullig');
		}
	}
	public static function labelEnumNiveau($value)
	{
		switch($value)
		{
		case 'OPEN':
			return _('Voor iedereen te zien');
		case 'INGELOGD':
			return _('Alleen voor ingelogden te zien');
		case 'CIE':
			return _('Alleen voor de commissie te zien');
		}
	}
	public static function labelEnumLevel($value)
	{
		$level = parent::labelEnumLevel($value);
		return ucfirst(strtolower($level));
	}
	public static function waardeMoment(Bug $obj)
	{
		if (($moment = $obj->getMoment()) instanceof DateTimeLocale)
			return $moment->strftime('%Y-%m-%d %H:%M');
		return NULL;
	}

	public static function opmerkingBericht()
	{
		return '';
	}

	public static function formCategorie(Bug $obj, $include_id = false)
	{
		$bugCategorie = $obj->getCategorie() ?: BugCategorie::geef(BUGCATEGORIE_DEFAULT_ID);
		return BugCategorieVerzamelingView::maakCiesComboBox('Bug[Categorie]', $bugCategorie, false);
	}

	public static function formBericht(Bug $obj, $include_id = false)
	{
		$bericht = tryPar('Bug[Bericht]', tryPar('bericht', $obj->getBericht()));
		$element = HtmlTextarea::withContent($bericht, 'Bug[Bericht]', 80, 10);
		$element->addClass('monospace');
		return $element;
	}

	public static function melder(Bug $bug)
	{
		if($bug->getMelder()) {
			$infoMelder = "Melder: " . PersoonView::naam($bug->getMelder());
		} elseif($bug->getMelderEmail()) {
			$infoMelder = "Melder: " . $bug->getMelderEmail();
		}
		return $infoMelder;
	}

	/**
	 * Geeft een RSS-item terug
	 */
	static public function rssItem (Bug $bug)
	{
		$bugbericht = $bug->getBerichten()->first();
		$categorie = $bug->getCategorie();
		$melder = $bugbericht->getMelder();
		$item = new Rss_Item();
		$item->setTitle($bug->geefID() . ": " . BugView::waardeTitel($bug))
			->setDescription(PersoonView::naam($melder) . " - " . BugCategorieView::waardeNaam($categorie) . "<br/>"
				. BugBerichtView::waardeBericht($bugbericht))
			->setPubdate($bugbericht->getMoment()->format(DateTime::RSS))
			->setLink(HTTPS_ROOT . $bug->url());
		return $item;
	}

	public static function labelBugID(Bug $obj = NULL)
	{
		return '#';
	}
	public static function labelMelder(Bug $obj = NULL)
	{
		return _('Melder');
	}
	public static function labelMelderEmail(Bug $obj = NULL)
	{
		return _('Emailadres melder');
	}
	public static function labelTitel(Bug $obj = NULL)
	{
		return _('Titel');
	}
	public static function labelCategorie(Bug $obj = NULL)
	{
		return _('Categorie');
	}
	public static function labelStatus(Bug $obj = NULL)
	{
		return _('Status');
	}
	public static function labelPrioriteit(Bug $obj = NULL)
	{
		return _('Prioriteit');
	}
	public static function labelMoment(Bug $obj = NULL)
	{
		return _('Moment');
	}
	public static function labelType(Bug $obj = NULL)
	{
		return _('Type');
	}
	public static function labelBericht(Bug $obj = NULL)
	{
		return _('Bericht') . (new HtmlSpan('*', 'verplicht'));
	}
	public static function labelLevel(Bug $obj = NULL)
	{
		return _('Moeilijkheid');
	}
	public static function labelNiveau(Bug $obj = NULL)
	{
		return _('Zichtbaarheid');
	}
	public static function labelToewijzing(Bug $obj = NULL)
	{
		return _('Toewijzing');
	}

	/**
	 * Maak een visuele representatie van het e-mailadres van een Bug.
	 *
	 * @param bug Een Bug.
	 * @return Een html-veilige string die het e-mailadres van bug 
	 * representeert.
	 * @see Bug::getEmail
	 * @see View::defaultWaardeString
	 */
	public static function waardeEmail (Bug $bug)
	{
		return static::defaultWaardeString($bug, 'Email');
	}

	/**
	 * Maak een afbeelding van de prioriteit van een Bug.
	 *
	 * @param bug Een Bug.
	 * @return Een visuele representatie van de prioriteit van bug.
	 */
	public static function waardePrioriteitImage (Bug $bug)
	{
		switch ($bug->getPrioriteit())
		{
		case 'URGENT':
			return new HtmlImage('/Layout/Images/Icons/prio7.png', _('Urgent'));
		case 'HOOG':
			return new HtmlImage('/Layout/Images/Icons/prio6.png', _('Hoog'));
		case 'GEMIDDELD':
			return new HtmlImage('/Layout/Images/Icons/prio5.png', _('Gemiddeld'));
		case 'LAAG':
			return new HtmlImage('/Layout/Images/Icons/prio4.png', _('Laag'));
		case 'ONBENULLIG':
			return new HtmlImage('/Layout/Images/Icons/prio3.png', _('Onbenullig'));
		}
	}

	/**
	 * Maak een visuele representatie van de op dit moment aan een Bug 
	 * toegewezen Personen.
	 *
	 * @param bug Een Bug.
	 * @return Een kommagescheiden lijst van met de namen van Personen die aan 
	 * bug zijn toegewezen.
	 */
	public static function waardeToewijzingen (Bug $bug)
	{
		$toewijzingen = $bug->getToewijzingPersonen();
		if ($toewijzingen->aantal() > 0)
		{
			$toewijzingen->sorteer('Naam');
			$string = '';
			$first = true;
			foreach($toewijzingen as $pers) {
				if(!$first)
					$string .= ", ";
				$string .= PersoonView::naam($pers);
				$first = false;
			}
			return $string;
		}
		return _('Er is nog niemand aan deze bug toegewezen');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
