<?
/**
 * $Id$
 */
abstract class BugCategorieView
	extends BugCategorieView_Generated
{

	/**
	 *  Maak een link naar een BugCategorie.
	 *
	 * @param categorie Een BugCategorie.
	 * @return Een HtmlAnchor met de naam van categorie die naar diens url wijst.
	 */
	public static function makeLink (BugCategorie $categorie)
	{
		return new HtmlAnchor($categorie->url(), self::waardeNaam($categorie));
	}

	/**
	 *  Maak een visuele representatie van een BugCategorie.
	 *
	 * @param categorie een BugCategorie.
	 * @return Een HtmlDiv met daarin de naam van categorie, een linkje om deze 
	 * te deactiveren (of een boodschap dat dat niet mag), en een tabel met 
	 * alle open Bugs.
	 */
	public static function toon (BugCategorie $categorie)
	{
		$isActief = $categorie->getActief();
		$bugs = $categorie->getBugs();

		$div = new HtmlDiv();
		$div->add(HtmlAnchor::button(BUGWEBBASE . '/Categorie/', _('Terug naar het categorie&#235;noverzicht')));

		$titel = sprintf('%s (%s)', self::waardeNaam($categorie), self::waardeCommissie($categorie));
		if (!$isActief)
		{
			$titel .= ' [' . new HtmlSpan(_('inactief'), 'inactief') . ']';
		}
		$div->add(new HtmlHeader(2, $titel));

		if ($categorie->magWijzigen())
		{
			if (!$isActief || $bugs->aantal() == 0)
			{
				$form = new HtmlForm('get', $categorie->url() . '/WijzigActief');
				$form->add(HtmlInput::makeSubmitButton($isActief
					? _('Deactiveer deze categorie')
					: _('Reactiveer deze categorie')));
				$div->add(new HtmlDiv($form));
			}
			else
			{
				$div->add(new HtmlDiv(new HtmlEmphasis(
					_('Deze categorie bevat open bugs. Hierdoor kan ze op dit moment niet worden gedeactiveerd.'))));
			}
		}

		if ($isActief)
		{
			$div->add(new HtmlDiv($form = new HtmlForm('get', BUGWEBBASE . '/Melden')));
			$form->add(HtmlInput::makeHidden('categorieID', $categorie->getBugCategorieID()));
			$form->add($submit = HtmlInput::makeSubmitButton(_('Voer nieuwe bug in')));
			$submit->setAttribute('name', '');
			$form->setToken(NULL);
		}


		$div->add(BugVerzamelingView::tabel($bugs));

		return $div;
	}

	public static function labelNaam(BugCategorie $obj)
	{
		return _('Naam');
	}
	public static function labelCommissie(BugCategorie $obj)
	{
		return _('Commissie');
	}
	public static function labelActief(BugCategorie $obj)
	{
		return _('Actief');
	}

	public static function formCommissie(BugCategorie $obj, $include_id = false)
	{
		$ingelogd = Persoon::getIngelogd();
		if (!$ingelogd)
		{
			return NULL;
		}

		if (hasAuth('god'))
		{
			$cies = CommissieVerzameling::huidige('ALLE');
		}
		else
		{
			$cies = $ingelogd->getCommissies();
		}

		$box = CommissieVerzamelingView::maakCommissieSelectbox('BugCategorie[Commissie]', $cies, $obj->getCommissie() ?: NULL);
		return $box;
	}

	public static function waardeCommissie(BugCategorie $obj)
	{
		return CommissieView::makeLink($obj->getCommissie());
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
