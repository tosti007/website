<?
/**
 * $Id$
 */
abstract class BugBerichtVerzamelingView
	extends BugBerichtVerzamelingView_Generated
{

	/**
	 * Maak een visuele representatie van een BugBerichtVerzameling.
	 *
	 * @param berichten Een BugBerichtVerzameling.
	 * @param omgekeerd Een bool om aan te geven of de selectie geïnverteerd moet worden
	 * @return Een door HtmlDiv die het bericht in elke BugBericht bevat, elk
	 * door een HtmlHR gescheiden.
	 * @see BugBerichtView::toon
	 */
	public static function toonBerichten (BugBerichtVerzameling $berichten, $omgekeerd)
	{
		if ($berichten->aantal() == 0)
		{
			return NULL;
		}

		$bug = $berichten->first()->getBug();
		$toewijzingen = $bug->getToewijzingen();

		$berichtTeksten = array();

		$laatste = NULL;
		$vorige = $berichten->first()->vorige();
		$huidgeToewijzingPersonen = $vorige->getBeginToewijzingen()->toPersoonVerzameling();
		foreach ($berichten as $bericht)
		{
			$berichtID = $bericht->getBugBerichtID();

			$vorigeToewijzingPersonen = clone $huidgeToewijzingPersonen;
			$toewijzingenGewijzigd = False;
			foreach ($toewijzingen as $toewijzing)
			{
				if ($toewijzing->getBeginBugBerichtID() == $berichtID)
				{
					$huidgeToewijzingPersonen->voegtoe($toewijzing->getPersoon());
					$toewijzingenGewijzigd = True;
				}
				else if ($toewijzing->getEindBugBerichtID() == $berichtID)
				{
					$huidgeToewijzingPersonen->verwijder($toewijzing->getPersoonContactID());
					$toewijzingenGewijzigd = True;
				}
			}

			$berichtTeksten[] = BugBerichtView::toon(
				$bericht,
				$vorige,
				$toewijzingenGewijzigd ? $vorigeToewijzingPersonen : NULL,
				$toewijzingenGewijzigd ? $huidgeToewijzingPersonen : NULL);

			$vorige = $bericht;
		}

		// Geef een id aan het recentste bericht
		end($berichtTeksten)->setId('laatste');

		if ($omgekeerd)
		{
			$berichtTeksten = array_reverse($berichtTeksten);
		}

		$div = new HtmlDiv();
		foreach ($berichtTeksten as $berichtTekst)
		{
			$div->add(new HtmlHR());
			$div->add($berichtTekst);
		}

		return $div;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
