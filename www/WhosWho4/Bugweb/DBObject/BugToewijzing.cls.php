<?
/**
 * $Id$
 */
class BugToewijzing
	extends BugToewijzing_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($bug, $persoon, $begin)
	{
		parent::__construct($bug, $persoon, $begin); // BugToewijzing_Generated
	}

	/**
	 * Fabrieksmethode om een nieuwe BugToewijzing te maken.
	 *
	 * Stelt ook automatisch de Bug in naar die van bericht.
	 *
	 * @param persoon De Persoon aan wie de Bug wordt toegewezen.
	 * @param bericht Het BugBericht waarbij de toewijzing begint.
	 * @return Een nieuwe BugToewijzing aan persoon, die begint bij bericht.
	 */
	public static function nieuweBugToewijzing (Persoon $persoon, BugBericht $bericht)
	{
		$toewijzing = new BugToewijzing($bericht->getBug(), $persoon, $bericht);

		return $toewijzing;
	}

	/**
	 * Bepaal of deze BugToewijzing door de huidig ingelogde gebruiker bekeken 
	 * mag worden.
	 *
	 * @return Of de Bug, waar deze BugToewijzing bij hoort, bekeken mag 
	 * worden.
	 */
	public function magBekijken ()
	{
		return $this->getBug()->magBekijken();
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
