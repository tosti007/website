<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
abstract class BugToewijzing_Generated
	extends Entiteit
{
	protected $bugToewijzingID;			/**< \brief PRIMARY */
	protected $bug;
	protected $bug_bugID;				/**< \brief PRIMARY */
	protected $persoon;
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $begin;					/**< \brief NULL */
	protected $begin_bugBerichtID;		/**< \brief PRIMARY */
	protected $eind;					/**< \brief NULL */
	protected $eind_bugBerichtID;		/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de BugToewijzing_Generated-klasse.
	 *
	 * @param mixed $a Bug (Bug OR Array(bug_bugID) OR bug_bugID)
	 * @param mixed $b Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 * @param mixed $c Begin (BugBericht OR Array(bugBericht_bugBerichtID) OR
	 * bugBericht_bugBerichtID)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Bug)
		{
			$this->bug = $a;
			$this->bug_bugID = $a->getBugID();
		}
		else if(is_array($a))
		{
			$this->bug = NULL;
			$this->bug_bugID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->bug = NULL;
			$this->bug_bugID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->persoon = $b;
			$this->persoon_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($c) && is_null($c[0]))
			$c = NULL;

		if($c instanceof BugBericht)
		{
			$this->begin = $c;
			$this->begin_bugBerichtID = $c->getBugBerichtID();
		}
		else if(is_array($c))
		{
			$this->begin = NULL;
			$this->begin_bugBerichtID = (int)$c[0];
		}
		else if(isset($c))
		{
			$this->begin = NULL;
			$this->begin_bugBerichtID = (int)$c;
		}
		else
		{
			$this->begin = NULL;
			$this->begin_bugBerichtID = NULL;
		}

		$this->bugToewijzingID = NULL;
		$this->eind = NULL;
		$this->eind_bugBerichtID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Bug';
		$volgorde[] = 'Persoon';
		$volgorde[] = 'Begin';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld bugToewijzingID.
	 *
	 * @return int
	 * De waarde van het veld bugToewijzingID.
	 */
	public function getBugToewijzingID()
	{
		return $this->bugToewijzingID;
	}
	/**
	 * @brief Geef de waarde van het veld bug.
	 *
	 * @return Bug
	 * De waarde van het veld bug.
	 */
	public function getBug()
	{
		if(!isset($this->bug)
		 && isset($this->bug_bugID)
		 ) {
			$this->bug = Bug::geef
					( $this->bug_bugID
					);
		}
		return $this->bug;
	}
	/**
	 * @brief Geef de waarde van het veld bug_bugID.
	 *
	 * @return int
	 * De waarde van het veld bug_bugID.
	 */
	public function getBugBugID()
	{
		if (is_null($this->bug_bugID) && isset($this->bug)) {
			$this->bug_bugID = $this->bug->getBugID();
		}
		return $this->bug_bugID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld begin.
	 *
	 * @return BugBericht
	 * De waarde van het veld begin.
	 */
	public function getBegin()
	{
		if(!isset($this->begin)
		 && isset($this->begin_bugBerichtID)
		 ) {
			$this->begin = BugBericht::geef
					( $this->begin_bugBerichtID
					);
		}
		return $this->begin;
	}
	/**
	 * @brief Geef de waarde van het veld begin_bugBerichtID.
	 *
	 * @return int
	 * De waarde van het veld begin_bugBerichtID.
	 */
	public function getBeginBugBerichtID()
	{
		if (is_null($this->begin_bugBerichtID) && isset($this->begin)) {
			$this->begin_bugBerichtID = $this->begin->getBugBerichtID();
		}
		return $this->begin_bugBerichtID;
	}
	/**
	 * @brief Geef de waarde van het veld eind.
	 *
	 * @return BugBericht
	 * De waarde van het veld eind.
	 */
	public function getEind()
	{
		if(!isset($this->eind)
		 && isset($this->eind_bugBerichtID)
		 ) {
			$this->eind = BugBericht::geef
					( $this->eind_bugBerichtID
					);
		}
		return $this->eind;
	}
	/**
	 * @brief Stel de waarde van het veld eind in.
	 *
	 * @param mixed $new_bugBerichtID De nieuwe waarde.
	 *
	 * @return BugToewijzing
	 * Dit BugToewijzing-object.
	 */
	public function setEind($new_bugBerichtID)
	{
		unset($this->errors['Eind']);
		if($new_bugBerichtID instanceof BugBericht
		) {
			if($this->eind == $new_bugBerichtID
			&& $this->eind_bugBerichtID == $this->eind->getBugBerichtID())
				return $this;
			$this->eind = $new_bugBerichtID;
			$this->eind_bugBerichtID
					= $this->eind->getBugBerichtID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_bugBerichtID) || $new_bugBerichtID == 0)) {
			if($this->eind == NULL && $this->eind_bugBerichtID == NULL)
				return $this;
			$this->eind = NULL;
			$this->eind_bugBerichtID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_bugBerichtID)
		) {
			if($this->eind == NULL 
				&& $this->eind_bugBerichtID == (int)$new_bugBerichtID)
				return $this;
			$this->eind = NULL;
			$this->eind_bugBerichtID
					= (int)$new_bugBerichtID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld eind geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld eind geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkEind()
	{
		if (array_key_exists('Eind', $this->errors))
			return $this->errors['Eind'];
		$waarde1 = $this->getEind();
		$waarde2 = $this->getEindBugBerichtID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld eind_bugBerichtID.
	 *
	 * @return int
	 * De waarde van het veld eind_bugBerichtID.
	 */
	public function getEindBugBerichtID()
	{
		if (is_null($this->eind_bugBerichtID) && isset($this->eind)) {
			$this->eind_bugBerichtID = $this->eind->getBugBerichtID();
		}
		return $this->eind_bugBerichtID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return BugToewijzing::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van BugToewijzing.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return BugToewijzing::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getBugToewijzingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return BugToewijzing|false
	 * Een BugToewijzing-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$bugToewijzingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$bugToewijzingID = (int)$a;
		}

		if(is_null($bugToewijzingID))
			throw new BadMethodCallException();

		static::cache(array( array($bugToewijzingID) ));
		return Entiteit::geefCache(array($bugToewijzingID), 'BugToewijzing');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'BugToewijzing');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('BugToewijzing::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `BugToewijzing`.`bugToewijzingID`'
		                 .     ', `BugToewijzing`.`bug_bugID`'
		                 .     ', `BugToewijzing`.`persoon_contactID`'
		                 .     ', `BugToewijzing`.`begin_bugBerichtID`'
		                 .     ', `BugToewijzing`.`eind_bugBerichtID`'
		                 .     ', `BugToewijzing`.`gewijzigdWanneer`'
		                 .     ', `BugToewijzing`.`gewijzigdWie`'
		                 .' FROM `BugToewijzing`'
		                 .' WHERE (`bugToewijzingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['bugToewijzingID']);

			$obj = new BugToewijzing(array($row['bug_bugID']), array($row['persoon_contactID']), array($row['begin_bugBerichtID']));

			$obj->inDB = True;

			$obj->bugToewijzingID  = (int) $row['bugToewijzingID'];
			$obj->eind_bugBerichtID  = (is_null($row['eind_bugBerichtID'])) ? null : (int) $row['eind_bugBerichtID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'BugToewijzing')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getBugBugID();
		$this->getPersoonContactID();
		$this->getBeginBugBerichtID();
		$this->getEindBugBerichtID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->bugToewijzingID =
			$WSW4DB->q('RETURNID INSERT INTO `BugToewijzing`'
			          . ' (`bug_bugID`, `persoon_contactID`, `begin_bugBerichtID`, `eind_bugBerichtID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %i, %s, %i)'
			          , $this->bug_bugID
			          , $this->persoon_contactID
			          , $this->begin_bugBerichtID
			          , $this->eind_bugBerichtID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'BugToewijzing')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `BugToewijzing`'
			          .' SET `bug_bugID` = %i'
			          .   ', `persoon_contactID` = %i'
			          .   ', `begin_bugBerichtID` = %i'
			          .   ', `eind_bugBerichtID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugToewijzingID` = %i'
			          , $this->bug_bugID
			          , $this->persoon_contactID
			          , $this->begin_bugBerichtID
			          , $this->eind_bugBerichtID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugToewijzingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBugToewijzing
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBugToewijzing($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBugToewijzing($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Eind';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Bug';
			$velden[] = 'Persoon';
			$velden[] = 'Begin';
			$velden[] = 'Eind';
			break;
		case 'get':
			$velden[] = 'Bug';
			$velden[] = 'Persoon';
			$velden[] = 'Begin';
			$velden[] = 'Eind';
		case 'primary':
			$velden[] = 'bug_bugID';
			$velden[] = 'persoon_contactID';
			$velden[] = 'begin_bugBerichtID';
			$velden[] = 'eind_bugBerichtID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Bug';
			$velden[] = 'Persoon';
			$velden[] = 'Begin';
			$velden[] = 'Eind';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `BugToewijzing`'
		          .' WHERE `bugToewijzingID` = %i'
		          .' LIMIT 1'
		          , $this->bugToewijzingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->bugToewijzingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `BugToewijzing`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugToewijzingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugToewijzingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van BugToewijzing terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bugtoewijzingid':
			return 'int';
		case 'bug':
			return 'foreign';
		case 'bug_bugid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'begin':
			return 'foreign';
		case 'begin_bugberichtid':
			return 'int';
		case 'eind':
			return 'foreign';
		case 'eind_bugberichtid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `BugToewijzing`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `bugToewijzingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->bugToewijzingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `BugToewijzing`'
		          .' WHERE `bugToewijzingID` = %i'
		                 , $veld
		          , $this->bugToewijzingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'BugToewijzing');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
