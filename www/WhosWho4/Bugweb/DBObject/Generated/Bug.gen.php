<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
abstract class Bug_Generated
	extends Entiteit
{
	protected $bugID;					/**< \brief PRIMARY */
	protected $melder;					/**< \brief NULL */
	protected $melder_contactID;		/**< \brief PRIMARY */
	protected $melderEmail;				/**< \brief NULL */
	protected $titel;
	protected $categorie;
	protected $categorie_bugCategorieID;/**< \brief PRIMARY */
	protected $status;					/**< \brief ENUM:OPEN/BEZIG/OPGELOST/GECOMMIT/UITGESTELD/GESLOTEN/BOGUS/DUBBEL/WANNAHAVE/FEEDBACK/WONTFIX */
	protected $prioriteit;				/**< \brief ENUM:ONBENULLIG/LAAG/GEMIDDELD/HOOG/URGENT */
	protected $niveau;					/**< \brief ENUM:OPEN/INGELOGD/CIE */
	protected $moment;
	protected $type;
	protected $level;					/**< \brief ENUM:BEGINNER/GEMIDDELD/EXPERT */
	/** Verzamelingen **/
	protected $bugBerichtVerzameling;
	protected $bugToewijzingVerzameling;
	protected $bugVolgerVerzameling;
	/**
	/**
	 * @brief De constructor van de Bug_Generated-klasse.
	 *
	 * @param mixed $a Moment (datetime)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(!$a instanceof DateTimeLocale)
		{
			if(is_null($a))
				$a = new DateTimeLocale();
			else
			{
				try {
					$a = new DateTimeLocale($a);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->moment = $a;

		$this->bugID = NULL;
		$this->melder = NULL;
		$this->melder_contactID = NULL;
		$this->melderEmail = NULL;
		$this->titel = '';
		$this->categorie = NULL;
		$this->categorie_bugCategorieID = 0;
		$this->status = 'OPEN';
		$this->prioriteit = 'ONBENULLIG';
		$this->niveau = 'OPEN';
		$this->type = '';
		$this->level = 'GEMIDDELD';
		$this->bugBerichtVerzameling = NULL;
		$this->bugToewijzingVerzameling = NULL;
		$this->bugVolgerVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Moment';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld bugID.
	 *
	 * @return int
	 * De waarde van het veld bugID.
	 */
	public function getBugID()
	{
		return $this->bugID;
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @return Persoon
	 * De waarde van het veld melder.
	 */
	public function getMelder()
	{
		if(!isset($this->melder)
		 && isset($this->melder_contactID)
		 ) {
			$this->melder = Persoon::geef
					( $this->melder_contactID
					);
		}
		return $this->melder;
	}
	/**
	 * @brief Stel de waarde van het veld melder in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setMelder($new_contactID)
	{
		unset($this->errors['Melder']);
		if($new_contactID instanceof Persoon
		) {
			if($this->melder == $new_contactID
			&& $this->melder_contactID == $this->melder->getContactID())
				return $this;
			$this->melder = $new_contactID;
			$this->melder_contactID
					= $this->melder->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->melder == NULL && $this->melder_contactID == NULL)
				return $this;
			$this->melder = NULL;
			$this->melder_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->melder == NULL 
				&& $this->melder_contactID == (int)$new_contactID)
				return $this;
			$this->melder = NULL;
			$this->melder_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld melder geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld melder geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMelder()
	{
		if (array_key_exists('Melder', $this->errors))
			return $this->errors['Melder'];
		$waarde1 = $this->getMelder();
		$waarde2 = $this->getMelderContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld melder_contactID.
	 *
	 * @return int
	 * De waarde van het veld melder_contactID.
	 */
	public function getMelderContactID()
	{
		if (is_null($this->melder_contactID) && isset($this->melder)) {
			$this->melder_contactID = $this->melder->getContactID();
		}
		return $this->melder_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld melderEmail.
	 *
	 * @return string
	 * De waarde van het veld melderEmail.
	 */
	public function getMelderEmail()
	{
		return $this->melderEmail;
	}
	/**
	 * @brief Stel de waarde van het veld melderEmail in.
	 *
	 * @param mixed $newMelderEmail De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setMelderEmail($newMelderEmail)
	{
		unset($this->errors['MelderEmail']);
		if(!is_null($newMelderEmail))
			$newMelderEmail = trim($newMelderEmail);
		if($newMelderEmail === "")
			$newMelderEmail = NULL;
		if($this->melderEmail === $newMelderEmail)
			return $this;

		$this->melderEmail = $newMelderEmail;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld melderEmail geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld melderEmail geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMelderEmail()
	{
		if (array_key_exists('MelderEmail', $this->errors))
			return $this->errors['MelderEmail'];
		$waarde = $this->getMelderEmail();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setTitel($newTitel)
	{
		unset($this->errors['Titel']);
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel === $newTitel)
			return $this;

		$this->titel = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel()
	{
		if (array_key_exists('Titel', $this->errors))
			return $this->errors['Titel'];
		$waarde = $this->getTitel();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @return BugCategorie
	 * De waarde van het veld categorie.
	 */
	public function getCategorie()
	{
		if(!isset($this->categorie)
		 && isset($this->categorie_bugCategorieID)
		 ) {
			$this->categorie = BugCategorie::geef
					( $this->categorie_bugCategorieID
					);
		}
		return $this->categorie;
	}
	/**
	 * @brief Stel de waarde van het veld categorie in.
	 *
	 * @param mixed $new_bugCategorieID De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setCategorie($new_bugCategorieID)
	{
		unset($this->errors['Categorie']);
		if($new_bugCategorieID instanceof BugCategorie
		) {
			if($this->categorie == $new_bugCategorieID
			&& $this->categorie_bugCategorieID == $this->categorie->getBugCategorieID())
				return $this;
			$this->categorie = $new_bugCategorieID;
			$this->categorie_bugCategorieID
					= $this->categorie->getBugCategorieID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_bugCategorieID)
		) {
			if($this->categorie == NULL 
				&& $this->categorie_bugCategorieID == (int)$new_bugCategorieID)
				return $this;
			$this->categorie = NULL;
			$this->categorie_bugCategorieID
					= (int)$new_bugCategorieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld categorie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld categorie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCategorie()
	{
		if (array_key_exists('Categorie', $this->errors))
			return $this->errors['Categorie'];
		$waarde1 = $this->getCategorie();
		$waarde2 = $this->getCategorieBugCategorieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld categorie_bugCategorieID.
	 *
	 * @return int
	 * De waarde van het veld categorie_bugCategorieID.
	 */
	public function getCategorieBugCategorieID()
	{
		if (is_null($this->categorie_bugCategorieID) && isset($this->categorie)) {
			$this->categorie_bugCategorieID = $this->categorie->getBugCategorieID();
		}
		return $this->categorie_bugCategorieID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('OPEN','BEZIG','OPGELOST','GECOMMIT','UITGESTELD','GESLOTEN','BOGUS','DUBBEL','WANNAHAVE','FEEDBACK','WONTFIX');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld prioriteit.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld prioriteit.
	 */
	static public function enumsPrioriteit()
	{
		static $vals = array('ONBENULLIG','LAAG','GEMIDDELD','HOOG','URGENT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld prioriteit.
	 *
	 * @return string
	 * De waarde van het veld prioriteit.
	 */
	public function getPrioriteit()
	{
		return $this->prioriteit;
	}
	/**
	 * @brief Stel de waarde van het veld prioriteit in.
	 *
	 * @param mixed $newPrioriteit De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setPrioriteit($newPrioriteit)
	{
		unset($this->errors['Prioriteit']);
		if(!is_null($newPrioriteit))
			$newPrioriteit = strtoupper(trim($newPrioriteit));
		if($newPrioriteit === "")
			$newPrioriteit = NULL;
		if($this->prioriteit === $newPrioriteit)
			return $this;

		$this->prioriteit = $newPrioriteit;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld prioriteit geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld prioriteit geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPrioriteit()
	{
		if (array_key_exists('Prioriteit', $this->errors))
			return $this->errors['Prioriteit'];
		$waarde = $this->getPrioriteit();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsPrioriteit()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld niveau.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld niveau.
	 */
	static public function enumsNiveau()
	{
		static $vals = array('OPEN','INGELOGD','CIE');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld niveau.
	 *
	 * @return string
	 * De waarde van het veld niveau.
	 */
	public function getNiveau()
	{
		return $this->niveau;
	}
	/**
	 * @brief Stel de waarde van het veld niveau in.
	 *
	 * @param mixed $newNiveau De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setNiveau($newNiveau)
	{
		unset($this->errors['Niveau']);
		if(!is_null($newNiveau))
			$newNiveau = strtoupper(trim($newNiveau));
		if($newNiveau === "")
			$newNiveau = NULL;
		if($this->niveau === $newNiveau)
			return $this;

		$this->niveau = $newNiveau;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld niveau geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld niveau geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNiveau()
	{
		if (array_key_exists('Niveau', $this->errors))
			return $this->errors['Niveau'];
		$waarde = $this->getNiveau();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsNiveau()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld moment.
	 */
	public function getMoment()
	{
		return $this->moment;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @return string
	 * De waarde van het veld type.
	 */
	public function getType()
	{
		return $this->type;
	}
	/**
	 * @brief Stel de waarde van het veld type in.
	 *
	 * @param mixed $newType De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setType($newType)
	{
		unset($this->errors['Type']);
		if(!is_null($newType))
			$newType = trim($newType);
		if($newType === "")
			$newType = NULL;
		if($this->type === $newType)
			return $this;

		$this->type = $newType;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld type geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld type geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkType()
	{
		if (array_key_exists('Type', $this->errors))
			return $this->errors['Type'];
		$waarde = $this->getType();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld level.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld level.
	 */
	static public function enumsLevel()
	{
		static $vals = array('BEGINNER','GEMIDDELD','EXPERT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld level.
	 *
	 * @return string
	 * De waarde van het veld level.
	 */
	public function getLevel()
	{
		return $this->level;
	}
	/**
	 * @brief Stel de waarde van het veld level in.
	 *
	 * @param mixed $newLevel De nieuwe waarde.
	 *
	 * @return Bug
	 * Dit Bug-object.
	 */
	public function setLevel($newLevel)
	{
		unset($this->errors['Level']);
		if(!is_null($newLevel))
			$newLevel = strtoupper(trim($newLevel));
		if($newLevel === "")
			$newLevel = NULL;
		if($this->level === $newLevel)
			return $this;

		$this->level = $newLevel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld level geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld level geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLevel()
	{
		if (array_key_exists('Level', $this->errors))
			return $this->errors['Level'];
		$waarde = $this->getLevel();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsLevel()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Returneert de BugBerichtVerzameling die hoort bij dit object.
	 */
	public function getBugBerichtVerzameling()
	{
		if(!$this->bugBerichtVerzameling instanceof BugBerichtVerzameling)
			$this->bugBerichtVerzameling = BugBerichtVerzameling::fromBug($this);
		return $this->bugBerichtVerzameling;
	}
	/**
	 * @brief Returneert de BugToewijzingVerzameling die hoort bij dit object.
	 */
	public function getBugToewijzingVerzameling()
	{
		if(!$this->bugToewijzingVerzameling instanceof BugToewijzingVerzameling)
			$this->bugToewijzingVerzameling = BugToewijzingVerzameling::fromBug($this);
		return $this->bugToewijzingVerzameling;
	}
	/**
	 * @brief Returneert de BugVolgerVerzameling die hoort bij dit object.
	 */
	public function getBugVolgerVerzameling()
	{
		if(!$this->bugVolgerVerzameling instanceof BugVolgerVerzameling)
			$this->bugVolgerVerzameling = BugVolgerVerzameling::fromBug($this);
		return $this->bugVolgerVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Bug::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Bug.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Bug::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getBugID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Bug|false
	 * Een Bug-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$bugID = (int)$a[0];
		}
		else if(isset($a))
		{
			$bugID = (int)$a;
		}

		if(is_null($bugID))
			throw new BadMethodCallException();

		static::cache(array( array($bugID) ));
		return Entiteit::geefCache(array($bugID), 'Bug');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Bug');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Bug::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Bug`.`bugID`'
		                 .     ', `Bug`.`melder_contactID`'
		                 .     ', `Bug`.`melderEmail`'
		                 .     ', `Bug`.`titel`'
		                 .     ', `Bug`.`categorie_bugCategorieID`'
		                 .     ', `Bug`.`status`'
		                 .     ', `Bug`.`prioriteit`'
		                 .     ', `Bug`.`niveau`'
		                 .     ', `Bug`.`moment`'
		                 .     ', `Bug`.`type`'
		                 .     ', `Bug`.`level`'
		                 .     ', `Bug`.`gewijzigdWanneer`'
		                 .     ', `Bug`.`gewijzigdWie`'
		                 .' FROM `Bug`'
		                 .' WHERE (`bugID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['bugID']);

			$obj = new Bug($row['moment']);

			$obj->inDB = True;

			$obj->bugID  = (int) $row['bugID'];
			$obj->melder_contactID  = (is_null($row['melder_contactID'])) ? null : (int) $row['melder_contactID'];
			$obj->melderEmail  = (is_null($row['melderEmail'])) ? null : trim($row['melderEmail']);
			$obj->titel  = trim($row['titel']);
			$obj->categorie_bugCategorieID  = (int) $row['categorie_bugCategorieID'];
			$obj->status  = strtoupper(trim($row['status']));
			$obj->prioriteit  = strtoupper(trim($row['prioriteit']));
			$obj->niveau  = strtoupper(trim($row['niveau']));
			$obj->type  = trim($row['type']);
			$obj->level  = strtoupper(trim($row['level']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Bug')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getMelderContactID();
		$this->getCategorieBugCategorieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->bugID =
			$WSW4DB->q('RETURNID INSERT INTO `Bug`'
			          . ' (`melder_contactID`, `melderEmail`, `titel`, `categorie_bugCategorieID`, `status`, `prioriteit`, `niveau`, `moment`, `type`, `level`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i, %s, %s, %s, %s, %s, %s, %s, %i)'
			          , $this->melder_contactID
			          , $this->melderEmail
			          , $this->titel
			          , $this->categorie_bugCategorieID
			          , $this->status
			          , $this->prioriteit
			          , $this->niveau
			          , $this->moment->strftime('%F %T')
			          , $this->type
			          , $this->level
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Bug')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Bug`'
			          .' SET `melder_contactID` = %i'
			          .   ', `melderEmail` = %s'
			          .   ', `titel` = %s'
			          .   ', `categorie_bugCategorieID` = %i'
			          .   ', `status` = %s'
			          .   ', `prioriteit` = %s'
			          .   ', `niveau` = %s'
			          .   ', `moment` = %s'
			          .   ', `type` = %s'
			          .   ', `level` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugID` = %i'
			          , $this->melder_contactID
			          , $this->melderEmail
			          , $this->titel
			          , $this->categorie_bugCategorieID
			          , $this->status
			          , $this->prioriteit
			          , $this->niveau
			          , $this->moment->strftime('%F %T')
			          , $this->type
			          , $this->level
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBug
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBug($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBug($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Type';
			$velden[] = 'Level';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Type';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Moment';
			$velden[] = 'Type';
			$velden[] = 'Level';
			break;
		case 'get':
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Moment';
			$velden[] = 'Type';
			$velden[] = 'Level';
		case 'primary':
			$velden[] = 'melder_contactID';
			$velden[] = 'categorie_bugCategorieID';
			break;
		case 'verzamelingen':
			$velden[] = 'BugBerichtVerzameling';
			$velden[] = 'BugToewijzingVerzameling';
			$velden[] = 'BugVolgerVerzameling';
			break;
		default:
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Moment';
			$velden[] = 'Type';
			$velden[] = 'Level';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle BugBericht-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugBerichtFromBugVerz = BugBerichtVerzameling::fromBug($this);
		$returnValue = $BugBerichtFromBugVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugBericht met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugToewijzing-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugToewijzingFromBugVerz = BugToewijzingVerzameling::fromBug($this);
		$returnValue = $BugToewijzingFromBugVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugToewijzing met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugVolger-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugVolgerFromBugVerz = BugVolgerVerzameling::fromBug($this);
		$returnValue = $BugVolgerFromBugVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugVolger met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $BugBerichtFromBugVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BugToewijzingFromBugVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BugVolgerFromBugVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Bug`'
		          .' WHERE `bugID` = %i'
		          .' LIMIT 1'
		          , $this->bugID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->bugID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Bug`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Bug terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bugid':
			return 'int';
		case 'melder':
			return 'foreign';
		case 'melder_contactid':
			return 'int';
		case 'melderemail':
			return 'string';
		case 'titel':
			return 'string';
		case 'categorie':
			return 'foreign';
		case 'categorie_bugcategorieid':
			return 'int';
		case 'status':
			return 'enum';
		case 'prioriteit':
			return 'enum';
		case 'niveau':
			return 'enum';
		case 'moment':
			return 'datetime';
		case 'type':
			return 'string';
		case 'level':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bugID':
		case 'melder_contactID':
		case 'categorie_bugCategorieID':
			$type = '%i';
			break;
		case 'melderEmail':
		case 'titel':
		case 'status':
		case 'prioriteit':
		case 'niveau':
		case 'moment':
		case 'type':
		case 'level':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Bug`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `bugID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->bugID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Bug`'
		          .' WHERE `bugID` = %i'
		                 , $veld
		          , $this->bugID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Bug');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle BugBericht-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugBerichtFromBugVerz = BugBerichtVerzameling::fromBug($this);
		$dependencies['BugBericht'] = $BugBerichtFromBugVerz;

		// Verzamel alle BugToewijzing-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugToewijzingFromBugVerz = BugToewijzingVerzameling::fromBug($this);
		$dependencies['BugToewijzing'] = $BugToewijzingFromBugVerz;

		// Verzamel alle BugVolger-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugVolgerFromBugVerz = BugVolgerVerzameling::fromBug($this);
		$dependencies['BugVolger'] = $BugVolgerFromBugVerz;

		return $dependencies;
	}
}
