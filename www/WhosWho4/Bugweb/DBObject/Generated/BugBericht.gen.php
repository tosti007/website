<?
abstract class BugBericht_Generated
	extends Entiteit
{
	protected $bugBerichtID;			/**< \brief PRIMARY */
	protected $bug;
	protected $bug_bugID;				/**< \brief PRIMARY */
	protected $melder;					/**< \brief NULL */
	protected $melder_contactID;		/**< \brief PRIMARY */
	protected $melderEmail;				/**< \brief NULL */
	protected $titel;					/**< \brief NULL */
	protected $categorie;				/**< \brief NULL */
	protected $categorie_bugCategorieID;/**< \brief PRIMARY */
	protected $status;					/**< \brief ENUM:OPEN/BEZIG/OPGELOST/GECOMMIT/UITGESTELD/GESLOTEN/BOGUS/DUBBEL/WANNAHAVE/FEEDBACK/WONTFIX */
	protected $prioriteit;				/**< \brief ENUM:ONBENULLIG/LAAG/GEMIDDELD/HOOG/URGENT */
	protected $niveau;					/**< \brief ENUM:OPEN/INGELOGD/CIE */
	protected $bericht;
	protected $moment;
	protected $revisie;					/**< \brief NULL */
	protected $level;					/**< \brief ENUM:BEGINNER/GEMIDDELD/EXPERT */
	protected $commit;					/**< \brief NULL */
	/** Verzamelingen **/
	protected $bugToewijzingVerzameling;
	/**
	/**
	 * @brief De constructor van de BugBericht_Generated-klasse.
	 *
	 * @param mixed $a Bug (Bug OR Array(bug_bugID) OR bug_bugID)
	 * @param mixed $b Titel (string)
	 * @param mixed $c Categorie (BugCategorie OR Array(bugCategorie_bugCategorieID) OR
	 * bugCategorie_bugCategorieID)
	 * @param mixed $d Status (enum)
	 * @param mixed $e Prioriteit (enum)
	 * @param mixed $f Niveau (enum)
	 * @param mixed $g Bericht (text)
	 * @param mixed $h Moment (datetime)
	 * @param mixed $i Revisie (int)
	 * @param mixed $j Level (enum)
	 * @param mixed $k Commit (string)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = NULL,
			$d = 'OPEN',
			$e = 'ONBENULLIG',
			$f = 'OPEN',
			$g = '',
			$h = NULL,
			$i = NULL,
			$j = 'GEMIDDELD',
			$k = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Bug)
		{
			$this->bug = $a;
			$this->bug_bugID = $a->getBugID();
		}
		else if(is_array($a))
		{
			$this->bug = NULL;
			$this->bug_bugID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->bug = NULL;
			$this->bug_bugID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$b = trim($b);
		$this->titel = $b;

		if(is_array($c) && is_null($c[0]))
			$c = NULL;

		if($c instanceof BugCategorie)
		{
			$this->categorie = $c;
			$this->categorie_bugCategorieID = $c->getBugCategorieID();
		}
		else if(is_array($c))
		{
			$this->categorie = NULL;
			$this->categorie_bugCategorieID = (int)$c[0];
		}
		else if(isset($c))
		{
			$this->categorie = NULL;
			$this->categorie_bugCategorieID = (int)$c;
		}
		else
		{
			$this->categorie = NULL;
			$this->categorie_bugCategorieID = NULL;
		}

		$d = strtoupper(trim($d));
		if(!in_array($d, static::enumsStatus()))
			throw new BadMethodCallException();
		$this->status = $d;

		$e = strtoupper(trim($e));
		if(!in_array($e, static::enumsPrioriteit()))
			throw new BadMethodCallException();
		$this->prioriteit = $e;

		$f = strtoupper(trim($f));
		if(!in_array($f, static::enumsNiveau()))
			throw new BadMethodCallException();
		$this->niveau = $f;

		$g = trim($g);
		if(is_null($g))
			$g = "";
		$this->bericht = $g;

		if(!$h instanceof DateTimeLocale)
		{
			if(is_null($h))
				$h = new DateTimeLocale();
			else
			{
				try {
					$h = new DateTimeLocale($h);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->moment = $h;

		$i = (int)$i;
		$this->revisie = $i;

		$j = strtoupper(trim($j));
		if(!in_array($j, static::enumsLevel()))
			throw new BadMethodCallException();
		$this->level = $j;

		$k = trim($k);
		$this->commit = $k;

		$this->bugBerichtID = NULL;
		$this->melder = NULL;
		$this->melder_contactID = NULL;
		$this->melderEmail = NULL;
		$this->bugToewijzingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Bug';
		$volgorde[] = 'Titel';
		$volgorde[] = 'Categorie';
		$volgorde[] = 'Status';
		$volgorde[] = 'Prioriteit';
		$volgorde[] = 'Niveau';
		$volgorde[] = 'Bericht';
		$volgorde[] = 'Moment';
		$volgorde[] = 'Revisie';
		$volgorde[] = 'Level';
		$volgorde[] = 'Commit';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld bugBerichtID.
	 *
	 * @return int
	 * De waarde van het veld bugBerichtID.
	 */
	public function getBugBerichtID()
	{
		return $this->bugBerichtID;
	}
	/**
	 * @brief Geef de waarde van het veld bug.
	 *
	 * @return Bug
	 * De waarde van het veld bug.
	 */
	public function getBug()
	{
		if(!isset($this->bug)
		 && isset($this->bug_bugID)
		 ) {
			$this->bug = Bug::geef
					( $this->bug_bugID
					);
		}
		return $this->bug;
	}
	/**
	 * @brief Geef de waarde van het veld bug_bugID.
	 *
	 * @return int
	 * De waarde van het veld bug_bugID.
	 */
	public function getBugBugID()
	{
		if (is_null($this->bug_bugID) && isset($this->bug)) {
			$this->bug_bugID = $this->bug->getBugID();
		}
		return $this->bug_bugID;
	}
	/**
	 * @brief Geef de waarde van het veld melder.
	 *
	 * @return Persoon
	 * De waarde van het veld melder.
	 */
	public function getMelder()
	{
		if(!isset($this->melder)
		 && isset($this->melder_contactID)
		 ) {
			$this->melder = Persoon::geef
					( $this->melder_contactID
					);
		}
		return $this->melder;
	}
	/**
	 * @brief Stel de waarde van het veld melder in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return BugBericht
	 * Dit BugBericht-object.
	 */
	public function setMelder($new_contactID)
	{
		unset($this->errors['Melder']);
		if($new_contactID instanceof Persoon
		) {
			if($this->melder == $new_contactID
			&& $this->melder_contactID == $this->melder->getContactID())
				return $this;
			$this->melder = $new_contactID;
			$this->melder_contactID
					= $this->melder->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->melder == NULL && $this->melder_contactID == NULL)
				return $this;
			$this->melder = NULL;
			$this->melder_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->melder == NULL 
				&& $this->melder_contactID == (int)$new_contactID)
				return $this;
			$this->melder = NULL;
			$this->melder_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld melder geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld melder geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMelder()
	{
		if (array_key_exists('Melder', $this->errors))
			return $this->errors['Melder'];
		$waarde1 = $this->getMelder();
		$waarde2 = $this->getMelderContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld melder_contactID.
	 *
	 * @return int
	 * De waarde van het veld melder_contactID.
	 */
	public function getMelderContactID()
	{
		if (is_null($this->melder_contactID) && isset($this->melder)) {
			$this->melder_contactID = $this->melder->getContactID();
		}
		return $this->melder_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld melderEmail.
	 *
	 * @return string
	 * De waarde van het veld melderEmail.
	 */
	public function getMelderEmail()
	{
		return $this->melderEmail;
	}
	/**
	 * @brief Stel de waarde van het veld melderEmail in.
	 *
	 * @param mixed $newMelderEmail De nieuwe waarde.
	 *
	 * @return BugBericht
	 * Dit BugBericht-object.
	 */
	public function setMelderEmail($newMelderEmail)
	{
		unset($this->errors['MelderEmail']);
		if(!is_null($newMelderEmail))
			$newMelderEmail = trim($newMelderEmail);
		if($newMelderEmail === "")
			$newMelderEmail = NULL;
		if($this->melderEmail === $newMelderEmail)
			return $this;

		$this->melderEmail = $newMelderEmail;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld melderEmail geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld melderEmail geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMelderEmail()
	{
		if (array_key_exists('MelderEmail', $this->errors))
			return $this->errors['MelderEmail'];
		$waarde = $this->getMelderEmail();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @return BugCategorie
	 * De waarde van het veld categorie.
	 */
	public function getCategorie()
	{
		if(!isset($this->categorie)
		 && isset($this->categorie_bugCategorieID)
		 ) {
			$this->categorie = BugCategorie::geef
					( $this->categorie_bugCategorieID
					);
		}
		return $this->categorie;
	}
	/**
	 * @brief Geef de waarde van het veld categorie_bugCategorieID.
	 *
	 * @return int
	 * De waarde van het veld categorie_bugCategorieID.
	 */
	public function getCategorieBugCategorieID()
	{
		if (is_null($this->categorie_bugCategorieID) && isset($this->categorie)) {
			$this->categorie_bugCategorieID = $this->categorie->getBugCategorieID();
		}
		return $this->categorie_bugCategorieID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('OPEN','BEZIG','OPGELOST','GECOMMIT','UITGESTELD','GESLOTEN','BOGUS','DUBBEL','WANNAHAVE','FEEDBACK','WONTFIX');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld prioriteit.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld prioriteit.
	 */
	static public function enumsPrioriteit()
	{
		static $vals = array('ONBENULLIG','LAAG','GEMIDDELD','HOOG','URGENT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld prioriteit.
	 *
	 * @return string
	 * De waarde van het veld prioriteit.
	 */
	public function getPrioriteit()
	{
		return $this->prioriteit;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld niveau.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld niveau.
	 */
	static public function enumsNiveau()
	{
		static $vals = array('OPEN','INGELOGD','CIE');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld niveau.
	 *
	 * @return string
	 * De waarde van het veld niveau.
	 */
	public function getNiveau()
	{
		return $this->niveau;
	}
	/**
	 * @brief Geef de waarde van het veld bericht.
	 *
	 * @return string
	 * De waarde van het veld bericht.
	 */
	public function getBericht()
	{
		return $this->bericht;
	}
	/**
	 * @brief Geef de waarde van het veld moment.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld moment.
	 */
	public function getMoment()
	{
		return $this->moment;
	}
	/**
	 * @brief Geef de waarde van het veld revisie.
	 *
	 * @return int
	 * De waarde van het veld revisie.
	 */
	public function getRevisie()
	{
		return $this->revisie;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld level.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld level.
	 */
	static public function enumsLevel()
	{
		static $vals = array('BEGINNER','GEMIDDELD','EXPERT');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld level.
	 *
	 * @return string
	 * De waarde van het veld level.
	 */
	public function getLevel()
	{
		return $this->level;
	}
	/**
	 * @brief Geef de waarde van het veld commit.
	 *
	 * @return string
	 * De waarde van het veld commit.
	 */
	public function getCommit()
	{
		return $this->commit;
	}
	/**
	 * @brief Returneert de BugToewijzingVerzameling die hoort bij dit object.
	 */
	public function getBugToewijzingVerzameling()
	{
		if(!$this->bugToewijzingVerzameling instanceof BugToewijzingVerzameling)
		{
			$this->bugToewijzingVerzameling = new BugToewijzingVerzameling();
			$this->bugToewijzingVerzameling->union(BugToewijzingVerzameling::fromBegin($this));
			$this->bugToewijzingVerzameling->union(BugToewijzingVerzameling::fromEind($this));
		}
		return $this->bugToewijzingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return BugBericht::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van BugBericht.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return BugBericht::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getBugBerichtID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return BugBericht|false
	 * Een BugBericht-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$bugBerichtID = (int)$a[0];
		}
		else if(isset($a))
		{
			$bugBerichtID = (int)$a;
		}

		if(is_null($bugBerichtID))
			throw new BadMethodCallException();

		static::cache(array( array($bugBerichtID) ));
		return Entiteit::geefCache(array($bugBerichtID), 'BugBericht');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'BugBericht');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('BugBericht::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `BugBericht`.`bugBerichtID`'
		                 .     ', `BugBericht`.`bug_bugID`'
		                 .     ', `BugBericht`.`melder_contactID`'
		                 .     ', `BugBericht`.`melderEmail`'
		                 .     ', `BugBericht`.`titel`'
		                 .     ', `BugBericht`.`categorie_bugCategorieID`'
		                 .     ', `BugBericht`.`status`'
		                 .     ', `BugBericht`.`prioriteit`'
		                 .     ', `BugBericht`.`niveau`'
		                 .     ', `BugBericht`.`bericht`'
		                 .     ', `BugBericht`.`moment`'
		                 .     ', `BugBericht`.`revisie`'
		                 .     ', `BugBericht`.`level`'
		                 .     ', `BugBericht`.`commit`'
		                 .     ', `BugBericht`.`gewijzigdWanneer`'
		                 .     ', `BugBericht`.`gewijzigdWie`'
		                 .' FROM `BugBericht`'
		                 .' WHERE (`bugBerichtID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['bugBerichtID']);

			$obj = new BugBericht(array($row['bug_bugID']), $row['titel'], array($row['categorie_bugCategorieID']), $row['status'], $row['prioriteit'], $row['niveau'], $row['bericht'], $row['moment'], $row['revisie'], $row['level'], $row['commit']);

			$obj->inDB = True;

			$obj->bugBerichtID  = (int) $row['bugBerichtID'];
			$obj->melder_contactID  = (is_null($row['melder_contactID'])) ? null : (int) $row['melder_contactID'];
			$obj->melderEmail  = (is_null($row['melderEmail'])) ? null : trim($row['melderEmail']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'BugBericht')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getBugBugID();
		$this->getMelderContactID();
		$this->getCategorieBugCategorieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->bugBerichtID =
			$WSW4DB->q('RETURNID INSERT INTO `BugBericht`'
			          . ' (`bug_bugID`, `melder_contactID`, `melderEmail`, `titel`, `categorie_bugCategorieID`, `status`, `prioriteit`, `niveau`, `bericht`, `moment`, `revisie`, `level`, `commit`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i, %s, %s, %s, %s, %s, %i, %s, %s, %s, %i)'
			          , $this->bug_bugID
			          , $this->melder_contactID
			          , $this->melderEmail
			          , $this->titel
			          , $this->categorie_bugCategorieID
			          , $this->status
			          , $this->prioriteit
			          , $this->niveau
			          , $this->bericht
			          , $this->moment->strftime('%F %T')
			          , $this->revisie
			          , $this->level
			          , $this->commit
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'BugBericht')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `BugBericht`'
			          .' SET `bug_bugID` = %i'
			          .   ', `melder_contactID` = %i'
			          .   ', `melderEmail` = %s'
			          .   ', `titel` = %s'
			          .   ', `categorie_bugCategorieID` = %i'
			          .   ', `status` = %s'
			          .   ', `prioriteit` = %s'
			          .   ', `niveau` = %s'
			          .   ', `bericht` = %s'
			          .   ', `moment` = %s'
			          .   ', `revisie` = %i'
			          .   ', `level` = %s'
			          .   ', `commit` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugBerichtID` = %i'
			          , $this->bug_bugID
			          , $this->melder_contactID
			          , $this->melderEmail
			          , $this->titel
			          , $this->categorie_bugCategorieID
			          , $this->status
			          , $this->prioriteit
			          , $this->niveau
			          , $this->bericht
			          , $this->moment->strftime('%F %T')
			          , $this->revisie
			          , $this->level
			          , $this->commit
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugBerichtID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBugBericht
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBugBericht($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBugBericht($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Bug';
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Bericht';
			$velden[] = 'Moment';
			$velden[] = 'Revisie';
			$velden[] = 'Level';
			$velden[] = 'Commit';
			break;
		case 'get':
			$velden[] = 'Bug';
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Bericht';
			$velden[] = 'Moment';
			$velden[] = 'Revisie';
			$velden[] = 'Level';
			$velden[] = 'Commit';
		case 'primary':
			$velden[] = 'bug_bugID';
			$velden[] = 'melder_contactID';
			$velden[] = 'categorie_bugCategorieID';
			break;
		case 'verzamelingen':
			$velden[] = 'BugToewijzingVerzameling';
			break;
		default:
			$velden[] = 'Bug';
			$velden[] = 'Melder';
			$velden[] = 'MelderEmail';
			$velden[] = 'Titel';
			$velden[] = 'Categorie';
			$velden[] = 'Status';
			$velden[] = 'Prioriteit';
			$velden[] = 'Niveau';
			$velden[] = 'Bericht';
			$velden[] = 'Moment';
			$velden[] = 'Revisie';
			$velden[] = 'Level';
			$velden[] = 'Commit';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle BugToewijzing-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugToewijzingFromBeginVerz = BugToewijzingVerzameling::fromBegin($this);
		$returnValue = $BugToewijzingFromBeginVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugToewijzing met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugToewijzing-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$BugToewijzingFromEindVerz = BugToewijzingVerzameling::fromEind($this);
		$returnValue = $BugToewijzingFromEindVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugToewijzing met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($BugToewijzingFromEindVerz as $v)
		{
			$v->setEind(NULL);
		}
		$BugToewijzingFromEindVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $BugToewijzingFromBeginVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `BugBericht`'
		          .' WHERE `bugBerichtID` = %i'
		          .' LIMIT 1'
		          , $this->bugBerichtID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->bugBerichtID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `BugBericht`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugBerichtID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugBerichtID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van BugBericht terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bugberichtid':
			return 'int';
		case 'bug':
			return 'foreign';
		case 'bug_bugid':
			return 'int';
		case 'melder':
			return 'foreign';
		case 'melder_contactid':
			return 'int';
		case 'melderemail':
			return 'string';
		case 'titel':
			return 'string';
		case 'categorie':
			return 'foreign';
		case 'categorie_bugcategorieid':
			return 'int';
		case 'status':
			return 'enum';
		case 'prioriteit':
			return 'enum';
		case 'niveau':
			return 'enum';
		case 'bericht':
			return 'text';
		case 'moment':
			return 'datetime';
		case 'revisie':
			return 'int';
		case 'level':
			return 'enum';
		case 'commit':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bugBerichtID':
		case 'bug_bugID':
		case 'melder_contactID':
		case 'categorie_bugCategorieID':
		case 'revisie':
			$type = '%i';
			break;
		case 'melderEmail':
		case 'titel':
		case 'status':
		case 'prioriteit':
		case 'niveau':
		case 'bericht':
		case 'moment':
		case 'level':
		case 'commit':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `BugBericht`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `bugBerichtID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->bugBerichtID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `BugBericht`'
		          .' WHERE `bugBerichtID` = %i'
		                 , $veld
		          , $this->bugBerichtID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'BugBericht');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle BugToewijzing-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugToewijzingFromBeginVerz = BugToewijzingVerzameling::fromBegin($this);
		$dependencies['BugToewijzing'] = $BugToewijzingFromBeginVerz;

		// Verzamel alle BugToewijzing-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugToewijzingFromEindVerz = BugToewijzingVerzameling::fromEind($this);
		$dependencies['BugToewijzing'] = $BugToewijzingFromEindVerz;

		return $dependencies;
	}
}
