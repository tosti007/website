<?
/**
 * @brief 'AT'AUTH_GET:actief,AUTH_SET:bestuur
 */
abstract class BugCategorie_Generated
	extends Entiteit
{
	protected $bugCategorieID;			/**< \brief PRIMARY */
	protected $naam;
	protected $commissie;
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $actief;
	/** Verzamelingen **/
	protected $bugVerzameling;
	protected $bugBerichtVerzameling;
	protected $persoonVoorkeurVerzameling;
	/**
	 * @brief De constructor van de BugCategorie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->bugCategorieID = NULL;
		$this->naam = '';
		$this->commissie = NULL;
		$this->commissie_commissieID = 0;
		$this->actief = False;
		$this->bugVerzameling = NULL;
		$this->bugBerichtVerzameling = NULL;
		$this->persoonVoorkeurVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld bugCategorieID.
	 *
	 * @return int
	 * De waarde van het veld bugCategorieID.
	 */
	public function getBugCategorieID()
	{
		return $this->bugCategorieID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return BugCategorie
	 * Dit BugCategorie-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return BugCategorie
	 * Dit BugCategorie-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld actief.
	 *
	 * @return bool
	 * De waarde van het veld actief.
	 */
	public function getActief()
	{
		return $this->actief;
	}
	/**
	 * @brief Stel de waarde van het veld actief in.
	 *
	 * @param mixed $newActief De nieuwe waarde.
	 *
	 * @return BugCategorie
	 * Dit BugCategorie-object.
	 */
	public function setActief($newActief)
	{
		unset($this->errors['Actief']);
		if(!is_null($newActief))
			$newActief = (bool)$newActief;
		if($this->actief === $newActief)
			return $this;

		$this->actief = $newActief;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld actief geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld actief geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkActief()
	{
		if (array_key_exists('Actief', $this->errors))
			return $this->errors['Actief'];
		$waarde = $this->getActief();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de BugVerzameling die hoort bij dit object.
	 */
	public function getBugVerzameling()
	{
		if(!$this->bugVerzameling instanceof BugVerzameling)
			$this->bugVerzameling = BugVerzameling::fromCategorie($this);
		return $this->bugVerzameling;
	}
	/**
	 * @brief Returneert de BugBerichtVerzameling die hoort bij dit object.
	 */
	public function getBugBerichtVerzameling()
	{
		if(!$this->bugBerichtVerzameling instanceof BugBerichtVerzameling)
			$this->bugBerichtVerzameling = BugBerichtVerzameling::fromCategorie($this);
		return $this->bugBerichtVerzameling;
	}
	/**
	 * @brief Returneert de PersoonVoorkeurVerzameling die hoort bij dit object.
	 */
	public function getPersoonVoorkeurVerzameling()
	{
		if(!$this->persoonVoorkeurVerzameling instanceof PersoonVoorkeurVerzameling)
			$this->persoonVoorkeurVerzameling = PersoonVoorkeurVerzameling::fromFavoBugCategorie($this);
		return $this->persoonVoorkeurVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('actief');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return BugCategorie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van BugCategorie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return BugCategorie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getBugCategorieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return BugCategorie|false
	 * Een BugCategorie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$bugCategorieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$bugCategorieID = (int)$a;
		}

		if(is_null($bugCategorieID))
			throw new BadMethodCallException();

		static::cache(array( array($bugCategorieID) ));
		return Entiteit::geefCache(array($bugCategorieID), 'BugCategorie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'BugCategorie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('BugCategorie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `BugCategorie`.`bugCategorieID`'
		                 .     ', `BugCategorie`.`naam`'
		                 .     ', `BugCategorie`.`commissie_commissieID`'
		                 .     ', `BugCategorie`.`actief`'
		                 .     ', `BugCategorie`.`gewijzigdWanneer`'
		                 .     ', `BugCategorie`.`gewijzigdWie`'
		                 .' FROM `BugCategorie`'
		                 .' WHERE (`bugCategorieID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['bugCategorieID']);

			$obj = new BugCategorie();

			$obj->inDB = True;

			$obj->bugCategorieID  = (int) $row['bugCategorieID'];
			$obj->naam  = trim($row['naam']);
			$obj->commissie_commissieID  = (int) $row['commissie_commissieID'];
			$obj->actief  = (bool) $row['actief'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'BugCategorie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getCommissieCommissieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->bugCategorieID =
			$WSW4DB->q('RETURNID INSERT INTO `BugCategorie`'
			          . ' (`naam`, `commissie_commissieID`, `actief`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %i, %s, %i)'
			          , $this->naam
			          , $this->commissie_commissieID
			          , $this->actief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'BugCategorie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `BugCategorie`'
			          .' SET `naam` = %s'
			          .   ', `commissie_commissieID` = %i'
			          .   ', `actief` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugCategorieID` = %i'
			          , $this->naam
			          , $this->commissie_commissieID
			          , $this->actief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugCategorieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBugCategorie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBugCategorie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBugCategorie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Commissie';
			$velden[] = 'Actief';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			$velden[] = 'Commissie';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Commissie';
			$velden[] = 'Actief';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Commissie';
			$velden[] = 'Actief';
		case 'primary':
			$velden[] = 'commissie_commissieID';
			break;
		case 'verzamelingen':
			$velden[] = 'BugVerzameling';
			$velden[] = 'BugBerichtVerzameling';
			$velden[] = 'PersoonVoorkeurVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Commissie';
			$velden[] = 'Actief';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Bug-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugFromCategorieVerz = BugVerzameling::fromCategorie($this);
		$returnValue = $BugFromCategorieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Bug met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BugBericht-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BugBerichtFromCategorieVerz = BugBerichtVerzameling::fromCategorie($this);
		$returnValue = $BugBerichtFromCategorieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BugBericht met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle PersoonVoorkeur-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$PersoonVoorkeurFromFavoBugCategorieVerz = PersoonVoorkeurVerzameling::fromFavoBugCategorie($this);
		$returnValue = $PersoonVoorkeurFromFavoBugCategorieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object PersoonVoorkeur met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($PersoonVoorkeurFromFavoBugCategorieVerz as $v)
		{
			$v->setFavoBugCategorie(NULL);
		}
		$PersoonVoorkeurFromFavoBugCategorieVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $BugFromCategorieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BugBerichtFromCategorieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `BugCategorie`'
		          .' WHERE `bugCategorieID` = %i'
		          .' LIMIT 1'
		          , $this->bugCategorieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->bugCategorieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `BugCategorie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bugCategorieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bugCategorieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van BugCategorie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bugcategorieid':
			return 'int';
		case 'naam':
			return 'string';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'actief':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bugCategorieID':
		case 'commissie_commissieID':
		case 'actief':
			$type = '%i';
			break;
		case 'naam':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `BugCategorie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `bugCategorieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->bugCategorieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `BugCategorie`'
		          .' WHERE `bugCategorieID` = %i'
		                 , $veld
		          , $this->bugCategorieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'BugCategorie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Bug-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugFromCategorieVerz = BugVerzameling::fromCategorie($this);
		$dependencies['Bug'] = $BugFromCategorieVerz;

		// Verzamel alle BugBericht-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BugBerichtFromCategorieVerz = BugBerichtVerzameling::fromCategorie($this);
		$dependencies['BugBericht'] = $BugBerichtFromCategorieVerz;

		// Verzamel alle PersoonVoorkeur-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$PersoonVoorkeurFromFavoBugCategorieVerz = PersoonVoorkeurVerzameling::fromFavoBugCategorie($this);
		$dependencies['PersoonVoorkeur'] = $PersoonVoorkeurFromFavoBugCategorieVerz;

		return $dependencies;
	}
}
