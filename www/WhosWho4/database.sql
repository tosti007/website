/* WhosWho4 DB Table definities */
/* Vergeet de DB niet aan te passen! */

CREATE TABLE `ActivatieCode`
( `lid_contactID` int(11) NOT NULL
, `code` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`lid_contactID`)
, CONSTRAINT `ActivatieCode_Lid`
    FOREIGN KEY (`lid_contactID`)
    REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Activiteit`
( `activiteitID` int(11) NOT NULL AUTO_INCREMENT
, `overerving` enum('Activiteit','ActiviteitInformatie','ActiviteitHerhaling') NOT NULL DEFAULT 'Activiteit'
, `momentBegin` datetime NOT NULL
, `momentEind` datetime NOT NULL
, `toegang` enum('PUBLIEK','PRIVE') NOT NULL DEFAULT 'PUBLIEK'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ActiviteitCategorie`
( `actCategorieID` int(11) NOT NULL AUTO_INCREMENT
, `titel_NL` varchar(255) NOT NULL DEFAULT ''
, `titel_EN` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`actCategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ActiviteitHerhaling`
( `activiteitID` int(11) NOT NULL
, `parent_activiteitID` int(11) NOT NULL
, PRIMARY KEY (`activiteitID`)
, CONSTRAINT `ActiviteitHerhaling_fk`
    FOREIGN KEY (`activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
, CONSTRAINT `ActiviteitHerhaling_Parent`
    FOREIGN KEY (`parent_activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ActiviteitInformatie`
( `activiteitID` int(11) NOT NULL
, `titel_NL` varchar(255) NOT NULL DEFAULT ''
, `titel_EN` varchar(255) DEFAULT ''
, `werftekst_NL` text NOT NULL DEFAULT ''
, `werftekst_EN` text NOT NULL DEFAULT ''
, `homepage` varchar(511) DEFAULT NULL
, `locatie_NL` varchar(255) DEFAULT NULL
, `locatie_EN` varchar(255) DEFAULT NULL
, `prijs_NL` varchar(255) DEFAULT NULL
, `prijs_EN` varchar(255) DEFAULT NULL
, `inschrijfbaar` int(1) NOT NULL DEFAULT False
, `stuurMail` int(1) NOT NULL DEFAULT True
, `datumInschrijvenMax` date DEFAULT NULL
, `datumUitschrijven` date DEFAULT NULL
, `maxDeelnemers` int(11) DEFAULT NULL
, `actsoort` enum('AES2','UU','LAND','ZUS','.COM','EXT') NOT NULL DEFAULT 'AES2'
, `onderwijs` enum('N','B','J') NOT NULL DEFAULT 'N'
, `aantalComputers` int(11) NOT NULL
, `categorie_actCategorieID` int(11) NOT NULL
, PRIMARY KEY (`activiteitID`)
, CONSTRAINT `ActiviteitInformatie_fk`
    FOREIGN KEY (`activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
, CONSTRAINT `ActiviteitInformatie_Categorie`
    FOREIGN KEY (`categorie_actCategorieID`)
    REFERENCES `ActiviteitCategorie` (`actCategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ActiviteitVraag`
( `activiteit_activiteitID` int(11) NOT NULL
, `vraagID` int(11) NOT NULL
, `type` enum('STRING','ENUM') NOT NULL DEFAULT 'STRING'
, `vraag_NL` varchar(255) DEFAULT NULL
, `vraag_EN` varchar(255) DEFAULT NULL
, `opties` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`activiteit_activiteitID`, `vraagID`)
, CONSTRAINT `ActiviteitVraag_Activiteit`
    FOREIGN KEY (`activiteit_activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `AesArtikel`
( `artikelID` int(11) NOT NULL
, `commissie_commissieID` int(11) NOT NULL
, PRIMARY KEY (`artikelID`)
, CONSTRAINT `AesArtikel_fk`
    FOREIGN KEY (`artikelID`)
    REFERENCES `Artikel` (`artikelID`)
, CONSTRAINT `AesArtikel_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Artikel`
( `artikelID` int(11) NOT NULL AUTO_INCREMENT
, `overerving` enum('Artikel','ColaProduct','Dictaat','DibsProduct','iDealKaartje','Boek','AesArtikel') NOT NULL DEFAULT 'Artikel'
, `naam` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Barcode`
( `barcodeID` int(11) NOT NULL AUTO_INCREMENT
, `voorraad_voorraadID` int(11) NOT NULL
, `wanneer` datetime NOT NULL
, `barcode` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`barcodeID`)
, CONSTRAINT `Barcode_Voorraad`
    FOREIGN KEY (`voorraad_voorraadID`)
    REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Bedrijf`
( `contactID` int(11) NOT NULL
, `status` enum('AFGEWEZEN','HUIDIG','KOSTENLOOS','OUD','POTENTIEEL','OVERGENOME','OPGEHEVEN','OVERIG') NOT NULL DEFAULT 'POTENTIEEL'
, `type` enum('ICT','CONSULTANCY','FINANCIEEL','ONDERZOEK','OVERIG') NOT NULL DEFAULT 'ICT'
, PRIMARY KEY (`contactID`)
, CONSTRAINT `Bedrijf_fk`
    FOREIGN KEY (`contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `BedrijfStudie`
( `Studie_studieID` int(11) NOT NULL
, `Bedrijf_contactID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`Studie_studieID`, `Bedrijf_contactID`)
, CONSTRAINT `BedrijfStudie_Studie`
    FOREIGN KEY (`Studie_studieID`)
    REFERENCES `Studie` (`studieID`)
, CONSTRAINT `BedrijfStudie_Bedrijf`
    FOREIGN KEY (`Bedrijf_contactID`)
    REFERENCES `Bedrijf` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Bedrijfsprofiel`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `contractonderdeel_id` int(11) NOT NULL
, `datumVerloop` date NOT NULL
, `inhoud_NL` text DEFAULT NULL
, `inhoud_EN` text DEFAULT NULL
, `url` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `Bedrijfsprofiel_Contractonderdeel`
    FOREIGN KEY (`contractonderdeel_id`)
    REFERENCES `Contractonderdeel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Bestelling`
( `bestellingID` int(11) NOT NULL AUTO_INCREMENT
, `persoon_contactID` int(11) NOT NULL
, `artikel_artikelID` int(11) NOT NULL
, `datumGeplaatst` date NOT NULL
, `status` enum('OPEN','GERESERVEERD','GESLOTEN','VERLOPEN') NOT NULL DEFAULT 'open'
, `aantal` int(11) NOT NULL
, `vervalDatum` date NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bestellingID`)
, CONSTRAINT `Bestelling_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Bestelling_Artikel`
    FOREIGN KEY (`artikel_artikelID`)
    REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Boek`
( `artikelID` int(11) NOT NULL
, `isbn` varchar(255) NOT NULL DEFAULT ''
, `auteur` varchar(255) DEFAULT NULL
, `druk` varchar(255) DEFAULT NULL
, `uitgever` varchar(255) DEFAULT NULL
, PRIMARY KEY (`artikelID`)
, CONSTRAINT `Boek_fk`
    FOREIGN KEY (`artikelID`)
    REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Boekverkoop`
( `verkoopdag` date NOT NULL
, `volgnummer` int(11) NOT NULL
, `opener_contactID` int(11) NOT NULL
, `open` datetime NOT NULL
, `sluiter_contactID` int(11) DEFAULT NULL
, `pin1` decimal(8,2) NOT NULL
, `pin2` decimal(8,2) NOT NULL
, `kasverschil` decimal(8,2) NOT NULL
, `gesloten` datetime DEFAULT NULL
, `opmerking` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`verkoopdag`, `volgnummer`)
, CONSTRAINT `Boekverkoop_Opener`
    FOREIGN KEY (`opener_contactID`)
    REFERENCES `Lid` (`contactID`)
, CONSTRAINT `Boekverkoop_Sluiter`
    FOREIGN KEY (`sluiter_contactID`)
    REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Bug`
( `bugID` int(11) NOT NULL AUTO_INCREMENT
, `melder_contactID` int(11) DEFAULT NULL
, `melderEmail` varchar(255) DEFAULT NULL
, `titel` varchar(255) NOT NULL DEFAULT ''
, `categorie_bugCategorieID` int(11) NOT NULL
, `status` enum('OPEN','BEZIG','OPGELOST','GECOMMIT','UITGESTELD','GESLOTEN','BOGUS','DUBBEL','WANNAHAVE','FEEDBACK','WONTFIX') NOT NULL DEFAULT 'OPEN'
, `prioriteit` enum('ONBENULLIG','LAAG','GEMIDDELD','HOOG','URGENT') NOT NULL DEFAULT 'ONBENULLIG'
, `niveau` enum('OPEN','INGELOGD','CIE') NOT NULL DEFAULT 'OPEN'
, `moment` datetime NOT NULL
, `type` varchar(255) NOT NULL DEFAULT ''
, `level` enum('BEGINNER','GEMIDDELD','EXPERT') NOT NULL DEFAULT 'GEMIDDELD'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bugID`)
, CONSTRAINT `Bug_Melder`
    FOREIGN KEY (`melder_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Bug_Categorie`
    FOREIGN KEY (`categorie_bugCategorieID`)
    REFERENCES `BugCategorie` (`bugCategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `BugBericht`
( `bugBerichtID` int(11) NOT NULL AUTO_INCREMENT
, `bug_bugID` int(11) NOT NULL
, `melder_contactID` int(11) DEFAULT NULL
, `melderEmail` varchar(255) DEFAULT NULL
, `titel` varchar(255) DEFAULT NULL
, `categorie_bugCategorieID` int(11) DEFAULT NULL
, `status` enum('OPEN','BEZIG','OPGELOST','GECOMMIT','UITGESTELD','GESLOTEN','BOGUS','DUBBEL','WANNAHAVE','FEEDBACK','WONTFIX') NOT NULL DEFAULT 'OPEN'
, `prioriteit` enum('ONBENULLIG','LAAG','GEMIDDELD','HOOG','URGENT') NOT NULL DEFAULT 'ONBENULLIG'
, `niveau` enum('OPEN','INGELOGD','CIE') NOT NULL DEFAULT 'OPEN'
, `bericht` text NOT NULL DEFAULT ''
, `moment` datetime NOT NULL
, `revisie` int(11) DEFAULT NULL
, `level` enum('BEGINNER','GEMIDDELD','EXPERT') NOT NULL DEFAULT 'GEMIDDELD'
, `commit` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bugBerichtID`)
, CONSTRAINT `BugBericht_Bug`
    FOREIGN KEY (`bug_bugID`)
    REFERENCES `Bug` (`bugID`)
, CONSTRAINT `BugBericht_Melder`
    FOREIGN KEY (`melder_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `BugBericht_Categorie`
    FOREIGN KEY (`categorie_bugCategorieID`)
    REFERENCES `BugCategorie` (`bugCategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `BugCategorie`
( `bugCategorieID` int(11) NOT NULL AUTO_INCREMENT
, `naam` varchar(255) NOT NULL DEFAULT ''
, `commissie_commissieID` int(11) NOT NULL
, `actief` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bugCategorieID`)
, CONSTRAINT `BugCategorie_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `BugToewijzing`
( `bugToewijzingID` int(11) NOT NULL AUTO_INCREMENT
, `bug_bugID` int(11) NOT NULL
, `persoon_contactID` int(11) NOT NULL
, `begin_bugBerichtID` int(11) DEFAULT NULL
, `eind_bugBerichtID` int(11) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bugToewijzingID`)
, CONSTRAINT `BugToewijzing_Bug`
    FOREIGN KEY (`bug_bugID`)
    REFERENCES `Bug` (`bugID`)
, CONSTRAINT `BugToewijzing_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `BugToewijzing_Begin`
    FOREIGN KEY (`begin_bugBerichtID`)
    REFERENCES `BugBericht` (`bugBerichtID`)
, CONSTRAINT `BugToewijzing_Eind`
    FOREIGN KEY (`eind_bugBerichtID`)
    REFERENCES `BugBericht` (`bugBerichtID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `BugVolger`
( `bug_bugID` int(11) NOT NULL
, `persoon_contactID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bug_bugID`, `persoon_contactID`)
, CONSTRAINT `BugVolger_Bug`
    FOREIGN KEY (`bug_bugID`)
    REFERENCES `Bug` (`bugID`)
, CONSTRAINT `BugVolger_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CSPReport`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `ingelogdLid_contactID` int(11) DEFAULT NULL
, `ip` varchar(255) NOT NULL DEFAULT ''
, `documentUri` varchar(255) NOT NULL DEFAULT ''
, `referrer` varchar(255) DEFAULT NULL
, `blockedUri` varchar(255) NOT NULL DEFAULT ''
, `violatedDirective` varchar(255) NOT NULL DEFAULT ''
, `originalPolicy` varchar(255) NOT NULL DEFAULT ''
, `userAgent` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `CSPReport_IngelogdLid`
    FOREIGN KEY (`ingelogdLid_contactID`)
    REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ColaProduct`
( `artikelID` int(11) NOT NULL
, `volgorde` int(11) NOT NULL
, PRIMARY KEY (`artikelID`)
, CONSTRAINT `ColaProduct_fk`
    FOREIGN KEY (`artikelID`)
    REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Collectie`
( `collectieID` int(11) NOT NULL AUTO_INCREMENT
, `parentCollectie_collectieID` int(11) DEFAULT NULL
, `naam` varchar(255) NOT NULL DEFAULT ''
, `omschrijving` varchar(255) DEFAULT NULL
, `actief` int(1) NOT NULL DEFAULT True
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`collectieID`)
, CONSTRAINT `Collectie_ParentCollectie`
    FOREIGN KEY (`parentCollectie_collectieID`)
    REFERENCES `Collectie` (`collectieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Commissie`
( `commissieID` int(11) NOT NULL AUTO_INCREMENT
, `naam` varchar(255) NOT NULL DEFAULT ''
, `soort` enum('CIE','GROEP','DISPUUT') NOT NULL DEFAULT 'CIE'
, `categorie_categorieID` int(11) DEFAULT NULL
, `thema` varchar(255) DEFAULT NULL
, `omschrijving` varchar(255) DEFAULT NULL
, `datumBegin` date DEFAULT NULL
, `datumEind` date DEFAULT NULL
, `login` varchar(255) NOT NULL DEFAULT ''
, `email` varchar(255) DEFAULT NULL
, `homepage` varchar(511) DEFAULT NULL
, `emailZichtbaar` int(1) NOT NULL DEFAULT True
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`commissieID`)
, CONSTRAINT `Commissie_Categorie`
    FOREIGN KEY (`categorie_categorieID`)
    REFERENCES `CommissieCategorie` (`categorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CommissieActiviteit`
( `commissie_commissieID` int(11) NOT NULL
, `activiteit_activiteitID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`commissie_commissieID`, `activiteit_activiteitID`)
, CONSTRAINT `CommissieActiviteit_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
, CONSTRAINT `CommissieActiviteit_Activiteit`
    FOREIGN KEY (`activiteit_activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CommissieCategorie`
( `categorieID` int(11) NOT NULL AUTO_INCREMENT
, `naam` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`categorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CommissieFoto`
( `commissie_commissieID` int(11) NOT NULL
, `media_mediaID` int(11) NOT NULL
, `status` enum('OUD','HUIDIG') NOT NULL DEFAULT 'OUD'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`commissie_commissieID`, `media_mediaID`)
, CONSTRAINT `CommissieFoto_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
, CONSTRAINT `CommissieFoto_Media`
    FOREIGN KEY (`media_mediaID`)
    REFERENCES `Media` (`mediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CommissieLid`
( `persoon_contactID` int(11) NOT NULL
, `commissie_commissieID` int(11) NOT NULL
, `datumBegin` date DEFAULT NULL
, `datumEind` date DEFAULT NULL
, `spreuk` varchar(255) DEFAULT NULL
, `spreukZichtbaar` int(1) NOT NULL DEFAULT False
, `functie` enum('OVERIG','VOORZ','SECR','PENNY','PR','SPONS','BESTUUR','SFEER') NOT NULL DEFAULT 'OVERIG'
, `functieNaam` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`, `commissie_commissieID`)
, CONSTRAINT `CommissieLid_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `CommissieLid_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Contact`
( `contactID` int(11) NOT NULL AUTO_INCREMENT
, `overerving` enum('Contact','Lid','Persoon','Organisatie','Bedrijf','Leverancier') NOT NULL DEFAULT 'Contact'
, `email` varchar(255) DEFAULT NULL
, `homepage` varchar(511) DEFAULT NULL
, `vakidOpsturen` int(1) NOT NULL DEFAULT False
, `aes2rootsOpsturen` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ContactAdres`
( `adresID` int(11) NOT NULL AUTO_INCREMENT
, `contact_contactID` int(11) NOT NULL
, `soort` enum('THUIS','OUDERS','WERK','POST','BEZOEK','FACTUUR') NOT NULL DEFAULT 'THUIS'
, `straat1` varchar(255) DEFAULT ''
, `straat2` varchar(255) DEFAULT NULL
, `huisnummer` varchar(255) DEFAULT NULL
, `postcode` varchar(255) DEFAULT ''
, `woonplaats` varchar(255) DEFAULT ''
, `land` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`adresID`)
, CONSTRAINT `ContactAdres_Contact`
    FOREIGN KEY (`contact_contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ContactMailingList`
( `contact_contactID` int(11) NOT NULL
, `mailingList_mailingListID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`contact_contactID`, `mailingList_mailingListID`)
, CONSTRAINT `ContactMailingList_Contact`
    FOREIGN KEY (`contact_contactID`)
    REFERENCES `Contact` (`contactID`)
, CONSTRAINT `ContactMailingList_MailingList`
    FOREIGN KEY (`mailingList_mailingListID`)
    REFERENCES `MailingList` (`mailingListID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ContactPersoon`
( `persoon_contactID` int(11) NOT NULL
, `organisatie_contactID` int(11) NOT NULL
, `type` enum('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS') NOT NULL
, `Functie` varchar(255) DEFAULT NULL
, `beginDatum` date DEFAULT NULL
, `eindDatum` date DEFAULT NULL
, `opmerking` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`, `organisatie_contactID`, `type`)
, CONSTRAINT `ContactPersoon_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `ContactPersoon_Organisatie`
    FOREIGN KEY (`organisatie_contactID`)
    REFERENCES `Organisatie` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ContactTelnr`
( `telnrID` int(11) NOT NULL AUTO_INCREMENT
, `contact_contactID` int(11) NOT NULL
, `telefoonnummer` varchar(255) NOT NULL DEFAULT ''
, `soort` enum('THUIS','MOBIEL','WERK','OUDERS','RECEPTIE','FAX') NOT NULL DEFAULT 'THUIS'
, `voorkeur` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`telnrID`)
, CONSTRAINT `ContactTelnr_Contact`
    FOREIGN KEY (`contact_contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Contract`
( `contractID` int(11) NOT NULL AUTO_INCREMENT
, `bedrijf_contactID` int(11) NOT NULL
, `korting` varchar(255) DEFAULT NULL
, `status` enum('GEFACTUREERD','MISLUKT','OPGESTUURD','RETOUR','VOORSTEL') NOT NULL DEFAULT 'GEFACTUREERD'
, `ingangsdatum` date NOT NULL
, `opmerking` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`contractID`)
, CONSTRAINT `Contract_Bedrijf`
    FOREIGN KEY (`bedrijf_contactID`)
    REFERENCES `Bedrijf` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Contractonderdeel`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `contract_contractID` int(11) NOT NULL
, `soort` enum('VAKID','VACATURE','MAILING','PROFIEL','LEZING','OVERIG') NOT NULL DEFAULT 'OVERIG'
, `bedrag` int(11) NOT NULL
, `uitvoerDatum` date DEFAULT NULL
, `omschrijving` text DEFAULT NULL
, `uitgevoerd` varchar(255) DEFAULT NULL
, `verantwoordelijke_contactID` int(11) DEFAULT NULL
, `gefactureerd` int(1) NOT NULL DEFAULT False
, `commissie_commissieID` int(11) DEFAULT NULL
, `latexString` text DEFAULT NULL
, `annulering` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `Contractonderdeel_Contract`
    FOREIGN KEY (`contract_contractID`)
    REFERENCES `Contract` (`contractID`)
, CONSTRAINT `Contractonderdeel_Verantwoordelijke`
    FOREIGN KEY (`verantwoordelijke_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Contractonderdeel_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Deelnemer`
( `persoon_contactID` int(11) NOT NULL
, `activiteit_activiteitID` int(11) NOT NULL
, `momentInschrijven` datetime
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`, `activiteit_activiteitID`)
, CONSTRAINT `Deelnemer_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Deelnemer_Activiteit`
    FOREIGN KEY (`activiteit_activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DeelnemerAntwoord`
( `deelnemer_contactID` int(11) NOT NULL
, `deelnemer_activiteitID` int(11) NOT NULL
, `vraag_activiteitID` int(11) NOT NULL
, `vraag_vraagID` int(11) NOT NULL
, `antwoord` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`deelnemer_contactID`, `deelnemer_activiteitID`, `vraag_activiteitID`, `vraag_vraagID`)
, CONSTRAINT `DeelnemerAntwoord_Deelnemer`
    FOREIGN KEY (`deelnemer_contactID`, `deelnemer_activiteitID`)
    REFERENCES `Deelnemer` (`persoon_contactID`, `activiteit_activiteitID`)
, CONSTRAINT `DeelnemerAntwoord_Vraag`
    FOREIGN KEY (`vraag_activiteitID`, `vraag_vraagID`)
    REFERENCES `ActiviteitVraag` (`activiteit_activiteitID`, `vraagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DibsInfo`
( `persoon_contactID` int(11) NOT NULL
, `kudos` int(11) NOT NULL
, `laatste_transactieID` int(11) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`)
, CONSTRAINT `DibsInfo_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `DibsInfo_Laatste`
    FOREIGN KEY (`laatste_transactieID`)
    REFERENCES `DibsTransactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DibsProduct`
( `artikelID` int(11) NOT NULL
, `kudos` int(11) NOT NULL
, PRIMARY KEY (`artikelID`)
, CONSTRAINT `DibsProduct_fk`
    FOREIGN KEY (`artikelID`)
    REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DibsTransactie`
( `transactieID` int(11) NOT NULL
, `bron` enum('WWW','TABLET','BVK','IDEAL') NOT NULL DEFAULT 'WWW'
, `pas_pasID` int(11) DEFAULT NULL
, PRIMARY KEY (`transactieID`)
, CONSTRAINT `DibsTransactie_fk`
    FOREIGN KEY (`transactieID`)
    REFERENCES `Transactie` (`transactieID`)
, CONSTRAINT `DibsTransactie_Pas`
    FOREIGN KEY (`pas_pasID`)
    REFERENCES `Pas` (`pasID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Dictaat`
( `artikelID` int(11) NOT NULL
, `auteur_contactID` int(11) NOT NULL
, `uitgaveJaar` int(11) NOT NULL
, `digitaalVerspreiden` int(1) NOT NULL DEFAULT False
, `beginGebruik` date NOT NULL
, `eindeGebruik` date NOT NULL
, `copyrightOpmerking` text DEFAULT NULL
, PRIMARY KEY (`artikelID`)
, CONSTRAINT `Dictaat_fk`
    FOREIGN KEY (`artikelID`)
    REFERENCES `Artikel` (`artikelID`)
, CONSTRAINT `Dictaat_Auteur`
    FOREIGN KEY (`auteur_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DictaatOrder`
( `orderID` int(11) NOT NULL
, `kaftPrijs` decimal(8,2) NOT NULL
, `marge` decimal(8,2) NOT NULL
, `factuurverschil` decimal(8,2) NOT NULL
, PRIMARY KEY (`orderID`)
, CONSTRAINT `DictaatOrder_fk`
    FOREIGN KEY (`orderID`)
    REFERENCES `Order` (`orderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DocuBestand`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `titel` varchar(255) NOT NULL DEFAULT ''
, `extensie` varchar(255) NOT NULL DEFAULT ''
, `categorie_id` int(11) NOT NULL
, `datum` datetime NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `DocuBestand_Categorie`
    FOREIGN KEY (`categorie_id`)
    REFERENCES `DocuCategorie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DocuCategorie`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `omschrijving` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Donateur`
( `persoon_contactID` int(11) NOT NULL
, `anderPersoon_contactID` int(11) DEFAULT NULL
, `aanhef` varchar(255) DEFAULT NULL
, `jaarBegin` int(11) NOT NULL
, `jaarEind` int(11) DEFAULT NULL
, `donatie` decimal(8,2) NOT NULL
, `machtiging` int(1) NOT NULL DEFAULT False
, `machtigingOud` int(1) NOT NULL DEFAULT False
, `almanak` int(1) NOT NULL DEFAULT False
, `avNotulen` enum('N','POST','EMAIL','POST_EMAIL') NOT NULL DEFAULT 'N'
, `jaarverslag` int(1) NOT NULL DEFAULT False
, `studiereis` int(1) NOT NULL DEFAULT False
, `symposium` int(1) NOT NULL DEFAULT False
, `vakid` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`)
, CONSTRAINT `Donateur_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Donateur_AnderPersoon`
    FOREIGN KEY (`anderPersoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ExtraPoster`
( `extraPosterID` int(11) NOT NULL AUTO_INCREMENT
, `naam` varchar(255) NOT NULL DEFAULT ''
, `beschrijving` varchar(255) NOT NULL DEFAULT ''
, `zichtbaar` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`extraPosterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Giro`
( `transactieID` int(11) NOT NULL
, `rekening` varchar(255) DEFAULT NULL
, `tegenrekening` varchar(255) DEFAULT NULL
, PRIMARY KEY (`transactieID`)
, CONSTRAINT `Giro_fk`
    FOREIGN KEY (`transactieID`)
    REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IOUBetaling`
( `betalingID` int(11) NOT NULL AUTO_INCREMENT
, `bon_bonID` int(11) DEFAULT NULL
, `van_contactID` int(11) NOT NULL
, `naar_contactID` int(11) NOT NULL
, `bedrag` decimal(8,2) NOT NULL
, `omschrijving` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`betalingID`)
, CONSTRAINT `IOUBetaling_Bon`
    FOREIGN KEY (`bon_bonID`)
    REFERENCES `IOUBon` (`bonID`)
, CONSTRAINT `IOUBetaling_Van`
    FOREIGN KEY (`van_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `IOUBetaling_Naar`
    FOREIGN KEY (`naar_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IOUBon`
( `bonID` int(11) NOT NULL AUTO_INCREMENT
, `persoon_contactID` int(11) NOT NULL
, `omschrijving` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bonID`)
, CONSTRAINT `IOUBon_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IOUSplit`
( `bon_bonID` int(11) NOT NULL
, `persoon_contactID` int(11) NOT NULL
, `moetBetalen` decimal(8,2) NOT NULL
, `heeftBetaald` decimal(8,2) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`bon_bonID`, `persoon_contactID`)
, CONSTRAINT `IOUSplit_Bon`
    FOREIGN KEY (`bon_bonID`)
    REFERENCES `IOUBon` (`bonID`)
, CONSTRAINT `IOUSplit_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Interactie`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `spook_persoon_contactID` int(11) NOT NULL
, `spook_organisatie_contactID` int(11) NOT NULL
, `spook_type` enum('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS') NOT NULL DEFAULT 'NORMAAL'
, `bedrijf_contactID` int(11) NOT NULL
, `bedrijfPersoon_persoon_contactID` int(11) DEFAULT NULL
, `bedrijfPersoon_organisatie_contactID` int(11) DEFAULT NULL
, `bedrijfPersoon_type` enum('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS') DEFAULT NULL
, `commissie_commissieID` int(11) DEFAULT NULL
, `datum` datetime NOT NULL
, `inhoud` text NOT NULL DEFAULT ''
, `medium` enum('EMAIL','FAX','GESPREK','NOTITIE','ONBEKEND','OVERIG','POST','TELEFOON') NOT NULL DEFAULT 'EMAIL'
, `todo` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `Interactie_Spook`
    FOREIGN KEY (`spook_contactID`, `spook_contactID2`, `spook_type`)
    REFERENCES `ContactPersoon` (`persoon_contactID`, `organisatie_contactID`, `type`)
, CONSTRAINT `Interactie_Bedrijf`
    FOREIGN KEY (`bedrijf_contactID`)
    REFERENCES `Bedrijf` (`contactID`)
, CONSTRAINT `Interactie_BedrijfPersoon`
    FOREIGN KEY (`bedrijfPersoon_contactID`, `bedrijfPersoon_contactID2`, `bedrijfPersoon_type`)
    REFERENCES `ContactPersoon` (`persoon_contactID`, `organisatie_contactID`, `type`)
, CONSTRAINT `Interactie_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IntroCluster`
( `clusterID` int(11) NOT NULL AUTO_INCREMENT
, `jaar` int(11) NOT NULL
, `naam` varchar(255) NOT NULL DEFAULT ''
, `persoon_contactID` int(11) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`clusterID`)
, CONSTRAINT `IntroCluster_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IntroDeelnemer`
( `lid_contactID` int(11) NOT NULL
, `betaald` enum('NEE','BANK','PIN','KAS','IDEAL','ONBEKEND') NOT NULL DEFAULT 'NEE'
, `kamp` int(1) NOT NULL DEFAULT False
, `vega` int(1) NOT NULL DEFAULT False
, `allergie` int(1) NOT NULL DEFAULT False
, `aanwezig` int(1) NOT NULL DEFAULT False
, `allergieen` text DEFAULT NULL
, `introOpmerkingen` text DEFAULT NULL
, `voedselBeperking` text DEFAULT NULL
, `magischeCode` text NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`lid_contactID`)
, CONSTRAINT `IntroDeelnemer_Lid`
    FOREIGN KEY (`lid_contactID`)
    REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IntroGroep`
( `groepID` int(11) NOT NULL AUTO_INCREMENT
, `cluster_clusterID` int(11) NOT NULL
, `naam` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`groepID`)
, CONSTRAINT `IntroGroep_Cluster`
    FOREIGN KEY (`cluster_clusterID`)
    REFERENCES `IntroCluster` (`clusterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Jaargang`
( `jaargangID` int(11) NOT NULL AUTO_INCREMENT
, `vak_vakID` int(11) NOT NULL
, `contactPersoon_contactID` int(11) NOT NULL
, `datumBegin` date NOT NULL
, `datumEinde` date NOT NULL
, `inschrijvingen` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`jaargangID`)
, CONSTRAINT `Jaargang_Vak`
    FOREIGN KEY (`vak_vakID`)
    REFERENCES `Vak` (`vakID`)
, CONSTRAINT `Jaargang_ContactPersoon`
    FOREIGN KEY (`contactPersoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `JaargangArtikel`
( `jaargang_jaargangID` int(11) NOT NULL
, `artikel_artikelID` int(11) NOT NULL
, `schattingNodig` int(11) DEFAULT NULL
, `opmerking` text DEFAULT NULL
, `verplicht` int(1) NOT NULL DEFAULT True
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`jaargang_jaargangID`, `artikel_artikelID`)
, CONSTRAINT `JaargangArtikel_Jaargang`
    FOREIGN KEY (`jaargang_jaargangID`)
    REFERENCES `Jaargang` (`jaargangID`)
, CONSTRAINT `JaargangArtikel_Artikel`
    FOREIGN KEY (`artikel_artikelID`)
    REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `KartVoorwerp`
( `kartVoorwerpID` int(11) NOT NULL AUTO_INCREMENT
, `persoonKart_persoon_contactID` int(11) NOT NULL
, `persoonKart_naam` varchar(255) NOT NULL DEFAULT ''
, `persoon_contactID` int(11) DEFAULT NULL
, `commissie_commissieID` int(11) DEFAULT NULL
, `media_mediaID` int(11) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`kartVoorwerpID`)
, CONSTRAINT `KartVoorwerp_PersoonKart`
    FOREIGN KEY (`persoonKart_contactID`, `persoonKart_naam`)
    REFERENCES `PersoonKart` (`persoon_contactID`, `naam`)
, CONSTRAINT `KartVoorwerp_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `KartVoorwerp_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
, CONSTRAINT `KartVoorwerp_Media`
    FOREIGN KEY (`media_mediaID`)
    REFERENCES `Media` (`mediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Leverancier`
( `contactID` int(11) NOT NULL
, `naam` varchar(255) NOT NULL DEFAULT ''
, `dictaten` int(1) NOT NULL DEFAULT False
, `dictatenIntern` int(1) NOT NULL DEFAULT False
, `dibsproducten` int(1) NOT NULL DEFAULT False
, `colaproducten` int(1) NOT NULL DEFAULT False
, PRIMARY KEY (`contactID`)
, CONSTRAINT `Leverancier_fk`
    FOREIGN KEY (`contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LeverancierRetour`
( `voorraadMutatieID` int(11) NOT NULL
, `order_orderID` int(11) DEFAULT NULL
, `leverancier_contactID` int(11) NOT NULL
, PRIMARY KEY (`voorraadMutatieID`)
, CONSTRAINT `LeverancierRetour_fk`
    FOREIGN KEY (`voorraadMutatieID`)
    REFERENCES `VoorraadMutatie` (`voorraadMutatieID`)
, CONSTRAINT `LeverancierRetour_Order`
    FOREIGN KEY (`order_orderID`)
    REFERENCES `Order` (`orderID`)
, CONSTRAINT `LeverancierRetour_Leverancier`
    FOREIGN KEY (`leverancier_contactID`)
    REFERENCES `Leverancier` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Levering`
( `voorraadMutatieID` int(11) NOT NULL
, `order_orderID` int(11) DEFAULT NULL
, `leverancier_contactID` int(11) NOT NULL
, PRIMARY KEY (`voorraadMutatieID`)
, CONSTRAINT `Levering_fk`
    FOREIGN KEY (`voorraadMutatieID`)
    REFERENCES `VoorraadMutatie` (`voorraadMutatieID`)
, CONSTRAINT `Levering_Order`
    FOREIGN KEY (`order_orderID`)
    REFERENCES `Order` (`orderID`)
, CONSTRAINT `Levering_Leverancier`
    FOREIGN KEY (`leverancier_contactID`)
    REFERENCES `Leverancier` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Lid`
( `contactID` int(11) NOT NULL
, `lidToestand` enum('BIJNALID','LID','OUDLID','BAL','LIDVANVERDIENSTE','ERELID','GESCHORST','BEESTJE','INGESCHREVEN') NOT NULL DEFAULT 'BIJNALID'
, `lidMaster` int(1) NOT NULL DEFAULT False
, `lidExchange` int(1) NOT NULL DEFAULT False
, `lidVan` date
, `lidTot` date DEFAULT NULL
, `studentnr` varchar(255) DEFAULT NULL
, `opzoekbaar` enum('A','J','N','CIE') NOT NULL DEFAULT 'N'
, `inAlmanak` int(1) NOT NULL DEFAULT True
, `planetRSS` varchar(255) DEFAULT NULL
, PRIMARY KEY (`contactID`)
, CONSTRAINT `Lid_fk`
    FOREIGN KEY (`contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LidStudie`
( `lidStudieID` int(11) NOT NULL AUTO_INCREMENT
, `studie_studieID` int(11) NOT NULL
, `lid_contactID` int(11) NOT NULL
, `datumBegin` date NOT NULL
, `datumEind` date DEFAULT NULL
, `status` enum('STUDEREND','ALUMNUS','GESTOPT') NOT NULL DEFAULT 'STUDEREND'
, `groep_groepID` int(11) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`lidStudieID`)
, CONSTRAINT `LidStudie_Studie`
    FOREIGN KEY (`studie_studieID`)
    REFERENCES `Studie` (`studieID`)
, CONSTRAINT `LidStudie_Lid`
    FOREIGN KEY (`lid_contactID`)
    REFERENCES `Lid` (`contactID`)
, CONSTRAINT `LidStudie_Groep`
    FOREIGN KEY (`groep_groepID`)
    REFERENCES `IntroGroep` (`groepID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LidmaatschapsBetaling`
( `lid_contactID` int(11) NOT NULL
, `soort` enum('BOEKVERKOOP','MACHTIGING','IDEAL') NOT NULL DEFAULT 'BOEKVERKOOP'
, `rekeningnummer` varchar(255) DEFAULT NULL
, `rekeninghouder` varchar(255) DEFAULT NULL
, `ideal_transactieID` int(11) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`lid_contactID`)
, CONSTRAINT `LidmaatschapsBetaling_Lid`
    FOREIGN KEY (`lid_contactID`)
    REFERENCES `Lid` (`contactID`)
, CONSTRAINT `LidmaatschapsBetaling_Ideal`
    FOREIGN KEY (`ideal_transactieID`)
    REFERENCES `iDeal` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Mailing`
( `mailingID` int(11) NOT NULL AUTO_INCREMENT
, `commissie_commissieID` int(11) DEFAULT NULL
, `omschrijving` varchar(255) NOT NULL DEFAULT ''
, `momentIngepland` datetime DEFAULT NULL
, `momentVerwerkt` datetime DEFAULT NULL
, `mailHeaders` text DEFAULT NULL
, `mailSubject` varchar(255) NOT NULL DEFAULT ''
, `mailFrom` varchar(255) NOT NULL DEFAULT ''
, `isDefinitief` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mailingID`)
, CONSTRAINT `Mailing_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MailingBounce`
( `mailingBounceID` int(11) NOT NULL AUTO_INCREMENT
, `mailing_mailingID` int(11) DEFAULT NULL
, `contact_contactID` int(11) NOT NULL
, `bounceContent` text NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mailingBounceID`)
, CONSTRAINT `MailingBounce_Mailing`
    FOREIGN KEY (`mailing_mailingID`)
    REFERENCES `Mailing` (`mailingID`)
, CONSTRAINT `MailingBounce_Contact`
    FOREIGN KEY (`contact_contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MailingList`
( `mailingListID` int(11) NOT NULL AUTO_INCREMENT
, `naam` varchar(255) NOT NULL DEFAULT ''
, `omschrijving` text NOT NULL DEFAULT ''
, `subjectPrefix` varchar(255) DEFAULT NULL
, `bodyPrefix` text DEFAULT NULL
, `bodySuffix` text DEFAULT NULL
, `verborgen` int(1) NOT NULL DEFAULT False
, `query` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mailingListID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MailingTemplate`
( `mailingTemplateID` int(11) NOT NULL AUTO_INCREMENT
, `omschrijving` text NOT NULL DEFAULT ''
, `standaardPreviewers` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mailingTemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Mailing_MailingList`
( `mailing_mailingID` int(11) NOT NULL
, `mailingList_mailingListID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mailing_mailingID`, `mailingList_mailingListID`)
, CONSTRAINT `Mailing_MailingList_Mailing`
    FOREIGN KEY (`mailing_mailingID`)
    REFERENCES `Mailing` (`mailingID`)
, CONSTRAINT `Mailing_MailingList_MailingList`
    FOREIGN KEY (`mailingList_mailingListID`)
    REFERENCES `MailingList` (`mailingListID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Mededeling`
( `mededelingID` int(11) NOT NULL AUTO_INCREMENT
, `commissie_commissieID` int(11) NOT NULL
, `doelgroep` enum('ALLEN','LEDEN','ACTIEF') NOT NULL DEFAULT 'ALLEN'
, `prioriteit` enum('LAAG','NORMAAL','HOOG') NOT NULL DEFAULT 'LAAG'
, `datumBegin` date NOT NULL
, `datumEind` date NOT NULL
, `url` varchar(511) DEFAULT NULL
, `omschrijving_NL` varchar(255) NOT NULL DEFAULT ''
, `omschrijving_EN` varchar(255) DEFAULT ''
, `mededeling_NL` text NOT NULL DEFAULT ''
, `mededeling_EN` text DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mededelingID`)
, CONSTRAINT `Mededeling_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Media`
( `mediaID` int(11) NOT NULL AUTO_INCREMENT
, `activiteit_activiteitID` int(11) DEFAULT NULL
, `uploader_contactID` int(11) DEFAULT NULL
, `fotograaf_contactID` int(11) DEFAULT NULL
, `soort` enum('FOTO','FILM') NOT NULL DEFAULT 'FOTO'
, `gemaakt` datetime DEFAULT NULL
, `licence` enum('CC3.0-BY-NC-SA','CC3.0-BY-NC-SA-AES2','CC3.0-BY-NC','CC3.0-BY-ND-AES2') NOT NULL DEFAULT 'CC3.0-BY-NC-SA'
, `premium` int(1) NOT NULL DEFAULT False
, `views` int(11) NOT NULL
, `lock` int(1) NOT NULL DEFAULT False
, `rating` int(11) NOT NULL DEFAULT 50
, `omschrijving` text DEFAULT NULL
, `breedte` int(11) NOT NULL
, `hoogte` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mediaID`)
, CONSTRAINT `Media_Activiteit`
    FOREIGN KEY (`activiteit_activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
, CONSTRAINT `Media_Uploader`
    FOREIGN KEY (`uploader_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Media_Fotograaf`
    FOREIGN KEY (`fotograaf_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Mentor`
( `lid_contactID` int(11) NOT NULL
, `groep_groepID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`lid_contactID`, `groep_groepID`)
, CONSTRAINT `Mentor_Lid`
    FOREIGN KEY (`lid_contactID`)
    REFERENCES `Lid` (`contactID`)
, CONSTRAINT `Mentor_Groep`
    FOREIGN KEY (`groep_groepID`)
    REFERENCES `IntroGroep` (`groepID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Offerte`
( `offerteID` int(11) NOT NULL AUTO_INCREMENT
, `artikel_artikelID` int(11) NOT NULL
, `leverancier_contactID` int(11) NOT NULL
, `waardePerStuk` decimal(8,2) NOT NULL
, `vanafDatum` date NOT NULL
, `vervalDatum` date NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`offerteID`)
, CONSTRAINT `Offerte_Artikel`
    FOREIGN KEY (`artikel_artikelID`)
    REFERENCES `Artikel` (`artikelID`)
, CONSTRAINT `Offerte_Leverancier`
    FOREIGN KEY (`leverancier_contactID`)
    REFERENCES `Leverancier` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Order`
( `orderID` int(11) NOT NULL AUTO_INCREMENT
, `overerving` enum('Order','DictaatOrder') NOT NULL DEFAULT 'Order'
, `artikel_artikelID` int(11) NOT NULL
, `leverancier_contactID` int(11) NOT NULL
, `waardePerStuk` decimal(8,2) DEFAULT NULL
, `datumGeplaatst` date NOT NULL
, `geannuleerd` int(1) NOT NULL DEFAULT False
, `aantal` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`orderID`)
, CONSTRAINT `Order_Artikel`
    FOREIGN KEY (`artikel_artikelID`)
    REFERENCES `Artikel` (`artikelID`)
, CONSTRAINT `Order_Leverancier`
    FOREIGN KEY (`leverancier_contactID`)
    REFERENCES `Leverancier` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Organisatie`
( `contactID` int(11) NOT NULL
, `naam` varchar(255) NOT NULL DEFAULT ''
, `soort` enum('ZUS_UU','ZUS_NL','OG_MED','UU','VAKGROEP','BEDRIJF','OVERIG') NOT NULL DEFAULT 'ZUS_UU'
, `omschrijving` varchar(255) DEFAULT NULL
, `logo` varchar(255) DEFAULT NULL
, PRIMARY KEY (`contactID`)
, CONSTRAINT `Organisatie_fk`
    FOREIGN KEY (`contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PapierMolen`
( `papierMolenID` int(11) NOT NULL AUTO_INCREMENT
, `geplaatst` datetime NOT NULL
, `lid_contactID` int(11) NOT NULL
, `ean` bigint(11) NOT NULL
, `auteur` varchar(255) DEFAULT NULL
, `titel` varchar(255) DEFAULT NULL
, `druk` varchar(255) DEFAULT NULL
, `prijs` decimal(8,2) NOT NULL
, `opmerking` varchar(255) DEFAULT NULL
, `studie` enum('WI','NA','IC','GT','IK') NOT NULL DEFAULT 'WI'
, `verloopdatum` date NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`papierMolenID`)
, CONSTRAINT `PapierMolen_Lid`
    FOREIGN KEY (`lid_contactID`)
    REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Pas`
( `pasID` int(11) NOT NULL AUTO_INCREMENT
, `rfidTag` varchar(255) NOT NULL DEFAULT ''
, `persoon_contactID` int(11) NOT NULL
, `actief` int(1) NOT NULL DEFAULT True
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`pasID`)
, CONSTRAINT `Pas_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Periode`
( `periodeID` int(11) NOT NULL AUTO_INCREMENT
, `datumBegin` date NOT NULL
, `collegejaar` int(11) NOT NULL
, `periodeNummer` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`periodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Persoon`
( `contactID` int(11) NOT NULL
, `voornaam` varchar(255) DEFAULT ''
, `bijnaam` varchar(255) DEFAULT NULL
, `voorletters` varchar(255) DEFAULT NULL
, `geboortenamen` varchar(255) DEFAULT NULL
, `tussenvoegsels` varchar(255) DEFAULT NULL
, `achternaam` varchar(255) NOT NULL DEFAULT ''
, `titelsPrefix` varchar(255) DEFAULT NULL
, `titelsPostfix` varchar(255) DEFAULT NULL
, `geslacht` enum('M','V','?') DEFAULT NULL
, `datumGeboorte` date DEFAULT NULL
, `overleden` int(1) NOT NULL DEFAULT False
, `GPGkey` varchar(255) DEFAULT NULL
, `Rekeningnummer` varchar(255) DEFAULT NULL
, `opmerkingen` text DEFAULT NULL
, `voornaamwoord_woordID` int(11) NOT NULL
, `voornaamwoordZichtbaar` int(1) NOT NULL DEFAULT True
, PRIMARY KEY (`contactID`)
, CONSTRAINT `Persoon_fk`
    FOREIGN KEY (`contactID`)
    REFERENCES `Contact` (`contactID`)
, CONSTRAINT `Persoon_Voornaamwoord`
    FOREIGN KEY (`voornaamwoord_woordID`)
    REFERENCES `Voornaamwoord` (`woordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PersoonBadge`
( `persoon_contactID` int(11) NOT NULL
, `badges` text NOT NULL DEFAULT ''
, `last` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`)
, CONSTRAINT `PersoonBadge_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PersoonIM`
( `persoon_contactID` int(11) NOT NULL
, `soort` enum('ICQ','MSN','JABBER','AOL','YAHOO!') NOT NULL
, `accountName` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`, `soort`)
, CONSTRAINT `PersoonIM_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PersoonKart`
( `persoon_contactID` int(11) NOT NULL
, `naam` varchar(255) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`, `naam`)
, CONSTRAINT `PersoonKart_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PersoonRating`
( `ratingObject_ratingObjectID` int(11) NOT NULL
, `persoon_contactID` int(11) NOT NULL
, `rating` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT '1'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`ratingObject_ratingObjectID`, `persoon_contactID`)
, CONSTRAINT `PersoonRating_RatingObject`
    FOREIGN KEY (`ratingObject_ratingObjectID`)
    REFERENCES `RatingObject` (`ratingObjectID`)
, CONSTRAINT `PersoonRating_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PersoonVoorkeur`
( `persoon_contactID` int(11) NOT NULL
, `taal` enum('GEEN','NL','EN') NOT NULL DEFAULT 'GEEN'
, `bugSortering` enum('ASC','DESC') NOT NULL DEFAULT 'ASC'
, `favoBugCategorie_bugCategorieID` int(11) DEFAULT NULL
, `tourAfgemaakt` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`)
, CONSTRAINT `PersoonVoorkeur_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `PersoonVoorkeur_FavoBugCategorie`
    FOREIGN KEY (`favoBugCategorie_bugCategorieID`)
    REFERENCES `BugCategorie` (`bugCategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Pin`
( `transactieID` int(11) NOT NULL
, `rekening` enum('507122690') NOT NULL DEFAULT '507122690'
, `apparaat` enum('SEPAY','XENTISSIMO') NOT NULL DEFAULT 'SEPAY'
, PRIMARY KEY (`transactieID`)
, CONSTRAINT `Pin_fk`
    FOREIGN KEY (`transactieID`)
    REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Planner`
( `plannerID` int(11) NOT NULL AUTO_INCREMENT
, `persoon_contactID` int(11) NOT NULL
, `titel` varchar(255) NOT NULL DEFAULT ''
, `omschrijving` text NOT NULL DEFAULT ''
, `mail` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`plannerID`)
, CONSTRAINT `Planner_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PlannerData`
( `plannerDataID` int(11) NOT NULL AUTO_INCREMENT
, `planner_plannerID` int(11) NOT NULL
, `momentBegin` datetime NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`plannerDataID`)
, CONSTRAINT `PlannerData_Planner`
    FOREIGN KEY (`planner_plannerID`)
    REFERENCES `Planner` (`plannerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PlannerDeelnemer`
( `plannerData_plannerDataID` int(11) NOT NULL
, `persoon_contactID` int(11) NOT NULL
, `antwoord` enum('ONBEKEND','LIEVER_NIET','NEE','JA') NOT NULL DEFAULT 'ONBEKEND'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`plannerData_plannerDataID`, `persoon_contactID`)
, CONSTRAINT `PlannerDeelnemer_PlannerData`
    FOREIGN KEY (`plannerData_plannerDataID`)
    REFERENCES `PlannerData` (`plannerDataID`)
, CONSTRAINT `PlannerDeelnemer_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ProfielFoto`
( `persoon_contactID` int(11) NOT NULL
, `media_mediaID` int(11) NOT NULL
, `status` enum('OUD','HUIDIG','NIEUW') NOT NULL DEFAULT 'OUD'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`, `media_mediaID`)
, CONSTRAINT `ProfielFoto_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `ProfielFoto_Media`
    FOREIGN KEY (`media_mediaID`)
    REFERENCES `Media` (`mediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Rating`
( `media_mediaID` int(11) NOT NULL
, `persoon_contactID` int(11) NOT NULL
, `rating` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT '1'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`media_mediaID`, `persoon_contactID`)
, CONSTRAINT `Rating_Media`
    FOREIGN KEY (`media_mediaID`)
    REFERENCES `Media` (`mediaID`)
, CONSTRAINT `Rating_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `RatingObject`
( `ratingObjectID` int(11) NOT NULL AUTO_INCREMENT
, `rating` int(11) NOT NULL DEFAULT 50
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`ratingObjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Register`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `label` varchar(255) NOT NULL DEFAULT ''
, `beschrijving` varchar(255) NOT NULL DEFAULT ''
, `waarde` varchar(255) DEFAULT NULL
, `type` enum('STRING','INT','BOOLEAN','FLOAT') NOT NULL DEFAULT 'STRING'
, `auth` enum('BESTUUR','GOD','INGELOGD') NOT NULL DEFAULT 'BESTUUR'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Schuld`
( `transactieID` int(11) NOT NULL
, `afgeschreven` datetime DEFAULT NULL
, `transactie_transactieID` int(11) DEFAULT NULL
, PRIMARY KEY (`transactieID`)
, CONSTRAINT `Schuld_fk`
    FOREIGN KEY (`transactieID`)
    REFERENCES `Transactie` (`transactieID`)
, CONSTRAINT `Schuld_Transactie`
    FOREIGN KEY (`transactie_transactieID`)
    REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Studie`
( `studieID` int(11) NOT NULL AUTO_INCREMENT
, `naam_NL` varchar(255) NOT NULL DEFAULT ''
, `naam_EN` varchar(255) NOT NULL DEFAULT ''
, `soort` enum('GT','IC','IK','NA','WI','WIT','OVERIG') NOT NULL DEFAULT 'GT'
, `fase` enum('BA','MA') NOT NULL DEFAULT 'BA'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`studieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Tag`
( `tagID` int(11) NOT NULL AUTO_INCREMENT
, `media_mediaID` int(11) NOT NULL
, `persoon_contactID` int(11) DEFAULT NULL
, `commissie_commissieID` int(11) DEFAULT NULL
, `introgroep_groepID` int(11) DEFAULT NULL
, `collectie_collectieID` int(11) DEFAULT NULL
, `tagger_contactID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`tagID`)
, CONSTRAINT `Tag_Media`
    FOREIGN KEY (`media_mediaID`)
    REFERENCES `Media` (`mediaID`)
, CONSTRAINT `Tag_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
, CONSTRAINT `Tag_Commissie`
    FOREIGN KEY (`commissie_commissieID`)
    REFERENCES `Commissie` (`commissieID`)
, CONSTRAINT `Tag_Introgroep`
    FOREIGN KEY (`introgroep_groepID`)
    REFERENCES `IntroGroep` (`groepID`)
, CONSTRAINT `Tag_Collectie`
    FOREIGN KEY (`collectie_collectieID`)
    REFERENCES `Collectie` (`collectieID`)
, CONSTRAINT `Tag_Tagger`
    FOREIGN KEY (`tagger_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TagArea`
( `tag_tagID` int(11) NOT NULL
, `beginPixelX` int(11) NOT NULL
, `eindPixelX` int(11) NOT NULL
, `beginPixelY` int(11) NOT NULL
, `eindPixelY` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`tag_tagID`)
, CONSTRAINT `TagArea_Tag`
    FOREIGN KEY (`tag_tagID`)
    REFERENCES `Tag` (`tagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Telling`
( `tellingID` int(11) NOT NULL AUTO_INCREMENT
, `wanneer` datetime NOT NULL
, `teller_contactID` int(11) NOT NULL
, `Opmerking` text NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`tellingID`)
, CONSTRAINT `Telling_Teller`
    FOREIGN KEY (`teller_contactID`)
    REFERENCES `Lid` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TellingItem`
( `telling_tellingID` int(11) NOT NULL
, `voorraad_voorraadID` int(11) NOT NULL
, `aantal` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`telling_tellingID`, `voorraad_voorraadID`)
, CONSTRAINT `TellingItem_Telling`
    FOREIGN KEY (`telling_tellingID`)
    REFERENCES `Telling` (`tellingID`)
, CONSTRAINT `TellingItem_Voorraad`
    FOREIGN KEY (`voorraad_voorraadID`)
    REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Template`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `label` varchar(255) NOT NULL DEFAULT ''
, `beschrijving` varchar(255) NOT NULL DEFAULT ''
, `waarde` text NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Tentamen`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `vak_vakID` int(11) NOT NULL
, `datum` date NOT NULL
, `naam` varchar(255) DEFAULT NULL
, `oud` int(1) NOT NULL DEFAULT False
, `tentamen` int(1) NOT NULL DEFAULT False
, `opmerking` text DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `Tentamen_Vak`
    FOREIGN KEY (`vak_vakID`)
    REFERENCES `Vak` (`vakID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TentamenUitwerking`
( `uitwerkingID` int(11) NOT NULL AUTO_INCREMENT
, `tentamen_id` int(11) NOT NULL
, `uploader_contactID` int(11) NOT NULL
, `wanneer` datetime NOT NULL
, `opmerking` text DEFAULT NULL
, `gecontroleerd` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`uitwerkingID`)
, CONSTRAINT `TentamenUitwerking_Tentamen`
    FOREIGN KEY (`tentamen_id`)
    REFERENCES `Tentamen` (`id`)
, CONSTRAINT `TentamenUitwerking_Uploader`
    FOREIGN KEY (`uploader_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TinyMailingUrl`
( `mailing_mailingID` int(11) NOT NULL
, `tinyUrl_id` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`mailing_mailingID`, `tinyUrl_id`)
, CONSTRAINT `TinyMailingUrl_Mailing`
    FOREIGN KEY (`mailing_mailingID`)
    REFERENCES `Mailing` (`mailingID`)
, CONSTRAINT `TinyMailingUrl_TinyUrl`
    FOREIGN KEY (`tinyUrl_id`)
    REFERENCES `TinyUrl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TinyUrl`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `tinyUrl` varchar(255) NOT NULL DEFAULT ''
, `outgoingUrl` varchar(511) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TinyUrlHit`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `tinyUrl_id` int(11) NOT NULL
, `host` varchar(255) DEFAULT NULL
, `hitTime` datetime NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `TinyUrlHit_TinyUrl`
    FOREIGN KEY (`tinyUrl_id`)
    REFERENCES `TinyUrl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Transactie`
( `transactieID` int(11) NOT NULL AUTO_INCREMENT
, `overerving` enum('Transactie','DibsTransactie','Pin','Giro','Schuld','iDeal') NOT NULL DEFAULT 'Transactie'
, `rubriek` enum('BOEKWEB','COLAKAS','WWW') NOT NULL DEFAULT 'BOEKWEB'
, `soort` enum('VERKOOP','DONATIE','BETALING','AFSCHRIJVING') NOT NULL DEFAULT 'VERKOOP'
, `contact_contactID` int(11) DEFAULT NULL
, `bedrag` decimal(8,2) DEFAULT NULL
, `uitleg` varchar(255) NOT NULL DEFAULT ''
, `wanneer` datetime NOT NULL
, `status` enum('INACTIVE','OPEN','SUCCESS','FAILURE','CANCELLED','EXPIRED','OPENPOGING2','OPENPOGING3','OPENPOGING4','OPENPOGING5','BANKFAILURE') NOT NULL DEFAULT 'SUCCESS'
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`transactieID`)
, CONSTRAINT `Transactie_Contact`
    FOREIGN KEY (`contact_contactID`)
    REFERENCES `Contact` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Vacature`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `contractonderdeel_id` int(11) NOT NULL
, `datumVerloop` date NOT NULL
, `datumPlaatsing` date NOT NULL
, `type` enum('BIJBAAN','PROMOTIEPLAATS','STAGEPLEK','STARTERSFUNCTIE','AFSTUDEEROPDRACHT','WERKSTUDENTSCHAP') NOT NULL DEFAULT 'BIJBAAN'
, `ica` int(1) NOT NULL DEFAULT False
, `iku` int(1) NOT NULL DEFAULT False
, `na` int(1) NOT NULL DEFAULT False
, `wis` int(1) NOT NULL DEFAULT False
, `titel_NL` varchar(255) NOT NULL DEFAULT ''
, `titel_EN` varchar(255) NOT NULL DEFAULT ''
, `inleiding_NL` text DEFAULT NULL
, `inleiding_EN` text DEFAULT NULL
, `inhoud_NL` text DEFAULT NULL
, `inhoud_EN` text DEFAULT NULL
, `views` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `Vacature_Contractonderdeel`
    FOREIGN KEY (`contractonderdeel_id`)
    REFERENCES `Contractonderdeel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `VacatureStudie`
( `Studie_studieID` int(11) NOT NULL
, `Vacature_id` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`Studie_studieID`, `Vacature_id`)
, CONSTRAINT `VacatureStudie_Studie`
    FOREIGN KEY (`Studie_studieID`)
    REFERENCES `Studie` (`studieID`)
, CONSTRAINT `VacatureStudie_Vacature`
    FOREIGN KEY (`Vacature_id`)
    REFERENCES `Vacature` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Vak`
( `vakID` int(11) NOT NULL AUTO_INCREMENT
, `code` varchar(255) NOT NULL DEFAULT ''
, `naam` varchar(255) DEFAULT NULL
, `departement` enum('NA','WI','IC','OV') NOT NULL DEFAULT 'NA'
, `opmerking` varchar(255) DEFAULT NULL
, `inTentamenLijsten` int(1) NOT NULL DEFAULT True
, `tentamensOutdated` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`vakID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `VakStudie`
( `vak_vakID` int(11) NOT NULL
, `studie_studieID` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`vak_vakID`, `studie_studieID`)
, CONSTRAINT `VakStudie_Vak`
    FOREIGN KEY (`vak_vakID`)
    REFERENCES `Vak` (`vakID`)
, CONSTRAINT `VakStudie_Studie`
    FOREIGN KEY (`studie_studieID`)
    REFERENCES `Studie` (`studieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Verkoop`
( `voorraadMutatieID` int(11) NOT NULL
, `transactie_transactieID` int(11) NOT NULL
, PRIMARY KEY (`voorraadMutatieID`)
, CONSTRAINT `Verkoop_fk`
    FOREIGN KEY (`voorraadMutatieID`)
    REFERENCES `VoorraadMutatie` (`voorraadMutatieID`)
, CONSTRAINT `Verkoop_Transactie`
    FOREIGN KEY (`transactie_transactieID`)
    REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `VerkoopPrijs`
( `voorraad_voorraadID` int(11) NOT NULL
, `wanneer` datetime NOT NULL
, `prijs` decimal(8,2) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`voorraad_voorraadID`, `wanneer`)
, CONSTRAINT `VerkoopPrijs_Voorraad`
    FOREIGN KEY (`voorraad_voorraadID`)
    REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Verplaatsing`
( `voorraadMutatieID` int(11) NOT NULL
, `tegenVoorraad_voorraadID` int(11) NOT NULL
, PRIMARY KEY (`voorraadMutatieID`)
, CONSTRAINT `Verplaatsing_fk`
    FOREIGN KEY (`voorraadMutatieID`)
    REFERENCES `VoorraadMutatie` (`voorraadMutatieID`)
, CONSTRAINT `Verplaatsing_TegenVoorraad`
    FOREIGN KEY (`tegenVoorraad_voorraadID`)
    REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Voornaamwoord`
( `woordID` int(11) NOT NULL AUTO_INCREMENT
, `soort_NL` varchar(255) NOT NULL DEFAULT ''
, `soort_EN` varchar(255) NOT NULL DEFAULT ''
, `onderwerp_NL` varchar(255) NOT NULL DEFAULT ''
, `onderwerp_EN` varchar(255) NOT NULL DEFAULT ''
, `voorwerp_NL` varchar(255) NOT NULL DEFAULT ''
, `voorwerp_EN` varchar(255) NOT NULL DEFAULT ''
, `bezittelijk_NL` varchar(255) NOT NULL DEFAULT ''
, `bezittelijk_EN` varchar(255) NOT NULL DEFAULT ''
, `wederkerend_NL` varchar(255) NOT NULL DEFAULT ''
, `wederkerend_EN` varchar(255) NOT NULL DEFAULT ''
, `ouder_NL` varchar(255) NOT NULL DEFAULT ''
, `ouder_EN` varchar(255) NOT NULL DEFAULT ''
, `kind_NL` varchar(255) NOT NULL DEFAULT ''
, `kind_EN` varchar(255) NOT NULL DEFAULT ''
, `brusje_NL` varchar(255) NOT NULL DEFAULT ''
, `brusje_EN` varchar(255) NOT NULL DEFAULT ''
, `kozijn_NL` varchar(255) NOT NULL DEFAULT ''
, `kozijn_EN` varchar(255) NOT NULL DEFAULT ''
, `kindGrootouder_NL` varchar(255) NOT NULL DEFAULT ''
, `kindGrootouder_EN` varchar(255) NOT NULL DEFAULT ''
, `kleinkindOuder_NL` varchar(255) NOT NULL DEFAULT ''
, `kleinkindOuder_EN` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`woordID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Voorraad`
( `voorraadID` int(11) NOT NULL AUTO_INCREMENT
, `artikel_artikelID` int(11) NOT NULL
, `locatie` enum('BW1','MAGAZIJN','BOEKENHOK','EJBV','VIRTUEEL') NOT NULL DEFAULT 'BW1'
, `waardePerStuk` decimal(8,2) NOT NULL
, `btw` enum('21','6','0','NULL') NOT NULL DEFAULT 'NULL'
, `omschrijving` text DEFAULT NULL
, `verkoopbaar` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`voorraadID`)
, CONSTRAINT `Voorraad_Artikel`
    FOREIGN KEY (`artikel_artikelID`)
    REFERENCES `Artikel` (`artikelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `VoorraadMutatie`
( `voorraadMutatieID` int(11) NOT NULL AUTO_INCREMENT
, `overerving` enum('VoorraadMutatie','Levering','Verplaatsing','Verkoop','LeverancierRetour') NOT NULL DEFAULT 'VoorraadMutatie'
, `voorraad_voorraadID` int(11) NOT NULL
, `wanneer` datetime NOT NULL
, `aantal` int(11) NOT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`voorraadMutatieID`)
, CONSTRAINT `VoorraadMutatie_Voorraad`
    FOREIGN KEY (`voorraad_voorraadID`)
    REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Wachtwoord`
( `persoon_contactID` int(11) NOT NULL
, `login` varchar(255) NOT NULL DEFAULT ''
, `wachtwoord` varchar(255) DEFAULT NULL
, `laatsteLogin` date DEFAULT NULL
, `magic` varchar(255) DEFAULT NULL
, `magicExpire` datetime DEFAULT NULL
, `crypthash` varchar(255) DEFAULT NULL
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`persoon_contactID`)
, CONSTRAINT `Wachtwoord_Persoon`
    FOREIGN KEY (`persoon_contactID`)
    REFERENCES `Persoon` (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `iDeal`
( `transactieID` int(11) NOT NULL
, `titel` varchar(255) NOT NULL DEFAULT ''
, `voorraad_voorraadID` int(11) NOT NULL
, `externeID` varchar(255) DEFAULT NULL
, `klantNaam` varchar(255) DEFAULT NULL
, `klantStad` varchar(255) DEFAULT NULL
, `expire` datetime NOT NULL
, `entranceCode` varchar(255) NOT NULL DEFAULT ''
, `magOpvragenTijdstip` datetime NOT NULL
, `emailadres` varchar(255) DEFAULT NULL
, `returnURL` varchar(255) DEFAULT NULL
, PRIMARY KEY (`transactieID`)
, CONSTRAINT `iDeal_fk`
    FOREIGN KEY (`transactieID`)
    REFERENCES `Transactie` (`transactieID`)
, CONSTRAINT `iDeal_Voorraad`
    FOREIGN KEY (`voorraad_voorraadID`)
    REFERENCES `Voorraad` (`voorraadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `iDealAntwoord`
( `ideal_transactieID` int(11) NOT NULL
, `vraag_activiteitID` int(11) NOT NULL
, `vraag_vraagID` int(11) NOT NULL
, `antwoord` varchar(255) NOT NULL DEFAULT ''
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`ideal_transactieID`, `vraag_activiteitID`, `vraag_vraagID`)
, CONSTRAINT `iDealAntwoord_Ideal`
    FOREIGN KEY (`ideal_transactieID`)
    REFERENCES `iDeal` (`transactieID`)
, CONSTRAINT `iDealAntwoord_Vraag`
    FOREIGN KEY (`vraag_activiteitID`, `vraag_vraagID`)
    REFERENCES `ActiviteitVraag` (`activiteit_activiteitID`, `vraagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `iDealKaartje`
( `artikelID` int(11) NOT NULL
, `activiteit_activiteitID` int(11) NOT NULL
, `autoInschrijven` int(1) NOT NULL DEFAULT False
, `returnURL` varchar(255) DEFAULT NULL
, `externVerkrijgbaar` int(1) NOT NULL DEFAULT False
, `digitaalVerkrijgbaar` enum('NAMENLIJST','CODE','PERSOONLIJK') NOT NULL DEFAULT 'NAMENLIJST'
, `magScannen` int(1) NOT NULL DEFAULT False
, PRIMARY KEY (`artikelID`)
, CONSTRAINT `iDealKaartje_fk`
    FOREIGN KEY (`artikelID`)
    REFERENCES `Artikel` (`artikelID`)
, CONSTRAINT `iDealKaartje_Activiteit`
    FOREIGN KEY (`activiteit_activiteitID`)
    REFERENCES `Activiteit` (`activiteitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `iDealKaartjeCode`
( `id` int(11) NOT NULL AUTO_INCREMENT
, `kaartje_artikelID` int(11) NOT NULL
, `code` varchar(255) NOT NULL DEFAULT ''
, `transactie_transactieID` int(11) DEFAULT NULL
, `gescand` int(1) NOT NULL DEFAULT False
, `gewijzigdWanneer` datetime DEFAULT NULL
, `gewijzigdWie` int(11) DEFAULT NULL
, PRIMARY KEY (`id`)
, CONSTRAINT `iDealKaartjeCode_Kaartje`
    FOREIGN KEY (`kaartje_artikelID`)
    REFERENCES `iDealKaartje` (`artikelID`)
, CONSTRAINT `iDealKaartjeCode_Transactie`
    FOREIGN KEY (`transactie_transactieID`)
    REFERENCES `Transactie` (`transactieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
