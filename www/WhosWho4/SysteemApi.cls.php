<?

/**
 * @brief Wordt gegooid door de SysteemApi-klasse als IPA eruit ligt.
 *
 * In dat geval werkt (bijna) geen methode van SysteemApi,
 * dus het beste is om een goede error te tonen.
 */
class IPALigtEruitException extends \Exception { }

class SysteemApi
{
	protected $connection; // Het LDAP-connectie-object
	protected $base_dn; // De base van de dn om in te zoeken

	/*** CONSTRUCTOR ***/

	/**
	 * @brief Constructor: maakt verbinding met LDAP.
	 *
	 * Als verbinding niet werkt, wordt een IPALigtEruitException gegooid.
	 */
	public function __construct()
	{
		$this->base_dn = 'dc=a-eskwadraat,dc=nl';

		// Maak een connectie met de LDAP-servers.
		// Uit PHP docs: This function does *not* open a connection.
		// It checks whether the given parameters are plausible and can be used
		// to open a connection as soon as one is needed.
		$this->connection = ldap_connect(LDAP_SERVER);

		// Check of de verbinding succesvol was door op de webcie te zoeken.
		// Dit is redelijk SOEPMES maar blijkbaar kan het niet beter...
		$use_dn = 'cn=users,cn=accounts,' . $this->base_dn;
		$search = @ldap_search($this->connection, $use_dn, 'uid=webcie', ['homeDirectory']);
		if ($search === false)
		{
			throw new IPALigtEruitException();
		}
	}

	static public function execIpa($command, $show_output = false)
	{
		$output = '';
		$ret = 0;

		exec('kinit website-ipa -kt ' . KEYTAB_FILE . '; ' . $command . ' 2>&1', $output, $ret);

		if($ret == 0 && !$show_output)
			return false;
		else
			return $output;
	}

	/**
	 *  Haalt het uid op van een persoon op de site
	 *
	 * @param persoon De persoon om het uid van te zoeken
	 *
	 * @return NULL als het persoon geen uid in het systeem heeft, anders het uid
	 */
	public function getUid(Persoon $persoon)
	{
		$filter = 'memberId=' . $persoon->geefID();
		$use_dn = 'cn=users,cn=accounts,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('uid'));
		$result = ldap_get_entries($this->connection, $search);

		if(!$result)
			return NULL;

		if($result['count'] == 1)
			return $result[0]['uid'][0];

		return NULL;
	}

	/**
	 *  Kijkt of de login bestaat
	 *
	 * @param login De uid om te checken
	 *
	 * @return True als het systeemaccount bestaat, false anders
	 */
	public function bestaatLogin($login)
	{
		// TODO: dit zou je ook gewoon kunnen schrijven als een null check van
		// lidnrVanLogin, maar ik heb geen zin om te checken of dat niet dingten
		// kapot maakt.

		$filter = 'uid=' . $login;
		$use_dn = 'cn=users,cn=accounts,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('uid'));
		$result = ldap_get_entries($this->connection, $search);

		if(!$result)
			return false;

		if($result['count'] == 1)
			return true;

		return false;
	}

	/** Zoek een lidnummer bij een bepaalde loginnaam.
	 *
	 * @param login De uid om te checken
	 *
	 * @return Een string van het bijbehorende lidnummer, anders null
	 */
	public function lidnrVanLogin($login)
	{
		$filter = 'uid=' . $login;
		$use_dn = 'cn=users,cn=accounts,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('uid', 'memberid'));
		$result = ldap_get_entries($this->connection, $search);

		if(!$result)
			return null;

		if($result['count'] == 1)
			return $result[0]['memberid'][0];

		return null;
	}

	/**
	 *  Kijkt of een lid in een commissie zit op het systeem
	 *
	 * @param uid De uid van het lid
	 * @param cie De commissie om in te zoeken
	 *
	 * @return True als het lid in de cie zit, false anders
	 */
	public function zitInCommissie($uid, Commissie $cie)
	{
		// De cielogin moet natuurlijk wel bestaan!
		if(!$this->bestaatLogin($cie->getLogin()))
			return false;

		$cieLogin = $cie->getLogin();

		$filter = '(&(cn=' . $cieLogin . ')(memberUid=' . $uid . '))';
		// omdat we op memberUid zoeken is de dn hier anders dan normaal
		$use_dn = 'cn=groups,cn=compat,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('uid'));
		$result = ldap_get_entries($this->connection, $search);

		if(!$result)
			return false;

		if($result['count'] == 1)
			return true;

		return false;
	}

	/**
	 *  Zoekt de locatie van de homedir op het fs van een commissie
	 *
	 * @param cie De commissie om de homedir bij te zoeken
	 *
	 * @return False als er geen homedir gevonden is, anders de homedir als string
	 */
	public function getCieHome(Commissie $cie)
	{
		// De cielogin moet wel bestaan
		if(!$this->bestaatLogin($cie->getLogin()))
			return false;

		$cieLogin = $cie->getLogin();

		$filter = 'uid=' . $cieLogin;
		$use_dn = 'cn=users,cn=accounts,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('homeDirectory'));
		$result = ldap_get_entries($this->connection, $search);

		if(!$result)
			return false;

		// Note: kennelijk is de index 'homedirectory' in de return niet met
		// een hoofdletter geschreven, maar de property in ldap wel :S
		if($result['count'] == 1)
			return $result[0]['homedirectory'][0];

		return false;
	}

	/**
	 *  Kijkt of een emailadres in de forward van een cie staat
	 *
	 * @param email de email om op te checken als string
	 * @param cie De commissie om in de forward te zoeken
	 *
	 * @return False als de email niet in de forward zit, true anders
	 */
	public function zitInForward($email, Commissie $cie)
	{
		// De cielogin moet wel op het systeem bestaan
		if(!$this->bestaatLogin($cie->getLogin()))
			return false;

		$cieHome = $this->getCieHome($cie);

		$output = '';
		$res = 0;

		// Grep op de email in de forward
		exec("grep -q '" . $email . "' " . $cieHome . "/.forward", $output, $res);

		// Grep geeft return-waarde 0 als de string WEL gevonden is
		return !$res;
	}

	/**
	 *  Voegt een lid-account toe aan een cie op het systeem
	 *
	 * @param uid De uid van het lid-account
	 * @param cie De commissie om aan toe te voegen
	 *
	 * @return False als alles goed gaat, anders een array met als index integers
	 *  die horen bij waar het mis ging en als waarden de fout die zich voordeed
	 */
	public function voegToeAanCie($uid, Commissie $cie)
	{
		// De cielogin moet wel op het systeem bestaan
		if(!$this->bestaatLogin($cie->getLogin()))
			return array(1 => array(_('Cielogin bestaat niet!')));

		$cieLogin = $cie->getLogin();
		$return = array();

		// Als het lid al in de cie ziet hoeven we niks te doen
		if(!$this->zitInCommissie($uid, $cie))
		{
			$output = '';
			$ret = 0;

			// Roep ipa aan om het lid in de groep te stoppen
			$output = self::execIpa('ipa group-add-member ' . $cieLogin . ' --users=' . $uid);

			// error code 1: kon persoon niet in de cie stoppen
			if($output)
				$return[1] = $output;
		}

		$output = '';
		$ret = 0;

		// Stop het lid in de forward. Als het lid al in de forward staat gebeurt er niets
		exec('sudo -u ' . $cieLogin . ' /usr/local/bin/changecieforward add ' . $uid . ' 2>&1',
			$output, $ret);

		// error code 2: kon persoon niet aan de forward toevoegen
		if($ret != 0)
			$return[2] = $output;

		$output = '';
		$ret = 0;

		// Maak symlinks naar de cie aan in de homedir van het lid. Als deze al
		// bestaan gebeurt er hier niks
		exec('sudo -u ' . $uid . ' /usr/local/bin/changeciesymlink add ' . $cieLogin . ' 2>&1',
			$output, $ret);

		// error code 3: kon niet de cie linken
		if($ret != 0)
			$return[3] = $output;

		if(sizeof($return) == 0)
			return false;
		return $return;
	}

	/**
	 *  Haal een lid weg uit een cie op het systeem
	 *
	 * @param uid De uid van het lid om weg te halen
	 * @param cie De commissie om bij weg te halen
	 *
	 * @return False als alles goed gaat, anders een array met als index integers
	 *  die horen bij waar het mis ging en als waarden de fout die zich voordeed
	 */
	public function haalWegUitCie($uid, Commissie $cie)
	{
		// De cielogin moet wel op het systeem bestaan
		if(!$this->bestaatLogin($cie->getLogin()))
			return array(1 => array(_('Cielogin bestaat niet!')));

		$cieLogin = $cie->getLogin();
		$return = array();

		// Als het lid niet in de cie zit op het systeem hoeven we hier niets te doen
		if($this->zitInCommissie($uid, $cie))
		{
			$output = '';
			$ret = 0;

			// Roep ipa aan om het lid in de groep te stoppen
			$output = self::execIpa('ipa group-remove-member ' . $cieLogin . ' --users=' . $uid);

			// error code 1: kon persoon niet uit de cie halen
			if($output)
				$return[1] = $output;
		}

		$output = '';
		$ret = 0;

		exec('sudo -u ' . $cieLogin . ' /usr/local/bin/changecieforward del ' . $uid . ' 2>&1',
			$output, $ret);

		// error code 2: kon persoon niet van de forward halen
		if($ret != 0)
			$return[2] = $output;

		$output = '';
		$ret = 0;

		exec('sudo -u ' . $uid . ' /usr/local/bin/changeciesymlink del ' . $cieLogin . ' 2>&1',
			$output, $ret);

		// error code 3: kon de cie niet unlinken
		if($ret != 0)
			$return[3] = $output;

		if(sizeof($return) == 0)
			return false;
		return $return;
	}

	/**
	 *  Voegt een email toe aan de forward van een cie op het systeem
	 *
	 * @param email Het emailadres om toe te voegen
	 * @param cie De commissie om aan toe te voegen
	 *
	 * @return False als het gelukt is, anders een array met de foutmelding
	 */
	public function voegEmailToeAanCie($email, Commissie $cie)
	{
		// De cielogin moet wel bestaan
		if(!$this->bestaatLogin($cie->getLogin()))
			return array(1 => array(_('Cielogin bestaat niet!')));

		$cieLogin = $cie->getLogin();

		$output = '';
		$ret = 0;

		exec('sudo -u ' . $cieLogin . ' /usr/local/bin/changecieforward add ' . $email,
			$output, $ret);

		if($ret != 0)
			return array(1 => $output);

		return false;
	}

	/**
	 *  Haalt een email weg uit de forward van een cie op het systeem
	 *
	 * @param email Het emailadres om toe te voegen
	 * @param cie De commissie om aan toe te voegen
	 *
	 * @return False als het gelukt is, anders een array met de foutmelding
	 */
	public function haalEmailWegUitCie($email, Commissie $cie)
	{
		// De cielogin moet wel bestaan
		if(!$this->bestaatLogin($cie->getLogin()))
			return array(1 => array(_('Cielogin bestaat niet!')));

		$cieLogin = $cie->getLogin();

		$output = '';
		$ret = 0;

		exec('sudo -u ' . $cieLogin . ' /usr/local/bin/changecieforward del ' . $email,
			$output, $ret);

		if($ret != 0)
			return array(1 => $output);

		return false;
	}

	/**
	 *  Checkt op aliassen of de accountnaam nog vrij is
	 *
	 * @param login De naam van de login om tegen de aliassen te checken
	 *
	 * @return True als het mag, false anders
	 */
	public function aliasCheck($login)
	{
		$output = '';
		$ret = 0;

		exec('kinit website-ipa -kt ' . KEYTAB_FILE . '; '
			. 'curl --silent --negotiate -u : http://vm-sysop.a-eskwadraat.nl/api/aliasCheck?alias=' . $login,
			$output, $ret);

		if($output[0] != 'OK')
			return false;

		if($this->bestaatLogin($login))
			return false;

		return true;
	}

	/**
	 *  Voegt de user en groep toe wat bij het totale cieadd hoort
	 *
	 * @param cie De commissie om het systeemaccount bij toe te voegen
	 * @param login De loginnaam van het nieuwe account
	 *
	 * @return False als alles goed gaat, anders de error als string
	 */
	public function cieaddUser(Commissie $cie, $login)
	{
		$first = 'cie';
		$type = 'cies';

		if($cie->getSoort() == 'GROEP')
			$first = 'groep';
		else if($cie->getSoort() == 'DISPUUT')
		{
			$first = 'extern';
			$type = 'extern';
		}

		$name = $cie->getNaam();

		// Maak eerst de groep aan
		$output = self::execIpa("ipa group-add $login --desc=\"$name\"");

		if($output)
			return $output;

		// Haal het group-id op
		$filter = 'cn=' . $login;
		$use_dn = 'cn=groups,cn=accounts,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('gidNumber'));
		$result = ldap_get_entries($this->connection, $search);

		$gid = $result[0]['gidnumber'][0];

		// maak de user aan
		$output = self::execIpa("ipa user-add $login --first=$first --last=\"$name\" --cn=\"$first $name\" "
			. "--homedir=\"/home/$type/$login\" --uid=$gid --noprivate --gid=$gid "
			. "--gecos=\"$name\" --displayname=\"$name\"");

		if($output)
			return $output;

		// Voeg de nieuwe cie in de algemene group toe
		$output = self::execIpa("ipa group-add-member $type --users=$login");

		if($output)
			return $output;

		// Voeg de cie aan z'n eigen group toe
		$output = self::execIpa("ipa group-add-member $login --users=$login");

		if($output)
			return $output;

		return false;
	}

	/**
	 *  Maakt de sudo-regels aan voor een nieuwe commissie wat bij het
	 *  total cieadd-script hoort
	 *
	 * @param cie De commissie waarvoor het systeemaccount wordt aangemaakt
	 * @param login De accountnaam van het nieuwe account
	 *
	 * @return False als alles goed gaat, anders een string met de fout
	 */
	public function cieaddSudo(Commissie $cie, $login)
	{
		$first = 'cie';
		$type = 'cies';

		if($cie->getSoort() == 'GROEP')
			$first = 'groep';
		else if($cie->getSoort() == 'DISPUUT')
		{
			$first = 'extern';
			$type = 'extern';
		}

		$name = $cie->getNaam();

		// Voeg de sudoregels toe
		$output = self::execIpa("ipa sudorule-add $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-add-user --group $login $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-add-runasuser --users=$login $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-mod --cmdcat='all' $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-add-option --sudooption=\"env_keep=XAUTHORITY\" $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-add-option --sudooption=\"env_keep=DISPLAY\" $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-add-option --sudooption=\"!authenticate\" $login");

		if($output)
			return $output;

		$output = self::execIpa("ipa sudorule-add-host $login --hostgroups=\"workstations\"");

		if($output)
			return $output;

		return false;
	}

	/**
	 *  Maakt een homedir van een nieuwe cie-account aan wat bij het
	 *  totale cieadd hoort
	 *
	 * @param cie De commissie waarvoor een homedir wordt aangemaakt
	 * @param login De accoutnaam van het nieuwe account
	 *
	 * @return False als alles goed gaat, een string met de foutmelding anders
	 */
	public function cieaddHomedir(Commissie $cie, $login)
	{
		$first = 'cie';
		$type = 'cies';

		if($cie->getSoort() == 'GROEP')
			$first = 'groep';
		else if($cie->getSoort() == 'DISPUUT')
		{
			$first = 'extern';
			$type = 'extern';
		}

		$name = $cie->getNaam();

		$output = 0;
		$ret = 0;
		exec('sudo /usr/local/bin/createciehomedir ' . $login . ' /home/' . $type . '/' . $login . ' 2>&1', $output, $ret);

		if($ret != 0)
			return $output;

		return false;
	}

	public function mensjeaddUser(Persoon $persoon, $login)
	{
		$first = $persoon->getVoornaam();
		$last = ltrim(ucfirst($persoon->getTussenvoegsels()) . ' ' . $persoon->getAchternaam());

		$homedir = '/home/mensjes/' . $login;

		// Maak eerst de groep aan
		$output = self::execIpa("ipa user-add $login --first=\"$first\" --last=\"$last\" "
			. "--cn=\"$first $last\" --homedir=\"$homedir\" --displayname=\"$first $last\" "
			. "--setattr=memberid=\"".$persoon->geefID()."\"");

		if($output)
			return $output;

		$output = self::execIpa("ipa group-add-member mensjes --users=\"$login\"");

		if($output)
			return $output;

		return false;
	}

	public function mensjeaddHomedir(Persoon $persoon, $login)
	{
		$homedir = '/home/mensjes/' . $login;
		$email = $persoon->getEmail();

		$output = 0;
		$ret = 0;
		exec('sudo /usr/local/bin/makemensjehomedir ' . $login . ' ' . $homedir . ' ' . $email . ' 2>&1', $output, $ret);

		if($ret != 0)
			return $output;

		return false;
	}

	/**
	 * Verandert het systeemaccount-wachtwoord van een persoon naar een random
	 * gegenereerde string.
	 *
	 * Als alles goed gegaan is, wordt er een array $A gereturnd waarbij
	 * $A['status'] = 'OK'. Verder bevat $A['output'] een array van regels die
	 * de output is van het achterliggende shell-commando wat uitgevoerd is.
	 * In $A['wachtwoord'] is het nieuwe wachtwoord van het account, die
	 * gebruikt kan worden door $login om mee in te loggen. De eerste keer dat
	 * zo iemand inlogt, wordt deze persoon gevraagd om zijn wachtwoord te
	 * veranderen.
	 *
	 * Als LDAP eruit ligt, of iets anders fout gaat. Dan wordt alle uitvoer van
	 * de commando's in $A['fout'] gepropt. Het laatst uitgevoerde commando is
	 * gestopt met een exitcode gelijk aan $A['exitcode']
	 *
	 * @param $login de systeemaccount-naam van een gebruiker
	 * @return een array met alle data of het gelukt is of niet.
	 */
	public function mensjeaddPassword($login)
	{
		$output = array();
		$ret = 0;

		$command = "ipa user-mod $login --random";
		exec('kinit website-ipa -kt ' . KEYTAB_FILE . '; ' . $command . ' 2>&1', $output, $ret);

		$wachtwoord = null;
		if ($ret == 0) {
			foreach ($output as $line) {
				$matches = array();
				if (preg_match('/^\s+Random password: ([^\n]+)$/', $line, $matches)) {
					$wachtwoord = $matches[1];
					break;
				}
			}
		}

		if (!is_null($wachtwoord)) {
			// Succesvol wachtwoord veranderd!
			return array(
				'status' => 'OK',
				'wachtwoord' => $wachtwoord,
				'output' => $output
			);
		} else {
			// Random password is niet gevonden (grep geeft dan 1 terug als exit
			// code), of er is iets anders fout gegaan.
			return array(
				'status' => 'FOUT',
				'fout' => $output,
				'exitcode' => $ret
			);
		}
	}

	public function getAllCies(Persoon $pers)
	{
		$uid = $this->getUid($pers);

		if(!$uid)
			return false;

		$filter = 'memberUid=' . $uid;
		$use_dn = 'cn=groups,cn=compat,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $filter, array('cn'));
		$result = ldap_get_entries($this->connection, $search);

		$gids = array();

		for($i = 0; $i < $result['count']; $i++)
		{
			$res = $result[$i];
			$gids[] = $res['cn'][0];
		}

		$gids_string = '(|(uid=' . implode(')(uid=', $gids) . '))';

		$use_dn = 'cn=users,cn=compat,' . $this->base_dn;
		$search = ldap_search($this->connection, $use_dn, $gids_string, array('uid'));
		$result = ldap_get_entries($this->connection, $search);

		$uids = array();

		for($i = 0; $i < $result['count']; $i++)
		{
			$res = $result[$i];
			$uids[] = $res['uid'][0];
		}

		return $uids;
	}
}
