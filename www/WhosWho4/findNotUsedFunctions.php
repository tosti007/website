#!/usr/bin/php
<?php
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}
exec("find . -name '*.php' ! -path '*Generated*' | xargs grep 'function' | grep static | awk '{first = $1; $1 = \"\"; print $0, first; }'", $res);

foreach($res as $r) {
	$split = explode('function', $r);
	$r1 = $split[1];
	$split2 = explode('(', $r1);
	$split3 = explode(')', $r1);
	$split4 = explode('/', end($split3));
	if(endsWith(end($split4), '.view.php:')) {
		$class = substr(end($split4), 0, -10) . 'View';
	} elseif(endsWith(end($split4), '.cls.php:')) {
		$class = substr(end($split4), 0, -9);
	} else {
		$class = null;
	}

	if(!is_null($class)) {
		$func = $class . "::" . trim(substr($split2[0], 1));
		exec("find ../../ -name '*.php' ! -path '*Generated*' | xargs grep '" . $func . "'", $res2);
		if(count($res2) == 0) {
			echo "$func\n";
		}
		$res2 = array();
	}
}
