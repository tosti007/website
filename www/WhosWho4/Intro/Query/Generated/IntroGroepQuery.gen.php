<?
abstract class IntroGroepQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de IntroGroepQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een IntroGroepQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw IntroGroepQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return IntroGroepQuery
	 * Het IntroGroepQuery-object waarvan meteen de db-table van het IntroGroep-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new IntroGroepQuery();

		$query->tables('IntroGroep');

		return $query;
	}

	/**
	 * @brief Maakt een IntroGroepVerzameling aan van de objecten die geselecteerd
	 * worden door de IntroGroepQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return IntroGroepVerzameling
	 * Een IntroGroepVerzameling verkregen door de query
	 */
	public function verzamel($object = 'IntroGroep', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een IntroGroep-iterator aan van de objecten die geselecteerd worden
	 * door de IntroGroepQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een IntroGroepVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'IntroGroep', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een IntroGroep die geslecteeerd wordt door de IntroGroepQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return IntroGroepVerzameling
	 * Een IntroGroepVerzameling verkregen door de query.
	 */
	public function geef($object = 'IntroGroep')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van IntroGroep.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'groepID',
			'cluster_clusterID',
			'naam',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'IntroGroep';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als IntroGroep een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan IntroGroep een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('IntroGroep'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'IntroCluster' => array(
				'cluster_clusterID' => 'clusterID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van IntroGroep.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'IntroGroep.groepID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'groepid':
			$type = 'int';
			$field = 'groepID';
			break;
		case 'cluster':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof IntroCluster; })))
					user_error('Alle waarden in de array moeten van type IntroCluster zijn', E_USER_ERROR);
			}
			else if(!($value instanceof IntroCluster) && !($value instanceof IntroClusterVerzameling) && !is_null($value))
				user_error('Value moet van type IntroCluster of NULL zijn', E_USER_ERROR);
			$field = 'cluster_clusterID';
			if(is_array($value) || $value instanceof IntroClusterVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getClusterID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getClusterID();
			$type = 'int';
			break;
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'IntroGroep.'.$v;
			}
		} else {
			$field = 'IntroGroep.'.$field;
		}

		return array($field, $value, $type);
	}

}
