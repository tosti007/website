<?
abstract class IntroDeelnemerQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de IntroDeelnemerQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een IntroDeelnemerQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw IntroDeelnemerQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return IntroDeelnemerQuery
	 * Het IntroDeelnemerQuery-object waarvan meteen de db-table van het
	 * IntroDeelnemer-object als table geset is.
	 */
	static public function table()
	{
		$query = new IntroDeelnemerQuery();

		$query->tables('IntroDeelnemer');

		return $query;
	}

	/**
	 * @brief Maakt een IntroDeelnemerVerzameling aan van de objecten die geselecteerd
	 * worden door de IntroDeelnemerQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return IntroDeelnemerVerzameling
	 * Een IntroDeelnemerVerzameling verkregen door de query
	 */
	public function verzamel($object = 'IntroDeelnemer', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een IntroDeelnemer-iterator aan van de objecten die geselecteerd
	 * worden door de IntroDeelnemerQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een IntroDeelnemerVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'IntroDeelnemer', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een IntroDeelnemer die geslecteeerd wordt door de
	 * IntroDeelnemerQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return IntroDeelnemerVerzameling
	 * Een IntroDeelnemerVerzameling verkregen door de query.
	 */
	public function geef($object = 'IntroDeelnemer')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van IntroDeelnemer.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'lid_contactID',
			'betaald',
			'kamp',
			'vega',
			'allergie',
			'aanwezig',
			'allergieen',
			'introOpmerkingen',
			'voedselBeperking',
			'magischeCode',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'IntroDeelnemer';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als IntroDeelnemer een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan IntroDeelnemer een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('IntroDeelnemer'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Lid' => array(
				'lid_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van IntroDeelnemer.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'IntroDeelnemer.lid_contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'lid':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling))
				user_error('Value moet van type Lid zijn', E_USER_ERROR);
			$field = 'lid_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'betaald':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, IntroDeelnemer::enumsBetaald()))
						user_error($value . ' is niet een geldige waarde voor betaald', E_USER_ERROR);
				}
			}
			else if(!in_array($value, IntroDeelnemer::enumsBetaald()))
				user_error($value . ' is niet een geldige waarde voor betaald', E_USER_ERROR);
			$type = 'string';
			$field = 'betaald';
			break;
		case 'kamp':
			$type = 'int';
			$field = 'kamp';
			break;
		case 'vega':
			$type = 'int';
			$field = 'vega';
			break;
		case 'allergie':
			$type = 'int';
			$field = 'allergie';
			break;
		case 'aanwezig':
			$type = 'int';
			$field = 'aanwezig';
			break;
		case 'allergieen':
			$type = 'string';
			$field = 'allergieen';
			break;
		case 'introopmerkingen':
			$type = 'string';
			$field = 'introOpmerkingen';
			break;
		case 'voedselbeperking':
			$type = 'string';
			$field = 'voedselBeperking';
			break;
		case 'magischecode':
			$type = 'string';
			$field = 'magischeCode';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'IntroDeelnemer.'.$v;
			}
		} else {
			$field = 'IntroDeelnemer.'.$field;
		}

		return array($field, $value, $type);
	}

}
