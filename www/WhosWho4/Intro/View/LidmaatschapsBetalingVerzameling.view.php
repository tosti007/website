<?
abstract class LidmaatschapsBetalingVerzamelingView
	extends LidmaatschapsBetalingVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in LidmaatschapsBetalingVerzamelingView.
	 *
	 * @param obj Het LidmaatschapsBetalingVerzameling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLidmaatschapsBetalingVerzameling(LidmaatschapsBetalingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Toon alle (gegeven) DBObjecten in een mooi tabelletje.
	 * @param verz Indien NULL, laad ze allemaal uit de databaas.
	 * @returns Een HtmlTable
	 */
	public static function tabel(LidmaatschapsBetalingVerzameling $verz = null) {
		if (is_null($verz)) {
			$verz = LidmaatschapsBetalingQuery::table()->verzamel();
		}
		$tabel = new HtmlTable(array(), "sortable");
		$tabel->add(new HtmlTableHead($row = new HtmlTableRow()));
		$row->add(new HtmlTableHeaderCell(_("Naam")));
		$row->add(new HtmlTableHeaderCell(_("Soort")));
		$row->add(new HtmlTableHeaderCell(_("Rekeningnummer")));
		$row->add(new HtmlTableHeaderCell(_("Rekeninghouder")));
		$row->add(new HtmlTableHeaderCell(_("Ons transactie-ID")));

		foreach ($verz as $obj) {
			$lid = $obj->getLid();

			$row = new HtmlTableRow();
			$row->add(new HtmlTableDataCell(IntroDeelnemerView::makeLink($lid)));
			$row->add(new HtmlTableDataCell(LidmaatschapsBetalingView::waardeSoort($obj)));
			$row->add(new HtmlTableDataCell(LidmaatschapsBetalingView::waardeRekeningnummer($obj)));
			$row->add(new HtmlTableDataCell(LidmaatschapsBetalingView::waardeRekeninghouder($obj)));
			$row->add(new HtmlTableDataCell(LidmaatschapsBetalingView::waardeIdeal($obj)));

			$tabel->addImmutable($row);
		}

		return $tabel;
	}

	/**
	 * Schrijf een CSV-weergave van deze verzameling.
	 * Deze weergave heeft de bedoeling om geladen te worden in Excel ofzo.
	 *
	 * @param file Een filehandle waar de csv in geschreven worden.
	 * @param verz Een verzameling van LidmaatschapsBetalingen. Indien NULL, alle betalingen.
	 * @return void
	 */
	static public function toCSV($file, LidmaatschapsBetalingVerzameling $verz = NULL) {
		if (is_null($verz)) {
			$verz = LidmaatschapsBetalingQuery::table()->verzamel();
		}
		$velden = array("Naam", "Soort", "Rekeningnummer", "Rekeninghouder", "iDEALtransactie");
		fputcsv($file, $velden);

		foreach ($verz as $obj) {
			$lid = $obj->getLid();

			static::schrijfCSVRegel(array(
				"Naam" => PersoonView::waardeNaam($lid),
				"Soort" => LidmaatschapsBetalingView::waardeSoort($obj),
				"Rekeningnummer" => LidmaatschapsBetalingView::waardeRekeningnummer($obj),
				"Rekeninghouder" => LidmaatschapsBetalingView::waardeRekeninghouder($obj),
				"iDEALtransactie" => LidmaatschapsBetalingView::waardeIdeal($obj),
			), $velden, $file);
		}
	}
}
