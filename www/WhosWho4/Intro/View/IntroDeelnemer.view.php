<?

/**
 * $Id$
 */
abstract class IntroDeelnemerView
	extends IntroDeelnemerView_Generated
{
	/** Objecten voor nieuwe deelnemer **/
	public static function labelKamp(IntroDeelnemer $obj)
	{
		return _('Mee op kamp');
	}
	public static function labelVega(IntroDeelnemer $obj)
	{
		return _('Vegetarisch');
	}
	public static function labelAllergie(IntroDeelnemer $obj)
	{
		return _('Allergieën');
	}
	public static function labelAllergieen(IntroDeelnemer $obj)
	{
		return _('Overige voedselbeperkingen');
	}
	public static function labelIntroOpmerkingen(IntroDeelnemer $obj)
	{
		return _('Overige Opmerkingen');
	}
	public static function labelBetaald(IntroDeelnemer $obj)
	{
		return _('Betaald');
	}

	public static function opmerkingAllergieen()
	{
		return _('Allergieën, intoleranties, etc; graag zo specifiek mogelijk.');
	}

	public static function waardeGroep(IntroDeelnemer $obj)
	{
		if($groep = $obj->getGroep()){
			if($groep->magWijzigen())
				return IntroGroepView::maakLink($groep);
			return IntroGroepView::waardeNaam($groep);

		}
		return null;
	}

	//Deze functie override NIETS, maar soms willen we toch deze info hebben dus volg lekker de standaard
	public static function waardeCluster(IntroDeelnemer $obj)
	{
		if($groep = $obj->getGroep()){
			if($cluster = $groep->getCluster()){
				if($cluster->magWijzigen())
					return IntroClusterView::maakLink($cluster);
				return IntroClusterView::waardeNaam($cluster);
			}
		}
		return null;
	}

	/**
	 * Geef een link naar de pagina van deze introdeelnemer
	 * @param obj Een Lid- of IntroDeelnemerobject waar we de link naar willen
	 * @param naam (optioneel) Een string die je kan tonen in plaats van de naam
	 */
	public static function makeLink($obj, $naam = null)
	{
		// zorg ervoor dat we het bij het juiste type hebben
		if ($obj instanceof IntroDeelnemer) {
			$introDeelnemer = $obj;
		}
		if ($obj instanceof Lid) {
			$lid = $obj;
		}

		// en dat we allebei de types beschikbaar hebben
		if(!@$lid) {
			$lid = Lid::geef($obj->geefID());
		}
		if (!@$introDeelnemer) {
			$introDeelnemer = IntroDeelnemer::geef($obj->geefID());
		}

		if ($naam == null) {
			$naam = PersoonView::naam($lid);
		}

		// rechtenchecks
		if($lid instanceof Lid && $lid->getLidToestand() !== 'BIJNALID') {
			return PersoonView::makeLink($lid);
		} elseif(hasAuth('intro')) {
			return new HtmlAnchor('/Leden/Intro/Deelnemer/' . $lid->getContactID(), $naam);
		} else {
			// voor het geval dat iemand in een mentorgroepje wordt gestopt
			// maar geen deelnemer is
			if (!$introDeelnemer) {
				return;
			}

			// mentoren mogen de namen zien van hun kindjes die bijna-leden zijn
			// maar hun pagina niet bekijken
			$groep = $introDeelnemer->getGroep();
			if (!$groep) return; // niet iedereen heeft een groepje
			$mentoren = $groep->mentoren();

			if ($mentoren && $mentoren->bevat(Persoon::getIngelogd())) {
				return $naam;
			}
		}
	}

	/**
	 * Override voor View::processForm.
	 *
	 * Zorgt er ook voor dat IntroDeelnemer-objecten zonder magische code er eentje krijgen.
	 *
	 * @return bool
	 * Of de IntroDeelnemer geldig is.
	 */
	static function processForm($obj, $velden='viewWijzig', $data=null, $include_id=false)
	{
		parent::processForm($obj, $velden, $data, $include_id);
		// Genereer alleen als die er nog niet is, want de introsite gebruikt dit als unieke identifier.
		if (!$obj->getMagischeCode())
		{
			$obj->genereerMagischeCode();
		}
		return $obj->valid();
	}

	static public function processNieuwForm(IntroDeelnemer $deel
		,ContactAdres $adres, ContactTelnr $tel1, ContactTelnr $tel2
		, LidStudie $stu1, LidStudie $stu2, LidStudie $stu3)
	{
		$bachelorStudies = StudieVerzameling::getBachelorStudies();
		if (tryPar('IntroDeelnemer[submit]',false))
		{
			$lid = $deel->getLid();
			$lid->setLidVan(new DateTimeLocale(EJBV));
			LidView::processForm($lid);
			IntroDeelnemerView::processForm($deel);
			ContactAdresView::processForm($adres);

			$stu = tryPar('studie');
			$studies = array("IK","IC","NA","WI");
			foreach($studies as $studie)
			{
				if($studie == $stu)
				{
					$stu1->setStudie($bachelorStudies[$stu]);
				}
			}
			switch($stu) {
			case "IC IK":
				$stu1->setStudie($bachelorStudies["IK"]);
				$stu2->setStudie($bachelorStudies["IC"]);
				break;
			case "IC WI":
				$stu1->setStudie($bachelorStudies["IC"]);
				$stu2->setStudie($bachelorStudies["WI"]);
				break;
			case "NA WI":
				$stu1->setStudie($bachelorStudies["NA"]);
				$stu2->setStudie($bachelorStudies["WI"]);
				break;
			case "IC WI NA":
				$stu1->setStudie($bachelorStudies["IC"]);
				$stu2->setStudie($bachelorStudies["WI"]);
				$stu3->setStudie($bachelorStudies["NA"]);
			}

			$telverz = tryPar("ContactTelnr");
			$telnrObjecten = [
				1 => $tel1,
				2 => $tel2,
			];
			foreach ($telnrObjecten as $id => $telnrObject)
			{
				$waarde = $telverz[$id]["Telefoonnummer"];
				if ($waarde != '')
				{
					$telnrObject->setTelefoonnummer($waarde);
				}
			}
		}
	}

	static public function toevoegen($deel,$adres,$tel1,$tel2,$stu1,$stu2,$stu3,$show_error)
	{
		$page = new HTMLPage();
		$page->setBrand('intro');
		$page->start(_("Introsmurf"), 'intro');
		$page->add(self::nieuweDeelnemer($deel,$adres,$tel1,$tel2,$stu1,$stu2,$stu3,$show_error));
		return $page;
	}

	static public function nieuweDeelnemer ($deel = null, $adres =null , $tel1 = null, $tel2 = null, $stu1 = null, $stu2 =null, $stu3 =null, $show_error = true)
	{

		$form = HtmlForm::named('nieuweDeelnemer');
		$form ->add($div = new HtmlDiv());
		$div->add(new HtmlSpan(_("Ben je komend jaar eerstejaars student bij één van onze studies?
									Met dit formulier kun je je inschrijven voor de komende introductie.
									Meer informatie vind je op")))
			->add(new HtmlAnchor(Commissie::geef(INTROCIE)->url()
												,"de website van de introductiecomissie."))
		    ->add(new HtmlBreak());

		$form->add($div = new HtmlDiv());

		$lid = $deel->getLid();

		$div->add(LidView::wijzigTR($lid, 'Voornaam', $show_error))
			->add(LidView::wijzigTR($lid, 'Voorletters', $show_error))
			->add(LidView::wijzigTR($lid, 'Tussenvoegsels', $show_error))
			->add(LidView::wijzigTR($lid, 'Achternaam', $show_error))
			->add(LidView::wijzigTR($lid, 'Geslacht', $show_error))
			->add(LidView::wijzigTR($lid, 'Voornaamwoord', $show_error))
			->add(LidView::wijzigTR($lid, 'DatumGeboorte', $show_error))
			->add(ContactAdresView::wijzigTR($adres, 'Straat1', $show_error))
			->add(ContactAdresView::wijzigTR($adres ,'Huisnummer',$show_error))
			->add(ContactAdresView::wijzigTR($adres ,'Postcode',$show_error))
			->add(ContactAdresView::wijzigTR($adres ,'Woonplaats',$show_error))
			->add(ContactAdresView::wijzigTR($adres ,'Land',$show_error))
			->add(ContactView::wijzigTR($lid, 'Email',$show_error)) // TODO: moet verplicht gemaakt worden!
			->add(LidView::wijzigTR($lid, 'Studentnr',$show_error));

		$form->add($div = new HtmlDiv());
		/** Pas ook telefoonnummers aan **/
		$telVerz = new ContactTelnrVerzameling();
		$telVerz->voegtoe($tel1);
		$telVerz->voegtoe($tel2);
		$obj = $telVerz;
		$i=1;

		foreach ($telVerz as $nummer)
		{
			$nummer->setFakeId($i++);
			$div->add(ContactTelnrView::wijzigTR($nummer, 'Telefoonnummer', $show_error, true));
		}

		$div->add(self::wijzigTR($deel, 'Studie', $show_error))
			->add(self::wijzigTR($deel, 'Kamp', $show_error))
			->add(self::wijzigTR($deel, 'Vega', $show_error))
			->add(self::wijzigTR($deel, 'Allergie', $show_error))
			->add(self::wijzigTR($deel, 'Allergieen', $show_error))
			->add(self::wijzigTR($deel, 'IntroOpmerkingen', $show_error));

		$div->add(HtmlInput::makeFormSubmitButton('Voer in','IntroDeelnemer[submit]'));
	    return new HtmlDiv($form, null, 'nieuweDeelnemerDiv');
	}

	public static function maandagForm($lid, $deelnr, $tel1, $tel2, $show_error = true)
	{
		$form = HtmlForm::named('nieuweDeelnemerMaandag');

		$form->add($div = new HtmlDiv());

		$div->add(LidView::wijzigTR($lid, 'Voornaam', $show_error))
			->add(LidView::wijzigTR($lid, 'Voorletters', $show_error))
			->add(LidView::wijzigTR($lid, 'Tussenvoegsels', $show_error))
			->add(LidView::wijzigTR($lid, 'Achternaam', $show_error))
			->add(ContactView::wijzigTR($lid, 'Email',$show_error));

		/** Pas ook telefoonnummers aan **/
		$telVerz = new ContactTelnrVerzameling();
		$telVerz->voegtoe($tel1);
		$telVerz->voegtoe($tel2);
		$obj = $telVerz;
		$i=1;
		foreach ($telVerz as $nummer)
		{
			$nummer->setFakeId($i++);
			$div->add(ContactTelnrView::wijzigTR($nummer, 'Telefoonnummer', $show_error, true));
		}

		$div->add(LidView::wijzigTR($lid, 'Studie', $show_error));

		$div->add(new HtmlHR())
			->add(new HtmlHeader(4, _("In te vullen door de introductiecommissie")));

		$selects = array(null => null);
		foreach(IntroGroepVerzameling::groepenVanJaar(colJaar()) as $id => $groep)
			$selects[$id] = IntroGroepView::waardeNaam($groep);

		$div->add(self::makeFormEnumRow('introgroepen', $selects
			, tryPar('introgroepen'), _('Mentorgroep')));

		$div->add(HtmlInput::makeFormSubmitButton('Voer in'));

		return $form;
	}

	public static function processMaandagNieuwForm($lid, $deelnr, $stu1, $stu2, $stu3, $tel1, $tel2)
	{
		$bachelorStudies = StudieVerzameling::getBachelorStudies();

		LidView::processForm($lid);
		IntroDeelnemerView::processForm($deelnr);

		$mentorgroepID = trypar('introgroepen');
		$stu1->setGroep($mentorgroepID);
		$stu2->setGroep($mentorgroepID);
		$stu3->setGroep($mentorgroepID);

		$stu = tryPar('studie');
		$studies = array("IK","IC","NA","WI");
		foreach($studies as $studie)
		{
			if($studie == $stu)
			{
				$stu1->setStudie($bachelorStudies[$stu]);
			}
		}
		switch($stu) {
		case "IC IK":
			$stu1->setStudie($bachelorStudies["IK"]);
			$stu2->setStudie($bachelorStudies["IC"]);
			break;
		case "IC WI":
			$stu1->setStudie($bachelorStudies["IC"]);
			$stu2->setStudie($bachelorStudies["WI"]);
			break;
		case "NA WI":
			$stu1->setStudie($bachelorStudies["NA"]);
			$stu2->setStudie($bachelorStudies["WI"]);
			break;
		case "IC WI NA":
			$stu1->setStudie($bachelorStudies["IC"]);
			$stu2->setStudie($bachelorStudies["WI"]);
			$stu3->setStudie($bachelorStudies["NA"]);
		}

		$telverz = tryPar("ContactTelnr");
		$set = array(1,2);
		foreach($set as $id)
		{ 
			if($id ==1 && !$telverz[1]["Telefoonnummer"] == "")
			{
				$tel1->setTelefoonnummer($telverz[1]["Telefoonnummer"]);
			}
			if($id == 2 && !$telverz[2]["Telefoonnummer"] == "")
			{
				$tel2->setTelefoonnummer($telverz[2]["Telefoonnummer"]);
			}
		}
	}

	public static function bekijkDeel(IntroDeelnemer $deel, $uitgebreid = true)
	{
		$vega = self::waardeVega($deel);
		$betaald = self::waardeBetaald($deel);
		$kamp = self::waardeKamp($deel);
		$all = self::waardeAllergie($deel);
		$aan = self::waardeAanwezig($deel);
		$allOpm = self::waardeAllergieen($deel);
		$opmr = self::waardeIntroOpmerkingen($deel);
		$voedsel = self::waardeVoedselBeperking($deel);

		$grp = $deel->getGroep();
		$persoon = Persoon::geef($deel->geefID());

		$ConAdrDiv = null;
		if(@ContactAdres::eersteBijContact($persoon))
			$ConAdrDiv = ContactAdresView::detailsDiv(ContactAdres::eersteBijContact($persoon));

		$telnrs = ContactTelnrVerzamelingView::lijst(ContactTelnrVerzameling::vanContact($persoon));
		$email = PersoonView::makeEmailLink($persoon);
		$gebDat = $persoon->getDatumGeboorte()->format('d-m-Y');

		$lid = $deel->getLid();
		if(! $lid)
		{
			$studies =  "Deze persoon is geen lid, dus daar mogen we niks van weten";
		}
		else
		{
			$studies = StudieVerzamelingView::toString($lid->getStudies()->toStudieVerzameling());
			$studnr = LidView::waardeStudentnr($lid);
			$lidnr = $lid->getContactID(); //Handig in geval van dubbele inschrijvingen daar contactID uniek is
			$status = $lid->getLidToestand();
		}


		$div = new HtmlDiv();

		$div->add(new HtmlDiv(new HtmlDiv($tbl = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));
		$tbl->add(self::infoTRData(_('Naam'), PersoonView::makeLink($persoon)))
			->add(self::infoTRData(_('Geboortedatum'), $gebDat))
			->add(self::infoTRData(_('Mentorgroep'), ($grp ? IntroGroepView::maakLink($grp) : '')))
			->add(self::infoTRData(_('Studies'), $studies))
			->add(self::infoTRData(_('Email'), $email));

		if($uitgebreid)
		{
			$tbl->add(self::infoTRData(_('Adres'), $ConAdrDiv))
				->add(self::infoTRData(_('Telefoon'), $telnrs));
		}

		$tbl->add(self::infoTRData(_('Betaald'), $betaald))
			->add(self::infoTRData(_('Kamp'), $kamp))
			->add(self::infoTRData(_('Vega'), $vega))
			->add(self::infoTRData(_('Allergie'), $all))
			->add(self::infoTRData(_('Allergie-opmerking'), $allOpm))
			->add(self::infoTRData(_('Voedselbeperkingen'), $voedsel))
			->add(self::infoTRData(_('Intro-opmerkingen'), $opmr));

		if($lid){
			$tbl->add(self::infoTRData(_('Lidstatus'), $status))
				->add(self::infoTRData(_('Lidnummer'), $lidnr))
				->add(self::infoTRData(_('Studentnummer'), $studnr));
		} else {
			$tbl->add(self::infoTRData(_('Lidstatus'), _('Niet lid of boekenkoper')));
		}

		$div->add(HtmlAnchor::button("../../ToonIntrodeelnemers","Terug naar overzicht"));

	    return $div;
	}

	/**
	 * Geef een wijzigpagina voor de deelnemer.
	 *
	 * @return HTMLPage
	 */
	static public function wijzig ($lid,$deel, $show_error = true)
	{
		$page = static::pageLinks($deel);
		$page
			 ->setBrand('intro')
			 ->addFooterJS(Page::minifiedFile('knockout.js', 'js'))
			 ->start(IntroDeelnemerView::makeLink($lid));
		$page->add(self::wijzigForm('DeelWijzig', $lid,$deel, $show_error));
		return $page;
	}

	static public function processWijzigForm ($obj, IntroDeelnemer $deel)
	{
		/** Kloon het oude lid, om wijzigingen te bekijken **/
		$lidOud = clone $obj;
		$view = $obj->getView();

		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($obj), null))
			return false;

		/** Vul alle statische informatie in **/
		$view::processForm($obj);

		$obj->wijzigingen = $lidOud->difference($obj);

		/** Verwerk telefoonnummer(s) **/
		$nrs = $obj->getTelefoonNummers();
		$datatel = $data['Telefoonnr'];
		if (!isset($datatel['pref']))
			$datatel['pref'] = 0;

		// De bestaande nummers veranderen
		foreach ($nrs as $nummer)
		{
			if ($datatel[$nummer->geefID()]['del'])
			{
				$nrs->verwijder($nummer);
				$obj->wijzigingen['Telnr' . uniqid()] = array($nummer->getTelefoonnummer(), '[Verwijderd]');
			}
			else
			{
				if($nummer != $datatel[$nummer->geefID()]['Telefoonnummer'])
					$obj->wijzigingen['Telnr' . uniqid()] = array($nummer->getTelefoonnummer(), $datatel[$nummer->geefID()]['Telefoonnummer']);
				$nummer->setTelefoonnummer($datatel[$nummer->geefID()]['Telefoonnummer'])
					   ->setSoort($datatel[$nummer->geefID()]['Soort'])
					   ->setVoorkeur(($datatel['pref'] == $nummer->geefID()) ? true : false);
			}
		}
		/** Verwerk introdeelnemergegevens **/
		IntroDeelnemerView::processForm($deel);
		/** Verwerk adressen **/
		$adressen = $obj->getAdressen();
		$dataadr = tryPar('ContactAdres');

		// de bestaande addressen
		foreach ($adressen as $adres)
		{

				$oudAdresString = ContactAdresView::toString($adres);
				ContactAdresView::processForm($adres, 'viewWijzig', $dataadr[$adres->geefID()]);
				if($oudAdresString != ContactAdresView::toString($adres))
					$obj->wijzigingen['Adres' . uniqid()] = array($oudAdresString, ContactAdresView::toString($adres));
		}

		//Als het ondertussen een boekenkoper is geworden hoeven we hier niets meer mee te doen.
		if($obj instanceof Lid) {
			/** Verwerk studies **/
			$datastu = tryPar('LidStudie', null);
			$studies = $obj->getStudies();

			// de bestaande studie
			foreach ($studies as $studie)
			{
				if ((bool) $datastu[$studie->geefID()]['verwijder'])
				{
					$studie->verwijderen();
				}
				else
				{
					LidStudieView::processForm($studie, 'viewWijzig', $datastu[$studie->geefID()]);
					$studie->setStudie($datastu[$studie->geefID()]['Studie']);
					$studie->opslaan();
				}

				// Eventueel een nieuwe studie toevoegen.
				$studie = new LidStudie($obj);
				LidStudieView::processForm($studie, 'viewWijzig', $datastu['0']);
				$studie->setStudie($datastu['0']['Studie']);
				if ($studie->valid())
				{
					$studie->opslaan();
				}
			}

			if(tryPar('IntroDeelnemerGroep')) {
				$groep = IntroGroep::geef(tryPar('IntroDeelnemerGroep'));

				if($groep) {
					foreach ($studies as $studie) {
						$studie->setGroep($groep);
						if ($studie->valid())
						{
							$studie->opslaan();
						}
					}
				}
			}
		}
	}

	public static function wijzigForm($name = 'DeelWijzig', $obj = null,$deel = null, $show_error = true)
	{
		/** vul de leegte **/
		if (is_null($obj))
		{
			$obj = new Lid();
		}
		if (is_null($deel))
		{
			$deel = new IntroDeelnemer();
		}


		//Nodig voor dynamische classaanroepen.
		$view = $obj->getView();
		$typeObj = get_class($obj);

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		/** Div voor Introdeelnemergegevens **/
		$form->add($div = new HtmlDiv());
		$div->add(IntroDeelnemerView::wijzigTR($deel,'Aanwezig',$show_error))
			->add(IntroDeelnemerView::wijzigTR($deel,'Kamp',$show_error))
			->add(IntroDeelnemerView::wijzigTR($deel,'Betaald',$show_error))
			->add(IntroDeelnemerView::wijzigTR($deel,'Vega',$show_error))
			->add(IntroDeelnemerView::wijzigTR($deel,'Allergie',$show_error))
			->add(IntroDeelnemerView::wijzigTR($deel,'Allergieen',$show_error))
			->add(IntroDeelnemerView::wijzigTR($deel,'IntroOpmerkingen',$show_error));

		/** Div voor persoonsinformatie **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Persoonsinformatie")));
		$div->add($view::wijzigTR($obj, 'Voornaam', $show_error))
			->add($view::wijzigTR($obj, 'Voorletters', $show_error))
			->add($view::wijzigTR($obj, 'Geboortenamen', $show_error))
			->add($view::wijzigTR($obj, 'Tussenvoegsels', $show_error))
			->add($view::wijzigTR($obj, 'Achternaam', $show_error))
			->add($view::wijzigTR($obj, 'Geslacht', $show_error))
			->add($view::wijzigTR($obj, 'Voornaamwoord', $show_error))
			->add($view::wijzigTR($obj, 'DatumGeboorte', $show_error))
			->add($view::wijzigTR($obj, 'Studentnr', $show_error));

		/** Het Adres **/
		$div->add($idi = new HtmlDiv());
		$idi->add(new HtmlHeader(4, _("Adressen")));
		if ($show_error && !$obj->getAdressen()->allValid())
			$idi->add(new HtmlParagraph(_("Er waren fouten in de ingevoerde adressen."), 'text-danger strongtext'));

	    $adres= $obj->getAdressen()->first();
		if(@!$adres)
			$adres = new ContactAdres($obj);
		$idi->add($iidiv = new HtmlDiv());
		$iidiv->add(new HtmlHeader(5, ContactAdresView::waardeSoort($adres)));
		$iidiv->add(ContactAdresView::wijzigTR($adres, 'Soort', $show_error, true))
			  ->add(ContactAdresView::wijzigTR($adres, 'Straat1', $show_error, true))
			  ->add(ContactAdresView::wijzigTR($adres, 'Huisnummer', $show_error, true))
			  ->add(ContactAdresView::wijzigTR($adres, 'Postcode', $show_error, true))
			  ->add(ContactAdresView::wijzigTR($adres, 'Woonplaats', $show_error, true))
			  ->add(ContactAdresView::wijzigTR($adres, 'Land', $show_error, true));

		/** De div voor de contactsinformatie **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Contact-informatie")));
		$div->add($view::wijzigTR($obj, 'Email', $show_error));

		/** Pas ook telefoonnummers aan **/
		$div->add($idi = new HtmlDiv());
		$idi->add(new HtmlHeader(4, _("Telefoonnummers")));
		if ($show_error && !$obj->getTelefoonNummers()->allValid())
			$idi->add(new HtmlParagraph(_("Er waren fouten in de ingevoerde telefoonnummers."), 'text-danger strongtext'));

		foreach ($obj->getTelefoonNummers() as $nummer)
		{
			$idi->add(self::makeFormRadiosRow($typeObj."[Telefoonnr][pref]"
				, array($nummer->geefID() => _('Voorkeur')), $nummer->getVoorkeur()))
				->add(self::makeFormEnumRow($typeObj."[Telefoonnr][".$nummer->geefID()."][Soort]"
				, ContactTelnrView::labelenumSoortArray(), $nummer->getSoort()
				, _('Categorie')))
				->add(self::makeFormStringRow($typeObj."[Telefoonnr][".$nummer->geefID()."][Telefoonnummer]"
				, $nummer->getTelefoonnummer(), _('Nummer')))
				->add(self::makeFormBoolRow($typeObj."[Telefoonnr][".$nummer->geefID()."][del]"
				, false, _('Verwijderen')));
		}

		//Als het ondertussen een boekenkoper is geworden hoeven we hier niets meer mee te doen.
		if($obj instanceof Lid) {
			/** De div voor de studies **/
			$form->add($div = new HtmlDiv());
			$div->add(new HtmlHeader(3, _("Studies")));
			$studies = $obj->getStudies();

			foreach ($studies as $studie)
			{
				$div->add($idiv = new HtmlDiv());
				$idiv->add(new htmlHeader(4, LidStudieView::waardeStudie($studie)));
				$idiv->add(LidStudieView::wijzigStudieTR($studie, $show_error))
					  ->add(LidStudieView::wijzigTR($studie, 'Status', $show_error, true));
				$idiv->add(self::makeFormBoolRow('LidStudie['.$studie->geefID().'][verwijder]'
					, False, _('Verwijder deze studie')));
			}
			$studie = new LidStudie($obj);
			$div->add($idiv = new HtmlDiv());
			$idiv->add(new htmlHeader(4, _("Nieuwe studie")));
			$idiv->add(LidStudieView::wijzigStudieTR($studie, $show_error))
				  ->add(LidStudieView::wijzigTR($studie, 'Status', $show_error, true));
		}

		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _('Mentorgroep')));

		$form->add(self::wijzigTR($deel, 'Mentorgroep', $show_error));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));

		return $form;
	}

	static public function labelStudie()
	{
		return _('Studie');
	}

	static public function opmerkingStudie()
	{
		return '';
	}

	static public function formStudie(IntroDeelnemer $obj, $include_id = false)
	{
		return HtmlSelectbox::fromArray('studie',
			   LidStudie::alleStudiesArray(),
					(string)tryPar('studie'), 1, false);
	}

	static public function labelMentorgroep()
	{
		return _('Mentorgroep');
	}

	static public function opmerkingMentorgroep()
	{
		return '';
	}

	static public function formMentorgroep(IntroDeelnemer $obj, $include_id = false)
	{
		$div = new HtmlDiv();

		$clusters = IntroClusterVerzameling::alleClusters();
		$clusterJS = null;

		$clusterArr = [];

		$grp = $obj->getGroep();

		foreach($clusters as $cluster) {
			$clusterArr[$cluster->geefID()] = IntroClusterView::waardeJaar($cluster) . ' ' . IntroClusterView::waardeNaam($cluster);

			$div->add($d = new HtmlDiv());
			foreach($cluster->groepjes() as $groep) {
				$d->add(HtmlInput::makeRadio('IntroDeelnemerGroep', array($groep->geefID(), IntroGroepView::waardeNaam($groep)), $groep == $grp));
			}

			$d->setAttribute('data-bind', 'visible: cluster() == ' . $cluster->geefID());
		}

		$div->add($sel = HtmlSelectbox::fromArray('', $clusterArr));
		$sel->setAttribute('data-bind', 'value: cluster');

		if($grp) {
			$clusterJS = $grp->getCluster()->geefID();
		}

		// We maken een knockout-model met de jaren zodat als het jaar gewijzigd
		// wordt, de zichtbare keuze-opties gewijzigd worden
		$div->add(HtmlScript::makeJavascript('$(function() {
	var ViewModel = function() {
		self.cluster = ko.observable(' .  $clusterJS . ');
	}

	ko.applyBindings(new ViewModel());
});'));

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
