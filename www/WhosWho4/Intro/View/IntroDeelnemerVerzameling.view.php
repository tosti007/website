<?

/**
 * $Id$
 */
abstract class IntroDeelnemerVerzamelingView
	extends IntroDeelnemerVerzamelingView_Generated
{
	/** METHODEN **/
	static private function totalenstats()
	{
		global $WSW4DB;
		return $WSW4DB->q('TUPLE SELECT count(*) as "totaal"'
				.	', sum(kamp=1) as "kamp"'
				.	', sum(kamp=1 AND vega=1) as "vega"'
				.	', sum(kamp=1 AND aanwezig=1) as "aanwezig"'
				.	', sum(kamp=1 AND betaald="NEE") as "nietbetaald"'
				.	', sum(kamp=1 AND allergie=1) as "allergisch"'
				.	', sum(introOpmerkingen != "") as "introOpmerking"'
				.   ', sum(TIMESTAMPDIFF(year,`Persoon`.`datumGeboorte`,NOW()) < 18) as "onderleef"'
				.   ', sum(`Persoon`.`datumGeboorte` IS NULL) as "onzijdig"'
				.   ', sum(kamp=1 AND `Persoon`.`datumGeboorte` IS NULL) as "onzijdigkamp"'
				.   ', sum(kamp=1 AND TIMESTAMPDIFF(year,`Persoon`.`datumGeboorte`,NOW()) < 18) as "onderkamp"' 
				.   ' FROM IntroDeelnemer LEFT JOIN Persoon ON IntroDeelnemer.lid_contactID = Persoon.contactID');
	}

	/** VIEWS **/
	static public function introstats()
	{
		$array = self::totalenstats();

		$div = new HtmlDiv();
		$div->add($table = new HtmlTable());		
		$table->add($tbody = new HtmlTableBody());
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Totaal aantal inschrijvingen:")));
		$row->addData($array['totaal']);
				
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp:")));
		$row->addData($array['kamp']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp en vega:")));
		$row->addData($array['vega']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp en aanwezig:")));
		$row->addData($array['aanwezig']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp en niet betaald:")));
		$row->addData($array['nietbetaald']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp en allergie:")));
		$row->addData($array['allergisch']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp en onder 18:")));
		$row->addData($array['onderkamp']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Mee op kamp en zonder bekende leeftijd:")));
		$row->addData($array['onzijdigkamp']);

		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Kindjes komen op kampreservelijst vanaf:")));
		$row->addData(MAXAANTALKAMP);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Kindjes met opmerkingen:")));
		$row->addData($array['introOpmerking']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Kindjes onder 18:")));
		$row->addData($array['onderleef']);
		
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan(_("Kindjes zonder bekende leeftijd:")));
		$row->addData($array['onzijdig']);
		return $div;
	}

	static public function shortForm($defKamp,$defVega,$defAanwezig,$defAllergie,$defBetaald,$defOpmvar,$defOpmerk)
	{
		$formTable = new HtmlTable();

		$row = $formTable->addRow();
		$row ->addHeader(_("Kamp"));
		$row ->addHeader(_("Vega"));
		$row ->addHeader(_("Aanwezig"));

		$row = $formTable->addRow();
	    $row->addData(HtmlInput::makeRadioFromArray('kamp', array(
			    'void'=>_("Kamp maakt niet uit"),
				'true' => _("Wel Kamp"),
				'false' => _("Niet Kamp")
				), (string) $defKamp, true));

		$row->addData(HtmlInput::makeRadioFromArray('vega', array(
				'void' =>_("Vega maakt niet uit"),
				'true' => _("Wel Vega"),
				'false' => _("Niet Vega")
				), (string) $defVega, true));

		$row->addData(HtmlInput::makeRadioFromArray('aanwezig', array(
				'void' =>_("Aanwezigheid maakt niet uit"),
				'true' => _("Wel Aanwezig"),
				'false' => _("Niet Aanwezig")
				), (string) $defAanwezig, true));

		$row = $formTable->addRow();
		$row ->addHeader(_("Allergie"));
	    $row ->addHeader(_("Betaald"));
	    $row ->addHeader(_("Opmerkingen"));
	    $row = $formTable->addRow();

		$row->addData(HtmlInput::makeRadioFromArray('allergie', array(
				'void' =>_("Allergie maakt niet uit"),
				'true' => _("Wel Allergie"),
				'false' => _("Niet Allergie")
				), (string) $defAllergie, true));

		$row->addData(HtmlInput::makeRadioFromArray('betaald', array(
			'ALLES' => 'Toon allen',
			'KAS'	=> 'Contant',
			'IDEAL'  => 'Ideal',
			'PIN'   => 'Gepind',
			'NEE'   => 'Niet betaald'
			), (string) $defBetaald, true));

		$row->addData(HtmlInput::makeRadioFromArray('opmerk', array(
			'void' => 'Toon allen',
			'true'	=> 'Alleen mensen met intro-opmerkingen tonen',
			'false'  => 'Alleen mensen zonder intro-opmerkingen tonen'
			), (string) $defOpmerk, true));

	    $row = $formTable->addRow();
		$row->addData(HtmlInput::makeSubmitButton('Toon Deelnemers', ''));

		return new HtmlDiv($formTable, null, 'shortFormAgenda');
	}

	static public function toonDeelnemers (IntroDeelnemerVerzameling $deelnemers,$defOpmvar, $inschrijfmodus = false)
	{
		$div = new HtmlDiv('', null, 'toonDeelnemers');

		$table = new HtmlTable(null, 'lijst sortable');

		$table->add($thead = new HtmlTableHead())
			  ->add($tbody = new HtmlTableBody());

		$thead->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell("Naam"))
			->add(($inschrijfmodus?new HtmlTableHeaderCell("Student #"):null))
			->add(new HtmlTableHeaderCell("Kamp"))
		    ->add(new HtmlTableHeaderCell("Vega"))
		    ->add(new HtmlTableHeaderCell("Betaald"))
		    ->add(new HtmlTableHeaderCell("Aanwezig"))
		    ->add(new HtmlTableHeaderCell("Allergie"))
			->add(($inschrijfmodus?new HtmlTableHeaderCell("Cluster en groep"):null));

		$form = new HtmlForm('get');
		$form->addClass('form-inline');
		$form->add(HtmlSelectbox::fromArray('opmvar', array(
                '1' => _("Intro Opmerkingen"),
				'2' => _("Omschrijving Allergie"),
				'3' => _("Voedsel Beperkingen")
				), (int) $defOpmvar, 1, true));
		$shortformdata = array('kamp', 'vega', 'aanwezig', 'allergie', 'betaald', 'opmerk');
		foreach($shortformdata as $data) {
			if(!is_null($d = tryPar($data, null))) {
				$form->add(HtmlInput::makeHidden($data, $d));
			}
		}

		$row->addData($form);

		$row->add(new HtmlTableHeaderCell(("Lidstatus")));

		foreach ($deelnemers as $deelnemer)
		{
			$row = new HtmlTableRow();
			$row->setId($deelnemer->geefID());

			$lid = $deelnemer->getLid();
			$lidid = $deelnemer->getLidContactID();

			if(!$lid)
			{
				$status = 'PERSOON';
				$lid = Persoon::geef($lidid); // Lid is dus geen lid, maar alleen persoon.

				// In het geval dat dit gebeurt is er iets goed mis,
				// maar doe ons best om de pagina wel te redden.
				// (Kan de oorzaak nu ook niet reproduceren, dus vandaar eerst een warning)
				if (!$lid) {
					user_error("Introdeelnemer zonder Persoonobject: +" . $lidid,
						E_USER_WARNING);
				}
			}
			else
			{
				$status = $lid->getLidToestand();
			}
			if(! $status == 'BIJNALID') // Niet bijna-leden worden naar intro/deelnemer/* gestuurd, ook personen want het gaat om oud bijna-leden waarvan de intro misschien nog iets op wil kunnen zoeken zoals introOpmerkingen.
			{
				$row->addData(PersoonView::makeLink($lid));
			}
			else
			{
				if($inschrijfmodus)
				{
					$row->addData($link = new HtmlAnchor("../intro/Deelnemer/".$lidid."/Wijzig",PersoonView::naam($lid)));
					//Op verzoek van de intro, ze willen graag dat er een tab bij de inschrijfmodus pagina blijft tijdens het doorvoeren van een wijziging.
					$link->setAttribute('target','_blank');
				}
				else
				{
					$row->addData(new HtmlAnchor("../intro/Deelnemer/".$lid->getContactID(),PersoonView::naam($lid)));
				}
			}

			if($inschrijfmodus) {
				if($deelnemer->getLid()){
					if($deelnemer->getLid()->getStudentnr()) {
						$row->addData(substr(LidView::waardeStudentnr($deelnemer->getLid()), 0, 20));
					} else {
						$row->addData("N.n.b.");
					}	
				}
			}
				

			$row->addImmutable(HtmlTableDataCell::maakBoolPicto($deelnemer->getKamp()));

			$row->addImmutable(HtmlTableDataCell::maakBoolPicto($deelnemer->getVega()));

			if($deelnemer->getBetaald() == "NEE")
				$row->addData(HtmlSpan::fa('times', _('Fout'), 'text-danger'));
			else
				$row->addData($deelnemer->getBetaald());

			if ($inschrijfmodus && !$deelnemer->getAanwezig())
			{
				$inschrijfform = HtmlForm::named("aanwezig")
					->add(HtmlInput::makeHidden('lidnr', $deelnemer->geefID()))
					->add(HtmlInput::makeSubmitButton('Aanwezig'));
				if ($deelnemer->getBetaald() == "NEE")
					$inschrijfform->add(HtmlInput::makeSubmitButton('Aanwezig + Gepind'));
				
				$row->addData($inschrijfform);
			}
			else
				$row->add(HtmlTableDataCell::maakBoolPicto($deelnemer->getAanwezig()));

			$row->add(HtmlTableDataCell::maakBoolPicto($deelnemer->getAllergie()));

			if($inschrijfmodus)
			{
				if(IntroDeelnemerView::waardeCluster($deelnemer) || IntroDeelnemerView::waardeGroep($deelnemer)) {
					$row->addData(IntroDeelnemerView::waardeCluster($deelnemer)
						 . ": " . IntroDeelnemerView::waardeGroep($deelnemer));
				} else {
					$row->addData(_("Geen!"));
				}
			}

			switch($defOpmvar)
			{
				case '1':
						if ($inschrijfmodus)
							$row->addData(substr(IntroDeelnemerView::waardeIntroOpmerkingen($deelnemer), 0, 20)
								. ((strlen(IntroDeelnemerView::waardeIntroOpmerkingen($deelnemer)) > 20) ? '...' : ''));
						else
							$row->addData(IntroDeelnemerView::waardeIntroOpmerkingen($deelnemer));
						break;
				case '2':
						$row->addData(IntroDeelnemerView::waardeAllergieen($deelnemer));
						break;
				case '3':
						$row->addData(IntroDeelnemerView::waardeVoedselBeperking($deelnemer));
						break;
			}
			$row->addData($status);

			$tbody->addImmutable($row);
		}

		$div->addImmutable($table);

		return $div;
	}
	//Met name voor bekijkjaar($jaar)
	public static function bekijkMentorGroepLozen($jaar = null)
	{
		$kindjes = IntroDeelnemerVerzameling::kindjesZonderGroep($jaar);
		$namen = array();
		foreach ($kindjes as $kindje)
		{
			if($kindje->magBekijken()) {
				$namen[] = IntroDeelnemerView::makeLink($kindje);
			}
		}
		$namen = array_unique($namen);
		return implode(', ', $namen);
	}

	/**
	 *  We maken eerst een pagina, gooien er wat groepsdingen boven en 
	 *  maken het koppenblad.
	 */
	static public function introDeelnemerKoppenBlad()
	{
		$groep = IntroGroep::geef(vfsVarEntryName());

		$page = Page::getInstance();
		$page->setBrand('intro');
		$page->start(sprintf(_("Koppenblad van %s"), $groep->getNaam()));

		$groepKoppenPaginaDiv = new HtmlDiv();

		$groepMentorKoppenDiv = new HtmlDiv();
		$groepMentorKoppenDiv->add(new HtmlHeader(3,_("Mentoren:")));
		$groepMentorKoppenDiv->add(PersoonVerzamelingView::koppen($groep->mentoren()));

		$groepKindjesKoppenDiv = new HtmlDiv();
		$groepKindjesKoppenDiv->add(new HtmlHeader(3,_("Kindjes:")));
		$groepKindjesKoppenDiv->add(PersoonVerzamelingView::koppen($groep->kindjes()));

		$groepKoppenPaginaDiv->add($groepMentorKoppenDiv);
		$groepKoppenPaginaDiv->add($groepKindjesKoppenDiv);

		$page->add($groepKoppenPaginaDiv);

		Page::getInstance()->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
