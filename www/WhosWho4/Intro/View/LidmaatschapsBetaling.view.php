<?
abstract class LidmaatschapsBetalingView
	extends LidmaatschapsBetalingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in LidmaatschapsBetalingView.
	 *
	 * @param obj Het LidmaatschapsBetaling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLidmaatschapsBetaling(LidmaatschapsBetaling $obj)
	{
		return self::defaultWaarde($obj);
	}

	public static function labelenumSoort($value)
	{
		switch($value)
		{
		case 'BOEKVERKOOP': return _('Betaal straks lidmaatschap via PIN');
		case 'IDEAL': return _('Betaal nu lidmaatschap via iDEAL');
		case 'MACHTIGING': return _('Betaal straks lidmaatschap via machtiging');
		}

		return parent::labelenumSoort($value);
	}

	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * Hier wordt voor bijnaleden de optie 'BOEKVERKOOP' verborgen,
	 * anders zouden ze gewoon lid worden zonder te betalen.
	 *
	 * @see genericFormsoort
	 * @param obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 * @return Een HtmlElement waarin de huidige waarde van het veld soort
	 * staat en kan worden bewerkt. Indien soort read-only is betreft
	 * het een statisch html-element.
	 */
	public static function formSoort(LidmaatschapsBetaling $obj, $include_id = false)
	{
		if(!$obj->getLid()->isBijnaLid()) {
			return parent::formSoort($obj, $include_id);
		}

		$veld = 'Soort';

		$name = self::formveld($obj, $veld, null, $include_id);
		$waarde = self::callobj($obj, 'get', $veld);
		$beschikbareEnums = LidmaatschapsBetaling::beschikbareEnumsSoort($waarde);

		return self::genericDefaultFormEnum($name, $waarde, $veld, $beschikbareEnums, false);
	}
}
