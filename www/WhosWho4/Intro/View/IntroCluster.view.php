<?
/**
 * $Id$
 */
abstract class IntroClusterView
	extends IntroClusterView_Generated
{
	// Basic kale view van het object.
	static public function view($cluster)
	{
		echo "<pre>\n";
		print_r($cluster);
		echo "</pre>\n";
	}

	public static function labelNaam(IntroCluster $obj)
	{
		return _('Naam');
	}

	public static function labelJaar(IntroCluster $obj)
	{
		return _('Jaar');
	}

	public static function labelPersoon(IntroCluster $obj)
	{
		return _('Supermentor');
	}

	public static function maakLink($cluster)
	{
		return new HtmlAnchor($cluster->url(), IntroClusterView::waardeNaam($cluster));
	}
	public static function maakNieuweGroepLink (IntroCluster $cluster, $titel)
	{
		if ($cluster && $clusterID = $cluster->geefID())
		{
			return new HtmlAnchor("/Leden/Intro/NieuweMentorgroep?IntroGroep[Cluster]=$clusterID", $titel);
		}
		return NULL;
	}
	static public function toevoegen ($cluster, $show_error)
	{
		$page = Page::getInstance()
			->setBrand('intro')
			->start(_("Introsmurf"), 'intro');
		$page->add(self::nieuweCluster($cluster, $show_error))
			->end();
	}

	static public function nieuweCluster($cluster = null, $show_error = true)
	{
		$form = HtmlForm::named('nieuweCluster');
		$form->add($div = new HtmlDiv());

		$div->add(IntroClusterView::wijzigTR($cluster, 'Naam', $show_error))
			->add(IntroClusterView::wijzigTR($cluster, 'Jaar', $show_error))
			->add(IntroClusterView::wijzigTR($cluster, 'Persoon', $show_error));

		$div->add(HtmlInput::makeFormSubmitButton('Voer in','IntroCluster[submit]'));
		$div->add(new HtmlParagraph("Voeg de mentorgroepjes toe via het mentorgroepformulier."));
		return new HtmlDiv($form, null, 'nieuweCluster');
	}

	public static function processNieuwForm(IntroCluster $clu)
	{
		if (tryPar('IntroCluster[submit]',false))
		{
			$clu->setNaam(tryPar("IntroCluster[Naam]"))
				->setJaar(tryPar("IntroCluster[Jaar]"))
				->setPersoon(tryPar("IntroCluster[Persoon]"));
		}
	}

	public static function formPersoon(IntroCluster $cluster, $include_id = false)
	{
		$a = static::defaultFormPersoonVerzameling(null, 'IntroCluster[Persoon]', true);
		return $a;
	}

	public static function bekijkCluster(IntroCluster $clu)
	{
		$naam = IntroClusterView::waardeNaam($clu);
		$id = $clu->geefID();
		$jaar = $clu->getJaar();
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(1, $naam . ' ' . $jaar));
		$div->add(new HtmlSpan(_("Naam: "), 'strongtext'))
			->add($naam)
			->add(new HtmlBreak())
			->add(new HtmlSpan(_("Jaar: "), 'strongtext'))
			->add(new HtmlAnchor('../../?jaar='.$jaar,$jaar))
			->add(new HtmlBreak());
		if(!is_null($clu->getPersoonContactID()))
		{
			$persoon = $clu->getPersoon();
			$div->add(new HtmlSpan(_("Supermentor: "), 'strongtext'))->add(new HtmlSpan(PersoonView::makeLink($persoon)));
		}
		else
		{
			$div->add(new HtmlSpan(_("Er is nog geen Supermentor"), 'strongtext'));
		}
		$div->add(new HtmlBreak())
			->add(new HtmlSpan(_("Mentorgroepen: "), 'strongtext'));
		$div->add(IntroGroepVerzamelingView::toonGroepen($clu->groepjes()));
		$allementoren = $clu->alleMentoren();
		if(tryPar('inKart',false))
		{ 
			$kart = Kart::geef();
			$kart->voegPersonenToe($allementoren);
			$div->add(_("Er zijn mentoren in je kart geduwd."));
		}
		elseif($allementoren->aantal() != 0)
		{
			$div->add(_("Shop alle mentoren in je kart: ") . new HtmlAnchor( '?inKart="true"',"Shop!"));
		}
		return $div;
	}

	public static function processWijzigForm(IntroCluster $editClu)
	{
		if(!$data = tryPar(self::formobj($editClu), NULL))
		{
			return false;
		}
		self::processForm($editClu);
	}

	public static function wijzig($editClu, $form, $msg)
	{
		$naam = IntroClusterView::waardeNaam($editClu); 
		$page = static::pageLinks($editClu);
		$page
			->setBrand('intro')
			->start(sprintf(_("Wijzig cluster %s"), $naam));

		if($msg !== null)
		{
			$page->add($msg);
		}
		if($form)
		{
			if($msg === null)
			{
				$page->add(IntroClusterView::wijzigForm('WijzigClu', $editClu, false));
			}
			else
			{
				$page->add(IntroClusterView::wijzigForm('WijzigClu', $editClu, true));
			}
		}
		return $page;
	}

	public static function wijzigForm($name = 'WijzigClu', $editClu = null,$show_error = true, $velden = 'viewWijzig')
	{
		if(is_null($editClu))
		{
			$editClu = new IntroCluster();
		}
		else
		{
			$superid = $editClu->getPersoon();
			if(!!$superid)
			{
				$superid = $superid->getContactID();
				Kart::geef()->voegPersoonToe($superid);
			}

		}
		$form = HtmlForm::named($name);
		$form->add($div = new HtmlDiv());

		$div->add($table = new HtmlTable());

		$table->add($thead = new HtmlTableHead())
			->add($tbody = new HtmlTableBody());
		$thead->add(self::wijzigHead($editClu, $show_error));

		$tbody->add(self::wijzigTR($editClu, 'Jaar', $show_error))
			->add(self::wijzigTR($editClu, 'Naam', $show_error))
			->add(self::wijzigTR($editClu, 'Persoon', $show_error));

		$form->add(new HtmlParagraph("Voeg de mentorgroepjes toe via het mentorgroepformulier."));

		$form->add(HtmlInput::makeSubmitButton(_("Wijzig de introcluster")));
		return $form;
	}

	public static function verwijderCluster( $clu, $msg)
	{
		if(hasAuth('god'))
		{
			// TODO SOEPMES: dit is echt niet beste code. Het idee is:
			// Indien de cluster niet in de db staat, komt hier uiteindelijk NULL
			// dus gaan we naar de else-case. Maar dit gaat o.a. fout als per ongeluk een fout objecttype gegeven wordt.
			if($clu instanceof IntroCluster)
			{
				$page = static::pageLinks($clu);
				$page
					->start(null, 'intro')
					->setBrand('intro');

				$page->add(new HtmlSpan("Weet je zeker dat je de volgende cluster "))
					->add(new HtmlSpan("en alle gekoppelde mentorgroepen", 'text-danger strongtext'))
					->add(new HtmlSpan(" wilt verwijderen?"))
					->add(IntroClusterView::bekijkCluster($clu));

				$form = new HtmlForm('post');
				$form->addClass('form-inline');
				$form->add(new HtmlDiv(HtmlInput::makeCheckbox('cluid')
					->setCaption(_("Ik weet het heel zeker!"))))
					->add(new HtmlDiv(HtmlInput::makeSubmitButton(_("Volgende"), 'check')));
				$page->add($form);
				$page->end();
			}
			else
			{
				Page::redirectMelding('/Leden/Intro', 'De cluster is verwijderd, hoera!');
			}
		}
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
