<?
abstract class IntroDeelnemerView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IntroDeelnemerView.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIntroDeelnemer(IntroDeelnemer $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld lid.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld lid labelt.
	 */
	public static function labelLid(IntroDeelnemer $obj)
	{
		return 'Lid';
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lid van het object obj
	 * representeert.
	 */
	public static function waardeLid(IntroDeelnemer $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getLid());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lid representeert.
	 */
	public static function opmerkingLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld betaald.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld betaald labelt.
	 */
	public static function labelBetaald(IntroDeelnemer $obj)
	{
		return 'Betaald';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld betaald.
	 *
	 * @param string $value Een enum-waarde van het veld betaald.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumBetaald($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld betaald horen.
	 *
	 * @see labelenumBetaald
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld betaald
	 * representeren.
	 */
	public static function labelenumBetaaldArray()
	{
		$soorten = array();
		foreach(IntroDeelnemer::enumsBetaald() as $id)
			$soorten[$id] = IntroDeelnemerView::labelenumBetaald($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld betaald.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld betaald van het object obj
	 * representeert.
	 */
	public static function waardeBetaald(IntroDeelnemer $obj)
	{
		return static::defaultWaardeEnum($obj, 'Betaald');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld betaald.
	 *
	 * @see genericFormbetaald
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld betaald staat en kan
	 * worden bewerkt. Indien betaald read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBetaald(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Betaald', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld betaald. In
	 * tegenstelling tot formbetaald moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbetaald
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld betaald staat en kan
	 * worden bewerkt. Indien betaald read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBetaald($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Betaald', IntroDeelnemer::enumsbetaald());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld betaald
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld betaald representeert.
	 */
	public static function opmerkingBetaald()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kamp.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld kamp labelt.
	 */
	public static function labelKamp(IntroDeelnemer $obj)
	{
		return 'Kamp';
	}
	/**
	 * @brief Geef de waarde van het veld kamp.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kamp van het object obj
	 * representeert.
	 */
	public static function waardeKamp(IntroDeelnemer $obj)
	{
		return static::defaultWaardeBool($obj, 'Kamp');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kamp.
	 *
	 * @see genericFormkamp
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kamp staat en kan worden
	 * bewerkt. Indien kamp read-only is betreft het een statisch html-element.
	 */
	public static function formKamp(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Kamp', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kamp. In tegenstelling
	 * tot formkamp moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formkamp
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kamp staat en kan worden
	 * bewerkt. Indien kamp read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormKamp($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Kamp');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld kamp
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kamp representeert.
	 */
	public static function opmerkingKamp()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vega.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld vega labelt.
	 */
	public static function labelVega(IntroDeelnemer $obj)
	{
		return 'Vega';
	}
	/**
	 * @brief Geef de waarde van het veld vega.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vega van het object obj
	 * representeert.
	 */
	public static function waardeVega(IntroDeelnemer $obj)
	{
		return static::defaultWaardeBool($obj, 'Vega');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vega.
	 *
	 * @see genericFormvega
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vega staat en kan worden
	 * bewerkt. Indien vega read-only is betreft het een statisch html-element.
	 */
	public static function formVega(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Vega', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vega. In tegenstelling
	 * tot formvega moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formvega
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vega staat en kan worden
	 * bewerkt. Indien vega read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormVega($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Vega');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vega
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vega representeert.
	 */
	public static function opmerkingVega()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld allergie.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld allergie labelt.
	 */
	public static function labelAllergie(IntroDeelnemer $obj)
	{
		return 'Allergie';
	}
	/**
	 * @brief Geef de waarde van het veld allergie.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld allergie van het object obj
	 * representeert.
	 */
	public static function waardeAllergie(IntroDeelnemer $obj)
	{
		return static::defaultWaardeBool($obj, 'Allergie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld allergie.
	 *
	 * @see genericFormallergie
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld allergie staat en kan
	 * worden bewerkt. Indien allergie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAllergie(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Allergie', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld allergie. In
	 * tegenstelling tot formallergie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formallergie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld allergie staat en kan
	 * worden bewerkt. Indien allergie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAllergie($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Allergie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * allergie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld allergie representeert.
	 */
	public static function opmerkingAllergie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aanwezig.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld aanwezig labelt.
	 */
	public static function labelAanwezig(IntroDeelnemer $obj)
	{
		return 'Aanwezig';
	}
	/**
	 * @brief Geef de waarde van het veld aanwezig.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aanwezig van het object obj
	 * representeert.
	 */
	public static function waardeAanwezig(IntroDeelnemer $obj)
	{
		return static::defaultWaardeBool($obj, 'Aanwezig');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aanwezig.
	 *
	 * @see genericFormaanwezig
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aanwezig staat en kan
	 * worden bewerkt. Indien aanwezig read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAanwezig(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Aanwezig', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aanwezig. In
	 * tegenstelling tot formaanwezig moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formaanwezig
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aanwezig staat en kan
	 * worden bewerkt. Indien aanwezig read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAanwezig($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Aanwezig');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * aanwezig bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aanwezig representeert.
	 */
	public static function opmerkingAanwezig()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld allergieen.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld allergieen labelt.
	 */
	public static function labelAllergieen(IntroDeelnemer $obj)
	{
		return 'Allergieen';
	}
	/**
	 * @brief Geef de waarde van het veld allergieen.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld allergieen van het object obj
	 * representeert.
	 */
	public static function waardeAllergieen(IntroDeelnemer $obj)
	{
		return static::defaultWaardeText($obj, 'Allergieen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld allergieen.
	 *
	 * @see genericFormallergieen
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld allergieen staat en kan
	 * worden bewerkt. Indien allergieen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAllergieen(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Allergieen', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld allergieen. In
	 * tegenstelling tot formallergieen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formallergieen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld allergieen staat en kan
	 * worden bewerkt. Indien allergieen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAllergieen($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Allergieen', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * allergieen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld allergieen representeert.
	 */
	public static function opmerkingAllergieen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld introOpmerkingen.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld introOpmerkingen labelt.
	 */
	public static function labelIntroOpmerkingen(IntroDeelnemer $obj)
	{
		return 'IntroOpmerkingen';
	}
	/**
	 * @brief Geef de waarde van het veld introOpmerkingen.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld introOpmerkingen van het
	 * object obj representeert.
	 */
	public static function waardeIntroOpmerkingen(IntroDeelnemer $obj)
	{
		return static::defaultWaardeText($obj, 'IntroOpmerkingen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld introOpmerkingen.
	 *
	 * @see genericFormintroOpmerkingen
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld introOpmerkingen staat en
	 * kan worden bewerkt. Indien introOpmerkingen read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formIntroOpmerkingen(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'IntroOpmerkingen', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld introOpmerkingen. In
	 * tegenstelling tot formintroOpmerkingen moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formintroOpmerkingen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld introOpmerkingen staat en
	 * kan worden bewerkt. Indien introOpmerkingen read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormIntroOpmerkingen($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'IntroOpmerkingen', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * introOpmerkingen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld introOpmerkingen representeert.
	 */
	public static function opmerkingIntroOpmerkingen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voedselBeperking.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld voedselBeperking labelt.
	 */
	public static function labelVoedselBeperking(IntroDeelnemer $obj)
	{
		return 'VoedselBeperking';
	}
	/**
	 * @brief Geef de waarde van het veld voedselBeperking.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voedselBeperking van het
	 * object obj representeert.
	 */
	public static function waardeVoedselBeperking(IntroDeelnemer $obj)
	{
		return static::defaultWaardeText($obj, 'VoedselBeperking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voedselBeperking.
	 *
	 * @see genericFormvoedselBeperking
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voedselBeperking staat en
	 * kan worden bewerkt. Indien voedselBeperking read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formVoedselBeperking(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'VoedselBeperking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voedselBeperking. In
	 * tegenstelling tot formvoedselBeperking moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formvoedselBeperking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voedselBeperking staat en
	 * kan worden bewerkt. Indien voedselBeperking read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormVoedselBeperking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'VoedselBeperking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voedselBeperking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voedselBeperking representeert.
	 */
	public static function opmerkingVoedselBeperking()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld magischeCode.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld magischeCode labelt.
	 */
	public static function labelMagischeCode(IntroDeelnemer $obj)
	{
		return 'MagischeCode';
	}
	/**
	 * @brief Geef de waarde van het veld magischeCode.
	 *
	 * @param IntroDeelnemer $obj Het IntroDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld magischeCode van het object
	 * obj representeert.
	 */
	public static function waardeMagischeCode(IntroDeelnemer $obj)
	{
		return static::defaultWaardeText($obj, 'MagischeCode');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld magischeCode.
	 *
	 * @see genericFormmagischeCode
	 *
	 * @param IntroDeelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld magischeCode staat en kan
	 * worden bewerkt. Indien magischeCode read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMagischeCode(IntroDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'MagischeCode', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld magischeCode. In
	 * tegenstelling tot formmagischeCode moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmagischeCode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld magischeCode staat en kan
	 * worden bewerkt. Indien magischeCode read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMagischeCode($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'MagischeCode', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * magischeCode bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld magischeCode representeert.
	 */
	public static function opmerkingMagischeCode()
	{
		return NULL;
	}
}
