<?
abstract class MentorView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MentorView.
	 *
	 * @param Mentor $obj Het Mentor-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMentor(Mentor $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld lid.
	 *
	 * @param Mentor $obj Het Mentor-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lid labelt.
	 */
	public static function labelLid(Mentor $obj)
	{
		return 'Lid';
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @param Mentor $obj Het Mentor-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lid van het object obj
	 * representeert.
	 */
	public static function waardeLid(Mentor $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLid())
			return NULL;
		return LidView::defaultWaardeLid($obj->getLid());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lid representeert.
	 */
	public static function opmerkingLid()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld groep.
	 *
	 * @param Mentor $obj Het Mentor-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld groep labelt.
	 */
	public static function labelGroep(Mentor $obj)
	{
		return 'Groep';
	}
	/**
	 * @brief Geef de waarde van het veld groep.
	 *
	 * @param Mentor $obj Het Mentor-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld groep van het object obj
	 * representeert.
	 */
	public static function waardeGroep(Mentor $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getGroep())
			return NULL;
		return IntroGroepView::defaultWaardeIntroGroep($obj->getGroep());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld groep
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld groep representeert.
	 */
	public static function opmerkingGroep()
	{
		return NULL;
	}
}
