<?
abstract class IntroGroepVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IntroGroepVerzamelingView.
	 *
	 * @param IntroGroepVerzameling $obj Het IntroGroepVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIntroGroepVerzameling(IntroGroepVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
