<?
abstract class IntroGroepView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IntroGroepView.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIntroGroep(IntroGroep $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld groepID.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld groepID labelt.
	 */
	public static function labelGroepID(IntroGroep $obj)
	{
		return 'GroepID';
	}
	/**
	 * @brief Geef de waarde van het veld groepID.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld groepID van het object obj
	 * representeert.
	 */
	public static function waardeGroepID(IntroGroep $obj)
	{
		return static::defaultWaardeInt($obj, 'GroepID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld groepID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld groepID representeert.
	 */
	public static function opmerkingGroepID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld cluster.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld cluster labelt.
	 */
	public static function labelCluster(IntroGroep $obj)
	{
		return 'Cluster';
	}
	/**
	 * @brief Geef de waarde van het veld cluster.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld cluster van het object obj
	 * representeert.
	 */
	public static function waardeCluster(IntroGroep $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCluster())
			return NULL;
		return IntroClusterView::defaultWaardeIntroCluster($obj->getCluster());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld cluster.
	 *
	 * @see genericFormcluster
	 *
	 * @param IntroGroep $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld cluster staat en kan
	 * worden bewerkt. Indien cluster read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCluster(IntroGroep $obj, $include_id = false)
	{
		return IntroClusterView::defaultForm($obj->getCluster());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld cluster. In
	 * tegenstelling tot formcluster moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formcluster
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld cluster staat en kan
	 * worden bewerkt. Indien cluster read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCluster($name, $waarde=NULL)
	{
		return IntroClusterView::genericDefaultForm('Cluster');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld cluster
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld cluster representeert.
	 */
	public static function opmerkingCluster()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(IntroGroep $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param IntroGroep $obj Het IntroGroep-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(IntroGroep $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param IntroGroep $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(IntroGroep $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
}
