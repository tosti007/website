<?
abstract class IntroDeelnemerVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IntroDeelnemerVerzamelingView.
	 *
	 * @param IntroDeelnemerVerzameling $obj Het IntroDeelnemerVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIntroDeelnemerVerzameling(IntroDeelnemerVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
