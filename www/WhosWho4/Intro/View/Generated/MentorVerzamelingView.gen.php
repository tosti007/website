<?
abstract class MentorVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MentorVerzamelingView.
	 *
	 * @param MentorVerzameling $obj Het MentorVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMentorVerzameling(MentorVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
