<?
abstract class IntroClusterView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IntroClusterView.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIntroCluster(IntroCluster $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld clusterID.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld clusterID labelt.
	 */
	public static function labelClusterID(IntroCluster $obj)
	{
		return 'ClusterID';
	}
	/**
	 * @brief Geef de waarde van het veld clusterID.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld clusterID van het object obj
	 * representeert.
	 */
	public static function waardeClusterID(IntroCluster $obj)
	{
		return static::defaultWaardeInt($obj, 'ClusterID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * clusterID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld clusterID representeert.
	 */
	public static function opmerkingClusterID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld jaar.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld jaar labelt.
	 */
	public static function labelJaar(IntroCluster $obj)
	{
		return 'Jaar';
	}
	/**
	 * @brief Geef de waarde van het veld jaar.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld jaar van het object obj
	 * representeert.
	 */
	public static function waardeJaar(IntroCluster $obj)
	{
		return static::defaultWaardeInt($obj, 'Jaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld jaar.
	 *
	 * @see genericFormjaar
	 *
	 * @param IntroCluster $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaar staat en kan worden
	 * bewerkt. Indien jaar read-only is betreft het een statisch html-element.
	 */
	public static function formJaar(IntroCluster $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Jaar', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld jaar. In tegenstelling
	 * tot formjaar moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formjaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaar staat en kan worden
	 * bewerkt. Indien jaar read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormJaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Jaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld jaar
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld jaar representeert.
	 */
	public static function opmerkingJaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(IntroCluster $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(IntroCluster $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param IntroCluster $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(IntroCluster $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(IntroCluster $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param IntroCluster $obj Het IntroCluster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(IntroCluster $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param IntroCluster $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(IntroCluster $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Persoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
}
