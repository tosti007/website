<?
abstract class ActivatieCodeVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActivatieCodeVerzamelingView.
	 *
	 * @param ActivatieCodeVerzameling $obj Het ActivatieCodeVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActivatieCodeVerzameling(ActivatieCodeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
