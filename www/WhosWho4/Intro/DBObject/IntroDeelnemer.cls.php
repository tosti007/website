<?

/***
 * $Id$
 */
class IntroDeelnemer
	extends IntroDeelnemer_Generated
{
	/** CONSTRUCTOR **/
	public function __construct($a)
	{
		parent::__construct($a);
	}

	/**
	 * @brief Geef de introdeelnemer met een gegeven magische code.
	 *
	 * @param code De magische code om op te geven.
	 *
	 * @return Een IntroDeelnemer-object, of null indien niet gevonden.
	 */
	public static function geefUitMagischeCode($code)
	{
		return IntroDeelnemerQuery::table()
			->whereProp('magischeCode', $code)
			->geef();
	}

	/**
	 * Stel een nieuwe willekeurige magische in voor de deelnemer.
	 *
	 * @return string
	 * De gegenereerde code.
	 */
	public function genereerMagischeCode()
	{
		$code = hash("sha256", Crypto::getRandomBytes(42));
		$this->setMagischeCode($code);
		return $code;
	}

	public function url()
	{
		return '/Leden/Intro/Deelnemer/' . $this->getLidContactID();
	}

	public function magWijzigen()
	{
		return hasAuth('intro');
	}

	public function magVerwijderen()
	{
		global $auth;
		return hasAuth('intro') || $auth->getLevel() == 'mollie';
	}

	public function getGroep()
	{
		if(!$this->getLid()) //Van niet leden weten wij geen studies, dus als je geen eens bijna-lid bent (een lidobject hebt) retourneren we null.
		{
			return null;
		}
		else
		{
			$studies = $this->getLid()->getStudies();
			foreach($studies as $studie)
			{
				if($studie->getDatumBegin()->format('Y') == colJaar())
				{
					return $studie->getGroep();
				}
			}
			return null;
		}
	}

	public static function introMail($lid = null, $deel = null, $reservelijst = false)
	{
		global $WSW4DB;
		
		$MAIL_FROM = "Introductiecommissie <bachelor@intro-utrecht.nl>";
		$MAIL_CC = "Introductiecommissie Inschrijvingen <intro-inschrijvingen@A-Eskwadraat.nl>";
		$EXTRA_HEADERS = "Cc: $MAIL_CC\nX-Mailer: A-Eskwadraat Who's Who 4";//Dus ge-CC't naar intro-inschrijvingen
		
		// TODO: maak het ophalen van de introcie mooier :P
		if(Commissie::cieByLogin("intro".Date('Y')) == NULL)
		{   
			$date = Date('Y') -1;
			$intro = "intro".$date;
		}
		else
		{
			$intro = "intro".Date('Y');
		}
		$com = Commissie::cieByLogin($intro);//Pak meest recente intro, heeft eigenlijk alleen zin in het geval van debug vlak voor nieuwe intro opgezet is.
		$comID = $com->geefID();
		$introsecr = $WSW4DB->q('COLUMN SELECT persoon_contactID'
								. ' FROM CommissieLid'
								. ' WHERE commissie_commissieID = %i AND functie = "SECR"'
								, $comID); //Retouneert lidnr van de secretaris vd meest recente intro
		
		$sig = CommissieView::waardeNaam($com);
		if(count($introsecr) == 1) //Als er 1 secretaris is, onderteken met secretaris+commissienaam ipv alleen commissienaam
		{
				$sig = PersoonView::naam(Persoon::geef($introsecr[0])) . "\nSecretaris " . $sig;
		}
		
$verhaalnormaal = <<<HEREDOC
Leuk dat je meegaat op kamp! Het gaat een groot en gezellig feest worden. Jullie zullen alle
informatie hierover te zijner tijd nog ontvangen. Hou ook vooral de website in de gaten.
Mocht je toch niet mee kunnen, schrijf je dan uiterlijk zondag 2 september voor 23:59 uur uit
door ons te mailen.
HEREDOC;
$verhaalreserve = <<<HEREDOC
LET OP! Je staat nu op de reservelijst en het is dus niet definitief dat je meegaat op kamp.
Vlak voor de intro bellen we je mogelijk met de vraag of je een van de vrijgekomen plekjes
wilt hebben.
Blijkt dat we woensdag nog steeds geen plek voor je hebben kunnen vinden, dan storten we
natuurlijk ook het inschrijfgeld weer terug.
Mocht je toch niet mee kunnen, schrijf je dan uiterlijk zondag 2 september voor 23:59 uur uit
door ons te mailen. Dan halen wij jou van de reservelijst af.
HEREDOC;

		if($lid instanceof Lid && $deel instanceof IntroDeelnemer)
		{
			if($reservelijst){
				$verhaal = $verhaalreserve;
			} else {
				$verhaal = $verhaalnormaal;
			}
		
			$naam = PersoonView::naam($lid);
			$geboorte = $lid->getDatumGeboorte()->format('d-m-Y');
			$lidnr = $lid->getContactID();
			$studies = LidStudieVerzamelingView::simpel(LidStudieVerzameling::vanLid($lid));
			$studnr = (LidView::waardeStudentnr($lid) == "") ? "Niet ingevuld" : (LidView::waardeStudentnr($lid));
			
			$adres = ContactAdresView::plainAdres(ContactAdres::eersteBijContact($lid));
			$tel = ContactTelnrVerzamelingView::regels(ContactTelnrVerzameling::vanContact($lid));
			$kamp = $deel->getKamp() ? "Ja." . ($reservelijst?" (reservelijst)":""): "Nee."; 
			$vega = $deel->getVega() ? "Ja." : "Nee.";
			$allergie = (!$deel->getAllergie()) ? "Nee." : ("Ja. \n". IntroDeelnemerView::waardeAllergieen($deel)); 
			$voedsel = (IntroDeelnemerView::waardeVoedselBeperking($deel) == "") ? "Nee." : ("Ja. \n". IntroDeelnemerView::waardeVoedselBeperking($deel));
			$opm = IntroDeelnemerView::waardeIntroOpmerkingen($deel);
		
			$email = $lid->getEmail();
			$subject = "Inschrijving introductie [$lidnr]";

			$kamp_verhaal = '';
			if ($deel->getKamp())
				$kamp_verhaal = $verhaal;

		}
		else
		{

			$verhaal = "[Normale aanmelding]\n\n" .
					$verhaalnormaal
					. "\n\n[\Normale aanmelding]\n[Reservelijst aanmelding]\n\n" .
					$verhaalreserve
					. "\n\n[\Reservelijst aanmelding]";

			$naam = "Voor Beeld";
			$geboorte = "1-1-1911";
			$lidnr = "1234";
			$studies = "Wiskunde, Natuurkunde";
			$adres = "Ergens op de aardbol 35";
			$tel = "Thuisnummer: 0612345678";
			$kamp = "Ja of Nee"; 
			$vega = "Ja of Nee";
			$allergie = "Ja, lekke banden."; 
			$opm = "Hier staat een opmerking";
			$email = "email@email.com";
			$subject = "Inschrijving introductie 1234";
			$voedsel = "Ja. \nIk kan niet tegen eten";
			$kamp_verhaal = $verhaal. "\n[Dit verhaal staat er alleen als de deelnemer mee op kamp gaat]";
			$studnr = '3477029';//Iemands studentnummer
		}

$message = <<<HEREDOC
Beste aankomende eerstejaars,

Bedankt voor je inschrijving!

$kamp_verhaal

Onderaan deze mail staan de door jou ingevulde gegevens. Mocht er hier
iets ontbreken, of verkeerd zijn ingevuld, reageer dan even op deze mail.

Op maandag 3, dinsdag 4 en donderdag 13 september gaan we gezamenlijk eten
en op donderdag 13 september is het eindfeest. Wil je hier bij zijn, en ja, dat wil je, dan kun
je een intropakket kopen via:
http://www.intro-utrecht.nl/#pakketten

Hieronder een aantal belangrijke data op een rijtje:
- Ma 3 september: De (verplichte) facultaire introductie voor alle eerstejaars en boekverkoop
+ allerlei andere praktische zaken (en natuurlijk ook nog gezellige dingen, zoals uit eten met
je mentorgroepje!)
- Di 4 september: Verplichte introductiedag en uit eten met je mentorgroepje.
- Wo 5 t/m vr 7 september: Introductiekamp met het thema 'Duel of the Decades'
- Do 13 september: Introductie-eindfeest

Het introductiekatern met aanvullende informatie kan je vinden op:
http://www.intro-utrecht.nl/#katern

We hopen je hiermee voldoende op de hoogte te hebben gebracht.
Nog een prettige vakantie en tot ziens op de introductie!

$sig

Studievereniging A–Eskwadraat
Universiteit Utrecht
tel: 030-253 4499
e-mail: bachelor@intro-utrecht.nl
www: bachelor.intro-utrecht.nl

Personalia:
$naam
$geboorte 
Inschrijfnummer: $lidnr
Studie: $studies
Studentnummer: $studnr

Adres:
$adres

Telefoonnummers: 
$tel
Mee op kamp? $kamp
Vegetariër? $vega

Allergieën:
$allergie

Overige voedsel beperking:
$voedsel

Overige opmerkingen:
$opm
HEREDOC;
		
		if($lid instanceof Lid && $deel instanceof IntroDeelnemer)
		{
			sendmail($MAIL_FROM, $email, $subject,nl2br($message), null, $EXTRA_HEADERS, 'intro@A-Eskwadraat.nl', true);
		}
		else
		{
			return "<pre>".$message."</pre>";
		}

	}

	/**
	 * @brief Zet bij een IntroDeelnemer dat die betaald heeft via iDEAL.
	 *
	 * Wordt aangeroepen vanuit iDeal::handelArtikelAf.
	 *
	 * @param email Het adres van de introdeelnemer.
     */
    public static function idealBetaald($email) {

        $lid = Contact::zoekOpEmail($email);
        if(!$lid) {
            sendmail('webcie@a-eskwadraat.nl'
                    , 'webcie@a-eskwadraat.nl'
                    , 'Introinschrijving misgegaan'
                    , "Yo WebCie!\r\nEr is net een iDealtransactie voor een introkamp succesvol betaald, "
                    . "maar de flippo van wie de betaling is hebben we niet kunnen vinden. "
                    . "Het gaat om iDealtransactie " . $this->geefID() . ". Fix dit moeilijk snel! "
                    . "\r\n\r\nGroetjes, de Website!");
            return;
        }
        $deelnemer = IntroDeelnemer::geef($lid->geefID());
        if(!$deelnemer || $deelnemer->getBetaald() != 'NEE') {
            sendmail('webcie@a-eskwadraat.nl'
                    , 'webcie@a-eskwadraat.nl'
                    , 'Introinschrijving misgegaan'
                    , "Yo WebCie!\r\nEr is net een iDealtransactie voor een introkamp succesvol betaald, "
                    . "maar er is geen deelnemer bij de flippo van wie de betaling is gevonden. "
                    . "Het gaat om iDealtransactie " . $this->geefID() . ". Fix dit moeilijk snel! "
                    . "\r\n\r\nGroetjes, de Website!");
            return;
        }

        //Verstuur de mail naar de deelnemer/intro om te laten weten dat de inschrijving succesvol was:
        $kampgangers = IntroDeelnemerVerzameling::alleBijnaLeden("true");
        $reservelijst = $kampgangers->aantal() >= MAXAANTALKAMP;
        self::introMail($lid, $deelnemer, $reservelijst);

        if (strlen($deelnemer->getIntroOpmerkingen()) > 1) {
            sendmail('www-data@a-eskwadraat.nl', 'bachelor@intro-utrecht.nl', 'Inschrijving [' . $lid->geefID() . '] heeft vragen/opmerkingen',
                    'Beste intro,' . "\n\n" .
                    'Er heeft zich iemand ingeschreven met nog een vraag/opmerking:' . "\n" .
                    $deelnemer->getIntroOpmerkingen() . "\n\n" .
                    'Handelen jullie het verder af?' . "\n\n" .
                    'Met vriendelijke groeten,' . "\n" .
                    'De introsmurf', $lid->getEmail());
        }

        //vanwege gekke voodoo magie moet opslaan gelijk na setBetaald.
        $deelnemer->setBetaald('IDEAL');
        $deelnemer->opslaan();
    }
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
