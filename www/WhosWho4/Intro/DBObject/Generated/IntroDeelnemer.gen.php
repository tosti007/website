<?
abstract class IntroDeelnemer_Generated
	extends Entiteit
{
	protected $lid;						/**< \brief PRIMARY */
	protected $lid_contactID;			/**< \brief PRIMARY */
	protected $betaald;					/**< \brief ENUM:NEE/BANK/PIN/KAS/IDEAL/ONBEKEND */
	protected $kamp;
	protected $vega;
	protected $allergie;
	protected $aanwezig;
	protected $allergieen;				/**< \brief NULL */
	protected $introOpmerkingen;		/**< \brief NULL */
	protected $voedselBeperking;		/**< \brief NULL */
	protected $magischeCode;
	/**
	/**
	 * @brief De constructor van de IntroDeelnemer_Generated-klasse.
	 *
	 * @param mixed $a Lid (Lid OR Array(lid_contactID) OR lid_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Lid)
		{
			$this->lid = $a;
			$this->lid_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->betaald = 'NEE';
		$this->kamp = False;
		$this->vega = False;
		$this->allergie = False;
		$this->aanwezig = False;
		$this->allergieen = NULL;
		$this->introOpmerkingen = NULL;
		$this->voedselBeperking = NULL;
		$this->magischeCode = '';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Lid';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @return Lid
	 * De waarde van het veld lid.
	 */
	public function getLid()
	{
		if(!isset($this->lid)
		 && isset($this->lid_contactID)
		 ) {
			$this->lid = Lid::geef
					( $this->lid_contactID
					);
		}
		return $this->lid;
	}
	/**
	 * @brief Geef de waarde van het veld lid_contactID.
	 *
	 * @return int
	 * De waarde van het veld lid_contactID.
	 */
	public function getLidContactID()
	{
		if (is_null($this->lid_contactID) && isset($this->lid)) {
			$this->lid_contactID = $this->lid->getContactID();
		}
		return $this->lid_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld betaald.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld betaald.
	 */
	static public function enumsBetaald()
	{
		static $vals = array('NEE','BANK','PIN','KAS','IDEAL','ONBEKEND');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld betaald.
	 *
	 * @return string
	 * De waarde van het veld betaald.
	 */
	public function getBetaald()
	{
		return $this->betaald;
	}
	/**
	 * @brief Stel de waarde van het veld betaald in.
	 *
	 * @param mixed $newBetaald De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setBetaald($newBetaald)
	{
		unset($this->errors['Betaald']);
		if(!is_null($newBetaald))
			$newBetaald = strtoupper(trim($newBetaald));
		if($newBetaald === "")
			$newBetaald = NULL;
		if($this->betaald === $newBetaald)
			return $this;

		$this->betaald = $newBetaald;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld betaald geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld betaald geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBetaald()
	{
		if (array_key_exists('Betaald', $this->errors))
			return $this->errors['Betaald'];
		$waarde = $this->getBetaald();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsBetaald()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld kamp.
	 *
	 * @return bool
	 * De waarde van het veld kamp.
	 */
	public function getKamp()
	{
		return $this->kamp;
	}
	/**
	 * @brief Stel de waarde van het veld kamp in.
	 *
	 * @param mixed $newKamp De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setKamp($newKamp)
	{
		unset($this->errors['Kamp']);
		if(!is_null($newKamp))
			$newKamp = (bool)$newKamp;
		if($this->kamp === $newKamp)
			return $this;

		$this->kamp = $newKamp;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kamp geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kamp geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkKamp()
	{
		if (array_key_exists('Kamp', $this->errors))
			return $this->errors['Kamp'];
		$waarde = $this->getKamp();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld vega.
	 *
	 * @return bool
	 * De waarde van het veld vega.
	 */
	public function getVega()
	{
		return $this->vega;
	}
	/**
	 * @brief Stel de waarde van het veld vega in.
	 *
	 * @param mixed $newVega De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setVega($newVega)
	{
		unset($this->errors['Vega']);
		if(!is_null($newVega))
			$newVega = (bool)$newVega;
		if($this->vega === $newVega)
			return $this;

		$this->vega = $newVega;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld vega geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld vega geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkVega()
	{
		if (array_key_exists('Vega', $this->errors))
			return $this->errors['Vega'];
		$waarde = $this->getVega();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld allergie.
	 *
	 * @return bool
	 * De waarde van het veld allergie.
	 */
	public function getAllergie()
	{
		return $this->allergie;
	}
	/**
	 * @brief Stel de waarde van het veld allergie in.
	 *
	 * @param mixed $newAllergie De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setAllergie($newAllergie)
	{
		unset($this->errors['Allergie']);
		if(!is_null($newAllergie))
			$newAllergie = (bool)$newAllergie;
		if($this->allergie === $newAllergie)
			return $this;

		$this->allergie = $newAllergie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld allergie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld allergie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAllergie()
	{
		if (array_key_exists('Allergie', $this->errors))
			return $this->errors['Allergie'];
		$waarde = $this->getAllergie();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld aanwezig.
	 *
	 * @return bool
	 * De waarde van het veld aanwezig.
	 */
	public function getAanwezig()
	{
		return $this->aanwezig;
	}
	/**
	 * @brief Stel de waarde van het veld aanwezig in.
	 *
	 * @param mixed $newAanwezig De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setAanwezig($newAanwezig)
	{
		unset($this->errors['Aanwezig']);
		if(!is_null($newAanwezig))
			$newAanwezig = (bool)$newAanwezig;
		if($this->aanwezig === $newAanwezig)
			return $this;

		$this->aanwezig = $newAanwezig;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aanwezig geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aanwezig geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAanwezig()
	{
		if (array_key_exists('Aanwezig', $this->errors))
			return $this->errors['Aanwezig'];
		$waarde = $this->getAanwezig();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld allergieen.
	 *
	 * @return string
	 * De waarde van het veld allergieen.
	 */
	public function getAllergieen()
	{
		return $this->allergieen;
	}
	/**
	 * @brief Stel de waarde van het veld allergieen in.
	 *
	 * @param mixed $newAllergieen De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setAllergieen($newAllergieen)
	{
		unset($this->errors['Allergieen']);
		if(!is_null($newAllergieen))
			$newAllergieen = trim($newAllergieen);
		if($newAllergieen === "")
			$newAllergieen = NULL;
		if($this->allergieen === $newAllergieen)
			return $this;

		$this->allergieen = $newAllergieen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld allergieen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld allergieen geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAllergieen()
	{
		if (array_key_exists('Allergieen', $this->errors))
			return $this->errors['Allergieen'];
		$waarde = $this->getAllergieen();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld introOpmerkingen.
	 *
	 * @return string
	 * De waarde van het veld introOpmerkingen.
	 */
	public function getIntroOpmerkingen()
	{
		return $this->introOpmerkingen;
	}
	/**
	 * @brief Stel de waarde van het veld introOpmerkingen in.
	 *
	 * @param mixed $newIntroOpmerkingen De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setIntroOpmerkingen($newIntroOpmerkingen)
	{
		unset($this->errors['IntroOpmerkingen']);
		if(!is_null($newIntroOpmerkingen))
			$newIntroOpmerkingen = trim($newIntroOpmerkingen);
		if($newIntroOpmerkingen === "")
			$newIntroOpmerkingen = NULL;
		if($this->introOpmerkingen === $newIntroOpmerkingen)
			return $this;

		$this->introOpmerkingen = $newIntroOpmerkingen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld introOpmerkingen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld introOpmerkingen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkIntroOpmerkingen()
	{
		if (array_key_exists('IntroOpmerkingen', $this->errors))
			return $this->errors['IntroOpmerkingen'];
		$waarde = $this->getIntroOpmerkingen();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld voedselBeperking.
	 *
	 * @return string
	 * De waarde van het veld voedselBeperking.
	 */
	public function getVoedselBeperking()
	{
		return $this->voedselBeperking;
	}
	/**
	 * @brief Stel de waarde van het veld voedselBeperking in.
	 *
	 * @param mixed $newVoedselBeperking De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setVoedselBeperking($newVoedselBeperking)
	{
		unset($this->errors['VoedselBeperking']);
		if(!is_null($newVoedselBeperking))
			$newVoedselBeperking = trim($newVoedselBeperking);
		if($newVoedselBeperking === "")
			$newVoedselBeperking = NULL;
		if($this->voedselBeperking === $newVoedselBeperking)
			return $this;

		$this->voedselBeperking = $newVoedselBeperking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld voedselBeperking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld voedselBeperking geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVoedselBeperking()
	{
		if (array_key_exists('VoedselBeperking', $this->errors))
			return $this->errors['VoedselBeperking'];
		$waarde = $this->getVoedselBeperking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld magischeCode.
	 *
	 * @return string
	 * De waarde van het veld magischeCode.
	 */
	public function getMagischeCode()
	{
		return $this->magischeCode;
	}
	/**
	 * @brief Stel de waarde van het veld magischeCode in.
	 *
	 * @param mixed $newMagischeCode De nieuwe waarde.
	 *
	 * @return IntroDeelnemer
	 * Dit IntroDeelnemer-object.
	 */
	public function setMagischeCode($newMagischeCode)
	{
		unset($this->errors['MagischeCode']);
		if(!is_null($newMagischeCode))
			$newMagischeCode = trim($newMagischeCode);
		if($newMagischeCode === "")
			$newMagischeCode = NULL;
		if($this->magischeCode === $newMagischeCode)
			return $this;

		$this->magischeCode = $newMagischeCode;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld magischeCode geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld magischeCode geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMagischeCode()
	{
		if (array_key_exists('MagischeCode', $this->errors))
			return $this->errors['MagischeCode'];
		$waarde = $this->getMagischeCode();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return IntroDeelnemer::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van IntroDeelnemer.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return IntroDeelnemer::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getLidContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return IntroDeelnemer|false
	 * Een IntroDeelnemer-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$lid_contactID = (int)$a[0];
		}
		else if($a instanceof Lid)
		{
			$lid_contactID = $a->getContactID();
		}
		else if(isset($a))
		{
			$lid_contactID = (int)$a;
		}

		if(is_null($lid_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($lid_contactID) ));
		return Entiteit::geefCache(array($lid_contactID), 'IntroDeelnemer');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'IntroDeelnemer');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('IntroDeelnemer::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `IntroDeelnemer`.`lid_contactID`'
		                 .     ', `IntroDeelnemer`.`betaald`'
		                 .     ', `IntroDeelnemer`.`kamp`'
		                 .     ', `IntroDeelnemer`.`vega`'
		                 .     ', `IntroDeelnemer`.`allergie`'
		                 .     ', `IntroDeelnemer`.`aanwezig`'
		                 .     ', `IntroDeelnemer`.`allergieen`'
		                 .     ', `IntroDeelnemer`.`introOpmerkingen`'
		                 .     ', `IntroDeelnemer`.`voedselBeperking`'
		                 .     ', `IntroDeelnemer`.`magischeCode`'
		                 .     ', `IntroDeelnemer`.`gewijzigdWanneer`'
		                 .     ', `IntroDeelnemer`.`gewijzigdWie`'
		                 .' FROM `IntroDeelnemer`'
		                 .' WHERE (`lid_contactID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['lid_contactID']);

			$obj = new IntroDeelnemer(array($row['lid_contactID']));

			$obj->inDB = True;

			$obj->betaald  = strtoupper(trim($row['betaald']));
			$obj->kamp  = (bool) $row['kamp'];
			$obj->vega  = (bool) $row['vega'];
			$obj->allergie  = (bool) $row['allergie'];
			$obj->aanwezig  = (bool) $row['aanwezig'];
			$obj->allergieen  = (is_null($row['allergieen'])) ? null : trim($row['allergieen']);
			$obj->introOpmerkingen  = (is_null($row['introOpmerkingen'])) ? null : trim($row['introOpmerkingen']);
			$obj->voedselBeperking  = (is_null($row['voedselBeperking'])) ? null : trim($row['voedselBeperking']);
			$obj->magischeCode  = trim($row['magischeCode']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'IntroDeelnemer')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getLid();
		if(!$rel->getInDB())
			throw new LogicException('foreign Lid is not in DB');
		$this->lid_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `IntroDeelnemer`'
			          . ' (`lid_contactID`, `betaald`, `kamp`, `vega`, `allergie`, `aanwezig`, `allergieen`, `introOpmerkingen`, `voedselBeperking`, `magischeCode`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %i, %i, %i, %i, %s, %s, %s, %s, %s, %i)'
			          , $this->lid_contactID
			          , $this->betaald
			          , $this->kamp
			          , $this->vega
			          , $this->allergie
			          , $this->aanwezig
			          , $this->allergieen
			          , $this->introOpmerkingen
			          , $this->voedselBeperking
			          , $this->magischeCode
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'IntroDeelnemer')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `IntroDeelnemer`'
			          .' SET `betaald` = %s'
			          .   ', `kamp` = %i'
			          .   ', `vega` = %i'
			          .   ', `allergie` = %i'
			          .   ', `aanwezig` = %i'
			          .   ', `allergieen` = %s'
			          .   ', `introOpmerkingen` = %s'
			          .   ', `voedselBeperking` = %s'
			          .   ', `magischeCode` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lid_contactID` = %i'
			          , $this->betaald
			          , $this->kamp
			          , $this->vega
			          , $this->allergie
			          , $this->aanwezig
			          , $this->allergieen
			          , $this->introOpmerkingen
			          , $this->voedselBeperking
			          , $this->magischeCode
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lid_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenIntroDeelnemer
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenIntroDeelnemer($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenIntroDeelnemer($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Betaald';
			$velden[] = 'Kamp';
			$velden[] = 'Vega';
			$velden[] = 'Allergie';
			$velden[] = 'Aanwezig';
			$velden[] = 'Allergieen';
			$velden[] = 'IntroOpmerkingen';
			$velden[] = 'VoedselBeperking';
			$velden[] = 'MagischeCode';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'MagischeCode';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Betaald';
			$velden[] = 'Kamp';
			$velden[] = 'Vega';
			$velden[] = 'Allergie';
			$velden[] = 'Aanwezig';
			$velden[] = 'Allergieen';
			$velden[] = 'IntroOpmerkingen';
			$velden[] = 'VoedselBeperking';
			$velden[] = 'MagischeCode';
			break;
		case 'get':
			$velden[] = 'Betaald';
			$velden[] = 'Kamp';
			$velden[] = 'Vega';
			$velden[] = 'Allergie';
			$velden[] = 'Aanwezig';
			$velden[] = 'Allergieen';
			$velden[] = 'IntroOpmerkingen';
			$velden[] = 'VoedselBeperking';
			$velden[] = 'MagischeCode';
		case 'primary':
			$velden[] = 'lid_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Betaald';
			$velden[] = 'Kamp';
			$velden[] = 'Vega';
			$velden[] = 'Allergie';
			$velden[] = 'Aanwezig';
			$velden[] = 'Allergieen';
			$velden[] = 'IntroOpmerkingen';
			$velden[] = 'VoedselBeperking';
			$velden[] = 'MagischeCode';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `IntroDeelnemer`'
		          .' WHERE `lid_contactID` = %i'
		          .' LIMIT 1'
		          , $this->lid_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->lid = NULL;
		$this->lid_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `IntroDeelnemer`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lid_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lid_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van IntroDeelnemer terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'lid':
			return 'foreign';
		case 'lid_contactid':
			return 'int';
		case 'betaald':
			return 'enum';
		case 'kamp':
			return 'bool';
		case 'vega':
			return 'bool';
		case 'allergie':
			return 'bool';
		case 'aanwezig':
			return 'bool';
		case 'allergieen':
			return 'text';
		case 'introopmerkingen':
			return 'text';
		case 'voedselbeperking':
			return 'text';
		case 'magischecode':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'lid_contactID':
		case 'kamp':
		case 'vega':
		case 'allergie':
		case 'aanwezig':
			$type = '%i';
			break;
		case 'betaald':
		case 'allergieen':
		case 'introOpmerkingen':
		case 'voedselBeperking':
		case 'magischeCode':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `IntroDeelnemer`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `lid_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->lid_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `IntroDeelnemer`'
		          .' WHERE `lid_contactID` = %i'
		                 , $veld
		          , $this->lid_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'IntroDeelnemer');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
