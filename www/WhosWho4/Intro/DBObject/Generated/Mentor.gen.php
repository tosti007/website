<?
abstract class Mentor_Generated
	extends Entiteit
{
	protected $lid;						/**< \brief PRIMARY */
	protected $lid_contactID;			/**< \brief PRIMARY */
	protected $groep;					/**< \brief PRIMARY */
	protected $groep_groepID;			/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de Mentor_Generated-klasse.
	 *
	 * @param mixed $a Lid (Lid OR Array(lid_contactID) OR lid_contactID)
	 * @param mixed $b Groep (IntroGroep OR Array(introGroep_groepID) OR
	 * introGroep_groepID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Lid)
		{
			$this->lid = $a;
			$this->lid_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof IntroGroep)
		{
			$this->groep = $b;
			$this->groep_groepID = $b->getGroepID();
		}
		else if(is_array($b))
		{
			$this->groep = NULL;
			$this->groep_groepID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->groep = NULL;
			$this->groep_groepID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Lid';
		$volgorde[] = 'Groep';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @return Lid
	 * De waarde van het veld lid.
	 */
	public function getLid()
	{
		if(!isset($this->lid)
		 && isset($this->lid_contactID)
		 ) {
			$this->lid = Lid::geef
					( $this->lid_contactID
					);
		}
		return $this->lid;
	}
	/**
	 * @brief Geef de waarde van het veld lid_contactID.
	 *
	 * @return int
	 * De waarde van het veld lid_contactID.
	 */
	public function getLidContactID()
	{
		if (is_null($this->lid_contactID) && isset($this->lid)) {
			$this->lid_contactID = $this->lid->getContactID();
		}
		return $this->lid_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld groep.
	 *
	 * @return IntroGroep
	 * De waarde van het veld groep.
	 */
	public function getGroep()
	{
		if(!isset($this->groep)
		 && isset($this->groep_groepID)
		 ) {
			$this->groep = IntroGroep::geef
					( $this->groep_groepID
					);
		}
		return $this->groep;
	}
	/**
	 * @brief Geef de waarde van het veld groep_groepID.
	 *
	 * @return int
	 * De waarde van het veld groep_groepID.
	 */
	public function getGroepGroepID()
	{
		if (is_null($this->groep_groepID) && isset($this->groep)) {
			$this->groep_groepID = $this->groep->getGroepID();
		}
		return $this->groep_groepID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Mentor::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Mentor.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Mentor::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getLidContactID()
		                      , $this->getGroepGroepID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return Mentor|false
	 * Een Mentor-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$lid_contactID = (int)$a[0];
			$groep_groepID = (int)$a[1];
		}
		else if($a instanceof Lid
		     && $b instanceof IntroGroep)
		{
			$lid_contactID = $a->getContactID();
			$groep_groepID = $b->getGroepID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$lid_contactID = (int)$a;
			$groep_groepID = (int)$b;
		}

		if(is_null($lid_contactID)
		|| is_null($groep_groepID))
			throw new BadMethodCallException();

		static::cache(array( array($lid_contactID, $groep_groepID) ));
		return Entiteit::geefCache(array($lid_contactID, $groep_groepID), 'Mentor');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Mentor');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Mentor::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Mentor`.`lid_contactID`'
		                 .     ', `Mentor`.`groep_groepID`'
		                 .     ', `Mentor`.`gewijzigdWanneer`'
		                 .     ', `Mentor`.`gewijzigdWie`'
		                 .' FROM `Mentor`'
		                 .' WHERE (`lid_contactID`, `groep_groepID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['lid_contactID']
			                  ,$row['groep_groepID']);

			$obj = new Mentor(array($row['lid_contactID']), array($row['groep_groepID']));

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Mentor')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getLid();
		if(!$rel->getInDB())
			throw new LogicException('foreign Lid is not in DB');
		$this->lid_contactID = $rel->getContactID();

		$rel = $this->getGroep();
		if(!$rel->getInDB())
			throw new LogicException('foreign Groep is not in DB');
		$this->groep_groepID = $rel->getGroepID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Mentor`'
			          . ' (`lid_contactID`, `groep_groepID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i)'
			          , $this->lid_contactID
			          , $this->groep_groepID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Mentor')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Mentor`'
			.' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lid_contactID` = %i'
			          .  ' AND `groep_groepID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lid_contactID
			          , $this->groep_groepID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMentor
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMentor($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMentor($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			break;
		case 'get':
		case 'primary':
			$velden[] = 'lid_contactID';
			$velden[] = 'groep_groepID';
			break;
		case 'verzamelingen':
			break;
		default:
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Mentor`'
		          .' WHERE `lid_contactID` = %i'
		          .  ' AND `groep_groepID` = %i'
		          .' LIMIT 1'
		          , $this->lid_contactID
		          , $this->groep_groepID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->lid = NULL;
		$this->lid_contactID = NULL;
		$this->groep = NULL;
		$this->groep_groepID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Mentor`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lid_contactID` = %i'
			          .  ' AND `groep_groepID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lid_contactID
			          , $this->groep_groepID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Mentor terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'lid':
			return 'foreign';
		case 'lid_contactid':
			return 'int';
		case 'groep':
			return 'foreign';
		case 'groep_groepid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Mentor`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `lid_contactID` = %i'
		          .  ' AND `groep_groepID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->lid_contactID
		          , $this->groep_groepID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Mentor`'
		          .' WHERE `lid_contactID` = %i'
		          .  ' AND `groep_groepID` = %i'
		                 , $veld
		          , $this->lid_contactID
		          , $this->groep_groepID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Mentor');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
