<?
abstract class IntroGroep_Generated
	extends Entiteit
{
	protected $groepID;					/**< \brief PRIMARY */
	protected $cluster;					/**< \brief LEGACY_NULL */
	protected $cluster_clusterID;		/**< \brief PRIMARY */
	protected $naam;
	/** Verzamelingen **/
	protected $lidStudieVerzameling;
	protected $mentorVerzameling;
	protected $tagVerzameling;
	/**
	 * @brief De constructor van de IntroGroep_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->groepID = NULL;
		$this->cluster = NULL;
		$this->cluster_clusterID = 0;
		$this->naam = '';
		$this->lidStudieVerzameling = NULL;
		$this->mentorVerzameling = NULL;
		$this->tagVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld groepID.
	 *
	 * @return int
	 * De waarde van het veld groepID.
	 */
	public function getGroepID()
	{
		return $this->groepID;
	}
	/**
	 * @brief Geef de waarde van het veld cluster.
	 *
	 * @return IntroCluster
	 * De waarde van het veld cluster.
	 */
	public function getCluster()
	{
		if(!isset($this->cluster)
		 && isset($this->cluster_clusterID)
		 ) {
			$this->cluster = IntroCluster::geef
					( $this->cluster_clusterID
					);
		}
		return $this->cluster;
	}
	/**
	 * @brief Stel de waarde van het veld cluster in.
	 *
	 * @param mixed $new_clusterID De nieuwe waarde.
	 *
	 * @return IntroGroep
	 * Dit IntroGroep-object.
	 */
	public function setCluster($new_clusterID)
	{
		unset($this->errors['Cluster']);
		if($new_clusterID instanceof IntroCluster
		) {
			if($this->cluster == $new_clusterID
			&& $this->cluster_clusterID == $this->cluster->getClusterID())
				return $this;
			$this->cluster = $new_clusterID;
			$this->cluster_clusterID
					= $this->cluster->getClusterID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_clusterID)
		) {
			if($this->cluster == NULL 
				&& $this->cluster_clusterID == (int)$new_clusterID)
				return $this;
			$this->cluster = NULL;
			$this->cluster_clusterID
					= (int)$new_clusterID;
			$this->gewijzigd();
			return $this;
		}
		if (isset($this->cluster)
		 || isset($this->cluster_clusterID))
		{
			$this->errors['Cluster'] = _('dit is een verplicht veld');
			$this->cluster = NULL;
			$this->cluster_clusterID = NULL;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld cluster geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld cluster geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCluster()
	{
		if (array_key_exists('Cluster', $this->errors))
			return $this->errors['Cluster'];
		$waarde1 = $this->getCluster();
		$waarde2 = $this->getClusterClusterID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld cluster_clusterID.
	 *
	 * @return int
	 * De waarde van het veld cluster_clusterID.
	 */
	public function getClusterClusterID()
	{
		if (is_null($this->cluster_clusterID) && isset($this->cluster)) {
			$this->cluster_clusterID = $this->cluster->getClusterID();
		}
		return $this->cluster_clusterID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return IntroGroep
	 * Dit IntroGroep-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de LidStudieVerzameling die hoort bij dit object.
	 */
	public function getLidStudieVerzameling()
	{
		if(!$this->lidStudieVerzameling instanceof LidStudieVerzameling)
			$this->lidStudieVerzameling = LidStudieVerzameling::fromGroep($this);
		return $this->lidStudieVerzameling;
	}
	/**
	 * @brief Returneert de MentorVerzameling die hoort bij dit object.
	 */
	public function getMentorVerzameling()
	{
		if(!$this->mentorVerzameling instanceof MentorVerzameling)
			$this->mentorVerzameling = MentorVerzameling::fromGroep($this);
		return $this->mentorVerzameling;
	}
	/**
	 * @brief Returneert de TagVerzameling die hoort bij dit object.
	 */
	public function getTagVerzameling()
	{
		if(!$this->tagVerzameling instanceof TagVerzameling)
			$this->tagVerzameling = TagVerzameling::fromIntrogroep($this);
		return $this->tagVerzameling;
	}
	abstract public function mentoren();
	abstract public function kindjes();
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return IntroGroep::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van IntroGroep.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return IntroGroep::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getGroepID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return IntroGroep|false
	 * Een IntroGroep-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$groepID = (int)$a[0];
		}
		else if(isset($a))
		{
			$groepID = (int)$a;
		}

		if(is_null($groepID))
			throw new BadMethodCallException();

		static::cache(array( array($groepID) ));
		return Entiteit::geefCache(array($groepID), 'IntroGroep');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'IntroGroep');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('IntroGroep::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `IntroGroep`.`groepID`'
		                 .     ', `IntroGroep`.`cluster_clusterID`'
		                 .     ', `IntroGroep`.`naam`'
		                 .     ', `IntroGroep`.`gewijzigdWanneer`'
		                 .     ', `IntroGroep`.`gewijzigdWie`'
		                 .' FROM `IntroGroep`'
		                 .' WHERE (`groepID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['groepID']);

			$obj = new IntroGroep();

			$obj->inDB = True;

			$obj->groepID  = (int) $row['groepID'];
			$obj->cluster_clusterID  = (int) $row['cluster_clusterID'];
			$obj->naam  = trim($row['naam']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'IntroGroep')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getClusterClusterID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->groepID =
			$WSW4DB->q('RETURNID INSERT INTO `IntroGroep`'
			          . ' (`cluster_clusterID`, `naam`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i)'
			          , $this->cluster_clusterID
			          , $this->naam
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'IntroGroep')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `IntroGroep`'
			          .' SET `cluster_clusterID` = %i'
			          .   ', `naam` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `groepID` = %i'
			          , $this->cluster_clusterID
			          , $this->naam
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->groepID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenIntroGroep
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenIntroGroep($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenIntroGroep($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Cluster';
			$velden[] = 'Naam';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Cluster';
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Cluster';
			$velden[] = 'Naam';
			break;
		case 'get':
			$velden[] = 'Cluster';
			$velden[] = 'Naam';
		case 'primary':
			$velden[] = 'cluster_clusterID';
			break;
		case 'verzamelingen':
			$velden[] = 'LidStudieVerzameling';
			$velden[] = 'MentorVerzameling';
			$velden[] = 'TagVerzameling';
			break;
		default:
			$velden[] = 'Cluster';
			$velden[] = 'Naam';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle LidStudie-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$LidStudieFromGroepVerz = LidStudieVerzameling::fromGroep($this);
		$returnValue = $LidStudieFromGroepVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LidStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Mentor-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$MentorFromGroepVerz = MentorVerzameling::fromGroep($this);
		$returnValue = $MentorFromGroepVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Mentor met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Tag-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$TagFromIntrogroepVerz = TagVerzameling::fromIntrogroep($this);
		$returnValue = $TagFromIntrogroepVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tag met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($LidStudieFromGroepVerz as $v)
		{
			$v->setGroep(NULL);
		}
		$LidStudieFromGroepVerz->opslaan();

		foreach($TagFromIntrogroepVerz as $v)
		{
			$v->setIntrogroep(NULL);
		}
		$TagFromIntrogroepVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $MentorFromGroepVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `IntroGroep`'
		          .' WHERE `groepID` = %i'
		          .' LIMIT 1'
		          , $this->groepID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->groepID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `IntroGroep`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `groepID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->groepID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van IntroGroep terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'groepid':
			return 'int';
		case 'cluster':
			return 'foreign';
		case 'cluster_clusterid':
			return 'int';
		case 'naam':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'groepID':
		case 'cluster_clusterID':
			$type = '%i';
			break;
		case 'naam':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `IntroGroep`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `groepID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->groepID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `IntroGroep`'
		          .' WHERE `groepID` = %i'
		                 , $veld
		          , $this->groepID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'IntroGroep');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle LidStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LidStudieFromGroepVerz = LidStudieVerzameling::fromGroep($this);
		$dependencies['LidStudie'] = $LidStudieFromGroepVerz;

		// Verzamel alle Mentor-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$MentorFromGroepVerz = MentorVerzameling::fromGroep($this);
		$dependencies['Mentor'] = $MentorFromGroepVerz;

		// Verzamel alle Tag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagFromIntrogroepVerz = TagVerzameling::fromIntrogroep($this);
		$dependencies['Tag'] = $TagFromIntrogroepVerz;

		return $dependencies;
	}
}
