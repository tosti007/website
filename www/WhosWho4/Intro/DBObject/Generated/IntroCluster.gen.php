<?
/**
 * @brief extends IntroGroepVerzameling ?
 */
abstract class IntroCluster_Generated
	extends Entiteit
{
	protected $clusterID;				/**< \brief PRIMARY */
	protected $jaar;
	protected $naam;
	protected $persoon;					/**< \brief supermentor NULL */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	/** Verzamelingen **/
	protected $introGroepVerzameling;
	/**
	 * @brief De constructor van de IntroCluster_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->clusterID = NULL;
		$this->jaar = 0;
		$this->naam = '';
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;
		$this->introGroepVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld clusterID.
	 *
	 * @return int
	 * De waarde van het veld clusterID.
	 */
	public function getClusterID()
	{
		return $this->clusterID;
	}
	/**
	 * @brief Geef de waarde van het veld jaar.
	 *
	 * @return int
	 * De waarde van het veld jaar.
	 */
	public function getJaar()
	{
		return $this->jaar;
	}
	/**
	 * @brief Stel de waarde van het veld jaar in.
	 *
	 * @param mixed $newJaar De nieuwe waarde.
	 *
	 * @return IntroCluster
	 * Dit IntroCluster-object.
	 */
	public function setJaar($newJaar)
	{
		unset($this->errors['Jaar']);
		if(!is_null($newJaar))
			$newJaar = (int)$newJaar;
		if($this->jaar === $newJaar)
			return $this;

		$this->jaar = $newJaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld jaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld jaar geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkJaar()
	{
		if (array_key_exists('Jaar', $this->errors))
			return $this->errors['Jaar'];
		$waarde = $this->getJaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return IntroCluster
	 * Dit IntroCluster-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Stel de waarde van het veld persoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return IntroCluster
	 * Dit IntroCluster-object.
	 */
	public function setPersoon($new_contactID)
	{
		unset($this->errors['Persoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->persoon == $new_contactID
			&& $this->persoon_contactID == $this->persoon->getContactID())
				return $this;
			$this->persoon = $new_contactID;
			$this->persoon_contactID
					= $this->persoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->persoon == NULL && $this->persoon_contactID == NULL)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->persoon == NULL 
				&& $this->persoon_contactID == (int)$new_contactID)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld persoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld persoon geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPersoon()
	{
		if (array_key_exists('Persoon', $this->errors))
			return $this->errors['Persoon'];
		$waarde1 = $this->getPersoon();
		$waarde2 = $this->getPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Returneert de IntroGroepVerzameling die hoort bij dit object.
	 */
	public function getIntroGroepVerzameling()
	{
		if(!$this->introGroepVerzameling instanceof IntroGroepVerzameling)
			$this->introGroepVerzameling = IntroGroepVerzameling::fromCluster($this);
		return $this->introGroepVerzameling;
	}
	abstract public function groepjes();
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return IntroCluster::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van IntroCluster.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return IntroCluster::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getClusterID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return IntroCluster|false
	 * Een IntroCluster-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$clusterID = (int)$a[0];
		}
		else if(isset($a))
		{
			$clusterID = (int)$a;
		}

		if(is_null($clusterID))
			throw new BadMethodCallException();

		static::cache(array( array($clusterID) ));
		return Entiteit::geefCache(array($clusterID), 'IntroCluster');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'IntroCluster');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('IntroCluster::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `IntroCluster`.`clusterID`'
		                 .     ', `IntroCluster`.`jaar`'
		                 .     ', `IntroCluster`.`naam`'
		                 .     ', `IntroCluster`.`persoon_contactID`'
		                 .     ', `IntroCluster`.`gewijzigdWanneer`'
		                 .     ', `IntroCluster`.`gewijzigdWie`'
		                 .' FROM `IntroCluster`'
		                 .' WHERE (`clusterID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['clusterID']);

			$obj = new IntroCluster();

			$obj->inDB = True;

			$obj->clusterID  = (int) $row['clusterID'];
			$obj->jaar  = (int) $row['jaar'];
			$obj->naam  = trim($row['naam']);
			$obj->persoon_contactID  = (is_null($row['persoon_contactID'])) ? null : (int) $row['persoon_contactID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'IntroCluster')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getPersoonContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->clusterID =
			$WSW4DB->q('RETURNID INSERT INTO `IntroCluster`'
			          . ' (`jaar`, `naam`, `persoon_contactID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %i, %s, %i)'
			          , $this->jaar
			          , $this->naam
			          , $this->persoon_contactID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'IntroCluster')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `IntroCluster`'
			          .' SET `jaar` = %i'
			          .   ', `naam` = %s'
			          .   ', `persoon_contactID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `clusterID` = %i'
			          , $this->jaar
			          , $this->naam
			          , $this->persoon_contactID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->clusterID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenIntroCluster
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenIntroCluster($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenIntroCluster($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Jaar';
			$velden[] = 'Naam';
			$velden[] = 'Persoon';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Jaar';
			$velden[] = 'Naam';
			$velden[] = 'Persoon';
			break;
		case 'get':
			$velden[] = 'Jaar';
			$velden[] = 'Naam';
			$velden[] = 'Persoon';
		case 'primary':
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'IntroGroepVerzameling';
			break;
		default:
			$velden[] = 'Jaar';
			$velden[] = 'Naam';
			$velden[] = 'Persoon';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle IntroGroep-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$IntroGroepFromClusterVerz = IntroGroepVerzameling::fromCluster($this);
		$returnValue = $IntroGroepFromClusterVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object IntroGroep met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $IntroGroepFromClusterVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `IntroCluster`'
		          .' WHERE `clusterID` = %i'
		          .' LIMIT 1'
		          , $this->clusterID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->clusterID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `IntroCluster`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `clusterID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->clusterID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van IntroCluster terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'clusterid':
			return 'int';
		case 'jaar':
			return 'int';
		case 'naam':
			return 'string';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'clusterID':
		case 'jaar':
		case 'persoon_contactID':
			$type = '%i';
			break;
		case 'naam':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `IntroCluster`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `clusterID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->clusterID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `IntroCluster`'
		          .' WHERE `clusterID` = %i'
		                 , $veld
		          , $this->clusterID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'IntroCluster');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle IntroGroep-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$IntroGroepFromClusterVerz = IntroGroepVerzameling::fromCluster($this);
		$dependencies['IntroGroep'] = $IntroGroepFromClusterVerz;

		return $dependencies;
	}
}
