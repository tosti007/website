<?
abstract class LidmaatschapsBetaling_Generated
	extends Entiteit
{
	protected $lid;						/**< \brief PRIMARY */
	protected $lid_contactID;			/**< \brief PRIMARY */
	protected $soort;					/**< \brief ENUM:BOEKVERKOOP/MACHTIGING/IDEAL */
	protected $rekeningnummer;			/**< \brief NULL */
	protected $rekeninghouder;			/**< \brief NULL */
	protected $ideal;					/**< \brief NULL */
	protected $ideal_transactieID;		/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de LidmaatschapsBetaling_Generated-klasse.
	 *
	 * @param mixed $a Lid (Lid OR Array(lid_contactID) OR lid_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Lid)
		{
			$this->lid = $a;
			$this->lid_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->lid = NULL;
			$this->lid_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->soort = 'BOEKVERKOOP';
		$this->rekeningnummer = NULL;
		$this->rekeninghouder = NULL;
		$this->ideal = NULL;
		$this->ideal_transactieID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Lid';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld lid.
	 *
	 * @return Lid
	 * De waarde van het veld lid.
	 */
	public function getLid()
	{
		if(!isset($this->lid)
		 && isset($this->lid_contactID)
		 ) {
			$this->lid = Lid::geef
					( $this->lid_contactID
					);
		}
		return $this->lid;
	}
	/**
	 * @brief Geef de waarde van het veld lid_contactID.
	 *
	 * @return int
	 * De waarde van het veld lid_contactID.
	 */
	public function getLidContactID()
	{
		if (is_null($this->lid_contactID) && isset($this->lid)) {
			$this->lid_contactID = $this->lid->getContactID();
		}
		return $this->lid_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('BOEKVERKOOP','MACHTIGING','IDEAL');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return LidmaatschapsBetaling
	 * Dit LidmaatschapsBetaling-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld rekeningnummer.
	 *
	 * @return string
	 * De waarde van het veld rekeningnummer.
	 */
	public function getRekeningnummer()
	{
		return $this->rekeningnummer;
	}
	/**
	 * @brief Stel de waarde van het veld rekeningnummer in.
	 *
	 * @param mixed $newRekeningnummer De nieuwe waarde.
	 *
	 * @return LidmaatschapsBetaling
	 * Dit LidmaatschapsBetaling-object.
	 */
	public function setRekeningnummer($newRekeningnummer)
	{
		unset($this->errors['Rekeningnummer']);
		if(!is_null($newRekeningnummer))
			$newRekeningnummer = trim($newRekeningnummer);
		if($newRekeningnummer === "")
			$newRekeningnummer = NULL;
		if($this->rekeningnummer === $newRekeningnummer)
			return $this;

		$this->rekeningnummer = $newRekeningnummer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rekeningnummer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rekeningnummer geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkRekeningnummer()
	{
		if (array_key_exists('Rekeningnummer', $this->errors))
			return $this->errors['Rekeningnummer'];
		$waarde = $this->getRekeningnummer();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld rekeninghouder.
	 *
	 * @return string
	 * De waarde van het veld rekeninghouder.
	 */
	public function getRekeninghouder()
	{
		return $this->rekeninghouder;
	}
	/**
	 * @brief Stel de waarde van het veld rekeninghouder in.
	 *
	 * @param mixed $newRekeninghouder De nieuwe waarde.
	 *
	 * @return LidmaatschapsBetaling
	 * Dit LidmaatschapsBetaling-object.
	 */
	public function setRekeninghouder($newRekeninghouder)
	{
		unset($this->errors['Rekeninghouder']);
		if(!is_null($newRekeninghouder))
			$newRekeninghouder = trim($newRekeninghouder);
		if($newRekeninghouder === "")
			$newRekeninghouder = NULL;
		if($this->rekeninghouder === $newRekeninghouder)
			return $this;

		$this->rekeninghouder = $newRekeninghouder;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rekeninghouder geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rekeninghouder geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkRekeninghouder()
	{
		if (array_key_exists('Rekeninghouder', $this->errors))
			return $this->errors['Rekeninghouder'];
		$waarde = $this->getRekeninghouder();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ideal.
	 *
	 * @return iDeal
	 * De waarde van het veld ideal.
	 */
	public function getIdeal()
	{
		if(!isset($this->ideal)
		 && isset($this->ideal_transactieID)
		 ) {
			$this->ideal = iDeal::geef
					( $this->ideal_transactieID
					);
		}
		return $this->ideal;
	}
	/**
	 * @brief Stel de waarde van het veld ideal in.
	 *
	 * @param mixed $new_transactieID De nieuwe waarde.
	 *
	 * @return LidmaatschapsBetaling
	 * Dit LidmaatschapsBetaling-object.
	 */
	public function setIdeal($new_transactieID)
	{
		unset($this->errors['Ideal']);
		if($new_transactieID instanceof iDeal
		) {
			if($this->ideal == $new_transactieID
			&& $this->ideal_transactieID == $this->ideal->getTransactieID())
				return $this;
			$this->ideal = $new_transactieID;
			$this->ideal_transactieID
					= $this->ideal->getTransactieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_transactieID) || $new_transactieID == 0)) {
			if($this->ideal == NULL && $this->ideal_transactieID == NULL)
				return $this;
			$this->ideal = NULL;
			$this->ideal_transactieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_transactieID)
		) {
			if($this->ideal == NULL 
				&& $this->ideal_transactieID == (int)$new_transactieID)
				return $this;
			$this->ideal = NULL;
			$this->ideal_transactieID
					= (int)$new_transactieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ideal geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ideal geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkIdeal()
	{
		if (array_key_exists('Ideal', $this->errors))
			return $this->errors['Ideal'];
		$waarde1 = $this->getIdeal();
		$waarde2 = $this->getIdealTransactieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ideal_transactieID.
	 *
	 * @return int
	 * De waarde van het veld ideal_transactieID.
	 */
	public function getIdealTransactieID()
	{
		if (is_null($this->ideal_transactieID) && isset($this->ideal)) {
			$this->ideal_transactieID = $this->ideal->getTransactieID();
		}
		return $this->ideal_transactieID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return LidmaatschapsBetaling::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van LidmaatschapsBetaling.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return LidmaatschapsBetaling::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getLidContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return LidmaatschapsBetaling|false
	 * Een LidmaatschapsBetaling-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$lid_contactID = (int)$a[0];
		}
		else if($a instanceof Lid)
		{
			$lid_contactID = $a->getContactID();
		}
		else if(isset($a))
		{
			$lid_contactID = (int)$a;
		}

		if(is_null($lid_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($lid_contactID) ));
		return Entiteit::geefCache(array($lid_contactID), 'LidmaatschapsBetaling');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'LidmaatschapsBetaling');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('LidmaatschapsBetaling::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `LidmaatschapsBetaling`.`lid_contactID`'
		                 .     ', `LidmaatschapsBetaling`.`soort`'
		                 .     ', `LidmaatschapsBetaling`.`rekeningnummer`'
		                 .     ', `LidmaatschapsBetaling`.`rekeninghouder`'
		                 .     ', `LidmaatschapsBetaling`.`ideal_transactieID`'
		                 .     ', `LidmaatschapsBetaling`.`gewijzigdWanneer`'
		                 .     ', `LidmaatschapsBetaling`.`gewijzigdWie`'
		                 .' FROM `LidmaatschapsBetaling`'
		                 .' WHERE (`lid_contactID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['lid_contactID']);

			$obj = new LidmaatschapsBetaling(array($row['lid_contactID']));

			$obj->inDB = True;

			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->rekeningnummer  = (is_null($row['rekeningnummer'])) ? null : trim($row['rekeningnummer']);
			$obj->rekeninghouder  = (is_null($row['rekeninghouder'])) ? null : trim($row['rekeninghouder']);
			$obj->ideal_transactieID  = (is_null($row['ideal_transactieID'])) ? null : (int) $row['ideal_transactieID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'LidmaatschapsBetaling')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getLid();
		if(!$rel->getInDB())
			throw new LogicException('foreign Lid is not in DB');
		$this->lid_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;
		$this->getIdealTransactieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `LidmaatschapsBetaling`'
			          . ' (`lid_contactID`, `soort`, `rekeningnummer`, `rekeninghouder`, `ideal_transactieID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %i, %s, %i)'
			          , $this->lid_contactID
			          , $this->soort
			          , $this->rekeningnummer
			          , $this->rekeninghouder
			          , $this->ideal_transactieID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'LidmaatschapsBetaling')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `LidmaatschapsBetaling`'
			          .' SET `soort` = %s'
			          .   ', `rekeningnummer` = %s'
			          .   ', `rekeninghouder` = %s'
			          .   ', `ideal_transactieID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lid_contactID` = %i'
			          , $this->soort
			          , $this->rekeningnummer
			          , $this->rekeninghouder
			          , $this->ideal_transactieID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lid_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenLidmaatschapsBetaling
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenLidmaatschapsBetaling($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenLidmaatschapsBetaling($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Soort';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Rekeninghouder';
			$velden[] = 'Ideal';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Soort';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Rekeninghouder';
			$velden[] = 'Ideal';
			break;
		case 'get':
			$velden[] = 'Soort';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Rekeninghouder';
			$velden[] = 'Ideal';
		case 'primary':
			$velden[] = 'lid_contactID';
			$velden[] = 'ideal_transactieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Soort';
			$velden[] = 'Rekeningnummer';
			$velden[] = 'Rekeninghouder';
			$velden[] = 'Ideal';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `LidmaatschapsBetaling`'
		          .' WHERE `lid_contactID` = %i'
		          .' LIMIT 1'
		          , $this->lid_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->lid = NULL;
		$this->lid_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `LidmaatschapsBetaling`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `lid_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->lid_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van LidmaatschapsBetaling terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'lid':
			return 'foreign';
		case 'lid_contactid':
			return 'int';
		case 'soort':
			return 'enum';
		case 'rekeningnummer':
			return 'string';
		case 'rekeninghouder':
			return 'string';
		case 'ideal':
			return 'foreign';
		case 'ideal_transactieid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'lid_contactID':
		case 'ideal_transactieID':
			$type = '%i';
			break;
		case 'soort':
		case 'rekeningnummer':
		case 'rekeninghouder':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `LidmaatschapsBetaling`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `lid_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->lid_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `LidmaatschapsBetaling`'
		          .' WHERE `lid_contactID` = %i'
		                 , $veld
		          , $this->lid_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'LidmaatschapsBetaling');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
