<?

/***
 * $Id$
 */
class IntroGroep
	extends IntroGroep_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	public function mentoren()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `lid_contactID`'
						.'FROM `Mentor`'
						.'WHERE `groep_groepID` = %i',$this->getGroepID());

		return LidVerzameling::verzamel($ids);
	}
	
	public function mentorVerzameling()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `lid_contactID`'
						.'FROM `Mentor`'
						.'WHERE `groep_groepID` = %i',$this->getGroepID());

		foreach ($ids as &$id)
			$id = array($id, $this->getGroepID());

		return MentorVerzameling::verzamel($ids);
	}

	public function kindjes()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `LidStudie`.`lid_contactID`'
						.'FROM `LidStudie`'
						.'WHERE `LidStudie`.`groep_groepID` = %i',$this->getGroepID());

		return LidVerzameling::verzamel($ids);
	}

	/**
	 *  Zoek de groepen waarvan de mentor een van onze kindjes is.
	 * @returns een IntroGroepVerzameling met alle gevonden groepen.
	 */
	public function getMentorGroepenVanMentoren()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `m1`.`groep_groepID` FROM `IntroGroep` AS `ig1` '
			. 'RIGHT JOIN `LidStudie` AS `ls1` ON `ls1`.`groep_groepID` = `ig1`.`groepID` '
			. 'RIGHT JOIN `Mentor` AS `m1` ON `m1`.`lid_contactID`=`ls1`.`lid_contactID` '
			. 'WHERE `ig1`.`groepID` = %i'
			, $this->geefID());

		return IntroGroepVerzameling::verzamel($ids);
	}
	
	public function url()
	{
		return '/Leden/Intro/Groep/' . $this->getGroepID();
	}

	public function fotoUrl()
	{
		return $this->url() . '/Fotos';
	}
	
	public function checkCluster()
	{
		if(!$this->getCluster())
			return _("dit is een verplicht veld");

		return false;
	}

	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('lid');
	}

	/**
	 * @brief Retourneert of de gebruiker de groep mag zien.
	 */
	public function magBekijken()
	{
		// Mentoren mogen hun groepjes sowieso zien.
		$pers = Persoon::getIngelogd();
		$mentoren = $this->mentoren();
		return $mentoren->bevat($pers) || static::magKlasseBekijken();
	}
	
	public function magWijzigen()
	{
		return hasAuth('intro');
	}
	
	public function magVerwijderen()
	{
		return hasAuth('intro');
	}

	public function getJaar()
	{
		return $this->getCluster()->getJaar();
	}

	public function makeFotoWebLink()
	{
		return new HtmlSpan(
			new HtmlAnchor('/Leden/Intro/?jaar='
				.$this->getJaar(),
				$this->getJaar()
			)
			." >> "
			.new HtmlAnchor(
				$this->getCluster()->url(),
				IntroClusterView::waardeNaam(
					$this->getCluster()
				)
			)
			." >> "
			.new HtmlAnchor(
				$this->fotoUrl(),
				IntroGroepView::waardeNaam($this)
			)
		);
	}	
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
