<?
class ActivatieCode
	extends ActivatieCode_Generated
{
	/** Verzamelingen **/
	/*** CONSTRUCTOR ***/
	/**
	 * @param a Lid ( Lid OR Array(lid_contactID) OR lid_contactID )
	 */
	public function __construct($a = NULL)
	{
		parent::__construct($a); // ActivatieCode_Generated
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Lid';
		return $volgorde;
	}
	
	/**
	 * @brief Geef het ActivatieCode-object dat hoort bij dit lid.
	 *
	 * @param lid Het lid voor wie we de activatiecode willen hebben.
	 *
	 * @returns Een ActivatieCode-object of NULL indien de code niet bestaat.
	 */
	static public function fromLid($lid) {
		return ActivatieCodeQuery::table()->whereProp('lid', $lid)->geef();
	}

	/**
	 * @brief Geef het Lid-object dat hoort bij deze code.
	 *
	 * @param code De activatiecode bij het lid, string of ActivatieCode.
	 *
	 * @returns Een Lid-object of NULL indien de code niet bestaat.
	 */
	static public function toLid($code) {
		if ($code instanceof ActivatieCode) {
			return $code->getLid();
		} elseif (is_string($code)) {
			$object = ActivatieCodeQuery::table()
				->whereProp('code', $code)
				->geef();

			if (is_null($object)) {
				return NULL;
			} else {
				return $object->getLid();
			}
		} else {
			user_error("Je kan niet zomaar '$code' als lid gaan behandelen joh!", E_USER_ERROR);
		}
	}

	/**
	 * @brief Maak een nieuwe ActivatieCode voor het gegeven lid aan.
	 *
	 * De code wordt willekeurig gegenereerd door deze functie.
	 * Daarna checken we dat die uniek is en zo niet proberen we opnieuw.
	 * Na een paar gefaalde generatiepogingen geven we op met een error.
	 */
	static public function genereer(Lid $lid) {
		$pogingen = 0;
		do {
			if ($pogingen > 5) {
				user_error("Tegen alle waarschijnlijkheid in, lukt het genereren van unieke strings niet.", E_USER_ERROR);
			}
			$nieuweCode = strtolower(randstr(8));
			$pogingen++;
		} while (!is_null(static::toLid($nieuweCode)));

		$code = new ActivatieCode($lid);
		$code->setCode($nieuweCode);
		return $code;
	}

	public function magVerwijderen() {
		return hasAuth('bestuur');
	}
}
