<?

/***
 * $Id$
 */
class IntroDeelnemerVerzameling
	extends IntroDeelnemerVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	// Geeft de lid-objecten terug van alle kindjes zonder groep via lidstudie-objecten
	static public function kindjesZonderGroep($jaar = null)
	{
		global $WSW4DB;

		if($jaar == null)
			$jaar = date('Y');

		$query = 'COLUMN SELECT DISTINCT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_StudieID` = `Studie`.`studieID` WHERE `datumBegin`="'.$jaar.'-09-01" AND `LidStudie`.`groep_groepID` IS NULL AND `fase`="BA"';
		$params = array($query);

		$ids = call_user_func_array(array($WSW4DB, 'q'), $params);
		return LidVerzameling::verzamel($ids);
	}

	//TODO: waarom worden hier alleen bools in de vorm van een string geaccepteerd!?
	static public function alleBijnaLeden($kamp = 'void',$vega = 'void',$aanwezig = 'void',$allergie = 'void' ,$betaald = "ALLES",$opmerk= 'void')
	{
		global $WSW4DB;
		$query = 'COLUMN SELECT `lid_contactID` FROM `IntroDeelnemer`';
		$params = array($query);
				
		$where = array();
				
		if($kamp != 'void')
				{
					$where[] = '`kamp` = %i';
					$params[] = (is_string($kamp)) ? ($kamp === 'true') : $kamp;
				}
				
		if($vega != 'void')
				{
				$where[] = '`vega` = %i';
				$params[] = (is_string($vega)) ? ($vega === 'true') : $vega;	
				}
				
				
		if($aanwezig != 'void')
				{
				$where[] = '`aanwezig` = %i';
				$params[] = (is_string($aanwezig)) ? ($aanwezig === 'true') : $aanwezig;	
				}
				
		if($allergie != 'void')
				{
				$where[] = '`allergie` = %i';
				$params[] = (is_string($allergie)) ? ($allergie === 'true') : $allergie;	
				}
				
		if($betaald != 'ALLES')
				{
				$where[] = '`betaald` = %s';
				$params[] = $betaald;
				}
		if($opmerk == 'true')
				{
				$where[] = '`introOpmerkingen` != \'\'';
				}
	   	if($opmerk == 'false')
				{
				$where[] = '`introOpmerkingen` = \'\'';
				}
		
		$params[0] .= (count($where) > 0) ? ' WHERE ' . implode(' AND ', $where) : '';
		$ids = call_user_func_array(array($WSW4DB, 'q'), $params);
		
		return self::verzamel($ids);	
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
