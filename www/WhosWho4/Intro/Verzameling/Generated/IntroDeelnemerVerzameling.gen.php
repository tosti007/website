<?
abstract class IntroDeelnemerVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de IntroDeelnemerVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze IntroDeelnemerVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze IntroDeelnemerVerzameling.
	 */
	public function toLidVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLidContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een IntroDeelnemerVerzameling van Lid.
	 *
	 * @return IntroDeelnemerVerzameling
	 * Een IntroDeelnemerVerzameling die elementen bevat die bij de Lid hoort.
	 */
	static public function fromLid($lid)
	{
		if(!isset($lid))
			return new IntroDeelnemerVerzameling();

		return IntroDeelnemerQuery::table()
			->whereProp('Lid', $lid)
			->verzamel();
	}
}
