<?
abstract class IntroClusterVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de IntroClusterVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze IntroClusterVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze IntroClusterVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getPersoonContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een IntroClusterVerzameling van Persoon.
	 *
	 * @return IntroClusterVerzameling
	 * Een IntroClusterVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new IntroClusterVerzameling();

		return IntroClusterQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
