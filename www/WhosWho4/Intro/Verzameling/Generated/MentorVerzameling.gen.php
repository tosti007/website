<?
abstract class MentorVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de MentorVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze MentorVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze MentorVerzameling.
	 */
	public function toLidVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLidContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze MentorVerzameling een IntroGroepVerzameling.
	 *
	 * @return IntroGroepVerzameling
	 * Een IntroGroepVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze MentorVerzameling.
	 */
	public function toGroepVerzameling()
	{
		if($this->aantal() == 0)
			return new IntroGroepVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getGroepGroepID()
			                      );
		}
		$this->positie = $origPositie;
		return IntroGroepVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een MentorVerzameling van Lid.
	 *
	 * @return MentorVerzameling
	 * Een MentorVerzameling die elementen bevat die bij de Lid hoort.
	 */
	static public function fromLid($lid)
	{
		if(!isset($lid))
			return new MentorVerzameling();

		return MentorQuery::table()
			->whereProp('Lid', $lid)
			->verzamel();
	}
	/**
	 * @brief Maak een MentorVerzameling van Groep.
	 *
	 * @return MentorVerzameling
	 * Een MentorVerzameling die elementen bevat die bij de Groep hoort.
	 */
	static public function fromGroep($groep)
	{
		if(!isset($groep))
			return new MentorVerzameling();

		return MentorQuery::table()
			->whereProp('Groep', $groep)
			->verzamel();
	}
}
