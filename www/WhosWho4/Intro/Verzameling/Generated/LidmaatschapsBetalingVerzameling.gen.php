<?
abstract class LidmaatschapsBetalingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de LidmaatschapsBetalingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze LidmaatschapsBetalingVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze LidmaatschapsBetalingVerzameling.
	 */
	public function toLidVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLidContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze LidmaatschapsBetalingVerzameling een iDealVerzameling.
	 *
	 * @return iDealVerzameling
	 * Een iDealVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze LidmaatschapsBetalingVerzameling.
	 */
	public function toIdealVerzameling()
	{
		if($this->aantal() == 0)
			return new iDealVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getIdealTransactieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getIdealTransactieID()
			                      );
		}
		$this->positie = $origPositie;
		return iDealVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een LidmaatschapsBetalingVerzameling van Lid.
	 *
	 * @return LidmaatschapsBetalingVerzameling
	 * Een LidmaatschapsBetalingVerzameling die elementen bevat die bij de Lid hoort.
	 */
	static public function fromLid($lid)
	{
		if(!isset($lid))
			return new LidmaatschapsBetalingVerzameling();

		return LidmaatschapsBetalingQuery::table()
			->whereProp('Lid', $lid)
			->verzamel();
	}
	/**
	 * @brief Maak een LidmaatschapsBetalingVerzameling van Ideal.
	 *
	 * @return LidmaatschapsBetalingVerzameling
	 * Een LidmaatschapsBetalingVerzameling die elementen bevat die bij de Ideal hoort.
	 */
	static public function fromIdeal($ideal)
	{
		if(!isset($ideal))
			return new LidmaatschapsBetalingVerzameling();

		return LidmaatschapsBetalingQuery::table()
			->whereProp('Ideal', $ideal)
			->verzamel();
	}
}
