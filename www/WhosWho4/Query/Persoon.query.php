<?
class PersoonQuery
	extends PersoonQuery_Generated
{
	static public function filterMagBekijken()
	{
		$return = array('joins' => array(), 'wheres' => false);

		if(hasAuth('spocie'))
			$return['joins'][] = 'ContactPersoon';

		$where = function($q) {
			$q->whereProp('ContactID', Persoon::getIngelogd()->geefID());

			if(hasAuth('spocie'))
				$q->orWhere(function($qa) {
					$qa->whereProp('ContactPersoon.Persoon', Persoon::getIngelogd())
						->whereProp('Type', 'bedrijfscontact');
				});
		};

		return $return;
	}
}
