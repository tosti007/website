<?
class DonateurQuery
	extends DonateurQuery_Generated
{
	/**
	 * @brief Geef wat je in de `->where` moet stoppen om huidige donateurs te krijgen.
	 *
	 * @return Het argument voor de where die selecteert op huidige donateurs.
	 */
	public static function whereHuidig()
	{
		return function($q) {
			$q->whereNull('jaarEind')
				->orWhereProp('jaarEind', '>', colJaar());
		};
	}
}
