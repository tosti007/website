<?
abstract class ContactQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de ContactQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een ContactQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw ContactQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return ContactQuery
	 * Het ContactQuery-object waarvan meteen de db-table van het Contact-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new ContactQuery();

		$query->tables('Contact');

		return $query;
	}

	/**
	 * @brief Maakt een ContactVerzameling aan van de objecten die geselecteerd worden
	 * door de ContactQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ContactVerzameling
	 * Een ContactVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Contact', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Contact-iterator aan van de objecten die geselecteerd worden
	 * door de ContactQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ContactVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Contact', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Contact die geslecteeerd wordt door de ContactQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ContactVerzameling
	 * Een ContactVerzameling verkregen door de query.
	 */
	public function geef($object = 'Contact')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Contact.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'overerving',
			'contactID',
			'email',
			'homepage',
			'vakidOpsturen',
			'aes2rootsOpsturen',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Contact';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Contact een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Contact een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Contact'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Contact.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Contact.contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'contactid':
			$type = 'int';
			$field = 'contactID';
			break;
		case 'email':
			$type = 'string';
			$field = 'email';
			break;
		case 'homepage':
			$type = 'string';
			$field = 'homepage';
			break;
		case 'vakidopsturen':
			$type = 'int';
			$field = 'vakidOpsturen';
			break;
		case 'aes2rootsopsturen':
			$type = 'int';
			$field = 'aes2rootsOpsturen';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Contact.'.$v;
			}
		} else {
			$field = 'Contact.'.$field;
		}

		return array($field, $value, $type);
	}

}
