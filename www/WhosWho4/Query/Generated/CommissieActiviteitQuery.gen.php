<?
abstract class CommissieActiviteitQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de CommissieActiviteitQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een CommissieActiviteitQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw
	 * CommissieActiviteitQuery-object aan om er zeker van te zijn dat we alle
	 * eigenschappen daarvan ook meenemen.
	 *
	 * @return CommissieActiviteitQuery
	 * Het CommissieActiviteitQuery-object waarvan meteen de db-table van het
	 * CommissieActiviteit-object als table geset is.
	 */
	static public function table()
	{
		$query = new CommissieActiviteitQuery();

		$query->tables('CommissieActiviteit');

		return $query;
	}

	/**
	 * @brief Maakt een CommissieActiviteitVerzameling aan van de objecten die
	 * geselecteerd worden door de CommissieActiviteitQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return CommissieActiviteitVerzameling
	 * Een CommissieActiviteitVerzameling verkregen door de query
	 */
	public function verzamel($object = 'CommissieActiviteit', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een CommissieActiviteit-iterator aan van de objecten die
	 * geselecteerd worden door de CommissieActiviteitQuery uit te voeren met een SQL
	 * cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een CommissieActiviteitVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'CommissieActiviteit', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een CommissieActiviteit die geslecteeerd wordt door de
	 * CommissieActiviteitQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return CommissieActiviteitVerzameling
	 * Een CommissieActiviteitVerzameling verkregen door de query.
	 */
	public function geef($object = 'CommissieActiviteit')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van CommissieActiviteit.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'commissie_commissieID',
			'activiteit_activiteitID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'CommissieActiviteit';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als CommissieActiviteit
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan CommissieActiviteit een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('CommissieActiviteit'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			),
			'Activiteit' => array(
				'activiteit_activiteitID' => 'activiteitID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van CommissieActiviteit.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'CommissieActiviteit.commissie_commissieID',
			'CommissieActiviteit.activiteit_activiteitID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling))
				user_error('Value moet van type Commissie zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else $value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'activiteit':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Activiteit; })))
					user_error('Alle waarden in de array moeten van type Activiteit zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Activiteit) && !($value instanceof ActiviteitVerzameling))
				user_error('Value moet van type Activiteit zijn', E_USER_ERROR);
			$field = 'activiteit_activiteitID';
			if(is_array($value) || $value instanceof ActiviteitVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getActiviteitID();
				$value = $new_value;
			}
			else $value = $value->getActiviteitID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'CommissieActiviteit.'.$v;
			}
		} else {
			$field = 'CommissieActiviteit.'.$field;
		}

		return array($field, $value, $type);
	}

}
