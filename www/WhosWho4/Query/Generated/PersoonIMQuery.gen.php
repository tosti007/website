<?
abstract class PersoonIMQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de PersoonIMQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een PersoonIMQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw PersoonIMQuery-object aan om er zeker van
	 * te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return PersoonIMQuery
	 * Het PersoonIMQuery-object waarvan meteen de db-table van het PersoonIM-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new PersoonIMQuery();

		$query->tables('PersoonIM');

		return $query;
	}

	/**
	 * @brief Maakt een PersoonIMVerzameling aan van de objecten die geselecteerd
	 * worden door de PersoonIMQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return PersoonIMVerzameling
	 * Een PersoonIMVerzameling verkregen door de query
	 */
	public function verzamel($object = 'PersoonIM', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een PersoonIM-iterator aan van de objecten die geselecteerd worden
	 * door de PersoonIMQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een PersoonIMVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'PersoonIM', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een PersoonIM die geslecteeerd wordt door de PersoonIMQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return PersoonIMVerzameling
	 * Een PersoonIMVerzameling verkregen door de query.
	 */
	public function geef($object = 'PersoonIM')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van PersoonIM.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'persoon_contactID',
			'soort',
			'accountName',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'PersoonIM';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als PersoonIM een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan PersoonIM een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('PersoonIM'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van PersoonIM.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'PersoonIM.persoon_contactID',
			'PersoonIM.soort'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, PersoonIM::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, PersoonIM::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'accountname':
			$type = 'string';
			$field = 'accountName';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'PersoonIM.'.$v;
			}
		} else {
			$field = 'PersoonIM.'.$field;
		}

		return array($field, $value, $type);
	}

}
