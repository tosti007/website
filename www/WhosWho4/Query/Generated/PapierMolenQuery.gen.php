<?
abstract class PapierMolenQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de PapierMolenQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een PapierMolenQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw PapierMolenQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return PapierMolenQuery
	 * Het PapierMolenQuery-object waarvan meteen de db-table van het
	 * PapierMolen-object als table geset is.
	 */
	static public function table()
	{
		$query = new PapierMolenQuery();

		$query->tables('PapierMolen');

		return $query;
	}

	/**
	 * @brief Maakt een PapierMolenVerzameling aan van de objecten die geselecteerd
	 * worden door de PapierMolenQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return PapierMolenVerzameling
	 * Een PapierMolenVerzameling verkregen door de query
	 */
	public function verzamel($object = 'PapierMolen', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een PapierMolen-iterator aan van de objecten die geselecteerd
	 * worden door de PapierMolenQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een PapierMolenVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'PapierMolen', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een PapierMolen die geslecteeerd wordt door de PapierMolenQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return PapierMolenVerzameling
	 * Een PapierMolenVerzameling verkregen door de query.
	 */
	public function geef($object = 'PapierMolen')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van PapierMolen.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'papierMolenID',
			'geplaatst',
			'lid_contactID',
			'ean',
			'auteur',
			'titel',
			'druk',
			'prijs',
			'opmerking',
			'studie',
			'verloopdatum',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'PapierMolen';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als PapierMolen een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan PapierMolen een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('PapierMolen'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Lid' => array(
				'lid_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van PapierMolen.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'PapierMolen.papierMolenID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'papiermolenid':
			$type = 'int';
			$field = 'papierMolenID';
			break;
		case 'geplaatst':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'geplaatst';
			break;
		case 'lid':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling))
				user_error('Value moet van type Lid zijn', E_USER_ERROR);
			$field = 'lid_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'ean':
			$type = 'int';
			$field = 'ean';
			break;
		case 'auteur':
			$type = 'string';
			$field = 'auteur';
			break;
		case 'titel':
			$type = 'string';
			$field = 'titel';
			break;
		case 'druk':
			$type = 'string';
			$field = 'druk';
			break;
		case 'prijs':
			$type = 'float';
			$field = 'prijs';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'opmerking';
			break;
		case 'studie':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, PapierMolen::enumsStudie()))
						user_error($value . ' is niet een geldige waarde voor studie', E_USER_ERROR);
				}
			}
			else if(!in_array($value, PapierMolen::enumsStudie()))
				user_error($value . ' is niet een geldige waarde voor studie', E_USER_ERROR);
			$type = 'string';
			$field = 'studie';
			break;
		case 'verloopdatum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'verloopdatum';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'PapierMolen.'.$v;
			}
		} else {
			$field = 'PapierMolen.'.$field;
		}

		return array($field, $value, $type);
	}

}
