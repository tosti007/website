<?
abstract class RegisterQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de RegisterQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een RegisterQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw RegisterQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return RegisterQuery
	 * Het RegisterQuery-object waarvan meteen de db-table van het Register-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new RegisterQuery();

		$query->tables('Register');

		return $query;
	}

	/**
	 * @brief Maakt een RegisterVerzameling aan van de objecten die geselecteerd worden
	 * door de RegisterQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return RegisterVerzameling
	 * Een RegisterVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Register', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Register-iterator aan van de objecten die geselecteerd worden
	 * door de RegisterQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een RegisterVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Register', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Register die geslecteeerd wordt door de RegisterQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return RegisterVerzameling
	 * Een RegisterVerzameling verkregen door de query.
	 */
	public function geef($object = 'Register')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Register.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'label',
			'beschrijving',
			'waarde',
			'type',
			'auth',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Register';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Register een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Register een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Register'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Register.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Register.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'label':
			$type = 'string';
			$field = 'label';
			break;
		case 'beschrijving':
			$type = 'string';
			$field = 'beschrijving';
			break;
		case 'waarde':
			$type = 'string';
			$field = 'waarde';
			break;
		case 'type':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Register::enumsType()))
						user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Register::enumsType()))
				user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
			$type = 'string';
			$field = 'type';
			break;
		case 'auth':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Register::enumsAuth()))
						user_error($value . ' is niet een geldige waarde voor auth', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Register::enumsAuth()))
				user_error($value . ' is niet een geldige waarde voor auth', E_USER_ERROR);
			$type = 'string';
			$field = 'auth';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Register.'.$v;
			}
		} else {
			$field = 'Register.'.$field;
		}

		return array($field, $value, $type);
	}

}
