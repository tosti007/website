<?
abstract class LidQuery_Generated
	extends PersoonQuery
{
	/**
	 * @brief De constructor van de LidQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // PersoonQuery
	}
	/**
	 * @brief Maakt een LidQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw LidQuery-object aan om er zeker van te zijn dat
	 * we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return LidQuery
	 * Het LidQuery-object waarvan meteen de db-table van het Lid-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new LidQuery();

		$query->tables('Lid');

		return $query;
	}

	/**
	 * @brief Maakt een LidVerzameling aan van de objecten die geselecteerd worden door
	 * de LidQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Lid', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Lid-iterator aan van de objecten die geselecteerd worden door
	 * de LidQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een LidVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Lid', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Lid die geslecteeerd wordt door de LidQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling verkregen door de query.
	 */
	public function geef($object = 'Lid')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Lid.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'contactID',
			'lidToestand',
			'lidMaster',
			'lidExchange',
			'lidVan',
			'lidTot',
			'studentnr',
			'opzoekbaar',
			'inAlmanak',
			'planetRSS'
		);

		if(in_array($field, $varArray))
			return 'Lid';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Lid een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Persoon' => array(
				'Persoon.contactID' => 'Lid.contactID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Lid een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Lid'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Voornaamwoord' => array(
				'voornaamwoord_woordID' => 'woordID'
			),
			'Persoon' => array(
				'Persoon.contactID' => 'Lid.contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Lid.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Lid.contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'lidtoestand':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Lid::enumsLidToestand()))
						user_error($value . ' is niet een geldige waarde voor lidToestand', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Lid::enumsLidToestand()))
				user_error($value . ' is niet een geldige waarde voor lidToestand', E_USER_ERROR);
			$type = 'string';
			$field = 'lidToestand';
			break;
		case 'lidmaster':
			$type = 'int';
			$field = 'lidMaster';
			break;
		case 'lidexchange':
			$type = 'int';
			$field = 'lidExchange';
			break;
		case 'lidvan':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'lidVan';
			break;
		case 'lidtot':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'lidTot';
			break;
		case 'studentnr':
			$type = 'string';
			$field = 'studentnr';
			break;
		case 'opzoekbaar':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Lid::enumsOpzoekbaar()))
						user_error($value . ' is niet een geldige waarde voor opzoekbaar', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Lid::enumsOpzoekbaar()))
				user_error($value . ' is niet een geldige waarde voor opzoekbaar', E_USER_ERROR);
			$type = 'string';
			$field = 'opzoekbaar';
			break;
		case 'inalmanak':
			$type = 'int';
			$field = 'inAlmanak';
			break;
		case 'planetrss':
			$type = 'string';
			$field = 'planetRSS';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Lid.'.$v;
			}
		} else {
			$field = 'Lid.'.$field;
		}

		return array($field, $value, $type);
	}

}
