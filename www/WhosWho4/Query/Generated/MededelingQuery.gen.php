<?
abstract class MededelingQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de MededelingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een MededelingQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw MededelingQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return MededelingQuery
	 * Het MededelingQuery-object waarvan meteen de db-table van het Mededeling-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new MededelingQuery();

		$query->tables('Mededeling');

		return $query;
	}

	/**
	 * @brief Maakt een MededelingVerzameling aan van de objecten die geselecteerd
	 * worden door de MededelingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return MededelingVerzameling
	 * Een MededelingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Mededeling', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Mededeling-iterator aan van de objecten die geselecteerd worden
	 * door de MededelingQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een MededelingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Mededeling', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Mededeling die geslecteeerd wordt door de MededelingQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return MededelingVerzameling
	 * Een MededelingVerzameling verkregen door de query.
	 */
	public function geef($object = 'Mededeling')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Mededeling.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'mededelingID',
			'commissie_commissieID',
			'doelgroep',
			'prioriteit',
			'datumBegin',
			'datumEind',
			'url',
			'omschrijving_NL',
			'omschrijving_EN',
			'mededeling_NL',
			'mededeling_EN',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Mededeling';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Mededeling een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Mededeling een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Mededeling'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Mededeling.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Mededeling.mededelingID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'mededelingid':
			$type = 'int';
			$field = 'mededelingID';
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling) && !is_null($value))
				user_error('Value moet van type Commissie of NULL zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'doelgroep':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Mededeling::enumsDoelgroep()))
						user_error($value . ' is niet een geldige waarde voor doelgroep', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Mededeling::enumsDoelgroep()))
				user_error($value . ' is niet een geldige waarde voor doelgroep', E_USER_ERROR);
			$type = 'string';
			$field = 'doelgroep';
			break;
		case 'prioriteit':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Mededeling::enumsPrioriteit()))
						user_error($value . ' is niet een geldige waarde voor prioriteit', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Mededeling::enumsPrioriteit()))
				user_error($value . ' is niet een geldige waarde voor prioriteit', E_USER_ERROR);
			$type = 'string';
			$field = 'prioriteit';
			break;
		case 'datumbegin':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumBegin';
			break;
		case 'datumeind':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumEind';
			break;
		case 'url':
			$type = 'string';
			$field = 'url';
			break;
		case 'omschrijving_nl':
			$type = 'string';
			$field = 'omschrijving_NL';
			break;
		case 'omschrijving_en':
			$type = 'string';
			$field = 'omschrijving_EN';
			break;
		case 'mededeling_nl':
			$type = 'string';
			$field = 'mededeling_NL';
			break;
		case 'mededeling_en':
			$type = 'string';
			$field = 'mededeling_EN';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Mededeling.'.$v;
			}
		} else {
			$field = 'Mededeling.'.$field;
		}

		return array($field, $value, $type);
	}

}
