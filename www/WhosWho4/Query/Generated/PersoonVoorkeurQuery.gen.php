<?
abstract class PersoonVoorkeurQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de PersoonVoorkeurQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een PersoonVoorkeurQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw PersoonVoorkeurQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return PersoonVoorkeurQuery
	 * Het PersoonVoorkeurQuery-object waarvan meteen de db-table van het
	 * PersoonVoorkeur-object als table geset is.
	 */
	static public function table()
	{
		$query = new PersoonVoorkeurQuery();

		$query->tables('PersoonVoorkeur');

		return $query;
	}

	/**
	 * @brief Maakt een PersoonVoorkeurVerzameling aan van de objecten die geselecteerd
	 * worden door de PersoonVoorkeurQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return PersoonVoorkeurVerzameling
	 * Een PersoonVoorkeurVerzameling verkregen door de query
	 */
	public function verzamel($object = 'PersoonVoorkeur', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een PersoonVoorkeur-iterator aan van de objecten die geselecteerd
	 * worden door de PersoonVoorkeurQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een PersoonVoorkeurVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'PersoonVoorkeur', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een PersoonVoorkeur die geslecteeerd wordt door de
	 * PersoonVoorkeurQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return PersoonVoorkeurVerzameling
	 * Een PersoonVoorkeurVerzameling verkregen door de query.
	 */
	public function geef($object = 'PersoonVoorkeur')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van PersoonVoorkeur.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'persoon_contactID',
			'taal',
			'bugSortering',
			'favoBugCategorie_bugCategorieID',
			'tourAfgemaakt',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'PersoonVoorkeur';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als PersoonVoorkeur een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan PersoonVoorkeur een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('PersoonVoorkeur'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			),
			'BugCategorie' => array(
				'favoBugCategorie_bugCategorieID' => 'bugCategorieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van PersoonVoorkeur.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'PersoonVoorkeur.persoon_contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'taal':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, PersoonVoorkeur::enumsTaal()))
						user_error($value . ' is niet een geldige waarde voor taal', E_USER_ERROR);
				}
			}
			else if(!in_array($value, PersoonVoorkeur::enumsTaal()))
				user_error($value . ' is niet een geldige waarde voor taal', E_USER_ERROR);
			$type = 'string';
			$field = 'taal';
			break;
		case 'bugsortering':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, PersoonVoorkeur::enumsBugSortering()))
						user_error($value . ' is niet een geldige waarde voor bugSortering', E_USER_ERROR);
				}
			}
			else if(!in_array($value, PersoonVoorkeur::enumsBugSortering()))
				user_error($value . ' is niet een geldige waarde voor bugSortering', E_USER_ERROR);
			$type = 'string';
			$field = 'bugSortering';
			break;
		case 'favobugcategorie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof BugCategorie; })))
					user_error('Alle waarden in de array moeten van type BugCategorie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof BugCategorie) && !($value instanceof BugCategorieVerzameling) && !is_null($value))
				user_error('Value moet van type BugCategorie of NULL zijn', E_USER_ERROR);
			$field = 'favoBugCategorie_bugCategorieID';
			if(is_array($value) || $value instanceof BugCategorieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getBugCategorieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getBugCategorieID();
			$type = 'int';
			break;
		case 'tourafgemaakt':
			$type = 'int';
			$field = 'tourAfgemaakt';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'PersoonVoorkeur.'.$v;
			}
		} else {
			$field = 'PersoonVoorkeur.'.$field;
		}

		return array($field, $value, $type);
	}

}
