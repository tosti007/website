<?
abstract class PersoonQuery_Generated
	extends ContactQuery
{
	/**
	 * @brief De constructor van de PersoonQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ContactQuery
	}
	/**
	 * @brief Maakt een PersoonQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw PersoonQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return PersoonQuery
	 * Het PersoonQuery-object waarvan meteen de db-table van het Persoon-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new PersoonQuery();

		$query->tables('Persoon');

		return $query;
	}

	/**
	 * @brief Maakt een PersoonVerzameling aan van de objecten die geselecteerd worden
	 * door de PersoonQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Persoon', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Persoon-iterator aan van de objecten die geselecteerd worden
	 * door de PersoonQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een PersoonVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Persoon', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Persoon die geslecteeerd wordt door de PersoonQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling verkregen door de query.
	 */
	public function geef($object = 'Persoon')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Persoon.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'contactID',
			'voornaam',
			'bijnaam',
			'voorletters',
			'geboortenamen',
			'tussenvoegsels',
			'achternaam',
			'titelsPrefix',
			'titelsPostfix',
			'geslacht',
			'datumGeboorte',
			'overleden',
			'GPGkey',
			'Rekeningnummer',
			'opmerkingen',
			'voornaamwoord_woordID',
			'voornaamwoordZichtbaar'
		);

		if(in_array($field, $varArray))
			return 'Persoon';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Persoon een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Contact' => array(
				'Contact.contactID' => 'Persoon.contactID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Persoon een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Persoon'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Voornaamwoord' => array(
				'voornaamwoord_woordID' => 'woordID'
			),
			'Contact' => array(
				'Contact.contactID' => 'Persoon.contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Persoon.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Persoon.contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'voornaam':
			$type = 'string';
			$field = 'voornaam';
			break;
		case 'bijnaam':
			$type = 'string';
			$field = 'bijnaam';
			break;
		case 'voorletters':
			$type = 'string';
			$field = 'voorletters';
			break;
		case 'geboortenamen':
			$type = 'string';
			$field = 'geboortenamen';
			break;
		case 'tussenvoegsels':
			$type = 'string';
			$field = 'tussenvoegsels';
			break;
		case 'achternaam':
			$type = 'string';
			$field = 'achternaam';
			break;
		case 'titelsprefix':
			$type = 'string';
			$field = 'titelsPrefix';
			break;
		case 'titelspostfix':
			$type = 'string';
			$field = 'titelsPostfix';
			break;
		case 'geslacht':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Persoon::enumsGeslacht()))
						user_error($value . ' is niet een geldige waarde voor geslacht', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Persoon::enumsGeslacht()) && !is_null($value))
				user_error($value . ' is niet een geldige waarde voor geslacht', E_USER_ERROR);
			$type = 'string';
			$field = 'geslacht';
			break;
		case 'datumgeboorte':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumGeboorte';
			break;
		case 'overleden':
			$type = 'int';
			$field = 'overleden';
			break;
		case 'gpgkey':
			$type = 'string';
			$field = 'GPGkey';
			break;
		case 'rekeningnummer':
			$type = 'string';
			$field = 'Rekeningnummer';
			break;
		case 'opmerkingen':
			$type = 'string';
			$field = 'opmerkingen';
			break;
		case 'voornaamwoord':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Voornaamwoord; })))
					user_error('Alle waarden in de array moeten van type Voornaamwoord zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Voornaamwoord) && !($value instanceof VoornaamwoordVerzameling))
				user_error('Value moet van type Voornaamwoord zijn', E_USER_ERROR);
			$field = 'voornaamwoord_woordID';
			if(is_array($value) || $value instanceof VoornaamwoordVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getWoordID();
				$value = $new_value;
			}
			else $value = $value->getWoordID();
			$type = 'int';
			break;
		case 'voornaamwoordzichtbaar':
			$type = 'int';
			$field = 'voornaamwoordZichtbaar';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Persoon.'.$v;
			}
		} else {
			$field = 'Persoon.'.$field;
		}

		return array($field, $value, $type);
	}

}
