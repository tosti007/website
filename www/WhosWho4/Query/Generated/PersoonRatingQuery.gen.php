<?
abstract class PersoonRatingQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de PersoonRatingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een PersoonRatingQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw PersoonRatingQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return PersoonRatingQuery
	 * Het PersoonRatingQuery-object waarvan meteen de db-table van het
	 * PersoonRating-object als table geset is.
	 */
	static public function table()
	{
		$query = new PersoonRatingQuery();

		$query->tables('PersoonRating');

		return $query;
	}

	/**
	 * @brief Maakt een PersoonRatingVerzameling aan van de objecten die geselecteerd
	 * worden door de PersoonRatingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return PersoonRatingVerzameling
	 * Een PersoonRatingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'PersoonRating', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een PersoonRating-iterator aan van de objecten die geselecteerd
	 * worden door de PersoonRatingQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een PersoonRatingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'PersoonRating', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een PersoonRating die geslecteeerd wordt door de PersoonRatingQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return PersoonRatingVerzameling
	 * Een PersoonRatingVerzameling verkregen door de query.
	 */
	public function geef($object = 'PersoonRating')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van PersoonRating.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'ratingObject_ratingObjectID',
			'persoon_contactID',
			'rating',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'PersoonRating';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als PersoonRating een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan PersoonRating een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('PersoonRating'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'RatingObject' => array(
				'ratingObject_ratingObjectID' => 'ratingObjectID'
			),
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van PersoonRating.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'PersoonRating.ratingObject_ratingObjectID',
			'PersoonRating.persoon_contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'ratingobject':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof RatingObject; })))
					user_error('Alle waarden in de array moeten van type RatingObject zijn', E_USER_ERROR);
			}
			else if(!($value instanceof RatingObject) && !($value instanceof RatingObjectVerzameling))
				user_error('Value moet van type RatingObject zijn', E_USER_ERROR);
			$field = 'ratingObject_ratingObjectID';
			if(is_array($value) || $value instanceof RatingObjectVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getRatingObjectID();
				$value = $new_value;
			}
			else $value = $value->getRatingObjectID();
			$type = 'int';
			break;
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'rating':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, PersoonRating::enumsRating()))
						user_error($value . ' is niet een geldige waarde voor rating', E_USER_ERROR);
				}
			}
			else if(!in_array($value, PersoonRating::enumsRating()))
				user_error($value . ' is niet een geldige waarde voor rating', E_USER_ERROR);
			$type = 'string';
			$field = 'rating';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'PersoonRating.'.$v;
			}
		} else {
			$field = 'PersoonRating.'.$field;
		}

		return array($field, $value, $type);
	}

}
