<?
abstract class ActiviteitInformatieQuery_Generated
	extends ActiviteitQuery
{
	/**
	 * @brief De constructor van de ActiviteitInformatieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ActiviteitQuery
	}
	/**
	 * @brief Maakt een ActiviteitInformatieQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw
	 * ActiviteitInformatieQuery-object aan om er zeker van te zijn dat we alle
	 * eigenschappen daarvan ook meenemen.
	 *
	 * @return ActiviteitInformatieQuery
	 * Het ActiviteitInformatieQuery-object waarvan meteen de db-table van het
	 * ActiviteitInformatie-object als table geset is.
	 */
	static public function table()
	{
		$query = new ActiviteitInformatieQuery();

		$query->tables('ActiviteitInformatie');

		return $query;
	}

	/**
	 * @brief Maakt een ActiviteitInformatieVerzameling aan van de objecten die
	 * geselecteerd worden door de ActiviteitInformatieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ActiviteitInformatieVerzameling
	 * Een ActiviteitInformatieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'ActiviteitInformatie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een ActiviteitInformatie-iterator aan van de objecten die
	 * geselecteerd worden door de ActiviteitInformatieQuery uit te voeren met een SQL
	 * cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ActiviteitInformatieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'ActiviteitInformatie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een ActiviteitInformatie die geslecteeerd wordt door de
	 * ActiviteitInformatieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ActiviteitInformatieVerzameling
	 * Een ActiviteitInformatieVerzameling verkregen door de query.
	 */
	public function geef($object = 'ActiviteitInformatie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van ActiviteitInformatie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'activiteitID',
			'titel_NL',
			'titel_EN',
			'werftekst_NL',
			'werftekst_EN',
			'homepage',
			'locatie_NL',
			'locatie_EN',
			'prijs_NL',
			'prijs_EN',
			'inschrijfbaar',
			'stuurMail',
			'datumInschrijvenMax',
			'datumUitschrijven',
			'maxDeelnemers',
			'actsoort',
			'onderwijs',
			'aantalComputers',
			'categorie_actCategorieID'
		);

		if(in_array($field, $varArray))
			return 'ActiviteitInformatie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als ActiviteitInformatie
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Activiteit' => array(
				'Activiteit.activiteitID' => 'ActiviteitInformatie.activiteitID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan ActiviteitInformatie een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('ActiviteitInformatie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'ActiviteitCategorie' => array(
				'categorie_actCategorieID' => 'actCategorieID'
			),
			'Activiteit' => array(
				'Activiteit.activiteitID' => 'ActiviteitInformatie.activiteitID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van ActiviteitInformatie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'ActiviteitInformatie.activiteitID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'titel_nl':
			$type = 'string';
			$field = 'titel_NL';
			break;
		case 'titel_en':
			$type = 'string';
			$field = 'titel_EN';
			break;
		case 'werftekst_nl':
			$type = 'string';
			$field = 'werftekst_NL';
			break;
		case 'werftekst_en':
			$type = 'string';
			$field = 'werftekst_EN';
			break;
		case 'homepage':
			$type = 'string';
			$field = 'homepage';
			break;
		case 'locatie_nl':
			$type = 'string';
			$field = 'locatie_NL';
			break;
		case 'locatie_en':
			$type = 'string';
			$field = 'locatie_EN';
			break;
		case 'prijs_nl':
			$type = 'string';
			$field = 'prijs_NL';
			break;
		case 'prijs_en':
			$type = 'string';
			$field = 'prijs_EN';
			break;
		case 'inschrijfbaar':
			$type = 'int';
			$field = 'inschrijfbaar';
			break;
		case 'stuurmail':
			$type = 'int';
			$field = 'stuurMail';
			break;
		case 'datuminschrijvenmax':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumInschrijvenMax';
			break;
		case 'datumuitschrijven':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumUitschrijven';
			break;
		case 'maxdeelnemers':
			$type = 'int';
			$field = 'maxDeelnemers';
			break;
		case 'actsoort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, ActiviteitInformatie::enumsActsoort()))
						user_error($value . ' is niet een geldige waarde voor actsoort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, ActiviteitInformatie::enumsActsoort()))
				user_error($value . ' is niet een geldige waarde voor actsoort', E_USER_ERROR);
			$type = 'string';
			$field = 'actsoort';
			break;
		case 'onderwijs':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, ActiviteitInformatie::enumsOnderwijs()))
						user_error($value . ' is niet een geldige waarde voor onderwijs', E_USER_ERROR);
				}
			}
			else if(!in_array($value, ActiviteitInformatie::enumsOnderwijs()))
				user_error($value . ' is niet een geldige waarde voor onderwijs', E_USER_ERROR);
			$type = 'string';
			$field = 'onderwijs';
			break;
		case 'aantalcomputers':
			$type = 'int';
			$field = 'aantalComputers';
			break;
		case 'categorie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof ActiviteitCategorie; })))
					user_error('Alle waarden in de array moeten van type ActiviteitCategorie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof ActiviteitCategorie) && !($value instanceof ActiviteitCategorieVerzameling) && !is_null($value))
				user_error('Value moet van type ActiviteitCategorie of NULL zijn', E_USER_ERROR);
			$field = 'categorie_actCategorieID';
			if(is_array($value) || $value instanceof ActiviteitCategorieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getActCategorieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getActCategorieID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'ActiviteitInformatie.'.$v;
			}
		} else {
			$field = 'ActiviteitInformatie.'.$field;
		}

		return array($field, $value, $type);
	}

}
