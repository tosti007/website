<?
abstract class LidStudieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de LidStudieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een LidStudieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw LidStudieQuery-object aan om er zeker van
	 * te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return LidStudieQuery
	 * Het LidStudieQuery-object waarvan meteen de db-table van het LidStudie-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new LidStudieQuery();

		$query->tables('LidStudie');

		return $query;
	}

	/**
	 * @brief Maakt een LidStudieVerzameling aan van de objecten die geselecteerd
	 * worden door de LidStudieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return LidStudieVerzameling
	 * Een LidStudieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'LidStudie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een LidStudie-iterator aan van de objecten die geselecteerd worden
	 * door de LidStudieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een LidStudieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'LidStudie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een LidStudie die geslecteeerd wordt door de LidStudieQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return LidStudieVerzameling
	 * Een LidStudieVerzameling verkregen door de query.
	 */
	public function geef($object = 'LidStudie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van LidStudie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'lidStudieID',
			'studie_studieID',
			'lid_contactID',
			'datumBegin',
			'datumEind',
			'status',
			'groep_groepID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'LidStudie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als LidStudie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan LidStudie een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('LidStudie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Studie' => array(
				'studie_studieID' => 'studieID'
			),
			'Lid' => array(
				'lid_contactID' => 'contactID'
			),
			'IntroGroep' => array(
				'groep_groepID' => 'groepID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van LidStudie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'LidStudie.lidStudieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'lidstudieid':
			$type = 'int';
			$field = 'lidStudieID';
			break;
		case 'studie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Studie; })))
					user_error('Alle waarden in de array moeten van type Studie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Studie) && !($value instanceof StudieVerzameling))
				user_error('Value moet van type Studie zijn', E_USER_ERROR);
			$field = 'studie_studieID';
			if(is_array($value) || $value instanceof StudieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getStudieID();
				$value = $new_value;
			}
			else $value = $value->getStudieID();
			$type = 'int';
			break;
		case 'lid':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling))
				user_error('Value moet van type Lid zijn', E_USER_ERROR);
			$field = 'lid_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'datumbegin':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumBegin';
			break;
		case 'datumeind':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumEind';
			break;
		case 'status':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, LidStudie::enumsStatus()))
						user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
				}
			}
			else if(!in_array($value, LidStudie::enumsStatus()))
				user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
			$type = 'string';
			$field = 'status';
			break;
		case 'groep':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof IntroGroep; })))
					user_error('Alle waarden in de array moeten van type IntroGroep zijn', E_USER_ERROR);
			}
			else if(!($value instanceof IntroGroep) && !($value instanceof IntroGroepVerzameling) && !is_null($value))
				user_error('Value moet van type IntroGroep of NULL zijn', E_USER_ERROR);
			$field = 'groep_groepID';
			if(is_array($value) || $value instanceof IntroGroepVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getGroepID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getGroepID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'LidStudie.'.$v;
			}
		} else {
			$field = 'LidStudie.'.$field;
		}

		return array($field, $value, $type);
	}

}
