<?
abstract class CSPReportQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de CSPReportQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een CSPReportQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw CSPReportQuery-object aan om er zeker van
	 * te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return CSPReportQuery
	 * Het CSPReportQuery-object waarvan meteen de db-table van het CSPReport-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new CSPReportQuery();

		$query->tables('CSPReport');

		return $query;
	}

	/**
	 * @brief Maakt een CSPReportVerzameling aan van de objecten die geselecteerd
	 * worden door de CSPReportQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return CSPReportVerzameling
	 * Een CSPReportVerzameling verkregen door de query
	 */
	public function verzamel($object = 'CSPReport', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een CSPReport-iterator aan van de objecten die geselecteerd worden
	 * door de CSPReportQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een CSPReportVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'CSPReport', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een CSPReport die geslecteeerd wordt door de CSPReportQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return CSPReportVerzameling
	 * Een CSPReportVerzameling verkregen door de query.
	 */
	public function geef($object = 'CSPReport')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van CSPReport.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'ingelogdLid_contactID',
			'ip',
			'documentUri',
			'referrer',
			'blockedUri',
			'violatedDirective',
			'originalPolicy',
			'userAgent',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'CSPReport';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als CSPReport een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan CSPReport een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('CSPReport'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Lid' => array(
				'ingelogdLid_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van CSPReport.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'CSPReport.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'ingelogdlid':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling) && !is_null($value))
				user_error('Value moet van type Lid of NULL zijn', E_USER_ERROR);
			$field = 'ingelogdLid_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'ip':
			$type = 'string';
			$field = 'ip';
			break;
		case 'documenturi':
			$type = 'string';
			$field = 'documentUri';
			break;
		case 'referrer':
			$type = 'string';
			$field = 'referrer';
			break;
		case 'blockeduri':
			$type = 'string';
			$field = 'blockedUri';
			break;
		case 'violateddirective':
			$type = 'string';
			$field = 'violatedDirective';
			break;
		case 'originalpolicy':
			$type = 'string';
			$field = 'originalPolicy';
			break;
		case 'useragent':
			$type = 'string';
			$field = 'userAgent';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'CSPReport.'.$v;
			}
		} else {
			$field = 'CSPReport.'.$field;
		}

		return array($field, $value, $type);
	}

}
