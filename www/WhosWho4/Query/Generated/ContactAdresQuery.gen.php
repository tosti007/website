<?
abstract class ContactAdresQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de ContactAdresQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een ContactAdresQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw ContactAdresQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return ContactAdresQuery
	 * Het ContactAdresQuery-object waarvan meteen de db-table van het
	 * ContactAdres-object als table geset is.
	 */
	static public function table()
	{
		$query = new ContactAdresQuery();

		$query->tables('ContactAdres');

		return $query;
	}

	/**
	 * @brief Maakt een ContactAdresVerzameling aan van de objecten die geselecteerd
	 * worden door de ContactAdresQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ContactAdresVerzameling
	 * Een ContactAdresVerzameling verkregen door de query
	 */
	public function verzamel($object = 'ContactAdres', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een ContactAdres-iterator aan van de objecten die geselecteerd
	 * worden door de ContactAdresQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ContactAdresVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'ContactAdres', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een ContactAdres die geslecteeerd wordt door de ContactAdresQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ContactAdresVerzameling
	 * Een ContactAdresVerzameling verkregen door de query.
	 */
	public function geef($object = 'ContactAdres')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van ContactAdres.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'adresID',
			'contact_contactID',
			'soort',
			'straat1',
			'straat2',
			'huisnummer',
			'postcode',
			'woonplaats',
			'land',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'ContactAdres';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als ContactAdres een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan ContactAdres een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('ContactAdres'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contact' => array(
				'contact_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van ContactAdres.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'ContactAdres.adresID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'adresid':
			$type = 'int';
			$field = 'adresID';
			break;
		case 'contact':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Contact; })))
					user_error('Alle waarden in de array moeten van type Contact zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Contact) && !($value instanceof ContactVerzameling))
				user_error('Value moet van type Contact zijn', E_USER_ERROR);
			$field = 'contact_contactID';
			if(is_array($value) || $value instanceof ContactVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, ContactAdres::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, ContactAdres::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'straat1':
			$type = 'string';
			$field = 'straat1';
			break;
		case 'straat2':
			$type = 'string';
			$field = 'straat2';
			break;
		case 'huisnummer':
			$type = 'string';
			$field = 'huisnummer';
			break;
		case 'postcode':
			$type = 'string';
			$field = 'postcode';
			break;
		case 'woonplaats':
			$type = 'string';
			$field = 'woonplaats';
			break;
		case 'land':
			$type = 'string';
			$field = 'land';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'ContactAdres.'.$v;
			}
		} else {
			$field = 'ContactAdres.'.$field;
		}

		return array($field, $value, $type);
	}

}
