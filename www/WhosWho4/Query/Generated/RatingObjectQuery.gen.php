<?
abstract class RatingObjectQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de RatingObjectQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een RatingObjectQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw RatingObjectQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return RatingObjectQuery
	 * Het RatingObjectQuery-object waarvan meteen de db-table van het
	 * RatingObject-object als table geset is.
	 */
	static public function table()
	{
		$query = new RatingObjectQuery();

		$query->tables('RatingObject');

		return $query;
	}

	/**
	 * @brief Maakt een RatingObjectVerzameling aan van de objecten die geselecteerd
	 * worden door de RatingObjectQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return RatingObjectVerzameling
	 * Een RatingObjectVerzameling verkregen door de query
	 */
	public function verzamel($object = 'RatingObject', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een RatingObject-iterator aan van de objecten die geselecteerd
	 * worden door de RatingObjectQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een RatingObjectVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'RatingObject', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een RatingObject die geslecteeerd wordt door de RatingObjectQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return RatingObjectVerzameling
	 * Een RatingObjectVerzameling verkregen door de query.
	 */
	public function geef($object = 'RatingObject')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van RatingObject.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'ratingObjectID',
			'rating',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'RatingObject';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als RatingObject een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan RatingObject een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('RatingObject'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van RatingObject.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'RatingObject.ratingObjectID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'ratingobjectid':
			$type = 'int';
			$field = 'ratingObjectID';
			break;
		case 'rating':
			$type = 'int';
			$field = 'rating';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'RatingObject.'.$v;
			}
		} else {
			$field = 'RatingObject.'.$field;
		}

		return array($field, $value, $type);
	}

}
