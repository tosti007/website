<?
abstract class OrganisatieQuery_Generated
	extends ContactQuery
{
	/**
	 * @brief De constructor van de OrganisatieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ContactQuery
	}
	/**
	 * @brief Maakt een OrganisatieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw OrganisatieQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return OrganisatieQuery
	 * Het OrganisatieQuery-object waarvan meteen de db-table van het
	 * Organisatie-object als table geset is.
	 */
	static public function table()
	{
		$query = new OrganisatieQuery();

		$query->tables('Organisatie');

		return $query;
	}

	/**
	 * @brief Maakt een OrganisatieVerzameling aan van de objecten die geselecteerd
	 * worden door de OrganisatieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return OrganisatieVerzameling
	 * Een OrganisatieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Organisatie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Organisatie-iterator aan van de objecten die geselecteerd
	 * worden door de OrganisatieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een OrganisatieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Organisatie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Organisatie die geslecteeerd wordt door de OrganisatieQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return OrganisatieVerzameling
	 * Een OrganisatieVerzameling verkregen door de query.
	 */
	public function geef($object = 'Organisatie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Organisatie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'contactID',
			'naam',
			'soort',
			'omschrijving',
			'logo'
		);

		if(in_array($field, $varArray))
			return 'Organisatie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Organisatie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Contact' => array(
				'Contact.contactID' => 'Organisatie.contactID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Organisatie een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Organisatie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contact' => array(
				'Contact.contactID' => 'Organisatie.contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Organisatie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Organisatie.contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Organisatie::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Organisatie::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'logo':
			$type = 'string';
			$field = 'logo';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Organisatie.'.$v;
			}
		} else {
			$field = 'Organisatie.'.$field;
		}

		return array($field, $value, $type);
	}

}
