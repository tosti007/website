<?
abstract class CommissieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de CommissieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een CommissieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw CommissieQuery-object aan om er zeker van
	 * te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return CommissieQuery
	 * Het CommissieQuery-object waarvan meteen de db-table van het Commissie-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new CommissieQuery();

		$query->tables('Commissie');

		return $query;
	}

	/**
	 * @brief Maakt een CommissieVerzameling aan van de objecten die geselecteerd
	 * worden door de CommissieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Commissie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Commissie-iterator aan van de objecten die geselecteerd worden
	 * door de CommissieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een CommissieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Commissie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Commissie die geslecteeerd wordt door de CommissieQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling verkregen door de query.
	 */
	public function geef($object = 'Commissie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Commissie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'commissieID',
			'naam',
			'soort',
			'categorie_categorieID',
			'thema',
			'omschrijving',
			'datumBegin',
			'datumEind',
			'login',
			'email',
			'homepage',
			'emailZichtbaar',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Commissie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Commissie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Commissie een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Commissie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'CommissieCategorie' => array(
				'categorie_categorieID' => 'categorieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Commissie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Commissie.commissieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'commissieid':
			$type = 'int';
			$field = 'commissieID';
			break;
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Commissie::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Commissie::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'categorie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof CommissieCategorie; })))
					user_error('Alle waarden in de array moeten van type CommissieCategorie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof CommissieCategorie) && !($value instanceof CommissieCategorieVerzameling) && !is_null($value))
				user_error('Value moet van type CommissieCategorie of NULL zijn', E_USER_ERROR);
			$field = 'categorie_categorieID';
			if(is_array($value) || $value instanceof CommissieCategorieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCategorieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCategorieID();
			$type = 'int';
			break;
		case 'thema':
			$type = 'string';
			$field = 'thema';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'datumbegin':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumBegin';
			break;
		case 'datumeind':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumEind';
			break;
		case 'login':
			$type = 'string';
			$field = 'login';
			break;
		case 'email':
			$type = 'string';
			$field = 'email';
			break;
		case 'homepage':
			$type = 'string';
			$field = 'homepage';
			break;
		case 'emailzichtbaar':
			$type = 'int';
			$field = 'emailZichtbaar';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Commissie.'.$v;
			}
		} else {
			$field = 'Commissie.'.$field;
		}

		return array($field, $value, $type);
	}

}
