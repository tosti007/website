<?
abstract class TinyUrlHitQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de TinyUrlHitQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een TinyUrlHitQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw TinyUrlHitQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return TinyUrlHitQuery
	 * Het TinyUrlHitQuery-object waarvan meteen de db-table van het TinyUrlHit-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new TinyUrlHitQuery();

		$query->tables('TinyUrlHit');

		return $query;
	}

	/**
	 * @brief Maakt een TinyUrlHitVerzameling aan van de objecten die geselecteerd
	 * worden door de TinyUrlHitQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return TinyUrlHitVerzameling
	 * Een TinyUrlHitVerzameling verkregen door de query
	 */
	public function verzamel($object = 'TinyUrlHit', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een TinyUrlHit-iterator aan van de objecten die geselecteerd worden
	 * door de TinyUrlHitQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een TinyUrlHitVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'TinyUrlHit', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een TinyUrlHit die geslecteeerd wordt door de TinyUrlHitQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return TinyUrlHitVerzameling
	 * Een TinyUrlHitVerzameling verkregen door de query.
	 */
	public function geef($object = 'TinyUrlHit')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van TinyUrlHit.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'tinyUrl_id',
			'host',
			'hitTime',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'TinyUrlHit';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als TinyUrlHit een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan TinyUrlHit een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('TinyUrlHit'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'TinyUrl' => array(
				'tinyUrl_id' => 'id'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van TinyUrlHit.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'TinyUrlHit.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'tinyurl':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof TinyUrl; })))
					user_error('Alle waarden in de array moeten van type TinyUrl zijn', E_USER_ERROR);
			}
			else if(!($value instanceof TinyUrl) && !($value instanceof TinyUrlVerzameling))
				user_error('Value moet van type TinyUrl zijn', E_USER_ERROR);
			$field = 'tinyUrl_id';
			if(is_array($value) || $value instanceof TinyUrlVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getId();
				$value = $new_value;
			}
			else $value = $value->getId();
			$type = 'int';
			break;
		case 'host':
			$type = 'string';
			$field = 'host';
			break;
		case 'hittime':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'hitTime';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'TinyUrlHit.'.$v;
			}
		} else {
			$field = 'TinyUrlHit.'.$field;
		}

		return array($field, $value, $type);
	}

}
