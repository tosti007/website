<?
abstract class ActiviteitHerhalingQuery_Generated
	extends ActiviteitQuery
{
	/**
	 * @brief De constructor van de ActiviteitHerhalingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ActiviteitQuery
	}
	/**
	 * @brief Maakt een ActiviteitHerhalingQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw
	 * ActiviteitHerhalingQuery-object aan om er zeker van te zijn dat we alle
	 * eigenschappen daarvan ook meenemen.
	 *
	 * @return ActiviteitHerhalingQuery
	 * Het ActiviteitHerhalingQuery-object waarvan meteen de db-table van het
	 * ActiviteitHerhaling-object als table geset is.
	 */
	static public function table()
	{
		$query = new ActiviteitHerhalingQuery();

		$query->tables('ActiviteitHerhaling');

		return $query;
	}

	/**
	 * @brief Maakt een ActiviteitHerhalingVerzameling aan van de objecten die
	 * geselecteerd worden door de ActiviteitHerhalingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ActiviteitHerhalingVerzameling
	 * Een ActiviteitHerhalingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'ActiviteitHerhaling', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een ActiviteitHerhaling-iterator aan van de objecten die
	 * geselecteerd worden door de ActiviteitHerhalingQuery uit te voeren met een SQL
	 * cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ActiviteitHerhalingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'ActiviteitHerhaling', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een ActiviteitHerhaling die geslecteeerd wordt door de
	 * ActiviteitHerhalingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ActiviteitHerhalingVerzameling
	 * Een ActiviteitHerhalingVerzameling verkregen door de query.
	 */
	public function geef($object = 'ActiviteitHerhaling')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van ActiviteitHerhaling.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'activiteitID',
			'parent_activiteitID'
		);

		if(in_array($field, $varArray))
			return 'ActiviteitHerhaling';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als ActiviteitHerhaling
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Activiteit' => array(
				'Activiteit.activiteitID' => 'ActiviteitHerhaling.activiteitID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan ActiviteitHerhaling een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('ActiviteitHerhaling'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Activiteit' => array(
				'Activiteit.activiteitID' => 'ActiviteitHerhaling.activiteitID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van ActiviteitHerhaling.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'ActiviteitHerhaling.activiteitID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'parent':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Activiteit; })))
					user_error('Alle waarden in de array moeten van type Activiteit zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Activiteit) && !($value instanceof ActiviteitVerzameling))
				user_error('Value moet van type Activiteit zijn', E_USER_ERROR);
			$field = 'parent_activiteitID';
			if(is_array($value) || $value instanceof ActiviteitVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getActiviteitID();
				$value = $new_value;
			}
			else $value = $value->getActiviteitID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'ActiviteitHerhaling.'.$v;
			}
		} else {
			$field = 'ActiviteitHerhaling.'.$field;
		}

		return array($field, $value, $type);
	}

}
