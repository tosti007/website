<?
abstract class VoornaamwoordQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de VoornaamwoordQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een VoornaamwoordQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw VoornaamwoordQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VoornaamwoordQuery
	 * Het VoornaamwoordQuery-object waarvan meteen de db-table van het
	 * Voornaamwoord-object als table geset is.
	 */
	static public function table()
	{
		$query = new VoornaamwoordQuery();

		$query->tables('Voornaamwoord');

		return $query;
	}

	/**
	 * @brief Maakt een VoornaamwoordVerzameling aan van de objecten die geselecteerd
	 * worden door de VoornaamwoordQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VoornaamwoordVerzameling
	 * Een VoornaamwoordVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Voornaamwoord', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Voornaamwoord-iterator aan van de objecten die geselecteerd
	 * worden door de VoornaamwoordQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VoornaamwoordVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Voornaamwoord', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Voornaamwoord die geslecteeerd wordt door de VoornaamwoordQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VoornaamwoordVerzameling
	 * Een VoornaamwoordVerzameling verkregen door de query.
	 */
	public function geef($object = 'Voornaamwoord')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Voornaamwoord.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'woordID',
			'soort_NL',
			'soort_EN',
			'onderwerp_NL',
			'onderwerp_EN',
			'voorwerp_NL',
			'voorwerp_EN',
			'bezittelijk_NL',
			'bezittelijk_EN',
			'wederkerend_NL',
			'wederkerend_EN',
			'ouder_NL',
			'ouder_EN',
			'kind_NL',
			'kind_EN',
			'brusje_NL',
			'brusje_EN',
			'kozijn_NL',
			'kozijn_EN',
			'kindGrootouder_NL',
			'kindGrootouder_EN',
			'kleinkindOuder_NL',
			'kleinkindOuder_EN',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Voornaamwoord';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Voornaamwoord een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Voornaamwoord een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Voornaamwoord'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Voornaamwoord.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Voornaamwoord.woordID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'woordid':
			$type = 'int';
			$field = 'woordID';
			break;
		case 'soort_nl':
			$type = 'string';
			$field = 'soort_NL';
			break;
		case 'soort_en':
			$type = 'string';
			$field = 'soort_EN';
			break;
		case 'onderwerp_nl':
			$type = 'string';
			$field = 'onderwerp_NL';
			break;
		case 'onderwerp_en':
			$type = 'string';
			$field = 'onderwerp_EN';
			break;
		case 'voorwerp_nl':
			$type = 'string';
			$field = 'voorwerp_NL';
			break;
		case 'voorwerp_en':
			$type = 'string';
			$field = 'voorwerp_EN';
			break;
		case 'bezittelijk_nl':
			$type = 'string';
			$field = 'bezittelijk_NL';
			break;
		case 'bezittelijk_en':
			$type = 'string';
			$field = 'bezittelijk_EN';
			break;
		case 'wederkerend_nl':
			$type = 'string';
			$field = 'wederkerend_NL';
			break;
		case 'wederkerend_en':
			$type = 'string';
			$field = 'wederkerend_EN';
			break;
		case 'ouder_nl':
			$type = 'string';
			$field = 'ouder_NL';
			break;
		case 'ouder_en':
			$type = 'string';
			$field = 'ouder_EN';
			break;
		case 'kind_nl':
			$type = 'string';
			$field = 'kind_NL';
			break;
		case 'kind_en':
			$type = 'string';
			$field = 'kind_EN';
			break;
		case 'brusje_nl':
			$type = 'string';
			$field = 'brusje_NL';
			break;
		case 'brusje_en':
			$type = 'string';
			$field = 'brusje_EN';
			break;
		case 'kozijn_nl':
			$type = 'string';
			$field = 'kozijn_NL';
			break;
		case 'kozijn_en':
			$type = 'string';
			$field = 'kozijn_EN';
			break;
		case 'kindgrootouder_nl':
			$type = 'string';
			$field = 'kindGrootouder_NL';
			break;
		case 'kindgrootouder_en':
			$type = 'string';
			$field = 'kindGrootouder_EN';
			break;
		case 'kleinkindouder_nl':
			$type = 'string';
			$field = 'kleinkindOuder_NL';
			break;
		case 'kleinkindouder_en':
			$type = 'string';
			$field = 'kleinkindOuder_EN';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Voornaamwoord.'.$v;
			}
		} else {
			$field = 'Voornaamwoord.'.$field;
		}

		return array($field, $value, $type);
	}

}
