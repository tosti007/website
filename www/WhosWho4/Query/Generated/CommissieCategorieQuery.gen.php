<?
abstract class CommissieCategorieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de CommissieCategorieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een CommissieCategorieQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw
	 * CommissieCategorieQuery-object aan om er zeker van te zijn dat we alle
	 * eigenschappen daarvan ook meenemen.
	 *
	 * @return CommissieCategorieQuery
	 * Het CommissieCategorieQuery-object waarvan meteen de db-table van het
	 * CommissieCategorie-object als table geset is.
	 */
	static public function table()
	{
		$query = new CommissieCategorieQuery();

		$query->tables('CommissieCategorie');

		return $query;
	}

	/**
	 * @brief Maakt een CommissieCategorieVerzameling aan van de objecten die
	 * geselecteerd worden door de CommissieCategorieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return CommissieCategorieVerzameling
	 * Een CommissieCategorieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'CommissieCategorie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een CommissieCategorie-iterator aan van de objecten die
	 * geselecteerd worden door de CommissieCategorieQuery uit te voeren met een SQL
	 * cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een CommissieCategorieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'CommissieCategorie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een CommissieCategorie die geslecteeerd wordt door de
	 * CommissieCategorieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return CommissieCategorieVerzameling
	 * Een CommissieCategorieVerzameling verkregen door de query.
	 */
	public function geef($object = 'CommissieCategorie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van CommissieCategorie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'categorieID',
			'naam',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'CommissieCategorie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als CommissieCategorie
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan CommissieCategorie een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('CommissieCategorie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van CommissieCategorie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'CommissieCategorie.categorieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'categorieid':
			$type = 'int';
			$field = 'categorieID';
			break;
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'CommissieCategorie.'.$v;
			}
		} else {
			$field = 'CommissieCategorie.'.$field;
		}

		return array($field, $value, $type);
	}

}
