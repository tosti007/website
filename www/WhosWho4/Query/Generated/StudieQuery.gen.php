<?
abstract class StudieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de StudieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een StudieQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw StudieQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return StudieQuery
	 * Het StudieQuery-object waarvan meteen de db-table van het Studie-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new StudieQuery();

		$query->tables('Studie');

		return $query;
	}

	/**
	 * @brief Maakt een StudieVerzameling aan van de objecten die geselecteerd worden
	 * door de StudieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return StudieVerzameling
	 * Een StudieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Studie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Studie-iterator aan van de objecten die geselecteerd worden
	 * door de StudieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een StudieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Studie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Studie die geslecteeerd wordt door de StudieQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return StudieVerzameling
	 * Een StudieVerzameling verkregen door de query.
	 */
	public function geef($object = 'Studie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Studie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'studieID',
			'naam_NL',
			'naam_EN',
			'soort',
			'fase',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Studie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Studie een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Studie een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Studie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Studie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Studie.studieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'studieid':
			$type = 'int';
			$field = 'studieID';
			break;
		case 'naam_nl':
			$type = 'string';
			$field = 'naam_NL';
			break;
		case 'naam_en':
			$type = 'string';
			$field = 'naam_EN';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Studie::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Studie::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'fase':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Studie::enumsFase()))
						user_error($value . ' is niet een geldige waarde voor fase', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Studie::enumsFase()))
				user_error($value . ' is niet een geldige waarde voor fase', E_USER_ERROR);
			$type = 'string';
			$field = 'fase';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Studie.'.$v;
			}
		} else {
			$field = 'Studie.'.$field;
		}

		return array($field, $value, $type);
	}

}
