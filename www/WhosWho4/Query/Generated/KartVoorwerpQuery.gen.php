<?
abstract class KartVoorwerpQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de KartVoorwerpQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een KartVoorwerpQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw KartVoorwerpQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return KartVoorwerpQuery
	 * Het KartVoorwerpQuery-object waarvan meteen de db-table van het
	 * KartVoorwerp-object als table geset is.
	 */
	static public function table()
	{
		$query = new KartVoorwerpQuery();

		$query->tables('KartVoorwerp');

		return $query;
	}

	/**
	 * @brief Maakt een KartVoorwerpVerzameling aan van de objecten die geselecteerd
	 * worden door de KartVoorwerpQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return KartVoorwerpVerzameling
	 * Een KartVoorwerpVerzameling verkregen door de query
	 */
	public function verzamel($object = 'KartVoorwerp', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een KartVoorwerp-iterator aan van de objecten die geselecteerd
	 * worden door de KartVoorwerpQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een KartVoorwerpVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'KartVoorwerp', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een KartVoorwerp die geslecteeerd wordt door de KartVoorwerpQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return KartVoorwerpVerzameling
	 * Een KartVoorwerpVerzameling verkregen door de query.
	 */
	public function geef($object = 'KartVoorwerp')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van KartVoorwerp.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'kartVoorwerpID',
			'persoonKart_persoon_contactID',
			'persoonKart_naam',
			'persoon_contactID',
			'commissie_commissieID',
			'media_mediaID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'KartVoorwerp';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als KartVoorwerp een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan KartVoorwerp een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('KartVoorwerp'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'PersoonKart' => array(
				'persoonKart_persoon_contactID' => 'persoon_contactID',
				'persoonKart_naam' => 'naam'
			),
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			),
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			),
			'Media' => array(
				'media_mediaID' => 'mediaID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van KartVoorwerp.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'KartVoorwerp.kartVoorwerpID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'kartvoorwerpid':
			$type = 'int';
			$field = 'kartVoorwerpID';
			break;
		case 'persoonkart':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof PersoonKart; })))
					user_error('Alle waarden in de array moeten van type PersoonKart zijn', E_USER_ERROR);
			}
			else if(!($value instanceof PersoonKart) && !($value instanceof PersoonKartVerzameling))
				user_error('Value moet van type PersoonKart zijn', E_USER_ERROR);
			$field = array(
				'persoonKart_persoon_contactID',
				'persoonKart_naam'
			);
			if(is_array($value) || $value instanceof PersoonKartVerzameling)
			{
				foreach($value as $k => $v)
					$value[$k] = array(
				$v->getPersoonContactID(),
				$v->getNaam()
					);
				if($value instanceof PersoonKartVerzameling)
					$value = $value->getVerzameling();
			}
			else
				$value = array(
				$value->getPersoonContactID(),
				$value->getNaam()
				);
			$type = array(
				'int',
				'string'				);
			break;
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling) && !is_null($value))
				user_error('Value moet van type Persoon of NULL zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling) && !is_null($value))
				user_error('Value moet van type Commissie of NULL zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'media':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Media; })))
					user_error('Alle waarden in de array moeten van type Media zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Media) && !($value instanceof MediaVerzameling) && !is_null($value))
				user_error('Value moet van type Media of NULL zijn', E_USER_ERROR);
			$field = 'media_mediaID';
			if(is_array($value) || $value instanceof MediaVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMediaID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getMediaID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'KartVoorwerp.'.$v;
			}
		} else {
			$field = 'KartVoorwerp.'.$field;
		}

		return array($field, $value, $type);
	}

}
