<?
abstract class TentamenUitwerkingQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de TentamenUitwerkingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een TentamenUitwerkingQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw
	 * TentamenUitwerkingQuery-object aan om er zeker van te zijn dat we alle
	 * eigenschappen daarvan ook meenemen.
	 *
	 * @return TentamenUitwerkingQuery
	 * Het TentamenUitwerkingQuery-object waarvan meteen de db-table van het
	 * TentamenUitwerking-object als table geset is.
	 */
	static public function table()
	{
		$query = new TentamenUitwerkingQuery();

		$query->tables('TentamenUitwerking');

		return $query;
	}

	/**
	 * @brief Maakt een TentamenUitwerkingVerzameling aan van de objecten die
	 * geselecteerd worden door de TentamenUitwerkingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return TentamenUitwerkingVerzameling
	 * Een TentamenUitwerkingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'TentamenUitwerking', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een TentamenUitwerking-iterator aan van de objecten die
	 * geselecteerd worden door de TentamenUitwerkingQuery uit te voeren met een SQL
	 * cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een TentamenUitwerkingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'TentamenUitwerking', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een TentamenUitwerking die geslecteeerd wordt door de
	 * TentamenUitwerkingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return TentamenUitwerkingVerzameling
	 * Een TentamenUitwerkingVerzameling verkregen door de query.
	 */
	public function geef($object = 'TentamenUitwerking')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van TentamenUitwerking.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'uitwerkingID',
			'tentamen_id',
			'uploader_contactID',
			'wanneer',
			'opmerking',
			'gecontroleerd',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'TentamenUitwerking';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als TentamenUitwerking
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan TentamenUitwerking een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('TentamenUitwerking'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Tentamen' => array(
				'tentamen_id' => 'id'
			),
			'Persoon' => array(
				'uploader_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van TentamenUitwerking.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'TentamenUitwerking.uitwerkingID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'uitwerkingid':
			$type = 'int';
			$field = 'uitwerkingID';
			break;
		case 'tentamen':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Tentamen; })))
					user_error('Alle waarden in de array moeten van type Tentamen zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Tentamen) && !($value instanceof TentamenVerzameling))
				user_error('Value moet van type Tentamen zijn', E_USER_ERROR);
			$field = 'tentamen_id';
			if(is_array($value) || $value instanceof TentamenVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getId();
				$value = $new_value;
			}
			else $value = $value->getId();
			$type = 'int';
			break;
		case 'uploader':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'uploader_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'wanneer':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'wanneer';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'opmerking';
			break;
		case 'gecontroleerd':
			$type = 'int';
			$field = 'gecontroleerd';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'TentamenUitwerking.'.$v;
			}
		} else {
			$field = 'TentamenUitwerking.'.$field;
		}

		return array($field, $value, $type);
	}

}
