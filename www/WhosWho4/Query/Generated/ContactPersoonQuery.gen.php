<?
abstract class ContactPersoonQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de ContactPersoonQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een ContactPersoonQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw ContactPersoonQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return ContactPersoonQuery
	 * Het ContactPersoonQuery-object waarvan meteen de db-table van het
	 * ContactPersoon-object als table geset is.
	 */
	static public function table()
	{
		$query = new ContactPersoonQuery();

		$query->tables('ContactPersoon');

		return $query;
	}

	/**
	 * @brief Maakt een ContactPersoonVerzameling aan van de objecten die geselecteerd
	 * worden door de ContactPersoonQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ContactPersoonVerzameling
	 * Een ContactPersoonVerzameling verkregen door de query
	 */
	public function verzamel($object = 'ContactPersoon', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een ContactPersoon-iterator aan van de objecten die geselecteerd
	 * worden door de ContactPersoonQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ContactPersoonVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'ContactPersoon', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een ContactPersoon die geslecteeerd wordt door de
	 * ContactPersoonQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ContactPersoonVerzameling
	 * Een ContactPersoonVerzameling verkregen door de query.
	 */
	public function geef($object = 'ContactPersoon')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van ContactPersoon.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'persoon_contactID',
			'organisatie_contactID',
			'type',
			'Functie',
			'beginDatum',
			'eindDatum',
			'opmerking',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'ContactPersoon';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als ContactPersoon een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan ContactPersoon een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('ContactPersoon'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			),
			'Organisatie' => array(
				'organisatie_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van ContactPersoon.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'ContactPersoon.persoon_contactID',
			'ContactPersoon.organisatie_contactID',
			'ContactPersoon.type'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'organisatie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Organisatie; })))
					user_error('Alle waarden in de array moeten van type Organisatie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Organisatie) && !($value instanceof OrganisatieVerzameling))
				user_error('Value moet van type Organisatie zijn', E_USER_ERROR);
			$field = 'organisatie_contactID';
			if(is_array($value) || $value instanceof OrganisatieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'type':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, ContactPersoon::enumsType()))
						user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
				}
			}
			else if(!in_array($value, ContactPersoon::enumsType()))
				user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
			$type = 'string';
			$field = 'type';
			break;
		case 'functie':
			$type = 'string';
			$field = 'Functie';
			break;
		case 'begindatum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'beginDatum';
			break;
		case 'einddatum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'eindDatum';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'opmerking';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'ContactPersoon.'.$v;
			}
		} else {
			$field = 'ContactPersoon.'.$field;
		}

		return array($field, $value, $type);
	}

}
