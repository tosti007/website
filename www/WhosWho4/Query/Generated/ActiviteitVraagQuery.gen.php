<?
abstract class ActiviteitVraagQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de ActiviteitVraagQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een ActiviteitVraagQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw ActiviteitVraagQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return ActiviteitVraagQuery
	 * Het ActiviteitVraagQuery-object waarvan meteen de db-table van het
	 * ActiviteitVraag-object als table geset is.
	 */
	static public function table()
	{
		$query = new ActiviteitVraagQuery();

		$query->tables('ActiviteitVraag');

		return $query;
	}

	/**
	 * @brief Maakt een ActiviteitVraagVerzameling aan van de objecten die geselecteerd
	 * worden door de ActiviteitVraagQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ActiviteitVraagVerzameling
	 * Een ActiviteitVraagVerzameling verkregen door de query
	 */
	public function verzamel($object = 'ActiviteitVraag', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een ActiviteitVraag-iterator aan van de objecten die geselecteerd
	 * worden door de ActiviteitVraagQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ActiviteitVraagVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'ActiviteitVraag', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een ActiviteitVraag die geslecteeerd wordt door de
	 * ActiviteitVraagQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ActiviteitVraagVerzameling
	 * Een ActiviteitVraagVerzameling verkregen door de query.
	 */
	public function geef($object = 'ActiviteitVraag')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van ActiviteitVraag.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'activiteit_activiteitID',
			'vraagID',
			'type',
			'vraag_NL',
			'vraag_EN',
			'opties',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'ActiviteitVraag';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als ActiviteitVraag een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan ActiviteitVraag een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('ActiviteitVraag'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Activiteit' => array(
				'activiteit_activiteitID' => 'activiteitID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van ActiviteitVraag.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'ActiviteitVraag.activiteit_activiteitID',
			'ActiviteitVraag.vraagID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'activiteit':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Activiteit; })))
					user_error('Alle waarden in de array moeten van type Activiteit zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Activiteit) && !($value instanceof ActiviteitVerzameling))
				user_error('Value moet van type Activiteit zijn', E_USER_ERROR);
			$field = 'activiteit_activiteitID';
			if(is_array($value) || $value instanceof ActiviteitVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getActiviteitID();
				$value = $new_value;
			}
			else $value = $value->getActiviteitID();
			$type = 'int';
			break;
		case 'vraagid':
			$type = 'int';
			$field = 'vraagID';
			break;
		case 'type':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, ActiviteitVraag::enumsType()))
						user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
				}
			}
			else if(!in_array($value, ActiviteitVraag::enumsType()))
				user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
			$type = 'string';
			$field = 'type';
			break;
		case 'vraag_nl':
			$type = 'string';
			$field = 'vraag_NL';
			break;
		case 'vraag_en':
			$type = 'string';
			$field = 'vraag_EN';
			break;
		case 'opties':
			$type = 'string';
			$field = 'opties';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'ActiviteitVraag.'.$v;
			}
		} else {
			$field = 'ActiviteitVraag.'.$field;
		}

		return array($field, $value, $type);
	}

}
