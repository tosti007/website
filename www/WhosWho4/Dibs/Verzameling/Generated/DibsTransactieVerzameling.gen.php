<?
abstract class DibsTransactieVerzameling_Generated
	extends TransactieVerzameling
{
	/**
	 * @brief De constructor van de DibsTransactieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // TransactieVerzameling
	}
	/**
	 * @brief Maak van deze DibsTransactieVerzameling een PasVerzameling.
	 *
	 * @return PasVerzameling
	 * Een PasVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze DibsTransactieVerzameling.
	 */
	public function toPasVerzameling()
	{
		if($this->aantal() == 0)
			return new PasVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getPasPasID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getPasPasID()
			                      );
		}
		$this->positie = $origPositie;
		return PasVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DibsTransactieVerzameling van Pas.
	 *
	 * @return DibsTransactieVerzameling
	 * Een DibsTransactieVerzameling die elementen bevat die bij de Pas hoort.
	 */
	static public function fromPas($pas)
	{
		if(!isset($pas))
			return new DibsTransactieVerzameling();

		return DibsTransactieQuery::table()
			->whereProp('Pas', $pas)
			->verzamel();
	}
}
