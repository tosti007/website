<?
abstract class DibsInfoVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de DibsInfoVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze DibsInfoVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze DibsInfoVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze DibsInfoVerzameling een DibsTransactieVerzameling.
	 *
	 * @return DibsTransactieVerzameling
	 * Een DibsTransactieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze DibsInfoVerzameling.
	 */
	public function toLaatsteVerzameling()
	{
		if($this->aantal() == 0)
			return new DibsTransactieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getLaatsteTransactieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getLaatsteTransactieID()
			                      );
		}
		$this->positie = $origPositie;
		return DibsTransactieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DibsInfoVerzameling van Persoon.
	 *
	 * @return DibsInfoVerzameling
	 * Een DibsInfoVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new DibsInfoVerzameling();

		return DibsInfoQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een DibsInfoVerzameling van Laatste.
	 *
	 * @return DibsInfoVerzameling
	 * Een DibsInfoVerzameling die elementen bevat die bij de Laatste hoort.
	 */
	static public function fromLaatste($laatste)
	{
		if(!isset($laatste))
			return new DibsInfoVerzameling();

		return DibsInfoQuery::table()
			->whereProp('Laatste', $laatste)
			->verzamel();
	}
}
