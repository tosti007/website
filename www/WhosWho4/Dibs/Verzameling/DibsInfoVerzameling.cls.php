<?
/**
 * $Id$
 */
class DibsInfoVerzameling
	extends DibsInfoVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DibsInfoVerzameling_Generated
	}

	static public function sorteerOpNaam($aID, $bID)
	{
		$a = PersoonView::naam(Persoon::geef($aID));
		$b = PersoonView::naam(Persoon::geef($bID));

		return strcasecmp($a, $b);
	}

	static public function getDibsers ()
	{
		return DibsInfoQuery::table()
			->orderByAsc('persoon_contactID')
			->verzamel();
	}

	static public function getTotaalKudos()
	{
		global $WSW4DB;
		return $WSW4DB->q('MAYBEVALUE SELECT SUM(`DibsInfo`.`kudos`) FROM `DibsInfo`');
	}

	static public function zoek ($arg)
	{
		global $WSW4DB;
		$ids = null;
		if (preg_match('/\d{1,}/', $arg, $matches))
		{
			$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `DibsInfo`.`persoon_contactID` FROM `DibsInfo`'
				. ' WHERE (`DibsInfo`.`persoon_contactID` LIKE %c)'
				. ' UNION ALL'
				. ' SELECT DISTINCT `DibsInfo`.`persoon_contactID` FROM `DibsInfo`'
				. ' LEFT JOIN `Pas`'
				. ' ON `DibsInfo`.`pas_pasID`=`Pas`.`pasID`'
				. ' WHERE (`Pas`.`rfidTag` LIKE %c)'
				, $matches[0]
				, $matches[0]);
			return DibsInfoVerzameling::verzamel($ids);
		} elseif (preg_match('/([A-z]|\s){1,}/', $arg, $matches)) {
			$result = $WSW4DB->q('COLUMN SELECT DISTINCT * FROM `DibsInfo` ORDER BY `DibsInfo`.`persoon_contactID` ASC');
			if ($result) {
				$tabel = DibsInfoVerzameling::verzamel($result);
				$ids = new DibsInfoVerzameling();
				foreach ($tabel as $entry) {
					$naam = PersoonView::naam(Persoon::geef($entry->getPersoonContactID()));
					if (strstr($naam, $arg) || strstr(mb_strtolower($naam), $arg))
						$ids->voegtoe($entry);
				}
				return $ids;
			}
		} else
			return $ids = new DibsInfoVerzameling();
	}
	
	static public function zoekQuickform ($arg)
	{
		global $WSW4DB;
		if (preg_match('/\d{1,}/', $arg))
		{
			$ids = $WSW4DB->q('COLUMN SELECT DISTINCT Persoon.contactID FROM Persoon'
				. ' JOIN Lid ON Persoon.contactID = Lid.contactID'
				. ' WHERE Persoon.contactID LIKE %c'
			    . ' AND (Lid.lidToestand = "LID" OR Lid.lidToestand = "BAL")'
				, $arg);
			return PersoonVerzameling::verzamel($ids);
		} elseif (preg_match('/([A-z]|\s){1,}/', $arg)) {
			$input = preg_split('/\s+/', $arg);
			if (count($input) == 1)
				$result = $WSW4DB->q('COLUMN SELECT DISTINCT Persoon.contactID FROM Persoon'
								  . ' JOIN Lid ON Persoon.contactID = Lid.contactID'
								  . ' WHERE (Persoon.voornaam LIKE %c OR Persoon.achternaam LIKE %c)'
								  . ' AND (Lid.lidToestand = "LID" OR Lid.lidToestand = "BAL")'
								  , current($input)
								  , end($input));
			else
				$result = $WSW4DB->q('COLUMN SELECT DISTINCT Persoon.contactID FROM Persoon'
								  . ' JOIN Lid ON Persoon.contactID = Lid.contactID'
								  . ' WHERE (Persoon.voornaam LIKE %c AND Persoon.achternaam LIKE %c)'
								  . ' AND (Lid.lidToestand = "LID" OR Lid.lidToestand = "BAL")'
									, current($input)
									, end($input));
			if ($result)
				return PersoonVerzameling::verzamel($result);
			else
				return new PersoonVerzameling();
		} else
			return new PersoonVerzameling();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
