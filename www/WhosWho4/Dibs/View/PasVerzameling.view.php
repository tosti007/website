<?
/**
 * $Id$
 */
abstract class PasVerzamelingView
	extends PasVerzamelingView_Generated
{
	static public function tabel (PasVerzameling $passen, $enkellid = false)
	{
		if ($passen->aantal())
		{
			$tablediv = new HtmlDiv();

			$form = new HtmlForm();
			$form->add($table = new HtmlTable(null, 'sortable'));
			$table->add($thead = new HtmlTableHead())
				  ->add($tbody = new HtmlTableBody());
			$thead->add($row = new HtmlTableRow());
			if (!$enkellid)
				$row->makeHeaderFromArray(array(
					PasView::labelPersoon(),
					_('Lidnummer')));

			$row->makeHeaderFromArray(array(
				PasView::labelPasID(),
				PasView::labelActief(),
				PasView::labelVerwijderen()));

			$color = '#FF0000';
			$lidid = $passen->first()->getPersoon();

			foreach ($passen as $pas)
			{
				if ($lidid !== $pas->getPersoon()) {
					$lidid = $pas->getPersoon();
					if ($color == '#FF0000')
						$color = '#0000FF';
					else
						$color = '#FF0000';
				}
				$tbody->add(PasView::toTableRow($pas, $color, $enkellid));
			}
			$tablediv->add($form);
		}
		else
		{
			$tablediv = new HtmlDiv(new HtmlParagraph(_('Er zijn geen passen gevonden.')));
		}

		return $tablediv;
	}

	public static function persoon (PasVerzameling $passen, Persoon $persoon)
	{
		$body = self::tabel($passen, true);

		return Page::getInstance()->start(sprintf(_('Passsen van %s'), new HtmlAnchor(DibsInfo::geef($persoon)->url(), PersoonView::naam($persoon))))
			 ->add(PasView::nieuwePasLink($persoon->getContactID(), _('Nieuwe pas toevoegen')))
			 ->add($body)
			 ->end();
	}

	/**
	 * Toon de pagina met een overzicht van alle passen op een rijtje.
	 *
	 * @param PasVerzameling $passen De passen om te tonen. In de praktijk gewoon alle Pas-objecten.
	 *
	 * @return HTMLPage
	 */
	static public function overzicht (PasVerzameling $passen)
	{
		$body = self::tabel($passen->sorteer('Lidnr'));

		$page = Page::getInstance()
			 ->start(_('Passen'))
			 ->add(new HtmlHeader(2, _('Alle passen op een rijtje')))
			 ->add($body);

		return $page;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
