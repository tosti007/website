<?
/**
 * $Id$
 */
abstract class DibsTransactieView
	extends DibsTransactieView_Generated
{

	/**
	 * Geef een pagina met alle informatie voor een DibsTransactie.
	 *
	 * @param DibsTransactie $t De transactie om te tonen.
	 *
	 * @return HTMLPage
	 */
	static public function toon (DibsTransactie $t)
	{
		$artikelen = static::waardeArtikelen($t);

		$body = new HtmlDiv($par = new HtmlParagraph());
		$par->add(new HtmlStrong(static::labelPersoon($t) . ': '))
			->add(new HtmlSpan(new HtmlAnchor(
				DibsInfo::geef($t->getContactContactID())->url(),
				static::waardePersoon($t)) .
			   	' (' . static::waardePersoonContactID($t) . ')'))
			->add(new HtmlBreak())

			->add(new HtmlStrong(static::labelKudos($t) . ': '))
			->add(new HtmlSpan(static::waardeKudos($t)))
			->add(new HtmlBreak())

			->add(new HtmlStrong(_('Datum') . ':'))
			->add(new HtmlSpan(static::waardeDatum($t)))
			->add(new HtmlBreak())

			->add(new HtmlStrong(_('Tijd') . ':'))
			->add(new HtmlSpan(static::waardeTijd($t)))
			->add(new HtmlBreak());

		$par->add(new HtmlStrong(static::labelPas($t) . ':'))
			->add(new HtmlSpan(static::waardePas($t)))
			->add(new HtmlBreak());

		$par->add(new HtmlStrong(static::labelBron($t) . ':'))
			->add(new HtmlSpan(static::waardeBron($t)))
			->add(new HtmlBreak());

		if ($t->getUitleg())
		{
			$par->add(new HtmlStrong(static::labelUitleg($t) . ':'))
				->add(new HtmlSpan(static::waardeUitleg($t)))
				->add(new HtmlBreak());
		}

		$par->add(new HtmlStrong(static::labelArtikelen($t) . ':'))
			->add(new HtmlSpan($artikelen ?: new HtmlEmphasis(_('geen'))))
			->add(new HtmlBreak());

		$page = Page::getInstance()
			 ->start(_('Transactie') . ' ' . static::waardeTransactieID($t))
			 ->add(new HtmlHeader(2, (_('Transactie') . ' ' . static::waardeTransactieID($t))))
			 ->add($body);

		return $page;
	}

	/**
	 * Pagina om een DibsTransactie toe te voegen (vanuit de boekverkoop).
	 *
	 * @param DibsTransactie $transactie Een nieuw Transactie-object.
	 * @param bool $error Toon errors in het form.
	 *
	 * @return HTMLPage
	 */
	static public function toevoegen (DibsTransactie $transactie, $error)
	{
		$persoon = $transactie->getPersoon();

		$page = Page::getInstance()
			->start(sprintf(_('Nieuwe transactie voor %s'), PersoonView::naam($persoon)))
			->add(new HtmlDiv(_('Let op: voer alleen een transactie voor '
			. 'de Boekverkoop in als er daadwerkelijk iets gepind wordt, anders '
			. 'raakt de administratie in de war. Voor het terugdraaien van '
			. 'transacties en uitdelen van gratis kudos kies je Overige en '
			. 'voer je bij Uitleg in waar de transactie voor is.'), 'bs-callout bs-callout-info'));
		$page->add(self::nieuwForm('Nieuwe Transactie', $transactie, $error));

		return $page;
	}

	static public function nieuwForm ($name, DibsTransactie $transactie, $error)
	{
		$kudos = $transactie->getKudos();

		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($transactie, 'kudos', $error));

		$selected = ($kudos > 0) ? 'naarvereniging' : 'naarlid';
		$values = array('naarlid' => _('Naar lid'), 'naarvereniging' => _('Naar vereniging'));
		$div->add(self::makeFormRadiosRow('richting', $values, $selected, _('Richiting')));

		$selected  = ($transactie->getBron() == 'BVK') ? 'bvk' : 'www';
		$values = array('bvk' => _('Boekverkoop'), 'www' => _('Overig'));
		$div->add(self::makeFormRadiosRow('bron', $values, $selected, _('Bron')));

		$div->add(self::wijzigTR($transactie, 'uitleg', $error));

		$form->add(HtmlInput::makeHidden('persoonID', $transactie->getContactContactID()));
		$form->add(HtmlInput::makeFormSubmitButton(_('Voeg toe!')));

		return $form;
	}

	static public function processNieuwForm ()
	{
		switch (tryPar('richting'))
		{
			case 'naarlid':        $factor = -1; break;
			case 'naarvereniging': $factor =  1; break;
			default:               return false;
		}

		$kudos = tryPar('kudos', 0);
		$bron = strtoupper(tryPar('bron'));
		if (!in_array($bron, DibsTransactie::enumsBron()))
			return false;
		$uitleg = tryPar('uitleg', '');
		$persoon = Persoon::geef(tryPar('persoonID'));
		// Geen transacties met negatief aantal kudos accepteren;
		// een eventuele negatieve factor wordt er hieronder wel ingezet.
		if ($kudos <= 0)
			$kudos = 0;

		return new DibsTransactie('WWW', 'VERKOOP', $persoon, ($factor * $kudos) / EURO_TO_KUDOS, $uitleg, new DateTimeLocale(), $bron);
	}

	/**
	 * Geef de pagina om voor iemand (voornamelijk jezelf) kudos te kopen met iDEAL.
	 *
	 * @param DibsTransactie $obj
	 *
	 * @return null|Page
	 */
	static public function toevoegenIdeal (DibsTransactie $obj)
	{
		$page = Page::getInstance()
			->start(_('Kudos kopen'), 'transacties');

		if ($obj->getContactContactID() != Persoon::getIngelogd()->getContactID())
			Page::getInstance()->add(new HtmlParagraph(_('LET OP! Je staat op het punt om voor iemand anders kudos te kopen!'), 'error'));

		$transactiekosten = iDeal::transactiekosten()->alsFloat();
		$idealarray = array('500 kudos (€'.(5 + $transactiekosten).')'
				, '1000 kudos (€'.(10 + $transactiekosten).')'
				, '1500 kudos (€'.(15 + $transactiekosten).')'
				, '2000 kudos (€'.(20 + $transactiekosten).')');

		$page->add($form = HtmlForm::named('IDEAL Transactie'));
		$form->add(new HtmlParagraph(sprintf(_("Je betaalt €%s[VOC: dit zijn transactiekosten] "
				 . "transactiekosten. De kudos staan meteen op je account en "
				 . "zijn direct te gebruiken!"), $transactiekosten)));

		foreach($idealarray as $key => $ideal)
		{
			$form->add(new HtmlDiv('<label>'.HtmlInput::makeRadio('idealkudos', $key, $key == 0).' '.$ideal.'</label>'));
		}

		$form->add(HtmlInput::makeSubmitButton(_('Ga door naar betalen via iDEAL'), 'IdealTransactie[submit]'));

		return $page;
	}

	static public function processNieuwIdealForm (DibsTransactie $t) {
		if (tryPar('IdealTransactie[submit]', false)) {
			$array = array(500, 1000, 1500, 2000);
			$data = (int)tryPar('idealkudos', 0);
			$kudos = $array[$data];
			return new DibsTransactie($t->getRubriek()
				, $t->getSoort()
				, $t->getPersoon()
				, (-1 * $kudos) / EURO_TO_KUDOS
				, $t->getUitleg()
				, $t->getWanneer()
				, $t->getBron()
				, $t->getPas());
		} else
			return false;
	}

	static public function labelPersoon (DibsTransactie $obj = NULL)
	{
		return _('Persoon');
	}
	static public function labelKudos (DibsTransactie $obj = NULL)
	{
		return _('Kudos');
	}
	static public function labelPas (DibsTransactie $obj = NULL)
	{
		return _('Pas');
	}

	static public function opmerkingKudos()
	{
		return '';
	}
	static public function opmerkingUitleg()
	{
		return '';
	}

	static public function formKudos($obj)
	{
		$kudos = tryPar('kudos', $obj->getKudos());
		return HtmlInput::makeNumber('kudos', abs($kudos), 0);
	}

	static public function formUitleg(Transactie $obj, $include_id = false)
	{
		return HtmlInput::makeText('uitleg', $obj->getUitleg());
	}

	static public function waardePersoon (DibsTransactie $obj)
	{
		if ($persoon = $obj->getPersoon())
			return PersoonView::naam($persoon);
		return NULL;
	}
	static public function waardePersoonContactID (DibsTransactie $obj)
	{
		if ($persoon = $obj->getPersoon())
			return PersoonView::waardeContactID($persoon);
		return NULL;
	}
	static public function waardeKudos (DibsTransactie $obj)
	{
		return (int) $obj->getKudos();
	}
	static public function waardeAbsKudos (DibsTransactie $obj)
	{
		return (int) abs($obj->getKudos());
	}
	static public function waardeDatum (DibsTransactie $obj)
	{
		if ($moment = $obj->getWanneer())
			return $moment->format('Y-m-d');
		return NULL;
	}
	static public function waardeTijd (DibsTransactie $obj)
	{
		if ($moment = $obj->getWanneer())
			return $moment->format('H:i');
		return NULL;
	}
	static public function waardePas (DibsTransactie $obj)
	{
		if ($pas = $obj->getPas())
			return PasView::waardeRfidTag($pas);
		return new HtmlEmphasis(_('n.v.t.'));
	}
	static public function waardePasPasID (DibsTransactie $obj)
	{
		if ($pas = $obj->getPas())
			return PasView::waardePasID($pas);
		return 0;
	}
	static public function waardeBron (DibsTransactie $obj)
	{
		switch ($obj->getBron())
		{
		case 'WWW':
			return _('Website');
		case 'TABLET':
			return _('Tablet');
		case 'BVK':
			return _('Boekverkoop');
		case 'IDEAL':
			return _('iDEAL');
		default:
			return NULL;
		}
	}

	static public function formPersoon (DibsTransactie $obj, $include_id = false)
	{
		return new HtmlAnchor(DibsInfo::geef($obj->getPersoon())->url(), static::waardePersoon($obj));
	}

	static public function toTableRow (DibsTransactie $t, $enkellid = false)
	{
		$heeftrechten = hasAuth('bestuur');

		$row = new HtmlTableRow();
		if ($heeftrechten)
			$row->addData(new HtmlAnchor($t->url(), static::waardeTransactieID($t)));
		if (!$enkellid)
			$row->addData(new HtmlAnchor(DIBSBASE . '/' . $t->getContactContactID(), static::waardePersoonContactID($t)));

		$kudos = $t->getKudos();
		$kudossign = $enkellid
			? ($kudos > 0 ? -1 :  1)
			: ($kudos > 0 ?  1 : -1);
		$kudostekst = $kudossign == 1 ? _('BIJ') : _('AF');
		$row->addData(static::waardeAbsKudos($t), 'halignright')
			->setAttribute('sorttable_customkey', $kudos);
		$row->addData(new HtmlSpan($kudostekst, $kudossign == 1 ? 'positief' : 'negatief'))
			->setAttribute('sorttable_customkey', $kudos);
		$row->add(static::dateTimeTD($t->getWanneer()));
		if ($heeftrechten)
		{
			$row->addData(static::waardePas($t));
			$row->addData(static::waardeBron($t));
			$row->addData(static::waardeUitleg($t));
		}

		if ($enkellid)
			$row->addData(static::waardeArtikelen($t));

		return $row;
	}

	static public function nieuweTransactieLink ($persoonID, $titel)
	{
		return HtmlAnchor::button(DIBSBASE . "/$persoonID/Transacties/Nieuw", $titel);
	}

	static public function alsCSVArray(DibsTransactie $t)
	{
		$result = array('TransactieID' => static::waardeTransactieID($t)
				,'PersoonID' => static::waardePersoonContactID($t)
				,'Kudos' => static::waardeKudos($t)
				,'Wanneer' => $t->getWanneer()->format("Y-m-d H:i")
				,'PasID' => static::waardePasPasID($t)
				,'Bron' => static::waardeBron($t)
				,'Uitleg' => static::waardeUitleg($t)
				);

		return $result;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
