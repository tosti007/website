<?
/**
 * $Id$
 */
abstract class DibsProductView
	extends DibsProductView_Generated
{
	static public function wijzigTable($obj, $nieuw = false, $show_error)
	{
		$tbody = parent::wijzigTable($obj, $nieuw, $show_error);

		if($nieuw)
			$tbody->add(self::wijzigTR($obj, 'kudos', $show_error));
		else
			$tbody->add(self::infoTR($obj, 'kudos'));

		return $tbody;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
