<?
/**
 * $Id$
 */
abstract class DibsTransactieVerzamelingView
	extends DibsTransactieVerzamelingView_Generated
{

	static public function tabel (DibsTransactieVerzameling $transacties, $van, $tot, $enkellid)
	{
		$outerdiv = new HtmlDiv();
		$heeftrechten = hasAuth("bestuur");

		if (!$enkellid)
		{
			$totalen = DibsTransactieVerzameling::getTotalen($van, $tot);
			$bedragbegin = -1 * DibsTransactieVerzameling::getBedragBegin($van);

			$outerdiv->add($totalentable = new HtmlTable());
			$totalentable->add($row = new HtmlTableRow());
			$row->addData(new HtmlStrong(_('Kudos in omloop aan het begin van de maand:&nbsp')));
			$row->addData(EURO_TO_KUDOS * $bedragbegin, 'halignright');

			$totalentable->add($row = new HtmlTableRow());
			$row->addData(new HtmlStrong(_('Totaal') . '&nbsp'
				. new HtmlSpan(_('BIJ'), 'positief') . '&nbsp;'
				. _('(gekochte producten)') . ':&nbsp;'));
			$row->addData(EURO_TO_KUDOS *  abs($totalen[1]), 'halignright');
			$row->addData();

			$row->addData(new HtmlStrong(_('Aantal kudos gekocht in boekverkoop:')));
			$row->addData(EURO_TO_KUDOS * abs($totalen[3]), 'halignright');

			$totalentable->add($row = new HtmlTableRow());
			$row->addData(new HtmlStrong(_('Totaal') . '&nbsp'
				. new HtmlSpan(_('AF'), 'negatief') . '&nbsp;'
				. _('(verkochte kudos)') . ':&nbsp;'));
			$row->addData(EURO_TO_KUDOS * abs($totalen[0]), 'halignright');
			$row->addData();

			$row->addData(new HtmlStrong(_('Aantal kudos gekocht met iDEAL:')));
			$row->addData(EURO_TO_KUDOS * abs($totalen[4]), 'halignright');

			$totalentable->add($row = new HtmlTableRow());
			$row->addData(new HtmlStrong(_('Totaal') . ':'));
			$row->addData(EURO_TO_KUDOS * abs($totalen[2]), 'halignright');
			$row->addData($totalen[2] <= 0
				? new HtmlSpan(_('AF'), 'negatief')
				: new HtmlSpan(_('BIJ'), 'positief'));

			$row->addData(new HtmlStrong(_('Totaal aantal kudos gekocht:')));
			$row->addData(EURO_TO_KUDOS * abs($totalen[5]), 'halignright');

			$outerdiv->add(new HtmlBreak());
		}

		if ($transacties->aantal())
		{
			$outerdiv->add($tablediv = new HtmlDiv());

			$form = new HtmlForm();
			$form->add($table = new HtmlTable(null, 'sortable'));
			$table->add($thead = new HtmlTableHead())
				  ->add($tbody = new HtmlTableBody());
			$thead->add($row = new HtmlTableRow());

			if ($heeftrechten)
				$row->addHeader(_('ID'));

			if (!$enkellid)
				$row->addHeader(DibsTransactieView::labelPersoon());

			$row->makeHeaderFromArray(array(
				DibsTransactieView::labelKudos(),
				NULL,
				DibsTransactieView::labelWanneer()));

			if ($heeftrechten)
				$row->makeHeaderFromArray(array(
					DibsTransactieView::labelPas(),
					_('Bron'),
					DibsTransactieView::labelUitleg()));

			if ($enkellid)
				$row->addHeader(DibsTransactieView::labelArtikelen());

			foreach ($transacties as $t)
			{
				$tbody->add(DibsTransactieView::toTableRow($t, $enkellid));
			}
			$tablediv->add($form);
		}
		else
		{
			$outerdiv->add(new HtmlParagraph(_('Er zijn geen transacties gevonden.')));
		}

		return $outerdiv;
	}

	/**
	 * Toon een overzicht van alle transacties in de verzameling.
	 *
	 * De parameters $maand en $jaar zijn voor het genereren van knoppen om naar andere tijdstippen te kijken.
	 * Als je wilt dat de gebruiker het snapt, dan wil je dat $transacties de verzameling is van alle transacties met gegeven maand en jaar,
	 * maar in principe gaat er niets stuk als dit niet zo is.
	 *
	 * @param DibsTransactieVerzameling $transacties De transacties om te tonen.
	 * @param string $maand De maand waarvoor $transacties de verzameling van alle transacties is.
	 * @param string $jaar Het jaar waarvoor $transacties de verzameling van alle transacties is.
	 *
	 * @return HTMLPage
	 */
	static public function dibsTransactieoverzicht (DibsTransactieVerzameling $transacties, $maand, $jaar)
	{
		global $MAANDEN;
		$jaren = array();
	   	foreach(range(DIBS_STARTJAAR, date('Y')) as $j)
		{
			$jaren[$j] = "$j";
		}

		$body = new HtmlDiv();

		$topdiv = new HtmlDiv($form = new HtmlForm('get'));
		$maandselect = HtmlSelectbox::fromArray('maand', $MAANDEN, $maand, 1, true)
			->setMultiple(false);
		$jaarselect = HtmlSelectbox::fromArray('jaar', $jaren, $jaar, 1, true)
			->setMultiple(false);

		$form->add($maandselect)
			 ->add($jaarselect);

		$form->add(
				HtmlAnchor::button(DIBSBASE . "/Transacties/Transacties?maand=$maand&jaar=$jaar",
				_('Download als CSV'))
			);

		$van = new DateTimeLocale("$jaar-$maand-01");
		$tot = clone $van;
		$tot->add(new DateInterval("P1M"));

		$tablediv = self::tabel($transacties, $van, $tot, false);

		$body->add($topdiv)
			 ->add($tablediv);

		$page = Page::getInstance()
			 ->start(_('Transacties') . ": $MAANDEN[$maand] $jaar")
			 ->add($body);

		return $page;
	}

	static public function persoon (DibsTransactieVerzameling $transacties, DibsInfo $info)
	{
		$prefix = _('Transacties van %s');
		$header = sprintf($prefix, DibsInfoView::waardePersoon($info));
		$titel = sprintf($prefix, DibsInfoView::maakLink($info));
		$body = self::tabel($transacties, NULL, NULL, true);

		return Page::getInstance()
			 ->start($header)
			 ->add(new HtmlHeader(2, $titel))
			 ->add($body)
			 ->end();
	}

	static public function csv($ts){

		$csvarray = array();
		foreach($ts as $t){
			$csvarray[] = DibsTransactieView::alsCSVArray($t);
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		if(count($csvarray) > 0){
			fputcsv($output, array_keys($csvarray[0]));
		} else {
			fwrite($output, _("Geen data."));
		}

		//Print alle inhoud voor de CSV
		foreach($csvarray as $t){
			fputcsv($output, $t);
		}

		fclose($output);

	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
