<?
/**
 * $Id$
 */
abstract class PasView
	extends PasView_Generated
{

	static public function toTableRow (Pas $pas, $color, $enkellid = false)
	{
		$persoon = $pas->getPersoon();
		/*Voor het geval er een pas zonder DibsInfo is
		 * misschien dat dit kan gebeuren in de toekomst
		 * als er mensen lidaf worden */
		if (!($info = DibsInfo::geef($persoon)))
			return;

		$actieve = $pas->getActief();

		$row = new HtmlTableRow();
		if (!$enkellid)
		{
			$row->addData(new HtmlAnchor($info->url(), static::waardePersoon($pas)));
			$cell = $row->addData($persoon->getContactID());
			$cell->setCssStyle("color: $color");
		}
		$row->addData(static::waardeRfidTag($pas));
		$row->addData(
			new HtmlAnchor($pas->url('Wijzig'),
			HtmlSpan::maakBoolPicto($actieve))
		);
		$row->addData($pas->isGebruikt()
			? new HtmlEmphasis(_('Pas reeds in gebruik'))
			: new HtmlAnchor($pas->url('Verwijder'), _('Verwijder pas'))
		);

		return $row;
	}

	static public function nieuwePasLink ($persoonID, $titel) {
		return HtmlAnchor::button(DIBSBASE . "/$persoonID/Passen/Nieuw", $titel);
	}

	/**
	 * Geef de wijzigpagina voor het toevoegen van een Pas.
	 *
	 * De Pas moet bij een Persoon met DibsInfo horen.
	 *
	 * @param Pas $obj Een nieuw Pas-object.
	 * @param bool $show_error Moeten errors in het form getoond worden?
	 *
	 * @return bool|HtmlPage
	 */
	static public function toevoegen (Pas $obj, $show_error)
	{
		$persoon = $obj->getPersoon();
		if (!($info = DibsInfo::geef($persoon)))
			return false;

		$page = Page::getInstance()
			->start(sprintf(_('Nieuwe pas voor %s'),
				new HtmlAnchor($info->url(), static::waardePersoon($obj))));

		$page->add($form = HtmlForm::named('Nieuwe Pas'));

		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($obj, 'Persoon', $show_error));
		$div->add(self::wijzigTR($obj, 'rfidTag', $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_('Voeg toe!')));

		return $page;
	}

	static public function formPersoon (Pas $obj, $include_id = false) {
		return new HtmlAnchor(DibsInfo::geef($obj->getPersoon())->url(), static::waardePersoon($obj));
	}

	static public function labelPasID (Pas $obj = NULL)
	{
		return _('Pasnummer');
	}
	static public function labelPersoon (Pas $obj = NULL)
	{
		return _('Persoon');
	}
	static public function labelRfidTag (Pas $obj = NULL)
	{
		return _('RFID-Tag');
	}
	static public function labelActief (Pas $obj = NULL)
	{
		return _('Actief');
	}
	static public function labelVerwijderen (Pas $obj = NULL)
	{
		return _('Verwijderen');
	}

	static public function waardePersoon(Pas $obj)
	{
		if ($persoon = $obj->getPersoon())
			return PersoonView::naam($persoon);
		return NULL;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
