<?
abstract class DibsTransactieView_Generated
	extends TransactieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DibsTransactieView.
	 *
	 * @param DibsTransactie $obj Het DibsTransactie-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDibsTransactie(DibsTransactie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bron.
	 *
	 * @param DibsTransactie $obj Het DibsTransactie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld bron labelt.
	 */
	public static function labelBron(DibsTransactie $obj)
	{
		return 'Bron';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld bron.
	 *
	 * @param string $value Een enum-waarde van het veld bron.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumBron($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld bron horen.
	 *
	 * @see labelenumBron
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld bron
	 * representeren.
	 */
	public static function labelenumBronArray()
	{
		$soorten = array();
		foreach(DibsTransactie::enumsBron() as $id)
			$soorten[$id] = DibsTransactieView::labelenumBron($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld bron.
	 *
	 * @param DibsTransactie $obj Het DibsTransactie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bron van het object obj
	 * representeert.
	 */
	public static function waardeBron(DibsTransactie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Bron');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bron.
	 *
	 * @see genericFormbron
	 *
	 * @param DibsTransactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bron staat en kan worden
	 * bewerkt. Indien bron read-only is betreft het een statisch html-element.
	 */
	public static function formBron(DibsTransactie $obj, $include_id = false)
	{
		return static::waardeBron($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bron. In tegenstelling
	 * tot formbron moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formbron
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bron staat en kan worden
	 * bewerkt. Indien bron read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBron($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bron
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bron representeert.
	 */
	public static function opmerkingBron()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld pas.
	 *
	 * @param DibsTransactie $obj Het DibsTransactie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld pas labelt.
	 */
	public static function labelPas(DibsTransactie $obj)
	{
		return 'Pas';
	}
	/**
	 * @brief Geef de waarde van het veld pas.
	 *
	 * @param DibsTransactie $obj Het DibsTransactie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld pas van het object obj
	 * representeert.
	 */
	public static function waardePas(DibsTransactie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPas())
			return NULL;
		return PasView::defaultWaardePas($obj->getPas());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld pas.
	 *
	 * @see genericFormpas
	 *
	 * @param DibsTransactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld pas staat en kan worden
	 * bewerkt. Indien pas read-only is betreft het een statisch html-element.
	 */
	public static function formPas(DibsTransactie $obj, $include_id = false)
	{
		return static::waardePas($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld pas. In tegenstelling
	 * tot formpas moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formpas
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld pas staat en kan worden
	 * bewerkt. Indien pas read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormPas($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld pas
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld pas representeert.
	 */
	public static function opmerkingPas()
	{
		return NULL;
	}
}
