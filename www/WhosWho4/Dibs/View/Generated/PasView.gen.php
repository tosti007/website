<?
abstract class PasView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PasView.
	 *
	 * @param Pas $obj Het Pas-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePas(Pas $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld pasID.
	 *
	 * @param Pas $obj Het Pas-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld pasID labelt.
	 */
	public static function labelPasID(Pas $obj)
	{
		return 'PasID';
	}
	/**
	 * @brief Geef de waarde van het veld pasID.
	 *
	 * @param Pas $obj Het Pas-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld pasID van het object obj
	 * representeert.
	 */
	public static function waardePasID(Pas $obj)
	{
		return static::defaultWaardeInt($obj, 'PasID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld pasID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld pasID representeert.
	 */
	public static function opmerkingPasID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rfidTag.
	 *
	 * @param Pas $obj Het Pas-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rfidTag labelt.
	 */
	public static function labelRfidTag(Pas $obj)
	{
		return 'RfidTag';
	}
	/**
	 * @brief Geef de waarde van het veld rfidTag.
	 *
	 * @param Pas $obj Het Pas-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rfidTag van het object obj
	 * representeert.
	 */
	public static function waardeRfidTag(Pas $obj)
	{
		return static::defaultWaardeString($obj, 'RfidTag');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rfidTag.
	 *
	 * @see genericFormrfidTag
	 *
	 * @param Pas $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rfidTag staat en kan
	 * worden bewerkt. Indien rfidTag read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRfidTag(Pas $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'RfidTag', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rfidTag. In
	 * tegenstelling tot formrfidTag moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formrfidTag
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rfidTag staat en kan
	 * worden bewerkt. Indien rfidTag read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRfidTag($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'RfidTag', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld rfidTag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rfidTag representeert.
	 */
	public static function opmerkingRfidTag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Pas $obj Het Pas-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Pas $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Pas $obj Het Pas-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Pas $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param Pas $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(Pas $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Persoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld actief.
	 *
	 * @param Pas $obj Het Pas-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld actief labelt.
	 */
	public static function labelActief(Pas $obj)
	{
		return 'Actief';
	}
	/**
	 * @brief Geef de waarde van het veld actief.
	 *
	 * @param Pas $obj Het Pas-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld actief van het object obj
	 * representeert.
	 */
	public static function waardeActief(Pas $obj)
	{
		return static::defaultWaardeBool($obj, 'Actief');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld actief.
	 *
	 * @see genericFormactief
	 *
	 * @param Pas $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actief staat en kan worden
	 * bewerkt. Indien actief read-only is betreft het een statisch html-element.
	 */
	public static function formActief(Pas $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Actief', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld actief. In
	 * tegenstelling tot formactief moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formactief
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actief staat en kan worden
	 * bewerkt. Indien actief read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormActief($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Actief');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld actief
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld actief representeert.
	 */
	public static function opmerkingActief()
	{
		return NULL;
	}
}
