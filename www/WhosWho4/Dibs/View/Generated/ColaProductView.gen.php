<?
abstract class ColaProductView_Generated
	extends ArtikelView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ColaProductView.
	 *
	 * @param ColaProduct $obj Het ColaProduct-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeColaProduct(ColaProduct $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld volgorde.
	 *
	 * @param ColaProduct $obj Het ColaProduct-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld volgorde labelt.
	 */
	public static function labelVolgorde(ColaProduct $obj)
	{
		return 'Volgorde';
	}
	/**
	 * @brief Geef de waarde van het veld volgorde.
	 *
	 * @param ColaProduct $obj Het ColaProduct-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld volgorde van het object obj
	 * representeert.
	 */
	public static function waardeVolgorde(ColaProduct $obj)
	{
		return static::defaultWaardeInt($obj, 'Volgorde');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld volgorde.
	 *
	 * @see genericFormvolgorde
	 *
	 * @param ColaProduct $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld volgorde staat en kan
	 * worden bewerkt. Indien volgorde read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVolgorde(ColaProduct $obj, $include_id = false)
	{
		return static::waardeVolgorde($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld volgorde. In
	 * tegenstelling tot formvolgorde moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvolgorde
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld volgorde staat en kan
	 * worden bewerkt. Indien volgorde read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVolgorde($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * volgorde bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld volgorde representeert.
	 */
	public static function opmerkingVolgorde()
	{
		return NULL;
	}
}
