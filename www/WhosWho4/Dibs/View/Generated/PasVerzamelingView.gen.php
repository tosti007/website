<?
abstract class PasVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PasVerzamelingView.
	 *
	 * @param PasVerzameling $obj Het PasVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePasVerzameling(PasVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
