<?
abstract class DibsInfoView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DibsInfoView.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDibsInfo(DibsInfo $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(DibsInfo $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(DibsInfo $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kudos.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld kudos labelt.
	 */
	public static function labelKudos(DibsInfo $obj)
	{
		return 'Kudos';
	}
	/**
	 * @brief Geef de waarde van het veld kudos.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kudos van het object obj
	 * representeert.
	 */
	public static function waardeKudos(DibsInfo $obj)
	{
		return static::defaultWaardeInt($obj, 'Kudos');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kudos.
	 *
	 * @see genericFormkudos
	 *
	 * @param DibsInfo $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kudos staat en kan worden
	 * bewerkt. Indien kudos read-only is betreft het een statisch html-element.
	 */
	public static function formKudos(DibsInfo $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Kudos', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kudos. In
	 * tegenstelling tot formkudos moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formkudos
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kudos staat en kan worden
	 * bewerkt. Indien kudos read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormKudos($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Kudos');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld kudos
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kudos representeert.
	 */
	public static function opmerkingKudos()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld laatste.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld laatste labelt.
	 */
	public static function labelLaatste(DibsInfo $obj)
	{
		return 'Laatste';
	}
	/**
	 * @brief Geef de waarde van het veld laatste.
	 *
	 * @param DibsInfo $obj Het DibsInfo-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld laatste van het object obj
	 * representeert.
	 */
	public static function waardeLaatste(DibsInfo $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLaatste())
			return NULL;
		return DibsTransactieView::defaultWaardeDibsTransactie($obj->getLaatste());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld laatste.
	 *
	 * @see genericFormlaatste
	 *
	 * @param DibsInfo $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld laatste staat en kan
	 * worden bewerkt. Indien laatste read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLaatste(DibsInfo $obj, $include_id = false)
	{
		return DibsTransactieView::defaultForm($obj->getLaatste());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld laatste. In
	 * tegenstelling tot formlaatste moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlaatste
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld laatste staat en kan
	 * worden bewerkt. Indien laatste read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLaatste($name, $waarde=NULL)
	{
		return DibsTransactieView::genericDefaultForm('Laatste');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld laatste
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld laatste representeert.
	 */
	public static function opmerkingLaatste()
	{
		return NULL;
	}
}
