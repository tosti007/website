<?
abstract class DibsTransactieVerzamelingView_Generated
	extends TransactieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DibsTransactieVerzamelingView.
	 *
	 * @param DibsTransactieVerzameling $obj Het DibsTransactieVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDibsTransactieVerzameling(DibsTransactieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
