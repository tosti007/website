<?
/**
 * $Id$
 */
abstract class DibsInfoView
	extends DibsInfoView_Generated
{
	static public function toonInfo(DibsInfo $info, $toonTransacties = false)
	{
		$persoon = $info->getPersoon();
		$transactieHeader = _('Laatste %d transacties');
		$passen = PasVerzameling::getPersoonPassen($info->getPersoon());
		$passen = $passen->filter("getActief", true);

		$body = new HtmlDiv();
		$body->add($row = new HtmlDiv($col = new HtmlDiv(null, 'col-md-6'), 'row'));
		$col->add(new HtmlSpan(HtmlAnchor::button($info->url() . '/iDeal', _('Kudos kopen'))));
		$col->add(new HtmlBreak());
		$col->add(new HtmlSpan(new HtmlStrong(_('Kudos') . ':&nbsp;') . static::waardeKudos($info)));

		if (hasAuth('bestuur'))
		{
			$row->add($col = new HtmlDiv(null, 'col-md-6'));
			$col->add(DibsTransactieView::nieuweTransactieLink($persoon->getContactID(), _('Transactie toevoegen')));
			$col->add(new HtmlBreak());
			$col->add(new HtmlBreak());

			$col->add($span = new HtmlSpan(new HtmlStrong(_('Actieve passen') . ':&nbsp')));

			foreach($passen as $pas) {
				$span->add(PasView::waardeRfidTag($pas));
			}

			$col->add(new HtmlSpan('(' .
				new HtmlAnchor($info->url('Passen'), _('toon alle passen')) .
				')'));
			$col->add(new HtmlBreak());
			$col->add(PasView::nieuwePasLink($persoon->getContactID(), _('Nieuwe pas toevoegen')));

			$body->add(new HtmlBreak());

			$transactieHeader .= ' (' . new HtmlAnchor($info->url('Transacties'), _('toon alle')) . ')';
		}

		$body->add(new HtmlHeader(3, sprintf($transactieHeader, DIBS_TOON_AANTAL_TRANSACTIES)));
		$transacties = DibsTransactieVerzameling::getPersoonTransacties($persoon, DIBS_TOON_AANTAL_TRANSACTIES);
		if ($toonTransacties)
			$body->add(DibsTransactieVerzamelingView::tabel($transacties, NULL, NULL, true));

		return $body;
	}

	/**
	 * Geef de HTMLPagina met DiBS-informatie voor een persoon.
	 *
	 * @param DibsInfo $info De informatie om te tonen.
	 * @param bool $toonTransacties Moeten ook alle transacties erbij staan?
	 *
	 * @see toonInfo
	 *
	 * @return null|Page
	 */
	public static function toon (DibsInfo $info, $toonTransacties = false)
	{
		$persoon = $info->getPersoon();
		return Page::getInstance()
			 ->start(sprintf(('DiBS-informatie over %s'), PersoonView::naam($persoon)))
			 ->add(self::toonInfo($info, $toonTransacties));
	}

	/**
	 * Toon de melding dat de persoon nog geen DiBS heeft en dus de relevante info niet kan zien.
	 *
	 * @param Persoon $persoon De Persoon om deze pagina over te tonen.
	 *
	 * @return null|Page
	 * @throws HtmlElementImmutableException
	 */
	public static function toonGeenDibs (Persoon $persoon)
	{
		$body = new HtmlDiv($par = new HtmlParagraph());

		$par->add(new HtmlSpan(sprintf(_('Je bent nog niet geactiveerd voor het %s.'), new HtmlAnchor(DIBSBASE, 'DiBS'))))
			->add(new HtmlBreak())
			->add(new HtmlSpan(_('Wil je dit wel, vraag dit dan bij het bestuur.')))
			->add(new HtmlBreak())
			->add(new HtmlSpan(_('DiBS is het Digitaal-BetaalSysteem van A–Eskwadraat waarmee je producten uit de colakas kunt kopen.')))
			->add(new HtmlBreak())
			->add(new HtmlSpan(sprintf(_('Voor meer informatie over DiBS kun je %s klikken.'), new HtmlAnchor(DIBSBASE, 'hier'))));

		$titel = sprintf(_('Dibsinformatie van %s'),
			new HtmlAnchor('/Leden/' . $persoon->geefID(), PersoonView::naam($persoon)));

		return Page::getInstance()
			 ->start(_('Dibsinformatie'))
			 ->add(new HtmlHeader(2, $titel))
			 ->add($body);
	}

	static public function toTable (DibsInfo $d) {
		$row = new HtmlTableRow();
		$row->addData(static::maakLink($d));
		$row->addData(new HtmlAnchor(
			'/Leden/' . $d->getPersoonContactID()
			, $d->getPersoonContactID()
		));
		$passen = PasVerzameling::getPersoonPassen($d->getPersoon());
		$passen = $passen->filter('getActief', true);
		$span = new HtmlSpan();
		foreach($passen as $pas) {
			$span->add(PasView::waardeRfidTag($pas));
		}
		$row->addData($span);
		$row->addData(static::waardeKudos($d));
		$row->addData(
			$d->getLaatste()
			? new HtmlAnchor($d->getLaatste()->url(), static::waardeLaatste($d))
			: NULL
		);
		if (hasAuth('god'))
		{
			$row->addData(static::waardeGewijzigdWanneer($d));
			$row->addData($d->getGewijzigdWie()
				? PersoonView::naam(Persoon::geef($d->getGewijzigdWie()))
				: NULL
			);
		}
		return $row;
	}

	/**
	 * Toon het quickform om nieuwe DiBSInfo aan iemand te koppelen.
	 *
	 * @param DibsInfo $dibsInfo Een nieuw DibsInfo-object.
	 * @param bool $show_error Of errors in het form getoond moeten worden.
	 *
	 * @return HTMLPage
	 */
	static public function toevoegenQuickform(DibsInfo $dibsInfo, $show_error = false) {
		$page = Page::getInstance()
			->start(sprintf(_('Quickform voor %s'), static::waardePersoon($dibsInfo)));
		$page->add(self::quickform('Nieuw Quickform', $dibsInfo, $show_error));
		return $page;
	}

	static public function quickform ($name = 'Nieuw Quickform', DibsInfo $d, $show_error = true)
	{
		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($d, 'Persoon', $show_error));

		$div->add(self::makeFormStringRow('rfidTag', '', _('Pas')));
		$form->add(HtmlInput::makeFormSubmitbutton(_('Genereer!')));

		return $form;
	}

	static public function processQuickform (DibsInfo $d, Pas $p) {
		$p->setRfidTag(tryPar('rfidTag', 0));
	}

	static public function formPersoon(DibsInfo $d, $include_id = false) {
		return new HtmlAnchor('/Leden/' . $d->getPersoonContactID(), static::waardePersoon($d));
	}

	/**
	 * Geef een pagina met allerlei DiBS-statistieken.
	 *
	 * @return HTMLPage
	 */
	static public function statistieken ()
	{
		global $WSW4DB;
		$dibsinfo = DibsInfoVerzameling::getDibsers();
		$passen = PasVerzameling::getPassen();
		$colaproducten = ColaProductVerzameling::getColaproducten();

		$aantalverkopen = -$WSW4DB->q('MAYBEVALUE SELECT SUM(`VoorraadMutatie`.`aantal`) FROM 
			(`Verkoop` JOIN `VoorraadMutatie` ON `Verkoop`.`voorraadMutatieID`=`VoorraadMutatie`.`voorraadMutatieID`)
		  JOIN `DibsTransactie` ON `Verkoop`.`transactie_transactieID`=`DibsTransactie`.`transactieID`');

		$tarray = array();
		$tijd = getdate();

		// We zoeken steeds transacties binnen een bepaalde grens
		// in principe is dit de bovengrens
		$tot = new DateTimeLocale("$tijd[year]-$tijd[mon]-$tijd[mday]");
		$tot->add(new DateInterval('P1D'));

		// Omdat we steeds dezelfde intervallen hanteren, stop ze gewoon in een lijstje
		$beginPunten = array();
		$beginPunten[] = new DateTimeLocale("0-01-01");
		$beginPunten[] = new DateTimeLocale(($tijd['year'] - 1) . "-9-01");
		$beginPunten[] = new DateTimeLocale(($tijd['year'] - 1) . "-8-01");
		$beginPunten[] = new DateTimeLocale("$tijd[year]-$tijd[mon]-01");
		$van = clone $tot;
		$van->sub(new DateInterval("P$tijd[wday]D"));
		$beginPunten[] = $van;

		$tsize = count($beginPunten);

		// bepaal welk jaar de college/boekjaren eigenlijk zijn
		$cjaren = ($tijd['mon'] >= 9) ? "$tijd[year]-" . ($tijd['year'] + 1) : ($tijd['year'] - 1) . "-$tijd[year]";
		$bjaren = ($tijd['mon'] >= 8) ? "$tijd[year]-" . ($tijd['year'] + 1) : ($tijd['year'] - 1) . "-$tijd[year]";

		// tel het totaal aantal transacties in de tijdsintervallen
		$taantal = array();
		foreach ($beginPunten as $van) {
			$taantal[] = DibsTransactieVerzameling::telTransacties($van, $tot);
		}
		$aantaltransacties = $taantal[0];

		$page = Page::getInstance()
			 ->addFooterJS(Page::minifiedFile('Chart.js', 'js'))
			 ->start(_("Statistieken"), 'Statistieken')
			 ->add(new HtmlParagraph(sprintf(_('Aantal DiBSende leden: %s'), $dibsinfo->aantal())))
			 ->add(new HtmlParagraph(sprintf(_('Aantal passen: %s'), $passen->aantal())));
		$page->add($tpar = new HtmlParagraph(sprintf(_('Aantal transacties tot nu toe: %s') ,$taantal[0])));
		$tpar->add(new HtmlBreak())
			 ->add(new HtmlList(false, Array(
			 sprintf(_("waarvan in het collegejaar $cjaren: %s") ,$taantal[1]),
			 sprintf(_("waarvan in het boekjaar $bjaren: %s") ,$taantal[2]),
			 sprintf(_('waarvan in deze maand: %s') ,$taantal[3]),
			 sprintf(_('waarvan in deze week: %s') ,$taantal[4]))));

		// tel het aantal kudos in die tijdsintervallen gekocht
		$tkudos = array();
		foreach ($beginPunten as $van) {
			$tkudos[] = DibsTransactieVerzameling::telKudosGekocht($van, $tot);
		}
		$page->add($tpar = new HtmlParagraph(sprintf(_('Aantal kudos door leden gekocht: %s'), $tkudos[0])));
		$tpar->add(new HtmlBreak())
			 ->add(new HtmlList(false, Array(
			 sprintf(_("waarvan in het collegejaar $cjaren: %s"), $tkudos[1]),
			 sprintf(_("waarvan in het boekjaar $bjaren: %s"), $tkudos[2]),
			 sprintf(_('waarvan in deze maand: %s'), $tkudos[3]),
			 sprintf(_('waarvan in deze week: %s'), $tkudos[4]))));

		// tel het aantal kudos in die tijdsintervallen betaald
		$tkudos = array();
		foreach ($beginPunten as $van) {
			$tkudos[] = DibsTransactieVerzameling::telKudosBetaald($van, $tot);
		}
		$page->add($tpar = new HtmlParagraph(sprintf(_('Aantal kudos betaald door leden: %s'), $tkudos[0])));
		$tpar->add(new HtmlBreak())
			 ->add(new HtmlList(false, Array(
			 sprintf(_("waarvan in het collegejaar $cjaren: %s"), $tkudos[1]),
			 sprintf(_("waarvan in het boekjaar $bjaren: %s"), $tkudos[2]),
			 sprintf(_('waarvan in deze maand: %s'), $tkudos[3]),
			 sprintf(_('waarvan in deze week: %s'), $tkudos[4]))));
		$page->add(new HtmlParagraph(sprintf(_('Aantal colaproducten: %s'), $colaproducten->aantal())))
			 ->add(new HtmlParagraph(sprintf(_('Aantal producten verkocht: %s'), $aantalverkopen)))
			 ->add(new HtmlParagraph(sprintf(_('Gemiddeld aantal producten per transactie: %s'), round($aantalverkopen / $aantaltransacties,2))));

		if(hasAuth('bestuur'))
		{
			// Geef een lijstje van de mensen die al lang geen transacties hebben
			$page->add(new HtmlHeader(3, _('Overzicht oud-DiBSers')));
			$page->add(new HtmlParagraph(_('Deze leden hebben al heel lang (dat wil zeggen, minstens vijf jaar) niet van hun DiBS gebruik gemaakt.')));
			// vijf jaar geleden
			$tot = new DateTimeLocale("$tijd[year]-$tijd[mon]-$tijd[mday]");
			$tot->sub(new DateInterval('P5Y'));
			$laatsteTransacties = DibsTransactieVerzameling::getLaatsteTransacties($tot);

			$table = new HtmlTable(null, 'sortable');
			$table->add($thead = new HtmlTableHead())
				  ->add($tbody = new HtmlTableBody());
			$thead->add($row = new HtmlTableRow());
			$row->addHeader(_('Laatste transactie'));
			$row->addHeader(DibsTransactieView::labelPersoon());
			$row->addHeader(_('Kudos op voorraad'));

			$iemandGevonden = false;
			foreach ($laatsteTransacties as $transactie) {
				$info = DibsInfo::geef($transactie->getContactContactID());
				// Boeit niets want dit lid heeft geen kudo's openstaan
				if ($info->getKudos() == 0) {
					continue;
				}

				$iemandGevonden = true;

				$tbody->add($row = new HtmlTableRow());
				$row->add(static::dateTimeTD($transactie->getWanneer()));
				$row->addData(DibsInfoView::maakLink($info));
				$row->addData(DibsInfoView::waardeKudos($info));
			}

			$tbody->add($rij = new HtmlTableRow());
			$rij->add(new HtmlTableDataCell(HtmlAnchor::button("/Leden/Dibs/OudeDibs", _("Verwijder al deze oude kudos"))));

			if ($iemandGevonden) {
				$page->add($table);
			} else {
				$page->add(new HtmlParagraph(_("Iedereen heeft DiBS in de afgelopen vijf jaar gebruikt! Feestje!")));
			}
		}
		

		$time = new DateTimeLocale(date('Y') . '-' . date('m'));
		$graphx = array();
		for($i = 0; $i < 12; $i++) {
			$graphx[($time->format('Y-m'))] = $i;
			$time->sub(new DateInterval('P1M'));
		}

		//$graphx = implode(",", array_keys(array_reverse($graphx)));

		$stats = array(
			"transactiesVerkocht" => array(
				"title" => "Transacties van verkochte kudos per maand",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(false, false, true)
			), "transactiesVerkochtCum" => array(
				"title" => "Cumulatief aantal transacties van verkochte kudos",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(true,  false, true)
			), "transactiesGekregen" => array(
				"title" => "Transacties van gekregen kudos per maand",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(false, false, false)
			), "transactiesGekregenCum" => array(
				"title" => "Cumulatief aantal transacties van gekregen kudos",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(true,  false, false)
			), "kudosVerkocht" => array(
				"title" => "Kudos verkocht per maand",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(false, true, true )
			), "kudosVerkochtCum" => array(
				"title" => "Cumulatief aantal kudos verkocht",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(true,  true, true )
			), "kudosGekregen" => array(
				"title" => "Kudos gekregen per maand",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(false, true, false)
			), "kudosGekregenCum" => array(
				"title" => "Cumulatief aantal gekregen",
				"data" => DibsTransactieVerzameling::arrayOfTransacties(true,  true, false)
			), "winst" => array(
				"title" => "'Winst' per maand'",
				"data" => NULL
			), "winstCum" => array(
				"title" => "Cumulatieve 'winst' per maand (kudos in omloop)",
				"data" => NULL
			), "gemTransVerkocht" => array(
				"title" => "Gemiddelde per transactie van verkochte kudos per maand",
				"data" => NULL
			), "gemTransVerkochtCum" => array(
				"title" => "Cumulatief gemiddelde per transactie van verkochte kudos per maand",
				"data" => NULL
			), "gemTransGekregen" => array(
				"title" => "Gemiddelde per transactie van gekregen kudos per maand",
				"data" => NULL
			), "gemTransGekregenCum" => array(
				"title" => "Cumulatief gemiddelde per transactie van gekregen kudosper maand",
				"data" => NULL
			)
		);

		$stats["winst"]["data"] = array_map(
			function($x, $y) { return $x - $y; },
			$stats["kudosVerkocht"]["data"],
			$stats["kudosGekregen"]["data"]
		);
		$stats["winstCum"]["data"] = array_map(
			function($x, $y) { return $x - $y; },
			$stats["kudosVerkochtCum"]["data"],
			$stats["kudosGekregenCum"]["data"]
		);

		$stats["gemTransGekregen"]["data"] = array_map(
			'deelDefault',
			$stats["kudosGekregen"]["data"],
			$stats["transactiesGekregen"]["data"]
		);

		$stats["gemTransGekregenCum"]["data"] = array_map(
			'deelDefault',
			$stats["kudosGekregenCum"]["data"],
			$stats["transactiesGekregenCum"]["data"]
		);

		$stats["gemTransVerkocht"]["data"] = array_map(
			'deelDefault',
			$stats["kudosVerkocht"]["data"],
			$stats["transactiesVerkocht"]["data"]
		);

		$stats["gemTransVerkochtCum"]["data"] = array_map(
			'deelDefault',
			$stats["kudosVerkochtCum"]["data"],
			$stats["transactiesVerkochtCum"]["data"]
		);

		$page->add(new HtmlHeader(3, _('Grafiekjes')));
		$page->add($row = new HtmlDiv(null, 'row'));

		$dataset = array();
		$cumDataset = array();

		foreach ($stats as $name => $stat)
		{
			$title = $stat["title"] . " (afgelopen 12 maanden)";

			if(substr($name, -3) == 'Cum')
				$cumDataset[$title] = array_values($stat['data']);
			else
				$dataset[$title] = array_values($stat['data']);
		}

		$row->add(new HtmlDiv(maakGrafiek(array_keys(array_reverse($graphx)), $dataset), 'col-md-12'));
		$row->add(new HtmlDiv(maakGrafiek(array_keys(array_reverse($graphx)), $cumDataset), 'col-md-12'));

		return $page;
	}

	/** 
	 * Maakt de pagina waarop je oude Dibs kan terugclaimen
	 */

	static public function oudeKudosVerwijderen()
	{
		$page = new HTMLPage();

		$page = Page::getInstance()
			 ->start()
			 ->add(new HtmlHeader(2, _("Alle inactieve kudos teruggeven")));

		$form = HtmlForm::named('oudeKudosVerwijderen');
		$form->add(new HtmlParagraph(_("Weet je het zeker?")))
			 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Jazeker!"))))
			 ->add(new HtmlAnchor("/Leden/Dibs/Statistieken", _("Ga terug")));
		$page->add($form);

		return new PageResponse($page);
	}


	static public function labelPersoon (DibsInfo $obj = NULL)
	{
		return _('Persoon');
	}
	static public function labelPersoonID (DibsInfo $obj = NULL)
	{
		return _('Lidnr');
	}
	static public function labelPas (DibsInfo $obj = NULL)
	{
		return _('Actieve pas');
	}
	static public function labelKudos (DibsInfo $obj = NULL)
	{
		return _('Kudos');
	}
	static public function labelLaatste (DibsInfo $obj = NULL)
	{
		return _('Laatste transactie');
	}

	static public function waardePersoon (DibsInfo $obj)
	{
		if ($persoon = $obj->getPersoon())
			return PersoonView::naam($persoon);
		return NULL;
	}
	static public function waardePas (DibsInfo $obj)
	{
		if ($pas = $obj->getPas())
			return PasView::waardeRfidTag($pas);
		return NULL;
	}
	static public function waardeLaatste (DibsInfo $obj)
	{
		if ($laatste = $obj->getLaatste())
			return DibsTransactieView::waardeTransactieID($laatste);
		return NULL;
	}

	static public function maakLink (DibsInfo $obj)
	{
		if ($naam = static::waardePersoon($obj))
			return new HtmlAnchor($obj->url(), $naam);
		return NULL;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
