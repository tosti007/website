<?
/**
 * $Id$
 */
class DibsProduct
	extends DibsProduct_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DibsProduct_Generated

		$this->artikelID = NULL;
	}

	public function voorraden()
	{
		return VoorraadQuery::table()
			->whereProp('Artikel', $this)
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
