<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
abstract class Pas_Generated
	extends Entiteit
{
	protected $pasID;					/**< \brief PRIMARY */
	protected $rfidTag;
	protected $persoon;
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $actief;
	/** Verzamelingen **/
	protected $dibsTransactieVerzameling;
	/**
	 * @brief De constructor van de Pas_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->pasID = NULL;
		$this->rfidTag = '';
		$this->persoon = NULL;
		$this->persoon_contactID = 0;
		$this->actief = True;
		$this->dibsTransactieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld pasID.
	 *
	 * @return int
	 * De waarde van het veld pasID.
	 */
	public function getPasID()
	{
		return $this->pasID;
	}
	/**
	 * @brief Geef de waarde van het veld rfidTag.
	 *
	 * @return string
	 * De waarde van het veld rfidTag.
	 */
	public function getRfidTag()
	{
		return $this->rfidTag;
	}
	/**
	 * @brief Stel de waarde van het veld rfidTag in.
	 *
	 * @param mixed $newRfidTag De nieuwe waarde.
	 *
	 * @return Pas
	 * Dit Pas-object.
	 */
	public function setRfidTag($newRfidTag)
	{
		unset($this->errors['RfidTag']);
		if(!is_null($newRfidTag))
			$newRfidTag = trim($newRfidTag);
		if($newRfidTag === "")
			$newRfidTag = NULL;
		if($this->rfidTag === $newRfidTag)
			return $this;

		$this->rfidTag = $newRfidTag;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rfidTag geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rfidTag geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkRfidTag()
	{
		if (array_key_exists('RfidTag', $this->errors))
			return $this->errors['RfidTag'];
		$waarde = $this->getRfidTag();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Stel de waarde van het veld persoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Pas
	 * Dit Pas-object.
	 */
	public function setPersoon($new_contactID)
	{
		unset($this->errors['Persoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->persoon == $new_contactID
			&& $this->persoon_contactID == $this->persoon->getContactID())
				return $this;
			$this->persoon = $new_contactID;
			$this->persoon_contactID
					= $this->persoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->persoon == NULL 
				&& $this->persoon_contactID == (int)$new_contactID)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld persoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld persoon geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPersoon()
	{
		if (array_key_exists('Persoon', $this->errors))
			return $this->errors['Persoon'];
		$waarde1 = $this->getPersoon();
		$waarde2 = $this->getPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld actief.
	 *
	 * @return bool
	 * De waarde van het veld actief.
	 */
	public function getActief()
	{
		return $this->actief;
	}
	/**
	 * @brief Stel de waarde van het veld actief in.
	 *
	 * @param mixed $newActief De nieuwe waarde.
	 *
	 * @return Pas
	 * Dit Pas-object.
	 */
	public function setActief($newActief)
	{
		unset($this->errors['Actief']);
		if(!is_null($newActief))
			$newActief = (bool)$newActief;
		if($this->actief === $newActief)
			return $this;

		$this->actief = $newActief;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld actief geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld actief geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkActief()
	{
		if (array_key_exists('Actief', $this->errors))
			return $this->errors['Actief'];
		$waarde = $this->getActief();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de DibsTransactieVerzameling die hoort bij dit object.
	 */
	public function getDibsTransactieVerzameling()
	{
		if(!$this->dibsTransactieVerzameling instanceof DibsTransactieVerzameling)
			$this->dibsTransactieVerzameling = DibsTransactieVerzameling::fromPas($this);
		return $this->dibsTransactieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Pas::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Pas.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Pas::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getPasID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Pas|false
	 * Een Pas-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$pasID = (int)$a[0];
		}
		else if(isset($a))
		{
			$pasID = (int)$a;
		}

		if(is_null($pasID))
			throw new BadMethodCallException();

		static::cache(array( array($pasID) ));
		return Entiteit::geefCache(array($pasID), 'Pas');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Pas');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Pas::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Pas`.`pasID`'
		                 .     ', `Pas`.`rfidTag`'
		                 .     ', `Pas`.`persoon_contactID`'
		                 .     ', `Pas`.`actief`'
		                 .     ', `Pas`.`gewijzigdWanneer`'
		                 .     ', `Pas`.`gewijzigdWie`'
		                 .' FROM `Pas`'
		                 .' WHERE (`pasID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['pasID']);

			$obj = new Pas();

			$obj->inDB = True;

			$obj->pasID  = (int) $row['pasID'];
			$obj->rfidTag  = trim($row['rfidTag']);
			$obj->persoon_contactID  = (int) $row['persoon_contactID'];
			$obj->actief  = (bool) $row['actief'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Pas')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getPersoonContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->pasID =
			$WSW4DB->q('RETURNID INSERT INTO `Pas`'
			          . ' (`rfidTag`, `persoon_contactID`, `actief`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %i, %s, %i)'
			          , $this->rfidTag
			          , $this->persoon_contactID
			          , $this->actief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Pas')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Pas`'
			          .' SET `rfidTag` = %s'
			          .   ', `persoon_contactID` = %i'
			          .   ', `actief` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `pasID` = %i'
			          , $this->rfidTag
			          , $this->persoon_contactID
			          , $this->actief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->pasID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPas
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPas($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPas($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'RfidTag';
			$velden[] = 'Persoon';
			$velden[] = 'Actief';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'RfidTag';
			$velden[] = 'Persoon';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'RfidTag';
			$velden[] = 'Persoon';
			$velden[] = 'Actief';
			break;
		case 'get':
			$velden[] = 'RfidTag';
			$velden[] = 'Persoon';
			$velden[] = 'Actief';
		case 'primary':
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'DibsTransactieVerzameling';
			break;
		default:
			$velden[] = 'RfidTag';
			$velden[] = 'Persoon';
			$velden[] = 'Actief';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle DibsTransactie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$DibsTransactieFromPasVerz = DibsTransactieVerzameling::fromPas($this);
		$returnValue = $DibsTransactieFromPasVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object DibsTransactie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $DibsTransactieFromPasVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Pas`'
		          .' WHERE `pasID` = %i'
		          .' LIMIT 1'
		          , $this->pasID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->pasID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Pas`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `pasID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->pasID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Pas terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'pasid':
			return 'int';
		case 'rfidtag':
			return 'string';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'actief':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'pasID':
		case 'persoon_contactID':
		case 'actief':
			$type = '%i';
			break;
		case 'rfidTag':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Pas`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `pasID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->pasID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Pas`'
		          .' WHERE `pasID` = %i'
		                 , $veld
		          , $this->pasID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Pas');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle DibsTransactie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DibsTransactieFromPasVerz = DibsTransactieVerzameling::fromPas($this);
		$dependencies['DibsTransactie'] = $DibsTransactieFromPasVerz;

		return $dependencies;
	}
}
