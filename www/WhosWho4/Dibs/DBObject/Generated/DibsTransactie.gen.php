<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
abstract class DibsTransactie_Generated
	extends Transactie
{
	protected $bron;					/**< \brief ENUM:www/tablet/bvk/ideal */
	protected $pas;						/**< \brief NULL */
	protected $pas_pasID;				/**< \brief PRIMARY */
	/** Verzamelingen **/
	protected $dibsInfoVerzameling;
	/**
	/**
	 * @brief De constructor van de DibsTransactie_Generated-klasse.
	 *
	 * @param mixed $a Rubriek (enum)
	 * @param mixed $b Soort (enum)
	 * @param mixed $c Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 * @param mixed $d Bedrag (money)
	 * @param mixed $e Uitleg (string)
	 * @param mixed $f Wanneer (datetime)
	 * @param mixed $g Bron (enum)
	 * @param mixed $h Pas (Pas OR Array(pas_pasID) OR pas_pasID)
	 */
	public function __construct($a = 'BOEKWEB',
			$b = 'VERKOOP',
			$c = NULL,
			$d = NULL,
			$e = '',
			$f = NULL,
			$g = 'WWW',
			$h = NULL)
	{
		parent::__construct($a, $b, $c, $d, $e, $f); // Transactie

		$g = strtoupper(trim($g));
		if(!in_array($g, static::enumsBron()))
			throw new BadMethodCallException();
		$this->bron = $g;

		if(is_array($h) && is_null($h[0]))
			$h = NULL;

		if($h instanceof Pas)
		{
			$this->pas = $h;
			$this->pas_pasID = $h->getPasID();
		}
		else if(is_array($h))
		{
			$this->pas = NULL;
			$this->pas_pasID = (int)$h[0];
		}
		else if(isset($h))
		{
			$this->pas = NULL;
			$this->pas_pasID = (int)$h;
		}
		else
		{
			$this->pas = NULL;
			$this->pas_pasID = NULL;
		}

		$this->transactieID = NULL;
		$this->dibsInfoVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Rubriek';
		$volgorde[] = 'Soort';
		$volgorde[] = 'Contact';
		$volgorde[] = 'Bedrag';
		$volgorde[] = 'Uitleg';
		$volgorde[] = 'Wanneer';
		$volgorde[] = 'Bron';
		$volgorde[] = 'Pas';
		return $volgorde;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld bron.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld bron.
	 */
	static public function enumsBron()
	{
		static $vals = array('WWW','TABLET','BVK','IDEAL');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld bron.
	 *
	 * @return string
	 * De waarde van het veld bron.
	 */
	public function getBron()
	{
		return $this->bron;
	}
	/**
	 * @brief Geef de waarde van het veld pas.
	 *
	 * @return Pas
	 * De waarde van het veld pas.
	 */
	public function getPas()
	{
		if(!isset($this->pas)
		 && isset($this->pas_pasID)
		 ) {
			$this->pas = Pas::geef
					( $this->pas_pasID
					);
		}
		return $this->pas;
	}
	/**
	 * @brief Geef de waarde van het veld pas_pasID.
	 *
	 * @return int
	 * De waarde van het veld pas_pasID.
	 */
	public function getPasPasID()
	{
		if (is_null($this->pas_pasID) && isset($this->pas)) {
			$this->pas_pasID = $this->pas->getPasID();
		}
		return $this->pas_pasID;
	}
	/**
	 * @brief Returneert de DibsInfoVerzameling die hoort bij dit object.
	 */
	public function getDibsInfoVerzameling()
	{
		if(!$this->dibsInfoVerzameling instanceof DibsInfoVerzameling)
			$this->dibsInfoVerzameling = DibsInfoVerzameling::fromLaatste($this);
		return $this->dibsInfoVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return DibsTransactie::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van DibsTransactie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return DibsTransactie::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTransactieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return DibsTransactie|false
	 * Een DibsTransactie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$transactieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$transactieID = (int)$a;
		}

		if(is_null($transactieID))
			throw new BadMethodCallException();

		static::cache(array( array($transactieID) ));
		return Entiteit::geefCache(array($transactieID), 'DibsTransactie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Transactie::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('DibsTransactie::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `DibsTransactie`.`bron`'
			                 .     ', `DibsTransactie`.`pas_pasID`'
			                 .     ', `Transactie`.`transactieID`'
			                 .     ', `Transactie`.`rubriek`'
			                 .     ', `Transactie`.`soort`'
			                 .     ', `Transactie`.`contact_contactID`'
			                 .     ', `Transactie`.`bedrag`'
			                 .     ', `Transactie`.`uitleg`'
			                 .     ', `Transactie`.`wanneer`'
			                 .     ', `Transactie`.`status`'
			                 .     ', `Transactie`.`gewijzigdWanneer`'
			                 .     ', `Transactie`.`gewijzigdWie`'
			                 .' FROM `DibsTransactie`'
			                 .' LEFT JOIN `Transactie` USING (`transactieID`)'
			                 .' WHERE (`transactieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['transactieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['DibsTransactie'][$id];
			} else {
				$obj = new DibsTransactie($row['rubriek'], $row['soort'], array($row['contact_contactID']), $row['bedrag'], $row['uitleg'], $row['wanneer'], $row['bron'], array($row['pas_pasID']));
			}

			$obj->inDB = True;

			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Transactie::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'DibsTransactie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getPasPasID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `DibsTransactie`'
			          . ' (`transactieID`, `bron`, `pas_pasID`)'
			          . ' VALUES (%i, %s, %i)'
			          , $this->transactieID
			          , $this->bron
			          , $this->pas_pasID
			          );

			if($classname == 'DibsTransactie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `DibsTransactie`'
			          .' SET `bron` = %s'
			          .   ', `pas_pasID` = %i'
			          .' WHERE `transactieID` = %i'
			          , $this->bron
			          , $this->pas_pasID
			          , $this->transactieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDibsTransactie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDibsTransactie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDibsTransactie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Bron';
			$velden[] = 'Pas';
			break;
		case 'get':
			$velden[] = 'Bron';
			$velden[] = 'Pas';
		case 'primary':
			$velden[] = 'pas_pasID';
			break;
		case 'verzamelingen':
			$velden[] = 'DibsInfoVerzameling';
			break;
		default:
			$velden[] = 'Bron';
			$velden[] = 'Pas';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle DibsInfo-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$DibsInfoFromLaatsteVerz = DibsInfoVerzameling::fromLaatste($this);
		$returnValue = $DibsInfoFromLaatsteVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object DibsInfo met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($DibsInfoFromLaatsteVerz as $v)
		{
			$v->setLaatste(NULL);
		}
		$DibsInfoFromLaatsteVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `DibsTransactie`'
		          .' WHERE `transactieID` = %i'
		          .' LIMIT 1'
		          , $this->transactieID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van DibsTransactie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bron':
			return 'enum';
		case 'pas':
			return 'foreign';
		case 'pas_pasid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'pas_pasID':
			$type = '%i';
			break;
		case 'bron':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `DibsTransactie`'
		          ." SET `%l` = $type"
		          .' WHERE `transactieID` = %i'
		          , $veld
		          , $waarde
		          , $this->transactieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bron':
		case 'pas':
		case 'pas_pasID':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `DibsTransactie`'
		          .' WHERE `transactieID` = %i'
		                 , $veld
		          , $this->transactieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle DibsInfo-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$DibsInfoFromLaatsteVerz = DibsInfoVerzameling::fromLaatste($this);
		$dependencies['DibsInfo'] = $DibsInfoFromLaatsteVerz;

		return $dependencies;
	}
}
