<?
/**
 * @brief 'AT'AUTH_GET:bestuur
 */
abstract class DibsInfo_Generated
	extends Entiteit
{
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $kudos;
	protected $laatste;					/**< \brief NULL */
	protected $laatste_transactieID;	/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de DibsInfo_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->kudos = 0;
		$this->laatste = NULL;
		$this->laatste_transactieID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld kudos.
	 *
	 * @return int
	 * De waarde van het veld kudos.
	 */
	public function getKudos()
	{
		return $this->kudos;
	}
	/**
	 * @brief Stel de waarde van het veld kudos in.
	 *
	 * @param mixed $newKudos De nieuwe waarde.
	 *
	 * @return DibsInfo
	 * Dit DibsInfo-object.
	 */
	public function setKudos($newKudos)
	{
		unset($this->errors['Kudos']);
		if(!is_null($newKudos))
			$newKudos = (int)$newKudos;
		if($this->kudos === $newKudos)
			return $this;

		$this->kudos = $newKudos;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kudos geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kudos geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkKudos()
	{
		if (array_key_exists('Kudos', $this->errors))
			return $this->errors['Kudos'];
		$waarde = $this->getKudos();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld laatste.
	 *
	 * @return DibsTransactie
	 * De waarde van het veld laatste.
	 */
	public function getLaatste()
	{
		if(!isset($this->laatste)
		 && isset($this->laatste_transactieID)
		 ) {
			$this->laatste = DibsTransactie::geef
					( $this->laatste_transactieID
					);
		}
		return $this->laatste;
	}
	/**
	 * @brief Stel de waarde van het veld laatste in.
	 *
	 * @param mixed $new_transactieID De nieuwe waarde.
	 *
	 * @return DibsInfo
	 * Dit DibsInfo-object.
	 */
	public function setLaatste($new_transactieID)
	{
		unset($this->errors['Laatste']);
		if($new_transactieID instanceof DibsTransactie
		) {
			if($this->laatste == $new_transactieID
			&& $this->laatste_transactieID == $this->laatste->getTransactieID())
				return $this;
			$this->laatste = $new_transactieID;
			$this->laatste_transactieID
					= $this->laatste->getTransactieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_transactieID) || $new_transactieID == 0)) {
			if($this->laatste == NULL && $this->laatste_transactieID == NULL)
				return $this;
			$this->laatste = NULL;
			$this->laatste_transactieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_transactieID)
		) {
			if($this->laatste == NULL 
				&& $this->laatste_transactieID == (int)$new_transactieID)
				return $this;
			$this->laatste = NULL;
			$this->laatste_transactieID
					= (int)$new_transactieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld laatste geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld laatste geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkLaatste()
	{
		if (array_key_exists('Laatste', $this->errors))
			return $this->errors['Laatste'];
		$waarde1 = $this->getLaatste();
		$waarde2 = $this->getLaatsteTransactieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld laatste_transactieID.
	 *
	 * @return int
	 * De waarde van het veld laatste_transactieID.
	 */
	public function getLaatsteTransactieID()
	{
		if (is_null($this->laatste_transactieID) && isset($this->laatste)) {
			$this->laatste_transactieID = $this->laatste->getTransactieID();
		}
		return $this->laatste_transactieID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return DibsInfo::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van DibsInfo.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return DibsInfo::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return DibsInfo|false
	 * Een DibsInfo-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$persoon_contactID = (int)$a[0];
		}
		else if($a instanceof Persoon)
		{
			$persoon_contactID = $a->getContactID();
		}
		else if(isset($a))
		{
			$persoon_contactID = (int)$a;
		}

		if(is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($persoon_contactID) ));
		return Entiteit::geefCache(array($persoon_contactID), 'DibsInfo');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'DibsInfo');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('DibsInfo::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `DibsInfo`.`persoon_contactID`'
		                 .     ', `DibsInfo`.`kudos`'
		                 .     ', `DibsInfo`.`laatste_transactieID`'
		                 .     ', `DibsInfo`.`gewijzigdWanneer`'
		                 .     ', `DibsInfo`.`gewijzigdWie`'
		                 .' FROM `DibsInfo`'
		                 .' WHERE (`persoon_contactID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['persoon_contactID']);

			$obj = new DibsInfo(array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->kudos  = (int) $row['kudos'];
			$obj->laatste_transactieID  = (is_null($row['laatste_transactieID'])) ? null : (int) $row['laatste_transactieID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'DibsInfo')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;
		$this->getLaatsteTransactieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `DibsInfo`'
			          . ' (`persoon_contactID`, `kudos`, `laatste_transactieID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %s, %i)'
			          , $this->persoon_contactID
			          , $this->kudos
			          , $this->laatste_transactieID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'DibsInfo')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `DibsInfo`'
			          .' SET `kudos` = %i'
			          .   ', `laatste_transactieID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          , $this->kudos
			          , $this->laatste_transactieID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDibsInfo
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDibsInfo($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDibsInfo($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Kudos';
			$velden[] = 'Laatste';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Kudos';
			$velden[] = 'Laatste';
			break;
		case 'get':
			$velden[] = 'Kudos';
			$velden[] = 'Laatste';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'laatste_transactieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Kudos';
			$velden[] = 'Laatste';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `DibsInfo`'
		          .' WHERE `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `DibsInfo`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van DibsInfo terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'kudos':
			return 'int';
		case 'laatste':
			return 'foreign';
		case 'laatste_transactieid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `DibsInfo`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `DibsInfo`'
		          .' WHERE `persoon_contactID` = %i'
		                 , $veld
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'DibsInfo');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
