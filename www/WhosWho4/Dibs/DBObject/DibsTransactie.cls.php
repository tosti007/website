<?
/**
 * $Id$
 */
class DibsTransactie
	extends DibsTransactie_Generated
{
	protected $producten;

	/*** CONSTRUCTOR ***/
	public function __construct ($rubriek = null, $soort = null, $persoon = null, $bedrag = null, $uitleg = null, $wanneer = null, $bron = null, $pas = null)
	{
		parent::__construct($rubriek, $soort, $persoon, $bedrag, $uitleg, $wanneer, $bron, $pas);

		$this->producten = NULL;
	}

	public function getKudos ()
	{
		return self::euroToKudos($this->getBedrag());
	}

	/**
	 *  Reken een bedrag in euro's om in kudos.
	 * We gaan hierbij ervan uit dat wisselkoersen niet veranderen.
	 * @param bedrag Een float met het bedrag in euro's (dus een euro en vijftig cent wordt 1.50)
	 * @returns Een int met het bedrag in kudos (dus 150)
	 */
	public static function euroToKudos($bedrag)
	{
		//Er staat hier een round omdat er met bedragen met floats wordt gegooid en we ints willen hebben
		return (int)round($bedrag * EURO_TO_KUDOS);
	}

	public function getPersoon ()
	{
		if ($this->getContact())
			if ($persoon = Persoon::geef($this->getContactContactID()))
				return $persoon;
		return NULL;
	}

	public function getRichting ()
	{
		if ($this->getKudos() > 0)
			return 1;
		return -1;
	}

	public function check ()
	{
		// nooit updaten
		if ($this->getInDB())
			return _('transactie zit al in db');

		if (!$this->getKudos())
			return _('aantal kudos is niet positief');

		if ($this->getBron() == 'BVK' && $this->getKudos() > 0)
			return _('boekverkooptransacties moeten kudos aan het lid geven');

		if ($this->getBron() == 'WWW' && !$this->getUitleg())
			return _('overige transacties moeten een uitleg hebben');

		$info = DibsInfo::geef($this->getContact());
		if (!$info)
			return _('persoon is nog niet geactiveerd voor DiBS');

		$lidlimiet = Register::getValue('lidLimiet');
		$aeskudos = Register::getValue('aesKudos');

//		if (DIBS_MAX_TRANSACTIE_KUDOS > 0
//			&& abs($this->getKudos()) > DIBS_MAX_TRANSACTIE_KUDOS)
//			return _('transactie omvat te veel kudos');
		
		// kudos naar lid:
		if ($this->getKudos() < 0)
		{
			if ($lidlimiet > 0
				&& $info->getKudos() - $this->getKudos() > $lidlimiet)
				return _('lid zou te veel kudos krijgen');
			if (-1 * ($this->getKudos()) > $aeskudos)
				return _('er zouden te veel kudos in de omloop komen');
		}
		// kudos naar vereniging:
		else
		{
			if ($this->getKudos() > $info->getKudos())
				return _('lid heeft niet genoeg kudos');
		}

		return false;
	}

	public function opslaan ($classname = 'DibsTransactie')
	{
		parent::opslaan($classname);

		//Registry opslaan
		$aeskudos = Register::getValue('aesKudos');
		$kudos = $aeskudos + $this->getKudos();
		Register::updateRegister('aesKudos', $kudos);

		return true;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
