<?
/**
 * $Id$
 */
class DibsInfo
	extends DibsInfo_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a)
	{
		parent::__construct($a); // DibsInfo_Generated

		$this->kudos = 0;
	}

	/**
	 * @brief Maak een nieuwe transactie voor deze DibsInfo en sla deze transactie op.
	 *
	 * Slaat de DibsTransactie meteen op om inconsistenties te voorkomen.
	 *
	 * @param transactie de DibsTransactie die aan deze DibsInfo moet worden 
	 * toegevoegd.
	 */ 
	public function nieuweTransactie (DibsTransactie $transactie)
	{
		if ($transactie->getInDB())
			throw new LogicException('transactie zit al in DB');
		if ($transactie->getContactContactID() != $this->getPersoonContactID())
			throw new LogicException('persoon hoort niet bij transactie');

		if ($check = $transactie->check())
			return $check;

		$this->setKudos($this->getKudos() - $transactie->getKudos());
		$transactie->opslaan();
		$this->laatste_transactieID = $transactie->getTransactieID();
		return false;
	}

	public function url ($extension = '')
	{
		if (empty($extension))
			$extension = '';
		else
			$extension = "/$extension";
		return DIBSBASE . '/' . $this->getPersoonContactID() . $extension;
	}

	public function magBekijken()
	{
		return parent::magBekijken() || $this->getPersoon() == Persoon::getIngelogd();
	}

	public function getGewijzigdWanneer() {
		return $this->gewijzigdWanneer->strftime('%F %T');
	}

	/**
	 * @brief Check of het aantal kudos van deze DibsInfo geldig is.
	 *
	 * @return een foutmelding als het aantal kudos negatief is; of False als 
	 * alles goed is.
	 */
	public function checkKudos ()
	{
		if ($this->getKudos() < 0)
			return _('Iemand kan geen negatief aantal kudos hebben');

		return false;
	}

	/**
	 * @brief Check of de laatste transactie bij deze DibsInfo geldig is.
	 * @return een foutmelding als de laatste DibsTransactie van deze DibsInfo niet overeenkomt met de daadwerkelijk laatsts ingevoerde DibsTransactie; False als alles 
	 * goed is.
	 */
	public function checkLaatste ()
	{
		$transactie = $this->getLaatste();
		if ($transactie && $transactie->getContactContactID() != $this->getPersoonContactID())
			return sprintf(_('Laatste transactie van %s hoort niet bij die persoon'), $this->getContactID());
	}

	public function getPassen()
	{
		return PasQuery::table()
			->whereProp('Persoon', $this->getPersoon())
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
