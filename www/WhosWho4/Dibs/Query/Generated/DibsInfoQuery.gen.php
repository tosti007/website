<?
abstract class DibsInfoQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de DibsInfoQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een DibsInfoQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw DibsInfoQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return DibsInfoQuery
	 * Het DibsInfoQuery-object waarvan meteen de db-table van het DibsInfo-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new DibsInfoQuery();

		$query->tables('DibsInfo');

		return $query;
	}

	/**
	 * @brief Maakt een DibsInfoVerzameling aan van de objecten die geselecteerd worden
	 * door de DibsInfoQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return DibsInfoVerzameling
	 * Een DibsInfoVerzameling verkregen door de query
	 */
	public function verzamel($object = 'DibsInfo', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een DibsInfo-iterator aan van de objecten die geselecteerd worden
	 * door de DibsInfoQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een DibsInfoVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'DibsInfo', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een DibsInfo die geslecteeerd wordt door de DibsInfoQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return DibsInfoVerzameling
	 * Een DibsInfoVerzameling verkregen door de query.
	 */
	public function geef($object = 'DibsInfo')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van DibsInfo.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'persoon_contactID',
			'kudos',
			'laatste_transactieID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'DibsInfo';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als DibsInfo een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan DibsInfo een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('DibsInfo'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			),
			'DibsTransactie' => array(
				'laatste_transactieID' => 'transactieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van DibsInfo.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'DibsInfo.persoon_contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'kudos':
			$type = 'int';
			$field = 'kudos';
			break;
		case 'laatste':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof DibsTransactie; })))
					user_error('Alle waarden in de array moeten van type DibsTransactie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof DibsTransactie) && !($value instanceof DibsTransactieVerzameling) && !is_null($value))
				user_error('Value moet van type DibsTransactie of NULL zijn', E_USER_ERROR);
			$field = 'laatste_transactieID';
			if(is_array($value) || $value instanceof DibsTransactieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getTransactieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getTransactieID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'DibsInfo.'.$v;
			}
		} else {
			$field = 'DibsInfo.'.$field;
		}

		return array($field, $value, $type);
	}

}
