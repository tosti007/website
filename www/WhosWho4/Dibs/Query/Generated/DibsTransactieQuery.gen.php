<?
abstract class DibsTransactieQuery_Generated
	extends TransactieQuery
{
	/**
	 * @brief De constructor van de DibsTransactieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // TransactieQuery
	}
	/**
	 * @brief Maakt een DibsTransactieQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw DibsTransactieQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return DibsTransactieQuery
	 * Het DibsTransactieQuery-object waarvan meteen de db-table van het
	 * DibsTransactie-object als table geset is.
	 */
	static public function table()
	{
		$query = new DibsTransactieQuery();

		$query->tables('DibsTransactie');

		return $query;
	}

	/**
	 * @brief Maakt een DibsTransactieVerzameling aan van de objecten die geselecteerd
	 * worden door de DibsTransactieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return DibsTransactieVerzameling
	 * Een DibsTransactieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'DibsTransactie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een DibsTransactie-iterator aan van de objecten die geselecteerd
	 * worden door de DibsTransactieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een DibsTransactieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'DibsTransactie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een DibsTransactie die geslecteeerd wordt door de
	 * DibsTransactieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return DibsTransactieVerzameling
	 * Een DibsTransactieVerzameling verkregen door de query.
	 */
	public function geef($object = 'DibsTransactie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van DibsTransactie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'transactieID',
			'bron',
			'pas_pasID'
		);

		if(in_array($field, $varArray))
			return 'DibsTransactie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als DibsTransactie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Transactie' => array(
				'Transactie.transactieID' => 'DibsTransactie.transactieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan DibsTransactie een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('DibsTransactie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Pas' => array(
				'pas_pasID' => 'pasID'
			),
			'Contact' => array(
				'contact_contactID' => 'contactID'
			),
			'Transactie' => array(
				'Transactie.transactieID' => 'DibsTransactie.transactieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van DibsTransactie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'DibsTransactie.transactieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'bron':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, DibsTransactie::enumsBron()))
						user_error($value . ' is niet een geldige waarde voor bron', E_USER_ERROR);
				}
			}
			else if(!in_array($value, DibsTransactie::enumsBron()))
				user_error($value . ' is niet een geldige waarde voor bron', E_USER_ERROR);
			$type = 'string';
			$field = 'bron';
			break;
		case 'pas':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Pas; })))
					user_error('Alle waarden in de array moeten van type Pas zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Pas) && !($value instanceof PasVerzameling) && !is_null($value))
				user_error('Value moet van type Pas of NULL zijn', E_USER_ERROR);
			$field = 'pas_pasID';
			if(is_array($value) || $value instanceof PasVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getPasID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getPasID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'DibsTransactie.'.$v;
			}
		} else {
			$field = 'DibsTransactie.'.$field;
		}

		return array($field, $value, $type);
	}

}
