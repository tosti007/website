<?

/**
 * $Id$
 */
class PlannerDataVerzameling
	extends PlannerDataVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PlannerDataVerzameling_Generated
	}

	/**
	 * Geef een lijstje met alle deelnemers van een planner
	 */
	static public function planner (Planner $planner)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `plannerDataID` FROM `PlannerData` '
						  . 'WHERE `planner_plannerID` = %i '
						  . 'ORDER BY momentBegin',
					$planner->geefID());

		return self::verzamel($ids);
	}

	public function toArrayMomentBegin ()
	{
		$ret = array();
		foreach ($this as $that)
			$ret[] = $that->getMomentBegin()->getTimestamp();
		return $ret;
	}

	public function toSQLArrayMomentBegin ()
	{
		$ret = array();
		foreach ($this as $that)
			$ret[] = $that->getMomentBegin()->strftime('%F %T');
		return $ret;
	}

	public function toSQLValues ()
	{
		$ret = array();
		foreach ($this as $that)
			$ret[] = array($that->getPlannerPlannerID(), $that->getMomentBegin()->strftime('%F %T'));
		return $ret;
	}

	/** Sorteer functies **/

	public function sorteerOpMomentBegin($aID, $bID)
	{
		$a = PlannerData::geef($aID)->getMomentBegin();
		$b = PlannerData::geef($bID)->getMomentBegin();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
