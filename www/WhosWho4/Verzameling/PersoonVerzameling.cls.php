<?

/***
 * $Id$
 */
class PersoonVerzameling
	extends PersoonVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Filter alle personen waar een lid-object aan gekoppeld is en retourneer de verzameling
	 */
	public function leden ()
	{
		$ret = new LidVerzameling();
		return $ret->verzamel($this->keys());
	}

	/**
		Retourneert een key -> value array met contactIDs als key, en de
		volledige naam van personen uit deze verzameling als value.
		Uiteraard wordt de sortering van de lijst aangehouden.

		Voorbeeld
		array(
			2998 => "Peter van de Werken",
			4042 => "Bas van Schaik"
		)
	**/
	public function geefNamen()
	{
		$res = array();
		foreach($this as $lidnr => $lid)
			$res[$lidnr] = PersoonView::naam($lid);
		return $res;
	}

	/**
		 Retourneert een PersoonVerzameling met Persoon objecten van
		leden die jarig zijn binnen $periode dagen.

		Enkel leden die hebben aangegeven hun gegevens gepubliceerd te willen
		zien, komen in de lijst voor. Bestuur mag wel alles zien.
	**/
	public static function jarig($periode = 1)
	{
		$opzoekbaar = array('J','A');
		if(hasAuth('bestuur'))
			$opzoekbaar = array('J','A','N');

		global $WSW4DB;

		$ids = array();
		for ($i = 0; $i < $periode; $i++)
		{
			$today = new DateTimeLocale();
			$ids = array_merge($ids, $WSW4DB->q('COLUMN SELECT `contactID`'
							. ' FROM `Lid`'
							. ' LEFT JOIN `Persoon` USING (`contactID`)'
							. ' WHERE DATE_FORMAT(`datumGeboorte`, \'%%d %%m\') LIKE  DATE_FORMAT(%s + INTERVAL %i DAY, \'%%d %%m\')'
							. '   AND `Lid`.`opzoekbaar` IN (%As)'
							. '   AND '.WSW4_WHERE_LID
							. ' ORDER BY `voornaam`, `achternaam`'
							, $today->format('Y-m-d')
							, $i, $opzoekbaar
							));
		}

		return self::verzamel($ids);
	}
	
	/**
		 Retourneert een PersoonVerzameling met Persoon objecten van
		leden die jarig zijn in een bepaalde maand.

		Enkel leden die hebben aangegeven hun gegevens gepubliceerd te willen
		zien, komen in de lijst voor. Bestuur mag wel alles zien.
	**/
	public static function jarigInMaand($maand)
	{
		$opzoekbaar = array('J','A');
		if(hasAuth('bestuur'))
			$opzoekbaar = array('J','A','N');

		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `contactID`'
							. ' FROM `Lid`'
							. ' LEFT JOIN `Persoon` USING (`contactID`)'
							. ' WHERE MONTH(`datumGeboorte`) = %i'
							. '   AND `Lid`.`opzoekbaar` IN (%As)'
							. '   AND '.WSW4_WHERE_LID
							. ' ORDER BY DAY(`datumGeboorte`), `voornaam`, `achternaam`'
							, $maand, $opzoekbaar
							);
		return self::verzamel($ids);
	}

	/*** Sorteren ***/
	static public function sorteerOpID($aID, $bID)
	{
		if($aID < $bID)
			return -1;
		if($aID > $bID)
			return 1;
		return 0;
	}

	static public function sorteerOpNaam($aID, $bID)
	{
		$a = Persoon::geef($aID);
		$b = Persoon::geef($bID);

		if(!is_null($a->getVoornaam()) && !is_null($b->getVoornaam())) {
			$c = strcasecmp($a->getVoornaam(), $b->getVoornaam());
			if($c != 0) return $c;
		}

		return strcasecmp(PersoonView::naam($a), PersoonView::naam($b));
	}

	static public function sorteerOpMijEerst($aID, $bID)
	{
		global $auth;

		if($auth->getLidnr() == $aID)
			return -1;
		if($auth->getLidnr() == $bID)
			return 1;
		return self::sorteerOpNaam($aID, $bID);
	}

	/**
	 * Speciaal om te kijken wie het eerste jarig is. Houdt nog geen rekening
	 * met de jaarwisseling.
	 **/
	static public function sorteerOpeersteJarig($aID, $bID)
	{
		$a = Persoon::geef($aID)->getDatumGeboorte()->format('m-d');
		$b = Persoon::geef($bID)->getDatumGeboorte()->format('m-d');

		if($a < $b)
			return -1;
		if($a > $b)
			return 1;
		return 0;
	}

	/**
	 * Geef een lijstje met alle deelnemers van een planner
	 */
	static public function planner (Planner $planner)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `persoon_contactID` FROM `PlannerDeelnemer` '
						  . 'LEFT JOIN `PlannerData` ON `plannerData_plannerDataID` = `plannerDataID` '
						  . 'WHERE `planner_plannerID` = %i ',
					$planner->geefID());
		
		return self::verzamel($ids);
	}

	static public function plannerDataVoor (PlannerData $datum)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `persoon_contactID` FROM `PlannerDeelnemer` '
						  . 'WHERE `plannerData_plannerDataID` = %i '
						  . 'AND `antwoord` != \'NEE\'',
						$datum->geefID()
						);
		
		return self::verzamel($ids);
	}

	static public function vanCommissie ($cie, $status = 'huidige')
	{
		global $WSW4DB;

		if ($cie instanceof Commissie)
			$cie = $cie->geefID();

		switch($status)
		{
			case 'huidige':
				$constraint = ' AND (`datumBegin` <= NOW() OR `datumBegin` is NULL)'
							. ' AND (`datumEind` > NOW() OR `datumEind` is NULL)';
				break;
			case 'oud':
				$constraint = ' AND (`datumEind` < NOW())';
				break;
			default:
				$constraint = '';
		}

		$ids = $WSW4DB->q('COLUMN SELECT `persoon_contactID`'
						. ' FROM `CommissieLid`'
						. ' WHERE `commissie_commissieID` = %i'
						. $constraint
						. ' ORDER BY `datumBegin`'
						, $cie
						);

		return self::verzamel($ids);
	}

	static public function getBugToewijzingen (Bug $bug)
	{
		global $WSW4DB;
		$id = $bug->geefID();

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `BugToewijzing`.`persoon_contactID`
			FROM `BugToewijzing`
			WHERE `BugToewijzing`.`bug_bugID` = %i
			AND `BugToewijzing`.`eind_bugBerichtID` IS NULL',
			$id);

		return self::verzamel($ids);
	}

	static public function getBugVolgers (Bug $bug)
	{
		global $WSW4DB;
		$id = $bug->geefID();

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `BugVolger`.`persoon_contactID`
			FROM `BugVolger`
			WHERE `BugVolger`.`bug_bugID` = %i',
			$id);

		return self::verzamel($ids);
	}

	static public function geefMogelijkeBugToewijzingen (Bug $bug)
	{
		$huidige = self::getBugToewijzingen($bug);
		$huidige->sorteer('Naam');

		$cie = $bug->getCategorie()->getCommissie();
		$cieLeden = $cie->leden();
		$cieLeden->sorteer('Naam');

		foreach ($cieLeden as $cieLid)
			$huidige->voegtoe($cieLid);

		return $huidige;
	}

	/*
	 * @brief Geef een SQL WHERE-expressie die uitdrukt wie dit lid kan vinden.
	 *
	 * @param bijnalid Geeft aan of de gebruiker alleen bijnaleden ziet.
	 */
	static private function opzoekFilter($bijnalid = false)
	{
		if ($bijnalid) {
			return '`Lid`.`lidToestand` = "BIJNALID"';
		}

		// bestuur ziet iedereen
		if (hasAuth('bestuur')) {
			return '';
		}

		// Begin met een lege string waar we steeds meer dingen inplakken.
		$opzoekFilter = "";

		// spoken mogen niet-leden ook zien
		if (hasAuth('spocie'))
			$opzoekFilter .= '`Lid`.`opzoekbaar` IS NULL OR';

		// intro mag bijnaleden zien
		if (hasAuth('intro'))
			$opzoekFilter .= '`Lid`.`lidToestand` = "BIJNALID" OR ';

		// (oud-)leden zien leden die voor iedereen zichtbaar zijn
		$opzoekFilter .= '(' . WSW4_WHERE_LIDOFOUDLID . ' AND (`Lid`.`opzoekbaar` = \'A\'';

		// leden zien leden die alleen voor huidige leden zichtbaar zijn
		if (hasAuth('lid'))
			$opzoekFilter .= ' OR (`Lid`.`opzoekbaar` = \'J\' AND (`Lid`.`lidTot` > NOW() OR `Lid`.`lidTot` IS NULL))';
		
		$opzoekFilter .= '))';

		return $opzoekFilter;
	}

	/*  Zoekt op alles wat het kan bedenken en geeft all contactid's
	 * (lidnrs) terug die het mogelijk weet te matchen.
	 */
	static public function zoek ($arg, $bijnalid = false)
	{
		if (!hasAuth('ingelogd'))
			return new PersoonVerzameling();

		global $WSW4DB;

		if ($arg > 1000 && $arg < 9999 && hasAuth('bestuur'))
			$arg = 'id:'.$arg;

		// bepaal wie wie mag opzoeken
		$opzoekFilter = '';
		
		$bijnalid = $bijnalid && Dili_Controller::magInschrijven();
		
		$opzoekFilter = static::opzoekFilter($bijnalid);

		// maak hem klaar om hem in de query te plakken
		if ($opzoekFilter)
			$opzoekExpressie = ' AND (' . $opzoekFilter . ')';
		else
			$opzoekExpressie = '';

		if (!$bijnalid && preg_match('/cie:(?:"|\')?([a-z0-9\ ]+)(?:"|\')?/i', $arg, $matches))
		{
			$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `persoon_contactID` FROM `CommissieLid`'
				   . ' LEFT JOIN `Commissie` ON `CommissieLid`.`commissie_commissieID` = `Commissie`.`commissieID`'
				   . ' LEFT JOIN `Persoon` ON `CommissieLid`.`persoon_contactID` = `Persoon`.`contactID`'
				   . ' LEFT JOIN `Lid` USING (`contactID`)'
				   //Begin zoek in cies
				   . ' WHERE (`Commissie`.`naam` LIKE %c'
				   . ' OR `Commissie`.`login` LIKE %c'
				   . ' OR `Commissie`.`email` LIKE %c)'
				   . ' AND (`CommissieLid`.`datumEind` IS NULL OR `CommissieLid`.`datumEind` > NOW())'
				   //Eind zoek, begin filter
				   . ((!hasAuth('lid')) ? ' AND `Lid`.`opzoekbaar` != \'J\'' : '')  //Oudleden mogen geen J zien
				   . ((!hasAuth('bestuur')) ? ' AND `Lid`.`opzoekbaar` != \'N\''	//(Oud)leden mogen geen N zien
											. ' AND (`Lid`.`lidTot` > NOW() OR `Lid`.`lidTot` IS NULL OR `Lid`.`opzoekbaar` = \'A\')'
											. ' AND '. WSW4_WHERE_LIDOFOUDLID : '')
				   . ' LIMIT 50',
					$matches[1], $matches[1], $matches[1]);
		}
		elseif (!$bijnalid && preg_match('/id:([0-9]+)/i', $arg, $matches) && hasAuth('bestuur'))
		{
			$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `contactID` FROM `Persoon`'
				   . ' WHERE `Persoon`.`contactID` = %i',
					$matches[1]);
		}
		elseif (!$bijnalid && preg_match('/(?:persoon|person):(?:"|\')?([a-z0-9\ ]+)(?:"|\')?/i', $arg, $matches))
		{
			$hastelmatches = preg_match('/(?:persoon|person):(?:"|\')?([0-9\(\)\ -]+)(?:"|\')?/i', $arg, $telmatches);
			$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `contactID` FROM `Persoon`'
				   . ' LEFT JOIN `Lid` USING (`contactID`)'
				   . ' LEFT JOIN `Contact` USING (`contactID`)'
				   . ' LEFT OUTER JOIN `ContactTelnr` ON `Persoon`.`contactID` = `ContactTelnr`.`contact_contactID`'
				   //Begin zoek in leden
				   . ' WHERE (CONCAT_WS(\' \', `Persoon`.`voornaam`, `Persoon`.`tussenvoegsels`, `Persoon`.`achternaam`) LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`voornaam`, `Persoon`.`achternaam`) LIKE %c'
                   // tussenvoegsels voor of na achternaam mogen allebei
				   . ' OR CONCAT_WS(\' \', `Persoon`.`tussenvoegsels`, `Persoon`.`achternaam`, `Persoon`.`voornaam`) LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`achternaam`, `Persoon`.`voornaam`, `Persoon`.`tussenvoegsels`)  LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`achternaam`, `Persoon`.`voornaam`) LIKE %c'
				   . ' OR `Persoon`.`bijnaam` LIKE %c'
				   . ' OR `Contact`.`email` LIKE %c'
				   . ($hastelmatches ? ' OR REPLACE(REPLACE(REPLACE(REPLACE(`ContactTelnr`.`telefoonnummer`, \'-\', \'\'), \' \', \'\'), \'(\', \'\'), \')\', \'\') LIKE %c' : '%_')
				   . (hasAuth('bestuur') ? ' OR `Lid`.`studentnr` LIKE %s' : '%_')
				   . ' OR `Persoon`.`contactID` = %i)'
				   //Eind zoek, begin filter
				   . $opzoekExpressie
				   . ' LIMIT 50',
					$matches[1], $matches[1], $matches[1], $matches[1], $matches[1], $matches[1], $matches[1], ($hastelmatches ? $telmatches[1] : $matches[1]), $matches[1], $matches[1]);
		}
		else
		{
			$tel = preg_replace('/[^0-9]+/i', '', $arg);
			$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `persoon_contactID` FROM `CommissieLid`'
				   . ' LEFT JOIN `Commissie` ON `CommissieLid`.`commissie_commissieID` = `Commissie`.`commissieID`'
				   . ' LEFT JOIN `Persoon` ON `CommissieLid`.`persoon_contactID` = `Persoon`.`contactID`'
				   . ' LEFT JOIN `Lid` USING (`contactID`)'

				   //Begin zoek in cies
				   . ' WHERE (`Commissie`.`naam` LIKE %c'
				   . ' OR `Commissie`.`login` LIKE %c'
				   . ' OR `Commissie`.`email` LIKE %c)'
				   . ' AND (`CommissieLid`.`datumEind` IS NULL OR `CommissieLid`.`datumEind` > NOW())'
				   //Eind zoek, begin filter
				   //TODO: Zoek uit waarom dit apart is, en niet bij de opzoekExpressie hoort
				   . ((!hasAuth('lid')) ? ' AND `Lid`.`opzoekbaar` != \'J\'' : '')  //Oudleden mogen geen J zien
				   . ((!hasAuth('bestuur')) ? ' AND `Lid`.`opzoekbaar` != \'N\''	//(Oud)leden mogen geen N zien
											. ' AND (`Lid`.`lidTot` > NOW() OR `Lid`.`lidTot` IS NULL OR `Lid`.`opzoekbaar` = \'A\')'
											. ' AND `Lid`.`lidToestand` != \'BIJNALID\' AND `Lid`.`lidToestand` != \'BOEKENKOPER\' AND `Lid`.`lidToestand` != \'GESCHORST\'' : '')
				   . $opzoekExpressie
				   . ' LIMIT 20'
				   . ' UNION' //Tweede query om ook in de ledentabel te zoeken
				   . ' SELECT DISTINCT `contactID` FROM `Persoon`'
				   . ' LEFT JOIN `Lid` USING (`contactID`)'
				   . ' LEFT JOIN `Contact` USING (`contactID`)'
				   . ' LEFT OUTER JOIN `ContactTelnr` ON `Persoon`.`contactID` = `ContactTelnr`.`contact_contactID`'
				   //Begin zoek in leden
				   . ' WHERE (CONCAT_WS(\' \', `Persoon`.`voornaam`, `Persoon`.`tussenvoegsels`, `Persoon`.`achternaam`) LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`voornaam`, `Persoon`.`achternaam`) LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`tussenvoegsels`, `Persoon`.`achternaam`, `Persoon`.`voornaam`) LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`achternaam`, `Persoon`.`voornaam`, `Persoon`.`tussenvoegsels`) LIKE %c'
				   . ' OR CONCAT_WS(\' \', `Persoon`.`achternaam`, `Persoon`.`voornaam`) LIKE %c'
				   . ' OR `Persoon`.`bijnaam` LIKE %c'
				   . ' OR `Contact`.`email` LIKE %c'
				   . ($tel ? ' OR REPLACE(REPLACE(REPLACE(REPLACE(`ContactTelnr`.`telefoonnummer`, \'-\', \'\'), \' \', \'\'), \'(\', \'\'), \')\', \'\') LIKE %c' : '%_')
				   . (hasAuth('bestuur') ? ' OR `Lid`.`studentnr` LIKE %s' : '%_')
				   . ' OR `Persoon`.`contactID` = %i)'
				   //Eind zoek, begin filter
				   . $opzoekExpressie
				   . ' LIMIT 50',
					$arg, $arg, $arg, $arg, $arg, $arg, $arg, $arg, $arg, $arg, ($tel ?: $arg), $arg, $arg);
		}
		$ids = array_reverse($ids);
		return PersoonVerzameling::verzamel($ids);
	}



/*
 *  <UITGEBREID ZOEKEN>
 */

	/**
	 *  We willen uitgebreid gaan zoeken in de database op groepen met bonus 
	 *  criteria en zo nodig ook nog op persoonscriteria die we aan normaal aan
	 *  simpelzoeken voeren. De gemaakte query bestaat dus uit vijf delen:
	 *  groepselectie, bonuscriteria (bal, master, oud-lid, etc...),
	 *  persoonscriteria, sortering en limieten van aantal resultaten.
	 **/
	public static function uitgebreidZoeken($groep, $criteriaOpties = array(), $nolimit = false)
	{
		global $WSW4DB;

		//Eerst de groep selecteren
		$groepsDeel = PersoonVerzameling::groepSelectie($groep);

		//Nu de extra criteria toevoegen
		/**
		 *  Binnen de criteriaset hebben we groepen: groep1 AND groep2 AND groep3
		 *  Binnen een groep hebben we verschillende criteria: c1 OR c2 OR c3
		 */
		$criteriaDeel = "";
		$and = array();
		foreach($criteriaOpties as $criteria)//Voor elke groep
		{
			$or = array();
			foreach($criteria as $criterion => $value) //voor elke criterion
			{
				// TODO: misschien dit op basis van voornaamwoorden doen
				if(($value != 0 && is_numeric($value)) || $value == "on" || in_array($value, Persoon::enumsGeslacht()))
				{
					$condition = PersoonVerzameling::criterionSelectie($criterion, $value);
					if (!is_null($condition))
						$or[] = $condition;
				}
			}
			//Voeg toe als er iets in de groep zit
			if(count($or) > 0) $and[] = "(" . implode(" OR ", $or) . ")";
		}
		//Voeg toe als er minstens een groep met inhoud is
		if (count($and) > 0 ) $criteriaDeel = " AND (".implode(" AND ", $and) . ")";

		//En nog even op persoonsinformatie zoeken

		//Sortering
		$sorteerDeel = PersoonVerzameling::uitgebreidZoekenSortering();

		//Limitering van aantal results en welk deel we willen hebben.
		$limietDeel = "";
		if(!$nolimit) $limietDeel = PersoonVerzameling::uitgebreidZoekenLimiet();

		//Doe de query twee keer, SQL is retesnel dus geen probleem!
		$res = $WSW4DB->q("COLUMN SELECT contactID FROM `Persoon` ". $groepsDeel . $criteriaDeel . $sorteerDeel . $limietDeel);
		$count = $WSW4DB->q("VALUE SELECT COUNT(contactID) FROM `Persoon` ". $groepsDeel . $criteriaDeel);

		//We geven het totale aantal en de deelverzameling terug.
		return array($count, $res);
	}

	/**
	 *  Geef de array met de mogelijke groeperingen voor een selectbox.
	 */
	public static function groeperingsMogelijkhedenArray()
	{
		return array(
			'iedereen' => _('Iedereen'),
			'welvakid' => _('Wil VakIdioot (niet voor adressenlijst)'),
			'geenvakid' => _('Geen VakIdioot opsturen'),
			'actief' => _('Actief (excl. disputen en groepen)'),
			'actiefplus' => _('Actief (incl. disputen en groepen)'),
			'deelnemers' => _('Deelnemers afgelopen 365 dagen'),
			'metadresgegevens' => _('Met adresgegevens'),
			'zonderadresgegevens' => _('Zonder adresgegevens'),
			'metemail' => _('Met email'),
			'zonderemail' => _('Zonder email'),
			'donateurs' => _('Donateurs'),
			'donateursoud' => _('Oude donateurs'),
			'recentafgestudeerd' => sprintf(_('Afgelopen collegejaar (%s) afgestudeerd'),
				colJaar()-1 . '-' . colJaar()),
			'almanak' => _('Mensen die in de almanak willen'),
			'actief365' => _('Afgelopen 365 dagen actief geweest'),
			'actiefgeweest' => _('Afgelopen collegejaar actief geweest'),
			'OntvangtAes2RootsGeenDona' => _('Mensen die A-es2 Roots ontvangen en geen Dona zijn'),
			'18min' => _('Mensen jonger dan 18'),
			'kart' => _('De inhoud van mijn kart')
		);
	}

	/**
	 *  Geef de juiste SQL om de groep te selecteren.
	 */
	public static function groepSelectie($selectie)
	{
		switch($selectie)
			{
				case "iedereen":
					return 'WHERE 1 = 1';
					break;
				case "welvakid":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Contact` WHERE `vakidOpsturen` = 1)';
					break;
				case "geenvakid":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Contact` WHERE `vakidOpsturen` = 0)';
					break;
				case "actief":
					return 'WHERE `Persoon`.`contactID` IN (SELECT DISTINCT `persoon_contactID` FROM `CommissieLid` 
							LEFT JOIN `Commissie` ON `Commissie`.`commissieID` = `CommissieLid`.`commissie_commissieID`
							WHERE (`CommissieLid`.`datumBegin` <= NOW() OR `Commissie`.`datumBegin` IS NULL) AND (`CommissieLid`.`datumEind` >= NOW() OR `CommissieLid`.`datumEind` IS NULL)
							AND (`Commissie`.`datumBegin` <= NOW() OR `Commissie`.`datumBegin` IS NULL) AND (`Commissie`.`datumEind` >= NOW() OR `Commissie`.`datumEind` IS NULL)
							AND `Commissie`.`soort` = "CIE")';
					break;
				case "actiefplus":
					return 'WHERE `Persoon`.`contactID` IN (SELECT DISTINCT `persoon_contactID` FROM `CommissieLid` 
							LEFT JOIN `Commissie` ON `Commissie`.`commissieID` = `CommissieLid`.`commissie_commissieID`
							WHERE (`CommissieLid`.`datumBegin` <= NOW() OR `Commissie`.`datumBegin` IS NULL) AND (`CommissieLid`.`datumEind` >= NOW() OR `CommissieLid`.`datumEind` IS NULL)
							AND (`Commissie`.`datumBegin` <= NOW() OR `Commissie`.`datumBegin` IS NULL) AND (`Commissie`.`datumEind` >= NOW() OR `Commissie`.`datumEind` IS NULL))';
					break;
				case "deelnemers":
					return 'WHERE `Persoon`.`contactID` IN (SELECT DISTINCT `persoon_contactID` FROM `Deelnemer` JOIN `Activiteit` 
							ON `Activiteit`.`activiteitID`=`Deelnemer`.`activiteit_activiteitID` 
							WHERE DATEDIFF(`Activiteit`.`momentBegin`, NOW()) > -365)';
					break;
				case "metadresgegevens":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contact_contactID` FROM `ContactAdres` WHERE `soort` IN ("THUIS", "OUDERS"))';
					break;
				case "zonderadresgegevens":
					return 'WHERE `Persoon`.`contactID` NOT IN (SELECT `contact_contactID` FROM `ContactAdres` WHERE `soort` IN ("THUIS", "OUDERS"))';
					break;
				case "metemail":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Contact` WHERE `email` IS NOT NULL)';
					break;
				case "zonderemail":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Contact` WHERE `email` IS NULL)';
					break;
				case "donateurs":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `persoon_contactID` FROM `Donateur` WHERE `jaarEind` IS NULL OR `jaarEind` >= YEAR(CURDATE()) )';
					break;
				case "donateursoud":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `persoon_contactID` FROM `Donateur` WHERE `jaarEind` IS NOT NULL AND `jaarEind` < YEAR(CURDATE()) )';
					break;
				case "recentafgestudeerd":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` l JOIN `LidStudie` ls 
							ON `l`.`contactID`=`ls`.`lid_contactID`
							WHERE   l.lidTot > "'.(colJaar()-1).'-09-01" AND l.lidtot <= "'.colJaar().'-09-01" 
									AND ls.datumEind > "'.(colJaar()-1) .'-09-01" AND ls.datumEind <= "'.colJaar().'-09-01")';
					break;
				case "almanak":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `inAlmanak` = 1)';
					break;
				case "actief365":
					return 'WHERE `Persoon`.`contactID` IN (SELECT DISTINCT `persoon_contactID` FROM `CommissieLid` WHERE 
						 	((DATEDIFF(NOW(),`datumEind`) < 365 OR `datumEind` IS NULL)
							AND (DATEDIFF(`datumBegin`,NOW()) < 0 OR `datumBegin` IS NULL))
							AND `commissie_commissieID` IN 
								(SELECT `commissieID` FROM `Commissie` WHERE
								`soort` = "CIE" 
						 		AND ((DATEDIFF(NOW(),`datumEind`) < 365 OR `datumEind` IS NULL)
								AND (DATEDIFF(`datumBegin`,NOW()) < 0 OR `datumBegin` IS NULL))
							))';
					break;
				case "actiefgeweest":
					return 'WHERE `Persoon`.`contactID` IN (SELECT DISTINCT `persoon_contactID` FROM `CommissieLid` WHERE 
						 	((DATEDIFF(`datumEind`,"'.(colJaar()-1).'-09-01") > 0 OR `datumEind` IS NULL)
							AND (DATEDIFF("'.colJaar().'-09-01", `datumBegin`) < 365 OR `datumBegin` IS NULL))
							AND `commissie_commissieID` IN 
								(SELECT `commissieID` FROM `Commissie` WHERE
								`soort` = "CIE" 
						 		AND ((DATEDIFF(`datumEind`, "'.(colJaar()-1).'-09-01") > 0 OR `datumEind` IS NULL)
								AND (DATEDIFF("'.colJaar().'-09-01", `datumBegin`) < 365 OR `datumBegin` IS NULL))
							))';
					break;
				case "OntvangtAes2RootsGeenDona":
					return 'WHERE `Persoon`.`contactID` IN (SELECT `contactID` FROM `Contact` WHERE `aes2rootsOpsturen` = 1) 
							AND `Persoon`.`contactID` NOT IN (SELECT `persoon_contactID` FROM `Donateur`)'; //Dona's worden apart gemaild.
					break;
				case "18min":
					return 'WHERE DATEDIFF(NOW() ,DATE_ADD(`datumGeboorte`, INTERVAL 18 YEAR)) < 0'; 
				case "kart":
					return 'WHERE `Persoon`.`contactID` IN (' . implode(", ", Kart::geef()->getAllePersonen()->keys()) . ')';
				default:
					return user_error('Onbekende groep: ' . $selectie, E_USER_ERROR);;
			}
	}



	/**
	 *  Geef de array met de mogelijke criteria bij uitgebreidzoeken. Hier kan
	 *  misschien beter iets generieks mee? Let wel op dat iedere subarray over
	 *  de zelfde colom in db/groep gaat. Tussen de subarrays krijgen we later
	 *  AND en binnen een subarray OR.
	 */
	public static function uitgebreidzoekenCriteriaArray()
	{
		return array(
			array(  "lid",
					"oudlid",
					"boekenkoper",
					"bijnalid",
					"bal"),
			array(  "icaactief",
					"ica",
                    "gtactief",
                    "gt",
					"ikuactief",
					"iku",
					"naactief",
					"na",
					"wisactief",
                    "wis",
                    "witactief",
                    "wit"),
			array(  "bachelor",
					"master",
					"exchange"),
			array(  "bounces"),
			array(	"geslacht"),
			array(  "lidvan")
		);
	}

	/**
	 *  Geef de juiste SQL om de groep te selecteren. Dit kan natuurlijk
	 *  efficienter kwa code maar is voor nu wel zo leesbaar.
	 */
	public static function criterionSelectie($criterion, $value)
	{
		$sql = "";
		$studieactief = "";
		switch($criterion)
			{
				case "lid":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `lidToestand` = "LID")';	
					break;
				case "exchange":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `lidExchange` = 1)';	
					break;
				case "bal":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `lidToestand` = "BAL")';
					break;
				case "oudlid":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `lidToestand` = "OUDLID" OR `lidToestand` = "LIDVANVERDIENSTE" OR `lidToestand` = "ERELID")'; //Geef mij een niet oud lid dat van verdienste dan wel ere is, en dan draai ik deze code terug.
					break;
				case "boekenkoper":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `lidToestand` = "BOEKENKOPER")';
					break;
				case "bijnalid":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE `lidToestand` = "BIJNALID")';
					break;
				case "bachelor":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT lid_contactID FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`fase` = "BA")';
					break;
				case "master":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT lid_contactID FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`fase` = "MA")';
					break;
				case "icaactief":
					$studieactief = ' AND '. WSW4_WHERE_STUDIE_ACTIEF;
				case "ica":
					if($value) return $sql .= "`Persoon`.`contactID` IN (SELECT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`soort` = 'IC' $studieactief)";
					break;
				case "gtactief":
					$studieactief = ' AND '. WSW4_WHERE_STUDIE_ACTIEF;
				case "gt":
					if($value) return $sql .= "`Persoon`.`contactID` IN (SELECT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`soort` = 'GT' $studieactief)";
					break;
				case "ikuactief":
					$studieactief = ' AND '. WSW4_WHERE_STUDIE_ACTIEF;
				case "iku":
					if($value) return $sql .= "`Persoon`.`contactID` IN (SELECT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`soort` = 'IK' $studieactief)";
					break;
				case "naactief":
					$studieactief = ' AND '. WSW4_WHERE_STUDIE_ACTIEF;
				case "na":
					if($value) return $sql .= "`Persoon`.`contactID` IN (SELECT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`soort` = 'NA' $studieactief)";
					break;
				case "wisactief":
					$studieactief = ' AND '. WSW4_WHERE_STUDIE_ACTIEF;
				case "wis":
					if($value) return $sql .= "`Persoon`.`contactID` IN (SELECT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`soort` = 'WI' $studieactief)";
					break;
				case "witactief":
					$studieactief = ' AND '. WSW4_WHERE_STUDIE_ACTIEF;
				case "wit":
					if($value) return $sql .= "`Persoon`.`contactID` IN (SELECT `lid_contactID` FROM `LidStudie` JOIN `Studie` ON `LidStudie`.`studie_studieID` = `Studie`.`studieID` WHERE `Studie`.`soort` = 'WIT' $studieactief)";
					break;
				case "bounces":
					if($value) return $sql .= '`Persoon`.`contactID` IN (SELECT `contact_contactID` FROM `MailingBounce`)';
					break;
				case "lidvan":
					//NOTE: hier wordt direct userinput in de query gepropt omdat het de rest van de code een stuk simpeler maakt.
					// Dit is eigenlijk niet de bedoeling maar gelukkig wordt er gecheckt of de inhoud een integer is of niet, zo
					// niet doen we ook niets. Als er meer van dit soort gevallen komen is het beter om de code te refactoren naar
					// naar de juiste manier.
					if(is_numeric($value)) return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Lid` WHERE
									  year(`lidVan`) = '.$value.')';
					break;
				case "geslacht":
					switch($value) {
						case "M":
							return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Persoon` WHERE `geslacht` = \'M\')';
						case "V":
							return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Persoon` WHERE `geslacht` = \'V\')';
						case "?":
							return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Persoon` WHERE `geslacht` = \'?\')';
						case "": // matchet ook waarde NULL
						default:
							return $sql .= '`Persoon`.`contactID` IN (SELECT `contactID` FROM `Persoon` WHERE `geslacht` IS NULL)';
					}
				default:
					return user_error('Onbekend criterion: ' . $criterion, E_USER_ERROR);
			}
	}

	public static function sorteerMogelijkheden()
	{
		return array(   "lidnr" => "Lidnummer",
						"voornaam" => "Voornaam",
						"achternaam" => "Achternaam"
					);
	}

	public static function uitgebreidZoekenSortering()
	{
		$sortering = trypar('sortering', 'lidnr');
		$sql = "";
		switch($sortering)
			{
				case "lidnr":
					$sql = " ORDER BY ContactID";
					break;
				case "voornaam":
					$sql = " ORDER BY voornaam";
					break;
				case "achternaam":
					$sql = " ORDER BY achternaam";
					break;
				default:
					return user_error('Onbekend sortering: ' . $sortering, E_USER_ERROR);
			}

		if(trypar('aflopend') == 'on') {
			$sql .= " DESC";
		}

		return $sql;
	}

	//Haal de limiet informatie uit de get parameters van de pagina en geef de sql.
	public static function uitgebreidZoekenLimiet()
	{
		$sql = "";
		if(trypar('limit')){
			$limit = explode("_",trypar('limit'));
			$sql = " LIMIT $limit[0], " . ($limit[1] - $limit[0]);
		} else {
			$sql = " LIMIT 50";
		}
		return $sql;
	}

/*
 *  </UITGEBREID ZOEKEN>
 */

	/**
	 *  Zoekfuncties voor onjuiste gegevens
	 **/

	/**
	 *  Vind alle mensen met namen die gecheckt kunnen worden
	 *  (die dus niet voldoen aan het standaardformaat)
	 **/
	public static function checkNaam()
	{
		$result = static::alleenAchternaam();
		$result->union(static::kleineLetterNaam());

		return $result;
	}

	/**
	 *  Mensen met alleen een achternaam
	 **/
	public static function alleenAchternaam()
	{

		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `contactID` FROM `Persoon` 
							WHERE `voornaam` IS NULL
							AND `voorletters` = ''
							AND `geboortenamen` IS NULL
							AND `tussenvoegsels` IS NULL
							AND `achternaam` IS NOT NULL
							");

		return PersoonVerzameling::verzamel($ids);
	}
	
	/**
	 *  Vind personen met een naam beginnend met een kleine letter
	 **/
	public static function kleineLetterNaam()
	{

		global $WSW4DB;

		// BINARY vergelijkingen omdat we normaal case-insensitive zijn
		$ids = $WSW4DB->q("COLUMN SELECT `contactID` FROM `Persoon`
		                    WHERE (`voornaam` <> ''
		                           AND LEFT(`voornaam`, 1) = BINARY LOWER(LEFT(`voornaam`, 1)))
		                    OR    (`voorletters` <> ''
		                           AND LEFT(`voorletters`, 1) = BINARY LOWER(LEFT(`voorletters`, 1)))
		                    OR    (`achternaam` <> ''
		                           AND LEFT(`achternaam`, 1) = BINARY LOWER(LEFT(`achternaam`, 1)))
		");

		return PersoonVerzameling::verzamel($ids);
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
