<?

/**
 * $Id$
 */
class CommissieActiviteitVerzameling
	extends CommissieActiviteitVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // CommissieActiviteitVerzameling_Generated
	}

	public function getCommissies()
	{
		$verz = new CommissieVerzameling();

		foreach($this as $cieAct)
		{
			$verz->voegtoe($cieAct->getCommissie());
		}

		return $verz;
	}

	static public function vanActiviteit(Activiteit $act)
	{
		return CommissieActiviteitQuery::table()
			->whereProp('Activiteit', $act)
			->verzamel();
	}

	static public function vanActiviteiten(ActiviteitVerzameling $acts)
	{
		$ids = array();
		if($acts->aantal() > 0)
		{
			$actIDs = array();
			foreach($acts as $act) {
				$actIDs[] = $act->getActiviteitID();
			}
			global $WSW4DB;
			$ids = $WSW4DB->q('TABLE SELECT `commissie_commissieID` as "0"'
							.	', `activiteit_activiteitID` as "1"'
							.' FROM `CommissieActiviteit`'
							.' WHERE `activiteit_activiteitID` IN (%Ai)'
							, $actIDs
							);
		}
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
