<?
abstract class ContactTelnrVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de ContactTelnrVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze ContactTelnrVerzameling een ContactVerzameling.
	 *
	 * @return ContactVerzameling
	 * Een ContactVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze ContactTelnrVerzameling.
	 */
	public function toContactVerzameling()
	{
		if($this->aantal() == 0)
			return new ContactVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getContactContactID()
			                      );
		}
		$this->positie = $origPositie;
		return ContactVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ContactTelnrVerzameling van Contact.
	 *
	 * @return ContactTelnrVerzameling
	 * Een ContactTelnrVerzameling die elementen bevat die bij de Contact hoort.
	 */
	static public function fromContact($contact)
	{
		if(!isset($contact))
			return new ContactTelnrVerzameling();

		return ContactTelnrQuery::table()
			->whereProp('Contact', $contact)
			->verzamel();
	}
}
