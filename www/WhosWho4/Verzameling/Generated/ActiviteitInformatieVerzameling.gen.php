<?
abstract class ActiviteitInformatieVerzameling_Generated
	extends ActiviteitVerzameling
{
	/**
	 * @brief De constructor van de ActiviteitInformatieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ActiviteitVerzameling
	}
	/**
	 * @brief Maak van deze ActiviteitInformatieVerzameling een
	 * ActiviteitCategorieVerzameling.
	 *
	 * @return ActiviteitCategorieVerzameling
	 * Een ActiviteitCategorieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze ActiviteitInformatieVerzameling.
	 */
	public function toCategorieVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitCategorieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCategorieActCategorieID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitCategorieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ActiviteitInformatieVerzameling van Categorie.
	 *
	 * @return ActiviteitInformatieVerzameling
	 * Een ActiviteitInformatieVerzameling die elementen bevat die bij de Categorie
	 * hoort.
	 */
	static public function fromCategorie($categorie)
	{
		if(!isset($categorie))
			return new ActiviteitInformatieVerzameling();

		return ActiviteitInformatieQuery::table()
			->whereProp('Categorie', $categorie)
			->verzamel();
	}
}
