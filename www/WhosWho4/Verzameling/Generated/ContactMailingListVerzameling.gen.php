<?
abstract class ContactMailingListVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de ContactMailingListVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze ContactMailingListVerzameling een ContactVerzameling.
	 *
	 * @return ContactVerzameling
	 * Een ContactVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze ContactMailingListVerzameling.
	 */
	public function toContactVerzameling()
	{
		if($this->aantal() == 0)
			return new ContactVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getContactContactID()
			                      );
		}
		$this->positie = $origPositie;
		return ContactVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze ContactMailingListVerzameling een MailingListVerzameling.
	 *
	 * @return MailingListVerzameling
	 * Een MailingListVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze ContactMailingListVerzameling.
	 */
	public function toMailingListVerzameling()
	{
		if($this->aantal() == 0)
			return new MailingListVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMailingListMailingListID()
			                      );
		}
		$this->positie = $origPositie;
		return MailingListVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ContactMailingListVerzameling van Contact.
	 *
	 * @return ContactMailingListVerzameling
	 * Een ContactMailingListVerzameling die elementen bevat die bij de Contact hoort.
	 */
	static public function fromContact($contact)
	{
		if(!isset($contact))
			return new ContactMailingListVerzameling();

		return ContactMailingListQuery::table()
			->whereProp('Contact', $contact)
			->verzamel();
	}
	/**
	 * @brief Maak een ContactMailingListVerzameling van MailingList.
	 *
	 * @return ContactMailingListVerzameling
	 * Een ContactMailingListVerzameling die elementen bevat die bij de MailingList
	 * hoort.
	 */
	static public function fromMailingList($mailingList)
	{
		if(!isset($mailingList))
			return new ContactMailingListVerzameling();

		return ContactMailingListQuery::table()
			->whereProp('MailingList', $mailingList)
			->verzamel();
	}
}
