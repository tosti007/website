<?
abstract class DeelnemerAntwoordVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de DeelnemerAntwoordVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze DeelnemerAntwoordVerzameling een DeelnemerVerzameling.
	 *
	 * @return DeelnemerVerzameling
	 * Een DeelnemerVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze DeelnemerAntwoordVerzameling.
	 */
	public function toDeelnemerVerzameling()
	{
		if($this->aantal() == 0)
			return new DeelnemerVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getDeelnemercontactID()
			                      ,$obj->getDeelnemeractiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return DeelnemerVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze DeelnemerAntwoordVerzameling een
	 * ActiviteitVraagVerzameling.
	 *
	 * @return ActiviteitVraagVerzameling
	 * Een ActiviteitVraagVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze DeelnemerAntwoordVerzameling.
	 */
	public function toVraagVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVraagVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVraagactiviteitID()
			                      ,$obj->getVraagVraagID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVraagVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DeelnemerAntwoordVerzameling van Deelnemer.
	 *
	 * @return DeelnemerAntwoordVerzameling
	 * Een DeelnemerAntwoordVerzameling die elementen bevat die bij de Deelnemer hoort.
	 */
	static public function fromDeelnemer($deelnemer)
	{
		if(!isset($deelnemer))
			return new DeelnemerAntwoordVerzameling();

		return DeelnemerAntwoordQuery::table()
			->whereProp('Deelnemer', $deelnemer)
			->verzamel();
	}
	/**
	 * @brief Maak een DeelnemerAntwoordVerzameling van Vraag.
	 *
	 * @return DeelnemerAntwoordVerzameling
	 * Een DeelnemerAntwoordVerzameling die elementen bevat die bij de Vraag hoort.
	 */
	static public function fromVraag($vraag)
	{
		if(!isset($vraag))
			return new DeelnemerAntwoordVerzameling();

		return DeelnemerAntwoordQuery::table()
			->whereProp('Vraag', $vraag)
			->verzamel();
	}
}
