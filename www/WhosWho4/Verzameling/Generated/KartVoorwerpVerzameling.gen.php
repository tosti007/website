<?
abstract class KartVoorwerpVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de KartVoorwerpVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze KartVoorwerpVerzameling een PersoonKartVerzameling.
	 *
	 * @return PersoonKartVerzameling
	 * Een PersoonKartVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze KartVoorwerpVerzameling.
	 */
	public function toPersoonKartVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonKartVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonKartcontactID()
			                      ,$obj->getPersoonKartNaam()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonKartVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze KartVoorwerpVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze KartVoorwerpVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getPersoonContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze KartVoorwerpVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze KartVoorwerpVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCommissieCommissieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze KartVoorwerpVerzameling een MediaVerzameling.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze KartVoorwerpVerzameling.
	 */
	public function toMediaVerzameling()
	{
		if($this->aantal() == 0)
			return new MediaVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getMediaMediaID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getMediaMediaID()
			                      );
		}
		$this->positie = $origPositie;
		return MediaVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een KartVoorwerpVerzameling van PersoonKart.
	 *
	 * @return KartVoorwerpVerzameling
	 * Een KartVoorwerpVerzameling die elementen bevat die bij de PersoonKart hoort.
	 */
	static public function fromPersoonKart($persoonKart)
	{
		if(!isset($persoonKart))
			return new KartVoorwerpVerzameling();

		return KartVoorwerpQuery::table()
			->whereProp('PersoonKart', $persoonKart)
			->verzamel();
	}
	/**
	 * @brief Maak een KartVoorwerpVerzameling van Persoon.
	 *
	 * @return KartVoorwerpVerzameling
	 * Een KartVoorwerpVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new KartVoorwerpVerzameling();

		return KartVoorwerpQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een KartVoorwerpVerzameling van Commissie.
	 *
	 * @return KartVoorwerpVerzameling
	 * Een KartVoorwerpVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new KartVoorwerpVerzameling();

		return KartVoorwerpQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
	/**
	 * @brief Maak een KartVoorwerpVerzameling van Media.
	 *
	 * @return KartVoorwerpVerzameling
	 * Een KartVoorwerpVerzameling die elementen bevat die bij de Media hoort.
	 */
	static public function fromMedia($media)
	{
		if(!isset($media))
			return new KartVoorwerpVerzameling();

		return KartVoorwerpQuery::table()
			->whereProp('Media', $media)
			->verzamel();
	}
}
