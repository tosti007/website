<?
abstract class PersoonKartVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PersoonKartVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PersoonKartVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze PersoonKartVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PersoonKartVerzameling van Persoon.
	 *
	 * @return PersoonKartVerzameling
	 * Een PersoonKartVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new PersoonKartVerzameling();

		return PersoonKartQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
