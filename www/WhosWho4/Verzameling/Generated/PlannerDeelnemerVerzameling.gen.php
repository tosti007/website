<?
abstract class PlannerDeelnemerVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PlannerDeelnemerVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PlannerDeelnemerVerzameling een PlannerDataVerzameling.
	 *
	 * @return PlannerDataVerzameling
	 * Een PlannerDataVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze PlannerDeelnemerVerzameling.
	 */
	public function toPlannerDataVerzameling()
	{
		if($this->aantal() == 0)
			return new PlannerDataVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPlannerDataPlannerDataID()
			                      );
		}
		$this->positie = $origPositie;
		return PlannerDataVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze PlannerDeelnemerVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze PlannerDeelnemerVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PlannerDeelnemerVerzameling van PlannerData.
	 *
	 * @return PlannerDeelnemerVerzameling
	 * Een PlannerDeelnemerVerzameling die elementen bevat die bij de PlannerData
	 * hoort.
	 */
	static public function fromPlannerData($plannerData)
	{
		if(!isset($plannerData))
			return new PlannerDeelnemerVerzameling();

		return PlannerDeelnemerQuery::table()
			->whereProp('PlannerData', $plannerData)
			->verzamel();
	}
	/**
	 * @brief Maak een PlannerDeelnemerVerzameling van Persoon.
	 *
	 * @return PlannerDeelnemerVerzameling
	 * Een PlannerDeelnemerVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new PlannerDeelnemerVerzameling();

		return PlannerDeelnemerQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
