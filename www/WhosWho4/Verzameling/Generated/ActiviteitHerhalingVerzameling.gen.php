<?
abstract class ActiviteitHerhalingVerzameling_Generated
	extends ActiviteitVerzameling
{
	/**
	 * @brief De constructor van de ActiviteitHerhalingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ActiviteitVerzameling
	}
	/**
	 * @brief Maak van deze ActiviteitHerhalingVerzameling een ActiviteitVerzameling.
	 *
	 * @return ActiviteitVerzameling
	 * Een ActiviteitVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze ActiviteitHerhalingVerzameling.
	 */
	public function toParentVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getParentActiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ActiviteitHerhalingVerzameling van Parent.
	 *
	 * @return ActiviteitHerhalingVerzameling
	 * Een ActiviteitHerhalingVerzameling die elementen bevat die bij de Parent hoort.
	 */
	static public function fromParent($parent)
	{
		if(!isset($parent))
			return new ActiviteitHerhalingVerzameling();

		return ActiviteitHerhalingQuery::table()
			->whereProp('Parent', $parent)
			->verzamel();
	}
}
