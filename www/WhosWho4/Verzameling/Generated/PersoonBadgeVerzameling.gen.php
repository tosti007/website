<?
abstract class PersoonBadgeVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PersoonBadgeVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PersoonBadgeVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze PersoonBadgeVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PersoonBadgeVerzameling van Persoon.
	 *
	 * @return PersoonBadgeVerzameling
	 * Een PersoonBadgeVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new PersoonBadgeVerzameling();

		return PersoonBadgeQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
}
