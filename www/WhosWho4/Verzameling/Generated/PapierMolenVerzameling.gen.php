<?
abstract class PapierMolenVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de PapierMolenVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze PapierMolenVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze PapierMolenVerzameling.
	 */
	public function toLidVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLidContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PapierMolenVerzameling van Lid.
	 *
	 * @return PapierMolenVerzameling
	 * Een PapierMolenVerzameling die elementen bevat die bij de Lid hoort.
	 */
	static public function fromLid($lid)
	{
		if(!isset($lid))
			return new PapierMolenVerzameling();

		return PapierMolenQuery::table()
			->whereProp('Lid', $lid)
			->verzamel();
	}
}
