<?
abstract class DonateurVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de DonateurVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze DonateurVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze DonateurVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze DonateurVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze DonateurVerzameling.
	 */
	public function toAnderPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getAnderPersoonContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getAnderPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DonateurVerzameling van Persoon.
	 *
	 * @return DonateurVerzameling
	 * Een DonateurVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new DonateurVerzameling();

		return DonateurQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een DonateurVerzameling van AnderPersoon.
	 *
	 * @return DonateurVerzameling
	 * Een DonateurVerzameling die elementen bevat die bij de AnderPersoon hoort.
	 */
	static public function fromAnderPersoon($anderPersoon)
	{
		if(!isset($anderPersoon))
			return new DonateurVerzameling();

		return DonateurQuery::table()
			->whereProp('AnderPersoon', $anderPersoon)
			->verzamel();
	}
}
