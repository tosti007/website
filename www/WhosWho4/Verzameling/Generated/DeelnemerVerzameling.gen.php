<?
abstract class DeelnemerVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de DeelnemerVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze DeelnemerVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze DeelnemerVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze DeelnemerVerzameling een ActiviteitVerzameling.
	 *
	 * @return ActiviteitVerzameling
	 * Een ActiviteitVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze DeelnemerVerzameling.
	 */
	public function toActiviteitVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getActiviteitActiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DeelnemerVerzameling van Persoon.
	 *
	 * @return DeelnemerVerzameling
	 * Een DeelnemerVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new DeelnemerVerzameling();

		return DeelnemerQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een DeelnemerVerzameling van Activiteit.
	 *
	 * @return DeelnemerVerzameling
	 * Een DeelnemerVerzameling die elementen bevat die bij de Activiteit hoort.
	 */
	static public function fromActiviteit($activiteit)
	{
		if(!isset($activiteit))
			return new DeelnemerVerzameling();

		return DeelnemerQuery::table()
			->whereProp('Activiteit', $activiteit)
			->verzamel();
	}
}
