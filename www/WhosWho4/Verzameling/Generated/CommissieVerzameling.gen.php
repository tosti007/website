<?
abstract class CommissieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de CommissieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze CommissieVerzameling een CommissieCategorieVerzameling.
	 *
	 * @return CommissieCategorieVerzameling
	 * Een CommissieCategorieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze CommissieVerzameling.
	 */
	public function toCategorieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieCategorieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCategorieCategorieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCategorieCategorieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieCategorieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een CommissieVerzameling van Categorie.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die bij de Categorie hoort.
	 */
	static public function fromCategorie($categorie)
	{
		if(!isset($categorie))
			return new CommissieVerzameling();

		return CommissieQuery::table()
			->whereProp('Categorie', $categorie)
			->verzamel();
	}
}
