<?
abstract class PersoonVerzameling_Generated
	extends ContactVerzameling
{
	/**
	 * @brief De constructor van de PersoonVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ContactVerzameling
	}
	/**
	 * @brief Maak van deze PersoonVerzameling een VoornaamwoordVerzameling.
	 *
	 * @return VoornaamwoordVerzameling
	 * Een VoornaamwoordVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze PersoonVerzameling.
	 */
	public function toVoornaamwoordVerzameling()
	{
		if($this->aantal() == 0)
			return new VoornaamwoordVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVoornaamwoordWoordID()
			                      );
		}
		$this->positie = $origPositie;
		return VoornaamwoordVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een PersoonVerzameling van Voornaamwoord.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die bij de Voornaamwoord hoort.
	 */
	static public function fromVoornaamwoord($voornaamwoord)
	{
		if(!isset($voornaamwoord))
			return new PersoonVerzameling();

		return PersoonQuery::table()
			->whereProp('Voornaamwoord', $voornaamwoord)
			->verzamel();
	}
}
