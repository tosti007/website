<?
abstract class ActiviteitVraagVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de ActiviteitVraagVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze ActiviteitVraagVerzameling een ActiviteitVerzameling.
	 *
	 * @return ActiviteitVerzameling
	 * Een ActiviteitVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze ActiviteitVraagVerzameling.
	 */
	public function toActiviteitVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getActiviteitActiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ActiviteitVraagVerzameling van Activiteit.
	 *
	 * @return ActiviteitVraagVerzameling
	 * Een ActiviteitVraagVerzameling die elementen bevat die bij de Activiteit hoort.
	 */
	static public function fromActiviteit($activiteit)
	{
		if(!isset($activiteit))
			return new ActiviteitVraagVerzameling();

		return ActiviteitVraagQuery::table()
			->whereProp('Activiteit', $activiteit)
			->verzamel();
	}
}
