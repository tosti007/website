<?

/**
 * $Id$
 */
class TinyUrlVerzameling
	extends TinyUrlVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TinyUrlVerzameling_Generated
	}

	//Geef het totaal aantal TinyUrls, handig voor overzichten.
	static public function totaalAantal()
	{
		global $WSW4DB;
		return $WSW4DB->q("COLUMN SELECT COUNT(`ID`) FROM `TinyUrl`");
	}

	static public function geefAlle($range = null)
	{
		global $WSW4DB;

		//Check of er een limit nodig is (format: plek_aantal)
		$rangeSQL = "";
		if($range){
			$range = explode("_",$range);
			$rangeSQL = " WHERE `id` <= $range[1] AND  `id` > $range[0]";
		} else {
			$rangeSQL = " WHERE `id` <= 50";
		}

		$ids = $WSW4DB->q("COLUMN SELECT `ID` FROM `TinyUrl`" . $rangeSQL);

		return TinyUrlVerzameling::verzamel($ids);

	}

	static public function vanMailing(Mailing $mailing)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `tinyUrl_id` FROM `TinyMailingUrl` 
							WHERE `mailing_mailingID` = %i", $mailing->getMailingID());

		return TinyUrlVerzameling::verzamel($ids);
	}

	public function totaalHits()
	{
		$totaal = 0;

		foreach($this as $tinyUrl){
			$totaal = $totaal + $tinyUrl->amountOfHits();
		}

		return $totaal;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
