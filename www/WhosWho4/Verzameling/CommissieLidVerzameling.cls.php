<?

/***
 * $Id$
 */
class CommissieLidVerzameling
	extends CommissieLidVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	static public function commissarissen($functie, $status = 'huidige')
	{
		global $WSW4DB;

		if(!in_array($functie, CommissieLid::enumsFunctie()))
			user_error('Onbekende functie', E_USER_ERROR);

		//TODO: dit kan kan beter worden gecentreerd zodat we geen/minder code duplicatie hebben.
		switch($status) {
		case 'huidige':
			$constraint = ' AND (`datumBegin` <= NOW() OR `datumBegin` is NULL)'
						. ' AND (`datumEind` > NOW() OR `datumEind` is NULL)'
						. ' ORDER BY `datumBegin`';
			//Cies om later op te filteren.
			$cies = CommissieVerzameling::huidige();
			break;
		/*
		TODO: hier moet nog groep oude commissies bij, zie ook bovenstaande $cies.
		case 'oud':
			$constraint = ' AND (`datumEind` < NOW())'
						. ' ORDER BY `datumEind` DESC';
			break;
		*/
		default:
			$constraint = 'ORDER BY `datumBegin`';
		}

		$ids = $WSW4DB->q('TABLE SELECT `persoon_contactID`, `commissie_commissieID` FROM `CommissieLid`
					WHERE `functie` = %s'
					. $constraint
					, $functie);

		$comids = array();
		foreach($ids as $id)
		{
			//Kijk of de commissielid ook nog bij de juist groep cies hoort.
			if($cies){
				if(!$cies->bevat($id[1]))
					continue;
			}

			$comids[] = array($id[0], $id[1]);
		}

		return self::verzamel($comids);
	}
	static public function actieveLeden()
	{
		user_error('501 Not Implemented', E_USER_ERROR);
	}

	static public function vanCommissie($commissieID, $status = 'huidige')
	{
		global $WSW4DB;

		if($commissieID instanceof Commissie)
			$commissieID = $commissieID->getCommissieID();

		//TODO: centreer dit ergens...
		switch($status) {
		case 'huidige':
			$constraint = ' AND (`datumBegin` <= NOW() OR `datumBegin` is NULL)'
						. ' AND (`datumEind` > NOW() OR `datumEind` is NULL)'
						. ' ORDER BY `datumBegin`';
			break;
		case 'oud':
			$constraint = ' AND (`datumEind` < NOW())'
						. ' ORDER BY `datumEind` DESC';
			break;
		default:
			$constraint = 'ORDER BY `datumBegin`';
		}

		$ids = $WSW4DB->q('COLUMN SELECT `persoon_contactID`'
						. ' FROM `CommissieLid`'
						. ' WHERE `commissie_commissieID` = %i'
						. $constraint
						, $commissieID
						);

		foreach($ids as &$id) {
			$id = array($id, $commissieID);
		}

		return self::verzamel($ids);
	}

	/**
	 * @brief Geef alle CommissieLid-objecten die horen bij de gegeven persoon.
	 *
	 * @param contact Een Persoon (of Contact, of zoiets) waarvan we de lidmaatschappen willen weten.
	 * @param huidige Zo ja (default), geef alleen de CommissieLid-instances die op dit moment actief zijn. Als datumBegin en/of datumEind NULL zijn, gaan we ervan uit dat die geen belemmering vormen.
	 *
	 * Dit checkt niet of de Commssie zelf nog actief is,
	 * in tegenstelling tot CommissieVerzameling::vanPersoon.
	 * Dat geeft dus "leuke" randgevallen! (soepmes, soepmes)
	 *
	 * @sa CommissieVerzameling::vanPersoon
	 *
	 * @return Een CommissieLidVerzameling.
	 */
	static public function vanPersoon($contact, $huidige = True)
	{
		// TODO: trek logica gelijk met CommissieVerzameling::vanPersoon
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `commissie_commissieID`'
						. ' FROM `CommissieLid`'
						. ' WHERE `persoon_contactID` = %i'
						. ($huidige
						? ' AND (`datumBegin` <= NOW() OR `datumBegin` is NULL)'
						. ' AND (NOW() < `datumEind` OR `datumEind` is NULL)'
						: '')
						, $contact->getContactID()
						);

		foreach($ids as &$id) {
			$id = array($contact->getContactID(), $id);
		}

		return self::verzamel($ids);
	}

	/*** Sorteren ***/
	/**
	 *  Maak het mogelijk om een CommissieLidVerzameling
	 * te sorteren op CommissieLid::getDatumBegin.
	 *
	 *	@see Verzameling::sorteer()
	 */
	static public function sorteerOpDatumBegin($aID, $bID)
	{
		$a = CommissieLid::geef($aID)->getDatumBegin();
		$b = CommissieLid::geef($bID)->getDatumBegin();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}

	/**
	 *  Maak het mogelijk om een CommissieLidVerzameling
	 * te sorteren op CommissieLid::getDatumEind.
	 *
	 *	@see Verzameling::sorteer()
	 */
	static public function sorteerOpDatumeind($aID, $bID)
	{
		$a = CommissieLid::geef($aID)->getDatumEind();
		$b = CommissieLid::geef($bID)->getDatumEind();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}
	/**
	 *	 Maak het mogelijk om een CommissieLidVerzameling
	 *  te sorteren op Lid::getNaam().
	 *
	 *	\note Vergeet niet eerst CommissieLidVerzameling::toPersoonVerzameling
	 *	aan te roepen zodat alle objecten in de cache zitten.
	 *
	 *	@see Verzameling::sorteer()
	 */
	static public function sorteerOpLidNaam($aID, $bID)
	{
		$a = CommissieLid::geef($aID)->getPersoonContactID();
		$b = CommissieLid::geef($bID)->getPersoonContactID();

		return LidVerzameling::sorteerOpNaam($a, $b);
	}
	/**
	 *  Maak het mogelijk om een CommissieLidVerzameling
	 * te sorteren op CommissieLid::getDatumBegin, en bij
	 * gelijkspel op Lid::getNaam().
	 *
	 * (Dit moet mooier kunnen met hogere-orde functies...)
	 *
	 *	@see Verzameling::sorteer()
	 *	@see CommissieLidVerzameling::sorteerOpDatumBegin
	 *	@see CommissieLidVerzameling::sorteerOpLidNaam
	 */
	static public function sorteerOpDatumBeginDanLidNaam($aID, $bID)
	{
		$a = CommissieLid::geef($aID)->getDatumBegin();
		$b = CommissieLid::geef($bID)->getDatumBegin();

		if($a < $b)
			return 1;
		if($a > $b)
			return -1;

		return self::sorteerOpLidNaam($aID, $bID);
	}

	/**
	 *  Maak het mogelijk om een CommissieLidVerzameling
	 * te sorteren op CommissieLid::getDatumEind, en bij
	 * gelijkspel op Lid::getNaam().
	 *
	 * (Dit moet mooier kunnen met hogere-orde functies...)
	 *
	 *	@see Verzameling::sorteer()
	 *	@see CommissieLidVerzameling::sorteerOpDatumEind
	 *	@see CommissieLidVerzameling::sorteerOpLidNaam
	 */
	static public function sorteerOpDatumEindDanLidNaam($aID, $bID)
	{
		$a = CommissieLid::geef($aID)->getDatumEind();
		$b = CommissieLid::geef($bID)->getDatumEind();

		if($a < $b)
			return 1;
		if($a > $b)
			return -1;

		return self::sorteerOpLidNaam($aID, $bID);
	}
	/**
	 *	 Maak het mogelijk om een CommissieLidVerzameling
	 *  te sorteren op Commissie::getNaam().
	 *
	 *	\note Vergeet niet eerst CommissieLidVerzameling::commissies aan te
	 *	roepen zodat alle Commissie objecten in de cache zitten.
	 *
	 *	@see Verzameling::sorteer()
	 */
	static public function sorteerOpCommissieNaam($aID, $bID)
	{
		$a = CommissieLid::geef($aID)->getCommissieCommissieID();
		$b = CommissieLid::geef($bID)->getCommissieCommissieID();

		return CommissieVerzameling::sorteerOpNaam($a, $b);
	}

	//Misschien generieke functie van maken? dit is nu duplicated met contactpersoon...
	public function personen()
	{
		return PersoonVerzameling::verzamel(explode(',',$this->implode('getPersoonContactID', ',')));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
