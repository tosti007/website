<?
/**
 * $Id$
 */
class StudieVerzameling
	extends StudieVerzameling_Generated
{
	static protected $bachelorStudies = array();

	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // StudieVerzameling_Generated
	}

	static public function alleStudies ($fase = NULL)
	{
		global $WSW4DB;
		$lang = getLang();
		$ids = $WSW4DB->q('COLUMN SELECT `Studie`.`studieID` FROM `Studie`' .
			(in_array($fase, Studie::enumsFase()) ? ' WHERE `Studie`.`fase`=%s' : '')
				. " ORDER BY `Studie`.`fase` ASC, `Studie`.`naam_$lang`, `Studie`.`studieID` ASC",
				$fase);
		return self::verzamel($ids);
	}

	static public function getBachelorStudies ()
	{
		if (!self::$bachelorStudies)
		{
			$studies = self::alleStudies('BA');
			foreach ($studies as $studie)
				self::$bachelorStudies[$studie->getSoort()] = $studie;
		}
		return self::$bachelorStudies;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
