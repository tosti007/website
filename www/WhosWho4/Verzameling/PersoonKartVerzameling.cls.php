<?
/**
 * $Id$
 */
class PersoonKartVerzameling
	extends PersoonKartVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PersoonKartVerzameling_Generated
	}

	static public function geefVanPersoon($persoon) {
		global $WSW4DB;

		$rows = $WSW4DB->q("TABLE SELECT `persoon_contactID`,`naam` FROM `PersoonKart` WHERE `persoon_contactID` = %i",$persoon->getContactID());

		$ids = array();
		foreach($rows as $r) {
			$ids[] = array($r['persoon_contactID'],$r['naam']);
		}

		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
