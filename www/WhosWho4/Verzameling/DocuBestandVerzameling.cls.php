<?
class DocuBestandVerzameling
	extends DocuBestandVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DocuBestandVerzameling_Generated
	}

	/*
	 *  Geeft alle bestanden bij een docucategorie
	 *
	 * @param categorie De categorie waarbij er bestanden gezocht moeten worden
	 * @return De verzameling van bestanden bij een categorie
	 */
	static public function getBestanden(DocuCategorie $categorie = null)
	{
		$query = DocuBestandQuery::table()
			->orderByDesc('datum');

		if($categorie)
		{
			$query->whereProp('Categorie', $categorie);
		}

		return $query->verzamel();
	}
}
