<?

/***
 * $Id$
 */
class LidVerzameling
	extends LidVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	static public function alleAlumni()
	{
		user_error('501 Not Implemented', E_USER_ERROR);
	}
	static public function alleInAlmanak()
	{
		user_error('501 Not Implemented', E_USER_ERROR);
	}

	//Alle leden die geen lidVV of erelid zijn.
	static public function alleLeden()
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `contactID`'
						.' FROM `Lid`'
						.' WHERE '.WSW4_WHERE_NORMAALLID
						);
		return self::verzamel($ids);
	}

	/**
	 *  Geef alle actieve leden die in commissies zitten.
	 **/
	static public function actief($oud = false)
	{

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `persoon_contactID`'
						.' FROM `CommissieLid` as a JOIN `Commissie` as b ON a.commissie_commissieID = b.commissieID'
						.' WHERE '
						. (($oud) ? ' a.datumEind < NOW()'
								  : ' a.datumBegin <= NOW()'
								  . ' AND (a.datumEind >= NOW() OR a.datumEind IS NULL)'
								  . ' AND b.soort != "DISPUUT" AND ( b.datumEind >= NOW() OR b.datumEind IS NULL)' )
								  //Dit laatste kan weg zodra 'b.datumEind heeft een gesette einddatum' => 'a.datumEind heeft een gesette datum'
						);
		return self::verzamel($ids);

	}


	/**
	 * Multi-zoeken
	 */
	static public function multizoek ($args)
	{
		global $WSW4DB;

		$query = array();
		$params = array();
		foreach ($args as $arg)
		{
			$arg = trim($arg);
			if (is_numeric($arg))
			{
				$query[] = '(SELECT `contactID` FROM `Lid`'
					   . ' LEFT JOIN `Persoon` USING (`contactID`)'
					   . ' LEFT JOIN `Contact` USING (`contactID`)'
					   . ' WHERE `Lid`.`contactID` = %i'
					   . ((hasAuth('bestuur')) ? ' OR `Lid`.`studentnr` = %i' : '%_')
					   . ((!hasAuth('lid')) ? ' AND `Lid`.`opzoekbaar` != \'J\'' : '')
					   . ((!hasAuth('bestuur')) ? ' AND `Lid`.`opzoekbaar` != \'N\''
												. ' AND (`Lid`.`lidTot` < NOW() OR `Lid`.`lidTot` < NOW() IS NULL  OR `Lid`.`opzoekbaar` = \'A\')'
												. ' AND `Lid`.`lidToestand` != \'BIJNALID\' AND `Lid`.`lidToestand` != \'BOEKENKOPER\' AND `Lid`.`lidToestand` != \'GESCHORST\'' : '')
					   . ' LIMIT 1)';
				$params = array_merge($params, array($arg, $arg));
			}
			else
			{
				$query[] = '(SELECT `contactID` FROM `Lid`'
					   . ' LEFT JOIN `Persoon` USING (`contactID`)'
					   . ' LEFT JOIN `Contact` USING (`contactID`)'
					   . ' WHERE CONCAT_WS(\' \', `Persoon`.`voornaam`, `Persoon`.`tussenvoegsels`, `Persoon`.`achternaam`) LIKE %c'
					   . ' OR CONCAT_WS(\' \', `Persoon`.`voornaam`, `Persoon`.`achternaam`) LIKE %c'
					   . ' OR `Persoon`.`bijnaam` LIKE %c'
					   . ' OR `Contact`.`email` LIKE %c'
					   . ((!hasAuth('lid')) ? ' AND `Lid`.`opzoekbaar` != \'J\'' : '')
					   . ((!hasAuth('bestuur')) ? ' AND `Lid`.`opzoekbaar` != \'N\''
												. ' AND (`Lid`.`lidTot` < NOW() OR `Lid`.`lidTot` < NOW() IS NULL  OR `Lid`.`opzoekbaar` = \'A\')'
												. ' AND `Lid`.`lidToestand` != \'BIJNALID\' AND `Lid`.`lidToestand` != \'BOEKENKOPER\' AND `Lid`.`lidToestand` != \'GESCHORST\'' : '')
					   . ' LIMIT 1)';
				$params = array_merge($params, array($arg, $arg, $arg, $arg));
			}
		}
		$last_query = 'COLUMN SELECT DISTINCT `contactID` FROM (' . implode(' UNION ', $query) . ') AS `t0`';

		array_unshift($params, $last_query);
		$ids = call_user_func_array(array($WSW4DB, 'q'), $params);
		return self::verzamel($ids);
	}

	static public function vanStudie (Studie $studie, $status = NULL)
	{
		global $WSW4DB;
		if (!in_array($status, LidStudie::enumsStatus()))
			$status = 'STUDEREND';

		$ids = $WSW4DB->q('COLUMN SELECT `LidStudie`.`lid_contactID`
			FROM `Studie` JOIN `LidStudie` ON `Studie`.`studieID` = `LidStudie`.`studie_studieID`
			WHERE `Studie`.`studieID`=%i AND `LidStudie`.`status`=%s',
			$studie->geefID(), $status);
		return self::verzamel($ids);
	}

    static public function geluksvogels($date)
    {
        global $WSW4DB;

        if(!$date)
            $date = date('Y-m-d');
        $ids = $WSW4DB->q("COLUMN SELECT `Persoon`.`contactID` FROM `Persoon`
            LEFT JOIN `Lid` ON `Lid`.`contactID` = `Persoon`.`contactID`
            WHERE TIMESTAMPDIFF(year,`Persoon`.`datumGeboorte`,%s) < 18
            AND `Lid`.`lidToestand` NOT IN ('BEESTJE','OUDLID')
			AND `Lid`.`lidVan` IS NOT NULL AND `Lid`.`lidVan` <= %s
			AND (`Lid`.`lidTot` IS NULL OR %s <= `Lid`.`lidTot`)", $date, $date, $date);

        return self::verzamel($ids);
    }
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
