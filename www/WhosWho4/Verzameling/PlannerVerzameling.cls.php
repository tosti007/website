<?

/**
 * $Id$
 */
class PlannerVerzameling
	extends PlannerVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PlannerVerzameling_Generated
	}

	/**
	 *  Haal alle planners op die nog data tot 3 maanden geleden hebben. Zo
	 *  mogelijk ook van een specifiek lid.
	 */
	static public function getOpenPlanners (Persoon $pers = null)
	{
		if ($pers && isPersoonBeperkt($pers->getContactID())) return self::verzamel(array());

		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `planner_plannerID` FROM `PlannerData` WHERE '
						. '`momentBegin` >= DATE_SUB(NOW(), INTERVAL 3 MONTH) '
						. ($pers? 'AND `PlannerDataID` IN ( SELECT `plannerData_plannerDataID`
							 FROM `PlannerDeelnemer` WHERE `persoon_contactID` = %i)':'')
						. ' ORDER BY `momentBegin` DESC',
						($pers?$pers->geefID():'')
						);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
