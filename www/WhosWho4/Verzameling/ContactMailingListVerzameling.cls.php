<?

/**
 * $Id$
 */
class ContactMailingListVerzameling
	extends ContactMailingListVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContactMailingListVerzameling_Generated
	}

	public static function vanContact(Contact $contact)
	{
		return ContactMailingListQuery::table()
			->whereProp('contact', $contact)
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
