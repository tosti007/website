<?

/**
 * $Id$
 */
class PlannerDeelnemerVerzameling
	extends PlannerDeelnemerVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PlannerDeelnemerVerzameling_Generated
	}

	/**
	 * Prepareer de cache
	 */
	static public function cache ($data, $leden)
	{
		$ids = array();
		foreach ($data as $datum)
			foreach ($leden as $lid)
				$ids[] = array($datum->geefID(), $lid->geefID());
		self::verzamel($ids);
	}

	static public function plannerData (PlannerData $datum)
	{
		global $WSW4DB;

		$int = $WSW4DB->q('COLUMN SELECT `persoon_contactID` FROM `PlannerDeelnemer` '
						  . 'WHERE `plannerData_plannerDataID` = %i',
						$datum->geefID()
						);

		$ids = array();
		foreach ($int as $i)
			$ids[] = array($datum->geefID(), $i);
		
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
