<?
class VoornaamwoordVerzameling
	extends VoornaamwoordVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VoornaamwoordVerzameling_Generated
	}

	/**
	 * @brief Geef alle soorten voornaamwoorden (in de verzameling).
	 *
	 * @param verz De voornaamwoorden waarvan we de soorten willen. Default: allemaal.
	 * @param lang De taal waarin we de soorten willen. Default: huidige taal.
	 * @return Een array met de unieke soorten in de verzameling.
	 */
	public static function alleSoorten(VoornaamwoordVerzameling $verz = NULL, $lang = NULL) {
		if (is_null($verz)) {
			$verz = VoornaamwoordQuery::table()->verzamel();
		}
		// wordt soort=>soort, want sneller dan array_unique()
		$soorten = array();

		foreach ($verz as $woord) {
			$soorten[$woord->getSoort($lang)] = $woord->getSoort($lang);
		}

		return array_keys($soorten);
	}
}
