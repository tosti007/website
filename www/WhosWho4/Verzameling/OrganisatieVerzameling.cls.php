<?

/**
 * $Id$
 */
class OrganisatieVerzameling
	extends OrganisatieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // OrganisatieVerzameling_Generated
	}

	static public function byType($type = NULL)
	{
		if($type == 'Alle'){
			$type = null;
		} elseif(!in_array($type, Organisatie::enumsSoort())) {
			return null;
		}
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `contactID`'
						. ' FROM `Organisatie`'
						. ($type ? ' WHERE `soort` = %s' : ' %_')
						. ' ORDER BY `naam`'
						, $type);

		return self::verzamel($ids);
	}

	static public function byNotType($type)
	{
		if(!in_array($type, Organisatie::enumsSoort()))
			return null;

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `contactID`'
						. ' FROM `Organisatie`'
						. ' WHERE `soort` != %s'
						. ' ORDER BY `naam`'
						, $type);

		return self::verzamel($ids);
	}

	static public function zoek ($arg, $limiet = 20)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Organisatie`.`contactID`
			FROM `Organisatie`
			WHERE `Organisatie`.`naam` LIKE %c
			ORDER BY `Organisatie`.`naam` ASC
			LIMIT %i',
			$arg, $limiet);

		return self::verzamel($ids);
	}

	static public function sorteerOpNaam($aID, $bID)
	{
		$a = Organisatie::geef($aID);
		$b = Organisatie::geef($bID);

		return strcasecmp(OrganisatieView::naam($a), OrganisatieView::naam($b));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
