<?

/***
 * $Id$
 */
class CommissieVerzameling
	extends CommissieVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/
	public function hasLid (Lid $lid = null)
	{
		foreach ($this as $cie)
			if ($cie->hasLid($lid))
				return true;
		return false;
	}

	static public function allemaal($type = NULL)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `commissieID`'
						. ' FROM `Commissie`'
						. ($type ? ' WHERE `soort` = %s' : ' %_')
						.' ORDER BY `naam`'
						, $type);

		return self::verzamel($ids);
	}
	
	public function aantalSoort($soort)
	{
		$counter = 0;
		foreach($this as $cie)
		{
			if($cie->getSoort() == $soort)
				$counter++;
		}
		return $counter;
	}

	/**
	 *	 Alle huidig actieve commissies (of groepen/disputen)
	 *
	 *	@param type CIE, GROEP, DISPUUT of ALLE
	 */
	static public function huidige($type = 'CIE')
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `commissieID`'
						.' FROM `Commissie`'
						.' WHERE'
						.($type == 'ALLE' ? ' %_' : ' `soort` = %s AND')
						.'     (to_days(now()) <= to_days(`datumEind`)'
						.'      OR `datumEind` IS NULL)'
						.' AND (to_days(`datumBegin`) <= to_days(now())'
						.'      OR `datumBegin` IS NULL)'
						.' ORDER BY `naam`'
						, $type
						);
		return self::verzamel($ids);
	}

	static public function activiteitVerzameling(ActiviteitVerzameling $act)
	{
		global $WSW4DB;
		$ids = implode(",", $act->keys());

		$commissies = $WSW4DB->q('COLUMN SELECT `commissie_commissieID`
						 FROM `CommissieActiviteit`
						 WHERE `activiteit_activiteitID` IN (%s)', $ids);
		return self::verzamel($commissies);
	}

	static public function metCategorie(CommissieCategorie $cat)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `commissieID`'
						.' FROM `Commissie`'
						.' WHERE `categorie_categorieID` = %i'
						.' ORDER BY `naam`'
						, $cat->getCategorieID()
						);
		return self::verzamel($ids);
	}

	/**
	 *	 Returneert alle commissies van een (actief) commissielid.
	 * @param pers De persoon van wie we de commissies willen weten.
	 * @param oud Zo ja, laat alleen de commissies zien waar de persoon al uit is.
	 * @param alleenIds Zo ja, geef alleen een array van commissie-id's in plaats van een verzameling.
	 * @param alleenCies Zo ja, geef geen disputen of groepen, alleen echte commissies.
	 * @param afgelopenCies Zo ja, geef ook commissies die al zijn afgelopen, zelfs al is $oud false.
	 */
	static public function vanPersoon (Persoon $pers, $oud = false, $alleenIds = false, $alleenCies = false, $afgelopenCies = false)
	{
		// TODO: gebruik gewoon CommissieLidVerzameling::vanPersoon()->toCommissieVerzameling()

		if (isPersoonBeperkt($pers->getContactID())) return self::verzamel(array());

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `commissie_commissieID`'
						.' FROM `CommissieLid`'
						.' LEFT JOIN `Commissie` ON `Commissie`.`commissieID` = `CommissieLid`.`commissie_commissieID`'
						.' WHERE `persoon_contactID` = %s'
						. (($alleenCies) ? ' AND (`Commissie`.`soort` = "CIE")' : '')
						. (($oud) ? ' AND (`CommissieLid`.`datumEind` < NOW()'
						          . ' OR `Commissie`.`datumEind` < NOW())'
						          : ' AND (`CommissieLid`.`datumBegin` <= NOW() OR `CommissieLid`.`datumBegin` IS NULL) '
						          . ' AND (`CommissieLid`.`datumEind` >= NOW() OR `CommissieLid`.`datumEind` IS NULL) '
						          . ' AND (`Commissie`.`datumBegin` <= NOW() OR `Commissie`.`datumBegin` IS NULL)'
						          . ((!$afgelopenCies) ? ' AND (`Commissie`.`datumEind` >= NOW() OR `Commissie`.`datumEind` IS NULL)'
						                               : ''))
						. ' ORDER BY `naam`'
						, $pers->geefID()
						);

		if($alleenIds)
			return $ids;
		return self::verzamel($ids);
	}

	static public function vanPersoonMetBugs(Persoon $pers)
	{
		$cies = $pers->getCommissies();
		$ciesMetBugs = BugCategorieVerzameling::geefActieveBugCommissies();

		$cies->intersection($ciesMetBugs);

		return $cies;
	}

	static public function vanActiviteit(Activiteit $act)
	{
		global $WSW4DB;
		$activiteitID = $act->getActiviteitID();

		$ids = $WSW4DB->q('COLUMN SELECT `commissie_commissieID` '
						. ' FROM `CommissieActiviteit`'
						. ' WHERE `activiteit_activiteitID` = %i'
						, $activiteitID
						);
		return self::verzamel($ids);
	}

	//Geef alle commissies terug die iets met spookweb van doen hebben gehad.
	public static function spookCommissies()
	{
		global $WSW4DB;
		return self::verzamel($WSW4DB->q('COLUMN SELECT DISTINCT `commissie_commissieID` FROM `Interactie` WHERE `commissie_commissieID` IS NOT NULL'));
	}

	public static function bugCommissies ($alleenActieve)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `BugCategorie`.`commissie_commissieID`
			FROM `BugCategorie`' .
			($alleenActieve ? 'WHERE `BugCategorie`.`actief` = 1' : ''));

		return CommissieVerzameling::verzamel($ids);
	}

	/**
	 * @brief geeft alle machtige commissies
	 * Dit geeft een verzameling van precies die commissies die verregaande
	 * permissies hebben om dingen op 'het systeem' te doen of op de site.
	 * Bijvoorbeeld: IBA-commissies en bestuur
	 *
	 * @return CommissieVerzameling
	 */
	public static function opSysteemInvloedHebbenden()
	{
		$verzameling = new CommissieVerzameling();

		$verzameling->voegtoe(Commissie::cieByLogin('sysop'));
		$verzameling->voegtoe(Commissie::cieByLogin('webcie'));
		$verzameling->voegtoe(Commissie::bestuur());

		// Er is niet altijd een kandidaatbestuur
		$kb = Commissie::kandidaatBestuur();
		if (!is_null($kb))
			$verzameling->voegtoe($kb);

		return $verzameling;
	}

	public static function zoek ($arg, $limiet = 20)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Commissie`.`commissieID`
			FROM `Commissie`
			WHERE `Commissie`.`naam` LIKE %c
			LIMIT %i',
			$arg, $limiet);

		return self::verzamel($ids);
	}

	/**
	 *	 Retourneert een CommissieLidVerzameling met alle commissieleden
	 * van commissies in deze CommissieVerzameling
	 */
	public function leden($status = 'huidige')
	{
		$cieleden = new CommissieLidVerzameling();

		foreach($this as $cie) {
			$cieleden->union($cie->cieleden($status));
		}

		return $cieleden;
	}

	/**
	 *	 Retourneert een CommissieVerzameling met alle commissies
	 *  waar je aan mag zitten (voor gewone leden alleen de cies waar ze
	 *  inzitten, voor level >= bestuur alle cies).
	 */
	public static function wijzigbare()
	{
		if(hasAuth('bestuur'))
			return self::huidige('ALLE');

		return self::vanPersoon(Persoon::getIngelogd());
	}

	/***
	 *	Default de cie verzameling sorteren op cie naam.
	 */
	static public function verzamel($ids)
	{
		$verz = parent::verzamel($ids);
		$verz->sorteer('naam');
		return $verz;
	}

	/***
	 *  Sorting
	 **/
	static public function sorteerOpNaam($aID, $bID)
	{
		$a = Commissie::geef($aID);
		$b = Commissie::geef($bID);

		return strcmp($a->getNaam(), $b->getNaam());
	}

	/**
	 *  Sorteer cies op volgorde van recentheid
	 *  Dat wil zeggen, een cie is recenter dan een andere als het minder lang geleden is beeindigd
	 *  Cies zonder einddatum of eentje in de toekomst tellen als allerrecentst
	 */
	static public function sorteerOpRecentst($aID, $bID)
	{
		$a = Commissie::geef($aID);
		$b = Commissie::geef($bID);

		$eindA = $a->getDatumEind();
		$eindB = $b->getDatumEind();

		// per default geeft een NULL een DateTime die precies vandaag is
		// alles wat vandaag afloopt tellen we als in de toekomst
		$preciesNu = new DateTimeLocale();
		$vandaag = new DateTimeLocale($preciesNu->strftime("%Y-%m-%d"));
		if ($eindA > $vandaag) {
			$eindA = $vandaag;
		}
		if ($eindB > $vandaag) {
			$eindB = $vandaag;
		}

		if ($eindA > $eindB) {
			return -1;
		} elseif ($eindA < $eindB) {
			return 1;
		} else {
			return self::sorteerOpNaam($aID, $bID);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
