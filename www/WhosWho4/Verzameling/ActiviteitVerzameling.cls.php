<?

/***
 * $Id$
 */
class ActiviteitVerzameling
	extends ActiviteitVerzameling_Generated
{
	/** CONSTRUCTOR **/
	public function __construct()
	{
		parent::__construct();
	}

	/** METHODEN **/

	/**
	 *  Geeft alle activiteiten van een bepaald collegejaar
	 *
	 * @param jaar Welk jaar er teruggeven moet worden als string
	 * @param toegang Welke toegang de activiteiten moeten hebben
	 *
	 * @return De ActiviteitVerzameling
	 */
	static public function vanCollegejaar($jaar, $toegang = 'PUBLIEK')
	{
		return ActiviteitVerzameling::tijdsSpan($jaar . '-08-01', $jaar+1 . '-07-31', $toegang);
	}

	/**
	 *  Retourneert activiteiten die beginnen binnen een bepaald tijdspanne.
	 *
	 * @param start De startdatum
	 * @param eind De einddatum
	 * @param toegang 'PUBLIEK' of 'PRIVE' of null (voor zowel publiek als prive)
	 *
	 * @return De ActiviteitVerzameling
	**/
	static public function tijdsSpan($start, $eind, $toegang = 'PUBLIEK')
	{
		$query = ActiviteitQuery::table()
			->whereProp('momentEind', '>=', new DateTimeLocale($start))
			->whereProp('momentBegin', '<', new DateTimeLocale($eind))
			->orderByAsc('momentBegin', 'momentEind', 'activiteitID');

		if($toegang !== null)
			$query->whereProp('toegang', $toegang);

		return $query->verzamel();
	}

	/**
	 *  Geeft alle activiteiten terug waar een persoon aan heeft deelgenomen
	 *
	 * @param pers De persoon waarvan je de activiteiten wilt
	 * @param publiek Een bool of je alleen publieke activiteiten wilt
	 * @param datum Een string die aangeeft of je activiteiten in het verleden of toekomst wil hebben. Indien dit null is, krijg je alles.
	 *
	 * @return De ActiviteitVerzameling
	 */
	static public function vanDeelgenomenPersoon (Persoon $pers, $publiek = true, $datum = null)
	{
		$query = DeelnemerQuery::table()
			->join('Activiteit')
			->whereProp('Persoon', $pers)
			->orderByDesc('momentBegin');

		if($publiek)
		{
			$query->whereProp('toegang', 'PUBLIEK');
		}

		if($datum == 'Toekomst')
		{
			$query->whereProp('momentBegin', '>', new DateTimeLocale());
		}

		elseif($datum == 'Verleden')
		{
			$query->whereProp('momentBegin', '<', new DateTimeLocale());
		}

		elseif($datum == null)
		{
			/* Hier voegen we niks extra toe */
		}

		else
		{
			throw new LogicException("De optie " . $datum . " bestaat hier helemaal niet!");
		}

		return $query->verzamel('Activiteit');
	}

	/**
	 *  Retourneert activiteiten op basis van opgegeven criteria, primaire functie agenda
	 *
	 * @param van Vanaf wanneer
	 * @param tot Tot wanneer
	 * @param toegang Welke toegang je zoekt
	 * @param onderwijs Of het onderwijsactiviteiten zijn
	 * @param cie Bij welke cie je activiteiten wilt
	 * @param computers Of ze computers moeten hebben gereserveerd
	 * @param categorie Bij welke categorie ze horen, of NULL voor allen.
	 *
	 * @return ActiviteitVerzameling De activiteitVerzameling die je zoekt
	**/
	static public function getActiviteiten ($van = 0, $tot = 0, $toegang = 'PUBLIEK', $onderwijs = '', $cie = '', $computers = 0, $categorie = NULL)
	{
		global $WSW4DB;

		$params = array();

		// We doen hier twee keer een left join op activiteitInformatie zodat we 
		// ook de info van de herhalingen erbij hebben
		$query = 'COLUMN SELECT `act`.`activiteitID` '
			. 'FROM `Activiteit` AS `act`'
			. 'LEFT JOIN `ActiviteitInformatie` AS `info1` ON `info1`.`activiteitID` = `act`.`activiteitID` '
			. 'LEFT JOIN `ActiviteitHerhaling` AS `her` ON `her`.`activiteitID` = `act`.`activiteitID` '
			. 'LEFT JOIN `ActiviteitInformatie` AS `info2` ON `info2`.`activiteitID` = `her`.`parent_activiteitID` ';

		if ($cie instanceof Commissie)
			$query .= ' LEFT JOIN `CommissieActiviteit` AS `oCommissieActiviteit` ON '
					. '`Activiteit`.`activiteitID`=`oCommissieActiviteit`.`activiteit_activiteitID`';

		$query .= ' WHERE ((';

		if (empty($van))
		{
			$query .=		' `act`.`momentBegin` >= %s';
			$params[] = date('Y-m-1');
		}
		else
		{
			$query .=		' `act`.`momentBegin` >= %s';
			$params[] = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		}

		if (empty($tot))
		{
			$query .=		' AND `act`.`momentBegin` <= %s';
			$params[] = (date('Y') + 1) . '-' . COLJAARSWITCHMAAND . '-1';
		}
		else
		{
			$query .=		' AND `act`.`momentBegin` <= %s';
			$params[] = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);
		}

		$query .= ') OR (';
		if (empty($van))
		{
			$query .=		' `act`.`momentEind` >= %s';
			$params[] = date('Y-m-1');
		}
		else
		{
			$query .=		' `act`.`momentEind` >= %s';
			$params[] = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		}

		if (empty($tot))
		{
			$query .=		' AND `act`.`momentEind` <= %s';
			$params[] = (date('Y') + 1) . '-' . COLJAARSWITCHMAAND . '-1';
		}
		else
		{
			$query .=		' AND `act`.`momentEind` <= %s';
			$params[] = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);
		}
		$query .= '))';

		$toegang = strtoupper($toegang);
		if ($toegang == 'PRIVE')
		{
			$query .=		' AND `act`.`toegang` = \'PRIVE\'';
		}
		elseif ($toegang != 'ALLE')
		{
			$query .=		' AND `act`.`toegang` = \'PUBLIEK\'';
		}

		switch ($onderwijs)
		{
			case 'J':
				$query .=	' AND ((`info1`.`onderwijs` LIKE \'J\' OR `info1`.`onderwijs` LIKE \'B\')'
						  . ' OR (`info2`.`onderwijs` LIKE \'J\' OR `info2`.`onderwijs` LIKE \'B\'))'; break;
			case 'N':
				$query .=	' AND (`info1`.`onderwijs` LIKE \'N\' OR `info2`.`onderwijs` LIKE \'N\')'; break;
		}
		if ($computers > 0)
		{
			$query .=		' AND (`info1`.`aantalComputers` >= %i OR `info2`.`aantalComputers` >= %i)';
			$params[] = $computers;
			$params[] = $computers;
		}

		if ($cie instanceof Commissie)
		{
			$query .= ' AND `oCommissieActiviteit`.`commissie_commissieID` = %s';
			$params[] = $cie->geefID();
		}

		if ($categorie instanceof ActiviteitCategorie) {
			$query .= ' AND (`info1`.`categorie_actCategorieID` = %i OR `info2`.`categorie_actCategorieID` = %i)';
			$params[] = $categorie->geefId();
			$params[] = $categorie->geefId();
		}

		$query .= ' ORDER BY `act`.`momentBegin`,`act`.`momentEind`,`act`.`activiteitID`';

		array_unshift($params, $query);

		$ids = call_user_func_array(array($WSW4DB, 'q'), $params);

		return self::verzamel($ids);
	}

	/**
	 *  Geeft alle activiteiten met een bepaalde begindatum
	 *
	 * @param datum De begindatum
	 * @param computers Of je alleen acts met reserveringen zoekt
	 * @param zichtbaar Welke zichtbaarheid de acts moeten hebben
	 *
	 * @return De ActiviteitVerzameling
	 */
	public static function getActiviteitenMetBegindatum($datum, $computers = 0, $zichtbaar = 'PUBLIEK')
	{
		$eind = clone $datum;
		$datum->setTime(0,0,0);
		$eind->setTime(23,59,59);
		return ActiviteitVerzameling::getActiviteiten($datum, $eind, $zichtbaar, '', '', $computers);
	}

	/**
	 *  Geeft alle commissies bij een activiteitVerzameling
	 *
	 * @return De CommissieVerzameling
	 */
	public function commissies()
	{
		return CommissieVerzameling::activiteitVerzameling($this);
	}

	/**
	 *  Functie om een verzameling activiteiten van een cie op te vragen.
	 *
	 * @param commissie De commissie
	 * @param status Kan 'komend', 'oud' of 'huidig' zijn
	 * @param toegang Welke toegang je zoekt
	 *
	 * @return De ActiviteitVerzameling
	 */
	static public function vanCommissie($commissie, $status = 'komend', $toegang = 'PUBLIEK')
	{
		$query = ActiviteitQuery::table()
			->join('CommissieActiviteit')
			->whereProp('Commissie', $commissie)
			->orderByAsc('momentBegin');

		switch ($status)
		{
			case 'oud':
				$query->whereProp('momentBegin', '<', 'SQL_NOW');
				break;
			case 'komend':
				$query->whereProp('momentEind', '>', 'SQL_NOW');
				break;
			case 'huidig':
			default:
			// In de db is soms het momentBegin of momentEind NULL
			// dus daarom kan hier geen wherePropNull
				$query->where(function($q) {
					$q->whereProp('momentBegin', '<=', 'SQL_NOW')
					  ->orWhereNull('momentBegin');
				})
				->where(function($q) {
					$q->whereProp('momentEind', '>', 'SQL_NOW')
					  ->orWhereNull('momentEind');
				});
				break;
		}

		if($toegang != 'alles')
			$query->whereProp('toegang', $toegang);

		return $query->verzamel();
	}

	/**
	 *  Functie om een verzameling activiteiten op te vragen die door deze commissies samen georganiseerd worden
	 *
	 * @param cieVerzameling Een CommissieVerzameling waarvan je de acts zoekt
	 *
	 * @return De ActiviteitVerzameling
	 */
	static public function vanCommissies($cieVerzameling)
	{
		global $WSW4DB;

		$cieIds = $cieVerzameling->keys();
		$count = $cieVerzameling->aantal();

		$ids = $WSW4DB->q('COLUMN SELECT `Activiteit`.`activiteitID`'
						. 'FROM `Activiteit` JOIN `CommissieActiviteit` ON `Activiteit`.`activiteitID` = `CommissieActiviteit`.`activiteit_activiteitID`'
						. 'WHERE `commissie_commissieID` IN (%Ai)'
						. 'GROUP BY `Activiteit`.`activiteitID`'
						. 'HAVING count( * ) = %i', $cieIds, $count);
		return self::verzamel($ids);
	}

	/**
	 *  Returneert een subverzameling van activiteiten op een dag
	 *
	 * @param date De dag
	 *
	 * @return De ActiviteitVerzameling
	 */
	public function subActiviteitenOpDag ($date)
	{
		$acts = new ActiviteitVerzameling();
		foreach ($this as $act)
		{
			if (($act->getMomentBegin()->format('Ymd') == $date->format('Ymd')) ||
				($act->getMomentBegin()->format('Ymd') < $date->format('Ymd') &&
				$act->getMomentEind()->format('Ymd') > $date->format('Ymd')))
			{
				$acts->voegtoe($act);
			}
		}
		return $acts;
	}

	/*** Sorteren ***/
	/**
	 *	 Maak het mogelijk om een ActiviteitVerzameling te sorteren op
	 *	Activiteit::getTitel().
	 *
	 *	@see Verzameling::sorteer()
	 *	@see Activiteit::getTitel()
	 */
	static public function sorteerOpTitel($aID, $bID)
	{
		$a = Activiteit::geef($aID);
		$b = Activiteit::geef($bID);

		return strcmp($a->getTitel(), $b->getTitel());
	}

	static public function sorteerOpBeginMoment($aID, $bID)
	{
		$a = Activiteit::geef($aID)->getMomentBegin();
		$b = Activiteit::geef($bID)->getMomentBegin();

		if($a > $b) {
			return 1;
		}
		if($a < $b) {
			return -1;
		}
		return 0;
	}

	/**
	 *  Geeft de laatste x acts die fotos hebben
	 *
	 * @param aantal Het aantal acts wat je terug wilt
	 *
	 * @return De ActiviteitVerzameling
	 */
	public static function geefLaatsteMetFoto($aantal = 8)
	{
		return MediaQuery::table()
			->join('Activiteit')
			->whereNotNull('Media.activiteit_activiteitID')
			->orderByDesc('momentBegin')
			->limit($aantal)
			->verzamel('Activiteit', true);
	}

	/**
	 *  Geeft Alle activiteiten van een bepaald jaar die fotos hebben
	 *
	 * @param jaar Van welk jaar de acts wilt hebben als string
	 *
	 * @return De ActiviteitVerzameling
	 */
	public static function geefVanJaarMetFoto($jaar)
	{
		$beginjaar = "$jaar-08-01 00:00:00";
		$eindjaar = ($jaar+1)."-08-01 00:00:00";

		return MediaQuery::table()
			->join('Activiteit')
			->whereNotNull('Media.activiteit_activiteitID')
			->whereProp('momentBegin', '>', new DateTimeLocale($beginjaar))
			->whereProp('momentBegin', '<', new DateTimeLocale($eindjaar))
			->orderByDesc('momentBegin')
			->verzamel('Activiteit', true);
	}

	/**
	 *  Geeft alle collegejaren terug die activiteiten met fotos hebben
	 *
	 * @return De jaren als array van strings
	 */
	public static function geefAlleCollegeJarenMetFotos()
	{
		return ActiviteitQuery::table()
			->setFetchType('COLUMN')
			->selectDistinct('YEAR(momentBegin)')
			->whereProp('momentBegin', '<', 'SQL_NOW')
			->orderByAsc('momentBegin')
			->get();
	}

	/**
	 *  Zoekt in de activiteiten
	 */
	public static function zoek ($arg, $limiet = 20)
	{
		global $WSW4DB;

		$cieIds = array();
		$query = 'COLUMN SELECT DISTINCT `Activiteit`.`activiteitID`
			FROM `Activiteit`
			JOIN `CommissieActiviteit` ON `Activiteit`.`activiteitID` = `CommissieActiviteit`.`activiteit_activiteitID`
			JOIN `Commissie` ON `CommissieActiviteit`.`commissie_commissieID` = `Commissie`.`commissieID`
			WHERE (`Activiteit`.`toegang` = \'PUBLIEK\'';

		if (hasAuth('bestuur'))
		{
			$query .= ' OR `Activiteit`.`toegang` = \'PRIVE\'';
		}
		else if (hasAuth('ingelogd'))
		{
			$cieIds = Persoon::getIngelogd()->getCommissies()->keys();
			if ($cieIds)
			{
				$query .= ' OR `CommissieActiviteit`.`commissie_commissieID` IN (%Ai)';
			}
		}

		$query .= ')
			  AND (`Activiteit`.`titel_NL` LIKE %c
				OR `Activiteit`.`titel_EN` LIKE %c
				OR `Activiteit`.`werftekst_NL` LIKE %c
				OR `Activiteit`.`werftekst_EN` LIKE %c
				OR `Commissie`.`naam` LIKE %c)
			ORDER BY `Activiteit`.`momentBegin` DESC
			LIMIT %i';

		$ids = $cieIds
			? $WSW4DB->q($query, $cieIds, $arg, $arg, $arg, $arg, $arg, $limiet)
			: $WSW4DB->q($query,          $arg, $arg, $arg, $arg, $arg, $limiet);

		return self::verzamel($ids);
	}

	static public function geefEerstAankomendePubliekeActs($aantal = 16)
	{
		global $WSW4DB;

		$date = new DateTimeLocale();

		$ids = $WSW4DB->q('COLUMN SELECT `act`.`activiteitID` '
			. 'FROM `Activiteit` AS `act` '
			. 'WHERE `act`.`momentBegin` >= %s '
			. 'AND `act`.`toegang` = \'PUBLIEK\' '
			. 'ORDER BY `act`.`momentBegin` '
			. 'LIMIT %i'
			, $date->format('Y-m-d')
			, $aantal);

		return self::verzamel($ids);
	}

	/**
	 * Retourneert het aantal unieke deelnemers aan een activiteit in deze verzameling.
	 *
	 * @return Het totaal aantal unieke deelnemers.
	 **/
	public function uniekeDeelnemerAantal()
	{
		$namen = array();
		foreach ($this as $act) {
			$deelnemers = $act->getDeelnemerVerzameling();
			foreach ($deelnemers as $deelnemer) {
			    $ID = $deelnemer->getPersoonContactID();
			    $namen[$ID] = true;
			}
		}
		return count($namen);
	}
}

// vim:sw=4:ts=4:tw=0:foldlevel=1
