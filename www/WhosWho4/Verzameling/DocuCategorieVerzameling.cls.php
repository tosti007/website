<?
class DocuCategorieVerzameling
	extends DocuCategorieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // DocuCategorieVerzameling_Generated
	}

	/*
	 *  Geeft alle categorieën terug
	 *
	 * @return Alle docucategorieën
	 */
	static public function getCategorieen()
	{
		global $WSW4DB;

		$res = $WSW4DB->q("COLUMN SELECT * FROM `DocuCategorie`");

		return self::verzamel($res);
	}
}
