<?

/**
 * $Id$
 */
class LidStudieVerzameling
	extends LidStudieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // LidStudieVerzameling_Generated
	}

	// Geef de studieverzameling van een lid.
	static public function vanLid (Lid $lid, $huidige = false)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `lidStudieID`'
		                .' FROM `LidStudie`'
		                .' WHERE `lid_contactID` = %s'
				.($huidige?' AND `status` = "STUDEREND"':'')
		                .' ORDER BY `LidStudie`.`datumBegin` ASC, `LidStudie`.`datumEind` ASC, `LidStudie`.`studie_studieID` ASC'
		                , $lid->geefID()
		                );
		return self::verzamel($ids);
	}

	public function huidige()
	{
		$result = clone $this;
		foreach($this as $studie){
			if($studie->getStatus() != 'STUDEREND') $result->verwijder($studie->getLidStudieID());
		}
		return $result;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
