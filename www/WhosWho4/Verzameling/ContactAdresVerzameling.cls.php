<?

/**
 * $Id$
 */
class ContactAdresVerzameling
	extends ContactAdresVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContactAdresVerzameling_Generated
	}

	static public function vanContact ($con)
	{
		$query = ContactAdresQuery::table()
			->orderByAsc('soort');

		if ($con instanceof Contact)
			$query->whereProp('contact', $con);
		elseif ($con instanceof ContactVerzameling)
			$query->whereInProp('contact', $con);
		else
			return new ContactAdresVerzameling();

		return $query->verzamel();
	}

	static public function sorteerOpSoort($a, $b) {
		$sortedArray = array('THUIS', 'POST', 'WERK', 'BEZOEK', 'FACTUUR', 'OUDERS');

		$keyA = array_search($a->getSoort(), $sortedArray);
		$keyB = array_search($b->getSoort(), $sortedArray);

		return $keyA > $keyB;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
