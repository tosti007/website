<?

/**
 * $Id$
 */
class ContactVerzameling
	extends ContactVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContactVerzameling_Generated
	}

	static public function vakidioten()
	{
		global $WSW4DB;
		$ids1 = $WSW4DB->q('COLUMN SELECT `contactID`'
						.' FROM `Lid`'
						.' JOIN `Persoon`'
						.' USING (`contactID`)'
						.' WHERE '.WSW4_WHERE_LID
						.' AND overleden = 0'
						.' AND `contactID` IN ('
							.' SELECT `contactID`'
							.' FROM `Contact`'
							.' WHERE `vakidOpsturen` = 1'
							.')'
						);

		$ids2 = $WSW4DB->q('COLUMN SELECT O.contactID'
						.' FROM Organisatie O, Contact C'
						.' WHERE O.contactID = C.contactID'
						.' AND C.vakidOpsturen = 1'
						);

		return self::verzamel(array_merge($ids1,$ids2));
	}

	//Verzamel alle a-es2roots contacten, standaard excl de donateurs en normale leden.
	static public function aes2roots($donateurs = false, $huidigeLeden = false)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `contactID`'
						.' FROM `Contact`'
						.' WHERE `aes2rootsOpsturen` = 1'
						. ($donateurs?'':(' AND ( `contactID` NOT IN ('
							. 'SELECT `persoon_contactID` FROM `Donateur`)'
							. 'OR `contactID` NOT IN ('
							. 'SELECT `anderpersoon_contactID` FROM `Donateur`))'
						))
						. ($huidigeLeden?'':(' AND ( `contactID` NOT IN ('
						. 'SELECT `contactID` FROM `Lid` '
						. 'WHERE ' . WSW4_WHERE_NORMAALLID . '))'
						))
						);

		return self::verzamel($ids);
	}
	
	/**
		 Maakt een ContactVerzameling met daarin alle Contacten die
		geabonneerd zijn op de gegeven MailingList
	**/
	public static function mailingListAbonnees(MailingList $mailinglist)
	{
		$ids = $mailinglist->abonneeLidnrs();
		return self::verzamel($ids);
	}

	static public function sorteerOpcontactID($aID, $bID)
	{
		if($aID < $bID)
			return -1;
		if($aID > $bID)
			return 1;
		return 0;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
