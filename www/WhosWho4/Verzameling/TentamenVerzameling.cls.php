<?
/**
 * $Id$
 */
class TentamenVerzameling
	extends TentamenVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TentamenVerzameling_Generated
	}

	/*
	 *  Functie om te kunnen sorteren op naam
	 */
	public function sorteerOpNaam($a,$b) {
		return $a->getNaam()<$b->getNaam();
	}

	/*
	 *  Geeft per studie alle vakken met het aantal tentamens bij het vak
	 *
	 * @param studie Welke studie er de vakken/aantallen moeten worden gegeven
	 * 			of laat dit null zijn voor alle studies
	 * @param outdated Of oude vakken ook meegenomen moeten worden.
	 * @return Een table met per rij het vak id en het aantal tentamens
	 */
	static public function vanStudieAantal($studie = null, $outdated = true, $zoektekst = null, $faseBA = true, $faseMA = true)
	{
		$query = VakQuery::table()
			->join('Tentamen', 'Vak.vakID', 'Tentamen.vak_vakID')
			->select('Vak.vakID', array('aantal' => 'COUNT(Tentamen.id)'))
			->whereProp('inTentamenLijsten', true)
			->groupBy('Vak.vakID')
			->orderByAsc('Vak.naam');

		if (!empty($studie) || !$faseBA || !$faseMA) {
			$query->join('VakStudie', 'Vak.vakID', 'VakStudie.vak_vakID')
				->join('Studie', 'VakStudie.studie_studieID', 'Studie.studieID');
		}
		if (!empty($studie)) {
			$query->whereProp('soort', $studie);
		}
		if (!$faseBA || !$faseMA) {
			if (!$faseBA && !$faseMA) return new TentamenVerzameling();
			if (!$faseBA && $faseMA) $fase = "MA";
			if ($faseBA && !$faseMA) $fase = "BA";
			$query->whereProp('fase', $fase);
		}

		if (!empty($zoektekst)) {
			// escape die handel:
			$zoektekst = "%" . str_replace("%", "%%", $zoektekst) . "%";
			$query->where(function ($q) use ($zoektekst) {
				$q->whereLikeProp('Vak.naam', $zoektekst);
				$q->orWhereLikeProp('Vak.code', $zoektekst);
			});
		}

		if(!$outdated)
			$query->whereProp('tentamensOutdated', false);

		return $query->get();
	}

	/*
	 *  Geeft alle tentamens bij een vak terug
	 *
	 * @param vak Het vak waarbij de tentamens gegeven moeten worden
	 * @return TentamenVerzameling met alle tentamens van een vak
	 */
	static public function getAlleVanVak($vak)
	{
		return TentamenQuery::table()
			->whereProp('Vak', $vak)
			->orderByDesc('datum')
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
