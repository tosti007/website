<?

/**
 * $Id$
 */
class CommissieCategorieVerzameling
	extends CommissieCategorieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // CommissieCategorieVerzameling_Generated
	}

	static public function geefAlle()
	{
		return CommissieCategorieQuery::table()->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
