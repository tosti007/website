<?
class PapierMolenVerzameling
	extends PapierMolenVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // PapierMolenVerzameling_Generated
	}

	static public function geefNietVerlopen($studie)
	{
		global $WSW4DB;

		$date = new DateTimeLocale();
		$ids = $WSW4DB->q("COLUMN SELECT `papierMolenID` FROM `PapierMolen` "
			. "WHERE `verloopdatum` >= %s AND `geplaatst` <= %s "
			. (!empty($studie) ? "AND `studie` = %s" : "%_")
			, $date->format('Y-m-d')
			, $date->format('Y-m-d H:i:s')
			, $studie);

		return self::verzamel($ids);
	}
}
