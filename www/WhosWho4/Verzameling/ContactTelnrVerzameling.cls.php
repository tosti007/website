<?

/**
 * $Id$
 */
class ContactTelnrVerzameling
	extends ContactTelnrVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContactTelnrVerzameling_Generated
	}

	static public function vanContact (Contact $con, $type = null)
	{
		$query = ContactTelnrQuery::table()
			->whereProp('contact', $con)
			->orderByDesc('voorkeur');

		if($type)
			$query->whereProp('soort', $type);

		return $query->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
