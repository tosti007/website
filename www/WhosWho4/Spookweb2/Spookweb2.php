<?php

use Symfony\Component\HttpFoundation\Response;

/**
 *
 *	Wat zit hier in
 *
***/

abstract class SpookWeb2Controller
{
	public static function home()
	{
		$mijnBedrijven = BedrijfVerzameling::verzamel(explode(',',ContactPersoonVerzameling::metConstraints(null, null, 'huidig', Persoon::getIngelogd()->geefID(), "SPOOK")->implode('getOrganisatieContactID', ',')));
		$mijnBedrijven->sorteer('Naam');

		$page = Page::getInstance();

		//Als er maar een bedrijf is voegen we meteen pagelinks toe aan de pagina voordat start is aangeroepen.
		// Hierdoor niet meest fijne oplossing doordat er twee ifs nodig zijn.
		if(trypar('gogogo')) {
			$bedrijven = BedrijfQuery::metConstraints(trypar('Naam'), trypar('Type'), trypar('Status'))->verzamel();
			if($bedrijven->aantal() == 1) {
				$page = BedrijfView::pageLinks($bedrijven->first());
			}
		}

		$page->start("Spookweb 2");

		$page->add(_("In spookweb kan je alle interacties (voorheen contacten) tussen A–Eskwadraat en bedrijven, bedrijfsinformatie en nog veel meer gerelateerde dingen terugvinden."));
//			->add(new HtmlHeader(4, _("Zoek hieronder het bedrijf dat je wilt bekijken")))
//			->add(BedrijfVerzamelingView::zoekform())
//			->add(new HtmlHeader(4, _("Of de interacties van een commissie")))
//			->add(InteractieVerzamelingView::zoekform());
		$mijnBedrijvenDiv = new HtmlDiv(array(), "block");
		//Maak er een uitklapbare div van als het er veel zijn.
		if($mijnBedrijven->aantal() > 6)
			$mijnBedrijvenDiv = new HtmlResizableDiv( "100%", "180px", "block");
		$mijnBedrijvenDiv->add(new HtmlHeader(4,_("Eigen bedrijven")));
		if($mijnBedrijven->aantal() > 0) {
			$mijnBedrijvenDiv->add(BedrijfVerzamelingView::mijnList($mijnBedrijven));
		} elseif(!hasAuth("SPOCIE")) {
			$mijnBedrijvenDiv->add(_("Alleen bruikbaar voor de spocie."));
		} else {
			$mijnBedrijvenDiv->add(_("Je hebt op dit moment geen bedrijven."));
		}

		$div = new HtmlDiv(array(), "blockRow");
		$div->add(new HtmlHR());
		$div->add(new HtmlDiv(array(new HtmlHeader(4, _("Zoek een bedrijf")),
			BedrijfVerzamelingView::zoekform()), "block"));
		$div->add(new HtmlDiv(array(), "blockSeparator"));
		$div->add(new HtmlDiv(array(new HtmlHeader(4, _("Zoek een interactie")),
			InteractieVerzamelingView::zoekform()), "block"));
		$div->add(new HtmlDiv(array(), "blockSeparator"));
		$div->add($mijnBedrijvenDiv);
		$div->add(new HtmlHR());

		$page->add($div);

		if(trypar('gogogo')) {
			if($bedrijven->aantal() == 1) {
				$page->add(BedrijfView::details($bedrijven->first()));
			} else {
				$page->add(new HtmlHeader(3, _("Gevonden bedrijven:")))
					->add(BedrijfVerzamelingView::htmlList($bedrijven));
			}
		}

		if(trypar('interactiezoek'))
		{
			$interacties = InteractieVerzameling::metConstraints(250, trypar('Medium'), null, null, null, trypar('Commissie'));
			$page->add(new HtmlHeader(3, _("Gevonden interacties (max 250):")))
				->add(InteractieVerzamelingView::htmlList($interacties, true));
		}

		return new PageResponse($page);
	}
	public static function overzicht()
	{
		//Voor nu alleen bestuur+
		if(!hasAuth('bestuur'))
			spaceHttp('403');

		$aantalBedrijven = BedrijfQuery::metConstraints()->count();
		$aantalBedrijfscontactpersonen = ContactPersoonQuery::metConstraints(null, null, 'alle', null, "BEDRIJFSCONTACT")->count();
		$aantalSpoken = ContactPersoonQuery::metConstraints(null, null, 'alle', null, "SPOOK")->count();
		$aantalInteracties = InteractieQuery::metConstraints('alle')->count();

		$aantalContracten = ContractVerzameling::metConstraints()->aantal();
		$aantalVacatures = VacatureVerzameling::metConstraints()->aantal();
		$aantalBedrijfsprofielen = BedrijfsprofielVerzameling::metConstraints()->aantal();

		$page = Page::getInstance();
		$page->start(_("Spookweb 2-overzichtpagina"));

		$page->add(new HtmlParagraph("Er zitten $aantalBedrijven bedrijven in de spookweb 2 db, bekijk het " . new HtmlAnchor("Bedrijf", "overzicht")))
			->add(new HtmlParagraph("Er zitten $aantalBedrijfscontactpersonen bedrijfscontacten in de spookweb 2 db: " . new HtmlAnchor("Bedrijfscontact", "overzicht")))
			->add(new HtmlParagraph("Er zitten $aantalSpoken spoken in de spookweb 2 db: " . new HtmlAnchor("Spook", "overzicht")))
			->add(new HtmlParagraph("Er zitten $aantalInteracties interacties in de spookweb 2 db: " . new HtmlAnchor("Interactie", "overzicht")))
			->add(new HtmlParagraph("Er zitten $aantalContracten contracten in de spookweb 2 db: " . new HtmlAnchor("Contract", "overzicht")))
			->add(new HtmlParagraph("Er zitten $aantalVacatures vacatures in de spookweb 2 db: " . new HtmlAnchor("Vacature", "overzicht")))
			->add(new HtmlParagraph("Er zitten $aantalBedrijfsprofielen bedrijfsprofielen in de spookweb 2 db: " . new HtmlAnchor("Bedrijfsprofiel", "overzicht")));
		return new PageResponse($page);
	}

	/**
	 *	Pagina's voor overzichten en de comext
	 **/

	public static function overzichtHome()
	{
		if(!hasAuth('spocie'))
			spaceHttp('403');

		$page = Page::getInstance();
		$page->start(_("Overzichtenpagina"));

		$page->add(new HtmlParagraph("Op deze pagina kan je vele overzichten vinden voor de administratie."));

		$optielijst = new HtmlList();
		$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("Deadlines","Deadlines"))));
		$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("Spokenoverzicht","Bedrijven en interacties per spook"))));
		$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("ToDos", "TODO's"))));
		$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("Contracten", "Contracten"))));
		$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("Bijnanetverlopenvacatures", "Bijna en net verlopen vacatures"))));
		$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("sponsorcommissarissen", "Alle huidige sponsorcommissarissen"))));
		$optielijst->addChild(new HtmlListItem(array("Bedrijven per studie")));
		$optielijst->addChild(new HtmlListItem(array("Alumni bij bedrijven overzicht")));
		//$optielijst->addChild(new HtmlListItem(array(new HtmlAnchor("Financieel", "Financieel"))));
		$page->add($optielijst);
		return new PageResponse($page);
	}

	public static function overzichtDeadlines()
	{
		$page = Page::getInstance();
		$page->start(_("Deadlines"));

		$page->add(new HtmlParagraph("Het overzicht voor alle deadlines van de komende of geselecteerde periode."));
		$page->add(ContractonderdeelVerzamelingView::deadlinePeriodeForm());
		$page->add(new HtmlHR());

		$now = new DateTimeLocale();
		$end = new DateTimeLocale();
		switch(trypar("Periode")) {
			case "3m":
				$end->add(new DateInterval("P3M"));
				break;
			case "1j":
				$end->add(new DateInterval("P1Y"));
				break;
			case "30d":
			default:
				$end->add(new DateInterval("P30D"));
		}
		$deadlines = ContractonderdeelVerzameling::metConstraints(null,null, $now->format("Y-m-d"), $end->format("Y-m-d"));

		if($deadlines->aantal() == 0) {
			$page->add(new HtmlParagraph("Er zijn geen deadlines binnen deze selectie gevonden."));
		} else {
			$page->add(ContractonderdeelVerzamelingView::deadlines($deadlines));
		}


		return new PageResponse($page);
	}

	public static function overzichtToDos()
	{
		$page = Page::getInstance();
		$page->start(_("ToDo's"));

		$page->add(new HtmlParagraph("Het overzicht voor alle ToDo's van alle spoken."));
		$todos = InteractieVerzameling::metConstraints(null,null, null, null, null, null, 1);
		if($todos->aantal() == 0) {
			$page->add("Er zijn geen ToDo's binnen deze selectie gevonden.");
		} else {
			$page->add(InteractieVerzamelingView::htmlList($todos, true));
		}
		return new PageResponse($page);
	}

	public static function spokenBedrijfOverzicht()
	{
		$page = Page::getInstance();
		$page->start(_("Bedrijven per spook"));
		$page->add("Zie hieronder het overzicht van alle bedrijven per spook. Klik op een spook om al hun interacties te bekijken.");
		$page->add(new HtmlHR());

		$spoken = PersoonVerzameling::vanCommissie(SPOCIE);
		$table = new HtmlTable();
		foreach($spoken as $spook)
		{
			$first = true;
			$cps = ContactPersoonVerzameling::metConstraints(null, null, "huidig", $spook->geefID(), "SPOOK");
			$url = $cps->singleSpookWebUrl();
			$bedrijven = $cps->organisaties();
			foreach($bedrijven as $bedrijf)
			{
				$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell($first?new HtmlAnchor($url, PersoonView::naam($spook)):""),
					new HtmlTableDataCell(new HtmlAnchor($bedrijf->url(), OrganisatieView::naam($bedrijf)))
					)));
				$first = false;
			}
		}
		$page->add($table);
		return new PageResponse($page);
	}

	public static function overzichtBijnaNetVerlopenVacatures()
	{
		$page = Page::getInstance();
		$page->start(_("Bijna en net verlopen vacatures"));

		$page->add(new HtmlParagraph("Het overzicht voor alle vacatures die de komende 3 maanden en afgelopen maand gaan/zijn verlopen."));
		$vacatures = VacatureVerzameling::metConstraints('bijna')->sorteer("datum");
		$page->add(VacatureVerzamelingView::administratielijst($vacatures));
		return new PageResponse($page);
	}

	public static function sponsorcommissarissen()
	{
		$page = Page::getInstance();
		$page->start();

		$commissarissen = CommissieLidVerzameling::commissarissen("SPONS");
		$page->add(CommissieLidVerzamelingView::tableLijst($commissarissen));

		return new PageResponse($page);
	}

/*
	//Het financiele overzicht van alle contracten.
	//Modeleren naar Dibs::transactieOverzicht
	public static function financieelOverzicht()
	{
		// verwerk get-data
		$jaar	= (int)tryPar('jaar', date('Y'));

		if ($jaar < 0 || $jaar >= 10000)
			spaceHttp(501);

		$van = new DateTimeLocale("$jaar-08-01");
		$tot = clone $van;
		$tot->add(new DateInterval("P1Y"));
		// laad de contracten
		$contracten = ContractVerzameling::getContractenInPeriode($van, $tot);
		return ContractVerzamelingView::FinancieelTotaalOverzicht($contracten, $jaar);
	}

	public static function financieelOverzicht2()
	{
		$jaren = array();

		$jaarselect = HtmlSelectbox::fromArray('jaar', $jaren, $jaar, 1, true)
			->setMultiple(false);

		$page = Page::getInstance();
		$page->start(_("Financieel Overzicht"));

		$page->add(new HtmlParagraph("Het overzicht van alle huidge contracten in spookweb."));
		$page->add($jaarselect);

		$contracten = ContractVerzameling::metConstraints(null, null);
		$page->add(ContractVerzamelingView::FinancieelTotaalOverzicht($contracten));
		$page->end();
	}*/


	/**
	 *	Persoonlijke pagina's voor de spoken zelf
	 **/
	public static function spokenHome()
	{
		$spook = Persoon::getIngelogd();
		$page = Page::getInstance();
		$page->start(_("Persoonlijk overzicht"));
		$page->add(new HtmlParagraph(_("Welkom in je eigen kamer in het spookhuis. Hier kan je verschillende persoonlijke overzichten vinden.")));

		//Deadlines
		$deadlineDiv = new HtmlDiv(array(), "block");
		$deadlineDiv->add(new HtmlHeader(3, "Deadlines"));
		$deadlineDiv->add(new HtmlParagraph("Het overzicht van je deadlines:"));
		$deadlineDiv->add(new HtmlParagraph(ContractonderdeelVerzamelingView::deadlinePeriodeForm()));


		$now = new DateTimeLocale();
		$end = new DateTimeLocale();
		switch(trypar("Periode")) {
			case "3m":
				$end->add(new DateInterval("P3M"));
				break;
			case "1j":
				$end->add(new DateInterval("P1Y"));
				break;
			case "30d":
			default:
				$end->add(new DateInterval("P30D"));
		}

		$deadlines = ContractonderdeelVerzameling::metConstraints(null,null, $now->format("Y-m-d"), $end->format("Y-m-d"), $spook->geefID());
		$deadlines->sorteer("Datum");
		if($deadlines->aantal() == 0) {
			$deadlineDiv->add(new HtmlParagraph(_("Je hebt geen deadlines binnen deze selectie.")));
		} else {
			$deadlineDiv->add(ContractonderdeelVerzamelingView::deadlines($deadlines, true));
		}

		//Bedrijven
		$mijnBedrijven = BedrijfVerzameling::verzamel(explode(',',ContactPersoonVerzameling::metConstraints(null, null, 'huidig', Persoon::getIngelogd()->geefID(), "SPOOK")->implode('getOrganisatieContactID', ',')));
		$mijnBedrijven->sorteer('Naam');
		$mijnBedrijvenDiv = new HtmlDiv(array(), "block");
		//Maak er een uitklapbare div van als het er veel zijn.
		if($mijnBedrijven->aantal() > 6)
			$mijnBedrijvenDiv = new HtmlResizableDiv( "100%", "180px", "block");
		$mijnBedrijvenDiv->add(new HtmlHeader(3,_("Bedrijven")));
		if($mijnBedrijven->aantal() > 0) {
			$mijnBedrijvenDiv->add(BedrijfVerzamelingView::mijnList($mijnBedrijven));
		} else {
			$mijnBedrijvenDiv->add(new HtmlParagraph(_("Je hebt op dit moment geen bedrijven.")));
		}

		//Blocks laten zien voor deadlines en bedrijven
		$blocksdiv = new HtmlDiv(array(), "blockRow");
		$blocksdiv->add(new HtmlHR());
		$blocksdiv->add($deadlineDiv);
		$blocksdiv->add(new HtmlDiv(array(), "blockSeparator"));
		$blocksdiv->add($mijnBedrijvenDiv);
		$blocksdiv->add(new HtmlHR());

		$page->add($blocksdiv);

		//ToDo's
		$page->add(new HtmlHeader(3, "ToDo's"));
		$todos = InteractieVerzameling::metConstraints(null,null, $spook->geefID(), null, null, null, 1);
		if($todos->aantal() == 0) {
			$page->add(new HtmlParagraph(_("Je hebt op dit moment geen ToDo's!")));
		} else {
		$page->add(new HtmlParagraph(_("De dingen die je als ToDo hebt aangemerkt:")));
			$page->add(InteractieVerzamelingView::htmlList($todos, true));
		}

		//Contracten
		$contracten = ContractVerzameling::metConstraints(null, trypar('Status'), null, array(Persoon::getIngelogd()->geefID()));
		$contractenDiv = new HtmlDiv();

		$contractenDiv->add(new HtmlHR())
			->add(new HtmlHeader(3, "Contracten"))
			->add(new HtmlParagraph(_("Contracten die aan jou verbonden zijn:")))
			->add(new HtmlParagraph(ContractVerzamelingView::zoekForm()))
			->add(ContractVerzamelingView::htmlList($contracten, true));

		$page->add($contractenDiv);

		$page->add(new HtmlParagraph(tipbox(sprintf(_("Heb je nog ideeen voor overzichten die missen of die handiger kunnen? Maak dan een bug of feature request aan in %s! "), new HtmlAnchor("/service/bugs/Nieuw", "bugweb")), false )));

		return new PageResponse($page);
	}

/************
 *  Alumni  *
 ************/

	/**
	 * Een alumnus bestaat uit een verzameling aan contactpersonen die allemaal aan een persoon gekoppeld zijn.
	 **/
	public static function alumnusEntry($args)
	{
		$alumnus = ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $args[0], "ALUMNUS");
		if ($alumnus->aantal() == 0) return false;

		return array( 'name' => $alumnus->first()->getPersoonContactID()
					, 'displayName' => 'Alumnus'
					, 'access' => $alumnus->first()->magBekijken());
	}

	/**
	 * Een specifieke alumnus relatie tussen een persoon en een bedrijf.
	 **/
	public static function alumnusSpecifiekEntry($args)
	{
		$alumnus = ContactPersoon::geef($args[1], $args[0], "ALUMNUS");

		return array( 'name' => $args[0]
					, 'displayName' => 'Alumnus'
					, 'access' => $alumnus->magBekijken());
	}

	public static function alumnusDetails()
	{
		/** Laad de gekozen alumnus **/
		$id = (int) vfsVarEntryName();

		$alumnus = ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $id, "ALUMNUS");

		if(!$alumnus->first()->magBekijken())
			spaceHTTP(403);

		$page = Page::getInstance();;
		$page->start();
		/** Ga naar de view **/
		$page->add(ContactPersoonVerzamelingView::alumnusDetails($alumnus));

		return new PageResponse($page);
	}

	/**
	 *	Een specifieke alumnus laten zien: een enkele contactpersoon uit de contactpersoonverzameling
	 *	die een alumnus representeert.
	 **/
	public static function alumnusSpecifiekDetails()
	{
		global $logger;
		/** Laad de gekozen alumnus **/
		$ids = vfsVarEntryNames();
		$alumnus = ContactPersoon::geef($ids[1], $ids[0], "ALUMNUS");
		$logger->debug('alumnusdetails ' . print_r($alumnus, true));

		if(!$alumnus->magBekijken())
			spaceHTTP(403);

		$page = ContactPersoonView::pageLinks($alumnus);
		$page->start(_("Details alumnus"));
		/** Ga naar de view **/
		$page->add(ContactPersoonView::details($alumnus));

		return new PageResponse($page);
	}

	public static function alumnusNieuw()
	{
		$bedrijf = Bedrijf::geef(vfsVarEntryName());
		$alumnus = new ContactPersoon(new Persoon(), $bedrijf, "ALUMNUS");
		$alumnus->setBeginDatum(new DateTimeLocale());
		return self::alumnusWijzigen($alumnus);
	}

	public static function alumnusWijzigen(ContactPersoon $alumnus = null)
	{
		if(!$alumnus) {
			/** Laad zo nodig de alumnus **/
			$ids = vfsVarEntryNames();
			$alumnus = ContactPersoon::geef($ids[1], $ids[0], "ALUMNUS");
		}

		//Als we in het nieuwformulier zojuist een alumnus hebben ingevuld dan moet dit ook in het object
		// worden verwerkt.
		if(!$alumnus->getIndb() && trypar("Persoon"))
		{
			$oudalumnus = clone($alumnus);
			$alumnus = new ContactPersoon(Persoon::geef(trypar("Persoon")), $oudalumnus->getOrganisatie(), "ALUMNUS");
			//TODO: uitzoeken waarom hij deze info niet uit het hidden veld in het formulier haalt.
		}

		if(!$alumnus->magWijzigen())
			spaceHTTP(403);

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ContactPersoonWijzig')
		{
			ContactPersoonView::processWijzigForm($alumnus);
			if ($alumnus->valid())
			{
				$alumnus->opslaan();
				Page::redirectMelding($alumnus->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		ContactPersoonView::wijzig($alumnus, $show_error);
	}

	public static function alumnusVerwijderen(ContactPersoon $alumnus = null)
	{
		if(!$alumnus) {
			/** Laad zo nodig de alumnus **/
			$ids = vfsVarEntryNames();
			$alumnus = ContactPersoon::geef($ids[1], $ids[0], "ALUMNUS");
		}

		if(!$alumnus->magWijzigen())
			spaceHTTP(403);

		//Zoek alle interacties van deze alumnus op
		$bedrijf = $alumnus->getOrganisatie();
		$interacties = InteractieVerzameling::metConstraints("alle", '', $alumnus->geefID(), '', $bedrijf->geefID());

		if (Token::processNamedForm() == 'ContactPersoonVerwijderen')
		{
			//Gooit ook de bijbehorende interacties weg!
			$returnVal = $alumnus->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(SPOOKBASE, sprintf(_("De alumnus is verwijderd! Ga terug naar het %s"),
					new HtmlAnchor($bedrijf->url(), _("bedrijf")) ));
			}
		}

		$page = Page::getInstance();
		$page->start(_("Alumnus &gt; Verwijderen"))
			->add(ContactPersoonView::verwijderForm($alumnus))
			->add(new HtmlParagraph(sprintf(_('Let op: je verwijdert ook meteen de %s bijbehorende interacties!'), $interacties->aantal()) ));
			return new PageResponse($page);

	}

/**************
 *  Bedrijven *
 **************/

	public static function bedrijfOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Spookweb 2: Bedrijfoverzichtpagina"));

		$bedrijven = BedrijfQuery::metConstraints(trypar('Naam'), trypar('Type'), trypar('Status'))->verzamel();

		$page->add(new HtmlHeader(3,_('Welkom op de spookweb 2 bedrijfoverzichtpagina!')))
			->add(new HtmlParagraph('Er zitten nu het volgende aantal bedrijven in je selectie uit de db van spookweb 2: '.
				$bedrijven->aantal()))
			->add(BedrijfVerzamelingView::zoekform())
			->add(BedrijfVerzamelingView::htmlList($bedrijven));
		return new PageResponse($page);
	}

	public static function bedrijfsEntry($args)
	{
		$bedrijf = Bedrijf::geef($args[0]);
		if (!$bedrijf) return false;

		return array( 'name' => $bedrijf->geefID()
					, 'displayName' => $bedrijf->getNaam()
					, 'access' => $bedrijf->magBekijken());
	}

	public static function bedrijfsDetails()
	{
		/** Laad het gekozen bedrijf **/
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$page = BedrijfView::pageLinks($bedrijf);

		$page->start($bedrijf->getnaam())
			->add(BedrijfView::details($bedrijf));
		return new PageResponse($page);
	}

	//Maak een nieuw bedrijf aan en ga deze 'wijzigen'
	public static function bedrijfNieuw()
	{
		return self::bedrijfWijzigen(new Bedrijf());
	}

	public static function bedrijfWijzigen($bedrijf = null)
	{

		/** Laad zo nodig het gekozen bedrijf **/
		if(!$bedrijf)
		{
			$id = (int) vfsVarEntryName();
			$bedrijf = Organisatie::geef($id);
		}

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'BedrijfWijzig')
		{
			BedrijfView::processForm($bedrijf);
			if ($bedrijf->valid())
			{
				$bedrijf->opslaan();

				//Check waarheen te redirecten.
				if(tryPar('submit') == 'Wijzig Adres (ook)')
				{
					Page::redirectMelding($bedrijf->url()."/".'WijzigAdres'
						, _("De gegevens zijn opgeslagen!"));
				}
				else
				{
					Page::redirectMelding($bedrijf->url()
						, _("De gegevens zijn opgeslagen!"));
				}
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		return new PageResponse(BedrijfView::wijzigen($bedrijf, $show_error));
	}

	public static function bedrijfVerwijderen($bedrijf = null)
	{
		/** Laad zo nodig het gekozen bedrijf **/
		if(!$bedrijf)
		{
			$id = (int) vfsVarEntryName();
			$bedrijf = Organisatie::geef($id);
		}

		if(!$bedrijf->magWijzigen())
			spaceHTTP(403);


		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'BedrijfVerwijderen')
		{
			$returnVal = $bedrijf->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(SPOOKBASE, _("Het bedrijf is verwijderd!"));
			}
		}

		$page = Page::getInstance();
		$page->start(_("Verwijderen"))
			->add(BedrijfView::verwijderForm($bedrijf))
			->add(new HtmlParagraph(_("Let op: je verwijdert ook meteen alle dingen gerelateerd aan het bedrijf!")));
		return new PageResponse($page);
	}

	public static function bedrijfSpokenOverzicht()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$spoken = ContactPersoonVerzameling::metConstraints($bedrijf, null, 'alle', null, "SPOOK");
		$spoken->sorteer("EindDatum");
		$overig = ContactPersoonVerzameling::metConstraints($bedrijf, null, 'alle', null, "OVERIGSPOOKWEB");
		$overig->sorteer("EindDatum");
		$page = Page::getInstance();
		$page->start($bedrijf->getNaam() . " &gt; Spoken");
		$page->add(new HtmlHeader(3, "(Oud-)spoken van dit bedrijf:"));
		$page->add(new HtmlParagraph(_("Om spoken hun gegevens te wijzigen kan je op hun naam klikken.")));
		$page->add(new HtmlParagraph(ContactPersoonVerzamelingView::htmlListBedrijfContact($spoken)));
		$page->add(new HtmlAnchor("Nieuwspook", _("Nieuw spook toevoegen")));
		$page->add(new HtmlHeader(3, "Overige leden die contact hebben (gehad) met dit bedrijf:"));
		$page->add(ContactPersoonVerzamelingView::htmlListBedrijfContact($overig));
		$page->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}

	public static function bedrijfAlumniOverzicht()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$alumni = ContactPersoonVerzameling::metConstraints($bedrijf, null, 'alle', null, "ALUMNUS");
		$alumni->sorteer("EindDatum");
		$page = Page::getInstance();
		$page->start($bedrijf->getNaam() . " &gt; Alumni");
		$page->add(new HtmlHeader(3, "(Oud-)medewerkers van dit bedrijf die A–Eskwadraat alumnus zijn:"));
		$page->add(new HtmlParagraph(ContactPersoonVerzamelingView::htmlListBedrijfContact($alumni)));
		$page->add(new HtmlAnchor("Nieuwalumnus", _("Nieuwe alumnus/a toevoegen")));
		$page->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}

	public static function bedrijfContactenOverzicht()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$q = ContactPersoonQuery::metConstraints($bedrijf, null, 'alle', null, 'BEDRIJFSCONTACT');
		$bedrijfscontacten = $q->orderByDesc('beginDatum')->verzamel();

		$page = Page::getInstance();
		$page->start($bedrijf->getNaam() . " &gt; Bedrijfscontacten");
		$page->add(ContactPersoonVerzamelingView::htmlListBedrijfContact($bedrijfscontacten));
		$page->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}

	public static function bedrijfContractenOverzicht()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$contracten = ContractVerzameling::metConstraints($bedrijf->geefID(), 'alle');
		$page = Page::getInstance();
		$page->start($bedrijf->getNaam() . " &gt; Contracten");
		$page->add(ContractVerzamelingView::bedrijfDetailPagina($contracten));
		$page->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}

	public static function bedrijfInteractieOverzicht()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$interacties = InteractieVerzameling::metConstraints('1990-01-01', null, null, null, $bedrijf->geefID());
		$page = Page::getInstance();
		$page->start($bedrijf->getNaam() . " &gt; Interacties");
		$page->add(InteractieVerzamelingView::htmlList($interacties));
		$page->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}

	public static function bedrijfStudiesOverzicht()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);

		if(!$bedrijf->magBekijken())
			spaceHTTP(403);

		$bedrijfstudies = BedrijfStudieVerzameling::vanBedrijf($bedrijf);
		$page = Page::getInstance();

		if ($bedrijf->magWijzigen())
			$page->setEditUrl($bedrijf->url() . '/Studieswijzigen');

		$page->start($bedrijf->getNaam() . " &gt; Studies");
		$page->add(new HtmlParagraph(_("Dit bedrijf is interessant voor of heeft interesse in de onderstaande studies.")));
		$page->add(BedrijfStudieVerzamelingView::htmllijst($bedrijfstudies));
		$page->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}


/**********************
 *  Bedrijfscontacten *
 **********************/

	public static function bedrijfscontactOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Spookweb 2 ? bedrijfscontactenoverzichtpagina"));

		$bedrijfscontacten = ContactPersoonVerzameling::metConstraints(null, null, null, null, 'Bedrijfscontact');

		$page->add(new HtmlHeader(3,_('Welkom op de spookweb 2 bedrijfscontactenoverzichtpagina!')))
			->add(new HtmlParagraph('Er zitten nu het volgende aantal bedrijfscontacten in je selectie uit de db van spookweb 2: '. $bedrijfscontacten->aantal()))
			->add(ContactPersoonVerzamelingView::htmlListSpookweb($bedrijfscontacten));
		return new PageResponse($page);
	}

	/**
	 * Een spook bestaat uit een verzameling aan contactpersonen die allemaal aan een persoon gekoppeld zijn.
	 **/
	public static function bedrijfscontactEntry($args)
	{
		global $logger;
		$logger->debug('bedrijfscontactEntry op ' . print_r($args, true));

		$bedrijfscontact = ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $args[0], 'BEDRIJFSCONTACT');
		if ($bedrijfscontact->aantal() == 0) return false;

		return array( 'name' => $bedrijfscontact->first()->getPersoonContactID()
					, 'displayName' => 'Bedrijfscontact'
					, 'access' => $bedrijfscontact->first()->magBekijken());
	}

	/**
	 * Een specifieke spook relatie tussen een persoon en een bedrijf.
	 **/
	public static function bedrijfscontactSpecifiekEntry($args)
	{
		$bedrijfscontact = ContactPersoon::geef($args[1], $args[0], "BEDRIJFSCONTACT");

		return array( 'name' => $args[0]
					, 'displayName' => 'Bedrijfscontact'
					, 'access' => $bedrijfscontact->magBekijken() && isBedrijfscontact($bedrijfscontact));
	}

	public static function bedrijfscontactDetails()
	{
		/** Laad het gekozen contact **/
		$id = (int) vfsVarEntryName();
		$bedrijfscontact = ContactPersoonVerzameling::metConstraints(null, 'Bedrijfscontact', 'ALLE', $id, "BEDRIJFSCONTACT");

		if(!$bedrijfscontact->first()->magBekijken())
			spaceHTTP(403);

		$page = Page::getInstance();
		$page->start();

		/** Ga naar de view **/
		$page->add(ContactPersoonVerzamelingView::bedrijfscontactDetails($bedrijfscontact));

		return new PageResponse($page);
	}

	/**
	 *	Een specifiek bedrijfscontact laten zien: een enkele contactpersoon uit de contactpersoonverzameling
	 *	die een bedrijfscontact representeert.
	 **/
	public static function bedrijfscontactSpecifiekDetails()
	{
		/** Laad het gekozen bedrijfscontact **/
		$ids = vfsVarEntryNames();
		$bedrijfscontact = ContactPersoon::geef($ids[1], $ids[0], "BEDRIJFSCONTACT");

		$persoonsnaam = PersoonView::naam(Persoon::geef($ids[1]));
		$bedrijfsnaam = BedrijfView::waardeNaam(Bedrijf::geef($ids[0]));

		if(!$bedrijfscontact->magBekijken())
			spaceHTTP(403);

		$page = ContactPersoonView::pageLinks($bedrijfscontact);
		$page->start(_("Interactie met $persoonsnaam voor $bedrijfsnaam"));
		/** Ga naar de view **/
		$page->add(ContactPersoonView::details($bedrijfscontact));

		return new PageResponse($page);
	}

	public static function bedrijfscontactNieuw()
	{
		$bedrijf = Bedrijf::geef(vfsVarEntryName());
		$bedrijfscontact = new ContactPersoon(new Persoon(), $bedrijf, "BEDRIJFSCONTACT");
		$bedrijfscontact->setBeginDatum(new DateTimeLocale());
		return self::bedrijfscontactWijzigen($bedrijfscontact);
	}

	public static function bedrijfscontactWijzigen(ContactPersoon $bedrijfscontact = null)
	{
		if(!$bedrijfscontact) {
			/** Laad zo nodig het bedrijfscontact **/
			$ids = vfsVarEntryNames();
			$persoon = Persoon::geef($ids[1]);
			$bedrijfscontact = ContactPersoon::geef($ids[1], $ids[0], "BEDRIJFSCONTACT");
		} else {
			$persoon = $bedrijfscontact->getPersoon();
		}

		//Als we in het nieuwformulier zojuist een bedrijfscontact hebben ingevuld dan moet dit ook in het object
		// worden verwerkt.
		if(!$bedrijfscontact->getIndb() && trypar("Persoon[Achternaam]"))
		{
			$oudbedrijfscontact = clone($bedrijfscontact);
			$bedrijfscontact = new ContactPersoon($persoon,
						$oudbedrijfscontact->getOrganisatie(),
						"BEDRIJFSCONTACT");
		}

		if(!$bedrijfscontact->magWijzigen())
			spaceHTTP(403);

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ContactPersoonWijzig')
		{
			ContactPersoonView::processWijzigForm($bedrijfscontact);
			if ($bedrijfscontact->valid() && $persoon->valid())
			{
				$persoon->opslaan();
				$bedrijfscontact->opslaan();
				Page::redirectMelding($bedrijfscontact->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

/* Maakt de boel mooi, maar moet even anders worden gedaan zodat alles consistent blijft.
		$page = Page::getInstance();
		if($spook->getIndb()) {
			$actie = "wijzigen";
		} else {
			$actie = "toevoegen";
		}
		$page->start($spook->getOrganisatie()->getNaam() . " > Spook " . $actie);
*/
		ContactPersoonView::wijzig($bedrijfscontact, $show_error);
	}

	public static function bedrijfscontactVerwijderen(ContactPersoon $bedrijfscontact = null)
	{
		if(!$bedrijfscontact) {
			/** Laad zo nodig het bedrijfscontact **/
			$ids = vfsVarEntryNames();
			$persoon = Persoon::geef($ids[1]);
			$bedrijfscontact = ContactPersoon::geef($ids[1], $ids[0], "BEDRIJFSCONTACT");
		} else {
			$persoon = $bedrijfscontact->getPersoon();
		}

		if(!$bedrijfscontact->magWijzigen())
			spaceHTTP(403);

		//Zoek alle interacties van dit bedrijfscontact op
		$bedrijf = $bedrijfscontact->getOrganisatie();
		$interacties = InteractieVerzameling::metConstraints("alle", '', '', $bedrijfscontact->geefID(), $bedrijf->geefID());

		if (Token::processNamedForm() == 'ContactPersoonVerwijderen')
		{
			//Gooit ook meteen de bijbehorende interacties weg
			$returnVal = $bedrijfscontact->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(SPOOKBASE, sprintf(_("Het bedrijfscontact is verwijderd! Ga terug naar het %s"),
					new HtmlAnchor($bedrijf->url(), _("bedrijf")) ));
			}
		}

		$page = Page::getInstance();
		$page->start(_("Bedrijfscontact &gt; Verwijderen"))
			->add(ContactPersoonView::verwijderForm($bedrijfscontact))
			->add(new HtmlParagraph(sprintf(_('Let op: je verwijdert ook meteen de %s bijbehorende interacties!'), $interacties->aantal()) ));
		return new PageResponse($page);
	}

/**********************
 *  Bedrijfsprofielen *
 **********************/

	public static function bedrijfsprofielOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Bedrijfsprofielen"));

		$selectie = 'huidig';
		if(trypar('oud') == 'ja' && hasAuth('spocie'))
			$selectie = 'alle';
		$profielen = BedrijfsprofielVerzameling::metConstraints($selectie);

		$page->add(new HtmlDiv(_('Dit zijn een aantal van de organisaties die '
			. aesnaam() . ' ondersteunen in haar activiteiten, klik op een logo '
			. 'voor meer informatie over de betreffende organisatie.'), 'alert alert-info'));
		if(hasAuth('spocie'))
		{
			$page->add(new HtmlDiv(array(new HtmlHeader(4, _("Spocie toptip:"))
				, new HtmlParagraph(_('Je kan nieuw bedrijfsprofiel toevoegen '
				. 'via een bedrijfsprofielcontractonderdeel van het betreffende bedrijf.')))
				, 'bs-callout bs-callout-info'));
			$page->add(new HtmlParagraph(HtmlAnchor::button('?oud=ja','Bekijk ook verlopen profielen')));
			//TODO: de variabele $lijst bestaat niet, deze regel gaat dus fout in de debug, dat moet nog gefixt worden
//			if(DEBUG)
//				$page->add("Profielen in CSV: " . count($lijst) . ", waarvan koppelbaar: " . $verzameling->aantal());
		}
		$page->add(BedrijfsprofielVerzamelingView::profielenPagina($profielen));

		/**
		 * Bug #7925: viewpane verandert tov bedrijfsprofiel-anchors tijdens
		 * laden van alle bedrijfsprofielthumbnails omdat de grootte hiervan nog
		 * niet bepaald is. Daarom, 're-adjusten' we de scroll-locatie naar het
		 * "#bla" deel als alle images geladen zijn.
		 */
		$page->addFooter(HtmlScript::makeJavascript("
$(window).load(function() {
	if (location.hash) location.hash = location.hash;
});
"));

		return new PageResponse($page);
	}

	public static function bedrijfsprofielEntry($args)
	{
		$profiel = Bedrijfsprofiel::geef($args[0]);
		if (!$profiel) return false;

		return array( 'name' => $profiel->geefID()
					, 'displayName' => Bedrijf::vanProfiel($profiel)->getNaam() . "'s profiel"
					, 'access' => $profiel->magBekijken());
	}

	//Maak een leeg object aan, dat gekoppeld wordt aan een contractonderdeel en
	// zorg dat we dit object kunnen aanpassen.
	public static function bedrijfsprofielNieuw(Contractonderdeel $contractonderdeel)
	{
		$now = new DateTimeLocale();
		$profiel = new Bedrijfsprofiel();
		$profiel->setContractonderdeel($contractonderdeel);
		$profiel->setDatumVerloop($now->add(new DateInterval("P1Y")));
		$profiel->setUrl(Bedrijf::vanProfiel($profiel)->getHomepage());
		return self::bedrijfsprofielWijzigen($profiel);
	}

	public static function bedrijfsprofielWijzigen($profiel = null)
	{
		/** Laad zo nodig de gekozen profiel **/
		if(!$profiel) {
			$id = (int) vfsVarEntryName();
			$profiel = Bedrijfsprofiel::geef($id);
		}

		$show_error = false;

		if(!$profiel->magWijzigen())
			spaceHTTP(403);

		/** Verwerk formulier-data **/
		//TODO: error verwerking fixxen
		if (Token::processNamedForm() == 'profielWijzig')
		{
			BedrijfsprofielView::processWijzigForm($profiel);
			if ($profiel->valid())
			{
				$profiel->opslaan();
				//Set zo nodig het id van het profiel bij het contractonderdeel
				$contractonderdeel = $profiel->getContractonderdeel();
				if(!$contractonderdeel->getUitgevoerd()) {
					$contractonderdeel->setUitgevoerd($profiel->geefID());
					$contractonderdeel->opslaan();
				}
				Page::redirectMelding('/Service/Spookweb2/Bedrijfsprofiel/', _("De gegevens zijn opgeslagen! 
					Je wordt doorgestuurd naar de bedrijfsprofielenpagina."));
			}
			else
			{
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		$page = BedrijfsprofielView::pageLinks($profiel);
		$page->start("Bedrijfsprofiel &gt; Wijzigen");
		$page->add(BedrijfsprofielView::wijzigen($profiel, $show_error));

		return new PageResponse($page);
	}

	public static function bedrijfsprofielVerwijderen($profiel = null)
	{
		/** Laad zo nodig de gekozen profiel **/
		if(!$profiel) {
			$id = (int) vfsVarEntryName();
			$profiel = Bedrijfsprofiel::geef($id);
		}

		if(!$profiel->magWijzigen())
			spaceHTTP(403);

		if (Token::processNamedForm() == 'BedrijfsprofielVerwijderen')
		{
			$contractonderdeel = $profiel->getContractonderdeel();
			$returnVal = $profiel->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				$contractonderdeel->setUitgevoerd(false);
				$contractonderdeel->opslaan();
				Page::redirectMelding(SPOOKBASE, sprintf(_("Het bedrijfsprofiel is verwijderd! Ga terug naar het %s"),
					new HtmlAnchor($contractonderdeel->getContract()->url(), _("contract")) ));
			}
		}

		$page = Page::getInstance();
		$page->start(_("Bedrijfsprofiel &gt; Verwijderen"))
			->add(BedrijfsprofielView::verwijderForm($profiel));
		return new PageResponse($page);
	}

/******************
 *  BedrijfStudie *
 ******************/

	/**
	 *	/brief Wijzig de verzameling studies van een bedrijf.
	 *
	 *	Laat de studieverzameling zien die bij dit bedrijf hoort en maak
	 *	deze aanpasbaar in een mhtmlmultiselectbox. De functie verwerkt
	 *	zelf de ingevoerde data aangezien het hier om een simpel verzameling
	 *	formulier gaat waar weinig generieks aan is te doen.
	 **/
	public static function bedrijfStudiesWijzigen()
	{
		$id = (int) vfsVarEntryName();
		$bedrijf = Organisatie::geef($id);
		$bedrijfstudies = BedrijfStudieVerzameling::vanBedrijf($bedrijf);

		$page = Page::getInstance();


		if (Token::processNamedForm() == 'studieverzameling')
		{
			$studies = StudieVerzameling::verzamel(trypar('studies'));

			//We gooien eerst alle huidige bedrijfstudies weg en maken daarna nieuwe aan,
			// klinkt overbodig, maar dan hoeven we niets te checken.
			$returnVal = $bedrijfstudies->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {

				foreach($studies as $studie)
				{
					$bedrijfstudie = new BedrijfStudie($studie, $bedrijf);
					$bedrijfstudie->opslaan();
				}

				$bedrijfstudies = BedrijfStudieVerzameling::vanBedrijf($bedrijf);
				Page::addMelding(_("De studies zijn aangepast."));
			}
		}

		$page->start();
		$page->add(new HtmlParagraph(_("Selecteer de studies die bij dit bedrijf horen.")))
			->add(StudieVerzamelingView::wijzigForm($bedrijfstudies->toStudieVerzameling()))
			->add(new HtmlParagraph(_("Naar het bedrijf: " . new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf))) ));
		return new PageResponse($page);
	}

/**************
 * Contracten *
 *************/

	public static function contractOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Contractoverzichtpagina"));

		$contracten = ContractVerzameling::metConstraints(null, trypar('Status'));
		$contracten->sorteer('Id');

		$page->add(new HtmlHeader(3,_('Welkom op de spookweb 2 contractoverzichtpagina!')))
			->add(new HtmlParagraph('Hieronder kan je een overzicht van de gekozen type contracten bekijken.'))
			->add(ContractVerzamelingView::zoekForm())
			->add(new HtmlHR())
			->add(ContractVerzamelingView::htmlList($contracten));

		return new PageResponse($page);
	}

	public static function contractEntry($args)
	{
		$contract = Contract::geef($args[0]);
		if (!$contract) return false;

		return array( 'name' => $contract->geefID()
					, 'displayName' => 'Contract'
					, 'access' => $contract->magBekijken());
	}

	public static function contractDetails()
	{
		/** Laad het gekozen contract **/
		$id = (int) vfsVarEntryName();
		$contract = Contract::geef($id);

		if(!$contract->magBekijken())
			spaceHTTP(403);

		/** Ga naar de view **/
		return new PageResponse(ContractView::details($contract));
	}

	public static function contractFinancieelDetails()
	{
		/** Laad het gekozen contract **/
		$id = (int) vfsVarEntryName();
		$contract = Contract::geef($id);

		if(!$contract->magBekijken())
			spaceHTTP(403);

		/** Ga naar de view **/
		return new PageResponse(ContractView::financieel($contract));
	}

	public static function contractLatex()
	{
		/** Laad het gekozen contract **/
		$id = (int) vfsVarEntryName();
		$contract = Contract::geef($id);

		if(!$contract->magBekijken())
			spaceHTTP(403);

		return new PageResponse(ContractView::latexPagina($contract));
	}

	public static function contractLatexDownload()
	{
		/** Laad het gekozen contract **/
		$id = (int) vfsVarEntryName();
		$contract = Contract::geef($id);

		if(!$contract->magBekijken())
			spaceHTTP(403);

		$bedrijf = BedrijfView::waardeNaamFilename($contract->getBedrijf());

		// TODO SOEPMES: maak LaTeXresponse-klasse en/of LaTeXpage-klasse
		// Voorlopig eventjes handmatig regelen...
		$response = new Response();
		$response->headers->set(
			"Content-Disposition",
			"attachment; filename=contract_$bedrijf.tex"
		);
		$response->headers->set("Content-type", "application/x-tex; encoding=utf-8");

		$response->setContent(ContractView::maakContractLatex($contract));
		return $response;
	}


	/**
	 * Maak een nieuw contract aan en ga deze 'wijzigen', zo mogelijk alvast
	 * met een bedrijf ingesteld.
	 */
	public static function contractNieuw()
	{
		$contract = new Contract();
		$bedrijf = Bedrijf::geef(vfsVarEntryName());
		if($bedrijf instanceof Bedrijf)
			$contract->setBedrijf($bedrijf);
		$contract->setStatus('VOORSTEL');
		return self::contractWijzigen($contract);
	}

	public static function contractWijzigen($contract = null)
	{
		/** Laad zo nodig het gekozen contract **/
		if(!$contract)
		{
			$id = (int) vfsVarEntryName();
			$contract = Contract::geef($id);
		}

		if(!$contract->magBekijken())
			spaceHTTP(403);

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ContractWijzig')
		{
			ContractView::processWijzigForm($contract);
			if ($contract->valid())
			{
				$contract->opslaan();
				Page::redirectMelding($contract->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		return new PageResponse(ContractView::wijzigen($contract, $show_error));
	}

	public static function contractVerwijderen($contract = null)
	{
		/** Laad zo nodig het gekozen contract **/
		if(!$contract)
		{
			$id = (int) vfsVarEntryName();
			$contract = Contract::geef($id);
		}

		if(!$contract->magWijzigen())
			spaceHTTP(403);


		//Zoek alle contractonderdelen van dit contract op
		$contractonderdelen = ContractonderdeelVerzameling::metConstraints($contract->geefID());

		if (Token::processNamedForm() == 'ContractVerwijderen')
		{
			$bedrijf = $contract->getBedrijf();

			//Contractonderdelen worden ook meteen weggegooit.
			$returnVal = $contract->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(SPOOKBASE, sprintf(_("Het contract is verwijderd! Ga terug naar het %s."),
					new HtmlAnchor($bedrijf->url(), _("bedrijf")) ));
			}
		}

		$page = Page::getInstance();
		$page->start(_("Contract &gt; Verwijderen"))
			->add(ContractView::verwijderForm($contract))
			->add(new HtmlParagraph(sprintf(_('Let op: je verwijdert ook meteen de %s bijbehorende contractonderdelen met hun uitvoeringen!'), $contractonderdelen->aantal()) ));

		return new PageResponse($page);
	}

 /**
  * Contractonderdeel
  **/

	public static function contractonderdeelEntry($args)
	{
		$contractonderdeel = Contractonderdeel::geef($args[0]);
		//Check ook of deze contract en onderdeel combo valide is
		if (!$contractonderdeel || $contractonderdeel->getContractContractID() != $args[1]) return false;

		return array( 'name' => $contractonderdeel->geefID()
					, 'displayName' => 'Contractonderdeel'
					, 'access' => $contractonderdeel->magBekijken());
	}

	//Maak een nieuw contractonderdeel aan en ga deze 'wijzigen'
	public static function contractonderdeelNieuw()
	{
		$contractonderdeel = new Contractonderdeel();
		$contractonderdeel->setContract(Contract::geef(vfsVarEntryName()));
		return self::contractonderdeelWijzigen($contractonderdeel);
	}


	public static function contractonderdeelWijzigen($contractonderdeel = null)
	{
		/** Laad zo nodig het gekozen contractonderdeel **/
		if(!$contractonderdeel)
		{
			$id = (int) vfsVarEntryName();
			$contractonderdeel = Contractonderdeel::geef($id);
		}

		if(!$contractonderdeel->magWijzigen())
			spaceHTTP(403);

		/** Buffer voor de output **/
		$form = true;
		$msg = null;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ContractonderdeelWijzig')
		{
			ContractonderdeelView::processWijzigForm($contractonderdeel);
			//TODO: valid checkt (nog) niet of de relatie van een contractonderdeel met z'n mogelijk
			// gekoppelde uitgevoerde onderdelen klopt. Dit moet nog gefixed worden zodat een bedrijfsprofiel
			// zo niet aan een mailing gekoppeld kan worden.
			if ($contractonderdeel->valid())
			{
				$contractonderdeel->opslaan();
				Page::redirectMelding($contractonderdeel->getContract()->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
		}

		return new PageResponse(ContractonderdeelView::wijzigen($contractonderdeel, $form, $msg));
	}

	public static function contractonderdeelVerwijderen($contractonderdeel = null)
	{
		/** Laad zo nodig het gekozen contractonderdeel **/
		if(!$contractonderdeel)
		{
			$id = (int) vfsVarEntryName();
			$contractonderdeel = Contractonderdeel::geef($id);
		}

		if(!$contractonderdeel->magWijzigen())
			spaceHTTP(403);

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ContractonderdeelVerwijderen')
		{
			$contract = $contractonderdeel->getContract();
			//Gooit ook meteen de uitvoering weg als dit mogelijk is
			$returnVal = $contractonderdeel->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding($contract->url(), sprintf(_("Het contractonderdeel is verwijderd!") ));
			}
		}

		$page = Page::getInstance();
		$page->start(_('Contractonderdeel &gt; Verwijderen'))
			->add(ContractonderdeelView::verwijderForm($contractonderdeel))
			->add(new HtmlParagraph(_('Let op: je verwijdert ook meteen de bijbehorende uitvoering!') ));
		return new PageResponse($page);
	}

	/**
	 *	Functie om een contractonderdeel uit te voeren. Zoek uit wat voor soort type
	 *	 het is en handel daar naar.
	 **/
	public static function contractonderdeelUitvoeren($contractonderdeel = null)
	{
		/** Laad zo nodig het gekozen contractonderdeel **/
		if(!$contractonderdeel)
		{
			$id = (int) vfsVarEntryName();
			$contractonderdeel = Contractonderdeel::geef($id);
		}

		$div = new HtmlDiv();
		//Afhankelijk van het soort onderdeel kunnen we dingen wel of niet uitvoeren.
		switch($contractonderdeel->getSoort())
		{
			case 'PROFIEL':
				if($contractonderdeel->getUitgevoerd()) {
					$profiel = Bedrijfsprofiel::geef($contractonderdeel->getUitgevoerd());
					spaceRedirect($profiel->wijzigURL());
				}
				//En maak anders een nieuwe
				return self::bedrijfsprofielNieuw($contractonderdeel);
				break;
			case 'VACATURE':
				if($contractonderdeel->getUitgevoerd()) {
					$vacature = Vacature::geef($contractonderdeel->getUitgevoerd());
					spaceRedirect($vacature->url());
				}
				//En maak anders een nieuwe
				return self::vacatureNieuw($contractonderdeel);
				break;
			default:
				//Markeer het contractonderdeel als uitgevoerd als dat nodig is.
				if (Token::processNamedForm() == 'uitvoeren')
				{
					//Hier zit geen echt id aan vast, maar het moet iig geen 0 zijn.
					$contractonderdeel->setUitgevoerd(-1);
					$contractonderdeel->opslaan();
					$contract = $contractonderdeel->getContract();
					Page::redirectMelding($contract->url(), _("Het contractonderdeel is als uitgevoerd gemarkeert!"));
				}

				$doenform = HtmlForm::named("uitvoeren");
				$doenform->add(HtmlInput::makeSubmitButton("uitvoeren"));
				$div->add(new HtmlParagraph("Dit type contractonderdeel is niet uitvoerbaar via de website, waarschijnlijk omdat het iets niet digitaals is of de mogelijkheid nog moet worden toegevoegd. Wel kan je het het contractonderdeel als 'uitgevoerd' markeren:" . $doenform));
				break;
		}
		$page = Page::getInstance();
		$page->start(_("Contractonderdeel &gt; Uitvoeren"))
			->add($div);
		return new PageResponse($page);
	}

	public static function ContractonderdeelMaakLatexVoorAJAX()
	{
		if(!hasAuth("actief"))
			spaceHTTP(403);

		$soort = requirepar("soort");
		//Maak het formulier en geef dit terug
		if(trypar("form")) {

			$aanmaakform = new HtmlForm();
			$aanmaakform->addClass('form-inline');
			$aanmaakform->setId("latexgenerateform");
			$aanmaakform->add($aanmaakdiv = new HtmlDiv());

			$aanmaakdiv->add(new HtmlHeader(3, _("Latex generator")))
				->add($table = new HtmlTable());
			$table->add($thead = new HtmlTableHead())
				  ->add($tbody = new HtmlTableBody());

//Misschien is deze form wel gewoon in zijn geheel ongelofelijk lelijk, omdat er geen wijzigTR gebruikt wordt.
			switch($soort) {
				case "PROFIEL":
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Er is niets in te stellen voor een bedrijfsprofiel.")) )) );
					break;
				case "LEZING":
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Wanneer wordt de lezing gegeven?")),
						new HtmlTableDataCell(HtmlInput::makeDate("uitvoerdatum", null, null, "lezingdatumid")) )) );
					break;
				case "MAILING":
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Wanneer wordt de mailing verstuurd?")),
						new HtmlTableDataCell(HtmlInput::makeDate("uitvoerdatum", null, null, "mailingdatumid")) )) );
					break;
				case "VACATURE":
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Hoe lang blij(ft)(ven) de vacature(s) geldig?")),
						new HtmlTableDataCell(HtmlInput::makeText("periode", null, null, null, "periodeid")) )) )
						->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Hoeveel vacatures zijn het?")),
						new HtmlTableDataCell(HtmlInput::makeNumber("aantal", "1", null, null, null, "aantalid")) )) );
					break;
				case "VAKID":
					global $COLJAREN, $MAANDEN;
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Geef het collegejaar aan waarin de vakid uit komt:")),
						new HtmlTableDataCell($coljaarbox = HtmlSelectbox::fromArray("coljaar", array_combine(array_values($COLJAREN),array_values($COLJAREN)), null, 1)) )) )
						->add(new HtmlTableRow(array(new HtmlTableDataCell(_("De maand waarin de vakid uit komt:")),
						new HtmlTableDataCell($editiemaandbox = HtmlSelectbox::fromArray("editiemaand", $MAANDEN, null, 1)) )) )
						->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Welk editienummer is dit:")),
						new HtmlTableDataCell($editie = HtmlSelectbox::fromArray("editie", array("1" => "1", "2" => "2", "3" => "3", "4" => "4", "5" => "5", "6" => "6"), null, 1 )) )) )
						->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Waar wordt de advertentie geplaatst:")),
						new HtmlTableDataCell($plaatsing = HtmlSelectbox::fromArray("plaatsing", array("binnenwerk" => "binnenwerk", "binnenvoor" => "binnen-voor", "achter" => "achterkant"), null, 1)) )) )
						->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Wat voor type is het:")),
						new HtmlTableDataCell($type = HtmlSelectbox::fromArray("type", array("advertentie" => "advertentie", "advertorial" => "advertorial"), null, 1)) )) )
						->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Geef het formaat aan:")),
						new HtmlTableDataCell($formaat = HtmlSelectbox::fromArray("formaat", array("enkel" => "enkel", "dubbel" => "dubbel"), null, 1)) )) );
					$coljaarbox->setid("coljaarid");
					$editiemaandbox->setid("editiemaandid");
					$editie->setid("editieid");
					$plaatsing->setid("plaatsingid");
					$type->setid("typeid");
					$formaat->setid("formaatid");
					break;
				case "OVERIG":
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Wat moet de sectietitel van dit onderdeel worden?")),
						new HtmlTableDataCell(HtmlInput::makeText("sectietitel", null, null, null, "sectietitelid")) )) );
					break;
				default:
					$table->add(new HtmlTableRow(array(new HtmlTableDataCell(_("Kies een soort...")) )) );
					break;
			}

			$aanmaakform->add(HtmlInput::makeHidden("soort", $soort));
			$aanmaakform->add(HtmlInput::makeButton("generate", _("Genereer latex!"), "generateLatex()"));
			$aanmaakform->add(HtmlInput::makeButton("zelfinvoeren", _("Zelf invoeren"), "zelfinvoerenfunc()"));

			echo $aanmaakform;

		//Genereer de daadwerkelijke latexstring
		} else if(trypar('generate')) {

			$bedrag = requirepar("bedrag");
			$latex = '';

			switch($soort) {
				case "PROFIEL":
					$latex = "\\aes zal het bedrijfsprofiel van \\spons plaatsen, yada yada yada...";
					break;
				case "LEZING":
					$date = requirepar("uitvoerdatum");
					$latex = "\\spons zal op $date een lezing aan de leden van \\aes geven.";
					break;
				case "MAILING":
					$date = requirepar("uitvoerdatum");
					$latex = "\\stagemailing{maand=$date, bedrag={$bedrag}}";
					break;
				case "VACATURE":
					$latex = "\\vacaturebank{periode=".requirepar('periode')
						. ", aantal=".requirepar('aantal')
						. ", bedrag={$bedrag}}"
						;
					break;
				case "VAKID":
					$latex = sprintf("\\vakid{collegejaar=".requirepar("coljaar")
						. ", editiemaand=".requirepar("editiemaand")
						. ", editie=".requirepar("editie")
						. ", plaatsing=".requirepar("plaatsing")
						. ", type=".requirepar("type")
						. ", formaat=".requirepar("formaat")
						. ", bedrag={$bedrag}}"
						);
					break;
				case "OVERIG":
					$sectietitel = requirepar("sectietitel");
					$latex = '\\section{'. $sectietitel.'} \\\\ Voer hier een omschrijving van het conractonderdeel in...';
					break;
				default:
					//Geen preset tekst.
					$latex = _("Vul hier de latex voor in het contractonderdeel in.");
			}

			echo $latex;
		}
		die;
	}

/**************
 * Interactie *
 *************/

	public static function interactieOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Interactieoverzichtpagina"));

		$interacties = InteractieQuery::metConstraints('alle', trypar('Medium'), null, null, null, Commissie::geef(trypar('Commissie')))->limit(150)->verzamel();

		$page->add(new HtmlHeader(3,_('Welkom op de spookweb 2 interactieoverzichtpagina!')))
			->add(new HtmlParagraph('Er zitten nu het volgende aantal interacties in je selectie uit de db van spookweb 2 van de laatste 3 maanden: '. $interacties->aantal()))
			->add(InteractieVerzamelingView::zoekForm())
			->add(InteractieVerzamelingView::htmlList($interacties, true));

		return new PageResponse($page);
	}

	public static function interactieEntry($args)
	{
		$interactie = Interactie::geef($args[0]);
		if (!$interactie) return false;

		return array( 'name' => $interactie->geefID()
					, 'displayName' => 'Interactie'
					, 'access' => $interactie->magBekijken());
	}

	public static function interactieDetails()
	{
		/** Laad het gekozen contract **/
		$id = (int) vfsVarEntryName();
		$interactie = Interactie::geef($id);

		if(!$interactie->magBekijken())
			spaceHTTP(403);

		/** Ga naar de view **/
		return new PageResponse(InteractieView::details($interactie));
	}

	public static function interactieNieuw()
	{
		$interactie = new Interactie();
		//Als we een bedrijf kunnen achterhalen doe dan slimme dingen.
		$bedrijf = Bedrijf::geef(vfsVarEntryName());
		if($bedrijf instanceof Bedrijf) {
			$interactie->setBedrijf($bedrijf);
			//als er nog geen spook of bedrijfscontact is helpen we even.
			/* TODO: in theorie zou dit moeten werken, maar de generated classes
				hebben nog wat bugs waardoor het niet goed om kan gaan met
				virtuele objecten (die dus nog niet in de db staan)
			$spook = new ContactPersoon(Persoon::getIngelogd(), $bedrijf);
			$spook->setFunctie("SPOOK");
			$algemeenContact = new Persoon();
			$algemeenContact->setAchternaam("Algemeen");
			$bedrijfscontact = new ContactPersoon($algemeenContact , $bedrijf);
			$bedrijfscontact->setFunctie('bedrijfscontact');
			$interactie->setSpook($spook);
			$interactie->setBedrijfscontact($bedrijfscontact);
			//+ en vergeet vervolgens niet om de twee nieuwe objecten ook
			//	op te slaan alvorens de interactie op te slaan!
			*/
		}
		return self::interactieWijzigen($interactie);
	}

	public static function interactieWijzigen($interactie = null)
	{
		/** Laad zo nodig de interactie **/
		if(!$interactie)
		{
			$id = (int) vfsVarEntryName();
			$interactie = Interactie::geef($id);
		}

		if(!$interactie->magBekijken())
			spaceHTTP(403);

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'InteractieWijzig')
		{
			if(!$interactie->getinDB()) {
				//Als het nu niet een actief spook is maak er dan een overigspookweb van.
				if(trypar('Interactie[Spook]') && (
						 !ContactPersoon::geef(trypar('Interactie[Spook]'))
					) ) {
					$spookidarray = explode('_', trypar('Interactie[Spook]'));
					$spook = new ContactPersoon($spookidarray[0], $spookidarray[1], "OVERIGSPOOKWEB");
					$spook->setBeginDatum(new DateTimeLocale());
					$spook->setEindDatum(new DateTimeLocale());
					ContactPersoon::stopInCache($spook->geefID(), $spook);
				//Als het object er wel is en in het verleden al is gestopt met contact zijn dan moeten we een nieuwe eindatum setten
				} elseif(trypar('Interactie[Spook]') && $spook = ContactPersoon::geef(trypar('Interactie[Spook]'))) {

					if($spook->getEindDatum()->hasTime() && $spook->getEindDatum() < new DateTimeLocale()) {
						$spook->setEindDatum(new DateTimeLocale());
					}
				}
				else
				{
					$spook = null;
				}
			}

			InteractieView::processForm($interactie);

			// We hebben 1 à 2 objecten om op te slaan, die moet(en allebei) geldig zijn.
			$geldig = $interactie->valid();
			if (!is_null($spook))
			{
				$geldig = $geldig && $spook->valid();
			}

			if ($geldig)
			{
				$interactie->opslaan();
				if (!is_null($spook))
				{
					$spook->opslaan();
				}
				Page::redirectMelding($interactie->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		return new PageResponse(InteractieView::wijzigen($interactie, $show_error));
	}

	public static function interactieVerwijderen($interactie = null)
	{
		/** Laad zo nodig de interactie **/
		if(!$interactie)
		{
			$id = (int) vfsVarEntryName();
			$interactie = Interactie::geef($id);
		}

		if(!$interactie->magWijzigen())
			spaceHTTP(403);

		if (Token::processNamedForm() == 'InteractieVerwijderen')
		{
			$bedrijf = $interactie->getBedrijf();
			$spook = $interactie->getSpook();
			$returnVal = $interactie->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(SPOOKBASE, sprintf(_("De interactie is verwijderd! Ga snel terug naar het %s of %s"),
					new HtmlAnchor($bedrijf->url(), _("bedrijf")), new HtmlAnchor($spook->url(), _("spook")) ));
			}
		}
		$page = Page::getInstance();
		$page->start(_("Verwijderen"))
			->add(InteractieView::verwijderForm($interactie));
			return new PageResponse($page);
	}

/**********
 * Spoken *
 **********/

	public static function spookOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Spookoverzichtpagina"));

		$spoken = ContactPersoonVerzameling::metConstraints(null, null, null, null, 'SPOOK');


		$page->add(new HtmlHeader(3,_('Welkom op de spookweb 2 spookoverzichtpagina!')))
			->add(new HtmlParagraph('Er zitten nu het volgende aantal spoken in je selectie uit de db van spookweb 2: '. $spoken->aantal()))
			->add(ContactPersoonVerzamelingView::htmlListSpookweb($spoken));

		return new PageResponse($page);
	}

	/**
	 * Een spook bestaat uit een verzameling aan contactpersonen die allemaal aan een persoon gekoppeld zijn.
	 **/
	public static function spookEntry($args)
	{
		$spook = ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $args[0], "SPOOK");
		$spook->union(ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $args[0], "OVERIGSPOOKWEB"));
		if ($spook->aantal() == 0) return false;

		return array( 'name' => $spook->first()->getPersoonContactID()
					, 'displayName' => 'Spook'
					, 'access' => $spook->first()->magBekijken());
	}

	/**
	 * Een specifieke spook relatie tussen een persoon en een bedrijf.
	 **/
	public static function spookSpecifiekEntry($args)
	{
		$spook = ContactPersoon::geef($args[1], $args[0], "SPOOK");
		if(!@$spook)
			$spook = ContactPersoon::geef($args[1], $args[0], "OVERIGSPOOKWEB");

		return array( 'name' => $args[0]
					, 'displayName' => 'Spook'
					, 'access' => $spook->magBekijken());
	}

	public static function spookDetails()
	{
		/** Laad het gekozen spook **/
		$id = (int) vfsVarEntryName();

		$spook = ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $id, "SPOOK");
		$spook->union(ContactPersoonVerzameling::metConstraints(null, null, 'ALLE', $id, "OVERIGSPOOKWEB"));

		if(!$spook->first()->magBekijken())
			spaceHTTP(403);

		$page = Page::getInstance();;
		$page->start();
		/** Ga naar de view **/
		$page->add(ContactPersoonVerzamelingView::spookDetails($spook));

		return new PageResponse($page);
	}

	/**
	 *	Een specifiek spook laten zien: een enkele contactpersoon uit de contactpersoonverzameling
	 *	die een spook representeert.
	 **/
	public static function spookSpecifiekDetails()
	{
		/** Laad het gekozen spook **/
		$ids = vfsVarEntryNames();
		$spook = ContactPersoon::geef($ids[1], $ids[0], "SPOOK");
		if(!@$spook)
			$spook = ContactPersoon::geef($ids[1], $ids[0], "OVERIGSPOOKWEB");

		if(!$spook->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		$page = ContactPersoonView::pageLinks($spook);
		$page->start(_("Details spook"));
		/** Ga naar de view **/
		$page->add(ContactPersoonView::details($spook));

		return new PageResponse($page);
	}

	public static function spookNieuw()
	{
		$bedrijf = Bedrijf::geef(vfsVarEntryName());
		$spook = new ContactPersoon(new Persoon(), $bedrijf, "SPOOK");
		$spook->setBeginDatum(new DateTimeLocale());
		return self::spookWijzigen($spook);
	}

	public static function spookWijzigen(ContactPersoon $spook = null)
	{
		if(!$spook) {
			/** Laad zo nodig het spook **/
			$ids = vfsVarEntryNames();
			$spook = ContactPersoon::geef($ids[1], $ids[0], "SPOOK");
			if(!@$spook)
				$spook = ContactPersoon::geef($ids[1], $ids[0], "OVERIGSPOOKWEB");
		}

		$show_error = false;
		//Als we in het nieuwformulier zojuist een spook hebben ingevuld dan moet dit ook in het object
		// worden verwerkt.
		// NOTE: je kan (nog) geen OVERIGSPOOKWEB handmatig toevoegen, die zijn altijd het gevolg van andere dingen.
		if(!$spook->getIndb())
		{
			$nieuwId = trypar("Persoon");
			if ($nieuwId) {
				$oudspook = clone($spook);
				$spook = new ContactPersoon(Persoon::geef($nieuwId), $oudspook->getOrganisatie(), "SPOOK");
				//TODO: uitzoeken waarom hij deze info niet uit het hidden veld in het formulier haalt.
			}
		}

		if(!$spook->magWijzigen())
			spaceHTTP(403);

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ContactPersoonWijzig')
		{
			ContactPersoonView::processWijzigForm($spook);
			if ($spook->valid())
			{
				$spook->opslaan();
				Page::redirectMelding($spook->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		ContactPersoonView::wijzig($spook, $show_error);
	}

	public static function spookVerwijderen(ContactPersoon $spook = null)
	{
		if(!$spook) {
			/** Laad zo nodig het spook **/
			$ids = vfsVarEntryNames();
			$spook = ContactPersoon::geef($ids[1], $ids[0], "SPOOK");
			if(!@$spook)
				$spook = ContactPersoon::geef($ids[1], $ids[0], "OVERIGSPOOKWEB");
		}

		if(!$spook->magWijzigen())
			spaceHTTP(403);

		//Zoek alle interacties van dit spook op
		$bedrijf = $spook->getOrganisatie();
		$interacties = InteractieVerzameling::metConstraints("alle", '', $spook->geefID(), '', $bedrijf->geefID());

		if (Token::processNamedForm() == 'ContactPersoonVerwijderen')
		{
			//Gooit ook de bijbehorende interacties weg!
			$returnVal = $spook->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(SPOOKBASE, sprintf(_("Het spook is verwijderd! Ga terug naar het %s"),
					new HtmlAnchor($bedrijf->url(), _("bedrijf")) ));
			}
		}

		$page = Page::getInstance();
		$page->start(_("Spook &gt; Verwijderen"))
			->add(ContactPersoonView::verwijderForm($spook))
			->add(new HtmlParagraph(sprintf(_('Let op: je verwijdert ook meteen de %s bijbehorende interacties!'), $interacties->aantal()) ));
		return new PageResponse($page);

	}

/**************
 *  Vacatures *
 *************/

	public static function vacatureOverzicht()
	{

		$page = Page::getInstance();
		$page->start(_("Vacaturebank"));

		$vacatures = VacatureVerzameling::metConstraints((trypar('oud') && hasAuth('spocie')?'alle':'huidig'), null, trypar('type'), trypar('ica'), trypar('iku'), trypar('na'), trypar('wis'));
		$vacatures->shuffle();

		$page->add(new HtmlHeader(2, "Vacatures"))
			->add(VacatureVerzamelingView::vacatureSelectieForm())
			->add(hasAuth('spocie')?new HtmlParagraph('SpoCie toptip: je kan een nieuwe vacature toevoegen via een contractonderdeel van het betreffende bedrijf.'):'')
			->add(new HtmlHR())
			->add(VacatureVerzamelingView::vacaturebanklijst($vacatures));

		return new PageResponse($page);
	}

	public static function vacatureEntry($args)
	{
		$vacature = Vacature::geef($args[0]);
		if (!$vacature) return false;

		return array( 'name' => $vacature->geefID()
					, 'displayName' => $vacature->getTitel()
					, 'access' => $vacature->magBekijken());
	}

	public static function vacatureDetails()
	{
		$id = (int) vfsVarEntryName();
		$vacature = Vacature::geef($id);

		$page = VacatureView::pageLinks($vacature);
		$page->start(VacatureView::waardeTitel($vacature));
		$page->add(VacatureView::details($vacature));
		$page->add(new HtmlHR());
		$page->add(new HtmlAnchor("..", _("Terug naar de vacaturebank")));
		$vacature->viewed();
		return new PageResponse($page);
	}

	public static function vacatureNieuw($contractonderdeel = null)
	{
		$vacature = new Vacature();
		//Laat een vacature default 3 maanden lopen
		$now = new DateTimeLocale();
		$vacature->setDatumVerloop($now->add(new DateInterval("P3M")));
		if($contractonderdeel)
			$vacature->setContractonderdeel($contractonderdeel);
		return self::vacatureWijzigen($vacature);
	}

	//Bewust geen mogelijkheid voor nieuwe op dit moment, dit moet via een contract of bedrijf gaan.
	public static function vacatureWijzigen(Vacature $vacature = null)
	{
		if(!$vacature) {
			/** Laad zo nodig de vacature **/
			$id = (int) vfsVarEntryName();
			$vacature = Vacature::geef($id);
		}

		if(!$vacature->magWijzigen())
			spaceHTTP(403);

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'vacatureWijzig')
		{
			VacatureView::processForm($vacature);
			if ($vacature->valid())
			{
				$vacature->opslaan();
				//Set zo nodig het id van de vacature bij het contractonderdeel
				$contractonderdeel = $vacature->getContractonderdeel();
				if(!$contractonderdeel->getUitgevoerd()) {
					$contractonderdeel->setUitgevoerd($vacature->geefID());
					$contractonderdeel->opslaan();
				}
				Page::redirectMelding('/Service/Spookweb2/Vacature/', _("De gegevens zijn opgeslagen!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		return new PageResponse(VacatureView::wijzigen($vacature, $show_error));
	}

	public static function vacatureVerwijderen(Vacature $vacature = null)
	{
		if(!$vacature) {
			/** Laad zo nodig de vacature **/
			$id = (int) vfsVarEntryName();
			$vacature = Vacature::geef($id);
		}

		if(!$vacature->magVerwijderen())
		{
			return responseUitStatusCode(403);
		}

		if (Token::processNamedForm() == 'VacatureVerwijderen')
		{
			$contractonderdeel = $vacature->getContractonderdeel();
			$returnVal = $vacature->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				$contractonderdeel->setUitgevoerd(false);
				$contractonderdeel->opslaan();
				Page::redirectMelding(SPOOKBASE, sprintf(_("De vacature is verwijderd! Ga terug naar het %s"),
					new HtmlAnchor($contractonderdeel->getContract()->url(), _("contract")) ));
			}
		}

		$page = VacatureView::verwijderen($vacature);
		$page->start(_("Vacature &gt; Verwijderen"))
			->add(VacatureView::verwijderForm($vacature));
		return new PageResponse($page);
	}
}
