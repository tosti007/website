<?
/**
 * $Id$
 */
class VacatureVerzameling
	extends VacatureVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VacatureVerzameling_Generated
	}

	static public function metConstraints($selectie = 'huidig', $contractonderdeelid = null, $type = null, $ica = null, $iku = null, $na = null, $wis = null)
	{
		switch($selectie)
		{
			case 'oud':
				$selectieSQL = ' AND `datumVerloop` < NOW()';
				break;
			case 'bijna':
				//Geef wat extra respijt door ook maandje geleden te kijken.
				$selectieSQL = ' AND `datumVerloop` > DATE_SUB(NOW(), INTERVAL 1 MONTH)
								AND `datumVerloop` < DATE_ADD(NOW(), INTERVAL 3 MONTH)';
				break;
			case 'alle':
				$selectieSQL = '';
				break;
			case 'huidig':
			default:
				$selectieSQL = ' AND `datumVerloop` >= NOW()';
				break;
		}

		$studies = array();
		($ica?$studies[] = '`ica` = 1':'');
		($iku?$studies[] = '`iku` = 1':'');
		($na?$studies[] = '`na` = 1':'');
		($wis?$studies[] = '`wis` = 1':'');
		$studiesSQL = implode(' OR ', $studies);

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `id`'
						. ' FROM `Vacature`'
						. ' WHERE 1'
						. $selectieSQL
						. ($contractonderdeelid?' AND `contractonderdeel_id` = %i':' %_')
						. ($type?' AND `type` = %s':' %_')
						. ($ica || $iku || $na || $wis?" AND ($studiesSQL)":' ')
						, $contractonderdeelid, $type );

		return self::verzamel($ids);
	}

	/**
	 *  Sorteringen
	 **/

	function sorteerOpDatum($aID, $bID)
	{
		$a = Vacature::geef($aID)->getDatumVerloop();
		$b = Vacature::geef($bID)->getDatumVerloop();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}

	static public function getRandom($aantal = 4)
	{
		$ids = VacatureQuery::table()
			->whereProp('datumVerloop', '>', new DateTimeLocale())
			->select('id')
			->setFetchType('COLUMN')
			->get();

		shuffle($ids);
		$ids = array_slice($ids, 0, $aantal);

		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
