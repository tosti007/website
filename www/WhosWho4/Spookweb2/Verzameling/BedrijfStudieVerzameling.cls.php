<?
/**
 * $Id$
 */
class BedrijfStudieVerzameling
	extends BedrijfStudieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BedrijfStudieVerzameling_Generated
	}

	public static function vanBedrijf($bedrijf)
	{
		global $WSW4DB;

		if(is_numeric($bedrijf))
			$bedrijf = Bedrijf::geef($bedrijf);

		$rows = $WSW4DB->q('TABLE SELECT `Studie_studieID` as studieid, `Bedrijf_contactID` as bedrijfid'
						. ' FROM `BedrijfStudie`'
						. ' WHERE 1'
						. ' AND `Bedrijf_contactID` = %i'
						, $bedrijf->geefID());

		$ids = array();
		foreach($rows as $row) {
			$ids[] = array($row['studieid'], $row['bedrijfid']);
		}

		return self::verzamel($ids);

	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
