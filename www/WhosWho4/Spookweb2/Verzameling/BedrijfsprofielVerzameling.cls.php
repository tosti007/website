<?
/**
 * $Id$
 */
class BedrijfsprofielVerzameling
	extends BedrijfsprofielVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BedrijfsprofielVerzameling_Generated
	}

	static public function metConstraints($selectie = 'huidig', $contractonderdeelid = null)
	{
		switch($selectie)
		{
			case 'oud':
				$selectieSQL = ' AND `datumVerloop` < NOW()';
				break;
			case 'alle':
				$selectieSQL = '';
				break;
			case 'huidig':
			default:
				$selectieSQL = ' AND `datumVerloop` >= NOW()';
				break;
		}

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `id`'
						. ' FROM `Bedrijfsprofiel`'
						. ' WHERE 1'
						. $selectieSQL
						.($contractonderdeelid?' AND `contractonderdeel_id` = %i':'%_')
						, $contractonderdeelid);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
