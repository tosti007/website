<?
/**
 * $Id$
 */
class ContractVerzameling
	extends ContractVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ContractVerzameling_Generated
	}

	static public function metConstraints($bedrijfid = null, $status = null, $vanaf = null,Array $betrokkenenids = array())
	{
		if($status == 'alle')
			$status = null;

		//Is het een nette datetimelocale, maak het dan SQLbaar.
		//Is het een aantal, zorg dan dat we $vanaf gebruiken als limiet.
		if($vanaf instanceof DateTimeLocale || $vanaf instanceof DateTime)
		{
			$vanaf = $vanaf->format('Y-m-d');
		}

		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `contractID`'
						. ' FROM `Contract`'
						. ' WHERE 1'
						. ($bedrijfid?' AND `bedrijf_contactID` = %i':' %_')
						. ($status?' AND `status` = %s':' %_')
						. ($vanaf?' AND `ingangsdatum` >= %s':' %_')
						. (empty($betrokkenenids)?' %_': ' AND `contractID` IN (
							SELECT `contract_contractID` FROM `Contractonderdeel` WHERE
								`verantwoordelijke_contactID` IN %Ai )')
						, $bedrijfid, $status, $vanaf, $betrokkenenids);

		return self::verzamel($ids);
	}

	public function sorteerOpId($idA, $idB)
	{
		if($idA > $idB)
			return 1;
		if($idA < $idB)
			return -1;

		return 0;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
