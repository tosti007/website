<?
abstract class InteractieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de InteractieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze InteractieVerzameling een ContactPersoonVerzameling.
	 *
	 * @return ContactPersoonVerzameling
	 * Een ContactPersoonVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze InteractieVerzameling.
	 */
	public function toSpookVerzameling()
	{
		if($this->aantal() == 0)
			return new ContactPersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getSpookcontactID()
			                      ,$obj->getSpookcontactID()
			                      ,$obj->getSpookType()
			                      );
		}
		$this->positie = $origPositie;
		return ContactPersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze InteractieVerzameling een BedrijfVerzameling.
	 *
	 * @return BedrijfVerzameling
	 * Een BedrijfVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze InteractieVerzameling.
	 */
	public function toBedrijfVerzameling()
	{
		if($this->aantal() == 0)
			return new BedrijfVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBedrijfContactID()
			                      );
		}
		$this->positie = $origPositie;
		return BedrijfVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze InteractieVerzameling een ContactPersoonVerzameling.
	 *
	 * @return ContactPersoonVerzameling
	 * Een ContactPersoonVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze InteractieVerzameling.
	 */
	public function toBedrijfPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new ContactPersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getBedrijfPersooncontactID()) || is_null($obj->getBedrijfPersooncontactID()) || is_null($obj->getBedrijfPersoonType())) {
				continue;
			}

			$foreignkeys[] = array($obj->getBedrijfPersooncontactID()
			                      ,$obj->getBedrijfPersooncontactID()
			                      ,$obj->getBedrijfPersoonType()
			                      );
		}
		$this->positie = $origPositie;
		return ContactPersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze InteractieVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze InteractieVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCommissieCommissieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een InteractieVerzameling van Spook.
	 *
	 * @return InteractieVerzameling
	 * Een InteractieVerzameling die elementen bevat die bij de Spook hoort.
	 */
	static public function fromSpook($spook)
	{
		if(!isset($spook))
			return new InteractieVerzameling();

		return InteractieQuery::table()
			->whereProp('Spook', $spook)
			->verzamel();
	}
	/**
	 * @brief Maak een InteractieVerzameling van Bedrijf.
	 *
	 * @return InteractieVerzameling
	 * Een InteractieVerzameling die elementen bevat die bij de Bedrijf hoort.
	 */
	static public function fromBedrijf($bedrijf)
	{
		if(!isset($bedrijf))
			return new InteractieVerzameling();

		return InteractieQuery::table()
			->whereProp('Bedrijf', $bedrijf)
			->verzamel();
	}
	/**
	 * @brief Maak een InteractieVerzameling van BedrijfPersoon.
	 *
	 * @return InteractieVerzameling
	 * Een InteractieVerzameling die elementen bevat die bij de BedrijfPersoon hoort.
	 */
	static public function fromBedrijfPersoon($bedrijfPersoon)
	{
		if(!isset($bedrijfPersoon))
			return new InteractieVerzameling();

		return InteractieQuery::table()
			->whereProp('BedrijfPersoon', $bedrijfPersoon)
			->verzamel();
	}
	/**
	 * @brief Maak een InteractieVerzameling van Commissie.
	 *
	 * @return InteractieVerzameling
	 * Een InteractieVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new InteractieVerzameling();

		return InteractieQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
}
