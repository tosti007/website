<?
abstract class VacatureVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de VacatureVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze VacatureVerzameling een ContractonderdeelVerzameling.
	 *
	 * @return ContractonderdeelVerzameling
	 * Een ContractonderdeelVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze VacatureVerzameling.
	 */
	public function toContractonderdeelVerzameling()
	{
		if($this->aantal() == 0)
			return new ContractonderdeelVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getContractonderdeelId()
			                      );
		}
		$this->positie = $origPositie;
		return ContractonderdeelVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VacatureVerzameling van Contractonderdeel.
	 *
	 * @return VacatureVerzameling
	 * Een VacatureVerzameling die elementen bevat die bij de Contractonderdeel hoort.
	 */
	static public function fromContractonderdeel($contractonderdeel)
	{
		if(!isset($contractonderdeel))
			return new VacatureVerzameling();

		return VacatureQuery::table()
			->whereProp('Contractonderdeel', $contractonderdeel)
			->verzamel();
	}
}
