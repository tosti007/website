<?
abstract class ContractVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de ContractVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze ContractVerzameling een BedrijfVerzameling.
	 *
	 * @return BedrijfVerzameling
	 * Een BedrijfVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze ContractVerzameling.
	 */
	public function toBedrijfVerzameling()
	{
		if($this->aantal() == 0)
			return new BedrijfVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getBedrijfContactID()
			                      );
		}
		$this->positie = $origPositie;
		return BedrijfVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een ContractVerzameling van Bedrijf.
	 *
	 * @return ContractVerzameling
	 * Een ContractVerzameling die elementen bevat die bij de Bedrijf hoort.
	 */
	static public function fromBedrijf($bedrijf)
	{
		if(!isset($bedrijf))
			return new ContractVerzameling();

		return ContractQuery::table()
			->whereProp('Bedrijf', $bedrijf)
			->verzamel();
	}
}
