<?
/**
 * $Id$
 */
class BedrijfVerzameling
	extends BedrijfVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BedrijfVerzameling_Generated
	}

	public static function metConstraints($naam = null, $type = null, $status = null)
	{
		//Voorkom injecties
		if($type && !in_array($type, Bedrijf::enumsType())) {
			return new self;
		}

		if($status && !in_array($status, Bedrijf::enumsStatus())) {
			return new self;
		}

		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `contactID` FROM `Bedrijf` '
						.' LEFT JOIN `Organisatie` USING (`contactID`)'
						.' WHERE 1'
						.($naam?' AND `Organisatie`.`naam` LIKE %c':'%_')
						.($status?' AND `Bedrijf`.`status` = %s':'%_')
						.($type?' AND `Bedrijf`.`type` = %s':'%_')
						, $naam, $status, $type);

		return BedrijfVerzameling::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
