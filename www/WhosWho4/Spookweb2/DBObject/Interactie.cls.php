<?
/**
 * $Id$
 *
 *  Betere naam verzinnen >_>
 */
class Interactie
	extends Interactie_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Interactie_Generated

		$this->id = NULL;
	}

	public function magVerwijderen ()
	{
		return hasAuth("spookweb");
	}

	public function url()
	{
		return SPOOKBASE . 'Interactie/' . $this->geefID();
	}

	/**
	 *  Als we een gecombineerde id van een contactPersoon krijgen dan moeten we deze even opzoeken.
	 **/
	/**
	 *  FIXME: workaround voor bug #5688, als die gefixxed is kan dit letterlijk weg!
	 **/
	public function setSpook($new_persoon, $new_persoon_contactID = NULL, $new_organisatie = NULL, $new_organisatie_contactID = NULL, $new_type = NULL)
	{
		unset($this->errors['spook']);
		if(is_string($new_persoon)
		&& is_null($new_persoon_contactID)
		&& is_null($new_organisatie)
		&& is_null($new_organisatie_contactID)
		&& is_null($new_type)
		) {
			//$new_persoon = ContactPersoon::geef($new_persoon);
			//TODO: workaround zolang bug #5325 nog niet is gefixxed.
			$this->spook = ContactPersoon::geef($new_persoon);
			$this->spook_contactID
					= $this->spook->getPersoonContactID();
			$this->spook_contactID2
					= $this->spook->getOrganisatieContactID();
			$this->spook_type
					= $this->spook->getType();
			return $this;
			//-
		} 
		parent::setSpook($new_persoon, $new_persoon_contactID, $new_organisatie, $new_organisatie_contactID, $new_type);
	}

	public function setBedrijfPersoon($new_persoon, $new_persoon_contactID = NULL, $new_organisatie = NULL, $new_organisatie_contactID = NULL, $new_type = NULL)
	{
		unset($this->errors['bedrijfPersoon']);
		//Legacy null compatibility... TODO?
		if($new_persoon == 0)
			return $this;

		if(is_string($new_persoon)
		&& is_null($new_persoon_contactID)
		&& is_null($new_organisatie)
		&& is_null($new_organisatie_contactID)
		&& is_null($new_type)
		) {
			$this->bedrijfPersoon = ContactPersoon::geef($new_persoon);
			$this->bedrijfPersoon_contactID
					= $this->bedrijfPersoon->getPersoonContactID();
			$this->bedrijfPersoon_contactID2
					= $this->bedrijfPersoon->getOrganisatieContactID();
			$this->bedrijfPersoon_type
					= $this->bedrijfPersoon->getType();
			//debug_dump($this);
			return $this;
			//-
		}
		parent::setBedrijfPersoon($new_persoon, $new_persoon_contactID, $new_organisatie, $new_organisatie_contactID, $new_type);
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
