<?
/**
 * $Id$
 */
class Vacature
	extends Vacature_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Vacature_Generated

		$this->id = NULL;
	}

	public function magVerwijderen()
	{
		return hasAuth("spocie");
	}

	public function url()
	{
		return SPOOKBASE . 'Vacature/' . $this->geefID();
	}

	public function getBedrijf()
	{
		return $this->getContractonderdeel()->getContract()->getBedrijf();
	}

	public function viewed()
	{
		$this->setViews($this->getViews() + 1);
		$this->opslaan();
	}

	/**
	 *  Heeft deze vacature een inleiding voor de huidige taal?
	 *
	 * @param lang Zo niet NULL, de taal waarop gekeken wordt.
	 * @return True als ->getInleiding($lang) iets oplevert, anders False.
	 */
	public function heeftInleiding($lang = NULL)
	{
		return !is_null($this->getInleiding($lang));
	}
	/**
	 *  Heeft deze vacature inhoud voor de huidige taal?
	 *
	 * @param lang Zo niet NULL, de taal waarop gekeken wordt.
	 * @return True als ->getInhoud($lang) iets oplevert, anders False.
	 */
	public function heeftInhoud($lang = NULL)
	{
		return !is_null($this->getInhoud($lang));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
