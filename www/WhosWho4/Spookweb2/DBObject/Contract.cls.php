<?
/**
 * $Id$
 */
class Contract
	extends Contract_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Contract_Generated

		$this->id = NULL;
	}

	public function magVerwijderen()
	{
		return hasAuth("spookweb");
	}

	public function url()
	{
		return SPOOKBASE . 'Contract/' . $this->geefID();
	}

	//Als dit contract "late" is geef dan "late" terug. "late" betekend dat er
	// echt eens iets met dit contract moet gebeuren.
	public function late()
	{
		$now = new DateTimeLocale();
		global $CONTRACTLATE;

		if(array_key_exists($this->getStatus(), $CONTRACTLATE))
		{
			if($this->getIngangsdatum() < $now->sub($CONTRACTLATE[$this->getStatus()]))
						return "late";
		}
		return null;
	}

	// Retourneer het totale bedrag, excl korting. TODO: kortingen
	public function totaalbedrag($subtotaal = false, $btw = true)
	{
		$onderdelen = ContractonderdeelVerzameling::metConstraints($this->geefID());

		$totaal = 0;
		foreach($onderdelen as $onderdeel)
		{
			$totaal += $onderdeel->getBedrag();
		}

		//We gaan er nu van uit dat btw voor korting is, maar hier moet de spocie nog uitsluitsel over geven.
		if(!$subtotaal)
		{
			if($btw)
				$totaal *= 1 + BTWPERCENTAGE/100;
			$korting = ContractView::waardeKorting($this);
			if(is_numeric($korting))
				$totaal -= $korting;
			if(substr($korting, -1) == '%'){
				$totaal -= $totaal * (substr($korting, 0, -1) / 100);
			}
		}

		if($subtotaal && $btw)
			$totaal *= BTWPERCENTAGE/100;

		return $totaal;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
