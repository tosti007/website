<?
/**
 * $Id$
 */
class Bedrijfsprofiel
	extends Bedrijfsprofiel_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Bedrijfsprofiel_Generated

		$this->id = NULL;
	}

	public function magVerwijderen()
	{
		return hasAuth('spocie');
	}

	public function url()
	{
		return SPOOKBASE . 'Bedrijfsprofiel/'. $this->geefID();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
