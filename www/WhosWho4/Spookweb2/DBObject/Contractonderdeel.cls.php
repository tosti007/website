<?
/**
 * $Id$
 */
class Contractonderdeel
	extends Contractonderdeel_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Contractonderdeel_Generated

		$this->id = NULL;
	}

	public function magVerwijderen ()
	{
		return hasAuth("spookweb");
	}

	public function url()
	{
		return SPOOKBASE . 'Contract/' . $this->getContract()->geefID() .'/'. $this->geefID();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
