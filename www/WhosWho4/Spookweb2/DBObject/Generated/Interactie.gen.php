<?
/**
 * @brief 'AT'AUTH_GET:spookweb,AUTH_SET:spookweb
 */
abstract class Interactie_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $spook;
	protected $spook_contactID;			/**< \brief PRIMARY */
	protected $spook_contactID2;		/**< \brief PRIMARY */
	protected $spook_type;				/**< \brief PRIMARY, ENUM:NORMAAL/SPOOK/BEDRIJFSCONTACT/OVERIGSPOOKWEB/ALUMNUS */
	protected $bedrijf;
	protected $bedrijf_contactID;		/**< \brief PRIMARY */
	protected $bedrijfPersoon;			/**< \brief NULL */
	protected $bedrijfPersoon_contactID;/**< \brief PRIMARY */
	protected $bedrijfPersoon_contactID2;/**< \brief PRIMARY */
	protected $bedrijfPersoon_type;		/**< \brief PRIMARY, ENUM:NORMAAL/SPOOK/BEDRIJFSCONTACT/OVERIGSPOOKWEB/ALUMNUS */
	protected $commissie;				/**< \brief NULL */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $datum;
	protected $inhoud;
	protected $medium;					/**< \brief ENUM:EMAIL/FAX/GESPREK/NOTITIE/ONBEKEND/OVERIG/POST/TELEFOON */
	protected $todo;
	/**
	 * @brief De constructor van de Interactie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->spook = NULL;
		$this->spook_contactID = 0;
		$this->spook_contactID2 = 0;
		$this->spook_type = 'NORMAAL';
		$this->bedrijf = NULL;
		$this->bedrijf_contactID = 0;
		$this->bedrijfPersoon = NULL;
		$this->bedrijfPersoon_contactID = NULL;
		$this->bedrijfPersoon_contactID2 = NULL;
		$this->bedrijfPersoon_type = NULL;
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->datum = new DateTimeLocale();
		$this->inhoud = '';
		$this->medium = 'EMAIL';
		$this->todo = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld spook.
	 *
	 * @return ContactPersoon
	 * De waarde van het veld spook.
	 */
	public function getSpook()
	{
		if(!isset($this->spook)
		 && isset($this->spook_contactID)
		 && isset($this->spook_contactID2)
		 && isset($this->spook_type)
		 ) {
			$this->spook = ContactPersoon::geef
					( $this->spook_contactID
					, $this->spook_contactID2
					, $this->spook_type
					);
		}
		return $this->spook;
	}
	/**
	 * @brief Stel de waarde van het veld spook in.
	 *
	 * @param mixed $new_persoon De nieuwe waarde.
	 * @param mixed $new_persoon_contactID De nieuwe waarde.
	 * @param mixed $new_organisatie De nieuwe waarde.
	 * @param mixed $new_organisatie_contactID De nieuwe waarde.
	 * @param mixed $new_type De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setSpook($new_persoon, $new_persoon_contactID = NULL, $new_organisatie = NULL, $new_organisatie_contactID = NULL, $new_type = NULL)
	{
		unset($this->errors['Spook']);
		if($new_persoon instanceof ContactPersoon
		&& is_null($new_persoon_contactID)
		&& is_null($new_organisatie)
		&& is_null($new_organisatie_contactID)
		&& is_null($new_type)
		) {
			if($this->spook == $new_persoon
			&& $this->spook_contactID == $this->spook->getPersoonContactID()
			&& $this->spook_contactID2 == $this->spook->getOrganisatieContactID()
			&& $this->spook_type == $this->spook->getType())
				return $this;
			$this->spook = $new_persoon;
			$this->spook_contactID
					= $this->spook->getPersoonContactID();
			$this->spook_contactID2
					= $this->spook->getOrganisatieContactID();
			$this->spook_type
					= $this->spook->getType();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_persoon)
		&& isset($new_persoon_contactID)
		&& isset($new_organisatie)
		&& isset($new_organisatie_contactID)
		&& isset($new_type)
		) {
			if($this->spook == NULL 
				&& $this->spook_contactID == (int)$new_persoon_contactID 
				&& $this->spook_contactID2 == $new_organisatie_contactID 
				&& $this->spook_type == $new_type)
				return $this;
			$this->spook = NULL;
			$this->spook_contactID
					= (int)$new_persoon_contactID;
			$this->spook_contactID2
					= $new_organisatie_contactID;
			$this->spook_type
					= $new_type;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld spook geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld spook geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSpook()
	{
		if (array_key_exists('Spook', $this->errors))
			return $this->errors['Spook'];
		$waarde1 = $this->getSpook();
		$waarde2 = $this->getSpookContactID();
		$waarde3 = $this->getSpookContactID2();
		$waarde4 = $this->getSpookType();
		if(empty($waarde1)
			&& (empty($waarde2)
				|| empty($waarde3)
				|| empty($waarde4)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld spook_contactID.
	 *
	 * @return int
	 * De waarde van het veld spook_contactID.
	 */
	public function getSpookContactID()
	{
		if (is_null($this->spook_contactID) && isset($this->spook)) {
			$this->spook_contactID = $this->spook->getContactID();
		}
		return $this->spook_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld spook_contactID2.
	 *
	 * @return int
	 * De waarde van het veld spook_contactID2.
	 */
	public function getSpookContactID2()
	{
		if (is_null($this->spook_contactID2) && isset($this->spook)) {
			$this->spook_contactID2 = $this->spook->getContactID2();
		}
		return $this->spook_contactID2;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld spook_type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld spook_type.
	 */
	static public function enumsSpook_type()
	{
		static $vals = array('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld spook_type.
	 *
	 * @return string
	 * De waarde van het veld spook_type.
	 */
	public function getSpookType()
	{
		if (is_null($this->spook_type) && isset($this->spook)) {
			$this->spook_type = $this->spook->getType();
		}
		return $this->spook_type;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijf.
	 *
	 * @return Bedrijf
	 * De waarde van het veld bedrijf.
	 */
	public function getBedrijf()
	{
		if(!isset($this->bedrijf)
		 && isset($this->bedrijf_contactID)
		 ) {
			$this->bedrijf = Bedrijf::geef
					( $this->bedrijf_contactID
					);
		}
		return $this->bedrijf;
	}
	/**
	 * @brief Stel de waarde van het veld bedrijf in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setBedrijf($new_contactID)
	{
		unset($this->errors['Bedrijf']);
		if($new_contactID instanceof Bedrijf
		) {
			if($this->bedrijf == $new_contactID
			&& $this->bedrijf_contactID == $this->bedrijf->getContactID())
				return $this;
			$this->bedrijf = $new_contactID;
			$this->bedrijf_contactID
					= $this->bedrijf->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->bedrijf == NULL 
				&& $this->bedrijf_contactID == (int)$new_contactID)
				return $this;
			$this->bedrijf = NULL;
			$this->bedrijf_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bedrijf geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bedrijf geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBedrijf()
	{
		if (array_key_exists('Bedrijf', $this->errors))
			return $this->errors['Bedrijf'];
		$waarde1 = $this->getBedrijf();
		$waarde2 = $this->getBedrijfContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijf_contactID.
	 *
	 * @return int
	 * De waarde van het veld bedrijf_contactID.
	 */
	public function getBedrijfContactID()
	{
		if (is_null($this->bedrijf_contactID) && isset($this->bedrijf)) {
			$this->bedrijf_contactID = $this->bedrijf->getContactID();
		}
		return $this->bedrijf_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijfPersoon.
	 *
	 * @return ContactPersoon
	 * De waarde van het veld bedrijfPersoon.
	 */
	public function getBedrijfPersoon()
	{
		if(!isset($this->bedrijfPersoon)
		 && isset($this->bedrijfPersoon_contactID)
		 && isset($this->bedrijfPersoon_contactID2)
		 && isset($this->bedrijfPersoon_type)
		 ) {
			$this->bedrijfPersoon = ContactPersoon::geef
					( $this->bedrijfPersoon_contactID
					, $this->bedrijfPersoon_contactID2
					, $this->bedrijfPersoon_type
					);
		}
		return $this->bedrijfPersoon;
	}
	/**
	 * @brief Stel de waarde van het veld bedrijfPersoon in.
	 *
	 * @param mixed $new_persoon De nieuwe waarde.
	 * @param mixed $new_persoon_contactID De nieuwe waarde.
	 * @param mixed $new_organisatie De nieuwe waarde.
	 * @param mixed $new_organisatie_contactID De nieuwe waarde.
	 * @param mixed $new_type De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setBedrijfPersoon($new_persoon, $new_persoon_contactID = NULL, $new_organisatie = NULL, $new_organisatie_contactID = NULL, $new_type = NULL)
	{
		unset($this->errors['BedrijfPersoon']);
		if($new_persoon instanceof ContactPersoon
		&& is_null($new_persoon_contactID)
		&& is_null($new_organisatie)
		&& is_null($new_organisatie_contactID)
		&& is_null($new_type)
		) {
			if($this->bedrijfPersoon == $new_persoon
			&& $this->bedrijfPersoon_contactID == $this->bedrijfPersoon->getPersoonContactID()
			&& $this->bedrijfPersoon_contactID2 == $this->bedrijfPersoon->getOrganisatieContactID()
			&& $this->bedrijfPersoon_type == $this->bedrijfPersoon->getType())
				return $this;
			$this->bedrijfPersoon = $new_persoon;
			$this->bedrijfPersoon_contactID
					= $this->bedrijfPersoon->getPersoonContactID();
			$this->bedrijfPersoon_contactID2
					= $this->bedrijfPersoon->getOrganisatieContactID();
			$this->bedrijfPersoon_type
					= $this->bedrijfPersoon->getType();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_persoon) || $new_persoon == 0) && (is_null($new_persoon_contactID) || $new_persoon_contactID == 0) && (is_null($new_organisatie) || $new_organisatie == 0) && (is_null($new_organisatie_contactID) || $new_organisatie_contactID == 0) && (is_null($new_type) || $new_type == 0)) {
			if($this->bedrijfPersoon == NULL && $this->bedrijfPersoon_persoon == NULL && persoon_contactID == NULL && organisatie == NULL && organisatie_contactID == NULL && type == NULL)
				return $this;
			$this->bedrijfPersoon = NULL;
			$this->bedrijfPersoon_persoon = NULL;
			$this->bedrijfPersoon_persoon_contactID = NULL;
			$this->bedrijfPersoon_organisatie = NULL;
			$this->bedrijfPersoon_organisatie_contactID = NULL;
			$this->bedrijfPersoon_type = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_persoon)
		&& isset($new_persoon_contactID)
		&& isset($new_organisatie)
		&& isset($new_organisatie_contactID)
		&& isset($new_type)
		) {
			if($this->bedrijfPersoon == NULL 
				&& $this->bedrijfPersoon_contactID == (int)$new_persoon_contactID 
				&& $this->bedrijfPersoon_contactID2 == $new_organisatie_contactID 
				&& $this->bedrijfPersoon_type == $new_type)
				return $this;
			$this->bedrijfPersoon = NULL;
			$this->bedrijfPersoon_contactID
					= (int)$new_persoon_contactID;
			$this->bedrijfPersoon_contactID2
					= $new_organisatie_contactID;
			$this->bedrijfPersoon_type
					= $new_type;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bedrijfPersoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bedrijfPersoon geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBedrijfPersoon()
	{
		if (array_key_exists('BedrijfPersoon', $this->errors))
			return $this->errors['BedrijfPersoon'];
		$waarde1 = $this->getBedrijfPersoon();
		$waarde2 = $this->getBedrijfPersoonContactID();
		$waarde3 = $this->getBedrijfPersoonContactID2();
		$waarde4 = $this->getBedrijfPersoonType();
		if(empty($waarde1)
			&& (empty($waarde2)
				|| empty($waarde3)
				|| empty($waarde4)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijfPersoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld bedrijfPersoon_contactID.
	 */
	public function getBedrijfPersoonContactID()
	{
		if (is_null($this->bedrijfPersoon_contactID) && isset($this->bedrijfPersoon)) {
			$this->bedrijfPersoon_contactID = $this->bedrijfPersoon->getContactID();
		}
		return $this->bedrijfPersoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijfPersoon_contactID2.
	 *
	 * @return int
	 * De waarde van het veld bedrijfPersoon_contactID2.
	 */
	public function getBedrijfPersoonContactID2()
	{
		if (is_null($this->bedrijfPersoon_contactID2) && isset($this->bedrijfPersoon)) {
			$this->bedrijfPersoon_contactID2 = $this->bedrijfPersoon->getContactID2();
		}
		return $this->bedrijfPersoon_contactID2;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld bedrijfPersoon_type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld bedrijfPersoon_type.
	 */
	static public function enumsBedrijfPersoon_type()
	{
		static $vals = array('NORMAAL','SPOOK','BEDRIJFSCONTACT','OVERIGSPOOKWEB','ALUMNUS','');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijfPersoon_type.
	 *
	 * @return string
	 * De waarde van het veld bedrijfPersoon_type.
	 */
	public function getBedrijfPersoonType()
	{
		if (is_null($this->bedrijfPersoon_type) && isset($this->bedrijfPersoon)) {
			$this->bedrijfPersoon_type = $this->bedrijfPersoon->getType();
		}
		return $this->bedrijfPersoon_type;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_commissieID) || $new_commissieID == 0)) {
			if($this->commissie == NULL && $this->commissie_commissieID == NULL)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld datum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datum.
	 */
	public function getDatum()
	{
		return $this->datum;
	}
	/**
	 * @brief Stel de waarde van het veld datum in.
	 *
	 * @param mixed $newDatum De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setDatum($newDatum)
	{
		unset($this->errors['Datum']);
		if(!$newDatum instanceof DateTimeLocale) {
			try {
				$newDatum = new DateTimeLocale($newDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datum->strftime('%F %T') == $newDatum->strftime('%F %T'))
			return $this;

		$this->datum = $newDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datum geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkDatum()
	{
		if (array_key_exists('Datum', $this->errors))
			return $this->errors['Datum'];
		$waarde = $this->getDatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inhoud.
	 *
	 * @return string
	 * De waarde van het veld inhoud.
	 */
	public function getInhoud()
	{
		return $this->inhoud;
	}
	/**
	 * @brief Stel de waarde van het veld inhoud in.
	 *
	 * @param mixed $newInhoud De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setInhoud($newInhoud)
	{
		unset($this->errors['Inhoud']);
		if(!is_null($newInhoud))
			$newInhoud = trim($newInhoud);
		if($newInhoud === "")
			$newInhoud = NULL;
		if (!is_null($newInhoud))
			$newInhoud = Purifier::Purify($newInhoud);
		if($this->inhoud === $newInhoud)
			return $this;

		$this->inhoud = $newInhoud;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inhoud geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inhoud geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkInhoud()
	{
		if (array_key_exists('Inhoud', $this->errors))
			return $this->errors['Inhoud'];
		$waarde = $this->getInhoud();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld medium.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld medium.
	 */
	static public function enumsMedium()
	{
		static $vals = array('EMAIL','FAX','GESPREK','NOTITIE','ONBEKEND','OVERIG','POST','TELEFOON');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld medium.
	 *
	 * @return string
	 * De waarde van het veld medium.
	 */
	public function getMedium()
	{
		return $this->medium;
	}
	/**
	 * @brief Stel de waarde van het veld medium in.
	 *
	 * @param mixed $newMedium De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setMedium($newMedium)
	{
		unset($this->errors['Medium']);
		if(!is_null($newMedium))
			$newMedium = strtoupper(trim($newMedium));
		if($newMedium === "")
			$newMedium = NULL;
		if($this->medium === $newMedium)
			return $this;

		$this->medium = $newMedium;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld medium geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld medium geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMedium()
	{
		if (array_key_exists('Medium', $this->errors))
			return $this->errors['Medium'];
		$waarde = $this->getMedium();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsMedium()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld todo.
	 *
	 * @return bool
	 * De waarde van het veld todo.
	 */
	public function getTodo()
	{
		return $this->todo;
	}
	/**
	 * @brief Stel de waarde van het veld todo in.
	 *
	 * @param mixed $newTodo De nieuwe waarde.
	 *
	 * @return Interactie
	 * Dit Interactie-object.
	 */
	public function setTodo($newTodo)
	{
		unset($this->errors['Todo']);
		if(!is_null($newTodo))
			$newTodo = (bool)$newTodo;
		if($this->todo === $newTodo)
			return $this;

		$this->todo = $newTodo;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld todo geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld todo geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTodo()
	{
		if (array_key_exists('Todo', $this->errors))
			return $this->errors['Todo'];
		$waarde = $this->getTodo();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Interactie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Interactie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Interactie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Interactie|false
	 * Een Interactie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'Interactie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Interactie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Interactie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Interactie`.`id`'
		                 .     ', `Interactie`.`spook_persoon_contactID`'
		                 .     ', `Interactie`.`spook_organisatie_contactID`'
		                 .     ', `Interactie`.`spook_type`'
		                 .     ', `Interactie`.`bedrijf_contactID`'
		                 .     ', `Interactie`.`bedrijfPersoon_persoon_contactID`'
		                 .     ', `Interactie`.`bedrijfPersoon_organisatie_contactID`'
		                 .     ', `Interactie`.`bedrijfPersoon_type`'
		                 .     ', `Interactie`.`commissie_commissieID`'
		                 .     ', `Interactie`.`datum`'
		                 .     ', `Interactie`.`inhoud`'
		                 .     ', `Interactie`.`medium`'
		                 .     ', `Interactie`.`todo`'
		                 .     ', `Interactie`.`gewijzigdWanneer`'
		                 .     ', `Interactie`.`gewijzigdWie`'
		                 .' FROM `Interactie`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new Interactie();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->spook_contactID  = (int) $row['spook_persoon_contactID'];
			$obj->spook_contactID2  = (int) $row['spook_organisatie_contactID'];
			$obj->spook_type  = strtoupper(trim($row['spook_type']));
			$obj->bedrijf_contactID  = (int) $row['bedrijf_contactID'];
			$obj->bedrijfPersoon_contactID  = (is_null($row['bedrijfPersoon_persoon_contactID'])) ? null : (int) $row['bedrijfPersoon_persoon_contactID'];
			$obj->bedrijfPersoon_contactID2  = (is_null($row['bedrijfPersoon_organisatie_contactID'])) ? null : (int) $row['bedrijfPersoon_organisatie_contactID'];
			$obj->bedrijfPersoon_type  = (is_null($row['bedrijfPersoon_type'])) ? null : strtoupper(trim($row['bedrijfPersoon_type']));
			$obj->commissie_commissieID  = (is_null($row['commissie_commissieID'])) ? null : (int) $row['commissie_commissieID'];
			$obj->datum  = new DateTimeLocale($row['datum']);
			$obj->inhoud = $row['inhoud'];
			$obj->medium  = strtoupper(trim($row['medium']));
			$obj->todo  = (bool) $row['todo'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Interactie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getSpookContactID();
		$this->getSpookContactID2();
		$this->getSpookType();
		$this->getBedrijfContactID();
		$this->getBedrijfPersoonContactID();
		$this->getBedrijfPersoonContactID2();
		$this->getBedrijfPersoonType();
		$this->getCommissieCommissieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `Interactie`'
			          . ' (`spook_persoon_contactID`, `spook_organisatie_contactID`, `spook_type`, `bedrijf_contactID`, `bedrijfPersoon_persoon_contactID`, `bedrijfPersoon_organisatie_contactID`, `bedrijfPersoon_type`, `commissie_commissieID`, `datum`, `inhoud`, `medium`, `todo`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i, %i, %i, %s, %i, %s, %s, %s, %i, %s, %i)'
			          , $this->spook_contactID
			          , $this->spook_contactID2
			          , $this->spook_type
			          , $this->bedrijf_contactID
			          , $this->bedrijfPersoon_contactID
			          , $this->bedrijfPersoon_contactID2
			          , $this->bedrijfPersoon_type
			          , $this->commissie_commissieID
			          , $this->datum->strftime('%F %T')
			          , $this->inhoud
			          , $this->medium
			          , $this->todo
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Interactie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Interactie`'
			          .' SET `spook_contactID` = %i'
			          .   ', `spook_contactID2` = %i'
			          .   ', `spook_type` = %s'
			          .   ', `bedrijf_contactID` = %i'
			          .   ', `bedrijfPersoon_contactID` = %i'
			          .   ', `bedrijfPersoon_contactID2` = %i'
			          .   ', `bedrijfPersoon_type` = %s'
			          .   ', `commissie_commissieID` = %i'
			          .   ', `datum` = %s'
			          .   ', `inhoud` = %s'
			          .   ', `medium` = %s'
			          .   ', `todo` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->spook_contactID
			          , $this->spook_contactID2
			          , $this->spook_type
			          , $this->bedrijf_contactID
			          , $this->bedrijfPersoon_contactID
			          , $this->bedrijfPersoon_contactID2
			          , $this->bedrijfPersoon_type
			          , $this->commissie_commissieID
			          , $this->datum->strftime('%F %T')
			          , $this->inhoud
			          , $this->medium
			          , $this->todo
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenInteractie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenInteractie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenInteractie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Spook';
			$velden[] = 'Bedrijf';
			$velden[] = 'BedrijfPersoon';
			$velden[] = 'Commissie';
			$velden[] = 'Datum';
			$velden[] = 'Inhoud';
			$velden[] = 'Medium';
			$velden[] = 'Todo';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Spook';
			$velden[] = 'Bedrijf';
			$velden[] = 'Datum';
			$velden[] = 'Inhoud';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Spook';
			$velden[] = 'Bedrijf';
			$velden[] = 'BedrijfPersoon';
			$velden[] = 'Commissie';
			$velden[] = 'Datum';
			$velden[] = 'Inhoud';
			$velden[] = 'Medium';
			$velden[] = 'Todo';
			break;
		case 'get':
			$velden[] = 'Spook';
			$velden[] = 'Bedrijf';
			$velden[] = 'BedrijfPersoon';
			$velden[] = 'Commissie';
			$velden[] = 'Datum';
			$velden[] = 'Inhoud';
			$velden[] = 'Medium';
			$velden[] = 'Todo';
		case 'primary':
			$velden[] = 'spook_contactID';
			$velden[] = 'spook_contactID2';
			$velden[] = 'spook_type';
			$velden[] = 'bedrijf_contactID';
			$velden[] = 'bedrijfPersoon_contactID';
			$velden[] = 'bedrijfPersoon_contactID2';
			$velden[] = 'bedrijfPersoon_type';
			$velden[] = 'commissie_commissieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Spook';
			$velden[] = 'Bedrijf';
			$velden[] = 'BedrijfPersoon';
			$velden[] = 'Commissie';
			$velden[] = 'Datum';
			$velden[] = 'Inhoud';
			$velden[] = 'Medium';
			$velden[] = 'Todo';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Interactie`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Interactie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Interactie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'spook':
			return 'foreign';
		case 'spook_contactid':
			return 'int';
		case 'spook_contactid2':
			return 'int';
		case 'spook_type':
			return 'enum';
		case 'bedrijf':
			return 'foreign';
		case 'bedrijf_contactid':
			return 'int';
		case 'bedrijfpersoon':
			return 'foreign';
		case 'bedrijfpersoon_contactid':
			return 'int';
		case 'bedrijfpersoon_contactid2':
			return 'int';
		case 'bedrijfpersoon_type':
			return 'enum';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'datum':
			return 'datetime';
		case 'inhoud':
			return 'html';
		case 'medium':
			return 'enum';
		case 'todo':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'spook_contactID':
		case 'spook_contactID2':
		case 'bedrijf_contactID':
		case 'bedrijfPersoon_contactID':
		case 'bedrijfPersoon_contactID2':
		case 'commissie_commissieID':
		case 'todo':
			$type = '%i';
			break;
		case 'spook_type':
		case 'bedrijfPersoon_type':
		case 'datum':
		case 'inhoud':
		case 'medium':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Interactie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Interactie`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Interactie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
