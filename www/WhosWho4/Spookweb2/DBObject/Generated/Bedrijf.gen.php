<?
/**
 * @brief 'AT'AUTH_GET:spookweb,AUTH_SET:spookweb
 */
abstract class Bedrijf_Generated
	extends Organisatie
{
	protected $status;					/**< \brief ENUM:AFGEWEZEN/HUIDIG/KOSTENLOOS/OUD/POTENTIEEL/OVERGENOME/OPGEHEVEN/OVERIG */
	protected $type;					/**< \brief ENUM:ICT/CONSULTANCY/FINANCIEEL/ONDERZOEK/OVERIG */
	/** Verzamelingen **/
	protected $contractVerzameling;
	protected $interactieVerzameling;
	protected $bedrijfStudieVerzameling;
	/**
	 * @brief De constructor van de Bedrijf_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Organisatie

		$this->contactID = NULL;
		$this->status = 'POTENTIEEL';
		$this->type = 'ICT';
		$this->contractVerzameling = NULL;
		$this->interactieVerzameling = NULL;
		$this->bedrijfStudieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('AFGEWEZEN','HUIDIG','KOSTENLOOS','OUD','POTENTIEEL','OVERGENOME','OPGEHEVEN','OVERIG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return Bedrijf
	 * Dit Bedrijf-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld type.
	 */
	static public function enumsType()
	{
		static $vals = array('ICT','CONSULTANCY','FINANCIEEL','ONDERZOEK','OVERIG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @return string
	 * De waarde van het veld type.
	 */
	public function getType()
	{
		return $this->type;
	}
	/**
	 * @brief Stel de waarde van het veld type in.
	 *
	 * @param mixed $newType De nieuwe waarde.
	 *
	 * @return Bedrijf
	 * Dit Bedrijf-object.
	 */
	public function setType($newType)
	{
		unset($this->errors['Type']);
		if(!is_null($newType))
			$newType = strtoupper(trim($newType));
		if($newType === "")
			$newType = NULL;
		if($this->type === $newType)
			return $this;

		$this->type = $newType;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld type geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld type geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkType()
	{
		if (array_key_exists('Type', $this->errors))
			return $this->errors['Type'];
		$waarde = $this->getType();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsType()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Returneert de ContractVerzameling die hoort bij dit object.
	 */
	public function getContractVerzameling()
	{
		if(!$this->contractVerzameling instanceof ContractVerzameling)
			$this->contractVerzameling = ContractVerzameling::fromBedrijf($this);
		return $this->contractVerzameling;
	}
	/**
	 * @brief Returneert de InteractieVerzameling die hoort bij dit object.
	 */
	public function getInteractieVerzameling()
	{
		if(!$this->interactieVerzameling instanceof InteractieVerzameling)
			$this->interactieVerzameling = InteractieVerzameling::fromBedrijf($this);
		return $this->interactieVerzameling;
	}
	/**
	 * @brief Returneert de BedrijfStudieVerzameling die hoort bij dit object.
	 */
	public function getBedrijfStudieVerzameling()
	{
		if(!$this->bedrijfStudieVerzameling instanceof BedrijfStudieVerzameling)
			$this->bedrijfStudieVerzameling = BedrijfStudieVerzameling::fromBedrijf($this);
		return $this->bedrijfStudieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Bedrijf::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Bedrijf.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Bedrijf::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContactID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Bedrijf|false
	 * Een Bedrijf-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contactID = (int)$a;
		}

		if(is_null($contactID))
			throw new BadMethodCallException();

		static::cache(array( array($contactID) ));
		return Entiteit::geefCache(array($contactID), 'Bedrijf');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Contact::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Bedrijf::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Bedrijf`.`status`'
			                 .     ', `Bedrijf`.`type`'
			                 .     ', `Organisatie`.`naam`'
			                 .     ', `Organisatie`.`soort`'
			                 .     ', `Organisatie`.`omschrijving`'
			                 .     ', `Organisatie`.`logo`'
			                 .     ', `Contact`.`contactID`'
			                 .     ', `Contact`.`email`'
			                 .     ', `Contact`.`homepage`'
			                 .     ', `Contact`.`vakidOpsturen`'
			                 .     ', `Contact`.`aes2rootsOpsturen`'
			                 .     ', `Contact`.`gewijzigdWanneer`'
			                 .     ', `Contact`.`gewijzigdWie`'
			                 .' FROM `Bedrijf`'
			                 .' LEFT JOIN `Organisatie` USING (`contactID`)'
			                 .' LEFT JOIN `Contact` USING (`contactID`)'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contactID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Bedrijf'][$id];
			} else {
				$obj = new Bedrijf();
			}

			$obj->inDB = True;

			$obj->status  = strtoupper(trim($row['status']));
			$obj->type  = strtoupper(trim($row['type']));
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Organisatie::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Bedrijf')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Bedrijf`'
			          . ' (`contactID`, `status`, `type`)'
			          . ' VALUES (%i, %s, %s)'
			          , $this->contactID
			          , $this->status
			          , $this->type
			          );

			if($classname == 'Bedrijf')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Bedrijf`'
			          .' SET `status` = %s'
			          .   ', `type` = %s'
			          .' WHERE `contactID` = %i'
			          , $this->status
			          , $this->type
			          , $this->contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBedrijf
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBedrijf($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBedrijf($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Status';
			$velden[] = 'Type';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Status';
			$velden[] = 'Type';
			break;
		case 'get':
			$velden[] = 'Status';
			$velden[] = 'Type';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'ContractVerzameling';
			$velden[] = 'InteractieVerzameling';
			$velden[] = 'BedrijfStudieVerzameling';
			break;
		default:
			$velden[] = 'Status';
			$velden[] = 'Type';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Contract-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContractFromBedrijfVerz = ContractVerzameling::fromBedrijf($this);
		$returnValue = $ContractFromBedrijfVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Contract met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Interactie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$InteractieFromBedrijfVerz = InteractieVerzameling::fromBedrijf($this);
		$returnValue = $InteractieFromBedrijfVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Interactie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle BedrijfStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BedrijfStudieFromBedrijfVerz = BedrijfStudieVerzameling::fromBedrijf($this);
		$returnValue = $BedrijfStudieFromBedrijfVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object BedrijfStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $ContractFromBedrijfVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $InteractieFromBedrijfVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BedrijfStudieFromBedrijfVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Bedrijf`'
		          .' WHERE `contactID` = %i'
		          .' LIMIT 1'
		          , $this->contactID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Bedrijf terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'status':
			return 'enum';
		case 'type':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'status':
		case 'type':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Bedrijf`'
		          ." SET `%l` = %s"
		          .' WHERE `contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'status':
		case 'type':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Bedrijf`'
		          .' WHERE `contactID` = %i'
		                 , $veld
		          , $this->contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Contract-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContractFromBedrijfVerz = ContractVerzameling::fromBedrijf($this);
		$dependencies['Contract'] = $ContractFromBedrijfVerz;

		// Verzamel alle Interactie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$InteractieFromBedrijfVerz = InteractieVerzameling::fromBedrijf($this);
		$dependencies['Interactie'] = $InteractieFromBedrijfVerz;

		// Verzamel alle BedrijfStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BedrijfStudieFromBedrijfVerz = BedrijfStudieVerzameling::fromBedrijf($this);
		$dependencies['BedrijfStudie'] = $BedrijfStudieFromBedrijfVerz;

		return $dependencies;
	}
}
