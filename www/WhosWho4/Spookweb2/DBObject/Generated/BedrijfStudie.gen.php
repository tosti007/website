<?
abstract class BedrijfStudie_Generated
	extends Entiteit
{
	protected $Studie;					/**< \brief PRIMARY */
	protected $Studie_studieID;			/**< \brief PRIMARY */
	protected $Bedrijf;					/**< \brief PRIMARY */
	protected $Bedrijf_contactID;		/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de BedrijfStudie_Generated-klasse.
	 *
	 * @param mixed $a Studie (Studie OR Array(studie_studieID) OR studie_studieID)
	 * @param mixed $b Bedrijf (Bedrijf OR Array(bedrijf_contactID) OR
	 * bedrijf_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Studie)
		{
			$this->Studie = $a;
			$this->Studie_studieID = $a->getStudieID();
		}
		else if(is_array($a))
		{
			$this->Studie = NULL;
			$this->Studie_studieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->Studie = NULL;
			$this->Studie_studieID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Bedrijf)
		{
			$this->Bedrijf = $b;
			$this->Bedrijf_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->Bedrijf = NULL;
			$this->Bedrijf_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->Bedrijf = NULL;
			$this->Bedrijf_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Studie';
		$volgorde[] = 'Bedrijf';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld Studie.
	 *
	 * @return Studie
	 * De waarde van het veld Studie.
	 */
	public function getStudie()
	{
		if(!isset($this->Studie)
		 && isset($this->Studie_studieID)
		 ) {
			$this->Studie = Studie::geef
					( $this->Studie_studieID
					);
		}
		return $this->Studie;
	}
	/**
	 * @brief Geef de waarde van het veld Studie_studieID.
	 *
	 * @return int
	 * De waarde van het veld Studie_studieID.
	 */
	public function getStudieStudieID()
	{
		if (is_null($this->Studie_studieID) && isset($this->Studie)) {
			$this->Studie_studieID = $this->Studie->getStudieID();
		}
		return $this->Studie_studieID;
	}
	/**
	 * @brief Geef de waarde van het veld Bedrijf.
	 *
	 * @return Bedrijf
	 * De waarde van het veld Bedrijf.
	 */
	public function getBedrijf()
	{
		if(!isset($this->Bedrijf)
		 && isset($this->Bedrijf_contactID)
		 ) {
			$this->Bedrijf = Bedrijf::geef
					( $this->Bedrijf_contactID
					);
		}
		return $this->Bedrijf;
	}
	/**
	 * @brief Geef de waarde van het veld Bedrijf_contactID.
	 *
	 * @return int
	 * De waarde van het veld Bedrijf_contactID.
	 */
	public function getBedrijfContactID()
	{
		if (is_null($this->Bedrijf_contactID) && isset($this->Bedrijf)) {
			$this->Bedrijf_contactID = $this->Bedrijf->getContactID();
		}
		return $this->Bedrijf_contactID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return BedrijfStudie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van BedrijfStudie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return BedrijfStudie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getStudieStudieID()
		                      , $this->getBedrijfContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return BedrijfStudie|false
	 * Een BedrijfStudie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$Studie_studieID = (int)$a[0];
			$Bedrijf_contactID = (int)$a[1];
		}
		else if($a instanceof Studie
		     && $b instanceof Bedrijf)
		{
			$Studie_studieID = $a->getStudieID();
			$Bedrijf_contactID = $b->getContactID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$Studie_studieID = (int)$a;
			$Bedrijf_contactID = (int)$b;
		}

		if(is_null($Studie_studieID)
		|| is_null($Bedrijf_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($Studie_studieID, $Bedrijf_contactID) ));
		return Entiteit::geefCache(array($Studie_studieID, $Bedrijf_contactID), 'BedrijfStudie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'BedrijfStudie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('BedrijfStudie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `BedrijfStudie`.`Studie_studieID`'
		                 .     ', `BedrijfStudie`.`Bedrijf_contactID`'
		                 .     ', `BedrijfStudie`.`gewijzigdWanneer`'
		                 .     ', `BedrijfStudie`.`gewijzigdWie`'
		                 .' FROM `BedrijfStudie`'
		                 .' WHERE (`Studie_studieID`, `Bedrijf_contactID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['Studie_studieID']
			                  ,$row['Bedrijf_contactID']);

			$obj = new BedrijfStudie(array($row['Studie_studieID']), array($row['Bedrijf_contactID']));

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'BedrijfStudie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getStudie();
		if(!$rel->getInDB())
			throw new LogicException('foreign Studie is not in DB');
		$this->Studie_studieID = $rel->getStudieID();

		$rel = $this->getBedrijf();
		if(!$rel->getInDB())
			throw new LogicException('foreign Bedrijf is not in DB');
		$this->Bedrijf_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `BedrijfStudie`'
			          . ' (`Studie_studieID`, `Bedrijf_contactID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i)'
			          , $this->Studie_studieID
			          , $this->Bedrijf_contactID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'BedrijfStudie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `BedrijfStudie`'
			.' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `Studie_studieID` = %i'
			          .  ' AND `Bedrijf_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->Studie_studieID
			          , $this->Bedrijf_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBedrijfStudie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBedrijfStudie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBedrijfStudie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			break;
		case 'get':
		case 'primary':
			$velden[] = 'Studie_studieID';
			$velden[] = 'Bedrijf_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `BedrijfStudie`'
		          .' WHERE `Studie_studieID` = %i'
		          .  ' AND `Bedrijf_contactID` = %i'
		          .' LIMIT 1'
		          , $this->Studie_studieID
		          , $this->Bedrijf_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->Studie = NULL;
		$this->Studie_studieID = NULL;
		$this->Bedrijf = NULL;
		$this->Bedrijf_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `BedrijfStudie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `Studie_studieID` = %i'
			          .  ' AND `Bedrijf_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->Studie_studieID
			          , $this->Bedrijf_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van BedrijfStudie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'studie':
			return 'foreign';
		case 'studie_studieid':
			return 'int';
		case 'bedrijf':
			return 'foreign';
		case 'bedrijf_contactid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `BedrijfStudie`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `Studie_studieID` = %i'
		          .  ' AND `Bedrijf_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->Studie_studieID
		          , $this->Bedrijf_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `BedrijfStudie`'
		          .' WHERE `Studie_studieID` = %i'
		          .  ' AND `Bedrijf_contactID` = %i'
		                 , $veld
		          , $this->Studie_studieID
		          , $this->Bedrijf_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'BedrijfStudie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
