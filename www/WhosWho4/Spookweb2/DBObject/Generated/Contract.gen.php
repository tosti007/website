<?
/**
 * @brief 'AT'AUTH_GET:spookweb,AUTH_SET:spookweb
 */
abstract class Contract_Generated
	extends Entiteit
{
	protected $contractID;				/**< \brief PRIMARY */
	protected $bedrijf;
	protected $bedrijf_contactID;		/**< \brief PRIMARY */
	protected $korting;					/**< \brief percentage of bedrag NULL */
	protected $status;					/**< \brief ENUM:GEFACTUREERD/MISLUKT/OPGESTUURD/RETOUR/VOORSTEL */
	protected $ingangsdatum;			/**< \brief NULL Datum wanneer het contract ingaat */
	protected $opmerking;				/**< \brief NULL */
	/** Verzamelingen **/
	protected $contractonderdeelVerzameling;
	/**
	 * @brief De constructor van de Contract_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->contractID = NULL;
		$this->bedrijf = NULL;
		$this->bedrijf_contactID = 0;
		$this->korting = NULL;
		$this->status = 'GEFACTUREERD';
		$this->ingangsdatum = new DateTimeLocale();
		$this->opmerking = NULL;
		$this->contractonderdeelVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld contractID.
	 *
	 * @return int
	 * De waarde van het veld contractID.
	 */
	public function getContractID()
	{
		return $this->contractID;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijf.
	 *
	 * @return Bedrijf
	 * De waarde van het veld bedrijf.
	 */
	public function getBedrijf()
	{
		if(!isset($this->bedrijf)
		 && isset($this->bedrijf_contactID)
		 ) {
			$this->bedrijf = Bedrijf::geef
					( $this->bedrijf_contactID
					);
		}
		return $this->bedrijf;
	}
	/**
	 * @brief Stel de waarde van het veld bedrijf in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Contract
	 * Dit Contract-object.
	 */
	public function setBedrijf($new_contactID)
	{
		unset($this->errors['Bedrijf']);
		if($new_contactID instanceof Bedrijf
		) {
			if($this->bedrijf == $new_contactID
			&& $this->bedrijf_contactID == $this->bedrijf->getContactID())
				return $this;
			$this->bedrijf = $new_contactID;
			$this->bedrijf_contactID
					= $this->bedrijf->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->bedrijf == NULL 
				&& $this->bedrijf_contactID == (int)$new_contactID)
				return $this;
			$this->bedrijf = NULL;
			$this->bedrijf_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bedrijf geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bedrijf geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBedrijf()
	{
		if (array_key_exists('Bedrijf', $this->errors))
			return $this->errors['Bedrijf'];
		$waarde1 = $this->getBedrijf();
		$waarde2 = $this->getBedrijfContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bedrijf_contactID.
	 *
	 * @return int
	 * De waarde van het veld bedrijf_contactID.
	 */
	public function getBedrijfContactID()
	{
		if (is_null($this->bedrijf_contactID) && isset($this->bedrijf)) {
			$this->bedrijf_contactID = $this->bedrijf->getContactID();
		}
		return $this->bedrijf_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld korting.
	 *
	 * @return string
	 * De waarde van het veld korting.
	 */
	public function getKorting()
	{
		return $this->korting;
	}
	/**
	 * @brief Stel de waarde van het veld korting in.
	 *
	 * @param mixed $newKorting De nieuwe waarde.
	 *
	 * @return Contract
	 * Dit Contract-object.
	 */
	public function setKorting($newKorting)
	{
		unset($this->errors['Korting']);
		if(!is_null($newKorting))
			$newKorting = trim($newKorting);
		if($newKorting === "")
			$newKorting = NULL;
		if($this->korting === $newKorting)
			return $this;

		$this->korting = $newKorting;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld korting geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld korting geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkKorting()
	{
		if (array_key_exists('Korting', $this->errors))
			return $this->errors['Korting'];
		$waarde = $this->getKorting();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('GEFACTUREERD','MISLUKT','OPGESTUURD','RETOUR','VOORSTEL');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return Contract
	 * Dit Contract-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ingangsdatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld ingangsdatum.
	 */
	public function getIngangsdatum()
	{
		return $this->ingangsdatum;
	}
	/**
	 * @brief Stel de waarde van het veld ingangsdatum in.
	 *
	 * @param mixed $newIngangsdatum De nieuwe waarde.
	 *
	 * @return Contract
	 * Dit Contract-object.
	 */
	public function setIngangsdatum($newIngangsdatum)
	{
		unset($this->errors['Ingangsdatum']);
		if(!$newIngangsdatum instanceof DateTimeLocale) {
			try {
				$newIngangsdatum = new DateTimeLocale($newIngangsdatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->ingangsdatum->strftime('%F %T') == $newIngangsdatum->strftime('%F %T'))
			return $this;

		$this->ingangsdatum = $newIngangsdatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ingangsdatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ingangsdatum geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkIngangsdatum()
	{
		if (array_key_exists('Ingangsdatum', $this->errors))
			return $this->errors['Ingangsdatum'];
		$waarde = $this->getIngangsdatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return Contract
	 * Dit Contract-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de ContractonderdeelVerzameling die hoort bij dit object.
	 */
	public function getContractonderdeelVerzameling()
	{
		if(!$this->contractonderdeelVerzameling instanceof ContractonderdeelVerzameling)
			$this->contractonderdeelVerzameling = ContractonderdeelVerzameling::fromContract($this);
		return $this->contractonderdeelVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Contract::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Contract.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Contract::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContractID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Contract|false
	 * Een Contract-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contractID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contractID = (int)$a;
		}

		if(is_null($contractID))
			throw new BadMethodCallException();

		static::cache(array( array($contractID) ));
		return Entiteit::geefCache(array($contractID), 'Contract');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Contract');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Contract::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Contract`.`contractID`'
		                 .     ', `Contract`.`bedrijf_contactID`'
		                 .     ', `Contract`.`korting`'
		                 .     ', `Contract`.`status`'
		                 .     ', `Contract`.`ingangsdatum`'
		                 .     ', `Contract`.`opmerking`'
		                 .     ', `Contract`.`gewijzigdWanneer`'
		                 .     ', `Contract`.`gewijzigdWie`'
		                 .' FROM `Contract`'
		                 .' WHERE (`contractID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contractID']);

			$obj = new Contract();

			$obj->inDB = True;

			$obj->contractID  = (int) $row['contractID'];
			$obj->bedrijf_contactID  = (int) $row['bedrijf_contactID'];
			$obj->korting  = (is_null($row['korting'])) ? null : trim($row['korting']);
			$obj->status  = strtoupper(trim($row['status']));
			$obj->ingangsdatum  = new DateTimeLocale($row['ingangsdatum']);
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Contract')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getBedrijfContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->contractID =
			$WSW4DB->q('RETURNID INSERT INTO `Contract`'
			          . ' (`bedrijf_contactID`, `korting`, `status`, `ingangsdatum`, `opmerking`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %i)'
			          , $this->bedrijf_contactID
			          , $this->korting
			          , $this->status
			          , $this->ingangsdatum->strftime('%F %T')
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Contract')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Contract`'
			          .' SET `bedrijf_contactID` = %i'
			          .   ', `korting` = %s'
			          .   ', `status` = %s'
			          .   ', `ingangsdatum` = %s'
			          .   ', `opmerking` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `contractID` = %i'
			          , $this->bedrijf_contactID
			          , $this->korting
			          , $this->status
			          , $this->ingangsdatum->strftime('%F %T')
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->contractID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenContract
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenContract($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenContract($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Bedrijf';
			$velden[] = 'Korting';
			$velden[] = 'Status';
			$velden[] = 'Ingangsdatum';
			$velden[] = 'Opmerking';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Bedrijf';
			$velden[] = 'Ingangsdatum';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Bedrijf';
			$velden[] = 'Korting';
			$velden[] = 'Status';
			$velden[] = 'Ingangsdatum';
			$velden[] = 'Opmerking';
			break;
		case 'get':
			$velden[] = 'Bedrijf';
			$velden[] = 'Korting';
			$velden[] = 'Status';
			$velden[] = 'Ingangsdatum';
			$velden[] = 'Opmerking';
		case 'primary':
			$velden[] = 'bedrijf_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'ContractonderdeelVerzameling';
			break;
		default:
			$velden[] = 'Bedrijf';
			$velden[] = 'Korting';
			$velden[] = 'Status';
			$velden[] = 'Ingangsdatum';
			$velden[] = 'Opmerking';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Contractonderdeel-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ContractonderdeelFromContractVerz = ContractonderdeelVerzameling::fromContract($this);
		$returnValue = $ContractonderdeelFromContractVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Contractonderdeel met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $ContractonderdeelFromContractVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Contract`'
		          .' WHERE `contractID` = %i'
		          .' LIMIT 1'
		          , $this->contractID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->contractID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Contract`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `contractID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->contractID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Contract terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'contractid':
			return 'int';
		case 'bedrijf':
			return 'foreign';
		case 'bedrijf_contactid':
			return 'int';
		case 'korting':
			return 'string';
		case 'status':
			return 'enum';
		case 'ingangsdatum':
			return 'date';
		case 'opmerking':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'contractID':
		case 'bedrijf_contactID':
			$type = '%i';
			break;
		case 'korting':
		case 'status':
		case 'ingangsdatum':
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Contract`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `contractID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->contractID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Contract`'
		          .' WHERE `contractID` = %i'
		                 , $veld
		          , $this->contractID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Contract');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Contractonderdeel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ContractonderdeelFromContractVerz = ContractonderdeelVerzameling::fromContract($this);
		$dependencies['Contractonderdeel'] = $ContractonderdeelFromContractVerz;

		return $dependencies;
	}
}
