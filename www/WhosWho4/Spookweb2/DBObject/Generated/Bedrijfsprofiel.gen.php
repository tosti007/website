<?
/**
 * @brief 'AT'AUTH_GET:gast,AUTH_SET:spocie
 */
abstract class Bedrijfsprofiel_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $contractonderdeel;		/**< \brief LEGACY_NULL */
	protected $contractonderdeel_id;	/**< \brief PRIMARY */
	protected $datumVerloop;
	protected $inhoud;					/**< \brief LANG,NULL */
	protected $url;						/**< \brief Url waar het logo naartoe moet linken (kan een specifieke recruitment pagina zijn) NULL */
	/**
	 * @brief De constructor van de Bedrijfsprofiel_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->contractonderdeel = NULL;
		$this->contractonderdeel_id = 0;
		$this->datumVerloop = new DateTimeLocale();
		$this->inhoud = array('nl' => NULL, 'en' => NULL);
		$this->url = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld contractonderdeel.
	 *
	 * @return Contractonderdeel
	 * De waarde van het veld contractonderdeel.
	 */
	public function getContractonderdeel()
	{
		if(!isset($this->contractonderdeel)
		 && isset($this->contractonderdeel_id)
		 ) {
			$this->contractonderdeel = Contractonderdeel::geef
					( $this->contractonderdeel_id
					);
		}
		return $this->contractonderdeel;
	}
	/**
	 * @brief Stel de waarde van het veld contractonderdeel in.
	 *
	 * @param mixed $new_id De nieuwe waarde.
	 *
	 * @return Bedrijfsprofiel
	 * Dit Bedrijfsprofiel-object.
	 */
	public function setContractonderdeel($new_id)
	{
		unset($this->errors['Contractonderdeel']);
		if($new_id instanceof Contractonderdeel
		) {
			if($this->contractonderdeel == $new_id
			&& $this->contractonderdeel_id == $this->contractonderdeel->getId())
				return $this;
			$this->contractonderdeel = $new_id;
			$this->contractonderdeel_id
					= $this->contractonderdeel->getId();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_id)
		) {
			if($this->contractonderdeel == NULL 
				&& $this->contractonderdeel_id == (int)$new_id)
				return $this;
			$this->contractonderdeel = NULL;
			$this->contractonderdeel_id
					= (int)$new_id;
			$this->gewijzigd();
			return $this;
		}
		if (isset($this->contractonderdeel)
		 || isset($this->contractonderdeel_id))
		{
			$this->errors['Contractonderdeel'] = _('dit is een verplicht veld');
			$this->contractonderdeel = NULL;
			$this->contractonderdeel_id = NULL;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld contractonderdeel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld contractonderdeel geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkContractonderdeel()
	{
		if (array_key_exists('Contractonderdeel', $this->errors))
			return $this->errors['Contractonderdeel'];
		$waarde1 = $this->getContractonderdeel();
		$waarde2 = $this->getContractonderdeelId();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld contractonderdeel_id.
	 *
	 * @return int
	 * De waarde van het veld contractonderdeel_id.
	 */
	public function getContractonderdeelId()
	{
		if (is_null($this->contractonderdeel_id) && isset($this->contractonderdeel)) {
			$this->contractonderdeel_id = $this->contractonderdeel->getId();
		}
		return $this->contractonderdeel_id;
	}
	/**
	 * @brief Geef de waarde van het veld datumVerloop.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumVerloop.
	 */
	public function getDatumVerloop()
	{
		return $this->datumVerloop;
	}
	/**
	 * @brief Stel de waarde van het veld datumVerloop in.
	 *
	 * @param mixed $newDatumVerloop De nieuwe waarde.
	 *
	 * @return Bedrijfsprofiel
	 * Dit Bedrijfsprofiel-object.
	 */
	public function setDatumVerloop($newDatumVerloop)
	{
		unset($this->errors['DatumVerloop']);
		if(!$newDatumVerloop instanceof DateTimeLocale) {
			try {
				$newDatumVerloop = new DateTimeLocale($newDatumVerloop);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumVerloop->strftime('%F %T') == $newDatumVerloop->strftime('%F %T'))
			return $this;

		$this->datumVerloop = $newDatumVerloop;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumVerloop geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumVerloop geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDatumVerloop()
	{
		if (array_key_exists('DatumVerloop', $this->errors))
			return $this->errors['DatumVerloop'];
		$waarde = $this->getDatumVerloop();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inhoud.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld inhoud.
	 */
	public function getInhoud($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->inhoud[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld inhoud in.
	 *
	 * @param mixed $newInhoud De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Bedrijfsprofiel
	 * Dit Bedrijfsprofiel-object.
	 */
	public function setInhoud($newInhoud, $lang = NULL)
	{
		unset($this->errors['Inhoud']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newInhoud))
			$newInhoud = trim($newInhoud);
		if($newInhoud === "")
			$newInhoud = NULL;
		if (!is_null($newInhoud))
			$newInhoud = Purifier::Purify($newInhoud);
		if($this->inhoud[$lang] === $newInhoud)
			return $this;

		$this->inhoud[$lang] = $newInhoud;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inhoud geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inhoud geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkInhoud($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkInhoud('nl');
			$ret['en'] = $this->checkInhoud('en');
			return $ret;
		}

		if (array_key_exists('Inhoud', $this->errors))
			if (is_array($this->errors['Inhoud']) && array_key_exists($lang, $this->errors['Inhoud']))
				return $this->errors['Inhoud'][$lang];
		$waarde = $this->getInhoud($lang);
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld url.
	 *
	 * @return string
	 * De waarde van het veld url.
	 */
	public function getUrl()
	{
		return $this->url;
	}
	/**
	 * @brief Stel de waarde van het veld url in.
	 *
	 * @param mixed $newUrl De nieuwe waarde.
	 *
	 * @return Bedrijfsprofiel
	 * Dit Bedrijfsprofiel-object.
	 */
	public function setUrl($newUrl)
	{
		unset($this->errors['Url']);
		if(!is_null($newUrl))
			$newUrl = trim($newUrl);
		if($newUrl === "")
			$newUrl = NULL;
		if($this->url === $newUrl)
			return $this;

		$this->url = $newUrl;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld url geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld url geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkUrl()
	{
		if (array_key_exists('Url', $this->errors))
			return $this->errors['Url'];
		$waarde = $this->getUrl();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Bedrijfsprofiel::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Bedrijfsprofiel.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('spocie');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Bedrijfsprofiel::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Bedrijfsprofiel|false
	 * Een Bedrijfsprofiel-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'Bedrijfsprofiel');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Bedrijfsprofiel');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Bedrijfsprofiel::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Bedrijfsprofiel`.`id`'
		                 .     ', `Bedrijfsprofiel`.`contractonderdeel_id`'
		                 .     ', `Bedrijfsprofiel`.`datumVerloop`'
		                 .     ', `Bedrijfsprofiel`.`inhoud_NL`'
		                 .     ', `Bedrijfsprofiel`.`inhoud_EN`'
		                 .     ', `Bedrijfsprofiel`.`url`'
		                 .     ', `Bedrijfsprofiel`.`gewijzigdWanneer`'
		                 .     ', `Bedrijfsprofiel`.`gewijzigdWie`'
		                 .' FROM `Bedrijfsprofiel`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new Bedrijfsprofiel();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->contractonderdeel_id  = (int) $row['contractonderdeel_id'];
			$obj->datumVerloop  = new DateTimeLocale($row['datumVerloop']);
			$obj->inhoud['nl'] = $row['inhoud_NL'];
			$obj->inhoud['en'] = $row['inhoud_EN'];
			$obj->url  = (is_null($row['url'])) ? null : trim($row['url']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Bedrijfsprofiel')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getContractonderdeelId();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `Bedrijfsprofiel`'
			          . ' (`contractonderdeel_id`, `datumVerloop`, `inhoud_NL`, `inhoud_EN`, `url`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %s, %s, %i)'
			          , $this->contractonderdeel_id
			          , $this->datumVerloop->strftime('%F %T')
			          , $this->inhoud['nl']
			          , $this->inhoud['en']
			          , $this->url
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Bedrijfsprofiel')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Bedrijfsprofiel`'
			          .' SET `contractonderdeel_id` = %i'
			          .   ', `datumVerloop` = %s'
			          .   ', `inhoud_NL` = %s'
			          .   ', `inhoud_EN` = %s'
			          .   ', `url` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->contractonderdeel_id
			          , $this->datumVerloop->strftime('%F %T')
			          , $this->inhoud['nl']
			          , $this->inhoud['en']
			          , $this->url
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBedrijfsprofiel
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBedrijfsprofiel($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBedrijfsprofiel($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'Inhoud';
			$velden[] = 'Url';
			break;
		case 'lang':
			$velden[] = 'Inhoud';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'Inhoud';
			$velden[] = 'Url';
			break;
		case 'get':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'Inhoud';
			$velden[] = 'Url';
		case 'primary':
			$velden[] = 'contractonderdeel_id';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'Inhoud';
			$velden[] = 'Url';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Bedrijfsprofiel`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Bedrijfsprofiel`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Bedrijfsprofiel terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'contractonderdeel':
			return 'foreign';
		case 'contractonderdeel_id':
			return 'int';
		case 'datumverloop':
			return 'date';
		case 'inhoud':
			return 'html';
		case 'url':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'contractonderdeel_id':
			$type = '%i';
			break;
		case 'datumVerloop':
		case 'inhoud':
		case 'url':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Bedrijfsprofiel`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Bedrijfsprofiel`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Bedrijfsprofiel');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
