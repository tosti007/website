<?
/**
 * @brief 'AT'AUTH_GET:gast,AUTH_SET:spocie
 */
abstract class Vacature_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $contractonderdeel;		/**< \brief LEGACY_NULL */
	protected $contractonderdeel_id;	/**< \brief PRIMARY */
	protected $datumVerloop;
	protected $datumPlaatsing;
	protected $type;					/**< \brief ENUM:BIJBAAN/PROMOTIEPLAATS/STAGEPLEK/STARTERSFUNCTIE/AFSTUDEEROPDRACHT/WERKSTUDENTSCHAP */
	protected $ica;
	protected $iku;
	protected $na;
	protected $wis;
	protected $titel;					/**< \brief LANG */
	protected $inleiding;				/**< \brief LANG,NULL */
	protected $inhoud;					/**< \brief LANG,NULL */
	protected $views;
	/** Verzamelingen **/
	protected $vacatureStudieVerzameling;
	/**
	 * @brief De constructor van de Vacature_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->contractonderdeel = NULL;
		$this->contractonderdeel_id = 0;
		$this->datumVerloop = new DateTimeLocale();
		$this->datumPlaatsing = new DateTimeLocale();
		$this->type = 'BIJBAAN';
		$this->ica = False;
		$this->iku = False;
		$this->na = False;
		$this->wis = False;
		$this->titel = array('nl' => '', 'en' => '');
		$this->inleiding = array('nl' => NULL, 'en' => NULL);
		$this->inhoud = array('nl' => NULL, 'en' => NULL);
		$this->views = 0;
		$this->vacatureStudieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld contractonderdeel.
	 *
	 * @return Contractonderdeel
	 * De waarde van het veld contractonderdeel.
	 */
	public function getContractonderdeel()
	{
		if(!isset($this->contractonderdeel)
		 && isset($this->contractonderdeel_id)
		 ) {
			$this->contractonderdeel = Contractonderdeel::geef
					( $this->contractonderdeel_id
					);
		}
		return $this->contractonderdeel;
	}
	/**
	 * @brief Stel de waarde van het veld contractonderdeel in.
	 *
	 * @param mixed $new_id De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setContractonderdeel($new_id)
	{
		unset($this->errors['Contractonderdeel']);
		if($new_id instanceof Contractonderdeel
		) {
			if($this->contractonderdeel == $new_id
			&& $this->contractonderdeel_id == $this->contractonderdeel->getId())
				return $this;
			$this->contractonderdeel = $new_id;
			$this->contractonderdeel_id
					= $this->contractonderdeel->getId();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_id)
		) {
			if($this->contractonderdeel == NULL 
				&& $this->contractonderdeel_id == (int)$new_id)
				return $this;
			$this->contractonderdeel = NULL;
			$this->contractonderdeel_id
					= (int)$new_id;
			$this->gewijzigd();
			return $this;
		}
		if (isset($this->contractonderdeel)
		 || isset($this->contractonderdeel_id))
		{
			$this->errors['Contractonderdeel'] = _('dit is een verplicht veld');
			$this->contractonderdeel = NULL;
			$this->contractonderdeel_id = NULL;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld contractonderdeel geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld contractonderdeel geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkContractonderdeel()
	{
		if (array_key_exists('Contractonderdeel', $this->errors))
			return $this->errors['Contractonderdeel'];
		$waarde1 = $this->getContractonderdeel();
		$waarde2 = $this->getContractonderdeelId();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld contractonderdeel_id.
	 *
	 * @return int
	 * De waarde van het veld contractonderdeel_id.
	 */
	public function getContractonderdeelId()
	{
		if (is_null($this->contractonderdeel_id) && isset($this->contractonderdeel)) {
			$this->contractonderdeel_id = $this->contractonderdeel->getId();
		}
		return $this->contractonderdeel_id;
	}
	/**
	 * @brief Geef de waarde van het veld datumVerloop.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumVerloop.
	 */
	public function getDatumVerloop()
	{
		return $this->datumVerloop;
	}
	/**
	 * @brief Stel de waarde van het veld datumVerloop in.
	 *
	 * @param mixed $newDatumVerloop De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setDatumVerloop($newDatumVerloop)
	{
		unset($this->errors['DatumVerloop']);
		if(!$newDatumVerloop instanceof DateTimeLocale) {
			try {
				$newDatumVerloop = new DateTimeLocale($newDatumVerloop);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumVerloop->strftime('%F %T') == $newDatumVerloop->strftime('%F %T'))
			return $this;

		$this->datumVerloop = $newDatumVerloop;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumVerloop geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumVerloop geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDatumVerloop()
	{
		if (array_key_exists('DatumVerloop', $this->errors))
			return $this->errors['DatumVerloop'];
		$waarde = $this->getDatumVerloop();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumPlaatsing.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumPlaatsing.
	 */
	public function getDatumPlaatsing()
	{
		return $this->datumPlaatsing;
	}
	/**
	 * @brief Stel de waarde van het veld datumPlaatsing in.
	 *
	 * @param mixed $newDatumPlaatsing De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setDatumPlaatsing($newDatumPlaatsing)
	{
		unset($this->errors['DatumPlaatsing']);
		if(!$newDatumPlaatsing instanceof DateTimeLocale) {
			try {
				$newDatumPlaatsing = new DateTimeLocale($newDatumPlaatsing);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumPlaatsing->strftime('%F %T') == $newDatumPlaatsing->strftime('%F %T'))
			return $this;

		$this->datumPlaatsing = $newDatumPlaatsing;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumPlaatsing geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumPlaatsing geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDatumPlaatsing()
	{
		if (array_key_exists('DatumPlaatsing', $this->errors))
			return $this->errors['DatumPlaatsing'];
		$waarde = $this->getDatumPlaatsing();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld type.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld type.
	 */
	static public function enumsType()
	{
		static $vals = array('BIJBAAN','PROMOTIEPLAATS','STAGEPLEK','STARTERSFUNCTIE','AFSTUDEEROPDRACHT','WERKSTUDENTSCHAP');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @return string
	 * De waarde van het veld type.
	 */
	public function getType()
	{
		return $this->type;
	}
	/**
	 * @brief Stel de waarde van het veld type in.
	 *
	 * @param mixed $newType De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setType($newType)
	{
		unset($this->errors['Type']);
		if(!is_null($newType))
			$newType = strtoupper(trim($newType));
		if($newType === "")
			$newType = NULL;
		if($this->type === $newType)
			return $this;

		$this->type = $newType;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld type geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld type geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkType()
	{
		if (array_key_exists('Type', $this->errors))
			return $this->errors['Type'];
		$waarde = $this->getType();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsType()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld ica.
	 *
	 * @return bool
	 * De waarde van het veld ica.
	 */
	public function getIca()
	{
		return $this->ica;
	}
	/**
	 * @brief Stel de waarde van het veld ica in.
	 *
	 * @param mixed $newIca De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setIca($newIca)
	{
		unset($this->errors['Ica']);
		if(!is_null($newIca))
			$newIca = (bool)$newIca;
		if($this->ica === $newIca)
			return $this;

		$this->ica = $newIca;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld ica geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld ica geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkIca()
	{
		if (array_key_exists('Ica', $this->errors))
			return $this->errors['Ica'];
		$waarde = $this->getIca();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld iku.
	 *
	 * @return bool
	 * De waarde van het veld iku.
	 */
	public function getIku()
	{
		return $this->iku;
	}
	/**
	 * @brief Stel de waarde van het veld iku in.
	 *
	 * @param mixed $newIku De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setIku($newIku)
	{
		unset($this->errors['Iku']);
		if(!is_null($newIku))
			$newIku = (bool)$newIku;
		if($this->iku === $newIku)
			return $this;

		$this->iku = $newIku;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld iku geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld iku geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkIku()
	{
		if (array_key_exists('Iku', $this->errors))
			return $this->errors['Iku'];
		$waarde = $this->getIku();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld na.
	 *
	 * @return bool
	 * De waarde van het veld na.
	 */
	public function getNa()
	{
		return $this->na;
	}
	/**
	 * @brief Stel de waarde van het veld na in.
	 *
	 * @param mixed $newNa De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setNa($newNa)
	{
		unset($this->errors['Na']);
		if(!is_null($newNa))
			$newNa = (bool)$newNa;
		if($this->na === $newNa)
			return $this;

		$this->na = $newNa;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld na geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld na geldig is; anders een string met een
	 * foutmelding.
	 */
	public function checkNa()
	{
		if (array_key_exists('Na', $this->errors))
			return $this->errors['Na'];
		$waarde = $this->getNa();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld wis.
	 *
	 * @return bool
	 * De waarde van het veld wis.
	 */
	public function getWis()
	{
		return $this->wis;
	}
	/**
	 * @brief Stel de waarde van het veld wis in.
	 *
	 * @param mixed $newWis De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setWis($newWis)
	{
		unset($this->errors['Wis']);
		if(!is_null($newWis))
			$newWis = (bool)$newWis;
		if($this->wis === $newWis)
			return $this;

		$this->wis = $newWis;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld wis geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld wis geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkWis()
	{
		if (array_key_exists('Wis', $this->errors))
			return $this->errors['Wis'];
		$waarde = $this->getWis();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->titel[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld titel in.
	 *
	 * @param mixed $newTitel De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setTitel($newTitel, $lang = NULL)
	{
		unset($this->errors['Titel']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newTitel))
			$newTitel = trim($newTitel);
		if($newTitel === "")
			$newTitel = NULL;
		if($this->titel[$lang] === $newTitel)
			return $this;

		$this->titel[$lang] = $newTitel;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld titel geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld titel geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTitel($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkTitel('nl');
			$ret['en'] = $this->checkTitel('en');
			return $ret;
		}

		if (array_key_exists('Titel', $this->errors))
			if (is_array($this->errors['Titel']) && array_key_exists($lang, $this->errors['Titel']))
				return $this->errors['Titel'][$lang];
		$waarde = $this->getTitel($lang);
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inleiding.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld inleiding.
	 */
	public function getInleiding($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->inleiding[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld inleiding in.
	 *
	 * @param mixed $newInleiding De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setInleiding($newInleiding, $lang = NULL)
	{
		unset($this->errors['Inleiding']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newInleiding))
			$newInleiding = trim($newInleiding);
		if($newInleiding === "")
			$newInleiding = NULL;
		if (!is_null($newInleiding))
			$newInleiding = Purifier::Purify($newInleiding);
		if($this->inleiding[$lang] === $newInleiding)
			return $this;

		$this->inleiding[$lang] = $newInleiding;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inleiding geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inleiding geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkInleiding($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkInleiding('nl');
			$ret['en'] = $this->checkInleiding('en');
			return $ret;
		}

		if (array_key_exists('Inleiding', $this->errors))
			if (is_array($this->errors['Inleiding']) && array_key_exists($lang, $this->errors['Inleiding']))
				return $this->errors['Inleiding'][$lang];
		$waarde = $this->getInleiding($lang);
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inhoud.
	 *
	 * @param string|null $lang De taal waarin de waarde verkregen wordt.
	 *
	 * @return string
	 * De waarde van het veld inhoud.
	 */
	public function getInhoud($lang = NULL)
	{
		if (!isset($lang))
			$lang = getLang();
		return $this->inhoud[$lang];
	}
	/**
	 * @brief Stel de waarde van het veld inhoud in.
	 *
	 * @param mixed $newInhoud De nieuwe waarde.
	 * @param string|null $lang De taal die wordt ingesteld.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setInhoud($newInhoud, $lang = NULL)
	{
		unset($this->errors['Inhoud']);
		if(!isset($lang))
			$lang = getLang();
		if(!is_null($newInhoud))
			$newInhoud = trim($newInhoud);
		if($newInhoud === "")
			$newInhoud = NULL;
		if (!is_null($newInhoud))
			$newInhoud = Purifier::Purify($newInhoud);
		if($this->inhoud[$lang] === $newInhoud)
			return $this;

		$this->inhoud[$lang] = $newInhoud;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inhoud geldig is.
	 *
	 * @param string|null $lang De taal.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inhoud geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkInhoud($lang = null)
	{
		if (is_null($lang)) {
			$ret = array();
			$ret['nl'] = $this->checkInhoud('nl');
			$ret['en'] = $this->checkInhoud('en');
			return $ret;
		}

		if (array_key_exists('Inhoud', $this->errors))
			if (is_array($this->errors['Inhoud']) && array_key_exists($lang, $this->errors['Inhoud']))
				return $this->errors['Inhoud'][$lang];
		$waarde = $this->getInhoud($lang);
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld views.
	 *
	 * @return int
	 * De waarde van het veld views.
	 */
	public function getViews()
	{
		return $this->views;
	}
	/**
	 * @brief Stel de waarde van het veld views in.
	 *
	 * @param mixed $newViews De nieuwe waarde.
	 *
	 * @return Vacature
	 * Dit Vacature-object.
	 */
	public function setViews($newViews)
	{
		unset($this->errors['Views']);
		if(!is_null($newViews))
			$newViews = (int)$newViews;
		if($this->views === $newViews)
			return $this;

		$this->views = $newViews;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld views geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld views geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkViews()
	{
		if (array_key_exists('Views', $this->errors))
			return $this->errors['Views'];
		$waarde = $this->getViews();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de VacatureStudieVerzameling die hoort bij dit object.
	 */
	public function getVacatureStudieVerzameling()
	{
		if(!$this->vacatureStudieVerzameling instanceof VacatureStudieVerzameling)
			$this->vacatureStudieVerzameling = VacatureStudieVerzameling::fromVacature($this);
		return $this->vacatureStudieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('gast');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Vacature::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Vacature.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('spocie');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Vacature::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Vacature|false
	 * Een Vacature-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'Vacature');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Vacature');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Vacature::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Vacature`.`id`'
		                 .     ', `Vacature`.`contractonderdeel_id`'
		                 .     ', `Vacature`.`datumVerloop`'
		                 .     ', `Vacature`.`datumPlaatsing`'
		                 .     ', `Vacature`.`type`'
		                 .     ', `Vacature`.`ica`'
		                 .     ', `Vacature`.`iku`'
		                 .     ', `Vacature`.`na`'
		                 .     ', `Vacature`.`wis`'
		                 .     ', `Vacature`.`titel_NL`'
		                 .     ', `Vacature`.`titel_EN`'
		                 .     ', `Vacature`.`inleiding_NL`'
		                 .     ', `Vacature`.`inleiding_EN`'
		                 .     ', `Vacature`.`inhoud_NL`'
		                 .     ', `Vacature`.`inhoud_EN`'
		                 .     ', `Vacature`.`views`'
		                 .     ', `Vacature`.`gewijzigdWanneer`'
		                 .     ', `Vacature`.`gewijzigdWie`'
		                 .' FROM `Vacature`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new Vacature();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->contractonderdeel_id  = (int) $row['contractonderdeel_id'];
			$obj->datumVerloop  = new DateTimeLocale($row['datumVerloop']);
			$obj->datumPlaatsing  = new DateTimeLocale($row['datumPlaatsing']);
			$obj->type  = strtoupper(trim($row['type']));
			$obj->ica  = (bool) $row['ica'];
			$obj->iku  = (bool) $row['iku'];
			$obj->na  = (bool) $row['na'];
			$obj->wis  = (bool) $row['wis'];
			$obj->titel['nl'] = $row['titel_NL'];
			$obj->titel['en'] = $row['titel_EN'];
			$obj->inleiding['nl'] = $row['inleiding_NL'];
			$obj->inleiding['en'] = $row['inleiding_EN'];
			$obj->inhoud['nl'] = $row['inhoud_NL'];
			$obj->inhoud['en'] = $row['inhoud_EN'];
			$obj->views  = (int) $row['views'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Vacature')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getContractonderdeelId();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `Vacature`'
			          . ' (`contractonderdeel_id`, `datumVerloop`, `datumPlaatsing`, `type`, `ica`, `iku`, `na`, `wis`, `titel_NL`, `titel_EN`, `inleiding_NL`, `inleiding_EN`, `inhoud_NL`, `inhoud_EN`, `views`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %s, %i, %i, %i, %i, %s, %s, %s, %s, %s, %s, %i, %s, %i)'
			          , $this->contractonderdeel_id
			          , $this->datumVerloop->strftime('%F %T')
			          , $this->datumPlaatsing->strftime('%F %T')
			          , $this->type
			          , $this->ica
			          , $this->iku
			          , $this->na
			          , $this->wis
			          , $this->titel['nl']
			          , $this->titel['en']
			          , $this->inleiding['nl']
			          , $this->inleiding['en']
			          , $this->inhoud['nl']
			          , $this->inhoud['en']
			          , $this->views
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Vacature')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Vacature`'
			          .' SET `contractonderdeel_id` = %i'
			          .   ', `datumVerloop` = %s'
			          .   ', `datumPlaatsing` = %s'
			          .   ', `type` = %s'
			          .   ', `ica` = %i'
			          .   ', `iku` = %i'
			          .   ', `na` = %i'
			          .   ', `wis` = %i'
			          .   ', `titel_NL` = %s'
			          .   ', `titel_EN` = %s'
			          .   ', `inleiding_NL` = %s'
			          .   ', `inleiding_EN` = %s'
			          .   ', `inhoud_NL` = %s'
			          .   ', `inhoud_EN` = %s'
			          .   ', `views` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->contractonderdeel_id
			          , $this->datumVerloop->strftime('%F %T')
			          , $this->datumPlaatsing->strftime('%F %T')
			          , $this->type
			          , $this->ica
			          , $this->iku
			          , $this->na
			          , $this->wis
			          , $this->titel['nl']
			          , $this->titel['en']
			          , $this->inleiding['nl']
			          , $this->inleiding['en']
			          , $this->inhoud['nl']
			          , $this->inhoud['en']
			          , $this->views
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVacature
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVacature($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVacature($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'DatumPlaatsing';
			$velden[] = 'Type';
			$velden[] = 'Ica';
			$velden[] = 'Iku';
			$velden[] = 'Na';
			$velden[] = 'Wis';
			$velden[] = 'Titel';
			$velden[] = 'Inleiding';
			$velden[] = 'Inhoud';
			$velden[] = 'Views';
			break;
		case 'lang':
			$velden[] = 'Titel';
			$velden[] = 'Inleiding';
			$velden[] = 'Inhoud';
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'DatumPlaatsing';
			$velden[] = 'Titel';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'DatumPlaatsing';
			$velden[] = 'Type';
			$velden[] = 'Ica';
			$velden[] = 'Iku';
			$velden[] = 'Na';
			$velden[] = 'Wis';
			$velden[] = 'Titel';
			$velden[] = 'Inleiding';
			$velden[] = 'Inhoud';
			$velden[] = 'Views';
			break;
		case 'get':
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'DatumPlaatsing';
			$velden[] = 'Type';
			$velden[] = 'Ica';
			$velden[] = 'Iku';
			$velden[] = 'Na';
			$velden[] = 'Wis';
			$velden[] = 'Titel';
			$velden[] = 'Inleiding';
			$velden[] = 'Inhoud';
			$velden[] = 'Views';
		case 'primary':
			$velden[] = 'contractonderdeel_id';
			break;
		case 'verzamelingen':
			$velden[] = 'VacatureStudieVerzameling';
			break;
		default:
			$velden[] = 'Contractonderdeel';
			$velden[] = 'DatumVerloop';
			$velden[] = 'DatumPlaatsing';
			$velden[] = 'Type';
			$velden[] = 'Ica';
			$velden[] = 'Iku';
			$velden[] = 'Na';
			$velden[] = 'Wis';
			$velden[] = 'Titel';
			$velden[] = 'Inleiding';
			$velden[] = 'Inhoud';
			$velden[] = 'Views';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle VacatureStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VacatureStudieFromVacatureVerz = VacatureStudieVerzameling::fromVacature($this);
		$returnValue = $VacatureStudieFromVacatureVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object VacatureStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $VacatureStudieFromVacatureVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Vacature`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Vacature`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Vacature terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'contractonderdeel':
			return 'foreign';
		case 'contractonderdeel_id':
			return 'int';
		case 'datumverloop':
			return 'date';
		case 'datumplaatsing':
			return 'date';
		case 'type':
			return 'enum';
		case 'ica':
			return 'bool';
		case 'iku':
			return 'bool';
		case 'na':
			return 'bool';
		case 'wis':
			return 'bool';
		case 'titel':
			return 'string';
		case 'inleiding':
			return 'html';
		case 'inhoud':
			return 'html';
		case 'views':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'contractonderdeel_id':
		case 'ica':
		case 'iku':
		case 'na':
		case 'wis':
		case 'views':
			$type = '%i';
			break;
		case 'datumVerloop':
		case 'datumPlaatsing':
		case 'type':
		case 'titel':
		case 'inleiding':
		case 'inhoud':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Vacature`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Vacature`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Vacature');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle VacatureStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VacatureStudieFromVacatureVerz = VacatureStudieVerzameling::fromVacature($this);
		$dependencies['VacatureStudie'] = $VacatureStudieFromVacatureVerz;

		return $dependencies;
	}
}
