<?
/**
 * @brief 'AT'AUTH_GET:spookweb,AUTH_SET:spookweb
 */
abstract class Contractonderdeel_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $contract;
	protected $contract_contractID;		/**< \brief PRIMARY */
	protected $soort;					/**< \brief ENUM:VAKID/VACATURE/MAILING/PROFIEL/LEZING/OVERIG */
	protected $bedrag;					/**< \brief NULL Moet misschien string ipv int worden (percentages e.d....) */
	protected $uitvoerDatum;			/**< \brief NULL */
	protected $omschrijving;			/**< \brief NULL */
	protected $uitgevoerd;				/**< \brief Value geeft zo mogelijk id van de uitvoering aan. Samen met soort kan je het object achterhalen. NULL */
	protected $verantwoordelijke;		/**< \brief NULL */
	protected $verantwoordelijke_contactID;/**< \brief PRIMARY */
	protected $gefactureerd;
	protected $commissie;				/**< \brief De commissie waar dit onderdeel voor is. NULL */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $latexString;				/**< \brief De latex output om een contract te maken. NULL */
	protected $annulering;				/**< \brief Is er een annuleringsclausule van toepassing op dit onderdeel? */
	/** Verzamelingen **/
	protected $vacatureVerzameling;
	protected $bedrijfsprofielVerzameling;
	/**
	 * @brief De constructor van de Contractonderdeel_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->id = NULL;
		$this->contract = NULL;
		$this->contract_contractID = 0;
		$this->soort = 'OVERIG';
		$this->bedrag = 0;
		$this->uitvoerDatum = new DateTimeLocale(NULL);
		$this->omschrijving = NULL;
		$this->uitgevoerd = NULL;
		$this->verantwoordelijke = NULL;
		$this->verantwoordelijke_contactID = NULL;
		$this->gefactureerd = False;
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->latexString = NULL;
		$this->annulering = False;
		$this->vacatureVerzameling = NULL;
		$this->bedrijfsprofielVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld contract.
	 *
	 * @return Contract
	 * De waarde van het veld contract.
	 */
	public function getContract()
	{
		if(!isset($this->contract)
		 && isset($this->contract_contractID)
		 ) {
			$this->contract = Contract::geef
					( $this->contract_contractID
					);
		}
		return $this->contract;
	}
	/**
	 * @brief Stel de waarde van het veld contract in.
	 *
	 * @param mixed $new_contractID De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setContract($new_contractID)
	{
		unset($this->errors['Contract']);
		if($new_contractID instanceof Contract
		) {
			if($this->contract == $new_contractID
			&& $this->contract_contractID == $this->contract->getContractID())
				return $this;
			$this->contract = $new_contractID;
			$this->contract_contractID
					= $this->contract->getContractID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contractID)
		) {
			if($this->contract == NULL 
				&& $this->contract_contractID == (int)$new_contractID)
				return $this;
			$this->contract = NULL;
			$this->contract_contractID
					= (int)$new_contractID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld contract geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld contract geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkContract()
	{
		if (array_key_exists('Contract', $this->errors))
			return $this->errors['Contract'];
		$waarde1 = $this->getContract();
		$waarde2 = $this->getContractContractID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld contract_contractID.
	 *
	 * @return int
	 * De waarde van het veld contract_contractID.
	 */
	public function getContractContractID()
	{
		if (is_null($this->contract_contractID) && isset($this->contract)) {
			$this->contract_contractID = $this->contract->getContractID();
		}
		return $this->contract_contractID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('VAKID','VACATURE','MAILING','PROFIEL','LEZING','OVERIG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bedrag.
	 *
	 * @return int
	 * De waarde van het veld bedrag.
	 */
	public function getBedrag()
	{
		return $this->bedrag;
	}
	/**
	 * @brief Stel de waarde van het veld bedrag in.
	 *
	 * @param mixed $newBedrag De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setBedrag($newBedrag)
	{
		unset($this->errors['Bedrag']);
		if(!is_null($newBedrag))
			$newBedrag = (int)$newBedrag;
		if($this->bedrag === $newBedrag)
			return $this;

		$this->bedrag = $newBedrag;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bedrag geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bedrag geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkBedrag()
	{
		if (array_key_exists('Bedrag', $this->errors))
			return $this->errors['Bedrag'];
		$waarde = $this->getBedrag();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld uitvoerDatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld uitvoerDatum.
	 */
	public function getUitvoerDatum()
	{
		return $this->uitvoerDatum;
	}
	/**
	 * @brief Stel de waarde van het veld uitvoerDatum in.
	 *
	 * @param mixed $newUitvoerDatum De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setUitvoerDatum($newUitvoerDatum)
	{
		unset($this->errors['UitvoerDatum']);
		if(!$newUitvoerDatum instanceof DateTimeLocale) {
			try {
				$newUitvoerDatum = new DateTimeLocale($newUitvoerDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->uitvoerDatum->strftime('%F %T') == $newUitvoerDatum->strftime('%F %T'))
			return $this;

		$this->uitvoerDatum = $newUitvoerDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld uitvoerDatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld uitvoerDatum geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkUitvoerDatum()
	{
		if (array_key_exists('UitvoerDatum', $this->errors))
			return $this->errors['UitvoerDatum'];
		$waarde = $this->getUitvoerDatum();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld uitgevoerd.
	 *
	 * @return string
	 * De waarde van het veld uitgevoerd.
	 */
	public function getUitgevoerd()
	{
		return $this->uitgevoerd;
	}
	/**
	 * @brief Stel de waarde van het veld uitgevoerd in.
	 *
	 * @param mixed $newUitgevoerd De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setUitgevoerd($newUitgevoerd)
	{
		unset($this->errors['Uitgevoerd']);
		if(!is_null($newUitgevoerd))
			$newUitgevoerd = trim($newUitgevoerd);
		if($newUitgevoerd === "")
			$newUitgevoerd = NULL;
		if($this->uitgevoerd === $newUitgevoerd)
			return $this;

		$this->uitgevoerd = $newUitgevoerd;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld uitgevoerd geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld uitgevoerd geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkUitgevoerd()
	{
		if (array_key_exists('Uitgevoerd', $this->errors))
			return $this->errors['Uitgevoerd'];
		$waarde = $this->getUitgevoerd();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld verantwoordelijke.
	 *
	 * @return Persoon
	 * De waarde van het veld verantwoordelijke.
	 */
	public function getVerantwoordelijke()
	{
		if(!isset($this->verantwoordelijke)
		 && isset($this->verantwoordelijke_contactID)
		 ) {
			$this->verantwoordelijke = Persoon::geef
					( $this->verantwoordelijke_contactID
					);
		}
		return $this->verantwoordelijke;
	}
	/**
	 * @brief Stel de waarde van het veld verantwoordelijke in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setVerantwoordelijke($new_contactID)
	{
		unset($this->errors['Verantwoordelijke']);
		if($new_contactID instanceof Persoon
		) {
			if($this->verantwoordelijke == $new_contactID
			&& $this->verantwoordelijke_contactID == $this->verantwoordelijke->getContactID())
				return $this;
			$this->verantwoordelijke = $new_contactID;
			$this->verantwoordelijke_contactID
					= $this->verantwoordelijke->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->verantwoordelijke == NULL && $this->verantwoordelijke_contactID == NULL)
				return $this;
			$this->verantwoordelijke = NULL;
			$this->verantwoordelijke_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->verantwoordelijke == NULL 
				&& $this->verantwoordelijke_contactID == (int)$new_contactID)
				return $this;
			$this->verantwoordelijke = NULL;
			$this->verantwoordelijke_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld verantwoordelijke geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld verantwoordelijke geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVerantwoordelijke()
	{
		if (array_key_exists('Verantwoordelijke', $this->errors))
			return $this->errors['Verantwoordelijke'];
		$waarde1 = $this->getVerantwoordelijke();
		$waarde2 = $this->getVerantwoordelijkeContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld verantwoordelijke_contactID.
	 *
	 * @return int
	 * De waarde van het veld verantwoordelijke_contactID.
	 */
	public function getVerantwoordelijkeContactID()
	{
		if (is_null($this->verantwoordelijke_contactID) && isset($this->verantwoordelijke)) {
			$this->verantwoordelijke_contactID = $this->verantwoordelijke->getContactID();
		}
		return $this->verantwoordelijke_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld gefactureerd.
	 *
	 * @return bool
	 * De waarde van het veld gefactureerd.
	 */
	public function getGefactureerd()
	{
		return $this->gefactureerd;
	}
	/**
	 * @brief Stel de waarde van het veld gefactureerd in.
	 *
	 * @param mixed $newGefactureerd De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setGefactureerd($newGefactureerd)
	{
		unset($this->errors['Gefactureerd']);
		if(!is_null($newGefactureerd))
			$newGefactureerd = (bool)$newGefactureerd;
		if($this->gefactureerd === $newGefactureerd)
			return $this;

		$this->gefactureerd = $newGefactureerd;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld gefactureerd geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld gefactureerd geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkGefactureerd()
	{
		if (array_key_exists('Gefactureerd', $this->errors))
			return $this->errors['Gefactureerd'];
		$waarde = $this->getGefactureerd();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_commissieID) || $new_commissieID == 0)) {
			if($this->commissie == NULL && $this->commissie_commissieID == NULL)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld latexString.
	 *
	 * @return string
	 * De waarde van het veld latexString.
	 */
	public function getLatexString()
	{
		return $this->latexString;
	}
	/**
	 * @brief Stel de waarde van het veld latexString in.
	 *
	 * @param mixed $newLatexString De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setLatexString($newLatexString)
	{
		unset($this->errors['LatexString']);
		if(!is_null($newLatexString))
			$newLatexString = trim($newLatexString);
		if($newLatexString === "")
			$newLatexString = NULL;
		if($this->latexString === $newLatexString)
			return $this;

		$this->latexString = $newLatexString;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld latexString geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld latexString geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkLatexString()
	{
		if (array_key_exists('LatexString', $this->errors))
			return $this->errors['LatexString'];
		$waarde = $this->getLatexString();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld annulering.
	 *
	 * @return bool
	 * De waarde van het veld annulering.
	 */
	public function getAnnulering()
	{
		return $this->annulering;
	}
	/**
	 * @brief Stel de waarde van het veld annulering in.
	 *
	 * @param mixed $newAnnulering De nieuwe waarde.
	 *
	 * @return Contractonderdeel
	 * Dit Contractonderdeel-object.
	 */
	public function setAnnulering($newAnnulering)
	{
		unset($this->errors['Annulering']);
		if(!is_null($newAnnulering))
			$newAnnulering = (bool)$newAnnulering;
		if($this->annulering === $newAnnulering)
			return $this;

		$this->annulering = $newAnnulering;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld annulering geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld annulering geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAnnulering()
	{
		if (array_key_exists('Annulering', $this->errors))
			return $this->errors['Annulering'];
		$waarde = $this->getAnnulering();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de VacatureVerzameling die hoort bij dit object.
	 */
	public function getVacatureVerzameling()
	{
		if(!$this->vacatureVerzameling instanceof VacatureVerzameling)
			$this->vacatureVerzameling = VacatureVerzameling::fromContractonderdeel($this);
		return $this->vacatureVerzameling;
	}
	/**
	 * @brief Returneert de BedrijfsprofielVerzameling die hoort bij dit object.
	 */
	public function getBedrijfsprofielVerzameling()
	{
		if(!$this->bedrijfsprofielVerzameling instanceof BedrijfsprofielVerzameling)
			$this->bedrijfsprofielVerzameling = BedrijfsprofielVerzameling::fromContractonderdeel($this);
		return $this->bedrijfsprofielVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Contractonderdeel::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Contractonderdeel.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('spookweb');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Contractonderdeel::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Contractonderdeel|false
	 * Een Contractonderdeel-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'Contractonderdeel');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Contractonderdeel');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Contractonderdeel::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Contractonderdeel`.`id`'
		                 .     ', `Contractonderdeel`.`contract_contractID`'
		                 .     ', `Contractonderdeel`.`soort`'
		                 .     ', `Contractonderdeel`.`bedrag`'
		                 .     ', `Contractonderdeel`.`uitvoerDatum`'
		                 .     ', `Contractonderdeel`.`omschrijving`'
		                 .     ', `Contractonderdeel`.`uitgevoerd`'
		                 .     ', `Contractonderdeel`.`verantwoordelijke_contactID`'
		                 .     ', `Contractonderdeel`.`gefactureerd`'
		                 .     ', `Contractonderdeel`.`commissie_commissieID`'
		                 .     ', `Contractonderdeel`.`latexString`'
		                 .     ', `Contractonderdeel`.`annulering`'
		                 .     ', `Contractonderdeel`.`gewijzigdWanneer`'
		                 .     ', `Contractonderdeel`.`gewijzigdWie`'
		                 .' FROM `Contractonderdeel`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new Contractonderdeel();

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->contract_contractID  = (int) $row['contract_contractID'];
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->bedrag  = (int) $row['bedrag'];
			$obj->uitvoerDatum  = new DateTimeLocale($row['uitvoerDatum']);
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->uitgevoerd  = (is_null($row['uitgevoerd'])) ? null : trim($row['uitgevoerd']);
			$obj->verantwoordelijke_contactID  = (is_null($row['verantwoordelijke_contactID'])) ? null : (int) $row['verantwoordelijke_contactID'];
			$obj->gefactureerd  = (bool) $row['gefactureerd'];
			$obj->commissie_commissieID  = (is_null($row['commissie_commissieID'])) ? null : (int) $row['commissie_commissieID'];
			$obj->latexString  = (is_null($row['latexString'])) ? null : trim($row['latexString']);
			$obj->annulering  = (bool) $row['annulering'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Contractonderdeel')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getContractContractID();
		$this->getVerantwoordelijkeContactID();
		$this->getCommissieCommissieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `Contractonderdeel`'
			          . ' (`contract_contractID`, `soort`, `bedrag`, `uitvoerDatum`, `omschrijving`, `uitgevoerd`, `verantwoordelijke_contactID`, `gefactureerd`, `commissie_commissieID`, `latexString`, `annulering`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %i, %s, %s, %s, %i, %i, %i, %s, %i, %s, %i)'
			          , $this->contract_contractID
			          , $this->soort
			          , $this->bedrag
			          , (!is_null($this->uitvoerDatum))?$this->uitvoerDatum->strftime('%F %T'):null
			          , $this->omschrijving
			          , $this->uitgevoerd
			          , $this->verantwoordelijke_contactID
			          , $this->gefactureerd
			          , $this->commissie_commissieID
			          , $this->latexString
			          , $this->annulering
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Contractonderdeel')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Contractonderdeel`'
			          .' SET `contract_contractID` = %i'
			          .   ', `soort` = %s'
			          .   ', `bedrag` = %i'
			          .   ', `uitvoerDatum` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `uitgevoerd` = %s'
			          .   ', `verantwoordelijke_contactID` = %i'
			          .   ', `gefactureerd` = %i'
			          .   ', `commissie_commissieID` = %i'
			          .   ', `latexString` = %s'
			          .   ', `annulering` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->contract_contractID
			          , $this->soort
			          , $this->bedrag
			          , (!is_null($this->uitvoerDatum))?$this->uitvoerDatum->strftime('%F %T'):null
			          , $this->omschrijving
			          , $this->uitgevoerd
			          , $this->verantwoordelijke_contactID
			          , $this->gefactureerd
			          , $this->commissie_commissieID
			          , $this->latexString
			          , $this->annulering
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenContractonderdeel
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenContractonderdeel($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenContractonderdeel($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Contract';
			$velden[] = 'Soort';
			$velden[] = 'Bedrag';
			$velden[] = 'UitvoerDatum';
			$velden[] = 'Omschrijving';
			$velden[] = 'Uitgevoerd';
			$velden[] = 'Verantwoordelijke';
			$velden[] = 'Gefactureerd';
			$velden[] = 'Commissie';
			$velden[] = 'LatexString';
			$velden[] = 'Annulering';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Contract';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Contract';
			$velden[] = 'Soort';
			$velden[] = 'Bedrag';
			$velden[] = 'UitvoerDatum';
			$velden[] = 'Omschrijving';
			$velden[] = 'Uitgevoerd';
			$velden[] = 'Verantwoordelijke';
			$velden[] = 'Gefactureerd';
			$velden[] = 'Commissie';
			$velden[] = 'LatexString';
			$velden[] = 'Annulering';
			break;
		case 'get':
			$velden[] = 'Contract';
			$velden[] = 'Soort';
			$velden[] = 'Bedrag';
			$velden[] = 'UitvoerDatum';
			$velden[] = 'Omschrijving';
			$velden[] = 'Uitgevoerd';
			$velden[] = 'Verantwoordelijke';
			$velden[] = 'Gefactureerd';
			$velden[] = 'Commissie';
			$velden[] = 'LatexString';
			$velden[] = 'Annulering';
		case 'primary':
			$velden[] = 'contract_contractID';
			$velden[] = 'verantwoordelijke_contactID';
			$velden[] = 'commissie_commissieID';
			break;
		case 'verzamelingen':
			$velden[] = 'VacatureVerzameling';
			$velden[] = 'BedrijfsprofielVerzameling';
			break;
		default:
			$velden[] = 'Contract';
			$velden[] = 'Soort';
			$velden[] = 'Bedrag';
			$velden[] = 'UitvoerDatum';
			$velden[] = 'Omschrijving';
			$velden[] = 'Uitgevoerd';
			$velden[] = 'Verantwoordelijke';
			$velden[] = 'Gefactureerd';
			$velden[] = 'Commissie';
			$velden[] = 'LatexString';
			$velden[] = 'Annulering';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Vacature-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VacatureFromContractonderdeelVerz = VacatureVerzameling::fromContractonderdeel($this);
		$returnValue = $VacatureFromContractonderdeelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Vacature met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Bedrijfsprofiel-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BedrijfsprofielFromContractonderdeelVerz = BedrijfsprofielVerzameling::fromContractonderdeel($this);
		$returnValue = $BedrijfsprofielFromContractonderdeelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Bedrijfsprofiel met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $VacatureFromContractonderdeelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BedrijfsprofielFromContractonderdeelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Contractonderdeel`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Contractonderdeel`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Contractonderdeel terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'contract':
			return 'foreign';
		case 'contract_contractid':
			return 'int';
		case 'soort':
			return 'enum';
		case 'bedrag':
			return 'int';
		case 'uitvoerdatum':
			return 'date';
		case 'omschrijving':
			return 'text';
		case 'uitgevoerd':
			return 'string';
		case 'verantwoordelijke':
			return 'foreign';
		case 'verantwoordelijke_contactid':
			return 'int';
		case 'gefactureerd':
			return 'bool';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'latexstring':
			return 'text';
		case 'annulering':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'contract_contractID':
		case 'bedrag':
		case 'verantwoordelijke_contactID':
		case 'gefactureerd':
		case 'commissie_commissieID':
		case 'annulering':
			$type = '%i';
			break;
		case 'soort':
		case 'uitvoerDatum':
		case 'omschrijving':
		case 'uitgevoerd':
		case 'latexString':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Contractonderdeel`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Contractonderdeel`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Contractonderdeel');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Vacature-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VacatureFromContractonderdeelVerz = VacatureVerzameling::fromContractonderdeel($this);
		$dependencies['Vacature'] = $VacatureFromContractonderdeelVerz;

		// Verzamel alle Bedrijfsprofiel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BedrijfsprofielFromContractonderdeelVerz = BedrijfsprofielVerzameling::fromContractonderdeel($this);
		$dependencies['Bedrijfsprofiel'] = $BedrijfsprofielFromContractonderdeelVerz;

		return $dependencies;
	}
}
