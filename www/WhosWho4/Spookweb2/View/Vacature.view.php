<?
/**
 * $Id$
 */
abstract class VacatureView
	extends VacatureView_Generated
{

	public static function details(Vacature $vacature)
	{
		$pageDiv = new HtmlDiv();
		$bedrijf = $vacature->getBedrijf();

		$pageDiv->add($logodiv = new HtmlDiv(array($img = new HtmlImage($bedrijf->getLogo(),_("Bedrijfslogo"), null, "vacaturelogo")), "vacaturelogodiv" ) )
			->add(new HtmlHeader(2, VacatureView::waardeTitel($vacature)))
			->add(new HtmlParagraph("(" . VacatureView::waardeType($vacature) ." "._("voor")." ". VacatureView::waardeStudies($vacature) .")"))
			->add(new HtmlParagraph(Vacatureview::waardeInhoud($vacature)));

		$img->setLazyLoad();

		return $pageDiv;
	}

	public static function wijzigen(Vacature $vacature, $show_error)
	{
		$page = static::pageLinks($vacature);
		$page->start();
		$pagediv = new HtmlDiv();

		$obj = $vacature;

		/** Dit wordt het formulier **/
		$form = HtmlForm::named('vacatureWijzig');

		/** Eerst de div voor persoonsinformatie in te vullen **/
		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($obj, 'DatumVerloop', $show_error))
			->add(self::wijzigTR($obj, 'DatumPlaatsing', $show_error))
			->add(self::wijzigTR($obj, 'Type', $show_error))
			->add(self::wijzigTR($obj, 'Ica', $show_error))
			->add(self::wijzigTR($obj, 'Iku', $show_error))
			->add(self::wijzigTR($obj, 'Na', $show_error))
			->add(self::wijzigTR($obj, 'Wis', $show_error))
			->add(self::wijzigTR($obj, 'Titel', $show_error))
			->add(self::wijzigTR($obj, 'Inleiding', $show_error))
			->add(self::wijzigTR($obj, 'Inhoud', $show_error));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB())
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));

		$pagediv->add($form);

		$page->add($pagediv);
		$page->end();
	}

	/**
	 *  Opmerkingen, labels, etc etc...
	 **/
	public static function labelenumType($value)
	{
		switch ($value)
		{
			case 'BIJBAAN': return _('Bijbaan');
			case 'PROMOTIEPLAATS': return _('Promotieplaats');
			case 'STAGEPLEK': return _('Stageplek');
			case 'STARTERSFUNCTIE': return _('Startersfunctie');
			case 'AFSTUDEEROPDRACHT': return _('Afstudeeropdracht');
			case 'WERKSTUDENTSCHAP': return _('Werkstudentschap');
			default: return parent::labelenumType($value);
		}
	}

	public static function labelDatumVerloop(Vacature $obj)
	{
		return _('Verloopdatum');
	}
	public static function labelDatumPlaatsing(Vacature $obj)
	{
		return _('Datum van plaatsing');
	}
	public static function labelType(Vacature $obj)
	{
		return _('Type');
	}
	public static function labelIca(Vacature $obj)
	{
		return _('Informatica');
	}
	public static function labelIku(Vacature $obj)
	{
		return _('Informatiekunde');
	}
	public static function labelNa(Vacature $obj)
	{
		return _('Natuurkunde');
	}
	public static function labelWis(Vacature $obj)
	{
		return _('Wiskunde');
	}
	public static function labelTitel(Vacature $obj)
	{
		return _('Titel');
	}
	public static function labelInleiding(Vacature $obj)
	{
		return _('Inleiding');
	}
	public static function labelInhoud(Vacature $obj)
	{
		return _('Inhoud');
	}

	/**
	 *  Custom waardes
	 **/
	//Maak string van alle studies die bij deze vacature passen
	public static function waardeStudies(Vacature $obj)
	{
		$studies = array();
		if($obj->getIca()) $studies[] = self::labelIca($obj);
		if($obj->getIku()) $studies[] = self::labelIku($obj);
		if($obj->getNa()) $studies[] = self::labelNa($obj);
		if($obj->getWis()) $studies[] = self::labelWis($obj);

		if(empty($studies))
			return _("alle studies");

		return implode(", ", $studies);
	}

	public static function waardePeriode(Vacature $obj)
	{
		$periode = date_diff($obj->getDatumVerloop(), $obj->getDatumPlaatsing());
		$periodestring = implode(array(($periode->format("%d")?$periode->format("%d") . " dag(en)":null)
			, ($periode->format("%m")?$periode->format("%m") . " maand(en)":null)
			, ($periode->format("%y")?$periode->format("%y") . " jaar(en)":null)), " ");

		return $periodestring;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
