<?
/**
 * $Id$
 */
abstract class BedrijfVerzamelingView
	extends BedrijfVerzamelingView_Generated
{

	public static function htmlList($bedrijven)
	{
		if($bedrijven->aantal() == 0)
			return new HtmlParagraph(_("Geen bedrijven gevonden of weer te geven."));

		$page = new HtmlDiv();

		$table = new HtmlTable();
		$table->add(new HtmlTableHead(new HtmlTableRow(array(
						new HtmlTableHeaderCell(_("Naam")), new HtmlTableHeaderCell(_("Type")),
						new HtmlTableHeaderCell(_("Status")), new HtmlTableHeaderCell(_("Website")),
						new HtmlTableHeaderCell(_("Omschrijving"))))));

		foreach($bedrijven as $bedrijf){
			$row = new HtmlTableRow(array(
						new HtmlTableDataCell(new HtmlAnchor($bedrijf->url(),$bedrijf->getNaam())),
						new HtmlTableDataCell(BedrijfView::waardeType($bedrijf)),
						new HtmlTableDataCell(BedrijfView::waardeStatus($bedrijf)),
						new HtmlTableDataCell(new HtmlAnchor($bedrijf->getHomepage(),$bedrijf->getHomepage())),
						new HtmlTableDataCell($bedrijf->getOmschrijving())
						));

			$table->add($row);
		}

		$page->add($table);

		return $page;
	}

	public static function mijnList($bedrijven)
	{
		$page = new HtmlDiv();

		$table = new HtmlTable();
		$table->add(new HtmlTableHead(new HtmlTableRow(array(
			new HtmlTableHeaderCell(_("Naam")), new HtmlTableHeaderCell(_("Acties"))
		))));

		foreach($bedrijven as $bedrijf){
			//TODO: placeholder smilies vervangen voor goede icons.
			$row = new HtmlTableRow(array(
						new HtmlTableDataCell(new HtmlAnchor($bedrijf->url(),$bedrijf->getNaam())),
						new HtmlTableDataCell(new HtmlAnchor($bedrijf->url() . '/Nieuwinteractie', HtmlSpan::fa('refresh', _("Nieuw interactie invoeren") )) . " " .
							new HtmlAnchor($bedrijf->url() . '/Nieuwcontract', HtmlSpan::fa('pencil-square-o', _("Nieuw contract invoeren"))) )
						));
			$table->add($row);
		}

		$page->add($table);

		return $page;
	}

	//Maak een mooie zoekformpje om in de db te zoeken op bedrijven.
	public static function zoekForm()
	{
		foreach(Bedrijf::enumsType() as $value)
		{
			$typearray[$value] = BedrijfView::labelenumType($value);
		}
		$typearray = array_merge(array( '' => 'Alle'), $typearray);
		foreach(Bedrijf::enumsStatus() as $value)
		{
			$statusarray[$value] = BedrijfView::labelenumStatus($value);
		}
		$statusarray = array_merge(array( '' => 'Alle'), $statusarray);

		$zoekForm = new HtmlForm('get');
		$zoekForm->addClass('form-inline');
		$zoektable = new HtmlTable();
		$zoektable->add(new HtmlTableRow(array(
					new HtmlTableDataCell(_("Bedrijfsnaam:")),
					new HtmlTableDataCell(HtmlInput::makeText("Naam", trypar('Naam'), 30)) )))
			->add(new HtmlTableRow(array(
					new HtmlTableDataCell(_("Type bedrijf:")),
					new HtmlTableDataCell($typeSelectbox = HtmlSelectbox::fromArray("Type", $typearray, trypar('Type'), 1) ))))
			->add(new HtmlTableRow(array(
					new HtmlTableDataCell(_("Status van bedrijf:")),
					new HtmlTableDataCell($statusSelectbox = HtmlSelectbox::fromArray("Status", $statusarray, trypar('Status'), 1)))));
		$zoekForm->add($zoektable)
			->add(HtmlInput::makeSubmitButton("Go go go!", "gogogo"));

		$typeSelectbox->addClass("zoekSelectboxSmall");
		$statusSelectbox->addClass("zoekSelectboxSmall");

		return $zoekForm;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
