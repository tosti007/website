<?
/**
 * $Id$
 */
abstract class InteractieView
	extends InteractieView_Generated
{

	static public function details(Interactie $interactie)
	{
		$bedrijfpersoon = $interactie->getBedrijfPersoon();
		$naam = _("Onbekend");
		if($bedrijfpersoon)
			$naam = PersoonView::naam($bedrijfpersoon->getPersoon());

		$pageDiv = new HtmlDiv();
		$pageDiv->add(InteractieView::viewInfo($interactie, true))
				->add(HtmlAnchor::button($interactie->getBedrijf()->url(), _('Naar bedrijf')));

		$page = static::pageLinks($interactie);
		$page->start()
			->add($pageDiv);
		return $page;
	}

	/**
	 * Toont het formulier voor het wijzigen van een interactie
	 *
	 * @return een HTMLPage.
	 */
	static public function wijzigen($interactie, $show_error = true)
	{
		$page = static::pageLinks($interactie);
		$page->start(BedrijfView::waardeNaam($interactie->getBedrijf()) . "> Interactie");
		$page->add(self::wijzigForm('InteractieWijzig', $interactie, $show_error));
		return $page;
	}

	/**
	 * Geef een formulier voor het wijzigen van een interactie terug
	 */
	static public function wijzigForm ($name = 'InteractieWijzig', $obj = null, $show_error = true)
	{
		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		// TODO: gebruik createForm en maak onderscheid tussen een nieuwe interactie en er eentje wijzigen!
		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($obj, 'Spook', $show_error))
			->add(self::wijzigTR($obj, 'BedrijfPersoon', $show_error))
			->add(self::wijzigTR($obj, 'Commissie', $show_error))
			->add(self::wijzigTR($obj, 'Datum', $show_error))
			->add(self::wijzigTR($obj, 'Medium', $show_error))
			->add(self::wijzigTR($obj, 'Todo', $show_error))
			->add(self::wijzigTR($obj, 'Inhoud', $show_error));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB())
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));
		return $form;
	}

	/**
	 *  Geeft een mooie entry weer van een interactie, is onderdeel van een tabel?
	 **/
	public static function lijstEntry($interactie, $bedrijfinfo = false, $class = null)
	{
		$rows = array($inforegel = new HtmlTableRow(), $inhoudregel = new HtmlTableRow());

		$inforegel->add(array(
						new HtmlTableDataCell(self::waardeDatum($interactie), 'strongtext'),
						$strong = new HtmlTableDataCell(
							sprintf(_("%s[VOC: spook] &#8904 %s[VOC: contact] (voor %s[VOC:commissie]%s[VOC: met bedrijf], %s[VOC:interactiemedium; per post, op gesprek, ...])"),
								self::waardeSpook($interactie),
								self::waardeBedrijfPersoon($interactie),
								self::waardeCommissie($interactie),
								($bedrijfinfo?sprintf(_("[VOC:interactie] met %s[VOC:bedrijf]"), self::waardeBedrijf($interactie)):''),
								strtolower(self::waardeMedium($interactie))
							),
							'strongtext')

						));
		$strong->add(new HtmlAnchor($interactie->wijzigURL(), HtmlSpan::fa('pencil', _("Wijzig interactie"))));
		$inhoudregel->add(array(
						new HtmlTableDataCell(self::waardeTodoUitroepteken($interactie)),
						new HtmlTableDataCell(self::waardeInhoud($interactie))
						));
		if($class)
		{
			$inforegel->addClass($class);
			$inhoudregel->addClass($class);
		}

		return $rows;
	}

	/**
	 * Custom invullingen voor de views
	 **/

	public static function waardeSpook(Interactie $obj)
	{
		$spook = $obj->getSpook();
		if(!$spook)
			return null;
		if($spook->magBekijken()){
			return new HtmlAnchor($spook->url(),PersoonView::naam($spook->getPersoon()));
		} else {
			return PersoonView::naam($spook->getPersoon());
		}
	}
	public static function waardeBedrijf(Interactie $obj)
	{
		$bedrijf = $obj->getBedrijf();
		if($bedrijf->magBekijken()){
			return new HtmlAnchor($bedrijf->url(),BedrijfView::waardeNaam($bedrijf));
		} else {
			return BedrijfView::waardeNaam($bedrijf);
		}
	}
	public static function waardeBedrijfPersoon(Interactie $obj)
	{
		$bedrijfpersoon = $obj->getBedrijfPersoon();
		if(!$bedrijfpersoon)
			return _("Onbekend");
		if($bedrijfpersoon->magBekijken()){
			return new HtmlAnchor($bedrijfpersoon->url(),PersoonView::naam($bedrijfpersoon->getPersoon()));
		} else {
			return PersoonView::naam($bedrijfpersoon->getPersoon());
		}
	}
	public static function waardeCommissie(Interactie $obj)
	{
		$commissie = $obj->getCommissie();
		if(!$commissie)
			return null;
		if($commissie->magBekijken()){
			return new HtmlAnchor($commissie->url(),CommissieView::waardeNaam($commissie));
		} else {
			return BedrijfView::waardeNaam($commissie);
		}
	}

	public static function waardeDatum(Interactie $obj)
	{
		$waarde = self::callobj($obj, 'get', 'Datum');
		if( is_null($waarde) ) {
			return null;
		} elseif( !($waarde instanceof DateTimeLocale)) {
			$waarde = new DateTimeLocale($waarde);
		}
		return $waarde->format('d-m-Y H:i');
	}

	public static function waardeTodoUitroepteken(Interactie $obj)
	{
		$waarde = self::callobj($obj, 'get', 'Todo');
		if($waarde){
			$uitroep =  new HtmlSpan(array("!"), "TodoUitroepteken");
			$uitroep->setAttribute('title','TODO!');
			return $uitroep;
		}
	}

	public static function formSpook(Interactie $obj, $include_id = false)
	{
		$spoken = new ContactPersoonVerzameling();
		//Zorg er voor dat het spook dat op dit moment bij de interactie staat er ook tussen staat.
		if($obj->getSpook())
			$spoken->voegtoe($obj->getSpook());

		//Laat alleen huidige spoken zien bij een nieuwe interactie en bij een wijziging van een interactie dus alle spoken.
		$selectie =  ($obj->getInDB()? "alle":"huidig");
		$spoken->union(ContactPersoonVerzameling::metConstraints($obj->getBedrijf(), null, $selectie, null, "SPOOK"));
		$spoken->sorteer("BeginDatum",true);
		
		//Als je geen spook bent en je wel op deze pagina uit komt gaan we er van uit dat je een sponsorcommissaris of aanverwante bent.
		// Dus als je zelf al niet in de spookselectie zit, geef dan jezelf als sponsorcommisaris.		
		if(!$spoken->bevat(implode("_", array (Persoon::getIngelogd()->geefID(),$obj->getBedrijf()->geefID(), "SPOOK" ))))
			$spoken->voegtoe(new ContactPersoon(Persoon::getIngelogd(),$obj->getBedrijf(), "OVERIGSPOOKWEB" ));
		
		
		$selected = array();
		if($obj->getSpook()){
			$selected[] = $obj->getSpook()->geefID();
		} else {
			//Probeer de huidige gebruiker te selecteren bij een nieuwe interactie als deze een spook is van dit bedrijf.
			$spook = ContactPersoonVerzameling::metConstraints($obj->getBedrijf(), null, 'alle', Persoon::getIngelogd()->geefID(), 'SPOOK');
			if($spook->aantal() > 0) $selected[] = $spook->first()->geefID();
		}

		return ContactPersoonVerzamelingView::htmlSelectBox($obj, $spoken, 'Spook', $selected);
	}

	public static function formBedrijfPersoon(Interactie $obj, $include_id = false)
	{
		//Laat alleen huidige selectie zien bij een nieuwe interactie
		$selectie =  ($obj->getInDB()? "alle":"huidig");

		$contacten = ContactPersoonVerzameling::metConstraints($obj->getBedrijf(), null, $selectie, null, 'Bedrijfscontact');
		$contacten->sorteer("BeginDatum", true);

		if($contacten->aantal() == 0 && !$obj->getInDB())
			return new HtmlAnchor($obj->getBedrijf()->url() . "/Nieuwbedrijfscontact",_("Voeg eerst een contact aan het bedrijf toe."));

		$selected = array();
		if($obj->getBedrijfPersoon())
			$selected[] = $obj->getBedrijfPersoon()->geefID();

		if(!$obj->getBedrijfPersoon() && $obj->getInDB())
			$selected[] = 0;

		return ContactPersoonVerzamelingView::htmlSelectBox($obj, $contacten, 'BedrijfPersoon', $selected);
	}

	public static function formCommissie(Interactie $obj, $include_id = false)
	{
		if($obj->getCommissie()){
			$selected = $obj->getCommissie()->geefID();
		} else {
			$selected = SPOCIE;
		}
		return self::defaultFormCommissieVerzameling($obj, 'Commissie', $selected);
	}

	/**
	 *  Labels etc etc...
	 **/

	public static function labelSpook(Interactie $obj)
	{
		return 'Spook';
	}
	public static function labelBedrijf(Interactie $obj)
	{
		return _('Bedrijf');
	}
	public static function labelBedrijfPersoon(Interactie $obj)
	{
		return _('Bedrijfscontact');
	}
	public static function labelCommissie(Interactie $obj)
	{
		return _('Commissie');
	}
	public static function labelDatum(Interactie $obj)
	{
		return _('Datum');
	}
	public static function labelInhoud(Interactie $obj)
	{
		return _('Inhoud');
	}
	public static function labelMedium(Interactie $obj)
	{
		return _('Medium');
	}
	public static function labelenumMedium($value)
	{
		switch ($value)
		{
			case 'EMAIL': return _('per email');
			case 'FAX': return _('per fax');
			case 'GESPREK': return _('op gesprek');
			case 'NOTITIE': return _('notitie');
			case 'ONBEKEND': return _('per onbekend');
			case 'OVERIG': return _('overig');
			case 'POST': return _('per post');
			case 'TELEFOON': return _('per telefoon');
			default: return parent::labelenumStatus($value);
		}
	}

	public static function labelTodo(Interactie $obj)
	{
		return 'Todo';
	}


	public static function opmerkingBedrijfPersoon()
	{
		return _('Wanneer er andere relevante e-mailadressen in de cc staan, <br> kan je deze expliciet in de inhoud van de interactie plaatsen.');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
