<?
/**
 * $Id$
 */
abstract class ContractonderdeelView
	extends ContractonderdeelView_Generated
{

	public static function latexString(Contractonderdeel $obj)
	{
		//Als er niets is geef dan niets terug zonder andere dingen te appenden
		if(!ContractonderdeelView::waardeLatexString($obj))
			return '';

		//Selecteer juiste type string, construeer en retourneer deze
		switch($obj->getSoort()) {
			case "PROFIEL":
				return "\\section{Bedrijfsprofiel}\n" . ContractonderdeelView::waardeLatexString($obj) . ($obj->getAnnulering()?"\n\annulering":"") . "\n\bedrag{{$obj->getBedrag()}}"; 
			case "LEZING":
				return "\\section{Lezing}\n" . ContractonderdeelView::waardeLatexString($obj) . ($obj->getAnnulering()?"\n\annulering":"") . "\n\bedrag{{$obj->getBedrag()}}";
			case "OVERIG":
				return ContractonderdeelView::waardeLatexString($obj) . ($obj->getAnnulering()?"\n\annulering":"") . "\n\bedrag{{$obj->getBedrag()}}";
			default:
				return ContractonderdeelView::waardeLatexString($obj) . ($obj->getAnnulering()?"\n\annulering":"");
		}
	}

	//Quick en dirty hack, FIXME!
	public static function wijzigtrhackvoorlatexstring(Contractonderdeel $obj)
	{
		return self::wijzigTR($obj, 'LatexString');
	}

	/**
	 * Toont het formulier voor het wijzigen van een contractonderdeel
	 */
	public static function wijzigen($obj, $form, $msg)
	{
		$page = static::pageLinks($obj);
		$page
			 ->start(null, 'leden')
			 ->add(new HtmlHeader(2, ($obj->getinDB()?new HtmlAnchor($obj->getContract()->url(), _("Contractonderdeel nr ") . $obj->geefID()):_('Nieuw contractonderdeel'))));
		if ($msg !== null)
			$page->add($msg);
		if ($form)
			if ($msg === null)
				$page->add(self::wijzigForm('ContractonderdeelWijzig', $obj, false));
			else
				$page->add(self::wijzigForm('ContractonderdeelWijzig', $obj, true));
		return $page;
	}

	/**
	 * Geef een formulier voor het wijzigen van een contractonderdeel terug
	 */
	public static function wijzigForm ($name = 'ContractonderdeelWijzig', $obj = null, $show_error = true)
	{
		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		$script = new HtmlScript();
		$script->setAttribute("src", Page::minifiedFile("SpookwebLatexGenerator.js"));
		$form->add($script);
		$script = HtmlScript::makeJavascript('$(function(){ customSelect(); });');
		$form->add($script);

		//Wil niet wijzigTR gaan overriden dus zoek het onderdeel dat we willen hebben op en pas dit aan.
		//TODO: dit kan vast beter worden opgelost door het toch wel via wijzigTR op te lossen...
		$bedragTR = self::wijzigTR($obj, 'Bedrag', $show_error);
		$bedragTD = $bedragTR->getChildren();
		$bedragbox = $bedragTD[1]->getChildren();
		$bedragbox[0]->setId("bedragid");

		$latexTR = self::wijzigTR($obj, 'LatexString', $show_error);
		$latexTD = $latexTR->getChildren();
		$latexString = $latexTD[1]->getChildren();
		$latexTD[1]->setId("latexstring");

		/** Eerst de div voor algemene informatie 
			Deze zal verschillend zijn afhankelijk of het onderdeel al is uitgevoerd.**/
		$form->add($div = new HtmlDiv());
		$form->add(HtmlInput::makeHidden("oldlatex",ContractonderdeelView::waardeLatexString($obj),null,"oldlatex"));
		$div->add(new HtmlHeader(3, _("Contractonderdeelinformatie")))
			->add($wijzigDiv = new HtmlDiv());

		$wijzigDiv->add(self::wijzigTR($obj, 'Contract', $show_error));

		/**Als het onderdeel nog niet uitgevoerd is wordt hier alles voor de javascript klaargezet.
			Als het onderdeel al wel uitgevoerd is, komt hier slecht een hiddeninput met een waarde die
			de javascript kan lezen.**/
		if($obj->GetUitgevoerd()==False)  {
			$soortTR = self::wijzigTR($obj, 'Soort', $show_error);
			$soortTD = $soortTR->getChildren();
			$selectbox = $soortTD[1]->getChildren();
			$selectbox[0]->setId("soortselectbox");
			$selectbox[0]->setAttribute("onchange","customSelect()");
			$wijzigDiv->add($soortTR);
		}
		else  {
			$form->add(HtmlInput::makeHidden("Soort", $obj->getSoort(), null, 'soortselectbox'));
		}

		$wijzigDiv->add($bedragTR)
			  ->add(self::wijzigTR($obj, 'Annulering', $show_error))
			  ->add(self::wijzigTR($obj, 'Verantwoordelijke', $show_error))
			  ->add(self::wijzigTR($obj, 'Uitgevoerd', $show_error))
			  ->add(self::wijzigTR($obj, 'Uitvoerdatum', $show_error))
			  ->add(self::wijzigTR($obj, 'Gefactureerd', $show_error))
			  ->add(self::wijzigTR($obj, 'Omschrijving', $show_error))
			  ->add($latexTR);

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB()) { 
			$form->add(new HtmlParagraph(_("Genereer een nieuwe latexstring zonder javascript "). " ". new HtmlAnchor($obj->url() . "/MaakLatex",_("hier")) ."."));
			$form->add(HtmlInput::makeSubmitButton(_("Wijzigen")));
		} else {
			$form->add(new HtmlParagraph(_("Mocht je geen javascript hebben sla dan eerst het contractonderdeel op, hierna kan je hier automagische een latex string maken.")));
			$form->add(HtmlInput::makeSubmitButton(_("Aanmaken!")));
		}

		return $form;

	}

	/**
	 * Verwerk het wijzig formulier
	 */
	public static function processWijzigForm ($obj)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($obj), null))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($obj);
	}

	public static function verwijderen($obj)
	{
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(3, sprintf(_('Contractonderdeel %s verwijderen'), $obj->geefID())))
			->add(new HtmlParagraph(_('Weet je zeker dat je dit contractonderdeel wilt verwijderen?')));

		$form = HtmlForm::named('ContractonderdeelVerwijderen');
		$form->add(HtmlInput::makeSubmitButton(_("Ik weet het zeker!")));
		$div->add($form);

		return $div;
	}

	/**
	 *  Labels etc, the fancy bunch.
	 **/

	public static function labelContract(Contractonderdeel $obj)
	{
		return _('Contract');
	}
	public static function labelSoort(Contractonderdeel $obj)
	{
		return _('Soort');
	}
	public static function labelBedrag(Contractonderdeel $obj)
	{
		return _('Bedrag');
	}
	public static function labelUitvoerDatum(Contractonderdeel $obj)
	{
		return _('Datum van uitvoering');
	}
	public static function labelOmschrijving(Contractonderdeel $obj)
	{
		return _('Omschrijving');
	}
	public static function labelAnnulering(Contractonderdeel $obj)
	{
		return _('Annuleringsclausule');
	}
	public static function labelUitgevoerd(Contractonderdeel $obj)
	{
		return _('Uitgevoerd');
	}
	public static function labelGefactureerd(Contractonderdeel $obj)
	{
		return _('Gefactureerd');
	}
	public static function labelVerantwoordelijke(Contractonderdeel $obj)
	{
		return _('Verantwoordelijke');
	}
	public static function labelenumSoort($value)
	{
		switch ($value)
		{
			case 'VAKID': return _('Vakidiootadvertentie');
			case 'VACATURE': return _('Vacature');
			case 'MAILING': return _('Mailing');
			case 'PROFIEL': return _('Bedrijfsprofiel');
			case 'OVERIG': return _('Overig (zie omschrijving)');
			case 'LEZING': return _('Lezing');
			default: return parent::labelenumSoort($value);
		}
	}

	public static function opmerkingUitgevoerd()
	{
		return _('0 of niets = niet uitgevoerd, -1 = niet via de website uitgevoerd en anders is het het idnummer van de uitvoering.');
	}

	public static function opmerkingGefactureerd()
	{
		return _('Geeft aan of dit losse onderdeel is gefactureerd.');
	}

	public static function opmerkingOmschrijving()
	{
		return _("Extra info over wat het contractonderdeel precies is.");
	}

	public static function opmerkingLatexString()
	{
		return _("Deze latex komt in de LaTeXversie van het contract te staan.");
	}

	public static function formContract(Contractonderdeel $obj, $include_id = false)
	{
		return self::waardeContract($obj);
	}

	public static function formVerantwoordelijke(Contractonderdeel $obj, $include_id = false)
	{
		$spoken = new PersoonVerzameling();

		//Als er geen spoken zijn en je wel op deze pagina uit komt gaan we er van uit dat je een sponsorcommissaris of aanverwante bent.
		// Geef dan de huidige gebruiker als selectie.
		$spoken->voegtoe(Persoon::getIngelogd());

		//Als je zeker bent dat het geen oude lul is laat die dan niet zien.
		$selectie = ($obj->getInDB()? "alle":"huidige");
		$spoken->union(ContactPersoonVerzameling::metConstraints($obj->getContract()->getBedrijf(), null, $selectie, null, 'SPOOK')->personen());
		$spoken->union(ContactPersoonVerzameling::metConstraints($obj->getContract()->getBedrijf(), null, $selectie, null, 'OVERIGSPOOKWEB')->personen());
		$spoken->union(CommissieLidVerzameling::vanCommissie(SPOCIE)->personen());

		$selected = 0;
		if($obj->getVerantwoordelijke())
			$selected = $obj->getVerantwoordelijke()->geefID();

		return PersoonVerzamelingView::htmlSelectBox($obj, $spoken, 'Verantwoordelijke', $selected, false, true);
		//TODO: onderstaande functie doet *bijna* het zelfde. Mergen is wenselijk.
		//return PersoonVerzamelingView::defaultFormPersoon($obj, 'Verantwoordelijke', '', $spoken);
	}

	//TODO: De generator handelt dit niet goed af, dus doen we het zelf maar even... FIXME!
	public static function waardeVerantwoordelijke(Contractonderdeel $obj)
	{
		if($obj->getVerantwoordelijke())
			return PersoonView::defaultWaardePersoon($obj->getVerantwoordelijke());
		return "Niemand";
	}

	public static function waardeBedrag(Contractonderdeel $obj)
	{
		return number_format(parent::waardeBedrag($obj), 2, ",",".");
	}

	//Maak er zo mogelijk een url van naar het uigevoerde ding.
	public static function waardeUitgevoerd(Contractonderdeel $obj, $tryHtml = true)
	{
		$string = parent::waardeUitgevoerd($obj);
		$link = '';
		//$string = 0 als hij niet is uitgevoerd
		if(!$string) {
			$string = _('nee');
			$link = _('uitvoeren');
		} else {
			$string = _('ja');
			if(in_array($obj->getSoort(), array("PROFIEL", "VACATURE")))
				$link = parent::waardeUitgevoerd($obj);
		}

		if($obj->magWijzigen() && $link && $tryHtml)
			return $string . ': '. new HtmlAnchor($obj->url() . '/Uitvoeren', $link);
		return $string;
	}

	public static function waardeContract(Contractonderdeel $obj)
	{
		$contract = $obj->getContract();
		return new HtmlAnchor($contract->url(), sprintf(_("Contract met %s"), BedrijfView::waardeNaam($contract->getBedrijf())));
	}

	//Zorg dat er een puur cijfermatige waarde komt te staan zodat we het makkelijk met js kunnen sorteren.
	public static function waardeUitvoerDatum(Contractonderdeel $obj)
	{
		$waarde = self::callobj($obj, 'get', "UitvoerDatum");
		return self::outputDateTime($waarde, "%d-%m-%Y");
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
