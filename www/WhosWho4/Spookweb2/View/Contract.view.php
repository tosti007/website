<?
/**
 * $Id$
 */
abstract class ContractView
	extends ContractView_Generated
{

	public static function details(Contract $contract)
	{
		$actieTable = new HtmlTable();
		$actieTable	->add(new HtmlTableRow(array(
			new HtmlTableDataCell(_("Nieuw contractonderdeel toevoegen:")),
			new HtmlTableDataCell(new HtmlAnchor('Nieuwonderdeel', HtmlSpan::fa('plus-circle', _("Contractonderdeel toevoegen") )))
		)))
		->add(new HtmlTableRow(array(
			new HtmlTableDataCell(_("Bekijk het financiele plaatje:")),
			new HtmlTableDataCell(new HtmlAnchor('Financieel', HtmlSpan::fa('euro', _("Financieel plaatje") )))
		)))
		->add(new HtmlTableRow(array(
			new HtmlTableDataCell(_("Bekijk de LaTeX versie:")),
			new HtmlTableDataCell(new HtmlAnchor('Latex', HtmlSpan::fa('file-excel-o', _("LaTeX versie") )))
		)));

		$pageDiv = new HtmlDiv();

		$pageDiv->add(ContractView::viewInfo($contract))
				->add(new HtmlHeader(3, _('Onderdelen')))
				->add(ContractonderdeelVerzamelingView::htmlLijst(ContractonderdeelVerzameling::metConstraints($contract->geefID())))
				->add(new HtmlHeader(3, _('Acties')))
				->add($actieTable);

		$page = static::pageLinks($contract);
		$page->start()->add($pageDiv);
		return $page;
	}

	public static function financieel(Contract $contract)
	{
		//TODO: dit is een luie oplossing, maar dit wordt in de toekomst vast een betere view ;-).
		$table = new HtmlTable(null, null, 'contractenInfoTable');
		$table->add(new HtmlTableHead(new HtmlTableRow(array(
						new HtmlTableHeaderCell(_("id")), new HtmlTableHeaderCell(_("Ingangs/uitvoerdatum")),
						new HtmlTableHeaderCell(_("Status/type")),
						new HtmlTableHeaderCell(_("Omschrijving")),
						new HtmlTableHeaderCell(_("Bedrag")),
						new HtmlTableHeaderCell(_("Verantwoordelijke"))
						))));
		ContractView::tableRowsMetOnderdelen($contract, $table);


		$pageDiv = new HtmlDiv();

		$pageDiv->add(new HtmlParagraph(_('Het financiele plaatje van dit contract met ' . BedrijfView::waardeNaam($contract->getBedrijf()))))
				->add($table)
				->add(new HtmlParagraph(_("Naar het contract: " .new HtmlAnchor($contract->url(), _("terug")) )));

		$page = static::pageLinks($contract);
		$page->start()->add($pageDiv);
		return $page;
	}

	public static function latexPagina(Contract $contract)
	{
		$latex = self::maakContractLatex($contract);
		//Vervangt alle html-linebreaks met latex-linebreaks.
		$latex = str_replace("<br />", '\\\\', $latex);

		$bedrijf = BedrijfView::waardeNaamFilename($contract->getBedrijf());
		$page = static::pageLinks($contract);

		$div = new HtmlDiv();
		$div->add(new HtmlParagraph(("Hieronder kan je latex bekijken van dit contract.")))
			->add(HtmlTextarea::withContent($latex, "", 100, 30))
			->add(new HtmlParagraph((" Je kan ook meteen de .tex downloaden: ")
					. new HtmlAnchor("Latexdownload", "contract_$bedrijf.tex" )))
			->add(new HtmlParagraph(_("Naar het contract: " .new HtmlAnchor($contract->url(), _("terug")) )));
		
		$page->start(_("LaTeX voor contract"))->add($div);
		return $page;
	}

	/**
	 *  Generereer de latex versie van het contract voor de spocie om zelf later nog te bewerken.
	 *  Let op, geeft veel standaardfuncties van de contractenclass die door de hektex worden beheerd,
	 *   als zij dus dingen aanpassen dan moet dat ook hier worden gefixxed.
	 **/
	public static function maakContractLatex(Contract $contract)
	{
		$tp = $contract->getBedrijf();

		//Probeer een goed adres te maken en zeg anders dat dit niet lukt.
		$tpAdres = ContactAdres::geefBySoort($tp, "BEZOEK");
		if(!$tpAdres)
			$tpAdres = ContactAdres::eersteBijContact($tp);

		if($tpAdres){
				$tpAdresString = ContactAdresView::latexAdres($tpAdres);
		} else {
			$tpAdresString = _("Geen adres bekend");
		}

		$tpKort = BedrijfView::waardeNaam($tp);
		$tpLang = BedrijfView::waardeNaam($tp) . "\\\\" . $tpAdresString;

		$latex = <<<HEREDOC
\documentclass{contract}
\setTegenpartijkort{{$tpKort}}
\settegenpartijkort{{$tpKort}}
\settegenpartij{{$tpLang}}
\welbtw
\begin{document}


HEREDOC;

		$contractonderdelen = ContractonderdeelVerzameling::metConstraints($contract->geefID());
		foreach($contractonderdelen as $contractonderdeel){
			$string = ContractonderdeelView::latexString($contractonderdeel);
			if(!$string)
				$string = _("%Geen latextekst gevonden voor dit contractonderdeel");
			$latex .= "$string\n\n";
		}

		$korting = $contract->getKorting();
		if(is_numeric($korting)) {
			$kortinglatex = "\kortingsbedrag{{$korting}}";
		} else {
			$korting = substr($korting, 0, -1);
			$kortinglatex = "\kortingspercentage{{$korting}}";
		}

		$latex .= <<<HEREDOC
$kortinglatex % default = 0
\end{document}
HEREDOC;

		return $latex;
	}

	public static function tableRowsMetOnderdelen(Contract $contract, $table)
	{
		$style = strtolower($contract->getStatus()) . 'style';
		$contractonderdelen = ContractonderdeelVerzameling::metConstraints($contract->geefID());
		$table->add(new HtmlTableRow(array(
						new HtmlTableDataCell(new HtmlAnchor($contract->url(),$contract->geefID())),
						new HtmlTableDataCell(ContractView::waardeIngangsdatum($contract)),
						new HtmlTableDataCell(ContractView::waardeStatus($contract), $style),
						new HtmlTableDataCell("Het gehele contract"),
						), 'contractInfoRow'));

		foreach($contractonderdelen as $onderdeel)
		{
		$table->add(new HtmlTableRow(array(
						new HtmlTableDataCell(ContractView::waardeContractID($contract).'-'.ContractonderdeelView::waardeID($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeUitvoerDatum($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeSoort($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeOmschrijving($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeBedrag($onderdeel)),
						new HtmlTableDataCell(ContractonderdeelView::waardeVerantwoordelijke($onderdeel))
						)));
		}

		$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell("Totaal", 'upperborder'),
					new HtmlTableDataCell(number_format($contract->totaalBedrag('subtotaal', false), 2, ",","."), 'upperborder'),
					new HtmlTableDataCell()
					)));

		$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell("Btw (" . BTWPERCENTAGE . "%)", 'upperborder'),
					new HtmlTableDataCell(number_format($contract->totaalBedrag('subtotaal'), 2, ",","."), 'upperborder'),
					new HtmlTableDataCell("Overleg voor een ander BTW-percentage </br> even met de Commissaris Extern.")
					)));

		$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell("Korting", 'upperborder'),
					new HtmlTableDataCell(ContractView::waardeKorting($contract), 'upperborder'),
					new HtmlTableDataCell()
					)));

		$table->add(new HtmlTableRow(array(
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell(),
					new HtmlTableDataCell("SubTotaal", 'upperborder'),
					new HtmlTableDataCell(number_format($contract->totaalBedrag(), 2, ",","."), 'upperborder'),
					new HtmlTableDataCell()
					)));

	}

	/**
	 * Toont het formulier voor het wijzigen van een contract
	 */
	static public function wijzigen($obj, $show_error)
	{
		$page = static::pageLinks($obj);
		$page->start(null, 'leden')
			 ->add(($obj->getIndb()?new HtmlHeader(2, new HtmlAnchor($obj->url(), _("Contract nr.") . $obj->geefID())):null));

		$page->add(self::wijzigForm('ContractWijzig', $obj, $show_error));
		return $page;
	}

	/**
	 * Geef een formulier voor het wijzigen van een contract terug
	 */
	static public function wijzigForm ($name = 'ContractWijzig', $obj = null, $show_error = true)
	{
		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		/** Eerst de div voor algemene informatie **/
		$form->add($div = new HtmlDiv());
		//TODO: bedrijf e.d. niet editbaar maken voor niet-bestuur?
		$div->add(self::wijzigTR($obj, 'Bedrijf', $show_error))
			->add(self::wijzigTR($obj, 'Status', $show_error))
			->add(self::wijzigTR($obj, 'Korting', $show_error))
			->add(self::wijzigTR($obj, 'Ingangsdatum', $show_error))
			->add(self::wijzigTR($obj, 'Opmerking', $show_error));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB())
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));

		return $form;
	}

	/**
	 * Verwerk het wijzig formulier
	 */
	static public function processWijzigForm ($obj)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($obj), null))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($obj);
	}

	/**
	 *  Labels en andere mooie dingen
	 **/
	public static function labelBedrijf(Contract $obj)
	{
		return _('Bedrijf');
	}
	public static function labelKorting(Contract $obj)
	{
		return _('Korting');
	}
	public static function labelStatus(Contract $obj)
	{
		return _('Status');
	}
	public static function labelIngangsdatum(Contract $obj)
	{
		return _('Ingangsdatum');
	}
	public static function labelOpmerking(Contract $obj)
	{
		return _('Opmerking');
	}

	public static function labelenumStatus($value)
	{
		switch ($value)
		{
			case 'GEFACTUREERD': return _('Gefactureerd');
			case 'MISLUKT': return _('Afgewezen door sponsor');
			case 'OPGESTUURD': return _('Opgestuurd naar sponsor');
			case 'RETOUR': return _('Retour, getekend en opgeslagen');
			case 'VOORSTEL': return _('Conceptvoorstel voor sponsor');
			default: return parent::labelenumStatus($value);
		}
	}

	public static function opmerkingKorting()
	{
		return _('Bedrag (, = .) of percentage (N%)');
	}

	public static function waardeBedrijf(Contract $obj)
	{
		$bedrijf = $obj->getBedrijf();
		if($bedrijf->magBekijken())
			return new HtmlAnchor($bedrijf->url(), BedrijfView::waardeNaam($bedrijf));
		return BedrijfView::waardeNaam($bedrijf);
	}

	public static function waardeKorting(Contract $obj)
	{
		$waarde = parent::waardeKorting($obj);
		if(is_numeric($waarde))
			return number_format($waarde, 2, ",",".");
		return $waarde;
	}

	public static function formBedrijf(Contract $obj, $include_id = false)
	{
		return self::waardeBedrijf($obj);
	}

	public static function formStatus(Contract $obj, $include_id = false)
	{
		if(!$obj->getInDB())
			return static::defaultFormEnum($obj, 'Status', $include_id, 'VOORSTEL');
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
