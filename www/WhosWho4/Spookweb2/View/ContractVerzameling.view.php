<?
/**
 * $Id$
 */
abstract class ContractVerzamelingView
	extends ContractVerzamelingView_Generated
{

	public static function htmlList(ContractVerzameling $contracten, $statusStyles = false, $bedrijfspagina = false)
	{
		$page = new HtmlDiv();
		$style = null;

		if($contracten->aantal() <= 0){
			return $page->add(new HtmlParagraph(_("Er zijn geen contracten gevonden om weer te geven.")));
		}

		$table = new HtmlTable();
		$table->add(new HtmlTableHead(new HtmlTableRow(array(
						new HtmlTableHeaderCell(_("id")), ($bedrijfspagina?null:new HtmlTableHeaderCell(_("Bedrijf"))),
						new HtmlTableHeaderCell(_("Status")), new HtmlTableHeaderCell(_("Korting")),
						new HtmlTableHeaderCell(_("Eindbedrag")), new HtmlTableHeaderCell(_("Ingangsdatum"))))));

		foreach($contracten as $contract)
		{
			$bedrijf = $contract->getBedrijf();
			if($statusStyles)
				$style = strtolower($contract->getStatus() . $contract->late()) . 'style';
			$row = new HtmlTableRow(array(
						new HtmlTableDataCell(new HtmlAnchor($contract->url(),$contract->geefID())),
						($bedrijfspagina?null:new HtmlTableDataCell(new HtmlAnchor($bedrijf->url(),$bedrijf->getNaam()))),
						new HtmlTableDataCell(ContractView::waardeStatus($contract), $style),
						new HtmlTableDataCell(ContractView::waardeKorting($contract)),
						new HtmlTableDataCell(number_format($contract->totaalBedrag(), 2, ",",".")),
						new HtmlTableDataCell(ContractView::waardeIngangsdatum($contract))
						));

			$table->add($row);
		}

		$page->add($table);

		return $page;
	}

	public static function zoekForm()
	{
		//Maak een mooie array voor de selectwaardes
		$statusarray = array_merge(array( '' => 'Alle'), array_combine(Contract::enumsStatus(), Contract::enumsStatus()));
		foreach(Contract::enumsStatus() as $value)
		{
			$statusarray[$value] = ucfirst(ContractView::labelenumStatus($value));
		}

		//Maak het daadwerkelijke form
		$zoekform = new HtmlForm('get');
		$zoektable = new HtmlTable();
		$zoektable->add(new HtmlTableRow(array(
					new HtmlTableDataCell("Op status:"),
					new HtmlTableDataCell($statusSelectbox = HtmlSelectbox::fromArray("Status", $statusarray, trypar('Status'), 1)))));
		$zoekform->add($zoektable);
			//FIXME: Als JS stuk is moet er eigenlijk wel een knoppie zijn, maar dat maakt nu auto-sumbit stuk.
			//->add(HtmlInput::makeSubmitButton("Zoeken"));

		//Maak de boel wat mooier en makkelijker
		$statusSelectbox->setAutoSubmit(true);
		$statusSelectbox->addClass("zoekSelectboxLarge");

		return $zoekform;
	}

	/**
	 *  Geef een lijst contracten weer samen met alle onderdelen van het contract.
	 **/
	public static function bedrijfDetailPagina(ContractVerzameling $contracten)
	{
		$page = new HtmlDiv();

		$table = new HtmlTable(null, null, 'contractenInfoTable');
		$table->add(new HtmlTableHead(new HtmlTableRow(array(
						new HtmlTableHeaderCell(_("id")), 
						new HtmlTableHeaderCell(_("Ingangs/uitvoerdatum")),
						new HtmlTableHeaderCell(_("Status/type")),
						new HtmlTableHeaderCell(_("Omschrijving")),
						new HtmlTableHeaderCell(_("Bedrag")),
						new HtmlTableHeaderCell(_("Verantwoordelijke"))
						))));

		foreach($contracten as $contract)
		{
			ContractView::tableRowsMetOnderdelen($contract, $table);
		}

		$page->add($table);

		return $page;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
