<?
/**
 * $Id$
 */
abstract class BedrijfsprofielVerzamelingView
	extends BedrijfsprofielVerzamelingView_Generated
{

	static public function profielenPagina($profielen, $random = true)
	{
		if($random) $profielen->shuffle();

		$div = new HtmlDiv();
		$div->add($thumbs = new HtmlDiv(null, 'bedrijfsprofiel-thumbs'));

		$i = 0;
		foreach($profielen as $profiel) {
			if(($i++ % 4) == 0) {
				// Er staan vier thumbnails op een regel
				$thumbs->add(new HtmlHR());
				$thumbs->add($row = new HtmlDiv(null, 'row is-table-row'));
			}

			$bedrijf = Bedrijf::vanProfiel($profiel);
			$img = new HtmlImage($bedrijf->getLogo(), $bedrijf->getNaam(), null, 'img-responsive img-thumbnail');
			$img->setLazyLoad();

			$a = new HtmlAnchor('#' . urlencode($bedrijf->getNaam()), $img);
			$row->add(new HtmlDiv($a, 'col-sm-3'));
		}

		$div->add(new HtmlHR());

		$li = new HtmlList();

		$light = false;
		foreach($profielen as $profiel)
		{
			//We delegeren ditmaal niet naar de view van een los object omdat ze onderling 
			// verband houden en de code redelijk simpel is.
			$bedrijf = Bedrijf::vanProfiel($profiel);
			$bedrijfnaam = BedrijfView::waardeNaam($bedrijf);

			$namelink = new HtmlAnchor();
			$namelink->setAttribute('name', urlencode($bedrijf->getNaam()));
			$namelink->addClass('bedrijfsprofiel-anchor');

			if(hasAuth('spocie'))
			{
				$verloopdatum = $profiel->getDatumVerloop()->format('Y-m-d');

				$row = new HtmlDiv(array(
					$namelink,
					"$bedrijfnaam [",
					new HtmlAnchor($profiel->wijzigURL(), _("Wijzigen")),
					"]",
					new HtmlBreak(),
					_("Dit profiel zal verlopen op $verloopdatum.")
				));

				// Gebruik de namelink niet meer bij het bedrijfsprofiel hieronder
				$namelink = null;

				$li->addChild($row);
			}

			$li->addChild($row = new HtmlDiv(null, 'row'));
			$row->add(new HtmlDiv(new HtmlAnchor($profiel->getUrl(),
				$img = new HtmlImage($bedrijf->getLogo(),$bedrijfnaam,null,'bedrijfslogo')), 'col-md-3'));
			$row->add(new HtmlDiv(array(
						$namelink,
						new HtmlHeader(2, new HtmlAnchor($profiel->getUrl(), $bedrijf->getNaam())),
						$profiel->getInhoud()
				), 'col-md-9'));
			$img->setLazyLoad();
		}
		$div->add($li);

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
