<?
/**
 * $Id$
 */
abstract class BedrijfView
	extends BedrijfView_Generated
{

	static public function details(Contact $bedrijf, $noPage = false)
	{
		$spoken = ContactPersoonVerzameling::metConstraints($bedrijf, null, null, null, 'SPOOK');
		$alumni = ContactPersoonVerzameling::metConstraints($bedrijf, null, null, null, 'ALUMNUS');
		$bezoekadres = ContactAdres::geefBySoort($bedrijf, "bezoek");
		$postadres = ContactAdres::geefBySoort($bedrijf, "post");
		$factuuradres = ContactAdres::geefBySoort($bedrijf, "factuur");
		$bedrijfstudies = BedrijfStudieVerzameling::vanBedrijf($bedrijf);

		$pageDiv = new HtmlDiv();

		$bedrijfInfoDiv = new HtmlDiv();
		$bedrijfInfoTable = new HtmlDiv(null, 'info-table');
		$bedrijfInfoTable->add(BedrijfView::infoTR($bedrijf,'Naam'))
						->add(BedrijfView::infoTR($bedrijf,'Type'))
						->add(BedrijfView::infoTRData(_('Studies')
							, ($bedrijfstudies->aantal() > 0 ? StudieVerzamelingView::toString($bedrijfstudies->toStudieVerzameling()) : _("nog geen gekoppeld"))
								. " (". new HtmlAnchor($bedrijf->url()."/".'Studies','details') . ")"
							))
						->add(BedrijfView::infoTR($bedrijf,'Status'))
						->add(BedrijfView::infoTR($bedrijf,'Homepage'))
						->add(BedrijfView::infoTRData(_('Spook')
							, ContactPersoonVerzamelingView::stringLijst($spoken)
								. " (". new HtmlAnchor($bedrijf->url()."/".'Spoken','details') . ")"
							))
						->add(BedrijfView::infoTRData(_('Alumni')
							, ContactPersoonVerzamelingView::stringLijst($alumni)
								. " (". new HtmlAnchor($bedrijf->url()."/".'Alumni','details') . ")"
							))
						->add(($bezoekadres ? ContactAdresView::tableTr($bezoekadres, true) : null))
						->add(($postadres ? ContactAdresView::tableTr($postadres) : null))
						->add(($factuuradres ? ContactAdresView::tableTr($factuuradres) : null));

		//Als er nog geen adres is, genereer dan een link om deze toe te voegen.
		if(!isset($bezoekadres) && !isset($postadres) && !isset($factuuradres))
		{
			$bedrijfInfoTable->add(new HtmlTableRow(array(new HtmlTableDataCell(_('Adres:')),
			new HtmlTableDataCell(_('Geen adres gevonden, ') . new HtmlAnchor($bedrijf->url()."/".'WijzigAdres','toevoegen')))));
		}

		$bedrijfInfoTable->add(BedrijfView::infoTR($bedrijf,'Logo'))
						->add(BedrijfView::infoTRVacature($bedrijf))
						->add(BedrijfView::infoTR($bedrijf,'Omschrijving'));
		$bedrijfInfoDiv->add($bedrijfInfoTable);

		$logoDiv = new HtmlDiv(null, null, 'bedrijfinfologo');
		if($bedrijf->getLogo())
		{
			$logoDiv->add(new HtmlImage($bedrijf->getLogo(),_("Bedrijfslogo")));
		} else {
			$logoDiv->add(_("Geen logo ingesteld") . (hasAuth('Spocie')? "," . new HtmlBreak() . 
				new HtmlAnchor('/Service/Intern/Publisher/Vereniging/Commissies/spocie/logos/', _('uploaden via de publisher')):"")
			);
		}

		$pageDiv->add(HtmlAnchor::button('/Service/Spookweb2', _('Terug naar het overzicht')));

		$pageDiv->add($row = new HtmlDiv(null, 'row'));
		$row->add(new HtmlDiv($logoDiv, 'col-md-3'));
		$row->add(new HtmLDiv(new HtmlDiv(new HtmlDiv($bedrijfInfoDiv, 'panel-body'), 'panel panel-default'), 'col-md-9'));

		$vanaf = new DateTimeLocale();
		$vanaf->modify('-2 year');
		$contracten = ContractVerzameling::metConstraints($bedrijf->geefID(), 'alle', $vanaf);
		$pageDiv->add(new HtmlHeader(3, array(
							_('Contracten afgelopen 2 jaar'),
							new HtmlSmall(new HtmlAnchor($bedrijf->url().'/Contracten', _('Alle')))
						)
					))
				->add(ContractVerzamelingView::htmlList($contracten, true, true))
				->add(HtmlAnchor::button($bedrijf->url().'/Nieuwcontract', _('Voeg een nieuw contract toe')));

		$bedrijfscontacten = ContactPersoonVerzameling::metConstraints($bedrijf, null, 'huidig', null, 'Bedrijfscontact');
		$pageDiv->add(new HtmlHeader(3, array(
							_('Huidige contactpersonen'),
							new HtmlSmall(new HtmlAnchor($bedrijf->url().'/Contactpersonen', _('Alle')))
						)
					))
				->add(ContactPersoonVerzamelingView::htmlListBedrijfContact($bedrijfscontacten, false))
				->add(HtmlAnchor::button($bedrijf->url().'/Nieuwbedrijfscontact', _('Voeg een nieuw bedrijfscontact toe')));

		$interacties = InteractieVerzameling::metConstraints(10,null,null,null,$bedrijf->getContactID());
		$pageDiv->add(new HtmlHeader(3, array(
							_("Laatste 10 interacties met dit bedrijf"),
							new HtmlSmall(new HtmlAnchor($bedrijf->url().'/Interactie', _('Alle')))
						)
					))
				->add(HtmlAnchor::button($bedrijf->url().'/Nieuwinteractie', _('Voeg een nieuwe interactie toe')))
				->add(new HtmlHR())
				->add(InteractieVerzamelingView::htmlList($interacties));

		return $pageDiv;

	}

	/**
	 * @brief Geef een HTML-pagina met wijzigformulier voor het bedrijf.
	 *
	 * @return een HTMLPage.
	 */
	static public function wijzigen($bedrijf, $show_error)
	{
		$page = static::pageLinks($bedrijf);
		$page->start(null, 'leden');
		$page->add(BedrijfView::wijzigForm('BedrijfWijzig', $bedrijf, $show_error));
		return $page;
	}

	/**
	 * Geef een formulier voor het wijzigen van een bedrijf terug
	 *
	 * @return Een HtmlForm.
	 */
	static public function wijzigForm ($name = 'BedrijfWijzig', $obj = null, $show_error = true)
	{
		/**
		 * We gaan 2 divs maken:
		 * - Bedrijfinformatie
		 * - Contactsinformatie + telefoonnummers + adressen
		 */

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		/** Eerst de div voor algemene informatie **/
		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($obj, 'Naam', $show_error))
			->add(self::wijzigTR($obj, 'Type', $show_error))
			->add(self::wijzigTR($obj, 'Status', $show_error))
			->add(self::wijzigTR($obj, 'Homepage', $show_error))
			->add(self::wijzigTR($obj, 'Logo', $show_error))
			->add(self::wijzigTR($obj, 'Omschrijving', $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Wijzig Adres (ook)")));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB())
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));

		return $form;
	}

	/**
	 * Verwerk het wijzig formulier
	 */
	static public function processWijzigForm ($obj)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($obj), null))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($obj);

	}

	/**
	 *  Labels e.d.
	 **/

	public static function labelType(Bedrijf $obj){
		return _('Branche');
	}

	public static function labelStatus(Bedrijf $obj){
		return _('Status');
	}

	//Gebruik Organisatie object om de functie uit de organisatieview te overriden.
	public static function labelOmschrijving(Organisatie $obj){
		return _('Opmerking');
	}

	public static function labelenumType($value)
	{
		switch ($value)
		{
			case 'CONSULTANCY': return _('Consultancy');
			case 'FINANCIEEL': return _('Financieel');
			case 'ONDERZOEK': return _('Onderzoek');
			case 'OVERIG': return _('Overig');
			default: return parent::labelenumType($value);
		}
	}

	public static function labelenumStatus($value)
	{
		switch ($value)
		{
			case 'AFGEWEZEN': return _('Samenwerking afgewezen');
			case 'HUIDIG': return _('Huidige sponsor');
			case 'KOSTENLOOS': return _('Kostenloze samenwerking');
			case 'OUD': return _('Oud-sponsor');
			case 'POTENTIEEL': return _('Potentiele sponsor');
			case 'OVERGENOME': return _('Is overgenomen');
			case 'OPGEHEVEN': return _('Is opgeheven');
			case 'OVERIG': return _('Overig/onbekend');
			default: return parent::labelenumStatus($value);
		}
	}

	public static function opmerkingLogo()
	{
		return "Geef de url, bv: '/Vereniging/Commissies/spocie/logos/quinity.png'. (je kan eerst een logo ". new HtmlAnchor('/Service/Intern/Publisher/Vereniging/Commissies/spocie/logos/', _('uploaden via de publisher')) . ")";
	}

	//Geeft een extra regel in de tabel terug waarop de links naar de vacaturen van dit bedrijf staan. 
	//Ben niet erg tevreden met de manier waarop.
	static function infoTRVacature($obj)
	{
		//Haal links van de vacaturen op.
		$links = $obj->VacaturenUrl();
		
		//Check of er uberhaupt links zijn om weer te geven.
		if($links!=array(NULL))
		{
			$row = new HtmlTableRow();
			$row->addData('Vacature(s):');
			$row->addData($links);
			return $row;
		}

		return;
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
