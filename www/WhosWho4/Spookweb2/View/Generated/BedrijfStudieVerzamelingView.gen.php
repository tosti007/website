<?
abstract class BedrijfStudieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BedrijfStudieVerzamelingView.
	 *
	 * @param BedrijfStudieVerzameling $obj Het BedrijfStudieVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBedrijfStudieVerzameling(BedrijfStudieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
