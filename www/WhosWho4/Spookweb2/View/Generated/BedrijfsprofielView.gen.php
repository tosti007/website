<?
abstract class BedrijfsprofielView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BedrijfsprofielView.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBedrijfsprofiel(Bedrijfsprofiel $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Bedrijfsprofiel $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Bedrijfsprofiel $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contractonderdeel.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld contractonderdeel labelt.
	 */
	public static function labelContractonderdeel(Bedrijfsprofiel $obj)
	{
		return 'Contractonderdeel';
	}
	/**
	 * @brief Geef de waarde van het veld contractonderdeel.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contractonderdeel van het
	 * object obj representeert.
	 */
	public static function waardeContractonderdeel(Bedrijfsprofiel $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContractonderdeel())
			return NULL;
		return ContractonderdeelView::defaultWaardeContractonderdeel($obj->getContractonderdeel());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contractonderdeel.
	 *
	 * @see genericFormcontractonderdeel
	 *
	 * @param Bedrijfsprofiel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contractonderdeel staat en
	 * kan worden bewerkt. Indien contractonderdeel read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formContractonderdeel(Bedrijfsprofiel $obj, $include_id = false)
	{
		return ContractonderdeelView::defaultForm($obj->getContractonderdeel());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contractonderdeel. In
	 * tegenstelling tot formcontractonderdeel moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formcontractonderdeel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contractonderdeel staat en
	 * kan worden bewerkt. Indien contractonderdeel read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormContractonderdeel($name, $waarde=NULL)
	{
		return ContractonderdeelView::genericDefaultForm('Contractonderdeel');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * contractonderdeel bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contractonderdeel representeert.
	 */
	public static function opmerkingContractonderdeel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumVerloop.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld datumVerloop labelt.
	 */
	public static function labelDatumVerloop(Bedrijfsprofiel $obj)
	{
		return 'DatumVerloop';
	}
	/**
	 * @brief Geef de waarde van het veld datumVerloop.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumVerloop van het object
	 * obj representeert.
	 */
	public static function waardeDatumVerloop(Bedrijfsprofiel $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumVerloop');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumVerloop.
	 *
	 * @see genericFormdatumVerloop
	 *
	 * @param Bedrijfsprofiel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumVerloop staat en kan
	 * worden bewerkt. Indien datumVerloop read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumVerloop(Bedrijfsprofiel $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumVerloop', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumVerloop. In
	 * tegenstelling tot formdatumVerloop moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumVerloop
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumVerloop staat en kan
	 * worden bewerkt. Indien datumVerloop read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumVerloop($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumVerloop');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumVerloop bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumVerloop representeert.
	 */
	public static function opmerkingDatumVerloop()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inhoud.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld inhoud labelt.
	 */
	public static function labelInhoud(Bedrijfsprofiel $obj)
	{
		return 'Inhoud';
	}
	/**
	 * @brief Geef de waarde van het veld inhoud.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inhoud van het object obj
	 * representeert.
	 */
	public static function waardeInhoud(Bedrijfsprofiel $obj)
	{
		return static::defaultWaardeHtml($obj, 'Inhoud');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inhoud.
	 *
	 * @see genericForminhoud
	 *
	 * @param Bedrijfsprofiel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inhoud staat en kan worden
	 * bewerkt. Indien inhoud read-only is betreft het een statisch html-element.
	 */
	public static function formInhoud(Bedrijfsprofiel $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormHtml($obj, 'Inhoud', 'nl', $include_id),
			'en' => static::defaultFormHtml($obj, 'Inhoud', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inhoud. In
	 * tegenstelling tot forminhoud moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see forminhoud
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inhoud staat en kan worden
	 * bewerkt. Indien inhoud read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormInhoud($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormHtml($name, $waarde, 'Inhoud', 'nl'),
			'en' => static::genericDefaultFormHtml($name, $waarde, 'Inhoud', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld inhoud
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inhoud representeert.
	 */
	public static function opmerkingInhoud()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld url.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld url labelt.
	 */
	public static function labelUrl(Bedrijfsprofiel $obj)
	{
		return 'Url';
	}
	/**
	 * @brief Geef de waarde van het veld url.
	 *
	 * @param Bedrijfsprofiel $obj Het Bedrijfsprofiel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld url van het object obj
	 * representeert.
	 */
	public static function waardeUrl(Bedrijfsprofiel $obj)
	{
		return static::defaultWaardeString($obj, 'Url');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld url.
	 *
	 * @see genericFormurl
	 *
	 * @param Bedrijfsprofiel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld url staat en kan worden
	 * bewerkt. Indien url read-only is betreft het een statisch html-element.
	 */
	public static function formUrl(Bedrijfsprofiel $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Url', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld url. In tegenstelling
	 * tot formurl moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formurl
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld url staat en kan worden
	 * bewerkt. Indien url read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormUrl($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Url', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld url
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld url representeert.
	 */
	public static function opmerkingUrl()
	{
		return NULL;
	}
}
