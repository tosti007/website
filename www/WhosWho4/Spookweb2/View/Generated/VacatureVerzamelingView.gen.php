<?
abstract class VacatureVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VacatureVerzamelingView.
	 *
	 * @param VacatureVerzameling $obj Het VacatureVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVacatureVerzameling(VacatureVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
