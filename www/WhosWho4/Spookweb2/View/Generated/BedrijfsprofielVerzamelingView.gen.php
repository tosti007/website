<?
abstract class BedrijfsprofielVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BedrijfsprofielVerzamelingView.
	 *
	 * @param BedrijfsprofielVerzameling $obj Het BedrijfsprofielVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBedrijfsprofielVerzameling(BedrijfsprofielVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
