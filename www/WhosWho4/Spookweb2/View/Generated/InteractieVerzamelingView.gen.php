<?
abstract class InteractieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in InteractieVerzamelingView.
	 *
	 * @param InteractieVerzameling $obj Het InteractieVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeInteractieVerzameling(InteractieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
