<?
abstract class VacatureStudieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VacatureStudieView.
	 *
	 * @param VacatureStudie $obj Het VacatureStudie-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVacatureStudie(VacatureStudie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld Studie.
	 *
	 * @param VacatureStudie $obj Het VacatureStudie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld Studie labelt.
	 */
	public static function labelStudie(VacatureStudie $obj)
	{
		return 'Studie';
	}
	/**
	 * @brief Geef de waarde van het veld Studie.
	 *
	 * @param VacatureStudie $obj Het VacatureStudie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Studie van het object obj
	 * representeert.
	 */
	public static function waardeStudie(VacatureStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getStudie())
			return NULL;
		return StudieView::defaultWaardeStudie($obj->getStudie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld Studie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Studie representeert.
	 */
	public static function opmerkingStudie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld Vacature.
	 *
	 * @param VacatureStudie $obj Het VacatureStudie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld Vacature labelt.
	 */
	public static function labelVacature(VacatureStudie $obj)
	{
		return 'Vacature';
	}
	/**
	 * @brief Geef de waarde van het veld Vacature.
	 *
	 * @param VacatureStudie $obj Het VacatureStudie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld Vacature van het object obj
	 * representeert.
	 */
	public static function waardeVacature(VacatureStudie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVacature())
			return NULL;
		return VacatureView::defaultWaardeVacature($obj->getVacature());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * Vacature bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld Vacature representeert.
	 */
	public static function opmerkingVacature()
	{
		return NULL;
	}
}
