<?
abstract class BedrijfView_Generated
	extends OrganisatieView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BedrijfView.
	 *
	 * @param Bedrijf $obj Het Bedrijf-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBedrijf(Bedrijf $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param Bedrijf $obj Het Bedrijf-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(Bedrijf $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(Bedrijf::enumsStatus() as $id)
			$soorten[$id] = BedrijfView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param Bedrijf $obj Het Bedrijf-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(Bedrijf $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param Bedrijf $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(Bedrijf $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', Bedrijf::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld type.
	 *
	 * @param Bedrijf $obj Het Bedrijf-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld type labelt.
	 */
	public static function labelType(Bedrijf $obj)
	{
		return 'Type';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld type.
	 *
	 * @param string $value Een enum-waarde van het veld type.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumType($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld type horen.
	 *
	 * @see labelenumType
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld type
	 * representeren.
	 */
	public static function labelenumTypeArray()
	{
		$soorten = array();
		foreach(Bedrijf::enumsType() as $id)
			$soorten[$id] = BedrijfView::labelenumType($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @param Bedrijf $obj Het Bedrijf-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld type van het object obj
	 * representeert.
	 */
	public static function waardeType(Bedrijf $obj)
	{
		return static::defaultWaardeEnum($obj, 'Type');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld type.
	 *
	 * @see genericFormtype
	 *
	 * @param Bedrijf $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is betreft het een statisch html-element.
	 */
	public static function formType(Bedrijf $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Type', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld type. In tegenstelling
	 * tot formtype moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtype
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormType($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Type', Bedrijf::enumstype());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld type
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld type representeert.
	 */
	public static function opmerkingType()
	{
		return NULL;
	}
}
