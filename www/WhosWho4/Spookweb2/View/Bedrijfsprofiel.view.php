<?
/**
 * $Id$
 */
abstract class BedrijfsprofielView
	extends BedrijfsprofielView_Generated
{

	public static function wijzigen(Bedrijfsprofiel $profiel, $show_error = null)
	{
		$pagediv = new HtmlDiv();
		$obj = $profiel;

		/** Dit wordt het formulier **/
		$form = HtmlForm::named('profielWijzig');

		/** Eerst de div voor persoonsinformatie in te vullen **/
		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($obj, 'DatumVerloop', $show_error))
			->add(self::wijzigTR($obj, 'Url', $show_error))
			->add(self::wijzigTR($obj, 'Inhoud', $show_error));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB())
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));

		$pagediv->add($form);

		return $pagediv;
	}

	public static function processWijzigForm(Bedrijfsprofiel $profiel)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($profiel), null))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($profiel);
	}

/************************************
 * Labels en opmerkingen voor forms *
 ************************************/

	public static function labelUrl(Bedrijfsprofiel $profiel)
	{
		return "Url";
	}

	public static function opmerkingUrl()
	{
		return _("De url waar het logo naar toe linkt");
	}

	public static function labelDatumVerloop(Bedrijfsprofiel $profiel)
	{
		return _("Verloopdatum");
	}

	public static function labelInhoud(Bedrijfsprofiel $profiel)
	{
		return _("Profieltekst");
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
