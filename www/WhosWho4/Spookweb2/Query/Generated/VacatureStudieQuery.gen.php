<?
abstract class VacatureStudieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de VacatureStudieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een VacatureStudieQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw VacatureStudieQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VacatureStudieQuery
	 * Het VacatureStudieQuery-object waarvan meteen de db-table van het
	 * VacatureStudie-object als table geset is.
	 */
	static public function table()
	{
		$query = new VacatureStudieQuery();

		$query->tables('VacatureStudie');

		return $query;
	}

	/**
	 * @brief Maakt een VacatureStudieVerzameling aan van de objecten die geselecteerd
	 * worden door de VacatureStudieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VacatureStudieVerzameling
	 * Een VacatureStudieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'VacatureStudie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een VacatureStudie-iterator aan van de objecten die geselecteerd
	 * worden door de VacatureStudieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VacatureStudieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'VacatureStudie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een VacatureStudie die geslecteeerd wordt door de
	 * VacatureStudieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VacatureStudieVerzameling
	 * Een VacatureStudieVerzameling verkregen door de query.
	 */
	public function geef($object = 'VacatureStudie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van VacatureStudie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'Studie_studieID',
			'Vacature_id',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'VacatureStudie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als VacatureStudie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan VacatureStudie een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('VacatureStudie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Studie' => array(
				'Studie_studieID' => 'studieID'
			),
			'Vacature' => array(
				'Vacature_id' => 'id'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van VacatureStudie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'VacatureStudie.Studie_studieID',
			'VacatureStudie.Vacature_id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'studie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Studie; })))
					user_error('Alle waarden in de array moeten van type Studie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Studie) && !($value instanceof StudieVerzameling))
				user_error('Value moet van type Studie zijn', E_USER_ERROR);
			$field = 'Studie_studieID';
			if(is_array($value) || $value instanceof StudieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getStudieID();
				$value = $new_value;
			}
			else $value = $value->getStudieID();
			$type = 'int';
			break;
		case 'vacature':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Vacature; })))
					user_error('Alle waarden in de array moeten van type Vacature zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Vacature) && !($value instanceof VacatureVerzameling))
				user_error('Value moet van type Vacature zijn', E_USER_ERROR);
			$field = 'Vacature_id';
			if(is_array($value) || $value instanceof VacatureVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getId();
				$value = $new_value;
			}
			else $value = $value->getId();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'VacatureStudie.'.$v;
			}
		} else {
			$field = 'VacatureStudie.'.$field;
		}

		return array($field, $value, $type);
	}

}
