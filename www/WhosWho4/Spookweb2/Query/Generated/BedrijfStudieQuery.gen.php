<?
abstract class BedrijfStudieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de BedrijfStudieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een BedrijfStudieQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw BedrijfStudieQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return BedrijfStudieQuery
	 * Het BedrijfStudieQuery-object waarvan meteen de db-table van het
	 * BedrijfStudie-object als table geset is.
	 */
	static public function table()
	{
		$query = new BedrijfStudieQuery();

		$query->tables('BedrijfStudie');

		return $query;
	}

	/**
	 * @brief Maakt een BedrijfStudieVerzameling aan van de objecten die geselecteerd
	 * worden door de BedrijfStudieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return BedrijfStudieVerzameling
	 * Een BedrijfStudieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'BedrijfStudie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een BedrijfStudie-iterator aan van de objecten die geselecteerd
	 * worden door de BedrijfStudieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een BedrijfStudieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'BedrijfStudie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een BedrijfStudie die geslecteeerd wordt door de BedrijfStudieQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return BedrijfStudieVerzameling
	 * Een BedrijfStudieVerzameling verkregen door de query.
	 */
	public function geef($object = 'BedrijfStudie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van BedrijfStudie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'Studie_studieID',
			'Bedrijf_contactID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'BedrijfStudie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als BedrijfStudie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan BedrijfStudie een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('BedrijfStudie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Studie' => array(
				'Studie_studieID' => 'studieID'
			),
			'Bedrijf' => array(
				'Bedrijf_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van BedrijfStudie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'BedrijfStudie.Studie_studieID',
			'BedrijfStudie.Bedrijf_contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'studie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Studie; })))
					user_error('Alle waarden in de array moeten van type Studie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Studie) && !($value instanceof StudieVerzameling))
				user_error('Value moet van type Studie zijn', E_USER_ERROR);
			$field = 'Studie_studieID';
			if(is_array($value) || $value instanceof StudieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getStudieID();
				$value = $new_value;
			}
			else $value = $value->getStudieID();
			$type = 'int';
			break;
		case 'bedrijf':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Bedrijf; })))
					user_error('Alle waarden in de array moeten van type Bedrijf zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Bedrijf) && !($value instanceof BedrijfVerzameling))
				user_error('Value moet van type Bedrijf zijn', E_USER_ERROR);
			$field = 'Bedrijf_contactID';
			if(is_array($value) || $value instanceof BedrijfVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'BedrijfStudie.'.$v;
			}
		} else {
			$field = 'BedrijfStudie.'.$field;
		}

		return array($field, $value, $type);
	}

}
