<?
abstract class BedrijfQuery_Generated
	extends OrganisatieQuery
{
	/**
	 * @brief De constructor van de BedrijfQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // OrganisatieQuery
	}
	/**
	 * @brief Maakt een BedrijfQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw BedrijfQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return BedrijfQuery
	 * Het BedrijfQuery-object waarvan meteen de db-table van het Bedrijf-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new BedrijfQuery();

		$query->tables('Bedrijf');

		return $query;
	}

	/**
	 * @brief Maakt een BedrijfVerzameling aan van de objecten die geselecteerd worden
	 * door de BedrijfQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return BedrijfVerzameling
	 * Een BedrijfVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Bedrijf', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Bedrijf-iterator aan van de objecten die geselecteerd worden
	 * door de BedrijfQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een BedrijfVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Bedrijf', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Bedrijf die geslecteeerd wordt door de BedrijfQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return BedrijfVerzameling
	 * Een BedrijfVerzameling verkregen door de query.
	 */
	public function geef($object = 'Bedrijf')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Bedrijf.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'contactID',
			'status',
			'type'
		);

		if(in_array($field, $varArray))
			return 'Bedrijf';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Bedrijf een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Organisatie' => array(
				'Organisatie.contactID' => 'Bedrijf.contactID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Bedrijf een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Bedrijf'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Organisatie' => array(
				'Organisatie.contactID' => 'Bedrijf.contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Bedrijf.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Bedrijf.contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'status':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Bedrijf::enumsStatus()))
						user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Bedrijf::enumsStatus()))
				user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
			$type = 'string';
			$field = 'status';
			break;
		case 'type':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Bedrijf::enumsType()))
						user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Bedrijf::enumsType()))
				user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
			$type = 'string';
			$field = 'type';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Bedrijf.'.$v;
			}
		} else {
			$field = 'Bedrijf.'.$field;
		}

		return array($field, $value, $type);
	}

}
