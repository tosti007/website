<?
abstract class VacatureQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de VacatureQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een VacatureQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw VacatureQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VacatureQuery
	 * Het VacatureQuery-object waarvan meteen de db-table van het Vacature-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new VacatureQuery();

		$query->tables('Vacature');

		return $query;
	}

	/**
	 * @brief Maakt een VacatureVerzameling aan van de objecten die geselecteerd worden
	 * door de VacatureQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VacatureVerzameling
	 * Een VacatureVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Vacature', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Vacature-iterator aan van de objecten die geselecteerd worden
	 * door de VacatureQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VacatureVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Vacature', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Vacature die geslecteeerd wordt door de VacatureQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VacatureVerzameling
	 * Een VacatureVerzameling verkregen door de query.
	 */
	public function geef($object = 'Vacature')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Vacature.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'contractonderdeel_id',
			'datumVerloop',
			'datumPlaatsing',
			'type',
			'ica',
			'iku',
			'na',
			'wis',
			'titel_NL',
			'titel_EN',
			'inleiding_NL',
			'inleiding_EN',
			'inhoud_NL',
			'inhoud_EN',
			'views',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Vacature';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Vacature een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Vacature een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Vacature'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contractonderdeel' => array(
				'contractonderdeel_id' => 'id'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Vacature.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Vacature.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'contractonderdeel':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Contractonderdeel; })))
					user_error('Alle waarden in de array moeten van type Contractonderdeel zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Contractonderdeel) && !($value instanceof ContractonderdeelVerzameling) && !is_null($value))
				user_error('Value moet van type Contractonderdeel of NULL zijn', E_USER_ERROR);
			$field = 'contractonderdeel_id';
			if(is_array($value) || $value instanceof ContractonderdeelVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getId();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getId();
			$type = 'int';
			break;
		case 'datumverloop':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumVerloop';
			break;
		case 'datumplaatsing':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumPlaatsing';
			break;
		case 'type':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Vacature::enumsType()))
						user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Vacature::enumsType()))
				user_error($value . ' is niet een geldige waarde voor type', E_USER_ERROR);
			$type = 'string';
			$field = 'type';
			break;
		case 'ica':
			$type = 'int';
			$field = 'ica';
			break;
		case 'iku':
			$type = 'int';
			$field = 'iku';
			break;
		case 'na':
			$type = 'int';
			$field = 'na';
			break;
		case 'wis':
			$type = 'int';
			$field = 'wis';
			break;
		case 'titel_nl':
			$type = 'string';
			$field = 'titel_NL';
			break;
		case 'titel_en':
			$type = 'string';
			$field = 'titel_EN';
			break;
		case 'inleiding_nl':
			$type = 'string';
			$field = 'inleiding_NL';
			break;
		case 'inleiding_en':
			$type = 'string';
			$field = 'inleiding_EN';
			break;
		case 'inhoud_nl':
			$type = 'string';
			$field = 'inhoud_NL';
			break;
		case 'inhoud_en':
			$type = 'string';
			$field = 'inhoud_EN';
			break;
		case 'views':
			$type = 'int';
			$field = 'views';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Vacature.'.$v;
			}
		} else {
			$field = 'Vacature.'.$field;
		}

		return array($field, $value, $type);
	}

}
