<?
abstract class ContractonderdeelQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de ContractonderdeelQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een ContractonderdeelQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw ContractonderdeelQuery-object
	 * aan om er zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return ContractonderdeelQuery
	 * Het ContractonderdeelQuery-object waarvan meteen de db-table van het
	 * Contractonderdeel-object als table geset is.
	 */
	static public function table()
	{
		$query = new ContractonderdeelQuery();

		$query->tables('Contractonderdeel');

		return $query;
	}

	/**
	 * @brief Maakt een ContractonderdeelVerzameling aan van de objecten die
	 * geselecteerd worden door de ContractonderdeelQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return ContractonderdeelVerzameling
	 * Een ContractonderdeelVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Contractonderdeel', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Contractonderdeel-iterator aan van de objecten die geselecteerd
	 * worden door de ContractonderdeelQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een ContractonderdeelVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Contractonderdeel', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Contractonderdeel die geslecteeerd wordt door de
	 * ContractonderdeelQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return ContractonderdeelVerzameling
	 * Een ContractonderdeelVerzameling verkregen door de query.
	 */
	public function geef($object = 'Contractonderdeel')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Contractonderdeel.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'contract_contractID',
			'soort',
			'bedrag',
			'uitvoerDatum',
			'omschrijving',
			'uitgevoerd',
			'verantwoordelijke_contactID',
			'gefactureerd',
			'commissie_commissieID',
			'latexString',
			'annulering',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Contractonderdeel';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Contractonderdeel
	 * een extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Contractonderdeel een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Contractonderdeel'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contract' => array(
				'contract_contractID' => 'contractID'
			),
			'Persoon' => array(
				'verantwoordelijke_contactID' => 'contactID'
			),
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Contractonderdeel.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Contractonderdeel.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'contract':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Contract; })))
					user_error('Alle waarden in de array moeten van type Contract zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Contract) && !($value instanceof ContractVerzameling))
				user_error('Value moet van type Contract zijn', E_USER_ERROR);
			$field = 'contract_contractID';
			if(is_array($value) || $value instanceof ContractVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContractID();
				$value = $new_value;
			}
			else $value = $value->getContractID();
			$type = 'int';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Contractonderdeel::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Contractonderdeel::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'bedrag':
			$type = 'int';
			$field = 'bedrag';
			break;
		case 'uitvoerdatum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'uitvoerDatum';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'uitgevoerd':
			$type = 'string';
			$field = 'uitgevoerd';
			break;
		case 'verantwoordelijke':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling) && !is_null($value))
				user_error('Value moet van type Persoon of NULL zijn', E_USER_ERROR);
			$field = 'verantwoordelijke_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'gefactureerd':
			$type = 'int';
			$field = 'gefactureerd';
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling) && !is_null($value))
				user_error('Value moet van type Commissie of NULL zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'latexstring':
			$type = 'string';
			$field = 'latexString';
			break;
		case 'annulering':
			$type = 'int';
			$field = 'annulering';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Contractonderdeel.'.$v;
			}
		} else {
			$field = 'Contractonderdeel.'.$field;
		}

		return array($field, $value, $type);
	}

}
