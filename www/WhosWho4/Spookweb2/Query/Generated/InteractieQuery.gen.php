<?
abstract class InteractieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de InteractieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een InteractieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw InteractieQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return InteractieQuery
	 * Het InteractieQuery-object waarvan meteen de db-table van het Interactie-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new InteractieQuery();

		$query->tables('Interactie');

		return $query;
	}

	/**
	 * @brief Maakt een InteractieVerzameling aan van de objecten die geselecteerd
	 * worden door de InteractieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return InteractieVerzameling
	 * Een InteractieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Interactie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Interactie-iterator aan van de objecten die geselecteerd worden
	 * door de InteractieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een InteractieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Interactie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Interactie die geslecteeerd wordt door de InteractieQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return InteractieVerzameling
	 * Een InteractieVerzameling verkregen door de query.
	 */
	public function geef($object = 'Interactie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Interactie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'spook_persoon_contactID',
			'spook_organisatie_contactID',
			'spook_type',
			'bedrijf_contactID',
			'bedrijfPersoon_persoon_contactID',
			'bedrijfPersoon_organisatie_contactID',
			'bedrijfPersoon_type',
			'commissie_commissieID',
			'datum',
			'inhoud',
			'medium',
			'todo',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Interactie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Interactie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Interactie een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Interactie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'ContactPersoon' => array(
				'spook' => array(
					'spook_persoon_contactID' => 'persoon_contactID',
					'spook_organisatie_contactID' => 'organisatie_contactID',
					'spook_type' => 'type'
				),
				'bedrijfPersoon' => array(
					'bedrijfPersoon_persoon_contactID' => 'persoon_contactID',
					'bedrijfPersoon_organisatie_contactID' => 'organisatie_contactID',
					'bedrijfPersoon_type' => 'type'
				)
			),
			'Bedrijf' => array(
				'bedrijf_contactID' => 'contactID'
			),
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Interactie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Interactie.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'spook':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof ContactPersoon; })))
					user_error('Alle waarden in de array moeten van type ContactPersoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof ContactPersoon) && !($value instanceof ContactPersoonVerzameling))
				user_error('Value moet van type ContactPersoon zijn', E_USER_ERROR);
			$field = array(
				'spook_persoon_contactID',
				'spook_organisatie_contactID',
				'spook_type'
			);
			if(is_array($value) || $value instanceof ContactPersoonVerzameling)
			{
				foreach($value as $k => $v)
					$value[$k] = array(
				$v->getPersoonContactID(),
				$v->getOrganisatieContactID(),
				$v->getType()
					);
				if($value instanceof ContactPersoonVerzameling)
					$value = $value->getVerzameling();
			}
			else
				$value = array(
				$value->getPersoonContactID(),
				$value->getOrganisatieContactID(),
				$value->getType()
				);
			$type = array(
				'int',
				'int',
				'string'				);
			break;
		case 'bedrijf':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Bedrijf; })))
					user_error('Alle waarden in de array moeten van type Bedrijf zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Bedrijf) && !($value instanceof BedrijfVerzameling))
				user_error('Value moet van type Bedrijf zijn', E_USER_ERROR);
			$field = 'bedrijf_contactID';
			if(is_array($value) || $value instanceof BedrijfVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'bedrijfpersoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof ContactPersoon; })))
					user_error('Alle waarden in de array moeten van type ContactPersoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof ContactPersoon) && !($value instanceof ContactPersoonVerzameling) && !is_null($value))
				user_error('Value moet van type ContactPersoon of NULL zijn', E_USER_ERROR);
			$field = array(
				'bedrijfPersoon_persoon_contactID',
				'bedrijfPersoon_organisatie_contactID',
				'bedrijfPersoon_type'
			);
			if(is_array($value) || $value instanceof ContactPersoonVerzameling)
			{
				foreach($value as $k => $v)
					$value[$k] = array(
				$v->getPersoonContactID(),
				$v->getOrganisatieContactID(),
				$v->getType()
					);
				if($value instanceof ContactPersoonVerzameling)
					$value = $value->getVerzameling();
			}
			else if(!is_null($value))
				$value = array(
				$value->getPersoonContactID(),
				$value->getOrganisatieContactID(),
				$value->getType()
				);
			$type = array(
				'int',
				'int',
				'string'				);
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling) && !is_null($value))
				user_error('Value moet van type Commissie of NULL zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'datum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datum';
			break;
		case 'inhoud':
			$type = 'string';
			$field = 'inhoud';
			break;
		case 'medium':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Interactie::enumsMedium()))
						user_error($value . ' is niet een geldige waarde voor medium', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Interactie::enumsMedium()))
				user_error($value . ' is niet een geldige waarde voor medium', E_USER_ERROR);
			$type = 'string';
			$field = 'medium';
			break;
		case 'todo':
			$type = 'int';
			$field = 'todo';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Interactie.'.$v;
			}
		} else {
			$field = 'Interactie.'.$field;
		}

		return array($field, $value, $type);
	}

}
