<?
class InteractieQuery
	extends InteractieQuery_Generated
{
	/**
	 *  Functie die met constraints op de db kan zoeken.
	 **/
	public static function metConstraints($vanaf = null, $medium = null, $spook = null,
		 $bedrijfscontact = null, $bedrijf = null, $commissie = null, $ToDo = null)
	{
		$q = InteractieQuery::table()->orderByDesc('datum');

		if ($vanaf === null) {
			$vanaf = new DateTimeLocale();
			$vanaf->modify('-3 month');
		}
		if ($vanaf !== 'alle') {
			if ($vanaf instanceof DateTime) {
				$vanaf = $vanaf->format('Y-m-d');
			}
			if (!($vanaf instanceof DateTimeLocale)) {
				$vanaf = new DateTimeLocale($vanaf);
			}
			$q->whereProp('datum', '>=', $vanaf);
		}

		if ($medium) {
			$q->whereProp('medium', $medium);
		}
		if ($spook) {
			$q->whereProp('Spook', $spook);
		}
		if ($bedrijfscontact) {
			$q->whereProp('Bedrijfpersoon', $bedrijfscontact);
		}
		if ($bedrijf) {
			$q->whereProp('Bedrijf', $bedrijf);
		}
		if ($commissie) {
			$q->whereProp('Commissie', $commissie);
		}
		if ($ToDo) {
			$q->whereProp('todo', '1');
		}
		return $q;
	}
}
