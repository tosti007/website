<?php

//Checkt of een CP een spook is, spaart veel static aanroepen uit.
function isSpook(ContactPersoon $persoon)
{
	if(strtoupper($persoon->getType()) == "SPOOK")
		return true;
	return false;
}

//Checkt of een CP een overige spookweb type is zoals sponsorcommissaris of overijverig bestuurslid
function isOverigSpookweb(ContactPersoon $persoon)
{
	if(strtoupper($persoon->getType()) == "OVERIGSPOOKWEB")
		return true;
	return false;
}

//En het zelfde voor een bedrijfscontact
function isBedrijfscontact(ContactPersoon $persoon)
{
	if(strtoupper($persoon->getType()) == "BEDRIJFSCONTACT")
		return true;
	return false;
}

//Maak een mooie array voor een selectbox met optgroups met een specifieke spookweb set.
function spookwebSelectboxCiesArray()
{
	$allspookcies = CommissieVerzameling::spookCommissies();
	$allspookcies->sorteer('naam');

	$huidigecies = CommissieVerzameling::huidige();
	$huidigecies->sorteer('naam');

	$cies = array('');
	foreach($huidigecies as $cie) {
		$cies['Lopende commissies'][$cie->geefID()] = $cie->getNaam();
	}
	foreach($allspookcies as $cie) {
		if(!$huidigecies->bevat($cie)) {
			$cies['Oude commissies'][$cie->geefID()] = $cie->getNaam();
		}
	}

	return $cies;
}

//Geef de html terug voor een bedrijfslogo met link voor bv bookweb.
function sponsorLogoSW2()
{
	$profielen = BedrijfsprofielVerzameling::metConstraints('huidig')->shuffle();
	$bedrijf = Bedrijf::vanProfiel($profielen->first());

	$div = new HtmlDiv(null, "advertentie");
	$div->add(_("(advertentie)") . new HtmlBreak())
		->add(new HtmlAnchor('/Vereniging/Bedrijven/Sponsors/#'.$bedrijf->getNaam(),
			$img = new HtmlImage($bedrijf->getLogo(),$bedrijf->getNaam(),null,'bedrijfslogo')));
	$img->setLazyLoad();
	return $div;

}
