<?
abstract class IOUBetalingVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de IOUBetalingVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze IOUBetalingVerzameling een IOUBonVerzameling.
	 *
	 * @return IOUBonVerzameling
	 * Een IOUBonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze IOUBetalingVerzameling.
	 */
	public function toBonVerzameling()
	{
		if($this->aantal() == 0)
			return new IOUBonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getBonBonID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getBonBonID()
			                      );
		}
		$this->positie = $origPositie;
		return IOUBonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze IOUBetalingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze IOUBetalingVerzameling.
	 */
	public function toVanVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVanContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze IOUBetalingVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze IOUBetalingVerzameling.
	 */
	public function toNaarVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getNaarContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een IOUBetalingVerzameling van Bon.
	 *
	 * @return IOUBetalingVerzameling
	 * Een IOUBetalingVerzameling die elementen bevat die bij de Bon hoort.
	 */
	static public function fromBon($bon)
	{
		if(!isset($bon))
			return new IOUBetalingVerzameling();

		return IOUBetalingQuery::table()
			->whereProp('Bon', $bon)
			->verzamel();
	}
	/**
	 * @brief Maak een IOUBetalingVerzameling van Van.
	 *
	 * @return IOUBetalingVerzameling
	 * Een IOUBetalingVerzameling die elementen bevat die bij de Van hoort.
	 */
	static public function fromVan($van)
	{
		if(!isset($van))
			return new IOUBetalingVerzameling();

		return IOUBetalingQuery::table()
			->whereProp('Van', $van)
			->verzamel();
	}
	/**
	 * @brief Maak een IOUBetalingVerzameling van Naar.
	 *
	 * @return IOUBetalingVerzameling
	 * Een IOUBetalingVerzameling die elementen bevat die bij de Naar hoort.
	 */
	static public function fromNaar($naar)
	{
		if(!isset($naar))
			return new IOUBetalingVerzameling();

		return IOUBetalingQuery::table()
			->whereProp('Naar', $naar)
			->verzamel();
	}
}
