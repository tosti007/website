<?
abstract class IOUSplit_Generated
	extends Entiteit
{
	protected $bon;						/**< \brief PRIMARY */
	protected $bon_bonID;				/**< \brief PRIMARY */
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $moetBetalen;
	protected $heeftBetaald;
	/**
	/**
	 * @brief De constructor van de IOUSplit_Generated-klasse.
	 *
	 * @param mixed $a Bon (IOUBon OR Array(iOUBon_bonID) OR iOUBon_bonID)
	 * @param mixed $b Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof IOUBon)
		{
			$this->bon = $a;
			$this->bon_bonID = $a->getBonID();
		}
		else if(is_array($a))
		{
			$this->bon = NULL;
			$this->bon_bonID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->bon = NULL;
			$this->bon_bonID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->persoon = $b;
			$this->persoon_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->moetBetalen = 0;
		$this->heeftBetaald = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Bon';
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld bon.
	 *
	 * @return IOUBon
	 * De waarde van het veld bon.
	 */
	public function getBon()
	{
		if(!isset($this->bon)
		 && isset($this->bon_bonID)
		 ) {
			$this->bon = IOUBon::geef
					( $this->bon_bonID
					);
		}
		return $this->bon;
	}
	/**
	 * @brief Geef de waarde van het veld bon_bonID.
	 *
	 * @return int
	 * De waarde van het veld bon_bonID.
	 */
	public function getBonBonID()
	{
		if (is_null($this->bon_bonID) && isset($this->bon)) {
			$this->bon_bonID = $this->bon->getBonID();
		}
		return $this->bon_bonID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld moetBetalen.
	 *
	 * @return float
	 * De waarde van het veld moetBetalen.
	 */
	public function getMoetBetalen()
	{
		return $this->moetBetalen;
	}
	/**
	 * @brief Stel de waarde van het veld moetBetalen in.
	 *
	 * @param mixed $newMoetBetalen De nieuwe waarde.
	 *
	 * @return IOUSplit
	 * Dit IOUSplit-object.
	 */
	public function setMoetBetalen($newMoetBetalen)
	{
		unset($this->errors['MoetBetalen']);
		if(!is_null($newMoetBetalen))
			$newMoetBetalen = (float)$newMoetBetalen;
		if($this->moetBetalen === $newMoetBetalen)
			return $this;

		$this->moetBetalen = $newMoetBetalen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld moetBetalen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld moetBetalen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkMoetBetalen()
	{
		if (array_key_exists('MoetBetalen', $this->errors))
			return $this->errors['MoetBetalen'];
		$waarde = $this->getMoetBetalen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld heeftBetaald.
	 *
	 * @return float
	 * De waarde van het veld heeftBetaald.
	 */
	public function getHeeftBetaald()
	{
		return $this->heeftBetaald;
	}
	/**
	 * @brief Stel de waarde van het veld heeftBetaald in.
	 *
	 * @param mixed $newHeeftBetaald De nieuwe waarde.
	 *
	 * @return IOUSplit
	 * Dit IOUSplit-object.
	 */
	public function setHeeftBetaald($newHeeftBetaald)
	{
		unset($this->errors['HeeftBetaald']);
		if(!is_null($newHeeftBetaald))
			$newHeeftBetaald = (float)$newHeeftBetaald;
		if($this->heeftBetaald === $newHeeftBetaald)
			return $this;

		$this->heeftBetaald = $newHeeftBetaald;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld heeftBetaald geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld heeftBetaald geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkHeeftBetaald()
	{
		if (array_key_exists('HeeftBetaald', $this->errors))
			return $this->errors['HeeftBetaald'];
		$waarde = $this->getHeeftBetaald();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return IOUSplit::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van IOUSplit.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return IOUSplit::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getBonBonID()
		                      , $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return IOUSplit|false
	 * Een IOUSplit-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$bon_bonID = (int)$a[0];
			$persoon_contactID = (int)$a[1];
		}
		else if($a instanceof IOUBon
		     && $b instanceof Persoon)
		{
			$bon_bonID = $a->getBonID();
			$persoon_contactID = $b->getContactID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$bon_bonID = (int)$a;
			$persoon_contactID = (int)$b;
		}

		if(is_null($bon_bonID)
		|| is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($bon_bonID, $persoon_contactID) ));
		return Entiteit::geefCache(array($bon_bonID, $persoon_contactID), 'IOUSplit');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'IOUSplit');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('IOUSplit::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `IOUSplit`.`bon_bonID`'
		                 .     ', `IOUSplit`.`persoon_contactID`'
		                 .     ', `IOUSplit`.`moetBetalen`'
		                 .     ', `IOUSplit`.`heeftBetaald`'
		                 .     ', `IOUSplit`.`gewijzigdWanneer`'
		                 .     ', `IOUSplit`.`gewijzigdWie`'
		                 .' FROM `IOUSplit`'
		                 .' WHERE (`bon_bonID`, `persoon_contactID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['bon_bonID']
			                  ,$row['persoon_contactID']);

			$obj = new IOUSplit(array($row['bon_bonID']), array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->moetBetalen  = (float) $row['moetBetalen'];
			$obj->heeftBetaald  = (float) $row['heeftBetaald'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'IOUSplit')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getBon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Bon is not in DB');
		$this->bon_bonID = $rel->getBonID();

		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `IOUSplit`'
			          . ' (`bon_bonID`, `persoon_contactID`, `moetBetalen`, `heeftBetaald`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %f, %f, %s, %i)'
			          , $this->bon_bonID
			          , $this->persoon_contactID
			          , $this->moetBetalen
			          , $this->heeftBetaald
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'IOUSplit')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `IOUSplit`'
			          .' SET `moetBetalen` = %f'
			          .   ', `heeftBetaald` = %f'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bon_bonID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->moetBetalen
			          , $this->heeftBetaald
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bon_bonID
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenIOUSplit
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenIOUSplit($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenIOUSplit($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'MoetBetalen';
			$velden[] = 'HeeftBetaald';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'MoetBetalen';
			$velden[] = 'HeeftBetaald';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'MoetBetalen';
			$velden[] = 'HeeftBetaald';
			break;
		case 'get':
			$velden[] = 'MoetBetalen';
			$velden[] = 'HeeftBetaald';
		case 'primary':
			$velden[] = 'bon_bonID';
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'MoetBetalen';
			$velden[] = 'HeeftBetaald';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `IOUSplit`'
		          .' WHERE `bon_bonID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->bon_bonID
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->bon = NULL;
		$this->bon_bonID = NULL;
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `IOUSplit`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bon_bonID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bon_bonID
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van IOUSplit terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bon':
			return 'foreign';
		case 'bon_bonid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'moetbetalen':
			return 'money';
		case 'heeftbetaald':
			return 'money';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'moetBetalen':
		case 'heeftBetaald':
			$type = '%f';
			break;
		case 'bon_bonID':
		case 'persoon_contactID':
			$type = '%i';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `IOUSplit`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `bon_bonID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->bon_bonID
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `IOUSplit`'
		          .' WHERE `bon_bonID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		                 , $veld
		          , $this->bon_bonID
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'IOUSplit');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
