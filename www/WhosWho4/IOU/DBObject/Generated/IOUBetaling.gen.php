<?
abstract class IOUBetaling_Generated
	extends Entiteit
{
	protected $betalingID;				/**< \brief PRIMARY */
	protected $bon;						/**< \brief NULL */
	protected $bon_bonID;				/**< \brief PRIMARY */
	protected $van;
	protected $van_contactID;			/**< \brief PRIMARY */
	protected $naar;
	protected $naar_contactID;			/**< \brief PRIMARY */
	protected $bedrag;
	protected $omschrijving;			/**< \brief NULL */
	/**
	 * @brief De constructor van de IOUBetaling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->betalingID = NULL;
		$this->bon = NULL;
		$this->bon_bonID = NULL;
		$this->van = NULL;
		$this->van_contactID = 0;
		$this->naar = NULL;
		$this->naar_contactID = 0;
		$this->bedrag = 0;
		$this->omschrijving = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld betalingID.
	 *
	 * @return int
	 * De waarde van het veld betalingID.
	 */
	public function getBetalingID()
	{
		return $this->betalingID;
	}
	/**
	 * @brief Geef de waarde van het veld bon.
	 *
	 * @return IOUBon
	 * De waarde van het veld bon.
	 */
	public function getBon()
	{
		if(!isset($this->bon)
		 && isset($this->bon_bonID)
		 ) {
			$this->bon = IOUBon::geef
					( $this->bon_bonID
					);
		}
		return $this->bon;
	}
	/**
	 * @brief Stel de waarde van het veld bon in.
	 *
	 * @param mixed $new_bonID De nieuwe waarde.
	 *
	 * @return IOUBetaling
	 * Dit IOUBetaling-object.
	 */
	public function setBon($new_bonID)
	{
		unset($this->errors['Bon']);
		if($new_bonID instanceof IOUBon
		) {
			if($this->bon == $new_bonID
			&& $this->bon_bonID == $this->bon->getBonID())
				return $this;
			$this->bon = $new_bonID;
			$this->bon_bonID
					= $this->bon->getBonID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_bonID) || $new_bonID == 0)) {
			if($this->bon == NULL && $this->bon_bonID == NULL)
				return $this;
			$this->bon = NULL;
			$this->bon_bonID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_bonID)
		) {
			if($this->bon == NULL 
				&& $this->bon_bonID == (int)$new_bonID)
				return $this;
			$this->bon = NULL;
			$this->bon_bonID
					= (int)$new_bonID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bon geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkBon()
	{
		if (array_key_exists('Bon', $this->errors))
			return $this->errors['Bon'];
		$waarde1 = $this->getBon();
		$waarde2 = $this->getBonBonID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld bon_bonID.
	 *
	 * @return int
	 * De waarde van het veld bon_bonID.
	 */
	public function getBonBonID()
	{
		if (is_null($this->bon_bonID) && isset($this->bon)) {
			$this->bon_bonID = $this->bon->getBonID();
		}
		return $this->bon_bonID;
	}
	/**
	 * @brief Geef de waarde van het veld van.
	 *
	 * @return Persoon
	 * De waarde van het veld van.
	 */
	public function getVan()
	{
		if(!isset($this->van)
		 && isset($this->van_contactID)
		 ) {
			$this->van = Persoon::geef
					( $this->van_contactID
					);
		}
		return $this->van;
	}
	/**
	 * @brief Stel de waarde van het veld van in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return IOUBetaling
	 * Dit IOUBetaling-object.
	 */
	public function setVan($new_contactID)
	{
		unset($this->errors['Van']);
		if($new_contactID instanceof Persoon
		) {
			if($this->van == $new_contactID
			&& $this->van_contactID == $this->van->getContactID())
				return $this;
			$this->van = $new_contactID;
			$this->van_contactID
					= $this->van->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->van == NULL 
				&& $this->van_contactID == (int)$new_contactID)
				return $this;
			$this->van = NULL;
			$this->van_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld van geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld van geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkVan()
	{
		if (array_key_exists('Van', $this->errors))
			return $this->errors['Van'];
		$waarde1 = $this->getVan();
		$waarde2 = $this->getVanContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld van_contactID.
	 *
	 * @return int
	 * De waarde van het veld van_contactID.
	 */
	public function getVanContactID()
	{
		if (is_null($this->van_contactID) && isset($this->van)) {
			$this->van_contactID = $this->van->getContactID();
		}
		return $this->van_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld naar.
	 *
	 * @return Persoon
	 * De waarde van het veld naar.
	 */
	public function getNaar()
	{
		if(!isset($this->naar)
		 && isset($this->naar_contactID)
		 ) {
			$this->naar = Persoon::geef
					( $this->naar_contactID
					);
		}
		return $this->naar;
	}
	/**
	 * @brief Stel de waarde van het veld naar in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return IOUBetaling
	 * Dit IOUBetaling-object.
	 */
	public function setNaar($new_contactID)
	{
		unset($this->errors['Naar']);
		if($new_contactID instanceof Persoon
		) {
			if($this->naar == $new_contactID
			&& $this->naar_contactID == $this->naar->getContactID())
				return $this;
			$this->naar = $new_contactID;
			$this->naar_contactID
					= $this->naar->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->naar == NULL 
				&& $this->naar_contactID == (int)$new_contactID)
				return $this;
			$this->naar = NULL;
			$this->naar_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naar geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaar()
	{
		if (array_key_exists('Naar', $this->errors))
			return $this->errors['Naar'];
		$waarde1 = $this->getNaar();
		$waarde2 = $this->getNaarContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld naar_contactID.
	 *
	 * @return int
	 * De waarde van het veld naar_contactID.
	 */
	public function getNaarContactID()
	{
		if (is_null($this->naar_contactID) && isset($this->naar)) {
			$this->naar_contactID = $this->naar->getContactID();
		}
		return $this->naar_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld bedrag.
	 *
	 * @return float
	 * De waarde van het veld bedrag.
	 */
	public function getBedrag()
	{
		return $this->bedrag;
	}
	/**
	 * @brief Stel de waarde van het veld bedrag in.
	 *
	 * @param mixed $newBedrag De nieuwe waarde.
	 *
	 * @return IOUBetaling
	 * Dit IOUBetaling-object.
	 */
	public function setBedrag($newBedrag)
	{
		unset($this->errors['Bedrag']);
		if(!is_null($newBedrag))
			$newBedrag = (float)$newBedrag;
		if($this->bedrag === $newBedrag)
			return $this;

		$this->bedrag = $newBedrag;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld bedrag geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld bedrag geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkBedrag()
	{
		if (array_key_exists('Bedrag', $this->errors))
			return $this->errors['Bedrag'];
		$waarde = $this->getBedrag();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return IOUBetaling
	 * Dit IOUBetaling-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return IOUBetaling::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van IOUBetaling.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return IOUBetaling::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getBetalingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return IOUBetaling|false
	 * Een IOUBetaling-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$betalingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$betalingID = (int)$a;
		}

		if(is_null($betalingID))
			throw new BadMethodCallException();

		static::cache(array( array($betalingID) ));
		return Entiteit::geefCache(array($betalingID), 'IOUBetaling');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'IOUBetaling');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('IOUBetaling::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `IOUBetaling`.`betalingID`'
		                 .     ', `IOUBetaling`.`bon_bonID`'
		                 .     ', `IOUBetaling`.`van_contactID`'
		                 .     ', `IOUBetaling`.`naar_contactID`'
		                 .     ', `IOUBetaling`.`bedrag`'
		                 .     ', `IOUBetaling`.`omschrijving`'
		                 .     ', `IOUBetaling`.`gewijzigdWanneer`'
		                 .     ', `IOUBetaling`.`gewijzigdWie`'
		                 .' FROM `IOUBetaling`'
		                 .' WHERE (`betalingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['betalingID']);

			$obj = new IOUBetaling();

			$obj->inDB = True;

			$obj->betalingID  = (int) $row['betalingID'];
			$obj->bon_bonID  = (is_null($row['bon_bonID'])) ? null : (int) $row['bon_bonID'];
			$obj->van_contactID  = (int) $row['van_contactID'];
			$obj->naar_contactID  = (int) $row['naar_contactID'];
			$obj->bedrag  = (float) $row['bedrag'];
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'IOUBetaling')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getBonBonID();
		$this->getVanContactID();
		$this->getNaarContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->betalingID =
			$WSW4DB->q('RETURNID INSERT INTO `IOUBetaling`'
			          . ' (`bon_bonID`, `van_contactID`, `naar_contactID`, `bedrag`, `omschrijving`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %f, %s, %s, %i)'
			          , $this->bon_bonID
			          , $this->van_contactID
			          , $this->naar_contactID
			          , $this->bedrag
			          , $this->omschrijving
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'IOUBetaling')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `IOUBetaling`'
			          .' SET `bon_bonID` = %i'
			          .   ', `van_contactID` = %i'
			          .   ', `naar_contactID` = %i'
			          .   ', `bedrag` = %f'
			          .   ', `omschrijving` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `betalingID` = %i'
			          , $this->bon_bonID
			          , $this->van_contactID
			          , $this->naar_contactID
			          , $this->bedrag
			          , $this->omschrijving
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->betalingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenIOUBetaling
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenIOUBetaling($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenIOUBetaling($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Bon';
			$velden[] = 'Van';
			$velden[] = 'Naar';
			$velden[] = 'Bedrag';
			$velden[] = 'Omschrijving';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Van';
			$velden[] = 'Naar';
			$velden[] = 'Bedrag';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Bon';
			$velden[] = 'Van';
			$velden[] = 'Naar';
			$velden[] = 'Bedrag';
			$velden[] = 'Omschrijving';
			break;
		case 'get':
			$velden[] = 'Bon';
			$velden[] = 'Van';
			$velden[] = 'Naar';
			$velden[] = 'Bedrag';
			$velden[] = 'Omschrijving';
		case 'primary':
			$velden[] = 'bon_bonID';
			$velden[] = 'van_contactID';
			$velden[] = 'naar_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Bon';
			$velden[] = 'Van';
			$velden[] = 'Naar';
			$velden[] = 'Bedrag';
			$velden[] = 'Omschrijving';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `IOUBetaling`'
		          .' WHERE `betalingID` = %i'
		          .' LIMIT 1'
		          , $this->betalingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->betalingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `IOUBetaling`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `betalingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->betalingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van IOUBetaling terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'betalingid':
			return 'int';
		case 'bon':
			return 'foreign';
		case 'bon_bonid':
			return 'int';
		case 'van':
			return 'foreign';
		case 'van_contactid':
			return 'int';
		case 'naar':
			return 'foreign';
		case 'naar_contactid':
			return 'int';
		case 'bedrag':
			return 'money';
		case 'omschrijving':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bedrag':
			$type = '%f';
			break;
		case 'betalingID':
		case 'bon_bonID':
		case 'van_contactID':
		case 'naar_contactID':
			$type = '%i';
			break;
		case 'omschrijving':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `IOUBetaling`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `betalingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->betalingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `IOUBetaling`'
		          .' WHERE `betalingID` = %i'
		                 , $veld
		          , $this->betalingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'IOUBetaling');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
