<?
/**
 * $Id$
 */
class IOUBon
	extends IOUBon_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($persoon)
	{
		parent::__construct($persoon); // IOUBon_Generated
	}

	public function magWijzigen()
	{
		return Persoon::getIngelogd() == $this->getPersoon() || parent::magWijzigen();
	}

	public function magVerwijderen()
	{
		// Check of de bon max 10 minuten geleden is aangemaakt
		$now = new DateTimeLocale();
		if($now->getTimestamp() - $this->getGewijzigdWanneer()->getTimestamp() > 10 * 60)
			return false;

		$IOUBetalingFromBonverz = IOUBetalingVerzameling::fromBon($this);
		$IOUSplitFromBonverz = IOUSplitVerzameling::fromBon($this);
		return $this->magWijzigen()
			&& $IOUBetalingFromBonverz->magVerwijderen()
			&& $IOUSplitFromBonverz->magVerwijderen();
	}

	public function verwijderen($foreigncall = NULL)
	{
		$IOUBetalingFromBonverz = IOUBetalingVerzameling::fromBon($this);
		$IOUSplitFromBonverz = IOUSplitVerzameling::fromBon($this);
		$IOUBetalingFromBonverz->verwijderen();
		$IOUSplitFromBonverz->verwijderen();

		parent::verwijderen($foreigncall);
	}

	public function getHeeftbetaald(Persoon $persoon)
	{
		$id = IOUSplitQuery::table()
			->setFetchType('MAYBEVALUE')
			->select('heeftBetaald')
			->whereProp('Bon', $this)
			->whereProp('Persoon', $persoon)
			->get();

		if($id)
			return $id;
		return 0;
	}

	public function getMoetbetalen(Persoon $persoon)
	{
		$id = IOUSplitQuery::table()
			->setFetchType('MAYBEVALUE')
			->select('moetBetalen')
			->whereProp('Bon', $this)
			->whereProp('Persoon', $persoon)
			->get();

		if($id)
			return $id;
		return 0;
	}

	public function url()
	{
		return IOUBASE . 'Bon/' . $this->getBonID();
	}

	public function getBetrokkenen()
	{
		$ids1 = IOUBetalingQuery::table()
			->whereProp('Bon', $this)
			->selectDistinct('van_contactID')
			->setFetchType('COLUMN')
			->get();
		$ids2 = IOUBetalingQuery::table()
			->whereProp('Bon', $this)
			->selectDistinct('naar_contactID')
			->setFetchType('COLUMN')
			->get();

		return array_unique(array_merge($ids1, $ids2));
	}

	public function getBedrag()
	{
		return IOUSplitQuery::table()
			->setFetchType('MAYBEVALUE')
			->select('SUM(heeftBetaald)')
			->whereProp('bon', $this)
			->groupBy('bon_bonID')
			->get();
	}

	public function getSplits()
	{
		return IOUSplitQuery::table()
			->whereProp('Bon', $this)
			->verzamel();
	}

	public function getBetalingen()
	{
		return IOUBetalingQuery::table()
			->whereProp('Bon', $this)
			->verzamel();
	}

	public function getResultaat(Persoon $persoon)
	{
		$ids1 = IOUBetalingQuery::table()
			->select('van_contactID', 'bedrag')
			->whereProp('Bon', $this)
			->whereProp('naar', $persoon)
			->get();
		$ids2 = IOUBetalingQuery::table()
			->select('naar_contactID', 'bedrag')
			->whereProp('Bon', $this)
			->whereProp('van', $persoon)
			->get();

		$result = array();
		foreach($ids2 as $row) {
			$result[$row['naar_contactID']] = $row['bedrag'];
		}

		foreach($ids1 as $row) {
			if(isset($result[$row['van_contactID']]))
				$result[$row['van_contactID']] -= $row['bedrag'];
			else
				$result[$row['van_contactID']] = -1 * $row['bedrag'];
		}

		return $result;
	}

	public function berekenResultaat($wie, $heeftbetaald, $moetbetalen)
	{
		if(Money::isSignificant(array_sum($moetbetalen) - array_sum($heeftbetaald)))
		{
			user_error('Te groot verschil tussen geld in en uit!', E_USER_ERROR);
			return;
		}

		foreach($wie as $who) {
			if(!isset($heeftbetaald[$who]))
				$heeftbetaald[$who] = 0;
			if(!isset($moetbetalen[$who]))
				$moetbetalen[$who] = 0;
		}

		sort($wie);
		$betalingen = new IOUBetalingVerzameling();
		$pos = array();
		$neg = array();
		foreach($wie as $who)
		{
			$diff = $heeftbetaald[$who]
					- $moetbetalen[$who];
			if($diff == 0)
				continue;
			if($diff > 0) {
				$pos[$who] = $diff;
			} else {
				$neg[$who] = $diff;
			}
		}

		// hmm... couldn't find a smart(er) algorithm for this :/
		// this kind of works, but idealy we want to minimise the
		// number of money transactions...
		while(count($neg) && count($pos))
		{
			arsort($pos);
			asort($neg);

			// get the biggest values from the array's
			foreach($neg as $negWho => $negDiff) { break; }
			foreach($pos as $posWho => $posDiff) { break; }

			// $negWho pays everything to $posWho
			if($negDiff + $posDiff >= 0) {
				$betaling = new IOUBetaling();
				$betaling->setBon($this)->setVan($posWho)->setNaar($negWho)->setBedrag($negDiff);
				$betalingen->voegtoe($betaling);
				unset($neg[$negWho]);
				$pos[$posWho] += $negDiff;
				if(!Money::isSignificant($pos[$posWho])) unset($pos[$posWho]);
				continue;
			}

			// $posWho gets $posDiff from $negWho
			$betaling = new IOUBetaling();
			$betaling->setBon($this)->setVan($negWho)->setNaar($posWho)->setBedrag($posDiff);
			$betalingen->voegtoe($betaling);
			unset($pos[$posWho]);
			$neg[$negWho] += $posDiff;
			if(!Money::isSignificant(abs($neg[$negWho]))) unset($neg[$negWho]);
		}

		return $betalingen;
	}

	public function mailen($verwijderen = false)
	{
		if(!$this->getBonId()) {
			user_error('De bon is nog niet opgeslagen!', E_USER_ERROR);
		}

		$afzender	= PersoonView::naam($this->getPersoon());
		$from		= '"IOU '.aesnaam().'" <iou@a-eskwadraat.nl>';

		$omschr		= $this->getOmschrijving();
		$bedragTot	= Money::addPrice($this->getBedrag(), false, true);

		$betrokkenen	= $this->getBetrokkenen();

		$namenbetrokkenen = "";//Dit wordt een string met de namen van de betrokken
		foreach($betrokkenen as $id)
		{
			$persoon = Persoon::geef($id);
			$naam = PersoonView::naam($persoon);
			$namenbetrokkenen .=$naam.",";
		}

		foreach($betrokkenen as $id)
		{
			$persoon = Persoon::geef($id);
			$bedrag = $this->getHeeftbetaald($persoon);
			$bedrag -= $this->getMoetbetalen($persoon);

			if($bedrag > 0) {
				$bedragPersoonlijk
					= sprintf(_('waarvan jij nog %s[VOC: bedrag] %s[VOC: moet of moest] ontvangen')
							, "€ " . Money::addPrice($bedrag, false, true)
							, ($verwijderen ? _('moest') : _('moet'))
							);
			} else {
				$bedragPersoonlijk
					= sprintf(_('waarvan jij nog %s[VOC: bedrag] %s[VOC: moet of moest] inleggen')
							, "€ " . Money::addPrice(-$bedrag, false, true)
							, ($verwijderen ? _('moest') : _('moet'))
							);
			}

			$resultaat = '';
			$balans = '';
			foreach($this->getResultaat($persoon) as $ander => $bedrag)
			{
				//getResult geef mooie arrays waar we nu liever niet mee stoeien
				$ander = Persoon::geef($ander);
				if($bedrag > 0) {

					$resultaat .= sprintf(_("%s nog € %s van jou krijgt\n")
										, PersoonView::naam($ander)
										, Money::addPrice($bedrag, false, true)
										);
					$rekeningnummer = $ander->getRekeningnummer();
					if(!is_null($rekeningnummer) && !$verwijderen)
					{
						$resultaat .= sprintf(_("Je kan dit overmaken op (giro)rekening %s tnv. %s.")
							, $rekeningnummer
							, PersoonView::naam($ander)
							)."\n";
					}

				} else {
					$resultaat .= sprintf(_("dat jij nog € %s krijgt van %s\n")
									, Money::addPrice(-$bedrag, false, true)
									, PersoonView::naam($ander)
									);
				}

				$bals = IOUBetalingVerzameling::balansVanTweePersonen($persoon, $ander);

				if($verwijderen)
				{
					$bals -= $bedrag;
				}

				if($bals < 0)
				{
					$balans .= sprintf(_("%s nog € %s schuldig aan jou is\n")
									, PersoonView::naam($ander)
									, Money::addPrice(abs($bals), false, true));
				}
				else
				{
					$balans .= sprintf(_("jij nog € %s schuldig bent aan %s\n")
									, Money::addPrice(abs($bals), false, true)
									, PersoonView::naam($ander));
				}
			}

			if($verwijderen)
			{
				$titel = _('I-Owe-You - bon VERWIJDERD');
				$tekst	= sprintf(_('Beste %s,

Er is zojuist een bon VERWIJDERD in het %s I-Owe-U-systeem.
Het totaalbedrag van deze bon was %s, %s.

Betrokkenen:  %s
Omschrijving: %s

Voor jouw was het relevant dat:
%s
Dit is nu komen te vervallen en je huidige balans is:
%s
Voor je persoonlijk financieel overzicht kun je, na inloggen,
terecht op %s

Groeten,

A–Eskwadraat "I Owe You",
namens %s')
							, PersoonView::naam($persoon, WSW_NAME_VOORNAAM)
							, aesnaam()
							, "€ " . $bedragTot
							, $bedragPersoonlijk
							, $namenbetrokkenen
							, $omschr
							, $resultaat
							, $balans
							, HTTPS_ROOT.IOUBASE
							, $afzender
							);
			}
			else
			{
				$titel = _('I-Owe-You - nieuwe bon');
				$tekst	= sprintf(_('Beste %s,

Er is zojuist een bon ingevoerd in het %s I-Owe-U-systeem.
Het totaalbedrag van deze bon is %s, %s.

Betrokkenen:  %s
Omschrijving: %s

Het voor jou relevant resultaat van deze bon is dat:
%s
Dit betekent dat de huidige balans is:
%s
Je kan de bon bekijken op %s
en voor je persoonlijk financieel overzicht kun je, na inloggen,
terecht op %s

Groeten,

A–Eskwadraat "I Owe You",
namens %s')
							, PersoonView::naam($persoon, WSW_NAME_VOORNAAM)
							, aesnaam()
							, "€ " . $bedragTot
							, $bedragPersoonlijk
							, $namenbetrokkenen
							, $omschr
							, $resultaat
							, $balans
							, HTTPS_ROOT.$this->url()
							, HTTPS_ROOT.IOUBASE
							, $afzender
							);
			}
			sendmail($from, $persoon->getContactID(), $titel, $tekst);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
