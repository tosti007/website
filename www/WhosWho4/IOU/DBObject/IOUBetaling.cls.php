<?
/**
 * $Id$
 */
class IOUBetaling
	extends IOUBetaling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // IOUBetaling_Generated

		$this->checkID = NULL;
	}

	public function magVerwijderen()
	{
		// Check of de betaling binnen 10 minuten is aangemaakt
		$now = new DateTimeLocale();
		if($now->getTimestamp() - $this->getGewijzigdWanneer()->getTimestamp() > 10 * 60)
			return false;

		return hasAuth('god')
			|| Persoon::getIngelogd() == $this->getVan()
			|| Persoon::getIngelogd() == $this->getNaar();
	}

	public function url()
	{
		return IOUBASE . 'Betaling/' . $this->getBetalingID();
	}

	public function mailen()
	{
		if(!$this->getBetalingId()) {
			user_error('De betaling is nog niet opgeslagen!', E_USER_ERROR);
		}

		$afzender	= PersoonView::naam(Persoon::getIngelogd());
		$from		= '"IOU A–Eskwadraat" <iou@a-eskwadraat.nl>';
		$omschr		= $this->getOmschrijving();

		foreach(array($this->getVan(), $this->getNaar()) as $persoon)
		{
			$tekst	= sprintf(_('Beste %s,

Er is zojuist een verrekening ingevoerd in het A–Eskwadraat-I-Owe-U-systeem.

%s

Voor je persoonlijk financieel overzicht kun je, na inloggen,
terecht op %s

Groeten,

A–Eskwadraat "I Owe You",
namens %s')
							, PersoonView::naam($persoon, WSW_NAME_VOORNAAM)
							, $omschr
							, HTTPS_ROOT.IOUBASE
							, $afzender
							);
			sendmail($from, $persoon->getContactID(), _('I-Owe-You - verrekening'), $tekst);
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
