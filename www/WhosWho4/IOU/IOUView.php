<?
/**
 * $Id$
 */
abstract class IOUView extends View
{
	static public function menu()
	{
		$div = new HtmlDiv(null, 'row');
		$div->add(new HtmlDiv(new HtmlDiv(array(
			HtmlAnchor::button(IOUBASE . 'Bon/Nieuw' , _('Nieuwe bon invoeren')),
			HtmlAnchor::button(IOUBASE . 'Betaling/Nieuw' , _('Nieuwe verrekening invoeren'))
		), 'btn-group'), 'col-sm-6 text-center'));
		$div->add(new HtmlDiv(new HtmlDiv(array(
			HtmlAnchor::button(IOUBASE , _('Persoonlijk financieel overzicht tonen')),
			HtmlAnchor::button(IOUBASE . 'Verzoeken' , _('Mensen verzoeken te betalen'))
		), 'btn-group'), 'col-sm-6 text-center'));
		return $div;
	}

	static public function verzoekenPagina($balans, $rekeningnr, $tennamevan, $mailtekst, $naam, $lidInf, $gemaild)
	{
		$page = Page::getInstance();
		$page->start(_("Mensen verzoeken te betalen"));

		$page->add(IOUView::menu());

		$page->add(new HtmlHeader(3, _("Mensen verzoeken te betalen")));

		$cred = 0;
		foreach($balans as $db) {
			if($db > 0)
				$cred++;
		}

		if($cred == 0) {
			$page->add(_("Je hebt geen wanbetalers!"));
			$page->end();
			return;
		}

		$rowdiv = new HtmlDiv(null, 'row');
		$rowdiv->add($links = new HtmlDiv(null, 'col-md-4'));

		$links->add(new HtmlStrong(_("Selecteer de personen die je wilt mailen"))
				, new HtmlBreak()
				, _("Van de volgende mensen moet je nog geld ontvangen:")
				, $table = new HtmlTable()
				);
		$row = $table->addRow();
		$row->addData(HtmlInput::makeCheckAllCheckbox("all"));
		$row->addData("Selecteer iedereen");
		$row->addData();
		foreach($balans as $lidnr => $bedrag)
		{
			if(round($bedrag,2) <= 0)
				continue;
			$persoon = Persoon::geef($lidnr);
			$row = $table->addRow();
			$row->addData(HtmlInput::makeCheckBox("mailMensjes[".$persoon->getContactID()."]"
							, @$mensjes[$persoon->getContactID()]));
			$row->addData(PersoonView::naam($persoon));
			$row->addData(Money::addPrice($bedrag, true, true))
					->setAttribute('align', 'right');
		}

		$rowdiv->add($rechts = new HtmlDiv(null, 'col-md-8'));

		$rechts->add( new HtmlStrong(_("Rekeninginformatie:"))
					, new HtmlBreak()
					, _("De volgende mail wordt zodirect verzonden naar de door
						jou geselecteerde personen. Uiteraard moet je nog wel
						even een rekeningnummer opgeven en het veld &quot;ten
						name van&quot; invullen.")
					, new HtmlBreak()
					, $pre = new HtmlPre()
					, self::makeFormStringRow('mailRekeningnr', $rekeningnr, _('Je rekeningnr'))
					, self::makeFormStringRow('mailTNV', $tennamevan, _('Ten name van'))
					, HtmlInput::makeFormSubmitButton(_('Mailen!'), 'knop')
					);

		$pre->add(sprintf($mailtekst
								, '$#voornaam$'
								, $naam
								, HTTPS_ROOT.'/Leden/I-Owe-U/$#lidnr$'
								, '$#euros$'
								, PersoonView::naam($lidInf, WSW_NAME_VOORNAAM)
								, ($rekeningnr ? $rekeningnr : '[rekeningnr]')
								, ($tennamevan ? $tennamevan : '[tennamevan]')
								, HTTPS_ROOT.IOUBASE."Betaling/Nieuw?check[".$lidInf->getContactID()."]=on#".$lidInf->getContactID()
								, $naam
								));

		if($gemaild) {
			$rechts->add( new HtmlBreak(), new HtmlBreak()
						, new HtmlEmphasis(_("De geselecteerde mensen zijn gemaild!"))
						);
		}
		if(!empty($melding)) {
			$rechts->add( new HtmlBreak(), new HtmlBreak()
						, new HtmlSpan(implode(new HtmlBreak(), $melding), 'waarschuwing')
						);
		}

		$form = HtmlForm::named('betalenKreng');
		$form->add($rowdiv);
		$page->add($form);

		$page->end();
	}
}
