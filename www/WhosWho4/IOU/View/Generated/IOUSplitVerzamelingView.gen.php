<?
abstract class IOUSplitVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IOUSplitVerzamelingView.
	 *
	 * @param IOUSplitVerzameling $obj Het IOUSplitVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIOUSplitVerzameling(IOUSplitVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
