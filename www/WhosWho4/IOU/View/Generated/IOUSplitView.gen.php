<?
abstract class IOUSplitView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in IOUSplitView.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeIOUSplit(IOUSplit $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld bon.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bon labelt.
	 */
	public static function labelBon(IOUSplit $obj)
	{
		return 'Bon';
	}
	/**
	 * @brief Geef de waarde van het veld bon.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bon van het object obj
	 * representeert.
	 */
	public static function waardeBon(IOUSplit $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getBon())
			return NULL;
		return IOUBonView::defaultWaardeIOUBon($obj->getBon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bon representeert.
	 */
	public static function opmerkingBon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(IOUSplit $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(IOUSplit $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld moetBetalen.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld moetBetalen labelt.
	 */
	public static function labelMoetBetalen(IOUSplit $obj)
	{
		return 'MoetBetalen';
	}
	/**
	 * @brief Geef de waarde van het veld moetBetalen.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld moetBetalen van het object
	 * obj representeert.
	 */
	public static function waardeMoetBetalen(IOUSplit $obj)
	{
		return static::defaultWaardeMoney($obj, 'MoetBetalen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld moetBetalen.
	 *
	 * @see genericFormmoetBetalen
	 *
	 * @param IOUSplit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moetBetalen staat en kan
	 * worden bewerkt. Indien moetBetalen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMoetBetalen(IOUSplit $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'MoetBetalen', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld moetBetalen. In
	 * tegenstelling tot formmoetBetalen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmoetBetalen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld moetBetalen staat en kan
	 * worden bewerkt. Indien moetBetalen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMoetBetalen($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'MoetBetalen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * moetBetalen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld moetBetalen representeert.
	 */
	public static function opmerkingMoetBetalen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld heeftBetaald.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld heeftBetaald labelt.
	 */
	public static function labelHeeftBetaald(IOUSplit $obj)
	{
		return 'HeeftBetaald';
	}
	/**
	 * @brief Geef de waarde van het veld heeftBetaald.
	 *
	 * @param IOUSplit $obj Het IOUSplit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld heeftBetaald van het object
	 * obj representeert.
	 */
	public static function waardeHeeftBetaald(IOUSplit $obj)
	{
		return static::defaultWaardeMoney($obj, 'HeeftBetaald');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld heeftBetaald.
	 *
	 * @see genericFormheeftBetaald
	 *
	 * @param IOUSplit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld heeftBetaald staat en kan
	 * worden bewerkt. Indien heeftBetaald read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formHeeftBetaald(IOUSplit $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'HeeftBetaald', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld heeftBetaald. In
	 * tegenstelling tot formheeftBetaald moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formheeftBetaald
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld heeftBetaald staat en kan
	 * worden bewerkt. Indien heeftBetaald read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormHeeftBetaald($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'HeeftBetaald');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * heeftBetaald bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld heeftBetaald representeert.
	 */
	public static function opmerkingHeeftBetaald()
	{
		return NULL;
	}
}
