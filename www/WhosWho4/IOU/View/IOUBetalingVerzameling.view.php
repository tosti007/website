<?
/**
 * $Id$
 */
abstract class IOUBetalingVerzamelingView
	extends IOUBetalingVerzamelingView_Generated
{
	/**
	 *  Functie die de pagina voor alle betalingen tussen twee personen geeft
	 *
	 * @param ingelogd De 'ingelogde' persoon
	 * @param persoon De 'andere' persson
	 * @param betalingen Alle betalingen tussen de twee personen
	 */
	static public function persoonOverzicht(Persoon $ingelogd, Persoon $persoon, IOUBetalingVerzameling $betalingen)
	{
		// We gaan hier al de tabel maken zodat we niet twee keer over alle
		// betalingen hoeven te lopen, 1 keer voor de tabel en 1 keer om
		// het totaal te berekenen
		$table = new HtmlTable();

		$row = $table->addRow();
		$row->addHeader(_('Datum'));
		$row->addHeader(_('Omschrijving'));
		$row->addHeader(sprintf(_('Jij krijgt van %s'), PersoonView::makeLink($persoon)));

		$total = 0;
		foreach($betalingen as $betaling) {
			$row = $table->addRow();
			$bon = $betaling->getBon();
			if($bon) {
				$row->addData(
					new HtmlAnchor($bon->url()
						, $betaling->getGewijzigdWanneer()->format('Y-m-d H:i:s')
					)
				);
				$row->addData(
					IOUBonView::waardeOmschrijving($bon)
				);
			} else {
				$row->addData(
					new HtmlAnchor($betaling->url()
						, $betaling->getGewijzigdWanneer()->format('Y-m-d H:i:s')
					)
				);
				$row->addData(
					IOUBetalingView::waardeOmschrijving($betaling)
				);
			}
			if($betaling->getVan() == $ingelogd)
				$bedrag = -1*$betaling->getBedrag();
			else
				$bedrag = $betaling->getBedrag();
			$cell = $row->addData(Money::addPrice($bedrag));
			$cell->setAttribute('align','right');

			$total += $bedrag;
		}

		$row = $table->addRow();
		$row->addData();
		$row->addData();
		$cell = $row->addData(Money::addPrice($total));
		$cell->setAttributes(array('align' => 'right','style' => 'border-top: double black'));

		$page = Page::getInstance()->start(
			sprintf(_('Bonnen met %s'),PersoonView::naam($persoon)));

		$page->add(IOUView::menu());

		$page->add(new HtmlHeader(2,
			sprintf(_('Alle bonnen die gerelateerd zijn aan %s en %s:')
				, PersoonView::makeLink($ingelogd)
				, PersoonView::makeLink($persoon))));

		if(round($total, 2) == 0) {
			$text = sprintf(_('Jij en %s staan quitte!')
					, PersoonView::naam($persoon));
		} elseif(round($total,2) < 0) {
			$text = sprintf(_('Jij moet aan %s nog %s betalen!')
					, PersoonView::naam($persoon)
					, Money::addPrice(abs($total)));
		} elseif(round($total,2) > 0) {
			$text = sprintf(_('Jij krijgt van %s nog %s!')
					, PersoonView::naam($persoon)
					, Money::addPrice(abs($total)));
		}

		$page->add(new HtmlDiv($text, 'alert alert-info'));

		$page->add(HtmlAnchor::button(IOUBASE . 'Betaling/Nieuw?check['.$persoon->getContactID().']=on#'.$persoon->getContactID()
			, _('Nieuwe verrekening invoeren')));

		$page->add($table);

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
