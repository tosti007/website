<?php

use \Space\Bedrag;

class iDeal
	extends iDeal_Generated
{
	protected $antwoorden;

	/*** CONSTRUCTOR ***/
	public function __construct ($rubriek = 'BOEKWEB', $soort = 'VERKOOP'
		, $contact = null, $bedrag = 0, $uitleg = null, $wanneer = null
		, $titel = '', $voorraad = null, $rekening = null, $tegenrekening = null)
	{
		parent::__construct ($rubriek, $soort, $contact, $bedrag, $uitleg, $wanneer, $titel, $voorraad);

		$this->expire = new DateTimeLocale();
		$this->entranceCode = self::genereerEntranceCode();
		$this->magOpvragenTijdstip = new DateTimeLocale();

		$this->setRekening($rekening);
		$this->setTegenrekening($tegenrekening);
	}

    /**
     * Mag je de transactie verwijderen?
     *
     * @return true als hij gefaald is, anders false.
     */
    public function magVerwijderen()
    {
        $status = $this->getStatus();
        return Transactie::isStatusGefaald($status); 
    }

	/**
	 *  Initieerd de verbinding met mollie
	 *
	 * @return Het mollie opject
	 */
	static public function initMollie()
	{
		try
		{
			$mollie = new Mollie_API_Client;
			if(DEBUG || DEMO)
				$mollie->setApiKey(MOLLIE_TEST);
			else
				$mollie->setApiKey(MOLLIE_LIVE);

			return $mollie;
		}
		catch(Mollie_API_Exception $e)
		{
			user_error("Mollie-api fout: " . htmlspecialchars($e->getMessage()), E_USER_ERROR);
		}
	}

	/**
	 *  Vraagt een mollie-payment-object van mollie op
	 *
	 * @param mollieId Het externe id van de transactie van mollie
	 *
	 * @return De payment
	 */
	static public function getMolliePayment($mollieId)
	{
		$mollie = iDeal::initMollie();

		try
		{
			$payment = $mollie->payments->get($mollieId);
			return $payment;
		}
		catch(Mollie_API_Exception $e)
		{
			user_error("Mollie-api fout: " . htmlspecialchars($e->getMessage()), E_USER_ERROR);
		}
	}

	/**
	 *  Maakt van een iDeal-object een mollie-payment-object. Zet alleen
	 *  de webhookUrl als we niet op een debug zitten, want de mollie api kan
	 *  daar op een debug niet bij
	 *
	 * @return Het mollie=payment-object
	 */
	public function maakMollieTransactie()
	{
		$mollie = iDeal::initMollie();

		try
		{
			$hashed = $this->maakHash();

			$payment_array = array(
				'amount' => $this->getBedrag(),
				'method' => Mollie_API_Object_Method::IDEAL,
				'description' => $this->getTitel(),
				'redirectUrl' => HTTPS_ROOT 
					. '/Service/IDEAL/Klaar?ec=' . $this->getEntranceCode()
					. '&id=' . $this->geefID()
					. (tryPar('cleanhtml') ? '&cleanhtml=1' : ''),
				'locale' => nl_en('nl_NL', 'en_US'),
			);

			if(!DEBUG) // Op de debug hebben we niks aan webhooks want de debugs zijn http-access beschermd
				$payment_array['webhookUrl'] = HTTPS_ROOT . '/Service/IDEAL/Webhook?trxid=' . $this->geefId() . '&hash=' . $hashed;

			return $mollie->payments->create($payment_array);
		}
		catch(Mollie_API_Exception $e)
		{
			user_error("Mollie-api fout: " . htmlspecialchars($e->getMessage()), E_USER_ERROR);
		}
	}

	public function maakHash()
	{
		return hash('sha512', $this->geefID() . MOLLIE_SALT_KEY);
	}

	/*
	 *  Geeft de idealtransactie die bij het id hoort en bij de gebruiker
	 *
	 * @param id Het transactieID waarvan je het object wilt
	 * @return De transactie die je wilt
	 */
	static public function geefVeilig($id)
	{
		// Override parent geef
		$transactie = iDeal::geef($id);
		// Voeg check toe dat deze gebruiker ook de 'eigenaar' van de transactie is
		if ($transactie && $transactie->getEntranceCode() != iDeal::genereerEntranceCode()) {
			// De sessies zijn anders...
			return null;
		}
		return $transactie;
	}

	/*
	 *  De functie die wordt aangeroepen op een een iDealobject om de
	 * content van de pagina van het idealscherm terug te geven
	 *
	 * @return Een bool om aan te geven of er geen fouten waren; True betekent
	 * alles is goed gegaan, False waren er fouten
	 */
	public function startTransactie()
	{
		//Functie voor gebruik binnen andere controller -> pagina al gestart
		$this->opslaan();

		Page::redirect('/Service/IDEAL/bankGekozen?transid='.$this->geefID().(tryPar('cleanhtml') ? '&cleanhtml=1' : ''));
	}

	/*
	 *  De functie die een idealtransactie goed afhandelt zodat er daadwerkelijk
	 * een verkoop gedaan wordt etc. We weten namelijk nooit zeker wanneer een idealtransactie
	 * voltooid wordt dus daarom is er een omslachtige aparte functie voor
	 *
	 * (Deze functie wordt alleen aangeroepen bij succes.)
	 */
	public function handelAf()
	{
		if (!is_null($this->getVoorraad()))
        {
			$artikelclass = get_class($this->getVoorraad()->getArtikel());
			switch($artikelclass) {
				case 'iDealKaartje':
					$this->handelKaartjeAf();
					break;
                case 'DibsProduct':
					$this->handelDibsAf();
                	break;
				case 'Artikel':
					$this->handelArtikelAf();
					break;
				default:
					user_error("Ik weet niet hoe ik de transactie voor " . $artikelclass . " moet afhandelen!", E_USER_WARNING);
			}
		}
		else
		{
			user_error("Er is geen voorraad bij transactie " . $this->geefID() . " gevonden!", E_USER_ERROR);
		}
	}

	public function handelFaalAf() {
		$voorraad = $this->getVoorraad();
		$class = get_class($voorraad->getArtikel());

		switch ($class) {
		case 'Artikel':
			$this->handelArtikelFaalAf();
			break;
		default:
			user_error("doen we ff later");
		}
	}

    public function handelArtikelFaalAf() {
        global $mylidnr, $MOLLIEWEBHOOKNR;

        $voorraad = $this->getVoorraad();

		// Is dit een inschrijving voor het introkamp?
		if ($voorraad->geefID() == Register::getValue('introKampVoorraad')) {
			// Deze transactie was gefaald, dus we gooien de inschrijving weg.
			$contact = $this->getContact();
			$introDeelnemer = IntroDeelnemer::geef($contact);
			$lid = $introDeelnemer->getLid();
			//gooi de transactie weg, de persoon weg:
			$this->verwijderen();
			$mylidnr = $MOLLIEWEBHOOKNR;
			$errors = $lid->verwijderen();
			$mylidnr = false;
			//wanneer de flippo niet weggegooid wordt, gooi een waarschuwing:
			if (count($errors) > 0) {
				foreach ($errors as $e) {
					user_error("Een introflippo heeft niet betaald en moet worden weggegooid, maar het weggooien geeft de error: " . $e, E_USER_WARNING);
				}
			}
		}
	}

	/*
	 *  Deze functie handelt idealtransacties af als het verkochte product
	 * een iDealKaartje is
	 *
	 * (Deze functie wordt alleen aangeroepen bij succes.)
	 *
	 * @see handelAf
	 */
	public function handelKaartjeAf()
	{
		// Bereken de prijs door de transactiekosten van het bedrag af te halen
		$transactieprijs = $this->getBedrag() - iDeal::transactiekosten()->alsFloat();
		$artikelprijs = $this->getVoorraad()->getVerkoopPrijs()->getPrijs();

		// Hoeveel artikelen zijn ermee verkocht?
		// Zet hier een round neer, omdat het anders een float wordt die naar
		// een verkeerd aantal geparsed wordt bij het opslaan!!!
		$aantal = round($transactieprijs/$artikelprijs);

		// Haal de voorraad op bij het artikel
		$voorraad = $this->getVoorraad();

		// Kunnen we een verkoop doen? Dit hoort wel te kunnen namelijk!
		if($voorraad->getAantal() < $aantal) {
			user_error("Er is iets misgegaan! Er zijn niet meer genoeg producten "
				. "op voorraad bij het afhandelen van de transactie! De iDealtransactie "
				. "is echter wel gelukt, fix dit snel webcie!", E_USER_WARNING);
			sendmail('webcie@a-eskwadraat.nl'
				, 'webcie@a-eskwadraat.nl'
				, 'Niet genoeg producten bij iDealTransactie'
				, "Yo WebCie!\r\nEr is net een iDealtransactie succesvol betaald, "
				. "maar er waren op het moment van verwerken niet meer genoeg producten! "
				. "Het gaat om de transactie met ID " . $this->geefID() . ". "
				. "Fix dit moeilijk snel!\r\n\r\nGroetjes, de Website!"
			);
		} else {
			// Anders doen we een nieuwe verkoop
			$verkoop = Verkoop::maakVerkoop($voorraad, new DateTimeLocale(), $aantal, $this);
			$verkoop->opslaan();
		}

		// Als er niet gecontroleerd wordt op namenlijsten schrijf personen dan
		// toch in als die gevonden worden en personen automatisch moeten worden
		// ingeschreven
		if($this->getVoorraad()->getArtikel()->getDigitaalVerkrijgbaar() != "NAMENLIJST"
			 && $this->getVoorraad()->getArtikel()->getAutoInschrijven())
		{
			$contact = $this->getContact();
			if($contact && $contact instanceof Persoon) {
				$activiteit = $this->getVoorraad()->getArtikel()->getActiviteit();
				if(!Deelnemer::geef($contact, $activiteit)) {
					$deelnemer = new Deelnemer($contact, $activiteit);
					$deelnemer->setMomentInschrijven(date("Y-m-d H:i:s"));
					$deelnemer->opslaan();
				}
			}
		}

		// Als we mensen controleren op naam, schrijf ze dan meteen in voor de act
		if($this->getVoorraad()->getArtikel()->getDigitaalVerkrijgbaar() == "NAMENLIJST")
		{
			$this->handelNamenLijst();
		}
		// Als de kaartjes met codes moeten worden gescand gaan we de codes opsturen
		else if($this->getVoorraad()->getArtikel()->getDigitaalVerkrijgbaar() == 'CODE')
		{
			$this->handelCode($aantal);
		}
	}

	/*
	 *  Deze functie schrijft de koper van het kaartje automatisch in voor
	 * de activiteit in geval van controle door NAMENLIJST
	 */
	public function handelNamenLijst()
	{
		// Als het goed is is er een contact geset, anders geven we een error!
		if(!($contact = $this->getContact())) {
			user_error("Er is iets misgegaan met de inschrijving! Er is geen "
				. "contact gevonden bij de transactie!", E_USER_ERROR);
			sendmail('webcie@a-eskwadraat.nl'
				, 'webcie@a-eskwadraat.nl'
				, 'Geen contact gevonden bij iDealTransactie'
				, "Yo biatches!\r\nEr is net een iDealTransactie betaald, "
				. "maar er was geen contact bij de transactie gevonden om "
				. "in te schrijven voor de activiteit!. Het gaat om de transactie "
				. "met ID " . $this->geefID() . ". Fix dit z.s.m.!\r\n"
				. "Kusjes, de Website"
			);
		}
		// Als het contact geen lid is gaat er natuurlijk ook iets fout
		elseif(!($contact instanceof Persoon))
		{
			user_error("Er is iets misgegaan met de inschrijving! Het contact "
				. "is geen persoon bij de transactie!", E_USER_ERROR);
			sendmail('webcie@a-eskwadraat.nl'
				, 'webcie@a-eskwadraat.nl'
				, 'Contact geen Persoon bij iDealTransactie'
				, "Yo biatches!\r\nEr is net een iDealTransactie betaald, "
				. "maar het contact bij de transactie gevonden is geen Persoon "
				. "Het gaat om de transactie "
				. "met ID " . $this->geefID() . ". Fix dit z.s.m.!\r\n"
				. "Kusjes, de Website"
			);
		}
		// Schrijf het lid in voor de act
		else
		{
			$activiteit = $this->getVoorraad()->getArtikel()->getActiviteit();
			if(!Deelnemer::geef($contact, $activiteit)) {
				$deelnemer = new Deelnemer($contact, $activiteit);
				$deelnemer->setMomentInschrijven(date("Y-m-d H:i:s"));
				$deelnemer->opslaan();
			}
		}
	}

	/*
	 *  Deze functie stuurt qr-codes van kaartjes naar de koper in het geval
	 * van controle door CODE bij de activiteit
	 *
	 * @param aantal Het aantal kaartjes waarvoor een code moet worden gemaakt
	 */
	public function handelCode($aantal)
	{
		// Haal het emailadres op
		$mailtje = new Email();
		$mailtje->setTo($this->getEmailadres());

		//Niet heel mooi, maar het werkt wel :)
		$from = new Contact();
		$from->setEmail('"Tickets @ A-Eskwadraat" <tickets@a-eskwadraat.nl>');
		$mailtje->setFrom($from);

		$kaartjesPNGs = Array();
		$activiteit = $this->getVoorraad()->getArtikel()->getActiviteit();

		for($i=0; $i<$aantal; $i++)
		{
			// Maak de willekeurige code en sla deze op in de db
			$code = iDealKaartjeCode::randomCode();
			$iDealkaartjePNG = iDealKaartjeCode::makeQR($code);
			$iDealkaartjecode = new iDealKaartjeCode($this->getVoorraad()->getArtikel());
			$iDealkaartjecode->setCode($code)
				->setTransactie($this)
				->opslaan();

			// Zet wat extra tekst bij de qr-code
			// TODO: Maak er een mooier kaartje van!
			if ($iDealkaartjePNG !== false)
			{
				$black = imagecolorallocate($iDealkaartjePNG, 0x00, 0x00, 0x00);

				imagefttext($iDealkaartjePNG, 16, 0, 5,		30, 	$black, LIBDIR . "/Consolas.ttf", '1 kaartje ' . ActiviteitView::titel($activiteit));
				imagefttext($iDealkaartjePNG, 28, 0, 10, 	320,	$black, LIBDIR . "/Consolas.ttf", $code);
				imagefttext($iDealkaartjePNG, 16, 0, 160, 	305, 	$black, LIBDIR . "/Consolas.ttf", date("y-m-d"));
				imagefttext($iDealkaartjePNG, 16, 0, 160, 	325, 	$black, LIBDIR . "/Consolas.ttf", date("H:i:s"));
				imagefttext($iDealkaartjePNG, 20, 0, 275, 	315, 	$black, LIBDIR . "/Consolas.ttf", ($i+1) . "/" . $aantal);

				$iDealkaartjesPNGs[] = $iDealkaartjePNG;
			}
			else 
			{
				user_error("Help, kan kaartje $i niet genereren n.a.v. transactie "
					. $transactie->getTransactieID(), E_USER_WARNING);
			}
		}

		if($iDealkaartjesPNGs)
		{
			// Stuur het mailtje naar de koper toe
			$mailtje->setSubject("Kaartjes/Tickets " . ActiviteitView::titel($activiteit));

			$kaartString = $aantal == 1 ? "één kaartje" : "$aantal kaartjes";
			$kaartStringEN = $aantal == 1 ? "A ticket" : "$aantal tickets";

			$bestelnummer = $this->getTransactieID();
			$momentbegin = $activiteit->getMomentBegin();

			// FIXME: dit is lelijk, moet beter kunnen!
			setlocale(LC_ALL, 'en_GB', 'english');
			$eng_time = $momentbegin->strftime('%a %e %b %Y, %R');
			setlocale(LC_ALL, 'nl_NL');

			// FIXME: gebruik hier een template voor!
			$mailtje->setBody(sprintf("<p>For English see below</p>"
				. "<h2>Kaartje(s) gekocht</h2>"
				. "<p>Hallo,</p>"
				. "<p>Deze mail bevat de kaartjes voor '%s' wat begint om %s</p>"
				. "<p>Je moet de kaartjes laten scannen voor binnenkomst. "
				. "Je kunt ze zelf uitprinten, of je laat ze zien op je smartphone "
				. "(zorg er dan wel voor dat het op het scherm goed te zien is).</p>"
				. "<p>Met vriendelijke groet,</p>"
				. "<p>De WebCie</p>"
				. "<b>Problemen?</b>"
				. "<span>Bij deze mail vind je als het goed is %s. "
				. "Is dit niet het geval, of zijn er andere problemen? "
				. "Stuur dan zo spoedig mogelijk een bericht naar tickets@a-eskwadraat.nl "
				. "met een beschrijving van je probleem en "
				. "onder vermelding van je bestelnummer: <b>%s</b>.</span>"
				. "<br>"
				. "<b>Voorwaarden:</b><br />"
				. "- De kaartjes zijn niet persoonsgebonden. "
				. "Je kunt ze dus doorsturen of doorverkopen aan je vrienden."
				. "<br />"
				. "- Een reeds gescand kaartje is niet meer geldig, "
				. "dus als je ze per ongeluk hebt laten slingeren of naar je "
				. "volledige contactenlijst hebt gestuurd kan het zijn dat je niet naar binnen kan."
				. "<br />"
				. "- Alleen kaartjes die je via A-Eskwadraat zijn aangeschaft zijn geldig. "
				. "Als je kaartje niet geldig blijken te zijn bij de entree is er geen discussie mogelijk."
				. "<br />"
				. "- Het is niet mogelijk je zojuist gekochte kaartje te retourneren."
				// Engels
				. "<h2>Ticket(s) bought</h2>"
				. "<p>Hello,</p>"
				. "<p>This email contains the tickets for '%s' which starts at %s</p>"
				. "<p>You have to scan your tickets before entering. "
				. "You can either print the tickets, or you can show them on your smartphone "
				. "(please make sure the tickets are shown properly on the screen).</p>"
				. "<p>Kind regards,</p>"
				. "<p>The Webcommittee</p>"
				. "<b>Problems? </b>"
				. "<span>%s should be attached to this email. If this is not "
				. "the case or if there are any other problems, please send a message "
				. "to tickets@a-eskwadraat.nl with a description of your problem "
				. "and note your order id: <b>%s</b>.</span>"
				. "<br>"
				. "<b>The terms and conditions:</b><br />"
				. "- The tickets are not personal. You can give or sell the tickets to your friends."
				. "<br />"
				. "- A ticket which has already been scanned is no longer valid. "
				. "So if you lost your tickets it might be possible that you are refused."
				. "<br />"
				. "- Only online tickets that are bought via A-Eskwadraat are valid. "
				. "If your ticket is not valid at the entrance there will be no discussion."
				. "<br />"
				. "- There is no refund possible for the tickets."
				, ActiviteitView::titel($activiteit)
				, ActiviteitView::waardeMomentBegin($activiteit)
				, $kaartString
				, $bestelnummer
				, ActiviteitView::titel($activiteit)
				, $eng_time
				, $kaartStringEN
				, $bestelnummer
			));
			// Voeg de kaartjes als attachment toe
			for($i=0; $i<$aantal; $i++)
			{
				//Waardeloze GD-lib heeft geen png->binary string methode...
				ob_start();
				imagepng($iDealkaartjesPNGs[$i]);
				$stringdata = ob_get_contents(); 
				ob_end_clean(); 
				$mailtje->setAttachment("image/png", "kaartje" . ($i+1) . ".png", $stringdata);
			}
		}
		else
		{
			// HELP er is iets niet geluk!
			$mailtje->setSubject("Kaartjes " . ActiviteitView::titel($activiteit));
			$mailtje->setBody("
				<H1>Kaartje(s) kopen mislukt</h1>
				<p>Er is een probleem opgetreden met het genereren van de kaartjes. Neem contact op met de ticketservice: tickets@a-eskwadraat.nl </p>
				<p>Onze excuses voor het ongemak. </p>
				<p>De WebCie</p>
				");
			user_error("Help, ik kan geen lijst met kaartjes genereren!", E_USER_WARNING);
		}

		$mailtje->send();
	}

	/*
	 *  Deze functie handelt kudos kopen via ideal af en schrijft kudos
	 * bij het lid erbij
	 */
	public function handelDibsAf()
	{
		// Haal de prijs op en trek transactiekosten eraf
		$transactieprijs = $this->getBedrag() - iDeal::transactiekosten()->alsFloat();
		$artikelprijs = $this->getVoorraad()->getVerkoopPrijs()->getPrijs();
		// Bereken de hoeveelheid die gekocht is
		$aantal = $transactieprijs/$artikelprijs;

		// Maak een verkoop en sla die op
		$voorraad = $this->getVoorraad();
		$verkoop = Verkoop::maakVerkoop($voorraad, new DateTimeLocale(), $aantal, $this);
		$verkoop->opslaan();

		// Zet de dibs bij het persoon erbij
		$dibsinfo = DibsInfo::geef(Persoon::geef($this->getContactContactID()));
		$transactie = new DibsTransactie('WWW'
			, 'VERKOOP'
			, Persoon::geef($this->getContactContactID())
			, (-1*$aantal) / EURO_TO_KUDOS
			, ''
			, new DateTimeLocale()
			, 'IDEAL'
			, null
		);
		$dibsinfo->nieuweTransactie($transactie);
		$dibsinfo->opslaan();
	}

	/*
	 *  Dit is een generieke handel-af-functie voor alles wat over is. Ook
	 * introkamp valt hieronder maar dat mooier kunnen!
	 */
	public function handelArtikelAf()
	{
		// Haal de prijs op.
		$transactieprijs = $this->getBedrag();
		$artikelprijs = $this->getVoorraad()->getVerkoopPrijs()->getPrijs();
		// Bekeren het aantal
		$aantal = $transactieprijs/$artikelprijs;

		// Doe een verkoop
		$voorraad = $this->getVoorraad();
		$verkoop = Verkoop::maakVerkoop($voorraad, new DateTimeLocale(), $aantal, $this);
		$verkoop->opslaan();

		// Artikelspecifieke afhandeling.
		switch ($voorraad->geefID()) {
		case Register::getValue('introKampVoorraad'):
			// Als het een introkampbetaling is zet dat deelnemer betaald heeft
			IntroDeelnemer::idealBetaald($this->getEmailAdres());
			break;
		case LIDMAATSCHAP_VOORRAAD:
			$this->getContact()->bijnaLidToLid();
			$lidm = LidmaatschapsBetaling::geef($this->getContact());
			$lidm->setIdeal($this);
			$lidm->opslaan();
			break;
		}
	}

	/*
	 *  Deze function wordt alleen gebruikt voor het checken of de ING ons opligt
	 */
	public static function geefUitExternID($trId)
	{
		return iDealQuery::table()
			->whereProp('externeID', $trId)
			->geef();
	}

	/*
	 *  Opent een nieuwe poging in het geval de vorige poging is mislukt
	 *
	 * @return De transactie die de nieuwe poging is
	 */
	public function nieuwePoging()
	{
		$t = new iDeal($this->getRubriek(), $this->getSoort(), $this->getContact()
			, $this->getBedrag(), $this->getUitleg(), new DateTimeLocale()
			, $this->getTitel(), ($this->getVoorraad()) ? $this->getVoorraad() : null
			, $this->getRekening(), $this->getTegenRekening());
		$t->setEmailadres($this->getEmailadres());
		$t->setReturnURL($this->getReturnURL());
		$t->setEntranceCode($this->getEntranceCode());
        $t->setStatus("INACTIVE");

		$t->opslaan();
		return $t;
	}

	/*
	 *  Genereert client-specifieke code (soort sessie-id voor wanneer de klant terugkomt vanaf de bankpagina)
	 *
	 * @return De code
	 */
	public static function genereerEntranceCode()
	{
		global $session;
		// Als we in een cronjob zitten hebben we geen sessie, maar maakt de
		// entrancecode niet zoveel uit
		return substr(sha1($session ? $session->getId() : ""), 0, 20);
	}

	/*
	 *  Set het externeID die bij een transactie hoort als we dat eenmaal
	 * van de ING gekregen hebben
	 *
	 * @param val Het externe ID om te setten
	 * @return this
	 */
	public function setExterneId($val)
	{
		$this->status = 'OPEN';
		return parent::setExterneId($val);
	}

	/*
	 *  Set het contact van de koper
	 *
	 * @param new_contactID het id van het nieuwe contact
	 */
	public function setContact($new_contactID)
	{
		if($this->getEmailadres())
			user_error("Kan geen contact koppelen, je hebt al een emailadres geset slimpie", E_USER_ERROR);
		parent::setContact($new_contactID);
		if($contact = $this->getContact())
		{
			$this->setEmailadres($contact->getEmail());
		}
	}

	/*
	 *  Checkt het bedrag
	 *
	 * @return False als alles goed gaat, anders een string met de foutmelding
	 */
	public function checkBedrag()
	{
		//Bedrag numeriek en kleiner dan 100 euro? (willekeurig maximum)
		if(!is_int($this->bedrag) || $this->bedrag <= 0 || $this->bedrag > 10000)
		{
			return "Opgegeven bedrag ongeldig of groter dan 100 euro!";
		}
		return false;
	}
	/*
	 *  Checkt de Titel
	 *
	 * @return False als alles goed gaat, anders een string met de foutmelding
	 */
	public function checkTitel()
	{
		if(strlen($this->titel) > 32)
			return "Titel ({$this->titel}) mag maximaal 32 tekens zijn!";
		if(self::bevatExotischeTekens($this->titel))
			return "Titel van transactie ({$this->titel}) bevat vreemde tekens";
		return false;
	}
	/*
	 *  Checkt het ArtikelID
	 *
	 * @return False als alles goed gaat, anders een string met de foutmelding
	 */
	public function checkArtikelId()
	{
		if(strlen($this->artikelId) > 16)
			return "ArtikelId ({$this->artikelId}) mag maximaal 16 tekens zijn!";
		if(self::bevatExotischeTekens($this->artikelId))
			return "ArtikelId ({$this->artikelId}) bevat vreemde tekens";
		return false;
	}
	/*
	 *  Checkt de returnURL
	 *
	 * @return False als alles goed gaat, anders een string met de foutmelding
	 */
	public function checkReturnURL()
	{
		//if(strpos($this->returnURL, 'https://') !== 0)
		//	return "ReturnURL moet met https:// beginnen!";
		if(strlen($this->returnURL) > 512)
			return "ReturnURL mag maximaal 512 tekens bevatten";
		return false;
	}
	/*
	 *  Checkt de entrancecode
	 *
	 * @return False als alles goed gaat, anders een string met de foutmelding
	 */
	public function checkEntranceCode()
	{
		if(!preg_match('/[a-zA-Z0-9]{5,40}/', $this->entranceCode))
			return "Entrancecode is te kort of te lang (5<>40) of bevat niet-alphanumerieke tekens!";
		return false;
	}

	/*
	 *  Haalt de status van een transactie op
	 *
	 * @param payment De mollie payment die bij deze transactie hoort
	 *
	 * @return De status van de transactie
	 */
	public function haalStatusOp($payment)
	{
		//Eindstatussen mogen niet opnieuw opgevraagd worden
		if($this->status != 'OPEN'
				&& $this->status != 'OPENPOGING2'
				&& $this->status != 'OPENPOGING3'
				&& $this->status != 'OPENPOGING4'
				&& $this->status != 'OPENPOGING5')
			return $this->getStatus();

		$status = $payment->status;

		switch(strtolower($status))
		{
			case 'paid':
				$this->setStatus("SUCCESS");
				$this->setKlantNaam($payment->details->consumerName);
				$this->setTegenrekening($payment->details->consumerAccount);
				$this->opslaan();
				break;
			case 'cancelled' :
				$this->setStatus("CANCELLED");
				$this->opslaan();
				break;
			case 'expired' :
				$this->setStatus("EXPIRED");
				$this->opslaan();
				break;
			case 'failed':
				$this->setStatus("FAILURE");
				$this->opslaan();
				break;
			case 'paidout':
				break;
			case 'open': 
				break;
			default:
				user_error("Fout bij ophalen status: " . $status . " is niet een geldige response-status", E_USER_WARNING);
				$status = 'open';
		}

		return $this->getStatus();
	}

	/*
	 *  Een check om te kijken of er geen exotische tekens in een waarde zitten
	 *
	 * @param val De string waarvan je het wilt controleren
	 * @return 1 als er iets gevonden is, 0 anders
	 */
	protected static function bevatExotischeTekens($val)
	{
		$pattern = "/[^A-Za-z0-9\=\ \%\*\+\-\,\.\/\\\&\@\"\'\:\;\?\(\)\$]/";
		return preg_match($pattern, $val);
	}

	public function antwoordenWijzigenHeeftZin()
	{
		return $this->getAntwoorden()->aantal() > 0;
	}

	public function getAntwoorden()
	{
		if(!$this->antwoorden instanceof iDealAntwoordVerzameling)
		{
			if($this->getVoorraad()->getArtikel() instanceof iDealKaartje)
			{
				$act = $this->getVoorraad()->getArtikel()->getActiviteit();
				$vragen = ActiviteitVraagVerzameling::vanActiviteit($act);
				$this->antwoorden = new iDealAntwoordVerzameling();

				foreach($vragen as $vraag)
				{
					$antwoord = null;
					if($this->inDB)
						$antwoord = iDealAntwoord::geef($this, $vraag);
					if(!$antwoord) {
						$antwoord = new iDealAntwoord($this, $vraag);
					}
					$this->antwoorden->voegtoe($antwoord);
				}
			}
			else
			{
				$this->antwoorden = new iDealAntwoordVerzameling();
			}
		}
		return $this->antwoorden;
	}

	public function setAntwoorden(iDealAntwoordVerzameling $antwoorden)
	{
		$this->antwoorden = $antwoorden;
	}

	/**
	 * Geef hoeveel Mollie rekent om een transactie te doen.
	 *
	 * We schuiven deze kosten door naar degene die een transactie doet.
	 * Deze waarde moet je optellen bij het bedrag bij het aanmaken van een iDeal-object.
	 *
	 * Dit bedrag is inclusief BTW.
	 *
	 * @return Bedrag
	 */
	public static function transactiekosten()
	{
		return new Bedrag(Register::getValue('idealTransactiekosten'));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
