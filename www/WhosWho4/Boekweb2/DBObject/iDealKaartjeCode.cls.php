<?
use Endroid\QrCode\QrCode;

class iDealKaartjeCode
	extends iDealKaartjeCode_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($kaartje = null)
	{
		parent::__construct($kaartje); // KaartjeCode_Generated
	}

	static public function randomCode ()
	{
		$code = randstr(6);
		while(iDealKaartjeCodeQuery::table()->whereLikeProp('code', $code)->count() > 0) {
			$code = randstr(6);
		}

		return $code;
	}

	public static function makeQR($data)
	{
		//$data, naar string, medium foutcorrectie, size, border, geen headers
		$QrCode = new QrCode($data);
		$QrCode->setSize(210);
		$QrCode->setMargin(60);
		$QrCode->writeFile("/tmp/qr-code.$data");
		$qrcode = imagecreatefrompng("/tmp/qr-code.$data");
		unlink("/tmp/qr-code.$data");
		return $qrcode;
	}
}
