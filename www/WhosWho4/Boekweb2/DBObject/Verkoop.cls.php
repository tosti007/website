<?
class Verkoop
	extends Verkoop_Generated
{
	/** Verzamelingen **/
	/*** CONSTRUCTOR ***/
	/**
	 * @param a Voorraad ( Voorraad OR Array(voorraad_voorraadID) OR voorraad_voorraadID )
	 * @param b Transactie ( Transactie OR Array(transactie_transactieID) OR transactie_transactieID )
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct($a, $b); // Verkoop_Generated

		$this->voorraadMutatieID = NULL;
	}

	static public function maakVerkoop($voorraad, $wanneer, $aantal, $transactie)
	{
		$aantal *= -1;
		$verkoop = new self($voorraad, $transactie);
		$verkoop->setWanneer($wanneer);
		$verkoop->setAantal($aantal);
		return $verkoop;
	}

	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Voorraad';
		$volgorde[] = 'Transactie';
		return $volgorde;
	}
}
