<?
class LeverancierRetour
	extends LeverancierRetour_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($voorraad, $wanneer, $aantal, $order, $leverancier)
	{
		parent::__construct($voorraad, $wanneer, $aantal, $order, $leverancier); // LeverancierRetour_Generated

		$this->voorraadMutatieID = NULL;
	}
}
