<?
class Telling
	extends Telling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($wanneer = NULL, $teller = NULL)
	{
		parent::__construct($wanneer, $teller = NULL); // Telling_Generated

		$this->tellingID = NULL;
	}

	public function url()
	{
		return BOEKWEBBASE . 'Telling/' . $this->geefID();
	}

	public function geefItems()
	{
		return TellingItemVerzameling::geefItemsVanTelling($this);
	}
}
