<?
/**
 * $Id$
 */
class Artikel
	extends Artikel_Generated
{
	public function url ()
    {
		return BOEKWEBBASE . 'Artikel/' . $this->geefID();
    }

	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Artikel_Generated

		$this->artikelID = NULL;
	}

	public function checkVerwijderen()
	{
		if(!hasAuth('bestuur')) {
			return _("Je bent geen bestuurslid!");
		}
		if($this->getVoorraden()->aantal() != 0) {
			return _("Er zitten voorraden aan het artikel gekoppeld!");
		}
		return false;
	}

	public function magVerwijderen()
	{
		if(!hasAuth('bestuur'))
			return false;
		if($this->getVoorraden()->aantal() != 0) {
			return false;
		}
		return true;
	}

	public function getAantalVerkopen ()
	{
		return -1 * VoorraadMutatieQuery::table()
			->join('Voorraad')
			->whereString('VoorraadMutatie.overerving', 'Verkoop')
			->whereProp('Artikel', $this)
			->setFetchType('MAYBEVALUE')
			->select('SUM(VoorraadMutatie.aantal)')
			->get();
	}

	public function inVoorraad ()
	{
		return VoorraadMutatieQuery::table()
			->join('Voorraad')
			->whereProp('Artikel', $this)
			->setFetchType('MAYBEVALUE')
			->select('SUM(VoorraadMutatie.aantal)')
			->get();
	}

	/**
	 *  Maak de nieuwe order aan die hoort bij dit soort Artikels.
	 * Is nodig omdat voor bijvoorbeeld Dictaten we kaften moeten bijhouden.
	 * @param leverancier De leverancier van deze order, of NULL.
	 * @returns Een instance van Order of een subklasse daarvan.
	 */
	public function nieuweOrder($leverancier)
	{
		return new Order($this, $leverancier);
	}

	public function voorradenInBoekenhok()
	{
		return VoorraadVerzameling::vanArtikelInBoekenhok($this);
	}

	public function getJaargangArtikelRelaties ()
	{
		return JaargangArtikelVerzameling::vanArtikel($this);
	}

	public function getOrderRelaties ()
	{
		return OrderVerzameling::vanArtikel($this);
	}

	public function getOpenOrderRelaties ()
	{
		return OrderVerzameling::openVanArtikel($this);
	}

	public function getGeslotenOrderRelaties ()
	{
		return OrderVerzameling::geslotenVanArtikel($this);
	}

	public function getGeannuleerdOrderRelaties ()
	{
		return OrderVerzameling::geannuleerdVanArtikel($this);
	}

	public function getVoorraden ()
	{
		return VoorraadVerzameling::voorradenVanArtikel($this);
	}

	public function getVerkoopbareVoorraadRelaties ()
	{
		return VoorraadVerzameling::verkoopbaarVanArtikel($this);
	}

	public function getOnverkoopbareVoorraadRelaties ()
	{
		return VoorraadVerzameling::onverkoopbaarVanArtikel($this);
	}

	public function getVerkoopRelaties ()
	{
		return VerkoopVerzameling::vanArtikel($this);
	}

	public function getVoorraadMutatieRelaties ()
	{
		return VoorraadMutatieVerzameling::vanArtikel($this);
	}

	/**
	 * @brief Is het toegestaan dat een voorraad van dit Artikel verkocht wordt?
	 *
	 * Dit wordt gebruikt in Voorraad::checkVerkoopbaar,
	 * als een artikel nog niet "af" is wil je verkopen voorkomen.
	 * Let op: dit slaat alleen op formulierverwerking,
	 * en wordt niet gebruikt bij beslissen of een voorraad verkoopbaar is!
	 *
	 * Defaultimplementatie is altijd FALSE geven.
	 *
	 * @return Een string die aangeeft waarom $voorraad->getVerkoopbaar() op TRUE zetten niet mag,
	 * of FALSE als er geen bezwaar is om het op TRUE te zetten.
	 */
	public function verkopenVerboden() {
		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
