<?
/**
 * $Id$
 */
class Vak
	extends Vak_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Vak_Generated

		$this->vakID = NULL;
	}

	/**
	 * Geef het vak met de code of null indien niet gevonden.
	 *
	 * @param string $code De code van het vak.
	 * @return Vak|null
	 */
	static public function geefDoorCode ($code)
	{
		return VakQuery::table()
			->whereProp('code', $code)
			->geef();
	}

	/**
	 * @inheritdoc
	 */
	public static function magKlasseBekijken()
	{
		return hasAuth('ingelogd');
	}

	public function magWijzigen() {
		return hasAuth('tbc');
	}

	public function magVerwijderen() {
		return hasAuth('bestuur');
	}

	public function url ()
	{
		return '/Onderwijs/Vak/'.$this->geefID().'/';
	}

	public function wijzigURL()
	{
		return $this->url() . 'Wijzig';
	}

	public function verwijderURL()
	{
		return $this->url() . 'Verwijder';
	}

	/**
	 * @inheritdoc
	 *
	 * @return bool|string
	 */
	public function checkCode()
	{
		// Neem eventuele foutmeldingen over van de parentklasse.
		$error = parent::checkCode();
		if ($error)
		{
			return $error;
		}

		// De vakcode moet uniek zijn.
		// Merk op dat null != een willekeurig object.
		$zelfdeCode = Vak::geefDoorCode($this->getCode());
		if (!is_null($zelfdeCode) && $zelfdeCode != $this)
		{
			return _("Er is een ander vak bekend met deze code!");
		}

		return false;
	}

	/**
	 * Voeg een studie toe aan dit vak.
	 *
	 * Als de studie al bij het vak hoort, gebeurt er niets.
	 *
	 * @param Studie $studie De studie om toe te voegen.
	 *
	 * @return VakStudie
	 * De VakStudie is nog niet opgeslagen!
	 */
	public function addStudie(Studie $studie)
	{
		// Doe niets als de studie er al bijhoort.
		// We kunnen alleen checken als dit al in de databaas is opgeslagen.
		if ($this->inDB && $vakstudie = VakStudie::geef($this, $studie))
		{
			return $vakstudie;
		}

		// Maak een nieuwe VakStudie aan, maar sla die niet op,
		// want het Vak kan nog verwijderd moeten worden.
		$vakstudie = new VakStudie($this, $studie);
		return $vakstudie;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
