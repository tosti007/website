<?
/**
 * $Id$
 */
class Verplaatsing
	extends Verplaatsing_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($voorraad, $wanneer = null,
								 $aantal = null, $tegenvoorraad)
	{
		parent::__construct($voorraad, $tegenvoorraad);
		$this->setWanneer($wanneer);
		$this->setAantal($aantal);
	}

	public function opslaan ($classname = 'Verplaatsing', $tegen = false)
	{
		if ($this->inDB)
			return;

		$this->getTegenVoorraad()->opslaan();

		if (!$tegen)
		{
			$mutatie = new Verplaatsing($this->getTegenVoorraad(), $this->getWanneer(),
									-1 * $this->getAantal(), $this->getVoorraad());
			$mutatie->opslaan('Verplaatsing', true);
		}

		return parent::opslaan($classname);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
