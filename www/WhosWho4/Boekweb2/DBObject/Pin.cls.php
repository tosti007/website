<?
/**
 * $Id$
 */
class Pin
	extends Pin_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($rubriek = null, $soort = null, $contact = null, $bedrag = null, $uitleg, $wanneer = null, $rekening = '507122690', $apparaat = null)
	{
		parent::__construct($rubriek, $soort, $contact, $bedrag, $uitleg, $wanneer, $rekening, $apparaat);

		$this->rekening = Register::getValue('pinRekening');

		if (in_array($apparaat, $this->enumsApparaat()))
			$this->apparaat = $apparaat;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
