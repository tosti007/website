<?
/**
 * $Id$
 */
class VoorraadMutatie
	extends VoorraadMutatie_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($voorraad = null, $wanneer = null,
								$aantal = null)
	{
		parent::__construct($voorraad, $wanneer, $aantal);
	}

	static public function veldenVoorraadMutatie ($welke = null)
	{
		$velden = parent::veldenVoorraadMutatie($welke);

		if ($welke == 'set')
			$velden[] = 'Aantal';

		return $velden;
	}

	public function checkAantal ()
	{
		if ($this->inDB)
			return false;

		if ($this->aantal == 0)
			return _('geen lege voorraadmutaties toegestaan');

		$voorraad = $this->getVoorraad();
		if (!$voorraad->isVirtueel())
		{
			$val = VoorraadMutatieQuery::table()
				->select('SUM(aantal)')
				->setFetchType('MAYBEVALUE')
				->whereProp('Voorraad', $this->getVoorraad())
				->get();
			if ($val + $this->aantal < 0)
				return _('zoveel artikelen zijn er niet');
		}

		return false;
	}

	public function opslaan ($classname = 'VoorraadMutatie')
	{
		if ($this->voorraad instanceof Voorraad)
			$this->voorraad->opslaan();
		return parent::opslaan($classname);
	}

	public function getArtikel ()
	{
		return $this->getVoorraad()->getArtikel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
