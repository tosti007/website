<?
class iDealKaartje
	extends iDealKaartje_Generated
{
	static public function zoek (Activiteit $activiteit)
	{
		return iDealKaartjeQuery::table()
			->whereProp('Activiteit', $activiteit)
			->geef();
	}

	/*** CONSTRUCTOR ***/
	public function __construct($activiteit = null)
	{
		parent::__construct($activiteit); // Kaartje_Generated
	}

	/**
	 * @brief Is de verkoop van dit kaartje per persoon?
	 *
	 * Zo ja, is het de bedoeling dat iemand maar 1 koopt.
	 * (We kunnen dit niet altijd checken als het over externen gaat.)
	 * Zo nee, kan iemand in 1 transactie veel tegelijk kopen,
	 * bijvoorbeeld om aan vrienden te sturen.
	 *
	 * @returns Een boolean.
	 */
	public function isPerPersoon() {
		return $this->getDigitaalVerkrijgbaar() == "NAMENLIJST" ||
			$this->getDigitaalVerkrijgbaar() == "PERSOONLIJK";
	}
}
