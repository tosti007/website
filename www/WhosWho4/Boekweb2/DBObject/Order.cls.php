<?
/**
 * $Id$
 */
class Order
	extends Order_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($artikel = null, $leverancier = null, $waardePerStuk = 0, $datumGeplaatst = null)
	{
		parent::__construct($artikel);

		$this->orderID = null;

		$this->setLeverancier($leverancier);

		$this->waardePerStuk = (float)$waardePerStuk;

		$this->datumGeplaatst = is_null($datumGeplaatst) ? new DateTimeLocale() : $datumeGeplaatst;
		if (!is_null($datumGeplaatst) && !$datumGeplaatst->hasTime()) {
			// De tijd boeit niet dus zet het op 0.
			$this->datumGeplaatst->setTime(0, 0, 0);
		}
	}

	public function checkAantal ()
	{
		$parent = parent::checkAantal();
		if ($parent) return $parent;

		$waarde = $this->getAantal();
		if ($waarde <= 0)
			return _('dit moet positief zijn');

		return false;
	}

	public function checkArtikel() {
		if ($this->artikel == NULL) {
			return _('dit moet een bestaand artikel zijn');
		}
		return false;
	}

	public function checkLeverancier() {
		if(is_null($this->getLeverancier())) {
			return _("Dit is een verplicht veld");
		}
		if(!is_null($this->getLeverancier())) {
			if(is_null($this->getLeverancier()->geefID())) {
				return _("Deze leverancier bestaat niet");
			}
		}
		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
