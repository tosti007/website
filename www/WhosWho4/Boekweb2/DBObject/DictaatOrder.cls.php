<?
class DictaatOrder
	extends DictaatOrder_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($artikel = null, $leverancier = null, $waardePerStuk = 0, $kaftPrijs = 0, $marge = 0)
	{
		parent::__construct($artikel, $leverancier); // DictaatOrder_Generated

		$this->orderID = NULL;
		$this->waardePerStuk = (float)$waardePerStuk;
		$this->kaftPrijs = (float)$kaftPrijs;
		$this->marge = (float)$marge;
	}
}
