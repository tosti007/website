<?
class iDealAntwoord
	extends iDealAntwoord_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // iDealAntwoord_Generated
	}

	public function valid ($welkeVelden = 'set')
	{
		$bool = parent::valid($welkeVelden);

		if($this->getVraag()->getType() == 'ENUM'
			&& !$this->getAntwoord())
		{
			$this->errors['Antwoord'] = _('Selecteer een optie');
			$bool = false;
		}
		else
		{
			if($this->errors['Antwoord'])
				$bool = true;
			$this->errors['Antwoord'] = false;
		}

		return $bool;
	}
}
