<?
/**
 * $Id$
 */
class Levering
	extends Levering_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct ($voorraad = null,
		$wanneer = null,
		$aantal = null,
		$order = null,
		$leverancier = null)
	{
		parent::__construct($voorraad); // Levering_Generated

		$this->setWanneer($wanneer);
		$this->setAantal($aantal);
		$this->setOrder($order);
		$this->setLeverancier($leverancier);
	}

	public function getErrors($welkeVelden = 'set')
	{
		$this->errors['order'] = $this->checkOrder();
		$this->errors['leverancier'] = $this->checkLeverancier();
		// TODO: waarom is parent::getErrors niet gewoon VoorraadMutatie::getErrors?
		$this->errors = array_merge($this->errors, $this->getVoorraad()->getErrors());
		return parent::getErrors($welkeVelden);
	}

	public function checkAantal ()
	{
		if (!$return = parent::checkAantal())
			return $return;

		if ($this->order instanceof Order)
			if ($this->order->getAantal() == $this->aantal)
				return _('Komt niet overeen met de order');

		if ($this->aantal <= 0)
			return _('Dit moet groter dan 0 zijn');

		return false;
	}

	public function checkOrder()
	{
		if(!is_null($this->getOrder())) {
			if (!$this->getOrder()->valid()) {
				return _("Deze order is niet geldig");
			}
		}
		return false;
	}

	public function checkLeverancier()
	{
		if(is_null($this->getLeverancier())) {
			return _("Dit is een verplicht veld");
		}
		if(!is_null($this->getLeverancier())) {
			if(is_null($this->getLeverancier()->geefID())) {
				return _("Deze leverancier bestaat niet");
			}
		}
		return false;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
