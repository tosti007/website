<?
abstract class VoorraadMutatie_Generated
	extends Entiteit
{
	protected $voorraadMutatieID;		/**< \brief PRIMARY */
	protected $voorraad;
	protected $voorraad_voorraadID;		/**< \brief PRIMARY */
	protected $wanneer;
	protected $aantal;
	/**
	/**
	 * @brief De constructor van de VoorraadMutatie_Generated-klasse.
	 *
	 * @param mixed $a Voorraad (Voorraad OR Array(voorraad_voorraadID) OR
	 * voorraad_voorraadID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Voorraad)
		{
			$this->voorraad = $a;
			$this->voorraad_voorraadID = $a->getVoorraadID();
		}
		else if(is_array($a))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->voorraadMutatieID = NULL;
		$this->wanneer = new DateTimeLocale();
		$this->aantal = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Voorraad';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld voorraadMutatieID.
	 *
	 * @return int
	 * De waarde van het veld voorraadMutatieID.
	 */
	public function getVoorraadMutatieID()
	{
		return $this->voorraadMutatieID;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @return Voorraad
	 * De waarde van het veld voorraad.
	 */
	public function getVoorraad()
	{
		if(!isset($this->voorraad)
		 && isset($this->voorraad_voorraadID)
		 ) {
			$this->voorraad = Voorraad::geef
					( $this->voorraad_voorraadID
					);
		}
		return $this->voorraad;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad_voorraadID.
	 *
	 * @return int
	 * De waarde van het veld voorraad_voorraadID.
	 */
	public function getVoorraadVoorraadID()
	{
		if (is_null($this->voorraad_voorraadID) && isset($this->voorraad)) {
			$this->voorraad_voorraadID = $this->voorraad->getVoorraadID();
		}
		return $this->voorraad_voorraadID;
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld wanneer.
	 */
	public function getWanneer()
	{
		return $this->wanneer;
	}
	/**
	 * @brief Stel de waarde van het veld wanneer in.
	 *
	 * @param mixed $newWanneer De nieuwe waarde.
	 *
	 * @return VoorraadMutatie
	 * Dit VoorraadMutatie-object.
	 */
	public function setWanneer($newWanneer)
	{
		unset($this->errors['Wanneer']);
		if(!$newWanneer instanceof DateTimeLocale) {
			try {
				$newWanneer = new DateTimeLocale($newWanneer);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->wanneer->strftime('%F %T') == $newWanneer->strftime('%F %T'))
			return $this;

		$this->wanneer = $newWanneer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld wanneer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld wanneer geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkWanneer()
	{
		if (array_key_exists('Wanneer', $this->errors))
			return $this->errors['Wanneer'];
		$waarde = $this->getWanneer();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @return int
	 * De waarde van het veld aantal.
	 */
	public function getAantal()
	{
		return $this->aantal;
	}
	/**
	 * @brief Stel de waarde van het veld aantal in.
	 *
	 * @param mixed $newAantal De nieuwe waarde.
	 *
	 * @return VoorraadMutatie
	 * Dit VoorraadMutatie-object.
	 */
	public function setAantal($newAantal)
	{
		unset($this->errors['Aantal']);
		if(!is_null($newAantal))
			$newAantal = (int)$newAantal;
		if($this->aantal === $newAantal)
			return $this;

		$this->aantal = $newAantal;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aantal geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aantal geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAantal()
	{
		if (array_key_exists('Aantal', $this->errors))
			return $this->errors['Aantal'];
		$waarde = $this->getAantal();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return VoorraadMutatie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van VoorraadMutatie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array('Levering',
			'Verplaatsing',
			'Verkoop',
			'LeverancierRetour');
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return VoorraadMutatie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getVoorraadMutatieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return VoorraadMutatie|false
	 * Een VoorraadMutatie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$voorraadMutatieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$voorraadMutatieID = (int)$a;
		}

		if(is_null($voorraadMutatieID))
			throw new BadMethodCallException();

		static::cache(array( array($voorraadMutatieID) ));
		return Entiteit::geefCache(array($voorraadMutatieID), 'VoorraadMutatie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			$ids = Entiteit::nietInCache($ids, 'VoorraadMutatie');

			// is er nog iets te doen?
			if(empty($ids)) return;

			// zoek uit wat de klasse van een id is
			$res = $WSW4DB->q('TABLE SELECT `voorraadMutatieID`'
			                 .     ', `overerving`'
			                 .' FROM `VoorraadMutatie`'
			                 .' WHERE (`voorraadMutatieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );

			$recur = array();
			foreach($res as $row)
			{
				$recur[$row['overerving']][]
				    = array($row['voorraadMutatieID']);
			}
			foreach($recur as $class => $obj)
			{
				$class::cache($obj, True);
			}

			return;
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('VoorraadMutatie::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `VoorraadMutatie`.`voorraadMutatieID`'
			                 .     ', `VoorraadMutatie`.`voorraad_voorraadID`'
			                 .     ', `VoorraadMutatie`.`wanneer`'
			                 .     ', `VoorraadMutatie`.`aantal`'
			                 .     ', `VoorraadMutatie`.`gewijzigdWanneer`'
			                 .     ', `VoorraadMutatie`.`gewijzigdWie`'
			                 .' FROM `VoorraadMutatie`'
			                 .' WHERE (`voorraadMutatieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['voorraadMutatieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['VoorraadMutatie'][$id];
			} else {
				$obj = new VoorraadMutatie(array($row['voorraad_voorraadID']));
			}

			$obj->inDB = True;

			$obj->voorraadMutatieID  = (int) $row['voorraadMutatieID'];
			$obj->wanneer  = new DateTimeLocale($row['wanneer']);
			$obj->aantal  = (int) $row['aantal'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			if($recur === True)
				self::stopInCache($id, $obj);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'VoorraadMutatie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getVoorraadVoorraadID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->voorraadMutatieID =
			$WSW4DB->q('RETURNID INSERT INTO `VoorraadMutatie`'
			          . ' (`overerving`, `voorraad_voorraadID`, `wanneer`, `aantal`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %s, %i, %s, %i)'
			          , $classname
			          , $this->voorraad_voorraadID
			          , $this->wanneer->strftime('%F %T')
			          , $this->aantal
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'VoorraadMutatie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `VoorraadMutatie`'
			          .' SET `voorraad_voorraadID` = %i'
			          .   ', `wanneer` = %s'
			          .   ', `aantal` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `voorraadMutatieID` = %i'
			          , $this->voorraad_voorraadID
			          , $this->wanneer->strftime('%F %T')
			          , $this->aantal
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->voorraadMutatieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVoorraadMutatie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVoorraadMutatie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVoorraadMutatie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Wanneer';
			$velden[] = 'Aantal';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Wanneer';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Voorraad';
			$velden[] = 'Wanneer';
			$velden[] = 'Aantal';
			break;
		case 'get':
			$velden[] = 'Voorraad';
			$velden[] = 'Wanneer';
			$velden[] = 'Aantal';
		case 'primary':
			$velden[] = 'voorraad_voorraadID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Voorraad';
			$velden[] = 'Wanneer';
			$velden[] = 'Aantal';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `VoorraadMutatie`'
		          .' WHERE `voorraadMutatieID` = %i'
		          .' LIMIT 1'
		          , $this->voorraadMutatieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->voorraadMutatieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `VoorraadMutatie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `voorraadMutatieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->voorraadMutatieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van VoorraadMutatie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'voorraadmutatieid':
			return 'int';
		case 'voorraad':
			return 'foreign';
		case 'voorraad_voorraadid':
			return 'int';
		case 'wanneer':
			return 'datetime';
		case 'aantal':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'voorraadMutatieID':
		case 'voorraad_voorraadID':
		case 'aantal':
			$type = '%i';
			break;
		case 'wanneer':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `VoorraadMutatie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `voorraadMutatieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->voorraadMutatieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `VoorraadMutatie`'
		          .' WHERE `voorraadMutatieID` = %i'
		                 , $veld
		          , $this->voorraadMutatieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'VoorraadMutatie');
		parent::stopInCache($id, $obj, 'Levering');
		parent::stopInCache($id, $obj, 'Verplaatsing');
		parent::stopInCache($id, $obj, 'Verkoop');
		parent::stopInCache($id, $obj, 'LeverancierRetour');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
