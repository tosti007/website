<?
abstract class Giro_Generated
	extends Transactie
{
	protected $rekening;				/**< \brief NULL */
	protected $tegenrekening;			/**< \brief NULL */
	/**
	/**
	 * @brief De constructor van de Giro_Generated-klasse.
	 *
	 * @param mixed $a Rubriek (enum)
	 * @param mixed $b Soort (enum)
	 * @param mixed $c Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 * @param mixed $d Bedrag (money)
	 * @param mixed $e Uitleg (string)
	 * @param mixed $f Wanneer (datetime)
	 */
	public function __construct($a = 'BOEKWEB',
			$b = 'VERKOOP',
			$c = NULL,
			$d = NULL,
			$e = '',
			$f = NULL)
	{
		parent::__construct($a, $b, $c, $d, $e, $f); // Transactie

		$this->transactieID = NULL;
		$this->rekening = NULL;
		$this->tegenrekening = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Rubriek';
		$volgorde[] = 'Soort';
		$volgorde[] = 'Contact';
		$volgorde[] = 'Bedrag';
		$volgorde[] = 'Uitleg';
		$volgorde[] = 'Wanneer';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld rekening.
	 *
	 * @return string
	 * De waarde van het veld rekening.
	 */
	public function getRekening()
	{
		return $this->rekening;
	}
	/**
	 * @brief Stel de waarde van het veld rekening in.
	 *
	 * @param mixed $newRekening De nieuwe waarde.
	 *
	 * @return Giro
	 * Dit Giro-object.
	 */
	public function setRekening($newRekening)
	{
		unset($this->errors['Rekening']);
		if(!is_null($newRekening))
			$newRekening = trim($newRekening);
		if($newRekening === "")
			$newRekening = NULL;
		if($this->rekening === $newRekening)
			return $this;

		$this->rekening = $newRekening;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rekening geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rekening geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkRekening()
	{
		if (array_key_exists('Rekening', $this->errors))
			return $this->errors['Rekening'];
		$waarde = $this->getRekening();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tegenrekening.
	 *
	 * @return string
	 * De waarde van het veld tegenrekening.
	 */
	public function getTegenrekening()
	{
		return $this->tegenrekening;
	}
	/**
	 * @brief Stel de waarde van het veld tegenrekening in.
	 *
	 * @param mixed $newTegenrekening De nieuwe waarde.
	 *
	 * @return Giro
	 * Dit Giro-object.
	 */
	public function setTegenrekening($newTegenrekening)
	{
		unset($this->errors['Tegenrekening']);
		if(!is_null($newTegenrekening))
			$newTegenrekening = trim($newTegenrekening);
		if($newTegenrekening === "")
			$newTegenrekening = NULL;
		if($this->tegenrekening === $newTegenrekening)
			return $this;

		$this->tegenrekening = $newTegenrekening;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tegenrekening geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tegenrekening geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTegenrekening()
	{
		if (array_key_exists('Tegenrekening', $this->errors))
			return $this->errors['Tegenrekening'];
		$waarde = $this->getTegenrekening();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Giro::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Giro.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Giro::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTransactieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Giro|false
	 * Een Giro-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$transactieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$transactieID = (int)$a;
		}

		if(is_null($transactieID))
			throw new BadMethodCallException();

		static::cache(array( array($transactieID) ));
		return Entiteit::geefCache(array($transactieID), 'Giro');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Transactie::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Giro::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Giro`.`rekening`'
			                 .     ', `Giro`.`tegenrekening`'
			                 .     ', `Transactie`.`transactieID`'
			                 .     ', `Transactie`.`rubriek`'
			                 .     ', `Transactie`.`soort`'
			                 .     ', `Transactie`.`contact_contactID`'
			                 .     ', `Transactie`.`bedrag`'
			                 .     ', `Transactie`.`uitleg`'
			                 .     ', `Transactie`.`wanneer`'
			                 .     ', `Transactie`.`status`'
			                 .     ', `Transactie`.`gewijzigdWanneer`'
			                 .     ', `Transactie`.`gewijzigdWie`'
			                 .' FROM `Giro`'
			                 .' LEFT JOIN `Transactie` USING (`transactieID`)'
			                 .' WHERE (`transactieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['transactieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Giro'][$id];
			} else {
				$obj = new Giro($row['rubriek'], $row['soort'], array($row['contact_contactID']), $row['bedrag'], $row['uitleg'], $row['wanneer']);
			}

			$obj->inDB = True;

			$obj->rekening  = (is_null($row['rekening'])) ? null : trim($row['rekening']);
			$obj->tegenrekening  = (is_null($row['tegenrekening'])) ? null : trim($row['tegenrekening']);
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Transactie::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Giro')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Giro`'
			          . ' (`transactieID`, `rekening`, `tegenrekening`)'
			          . ' VALUES (%i, %s, %s)'
			          , $this->transactieID
			          , $this->rekening
			          , $this->tegenrekening
			          );

			if($classname == 'Giro')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Giro`'
			          .' SET `rekening` = %s'
			          .   ', `tegenrekening` = %s'
			          .' WHERE `transactieID` = %i'
			          , $this->rekening
			          , $this->tegenrekening
			          , $this->transactieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenGiro
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenGiro($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenGiro($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Rekening';
			$velden[] = 'Tegenrekening';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Rekening';
			$velden[] = 'Tegenrekening';
			break;
		case 'get':
			$velden[] = 'Rekening';
			$velden[] = 'Tegenrekening';
		case 'primary':
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Rekening';
			$velden[] = 'Tegenrekening';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Giro`'
		          .' WHERE `transactieID` = %i'
		          .' LIMIT 1'
		          , $this->transactieID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Giro terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'rekening':
			return 'string';
		case 'tegenrekening':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'rekening':
		case 'tegenrekening':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Giro`'
		          ." SET `%l` = %s"
		          .' WHERE `transactieID` = %i'
		          , $veld
		          , $waarde
		          , $this->transactieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'rekening':
		case 'tegenrekening':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Giro`'
		          .' WHERE `transactieID` = %i'
		                 , $veld
		          , $this->transactieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
