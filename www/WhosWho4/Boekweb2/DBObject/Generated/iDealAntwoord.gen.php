<?
abstract class iDealAntwoord_Generated
	extends Entiteit
{
	protected $ideal;					/**< \brief PRIMARY */
	protected $ideal_transactieID;		/**< \brief PRIMARY */
	protected $vraag;					/**< \brief PRIMARY */
	protected $vraag_activiteitID;		/**< \brief PRIMARY */
	protected $vraag_vraagID;			/**< \brief PRIMARY */
	protected $antwoord;
	/**
	/**
	 * @brief De constructor van de iDealAntwoord_Generated-klasse.
	 *
	 * @param mixed $a Ideal (iDeal OR Array(iDeal_transactieID) OR iDeal_transactieID)
	 * @param mixed $b Vraag (ActiviteitVraag OR Array(activiteitVraag_activiteitID,
	 * activiteitVraag_vraagID))
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof iDeal)
		{
			$this->ideal = $a;
			$this->ideal_transactieID = $a->getTransactieID();
		}
		else if(is_array($a))
		{
			$this->ideal = NULL;
			$this->ideal_transactieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->ideal = NULL;
			$this->ideal_transactieID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]) && is_null($b[1]) && is_null($b[2]))
			$b = NULL;

		if($b instanceof ActiviteitVraag)
		{
			$this->vraag = $b;
			$this->vraag_activiteitID = $b->getActiviteitActiviteitID();
			$this->vraag_vraagID = $b->getVraagID();
		}
		else if(is_array($b))
		{
			$this->vraag = NULL;
			$this->vraag_activiteitID = (int)$b[0];
			$this->vraag_vraagID = (int)$b[1];
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->antwoord = '';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Ideal';
		$volgorde[] = 'Vraag';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld ideal.
	 *
	 * @return iDeal
	 * De waarde van het veld ideal.
	 */
	public function getIdeal()
	{
		if(!isset($this->ideal)
		 && isset($this->ideal_transactieID)
		 ) {
			$this->ideal = iDeal::geef
					( $this->ideal_transactieID
					);
		}
		return $this->ideal;
	}
	/**
	 * @brief Geef de waarde van het veld ideal_transactieID.
	 *
	 * @return int
	 * De waarde van het veld ideal_transactieID.
	 */
	public function getIdealTransactieID()
	{
		if (is_null($this->ideal_transactieID) && isset($this->ideal)) {
			$this->ideal_transactieID = $this->ideal->getTransactieID();
		}
		return $this->ideal_transactieID;
	}
	/**
	 * @brief Geef de waarde van het veld vraag.
	 *
	 * @return ActiviteitVraag
	 * De waarde van het veld vraag.
	 */
	public function getVraag()
	{
		if(!isset($this->vraag)
		 && isset($this->vraag_activiteitID)
		 && isset($this->vraag_vraagID)
		 ) {
			$this->vraag = ActiviteitVraag::geef
					( $this->vraag_activiteitID
					, $this->vraag_vraagID
					);
		}
		return $this->vraag;
	}
	/**
	 * @brief Geef de waarde van het veld vraag_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld vraag_activiteitID.
	 */
	public function getVraagActiviteitID()
	{
		if (is_null($this->vraag_activiteitID) && isset($this->vraag)) {
			$this->vraag_activiteitID = $this->vraag->getActiviteitActiviteitID();
		}
		return $this->vraag_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld vraag_vraagID.
	 *
	 * @return int
	 * De waarde van het veld vraag_vraagID.
	 */
	public function getVraagVraagID()
	{
		if (is_null($this->vraag_vraagID) && isset($this->vraag)) {
			$this->vraag_vraagID = $this->vraag->getVraagID();
		}
		return $this->vraag_vraagID;
	}
	/**
	 * @brief Geef de waarde van het veld antwoord.
	 *
	 * @return string
	 * De waarde van het veld antwoord.
	 */
	public function getAntwoord()
	{
		return $this->antwoord;
	}
	/**
	 * @brief Stel de waarde van het veld antwoord in.
	 *
	 * @param mixed $newAntwoord De nieuwe waarde.
	 *
	 * @return iDealAntwoord
	 * Dit iDealAntwoord-object.
	 */
	public function setAntwoord($newAntwoord)
	{
		unset($this->errors['Antwoord']);
		if(!is_null($newAntwoord))
			$newAntwoord = trim($newAntwoord);
		if($newAntwoord === "")
			$newAntwoord = NULL;
		if($this->antwoord === $newAntwoord)
			return $this;

		$this->antwoord = $newAntwoord;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld antwoord geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld antwoord geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkAntwoord()
	{
		if (array_key_exists('Antwoord', $this->errors))
			return $this->errors['Antwoord'];
		$waarde = $this->getAntwoord();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return iDealAntwoord::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van iDealAntwoord.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return iDealAntwoord::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getIdealTransactieID()
		                      , $this->getVraagActiviteitID()
		                      , $this->getVraagVraagID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 * @param mixed $c Object of ID waarop gezocht moet worden.
	 *
	 * @return iDealAntwoord|false
	 * Een iDealAntwoord-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL, $c = NULL)
	{
		if(is_null($a)
		&& is_null($b)
		&& is_null($c))
			return false;

		if(is_string($a)
		&& is_null($b)
		&& is_null($c))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& is_null($c)
		&& count($a) == 3)
		{
			$ideal_transactieID = (int)$a[0];
			$vraag_activiteitID = (int)$a[1];
			$vraag_vraagID = (int)$a[2];
		}
		else if($a instanceof iDeal
		     && $b instanceof ActiviteitVraag
		     && !isset($c))
		{
			$ideal_transactieID = $a->getTransactieID();
			$vraag_activiteitID = $b->getActiviteitActiviteitID();
			$vraag_vraagID = $b->getVraagID();
		}
		else if(isset($a)
		     && isset($b)
		     && isset($c))
		{
			$ideal_transactieID = (int)$a;
			$vraag_activiteitID = (int)$b;
			$vraag_vraagID = (int)$c;
		}

		if(is_null($ideal_transactieID)
		|| is_null($vraag_activiteitID)
		|| is_null($vraag_vraagID))
			throw new BadMethodCallException();

		static::cache(array( array($ideal_transactieID, $vraag_activiteitID, $vraag_vraagID) ));
		return Entiteit::geefCache(array($ideal_transactieID, $vraag_activiteitID, $vraag_vraagID), 'iDealAntwoord');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'iDealAntwoord');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('iDealAntwoord::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `iDealAntwoord`.`ideal_transactieID`'
		                 .     ', `iDealAntwoord`.`vraag_activiteit_activiteitID`'
		                 .     ', `iDealAntwoord`.`vraag_vraagID`'
		                 .     ', `iDealAntwoord`.`antwoord`'
		                 .     ', `iDealAntwoord`.`gewijzigdWanneer`'
		                 .     ', `iDealAntwoord`.`gewijzigdWie`'
		                 .' FROM `iDealAntwoord`'
		                 .' WHERE (`ideal_transactieID`, `vraag_activiteit_activiteitID`, `vraag_vraagID`)'
		                 .      ' IN (%A{iii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['ideal_transactieID']
			                  ,$row['vraag_activiteit_activiteitID']
			                  ,$row['vraag_vraagID']);

			$obj = new iDealAntwoord(array($row['ideal_transactieID']), array($row['vraag_activiteit_activiteitID'], $row['vraag_vraagID']));

			$obj->inDB = True;

			$obj->antwoord  = trim($row['antwoord']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'iDealAntwoord')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getIdeal();
		if(!$rel->getInDB())
			throw new LogicException('foreign Ideal is not in DB');
		$this->ideal_transactieID = $rel->getTransactieID();

		$rel = $this->getVraag();
		if(!$rel->getInDB())
			throw new LogicException('foreign Vraag is not in DB');
		$this->vraag_activiteitID = $rel->getActiviteitActiviteitID();
		$this->vraag_vraagID = $rel->getVraagID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `iDealAntwoord`'
			          . ' (`ideal_transactieID`, `vraag_activiteit_activiteitID`, `vraag_vraagID`, `antwoord`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %s, %s, %i)'
			          , $this->ideal_transactieID
			          , $this->vraag_activiteitID
			          , $this->vraag_vraagID
			          , $this->antwoord
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'iDealAntwoord')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `iDealAntwoord`'
			          .' SET `antwoord` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `ideal_transactieID` = %i'
			          .  ' AND `vraag_activiteit_activiteitID` = %i'
			          .  ' AND `vraag_vraagID` = %i'
			          , $this->antwoord
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->ideal_transactieID
			          , $this->vraag_activiteitID
			          , $this->vraag_vraagID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldeniDealAntwoord
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldeniDealAntwoord($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldeniDealAntwoord($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Antwoord';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Antwoord';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Antwoord';
			break;
		case 'get':
			$velden[] = 'Antwoord';
		case 'primary':
			$velden[] = 'ideal_transactieID';
			$velden[] = 'vraag_activiteitID';
			$velden[] = 'vraag_vraagID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Antwoord';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `iDealAntwoord`'
		          .' WHERE `ideal_transactieID` = %i'
		          .  ' AND `vraag_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_vraagID` = %i'
		          .' LIMIT 1'
		          , $this->ideal_transactieID
		          , $this->vraag_activiteitID
		          , $this->vraag_vraagID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->ideal = NULL;
		$this->ideal_transactieID = NULL;
		$this->vraag = NULL;
		$this->vraag_activiteitID = NULL;
		$this->vraag_vraagID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `iDealAntwoord`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `ideal_transactieID` = %i'
			          .  ' AND `vraag_activiteit_activiteitID` = %i'
			          .  ' AND `vraag_vraagID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->ideal_transactieID
			          , $this->vraag_activiteitID
			          , $this->vraag_vraagID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van iDealAntwoord terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'ideal':
			return 'foreign';
		case 'ideal_transactieid':
			return 'int';
		case 'vraag':
			return 'foreign';
		case 'vraag_activiteitid':
			return 'int';
		case 'vraag_vraagid':
			return 'int';
		case 'antwoord':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'ideal_transactieID':
		case 'vraag_activiteitID':
		case 'vraag_vraagID':
			$type = '%i';
			break;
		case 'antwoord':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `iDealAntwoord`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `ideal_transactieID` = %i'
		          .  ' AND `vraag_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_vraagID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->ideal_transactieID
		          , $this->vraag_activiteitID
		          , $this->vraag_vraagID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `iDealAntwoord`'
		          .' WHERE `ideal_transactieID` = %i'
		          .  ' AND `vraag_activiteit_activiteitID` = %i'
		          .  ' AND `vraag_vraagID` = %i'
		                 , $veld
		          , $this->ideal_transactieID
		          , $this->vraag_activiteitID
		          , $this->vraag_vraagID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'iDealAntwoord');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
