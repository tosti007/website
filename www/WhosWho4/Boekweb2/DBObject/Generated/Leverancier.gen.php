<?
abstract class Leverancier_Generated
	extends Contact
{
	protected $naam;
	protected $dictaten;
	protected $dictatenIntern;
	protected $dibsproducten;
	protected $colaproducten;
	/** Verzamelingen **/
	protected $leveringVerzameling;
	protected $orderVerzameling;
	protected $offerteVerzameling;
	protected $leverancierRetourVerzameling;
	/**
	 * @brief De constructor van de Leverancier_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Contact

		$this->contactID = NULL;
		$this->naam = '';
		$this->dictaten = False;
		$this->dictatenIntern = False;
		$this->dibsproducten = False;
		$this->colaproducten = False;
		$this->leveringVerzameling = NULL;
		$this->orderVerzameling = NULL;
		$this->offerteVerzameling = NULL;
		$this->leverancierRetourVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Leverancier
	 * Dit Leverancier-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld dictaten.
	 *
	 * @return bool
	 * De waarde van het veld dictaten.
	 */
	public function getDictaten()
	{
		return $this->dictaten;
	}
	/**
	 * @brief Stel de waarde van het veld dictaten in.
	 *
	 * @param mixed $newDictaten De nieuwe waarde.
	 *
	 * @return Leverancier
	 * Dit Leverancier-object.
	 */
	public function setDictaten($newDictaten)
	{
		unset($this->errors['Dictaten']);
		if(!is_null($newDictaten))
			$newDictaten = (bool)$newDictaten;
		if($this->dictaten === $newDictaten)
			return $this;

		$this->dictaten = $newDictaten;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld dictaten geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld dictaten geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDictaten()
	{
		if (array_key_exists('Dictaten', $this->errors))
			return $this->errors['Dictaten'];
		$waarde = $this->getDictaten();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld dictatenIntern.
	 *
	 * @return bool
	 * De waarde van het veld dictatenIntern.
	 */
	public function getDictatenIntern()
	{
		return $this->dictatenIntern;
	}
	/**
	 * @brief Stel de waarde van het veld dictatenIntern in.
	 *
	 * @param mixed $newDictatenIntern De nieuwe waarde.
	 *
	 * @return Leverancier
	 * Dit Leverancier-object.
	 */
	public function setDictatenIntern($newDictatenIntern)
	{
		unset($this->errors['DictatenIntern']);
		if(!is_null($newDictatenIntern))
			$newDictatenIntern = (bool)$newDictatenIntern;
		if($this->dictatenIntern === $newDictatenIntern)
			return $this;

		$this->dictatenIntern = $newDictatenIntern;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld dictatenIntern geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld dictatenIntern geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDictatenIntern()
	{
		if (array_key_exists('DictatenIntern', $this->errors))
			return $this->errors['DictatenIntern'];
		$waarde = $this->getDictatenIntern();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld dibsproducten.
	 *
	 * @return bool
	 * De waarde van het veld dibsproducten.
	 */
	public function getDibsproducten()
	{
		return $this->dibsproducten;
	}
	/**
	 * @brief Stel de waarde van het veld dibsproducten in.
	 *
	 * @param mixed $newDibsproducten De nieuwe waarde.
	 *
	 * @return Leverancier
	 * Dit Leverancier-object.
	 */
	public function setDibsproducten($newDibsproducten)
	{
		unset($this->errors['Dibsproducten']);
		if(!is_null($newDibsproducten))
			$newDibsproducten = (bool)$newDibsproducten;
		if($this->dibsproducten === $newDibsproducten)
			return $this;

		$this->dibsproducten = $newDibsproducten;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld dibsproducten geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld dibsproducten geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDibsproducten()
	{
		if (array_key_exists('Dibsproducten', $this->errors))
			return $this->errors['Dibsproducten'];
		$waarde = $this->getDibsproducten();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld colaproducten.
	 *
	 * @return bool
	 * De waarde van het veld colaproducten.
	 */
	public function getColaproducten()
	{
		return $this->colaproducten;
	}
	/**
	 * @brief Stel de waarde van het veld colaproducten in.
	 *
	 * @param mixed $newColaproducten De nieuwe waarde.
	 *
	 * @return Leverancier
	 * Dit Leverancier-object.
	 */
	public function setColaproducten($newColaproducten)
	{
		unset($this->errors['Colaproducten']);
		if(!is_null($newColaproducten))
			$newColaproducten = (bool)$newColaproducten;
		if($this->colaproducten === $newColaproducten)
			return $this;

		$this->colaproducten = $newColaproducten;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld colaproducten geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld colaproducten geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkColaproducten()
	{
		if (array_key_exists('Colaproducten', $this->errors))
			return $this->errors['Colaproducten'];
		$waarde = $this->getColaproducten();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de LeveringVerzameling die hoort bij dit object.
	 */
	public function getLeveringVerzameling()
	{
		if(!$this->leveringVerzameling instanceof LeveringVerzameling)
			$this->leveringVerzameling = LeveringVerzameling::fromLeverancier($this);
		return $this->leveringVerzameling;
	}
	/**
	 * @brief Returneert de OrderVerzameling die hoort bij dit object.
	 */
	public function getOrderVerzameling()
	{
		if(!$this->orderVerzameling instanceof OrderVerzameling)
			$this->orderVerzameling = OrderVerzameling::fromLeverancier($this);
		return $this->orderVerzameling;
	}
	/**
	 * @brief Returneert de OfferteVerzameling die hoort bij dit object.
	 */
	public function getOfferteVerzameling()
	{
		if(!$this->offerteVerzameling instanceof OfferteVerzameling)
			$this->offerteVerzameling = OfferteVerzameling::fromLeverancier($this);
		return $this->offerteVerzameling;
	}
	/**
	 * @brief Returneert de LeverancierRetourVerzameling die hoort bij dit object.
	 */
	public function getLeverancierRetourVerzameling()
	{
		if(!$this->leverancierRetourVerzameling instanceof LeverancierRetourVerzameling)
			$this->leverancierRetourVerzameling = LeverancierRetourVerzameling::fromLeverancier($this);
		return $this->leverancierRetourVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Leverancier::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Leverancier.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Leverancier::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getContactID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Leverancier|false
	 * Een Leverancier-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$contactID = (int)$a;
		}

		if(is_null($contactID))
			throw new BadMethodCallException();

		static::cache(array( array($contactID) ));
		return Entiteit::geefCache(array($contactID), 'Leverancier');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Contact::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Leverancier::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Leverancier`.`naam`'
			                 .     ', `Leverancier`.`dictaten`'
			                 .     ', `Leverancier`.`dictatenIntern`'
			                 .     ', `Leverancier`.`dibsproducten`'
			                 .     ', `Leverancier`.`colaproducten`'
			                 .     ', `Contact`.`contactID`'
			                 .     ', `Contact`.`email`'
			                 .     ', `Contact`.`homepage`'
			                 .     ', `Contact`.`vakidOpsturen`'
			                 .     ', `Contact`.`aes2rootsOpsturen`'
			                 .     ', `Contact`.`gewijzigdWanneer`'
			                 .     ', `Contact`.`gewijzigdWie`'
			                 .' FROM `Leverancier`'
			                 .' LEFT JOIN `Contact` USING (`contactID`)'
			                 .' WHERE (`contactID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['contactID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Leverancier'][$id];
			} else {
				$obj = new Leverancier();
			}

			$obj->inDB = True;

			$obj->naam  = trim($row['naam']);
			$obj->dictaten  = (bool) $row['dictaten'];
			$obj->dictatenIntern  = (bool) $row['dictatenIntern'];
			$obj->dibsproducten  = (bool) $row['dibsproducten'];
			$obj->colaproducten  = (bool) $row['colaproducten'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Contact::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Leverancier')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Leverancier`'
			          . ' (`contactID`, `naam`, `dictaten`, `dictatenIntern`, `dibsproducten`, `colaproducten`)'
			          . ' VALUES (%i, %s, %i, %i, %i, %i)'
			          , $this->contactID
			          , $this->naam
			          , $this->dictaten
			          , $this->dictatenIntern
			          , $this->dibsproducten
			          , $this->colaproducten
			          );

			if($classname == 'Leverancier')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Leverancier`'
			          .' SET `naam` = %s'
			          .   ', `dictaten` = %i'
			          .   ', `dictatenIntern` = %i'
			          .   ', `dibsproducten` = %i'
			          .   ', `colaproducten` = %i'
			          .' WHERE `contactID` = %i'
			          , $this->naam
			          , $this->dictaten
			          , $this->dictatenIntern
			          , $this->dibsproducten
			          , $this->colaproducten
			          , $this->contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenLeverancier
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenLeverancier($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenLeverancier($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			$velden[] = 'Dictaten';
			$velden[] = 'DictatenIntern';
			$velden[] = 'Dibsproducten';
			$velden[] = 'Colaproducten';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			$velden[] = 'Dictaten';
			$velden[] = 'DictatenIntern';
			$velden[] = 'Dibsproducten';
			$velden[] = 'Colaproducten';
			break;
		case 'get':
			$velden[] = 'Naam';
			$velden[] = 'Dictaten';
			$velden[] = 'DictatenIntern';
			$velden[] = 'Dibsproducten';
			$velden[] = 'Colaproducten';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'LeveringVerzameling';
			$velden[] = 'OrderVerzameling';
			$velden[] = 'OfferteVerzameling';
			$velden[] = 'LeverancierRetourVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			$velden[] = 'Dictaten';
			$velden[] = 'DictatenIntern';
			$velden[] = 'Dibsproducten';
			$velden[] = 'Colaproducten';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Levering-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$LeveringFromLeverancierVerz = LeveringVerzameling::fromLeverancier($this);
		$returnValue = $LeveringFromLeverancierVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Levering met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Order-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$OrderFromLeverancierVerz = OrderVerzameling::fromLeverancier($this);
		$returnValue = $OrderFromLeverancierVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Order met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Offerte-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$OfferteFromLeverancierVerz = OfferteVerzameling::fromLeverancier($this);
		$returnValue = $OfferteFromLeverancierVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Offerte met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle LeverancierRetour-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$LeverancierRetourFromLeverancierVerz = LeverancierRetourVerzameling::fromLeverancier($this);
		$returnValue = $LeverancierRetourFromLeverancierVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LeverancierRetour met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $LeveringFromLeverancierVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $OrderFromLeverancierVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $OfferteFromLeverancierVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $LeverancierRetourFromLeverancierVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Leverancier`'
		          .' WHERE `contactID` = %i'
		          .' LIMIT 1'
		          , $this->contactID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Leverancier terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'naam':
			return 'string';
		case 'dictaten':
			return 'bool';
		case 'dictatenintern':
			return 'bool';
		case 'dibsproducten':
			return 'bool';
		case 'colaproducten':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'dictaten':
		case 'dictatenIntern':
		case 'dibsproducten':
		case 'colaproducten':
			$type = '%i';
			break;
		case 'naam':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Leverancier`'
		          ." SET `%l` = $type"
		          .' WHERE `contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'naam':
		case 'dictaten':
		case 'dictatenIntern':
		case 'dibsproducten':
		case 'colaproducten':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Leverancier`'
		          .' WHERE `contactID` = %i'
		                 , $veld
		          , $this->contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Levering-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LeveringFromLeverancierVerz = LeveringVerzameling::fromLeverancier($this);
		$dependencies['Levering'] = $LeveringFromLeverancierVerz;

		// Verzamel alle Order-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$OrderFromLeverancierVerz = OrderVerzameling::fromLeverancier($this);
		$dependencies['Order'] = $OrderFromLeverancierVerz;

		// Verzamel alle Offerte-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$OfferteFromLeverancierVerz = OfferteVerzameling::fromLeverancier($this);
		$dependencies['Offerte'] = $OfferteFromLeverancierVerz;

		// Verzamel alle LeverancierRetour-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LeverancierRetourFromLeverancierVerz = LeverancierRetourVerzameling::fromLeverancier($this);
		$dependencies['LeverancierRetour'] = $LeverancierRetourFromLeverancierVerz;

		return $dependencies;
	}
}
