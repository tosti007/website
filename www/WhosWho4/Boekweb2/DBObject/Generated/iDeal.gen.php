<?
abstract class iDeal_Generated
	extends Giro
{
	protected $titel;
	protected $voorraad;
	protected $voorraad_voorraadID;		/**< \brief PRIMARY */
	protected $externeID;				/**< \brief NULL */
	protected $klantNaam;				/**< \brief NULL */
	protected $klantStad;				/**< \brief NULL */
	protected $expire;
	protected $entranceCode;
	protected $magOpvragenTijdstip;
	protected $emailadres;				/**< \brief NULL */
	protected $returnURL;				/**< \brief NULL */
	/** Verzamelingen **/
	protected $iDealAntwoordVerzameling;
	protected $lidmaatschapsBetalingVerzameling;
	/**
	/**
	 * @brief De constructor van de iDeal_Generated-klasse.
	 *
	 * @param mixed $a Rubriek (enum)
	 * @param mixed $b Soort (enum)
	 * @param mixed $c Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 * @param mixed $d Bedrag (money)
	 * @param mixed $e Uitleg (string)
	 * @param mixed $f Wanneer (datetime)
	 * @param mixed $g Titel (string)
	 * @param mixed $h Voorraad (Voorraad OR Array(voorraad_voorraadID) OR
	 * voorraad_voorraadID)
	 */
	public function __construct($a = 'BOEKWEB',
			$b = 'VERKOOP',
			$c = NULL,
			$d = NULL,
			$e = '',
			$f = NULL,
			$g = '',
			$h = NULL)
	{
		parent::__construct($a, $b, $c, $d, $e, $f); // Giro

		$g = trim($g);
		if(is_null($g))
			$g = "";
		$this->titel = $g;

		if(is_array($h) && is_null($h[0]))
			$h = NULL;

		if($h instanceof Voorraad)
		{
			$this->voorraad = $h;
			$this->voorraad_voorraadID = $h->getVoorraadID();
		}
		else if(is_array($h))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$h[0];
		}
		else if(isset($h))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$h;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->transactieID = NULL;
		$this->externeID = NULL;
		$this->klantNaam = NULL;
		$this->klantStad = NULL;
		$this->expire = new DateTimeLocale();
		$this->entranceCode = '';
		$this->magOpvragenTijdstip = new DateTimeLocale();
		$this->emailadres = NULL;
		$this->returnURL = NULL;
		$this->iDealAntwoordVerzameling = NULL;
		$this->lidmaatschapsBetalingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Rubriek';
		$volgorde[] = 'Soort';
		$volgorde[] = 'Contact';
		$volgorde[] = 'Bedrag';
		$volgorde[] = 'Uitleg';
		$volgorde[] = 'Wanneer';
		$volgorde[] = 'Titel';
		$volgorde[] = 'Voorraad';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @return string
	 * De waarde van het veld titel.
	 */
	public function getTitel()
	{
		return $this->titel;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @return Voorraad
	 * De waarde van het veld voorraad.
	 */
	public function getVoorraad()
	{
		if(!isset($this->voorraad)
		 && isset($this->voorraad_voorraadID)
		 ) {
			$this->voorraad = Voorraad::geef
					( $this->voorraad_voorraadID
					);
		}
		return $this->voorraad;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad_voorraadID.
	 *
	 * @return int
	 * De waarde van het veld voorraad_voorraadID.
	 */
	public function getVoorraadVoorraadID()
	{
		if (is_null($this->voorraad_voorraadID) && isset($this->voorraad)) {
			$this->voorraad_voorraadID = $this->voorraad->getVoorraadID();
		}
		return $this->voorraad_voorraadID;
	}
	/**
	 * @brief Geef de waarde van het veld externeID.
	 *
	 * @return string
	 * De waarde van het veld externeID.
	 */
	public function getExterneID()
	{
		return $this->externeID;
	}
	/**
	 * @brief Stel de waarde van het veld externeID in.
	 *
	 * @param mixed $newExterneID De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setExterneID($newExterneID)
	{
		unset($this->errors['ExterneID']);
		if(!is_null($newExterneID))
			$newExterneID = trim($newExterneID);
		if($newExterneID === "")
			$newExterneID = NULL;
		if($this->externeID === $newExterneID)
			return $this;

		$this->externeID = $newExterneID;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld externeID geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld externeID geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkExterneID()
	{
		if (array_key_exists('ExterneID', $this->errors))
			return $this->errors['ExterneID'];
		$waarde = $this->getExterneID();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld klantNaam.
	 *
	 * @return string
	 * De waarde van het veld klantNaam.
	 */
	public function getKlantNaam()
	{
		return $this->klantNaam;
	}
	/**
	 * @brief Stel de waarde van het veld klantNaam in.
	 *
	 * @param mixed $newKlantNaam De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setKlantNaam($newKlantNaam)
	{
		unset($this->errors['KlantNaam']);
		if(!is_null($newKlantNaam))
			$newKlantNaam = trim($newKlantNaam);
		if($newKlantNaam === "")
			$newKlantNaam = NULL;
		if($this->klantNaam === $newKlantNaam)
			return $this;

		$this->klantNaam = $newKlantNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld klantNaam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld klantNaam geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkKlantNaam()
	{
		if (array_key_exists('KlantNaam', $this->errors))
			return $this->errors['KlantNaam'];
		$waarde = $this->getKlantNaam();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld klantStad.
	 *
	 * @return string
	 * De waarde van het veld klantStad.
	 */
	public function getKlantStad()
	{
		return $this->klantStad;
	}
	/**
	 * @brief Stel de waarde van het veld klantStad in.
	 *
	 * @param mixed $newKlantStad De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setKlantStad($newKlantStad)
	{
		unset($this->errors['KlantStad']);
		if(!is_null($newKlantStad))
			$newKlantStad = trim($newKlantStad);
		if($newKlantStad === "")
			$newKlantStad = NULL;
		if($this->klantStad === $newKlantStad)
			return $this;

		$this->klantStad = $newKlantStad;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld klantStad geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld klantStad geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkKlantStad()
	{
		if (array_key_exists('KlantStad', $this->errors))
			return $this->errors['KlantStad'];
		$waarde = $this->getKlantStad();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld expire.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld expire.
	 */
	public function getExpire()
	{
		return $this->expire;
	}
	/**
	 * @brief Stel de waarde van het veld expire in.
	 *
	 * @param mixed $newExpire De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setExpire($newExpire)
	{
		unset($this->errors['Expire']);
		if(!$newExpire instanceof DateTimeLocale) {
			try {
				$newExpire = new DateTimeLocale($newExpire);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->expire->strftime('%F %T') == $newExpire->strftime('%F %T'))
			return $this;

		$this->expire = $newExpire;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld expire geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld expire geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkExpire()
	{
		if (array_key_exists('Expire', $this->errors))
			return $this->errors['Expire'];
		$waarde = $this->getExpire();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld entranceCode.
	 *
	 * @return string
	 * De waarde van het veld entranceCode.
	 */
	public function getEntranceCode()
	{
		return $this->entranceCode;
	}
	/**
	 * @brief Stel de waarde van het veld entranceCode in.
	 *
	 * @param mixed $newEntranceCode De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setEntranceCode($newEntranceCode)
	{
		unset($this->errors['EntranceCode']);
		if(!is_null($newEntranceCode))
			$newEntranceCode = trim($newEntranceCode);
		if($newEntranceCode === "")
			$newEntranceCode = NULL;
		if($this->entranceCode === $newEntranceCode)
			return $this;

		$this->entranceCode = $newEntranceCode;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld entranceCode geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld entranceCode geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkEntranceCode()
	{
		if (array_key_exists('EntranceCode', $this->errors))
			return $this->errors['EntranceCode'];
		$waarde = $this->getEntranceCode();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld magOpvragenTijdstip.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld magOpvragenTijdstip.
	 */
	public function getMagOpvragenTijdstip()
	{
		return $this->magOpvragenTijdstip;
	}
	/**
	 * @brief Stel de waarde van het veld magOpvragenTijdstip in.
	 *
	 * @param mixed $newMagOpvragenTijdstip De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setMagOpvragenTijdstip($newMagOpvragenTijdstip)
	{
		unset($this->errors['MagOpvragenTijdstip']);
		if(!$newMagOpvragenTijdstip instanceof DateTimeLocale) {
			try {
				$newMagOpvragenTijdstip = new DateTimeLocale($newMagOpvragenTijdstip);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->magOpvragenTijdstip->strftime('%F %T') == $newMagOpvragenTijdstip->strftime('%F %T'))
			return $this;

		$this->magOpvragenTijdstip = $newMagOpvragenTijdstip;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld magOpvragenTijdstip geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld magOpvragenTijdstip geldig is; anders
	 * een string met een foutmelding.
	 */
	public function checkMagOpvragenTijdstip()
	{
		if (array_key_exists('MagOpvragenTijdstip', $this->errors))
			return $this->errors['MagOpvragenTijdstip'];
		$waarde = $this->getMagOpvragenTijdstip();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld emailadres.
	 *
	 * @return string
	 * De waarde van het veld emailadres.
	 */
	public function getEmailadres()
	{
		return $this->emailadres;
	}
	/**
	 * @brief Stel de waarde van het veld emailadres in.
	 *
	 * @param mixed $newEmailadres De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setEmailadres($newEmailadres)
	{
		unset($this->errors['Emailadres']);
		if(!is_null($newEmailadres))
			$newEmailadres = trim($newEmailadres);
		if($newEmailadres === "")
			$newEmailadres = NULL;
		if($this->emailadres === $newEmailadres)
			return $this;

		$this->emailadres = $newEmailadres;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld emailadres geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld emailadres geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkEmailadres()
	{
		if (array_key_exists('Emailadres', $this->errors))
			return $this->errors['Emailadres'];
		$waarde = $this->getEmailadres();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld returnURL.
	 *
	 * @return string
	 * De waarde van het veld returnURL.
	 */
	public function getReturnURL()
	{
		return $this->returnURL;
	}
	/**
	 * @brief Stel de waarde van het veld returnURL in.
	 *
	 * @param mixed $newReturnURL De nieuwe waarde.
	 *
	 * @return iDeal
	 * Dit iDeal-object.
	 */
	public function setReturnURL($newReturnURL)
	{
		unset($this->errors['ReturnURL']);
		if(!is_null($newReturnURL))
			$newReturnURL = trim($newReturnURL);
		if($newReturnURL === "")
			$newReturnURL = NULL;
		if($this->returnURL === $newReturnURL)
			return $this;

		$this->returnURL = $newReturnURL;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld returnURL geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld returnURL geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkReturnURL()
	{
		if (array_key_exists('ReturnURL', $this->errors))
			return $this->errors['ReturnURL'];
		$waarde = $this->getReturnURL();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Returneert de IDealAntwoordVerzameling die hoort bij dit object.
	 */
	public function getIDealAntwoordVerzameling()
	{
		if(!$this->iDealAntwoordVerzameling instanceof IDealAntwoordVerzameling)
			$this->iDealAntwoordVerzameling = IDealAntwoordVerzameling::fromIdeal($this);
		return $this->iDealAntwoordVerzameling;
	}
	/**
	 * @brief Returneert de LidmaatschapsBetalingVerzameling die hoort bij dit object.
	 */
	public function getLidmaatschapsBetalingVerzameling()
	{
		if(!$this->lidmaatschapsBetalingVerzameling instanceof LidmaatschapsBetalingVerzameling)
			$this->lidmaatschapsBetalingVerzameling = LidmaatschapsBetalingVerzameling::fromIdeal($this);
		return $this->lidmaatschapsBetalingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return iDeal::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van iDeal.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return iDeal::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTransactieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return iDeal|false
	 * Een iDeal-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$transactieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$transactieID = (int)$a;
		}

		if(is_null($transactieID))
			throw new BadMethodCallException();

		static::cache(array( array($transactieID) ));
		return Entiteit::geefCache(array($transactieID), 'iDeal');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Transactie::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('iDeal::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `iDeal`.`titel`'
			                 .     ', `iDeal`.`voorraad_voorraadID`'
			                 .     ', `iDeal`.`externeID`'
			                 .     ', `iDeal`.`klantNaam`'
			                 .     ', `iDeal`.`klantStad`'
			                 .     ', `iDeal`.`expire`'
			                 .     ', `iDeal`.`entranceCode`'
			                 .     ', `iDeal`.`magOpvragenTijdstip`'
			                 .     ', `iDeal`.`emailadres`'
			                 .     ', `iDeal`.`returnURL`'
			                 .     ', `Giro`.`rekening`'
			                 .     ', `Giro`.`tegenrekening`'
			                 .     ', `Transactie`.`transactieID`'
			                 .     ', `Transactie`.`rubriek`'
			                 .     ', `Transactie`.`soort`'
			                 .     ', `Transactie`.`contact_contactID`'
			                 .     ', `Transactie`.`bedrag`'
			                 .     ', `Transactie`.`uitleg`'
			                 .     ', `Transactie`.`wanneer`'
			                 .     ', `Transactie`.`status`'
			                 .     ', `Transactie`.`gewijzigdWanneer`'
			                 .     ', `Transactie`.`gewijzigdWie`'
			                 .' FROM `iDeal`'
			                 .' LEFT JOIN `Giro` USING (`transactieID`)'
			                 .' LEFT JOIN `Transactie` USING (`transactieID`)'
			                 .' WHERE (`transactieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['transactieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['iDeal'][$id];
			} else {
				$obj = new iDeal($row['rubriek'], $row['soort'], array($row['contact_contactID']), $row['bedrag'], $row['uitleg'], $row['wanneer'], $row['titel'], array($row['voorraad_voorraadID']));
			}

			$obj->inDB = True;

			$obj->externeID  = (is_null($row['externeID'])) ? null : trim($row['externeID']);
			$obj->klantNaam  = (is_null($row['klantNaam'])) ? null : trim($row['klantNaam']);
			$obj->klantStad  = (is_null($row['klantStad'])) ? null : trim($row['klantStad']);
			$obj->expire  = new DateTimeLocale($row['expire']);
			$obj->entranceCode  = trim($row['entranceCode']);
			$obj->magOpvragenTijdstip  = new DateTimeLocale($row['magOpvragenTijdstip']);
			$obj->emailadres  = (is_null($row['emailadres'])) ? null : trim($row['emailadres']);
			$obj->returnURL  = (is_null($row['returnURL'])) ? null : trim($row['returnURL']);
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Giro::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'iDeal')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getVoorraadVoorraadID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `iDeal`'
			          . ' (`transactieID`, `titel`, `voorraad_voorraadID`, `externeID`, `klantNaam`, `klantStad`, `expire`, `entranceCode`, `magOpvragenTijdstip`, `emailadres`, `returnURL`)'
			          . ' VALUES (%i, %s, %i, %s, %s, %s, %s, %s, %s, %s, %s)'
			          , $this->transactieID
			          , $this->titel
			          , $this->voorraad_voorraadID
			          , $this->externeID
			          , $this->klantNaam
			          , $this->klantStad
			          , $this->expire->strftime('%F %T')
			          , $this->entranceCode
			          , $this->magOpvragenTijdstip->strftime('%F %T')
			          , $this->emailadres
			          , $this->returnURL
			          );

			if($classname == 'iDeal')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `iDeal`'
			          .' SET `titel` = %s'
			          .   ', `voorraad_voorraadID` = %i'
			          .   ', `externeID` = %s'
			          .   ', `klantNaam` = %s'
			          .   ', `klantStad` = %s'
			          .   ', `expire` = %s'
			          .   ', `entranceCode` = %s'
			          .   ', `magOpvragenTijdstip` = %s'
			          .   ', `emailadres` = %s'
			          .   ', `returnURL` = %s'
			          .' WHERE `transactieID` = %i'
			          , $this->titel
			          , $this->voorraad_voorraadID
			          , $this->externeID
			          , $this->klantNaam
			          , $this->klantStad
			          , $this->expire->strftime('%F %T')
			          , $this->entranceCode
			          , $this->magOpvragenTijdstip->strftime('%F %T')
			          , $this->emailadres
			          , $this->returnURL
			          , $this->transactieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldeniDeal
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldeniDeal($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldeniDeal($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'ExterneID';
			$velden[] = 'KlantNaam';
			$velden[] = 'KlantStad';
			$velden[] = 'Expire';
			$velden[] = 'EntranceCode';
			$velden[] = 'MagOpvragenTijdstip';
			$velden[] = 'Emailadres';
			$velden[] = 'ReturnURL';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Expire';
			$velden[] = 'EntranceCode';
			$velden[] = 'MagOpvragenTijdstip';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Titel';
			$velden[] = 'Voorraad';
			$velden[] = 'ExterneID';
			$velden[] = 'KlantNaam';
			$velden[] = 'KlantStad';
			$velden[] = 'Expire';
			$velden[] = 'EntranceCode';
			$velden[] = 'MagOpvragenTijdstip';
			$velden[] = 'Emailadres';
			$velden[] = 'ReturnURL';
			break;
		case 'get':
			$velden[] = 'Titel';
			$velden[] = 'Voorraad';
			$velden[] = 'ExterneID';
			$velden[] = 'KlantNaam';
			$velden[] = 'KlantStad';
			$velden[] = 'Expire';
			$velden[] = 'EntranceCode';
			$velden[] = 'MagOpvragenTijdstip';
			$velden[] = 'Emailadres';
			$velden[] = 'ReturnURL';
		case 'primary':
			$velden[] = 'voorraad_voorraadID';
			break;
		case 'verzamelingen':
			$velden[] = 'IDealAntwoordVerzameling';
			$velden[] = 'LidmaatschapsBetalingVerzameling';
			break;
		default:
			$velden[] = 'Titel';
			$velden[] = 'Voorraad';
			$velden[] = 'ExterneID';
			$velden[] = 'KlantNaam';
			$velden[] = 'KlantStad';
			$velden[] = 'Expire';
			$velden[] = 'EntranceCode';
			$velden[] = 'MagOpvragenTijdstip';
			$velden[] = 'Emailadres';
			$velden[] = 'ReturnURL';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle iDealAntwoord-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$iDealAntwoordFromIdealVerz = iDealAntwoordVerzameling::fromIdeal($this);
		$returnValue = $iDealAntwoordFromIdealVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object iDealAntwoord met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle LidmaatschapsBetaling-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$LidmaatschapsBetalingFromIdealVerz = LidmaatschapsBetalingVerzameling::fromIdeal($this);
		$returnValue = $LidmaatschapsBetalingFromIdealVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LidmaatschapsBetaling met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($LidmaatschapsBetalingFromIdealVerz as $v)
		{
			$v->setIdeal(NULL);
		}
		$LidmaatschapsBetalingFromIdealVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $iDealAntwoordFromIdealVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `iDeal`'
		          .' WHERE `transactieID` = %i'
		          .' LIMIT 1'
		          , $this->transactieID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van iDeal terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'titel':
			return 'string';
		case 'voorraad':
			return 'foreign';
		case 'voorraad_voorraadid':
			return 'int';
		case 'externeid':
			return 'string';
		case 'klantnaam':
			return 'string';
		case 'klantstad':
			return 'string';
		case 'expire':
			return 'datetime';
		case 'entrancecode':
			return 'string';
		case 'magopvragentijdstip':
			return 'datetime';
		case 'emailadres':
			return 'string';
		case 'returnurl':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'voorraad_voorraadID':
			$type = '%i';
			break;
		case 'titel':
		case 'externeID':
		case 'klantNaam':
		case 'klantStad':
		case 'expire':
		case 'entranceCode':
		case 'magOpvragenTijdstip':
		case 'emailadres':
		case 'returnURL':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `iDeal`'
		          ." SET `%l` = $type"
		          .' WHERE `transactieID` = %i'
		          , $veld
		          , $waarde
		          , $this->transactieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'titel':
		case 'voorraad':
		case 'voorraad_voorraadID':
		case 'externeID':
		case 'klantNaam':
		case 'klantStad':
		case 'expire':
		case 'entranceCode':
		case 'magOpvragenTijdstip':
		case 'emailadres':
		case 'returnURL':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `iDeal`'
		          .' WHERE `transactieID` = %i'
		                 , $veld
		          , $this->transactieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle iDealAntwoord-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$iDealAntwoordFromIdealVerz = iDealAntwoordVerzameling::fromIdeal($this);
		$dependencies['iDealAntwoord'] = $iDealAntwoordFromIdealVerz;

		// Verzamel alle LidmaatschapsBetaling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LidmaatschapsBetalingFromIdealVerz = LidmaatschapsBetalingVerzameling::fromIdeal($this);
		$dependencies['LidmaatschapsBetaling'] = $LidmaatschapsBetalingFromIdealVerz;

		return $dependencies;
	}
}
