<?
abstract class Transactie_Generated
	extends Entiteit
{
	protected $transactieID;			/**< \brief PRIMARY */
	protected $rubriek;					/**< \brief ENUM:boekweb/colakas/www */
	protected $soort;					/**< \brief ENUM:verkoop/donatie/betaling/afschrijving */
	protected $contact;					/**< \brief NULL */
	protected $contact_contactID;		/**< \brief PRIMARY */
	protected $bedrag;					/**< \brief NULL */
	protected $uitleg;
	protected $wanneer;
	protected $status;					/**< \brief ENUM:INACTIVE/OPEN/SUCCESS/FAILURE/CANCELLED/EXPIRED/OPENPOGING2/OPENPOGING3/OPENPOGING4/OPENPOGING5/BANKFAILURE */
	/** Verzamelingen **/
	protected $verkoopVerzameling;
	protected $schuldVerzameling;
	protected $iDealKaartjeCodeVerzameling;
	/**
	/**
	 * @brief De constructor van de Transactie_Generated-klasse.
	 *
	 * @param mixed $a Rubriek (enum)
	 * @param mixed $b Soort (enum)
	 * @param mixed $c Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 * @param mixed $d Bedrag (money)
	 * @param mixed $e Uitleg (string)
	 * @param mixed $f Wanneer (datetime)
	 */
	public function __construct($a = 'BOEKWEB',
			$b = 'VERKOOP',
			$c = NULL,
			$d = NULL,
			$e = '',
			$f = NULL)
	{
		parent::__construct(); // Entiteit

		$a = strtoupper(trim($a));
		if(!in_array($a, static::enumsRubriek()))
			throw new BadMethodCallException();
		$this->rubriek = $a;

		$b = strtoupper(trim($b));
		if(!in_array($b, static::enumsSoort()))
			throw new BadMethodCallException();
		$this->soort = $b;

		if(is_array($c) && is_null($c[0]))
			$c = NULL;

		if($c instanceof Contact)
		{
			$this->contact = $c;
			$this->contact_contactID = $c->getContactID();
		}
		else if(is_array($c))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$c[0];
		}
		else if(isset($c))
		{
			$this->contact = NULL;
			$this->contact_contactID = (int)$c;
		}
		else
		{
			$this->contact = NULL;
			$this->contact_contactID = NULL;
		}

		$d = (float)$d;
		$this->bedrag = $d;

		$e = trim($e);
		if(is_null($e))
			$e = "";
		$this->uitleg = $e;

		if(!$f instanceof DateTimeLocale)
		{
			if(is_null($f))
				$f = new DateTimeLocale();
			else
			{
				try {
					$f = new DateTimeLocale($f);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->wanneer = $f;

		$this->transactieID = NULL;
		$this->status = 'SUCCESS';
		$this->verkoopVerzameling = NULL;
		$this->schuldVerzameling = NULL;
		$this->iDealKaartjeCodeVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Rubriek';
		$volgorde[] = 'Soort';
		$volgorde[] = 'Contact';
		$volgorde[] = 'Bedrag';
		$volgorde[] = 'Uitleg';
		$volgorde[] = 'Wanneer';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld transactieID.
	 *
	 * @return int
	 * De waarde van het veld transactieID.
	 */
	public function getTransactieID()
	{
		return $this->transactieID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld rubriek.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld rubriek.
	 */
	static public function enumsRubriek()
	{
		static $vals = array('BOEKWEB','COLAKAS','WWW');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld rubriek.
	 *
	 * @return string
	 * De waarde van het veld rubriek.
	 */
	public function getRubriek()
	{
		return $this->rubriek;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('VERKOOP','DONATIE','BETALING','AFSCHRIJVING');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @return Contact
	 * De waarde van het veld contact.
	 */
	public function getContact()
	{
		if(!isset($this->contact)
		 && isset($this->contact_contactID)
		 ) {
			$this->contact = Contact::geef
					( $this->contact_contactID
					);
		}
		return $this->contact;
	}
	/**
	 * @brief Geef de waarde van het veld contact_contactID.
	 *
	 * @return int
	 * De waarde van het veld contact_contactID.
	 */
	public function getContactContactID()
	{
		if (is_null($this->contact_contactID) && isset($this->contact)) {
			$this->contact_contactID = $this->contact->getContactID();
		}
		return $this->contact_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld bedrag.
	 *
	 * @return float
	 * De waarde van het veld bedrag.
	 */
	public function getBedrag()
	{
		return $this->bedrag;
	}
	/**
	 * @brief Geef de waarde van het veld uitleg.
	 *
	 * @return string
	 * De waarde van het veld uitleg.
	 */
	public function getUitleg()
	{
		return $this->uitleg;
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld wanneer.
	 */
	public function getWanneer()
	{
		return $this->wanneer;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('INACTIVE','OPEN','SUCCESS','FAILURE','CANCELLED','EXPIRED','OPENPOGING2','OPENPOGING3','OPENPOGING4','OPENPOGING5','BANKFAILURE');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return Transactie
	 * Dit Transactie-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Returneert de VerkoopVerzameling die hoort bij dit object.
	 */
	public function getVerkoopVerzameling()
	{
		if(!$this->verkoopVerzameling instanceof VerkoopVerzameling)
			$this->verkoopVerzameling = VerkoopVerzameling::fromTransactie($this);
		return $this->verkoopVerzameling;
	}
	/**
	 * @brief Returneert de SchuldVerzameling die hoort bij dit object.
	 */
	public function getSchuldVerzameling()
	{
		if(!$this->schuldVerzameling instanceof SchuldVerzameling)
			$this->schuldVerzameling = SchuldVerzameling::fromTransactie($this);
		return $this->schuldVerzameling;
	}
	/**
	 * @brief Returneert de IDealKaartjeCodeVerzameling die hoort bij dit object.
	 */
	public function getIDealKaartjeCodeVerzameling()
	{
		if(!$this->iDealKaartjeCodeVerzameling instanceof IDealKaartjeCodeVerzameling)
			$this->iDealKaartjeCodeVerzameling = IDealKaartjeCodeVerzameling::fromTransactie($this);
		return $this->iDealKaartjeCodeVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Transactie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Transactie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array('DibsTransactie',
			'Pin',
			'Giro',
			'Schuld',
			'iDeal');
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Transactie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTransactieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Transactie|false
	 * Een Transactie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$transactieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$transactieID = (int)$a;
		}

		if(is_null($transactieID))
			throw new BadMethodCallException();

		static::cache(array( array($transactieID) ));
		return Entiteit::geefCache(array($transactieID), 'Transactie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			$ids = Entiteit::nietInCache($ids, 'Transactie');

			// is er nog iets te doen?
			if(empty($ids)) return;

			// zoek uit wat de klasse van een id is
			$res = $WSW4DB->q('TABLE SELECT `transactieID`'
			                 .     ', `overerving`'
			                 .' FROM `Transactie`'
			                 .' WHERE (`transactieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );

			$recur = array();
			foreach($res as $row)
			{
				$recur[$row['overerving']][]
				    = array($row['transactieID']);
			}
			foreach($recur as $class => $obj)
			{
				$class::cache($obj, True);
			}

			return;
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Transactie::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Transactie`.`transactieID`'
			                 .     ', `Transactie`.`rubriek`'
			                 .     ', `Transactie`.`soort`'
			                 .     ', `Transactie`.`contact_contactID`'
			                 .     ', `Transactie`.`bedrag`'
			                 .     ', `Transactie`.`uitleg`'
			                 .     ', `Transactie`.`wanneer`'
			                 .     ', `Transactie`.`status`'
			                 .     ', `Transactie`.`gewijzigdWanneer`'
			                 .     ', `Transactie`.`gewijzigdWie`'
			                 .' FROM `Transactie`'
			                 .' WHERE (`transactieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['transactieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Transactie'][$id];
			} else {
				$obj = new Transactie($row['rubriek'], $row['soort'], array($row['contact_contactID']), $row['bedrag'], $row['uitleg'], $row['wanneer']);
			}

			$obj->inDB = True;

			$obj->transactieID  = (int) $row['transactieID'];
			$obj->status  = strtoupper(trim($row['status']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			if($recur === True)
				self::stopInCache($id, $obj);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Transactie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getContactContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->transactieID =
			$WSW4DB->q('RETURNID INSERT INTO `Transactie`'
			          . ' (`overerving`, `rubriek`, `soort`, `contact_contactID`, `bedrag`, `uitleg`, `wanneer`, `status`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %i, %f, %s, %s, %s, %s, %i)'
			          , $classname
			          , $this->rubriek
			          , $this->soort
			          , $this->contact_contactID
			          , $this->bedrag
			          , $this->uitleg
			          , $this->wanneer->strftime('%F %T')
			          , $this->status
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Transactie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Transactie`'
			          .' SET `rubriek` = %s'
			          .   ', `soort` = %s'
			          .   ', `contact_contactID` = %i'
			          .   ', `bedrag` = %f'
			          .   ', `uitleg` = %s'
			          .   ', `wanneer` = %s'
			          .   ', `status` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `transactieID` = %i'
			          , $this->rubriek
			          , $this->soort
			          , $this->contact_contactID
			          , $this->bedrag
			          , $this->uitleg
			          , $this->wanneer->strftime('%F %T')
			          , $this->status
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->transactieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTransactie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTransactie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTransactie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Status';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Rubriek';
			$velden[] = 'Soort';
			$velden[] = 'Contact';
			$velden[] = 'Bedrag';
			$velden[] = 'Uitleg';
			$velden[] = 'Wanneer';
			$velden[] = 'Status';
			break;
		case 'get':
			$velden[] = 'Rubriek';
			$velden[] = 'Soort';
			$velden[] = 'Contact';
			$velden[] = 'Bedrag';
			$velden[] = 'Uitleg';
			$velden[] = 'Wanneer';
			$velden[] = 'Status';
		case 'primary':
			$velden[] = 'contact_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'VerkoopVerzameling';
			$velden[] = 'SchuldVerzameling';
			$velden[] = 'IDealKaartjeCodeVerzameling';
			break;
		default:
			$velden[] = 'Rubriek';
			$velden[] = 'Soort';
			$velden[] = 'Contact';
			$velden[] = 'Bedrag';
			$velden[] = 'Uitleg';
			$velden[] = 'Wanneer';
			$velden[] = 'Status';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Verkoop-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VerkoopFromTransactieVerz = VerkoopVerzameling::fromTransactie($this);
		$returnValue = $VerkoopFromTransactieVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Verkoop met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Schuld-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$SchuldFromTransactieVerz = SchuldVerzameling::fromTransactie($this);
		$returnValue = $SchuldFromTransactieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Schuld met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle iDealKaartjeCode-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$iDealKaartjeCodeFromTransactieVerz = iDealKaartjeCodeVerzameling::fromTransactie($this);
		$returnValue = $iDealKaartjeCodeFromTransactieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object iDealKaartjeCode met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($SchuldFromTransactieVerz as $v)
		{
			$v->setTransactie(NULL);
		}
		$SchuldFromTransactieVerz->opslaan();

		foreach($iDealKaartjeCodeFromTransactieVerz as $v)
		{
			$v->setTransactie(NULL);
		}
		$iDealKaartjeCodeFromTransactieVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $VerkoopFromTransactieVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Transactie`'
		          .' WHERE `transactieID` = %i'
		          .' LIMIT 1'
		          , $this->transactieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->transactieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Transactie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `transactieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->transactieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Transactie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'transactieid':
			return 'int';
		case 'rubriek':
			return 'enum';
		case 'soort':
			return 'enum';
		case 'contact':
			return 'foreign';
		case 'contact_contactid':
			return 'int';
		case 'bedrag':
			return 'money';
		case 'uitleg':
			return 'string';
		case 'wanneer':
			return 'datetime';
		case 'status':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bedrag':
			$type = '%f';
			break;
		case 'transactieID':
		case 'contact_contactID':
			$type = '%i';
			break;
		case 'rubriek':
		case 'soort':
		case 'uitleg':
		case 'wanneer':
		case 'status':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Transactie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `transactieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->transactieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Transactie`'
		          .' WHERE `transactieID` = %i'
		                 , $veld
		          , $this->transactieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Transactie');
		parent::stopInCache($id, $obj, 'DibsTransactie');
		parent::stopInCache($id, $obj, 'Pin');
		parent::stopInCache($id, $obj, 'Giro');
		parent::stopInCache($id, $obj, 'Schuld');
		parent::stopInCache($id, $obj, 'iDeal');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Verkoop-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VerkoopFromTransactieVerz = VerkoopVerzameling::fromTransactie($this);
		$dependencies['Verkoop'] = $VerkoopFromTransactieVerz;

		// Verzamel alle Schuld-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$SchuldFromTransactieVerz = SchuldVerzameling::fromTransactie($this);
		$dependencies['Schuld'] = $SchuldFromTransactieVerz;

		// Verzamel alle iDealKaartjeCode-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$iDealKaartjeCodeFromTransactieVerz = iDealKaartjeCodeVerzameling::fromTransactie($this);
		$dependencies['iDealKaartjeCode'] = $iDealKaartjeCodeFromTransactieVerz;

		return $dependencies;
	}
}
