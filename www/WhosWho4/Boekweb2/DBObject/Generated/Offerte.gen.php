<?
abstract class Offerte_Generated
	extends Entiteit
{
	protected $offerteID;				/**< \brief PRIMARY */
	protected $artikel;
	protected $artikel_artikelID;		/**< \brief PRIMARY */
	protected $leverancier;
	protected $leverancier_contactID;	/**< \brief PRIMARY */
	protected $waardePerStuk;
	protected $vanafDatum;
	protected $vervalDatum;
	/**
	/**
	 * @brief De constructor van de Offerte_Generated-klasse.
	 *
	 * @param mixed $a Artikel (Artikel OR Array(artikel_artikelID) OR
	 * artikel_artikelID)
	 * @param mixed $b Leverancier (Leverancier OR Array(leverancier_contactID) OR
	 * leverancier_contactID)
	 * @param mixed $c WaardePerStuk (money)
	 * @param mixed $d VanafDatum (date)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = 0,
			$d = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Artikel)
		{
			$this->artikel = $a;
			$this->artikel_artikelID = $a->getArtikelID();
		}
		else if(is_array($a))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Leverancier)
		{
			$this->leverancier = $b;
			$this->leverancier_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->leverancier = NULL;
			$this->leverancier_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->leverancier = NULL;
			$this->leverancier_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$c = (float)$c;
		$this->waardePerStuk = $c;

		if(!$d instanceof DateTimeLocale)
		{
			if(is_null($d))
				$d = new DateTimeLocale();
			else
			{
				try {
					$d = new DateTimeLocale($d);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->vanafDatum = $d;

		$this->offerteID = NULL;
		$this->vervalDatum = new DateTimeLocale();
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Artikel';
		$volgorde[] = 'Leverancier';
		$volgorde[] = 'WaardePerStuk';
		$volgorde[] = 'VanafDatum';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld offerteID.
	 *
	 * @return int
	 * De waarde van het veld offerteID.
	 */
	public function getOfferteID()
	{
		return $this->offerteID;
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @return Artikel
	 * De waarde van het veld artikel.
	 */
	public function getArtikel()
	{
		if(!isset($this->artikel)
		 && isset($this->artikel_artikelID)
		 ) {
			$this->artikel = Artikel::geef
					( $this->artikel_artikelID
					);
		}
		return $this->artikel;
	}
	/**
	 * @brief Geef de waarde van het veld artikel_artikelID.
	 *
	 * @return int
	 * De waarde van het veld artikel_artikelID.
	 */
	public function getArtikelArtikelID()
	{
		if (is_null($this->artikel_artikelID) && isset($this->artikel)) {
			$this->artikel_artikelID = $this->artikel->getArtikelID();
		}
		return $this->artikel_artikelID;
	}
	/**
	 * @brief Geef de waarde van het veld leverancier.
	 *
	 * @return Leverancier
	 * De waarde van het veld leverancier.
	 */
	public function getLeverancier()
	{
		if(!isset($this->leverancier)
		 && isset($this->leverancier_contactID)
		 ) {
			$this->leverancier = Leverancier::geef
					( $this->leverancier_contactID
					);
		}
		return $this->leverancier;
	}
	/**
	 * @brief Geef de waarde van het veld leverancier_contactID.
	 *
	 * @return int
	 * De waarde van het veld leverancier_contactID.
	 */
	public function getLeverancierContactID()
	{
		if (is_null($this->leverancier_contactID) && isset($this->leverancier)) {
			$this->leverancier_contactID = $this->leverancier->getContactID();
		}
		return $this->leverancier_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld waardePerStuk.
	 *
	 * @return float
	 * De waarde van het veld waardePerStuk.
	 */
	public function getWaardePerStuk()
	{
		return $this->waardePerStuk;
	}
	/**
	 * @brief Geef de waarde van het veld vanafDatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld vanafDatum.
	 */
	public function getVanafDatum()
	{
		return $this->vanafDatum;
	}
	/**
	 * @brief Geef de waarde van het veld vervalDatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld vervalDatum.
	 */
	public function getVervalDatum()
	{
		return $this->vervalDatum;
	}
	/**
	 * @brief Stel de waarde van het veld vervalDatum in.
	 *
	 * @param mixed $newVervalDatum De nieuwe waarde.
	 *
	 * @return Offerte
	 * Dit Offerte-object.
	 */
	public function setVervalDatum($newVervalDatum)
	{
		unset($this->errors['VervalDatum']);
		if(!$newVervalDatum instanceof DateTimeLocale) {
			try {
				$newVervalDatum = new DateTimeLocale($newVervalDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->vervalDatum->strftime('%F %T') == $newVervalDatum->strftime('%F %T'))
			return $this;

		$this->vervalDatum = $newVervalDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld vervalDatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld vervalDatum geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVervalDatum()
	{
		if (array_key_exists('VervalDatum', $this->errors))
			return $this->errors['VervalDatum'];
		$waarde = $this->getVervalDatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Offerte::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Offerte.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Offerte::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getOfferteID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Offerte|false
	 * Een Offerte-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$offerteID = (int)$a[0];
		}
		else if(isset($a))
		{
			$offerteID = (int)$a;
		}

		if(is_null($offerteID))
			throw new BadMethodCallException();

		static::cache(array( array($offerteID) ));
		return Entiteit::geefCache(array($offerteID), 'Offerte');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Offerte');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Offerte::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Offerte`.`offerteID`'
		                 .     ', `Offerte`.`artikel_artikelID`'
		                 .     ', `Offerte`.`leverancier_contactID`'
		                 .     ', `Offerte`.`waardePerStuk`'
		                 .     ', `Offerte`.`vanafDatum`'
		                 .     ', `Offerte`.`vervalDatum`'
		                 .     ', `Offerte`.`gewijzigdWanneer`'
		                 .     ', `Offerte`.`gewijzigdWie`'
		                 .' FROM `Offerte`'
		                 .' WHERE (`offerteID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['offerteID']);

			$obj = new Offerte(array($row['artikel_artikelID']), array($row['leverancier_contactID']), $row['waardePerStuk'], $row['vanafDatum']);

			$obj->inDB = True;

			$obj->offerteID  = (int) $row['offerteID'];
			$obj->vervalDatum  = new DateTimeLocale($row['vervalDatum']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Offerte')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getArtikelArtikelID();
		$this->getLeverancierContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->offerteID =
			$WSW4DB->q('RETURNID INSERT INTO `Offerte`'
			          . ' (`artikel_artikelID`, `leverancier_contactID`, `waardePerStuk`, `vanafDatum`, `vervalDatum`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %f, %s, %s, %s, %i)'
			          , $this->artikel_artikelID
			          , $this->leverancier_contactID
			          , $this->waardePerStuk
			          , $this->vanafDatum->strftime('%F %T')
			          , $this->vervalDatum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Offerte')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Offerte`'
			          .' SET `artikel_artikelID` = %i'
			          .   ', `leverancier_contactID` = %i'
			          .   ', `waardePerStuk` = %f'
			          .   ', `vanafDatum` = %s'
			          .   ', `vervalDatum` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `offerteID` = %i'
			          , $this->artikel_artikelID
			          , $this->leverancier_contactID
			          , $this->waardePerStuk
			          , $this->vanafDatum->strftime('%F %T')
			          , $this->vervalDatum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->offerteID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenOfferte
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenOfferte($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenOfferte($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'VervalDatum';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'VervalDatum';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Artikel';
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'VanafDatum';
			$velden[] = 'VervalDatum';
			break;
		case 'get':
			$velden[] = 'Artikel';
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'VanafDatum';
			$velden[] = 'VervalDatum';
		case 'primary':
			$velden[] = 'artikel_artikelID';
			$velden[] = 'leverancier_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Artikel';
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'VanafDatum';
			$velden[] = 'VervalDatum';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Offerte`'
		          .' WHERE `offerteID` = %i'
		          .' LIMIT 1'
		          , $this->offerteID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->offerteID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Offerte`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `offerteID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->offerteID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Offerte terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'offerteid':
			return 'int';
		case 'artikel':
			return 'foreign';
		case 'artikel_artikelid':
			return 'int';
		case 'leverancier':
			return 'foreign';
		case 'leverancier_contactid':
			return 'int';
		case 'waardeperstuk':
			return 'money';
		case 'vanafdatum':
			return 'date';
		case 'vervaldatum':
			return 'date';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'waardePerStuk':
			$type = '%f';
			break;
		case 'offerteID':
		case 'artikel_artikelID':
		case 'leverancier_contactID':
			$type = '%i';
			break;
		case 'vanafDatum':
		case 'vervalDatum':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Offerte`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `offerteID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->offerteID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Offerte`'
		          .' WHERE `offerteID` = %i'
		                 , $veld
		          , $this->offerteID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Offerte');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
