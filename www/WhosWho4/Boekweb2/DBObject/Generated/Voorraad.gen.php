<?
abstract class Voorraad_Generated
	extends Entiteit
{
	protected $voorraadID;				/**< \brief PRIMARY */
	protected $artikel;
	protected $artikel_artikelID;		/**< \brief PRIMARY */
	protected $locatie;					/**< \brief ENUM:bw1/magazijn/boekenhok/ejbv/virtueel */
	protected $waardePerStuk;
	protected $btw;						/**< \brief ENUM:21/6/0/NULL */
	protected $omschrijving;			/**< \brief NULL */
	protected $verkoopbaar;
	/** Verzamelingen **/
	protected $voorraadMutatieVerzameling;
	protected $verplaatsingVerzameling;
	protected $verkoopPrijsVerzameling;
	protected $iDealVerzameling;
	protected $barcodeVerzameling;
	protected $tellingItemVerzameling;
	/**
	/**
	 * @brief De constructor van de Voorraad_Generated-klasse.
	 *
	 * @param mixed $a Artikel (Artikel OR Array(artikel_artikelID) OR
	 * artikel_artikelID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Artikel)
		{
			$this->artikel = $a;
			$this->artikel_artikelID = $a->getArtikelID();
		}
		else if(is_array($a))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->voorraadID = NULL;
		$this->locatie = 'BW1';
		$this->waardePerStuk = 0;
		$this->btw = 'NULL';
		$this->omschrijving = NULL;
		$this->verkoopbaar = False;
		$this->voorraadMutatieVerzameling = NULL;
		$this->verplaatsingVerzameling = NULL;
		$this->verkoopPrijsVerzameling = NULL;
		$this->iDealVerzameling = NULL;
		$this->barcodeVerzameling = NULL;
		$this->tellingItemVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Artikel';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld voorraadID.
	 *
	 * @return int
	 * De waarde van het veld voorraadID.
	 */
	public function getVoorraadID()
	{
		return $this->voorraadID;
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @return Artikel
	 * De waarde van het veld artikel.
	 */
	public function getArtikel()
	{
		if(!isset($this->artikel)
		 && isset($this->artikel_artikelID)
		 ) {
			$this->artikel = Artikel::geef
					( $this->artikel_artikelID
					);
		}
		return $this->artikel;
	}
	/**
	 * @brief Geef de waarde van het veld artikel_artikelID.
	 *
	 * @return int
	 * De waarde van het veld artikel_artikelID.
	 */
	public function getArtikelArtikelID()
	{
		if (is_null($this->artikel_artikelID) && isset($this->artikel)) {
			$this->artikel_artikelID = $this->artikel->getArtikelID();
		}
		return $this->artikel_artikelID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld locatie.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld locatie.
	 */
	static public function enumsLocatie()
	{
		static $vals = array('BW1','MAGAZIJN','BOEKENHOK','EJBV','VIRTUEEL');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld locatie.
	 *
	 * @return string
	 * De waarde van het veld locatie.
	 */
	public function getLocatie()
	{
		return $this->locatie;
	}
	/**
	 * @brief Stel de waarde van het veld locatie in.
	 *
	 * @param mixed $newLocatie De nieuwe waarde.
	 *
	 * @return Voorraad
	 * Dit Voorraad-object.
	 */
	public function setLocatie($newLocatie)
	{
		unset($this->errors['Locatie']);
		if(!is_null($newLocatie))
			$newLocatie = strtoupper(trim($newLocatie));
		if($newLocatie === "")
			$newLocatie = NULL;
		if($this->locatie === $newLocatie)
			return $this;

		$this->locatie = $newLocatie;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld locatie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld locatie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkLocatie()
	{
		if (array_key_exists('Locatie', $this->errors))
			return $this->errors['Locatie'];
		$waarde = $this->getLocatie();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsLocatie()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld waardePerStuk.
	 *
	 * @return float
	 * De waarde van het veld waardePerStuk.
	 */
	public function getWaardePerStuk()
	{
		return $this->waardePerStuk;
	}
	/**
	 * @brief Stel de waarde van het veld waardePerStuk in.
	 *
	 * @param mixed $newWaardePerStuk De nieuwe waarde.
	 *
	 * @return Voorraad
	 * Dit Voorraad-object.
	 */
	public function setWaardePerStuk($newWaardePerStuk)
	{
		unset($this->errors['WaardePerStuk']);
		if(!is_null($newWaardePerStuk))
			$newWaardePerStuk = (float)$newWaardePerStuk;
		if($this->waardePerStuk === $newWaardePerStuk)
			return $this;

		$this->waardePerStuk = $newWaardePerStuk;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld waardePerStuk geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld waardePerStuk geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkWaardePerStuk()
	{
		if (array_key_exists('WaardePerStuk', $this->errors))
			return $this->errors['WaardePerStuk'];
		$waarde = $this->getWaardePerStuk();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld btw.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld btw.
	 */
	static public function enumsBtw()
	{
		static $vals = array('21','6','0','NULL');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld btw.
	 *
	 * @return string
	 * De waarde van het veld btw.
	 */
	public function getBtw()
	{
		return $this->btw;
	}
	/**
	 * @brief Stel de waarde van het veld btw in.
	 *
	 * @param mixed $newBtw De nieuwe waarde.
	 *
	 * @return Voorraad
	 * Dit Voorraad-object.
	 */
	public function setBtw($newBtw)
	{
		unset($this->errors['Btw']);
		if(!is_null($newBtw))
			$newBtw = strtoupper(trim($newBtw));
		if($newBtw === "")
			$newBtw = NULL;
		if($this->btw === $newBtw)
			return $this;

		$this->btw = $newBtw;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld btw geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld btw geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkBtw()
	{
		if (array_key_exists('Btw', $this->errors))
			return $this->errors['Btw'];
		$waarde = $this->getBtw();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsBtw()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Voorraad
	 * Dit Voorraad-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld verkoopbaar.
	 *
	 * @return bool
	 * De waarde van het veld verkoopbaar.
	 */
	public function getVerkoopbaar()
	{
		return $this->verkoopbaar;
	}
	/**
	 * @brief Stel de waarde van het veld verkoopbaar in.
	 *
	 * @param mixed $newVerkoopbaar De nieuwe waarde.
	 *
	 * @return Voorraad
	 * Dit Voorraad-object.
	 */
	public function setVerkoopbaar($newVerkoopbaar)
	{
		unset($this->errors['Verkoopbaar']);
		if(!is_null($newVerkoopbaar))
			$newVerkoopbaar = (bool)$newVerkoopbaar;
		if($this->verkoopbaar === $newVerkoopbaar)
			return $this;

		$this->verkoopbaar = $newVerkoopbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld verkoopbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld verkoopbaar geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVerkoopbaar()
	{
		if (array_key_exists('Verkoopbaar', $this->errors))
			return $this->errors['Verkoopbaar'];
		$waarde = $this->getVerkoopbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de VoorraadMutatieVerzameling die hoort bij dit object.
	 */
	public function getVoorraadMutatieVerzameling()
	{
		if(!$this->voorraadMutatieVerzameling instanceof VoorraadMutatieVerzameling)
			$this->voorraadMutatieVerzameling = VoorraadMutatieVerzameling::fromVoorraad($this);
		return $this->voorraadMutatieVerzameling;
	}
	/**
	 * @brief Returneert de VerplaatsingVerzameling die hoort bij dit object.
	 */
	public function getVerplaatsingVerzameling()
	{
		if(!$this->verplaatsingVerzameling instanceof VerplaatsingVerzameling)
			$this->verplaatsingVerzameling = VerplaatsingVerzameling::fromTegenVoorraad($this);
		return $this->verplaatsingVerzameling;
	}
	/**
	 * @brief Returneert de VerkoopPrijsVerzameling die hoort bij dit object.
	 */
	public function getVerkoopPrijsVerzameling()
	{
		if(!$this->verkoopPrijsVerzameling instanceof VerkoopPrijsVerzameling)
			$this->verkoopPrijsVerzameling = VerkoopPrijsVerzameling::fromVoorraad($this);
		return $this->verkoopPrijsVerzameling;
	}
	/**
	 * @brief Returneert de IDealVerzameling die hoort bij dit object.
	 */
	public function getIDealVerzameling()
	{
		if(!$this->iDealVerzameling instanceof IDealVerzameling)
			$this->iDealVerzameling = IDealVerzameling::fromVoorraad($this);
		return $this->iDealVerzameling;
	}
	/**
	 * @brief Returneert de BarcodeVerzameling die hoort bij dit object.
	 */
	public function getBarcodeVerzameling()
	{
		if(!$this->barcodeVerzameling instanceof BarcodeVerzameling)
			$this->barcodeVerzameling = BarcodeVerzameling::fromVoorraad($this);
		return $this->barcodeVerzameling;
	}
	/**
	 * @brief Returneert de TellingItemVerzameling die hoort bij dit object.
	 */
	public function getTellingItemVerzameling()
	{
		if(!$this->tellingItemVerzameling instanceof TellingItemVerzameling)
			$this->tellingItemVerzameling = TellingItemVerzameling::fromVoorraad($this);
		return $this->tellingItemVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Voorraad::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Voorraad.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Voorraad::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getVoorraadID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Voorraad|false
	 * Een Voorraad-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$voorraadID = (int)$a[0];
		}
		else if(isset($a))
		{
			$voorraadID = (int)$a;
		}

		if(is_null($voorraadID))
			throw new BadMethodCallException();

		static::cache(array( array($voorraadID) ));
		return Entiteit::geefCache(array($voorraadID), 'Voorraad');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Voorraad');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Voorraad::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Voorraad`.`voorraadID`'
		                 .     ', `Voorraad`.`artikel_artikelID`'
		                 .     ', `Voorraad`.`locatie`'
		                 .     ', `Voorraad`.`waardePerStuk`'
		                 .     ', `Voorraad`.`btw`'
		                 .     ', `Voorraad`.`omschrijving`'
		                 .     ', `Voorraad`.`verkoopbaar`'
		                 .     ', `Voorraad`.`gewijzigdWanneer`'
		                 .     ', `Voorraad`.`gewijzigdWie`'
		                 .' FROM `Voorraad`'
		                 .' WHERE (`voorraadID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['voorraadID']);

			$obj = new Voorraad(array($row['artikel_artikelID']));

			$obj->inDB = True;

			$obj->voorraadID  = (int) $row['voorraadID'];
			$obj->locatie  = strtoupper(trim($row['locatie']));
			$obj->waardePerStuk  = (float) $row['waardePerStuk'];
			$obj->btw  = strtoupper(trim($row['btw']));
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->verkoopbaar  = (bool) $row['verkoopbaar'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Voorraad')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getArtikelArtikelID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->voorraadID =
			$WSW4DB->q('RETURNID INSERT INTO `Voorraad`'
			          . ' (`artikel_artikelID`, `locatie`, `waardePerStuk`, `btw`, `omschrijving`, `verkoopbaar`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %f, %s, %s, %i, %s, %i)'
			          , $this->artikel_artikelID
			          , $this->locatie
			          , $this->waardePerStuk
			          , $this->btw
			          , $this->omschrijving
			          , $this->verkoopbaar
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Voorraad')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Voorraad`'
			          .' SET `artikel_artikelID` = %i'
			          .   ', `locatie` = %s'
			          .   ', `waardePerStuk` = %f'
			          .   ', `btw` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `verkoopbaar` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `voorraadID` = %i'
			          , $this->artikel_artikelID
			          , $this->locatie
			          , $this->waardePerStuk
			          , $this->btw
			          , $this->omschrijving
			          , $this->verkoopbaar
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->voorraadID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVoorraad
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVoorraad($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVoorraad($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Locatie';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'Btw';
			$velden[] = 'Omschrijving';
			$velden[] = 'Verkoopbaar';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'WaardePerStuk';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Artikel';
			$velden[] = 'Locatie';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'Btw';
			$velden[] = 'Omschrijving';
			$velden[] = 'Verkoopbaar';
			break;
		case 'get':
			$velden[] = 'Artikel';
			$velden[] = 'Locatie';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'Btw';
			$velden[] = 'Omschrijving';
			$velden[] = 'Verkoopbaar';
		case 'primary':
			$velden[] = 'artikel_artikelID';
			break;
		case 'verzamelingen':
			$velden[] = 'VoorraadMutatieVerzameling';
			$velden[] = 'VerplaatsingVerzameling';
			$velden[] = 'VerkoopPrijsVerzameling';
			$velden[] = 'IDealVerzameling';
			$velden[] = 'BarcodeVerzameling';
			$velden[] = 'TellingItemVerzameling';
			break;
		default:
			$velden[] = 'Artikel';
			$velden[] = 'Locatie';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'Btw';
			$velden[] = 'Omschrijving';
			$velden[] = 'Verkoopbaar';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle VoorraadMutatie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VoorraadMutatieFromVoorraadVerz = VoorraadMutatieVerzameling::fromVoorraad($this);
		$returnValue = $VoorraadMutatieFromVoorraadVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object VoorraadMutatie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Verplaatsing-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VerplaatsingFromTegenVoorraadVerz = VerplaatsingVerzameling::fromTegenVoorraad($this);
		$returnValue = $VerplaatsingFromTegenVoorraadVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Verplaatsing met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle VerkoopPrijs-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VerkoopPrijsFromVoorraadVerz = VerkoopPrijsVerzameling::fromVoorraad($this);
		$returnValue = $VerkoopPrijsFromVoorraadVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object VerkoopPrijs met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle iDeal-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$iDealFromVoorraadVerz = iDealVerzameling::fromVoorraad($this);
		$returnValue = $iDealFromVoorraadVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object iDeal met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Barcode-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BarcodeFromVoorraadVerz = BarcodeVerzameling::fromVoorraad($this);
		$returnValue = $BarcodeFromVoorraadVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Barcode met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle TellingItem-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TellingItemFromVoorraadVerz = TellingItemVerzameling::fromVoorraad($this);
		$returnValue = $TellingItemFromVoorraadVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TellingItem met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $VoorraadMutatieFromVoorraadVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $VerplaatsingFromTegenVoorraadVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $VerkoopPrijsFromVoorraadVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $iDealFromVoorraadVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BarcodeFromVoorraadVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TellingItemFromVoorraadVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Voorraad`'
		          .' WHERE `voorraadID` = %i'
		          .' LIMIT 1'
		          , $this->voorraadID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->voorraadID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Voorraad`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `voorraadID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->voorraadID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Voorraad terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'voorraadid':
			return 'int';
		case 'artikel':
			return 'foreign';
		case 'artikel_artikelid':
			return 'int';
		case 'locatie':
			return 'enum';
		case 'waardeperstuk':
			return 'money';
		case 'btw':
			return 'enum';
		case 'omschrijving':
			return 'text';
		case 'verkoopbaar':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'waardePerStuk':
			$type = '%f';
			break;
		case 'voorraadID':
		case 'artikel_artikelID':
		case 'verkoopbaar':
			$type = '%i';
			break;
		case 'locatie':
		case 'btw':
		case 'omschrijving':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Voorraad`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `voorraadID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->voorraadID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Voorraad`'
		          .' WHERE `voorraadID` = %i'
		                 , $veld
		          , $this->voorraadID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Voorraad');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle VoorraadMutatie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VoorraadMutatieFromVoorraadVerz = VoorraadMutatieVerzameling::fromVoorraad($this);
		$dependencies['VoorraadMutatie'] = $VoorraadMutatieFromVoorraadVerz;

		// Verzamel alle Verplaatsing-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VerplaatsingFromTegenVoorraadVerz = VerplaatsingVerzameling::fromTegenVoorraad($this);
		$dependencies['Verplaatsing'] = $VerplaatsingFromTegenVoorraadVerz;

		// Verzamel alle VerkoopPrijs-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VerkoopPrijsFromVoorraadVerz = VerkoopPrijsVerzameling::fromVoorraad($this);
		$dependencies['VerkoopPrijs'] = $VerkoopPrijsFromVoorraadVerz;

		// Verzamel alle iDeal-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$iDealFromVoorraadVerz = iDealVerzameling::fromVoorraad($this);
		$dependencies['iDeal'] = $iDealFromVoorraadVerz;

		// Verzamel alle Barcode-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BarcodeFromVoorraadVerz = BarcodeVerzameling::fromVoorraad($this);
		$dependencies['Barcode'] = $BarcodeFromVoorraadVerz;

		// Verzamel alle TellingItem-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TellingItemFromVoorraadVerz = TellingItemVerzameling::fromVoorraad($this);
		$dependencies['TellingItem'] = $TellingItemFromVoorraadVerz;

		return $dependencies;
	}
}
