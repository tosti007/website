<?
abstract class Artikel_Generated
	extends Entiteit
{
	protected $artikelID;				/**< \brief PRIMARY */
	protected $naam;
	/** Verzamelingen **/
	protected $voorraadVerzameling;
	protected $orderVerzameling;
	protected $jaargangArtikelVerzameling;
	protected $offerteVerzameling;
	protected $bestellingVerzameling;
	/**
	 * @brief De constructor van de Artikel_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->artikelID = NULL;
		$this->naam = '';
		$this->voorraadVerzameling = NULL;
		$this->orderVerzameling = NULL;
		$this->jaargangArtikelVerzameling = NULL;
		$this->offerteVerzameling = NULL;
		$this->bestellingVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld artikelID.
	 *
	 * @return int
	 * De waarde van het veld artikelID.
	 */
	public function getArtikelID()
	{
		return $this->artikelID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Artikel
	 * Dit Artikel-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de VoorraadVerzameling die hoort bij dit object.
	 */
	public function getVoorraadVerzameling()
	{
		if(!$this->voorraadVerzameling instanceof VoorraadVerzameling)
			$this->voorraadVerzameling = VoorraadVerzameling::fromArtikel($this);
		return $this->voorraadVerzameling;
	}
	/**
	 * @brief Returneert de OrderVerzameling die hoort bij dit object.
	 */
	public function getOrderVerzameling()
	{
		if(!$this->orderVerzameling instanceof OrderVerzameling)
			$this->orderVerzameling = OrderVerzameling::fromArtikel($this);
		return $this->orderVerzameling;
	}
	/**
	 * @brief Returneert de JaargangArtikelVerzameling die hoort bij dit object.
	 */
	public function getJaargangArtikelVerzameling()
	{
		if(!$this->jaargangArtikelVerzameling instanceof JaargangArtikelVerzameling)
			$this->jaargangArtikelVerzameling = JaargangArtikelVerzameling::fromArtikel($this);
		return $this->jaargangArtikelVerzameling;
	}
	/**
	 * @brief Returneert de OfferteVerzameling die hoort bij dit object.
	 */
	public function getOfferteVerzameling()
	{
		if(!$this->offerteVerzameling instanceof OfferteVerzameling)
			$this->offerteVerzameling = OfferteVerzameling::fromArtikel($this);
		return $this->offerteVerzameling;
	}
	/**
	 * @brief Returneert de BestellingVerzameling die hoort bij dit object.
	 */
	public function getBestellingVerzameling()
	{
		if(!$this->bestellingVerzameling instanceof BestellingVerzameling)
			$this->bestellingVerzameling = BestellingVerzameling::fromArtikel($this);
		return $this->bestellingVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Artikel::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Artikel.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array('ColaProduct',
			'Dictaat',
			'DibsProduct',
			'iDealKaartje',
			'Boek',
			'AesArtikel');
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Artikel::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getArtikelID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Artikel|false
	 * Een Artikel-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$artikelID = (int)$a;
		}

		if(is_null($artikelID))
			throw new BadMethodCallException();

		static::cache(array( array($artikelID) ));
		return Entiteit::geefCache(array($artikelID), 'Artikel');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			$ids = Entiteit::nietInCache($ids, 'Artikel');

			// is er nog iets te doen?
			if(empty($ids)) return;

			// zoek uit wat de klasse van een id is
			$res = $WSW4DB->q('TABLE SELECT `artikelID`'
			                 .     ', `overerving`'
			                 .' FROM `Artikel`'
			                 .' WHERE (`artikelID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );

			$recur = array();
			foreach($res as $row)
			{
				$recur[$row['overerving']][]
				    = array($row['artikelID']);
			}
			foreach($recur as $class => $obj)
			{
				$class::cache($obj, True);
			}

			return;
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Artikel::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Artikel`.`artikelID`'
			                 .     ', `Artikel`.`naam`'
			                 .     ', `Artikel`.`gewijzigdWanneer`'
			                 .     ', `Artikel`.`gewijzigdWie`'
			                 .' FROM `Artikel`'
			                 .' WHERE (`artikelID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['artikelID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Artikel'][$id];
			} else {
				$obj = new Artikel();
			}

			$obj->inDB = True;

			$obj->artikelID  = (int) $row['artikelID'];
			$obj->naam  = trim($row['naam']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			if($recur === True)
				self::stopInCache($id, $obj);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Artikel')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->artikelID =
			$WSW4DB->q('RETURNID INSERT INTO `Artikel`'
			          . ' (`overerving`, `naam`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %i)'
			          , $classname
			          , $this->naam
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Artikel')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Artikel`'
			          .' SET `naam` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `artikelID` = %i'
			          , $this->naam
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->artikelID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenArtikel
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenArtikel($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenArtikel($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Naam';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Naam';
			break;
		case 'get':
			$velden[] = 'Naam';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'VoorraadVerzameling';
			$velden[] = 'OrderVerzameling';
			$velden[] = 'JaargangArtikelVerzameling';
			$velden[] = 'OfferteVerzameling';
			$velden[] = 'BestellingVerzameling';
			break;
		default:
			$velden[] = 'Naam';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Voorraad-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VoorraadFromArtikelVerz = VoorraadVerzameling::fromArtikel($this);
		$returnValue = $VoorraadFromArtikelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Voorraad met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Order-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$OrderFromArtikelVerz = OrderVerzameling::fromArtikel($this);
		$returnValue = $OrderFromArtikelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Order met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle JaargangArtikel-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$JaargangArtikelFromArtikelVerz = JaargangArtikelVerzameling::fromArtikel($this);
		$returnValue = $JaargangArtikelFromArtikelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object JaargangArtikel met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Offerte-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$OfferteFromArtikelVerz = OfferteVerzameling::fromArtikel($this);
		$returnValue = $OfferteFromArtikelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Offerte met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Bestelling-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$BestellingFromArtikelVerz = BestellingVerzameling::fromArtikel($this);
		$returnValue = $BestellingFromArtikelVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Bestelling met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $VoorraadFromArtikelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $OrderFromArtikelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $JaargangArtikelFromArtikelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $OfferteFromArtikelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $BestellingFromArtikelVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Artikel`'
		          .' WHERE `artikelID` = %i'
		          .' LIMIT 1'
		          , $this->artikelID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->artikelID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Artikel`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `artikelID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->artikelID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Artikel terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'artikelid':
			return 'int';
		case 'naam':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'artikelID':
			$type = '%i';
			break;
		case 'naam':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Artikel`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `artikelID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->artikelID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Artikel`'
		          .' WHERE `artikelID` = %i'
		                 , $veld
		          , $this->artikelID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Artikel');
		parent::stopInCache($id, $obj, 'ColaProduct');
		parent::stopInCache($id, $obj, 'Dictaat');
		parent::stopInCache($id, $obj, 'DibsProduct');
		parent::stopInCache($id, $obj, 'iDealKaartje');
		parent::stopInCache($id, $obj, 'Boek');
		parent::stopInCache($id, $obj, 'AesArtikel');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Voorraad-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VoorraadFromArtikelVerz = VoorraadVerzameling::fromArtikel($this);
		$dependencies['Voorraad'] = $VoorraadFromArtikelVerz;

		// Verzamel alle Order-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$OrderFromArtikelVerz = OrderVerzameling::fromArtikel($this);
		$dependencies['Order'] = $OrderFromArtikelVerz;

		// Verzamel alle JaargangArtikel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$JaargangArtikelFromArtikelVerz = JaargangArtikelVerzameling::fromArtikel($this);
		$dependencies['JaargangArtikel'] = $JaargangArtikelFromArtikelVerz;

		// Verzamel alle Offerte-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$OfferteFromArtikelVerz = OfferteVerzameling::fromArtikel($this);
		$dependencies['Offerte'] = $OfferteFromArtikelVerz;

		// Verzamel alle Bestelling-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$BestellingFromArtikelVerz = BestellingVerzameling::fromArtikel($this);
		$dependencies['Bestelling'] = $BestellingFromArtikelVerz;

		return $dependencies;
	}
}
