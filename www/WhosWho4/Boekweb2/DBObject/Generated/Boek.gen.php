<?
abstract class Boek_Generated
	extends Artikel
{
	protected $isbn;
	protected $auteur;					/**< \brief NULL */
	protected $druk;					/**< \brief NULL */
	protected $uitgever;				/**< \brief NULL */
	/**
	 * @brief De constructor van de Boek_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Artikel

		$this->artikelID = NULL;
		$this->isbn = '';
		$this->auteur = NULL;
		$this->druk = NULL;
		$this->uitgever = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld isbn.
	 *
	 * @return string
	 * De waarde van het veld isbn.
	 */
	public function getIsbn()
	{
		return $this->isbn;
	}
	/**
	 * @brief Stel de waarde van het veld isbn in.
	 *
	 * @param mixed $newIsbn De nieuwe waarde.
	 *
	 * @return Boek
	 * Dit Boek-object.
	 */
	public function setIsbn($newIsbn)
	{
		unset($this->errors['Isbn']);
		if(!is_null($newIsbn))
			$newIsbn = trim($newIsbn);
		if($newIsbn === "")
			$newIsbn = NULL;
		if($this->isbn === $newIsbn)
			return $this;

		$this->isbn = $newIsbn;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld isbn geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld isbn geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkIsbn()
	{
		if (array_key_exists('Isbn', $this->errors))
			return $this->errors['Isbn'];
		$waarde = $this->getIsbn();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld auteur.
	 *
	 * @return string
	 * De waarde van het veld auteur.
	 */
	public function getAuteur()
	{
		return $this->auteur;
	}
	/**
	 * @brief Stel de waarde van het veld auteur in.
	 *
	 * @param mixed $newAuteur De nieuwe waarde.
	 *
	 * @return Boek
	 * Dit Boek-object.
	 */
	public function setAuteur($newAuteur)
	{
		unset($this->errors['Auteur']);
		if(!is_null($newAuteur))
			$newAuteur = trim($newAuteur);
		if($newAuteur === "")
			$newAuteur = NULL;
		if($this->auteur === $newAuteur)
			return $this;

		$this->auteur = $newAuteur;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld auteur geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld auteur geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAuteur()
	{
		if (array_key_exists('Auteur', $this->errors))
			return $this->errors['Auteur'];
		$waarde = $this->getAuteur();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld druk.
	 *
	 * @return string
	 * De waarde van het veld druk.
	 */
	public function getDruk()
	{
		return $this->druk;
	}
	/**
	 * @brief Stel de waarde van het veld druk in.
	 *
	 * @param mixed $newDruk De nieuwe waarde.
	 *
	 * @return Boek
	 * Dit Boek-object.
	 */
	public function setDruk($newDruk)
	{
		unset($this->errors['Druk']);
		if(!is_null($newDruk))
			$newDruk = trim($newDruk);
		if($newDruk === "")
			$newDruk = NULL;
		if($this->druk === $newDruk)
			return $this;

		$this->druk = $newDruk;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld druk geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld druk geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkDruk()
	{
		if (array_key_exists('Druk', $this->errors))
			return $this->errors['Druk'];
		$waarde = $this->getDruk();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld uitgever.
	 *
	 * @return string
	 * De waarde van het veld uitgever.
	 */
	public function getUitgever()
	{
		return $this->uitgever;
	}
	/**
	 * @brief Stel de waarde van het veld uitgever in.
	 *
	 * @param mixed $newUitgever De nieuwe waarde.
	 *
	 * @return Boek
	 * Dit Boek-object.
	 */
	public function setUitgever($newUitgever)
	{
		unset($this->errors['Uitgever']);
		if(!is_null($newUitgever))
			$newUitgever = trim($newUitgever);
		if($newUitgever === "")
			$newUitgever = NULL;
		if($this->uitgever === $newUitgever)
			return $this;

		$this->uitgever = $newUitgever;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld uitgever geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld uitgever geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkUitgever()
	{
		if (array_key_exists('Uitgever', $this->errors))
			return $this->errors['Uitgever'];
		$waarde = $this->getUitgever();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Boek::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Boek.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Boek::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getArtikelID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Boek|false
	 * Een Boek-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$artikelID = (int)$a;
		}

		if(is_null($artikelID))
			throw new BadMethodCallException();

		static::cache(array( array($artikelID) ));
		return Entiteit::geefCache(array($artikelID), 'Boek');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Artikel::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Boek::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Boek`.`isbn`'
			                 .     ', `Boek`.`auteur`'
			                 .     ', `Boek`.`druk`'
			                 .     ', `Boek`.`uitgever`'
			                 .     ', `Artikel`.`artikelID`'
			                 .     ', `Artikel`.`naam`'
			                 .     ', `Artikel`.`gewijzigdWanneer`'
			                 .     ', `Artikel`.`gewijzigdWie`'
			                 .' FROM `Boek`'
			                 .' LEFT JOIN `Artikel` USING (`artikelID`)'
			                 .' WHERE (`artikelID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['artikelID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Boek'][$id];
			} else {
				$obj = new Boek();
			}

			$obj->inDB = True;

			$obj->isbn  = trim($row['isbn']);
			$obj->auteur  = (is_null($row['auteur'])) ? null : trim($row['auteur']);
			$obj->druk  = (is_null($row['druk'])) ? null : trim($row['druk']);
			$obj->uitgever  = (is_null($row['uitgever'])) ? null : trim($row['uitgever']);
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Artikel::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Boek')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Boek`'
			          . ' (`artikelID`, `isbn`, `auteur`, `druk`, `uitgever`)'
			          . ' VALUES (%i, %s, %s, %s, %s)'
			          , $this->artikelID
			          , $this->isbn
			          , $this->auteur
			          , $this->druk
			          , $this->uitgever
			          );

			if($classname == 'Boek')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Boek`'
			          .' SET `isbn` = %s'
			          .   ', `auteur` = %s'
			          .   ', `druk` = %s'
			          .   ', `uitgever` = %s'
			          .' WHERE `artikelID` = %i'
			          , $this->isbn
			          , $this->auteur
			          , $this->druk
			          , $this->uitgever
			          , $this->artikelID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBoek
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBoek($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBoek($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Isbn';
			$velden[] = 'Auteur';
			$velden[] = 'Druk';
			$velden[] = 'Uitgever';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Isbn';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Isbn';
			$velden[] = 'Auteur';
			$velden[] = 'Druk';
			$velden[] = 'Uitgever';
			break;
		case 'get':
			$velden[] = 'Isbn';
			$velden[] = 'Auteur';
			$velden[] = 'Druk';
			$velden[] = 'Uitgever';
		case 'primary':
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Isbn';
			$velden[] = 'Auteur';
			$velden[] = 'Druk';
			$velden[] = 'Uitgever';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Boek`'
		          .' WHERE `artikelID` = %i'
		          .' LIMIT 1'
		          , $this->artikelID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Boek terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'isbn':
			return 'string';
		case 'auteur':
			return 'string';
		case 'druk':
			return 'string';
		case 'uitgever':
			return 'string';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'isbn':
		case 'auteur':
		case 'druk':
		case 'uitgever':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Boek`'
		          ." SET `%l` = %s"
		          .' WHERE `artikelID` = %i'
		          , $veld
		          , $waarde
		          , $this->artikelID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'isbn':
		case 'auteur':
		case 'druk':
		case 'uitgever':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Boek`'
		          .' WHERE `artikelID` = %i'
		                 , $veld
		          , $this->artikelID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
