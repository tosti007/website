<?
abstract class Jaargang_Generated
	extends Entiteit
{
	protected $jaargangID;				/**< \brief PRIMARY */
	protected $vak;
	protected $vak_vakID;				/**< \brief PRIMARY */
	protected $contactPersoon;
	protected $contactPersoon_contactID;/**< \brief PRIMARY */
	protected $datumBegin;
	protected $datumEinde;
	protected $inschrijvingen;
	/** Verzamelingen **/
	protected $jaargangArtikelVerzameling;
	/**
	/**
	 * @brief De constructor van de Jaargang_Generated-klasse.
	 *
	 * @param mixed $a Vak (Vak OR Array(vak_vakID) OR vak_vakID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Vak)
		{
			$this->vak = $a;
			$this->vak_vakID = $a->getVakID();
		}
		else if(is_array($a))
		{
			$this->vak = NULL;
			$this->vak_vakID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->vak = NULL;
			$this->vak_vakID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->jaargangID = NULL;
		$this->contactPersoon = NULL;
		$this->contactPersoon_contactID = 0;
		$this->datumBegin = new DateTimeLocale();
		$this->datumEinde = new DateTimeLocale();
		$this->inschrijvingen = 0;
		$this->jaargangArtikelVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Vak';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld jaargangID.
	 *
	 * @return int
	 * De waarde van het veld jaargangID.
	 */
	public function getJaargangID()
	{
		return $this->jaargangID;
	}
	/**
	 * @brief Geef de waarde van het veld vak.
	 *
	 * @return Vak
	 * De waarde van het veld vak.
	 */
	public function getVak()
	{
		if(!isset($this->vak)
		 && isset($this->vak_vakID)
		 ) {
			$this->vak = Vak::geef
					( $this->vak_vakID
					);
		}
		return $this->vak;
	}
	/**
	 * @brief Geef de waarde van het veld vak_vakID.
	 *
	 * @return int
	 * De waarde van het veld vak_vakID.
	 */
	public function getVakVakID()
	{
		if (is_null($this->vak_vakID) && isset($this->vak)) {
			$this->vak_vakID = $this->vak->getVakID();
		}
		return $this->vak_vakID;
	}
	/**
	 * @brief Geef de waarde van het veld contactPersoon.
	 *
	 * @return Persoon
	 * De waarde van het veld contactPersoon.
	 */
	public function getContactPersoon()
	{
		if(!isset($this->contactPersoon)
		 && isset($this->contactPersoon_contactID)
		 ) {
			$this->contactPersoon = Persoon::geef
					( $this->contactPersoon_contactID
					);
		}
		return $this->contactPersoon;
	}
	/**
	 * @brief Stel de waarde van het veld contactPersoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Jaargang
	 * Dit Jaargang-object.
	 */
	public function setContactPersoon($new_contactID)
	{
		unset($this->errors['ContactPersoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->contactPersoon == $new_contactID
			&& $this->contactPersoon_contactID == $this->contactPersoon->getContactID())
				return $this;
			$this->contactPersoon = $new_contactID;
			$this->contactPersoon_contactID
					= $this->contactPersoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->contactPersoon == NULL 
				&& $this->contactPersoon_contactID == (int)$new_contactID)
				return $this;
			$this->contactPersoon = NULL;
			$this->contactPersoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld contactPersoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld contactPersoon geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkContactPersoon()
	{
		if (array_key_exists('ContactPersoon', $this->errors))
			return $this->errors['ContactPersoon'];
		$waarde1 = $this->getContactPersoon();
		$waarde2 = $this->getContactPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld contactPersoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld contactPersoon_contactID.
	 */
	public function getContactPersoonContactID()
	{
		if (is_null($this->contactPersoon_contactID) && isset($this->contactPersoon)) {
			$this->contactPersoon_contactID = $this->contactPersoon->getContactID();
		}
		return $this->contactPersoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumBegin.
	 */
	public function getDatumBegin()
	{
		return $this->datumBegin;
	}
	/**
	 * @brief Stel de waarde van het veld datumBegin in.
	 *
	 * @param mixed $newDatumBegin De nieuwe waarde.
	 *
	 * @return Jaargang
	 * Dit Jaargang-object.
	 */
	public function setDatumBegin($newDatumBegin)
	{
		unset($this->errors['DatumBegin']);
		if(!$newDatumBegin instanceof DateTimeLocale) {
			try {
				$newDatumBegin = new DateTimeLocale($newDatumBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumBegin->strftime('%F %T') == $newDatumBegin->strftime('%F %T'))
			return $this;

		$this->datumBegin = $newDatumBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumBegin()
	{
		if (array_key_exists('DatumBegin', $this->errors))
			return $this->errors['DatumBegin'];
		$waarde = $this->getDatumBegin();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumEinde.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumEinde.
	 */
	public function getDatumEinde()
	{
		return $this->datumEinde;
	}
	/**
	 * @brief Stel de waarde van het veld datumEinde in.
	 *
	 * @param mixed $newDatumEinde De nieuwe waarde.
	 *
	 * @return Jaargang
	 * Dit Jaargang-object.
	 */
	public function setDatumEinde($newDatumEinde)
	{
		unset($this->errors['DatumEinde']);
		if(!$newDatumEinde instanceof DateTimeLocale) {
			try {
				$newDatumEinde = new DateTimeLocale($newDatumEinde);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumEinde->strftime('%F %T') == $newDatumEinde->strftime('%F %T'))
			return $this;

		$this->datumEinde = $newDatumEinde;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumEinde geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumEinde geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumEinde()
	{
		if (array_key_exists('DatumEinde', $this->errors))
			return $this->errors['DatumEinde'];
		$waarde = $this->getDatumEinde();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inschrijvingen.
	 *
	 * @return int
	 * De waarde van het veld inschrijvingen.
	 */
	public function getInschrijvingen()
	{
		return $this->inschrijvingen;
	}
	/**
	 * @brief Stel de waarde van het veld inschrijvingen in.
	 *
	 * @param mixed $newInschrijvingen De nieuwe waarde.
	 *
	 * @return Jaargang
	 * Dit Jaargang-object.
	 */
	public function setInschrijvingen($newInschrijvingen)
	{
		unset($this->errors['Inschrijvingen']);
		if(!is_null($newInschrijvingen))
			$newInschrijvingen = (int)$newInschrijvingen;
		if($this->inschrijvingen === $newInschrijvingen)
			return $this;

		$this->inschrijvingen = $newInschrijvingen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inschrijvingen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inschrijvingen geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkInschrijvingen()
	{
		if (array_key_exists('Inschrijvingen', $this->errors))
			return $this->errors['Inschrijvingen'];
		$waarde = $this->getInschrijvingen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de JaargangArtikelVerzameling die hoort bij dit object.
	 */
	public function getJaargangArtikelVerzameling()
	{
		if(!$this->jaargangArtikelVerzameling instanceof JaargangArtikelVerzameling)
			$this->jaargangArtikelVerzameling = JaargangArtikelVerzameling::fromJaargang($this);
		return $this->jaargangArtikelVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Jaargang::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Jaargang.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Jaargang::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getJaargangID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Jaargang|false
	 * Een Jaargang-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$jaargangID = (int)$a[0];
		}
		else if(isset($a))
		{
			$jaargangID = (int)$a;
		}

		if(is_null($jaargangID))
			throw new BadMethodCallException();

		static::cache(array( array($jaargangID) ));
		return Entiteit::geefCache(array($jaargangID), 'Jaargang');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Jaargang');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Jaargang::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Jaargang`.`jaargangID`'
		                 .     ', `Jaargang`.`vak_vakID`'
		                 .     ', `Jaargang`.`contactPersoon_contactID`'
		                 .     ', `Jaargang`.`datumBegin`'
		                 .     ', `Jaargang`.`datumEinde`'
		                 .     ', `Jaargang`.`inschrijvingen`'
		                 .     ', `Jaargang`.`gewijzigdWanneer`'
		                 .     ', `Jaargang`.`gewijzigdWie`'
		                 .' FROM `Jaargang`'
		                 .' WHERE (`jaargangID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['jaargangID']);

			$obj = new Jaargang(array($row['vak_vakID']));

			$obj->inDB = True;

			$obj->jaargangID  = (int) $row['jaargangID'];
			$obj->contactPersoon_contactID  = (int) $row['contactPersoon_contactID'];
			$obj->datumBegin  = new DateTimeLocale($row['datumBegin']);
			$obj->datumEinde  = new DateTimeLocale($row['datumEinde']);
			$obj->inschrijvingen  = (int) $row['inschrijvingen'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Jaargang')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getVakVakID();
		$this->getContactPersoonContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->jaargangID =
			$WSW4DB->q('RETURNID INSERT INTO `Jaargang`'
			          . ' (`vak_vakID`, `contactPersoon_contactID`, `datumBegin`, `datumEinde`, `inschrijvingen`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i, %s, %i)'
			          , $this->vak_vakID
			          , $this->contactPersoon_contactID
			          , $this->datumBegin->strftime('%F %T')
			          , $this->datumEinde->strftime('%F %T')
			          , $this->inschrijvingen
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Jaargang')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Jaargang`'
			          .' SET `vak_vakID` = %i'
			          .   ', `contactPersoon_contactID` = %i'
			          .   ', `datumBegin` = %s'
			          .   ', `datumEinde` = %s'
			          .   ', `inschrijvingen` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `jaargangID` = %i'
			          , $this->vak_vakID
			          , $this->contactPersoon_contactID
			          , $this->datumBegin->strftime('%F %T')
			          , $this->datumEinde->strftime('%F %T')
			          , $this->inschrijvingen
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->jaargangID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenJaargang
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenJaargang($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenJaargang($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'ContactPersoon';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEinde';
			$velden[] = 'Inschrijvingen';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'ContactPersoon';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEinde';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Vak';
			$velden[] = 'ContactPersoon';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEinde';
			$velden[] = 'Inschrijvingen';
			break;
		case 'get':
			$velden[] = 'Vak';
			$velden[] = 'ContactPersoon';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEinde';
			$velden[] = 'Inschrijvingen';
		case 'primary':
			$velden[] = 'vak_vakID';
			$velden[] = 'contactPersoon_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'JaargangArtikelVerzameling';
			break;
		default:
			$velden[] = 'Vak';
			$velden[] = 'ContactPersoon';
			$velden[] = 'DatumBegin';
			$velden[] = 'DatumEinde';
			$velden[] = 'Inschrijvingen';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle JaargangArtikel-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$JaargangArtikelFromJaargangVerz = JaargangArtikelVerzameling::fromJaargang($this);
		$returnValue = $JaargangArtikelFromJaargangVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object JaargangArtikel met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $JaargangArtikelFromJaargangVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Jaargang`'
		          .' WHERE `jaargangID` = %i'
		          .' LIMIT 1'
		          , $this->jaargangID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->jaargangID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Jaargang`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `jaargangID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->jaargangID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Jaargang terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'jaargangid':
			return 'int';
		case 'vak':
			return 'foreign';
		case 'vak_vakid':
			return 'int';
		case 'contactpersoon':
			return 'foreign';
		case 'contactpersoon_contactid':
			return 'int';
		case 'datumbegin':
			return 'date';
		case 'datumeinde':
			return 'date';
		case 'inschrijvingen':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'jaargangID':
		case 'vak_vakID':
		case 'contactPersoon_contactID':
		case 'inschrijvingen':
			$type = '%i';
			break;
		case 'datumBegin':
		case 'datumEinde':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Jaargang`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `jaargangID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->jaargangID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Jaargang`'
		          .' WHERE `jaargangID` = %i'
		                 , $veld
		          , $this->jaargangID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Jaargang');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle JaargangArtikel-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$JaargangArtikelFromJaargangVerz = JaargangArtikelVerzameling::fromJaargang($this);
		$dependencies['JaargangArtikel'] = $JaargangArtikelFromJaargangVerz;

		return $dependencies;
	}
}
