<?
abstract class VakStudie_Generated
	extends Entiteit
{
	protected $vak;						/**< \brief PRIMARY */
	protected $vak_vakID;				/**< \brief PRIMARY */
	protected $studie;					/**< \brief PRIMARY */
	protected $studie_studieID;			/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de VakStudie_Generated-klasse.
	 *
	 * @param mixed $a Vak (Vak OR Array(vak_vakID) OR vak_vakID)
	 * @param mixed $b Studie (Studie OR Array(studie_studieID) OR studie_studieID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Vak)
		{
			$this->vak = $a;
			$this->vak_vakID = $a->getVakID();
		}
		else if(is_array($a))
		{
			$this->vak = NULL;
			$this->vak_vakID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->vak = NULL;
			$this->vak_vakID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Studie)
		{
			$this->studie = $b;
			$this->studie_studieID = $b->getStudieID();
		}
		else if(is_array($b))
		{
			$this->studie = NULL;
			$this->studie_studieID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->studie = NULL;
			$this->studie_studieID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Vak';
		$volgorde[] = 'Studie';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld vak.
	 *
	 * @return Vak
	 * De waarde van het veld vak.
	 */
	public function getVak()
	{
		if(!isset($this->vak)
		 && isset($this->vak_vakID)
		 ) {
			$this->vak = Vak::geef
					( $this->vak_vakID
					);
		}
		return $this->vak;
	}
	/**
	 * @brief Geef de waarde van het veld vak_vakID.
	 *
	 * @return int
	 * De waarde van het veld vak_vakID.
	 */
	public function getVakVakID()
	{
		if (is_null($this->vak_vakID) && isset($this->vak)) {
			$this->vak_vakID = $this->vak->getVakID();
		}
		return $this->vak_vakID;
	}
	/**
	 * @brief Geef de waarde van het veld studie.
	 *
	 * @return Studie
	 * De waarde van het veld studie.
	 */
	public function getStudie()
	{
		if(!isset($this->studie)
		 && isset($this->studie_studieID)
		 ) {
			$this->studie = Studie::geef
					( $this->studie_studieID
					);
		}
		return $this->studie;
	}
	/**
	 * @brief Geef de waarde van het veld studie_studieID.
	 *
	 * @return int
	 * De waarde van het veld studie_studieID.
	 */
	public function getStudieStudieID()
	{
		if (is_null($this->studie_studieID) && isset($this->studie)) {
			$this->studie_studieID = $this->studie->getStudieID();
		}
		return $this->studie_studieID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return VakStudie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van VakStudie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return VakStudie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getVakVakID()
		                      , $this->getStudieStudieID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return VakStudie|false
	 * Een VakStudie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$vak_vakID = (int)$a[0];
			$studie_studieID = (int)$a[1];
		}
		else if($a instanceof Vak
		     && $b instanceof Studie)
		{
			$vak_vakID = $a->getVakID();
			$studie_studieID = $b->getStudieID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$vak_vakID = (int)$a;
			$studie_studieID = (int)$b;
		}

		if(is_null($vak_vakID)
		|| is_null($studie_studieID))
			throw new BadMethodCallException();

		static::cache(array( array($vak_vakID, $studie_studieID) ));
		return Entiteit::geefCache(array($vak_vakID, $studie_studieID), 'VakStudie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'VakStudie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('VakStudie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `VakStudie`.`vak_vakID`'
		                 .     ', `VakStudie`.`studie_studieID`'
		                 .     ', `VakStudie`.`gewijzigdWanneer`'
		                 .     ', `VakStudie`.`gewijzigdWie`'
		                 .' FROM `VakStudie`'
		                 .' WHERE (`vak_vakID`, `studie_studieID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['vak_vakID']
			                  ,$row['studie_studieID']);

			$obj = new VakStudie(array($row['vak_vakID']), array($row['studie_studieID']));

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'VakStudie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getVak();
		if(!$rel->getInDB())
			throw new LogicException('foreign Vak is not in DB');
		$this->vak_vakID = $rel->getVakID();

		$rel = $this->getStudie();
		if(!$rel->getInDB())
			throw new LogicException('foreign Studie is not in DB');
		$this->studie_studieID = $rel->getStudieID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `VakStudie`'
			          . ' (`vak_vakID`, `studie_studieID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %i)'
			          , $this->vak_vakID
			          , $this->studie_studieID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'VakStudie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `VakStudie`'
			.' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `vak_vakID` = %i'
			          .  ' AND `studie_studieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->vak_vakID
			          , $this->studie_studieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVakStudie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVakStudie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVakStudie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			break;
		case 'get':
		case 'primary':
			$velden[] = 'vak_vakID';
			$velden[] = 'studie_studieID';
			break;
		case 'verzamelingen':
			break;
		default:
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `VakStudie`'
		          .' WHERE `vak_vakID` = %i'
		          .  ' AND `studie_studieID` = %i'
		          .' LIMIT 1'
		          , $this->vak_vakID
		          , $this->studie_studieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->vak = NULL;
		$this->vak_vakID = NULL;
		$this->studie = NULL;
		$this->studie_studieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `VakStudie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `vak_vakID` = %i'
			          .  ' AND `studie_studieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->vak_vakID
			          , $this->studie_studieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van VakStudie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'vak':
			return 'foreign';
		case 'vak_vakid':
			return 'int';
		case 'studie':
			return 'foreign';
		case 'studie_studieid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `VakStudie`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `vak_vakID` = %i'
		          .  ' AND `studie_studieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->vak_vakID
		          , $this->studie_studieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `VakStudie`'
		          .' WHERE `vak_vakID` = %i'
		          .  ' AND `studie_studieID` = %i'
		                 , $veld
		          , $this->vak_vakID
		          , $this->studie_studieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'VakStudie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
