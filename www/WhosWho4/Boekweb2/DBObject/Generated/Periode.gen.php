<?
abstract class Periode_Generated
	extends Entiteit
{
	protected $periodeID;				/**< \brief PRIMARY */
	protected $datumBegin;
	protected $collegejaar;
	protected $periodeNummer;
	/**
	 * @brief De constructor van de Periode_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->periodeID = NULL;
		$this->datumBegin = new DateTimeLocale();
		$this->collegejaar = 0;
		$this->periodeNummer = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld periodeID.
	 *
	 * @return int
	 * De waarde van het veld periodeID.
	 */
	public function getPeriodeID()
	{
		return $this->periodeID;
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumBegin.
	 */
	public function getDatumBegin()
	{
		return $this->datumBegin;
	}
	/**
	 * @brief Stel de waarde van het veld datumBegin in.
	 *
	 * @param mixed $newDatumBegin De nieuwe waarde.
	 *
	 * @return Periode
	 * Dit Periode-object.
	 */
	public function setDatumBegin($newDatumBegin)
	{
		unset($this->errors['DatumBegin']);
		if(!$newDatumBegin instanceof DateTimeLocale) {
			try {
				$newDatumBegin = new DateTimeLocale($newDatumBegin);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumBegin->strftime('%F %T') == $newDatumBegin->strftime('%F %T'))
			return $this;

		$this->datumBegin = $newDatumBegin;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumBegin geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumBegin geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkDatumBegin()
	{
		if (array_key_exists('DatumBegin', $this->errors))
			return $this->errors['DatumBegin'];
		$waarde = $this->getDatumBegin();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld collegejaar.
	 *
	 * @return int
	 * De waarde van het veld collegejaar.
	 */
	public function getCollegejaar()
	{
		return $this->collegejaar;
	}
	/**
	 * @brief Stel de waarde van het veld collegejaar in.
	 *
	 * @param mixed $newCollegejaar De nieuwe waarde.
	 *
	 * @return Periode
	 * Dit Periode-object.
	 */
	public function setCollegejaar($newCollegejaar)
	{
		unset($this->errors['Collegejaar']);
		if(!is_null($newCollegejaar))
			$newCollegejaar = (int)$newCollegejaar;
		if($this->collegejaar === $newCollegejaar)
			return $this;

		$this->collegejaar = $newCollegejaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld collegejaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld collegejaar geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkCollegejaar()
	{
		if (array_key_exists('Collegejaar', $this->errors))
			return $this->errors['Collegejaar'];
		$waarde = $this->getCollegejaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld periodeNummer.
	 *
	 * @return int
	 * De waarde van het veld periodeNummer.
	 */
	public function getPeriodeNummer()
	{
		return $this->periodeNummer;
	}
	/**
	 * @brief Stel de waarde van het veld periodeNummer in.
	 *
	 * @param mixed $newPeriodeNummer De nieuwe waarde.
	 *
	 * @return Periode
	 * Dit Periode-object.
	 */
	public function setPeriodeNummer($newPeriodeNummer)
	{
		unset($this->errors['PeriodeNummer']);
		if(!is_null($newPeriodeNummer))
			$newPeriodeNummer = (int)$newPeriodeNummer;
		if($this->periodeNummer === $newPeriodeNummer)
			return $this;

		$this->periodeNummer = $newPeriodeNummer;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld periodeNummer geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld periodeNummer geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkPeriodeNummer()
	{
		if (array_key_exists('PeriodeNummer', $this->errors))
			return $this->errors['PeriodeNummer'];
		$waarde = $this->getPeriodeNummer();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Periode::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Periode.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Periode::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getPeriodeID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Periode|false
	 * Een Periode-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$periodeID = (int)$a[0];
		}
		else if(isset($a))
		{
			$periodeID = (int)$a;
		}

		if(is_null($periodeID))
			throw new BadMethodCallException();

		static::cache(array( array($periodeID) ));
		return Entiteit::geefCache(array($periodeID), 'Periode');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Periode');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Periode::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Periode`.`periodeID`'
		                 .     ', `Periode`.`datumBegin`'
		                 .     ', `Periode`.`collegejaar`'
		                 .     ', `Periode`.`periodeNummer`'
		                 .     ', `Periode`.`gewijzigdWanneer`'
		                 .     ', `Periode`.`gewijzigdWie`'
		                 .' FROM `Periode`'
		                 .' WHERE (`periodeID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['periodeID']);

			$obj = new Periode();

			$obj->inDB = True;

			$obj->periodeID  = (int) $row['periodeID'];
			$obj->datumBegin  = new DateTimeLocale($row['datumBegin']);
			$obj->collegejaar  = (int) $row['collegejaar'];
			$obj->periodeNummer  = (int) $row['periodeNummer'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Periode')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->periodeID =
			$WSW4DB->q('RETURNID INSERT INTO `Periode`'
			          . ' (`datumBegin`, `collegejaar`, `periodeNummer`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %i, %s, %i)'
			          , $this->datumBegin->strftime('%F %T')
			          , $this->collegejaar
			          , $this->periodeNummer
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Periode')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Periode`'
			          .' SET `datumBegin` = %s'
			          .   ', `collegejaar` = %i'
			          .   ', `periodeNummer` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `periodeID` = %i'
			          , $this->datumBegin->strftime('%F %T')
			          , $this->collegejaar
			          , $this->periodeNummer
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->periodeID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenPeriode
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenPeriode($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenPeriode($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'DatumBegin';
			$velden[] = 'Collegejaar';
			$velden[] = 'PeriodeNummer';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'DatumBegin';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'DatumBegin';
			$velden[] = 'Collegejaar';
			$velden[] = 'PeriodeNummer';
			break;
		case 'get':
			$velden[] = 'DatumBegin';
			$velden[] = 'Collegejaar';
			$velden[] = 'PeriodeNummer';
		case 'primary':
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'DatumBegin';
			$velden[] = 'Collegejaar';
			$velden[] = 'PeriodeNummer';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Periode`'
		          .' WHERE `periodeID` = %i'
		          .' LIMIT 1'
		          , $this->periodeID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->periodeID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Periode`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `periodeID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->periodeID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Periode terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'periodeid':
			return 'int';
		case 'datumbegin':
			return 'date';
		case 'collegejaar':
			return 'int';
		case 'periodenummer':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'periodeID':
		case 'collegejaar':
		case 'periodeNummer':
			$type = '%i';
			break;
		case 'datumBegin':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Periode`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `periodeID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->periodeID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Periode`'
		          .' WHERE `periodeID` = %i'
		                 , $veld
		          , $this->periodeID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Periode');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
