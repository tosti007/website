<?
abstract class iDealKaartje_Generated
	extends Artikel
{
	protected $activiteit;
	protected $activiteit_activiteitID;	/**< \brief PRIMARY */
	protected $autoInschrijven;
	protected $returnURL;				/**< \brief NULL */
	protected $externVerkrijgbaar;
	protected $digitaalVerkrijgbaar;	/**< \brief ENUM:NAMENLIJST/CODE/PERSOONLIJK */
	protected $magScannen;
	/** Verzamelingen **/
	protected $iDealKaartjeCodeVerzameling;
	/**
	/**
	 * @brief De constructor van de iDealKaartje_Generated-klasse.
	 *
	 * @param mixed $a Activiteit (Activiteit OR Array(activiteit_activiteitID) OR
	 * activiteit_activiteitID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Artikel

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Activiteit)
		{
			$this->activiteit = $a;
			$this->activiteit_activiteitID = $a->getActiviteitID();
		}
		else if(is_array($a))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->artikelID = NULL;
		$this->autoInschrijven = False;
		$this->returnURL = NULL;
		$this->externVerkrijgbaar = False;
		$this->digitaalVerkrijgbaar = 'NAMENLIJST';
		$this->magScannen = False;
		$this->iDealKaartjeCodeVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Activiteit';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @return Activiteit
	 * De waarde van het veld activiteit.
	 */
	public function getActiviteit()
	{
		if(!isset($this->activiteit)
		 && isset($this->activiteit_activiteitID)
		 ) {
			$this->activiteit = Activiteit::geef
					( $this->activiteit_activiteitID
					);
		}
		return $this->activiteit;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld activiteit_activiteitID.
	 */
	public function getActiviteitActiviteitID()
	{
		if (is_null($this->activiteit_activiteitID) && isset($this->activiteit)) {
			$this->activiteit_activiteitID = $this->activiteit->getActiviteitID();
		}
		return $this->activiteit_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld autoInschrijven.
	 *
	 * @return bool
	 * De waarde van het veld autoInschrijven.
	 */
	public function getAutoInschrijven()
	{
		return $this->autoInschrijven;
	}
	/**
	 * @brief Stel de waarde van het veld autoInschrijven in.
	 *
	 * @param mixed $newAutoInschrijven De nieuwe waarde.
	 *
	 * @return iDealKaartje
	 * Dit iDealKaartje-object.
	 */
	public function setAutoInschrijven($newAutoInschrijven)
	{
		unset($this->errors['AutoInschrijven']);
		if(!is_null($newAutoInschrijven))
			$newAutoInschrijven = (bool)$newAutoInschrijven;
		if($this->autoInschrijven === $newAutoInschrijven)
			return $this;

		$this->autoInschrijven = $newAutoInschrijven;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld autoInschrijven geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld autoInschrijven geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkAutoInschrijven()
	{
		if (array_key_exists('AutoInschrijven', $this->errors))
			return $this->errors['AutoInschrijven'];
		$waarde = $this->getAutoInschrijven();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld returnURL.
	 *
	 * @return string
	 * De waarde van het veld returnURL.
	 */
	public function getReturnURL()
	{
		return $this->returnURL;
	}
	/**
	 * @brief Stel de waarde van het veld returnURL in.
	 *
	 * @param mixed $newReturnURL De nieuwe waarde.
	 *
	 * @return iDealKaartje
	 * Dit iDealKaartje-object.
	 */
	public function setReturnURL($newReturnURL)
	{
		unset($this->errors['ReturnURL']);
		if(!is_null($newReturnURL))
			$newReturnURL = trim($newReturnURL);
		if($newReturnURL === "")
			$newReturnURL = NULL;
		if($this->returnURL === $newReturnURL)
			return $this;

		$this->returnURL = $newReturnURL;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld returnURL geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld returnURL geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkReturnURL()
	{
		if (array_key_exists('ReturnURL', $this->errors))
			return $this->errors['ReturnURL'];
		$waarde = $this->getReturnURL();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld externVerkrijgbaar.
	 *
	 * @return bool
	 * De waarde van het veld externVerkrijgbaar.
	 */
	public function getExternVerkrijgbaar()
	{
		return $this->externVerkrijgbaar;
	}
	/**
	 * @brief Stel de waarde van het veld externVerkrijgbaar in.
	 *
	 * @param mixed $newExternVerkrijgbaar De nieuwe waarde.
	 *
	 * @return iDealKaartje
	 * Dit iDealKaartje-object.
	 */
	public function setExternVerkrijgbaar($newExternVerkrijgbaar)
	{
		unset($this->errors['ExternVerkrijgbaar']);
		if(!is_null($newExternVerkrijgbaar))
			$newExternVerkrijgbaar = (bool)$newExternVerkrijgbaar;
		if($this->externVerkrijgbaar === $newExternVerkrijgbaar)
			return $this;

		$this->externVerkrijgbaar = $newExternVerkrijgbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld externVerkrijgbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld externVerkrijgbaar geldig is; anders
	 * een string met een foutmelding.
	 */
	public function checkExternVerkrijgbaar()
	{
		if (array_key_exists('ExternVerkrijgbaar', $this->errors))
			return $this->errors['ExternVerkrijgbaar'];
		$waarde = $this->getExternVerkrijgbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld digitaalVerkrijgbaar.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld digitaalVerkrijgbaar.
	 */
	static public function enumsDigitaalVerkrijgbaar()
	{
		static $vals = array('NAMENLIJST','CODE','PERSOONLIJK');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld digitaalVerkrijgbaar.
	 *
	 * @return string
	 * De waarde van het veld digitaalVerkrijgbaar.
	 */
	public function getDigitaalVerkrijgbaar()
	{
		return $this->digitaalVerkrijgbaar;
	}
	/**
	 * @brief Stel de waarde van het veld digitaalVerkrijgbaar in.
	 *
	 * @param mixed $newDigitaalVerkrijgbaar De nieuwe waarde.
	 *
	 * @return iDealKaartje
	 * Dit iDealKaartje-object.
	 */
	public function setDigitaalVerkrijgbaar($newDigitaalVerkrijgbaar)
	{
		unset($this->errors['DigitaalVerkrijgbaar']);
		if(!is_null($newDigitaalVerkrijgbaar))
			$newDigitaalVerkrijgbaar = strtoupper(trim($newDigitaalVerkrijgbaar));
		if($newDigitaalVerkrijgbaar === "")
			$newDigitaalVerkrijgbaar = NULL;
		if($this->digitaalVerkrijgbaar === $newDigitaalVerkrijgbaar)
			return $this;

		$this->digitaalVerkrijgbaar = $newDigitaalVerkrijgbaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld digitaalVerkrijgbaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld digitaalVerkrijgbaar geldig is; anders
	 * een string met een foutmelding.
	 */
	public function checkDigitaalVerkrijgbaar()
	{
		if (array_key_exists('DigitaalVerkrijgbaar', $this->errors))
			return $this->errors['DigitaalVerkrijgbaar'];
		$waarde = $this->getDigitaalVerkrijgbaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsDigitaalVerkrijgbaar()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld magScannen.
	 *
	 * @return bool
	 * De waarde van het veld magScannen.
	 */
	public function getMagScannen()
	{
		return $this->magScannen;
	}
	/**
	 * @brief Stel de waarde van het veld magScannen in.
	 *
	 * @param mixed $newMagScannen De nieuwe waarde.
	 *
	 * @return iDealKaartje
	 * Dit iDealKaartje-object.
	 */
	public function setMagScannen($newMagScannen)
	{
		unset($this->errors['MagScannen']);
		if(!is_null($newMagScannen))
			$newMagScannen = (bool)$newMagScannen;
		if($this->magScannen === $newMagScannen)
			return $this;

		$this->magScannen = $newMagScannen;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld magScannen geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld magScannen geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkMagScannen()
	{
		if (array_key_exists('MagScannen', $this->errors))
			return $this->errors['MagScannen'];
		$waarde = $this->getMagScannen();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de IDealKaartjeCodeVerzameling die hoort bij dit object.
	 */
	public function getIDealKaartjeCodeVerzameling()
	{
		if(!$this->iDealKaartjeCodeVerzameling instanceof IDealKaartjeCodeVerzameling)
			$this->iDealKaartjeCodeVerzameling = IDealKaartjeCodeVerzameling::fromKaartje($this);
		return $this->iDealKaartjeCodeVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return iDealKaartje::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van iDealKaartje.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return iDealKaartje::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getArtikelID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return iDealKaartje|false
	 * Een iDealKaartje-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$artikelID = (int)$a;
		}

		if(is_null($artikelID))
			throw new BadMethodCallException();

		static::cache(array( array($artikelID) ));
		return Entiteit::geefCache(array($artikelID), 'iDealKaartje');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Artikel::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('iDealKaartje::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `iDealKaartje`.`activiteit_activiteitID`'
			                 .     ', `iDealKaartje`.`autoInschrijven`'
			                 .     ', `iDealKaartje`.`returnURL`'
			                 .     ', `iDealKaartje`.`externVerkrijgbaar`'
			                 .     ', `iDealKaartje`.`digitaalVerkrijgbaar`'
			                 .     ', `iDealKaartje`.`magScannen`'
			                 .     ', `Artikel`.`artikelID`'
			                 .     ', `Artikel`.`naam`'
			                 .     ', `Artikel`.`gewijzigdWanneer`'
			                 .     ', `Artikel`.`gewijzigdWie`'
			                 .' FROM `iDealKaartje`'
			                 .' LEFT JOIN `Artikel` USING (`artikelID`)'
			                 .' WHERE (`artikelID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['artikelID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['iDealKaartje'][$id];
			} else {
				$obj = new iDealKaartje(array($row['activiteit_activiteitID']));
			}

			$obj->inDB = True;

			$obj->autoInschrijven  = (bool) $row['autoInschrijven'];
			$obj->returnURL  = (is_null($row['returnURL'])) ? null : trim($row['returnURL']);
			$obj->externVerkrijgbaar  = (bool) $row['externVerkrijgbaar'];
			$obj->digitaalVerkrijgbaar  = strtoupper(trim($row['digitaalVerkrijgbaar']));
			$obj->magScannen  = (bool) $row['magScannen'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Artikel::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'iDealKaartje')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getActiviteitActiviteitID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `iDealKaartje`'
			          . ' (`artikelID`, `activiteit_activiteitID`, `autoInschrijven`, `returnURL`, `externVerkrijgbaar`, `digitaalVerkrijgbaar`, `magScannen`)'
			          . ' VALUES (%i, %i, %i, %s, %i, %s, %i)'
			          , $this->artikelID
			          , $this->activiteit_activiteitID
			          , $this->autoInschrijven
			          , $this->returnURL
			          , $this->externVerkrijgbaar
			          , $this->digitaalVerkrijgbaar
			          , $this->magScannen
			          );

			if($classname == 'iDealKaartje')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `iDealKaartje`'
			          .' SET `activiteit_activiteitID` = %i'
			          .   ', `autoInschrijven` = %i'
			          .   ', `returnURL` = %s'
			          .   ', `externVerkrijgbaar` = %i'
			          .   ', `digitaalVerkrijgbaar` = %s'
			          .   ', `magScannen` = %i'
			          .' WHERE `artikelID` = %i'
			          , $this->activiteit_activiteitID
			          , $this->autoInschrijven
			          , $this->returnURL
			          , $this->externVerkrijgbaar
			          , $this->digitaalVerkrijgbaar
			          , $this->magScannen
			          , $this->artikelID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldeniDealKaartje
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldeniDealKaartje($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldeniDealKaartje($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'AutoInschrijven';
			$velden[] = 'ReturnURL';
			$velden[] = 'ExternVerkrijgbaar';
			$velden[] = 'DigitaalVerkrijgbaar';
			$velden[] = 'MagScannen';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Activiteit';
			$velden[] = 'AutoInschrijven';
			$velden[] = 'ReturnURL';
			$velden[] = 'ExternVerkrijgbaar';
			$velden[] = 'DigitaalVerkrijgbaar';
			$velden[] = 'MagScannen';
			break;
		case 'get':
			$velden[] = 'Activiteit';
			$velden[] = 'AutoInschrijven';
			$velden[] = 'ReturnURL';
			$velden[] = 'ExternVerkrijgbaar';
			$velden[] = 'DigitaalVerkrijgbaar';
			$velden[] = 'MagScannen';
		case 'primary':
			$velden[] = 'activiteit_activiteitID';
			break;
		case 'verzamelingen':
			$velden[] = 'IDealKaartjeCodeVerzameling';
			break;
		default:
			$velden[] = 'Activiteit';
			$velden[] = 'AutoInschrijven';
			$velden[] = 'ReturnURL';
			$velden[] = 'ExternVerkrijgbaar';
			$velden[] = 'DigitaalVerkrijgbaar';
			$velden[] = 'MagScannen';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle iDealKaartjeCode-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$iDealKaartjeCodeFromKaartjeVerz = iDealKaartjeCodeVerzameling::fromKaartje($this);
		$returnValue = $iDealKaartjeCodeFromKaartjeVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object iDealKaartjeCode met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $iDealKaartjeCodeFromKaartjeVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `iDealKaartje`'
		          .' WHERE `artikelID` = %i'
		          .' LIMIT 1'
		          , $this->artikelID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van iDealKaartje terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'activiteit':
			return 'foreign';
		case 'activiteit_activiteitid':
			return 'int';
		case 'autoinschrijven':
			return 'bool';
		case 'returnurl':
			return 'string';
		case 'externverkrijgbaar':
			return 'bool';
		case 'digitaalverkrijgbaar':
			return 'enum';
		case 'magscannen':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'activiteit_activiteitID':
		case 'autoInschrijven':
		case 'externVerkrijgbaar':
		case 'magScannen':
			$type = '%i';
			break;
		case 'returnURL':
		case 'digitaalVerkrijgbaar':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `iDealKaartje`'
		          ." SET `%l` = $type"
		          .' WHERE `artikelID` = %i'
		          , $veld
		          , $waarde
		          , $this->artikelID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'activiteit':
		case 'activiteit_activiteitID':
		case 'autoInschrijven':
		case 'returnURL':
		case 'externVerkrijgbaar':
		case 'digitaalVerkrijgbaar':
		case 'magScannen':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `iDealKaartje`'
		          .' WHERE `artikelID` = %i'
		                 , $veld
		          , $this->artikelID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle iDealKaartjeCode-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$iDealKaartjeCodeFromKaartjeVerz = iDealKaartjeCodeVerzameling::fromKaartje($this);
		$dependencies['iDealKaartjeCode'] = $iDealKaartjeCodeFromKaartjeVerz;

		return $dependencies;
	}
}
