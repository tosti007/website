<?
abstract class DictaatOrder_Generated
	extends Order
{
	protected $kaftPrijs;
	protected $marge;
	protected $factuurverschil;
	/**
	/**
	 * @brief De constructor van de DictaatOrder_Generated-klasse.
	 *
	 * @param mixed $a Artikel (Artikel OR Array(artikel_artikelID) OR
	 * artikel_artikelID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct($a); // Order

		$this->orderID = NULL;
		$this->kaftPrijs = 0;
		$this->marge = 0;
		$this->factuurverschil = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Artikel';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld kaftPrijs.
	 *
	 * @return float
	 * De waarde van het veld kaftPrijs.
	 */
	public function getKaftPrijs()
	{
		return $this->kaftPrijs;
	}
	/**
	 * @brief Stel de waarde van het veld kaftPrijs in.
	 *
	 * @param mixed $newKaftPrijs De nieuwe waarde.
	 *
	 * @return DictaatOrder
	 * Dit DictaatOrder-object.
	 */
	public function setKaftPrijs($newKaftPrijs)
	{
		unset($this->errors['KaftPrijs']);
		if(!is_null($newKaftPrijs))
			$newKaftPrijs = (float)$newKaftPrijs;
		if($this->kaftPrijs === $newKaftPrijs)
			return $this;

		$this->kaftPrijs = $newKaftPrijs;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kaftPrijs geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kaftPrijs geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkKaftPrijs()
	{
		if (array_key_exists('KaftPrijs', $this->errors))
			return $this->errors['KaftPrijs'];
		$waarde = $this->getKaftPrijs();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld marge.
	 *
	 * @return float
	 * De waarde van het veld marge.
	 */
	public function getMarge()
	{
		return $this->marge;
	}
	/**
	 * @brief Stel de waarde van het veld marge in.
	 *
	 * @param mixed $newMarge De nieuwe waarde.
	 *
	 * @return DictaatOrder
	 * Dit DictaatOrder-object.
	 */
	public function setMarge($newMarge)
	{
		unset($this->errors['Marge']);
		if(!is_null($newMarge))
			$newMarge = (float)$newMarge;
		if($this->marge === $newMarge)
			return $this;

		$this->marge = $newMarge;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld marge geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld marge geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMarge()
	{
		if (array_key_exists('Marge', $this->errors))
			return $this->errors['Marge'];
		$waarde = $this->getMarge();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld factuurverschil.
	 *
	 * @return float
	 * De waarde van het veld factuurverschil.
	 */
	public function getFactuurverschil()
	{
		return $this->factuurverschil;
	}
	/**
	 * @brief Stel de waarde van het veld factuurverschil in.
	 *
	 * @param mixed $newFactuurverschil De nieuwe waarde.
	 *
	 * @return DictaatOrder
	 * Dit DictaatOrder-object.
	 */
	public function setFactuurverschil($newFactuurverschil)
	{
		unset($this->errors['Factuurverschil']);
		if(!is_null($newFactuurverschil))
			$newFactuurverschil = (float)$newFactuurverschil;
		if($this->factuurverschil === $newFactuurverschil)
			return $this;

		$this->factuurverschil = $newFactuurverschil;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld factuurverschil geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld factuurverschil geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkFactuurverschil()
	{
		if (array_key_exists('Factuurverschil', $this->errors))
			return $this->errors['Factuurverschil'];
		$waarde = $this->getFactuurverschil();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return DictaatOrder::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van DictaatOrder.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return DictaatOrder::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getOrderID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return DictaatOrder|false
	 * Een DictaatOrder-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$orderID = (int)$a[0];
		}
		else if(isset($a))
		{
			$orderID = (int)$a;
		}

		if(is_null($orderID))
			throw new BadMethodCallException();

		static::cache(array( array($orderID) ));
		return Entiteit::geefCache(array($orderID), 'DictaatOrder');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Order::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('DictaatOrder::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `DictaatOrder`.`kaftPrijs`'
			                 .     ', `DictaatOrder`.`marge`'
			                 .     ', `DictaatOrder`.`factuurverschil`'
			                 .     ', `Order`.`orderID`'
			                 .     ', `Order`.`artikel_artikelID`'
			                 .     ', `Order`.`leverancier_contactID`'
			                 .     ', `Order`.`waardePerStuk`'
			                 .     ', `Order`.`datumGeplaatst`'
			                 .     ', `Order`.`geannuleerd`'
			                 .     ', `Order`.`aantal`'
			                 .     ', `Order`.`gewijzigdWanneer`'
			                 .     ', `Order`.`gewijzigdWie`'
			                 .' FROM `DictaatOrder`'
			                 .' LEFT JOIN `Order` USING (`orderID`)'
			                 .' WHERE (`orderID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['orderID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['DictaatOrder'][$id];
			} else {
				$obj = new DictaatOrder(array($row['artikel_artikelID']));
			}

			$obj->inDB = True;

			$obj->kaftPrijs  = (float) $row['kaftPrijs'];
			$obj->marge  = (float) $row['marge'];
			$obj->factuurverschil  = (float) $row['factuurverschil'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Order::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'DictaatOrder')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `DictaatOrder`'
			          . ' (`orderID`, `kaftPrijs`, `marge`, `factuurverschil`)'
			          . ' VALUES (%i, %f, %f, %f)'
			          , $this->orderID
			          , $this->kaftPrijs
			          , $this->marge
			          , $this->factuurverschil
			          );

			if($classname == 'DictaatOrder')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `DictaatOrder`'
			          .' SET `kaftPrijs` = %f'
			          .   ', `marge` = %f'
			          .   ', `factuurverschil` = %f'
			          .' WHERE `orderID` = %i'
			          , $this->kaftPrijs
			          , $this->marge
			          , $this->factuurverschil
			          , $this->orderID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDictaatOrder
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDictaatOrder($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDictaatOrder($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'KaftPrijs';
			$velden[] = 'Marge';
			$velden[] = 'Factuurverschil';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'KaftPrijs';
			$velden[] = 'Marge';
			$velden[] = 'Factuurverschil';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'KaftPrijs';
			$velden[] = 'Marge';
			$velden[] = 'Factuurverschil';
			break;
		case 'get':
			$velden[] = 'KaftPrijs';
			$velden[] = 'Marge';
			$velden[] = 'Factuurverschil';
		case 'primary':
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'KaftPrijs';
			$velden[] = 'Marge';
			$velden[] = 'Factuurverschil';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `DictaatOrder`'
		          .' WHERE `orderID` = %i'
		          .' LIMIT 1'
		          , $this->orderID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van DictaatOrder terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'kaftprijs':
			return 'money';
		case 'marge':
			return 'money';
		case 'factuurverschil':
			return 'money';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'kaftPrijs':
		case 'marge':
		case 'factuurverschil':
			$type = '%f';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `DictaatOrder`'
		          ." SET `%l` = %f"
		          .' WHERE `orderID` = %i'
		          , $veld
		          , $waarde
		          , $this->orderID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'kaftPrijs':
		case 'marge':
		case 'factuurverschil':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `DictaatOrder`'
		          .' WHERE `orderID` = %i'
		                 , $veld
		          , $this->orderID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
