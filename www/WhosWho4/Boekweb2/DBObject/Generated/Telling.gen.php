<?
abstract class Telling_Generated
	extends Entiteit
{
	protected $tellingID;				/**< \brief PRIMARY */
	protected $wanneer;
	protected $teller;
	protected $teller_contactID;		/**< \brief PRIMARY */
	protected $Opmerking;
	/** Verzamelingen **/
	protected $tellingItemVerzameling;
	/**
	/**
	 * @brief De constructor van de Telling_Generated-klasse.
	 *
	 * @param mixed $a Wanneer (datetime)
	 * @param mixed $b Teller (Lid OR Array(lid_contactID) OR lid_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(!$a instanceof DateTimeLocale)
		{
			if(is_null($a))
				$a = new DateTimeLocale();
			else
			{
				try {
					$a = new DateTimeLocale($a);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->wanneer = $a;

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Lid)
		{
			$this->teller = $b;
			$this->teller_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->teller = NULL;
			$this->teller_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->teller = NULL;
			$this->teller_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->tellingID = NULL;
		$this->Opmerking = '';
		$this->tellingItemVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Wanneer';
		$volgorde[] = 'Teller';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld tellingID.
	 *
	 * @return int
	 * De waarde van het veld tellingID.
	 */
	public function getTellingID()
	{
		return $this->tellingID;
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld wanneer.
	 */
	public function getWanneer()
	{
		return $this->wanneer;
	}
	/**
	 * @brief Geef de waarde van het veld teller.
	 *
	 * @return Lid
	 * De waarde van het veld teller.
	 */
	public function getTeller()
	{
		if(!isset($this->teller)
		 && isset($this->teller_contactID)
		 ) {
			$this->teller = Lid::geef
					( $this->teller_contactID
					);
		}
		return $this->teller;
	}
	/**
	 * @brief Geef de waarde van het veld teller_contactID.
	 *
	 * @return int
	 * De waarde van het veld teller_contactID.
	 */
	public function getTellerContactID()
	{
		if (is_null($this->teller_contactID) && isset($this->teller)) {
			$this->teller_contactID = $this->teller->getContactID();
		}
		return $this->teller_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld Opmerking.
	 *
	 * @return string
	 * De waarde van het veld Opmerking.
	 */
	public function getOpmerking()
	{
		return $this->Opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld Opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return Telling
	 * Dit Telling-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->Opmerking === $newOpmerking)
			return $this;

		$this->Opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld Opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld Opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de TellingItemVerzameling die hoort bij dit object.
	 */
	public function getTellingItemVerzameling()
	{
		if(!$this->tellingItemVerzameling instanceof TellingItemVerzameling)
			$this->tellingItemVerzameling = TellingItemVerzameling::fromTelling($this);
		return $this->tellingItemVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Telling::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Telling.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Telling::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTellingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Telling|false
	 * Een Telling-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$tellingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$tellingID = (int)$a;
		}

		if(is_null($tellingID))
			throw new BadMethodCallException();

		static::cache(array( array($tellingID) ));
		return Entiteit::geefCache(array($tellingID), 'Telling');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Telling');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Telling::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Telling`.`tellingID`'
		                 .     ', `Telling`.`wanneer`'
		                 .     ', `Telling`.`teller_contactID`'
		                 .     ', `Telling`.`Opmerking`'
		                 .     ', `Telling`.`gewijzigdWanneer`'
		                 .     ', `Telling`.`gewijzigdWie`'
		                 .' FROM `Telling`'
		                 .' WHERE (`tellingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['tellingID']);

			$obj = new Telling($row['wanneer'], array($row['teller_contactID']));

			$obj->inDB = True;

			$obj->tellingID  = (int) $row['tellingID'];
			$obj->Opmerking  = trim($row['Opmerking']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Telling')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getTellerContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->tellingID =
			$WSW4DB->q('RETURNID INSERT INTO `Telling`'
			          . ' (`wanneer`, `teller_contactID`, `Opmerking`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %s, %s, %i)'
			          , $this->wanneer->strftime('%F %T')
			          , $this->teller_contactID
			          , $this->Opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Telling')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Telling`'
			          .' SET `wanneer` = %s'
			          .   ', `teller_contactID` = %i'
			          .   ', `Opmerking` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `tellingID` = %i'
			          , $this->wanneer->strftime('%F %T')
			          , $this->teller_contactID
			          , $this->Opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->tellingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTelling
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTelling($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTelling($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Opmerking';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Opmerking';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Wanneer';
			$velden[] = 'Teller';
			$velden[] = 'Opmerking';
			break;
		case 'get':
			$velden[] = 'Wanneer';
			$velden[] = 'Teller';
			$velden[] = 'Opmerking';
		case 'primary':
			$velden[] = 'teller_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'TellingItemVerzameling';
			break;
		default:
			$velden[] = 'Wanneer';
			$velden[] = 'Teller';
			$velden[] = 'Opmerking';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle TellingItem-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TellingItemFromTellingVerz = TellingItemVerzameling::fromTelling($this);
		$returnValue = $TellingItemFromTellingVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TellingItem met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $TellingItemFromTellingVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Telling`'
		          .' WHERE `tellingID` = %i'
		          .' LIMIT 1'
		          , $this->tellingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->tellingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Telling`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `tellingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->tellingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Telling terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'tellingid':
			return 'int';
		case 'wanneer':
			return 'datetime';
		case 'teller':
			return 'foreign';
		case 'teller_contactid':
			return 'int';
		case 'opmerking':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'tellingID':
		case 'teller_contactID':
			$type = '%i';
			break;
		case 'wanneer':
		case 'Opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Telling`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `tellingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->tellingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Telling`'
		          .' WHERE `tellingID` = %i'
		                 , $veld
		          , $this->tellingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Telling');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle TellingItem-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TellingItemFromTellingVerz = TellingItemVerzameling::fromTelling($this);
		$dependencies['TellingItem'] = $TellingItemFromTellingVerz;

		return $dependencies;
	}
}
