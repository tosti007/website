<?
abstract class TellingItem_Generated
	extends Entiteit
{
	protected $telling;					/**< \brief PRIMARY */
	protected $telling_tellingID;		/**< \brief PRIMARY */
	protected $voorraad;				/**< \brief PRIMARY */
	protected $voorraad_voorraadID;		/**< \brief PRIMARY */
	protected $aantal;
	/**
	/**
	 * @brief De constructor van de TellingItem_Generated-klasse.
	 *
	 * @param mixed $a Telling (Telling OR Array(telling_tellingID) OR
	 * telling_tellingID)
	 * @param mixed $b Voorraad (Voorraad OR Array(voorraad_voorraadID) OR
	 * voorraad_voorraadID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Telling)
		{
			$this->telling = $a;
			$this->telling_tellingID = $a->getTellingID();
		}
		else if(is_array($a))
		{
			$this->telling = NULL;
			$this->telling_tellingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->telling = NULL;
			$this->telling_tellingID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Voorraad)
		{
			$this->voorraad = $b;
			$this->voorraad_voorraadID = $b->getVoorraadID();
		}
		else if(is_array($b))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->aantal = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Telling';
		$volgorde[] = 'Voorraad';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld telling.
	 *
	 * @return Telling
	 * De waarde van het veld telling.
	 */
	public function getTelling()
	{
		if(!isset($this->telling)
		 && isset($this->telling_tellingID)
		 ) {
			$this->telling = Telling::geef
					( $this->telling_tellingID
					);
		}
		return $this->telling;
	}
	/**
	 * @brief Geef de waarde van het veld telling_tellingID.
	 *
	 * @return int
	 * De waarde van het veld telling_tellingID.
	 */
	public function getTellingTellingID()
	{
		if (is_null($this->telling_tellingID) && isset($this->telling)) {
			$this->telling_tellingID = $this->telling->getTellingID();
		}
		return $this->telling_tellingID;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @return Voorraad
	 * De waarde van het veld voorraad.
	 */
	public function getVoorraad()
	{
		if(!isset($this->voorraad)
		 && isset($this->voorraad_voorraadID)
		 ) {
			$this->voorraad = Voorraad::geef
					( $this->voorraad_voorraadID
					);
		}
		return $this->voorraad;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad_voorraadID.
	 *
	 * @return int
	 * De waarde van het veld voorraad_voorraadID.
	 */
	public function getVoorraadVoorraadID()
	{
		if (is_null($this->voorraad_voorraadID) && isset($this->voorraad)) {
			$this->voorraad_voorraadID = $this->voorraad->getVoorraadID();
		}
		return $this->voorraad_voorraadID;
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @return int
	 * De waarde van het veld aantal.
	 */
	public function getAantal()
	{
		return $this->aantal;
	}
	/**
	 * @brief Stel de waarde van het veld aantal in.
	 *
	 * @param mixed $newAantal De nieuwe waarde.
	 *
	 * @return TellingItem
	 * Dit TellingItem-object.
	 */
	public function setAantal($newAantal)
	{
		unset($this->errors['Aantal']);
		if(!is_null($newAantal))
			$newAantal = (int)$newAantal;
		if($this->aantal === $newAantal)
			return $this;

		$this->aantal = $newAantal;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aantal geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aantal geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAantal()
	{
		if (array_key_exists('Aantal', $this->errors))
			return $this->errors['Aantal'];
		$waarde = $this->getAantal();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return TellingItem::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van TellingItem.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return TellingItem::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getTellingTellingID()
		                      , $this->getVoorraadVoorraadID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return TellingItem|false
	 * Een TellingItem-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$telling_tellingID = (int)$a[0];
			$voorraad_voorraadID = (int)$a[1];
		}
		else if($a instanceof Telling
		     && $b instanceof Voorraad)
		{
			$telling_tellingID = $a->getTellingID();
			$voorraad_voorraadID = $b->getVoorraadID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$telling_tellingID = (int)$a;
			$voorraad_voorraadID = (int)$b;
		}

		if(is_null($telling_tellingID)
		|| is_null($voorraad_voorraadID))
			throw new BadMethodCallException();

		static::cache(array( array($telling_tellingID, $voorraad_voorraadID) ));
		return Entiteit::geefCache(array($telling_tellingID, $voorraad_voorraadID), 'TellingItem');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'TellingItem');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('TellingItem::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `TellingItem`.`telling_tellingID`'
		                 .     ', `TellingItem`.`voorraad_voorraadID`'
		                 .     ', `TellingItem`.`aantal`'
		                 .     ', `TellingItem`.`gewijzigdWanneer`'
		                 .     ', `TellingItem`.`gewijzigdWie`'
		                 .' FROM `TellingItem`'
		                 .' WHERE (`telling_tellingID`, `voorraad_voorraadID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['telling_tellingID']
			                  ,$row['voorraad_voorraadID']);

			$obj = new TellingItem(array($row['telling_tellingID']), array($row['voorraad_voorraadID']));

			$obj->inDB = True;

			$obj->aantal  = (int) $row['aantal'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'TellingItem')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getTelling();
		if(!$rel->getInDB())
			throw new LogicException('foreign Telling is not in DB');
		$this->telling_tellingID = $rel->getTellingID();

		$rel = $this->getVoorraad();
		if(!$rel->getInDB())
			throw new LogicException('foreign Voorraad is not in DB');
		$this->voorraad_voorraadID = $rel->getVoorraadID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `TellingItem`'
			          . ' (`telling_tellingID`, `voorraad_voorraadID`, `aantal`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %s, %i)'
			          , $this->telling_tellingID
			          , $this->voorraad_voorraadID
			          , $this->aantal
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'TellingItem')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `TellingItem`'
			          .' SET `aantal` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `telling_tellingID` = %i'
			          .  ' AND `voorraad_voorraadID` = %i'
			          , $this->aantal
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->telling_tellingID
			          , $this->voorraad_voorraadID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTellingItem
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTellingItem($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTellingItem($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Aantal';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Aantal';
			break;
		case 'get':
			$velden[] = 'Aantal';
		case 'primary':
			$velden[] = 'telling_tellingID';
			$velden[] = 'voorraad_voorraadID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Aantal';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `TellingItem`'
		          .' WHERE `telling_tellingID` = %i'
		          .  ' AND `voorraad_voorraadID` = %i'
		          .' LIMIT 1'
		          , $this->telling_tellingID
		          , $this->voorraad_voorraadID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->telling = NULL;
		$this->telling_tellingID = NULL;
		$this->voorraad = NULL;
		$this->voorraad_voorraadID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `TellingItem`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `telling_tellingID` = %i'
			          .  ' AND `voorraad_voorraadID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->telling_tellingID
			          , $this->voorraad_voorraadID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van TellingItem terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'telling':
			return 'foreign';
		case 'telling_tellingid':
			return 'int';
		case 'voorraad':
			return 'foreign';
		case 'voorraad_voorraadid':
			return 'int';
		case 'aantal':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `TellingItem`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `telling_tellingID` = %i'
		          .  ' AND `voorraad_voorraadID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->telling_tellingID
		          , $this->voorraad_voorraadID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `TellingItem`'
		          .' WHERE `telling_tellingID` = %i'
		          .  ' AND `voorraad_voorraadID` = %i'
		                 , $veld
		          , $this->telling_tellingID
		          , $this->voorraad_voorraadID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'TellingItem');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
