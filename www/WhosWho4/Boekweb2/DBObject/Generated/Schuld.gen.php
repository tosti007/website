<?
abstract class Schuld_Generated
	extends Transactie
{
	protected $afgeschreven;			/**< \brief NULL */
	protected $transactie;				/**< \brief NULL */
	protected $transactie_transactieID;	/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de Schuld_Generated-klasse.
	 *
	 * @param mixed $a Rubriek (enum)
	 * @param mixed $b Soort (enum)
	 * @param mixed $c Contact (Contact OR Array(contact_contactID) OR
	 * contact_contactID)
	 * @param mixed $d Bedrag (money)
	 * @param mixed $e Uitleg (string)
	 * @param mixed $f Wanneer (datetime)
	 */
	public function __construct($a = 'BOEKWEB',
			$b = 'VERKOOP',
			$c = NULL,
			$d = NULL,
			$e = '',
			$f = NULL)
	{
		parent::__construct($a, $b, $c, $d, $e, $f); // Transactie

		$this->transactieID = NULL;
		$this->afgeschreven = new DateTimeLocale(NULL);
		$this->transactie = NULL;
		$this->transactie_transactieID = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Rubriek';
		$volgorde[] = 'Soort';
		$volgorde[] = 'Contact';
		$volgorde[] = 'Bedrag';
		$volgorde[] = 'Uitleg';
		$volgorde[] = 'Wanneer';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld afgeschreven.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld afgeschreven.
	 */
	public function getAfgeschreven()
	{
		return $this->afgeschreven;
	}
	/**
	 * @brief Stel de waarde van het veld afgeschreven in.
	 *
	 * @param mixed $newAfgeschreven De nieuwe waarde.
	 *
	 * @return Schuld
	 * Dit Schuld-object.
	 */
	public function setAfgeschreven($newAfgeschreven)
	{
		unset($this->errors['Afgeschreven']);
		if(!$newAfgeschreven instanceof DateTimeLocale) {
			try {
				$newAfgeschreven = new DateTimeLocale($newAfgeschreven);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->afgeschreven->strftime('%F %T') == $newAfgeschreven->strftime('%F %T'))
			return $this;

		$this->afgeschreven = $newAfgeschreven;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld afgeschreven geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld afgeschreven geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkAfgeschreven()
	{
		if (array_key_exists('Afgeschreven', $this->errors))
			return $this->errors['Afgeschreven'];
		$waarde = $this->getAfgeschreven();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld transactie.
	 *
	 * @return Transactie
	 * De waarde van het veld transactie.
	 */
	public function getTransactie()
	{
		if(!isset($this->transactie)
		 && isset($this->transactie_transactieID)
		 ) {
			$this->transactie = Transactie::geef
					( $this->transactie_transactieID
					);
		}
		return $this->transactie;
	}
	/**
	 * @brief Stel de waarde van het veld transactie in.
	 *
	 * @param mixed $new_transactieID De nieuwe waarde.
	 *
	 * @return Schuld
	 * Dit Schuld-object.
	 */
	public function setTransactie($new_transactieID)
	{
		unset($this->errors['Transactie']);
		if($new_transactieID instanceof Transactie
		) {
			if($this->transactie == $new_transactieID
			&& $this->transactie_transactieID == $this->transactie->getTransactieID())
				return $this;
			$this->transactie = $new_transactieID;
			$this->transactie_transactieID
					= $this->transactie->getTransactieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_transactieID) || $new_transactieID == 0)) {
			if($this->transactie == NULL && $this->transactie_transactieID == NULL)
				return $this;
			$this->transactie = NULL;
			$this->transactie_transactieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_transactieID)
		) {
			if($this->transactie == NULL 
				&& $this->transactie_transactieID == (int)$new_transactieID)
				return $this;
			$this->transactie = NULL;
			$this->transactie_transactieID
					= (int)$new_transactieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld transactie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld transactie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkTransactie()
	{
		if (array_key_exists('Transactie', $this->errors))
			return $this->errors['Transactie'];
		$waarde1 = $this->getTransactie();
		$waarde2 = $this->getTransactieTransactieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld transactie_transactieID.
	 *
	 * @return int
	 * De waarde van het veld transactie_transactieID.
	 */
	public function getTransactieTransactieID()
	{
		if (is_null($this->transactie_transactieID) && isset($this->transactie)) {
			$this->transactie_transactieID = $this->transactie->getTransactieID();
		}
		return $this->transactie_transactieID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Schuld::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Schuld.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Schuld::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTransactieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Schuld|false
	 * Een Schuld-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$transactieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$transactieID = (int)$a;
		}

		if(is_null($transactieID))
			throw new BadMethodCallException();

		static::cache(array( array($transactieID) ));
		return Entiteit::geefCache(array($transactieID), 'Schuld');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Transactie::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Schuld::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Schuld`.`afgeschreven`'
			                 .     ', `Schuld`.`transactie_transactieID`'
			                 .     ', `Transactie`.`transactieID`'
			                 .     ', `Transactie`.`rubriek`'
			                 .     ', `Transactie`.`soort`'
			                 .     ', `Transactie`.`contact_contactID`'
			                 .     ', `Transactie`.`bedrag`'
			                 .     ', `Transactie`.`uitleg`'
			                 .     ', `Transactie`.`wanneer`'
			                 .     ', `Transactie`.`status`'
			                 .     ', `Transactie`.`gewijzigdWanneer`'
			                 .     ', `Transactie`.`gewijzigdWie`'
			                 .' FROM `Schuld`'
			                 .' LEFT JOIN `Transactie` USING (`transactieID`)'
			                 .' WHERE (`transactieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['transactieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Schuld'][$id];
			} else {
				$obj = new Schuld($row['rubriek'], $row['soort'], array($row['contact_contactID']), $row['bedrag'], $row['uitleg'], $row['wanneer']);
			}

			$obj->inDB = True;

			$obj->afgeschreven  = new DateTimeLocale($row['afgeschreven']);
			$obj->transactie_transactieID  = (is_null($row['transactie_transactieID'])) ? null : (int) $row['transactie_transactieID'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Transactie::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Schuld')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getTransactieTransactieID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Schuld`'
			          . ' (`transactieID`, `afgeschreven`, `transactie_transactieID`)'
			          . ' VALUES (%i, %s, %i)'
			          , $this->transactieID
			          , (!is_null($this->afgeschreven))?$this->afgeschreven->strftime('%F %T'):null
			          , $this->transactie_transactieID
			          );

			if($classname == 'Schuld')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Schuld`'
			          .' SET `afgeschreven` = %s'
			          .   ', `transactie_transactieID` = %i'
			          .' WHERE `transactieID` = %i'
			          , (!is_null($this->afgeschreven))?$this->afgeschreven->strftime('%F %T'):null
			          , $this->transactie_transactieID
			          , $this->transactieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenSchuld
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenSchuld($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenSchuld($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Afgeschreven';
			$velden[] = 'Transactie';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Afgeschreven';
			$velden[] = 'Transactie';
			break;
		case 'get':
			$velden[] = 'Afgeschreven';
			$velden[] = 'Transactie';
		case 'primary':
			$velden[] = 'transactie_transactieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Afgeschreven';
			$velden[] = 'Transactie';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Schuld`'
		          .' WHERE `transactieID` = %i'
		          .' LIMIT 1'
		          , $this->transactieID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Schuld terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'afgeschreven':
			return 'datetime';
		case 'transactie':
			return 'foreign';
		case 'transactie_transactieid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'transactie_transactieID':
			$type = '%i';
			break;
		case 'afgeschreven':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Schuld`'
		          ." SET `%l` = $type"
		          .' WHERE `transactieID` = %i'
		          , $veld
		          , $waarde
		          , $this->transactieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'afgeschreven':
		case 'transactie':
		case 'transactie_transactieID':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Schuld`'
		          .' WHERE `transactieID` = %i'
		                 , $veld
		          , $this->transactieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
