<?
abstract class iDealKaartjeCode_Generated
	extends Entiteit
{
	protected $id;						/**< \brief PRIMARY */
	protected $kaartje;
	protected $kaartje_artikelID;		/**< \brief PRIMARY */
	protected $code;
	protected $transactie;				/**< \brief NULL */
	protected $transactie_transactieID;	/**< \brief PRIMARY */
	protected $gescand;
	/**
	/**
	 * @brief De constructor van de iDealKaartjeCode_Generated-klasse.
	 *
	 * @param mixed $a Kaartje (iDealKaartje OR Array(iDealKaartje_artikelID) OR
	 * iDealKaartje_artikelID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof iDealKaartje)
		{
			$this->kaartje = $a;
			$this->kaartje_artikelID = $a->getArtikelID();
		}
		else if(is_array($a))
		{
			$this->kaartje = NULL;
			$this->kaartje_artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->kaartje = NULL;
			$this->kaartje_artikelID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->id = NULL;
		$this->code = '';
		$this->transactie = NULL;
		$this->transactie_transactieID = NULL;
		$this->gescand = False;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Kaartje';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @return int
	 * De waarde van het veld id.
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * @brief Geef de waarde van het veld kaartje.
	 *
	 * @return iDealKaartje
	 * De waarde van het veld kaartje.
	 */
	public function getKaartje()
	{
		if(!isset($this->kaartje)
		 && isset($this->kaartje_artikelID)
		 ) {
			$this->kaartje = iDealKaartje::geef
					( $this->kaartje_artikelID
					);
		}
		return $this->kaartje;
	}
	/**
	 * @brief Geef de waarde van het veld kaartje_artikelID.
	 *
	 * @return int
	 * De waarde van het veld kaartje_artikelID.
	 */
	public function getKaartjeArtikelID()
	{
		if (is_null($this->kaartje_artikelID) && isset($this->kaartje)) {
			$this->kaartje_artikelID = $this->kaartje->getArtikelID();
		}
		return $this->kaartje_artikelID;
	}
	/**
	 * @brief Geef de waarde van het veld code.
	 *
	 * @return string
	 * De waarde van het veld code.
	 */
	public function getCode()
	{
		return $this->code;
	}
	/**
	 * @brief Stel de waarde van het veld code in.
	 *
	 * @param mixed $newCode De nieuwe waarde.
	 *
	 * @return iDealKaartjeCode
	 * Dit iDealKaartjeCode-object.
	 */
	public function setCode($newCode)
	{
		unset($this->errors['Code']);
		if(!is_null($newCode))
			$newCode = trim($newCode);
		if($newCode === "")
			$newCode = NULL;
		if($this->code === $newCode)
			return $this;

		$this->code = $newCode;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld code geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld code geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkCode()
	{
		if (array_key_exists('Code', $this->errors))
			return $this->errors['Code'];
		$waarde = $this->getCode();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld transactie.
	 *
	 * @return Transactie
	 * De waarde van het veld transactie.
	 */
	public function getTransactie()
	{
		if(!isset($this->transactie)
		 && isset($this->transactie_transactieID)
		 ) {
			$this->transactie = Transactie::geef
					( $this->transactie_transactieID
					);
		}
		return $this->transactie;
	}
	/**
	 * @brief Stel de waarde van het veld transactie in.
	 *
	 * @param mixed $new_transactieID De nieuwe waarde.
	 *
	 * @return iDealKaartjeCode
	 * Dit iDealKaartjeCode-object.
	 */
	public function setTransactie($new_transactieID)
	{
		unset($this->errors['Transactie']);
		if($new_transactieID instanceof Transactie
		) {
			if($this->transactie == $new_transactieID
			&& $this->transactie_transactieID == $this->transactie->getTransactieID())
				return $this;
			$this->transactie = $new_transactieID;
			$this->transactie_transactieID
					= $this->transactie->getTransactieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_transactieID) || $new_transactieID == 0)) {
			if($this->transactie == NULL && $this->transactie_transactieID == NULL)
				return $this;
			$this->transactie = NULL;
			$this->transactie_transactieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_transactieID)
		) {
			if($this->transactie == NULL 
				&& $this->transactie_transactieID == (int)$new_transactieID)
				return $this;
			$this->transactie = NULL;
			$this->transactie_transactieID
					= (int)$new_transactieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld transactie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld transactie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkTransactie()
	{
		if (array_key_exists('Transactie', $this->errors))
			return $this->errors['Transactie'];
		$waarde1 = $this->getTransactie();
		$waarde2 = $this->getTransactieTransactieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld transactie_transactieID.
	 *
	 * @return int
	 * De waarde van het veld transactie_transactieID.
	 */
	public function getTransactieTransactieID()
	{
		if (is_null($this->transactie_transactieID) && isset($this->transactie)) {
			$this->transactie_transactieID = $this->transactie->getTransactieID();
		}
		return $this->transactie_transactieID;
	}
	/**
	 * @brief Geef de waarde van het veld gescand.
	 *
	 * @return bool
	 * De waarde van het veld gescand.
	 */
	public function getGescand()
	{
		return $this->gescand;
	}
	/**
	 * @brief Stel de waarde van het veld gescand in.
	 *
	 * @param mixed $newGescand De nieuwe waarde.
	 *
	 * @return iDealKaartjeCode
	 * Dit iDealKaartjeCode-object.
	 */
	public function setGescand($newGescand)
	{
		unset($this->errors['Gescand']);
		if(!is_null($newGescand))
			$newGescand = (bool)$newGescand;
		if($this->gescand === $newGescand)
			return $this;

		$this->gescand = $newGescand;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld gescand geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld gescand geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkGescand()
	{
		if (array_key_exists('Gescand', $this->errors))
			return $this->errors['Gescand'];
		$waarde = $this->getGescand();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return iDealKaartjeCode::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van iDealKaartjeCode.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return iDealKaartjeCode::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getId());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return iDealKaartjeCode|false
	 * Een iDealKaartjeCode-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$id = (int)$a[0];
		}
		else if(isset($a))
		{
			$id = (int)$a;
		}

		if(is_null($id))
			throw new BadMethodCallException();

		static::cache(array( array($id) ));
		return Entiteit::geefCache(array($id), 'iDealKaartjeCode');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'iDealKaartjeCode');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('iDealKaartjeCode::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `iDealKaartjeCode`.`id`'
		                 .     ', `iDealKaartjeCode`.`kaartje_artikelID`'
		                 .     ', `iDealKaartjeCode`.`code`'
		                 .     ', `iDealKaartjeCode`.`transactie_transactieID`'
		                 .     ', `iDealKaartjeCode`.`gescand`'
		                 .     ', `iDealKaartjeCode`.`gewijzigdWanneer`'
		                 .     ', `iDealKaartjeCode`.`gewijzigdWie`'
		                 .' FROM `iDealKaartjeCode`'
		                 .' WHERE (`id`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['id']);

			$obj = new iDealKaartjeCode(array($row['kaartje_artikelID']));

			$obj->inDB = True;

			$obj->id  = (int) $row['id'];
			$obj->code  = trim($row['code']);
			$obj->transactie_transactieID  = (is_null($row['transactie_transactieID'])) ? null : (int) $row['transactie_transactieID'];
			$obj->gescand  = (bool) $row['gescand'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'iDealKaartjeCode')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getKaartjeArtikelID();
		$this->getTransactieTransactieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->id =
			$WSW4DB->q('RETURNID INSERT INTO `iDealKaartjeCode`'
			          . ' (`kaartje_artikelID`, `code`, `transactie_transactieID`, `gescand`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %i, %i, %s, %i)'
			          , $this->kaartje_artikelID
			          , $this->code
			          , $this->transactie_transactieID
			          , $this->gescand
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'iDealKaartjeCode')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `iDealKaartjeCode`'
			          .' SET `kaartje_artikelID` = %i'
			          .   ', `code` = %s'
			          .   ', `transactie_transactieID` = %i'
			          .   ', `gescand` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->kaartje_artikelID
			          , $this->code
			          , $this->transactie_transactieID
			          , $this->gescand
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldeniDealKaartjeCode
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldeniDealKaartjeCode($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldeniDealKaartjeCode($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Code';
			$velden[] = 'Transactie';
			$velden[] = 'Gescand';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Code';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Kaartje';
			$velden[] = 'Code';
			$velden[] = 'Transactie';
			$velden[] = 'Gescand';
			break;
		case 'get':
			$velden[] = 'Kaartje';
			$velden[] = 'Code';
			$velden[] = 'Transactie';
			$velden[] = 'Gescand';
		case 'primary':
			$velden[] = 'kaartje_artikelID';
			$velden[] = 'transactie_transactieID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Kaartje';
			$velden[] = 'Code';
			$velden[] = 'Transactie';
			$velden[] = 'Gescand';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `iDealKaartjeCode`'
		          .' WHERE `id` = %i'
		          .' LIMIT 1'
		          , $this->id
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->id = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `iDealKaartjeCode`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `id` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->id
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van iDealKaartjeCode terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'id':
			return 'int';
		case 'kaartje':
			return 'foreign';
		case 'kaartje_artikelid':
			return 'int';
		case 'code':
			return 'string';
		case 'transactie':
			return 'foreign';
		case 'transactie_transactieid':
			return 'int';
		case 'gescand':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'id':
		case 'kaartje_artikelID':
		case 'transactie_transactieID':
		case 'gescand':
			$type = '%i';
			break;
		case 'code':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `iDealKaartjeCode`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `id` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->id
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `iDealKaartjeCode`'
		          .' WHERE `id` = %i'
		                 , $veld
		          , $this->id
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'iDealKaartjeCode');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
