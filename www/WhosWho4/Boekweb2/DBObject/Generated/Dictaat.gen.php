<?
/**
 * @brief 'AT'AUTH_GET:bestuur,AUTH_SET:bestuur
 */
abstract class Dictaat_Generated
	extends Artikel
{
	protected $auteur;
	protected $auteur_contactID;		/**< \brief PRIMARY */
	protected $uitgaveJaar;
	protected $digitaalVerspreiden;
	protected $beginGebruik;
	protected $eindeGebruik;
	protected $copyrightOpmerking;		/**< \brief NULL */
	/**
	 * @brief De constructor van de Dictaat_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Artikel

		$this->artikelID = NULL;
		$this->auteur = NULL;
		$this->auteur_contactID = 0;
		$this->uitgaveJaar = 0;
		$this->digitaalVerspreiden = False;
		$this->beginGebruik = new DateTimeLocale();
		$this->eindeGebruik = new DateTimeLocale();
		$this->copyrightOpmerking = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld auteur.
	 *
	 * @return Persoon
	 * De waarde van het veld auteur.
	 */
	public function getAuteur()
	{
		if(!isset($this->auteur)
		 && isset($this->auteur_contactID)
		 ) {
			$this->auteur = Persoon::geef
					( $this->auteur_contactID
					);
		}
		return $this->auteur;
	}
	/**
	 * @brief Stel de waarde van het veld auteur in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Dictaat
	 * Dit Dictaat-object.
	 */
	public function setAuteur($new_contactID)
	{
		unset($this->errors['Auteur']);
		if($new_contactID instanceof Persoon
		) {
			if($this->auteur == $new_contactID
			&& $this->auteur_contactID == $this->auteur->getContactID())
				return $this;
			$this->auteur = $new_contactID;
			$this->auteur_contactID
					= $this->auteur->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->auteur == NULL 
				&& $this->auteur_contactID == (int)$new_contactID)
				return $this;
			$this->auteur = NULL;
			$this->auteur_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld auteur geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld auteur geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAuteur()
	{
		if (array_key_exists('Auteur', $this->errors))
			return $this->errors['Auteur'];
		$waarde1 = $this->getAuteur();
		$waarde2 = $this->getAuteurContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld auteur_contactID.
	 *
	 * @return int
	 * De waarde van het veld auteur_contactID.
	 */
	public function getAuteurContactID()
	{
		if (is_null($this->auteur_contactID) && isset($this->auteur)) {
			$this->auteur_contactID = $this->auteur->getContactID();
		}
		return $this->auteur_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld uitgaveJaar.
	 *
	 * @return int
	 * De waarde van het veld uitgaveJaar.
	 */
	public function getUitgaveJaar()
	{
		return $this->uitgaveJaar;
	}
	/**
	 * @brief Stel de waarde van het veld uitgaveJaar in.
	 *
	 * @param mixed $newUitgaveJaar De nieuwe waarde.
	 *
	 * @return Dictaat
	 * Dit Dictaat-object.
	 */
	public function setUitgaveJaar($newUitgaveJaar)
	{
		unset($this->errors['UitgaveJaar']);
		if(!is_null($newUitgaveJaar))
			$newUitgaveJaar = (int)$newUitgaveJaar;
		if($this->uitgaveJaar === $newUitgaveJaar)
			return $this;

		$this->uitgaveJaar = $newUitgaveJaar;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld uitgaveJaar geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld uitgaveJaar geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkUitgaveJaar()
	{
		if (array_key_exists('UitgaveJaar', $this->errors))
			return $this->errors['UitgaveJaar'];
		$waarde = $this->getUitgaveJaar();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld digitaalVerspreiden.
	 *
	 * @return bool
	 * De waarde van het veld digitaalVerspreiden.
	 */
	public function getDigitaalVerspreiden()
	{
		return $this->digitaalVerspreiden;
	}
	/**
	 * @brief Stel de waarde van het veld digitaalVerspreiden in.
	 *
	 * @param mixed $newDigitaalVerspreiden De nieuwe waarde.
	 *
	 * @return Dictaat
	 * Dit Dictaat-object.
	 */
	public function setDigitaalVerspreiden($newDigitaalVerspreiden)
	{
		unset($this->errors['DigitaalVerspreiden']);
		if(!is_null($newDigitaalVerspreiden))
			$newDigitaalVerspreiden = (bool)$newDigitaalVerspreiden;
		if($this->digitaalVerspreiden === $newDigitaalVerspreiden)
			return $this;

		$this->digitaalVerspreiden = $newDigitaalVerspreiden;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld digitaalVerspreiden geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld digitaalVerspreiden geldig is; anders
	 * een string met een foutmelding.
	 */
	public function checkDigitaalVerspreiden()
	{
		if (array_key_exists('DigitaalVerspreiden', $this->errors))
			return $this->errors['DigitaalVerspreiden'];
		$waarde = $this->getDigitaalVerspreiden();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld beginGebruik.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld beginGebruik.
	 */
	public function getBeginGebruik()
	{
		return $this->beginGebruik;
	}
	/**
	 * @brief Stel de waarde van het veld beginGebruik in.
	 *
	 * @param mixed $newBeginGebruik De nieuwe waarde.
	 *
	 * @return Dictaat
	 * Dit Dictaat-object.
	 */
	public function setBeginGebruik($newBeginGebruik)
	{
		unset($this->errors['BeginGebruik']);
		if(!$newBeginGebruik instanceof DateTimeLocale) {
			try {
				$newBeginGebruik = new DateTimeLocale($newBeginGebruik);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->beginGebruik->strftime('%F %T') == $newBeginGebruik->strftime('%F %T'))
			return $this;

		$this->beginGebruik = $newBeginGebruik;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld beginGebruik geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld beginGebruik geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBeginGebruik()
	{
		if (array_key_exists('BeginGebruik', $this->errors))
			return $this->errors['BeginGebruik'];
		$waarde = $this->getBeginGebruik();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld eindeGebruik.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld eindeGebruik.
	 */
	public function getEindeGebruik()
	{
		return $this->eindeGebruik;
	}
	/**
	 * @brief Stel de waarde van het veld eindeGebruik in.
	 *
	 * @param mixed $newEindeGebruik De nieuwe waarde.
	 *
	 * @return Dictaat
	 * Dit Dictaat-object.
	 */
	public function setEindeGebruik($newEindeGebruik)
	{
		unset($this->errors['EindeGebruik']);
		if(!$newEindeGebruik instanceof DateTimeLocale) {
			try {
				$newEindeGebruik = new DateTimeLocale($newEindeGebruik);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->eindeGebruik->strftime('%F %T') == $newEindeGebruik->strftime('%F %T'))
			return $this;

		$this->eindeGebruik = $newEindeGebruik;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld eindeGebruik geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld eindeGebruik geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkEindeGebruik()
	{
		if (array_key_exists('EindeGebruik', $this->errors))
			return $this->errors['EindeGebruik'];
		$waarde = $this->getEindeGebruik();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld copyrightOpmerking.
	 *
	 * @return string
	 * De waarde van het veld copyrightOpmerking.
	 */
	public function getCopyrightOpmerking()
	{
		return $this->copyrightOpmerking;
	}
	/**
	 * @brief Stel de waarde van het veld copyrightOpmerking in.
	 *
	 * @param mixed $newCopyrightOpmerking De nieuwe waarde.
	 *
	 * @return Dictaat
	 * Dit Dictaat-object.
	 */
	public function setCopyrightOpmerking($newCopyrightOpmerking)
	{
		unset($this->errors['CopyrightOpmerking']);
		if(!is_null($newCopyrightOpmerking))
			$newCopyrightOpmerking = trim($newCopyrightOpmerking);
		if($newCopyrightOpmerking === "")
			$newCopyrightOpmerking = NULL;
		if($this->copyrightOpmerking === $newCopyrightOpmerking)
			return $this;

		$this->copyrightOpmerking = $newCopyrightOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld copyrightOpmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld copyrightOpmerking geldig is; anders
	 * een string met een foutmelding.
	 */
	public function checkCopyrightOpmerking()
	{
		if (array_key_exists('CopyrightOpmerking', $this->errors))
			return $this->errors['CopyrightOpmerking'];
		$waarde = $this->getCopyrightOpmerking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Dictaat::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Dictaat.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('bestuur');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Dictaat::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getArtikelID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Dictaat|false
	 * Een Dictaat-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$artikelID = (int)$a;
		}

		if(is_null($artikelID))
			throw new BadMethodCallException();

		static::cache(array( array($artikelID) ));
		return Entiteit::geefCache(array($artikelID), 'Dictaat');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return Artikel::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Dictaat::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Dictaat`.`auteur_contactID`'
			                 .     ', `Dictaat`.`uitgaveJaar`'
			                 .     ', `Dictaat`.`digitaalVerspreiden`'
			                 .     ', `Dictaat`.`beginGebruik`'
			                 .     ', `Dictaat`.`eindeGebruik`'
			                 .     ', `Dictaat`.`copyrightOpmerking`'
			                 .     ', `Artikel`.`artikelID`'
			                 .     ', `Artikel`.`naam`'
			                 .     ', `Artikel`.`gewijzigdWanneer`'
			                 .     ', `Artikel`.`gewijzigdWie`'
			                 .' FROM `Dictaat`'
			                 .' LEFT JOIN `Artikel` USING (`artikelID`)'
			                 .' WHERE (`artikelID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['artikelID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Dictaat'][$id];
			} else {
				$obj = new Dictaat();
			}

			$obj->inDB = True;

			$obj->auteur_contactID  = (int) $row['auteur_contactID'];
			$obj->uitgaveJaar  = (int) $row['uitgaveJaar'];
			$obj->digitaalVerspreiden  = (bool) $row['digitaalVerspreiden'];
			$obj->beginGebruik  = new DateTimeLocale($row['beginGebruik']);
			$obj->eindeGebruik  = new DateTimeLocale($row['eindeGebruik']);
			$obj->copyrightOpmerking  = (is_null($row['copyrightOpmerking'])) ? null : trim($row['copyrightOpmerking']);
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			Artikel::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Dictaat')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getAuteurContactID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Dictaat`'
			          . ' (`artikelID`, `auteur_contactID`, `uitgaveJaar`, `digitaalVerspreiden`, `beginGebruik`, `eindeGebruik`, `copyrightOpmerking`)'
			          . ' VALUES (%i, %i, %i, %i, %s, %s, %s)'
			          , $this->artikelID
			          , $this->auteur_contactID
			          , $this->uitgaveJaar
			          , $this->digitaalVerspreiden
			          , $this->beginGebruik->strftime('%F %T')
			          , $this->eindeGebruik->strftime('%F %T')
			          , $this->copyrightOpmerking
			          );

			if($classname == 'Dictaat')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Dictaat`'
			          .' SET `auteur_contactID` = %i'
			          .   ', `uitgaveJaar` = %i'
			          .   ', `digitaalVerspreiden` = %i'
			          .   ', `beginGebruik` = %s'
			          .   ', `eindeGebruik` = %s'
			          .   ', `copyrightOpmerking` = %s'
			          .' WHERE `artikelID` = %i'
			          , $this->auteur_contactID
			          , $this->uitgaveJaar
			          , $this->digitaalVerspreiden
			          , $this->beginGebruik->strftime('%F %T')
			          , $this->eindeGebruik->strftime('%F %T')
			          , $this->copyrightOpmerking
			          , $this->artikelID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenDictaat
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenDictaat($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenDictaat($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Auteur';
			$velden[] = 'UitgaveJaar';
			$velden[] = 'DigitaalVerspreiden';
			$velden[] = 'BeginGebruik';
			$velden[] = 'EindeGebruik';
			$velden[] = 'CopyrightOpmerking';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Auteur';
			$velden[] = 'BeginGebruik';
			$velden[] = 'EindeGebruik';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Auteur';
			$velden[] = 'UitgaveJaar';
			$velden[] = 'DigitaalVerspreiden';
			$velden[] = 'BeginGebruik';
			$velden[] = 'EindeGebruik';
			$velden[] = 'CopyrightOpmerking';
			break;
		case 'get':
			$velden[] = 'Auteur';
			$velden[] = 'UitgaveJaar';
			$velden[] = 'DigitaalVerspreiden';
			$velden[] = 'BeginGebruik';
			$velden[] = 'EindeGebruik';
			$velden[] = 'CopyrightOpmerking';
		case 'primary':
			$velden[] = 'auteur_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Auteur';
			$velden[] = 'UitgaveJaar';
			$velden[] = 'DigitaalVerspreiden';
			$velden[] = 'BeginGebruik';
			$velden[] = 'EindeGebruik';
			$velden[] = 'CopyrightOpmerking';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Dictaat`'
		          .' WHERE `artikelID` = %i'
		          .' LIMIT 1'
		          , $this->artikelID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Dictaat terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'auteur':
			return 'foreign';
		case 'auteur_contactid':
			return 'int';
		case 'uitgavejaar':
			return 'int';
		case 'digitaalverspreiden':
			return 'bool';
		case 'begingebruik':
			return 'date';
		case 'eindegebruik':
			return 'date';
		case 'copyrightopmerking':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'auteur_contactID':
		case 'uitgaveJaar':
		case 'digitaalVerspreiden':
			$type = '%i';
			break;
		case 'beginGebruik':
		case 'eindeGebruik':
		case 'copyrightOpmerking':
			$type = '%s';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Dictaat`'
		          ." SET `%l` = $type"
		          .' WHERE `artikelID` = %i'
		          , $veld
		          , $waarde
		          , $this->artikelID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'auteur':
		case 'auteur_contactID':
		case 'uitgaveJaar':
		case 'digitaalVerspreiden':
		case 'beginGebruik':
		case 'eindeGebruik':
		case 'copyrightOpmerking':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Dictaat`'
		          .' WHERE `artikelID` = %i'
		                 , $veld
		          , $this->artikelID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
