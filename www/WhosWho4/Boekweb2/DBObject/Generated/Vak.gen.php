<?
abstract class Vak_Generated
	extends Entiteit
{
	protected $vakID;					/**< \brief PRIMARY */
	protected $code;
	protected $naam;					/**< \brief NULL */
	protected $departement;				/**< \brief ENUM:na/wi/ic/ov */
	protected $opmerking;				/**< \brief NULL */
	protected $inTentamenLijsten;
	protected $tentamensOutdated;
	/** Verzamelingen **/
	protected $jaargangVerzameling;
	protected $tentamenVerzameling;
	protected $vakStudieVerzameling;
	/**
	 * @brief De constructor van de Vak_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->vakID = NULL;
		$this->code = '';
		$this->naam = NULL;
		$this->departement = 'NA';
		$this->opmerking = NULL;
		$this->inTentamenLijsten = True;
		$this->tentamensOutdated = False;
		$this->jaargangVerzameling = NULL;
		$this->tentamenVerzameling = NULL;
		$this->vakStudieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld vakID.
	 *
	 * @return int
	 * De waarde van het veld vakID.
	 */
	public function getVakID()
	{
		return $this->vakID;
	}
	/**
	 * @brief Geef de waarde van het veld code.
	 *
	 * @return string
	 * De waarde van het veld code.
	 */
	public function getCode()
	{
		return $this->code;
	}
	/**
	 * @brief Stel de waarde van het veld code in.
	 *
	 * @param mixed $newCode De nieuwe waarde.
	 *
	 * @return Vak
	 * Dit Vak-object.
	 */
	public function setCode($newCode)
	{
		unset($this->errors['Code']);
		if(!is_null($newCode))
			$newCode = trim($newCode);
		if($newCode === "")
			$newCode = NULL;
		if($this->code === $newCode)
			return $this;

		$this->code = $newCode;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld code geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld code geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkCode()
	{
		if (array_key_exists('Code', $this->errors))
			return $this->errors['Code'];
		$waarde = $this->getCode();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Vak
	 * Dit Vak-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld departement.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld departement.
	 */
	static public function enumsDepartement()
	{
		static $vals = array('NA','WI','IC','OV');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld departement.
	 *
	 * @return string
	 * De waarde van het veld departement.
	 */
	public function getDepartement()
	{
		return $this->departement;
	}
	/**
	 * @brief Stel de waarde van het veld departement in.
	 *
	 * @param mixed $newDepartement De nieuwe waarde.
	 *
	 * @return Vak
	 * Dit Vak-object.
	 */
	public function setDepartement($newDepartement)
	{
		unset($this->errors['Departement']);
		if(!is_null($newDepartement))
			$newDepartement = strtoupper(trim($newDepartement));
		if($newDepartement === "")
			$newDepartement = NULL;
		if($this->departement === $newDepartement)
			return $this;

		$this->departement = $newDepartement;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld departement geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld departement geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDepartement()
	{
		if (array_key_exists('Departement', $this->errors))
			return $this->errors['Departement'];
		$waarde = $this->getDepartement();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsDepartement()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return Vak
	 * Dit Vak-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld inTentamenLijsten.
	 *
	 * @return bool
	 * De waarde van het veld inTentamenLijsten.
	 */
	public function getInTentamenLijsten()
	{
		return $this->inTentamenLijsten;
	}
	/**
	 * @brief Stel de waarde van het veld inTentamenLijsten in.
	 *
	 * @param mixed $newInTentamenLijsten De nieuwe waarde.
	 *
	 * @return Vak
	 * Dit Vak-object.
	 */
	public function setInTentamenLijsten($newInTentamenLijsten)
	{
		unset($this->errors['InTentamenLijsten']);
		if(!is_null($newInTentamenLijsten))
			$newInTentamenLijsten = (bool)$newInTentamenLijsten;
		if($this->inTentamenLijsten === $newInTentamenLijsten)
			return $this;

		$this->inTentamenLijsten = $newInTentamenLijsten;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld inTentamenLijsten geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld inTentamenLijsten geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkInTentamenLijsten()
	{
		if (array_key_exists('InTentamenLijsten', $this->errors))
			return $this->errors['InTentamenLijsten'];
		$waarde = $this->getInTentamenLijsten();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tentamensOutdated.
	 *
	 * @return bool
	 * De waarde van het veld tentamensOutdated.
	 */
	public function getTentamensOutdated()
	{
		return $this->tentamensOutdated;
	}
	/**
	 * @brief Stel de waarde van het veld tentamensOutdated in.
	 *
	 * @param mixed $newTentamensOutdated De nieuwe waarde.
	 *
	 * @return Vak
	 * Dit Vak-object.
	 */
	public function setTentamensOutdated($newTentamensOutdated)
	{
		unset($this->errors['TentamensOutdated']);
		if(!is_null($newTentamensOutdated))
			$newTentamensOutdated = (bool)$newTentamensOutdated;
		if($this->tentamensOutdated === $newTentamensOutdated)
			return $this;

		$this->tentamensOutdated = $newTentamensOutdated;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tentamensOutdated geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tentamensOutdated geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkTentamensOutdated()
	{
		if (array_key_exists('TentamensOutdated', $this->errors))
			return $this->errors['TentamensOutdated'];
		$waarde = $this->getTentamensOutdated();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de JaargangVerzameling die hoort bij dit object.
	 */
	public function getJaargangVerzameling()
	{
		if(!$this->jaargangVerzameling instanceof JaargangVerzameling)
			$this->jaargangVerzameling = JaargangVerzameling::fromVak($this);
		return $this->jaargangVerzameling;
	}
	/**
	 * @brief Returneert de TentamenVerzameling die hoort bij dit object.
	 */
	public function getTentamenVerzameling()
	{
		if(!$this->tentamenVerzameling instanceof TentamenVerzameling)
			$this->tentamenVerzameling = TentamenVerzameling::fromVak($this);
		return $this->tentamenVerzameling;
	}
	/**
	 * @brief Returneert de VakStudieVerzameling die hoort bij dit object.
	 */
	public function getVakStudieVerzameling()
	{
		if(!$this->vakStudieVerzameling instanceof VakStudieVerzameling)
			$this->vakStudieVerzameling = VakStudieVerzameling::fromVak($this);
		return $this->vakStudieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Vak::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Vak.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Vak::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getVakID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Vak|false
	 * Een Vak-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$vakID = (int)$a[0];
		}
		else if(isset($a))
		{
			$vakID = (int)$a;
		}

		if(is_null($vakID))
			throw new BadMethodCallException();

		static::cache(array( array($vakID) ));
		return Entiteit::geefCache(array($vakID), 'Vak');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Vak');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Vak::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Vak`.`vakID`'
		                 .     ', `Vak`.`code`'
		                 .     ', `Vak`.`naam`'
		                 .     ', `Vak`.`departement`'
		                 .     ', `Vak`.`opmerking`'
		                 .     ', `Vak`.`inTentamenLijsten`'
		                 .     ', `Vak`.`tentamensOutdated`'
		                 .     ', `Vak`.`gewijzigdWanneer`'
		                 .     ', `Vak`.`gewijzigdWie`'
		                 .' FROM `Vak`'
		                 .' WHERE (`vakID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['vakID']);

			$obj = new Vak();

			$obj->inDB = True;

			$obj->vakID  = (int) $row['vakID'];
			$obj->code  = trim($row['code']);
			$obj->naam  = (is_null($row['naam'])) ? null : trim($row['naam']);
			$obj->departement  = strtoupper(trim($row['departement']));
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->inTentamenLijsten  = (bool) $row['inTentamenLijsten'];
			$obj->tentamensOutdated  = (bool) $row['tentamensOutdated'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Vak')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->vakID =
			$WSW4DB->q('RETURNID INSERT INTO `Vak`'
			          . ' (`code`, `naam`, `departement`, `opmerking`, `inTentamenLijsten`, `tentamensOutdated`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %s, %s, %s, %i, %i, %s, %i)'
			          , $this->code
			          , $this->naam
			          , $this->departement
			          , $this->opmerking
			          , $this->inTentamenLijsten
			          , $this->tentamensOutdated
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Vak')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Vak`'
			          .' SET `code` = %s'
			          .   ', `naam` = %s'
			          .   ', `departement` = %s'
			          .   ', `opmerking` = %s'
			          .   ', `inTentamenLijsten` = %i'
			          .   ', `tentamensOutdated` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `vakID` = %i'
			          , $this->code
			          , $this->naam
			          , $this->departement
			          , $this->opmerking
			          , $this->inTentamenLijsten
			          , $this->tentamensOutdated
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->vakID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVak
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVak($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVak($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Code';
			$velden[] = 'Naam';
			$velden[] = 'Departement';
			$velden[] = 'Opmerking';
			$velden[] = 'InTentamenLijsten';
			$velden[] = 'TentamensOutdated';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Code';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Code';
			$velden[] = 'Naam';
			$velden[] = 'Departement';
			$velden[] = 'Opmerking';
			$velden[] = 'InTentamenLijsten';
			$velden[] = 'TentamensOutdated';
			break;
		case 'get':
			$velden[] = 'Code';
			$velden[] = 'Naam';
			$velden[] = 'Departement';
			$velden[] = 'Opmerking';
			$velden[] = 'InTentamenLijsten';
			$velden[] = 'TentamensOutdated';
		case 'primary':
			break;
		case 'verzamelingen':
			$velden[] = 'JaargangVerzameling';
			$velden[] = 'TentamenVerzameling';
			$velden[] = 'VakStudieVerzameling';
			break;
		default:
			$velden[] = 'Code';
			$velden[] = 'Naam';
			$velden[] = 'Departement';
			$velden[] = 'Opmerking';
			$velden[] = 'InTentamenLijsten';
			$velden[] = 'TentamensOutdated';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Jaargang-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$JaargangFromVakVerz = JaargangVerzameling::fromVak($this);
		$returnValue = $JaargangFromVakVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Jaargang met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Tentamen-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TentamenFromVakVerz = TentamenVerzameling::fromVak($this);
		$returnValue = $TentamenFromVakVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tentamen met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle VakStudie-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$VakStudieFromVakVerz = VakStudieVerzameling::fromVak($this);
		$returnValue = $VakStudieFromVakVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object VakStudie met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $JaargangFromVakVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $TentamenFromVakVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $VakStudieFromVakVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Vak`'
		          .' WHERE `vakID` = %i'
		          .' LIMIT 1'
		          , $this->vakID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->vakID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Vak`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `vakID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->vakID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Vak terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'vakid':
			return 'int';
		case 'code':
			return 'string';
		case 'naam':
			return 'string';
		case 'departement':
			return 'enum';
		case 'opmerking':
			return 'string';
		case 'intentamenlijsten':
			return 'bool';
		case 'tentamensoutdated':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'vakID':
		case 'inTentamenLijsten':
		case 'tentamensOutdated':
			$type = '%i';
			break;
		case 'code':
		case 'naam':
		case 'departement':
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Vak`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `vakID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->vakID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Vak`'
		          .' WHERE `vakID` = %i'
		                 , $veld
		          , $this->vakID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Vak');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Jaargang-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$JaargangFromVakVerz = JaargangVerzameling::fromVak($this);
		$dependencies['Jaargang'] = $JaargangFromVakVerz;

		// Verzamel alle Tentamen-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TentamenFromVakVerz = TentamenVerzameling::fromVak($this);
		$dependencies['Tentamen'] = $TentamenFromVakVerz;

		// Verzamel alle VakStudie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$VakStudieFromVakVerz = VakStudieVerzameling::fromVak($this);
		$dependencies['VakStudie'] = $VakStudieFromVakVerz;

		return $dependencies;
	}
}
