<?
abstract class Bestelling_Generated
	extends Entiteit
{
	protected $bestellingID;			/**< \brief PRIMARY */
	protected $persoon;
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $artikel;
	protected $artikel_artikelID;		/**< \brief PRIMARY */
	protected $datumGeplaatst;
	protected $status;					/**< \brief ENUM:open/gereserveerd/gesloten/verlopen */
	protected $aantal;
	protected $vervalDatum;
	/**
	/**
	 * @brief De constructor van de Bestelling_Generated-klasse.
	 *
	 * @param mixed $a Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 * @param mixed $b Artikel (Artikel OR Array(artikel_artikelID) OR
	 * artikel_artikelID)
	 * @param mixed $c DatumGeplaatst (date)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Persoon)
		{
			$this->persoon = $a;
			$this->persoon_contactID = $a->getContactID();
		}
		else if(is_array($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Artikel)
		{
			$this->artikel = $b;
			$this->artikel_artikelID = $b->getArtikelID();
		}
		else if(is_array($b))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(!$c instanceof DateTimeLocale)
		{
			if(is_null($c))
				$c = new DateTimeLocale();
			else
			{
				try {
					$c = new DateTimeLocale($c);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->datumGeplaatst = $c;

		$this->bestellingID = NULL;
		$this->status = 'open';
		$this->aantal = 0;
		$this->vervalDatum = new DateTimeLocale();
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Persoon';
		$volgorde[] = 'Artikel';
		$volgorde[] = 'DatumGeplaatst';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld bestellingID.
	 *
	 * @return int
	 * De waarde van het veld bestellingID.
	 */
	public function getBestellingID()
	{
		return $this->bestellingID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @return Artikel
	 * De waarde van het veld artikel.
	 */
	public function getArtikel()
	{
		if(!isset($this->artikel)
		 && isset($this->artikel_artikelID)
		 ) {
			$this->artikel = Artikel::geef
					( $this->artikel_artikelID
					);
		}
		return $this->artikel;
	}
	/**
	 * @brief Geef de waarde van het veld artikel_artikelID.
	 *
	 * @return int
	 * De waarde van het veld artikel_artikelID.
	 */
	public function getArtikelArtikelID()
	{
		if (is_null($this->artikel_artikelID) && isset($this->artikel)) {
			$this->artikel_artikelID = $this->artikel->getArtikelID();
		}
		return $this->artikel_artikelID;
	}
	/**
	 * @brief Geef de waarde van het veld datumGeplaatst.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumGeplaatst.
	 */
	public function getDatumGeplaatst()
	{
		return $this->datumGeplaatst;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('OPEN','GERESERVEERD','GESLOTEN','VERLOPEN');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return Bestelling
	 * Dit Bestelling-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @return int
	 * De waarde van het veld aantal.
	 */
	public function getAantal()
	{
		return $this->aantal;
	}
	/**
	 * @brief Stel de waarde van het veld aantal in.
	 *
	 * @param mixed $newAantal De nieuwe waarde.
	 *
	 * @return Bestelling
	 * Dit Bestelling-object.
	 */
	public function setAantal($newAantal)
	{
		unset($this->errors['Aantal']);
		if(!is_null($newAantal))
			$newAantal = (int)$newAantal;
		if($this->aantal === $newAantal)
			return $this;

		$this->aantal = $newAantal;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aantal geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aantal geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAantal()
	{
		if (array_key_exists('Aantal', $this->errors))
			return $this->errors['Aantal'];
		$waarde = $this->getAantal();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld vervalDatum.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld vervalDatum.
	 */
	public function getVervalDatum()
	{
		return $this->vervalDatum;
	}
	/**
	 * @brief Stel de waarde van het veld vervalDatum in.
	 *
	 * @param mixed $newVervalDatum De nieuwe waarde.
	 *
	 * @return Bestelling
	 * Dit Bestelling-object.
	 */
	public function setVervalDatum($newVervalDatum)
	{
		unset($this->errors['VervalDatum']);
		if(!$newVervalDatum instanceof DateTimeLocale) {
			try {
				$newVervalDatum = new DateTimeLocale($newVervalDatum);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->vervalDatum->strftime('%F %T') == $newVervalDatum->strftime('%F %T'))
			return $this;

		$this->vervalDatum = $newVervalDatum;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld vervalDatum geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld vervalDatum geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkVervalDatum()
	{
		if (array_key_exists('VervalDatum', $this->errors))
			return $this->errors['VervalDatum'];
		$waarde = $this->getVervalDatum();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Bestelling::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Bestelling.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Bestelling::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getBestellingID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Bestelling|false
	 * Een Bestelling-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$bestellingID = (int)$a[0];
		}
		else if(isset($a))
		{
			$bestellingID = (int)$a;
		}

		if(is_null($bestellingID))
			throw new BadMethodCallException();

		static::cache(array( array($bestellingID) ));
		return Entiteit::geefCache(array($bestellingID), 'Bestelling');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Bestelling');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Bestelling::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Bestelling`.`bestellingID`'
		                 .     ', `Bestelling`.`persoon_contactID`'
		                 .     ', `Bestelling`.`artikel_artikelID`'
		                 .     ', `Bestelling`.`datumGeplaatst`'
		                 .     ', `Bestelling`.`status`'
		                 .     ', `Bestelling`.`aantal`'
		                 .     ', `Bestelling`.`vervalDatum`'
		                 .     ', `Bestelling`.`gewijzigdWanneer`'
		                 .     ', `Bestelling`.`gewijzigdWie`'
		                 .' FROM `Bestelling`'
		                 .' WHERE (`bestellingID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['bestellingID']);

			$obj = new Bestelling(array($row['persoon_contactID']), array($row['artikel_artikelID']), $row['datumGeplaatst']);

			$obj->inDB = True;

			$obj->bestellingID  = (int) $row['bestellingID'];
			$obj->status  = strtoupper(trim($row['status']));
			$obj->aantal  = (int) $row['aantal'];
			$obj->vervalDatum  = new DateTimeLocale($row['vervalDatum']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Bestelling')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getPersoonContactID();
		$this->getArtikelArtikelID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->bestellingID =
			$WSW4DB->q('RETURNID INSERT INTO `Bestelling`'
			          . ' (`persoon_contactID`, `artikel_artikelID`, `datumGeplaatst`, `status`, `aantal`, `vervalDatum`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i, %s, %s, %i)'
			          , $this->persoon_contactID
			          , $this->artikel_artikelID
			          , $this->datumGeplaatst->strftime('%F %T')
			          , $this->status
			          , $this->aantal
			          , $this->vervalDatum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Bestelling')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Bestelling`'
			          .' SET `persoon_contactID` = %i'
			          .   ', `artikel_artikelID` = %i'
			          .   ', `datumGeplaatst` = %s'
			          .   ', `status` = %s'
			          .   ', `aantal` = %i'
			          .   ', `vervalDatum` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bestellingID` = %i'
			          , $this->persoon_contactID
			          , $this->artikel_artikelID
			          , $this->datumGeplaatst->strftime('%F %T')
			          , $this->status
			          , $this->aantal
			          , $this->vervalDatum->strftime('%F %T')
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bestellingID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBestelling
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBestelling($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBestelling($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Status';
			$velden[] = 'Aantal';
			$velden[] = 'VervalDatum';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'VervalDatum';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Persoon';
			$velden[] = 'Artikel';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Status';
			$velden[] = 'Aantal';
			$velden[] = 'VervalDatum';
			break;
		case 'get':
			$velden[] = 'Persoon';
			$velden[] = 'Artikel';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Status';
			$velden[] = 'Aantal';
			$velden[] = 'VervalDatum';
		case 'primary':
			$velden[] = 'persoon_contactID';
			$velden[] = 'artikel_artikelID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Persoon';
			$velden[] = 'Artikel';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Status';
			$velden[] = 'Aantal';
			$velden[] = 'VervalDatum';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Bestelling`'
		          .' WHERE `bestellingID` = %i'
		          .' LIMIT 1'
		          , $this->bestellingID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->bestellingID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Bestelling`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `bestellingID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->bestellingID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Bestelling terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'bestellingid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'artikel':
			return 'foreign';
		case 'artikel_artikelid':
			return 'int';
		case 'datumgeplaatst':
			return 'date';
		case 'status':
			return 'enum';
		case 'aantal':
			return 'int';
		case 'vervaldatum':
			return 'date';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'bestellingID':
		case 'persoon_contactID':
		case 'artikel_artikelID':
		case 'aantal':
			$type = '%i';
			break;
		case 'datumGeplaatst':
		case 'status':
		case 'vervalDatum':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Bestelling`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `bestellingID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->bestellingID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Bestelling`'
		          .' WHERE `bestellingID` = %i'
		                 , $veld
		          , $this->bestellingID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Bestelling');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
