<?
abstract class Order_Generated
	extends Entiteit
{
	protected $orderID;					/**< \brief PRIMARY */
	protected $artikel;
	protected $artikel_artikelID;		/**< \brief PRIMARY */
	protected $leverancier;
	protected $leverancier_contactID;	/**< \brief PRIMARY */
	protected $waardePerStuk;			/**< \brief NULL */
	protected $datumGeplaatst;
	protected $geannuleerd;
	protected $aantal;
	/** Verzamelingen **/
	protected $leveringVerzameling;
	protected $leverancierRetourVerzameling;
	/**
	/**
	 * @brief De constructor van de Order_Generated-klasse.
	 *
	 * @param mixed $a Artikel (Artikel OR Array(artikel_artikelID) OR
	 * artikel_artikelID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Artikel)
		{
			$this->artikel = $a;
			$this->artikel_artikelID = $a->getArtikelID();
		}
		else if(is_array($a))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		$this->orderID = NULL;
		$this->leverancier = NULL;
		$this->leverancier_contactID = 0;
		$this->waardePerStuk = NULL;
		$this->datumGeplaatst = new DateTimeLocale();
		$this->geannuleerd = False;
		$this->aantal = 0;
		$this->leveringVerzameling = NULL;
		$this->leverancierRetourVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Artikel';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld orderID.
	 *
	 * @return int
	 * De waarde van het veld orderID.
	 */
	public function getOrderID()
	{
		return $this->orderID;
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @return Artikel
	 * De waarde van het veld artikel.
	 */
	public function getArtikel()
	{
		if(!isset($this->artikel)
		 && isset($this->artikel_artikelID)
		 ) {
			$this->artikel = Artikel::geef
					( $this->artikel_artikelID
					);
		}
		return $this->artikel;
	}
	/**
	 * @brief Geef de waarde van het veld artikel_artikelID.
	 *
	 * @return int
	 * De waarde van het veld artikel_artikelID.
	 */
	public function getArtikelArtikelID()
	{
		if (is_null($this->artikel_artikelID) && isset($this->artikel)) {
			$this->artikel_artikelID = $this->artikel->getArtikelID();
		}
		return $this->artikel_artikelID;
	}
	/**
	 * @brief Geef de waarde van het veld leverancier.
	 *
	 * @return Leverancier
	 * De waarde van het veld leverancier.
	 */
	public function getLeverancier()
	{
		if(!isset($this->leverancier)
		 && isset($this->leverancier_contactID)
		 ) {
			$this->leverancier = Leverancier::geef
					( $this->leverancier_contactID
					);
		}
		return $this->leverancier;
	}
	/**
	 * @brief Stel de waarde van het veld leverancier in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Order
	 * Dit Order-object.
	 */
	public function setLeverancier($new_contactID)
	{
		unset($this->errors['Leverancier']);
		if($new_contactID instanceof Leverancier
		) {
			if($this->leverancier == $new_contactID
			&& $this->leverancier_contactID == $this->leverancier->getContactID())
				return $this;
			$this->leverancier = $new_contactID;
			$this->leverancier_contactID
					= $this->leverancier->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->leverancier == NULL 
				&& $this->leverancier_contactID == (int)$new_contactID)
				return $this;
			$this->leverancier = NULL;
			$this->leverancier_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld leverancier geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld leverancier geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkLeverancier()
	{
		if (array_key_exists('Leverancier', $this->errors))
			return $this->errors['Leverancier'];
		$waarde1 = $this->getLeverancier();
		$waarde2 = $this->getLeverancierContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld leverancier_contactID.
	 *
	 * @return int
	 * De waarde van het veld leverancier_contactID.
	 */
	public function getLeverancierContactID()
	{
		if (is_null($this->leverancier_contactID) && isset($this->leverancier)) {
			$this->leverancier_contactID = $this->leverancier->getContactID();
		}
		return $this->leverancier_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld waardePerStuk.
	 *
	 * @return float
	 * De waarde van het veld waardePerStuk.
	 */
	public function getWaardePerStuk()
	{
		return $this->waardePerStuk;
	}
	/**
	 * @brief Stel de waarde van het veld waardePerStuk in.
	 *
	 * @param mixed $newWaardePerStuk De nieuwe waarde.
	 *
	 * @return Order
	 * Dit Order-object.
	 */
	public function setWaardePerStuk($newWaardePerStuk)
	{
		unset($this->errors['WaardePerStuk']);
		if(!is_null($newWaardePerStuk))
			$newWaardePerStuk = (float)$newWaardePerStuk;
		if($this->waardePerStuk === $newWaardePerStuk)
			return $this;

		$this->waardePerStuk = $newWaardePerStuk;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld waardePerStuk geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld waardePerStuk geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkWaardePerStuk()
	{
		if (array_key_exists('WaardePerStuk', $this->errors))
			return $this->errors['WaardePerStuk'];
		$waarde = $this->getWaardePerStuk();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld datumGeplaatst.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld datumGeplaatst.
	 */
	public function getDatumGeplaatst()
	{
		return $this->datumGeplaatst;
	}
	/**
	 * @brief Stel de waarde van het veld datumGeplaatst in.
	 *
	 * @param mixed $newDatumGeplaatst De nieuwe waarde.
	 *
	 * @return Order
	 * Dit Order-object.
	 */
	public function setDatumGeplaatst($newDatumGeplaatst)
	{
		unset($this->errors['DatumGeplaatst']);
		if(!$newDatumGeplaatst instanceof DateTimeLocale) {
			try {
				$newDatumGeplaatst = new DateTimeLocale($newDatumGeplaatst);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->datumGeplaatst->strftime('%F %T') == $newDatumGeplaatst->strftime('%F %T'))
			return $this;

		$this->datumGeplaatst = $newDatumGeplaatst;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld datumGeplaatst geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld datumGeplaatst geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkDatumGeplaatst()
	{
		if (array_key_exists('DatumGeplaatst', $this->errors))
			return $this->errors['DatumGeplaatst'];
		$waarde = $this->getDatumGeplaatst();
		if (!$waarde->hasTime())
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld geannuleerd.
	 *
	 * @return bool
	 * De waarde van het veld geannuleerd.
	 */
	public function getGeannuleerd()
	{
		return $this->geannuleerd;
	}
	/**
	 * @brief Stel de waarde van het veld geannuleerd in.
	 *
	 * @param mixed $newGeannuleerd De nieuwe waarde.
	 *
	 * @return Order
	 * Dit Order-object.
	 */
	public function setGeannuleerd($newGeannuleerd)
	{
		unset($this->errors['Geannuleerd']);
		if(!is_null($newGeannuleerd))
			$newGeannuleerd = (bool)$newGeannuleerd;
		if($this->geannuleerd === $newGeannuleerd)
			return $this;

		$this->geannuleerd = $newGeannuleerd;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld geannuleerd geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld geannuleerd geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkGeannuleerd()
	{
		if (array_key_exists('Geannuleerd', $this->errors))
			return $this->errors['Geannuleerd'];
		$waarde = $this->getGeannuleerd();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @return int
	 * De waarde van het veld aantal.
	 */
	public function getAantal()
	{
		return $this->aantal;
	}
	/**
	 * @brief Stel de waarde van het veld aantal in.
	 *
	 * @param mixed $newAantal De nieuwe waarde.
	 *
	 * @return Order
	 * Dit Order-object.
	 */
	public function setAantal($newAantal)
	{
		unset($this->errors['Aantal']);
		if(!is_null($newAantal))
			$newAantal = (int)$newAantal;
		if($this->aantal === $newAantal)
			return $this;

		$this->aantal = $newAantal;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld aantal geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld aantal geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkAantal()
	{
		if (array_key_exists('Aantal', $this->errors))
			return $this->errors['Aantal'];
		$waarde = $this->getAantal();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de LeveringVerzameling die hoort bij dit object.
	 */
	public function getLeveringVerzameling()
	{
		if(!$this->leveringVerzameling instanceof LeveringVerzameling)
			$this->leveringVerzameling = LeveringVerzameling::fromOrder($this);
		return $this->leveringVerzameling;
	}
	/**
	 * @brief Returneert de LeverancierRetourVerzameling die hoort bij dit object.
	 */
	public function getLeverancierRetourVerzameling()
	{
		if(!$this->leverancierRetourVerzameling instanceof LeverancierRetourVerzameling)
			$this->leverancierRetourVerzameling = LeverancierRetourVerzameling::fromOrder($this);
		return $this->leverancierRetourVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Order::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Order.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array('DictaatOrder');
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Order::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getOrderID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Order|false
	 * Een Order-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$orderID = (int)$a[0];
		}
		else if(isset($a))
		{
			$orderID = (int)$a;
		}

		if(is_null($orderID))
			throw new BadMethodCallException();

		static::cache(array( array($orderID) ));
		return Entiteit::geefCache(array($orderID), 'Order');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			$ids = Entiteit::nietInCache($ids, 'Order');

			// is er nog iets te doen?
			if(empty($ids)) return;

			// zoek uit wat de klasse van een id is
			$res = $WSW4DB->q('TABLE SELECT `orderID`'
			                 .     ', `overerving`'
			                 .' FROM `Order`'
			                 .' WHERE (`orderID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );

			$recur = array();
			foreach($res as $row)
			{
				$recur[$row['overerving']][]
				    = array($row['orderID']);
			}
			foreach($recur as $class => $obj)
			{
				$class::cache($obj, True);
			}

			return;
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Order::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Order`.`orderID`'
			                 .     ', `Order`.`artikel_artikelID`'
			                 .     ', `Order`.`leverancier_contactID`'
			                 .     ', `Order`.`waardePerStuk`'
			                 .     ', `Order`.`datumGeplaatst`'
			                 .     ', `Order`.`geannuleerd`'
			                 .     ', `Order`.`aantal`'
			                 .     ', `Order`.`gewijzigdWanneer`'
			                 .     ', `Order`.`gewijzigdWie`'
			                 .' FROM `Order`'
			                 .' WHERE (`orderID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['orderID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Order'][$id];
			} else {
				$obj = new Order(array($row['artikel_artikelID']));
			}

			$obj->inDB = True;

			$obj->orderID  = (int) $row['orderID'];
			$obj->leverancier_contactID  = (int) $row['leverancier_contactID'];
			$obj->waardePerStuk  = (is_null($row['waardePerStuk'])) ? null : (float) $row['waardePerStuk'];
			$obj->datumGeplaatst  = new DateTimeLocale($row['datumGeplaatst']);
			$obj->geannuleerd  = (bool) $row['geannuleerd'];
			$obj->aantal  = (int) $row['aantal'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			if($recur === True)
				self::stopInCache($id, $obj);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Order')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getArtikelArtikelID();
		$this->getLeverancierContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->orderID =
			$WSW4DB->q('RETURNID INSERT INTO `Order`'
			          . ' (`overerving`, `artikel_artikelID`, `leverancier_contactID`, `waardePerStuk`, `datumGeplaatst`, `geannuleerd`, `aantal`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %i, %f, %s, %i, %i, %s, %i)'
			          , $classname
			          , $this->artikel_artikelID
			          , $this->leverancier_contactID
			          , $this->waardePerStuk
			          , $this->datumGeplaatst->strftime('%F %T')
			          , $this->geannuleerd
			          , $this->aantal
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Order')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Order`'
			          .' SET `artikel_artikelID` = %i'
			          .   ', `leverancier_contactID` = %i'
			          .   ', `waardePerStuk` = %f'
			          .   ', `datumGeplaatst` = %s'
			          .   ', `geannuleerd` = %i'
			          .   ', `aantal` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `orderID` = %i'
			          , $this->artikel_artikelID
			          , $this->leverancier_contactID
			          , $this->waardePerStuk
			          , $this->datumGeplaatst->strftime('%F %T')
			          , $this->geannuleerd
			          , $this->aantal
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->orderID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenOrder
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenOrder($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenOrder($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Geannuleerd';
			$velden[] = 'Aantal';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Leverancier';
			$velden[] = 'DatumGeplaatst';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Artikel';
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Geannuleerd';
			$velden[] = 'Aantal';
			break;
		case 'get':
			$velden[] = 'Artikel';
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Geannuleerd';
			$velden[] = 'Aantal';
		case 'primary':
			$velden[] = 'artikel_artikelID';
			$velden[] = 'leverancier_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'LeveringVerzameling';
			$velden[] = 'LeverancierRetourVerzameling';
			break;
		default:
			$velden[] = 'Artikel';
			$velden[] = 'Leverancier';
			$velden[] = 'WaardePerStuk';
			$velden[] = 'DatumGeplaatst';
			$velden[] = 'Geannuleerd';
			$velden[] = 'Aantal';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Levering-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$LeveringFromOrderVerz = LeveringVerzameling::fromOrder($this);
		$returnValue = $LeveringFromOrderVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Levering met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle LeverancierRetour-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$LeverancierRetourFromOrderVerz = LeverancierRetourVerzameling::fromOrder($this);
		$returnValue = $LeverancierRetourFromOrderVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object LeverancierRetour met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($LeveringFromOrderVerz as $v)
		{
			$v->setOrder(NULL);
		}
		$LeveringFromOrderVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $LeverancierRetourFromOrderVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Order`'
		          .' WHERE `orderID` = %i'
		          .' LIMIT 1'
		          , $this->orderID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->orderID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Order`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `orderID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->orderID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Order terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'orderid':
			return 'int';
		case 'artikel':
			return 'foreign';
		case 'artikel_artikelid':
			return 'int';
		case 'leverancier':
			return 'foreign';
		case 'leverancier_contactid':
			return 'int';
		case 'waardeperstuk':
			return 'money';
		case 'datumgeplaatst':
			return 'date';
		case 'geannuleerd':
			return 'bool';
		case 'aantal':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'waardePerStuk':
			$type = '%f';
			break;
		case 'orderID':
		case 'artikel_artikelID':
		case 'leverancier_contactID':
		case 'geannuleerd':
		case 'aantal':
			$type = '%i';
			break;
		case 'datumGeplaatst':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Order`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `orderID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->orderID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Order`'
		          .' WHERE `orderID` = %i'
		                 , $veld
		          , $this->orderID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Order');
		parent::stopInCache($id, $obj, 'DictaatOrder');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Levering-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LeveringFromOrderVerz = LeveringVerzameling::fromOrder($this);
		$dependencies['Levering'] = $LeveringFromOrderVerz;

		// Verzamel alle LeverancierRetour-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$LeverancierRetourFromOrderVerz = LeverancierRetourVerzameling::fromOrder($this);
		$dependencies['LeverancierRetour'] = $LeverancierRetourFromOrderVerz;

		return $dependencies;
	}
}
