<?
abstract class JaargangArtikel_Generated
	extends Entiteit
{
	protected $jaargang;				/**< \brief PRIMARY */
	protected $jaargang_jaargangID;		/**< \brief PRIMARY */
	protected $artikel;					/**< \brief PRIMARY */
	protected $artikel_artikelID;		/**< \brief PRIMARY */
	protected $schattingNodig;			/**< \brief NULL */
	protected $opmerking;				/**< \brief NULL */
	protected $verplicht;
	/**
	/**
	 * @brief De constructor van de JaargangArtikel_Generated-klasse.
	 *
	 * @param mixed $a Jaargang (Jaargang OR Array(jaargang_jaargangID) OR
	 * jaargang_jaargangID)
	 * @param mixed $b Artikel (Artikel OR Array(artikel_artikelID) OR
	 * artikel_artikelID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Jaargang)
		{
			$this->jaargang = $a;
			$this->jaargang_jaargangID = $a->getJaargangID();
		}
		else if(is_array($a))
		{
			$this->jaargang = NULL;
			$this->jaargang_jaargangID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->jaargang = NULL;
			$this->jaargang_jaargangID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Artikel)
		{
			$this->artikel = $b;
			$this->artikel_artikelID = $b->getArtikelID();
		}
		else if(is_array($b))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->artikel = NULL;
			$this->artikel_artikelID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->schattingNodig = NULL;
		$this->opmerking = NULL;
		$this->verplicht = True;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Jaargang';
		$volgorde[] = 'Artikel';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld jaargang.
	 *
	 * @return Jaargang
	 * De waarde van het veld jaargang.
	 */
	public function getJaargang()
	{
		if(!isset($this->jaargang)
		 && isset($this->jaargang_jaargangID)
		 ) {
			$this->jaargang = Jaargang::geef
					( $this->jaargang_jaargangID
					);
		}
		return $this->jaargang;
	}
	/**
	 * @brief Geef de waarde van het veld jaargang_jaargangID.
	 *
	 * @return int
	 * De waarde van het veld jaargang_jaargangID.
	 */
	public function getJaargangJaargangID()
	{
		if (is_null($this->jaargang_jaargangID) && isset($this->jaargang)) {
			$this->jaargang_jaargangID = $this->jaargang->getJaargangID();
		}
		return $this->jaargang_jaargangID;
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @return Artikel
	 * De waarde van het veld artikel.
	 */
	public function getArtikel()
	{
		if(!isset($this->artikel)
		 && isset($this->artikel_artikelID)
		 ) {
			$this->artikel = Artikel::geef
					( $this->artikel_artikelID
					);
		}
		return $this->artikel;
	}
	/**
	 * @brief Geef de waarde van het veld artikel_artikelID.
	 *
	 * @return int
	 * De waarde van het veld artikel_artikelID.
	 */
	public function getArtikelArtikelID()
	{
		if (is_null($this->artikel_artikelID) && isset($this->artikel)) {
			$this->artikel_artikelID = $this->artikel->getArtikelID();
		}
		return $this->artikel_artikelID;
	}
	/**
	 * @brief Geef de waarde van het veld schattingNodig.
	 *
	 * @return int
	 * De waarde van het veld schattingNodig.
	 */
	public function getSchattingNodig()
	{
		return $this->schattingNodig;
	}
	/**
	 * @brief Stel de waarde van het veld schattingNodig in.
	 *
	 * @param mixed $newSchattingNodig De nieuwe waarde.
	 *
	 * @return JaargangArtikel
	 * Dit JaargangArtikel-object.
	 */
	public function setSchattingNodig($newSchattingNodig)
	{
		unset($this->errors['SchattingNodig']);
		if(!is_null($newSchattingNodig))
			$newSchattingNodig = (int)$newSchattingNodig;
		if($this->schattingNodig === $newSchattingNodig)
			return $this;

		$this->schattingNodig = $newSchattingNodig;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld schattingNodig geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld schattingNodig geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkSchattingNodig()
	{
		if (array_key_exists('SchattingNodig', $this->errors))
			return $this->errors['SchattingNodig'];
		$waarde = $this->getSchattingNodig();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return JaargangArtikel
	 * Dit JaargangArtikel-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld verplicht.
	 *
	 * @return bool
	 * De waarde van het veld verplicht.
	 */
	public function getVerplicht()
	{
		return $this->verplicht;
	}
	/**
	 * @brief Stel de waarde van het veld verplicht in.
	 *
	 * @param mixed $newVerplicht De nieuwe waarde.
	 *
	 * @return JaargangArtikel
	 * Dit JaargangArtikel-object.
	 */
	public function setVerplicht($newVerplicht)
	{
		unset($this->errors['Verplicht']);
		if(!is_null($newVerplicht))
			$newVerplicht = (bool)$newVerplicht;
		if($this->verplicht === $newVerplicht)
			return $this;

		$this->verplicht = $newVerplicht;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld verplicht geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld verplicht geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkVerplicht()
	{
		if (array_key_exists('Verplicht', $this->errors))
			return $this->errors['Verplicht'];
		$waarde = $this->getVerplicht();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return JaargangArtikel::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van JaargangArtikel.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return JaargangArtikel::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getJaargangJaargangID()
		                      , $this->getArtikelArtikelID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return JaargangArtikel|false
	 * Een JaargangArtikel-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$jaargang_jaargangID = (int)$a[0];
			$artikel_artikelID = (int)$a[1];
		}
		else if($a instanceof Jaargang
		     && $b instanceof Artikel)
		{
			$jaargang_jaargangID = $a->getJaargangID();
			$artikel_artikelID = $b->getArtikelID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$jaargang_jaargangID = (int)$a;
			$artikel_artikelID = (int)$b;
		}

		if(is_null($jaargang_jaargangID)
		|| is_null($artikel_artikelID))
			throw new BadMethodCallException();

		static::cache(array( array($jaargang_jaargangID, $artikel_artikelID) ));
		return Entiteit::geefCache(array($jaargang_jaargangID, $artikel_artikelID), 'JaargangArtikel');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'JaargangArtikel');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('JaargangArtikel::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `JaargangArtikel`.`jaargang_jaargangID`'
		                 .     ', `JaargangArtikel`.`artikel_artikelID`'
		                 .     ', `JaargangArtikel`.`schattingNodig`'
		                 .     ', `JaargangArtikel`.`opmerking`'
		                 .     ', `JaargangArtikel`.`verplicht`'
		                 .     ', `JaargangArtikel`.`gewijzigdWanneer`'
		                 .     ', `JaargangArtikel`.`gewijzigdWie`'
		                 .' FROM `JaargangArtikel`'
		                 .' WHERE (`jaargang_jaargangID`, `artikel_artikelID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['jaargang_jaargangID']
			                  ,$row['artikel_artikelID']);

			$obj = new JaargangArtikel(array($row['jaargang_jaargangID']), array($row['artikel_artikelID']));

			$obj->inDB = True;

			$obj->schattingNodig  = (is_null($row['schattingNodig'])) ? null : (int) $row['schattingNodig'];
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->verplicht  = (bool) $row['verplicht'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'JaargangArtikel')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getJaargang();
		if(!$rel->getInDB())
			throw new LogicException('foreign Jaargang is not in DB');
		$this->jaargang_jaargangID = $rel->getJaargangID();

		$rel = $this->getArtikel();
		if(!$rel->getInDB())
			throw new LogicException('foreign Artikel is not in DB');
		$this->artikel_artikelID = $rel->getArtikelID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `JaargangArtikel`'
			          . ' (`jaargang_jaargangID`, `artikel_artikelID`, `schattingNodig`, `opmerking`, `verplicht`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %s, %i, %s, %i)'
			          , $this->jaargang_jaargangID
			          , $this->artikel_artikelID
			          , $this->schattingNodig
			          , $this->opmerking
			          , $this->verplicht
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'JaargangArtikel')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `JaargangArtikel`'
			          .' SET `schattingNodig` = %i'
			          .   ', `opmerking` = %s'
			          .   ', `verplicht` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `jaargang_jaargangID` = %i'
			          .  ' AND `artikel_artikelID` = %i'
			          , $this->schattingNodig
			          , $this->opmerking
			          , $this->verplicht
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->jaargang_jaargangID
			          , $this->artikel_artikelID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenJaargangArtikel
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenJaargangArtikel($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenJaargangArtikel($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'SchattingNodig';
			$velden[] = 'Opmerking';
			$velden[] = 'Verplicht';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'SchattingNodig';
			$velden[] = 'Opmerking';
			$velden[] = 'Verplicht';
			break;
		case 'get':
			$velden[] = 'SchattingNodig';
			$velden[] = 'Opmerking';
			$velden[] = 'Verplicht';
		case 'primary':
			$velden[] = 'jaargang_jaargangID';
			$velden[] = 'artikel_artikelID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'SchattingNodig';
			$velden[] = 'Opmerking';
			$velden[] = 'Verplicht';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `JaargangArtikel`'
		          .' WHERE `jaargang_jaargangID` = %i'
		          .  ' AND `artikel_artikelID` = %i'
		          .' LIMIT 1'
		          , $this->jaargang_jaargangID
		          , $this->artikel_artikelID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->jaargang = NULL;
		$this->jaargang_jaargangID = NULL;
		$this->artikel = NULL;
		$this->artikel_artikelID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `JaargangArtikel`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `jaargang_jaargangID` = %i'
			          .  ' AND `artikel_artikelID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->jaargang_jaargangID
			          , $this->artikel_artikelID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van JaargangArtikel terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'jaargang':
			return 'foreign';
		case 'jaargang_jaargangid':
			return 'int';
		case 'artikel':
			return 'foreign';
		case 'artikel_artikelid':
			return 'int';
		case 'schattingnodig':
			return 'int';
		case 'opmerking':
			return 'text';
		case 'verplicht':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'jaargang_jaargangID':
		case 'artikel_artikelID':
		case 'schattingNodig':
		case 'verplicht':
			$type = '%i';
			break;
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `JaargangArtikel`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `jaargang_jaargangID` = %i'
		          .  ' AND `artikel_artikelID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->jaargang_jaargangID
		          , $this->artikel_artikelID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `JaargangArtikel`'
		          .' WHERE `jaargang_jaargangID` = %i'
		          .  ' AND `artikel_artikelID` = %i'
		                 , $veld
		          , $this->jaargang_jaargangID
		          , $this->artikel_artikelID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'JaargangArtikel');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
