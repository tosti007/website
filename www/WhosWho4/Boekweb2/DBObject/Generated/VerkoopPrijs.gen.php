<?
abstract class VerkoopPrijs_Generated
	extends Entiteit
{
	protected $voorraad;				/**< \brief PRIMARY */
	protected $voorraad_voorraadID;		/**< \brief PRIMARY */
	protected $wanneer;					/**< \brief PRIMARY */
	protected $prijs;
	/**
	/**
	 * @brief De constructor van de VerkoopPrijs_Generated-klasse.
	 *
	 * @param mixed $a Voorraad (Voorraad OR Array(voorraad_voorraadID) OR
	 * voorraad_voorraadID)
	 * @param mixed $b Wanneer (datetime)
	 * @param mixed $c Prijs (money)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = 0)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Voorraad)
		{
			$this->voorraad = $a;
			$this->voorraad_voorraadID = $a->getVoorraadID();
		}
		else if(is_array($a))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->voorraad = NULL;
			$this->voorraad_voorraadID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(!$b instanceof DateTimeLocale)
		{
			if(is_null($b))
				$b = new DateTimeLocale();
			else
			{
				try {
					$b = new DateTimeLocale($b);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->wanneer = $b;

		$c = (float)$c;
		$this->prijs = $c;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Voorraad';
		$volgorde[] = 'Wanneer';
		$volgorde[] = 'Prijs';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @return Voorraad
	 * De waarde van het veld voorraad.
	 */
	public function getVoorraad()
	{
		if(!isset($this->voorraad)
		 && isset($this->voorraad_voorraadID)
		 ) {
			$this->voorraad = Voorraad::geef
					( $this->voorraad_voorraadID
					);
		}
		return $this->voorraad;
	}
	/**
	 * @brief Geef de waarde van het veld voorraad_voorraadID.
	 *
	 * @return int
	 * De waarde van het veld voorraad_voorraadID.
	 */
	public function getVoorraadVoorraadID()
	{
		if (is_null($this->voorraad_voorraadID) && isset($this->voorraad)) {
			$this->voorraad_voorraadID = $this->voorraad->getVoorraadID();
		}
		return $this->voorraad_voorraadID;
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld wanneer.
	 */
	public function getWanneer()
	{
		return $this->wanneer;
	}
	/**
	 * @brief Geef de waarde van het veld prijs.
	 *
	 * @return float
	 * De waarde van het veld prijs.
	 */
	public function getPrijs()
	{
		return $this->prijs;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return VerkoopPrijs::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van VerkoopPrijs.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return VerkoopPrijs::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getVoorraadVoorraadID()
		                      , $this->getWanneer()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return VerkoopPrijs|false
	 * Een VerkoopPrijs-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$voorraad_voorraadID = (int)$a[0];
			$wanneer = $a[1];
		}
		else if($a instanceof Voorraad
		     && isset($b))
		{
			$voorraad_voorraadID = $a->getVoorraadID();
			$wanneer = $b;
		}
		else if(isset($a)
		     && isset($b))
		{
			$voorraad_voorraadID = (int)$a;
			$wanneer = $b;
		}

		if(is_null($voorraad_voorraadID)
		|| is_null($wanneer))
			throw new BadMethodCallException();

		static::cache(array( array($voorraad_voorraadID, $wanneer) ));
		return Entiteit::geefCache(array($voorraad_voorraadID, $wanneer), 'VerkoopPrijs');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'VerkoopPrijs');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('VerkoopPrijs::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `VerkoopPrijs`.`voorraad_voorraadID`'
		                 .     ', `VerkoopPrijs`.`wanneer`'
		                 .     ', `VerkoopPrijs`.`prijs`'
		                 .     ', `VerkoopPrijs`.`gewijzigdWanneer`'
		                 .     ', `VerkoopPrijs`.`gewijzigdWie`'
		                 .' FROM `VerkoopPrijs`'
		                 .' WHERE (`voorraad_voorraadID`, `wanneer`)'
		                 .      ' IN (%A{is})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['voorraad_voorraadID']
			                  ,$row['wanneer']);

			$obj = new VerkoopPrijs(array($row['voorraad_voorraadID']), $row['wanneer'], $row['prijs']);

			$obj->inDB = True;

			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'VerkoopPrijs')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getVoorraad();
		if(!$rel->getInDB())
			throw new LogicException('foreign Voorraad is not in DB');
		$this->voorraad_voorraadID = $rel->getVoorraadID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `VerkoopPrijs`'
			          . ' (`voorraad_voorraadID`, `wanneer`, `prijs`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %f, %s, %i)'
			          , $this->voorraad_voorraadID
			          , $this->wanneer->strftime('%F %T')
			          , $this->prijs
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'VerkoopPrijs')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `VerkoopPrijs`'
			          .' SET `prijs` = %f'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `voorraad_voorraadID` = %i'
			          .  ' AND `wanneer` = %s'
			          , $this->prijs
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->voorraad_voorraadID
			          , $this->wanneer
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenVerkoopPrijs
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenVerkoopPrijs($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenVerkoopPrijs($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Prijs';
			break;
		case 'get':
			$velden[] = 'Prijs';
		case 'primary':
			$velden[] = 'voorraad_voorraadID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Prijs';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `VerkoopPrijs`'
		          .' WHERE `voorraad_voorraadID` = %i'
		          .  ' AND `wanneer` = %s'
		          .' LIMIT 1'
		          , $this->voorraad_voorraadID
		          , $this->wanneer
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->voorraad = NULL;
		$this->voorraad_voorraadID = NULL;
		$this->wanneer = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `VerkoopPrijs`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `voorraad_voorraadID` = %i'
			          .  ' AND `wanneer` = %s'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->voorraad_voorraadID
			          , $this->wanneer
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van VerkoopPrijs terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'voorraad':
			return 'foreign';
		case 'voorraad_voorraadid':
			return 'int';
		case 'wanneer':
			return 'datetime';
		case 'prijs':
			return 'money';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'prijs':
			$type = '%f';
			break;
		case 'voorraad_voorraadID':
			$type = '%i';
			break;
		case 'wanneer':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `VerkoopPrijs`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `voorraad_voorraadID` = %i'
		          .  ' AND `wanneer` = %s'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->voorraad_voorraadID
		          , $this->wanneer
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `VerkoopPrijs`'
		          .' WHERE `voorraad_voorraadID` = %i'
		          .  ' AND `wanneer` = %s'
		                 , $veld
		          , $this->voorraad_voorraadID
		          , $this->wanneer
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'VerkoopPrijs');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
