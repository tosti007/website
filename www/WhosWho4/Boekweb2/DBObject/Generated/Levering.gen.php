<?
abstract class Levering_Generated
	extends VoorraadMutatie
{
	protected $order;					/**< \brief NULL */
	protected $order_orderID;			/**< \brief PRIMARY */
	protected $leverancier;
	protected $leverancier_contactID;	/**< \brief PRIMARY */
	/**
	/**
	 * @brief De constructor van de Levering_Generated-klasse.
	 *
	 * @param mixed $a Voorraad (Voorraad OR Array(voorraad_voorraadID) OR
	 * voorraad_voorraadID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct($a); // VoorraadMutatie

		$this->voorraadMutatieID = NULL;
		$this->order = NULL;
		$this->order_orderID = NULL;
		$this->leverancier = NULL;
		$this->leverancier_contactID = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Voorraad';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld order.
	 *
	 * @return Order
	 * De waarde van het veld order.
	 */
	public function getOrder()
	{
		if(!isset($this->order)
		 && isset($this->order_orderID)
		 ) {
			$this->order = Order::geef
					( $this->order_orderID
					);
		}
		return $this->order;
	}
	/**
	 * @brief Stel de waarde van het veld order in.
	 *
	 * @param mixed $new_orderID De nieuwe waarde.
	 *
	 * @return Levering
	 * Dit Levering-object.
	 */
	public function setOrder($new_orderID)
	{
		unset($this->errors['Order']);
		if($new_orderID instanceof Order
		) {
			if($this->order == $new_orderID
			&& $this->order_orderID == $this->order->getOrderID())
				return $this;
			$this->order = $new_orderID;
			$this->order_orderID
					= $this->order->getOrderID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_orderID) || $new_orderID == 0)) {
			if($this->order == NULL && $this->order_orderID == NULL)
				return $this;
			$this->order = NULL;
			$this->order_orderID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_orderID)
		) {
			if($this->order == NULL 
				&& $this->order_orderID == (int)$new_orderID)
				return $this;
			$this->order = NULL;
			$this->order_orderID
					= (int)$new_orderID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld order geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld order geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkOrder()
	{
		if (array_key_exists('Order', $this->errors))
			return $this->errors['Order'];
		$waarde1 = $this->getOrder();
		$waarde2 = $this->getOrderOrderID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld order_orderID.
	 *
	 * @return int
	 * De waarde van het veld order_orderID.
	 */
	public function getOrderOrderID()
	{
		if (is_null($this->order_orderID) && isset($this->order)) {
			$this->order_orderID = $this->order->getOrderID();
		}
		return $this->order_orderID;
	}
	/**
	 * @brief Geef de waarde van het veld leverancier.
	 *
	 * @return Leverancier
	 * De waarde van het veld leverancier.
	 */
	public function getLeverancier()
	{
		if(!isset($this->leverancier)
		 && isset($this->leverancier_contactID)
		 ) {
			$this->leverancier = Leverancier::geef
					( $this->leverancier_contactID
					);
		}
		return $this->leverancier;
	}
	/**
	 * @brief Stel de waarde van het veld leverancier in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Levering
	 * Dit Levering-object.
	 */
	public function setLeverancier($new_contactID)
	{
		unset($this->errors['Leverancier']);
		if($new_contactID instanceof Leverancier
		) {
			if($this->leverancier == $new_contactID
			&& $this->leverancier_contactID == $this->leverancier->getContactID())
				return $this;
			$this->leverancier = $new_contactID;
			$this->leverancier_contactID
					= $this->leverancier->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->leverancier == NULL 
				&& $this->leverancier_contactID == (int)$new_contactID)
				return $this;
			$this->leverancier = NULL;
			$this->leverancier_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld leverancier geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld leverancier geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkLeverancier()
	{
		if (array_key_exists('Leverancier', $this->errors))
			return $this->errors['Leverancier'];
		$waarde1 = $this->getLeverancier();
		$waarde2 = $this->getLeverancierContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld leverancier_contactID.
	 *
	 * @return int
	 * De waarde van het veld leverancier_contactID.
	 */
	public function getLeverancierContactID()
	{
		if (is_null($this->leverancier_contactID) && isset($this->leverancier)) {
			$this->leverancier_contactID = $this->leverancier->getContactID();
		}
		return $this->leverancier_contactID;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Levering::magKlasseBekijken() || parent::magBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Levering.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return false;
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Levering::magKlasseWijzigen() || parent::magWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getVoorraadMutatieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Levering|false
	 * Een Levering-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$voorraadMutatieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$voorraadMutatieID = (int)$a;
		}

		if(is_null($voorraadMutatieID))
			throw new BadMethodCallException();

		static::cache(array( array($voorraadMutatieID) ));
		return Entiteit::geefCache(array($voorraadMutatieID), 'Levering');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		if($recur === False)
		{
			// de basis klasse gaat het allemaal fixen
			return VoorraadMutatie::cache($ids);
		}

		if(!is_array($recur))
		{
			$cachetm = new ProfilerTimerMark('Levering::cache( #'.count($ids).' )');
			Profiler::getSingleton()->addTimerMark($cachetm);

			// query alles in 1x
			$res = $WSW4DB->q('TABLE SELECT `Levering`.`order_orderID`'
			                 .     ', `Levering`.`leverancier_contactID`'
			                 .     ', `VoorraadMutatie`.`voorraadMutatieID`'
			                 .     ', `VoorraadMutatie`.`voorraad_voorraadID`'
			                 .     ', `VoorraadMutatie`.`wanneer`'
			                 .     ', `VoorraadMutatie`.`aantal`'
			                 .     ', `VoorraadMutatie`.`gewijzigdWanneer`'
			                 .     ', `VoorraadMutatie`.`gewijzigdWie`'
			                 .' FROM `Levering`'
			                 .' LEFT JOIN `VoorraadMutatie` USING (`voorraadMutatieID`)'
			                 .' WHERE (`voorraadMutatieID`)'
			                 .      ' IN (%A{i})'
			                 , $ids
			                 );
		}
		else
		{
			$res = array($recur);
		}

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['voorraadMutatieID']);

			if(is_array($recur)) { // dit is een recursieve invulstap
				$obj = static::$objcache['Levering'][$id];
			} else {
				$obj = new Levering(array($row['voorraad_voorraadID']));
			}

			$obj->inDB = True;

			$obj->order_orderID  = (is_null($row['order_orderID'])) ? null : (int) $row['order_orderID'];
			$obj->leverancier_contactID  = (int) $row['leverancier_contactID'];
			if($recur === True)
				self::stopInCache($id, $obj);

			// naar de super klasse de andere velden
			VoorraadMutatie::cache(array(), $row);

			if(is_array($recur))
				return; // dit was een recursieve invul stap

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Levering')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$isDirty = $this->dirty;
		parent::opslaan($classname);

		$stillDirty = $this->dirty;

		// Controleer of het object niet meer dirty is gemaakt door de parent::opslaan
		if ($isDirty == $stillDirty && !$this->dirty)
			return;
		$this->getOrderOrderID();
		$this->getLeverancierContactID();

		$this->gewijzigd();
		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Levering`'
			          . ' (`voorraadMutatieID`, `order_orderID`, `leverancier_contactID`)'
			          . ' VALUES (%i, %i, %i)'
			          , $this->voorraadMutatieID
			          , $this->order_orderID
			          , $this->leverancier_contactID
			          );

			if($classname == 'Levering')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Levering`'
			          .' SET `order_orderID` = %i'
			          .   ', `leverancier_contactID` = %i'
			          .' WHERE `voorraadMutatieID` = %i'
			          , $this->order_orderID
			          , $this->leverancier_contactID
			          , $this->voorraadMutatieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenLevering
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenLevering($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenLevering($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Order';
			$velden[] = 'Leverancier';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Leverancier';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Order';
			$velden[] = 'Leverancier';
			break;
		case 'get':
			$velden[] = 'Order';
			$velden[] = 'Leverancier';
		case 'primary':
			$velden[] = 'order_orderID';
			$velden[] = 'leverancier_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Order';
			$velden[] = 'Leverancier';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Levering`'
		          .' WHERE `voorraadMutatieID` = %i'
		          .' LIMIT 1'
		          , $this->voorraadMutatieID
		          );

		$queryarray[] = parent::verwijderen(true);

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd($updatedb);
	}
	/**
	 * @brief Geeft van een veld van Levering terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'order':
			return 'foreign';
		case 'order_orderid':
			return 'int';
		case 'leverancier':
			return 'foreign';
		case 'leverancier_contactid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'order_orderID':
		case 'leverancier_contactID':
			$type = '%i';
			break;
		default:
			return parent::naarDB($veld, $waarde, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Levering`'
		          ." SET `%l` = %i"
		          .' WHERE `voorraadMutatieID` = %i'
		          , $veld
		          , $waarde
		          , $this->voorraadMutatieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'order':
		case 'order_orderID':
		case 'leverancier':
		case 'leverancier_contactID':
			throw new BadMethodCallException("veld '$veld' is niet LAZY");
		default:
			return parent::uitDB($veld, $lang);
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Levering`'
		          .' WHERE `voorraadMutatieID` = %i'
		                 , $veld
		          , $this->voorraadMutatieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj);
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
