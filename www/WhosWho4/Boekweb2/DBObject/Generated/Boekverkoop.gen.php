<?
abstract class Boekverkoop_Generated
	extends Entiteit
{
	protected $verkoopdag;				/**< \brief PRIMARY */
	protected $volgnummer;				/**< \brief PRIMARY */
	protected $opener;
	protected $opener_contactID;		/**< \brief PRIMARY */
	protected $open;
	protected $sluiter;					/**< \brief NULL */
	protected $sluiter_contactID;		/**< \brief PRIMARY */
	protected $pin1;
	protected $pin2;
	protected $kasverschil;
	protected $gesloten;				/**< \brief NULL */
	protected $opmerking;				/**< \brief NULL */
	/**
	/**
	 * @brief De constructor van de Boekverkoop_Generated-klasse.
	 *
	 * @param mixed $a Verkoopdag (date)
	 * @param mixed $b Opener (Lid OR Array(lid_contactID) OR lid_contactID)
	 * @param mixed $c Open (datetime)
	 */
	public function __construct($a = NULL,
			$b = NULL,
			$c = NULL)
	{
		parent::__construct(); // Entiteit

		if(!$a instanceof DateTimeLocale)
		{
			if(is_null($a))
				$a = new DateTimeLocale();
			else
			{
				try {
					$a = new DateTimeLocale($a);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->verkoopdag = $a;

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Lid)
		{
			$this->opener = $b;
			$this->opener_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->opener = NULL;
			$this->opener_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->opener = NULL;
			$this->opener_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(!$c instanceof DateTimeLocale)
		{
			if(is_null($c))
				$c = new DateTimeLocale();
			else
			{
				try {
					$c = new DateTimeLocale($c);
				} catch(Exception $e) {
					throw new BadMethodCallException();
				}
			}
		}
		$this->open = $c;

		$this->volgnummer = NULL;
		$this->sluiter = NULL;
		$this->sluiter_contactID = NULL;
		$this->pin1 = 0;
		$this->pin2 = 0;
		$this->kasverschil = 0;
		$this->gesloten = new DateTimeLocale(NULL);
		$this->opmerking = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Verkoopdag';
		$volgorde[] = 'Opener';
		$volgorde[] = 'Open';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld verkoopdag.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld verkoopdag.
	 */
	public function getVerkoopdag()
	{
		return $this->verkoopdag;
	}
	/**
	 * @brief Geef de waarde van het veld volgnummer.
	 *
	 * @return int
	 * De waarde van het veld volgnummer.
	 */
	public function getVolgnummer()
	{
		return $this->volgnummer;
	}
	/**
	 * @brief Geef de waarde van het veld opener.
	 *
	 * @return Lid
	 * De waarde van het veld opener.
	 */
	public function getOpener()
	{
		if(!isset($this->opener)
		 && isset($this->opener_contactID)
		 ) {
			$this->opener = Lid::geef
					( $this->opener_contactID
					);
		}
		return $this->opener;
	}
	/**
	 * @brief Geef de waarde van het veld opener_contactID.
	 *
	 * @return int
	 * De waarde van het veld opener_contactID.
	 */
	public function getOpenerContactID()
	{
		if (is_null($this->opener_contactID) && isset($this->opener)) {
			$this->opener_contactID = $this->opener->getContactID();
		}
		return $this->opener_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld open.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld open.
	 */
	public function getOpen()
	{
		return $this->open;
	}
	/**
	 * @brief Geef de waarde van het veld sluiter.
	 *
	 * @return Lid
	 * De waarde van het veld sluiter.
	 */
	public function getSluiter()
	{
		if(!isset($this->sluiter)
		 && isset($this->sluiter_contactID)
		 ) {
			$this->sluiter = Lid::geef
					( $this->sluiter_contactID
					);
		}
		return $this->sluiter;
	}
	/**
	 * @brief Stel de waarde van het veld sluiter in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Boekverkoop
	 * Dit Boekverkoop-object.
	 */
	public function setSluiter($new_contactID)
	{
		unset($this->errors['Sluiter']);
		if($new_contactID instanceof Lid
		) {
			if($this->sluiter == $new_contactID
			&& $this->sluiter_contactID == $this->sluiter->getContactID())
				return $this;
			$this->sluiter = $new_contactID;
			$this->sluiter_contactID
					= $this->sluiter->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->sluiter == NULL && $this->sluiter_contactID == NULL)
				return $this;
			$this->sluiter = NULL;
			$this->sluiter_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->sluiter == NULL 
				&& $this->sluiter_contactID == (int)$new_contactID)
				return $this;
			$this->sluiter = NULL;
			$this->sluiter_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld sluiter geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld sluiter geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkSluiter()
	{
		if (array_key_exists('Sluiter', $this->errors))
			return $this->errors['Sluiter'];
		$waarde1 = $this->getSluiter();
		$waarde2 = $this->getSluiterContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld sluiter_contactID.
	 *
	 * @return int
	 * De waarde van het veld sluiter_contactID.
	 */
	public function getSluiterContactID()
	{
		if (is_null($this->sluiter_contactID) && isset($this->sluiter)) {
			$this->sluiter_contactID = $this->sluiter->getContactID();
		}
		return $this->sluiter_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld pin1.
	 *
	 * @return float
	 * De waarde van het veld pin1.
	 */
	public function getPin1()
	{
		return $this->pin1;
	}
	/**
	 * @brief Stel de waarde van het veld pin1 in.
	 *
	 * @param mixed $newPin1 De nieuwe waarde.
	 *
	 * @return Boekverkoop
	 * Dit Boekverkoop-object.
	 */
	public function setPin1($newPin1)
	{
		unset($this->errors['Pin1']);
		if(!is_null($newPin1))
			$newPin1 = (float)$newPin1;
		if($this->pin1 === $newPin1)
			return $this;

		$this->pin1 = $newPin1;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld pin1 geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld pin1 geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkPin1()
	{
		if (array_key_exists('Pin1', $this->errors))
			return $this->errors['Pin1'];
		$waarde = $this->getPin1();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld pin2.
	 *
	 * @return float
	 * De waarde van het veld pin2.
	 */
	public function getPin2()
	{
		return $this->pin2;
	}
	/**
	 * @brief Stel de waarde van het veld pin2 in.
	 *
	 * @param mixed $newPin2 De nieuwe waarde.
	 *
	 * @return Boekverkoop
	 * Dit Boekverkoop-object.
	 */
	public function setPin2($newPin2)
	{
		unset($this->errors['Pin2']);
		if(!is_null($newPin2))
			$newPin2 = (float)$newPin2;
		if($this->pin2 === $newPin2)
			return $this;

		$this->pin2 = $newPin2;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld pin2 geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld pin2 geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkPin2()
	{
		if (array_key_exists('Pin2', $this->errors))
			return $this->errors['Pin2'];
		$waarde = $this->getPin2();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld kasverschil.
	 *
	 * @return float
	 * De waarde van het veld kasverschil.
	 */
	public function getKasverschil()
	{
		return $this->kasverschil;
	}
	/**
	 * @brief Stel de waarde van het veld kasverschil in.
	 *
	 * @param mixed $newKasverschil De nieuwe waarde.
	 *
	 * @return Boekverkoop
	 * Dit Boekverkoop-object.
	 */
	public function setKasverschil($newKasverschil)
	{
		unset($this->errors['Kasverschil']);
		if(!is_null($newKasverschil))
			$newKasverschil = (float)$newKasverschil;
		if($this->kasverschil === $newKasverschil)
			return $this;

		$this->kasverschil = $newKasverschil;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld kasverschil geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld kasverschil geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkKasverschil()
	{
		if (array_key_exists('Kasverschil', $this->errors))
			return $this->errors['Kasverschil'];
		$waarde = $this->getKasverschil();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld gesloten.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld gesloten.
	 */
	public function getGesloten()
	{
		return $this->gesloten;
	}
	/**
	 * @brief Stel de waarde van het veld gesloten in.
	 *
	 * @param mixed $newGesloten De nieuwe waarde.
	 *
	 * @return Boekverkoop
	 * Dit Boekverkoop-object.
	 */
	public function setGesloten($newGesloten)
	{
		unset($this->errors['Gesloten']);
		if(!$newGesloten instanceof DateTimeLocale) {
			try {
				$newGesloten = new DateTimeLocale($newGesloten);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->gesloten->strftime('%F %T') == $newGesloten->strftime('%F %T'))
			return $this;

		$this->gesloten = $newGesloten;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld gesloten geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld gesloten geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkGesloten()
	{
		if (array_key_exists('Gesloten', $this->errors))
			return $this->errors['Gesloten'];
		$waarde = $this->getGesloten();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @return string
	 * De waarde van het veld opmerking.
	 */
	public function getOpmerking()
	{
		return $this->opmerking;
	}
	/**
	 * @brief Stel de waarde van het veld opmerking in.
	 *
	 * @param mixed $newOpmerking De nieuwe waarde.
	 *
	 * @return Boekverkoop
	 * Dit Boekverkoop-object.
	 */
	public function setOpmerking($newOpmerking)
	{
		unset($this->errors['Opmerking']);
		if(!is_null($newOpmerking))
			$newOpmerking = trim($newOpmerking);
		if($newOpmerking === "")
			$newOpmerking = NULL;
		if($this->opmerking === $newOpmerking)
			return $this;

		$this->opmerking = $newOpmerking;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld opmerking geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld opmerking geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkOpmerking()
	{
		if (array_key_exists('Opmerking', $this->errors))
			return $this->errors['Opmerking'];
		$waarde = $this->getOpmerking();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Boekverkoop::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Boekverkoop.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Boekverkoop::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getVerkoopdag()
		                      , $this->getVolgnummer()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return Boekverkoop|false
	 * Een Boekverkoop-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$verkoopdag = $a[0];
			$volgnummer = $a[1];
		}
		else if(isset($a)
		     && isset($b))
		{
			$verkoopdag = $a;
			$volgnummer = $b;
		}

		if(is_null($verkoopdag)
		|| is_null($volgnummer))
			throw new BadMethodCallException();

		static::cache(array( array($verkoopdag, $volgnummer) ));
		return Entiteit::geefCache(array($verkoopdag, $volgnummer), 'Boekverkoop');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Boekverkoop');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Boekverkoop::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Boekverkoop`.`verkoopdag`'
		                 .     ', `Boekverkoop`.`volgnummer`'
		                 .     ', `Boekverkoop`.`opener_contactID`'
		                 .     ', `Boekverkoop`.`open`'
		                 .     ', `Boekverkoop`.`sluiter_contactID`'
		                 .     ', `Boekverkoop`.`pin1`'
		                 .     ', `Boekverkoop`.`pin2`'
		                 .     ', `Boekverkoop`.`kasverschil`'
		                 .     ', `Boekverkoop`.`gesloten`'
		                 .     ', `Boekverkoop`.`opmerking`'
		                 .     ', `Boekverkoop`.`gewijzigdWanneer`'
		                 .     ', `Boekverkoop`.`gewijzigdWie`'
		                 .' FROM `Boekverkoop`'
		                 .' WHERE (`verkoopdag`, `volgnummer`)'
		                 .      ' IN (%A{si})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['verkoopdag']
			                  ,$row['volgnummer']);

			$obj = new Boekverkoop($row['verkoopdag'], array($row['opener_contactID']), $row['open']);

			$obj->inDB = True;

			$obj->volgnummer  = (int) $row['volgnummer'];
			$obj->sluiter_contactID  = (is_null($row['sluiter_contactID'])) ? null : (int) $row['sluiter_contactID'];
			$obj->pin1  = (float) $row['pin1'];
			$obj->pin2  = (float) $row['pin2'];
			$obj->kasverschil  = (float) $row['kasverschil'];
			$obj->gesloten  = new DateTimeLocale($row['gesloten']);
			$obj->opmerking  = (is_null($row['opmerking'])) ? null : trim($row['opmerking']);
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Boekverkoop')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getOpenerContactID();
		$this->getSluiterContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Boekverkoop`'
			          . ' (`verkoopdag`, `volgnummer`, `opener_contactID`, `open`, `sluiter_contactID`, `pin1`, `pin2`, `kasverschil`, `gesloten`, `opmerking`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%s, %i, %i, %s, %i, %f, %f, %f, %s, %s, %s, %i)'
			          , $this->verkoopdag->strftime('%F %T')
			          , $this->volgnummer
			          , $this->opener_contactID
			          , $this->open->strftime('%F %T')
			          , $this->sluiter_contactID
			          , $this->pin1
			          , $this->pin2
			          , $this->kasverschil
			          , (!is_null($this->gesloten))?$this->gesloten->strftime('%F %T'):null
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Boekverkoop')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Boekverkoop`'
			          .' SET `opener_contactID` = %i'
			          .   ', `open` = %s'
			          .   ', `sluiter_contactID` = %i'
			          .   ', `pin1` = %f'
			          .   ', `pin2` = %f'
			          .   ', `kasverschil` = %f'
			          .   ', `gesloten` = %s'
			          .   ', `opmerking` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `verkoopdag` = %s'
			          .  ' AND `volgnummer` = %i'
			          , $this->opener_contactID
			          , $this->open->strftime('%F %T')
			          , $this->sluiter_contactID
			          , $this->pin1
			          , $this->pin2
			          , $this->kasverschil
			          , (!is_null($this->gesloten))?$this->gesloten->strftime('%F %T'):null
			          , $this->opmerking
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->verkoopdag
			          , $this->volgnummer
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenBoekverkoop
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenBoekverkoop($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenBoekverkoop($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Sluiter';
			$velden[] = 'Pin1';
			$velden[] = 'Pin2';
			$velden[] = 'Kasverschil';
			$velden[] = 'Gesloten';
			$velden[] = 'Opmerking';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Pin1';
			$velden[] = 'Pin2';
			$velden[] = 'Kasverschil';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Opener';
			$velden[] = 'Open';
			$velden[] = 'Sluiter';
			$velden[] = 'Pin1';
			$velden[] = 'Pin2';
			$velden[] = 'Kasverschil';
			$velden[] = 'Gesloten';
			$velden[] = 'Opmerking';
			break;
		case 'get':
			$velden[] = 'Opener';
			$velden[] = 'Open';
			$velden[] = 'Sluiter';
			$velden[] = 'Pin1';
			$velden[] = 'Pin2';
			$velden[] = 'Kasverschil';
			$velden[] = 'Gesloten';
			$velden[] = 'Opmerking';
		case 'primary':
			$velden[] = 'opener_contactID';
			$velden[] = 'sluiter_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Opener';
			$velden[] = 'Open';
			$velden[] = 'Sluiter';
			$velden[] = 'Pin1';
			$velden[] = 'Pin2';
			$velden[] = 'Kasverschil';
			$velden[] = 'Gesloten';
			$velden[] = 'Opmerking';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Boekverkoop`'
		          .' WHERE `verkoopdag` = %s'
		          .  ' AND `volgnummer` = %i'
		          .' LIMIT 1'
		          , $this->verkoopdag
		          , $this->volgnummer
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->verkoopdag = NULL;
		$this->volgnummer = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Boekverkoop`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `verkoopdag` = %s'
			          .  ' AND `volgnummer` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->verkoopdag
			          , $this->volgnummer
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Boekverkoop terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'verkoopdag':
			return 'date';
		case 'volgnummer':
			return 'int';
		case 'opener':
			return 'foreign';
		case 'opener_contactid':
			return 'int';
		case 'open':
			return 'datetime';
		case 'sluiter':
			return 'foreign';
		case 'sluiter_contactid':
			return 'int';
		case 'pin1':
			return 'money';
		case 'pin2':
			return 'money';
		case 'kasverschil':
			return 'money';
		case 'gesloten':
			return 'datetime';
		case 'opmerking':
			return 'text';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'pin1':
		case 'pin2':
		case 'kasverschil':
			$type = '%f';
			break;
		case 'volgnummer':
		case 'opener_contactID':
		case 'sluiter_contactID':
			$type = '%i';
			break;
		case 'verkoopdag':
		case 'open':
		case 'gesloten':
		case 'opmerking':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Boekverkoop`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `verkoopdag` = %s'
		          .  ' AND `volgnummer` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->verkoopdag
		          , $this->volgnummer
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Boekverkoop`'
		          .' WHERE `verkoopdag` = %s'
		          .  ' AND `volgnummer` = %i'
		                 , $veld
		          , $this->verkoopdag
		          , $this->volgnummer
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Boekverkoop');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
