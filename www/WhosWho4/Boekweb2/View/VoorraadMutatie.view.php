<?
/**
 * $Id$
 */
abstract class VoorraadMutatieView
	extends VoorraadMutatieView_Generated
{
	static public function waardeAantal (VoorraadMutatie $mutatie)
	{
		return sprintf('%+d', $mutatie->getAantal());
	}

	static public function waardeOmschrijving (VoorraadMutatie $mutatie)
	{
		return VoorraadView::waardeOmschrijving($mutatie->getVoorraad());
	}

	static public function waardeWaardePerStuk (VoorraadMutatie $mutatie)
	{
		return VoorraadView::waardeWaardePerStuk($mutatie->getVoorraad());
	}

	static public function waardePrijs (VoorraadMutatie $mutatie)
	{
		return VerkoopPrijsView::waardePrijs($mutatie->getVoorraad()->getVerkoopPrijs());
	}

	static public function uitgebreideRegel (VoorraadMutatie $mutatie, $glue = "\n\n")
	{
		$return = VoorraadMutatieView::waardeWanneer($mutatie) . ': '
				. VoorraadMutatieView::waardeAantal($mutatie);

		if ($mutatie instanceof Levering)
		{
			$return = sprintf(_("[Levering] %s\n"
					. "door %s"), $return, LeveringView::waardeLeverancier($mutatie));

			if ($mutatie->getOrder() instanceof Order)
				$return .= sprintf(_("\n"
						. "order geplaatst op: %s"),
						OrderView::waardeDatumGeplaatst($mutatie->getOrder()));
		}

		if ($mutatie instanceof Verplaatsing)
		{
			$return = sprintf(_("[Verplaatsing] %s\n"
					. "vanaf %s"), $return, VoorraadView::waardeLocatie($mutatie->getTegenVoorraad()));
		}

		return $return . $glue;
	}

	static public function type (VoorraadMutatie $mutatie)
	{
		if ($mutatie instanceof Verkoop)
			return _('verkoop');
		if ($mutatie instanceof Verplaatsing)
			return _('verplaatsing');
		if ($mutatie instanceof Levering)
			return _('levering');
	}

	static public function formWanneer(VoorraadMutatie $obj, $include_id = false)
	{
		$waarde = $obj->getWanneer();
		return HtmlInput::makeDateTime('wanneer', tryPar('wanneer', $waarde));
	}

	static public function formAantal(VoorraadMutatie $obj, $include_id = false)
	{
		$waarde = $obj->getAantal();
		return HtmlInput::makeNumber('aantal', tryPar('aantal', $waarde));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
