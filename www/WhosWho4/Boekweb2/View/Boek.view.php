<?
abstract class BoekView
	extends BoekView_Generated
{
	static public function tableRow($boek)
	{
		$tr = parent::tableRow($boek);
		$tr->addData(self::waardeISBN($boek));
		$tr->addData(self::waardeAuteur($boek));
		$tr->addData(self::waardeUitgever($boek));
		$tr->addData(self::waardeDruk($boek));
		return $tr;
	}

	static public function tableHeaderRow()
	{
		$tr = parent::tableHeaderRow();
		$tr->addHeader(_('ISBN'));
		$tr->addHeader(_('Auteur'));
		$tr->addHeader(_('Uitgever'));
		$tr->addHeader(_('Druk'));
		return $tr;
	}

	static public function wijzigTable($obj, $nieuw = false, $show_error)
	{
		$tbody = parent::wijzigTable($obj, $nieuw, $show_error);

		$tbody->add(self::wijzigTR($obj, 'isbn', $show_error));
		$tbody->add(self::wijzigTR($obj, 'auteur', $show_error));
		$tbody->add(self::wijzigTR($obj, 'druk', $show_error));

		return $tbody;
	}
}
