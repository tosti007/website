<?
abstract class iDealAntwoordVerzamelingView
	extends iDealAntwoordVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function processAntwoordForm(iDealAntwoordVerzameling $antwoorden)
	{
		foreach($antwoorden as $antwoord)
		{
			$antwoordData = tryPar(self::formobj($antwoord), NULL);
			iDealAntwoordView::processForm($antwoord, 'viewWijzig', $antwoordData[$antwoord->getVraagActiviteitID().'_'.$antwoord->getVraagVraagID()]);
		}
	}

	static public function antwoorden(iDealAntwoordVerzameling $antwoorden)
	{
		$page = (new HTMLPage())->start();

		$page->add(HtmlAnchor::button('?csv=true', _('Exporteer als CSV')));

		$page->add($table = new HtmlTable(null, 'sortable'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_('iDealtransactie'), _('Vraag'), _('Antwoord'), _('Aantal')));

		foreach($antwoorden as $antwoord)
		{
			$row = $tbody->addRow();
			$row->addData($antwoord->getIdeal()->geefID());
			$row->addData(ActiviteitVraagView::waardeVraag($antwoord->getVraag()));
			$row->addData(iDealAntwoordView::waardeAntwoord($antwoord));
			$row->addData($antwoord->getiDeal()->getVerkopen()->totaal());
		}

		return $page;
	}

	static public function toCSV(iDealAntwoordVerzameling $antwoorden, $file)
	{
		$velden = array(_('iDealTransactie'), _('Vraag'), _('Antwoord'), _('Aantal'));
		fputcsv($file, $velden);

		foreach($antwoorden as $antwoord)
		{
			static::schrijfCSVRegel(array(
				'iDealTransactie' => $antwoord->getIdeal()->geefID(),
				'Vraag' => ActiviteitVraagView::waardeVraag($antwoord->getVraag()),
				'Antwoord' => iDealAntwoordView::waardeAntwoord($antwoord),
				'Aantal' => $antwoord->getiDeal()->getVerkopen()->totaal(),
			), $velden, $file);
		}
	}
}
