<?
/**
 * $Id$
 */
abstract class VakVerzamelingView
	extends VakVerzamelingView_Generated
{
	/**
	 * Geef een pagina met een overzicht van een hele verzameling aan vakken.
	 *
	 * @param VakVerzameling $vakken De vakken om te tonen.
	 * Dit zijn vaak alle (zichtbare) vakken in de databaas.
	 *
	 * @return HTMLPage
	 */
	public static function vakOverzicht (VakVerzameling $vakken) {
		$page = new HTMLPage();
		$page->start(_('Alle Vakken'));
		if(hasAuth('bestuur')) {
			$page->add(HtmlAnchor::button('/Onderwijs/Vak/Nieuw', _('Maak nieuw vak aan')));
			$page->add(HtmlAnchor::button('/Onderwijs/Vak/NieuwCSV', _("Upload een CSV-bestand met vakken")));
		}
		$page->add($table = new HtmlTable(null, 'sortable'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$tr = $thead->addRow();
		$tr->makeHeaderFromArray([_('Vakcode')
								, _('Naam')
								, _('Studies')
								, _('Departement')
								, _('Opmerking')]);

		foreach ($vakken as $vak) {
			$tr = $tbody->addRow();
			$tr->addData(new HtmlAnchor($vak->url(), VakView::waardeCode($vak)));
			$tr->addData(VakView::waardeNaam($vak));
			$tr->addData(VakView::waardeStudies($vak));
			$tr->addData(VakView::waardeDepartement($vak));
			$tr->addData(VakView::waardeOpmerking($vak));
		}
		return $page;
	}

	/**
	 * Geef een HTML-pagina waar het bestuur een csv-bestand met vakinformatie kan uploaden.
	 *
	 * @return HTMLPage;
	 */
	static public function nieuwCsv()
	{
		$page = new HTMLPage();
		$page->start(_('Vakken met csv toevoegen'));
		// TODO: programmeer een HTMLPanel-klasse die dit voor je regelt.
		$page->add($panel = new HtmlDiv(null, 'panel panel-default'));
		$panel->add(new HtmlDiv("WebCie TopTip!", 'panel-heading'));
		$panel->add($panelBody = new HtmlDiv(null, 'panel-body'));
		$panelBody->add(new HtmlParagraph("Een vak kan meerdere jaren gegeven worden door verschillende docenten, en voor een vak kun je bijvoorbeeld oude tentamens opzoeken. Met deze pagina voer je ook meteen een jaargang in: dat representeert dat een vak in een zekere periode gegeven wordt, met een specifieke docent en boeken enzovoorts. Met jaargangen kun je bijvoorbeeld bijhouden of het dictaat van vorig jaar nog klopt met dat van dit jaar, en dan de oude dictaten uit het hok halen voor pure profit!"));

		$page->add($form = HtmlForm::named('nieuwCSV'));
		$form->add(self::makeFormTextRow('csv', tryPar('csv'), _('Vul hier je csv in')));
		$form->add(new HtmlParagraph(_(
			// Voor een WebCie'er: je kan een voorbeeldspreadsheet vinden in de comments van GitLab issue #14,
			// die ik gebruikt heb om dit formulier te testen.
			"Heeft de Grote Boekencommissarisvakkenspreadsheet jou niet bereikt? Hier een overzichtje:"
			. " Je kan tabs of komma's gebruiken om velden te scheiden."
			. " De eerste regel moet de veldnamen bevatten:"
			. " Vakcode,Vaknaam,Docent,Email,Studie,Dictaat,Boek,B/M,Begindatum,Einddatum"
			. " (in jouw favoriete volgorde!)"
			. " Daarna op elke regel de informatie per vak."
			. " Datums zijn DD-MM-YYYY."
		)));

		$form->add(HtmlInput::makeFormSubmitButton(_('Voer in')));

		return $page;
	}

	static public function makeVakSelectbox()
	{
		$vakken = VakVerzameling::geefAlleVakken();

		$selects = array();

		foreach($vakken as $vak)
			$selects[$vak->geefID()] = sprintf('%s (%s)', VakView::waardeNaam($vak)
				, VakView::waardeCode($vak));

		return self::makeFormEnumRow('vakSelect', $selects, tryPar('vakSelect')
			, _('Vak'));
	}
}
