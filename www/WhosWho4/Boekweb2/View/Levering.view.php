<?
/**
 * $Id$
 */
abstract class LeveringView
	extends LeveringView_Generated
{
	static public function wijzigForm(Levering $levering, $show_error = false)
	{
		$page = Page::getInstance();
		$page->start(_("Nieuwe levering"));

		$page->add($form = HtmlForm::named('leveringForm'));

		$form->add(self::wijzigTable($levering, $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Voer in")));

		// Automagische margeberekening
		$page->addFooterJs(Page::minifiedFile('Boekweb.js'));

		$page->end();
	}

	static public function wijzigTable(Levering $levering, $show_error, $losse_levering = true)
	{
		$tbody = new HtmlDiv();

		if($levering->getVoorraad())
			$tbody->add(self::infoTR($levering, 'voorraad'));
		else
			$tbody->add(self::wijzigTR($levering, 'voorraad', $show_error));

		$tbody->add(self::wijzigTR($levering, 'leverancier', $show_error));
		$tbody->add(self::wijzigTR($levering, 'wanneer', $show_error));
		if ($levering->getOrder() == null) {
			// Om meteen een order te maken als je die nog niet hebt.
			$order = $levering->getArtikel()->nieuweOrder($levering->getLeverancier());
		} else {
			$order = $levering->getOrder();
		}
		// Plaats aantal hier want dat helpt met losseleveringworkflow
		$tbody->add(self::wijzigTR($levering, 'aantal', $show_error));
		if ($losse_levering) {
			$orderView = $order->getView();
			$tbody->add($orderView::losseLeveringOrder($order, $show_error));
		}

		return $tbody;
	}

	/**
	 *  Vul velden in het Levering-opject in met de form-parameters.
	 * @param levering Een nieuw Leveringobject (dat nog niet in de database staat.)
	 * processNieuwForm($levering->getOrder()) moet ook kunnen werken.
	 * @returns Een Leveringobject met de velden ingevuld.
	 */
	static public function processNieuwForm(Levering $levering)
	{
		$voorraad = $levering->getVoorraad();
		$datetime = new DateTimeLocale(tryPar('wanneer') . " " . tryPar('Timewanneer'));
		$leverancier = Leverancier::geef(tryPar('leverancier', null));
		if(!$leverancier)
			$leverancier = null;

		$order = $voorraad->getArtikel()->nieuweOrder($leverancier);
		$orderView = $order->getView();
		$order = $orderView::processLosseLeveringOrder($order);
		$aantal = tryPar('aantal');

		$levering = new Levering($voorraad, $datetime, $aantal, $order, $leverancier);

		self::processForm($levering);

		return $levering;
	}

	static public function waardeLeverancier (Levering $levering)
	{
		return LeverancierView::waardeNaam($levering->getLeverancier());
	}

	static public function formLeverancier (Levering $levering, $include_id = false)
	{
		$leverancier = $levering->getLeverancier();
		if($leverancier)
			$select = $leverancier->geefID();
		else
			$select = null;
		return LeverancierVerzamelingView::geefSelectbox($select);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
