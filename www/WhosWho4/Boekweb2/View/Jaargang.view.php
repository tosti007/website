<?
/**
 * $Id$
 */
abstract class JaargangView
	extends JaargangView_Generated
{
	/**
	 * Pagina met alle informatie van deze Jaargang.
	 *
	 * @param Jaargang $jaargang
	 *
	 * @return HTMLPage
	 */
	public static function infopagina(Jaargang $jaargang)
	{
		$vak = $jaargang->getVak();
		$jgArtikels = JaargangArtikelVerzameling::fromJaargang($jaargang);

		$page = static::pageLinks($jaargang);
		$page->start();

		$page->add($knopDiv = new HtmlDiv(null, 'btn-group'));
		$knopDiv->add(HtmlAnchor::button($vak->url() . 'Jaargang',
			_("Jaargangoverzicht"))
		);
		$knopDiv->add(HtmlAnchor::button($vak->url() . 'Jaargang/Toevoegen',
			_("Jaargang toevoegen"))
		);

		$page->add(static::viewInfo($jaargang));

		if ($jgArtikels->aantal() > 0)
		{
			$page->add(new HtmlHeader(3, _("Artikels bij deze jaargang")));
			$page->add(JaargangArtikelVerzamelingView::infoTable($jgArtikels));
		}

		return $page;
	}

	/**
	 * Pagina met een formulier voor Jaargang-informatie,
	 * om die te wijzigen of toe te voegen.
	 *
	 * @param Jaargang $jaargang
	 * @param bool $nieuw Is true als de jaargang nog niet in de db staat.
	 * @param bool $show_error Zo ja, toon wat er fout is bij de velden.
	 *
	 * @return HTMLPage
	 */
	public static function wijzigen(Jaargang $jaargang, $nieuw = false, $show_error = true)
	{
		$page = new HTMLPage();

		if ($nieuw)
		{
			$page->start(sprintf(_("Jaargang toevoegen voor vak %s"), VakView::waardeNaam($jaargang->getVak())));
		}
		else
		{
			$page->start(sprintf(_("Jaargang wijzigen voor vak %s"), VakView::waardeNaam($jaargang->getVak())));
		}

		$page->add($form = HtmlForm::named('jaargangToevoegen'));
		$form->add(self::createForm($jaargang, $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Voeg toe")));

		return $page;
	}

	static public function processNieuwForm($obj)
	{
		if(!$data = tryPar(self::formObj($obj), NULL))
			return false;

		self::processForm($obj);
	}

	/**
	 * Pagina om een nieuwe jaargang toe te voegen bij een vak.
	 *
	 * @return HTMLPage
	 */
	public static function nieuwJaargang()
	{
		$page = new HTMLPage();
		$page->start(_("Nieuwe jaargang bij een vak toevoegen"));

		$page->add($form = HtmlForm::named('nieuwJaargang'));
		$form->add(VakVerzamelingView::makeVakSelectbox());
		$form->add(HtmlInput::makeFormSubmitButton(_('Doe')));

		return $page;
	}

	static public function formContactPersoon(Jaargang $obj, $include_id = false)
	{
		return self::defaultFormPersoonVerzameling(null, 'Jaargang[ContactPersoon]', true);
	}

	static public function opmerkingContactPersoon()
	{
		return _("Selecteer hier de docent in. Als je de docent niet kan vinden maak dan eerst een contact voor de docent aan.");
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
