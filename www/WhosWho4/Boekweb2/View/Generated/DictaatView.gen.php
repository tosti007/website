<?
abstract class DictaatView_Generated
	extends ArtikelView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DictaatView.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDictaat(Dictaat $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld auteur.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld auteur labelt.
	 */
	public static function labelAuteur(Dictaat $obj)
	{
		return 'Auteur';
	}
	/**
	 * @brief Geef de waarde van het veld auteur.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld auteur van het object obj
	 * representeert.
	 */
	public static function waardeAuteur(Dictaat $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getAuteur())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getAuteur());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld auteur.
	 *
	 * @see genericFormauteur
	 *
	 * @param Dictaat $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auteur staat en kan worden
	 * bewerkt. Indien auteur read-only is betreft het een statisch html-element.
	 */
	public static function formAuteur(Dictaat $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getAuteur());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld auteur. In
	 * tegenstelling tot formauteur moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formauteur
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld auteur staat en kan worden
	 * bewerkt. Indien auteur read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAuteur($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Auteur');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld auteur
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld auteur representeert.
	 */
	public static function opmerkingAuteur()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uitgaveJaar.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uitgaveJaar labelt.
	 */
	public static function labelUitgaveJaar(Dictaat $obj)
	{
		return 'UitgaveJaar';
	}
	/**
	 * @brief Geef de waarde van het veld uitgaveJaar.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uitgaveJaar van het object
	 * obj representeert.
	 */
	public static function waardeUitgaveJaar(Dictaat $obj)
	{
		return static::defaultWaardeInt($obj, 'UitgaveJaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uitgaveJaar.
	 *
	 * @see genericFormuitgaveJaar
	 *
	 * @param Dictaat $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitgaveJaar staat en kan
	 * worden bewerkt. Indien uitgaveJaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formUitgaveJaar(Dictaat $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'UitgaveJaar', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uitgaveJaar. In
	 * tegenstelling tot formuitgaveJaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formuitgaveJaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitgaveJaar staat en kan
	 * worden bewerkt. Indien uitgaveJaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormUitgaveJaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'UitgaveJaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * uitgaveJaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uitgaveJaar representeert.
	 */
	public static function opmerkingUitgaveJaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld digitaalVerspreiden.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld digitaalVerspreiden labelt.
	 */
	public static function labelDigitaalVerspreiden(Dictaat $obj)
	{
		return 'DigitaalVerspreiden';
	}
	/**
	 * @brief Geef de waarde van het veld digitaalVerspreiden.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld digitaalVerspreiden van het
	 * object obj representeert.
	 */
	public static function waardeDigitaalVerspreiden(Dictaat $obj)
	{
		return static::defaultWaardeBool($obj, 'DigitaalVerspreiden');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld digitaalVerspreiden.
	 *
	 * @see genericFormdigitaalVerspreiden
	 *
	 * @param Dictaat $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld digitaalVerspreiden staat
	 * en kan worden bewerkt. Indien digitaalVerspreiden read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formDigitaalVerspreiden(Dictaat $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'DigitaalVerspreiden', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld digitaalVerspreiden.
	 * In tegenstelling tot formdigitaalVerspreiden moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formdigitaalVerspreiden
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld digitaalVerspreiden staat
	 * en kan worden bewerkt. Indien digitaalVerspreiden read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormDigitaalVerspreiden($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'DigitaalVerspreiden');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * digitaalVerspreiden bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld digitaalVerspreiden representeert.
	 */
	public static function opmerkingDigitaalVerspreiden()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beginGebruik.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld beginGebruik labelt.
	 */
	public static function labelBeginGebruik(Dictaat $obj)
	{
		return 'BeginGebruik';
	}
	/**
	 * @brief Geef de waarde van het veld beginGebruik.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beginGebruik van het object
	 * obj representeert.
	 */
	public static function waardeBeginGebruik(Dictaat $obj)
	{
		return static::defaultWaardeDate($obj, 'BeginGebruik');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beginGebruik.
	 *
	 * @see genericFormbeginGebruik
	 *
	 * @param Dictaat $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginGebruik staat en kan
	 * worden bewerkt. Indien beginGebruik read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeginGebruik(Dictaat $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'BeginGebruik', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beginGebruik. In
	 * tegenstelling tot formbeginGebruik moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeginGebruik
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginGebruik staat en kan
	 * worden bewerkt. Indien beginGebruik read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeginGebruik($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'BeginGebruik');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beginGebruik bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beginGebruik representeert.
	 */
	public static function opmerkingBeginGebruik()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld eindeGebruik.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld eindeGebruik labelt.
	 */
	public static function labelEindeGebruik(Dictaat $obj)
	{
		return 'EindeGebruik';
	}
	/**
	 * @brief Geef de waarde van het veld eindeGebruik.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld eindeGebruik van het object
	 * obj representeert.
	 */
	public static function waardeEindeGebruik(Dictaat $obj)
	{
		return static::defaultWaardeDate($obj, 'EindeGebruik');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld eindeGebruik.
	 *
	 * @see genericFormeindeGebruik
	 *
	 * @param Dictaat $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindeGebruik staat en kan
	 * worden bewerkt. Indien eindeGebruik read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEindeGebruik(Dictaat $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'EindeGebruik', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld eindeGebruik. In
	 * tegenstelling tot formeindeGebruik moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formeindeGebruik
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindeGebruik staat en kan
	 * worden bewerkt. Indien eindeGebruik read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEindeGebruik($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'EindeGebruik');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * eindeGebruik bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld eindeGebruik representeert.
	 */
	public static function opmerkingEindeGebruik()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld copyrightOpmerking.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld copyrightOpmerking labelt.
	 */
	public static function labelCopyrightOpmerking(Dictaat $obj)
	{
		return 'CopyrightOpmerking';
	}
	/**
	 * @brief Geef de waarde van het veld copyrightOpmerking.
	 *
	 * @param Dictaat $obj Het Dictaat-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld copyrightOpmerking van het
	 * object obj representeert.
	 */
	public static function waardeCopyrightOpmerking(Dictaat $obj)
	{
		return static::defaultWaardeText($obj, 'CopyrightOpmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld copyrightOpmerking.
	 *
	 * @see genericFormcopyrightOpmerking
	 *
	 * @param Dictaat $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld copyrightOpmerking staat
	 * en kan worden bewerkt. Indien copyrightOpmerking read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formCopyrightOpmerking(Dictaat $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'CopyrightOpmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld copyrightOpmerking. In
	 * tegenstelling tot formcopyrightOpmerking moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formcopyrightOpmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld copyrightOpmerking staat
	 * en kan worden bewerkt. Indien copyrightOpmerking read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormCopyrightOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'CopyrightOpmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * copyrightOpmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld copyrightOpmerking representeert.
	 */
	public static function opmerkingCopyrightOpmerking()
	{
		return NULL;
	}
}
