<?
abstract class VoorraadVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VoorraadVerzamelingView.
	 *
	 * @param VoorraadVerzameling $obj Het VoorraadVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoorraadVerzameling(VoorraadVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
