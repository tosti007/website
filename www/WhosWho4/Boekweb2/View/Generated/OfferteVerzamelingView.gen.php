<?
abstract class OfferteVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in OfferteVerzamelingView.
	 *
	 * @param OfferteVerzameling $obj Het OfferteVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeOfferteVerzameling(OfferteVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
