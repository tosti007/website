<?
abstract class TransactieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TransactieView.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTransactie(Transactie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld transactieID.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld transactieID labelt.
	 */
	public static function labelTransactieID(Transactie $obj)
	{
		return 'TransactieID';
	}
	/**
	 * @brief Geef de waarde van het veld transactieID.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld transactieID van het object
	 * obj representeert.
	 */
	public static function waardeTransactieID(Transactie $obj)
	{
		return static::defaultWaardeInt($obj, 'TransactieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * transactieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld transactieID representeert.
	 */
	public static function opmerkingTransactieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rubriek.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld rubriek labelt.
	 */
	public static function labelRubriek(Transactie $obj)
	{
		return 'Rubriek';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld rubriek.
	 *
	 * @param string $value Een enum-waarde van het veld rubriek.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumRubriek($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld rubriek horen.
	 *
	 * @see labelenumRubriek
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld rubriek
	 * representeren.
	 */
	public static function labelenumRubriekArray()
	{
		$soorten = array();
		foreach(Transactie::enumsRubriek() as $id)
			$soorten[$id] = TransactieView::labelenumRubriek($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld rubriek.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rubriek van het object obj
	 * representeert.
	 */
	public static function waardeRubriek(Transactie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Rubriek');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rubriek.
	 *
	 * @see genericFormrubriek
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rubriek staat en kan
	 * worden bewerkt. Indien rubriek read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formRubriek(Transactie $obj, $include_id = false)
	{
		return static::waardeRubriek($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rubriek. In
	 * tegenstelling tot formrubriek moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formrubriek
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rubriek staat en kan
	 * worden bewerkt. Indien rubriek read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormRubriek($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld rubriek
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rubriek representeert.
	 */
	public static function opmerkingRubriek()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(Transactie $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(Transactie::enumsSoort() as $id)
			$soorten[$id] = TransactieView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(Transactie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(Transactie $obj, $include_id = false)
	{
		return static::waardeSoort($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contact.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contact labelt.
	 */
	public static function labelContact(Transactie $obj)
	{
		return 'Contact';
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contact van het object obj
	 * representeert.
	 */
	public static function waardeContact(Transactie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContact())
			return NULL;
		return ContactView::defaultWaardeContact($obj->getContact());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contact.
	 *
	 * @see genericFormcontact
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formContact(Transactie $obj, $include_id = false)
	{
		return static::waardeContact($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contact. In
	 * tegenstelling tot formcontact moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formcontact
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormContact($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld contact
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contact representeert.
	 */
	public static function opmerkingContact()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bedrag.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld bedrag labelt.
	 */
	public static function labelBedrag(Transactie $obj)
	{
		return 'Bedrag';
	}
	/**
	 * @brief Geef de waarde van het veld bedrag.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bedrag van het object obj
	 * representeert.
	 */
	public static function waardeBedrag(Transactie $obj)
	{
		return static::defaultWaardeMoney($obj, 'Bedrag');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bedrag.
	 *
	 * @see genericFormbedrag
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrag staat en kan worden
	 * bewerkt. Indien bedrag read-only is betreft het een statisch html-element.
	 */
	public static function formBedrag(Transactie $obj, $include_id = false)
	{
		return static::waardeBedrag($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bedrag. In
	 * tegenstelling tot formbedrag moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbedrag
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bedrag staat en kan worden
	 * bewerkt. Indien bedrag read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBedrag($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld bedrag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bedrag representeert.
	 */
	public static function opmerkingBedrag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld uitleg.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld uitleg labelt.
	 */
	public static function labelUitleg(Transactie $obj)
	{
		return 'Uitleg';
	}
	/**
	 * @brief Geef de waarde van het veld uitleg.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld uitleg van het object obj
	 * representeert.
	 */
	public static function waardeUitleg(Transactie $obj)
	{
		return static::defaultWaardeString($obj, 'Uitleg');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld uitleg.
	 *
	 * @see genericFormuitleg
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitleg staat en kan worden
	 * bewerkt. Indien uitleg read-only is betreft het een statisch html-element.
	 */
	public static function formUitleg(Transactie $obj, $include_id = false)
	{
		return static::waardeUitleg($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld uitleg. In
	 * tegenstelling tot formuitleg moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formuitleg
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld uitleg staat en kan worden
	 * bewerkt. Indien uitleg read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormUitleg($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld uitleg
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld uitleg representeert.
	 */
	public static function opmerkingUitleg()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wanneer.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld wanneer labelt.
	 */
	public static function labelWanneer(Transactie $obj)
	{
		return 'Wanneer';
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wanneer van het object obj
	 * representeert.
	 */
	public static function waardeWanneer(Transactie $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Wanneer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wanneer.
	 *
	 * @see genericFormwanneer
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWanneer(Transactie $obj, $include_id = false)
	{
		return static::waardeWanneer($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wanneer. In
	 * tegenstelling tot formwanneer moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwanneer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWanneer($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wanneer
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wanneer representeert.
	 */
	public static function opmerkingWanneer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld status.
	 *
	 * @param Transactie $obj Het Transactie-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld status labelt.
	 */
	public static function labelStatus(Transactie $obj)
	{
		return 'Status';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld status.
	 *
	 * @param string $value Een enum-waarde van het veld status.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumStatus($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld status horen.
	 *
	 * @see labelenumStatus
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld status
	 * representeren.
	 */
	public static function labelenumStatusArray()
	{
		$soorten = array();
		foreach(Transactie::enumsStatus() as $id)
			$soorten[$id] = TransactieView::labelenumStatus($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @param Transactie $obj Het Transactie-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld status van het object obj
	 * representeert.
	 */
	public static function waardeStatus(Transactie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Status');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld status.
	 *
	 * @see genericFormstatus
	 *
	 * @param Transactie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is betreft het een statisch html-element.
	 */
	public static function formStatus(Transactie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Status', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld status. In
	 * tegenstelling tot formstatus moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstatus
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld status staat en kan worden
	 * bewerkt. Indien status read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormStatus($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Status', Transactie::enumsstatus());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld status
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld status representeert.
	 */
	public static function opmerkingStatus()
	{
		return NULL;
	}
}
