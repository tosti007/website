<?
abstract class iDealKaartjeView_Generated
	extends ArtikelView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealKaartjeView.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDealKaartje(iDealKaartje $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld activiteit.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld activiteit labelt.
	 */
	public static function labelActiviteit(iDealKaartje $obj)
	{
		return 'Activiteit';
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld activiteit van het object obj
	 * representeert.
	 */
	public static function waardeActiviteit(iDealKaartje $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getActiviteit())
			return NULL;
		return ActiviteitView::defaultWaardeActiviteit($obj->getActiviteit());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld activiteit.
	 *
	 * @see genericFormactiviteit
	 *
	 * @param iDealKaartje $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld activiteit staat en kan
	 * worden bewerkt. Indien activiteit read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formActiviteit(iDealKaartje $obj, $include_id = false)
	{
		return static::waardeActiviteit($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld activiteit. In
	 * tegenstelling tot formactiviteit moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formactiviteit
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld activiteit staat en kan
	 * worden bewerkt. Indien activiteit read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormActiviteit($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * activiteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld activiteit representeert.
	 */
	public static function opmerkingActiviteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld autoInschrijven.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld autoInschrijven labelt.
	 */
	public static function labelAutoInschrijven(iDealKaartje $obj)
	{
		return 'AutoInschrijven';
	}
	/**
	 * @brief Geef de waarde van het veld autoInschrijven.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld autoInschrijven van het
	 * object obj representeert.
	 */
	public static function waardeAutoInschrijven(iDealKaartje $obj)
	{
		return static::defaultWaardeBool($obj, 'AutoInschrijven');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld autoInschrijven.
	 *
	 * @see genericFormautoInschrijven
	 *
	 * @param iDealKaartje $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld autoInschrijven staat en
	 * kan worden bewerkt. Indien autoInschrijven read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAutoInschrijven(iDealKaartje $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'AutoInschrijven', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld autoInschrijven. In
	 * tegenstelling tot formautoInschrijven moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formautoInschrijven
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld autoInschrijven staat en
	 * kan worden bewerkt. Indien autoInschrijven read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormAutoInschrijven($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'AutoInschrijven');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * autoInschrijven bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld autoInschrijven representeert.
	 */
	public static function opmerkingAutoInschrijven()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld returnURL.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld returnURL labelt.
	 */
	public static function labelReturnURL(iDealKaartje $obj)
	{
		return 'ReturnURL';
	}
	/**
	 * @brief Geef de waarde van het veld returnURL.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld returnURL van het object obj
	 * representeert.
	 */
	public static function waardeReturnURL(iDealKaartje $obj)
	{
		return static::defaultWaardeString($obj, 'ReturnURL');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld returnURL.
	 *
	 * @see genericFormreturnURL
	 *
	 * @param iDealKaartje $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld returnURL staat en kan
	 * worden bewerkt. Indien returnURL read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formReturnURL(iDealKaartje $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'ReturnURL', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld returnURL. In
	 * tegenstelling tot formreturnURL moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formreturnURL
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld returnURL staat en kan
	 * worden bewerkt. Indien returnURL read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormReturnURL($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'ReturnURL', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * returnURL bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld returnURL representeert.
	 */
	public static function opmerkingReturnURL()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld externVerkrijgbaar.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld externVerkrijgbaar labelt.
	 */
	public static function labelExternVerkrijgbaar(iDealKaartje $obj)
	{
		return 'ExternVerkrijgbaar';
	}
	/**
	 * @brief Geef de waarde van het veld externVerkrijgbaar.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld externVerkrijgbaar van het
	 * object obj representeert.
	 */
	public static function waardeExternVerkrijgbaar(iDealKaartje $obj)
	{
		return static::defaultWaardeBool($obj, 'ExternVerkrijgbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld externVerkrijgbaar.
	 *
	 * @see genericFormexternVerkrijgbaar
	 *
	 * @param iDealKaartje $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld externVerkrijgbaar staat
	 * en kan worden bewerkt. Indien externVerkrijgbaar read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formExternVerkrijgbaar(iDealKaartje $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'ExternVerkrijgbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld externVerkrijgbaar. In
	 * tegenstelling tot formexternVerkrijgbaar moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formexternVerkrijgbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld externVerkrijgbaar staat
	 * en kan worden bewerkt. Indien externVerkrijgbaar read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormExternVerkrijgbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'ExternVerkrijgbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * externVerkrijgbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld externVerkrijgbaar representeert.
	 */
	public static function opmerkingExternVerkrijgbaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld digitaalVerkrijgbaar.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld digitaalVerkrijgbaar labelt.
	 */
	public static function labelDigitaalVerkrijgbaar(iDealKaartje $obj)
	{
		return 'DigitaalVerkrijgbaar';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld
	 * digitaalVerkrijgbaar.
	 *
	 * @param string $value Een enum-waarde van het veld digitaalVerkrijgbaar.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumDigitaalVerkrijgbaar($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld digitaalVerkrijgbaar horen.
	 *
	 * @see labelenumDigitaalVerkrijgbaar
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld
	 * digitaalVerkrijgbaar representeren.
	 */
	public static function labelenumDigitaalVerkrijgbaarArray()
	{
		$soorten = array();
		foreach(iDealKaartje::enumsDigitaalVerkrijgbaar() as $id)
			$soorten[$id] = iDealKaartjeView::labelenumDigitaalVerkrijgbaar($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld digitaalVerkrijgbaar.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld digitaalVerkrijgbaar van het
	 * object obj representeert.
	 */
	public static function waardeDigitaalVerkrijgbaar(iDealKaartje $obj)
	{
		return static::defaultWaardeEnum($obj, 'DigitaalVerkrijgbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld digitaalVerkrijgbaar.
	 *
	 * @see genericFormdigitaalVerkrijgbaar
	 *
	 * @param iDealKaartje $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld digitaalVerkrijgbaar staat
	 * en kan worden bewerkt. Indien digitaalVerkrijgbaar read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formDigitaalVerkrijgbaar(iDealKaartje $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'DigitaalVerkrijgbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld digitaalVerkrijgbaar.
	 * In tegenstelling tot formdigitaalVerkrijgbaar moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formdigitaalVerkrijgbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld digitaalVerkrijgbaar staat
	 * en kan worden bewerkt. Indien digitaalVerkrijgbaar read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormDigitaalVerkrijgbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'DigitaalVerkrijgbaar', iDealKaartje::enumsdigitaalVerkrijgbaar());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * digitaalVerkrijgbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld digitaalVerkrijgbaar representeert.
	 */
	public static function opmerkingDigitaalVerkrijgbaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld magScannen.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld magScannen labelt.
	 */
	public static function labelMagScannen(iDealKaartje $obj)
	{
		return 'MagScannen';
	}
	/**
	 * @brief Geef de waarde van het veld magScannen.
	 *
	 * @param iDealKaartje $obj Het iDealKaartje-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld magScannen van het object obj
	 * representeert.
	 */
	public static function waardeMagScannen(iDealKaartje $obj)
	{
		return static::defaultWaardeBool($obj, 'MagScannen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld magScannen.
	 *
	 * @see genericFormmagScannen
	 *
	 * @param iDealKaartje $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld magScannen staat en kan
	 * worden bewerkt. Indien magScannen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMagScannen(iDealKaartje $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'MagScannen', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld magScannen. In
	 * tegenstelling tot formmagScannen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmagScannen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld magScannen staat en kan
	 * worden bewerkt. Indien magScannen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMagScannen($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'MagScannen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * magScannen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld magScannen representeert.
	 */
	public static function opmerkingMagScannen()
	{
		return NULL;
	}
}
