<?
abstract class VoorraadMutatieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VoorraadMutatieView.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoorraadMutatie(VoorraadMutatie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld voorraadMutatieID.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld voorraadMutatieID labelt.
	 */
	public static function labelVoorraadMutatieID(VoorraadMutatie $obj)
	{
		return 'VoorraadMutatieID';
	}
	/**
	 * @brief Geef de waarde van het veld voorraadMutatieID.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraadMutatieID van het
	 * object obj representeert.
	 */
	public static function waardeVoorraadMutatieID(VoorraadMutatie $obj)
	{
		return static::defaultWaardeInt($obj, 'VoorraadMutatieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraadMutatieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraadMutatieID representeert.
	 */
	public static function opmerkingVoorraadMutatieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorraad.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld voorraad labelt.
	 */
	public static function labelVoorraad(VoorraadMutatie $obj)
	{
		return 'Voorraad';
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraad van het object obj
	 * representeert.
	 */
	public static function waardeVoorraad(VoorraadMutatie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVoorraad())
			return NULL;
		return VoorraadView::defaultWaardeVoorraad($obj->getVoorraad());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voorraad.
	 *
	 * @see genericFormvoorraad
	 *
	 * @param VoorraadMutatie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorraad staat en kan
	 * worden bewerkt. Indien voorraad read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoorraad(VoorraadMutatie $obj, $include_id = false)
	{
		return static::waardeVoorraad($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voorraad. In
	 * tegenstelling tot formvoorraad moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoorraad
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorraad staat en kan
	 * worden bewerkt. Indien voorraad read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoorraad($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraad representeert.
	 */
	public static function opmerkingVoorraad()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wanneer.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld wanneer labelt.
	 */
	public static function labelWanneer(VoorraadMutatie $obj)
	{
		return 'Wanneer';
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wanneer van het object obj
	 * representeert.
	 */
	public static function waardeWanneer(VoorraadMutatie $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Wanneer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wanneer.
	 *
	 * @see genericFormwanneer
	 *
	 * @param VoorraadMutatie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWanneer(VoorraadMutatie $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'Wanneer', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wanneer. In
	 * tegenstelling tot formwanneer moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwanneer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWanneer($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'Wanneer');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wanneer
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wanneer representeert.
	 */
	public static function opmerkingWanneer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aantal.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld aantal labelt.
	 */
	public static function labelAantal(VoorraadMutatie $obj)
	{
		return 'Aantal';
	}
	/**
	 * @brief Geef de waarde van het veld aantal.
	 *
	 * @param VoorraadMutatie $obj Het VoorraadMutatie-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aantal van het object obj
	 * representeert.
	 */
	public static function waardeAantal(VoorraadMutatie $obj)
	{
		return static::defaultWaardeInt($obj, 'Aantal');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aantal.
	 *
	 * @see genericFormaantal
	 *
	 * @param VoorraadMutatie $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is betreft het een statisch html-element.
	 */
	public static function formAantal(VoorraadMutatie $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Aantal', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aantal. In
	 * tegenstelling tot formaantal moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formaantal
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantal staat en kan worden
	 * bewerkt. Indien aantal read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAantal($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Aantal');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld aantal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aantal representeert.
	 */
	public static function opmerkingAantal()
	{
		return NULL;
	}
}
