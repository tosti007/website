<?
abstract class BarcodeView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in BarcodeView.
	 *
	 * @param Barcode $obj Het Barcode-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeBarcode(Barcode $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld barcodeID.
	 *
	 * @param Barcode $obj Het Barcode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld barcodeID labelt.
	 */
	public static function labelBarcodeID(Barcode $obj)
	{
		return 'BarcodeID';
	}
	/**
	 * @brief Geef de waarde van het veld barcodeID.
	 *
	 * @param Barcode $obj Het Barcode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld barcodeID van het object obj
	 * representeert.
	 */
	public static function waardeBarcodeID(Barcode $obj)
	{
		return static::defaultWaardeInt($obj, 'BarcodeID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * barcodeID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld barcodeID representeert.
	 */
	public static function opmerkingBarcodeID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorraad.
	 *
	 * @param Barcode $obj Het Barcode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voorraad labelt.
	 */
	public static function labelVoorraad(Barcode $obj)
	{
		return 'Voorraad';
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @param Barcode $obj Het Barcode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraad van het object obj
	 * representeert.
	 */
	public static function waardeVoorraad(Barcode $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVoorraad())
			return NULL;
		return VoorraadView::defaultWaardeVoorraad($obj->getVoorraad());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voorraad.
	 *
	 * @see genericFormvoorraad
	 *
	 * @param Barcode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorraad staat en kan
	 * worden bewerkt. Indien voorraad read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoorraad(Barcode $obj, $include_id = false)
	{
		return static::waardeVoorraad($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voorraad. In
	 * tegenstelling tot formvoorraad moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoorraad
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorraad staat en kan
	 * worden bewerkt. Indien voorraad read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoorraad($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraad representeert.
	 */
	public static function opmerkingVoorraad()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wanneer.
	 *
	 * @param Barcode $obj Het Barcode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld wanneer labelt.
	 */
	public static function labelWanneer(Barcode $obj)
	{
		return 'Wanneer';
	}
	/**
	 * @brief Geef de waarde van het veld wanneer.
	 *
	 * @param Barcode $obj Het Barcode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wanneer van het object obj
	 * representeert.
	 */
	public static function waardeWanneer(Barcode $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Wanneer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wanneer.
	 *
	 * @see genericFormwanneer
	 *
	 * @param Barcode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWanneer(Barcode $obj, $include_id = false)
	{
		return static::waardeWanneer($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wanneer. In
	 * tegenstelling tot formwanneer moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formwanneer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wanneer staat en kan
	 * worden bewerkt. Indien wanneer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWanneer($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld wanneer
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wanneer representeert.
	 */
	public static function opmerkingWanneer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld barcode.
	 *
	 * @param Barcode $obj Het Barcode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld barcode labelt.
	 */
	public static function labelBarcode(Barcode $obj)
	{
		return 'Barcode';
	}
	/**
	 * @brief Geef de waarde van het veld barcode.
	 *
	 * @param Barcode $obj Het Barcode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld barcode van het object obj
	 * representeert.
	 */
	public static function waardeBarcode(Barcode $obj)
	{
		return static::defaultWaardeString($obj, 'Barcode');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld barcode.
	 *
	 * @see genericFormbarcode
	 *
	 * @param Barcode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld barcode staat en kan
	 * worden bewerkt. Indien barcode read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBarcode(Barcode $obj, $include_id = false)
	{
		return static::waardeBarcode($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld barcode. In
	 * tegenstelling tot formbarcode moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbarcode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld barcode staat en kan
	 * worden bewerkt. Indien barcode read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBarcode($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld barcode
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld barcode representeert.
	 */
	public static function opmerkingBarcode()
	{
		return NULL;
	}
}
