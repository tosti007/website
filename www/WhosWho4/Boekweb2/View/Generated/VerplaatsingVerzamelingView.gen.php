<?
abstract class VerplaatsingVerzamelingView_Generated
	extends VoorraadMutatieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VerplaatsingVerzamelingView.
	 *
	 * @param VerplaatsingVerzameling $obj Het VerplaatsingVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVerplaatsingVerzameling(VerplaatsingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
