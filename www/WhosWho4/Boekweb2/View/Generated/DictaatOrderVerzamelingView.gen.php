<?
abstract class DictaatOrderVerzamelingView_Generated
	extends OrderVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DictaatOrderVerzamelingView.
	 *
	 * @param DictaatOrderVerzameling $obj Het DictaatOrderVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDictaatOrderVerzameling(DictaatOrderVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
