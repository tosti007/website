<?
abstract class DictaatVerzamelingView_Generated
	extends ArtikelVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DictaatVerzamelingView.
	 *
	 * @param DictaatVerzameling $obj Het DictaatVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDictaatVerzameling(DictaatVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
