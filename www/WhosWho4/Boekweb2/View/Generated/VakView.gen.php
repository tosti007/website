<?
abstract class VakView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VakView.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVak(Vak $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld vakID.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vakID labelt.
	 */
	public static function labelVakID(Vak $obj)
	{
		return 'VakID';
	}
	/**
	 * @brief Geef de waarde van het veld vakID.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vakID van het object obj
	 * representeert.
	 */
	public static function waardeVakID(Vak $obj)
	{
		return static::defaultWaardeInt($obj, 'VakID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vakID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vakID representeert.
	 */
	public static function opmerkingVakID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld code.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld code labelt.
	 */
	public static function labelCode(Vak $obj)
	{
		return 'Code';
	}
	/**
	 * @brief Geef de waarde van het veld code.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld code van het object obj
	 * representeert.
	 */
	public static function waardeCode(Vak $obj)
	{
		return static::defaultWaardeString($obj, 'Code');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld code.
	 *
	 * @see genericFormcode
	 *
	 * @param Vak $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld code staat en kan worden
	 * bewerkt. Indien code read-only is betreft het een statisch html-element.
	 */
	public static function formCode(Vak $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Code', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld code. In tegenstelling
	 * tot formcode moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formcode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld code staat en kan worden
	 * bewerkt. Indien code read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormCode($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Code', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld code
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld code representeert.
	 */
	public static function opmerkingCode()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Vak $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Vak $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param Vak $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(Vak $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld departement.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld departement labelt.
	 */
	public static function labelDepartement(Vak $obj)
	{
		return 'Departement';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld departement.
	 *
	 * @param string $value Een enum-waarde van het veld departement.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumDepartement($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld departement horen.
	 *
	 * @see labelenumDepartement
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld departement
	 * representeren.
	 */
	public static function labelenumDepartementArray()
	{
		$soorten = array();
		foreach(Vak::enumsDepartement() as $id)
			$soorten[$id] = VakView::labelenumDepartement($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld departement.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld departement van het object
	 * obj representeert.
	 */
	public static function waardeDepartement(Vak $obj)
	{
		return static::defaultWaardeEnum($obj, 'Departement');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld departement.
	 *
	 * @see genericFormdepartement
	 *
	 * @param Vak $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld departement staat en kan
	 * worden bewerkt. Indien departement read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDepartement(Vak $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Departement', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld departement. In
	 * tegenstelling tot formdepartement moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdepartement
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld departement staat en kan
	 * worden bewerkt. Indien departement read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDepartement($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Departement', Vak::enumsdepartement());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * departement bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld departement representeert.
	 */
	public static function opmerkingDepartement()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(Vak $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(Vak $obj)
	{
		return static::defaultWaardeString($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param Vak $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(Vak $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inTentamenLijsten.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inTentamenLijsten labelt.
	 */
	public static function labelInTentamenLijsten(Vak $obj)
	{
		return 'InTentamenLijsten';
	}
	/**
	 * @brief Geef de waarde van het veld inTentamenLijsten.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inTentamenLijsten van het
	 * object obj representeert.
	 */
	public static function waardeInTentamenLijsten(Vak $obj)
	{
		return static::defaultWaardeBool($obj, 'InTentamenLijsten');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inTentamenLijsten.
	 *
	 * @see genericForminTentamenLijsten
	 *
	 * @param Vak $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inTentamenLijsten staat en
	 * kan worden bewerkt. Indien inTentamenLijsten read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formInTentamenLijsten(Vak $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'InTentamenLijsten', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inTentamenLijsten. In
	 * tegenstelling tot forminTentamenLijsten moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see forminTentamenLijsten
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inTentamenLijsten staat en
	 * kan worden bewerkt. Indien inTentamenLijsten read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormInTentamenLijsten($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'InTentamenLijsten');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * inTentamenLijsten bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inTentamenLijsten representeert.
	 */
	public static function opmerkingInTentamenLijsten()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tentamensOutdated.
	 *
	 * @param Vak $obj Het Vak-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tentamensOutdated labelt.
	 */
	public static function labelTentamensOutdated(Vak $obj)
	{
		return 'TentamensOutdated';
	}
	/**
	 * @brief Geef de waarde van het veld tentamensOutdated.
	 *
	 * @param Vak $obj Het Vak-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tentamensOutdated van het
	 * object obj representeert.
	 */
	public static function waardeTentamensOutdated(Vak $obj)
	{
		return static::defaultWaardeBool($obj, 'TentamensOutdated');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tentamensOutdated.
	 *
	 * @see genericFormtentamensOutdated
	 *
	 * @param Vak $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tentamensOutdated staat en
	 * kan worden bewerkt. Indien tentamensOutdated read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formTentamensOutdated(Vak $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'TentamensOutdated', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tentamensOutdated. In
	 * tegenstelling tot formtentamensOutdated moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formtentamensOutdated
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tentamensOutdated staat en
	 * kan worden bewerkt. Indien tentamensOutdated read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormTentamensOutdated($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'TentamensOutdated');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tentamensOutdated bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tentamensOutdated representeert.
	 */
	public static function opmerkingTentamensOutdated()
	{
		return NULL;
	}
}
