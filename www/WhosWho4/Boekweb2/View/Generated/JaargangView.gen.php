<?
abstract class JaargangView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in JaargangView.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeJaargang(Jaargang $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld jaargangID.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld jaargangID labelt.
	 */
	public static function labelJaargangID(Jaargang $obj)
	{
		return 'JaargangID';
	}
	/**
	 * @brief Geef de waarde van het veld jaargangID.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld jaargangID van het object obj
	 * representeert.
	 */
	public static function waardeJaargangID(Jaargang $obj)
	{
		return static::defaultWaardeInt($obj, 'JaargangID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * jaargangID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld jaargangID representeert.
	 */
	public static function opmerkingJaargangID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vak.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vak labelt.
	 */
	public static function labelVak(Jaargang $obj)
	{
		return 'Vak';
	}
	/**
	 * @brief Geef de waarde van het veld vak.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vak van het object obj
	 * representeert.
	 */
	public static function waardeVak(Jaargang $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVak())
			return NULL;
		return VakView::defaultWaardeVak($obj->getVak());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vak.
	 *
	 * @see genericFormvak
	 *
	 * @param Jaargang $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vak staat en kan worden
	 * bewerkt. Indien vak read-only is betreft het een statisch html-element.
	 */
	public static function formVak(Jaargang $obj, $include_id = false)
	{
		return static::waardeVak($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vak. In tegenstelling
	 * tot formvak moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formvak
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vak staat en kan worden
	 * bewerkt. Indien vak read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormVak($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vak
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vak representeert.
	 */
	public static function opmerkingVak()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contactPersoon.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contactPersoon labelt.
	 */
	public static function labelContactPersoon(Jaargang $obj)
	{
		return 'ContactPersoon';
	}
	/**
	 * @brief Geef de waarde van het veld contactPersoon.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contactPersoon van het object
	 * obj representeert.
	 */
	public static function waardeContactPersoon(Jaargang $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContactPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getContactPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contactPersoon.
	 *
	 * @see genericFormcontactPersoon
	 *
	 * @param Jaargang $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contactPersoon staat en
	 * kan worden bewerkt. Indien contactPersoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formContactPersoon(Jaargang $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getContactPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contactPersoon. In
	 * tegenstelling tot formcontactPersoon moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcontactPersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contactPersoon staat en
	 * kan worden bewerkt. Indien contactPersoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormContactPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('ContactPersoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * contactPersoon bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contactPersoon representeert.
	 */
	public static function opmerkingContactPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumBegin.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumBegin labelt.
	 */
	public static function labelDatumBegin(Jaargang $obj)
	{
		return 'DatumBegin';
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumBegin van het object obj
	 * representeert.
	 */
	public static function waardeDatumBegin(Jaargang $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumBegin.
	 *
	 * @see genericFormdatumBegin
	 *
	 * @param Jaargang $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumBegin(Jaargang $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumBegin. In
	 * tegenstelling tot formdatumBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumBegin representeert.
	 */
	public static function opmerkingDatumBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumEinde.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumEinde labelt.
	 */
	public static function labelDatumEinde(Jaargang $obj)
	{
		return 'DatumEinde';
	}
	/**
	 * @brief Geef de waarde van het veld datumEinde.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumEinde van het object obj
	 * representeert.
	 */
	public static function waardeDatumEinde(Jaargang $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumEinde');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumEinde.
	 *
	 * @see genericFormdatumEinde
	 *
	 * @param Jaargang $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEinde staat en kan
	 * worden bewerkt. Indien datumEinde read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumEinde(Jaargang $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumEinde', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumEinde. In
	 * tegenstelling tot formdatumEinde moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumEinde
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEinde staat en kan
	 * worden bewerkt. Indien datumEinde read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumEinde($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumEinde');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumEinde bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumEinde representeert.
	 */
	public static function opmerkingDatumEinde()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inschrijvingen.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inschrijvingen labelt.
	 */
	public static function labelInschrijvingen(Jaargang $obj)
	{
		return 'Inschrijvingen';
	}
	/**
	 * @brief Geef de waarde van het veld inschrijvingen.
	 *
	 * @param Jaargang $obj Het Jaargang-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inschrijvingen van het object
	 * obj representeert.
	 */
	public static function waardeInschrijvingen(Jaargang $obj)
	{
		return static::defaultWaardeInt($obj, 'Inschrijvingen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inschrijvingen.
	 *
	 * @see genericForminschrijvingen
	 *
	 * @param Jaargang $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inschrijvingen staat en
	 * kan worden bewerkt. Indien inschrijvingen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formInschrijvingen(Jaargang $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Inschrijvingen', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inschrijvingen. In
	 * tegenstelling tot forminschrijvingen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see forminschrijvingen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inschrijvingen staat en
	 * kan worden bewerkt. Indien inschrijvingen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormInschrijvingen($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Inschrijvingen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * inschrijvingen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inschrijvingen representeert.
	 */
	public static function opmerkingInschrijvingen()
	{
		return NULL;
	}
}
