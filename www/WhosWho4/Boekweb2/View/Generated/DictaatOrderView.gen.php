<?
abstract class DictaatOrderView_Generated
	extends OrderView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DictaatOrderView.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDictaatOrder(DictaatOrder $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld kaftPrijs.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld kaftPrijs labelt.
	 */
	public static function labelKaftPrijs(DictaatOrder $obj)
	{
		return 'KaftPrijs';
	}
	/**
	 * @brief Geef de waarde van het veld kaftPrijs.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kaftPrijs van het object obj
	 * representeert.
	 */
	public static function waardeKaftPrijs(DictaatOrder $obj)
	{
		return static::defaultWaardeMoney($obj, 'KaftPrijs');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kaftPrijs.
	 *
	 * @see genericFormkaftPrijs
	 *
	 * @param DictaatOrder $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kaftPrijs staat en kan
	 * worden bewerkt. Indien kaftPrijs read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKaftPrijs(DictaatOrder $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'KaftPrijs', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kaftPrijs. In
	 * tegenstelling tot formkaftPrijs moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formkaftPrijs
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kaftPrijs staat en kan
	 * worden bewerkt. Indien kaftPrijs read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKaftPrijs($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'KaftPrijs');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * kaftPrijs bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kaftPrijs representeert.
	 */
	public static function opmerkingKaftPrijs()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld marge.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld marge labelt.
	 */
	public static function labelMarge(DictaatOrder $obj)
	{
		return 'Marge';
	}
	/**
	 * @brief Geef de waarde van het veld marge.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld marge van het object obj
	 * representeert.
	 */
	public static function waardeMarge(DictaatOrder $obj)
	{
		return static::defaultWaardeMoney($obj, 'Marge');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld marge.
	 *
	 * @see genericFormmarge
	 *
	 * @param DictaatOrder $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld marge staat en kan worden
	 * bewerkt. Indien marge read-only is betreft het een statisch html-element.
	 */
	public static function formMarge(DictaatOrder $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Marge', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld marge. In
	 * tegenstelling tot formmarge moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmarge
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld marge staat en kan worden
	 * bewerkt. Indien marge read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMarge($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Marge');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld marge
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld marge representeert.
	 */
	public static function opmerkingMarge()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld factuurverschil.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld factuurverschil labelt.
	 */
	public static function labelFactuurverschil(DictaatOrder $obj)
	{
		return 'Factuurverschil';
	}
	/**
	 * @brief Geef de waarde van het veld factuurverschil.
	 *
	 * @param DictaatOrder $obj Het DictaatOrder-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld factuurverschil van het
	 * object obj representeert.
	 */
	public static function waardeFactuurverschil(DictaatOrder $obj)
	{
		return static::defaultWaardeMoney($obj, 'Factuurverschil');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld factuurverschil.
	 *
	 * @see genericFormfactuurverschil
	 *
	 * @param DictaatOrder $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld factuurverschil staat en
	 * kan worden bewerkt. Indien factuurverschil read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formFactuurverschil(DictaatOrder $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Factuurverschil', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld factuurverschil. In
	 * tegenstelling tot formfactuurverschil moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formfactuurverschil
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld factuurverschil staat en
	 * kan worden bewerkt. Indien factuurverschil read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormFactuurverschil($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Factuurverschil');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * factuurverschil bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld factuurverschil representeert.
	 */
	public static function opmerkingFactuurverschil()
	{
		return NULL;
	}
}
