<?
abstract class VerkoopVerzamelingView_Generated
	extends VoorraadMutatieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VerkoopVerzamelingView.
	 *
	 * @param VerkoopVerzameling $obj Het VerkoopVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVerkoopVerzameling(VerkoopVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
