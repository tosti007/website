<?
abstract class PeriodeView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PeriodeView.
	 *
	 * @param Periode $obj Het Periode-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePeriode(Periode $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld periodeID.
	 *
	 * @param Periode $obj Het Periode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld periodeID labelt.
	 */
	public static function labelPeriodeID(Periode $obj)
	{
		return 'PeriodeID';
	}
	/**
	 * @brief Geef de waarde van het veld periodeID.
	 *
	 * @param Periode $obj Het Periode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld periodeID van het object obj
	 * representeert.
	 */
	public static function waardePeriodeID(Periode $obj)
	{
		return static::defaultWaardeInt($obj, 'PeriodeID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * periodeID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld periodeID representeert.
	 */
	public static function opmerkingPeriodeID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumBegin.
	 *
	 * @param Periode $obj Het Periode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumBegin labelt.
	 */
	public static function labelDatumBegin(Periode $obj)
	{
		return 'DatumBegin';
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @param Periode $obj Het Periode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumBegin van het object obj
	 * representeert.
	 */
	public static function waardeDatumBegin(Periode $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumBegin.
	 *
	 * @see genericFormdatumBegin
	 *
	 * @param Periode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumBegin(Periode $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumBegin. In
	 * tegenstelling tot formdatumBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumBegin representeert.
	 */
	public static function opmerkingDatumBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld collegejaar.
	 *
	 * @param Periode $obj Het Periode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld collegejaar labelt.
	 */
	public static function labelCollegejaar(Periode $obj)
	{
		return 'Collegejaar';
	}
	/**
	 * @brief Geef de waarde van het veld collegejaar.
	 *
	 * @param Periode $obj Het Periode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld collegejaar van het object
	 * obj representeert.
	 */
	public static function waardeCollegejaar(Periode $obj)
	{
		return static::defaultWaardeInt($obj, 'Collegejaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld collegejaar.
	 *
	 * @see genericFormcollegejaar
	 *
	 * @param Periode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld collegejaar staat en kan
	 * worden bewerkt. Indien collegejaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCollegejaar(Periode $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Collegejaar', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld collegejaar. In
	 * tegenstelling tot formcollegejaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcollegejaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld collegejaar staat en kan
	 * worden bewerkt. Indien collegejaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCollegejaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Collegejaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * collegejaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld collegejaar representeert.
	 */
	public static function opmerkingCollegejaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld periodeNummer.
	 *
	 * @param Periode $obj Het Periode-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld periodeNummer labelt.
	 */
	public static function labelPeriodeNummer(Periode $obj)
	{
		return 'PeriodeNummer';
	}
	/**
	 * @brief Geef de waarde van het veld periodeNummer.
	 *
	 * @param Periode $obj Het Periode-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld periodeNummer van het object
	 * obj representeert.
	 */
	public static function waardePeriodeNummer(Periode $obj)
	{
		return static::defaultWaardeInt($obj, 'PeriodeNummer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld periodeNummer.
	 *
	 * @see genericFormperiodeNummer
	 *
	 * @param Periode $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld periodeNummer staat en kan
	 * worden bewerkt. Indien periodeNummer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPeriodeNummer(Periode $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'PeriodeNummer', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld periodeNummer. In
	 * tegenstelling tot formperiodeNummer moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formperiodeNummer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld periodeNummer staat en kan
	 * worden bewerkt. Indien periodeNummer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPeriodeNummer($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'PeriodeNummer');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * periodeNummer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld periodeNummer representeert.
	 */
	public static function opmerkingPeriodeNummer()
	{
		return NULL;
	}
}
