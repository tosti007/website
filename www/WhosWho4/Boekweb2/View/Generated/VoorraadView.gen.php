<?
abstract class VoorraadView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VoorraadView.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoorraad(Voorraad $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld voorraadID.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voorraadID labelt.
	 */
	public static function labelVoorraadID(Voorraad $obj)
	{
		return 'VoorraadID';
	}
	/**
	 * @brief Geef de waarde van het veld voorraadID.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraadID van het object obj
	 * representeert.
	 */
	public static function waardeVoorraadID(Voorraad $obj)
	{
		return static::defaultWaardeInt($obj, 'VoorraadID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraadID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraadID representeert.
	 */
	public static function opmerkingVoorraadID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld artikel.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld artikel labelt.
	 */
	public static function labelArtikel(Voorraad $obj)
	{
		return 'Artikel';
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld artikel van het object obj
	 * representeert.
	 */
	public static function waardeArtikel(Voorraad $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getArtikel())
			return NULL;
		return ArtikelView::defaultWaardeArtikel($obj->getArtikel());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld artikel.
	 *
	 * @see genericFormartikel
	 *
	 * @param Voorraad $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formArtikel(Voorraad $obj, $include_id = false)
	{
		return static::waardeArtikel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld artikel. In
	 * tegenstelling tot formartikel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formartikel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormArtikel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld artikel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld artikel representeert.
	 */
	public static function opmerkingArtikel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld locatie.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld locatie labelt.
	 */
	public static function labelLocatie(Voorraad $obj)
	{
		return 'Locatie';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld locatie.
	 *
	 * @param string $value Een enum-waarde van het veld locatie.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumLocatie($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld locatie horen.
	 *
	 * @see labelenumLocatie
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld locatie
	 * representeren.
	 */
	public static function labelenumLocatieArray()
	{
		$soorten = array();
		foreach(Voorraad::enumsLocatie() as $id)
			$soorten[$id] = VoorraadView::labelenumLocatie($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld locatie.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld locatie van het object obj
	 * representeert.
	 */
	public static function waardeLocatie(Voorraad $obj)
	{
		return static::defaultWaardeEnum($obj, 'Locatie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld locatie.
	 *
	 * @see genericFormlocatie
	 *
	 * @param Voorraad $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld locatie staat en kan
	 * worden bewerkt. Indien locatie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLocatie(Voorraad $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Locatie', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld locatie. In
	 * tegenstelling tot formlocatie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlocatie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld locatie staat en kan
	 * worden bewerkt. Indien locatie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLocatie($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Locatie', Voorraad::enumslocatie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld locatie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld locatie representeert.
	 */
	public static function opmerkingLocatie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld waardePerStuk.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld waardePerStuk labelt.
	 */
	public static function labelWaardePerStuk(Voorraad $obj)
	{
		return 'WaardePerStuk';
	}
	/**
	 * @brief Geef de waarde van het veld waardePerStuk.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld waardePerStuk van het object
	 * obj representeert.
	 */
	public static function waardeWaardePerStuk(Voorraad $obj)
	{
		return static::defaultWaardeMoney($obj, 'WaardePerStuk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld waardePerStuk.
	 *
	 * @see genericFormwaardePerStuk
	 *
	 * @param Voorraad $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waardePerStuk staat en kan
	 * worden bewerkt. Indien waardePerStuk read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWaardePerStuk(Voorraad $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'WaardePerStuk', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld waardePerStuk. In
	 * tegenstelling tot formwaardePerStuk moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formwaardePerStuk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waardePerStuk staat en kan
	 * worden bewerkt. Indien waardePerStuk read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWaardePerStuk($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'WaardePerStuk');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * waardePerStuk bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld waardePerStuk representeert.
	 */
	public static function opmerkingWaardePerStuk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld btw.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld btw labelt.
	 */
	public static function labelBtw(Voorraad $obj)
	{
		return 'Btw';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld btw.
	 *
	 * @param string $value Een enum-waarde van het veld btw.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumBtw($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld btw horen.
	 *
	 * @see labelenumBtw
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld btw
	 * representeren.
	 */
	public static function labelenumBtwArray()
	{
		$soorten = array();
		foreach(Voorraad::enumsBtw() as $id)
			$soorten[$id] = VoorraadView::labelenumBtw($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld btw.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld btw van het object obj
	 * representeert.
	 */
	public static function waardeBtw(Voorraad $obj)
	{
		return static::defaultWaardeEnum($obj, 'Btw');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld btw.
	 *
	 * @see genericFormbtw
	 *
	 * @param Voorraad $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld btw staat en kan worden
	 * bewerkt. Indien btw read-only is betreft het een statisch html-element.
	 */
	public static function formBtw(Voorraad $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Btw', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld btw. In tegenstelling
	 * tot formbtw moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formbtw
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld btw staat en kan worden
	 * bewerkt. Indien btw read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBtw($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Btw', Voorraad::enumsbtw());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld btw
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld btw representeert.
	 */
	public static function opmerkingBtw()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld omschrijving.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld omschrijving labelt.
	 */
	public static function labelOmschrijving(Voorraad $obj)
	{
		return 'Omschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld omschrijving van het object
	 * obj representeert.
	 */
	public static function waardeOmschrijving(Voorraad $obj)
	{
		return static::defaultWaardeText($obj, 'Omschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld omschrijving.
	 *
	 * @see genericFormomschrijving
	 *
	 * @param Voorraad $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOmschrijving(Voorraad $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Omschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld omschrijving. In
	 * tegenstelling tot formomschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formomschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld omschrijving staat en kan
	 * worden bewerkt. Indien omschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOmschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Omschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * omschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld omschrijving representeert.
	 */
	public static function opmerkingOmschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld verkoopbaar.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld verkoopbaar labelt.
	 */
	public static function labelVerkoopbaar(Voorraad $obj)
	{
		return 'Verkoopbaar';
	}
	/**
	 * @brief Geef de waarde van het veld verkoopbaar.
	 *
	 * @param Voorraad $obj Het Voorraad-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld verkoopbaar van het object
	 * obj representeert.
	 */
	public static function waardeVerkoopbaar(Voorraad $obj)
	{
		return static::defaultWaardeBool($obj, 'Verkoopbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld verkoopbaar.
	 *
	 * @see genericFormverkoopbaar
	 *
	 * @param Voorraad $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verkoopbaar staat en kan
	 * worden bewerkt. Indien verkoopbaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVerkoopbaar(Voorraad $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Verkoopbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld verkoopbaar. In
	 * tegenstelling tot formverkoopbaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formverkoopbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verkoopbaar staat en kan
	 * worden bewerkt. Indien verkoopbaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVerkoopbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Verkoopbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * verkoopbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld verkoopbaar representeert.
	 */
	public static function opmerkingVerkoopbaar()
	{
		return NULL;
	}
}
