<?
abstract class LeveringVerzamelingView_Generated
	extends VoorraadMutatieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LeveringVerzamelingView.
	 *
	 * @param LeveringVerzameling $obj Het LeveringVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLeveringVerzameling(LeveringVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
