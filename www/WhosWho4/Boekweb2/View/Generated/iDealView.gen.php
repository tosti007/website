<?
abstract class iDealView_Generated
	extends GiroView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealView.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDeal(iDeal $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(iDeal $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(iDeal $obj, $include_id = false)
	{
		return static::waardeTitel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorraad.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld voorraad labelt.
	 */
	public static function labelVoorraad(iDeal $obj)
	{
		return 'Voorraad';
	}
	/**
	 * @brief Geef de waarde van het veld voorraad.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorraad van het object obj
	 * representeert.
	 */
	public static function waardeVoorraad(iDeal $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVoorraad())
			return NULL;
		return VoorraadView::defaultWaardeVoorraad($obj->getVoorraad());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voorraad.
	 *
	 * @see genericFormvoorraad
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorraad staat en kan
	 * worden bewerkt. Indien voorraad read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoorraad(iDeal $obj, $include_id = false)
	{
		return static::waardeVoorraad($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voorraad. In
	 * tegenstelling tot formvoorraad moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoorraad
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorraad staat en kan
	 * worden bewerkt. Indien voorraad read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoorraad($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorraad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorraad representeert.
	 */
	public static function opmerkingVoorraad()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld externeID.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld externeID labelt.
	 */
	public static function labelExterneID(iDeal $obj)
	{
		return 'ExterneID';
	}
	/**
	 * @brief Geef de waarde van het veld externeID.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld externeID van het object obj
	 * representeert.
	 */
	public static function waardeExterneID(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'ExterneID');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld externeID.
	 *
	 * @see genericFormexterneID
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld externeID staat en kan
	 * worden bewerkt. Indien externeID read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formExterneID(iDeal $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'ExterneID', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld externeID. In
	 * tegenstelling tot formexterneID moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formexterneID
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld externeID staat en kan
	 * worden bewerkt. Indien externeID read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormExterneID($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'ExterneID', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * externeID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld externeID representeert.
	 */
	public static function opmerkingExterneID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld klantNaam.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld klantNaam labelt.
	 */
	public static function labelKlantNaam(iDeal $obj)
	{
		return 'KlantNaam';
	}
	/**
	 * @brief Geef de waarde van het veld klantNaam.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld klantNaam van het object obj
	 * representeert.
	 */
	public static function waardeKlantNaam(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'KlantNaam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld klantNaam.
	 *
	 * @see genericFormklantNaam
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld klantNaam staat en kan
	 * worden bewerkt. Indien klantNaam read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKlantNaam(iDeal $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'KlantNaam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld klantNaam. In
	 * tegenstelling tot formklantNaam moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formklantNaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld klantNaam staat en kan
	 * worden bewerkt. Indien klantNaam read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKlantNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'KlantNaam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * klantNaam bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld klantNaam representeert.
	 */
	public static function opmerkingKlantNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld klantStad.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld klantStad labelt.
	 */
	public static function labelKlantStad(iDeal $obj)
	{
		return 'KlantStad';
	}
	/**
	 * @brief Geef de waarde van het veld klantStad.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld klantStad van het object obj
	 * representeert.
	 */
	public static function waardeKlantStad(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'KlantStad');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld klantStad.
	 *
	 * @see genericFormklantStad
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld klantStad staat en kan
	 * worden bewerkt. Indien klantStad read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKlantStad(iDeal $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'KlantStad', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld klantStad. In
	 * tegenstelling tot formklantStad moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formklantStad
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld klantStad staat en kan
	 * worden bewerkt. Indien klantStad read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKlantStad($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'KlantStad', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * klantStad bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld klantStad representeert.
	 */
	public static function opmerkingKlantStad()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld expire.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld expire labelt.
	 */
	public static function labelExpire(iDeal $obj)
	{
		return 'Expire';
	}
	/**
	 * @brief Geef de waarde van het veld expire.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld expire van het object obj
	 * representeert.
	 */
	public static function waardeExpire(iDeal $obj)
	{
		return static::defaultWaardeDatetime($obj, 'Expire');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld expire.
	 *
	 * @see genericFormexpire
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld expire staat en kan worden
	 * bewerkt. Indien expire read-only is betreft het een statisch html-element.
	 */
	public static function formExpire(iDeal $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'Expire', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld expire. In
	 * tegenstelling tot formexpire moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formexpire
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld expire staat en kan worden
	 * bewerkt. Indien expire read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormExpire($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'Expire');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld expire
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld expire representeert.
	 */
	public static function opmerkingExpire()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld entranceCode.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld entranceCode labelt.
	 */
	public static function labelEntranceCode(iDeal $obj)
	{
		return 'EntranceCode';
	}
	/**
	 * @brief Geef de waarde van het veld entranceCode.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld entranceCode van het object
	 * obj representeert.
	 */
	public static function waardeEntranceCode(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'EntranceCode');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld entranceCode.
	 *
	 * @see genericFormentranceCode
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld entranceCode staat en kan
	 * worden bewerkt. Indien entranceCode read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEntranceCode(iDeal $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'EntranceCode', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld entranceCode. In
	 * tegenstelling tot formentranceCode moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formentranceCode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld entranceCode staat en kan
	 * worden bewerkt. Indien entranceCode read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEntranceCode($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'EntranceCode', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * entranceCode bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld entranceCode representeert.
	 */
	public static function opmerkingEntranceCode()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld magOpvragenTijdstip.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld magOpvragenTijdstip labelt.
	 */
	public static function labelMagOpvragenTijdstip(iDeal $obj)
	{
		return 'MagOpvragenTijdstip';
	}
	/**
	 * @brief Geef de waarde van het veld magOpvragenTijdstip.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld magOpvragenTijdstip van het
	 * object obj representeert.
	 */
	public static function waardeMagOpvragenTijdstip(iDeal $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MagOpvragenTijdstip');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld magOpvragenTijdstip.
	 *
	 * @see genericFormmagOpvragenTijdstip
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld magOpvragenTijdstip staat
	 * en kan worden bewerkt. Indien magOpvragenTijdstip read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formMagOpvragenTijdstip(iDeal $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MagOpvragenTijdstip', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld magOpvragenTijdstip.
	 * In tegenstelling tot formmagOpvragenTijdstip moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formmagOpvragenTijdstip
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld magOpvragenTijdstip staat
	 * en kan worden bewerkt. Indien magOpvragenTijdstip read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormMagOpvragenTijdstip($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MagOpvragenTijdstip');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * magOpvragenTijdstip bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld magOpvragenTijdstip representeert.
	 */
	public static function opmerkingMagOpvragenTijdstip()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld emailadres.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld emailadres labelt.
	 */
	public static function labelEmailadres(iDeal $obj)
	{
		return 'Emailadres';
	}
	/**
	 * @brief Geef de waarde van het veld emailadres.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld emailadres van het object obj
	 * representeert.
	 */
	public static function waardeEmailadres(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'Emailadres');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld emailadres.
	 *
	 * @see genericFormemailadres
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld emailadres staat en kan
	 * worden bewerkt. Indien emailadres read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEmailadres(iDeal $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Emailadres', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld emailadres. In
	 * tegenstelling tot formemailadres moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formemailadres
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld emailadres staat en kan
	 * worden bewerkt. Indien emailadres read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEmailadres($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Emailadres', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * emailadres bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld emailadres representeert.
	 */
	public static function opmerkingEmailadres()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld returnURL.
	 *
	 * @param iDeal $obj Het iDeal-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld returnURL labelt.
	 */
	public static function labelReturnURL(iDeal $obj)
	{
		return 'ReturnURL';
	}
	/**
	 * @brief Geef de waarde van het veld returnURL.
	 *
	 * @param iDeal $obj Het iDeal-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld returnURL van het object obj
	 * representeert.
	 */
	public static function waardeReturnURL(iDeal $obj)
	{
		return static::defaultWaardeString($obj, 'ReturnURL');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld returnURL.
	 *
	 * @see genericFormreturnURL
	 *
	 * @param iDeal $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld returnURL staat en kan
	 * worden bewerkt. Indien returnURL read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formReturnURL(iDeal $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'ReturnURL', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld returnURL. In
	 * tegenstelling tot formreturnURL moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formreturnURL
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld returnURL staat en kan
	 * worden bewerkt. Indien returnURL read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormReturnURL($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'ReturnURL', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * returnURL bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld returnURL representeert.
	 */
	public static function opmerkingReturnURL()
	{
		return NULL;
	}
}
