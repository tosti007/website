<?
abstract class iDealVerzamelingView_Generated
	extends GiroVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealVerzamelingView.
	 *
	 * @param iDealVerzameling $obj Het iDealVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDealVerzameling(iDealVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
