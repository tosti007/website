<?
abstract class VoorraadMutatieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VoorraadMutatieVerzamelingView.
	 *
	 * @param VoorraadMutatieVerzameling $obj Het VoorraadMutatieVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoorraadMutatieVerzameling(VoorraadMutatieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
