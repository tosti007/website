<?
abstract class TellingVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TellingVerzamelingView.
	 *
	 * @param TellingVerzameling $obj Het TellingVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTellingVerzameling(TellingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
