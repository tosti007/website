<?
abstract class iDealAntwoordView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealAntwoordView.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDealAntwoord(iDealAntwoord $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld ideal.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld ideal labelt.
	 */
	public static function labelIdeal(iDealAntwoord $obj)
	{
		return 'Ideal';
	}
	/**
	 * @brief Geef de waarde van het veld ideal.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ideal van het object obj
	 * representeert.
	 */
	public static function waardeIdeal(iDealAntwoord $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getIdeal())
			return NULL;
		return iDealView::defaultWaardeiDeal($obj->getIdeal());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld ideal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ideal representeert.
	 */
	public static function opmerkingIdeal()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vraag.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld vraag labelt.
	 */
	public static function labelVraag(iDealAntwoord $obj)
	{
		return 'Vraag';
	}
	/**
	 * @brief Geef de waarde van het veld vraag.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vraag van het object obj
	 * representeert.
	 */
	public static function waardeVraag(iDealAntwoord $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVraag())
			return NULL;
		return ActiviteitVraagView::defaultWaardeActiviteitVraag($obj->getVraag());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vraag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vraag representeert.
	 */
	public static function opmerkingVraag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld antwoord.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld antwoord labelt.
	 */
	public static function labelAntwoord(iDealAntwoord $obj)
	{
		return 'Antwoord';
	}
	/**
	 * @brief Geef de waarde van het veld antwoord.
	 *
	 * @param iDealAntwoord $obj Het iDealAntwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld antwoord van het object obj
	 * representeert.
	 */
	public static function waardeAntwoord(iDealAntwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Antwoord');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld antwoord.
	 *
	 * @see genericFormantwoord
	 *
	 * @param iDealAntwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld antwoord staat en kan
	 * worden bewerkt. Indien antwoord read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAntwoord(iDealAntwoord $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Antwoord', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld antwoord. In
	 * tegenstelling tot formantwoord moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formantwoord
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld antwoord staat en kan
	 * worden bewerkt. Indien antwoord read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAntwoord($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Antwoord', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * antwoord bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld antwoord representeert.
	 */
	public static function opmerkingAntwoord()
	{
		return NULL;
	}
}
