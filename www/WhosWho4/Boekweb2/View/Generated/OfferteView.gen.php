<?
abstract class OfferteView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in OfferteView.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeOfferte(Offerte $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld offerteID.
	 *
	 * @param Offerte $obj Het Offerte-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld offerteID labelt.
	 */
	public static function labelOfferteID(Offerte $obj)
	{
		return 'OfferteID';
	}
	/**
	 * @brief Geef de waarde van het veld offerteID.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld offerteID van het object obj
	 * representeert.
	 */
	public static function waardeOfferteID(Offerte $obj)
	{
		return static::defaultWaardeInt($obj, 'OfferteID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * offerteID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld offerteID representeert.
	 */
	public static function opmerkingOfferteID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld artikel.
	 *
	 * @param Offerte $obj Het Offerte-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld artikel labelt.
	 */
	public static function labelArtikel(Offerte $obj)
	{
		return 'Artikel';
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld artikel van het object obj
	 * representeert.
	 */
	public static function waardeArtikel(Offerte $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getArtikel())
			return NULL;
		return ArtikelView::defaultWaardeArtikel($obj->getArtikel());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld artikel.
	 *
	 * @see genericFormartikel
	 *
	 * @param Offerte $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formArtikel(Offerte $obj, $include_id = false)
	{
		return static::waardeArtikel($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld artikel. In
	 * tegenstelling tot formartikel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formartikel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld artikel staat en kan
	 * worden bewerkt. Indien artikel read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormArtikel($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld artikel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld artikel representeert.
	 */
	public static function opmerkingArtikel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld leverancier.
	 *
	 * @param Offerte $obj Het Offerte-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld leverancier labelt.
	 */
	public static function labelLeverancier(Offerte $obj)
	{
		return 'Leverancier';
	}
	/**
	 * @brief Geef de waarde van het veld leverancier.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld leverancier van het object
	 * obj representeert.
	 */
	public static function waardeLeverancier(Offerte $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getLeverancier())
			return NULL;
		return LeverancierView::defaultWaardeLeverancier($obj->getLeverancier());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld leverancier.
	 *
	 * @see genericFormleverancier
	 *
	 * @param Offerte $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld leverancier staat en kan
	 * worden bewerkt. Indien leverancier read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLeverancier(Offerte $obj, $include_id = false)
	{
		return static::waardeLeverancier($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld leverancier. In
	 * tegenstelling tot formleverancier moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formleverancier
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld leverancier staat en kan
	 * worden bewerkt. Indien leverancier read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLeverancier($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * leverancier bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld leverancier representeert.
	 */
	public static function opmerkingLeverancier()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld waardePerStuk.
	 *
	 * @param Offerte $obj Het Offerte-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld waardePerStuk labelt.
	 */
	public static function labelWaardePerStuk(Offerte $obj)
	{
		return 'WaardePerStuk';
	}
	/**
	 * @brief Geef de waarde van het veld waardePerStuk.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld waardePerStuk van het object
	 * obj representeert.
	 */
	public static function waardeWaardePerStuk(Offerte $obj)
	{
		return static::defaultWaardeMoney($obj, 'WaardePerStuk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld waardePerStuk.
	 *
	 * @see genericFormwaardePerStuk
	 *
	 * @param Offerte $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waardePerStuk staat en kan
	 * worden bewerkt. Indien waardePerStuk read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWaardePerStuk(Offerte $obj, $include_id = false)
	{
		return static::waardeWaardePerStuk($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld waardePerStuk. In
	 * tegenstelling tot formwaardePerStuk moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formwaardePerStuk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld waardePerStuk staat en kan
	 * worden bewerkt. Indien waardePerStuk read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWaardePerStuk($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * waardePerStuk bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld waardePerStuk representeert.
	 */
	public static function opmerkingWaardePerStuk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vanafDatum.
	 *
	 * @param Offerte $obj Het Offerte-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vanafDatum labelt.
	 */
	public static function labelVanafDatum(Offerte $obj)
	{
		return 'VanafDatum';
	}
	/**
	 * @brief Geef de waarde van het veld vanafDatum.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vanafDatum van het object obj
	 * representeert.
	 */
	public static function waardeVanafDatum(Offerte $obj)
	{
		return static::defaultWaardeDate($obj, 'VanafDatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vanafDatum.
	 *
	 * @see genericFormvanafDatum
	 *
	 * @param Offerte $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vanafDatum staat en kan
	 * worden bewerkt. Indien vanafDatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVanafDatum(Offerte $obj, $include_id = false)
	{
		return static::waardeVanafDatum($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vanafDatum. In
	 * tegenstelling tot formvanafDatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvanafDatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vanafDatum staat en kan
	 * worden bewerkt. Indien vanafDatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVanafDatum($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * vanafDatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vanafDatum representeert.
	 */
	public static function opmerkingVanafDatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vervalDatum.
	 *
	 * @param Offerte $obj Het Offerte-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vervalDatum labelt.
	 */
	public static function labelVervalDatum(Offerte $obj)
	{
		return 'VervalDatum';
	}
	/**
	 * @brief Geef de waarde van het veld vervalDatum.
	 *
	 * @param Offerte $obj Het Offerte-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vervalDatum van het object
	 * obj representeert.
	 */
	public static function waardeVervalDatum(Offerte $obj)
	{
		return static::defaultWaardeDate($obj, 'VervalDatum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vervalDatum.
	 *
	 * @see genericFormvervalDatum
	 *
	 * @param Offerte $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vervalDatum staat en kan
	 * worden bewerkt. Indien vervalDatum read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVervalDatum(Offerte $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'VervalDatum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vervalDatum. In
	 * tegenstelling tot formvervalDatum moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvervalDatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vervalDatum staat en kan
	 * worden bewerkt. Indien vervalDatum read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVervalDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'VervalDatum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * vervalDatum bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vervalDatum representeert.
	 */
	public static function opmerkingVervalDatum()
	{
		return NULL;
	}
}
