<?
abstract class TransactieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TransactieVerzamelingView.
	 *
	 * @param TransactieVerzameling $obj Het TransactieVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTransactieVerzameling(TransactieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
