<?
abstract class iDealKaartjeVerzamelingView_Generated
	extends ArtikelVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in iDealKaartjeVerzamelingView.
	 *
	 * @param iDealKaartjeVerzameling $obj Het iDealKaartjeVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeiDealKaartjeVerzameling(iDealKaartjeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
