<?
abstract class JaargangArtikelView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in JaargangArtikelView.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeJaargangArtikel(JaargangArtikel $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld jaargang.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld jaargang labelt.
	 */
	public static function labelJaargang(JaargangArtikel $obj)
	{
		return 'Jaargang';
	}
	/**
	 * @brief Geef de waarde van het veld jaargang.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld jaargang van het object obj
	 * representeert.
	 */
	public static function waardeJaargang(JaargangArtikel $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getJaargang())
			return NULL;
		return JaargangView::defaultWaardeJaargang($obj->getJaargang());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * jaargang bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld jaargang representeert.
	 */
	public static function opmerkingJaargang()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld artikel.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld artikel labelt.
	 */
	public static function labelArtikel(JaargangArtikel $obj)
	{
		return 'Artikel';
	}
	/**
	 * @brief Geef de waarde van het veld artikel.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld artikel van het object obj
	 * representeert.
	 */
	public static function waardeArtikel(JaargangArtikel $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getArtikel())
			return NULL;
		return ArtikelView::defaultWaardeArtikel($obj->getArtikel());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld artikel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld artikel representeert.
	 */
	public static function opmerkingArtikel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld schattingNodig.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld schattingNodig labelt.
	 */
	public static function labelSchattingNodig(JaargangArtikel $obj)
	{
		return 'SchattingNodig';
	}
	/**
	 * @brief Geef de waarde van het veld schattingNodig.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld schattingNodig van het object
	 * obj representeert.
	 */
	public static function waardeSchattingNodig(JaargangArtikel $obj)
	{
		return static::defaultWaardeInt($obj, 'SchattingNodig');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld schattingNodig.
	 *
	 * @see genericFormschattingNodig
	 *
	 * @param JaargangArtikel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld schattingNodig staat en
	 * kan worden bewerkt. Indien schattingNodig read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formSchattingNodig(JaargangArtikel $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'SchattingNodig', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld schattingNodig. In
	 * tegenstelling tot formschattingNodig moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formschattingNodig
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld schattingNodig staat en
	 * kan worden bewerkt. Indien schattingNodig read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormSchattingNodig($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'SchattingNodig');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * schattingNodig bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld schattingNodig representeert.
	 */
	public static function opmerkingSchattingNodig()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(JaargangArtikel $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(JaargangArtikel $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param JaargangArtikel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(JaargangArtikel $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld verplicht.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld verplicht labelt.
	 */
	public static function labelVerplicht(JaargangArtikel $obj)
	{
		return 'Verplicht';
	}
	/**
	 * @brief Geef de waarde van het veld verplicht.
	 *
	 * @param JaargangArtikel $obj Het JaargangArtikel-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld verplicht van het object obj
	 * representeert.
	 */
	public static function waardeVerplicht(JaargangArtikel $obj)
	{
		return static::defaultWaardeBool($obj, 'Verplicht');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld verplicht.
	 *
	 * @see genericFormverplicht
	 *
	 * @param JaargangArtikel $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verplicht staat en kan
	 * worden bewerkt. Indien verplicht read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVerplicht(JaargangArtikel $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Verplicht', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld verplicht. In
	 * tegenstelling tot formverplicht moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formverplicht
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld verplicht staat en kan
	 * worden bewerkt. Indien verplicht read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVerplicht($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Verplicht');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * verplicht bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld verplicht representeert.
	 */
	public static function opmerkingVerplicht()
	{
		return NULL;
	}
}
