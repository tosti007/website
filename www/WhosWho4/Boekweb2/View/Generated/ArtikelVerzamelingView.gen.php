<?
abstract class ArtikelVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ArtikelVerzamelingView.
	 *
	 * @param ArtikelVerzameling $obj Het ArtikelVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeArtikelVerzameling(ArtikelVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
