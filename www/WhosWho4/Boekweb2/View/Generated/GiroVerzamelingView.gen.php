<?
abstract class GiroVerzamelingView_Generated
	extends TransactieVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in GiroVerzamelingView.
	 *
	 * @param GiroVerzameling $obj Het GiroVerzameling-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeGiroVerzameling(GiroVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
