<?
/**
 * $Id$
 */
abstract class LeverancierView
	extends LeverancierView_Generated
{
	static public function details(Contact $leverancier, $noPage = false)
	{
		$page = self::pageLinks($leverancier);
		$page->start(LeverancierView::waardeNaam($leverancier));

		$page->add($li = new HtmlList());
		$li->addChild(sprintf('%s: %s', _('Email'), LeverancierView::makeEmailLink($leverancier)));
		$li->addChild(sprintf('%s: %s', _('Homepage'), LeverancierView::makeHomepageLink($leverancier)));
		$li->addChild(sprintf('%s: %s', _('Aantal leveringen'), LeverancierView::waardeAantalLeveringen($leverancier)));
		$li->addChild(sprintf('%s: %s', _('Eerste levering'), LeverancierView::waardeEersteLevering($leverancier)));
		$li->addChild(sprintf('%s: %s', _('Laatste levering'), LeverancierView::waardeLaatsteLevering($leverancier)));

		$orderArray = array($leverancier->getOpenOrders(), $leverancier->getGeslotenOrders(), $leverancier->getGeannuleerdOrders());
		$i = 0;
		foreach($orderArray as $orderVerzameling)
		{
			$i++;
			if($orderVerzameling->aantal() > 0)
			{
				switch($i)
				{
				case 1:
					$page->add(new HtmlHeader(3, _('Open orders')));
					break;
				case 2:
					$page->add(new HtmlHeader(3, _('Gesloten orders')));
					break;
				case 3:
					$page->add(new HtmlHeader(3, _('Geannuleerde orders')));
					break;
				}

				$page->add($table = new HtmlTable(null, 'table-condensed'));
				$thead = $table->addHead();
				$tbody = $table->addBody();

				$row = $thead->addRow();
				$headerArray = array(_('Geplaatst'), _('Artikel'), _('Aantal'), ('Waarde'));
				if(hasAuth('boekcom'))
					$headerArray[] = '';
				$row->makeHeaderFromArray($headerArray);

				foreach($orderVerzameling as $order)
				{
					$row = $tbody->addRow();

					$row->addData(OrderView::waardeDatumGeplaatst($order));
					$row->addData(OrderView::waardeArtikel($order));
					$row->addData(OrderView::waardeAantal($order));
					$row->addData(OrderView::waardeWaardePerStuk($order));

					if(hasAuth('boekcom'))
					{
						$url = $order->getArtikel()->url() . '/Orders/' . $order->geefID();
						$row->addData('(' . new HtmlAnchor($url . '/Levering', _('Levering'))
							. ', ' . new HtmlAnchor($url . '/Wijzig', _('Wijzig'))
							. ', ' . new HtmlAnchor($url . '/Annuleer', _('Annuleer')) . ')');
					}
				}
			}
		}

		$page->end();
	}

	static public function wijzigForm(Leverancier $leverancier, $nieuw = false, $show_error = false)
	{
		$page = Page::getInstance();

		if($nieuw)
			$page->start(_("Leverancier toevoegen"));
		else
			$page->start(_("Leverancier wijzigen"));

		$page->add($form = HtmlForm::named('leverancierForm'));

		$form->add(self::wijzigTable($leverancier, $show_error));

		$form->add(HtmlInput::makeFormSubmitButton(_("Voer in")));

		$page->end();
	}

	static public function wijzigTable(Leverancier $leverancier, $show_error)
	{
		$tbody = new HtmlDiv();
		$tbody->add(self::wijzigTR($leverancier, 'naam', $show_error));
		$tbody->add(self::wijzigTR($leverancier, 'email', $show_error));
		$tbody->add(self::wijzigTR($leverancier, 'homepage', $show_error));
		$tbody->add(self::wijzigTR($leverancier, 'dictaten', $show_error));
		$tbody->add(self::wijzigTR($leverancier, 'dibsproducten', $show_error));
		$tbody->add(self::wijzigTR($leverancier, 'colaproducten', $show_error));
		return $tbody;
	}

	static public function processNieuwForm(Leverancier $leverancier)
	{
		if(!$data = tryPar(self::formobj($leverancier), NULL))
			return false;

		self::processForm($leverancier);
	}

	static public function waardeAantalLeveringen (Leverancier $leverancier)
	{
		return static::defaultWaardeInt($leverancier, 'AantalLeveringen');
	}

	static public function waardeEersteLevering (Leverancier $leverancier)
	{
		if (!is_null($leverancier->getEersteLevering()))
			return LeveringView::waardeWanneer($leverancier->getEersteLevering(), 'wanneer');
		return;
	}

	static public function waardeLaatsteLevering (Leverancier $leverancier)
	{
		if (!is_null($leverancier->getLaatsteLevering()))
			return LeveringView::waardeWanneer($leverancier->getLaatsteLevering(), 'wanneer');
		return;
	}

	static public function detailsLink (Leverancier $leverancier)
	{
		return new HtmlAnchor($leverancier->url(),
				self::waardeNaam($leverancier));
	}

	static public function labelDictaten(Leverancier $obj)
	{
		return _('Kan dictaten leveren');
	}
	static public function labelDibsproducten(Leverancier $obj)
	{
		return _('Kan dibsproducten leveren');
	}
	static public function labelColaproducten(Leverancier $obj)
	{
		return _('Kan colaproducten leveren');
	}
	static public function opmerkingEmail()
	{
		return _('Optioneel');
	}
	static public function opmerkingHomepage()
	{
		return _('Optioneel');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
