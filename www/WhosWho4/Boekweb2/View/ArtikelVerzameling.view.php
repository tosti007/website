<?
/**
 * $Id$
 */
abstract class ArtikelVerzamelingView
	extends ArtikelVerzamelingView_Generated
{
	static public function tabel (ArtikelVerzameling $artikelen)
	{
		$div = new HtmlDiv();

		$div->add($form = new HtmlForm("GET"));
		$form->add($box = new HtmlSelectbox('artikel'));
		$box->setAutoSubmit();
		$box->add(new HtmlSelectboxOption('', _("Alle soorten")))
			->add(new HtmlSelectboxOption('Artikel', _("Artikel")))
			->add(new HtmlSelectboxOption('AesArtikel', _("AesArtikel")))
			->add(new HtmlSelectboxOption('Boek', _("Boek")))
			->add(new HtmlSelectboxOption('DibsProduct', _("DibsProduct")))
			->add(new HtmlSelectboxOption('Dictaat', _("Dictaat")))
			->add(new HtmlSelectboxOption('iDealKaartje', _("iDealKaartje")));
		$box->select(tryPar('artikel', ''));

		if($artikelen->aantal() == 0) {
			$div->add(new HtmlEmphasis(_("Er zijn geen artikelen gevonden")));
			return $div;
		}

		$class = false;
		foreach ($artikelen as $artikel)
		{
			$newclass = get_class($artikel) . 'View';
			if($class != $newclass) {
				$class = $newclass;
				$div->add(new HtmlHeader(3, get_class($artikel)));
				$div->add($table = new HtmlTable());

				$table->add($thead = new HtmlTableHead())
					  ->add($tbody = new HtmlTableBody());

				$thead->add($class::tableHeaderRow());
			}

			$tbody->add($class::tableRow($artikel));
		}

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
