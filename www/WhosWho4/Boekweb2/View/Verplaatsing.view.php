<?
/**
 * $Id$
 */
abstract class VerplaatsingView
	extends VerplaatsingView_Generated
{
	static public function wijzigForm (Verplaatsing $verplaatsing, $show_error)
	{
		$voorraad = $verplaatsing->getVoorraad();
		$page = Page::getInstance()->start(_("Voorraad verplaatsen"));
		$page->add(new HtmlParagraph(sprintf(_("Klik %s om terug naar de voorraad te gaan."), new HtmlAnchor($voorraad->url(), _("hier")))));

		$page->add($table = new HtmlDiv());
		$table->add(VoorraadView::infoTR($voorraad, 'aantal'));
		$table->add(VoorraadView::infoTR($voorraad, 'locatie'));

		$page->add($form = HtmlForm::named('verplaatsenForm'));

		$form->add(self::wijzigTR($verplaatsing, 'aantal', $show_error));
		$form->add(self::wijzigTR($verplaatsing, 'tegenvoorraad', $show_error));

		$form->add(Htmlinput::makeFormSubmitButton(_("Verplaats")));

		$page->end();
	}

	static public function processNieuwForm(Verplaatsing $verplaatsing)
	{
		$voorraad = $verplaatsing->getVoorraad();
		$tegenvoorraad = Voorraad::zoek($voorraad->getArtikel(), tryPar('locatie', 'BW1'), $voorraad->getWaardePerStuk(), $voorraad->getBtw());
		$verplaatsing = new Verplaatsing($verplaatsing->getVoorraad(), new DateTimeLocale(), -1 * tryPar('aantal', 0), $tegenvoorraad);

		return $verplaatsing;
	}

	static public function formTegenvoorraad(Verplaatsing $verplaatsing, $include_id = false)
	{
		$select = new HtmlSelectbox("locatie");
		foreach(VoorraadView::labelenumLocatieArray() as $id => $label) {
			$select->add(new HtmlSelectboxOption($id, $label));
		}
		$select->select(tryPar("locatie"));
		return $select;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
