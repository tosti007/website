<?
abstract class BestellingView
	extends BestellingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	public static function tableHeaderRow() {
		$tr = new HtmlTableRow();
		$tr->addHeader(_('Aantal'));
		$tr->addHeader(_('Artikel'));
		$tr->addHeader(_('Reserveerder'));
		$tr->addHeader(_('Geplaatst'));
		$tr->addHeader(_('Vervalt'));
		$tr->addHeader(_('Status'));

		return $tr;
	}

	public static function tableRow(Bestelling $bestelling) {
		$tr = new HtmlTableRow();
		$tr->addData(self::waardeAantal($bestelling));
		$tr->addData(ArtikelView::detailsLink($bestelling->getArtikel()));
		$tr->addData(PersoonView::makeLink($bestelling->getPersoon()));
		$tr->addData(self::waardeDatumGeplaatst($bestelling));
		$tr->addData(self::waardeVervalDatum($bestelling));
		$tr->addData(self::waardeStatus($bestelling));
		return $tr;
	}
}

