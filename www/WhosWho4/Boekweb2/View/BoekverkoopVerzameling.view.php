<?
abstract class BoekverkoopVerzamelingView
	extends BoekverkoopVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function verkoopGeschiedenis(BoekverkoopVerzameling $verkopen)
	{
		$page = Page::getInstance();
		$page->start("Boekverkoopgeschiedenis");

		$page->add($table = new HtmlTable(null, 'table-condensed'));
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add(BoekverkoopView::tableHeaderRow());

		foreach($verkopen as $verkoop) {
			$tbody->add(BoekverkoopView::tableRow($verkoop));
		}

		$page->end();
	}

	static public function boekverkoop(BoekverkoopVerzameling $verkopen, TransactieVerzameling $transacties)
	{
		$page = Page::getInstance();
		$page->start(sprintf(_("Verkopen van dag %s"), $verkopen->first()->getVerkoopdag()));

		$page->add(new HtmlHeader(3, _("Overzicht van de geopende boekverkopen")));
		$page->add($table = new HtmlTable());
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add(BoekverkoopView::tableHeaderRow());

		foreach($verkopen as $verkoop) {
			$tbody->add(BoekverkoopView::tableRow($verkoop));
		}

		$page->add(new HtmlHeader(3, _("Verkopen per product")));

		//TODO: hier moet een lijst van verkopen per product komen

		$page->end();
	}
}
