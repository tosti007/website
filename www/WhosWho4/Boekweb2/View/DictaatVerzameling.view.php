<?
/**
 * $Id$
 */
abstract class DictaatVerzamelingView
	extends DictaatVerzamelingView_Generated
{
	static public function lijst($dictaten)
	{
		$page = Page::getInstance()->start(_("Dictatenlijst"));

		if ($dictaten->aantal() == 0) {
			$page->add(new HtmlDiv(_("Er zijn geen dictaten gevonden."), 'alert alert-danger'));
		} else {
			$page->add($table = new HtmlTable(null, 'dictaten'));
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableHeaderCell(_("Naam")))
				->add(new HtmlTableHeaderCell(_("Auteur")))
				->add(new HtmlTableHeaderCell(_("Versie")))
				->add(new HtmlTableHeaderCell(_("Begin gebruik")))
				->add(new HtmlTableHeaderCell(_("Einde gebruik")))
				->add(new HtmlTableHeaderCell(_("Downloaden")));

			foreach($dictaten as $dictaat) {
				$table->add($row = new HtmlTableRow());
				$row->add(new HtmlTableDataCell(DictaatView::detailsLink($dictaat)))
					->add(new HtmlTableDataCell(PersoonView::naam($dictaat->getAuteur())))
					->add(new HtmlTableDataCell(DictaatView::waardeUitgaveJaar($dictaat)))
					->add(new HtmlTableDataCell(DictaatView::waardeBeginGebruik($dictaat)))
					->add(new HtmlTableDataCell(DictaatView::waardeEindeGebruik($dictaat)))
					->add(new HtmlTableDataCell(DictaatView::downloadLink($dictaat)));
			}
		}
		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
