<?
abstract class TellingVerzamelingView
	extends TellingVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	public static function tellingen(TellingVerzameling $tellingen)
	{
		$page = Page::getInstance()->start(_('Alle tellingen'));

		$page->add(self::makeTable($tellingen, array('link'), array(_('Telling'))));

		$page->end();
	}
}
