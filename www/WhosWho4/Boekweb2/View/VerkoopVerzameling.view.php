<?
/**
 * $Id$
 */
abstract class VerkoopVerzamelingView
	extends VerkoopVerzamelingView_Generated
{
	static public function verkoop (Dictaat $dictaat, VerkoopVerzameling $verkopen)
	{
		$page = Page::getInstance()->start(sprintf(_("Verkopen van %s"), DictaatView::waardeNaam($dictaat)));

		$page->add(HtmlAnchor::button($dictaat->url(), _('Terug naar de dictaatpagina')));

		$page->add(new HtmlHeader(2, _("Verkopen geaggregeerd van alle varianten")));
		$page->add(new HtmlDiv($div = new HtmlDiv(null, 'col-md-6'), 'row'));
		$div->add($ul = new HtmlList());
		$ul->add(new HtmlListItem(sprintf(_("Totaal verkocht: %s"), VerkoopVerzamelingView::aantal($verkopen))));
		$ul->add(new HtmlListItem(sprintf(_("Omzet: %s"), VerkoopVerzamelingView::omzet($verkopen))));

		$page->add($table = new HtmlTable());
		$table->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell(_("Wanneer")))
			->add(new HtmlTableHeaderCell(_("Aan wie")))
			->add(new HtmlTableHeaderCell(_("Aantal")))
			->add(new HtmlTableHeaderCell(_("Prijs per stuk")))
			->add(new HtmlTableHeaderCell(_("Totaalprijs")));

		foreach($verkopen as $verkoop) {
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableDataCell(VerkoopView::waardeWanneer($verkoop)))
				->add(new HtmlTableDataCell(ContactView::naam($verkoop->getTransactie()->getContact())))
				->add(new HtmlTableDataCell(VerkoopView::aantalVerkocht($verkoop)))
				->add(new HtmlTableDataCell(VerkoopPrijsView::waardePrijs($verkoop->getVoorraad()->getVerkoopPrijs())))
				->add(new HtmlTableDataCell(VoorraadView::waardePrijs($verkoop->getVoorraad(), -1 * $verkoop->getAantal())));
		}

		$page->end();
	}

	static public function aantal (VerkoopVerzameling $verkopen)
	{
		$aantal = 0;
		foreach ($verkopen as $verkoop)
			$aantal += -1 * $verkoop->getAantal();
		return $aantal;
	}

	static public function omzet (VerkoopVerzameling $verkopen)
	{
		$omzet = 0;
		foreach ($verkopen as $verkoop)
			$omzet += -1 * $verkoop->getAantal() * $verkoop->getVoorraad()->getVerkoopPrijs()->getPrijs();
		return Money::addPrice($omzet);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
