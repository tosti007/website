<?
abstract class VakStudieVerzamelingView
	extends VakStudieVerzamelingView_Generated
{
	/**
	 * \brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in VakStudieVerzamelingView.
	 *
	 * \param obj Het VakStudieVerzameling-object waarvan de waarde kregen moet worden.
	 * \return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVakStudieVerzameling(VakStudieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
