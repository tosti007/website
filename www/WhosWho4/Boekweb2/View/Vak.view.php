<?
/**
 * $Id$
 */
abstract class VakView
	extends VakView_Generated
{
	static public function defaultWaardeVak(Vak $vak)
	{
		return self::waardeNaam($vak);
	}
	static public function makeLink (Vak $vak)
	{
		return self::waardeCode($vak);
	}

	static public function labelInTentamenLijsten (Vak $vak) {
		return _("In tentamenlijsten");
	}
	static public function labelTentamensOutdated (Vak $vak) {
		return _("Outdated");
	}
	static public function opmerkingTentamensOutdated () {
		return _("Op de tentamenspagina moet je een vinkje aanzetten om outdated vakken te zien.");
	}
	static public function labelVakcode(Vak $vak) {
		return _("Vakcode");
	}

	static public function labelStudies(Vak $vak) {
		return _("Studies");
	}

	// Zie Basis/View.abs.php
	static public function viewInfo($vak, $header = false) {
		//Zoek uit welke class we moeten gebruiken: pak de class van de view zonder view.
		$class = substr(get_called_class(), 0, -4);

		$tbl = new HtmlDiv(null, 'info-table');
		foreach($class::velden('viewInfo') as $veld)
		{
			$tbl->add(self::call('infoTR', $veld, $vak));
		}
		$tbl->add(self::call('infoTR', 'Studies', $vak));

		return new HtmlDiv(
			array(
				($header ? new HtmlDiv(new HtmlStrong(get_class($vak)), 'panel-heading') : ''),
				new HtmlDiv($tbl, 'panel-body')
			)
			, 'panel panel-default'
		);
	}

	/**
	 * Geef een pagina met informatie over een vak,
	 * inclusief jaargangen en tentamens.
	 *
	 * @return HTMLPage
	 */
	public static function vakInfo (Vak $vak, JaargangVerzameling $jaargangen,
		TentamenVerzameling $tents)
	{
		$page = static::pageLinks($vak);
		$page->start(sprintf(_("Vak %s"), $vak->getCode()));
		$page->add(VakView::viewInfo($vak));
		$page->add(HtmlAnchor::button('/Onderwijs/Tentamens', _('Terug')));

		if (hasAuth('bestuur'))
		{
			$page->add($div = new HtmlDiv());
			$div->add(new HtmlHeader(3, _("Jaargangen")));

			$div->add($knopDiv = new HtmlDiv(null, 'btn-group'));
			$knopDiv->add(HtmlAnchor::button($vak->url() . 'Jaargang',
				_("Jaargangoverzicht"))
			);
			$knopDiv->add(HtmlAnchor::button($vak->url() . 'Jaargang/Toevoegen',
				_("Jaargang toevoegen"))
			);

			$div->add(JaargangVerzamelingView::toTable($jaargangen));
		}

		$page->add(new HtmlHeader(3, _("Tentamens")));

		if (hasAuth('tbc'))
		{
			$page->add($div = new HtmlDiv(null, 'btn-group'));
			$div->add(HtmlAnchor::button($vak->url() . 'TentamenToevoegen', _('Tentamen toevoegen')));
			$div->add(HtmlAnchor::button('/Onderwijs/Vak/Controleren', _('Uitwerkingen controleren')));
		}

		$page->add(TentamenVerzamelingView::toTable($tents));
		return $page;
	}

	static public function processNieuwForm(Vak $vak, &$studies)
	{
		self::processForm($vak);
		$studies = StudieVerzameling::verzamel(tryPar('studies', array()));
	}

	/**
	 * Geef een pagina om dit Vak te wijzigen.
	 *
	 * @param Vak $vak Het vak om te wijzigen.
	 * @param string $paginaTitel De titel van de pagina om te tonen.
	 * @param string $formName De naam van het form (argument van HtmlForm::named()).
	 * @param bool $show_error Of fouten in het form getoond moeten worden?
	 * Dit is in principe pas zo als de gebruiker op submit heeft geklikt.
	 *
	 * @return HTMLPage
	 */
	static public function wijzigForm(Vak $vak, $paginaTitel, $formName, $show_error = true) {
		$page = new HTMLPage();
		$page->start($paginaTitel);
		$page->add($form = HtmlForm::named($formName));

		$form->add(self::createForm($vak, $show_error));
		$form->add(self::wijzigTR($vak, 'Studies', $show_error));

		$form->add(HtmlInput::makeFormSubmitbutton(_("Ga ervoor!")));

		return $page;
	}

	static public function waardeDepartement(Vak $obj)
	{
		global $DEPARTEMENTNAMEN;
		if(empty(parent::waardeDepartement($obj)))
		{
			return "";
		}
		return $DEPARTEMENTNAMEN[strtolower(parent::waardeDepartement($obj))];
	}

	static public function formStudies(Vak $vak)
	{
		$studies = VakStudieVerzameling::fromVak($vak)->toStudieVerzameling();
		$ids = array();
		foreach ($studies as $key => $value)
			$ids[] = $key;

		$box = StudieVerzamelingView::alleStudiesSelect('studies', $ids);
		$box->setMultiple(true);
		return $box;
	}

	static public function opmerkingStudies(Vak $vak) { return NULL; }
	
	static public function waardeStudies(Vak $vak)
	{
		$studies = VakStudieVerzameling::fromVak($vak)->toStudieVerzameling();
		if ($studies->aantal() == 0)
			return new HtmlEmphasis("Geen");
		$ret = array();
		foreach ($studies as $key => $value) {
			$ret[] = $value->getNaam() . " (" . $value->getFase() . ")";
		}
		return implode($ret, ', ');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
