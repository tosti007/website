<?
abstract class iDealAntwoordView
	extends iDealAntwoordView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	public static function formAntwoord(iDealAntwoord $obj, $include_id = false)
	{
		$name = 'iDealAntwoord['.$obj->getVraagActiviteitID().'_'.$obj->getVraagVraagID().'][Antwoord]';
		if($obj->getVraag()->getType() == 'ENUM')
		{
			$opties = $obj->getVraag()->getOpties();
			$opties = array(null => _('-- Selecteer een antwoord --')) + $opties;
			return HtmlSelectbox::fromArray($name, $opties, tryPar($name, array()));
		}
		else
			return HtmlInput::makeText($name, tryPar($name, ''));
	}

	static public function processForm($obj, $velden = 'viewWijzig', $data = null, $include_id = false)
	{
		parent::processForm($obj, $velden, $data, $include_id);

		if($obj->getVraag()->getType() == 'ENUM')
		{
			$opties = $obj->getVraag()->getOpties();

			if(isset($opties[$data['Antwoord']]))
				$obj->setAntwoord($opties[$data['Antwoord']]);
			else
				$obj->setAntwoord('');
		}

		return $obj->valid();
	}
}
