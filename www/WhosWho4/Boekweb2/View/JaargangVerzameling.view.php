<?php

abstract class JaargangVerzamelingView
	extends JaargangVerzamelingView_Generated
{
	/**
	 * Geef een pagina met informatie over al de jaargangen.
	 *
	 * @param JaargangVerzameling $jaargangen
	 * @param Vak|null $vak
	 * Indien niet null, horen al de jaargangen bij dit vak.
	 *
	 * @return HTMLPage
	 */
	public static function infopagina(JaargangVerzameling $jaargangen, $vak = null)
	{
		$page = new HTMLPage();
		$page->start();

		if ($vak)
		{
			$page->add($knopDiv = new HtmlDiv(null, 'btn-group'));
			$knopDiv->add(HtmlAnchor::button($vak->url(),
				_("Terug naar het vak"))
			);
			$knopDiv->add(HtmlAnchor::button($vak->url() . 'Jaargang/Toevoegen',
				_("Nieuwe jaargang toevoegen voor dit vak"))
			);
		}

		$page->add(static::toTable($jaargangen));

		return $page;
	}

	public static function toTable(JaargangVerzameling $jaargangen)
	{
		$table = new HtmlTable();
		$table->add($thead = new HtmlTableHead());
		$table->add($tbody = new HtmlTableBody());

		$thead->add($tr = new HtmlTableRow());
		$tr->addHeader(_('Jaargang-ID'));
		$tr->addHeader(_('Contactpersoon'));
		$tr->addHeader(_('Begindatum'));
		$tr->addHeader(_('Einddatum'));
		if(hasAuth('bestuur')) {
			$tr->addHeader(_('Inschrijvingen'));
		}

		foreach($jaargangen as $jg) {
			$tbody->add($tr = new HtmlTableRow());
			$tr->addData(new HtmlAnchor(
				$jg->url(),
				JaargangView::waardeJaargangID($jg))
			);
			$tr->addData(JaargangView::waardeContactPersoon($jg));
			$tr->addData(JaargangView::waardeDatumBegin($jg));
			$tr->addData(JaargangView::waardeDatumEinde($jg));
			if(hasAuth('bestuur')) {
				$tr->addData(JaargangView::waardeInschrijvingen($jg));
			}
		}

		return $table;
	}
}
