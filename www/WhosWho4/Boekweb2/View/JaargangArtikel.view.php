<?php
declare(strict_types=1);

abstract class JaargangArtikelView
	extends JaargangArtikelView_Generated
{
	/**
	 * Pagina met alle informatie van dit JaargangArtikel.
	 *
	 * @param JaargangArtikel $jgArtikel
	 *
	 * @return HTMLPage
	 */
	public static function infopagina(JaargangArtikel $jgArtikel)
	{
		$page = static::pageLinks($jgArtikel);
		$page->start(sprintf(_("Info over %s[VOC: artikel] bij %s[VOC: vak]"),
			ArtikelView::waardeNaam($jgArtikel->getArtikel()),
			VakView::waardeNaam($jgArtikel->getJaargang()->getVak())
		));

		$page->add($knopDiv = new HtmlDiv(null, 'btn-group'));
		$knopDiv->add(HtmlAnchor::button($jgArtikel->getJaargang()->url(),
			_("Terug naar de Jaargang"))
		);
		$knopDiv->add(HtmlAnchor::button($jgArtikel->getArtikel()->url(),
			_("Terug naar het Artikel"))
		);

		$page->add(static::viewInfo($jgArtikel));

		return $page;
	}
}
