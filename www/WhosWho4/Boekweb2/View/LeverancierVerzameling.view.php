<?
/**
 * $Id$
 */
abstract class LeverancierVerzamelingView
	extends LeverancierVerzamelingView_Generated
{
	static public function geefSelectBox($selectid)
	{
		$leveranciers = LeverancierVerzameling::geefAlleLeveranciers();

		$selectbox = new HtmlSelectbox('leverancier');
		$selectbox->add(new HtmlSelectboxOption(null, ''));

		foreach($leveranciers as $l)
		{
			$selectbox->add(new HtmlSelectboxOption($l->geefID(), LeverancierView::waardeNaam($l)));
		}
		$selectbox->select($selectid);

		return $selectbox;
	}

	static public function leverancierOverzicht(LeverancierVerzameling $leveranciers)
	{
		$page = Page::getInstance()->start(_("Alle leveranciers"));

		$page->add(HtmlAnchor::button(BOEKWEBBASE . 'Leverancier/Nieuw', _("Leverancier toevoegen")));

		$page->add(new HtmlDiv(new HtmlDiv($ul = new HtmlList(), 'col-md-6'), 'row'));

		foreach($leveranciers as $l)
		{
			$ul->add(new HtmlListItem(new HtmlAnchor($l->url(), LeverancierView::waardeNaam($l))));
		}

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
