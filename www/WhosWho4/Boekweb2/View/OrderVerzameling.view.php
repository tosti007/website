<?
/**
 * $Id$
 */
abstract class OrderVerzamelingView
	extends OrderVerzamelingView_Generated
{
	static public function geefSelectbox($selectid)
	{
		$orders = OrderVerzameling::geefAlleOrders();

		$selectbox = new HtmlSelectbox('order');
		$selectbox->add(new HtmlSelectboxOption(null, ''));

		foreach($orders as $order)
		{
			$selectbox->add(new HtmlSelectboxOption($order->geefID(), $order->geefID()));
		}
		$selectbox->select($selectid);

		return $selectbox;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
