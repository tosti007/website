<?php
define('BOEKEN_TOEVOEGENFORM_CONTACT', 1);

abstract class Leveranciers_Controller
{
	static public function lijst ()
	{
		$leveranciers = new LeverancierVerzameling();
		$leveranciers->union(LeverancierVerzameling::dibsproductenLeveranciers());
		$leveranciers->union(LeverancierVerzameling::dictatenLeveranciers());

		LeverancierVerzamelingView::leverancierOverzicht($leveranciers);
	}

	static public function toevoegen ()
	{
		$leverancier = new Leverancier();
		self::wijzigForm($leverancier, false);
	}

	static public function wijzigen()
	{
		$id = vfsVarEntryName();
		if(!$leverancier = Leverancier::geef($id))
			spaceHttp(403);

		self::wijzigForm($leverancier, false);
	}

	static public function wijzigForm(Leverancier $leverancier, $nieuw = false)
	{
		$show_error = false;

		if(Token::processNamedForm() == 'leverancierForm')
		{
			LeverancierView::processNieuwForm($leverancier);

			if($leverancier->valid())
			{
				$leverancier->opslaan();

				Page::redirectMelding($leverancier->url(), _('Gelukt!'));
			}
			else
			{
				$show_error = true;
				Page::addMelding(_('Er waren fouten'), 'fout');
			}
		}

		LeverancierView::wijzigForm($leverancier, $nieuw, $show_error);
	}

	static public function LeverancierEntry ($args)
	{
		$leverancier = Leverancier::geef($args[0]);
		if (!$leverancier) return false;

		return array('name' => $leverancier->geefID(),
				'access' => true);
	}

	static public function details ()
	{
		$id = (int) vfsVarEntryName();
		$leverancier = Leverancier::geef($id);

		LeverancierView::details($leverancier);
	}

	static public function verwijderen()
	{
		$id = vfsVarEntryName();
		if(!$leverancier = Leverancier::geef($id))
			spaceHttp(403);

		if(Token::processNamedForm() == 'LeverancierVerwijderen')
		{
			$error = $leverancier->verwijderen();
			if(!is_null($error))
				Page::addMeldingArray($error, 'fout');
			else
				Page::redirectMelding(BOEKWEBBASE . 'Leverancier/', _('De leverancier is weggeflikkerd'));
		}

		$page = Page::getInstance()->start('Wegflikkeren');
		$page->add(LeverancierView::verwijderForm($leverancier));
		$page->end();
	}
}
