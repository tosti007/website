<?php

abstract class Mail_Controller {
	/**
	 *  Pagina om dictaat door te mailen naar de xerox.
	 */
	public static function xeroxmail() {
		// Check eerst dat we een mail van een dictaat versturen.
		$id = (int) vfsVarEntryName();
		$dictaat = Dictaat::geef($id);

		$page = Page::getInstance()->start();

		if (Token::processNamedForm("stuurMail")) {
			self::stuurXeroxMail($page, $dictaat);
		}

		$page->add(DictaatView::xeroxMailForm($dictaat));

		$page->add(new HtmlParagraph(sprintf(
			_("Terug naar %s[VOC: detailslink van artikel]"),
			DictaatView::detailsLink($dictaat))));
		$page->end();
	}

	/**
	 *  Stuur de mail uit het xeroxMailForm naar de Xerox.
	 *
	 * Bij het mails sturen wordt de pagina gevuld met statusinformatie.
	 *
	 * @param page De HtmlPage waar we bij het versturen onze status aan toe mogen voegen.
	 * @param dictaat Het dictaat waar we over mailen.
	 *
	 * @return Of alles met succes is afgerond.
	 */
	private static function stuurXeroxMail($page, Dictaat $dictaat)
	{
		global $filesystem, $BESTUUR;

		$page->add($span = new HtmlParagraph(_("Stap 1: informatie uit het dictaat inlezen... ")));

		$pdfnaam = "dictaat-" . $dictaat->geefId() . ".pdf";
		$aantal = (int)tryPar("aantal");
		$kaftsoort = tryPar("kaftsoort");
		if (!$aantal) {
			Page::addMelding(sprintf(_("Je moet een aantal invullen, niet '%s'!"), tryPar("aantal")), 'fout');
			return false;
		}
		if (!$kaftsoort) {
			Page::addMelding(sprintf(_("Je moet een kaftsoort (bijvoorbeeld kleur) invullen, niet '%s'!"), $kaftsoort), 'fout');
			return false;
		}
		$bestand = $dictaat->geefAbsoluutPad();
		if (!$filesystem->has($bestand)) {
			Page::addMelding(_("Kan geen PDF-bestand voor het dictaat vinden, upload die eerst!"), 'fout');
			return false;
		}

		$span->add(_("succes!"));

		// TODO: dit kunnen we misschien beter samenvoegen met Mail_Controller::sendMail
		$page->add($span = new HtmlParagraph(_("Stap 2: mail in elkaar zetten... ")));
		$mail = new Email();
		$mail->setSubject(sprintf("Dictaat %s", DictaatView::waardeNaam($dictaat)));
		$mail->setFrom($BESTUUR['boekencommissaris']);
		$mail->setTo(Register::getValue("bw2_dictaatOrderAdres"));
		// TODO: willen we meteen fancy html-mails mogelijk maken?
		$mail->setBody(Template::render(BW2_XEROXMAIL_TEMPLATE, array(
			"pdfnaam" => $pdfnaam,
			"aantal" => $aantal,
			"kaftsoort" => $kaftsoort,
		)), false);
		$mail->setAttachmentFromFile("application/pdf", FILESYSTEM_PREFIX . $bestand, $pdfnaam);
		$span->add(_("succes!"));

		$page->add($span = new HtmlParagraph(_("Stap 3: mail versturen... ")));
		$mail->send();
		$span->add(_("succes!"));
	}

	/**
	 * Pagina om docenten te vragen of vakdata nog klopt.
	 *
	 * In principe wordt deze pagina drie keer aangeroepen:
	 *
	 * aanroep 1
	 *    De boekcom selecteert de periode om over te mailen
	 *    (deze komt in de POST-parameter 'jaarperiode')
	 *    (alle jaargangen en boeken van deze periode zijn al ingevoerd).
	 *    De boekcom klikt op de "Genereer en toon"-knop.
	 *
     *    Nu wordt een form gepost met naam stuurMail,
	 *    en parameter preview op true,
	 *    dit zorgt voor:
	 * aanroep 2
	 *    De boekcom controleert de e-mails en klikt op de "Verstuur"-knop.
	 *
	 *    Nu wordt een form gepost met naam stuurMail
	 *    en parameter preview op false,
	 *    dit zorgt voor:
	 * aanroep 3
	 *    De mails worden nu verstuurd.
	 *
	 * @site{/Onderwijs/Boekweb/Docentenmail}
	 */
	public static function docentenmail() {
		$output = null;
		if (Token::processNamedForm("stuurMail"))
		{
			$output = new HtmlDiv();
			$preview = tryPar('preview', false);
			$succes = self::stuurDocentenMail($output, $preview);
			$magEchtVersturen = $preview && $succes;
		}
		else
		{
			// Kijk eerst naar een preview!
			$magEchtVersturen = false;
		}

		$page = new HTMLPage();
		$page->start();
		if ($output)
		{
			$page->add($output);
		}
		if ($magEchtVersturen)
		{
			$page->add($form = HtmlForm::named('stuurMail'));
			$form->add(HtmlInput::makeHidden('jaarperiode', tryPar('jaarperiode')));
			$form->add(HtmlInput::makeHidden('preview', 0));
			$form->add(HtmlInput::makeSubmitButton(_("Deze voorbeeldmails zijn goed, de deur uit ermee!")));
		}
		else
		{
			$page->add(self::docentenMailForm());
		}
		return new PageResponse($page);
	}

	/**
	 * Voeg een formulier toe aan de pagina om docenten te mailen.
	 *
	 * @return HtmlElement
	 * Een formulier, klaar om te adden.
	 */
	private static function docentenMailForm()
	{
		global $PERIODES;
		$form = HtmlForm::named("stuurMail");
		$form->add(new HtmlHeader(3, _("Verstuur een mail aan docenten:")));

		// Bepaal welke periodes relevant zijn.
		$begindatum = new DateTimeLocale();
		$einddatum = new DateTimeLocale();
		$einddatum->add(new DateInterval('P1Y'));
		$periodes = PeriodeVerzameling::tussen($begindatum, $einddatum);

		$form->addImmutable(new HtmlParagraph(_(
			"Met onderstaande knop kun je controlemails genereren voor docenten ".
			"met daarin de vraag of de informatie die A&ndash;Eskwadraat heeft ".
			"over de boeken bij een vak correct is. Er wordt gemaild over elk ".
			"vak dat begint in de geselecteerde (komende) periode."
		)));

		// Toon een preview van de mail die gemaild gaat worden
		$form->addImmutable(new HtmlHeader(4, sprintf(
			_("Template voor de mail: %s[VOC: (wijzig-link)]"),
			new HtmlAnchor(Template::geefVanLabel(BW2_DOCENTMAIL_TEMPLATE)->wijzigURL(), _("(wijzig)"))
		)));
		$form->addImmutable(new HtmlPre(Template::geefOngerenderd(BW2_DOCENTMAIL_TEMPLATE)));

		// Bepaal welke periodes zinnig zijn om over te mailen
		$optimalePeriode = null;
		foreach ($periodes as $periode)
		{
			if (!$periode->isToekomstig())
			{
				continue;
			}
			$jaargangen = JaargangVerzameling::verzamelHuidige($periode->getDatumBegin(), $periode->getDatumEind());
			$periodeNaam = $periode->getCollegejaar() . "-" . $periode->getPeriodeNummer();
			$periodeOmschrijving = new HtmlParagraph($periodeNaam . ': ');
			foreach ($jaargangen as $jaargang)
			{
				$periodeOmschrijving->add($jaargang->getVak()->getCode() . ', ');
			}

			$mogelijkePeriodes[$periodeNaam] = $periodeOmschrijving;
			if (is_null($optimalePeriode))
			{
				$optimalePeriode = $periodeNaam;
			}
		}
		$form->addImmutable(new HtmlLabel("jaarperiode", _("Mail voor periode:[VOC: stuur docenten een mail over de vakken die ze geven in periode:]")));
		$form->addImmutable(HtmlInput::makeRadioFromArray("jaarperiode", $mogelijkePeriodes, $optimalePeriode), tryPar('jaarperiode'));
		$form->add(HtmlInput::makeHidden('preview', 1));
		$form->add(HtmlInput::makeSubmitButton(_("Genereer de e-mails en laat ze me zien!")));

		$form->addImmutable(new HtmlAnchor('/Onderwijs/Vak', _("Klik op mij om vakken te beheren!")));
		return $form;
	}

	/**
	 *  Laad uit de formdata de periode waarover we mails moeten sturen.
	 *
	 * @param par De form-parameter waar de periode in staat, of 'jaarperiode' als default.
	 * @return Periode|null
	 * Een Periode-object, of NULL.
	 */
	private static function getJaarPeriode($par = "jaarperiode") {
		$jaarperiode = tryPar($par, NULL);
		if (!$jaarperiode) {
			return NULL;
		}
		// formaat moet zijn $jaar-$periode
		$parts = explode("-", $jaarperiode);
		if (count(explode("-", $jaarperiode)) != 2) {
			Page::addMelding(_("Ik snap geen drol van dit periodeformaat!"), "fout");
			return NULL;
		}
		return Periode::vanColjaar($parts[0], $parts[1]);
	}

	/**
	 * Mail docenten op basis van de data.
	 *
	 * Bij het mails sturen wordt de pagina gevuld met statusinformatie.
	 *
	 * @param HtmlDiv $output
	 * Een HtmlDiv waar de output in gezet mag worden.
	 * @param bool $preview
	 * Indien true, worden de mails op het scherm getoond en niet verzonden.
	 *
	 * @return bool Of het gelukt is.
	 */
	public static function stuurDocentenMail(HtmlDiv $output, $preview = true)
	{
		// Begin met mailen!
		// Bepaal de periode om over te mailen
		$jaarperiode = self::getJaarPeriode();
		if (!$jaarperiode)
		{
			Page::addMelding(_("Er waren helemaal geen periodes geselecteerd! Dan kan ik ook niet mailen, hoor."), 'fout');
			return false;
		}
		$jaar = $jaarperiode->getCollegejaar();
		$periode = $jaarperiode->getPeriodeNummer();
		$begindatum = $jaarperiode->getDatumBegin();
		$einddatum = $jaarperiode->getDatumEind();

		if (!$einddatum)
		{
			Page::addMelding(sprintf(_("Geen einddatum gevonden voor de periode %d-%d[VOC: jaar - periodenummer]. Is er wel een periode na %d[VOC: periodenummer] gedefinieerd?"), $jaar, $periode, $periode), 'fout');
			return false;
		}

		$vakkendata = JaargangVerzameling::verzamelHuidige($begindatum, $einddatum);

		if ($vakkendata->aantal() == 0)
		{
			Page::addMelding(_('Er zijn geen vakken die beginnen in de geselecteerde periode. Komen de begindata van de vakken wel overeen met die van de periodes?'), 'fout');
			return false;
		}

		// En dan de mails sturen...
		if ($preview)
		{
			Page::addMelding(_("De volgende mails zouden verstuurd worden. Lees ze even door of ze correct zijn. Onderaan de pagina staat de knop om de mails definitief te versturen."), 'succes');
		}
		$succes = self::docentenBoekenMailPerVak($vakkendata, $output, $preview);
		if (!$succes)
		{
			return false;
		}

		if (!$preview)
		{
			Page::addMelding(_("De mails zijn allemaal succesvol de deur uit! Stel je voor dat je dat allemaal met de hand had moeten doen joh."));
		}
		return true;
	}

	/**
	 * Stuur docenten de daadwerkelijke boekenmail.
     * Geeft aan wat er allemaal misloopt.
	 *
	 * @param JaargangVerzameling $data De jaargangen om over te mailen.
	 * @param HtmlElement $output
	 * Hier wordt de voortgang van het proces en eventueel de preview getoond.
	 * @param bool $preview
	 * Indien true, worden de mails op het scherm getoond en niet verzonden.
	 *
	 * @return bool Of het succesvol was.
	 */
	private static function docentenBoekenMailPerVak(JaargangVerzameling $data, HtmlElement $output, $preview = true)
	{
		// Mapt emailadres naar de variabelen die in de template gezet worden
		// zo hoeft een docent niet heel veel mailtjes tegelijk te krijgen.
		$vakkenPerAdres = [];
		foreach ($data as $jaargang)
		{
			/** @var Jaargang $jaargang */
			$email = $jaargang->getContactPersoon()->getEmail();

			// Zoek op welke dictaten/boeken we voor dit vak kennen,
			// stop dat in een associatieve array voor boeken,
			// zodat de template het kan invullen.
			$artikels = $jaargang->getJaargangArtikelVerzameling();
			$dictaatCount = 0;
			$boekenData = [];
			foreach ($artikels as $jaargangArtikel)
			{
				/** @var JaargangArtikel $jaargangArtikel */
				$artikel = $jaargangArtikel->getArtikel();
				if ($artikel instanceof Dictaat)
				{
					$dictaatCount++;
					// Merk op dat we ook ondersteunen dat er meerdere dictaten zijn.
					// (Het randgeval is hier Functies en Reeksen, met een apart theorie- en opgavenboekje.)
				}
				else
				{
					if (!$artikel instanceof Boek)
					{
						Page::addMelding(sprintf(
							_("Geen idee wat te doen met artikel %s: geen boek of dictaat :S"),
							$artikel->getNaam()
						), 'fout');
						return false;
					}
					$boekenData[] = [
						'titel' => $artikel->getNaam(),
						'auteur' => $artikel->getAuteur(),
						'druk' => $artikel->getDruk(),
						'isbn' => $artikel->getIsbn(),
						'verplicht' => $jaargangArtikel->getVerplicht(),
					];
				}
			}

			// De template krijgt een associatieve array met de volgende data:
			$vakkenPerAdres[$email][] = [
				'docent' => $jaargang->getContactPersoon(),
				'vak' => $jaargang->getVak()->getNaam(),
				'afkorting' => $jaargang->getVak()->getCode(),
				'dictaat' => $dictaatCount > 0,
				'boeken' => $boekenData,
			];
		}

		foreach ($vakkenPerAdres as $email => $vakken)
		{
			if (count($vakken) == 0)
			{
				Page::addMelding(_("Ik stond op het punt het adres $email te mailen zonder vakken, paniek!"), 'fout');
				return false;
			}
		}

		foreach ($vakkenPerAdres as $email => $vakken)
		{
			// Rond het klusje af: vul de template in.
			$variabelen = [];
			$variabelen["vakken"] = $vakken;
			$variabelen["numvakken"] = count($vakken);
			$mailInhoud = Template::render(BW2_DOCENTMAIL_TEMPLATE, $variabelen);

			// En het kan de deur uit!
			if ($preview)
			{
				$output->add(new HtmlHeader(3, $email));
				$output->add(new HtmlPre($mailInhoud));
			}
			else
			{
				self::sendMail($email, _("Gegevens die A–Eskwadraat van uw vakken heeft"), $mailInhoud);
			}
		}
		return true;
	}

	/**
	 *  Stuur als de boekencommissaris een mailtje naar het aangegeven adres.
	 *
	 * Er wordt automatisch een nette footer onder gezet.
	 *
	 * @param ontvanger Het adres waar de mail heen verstuurd moet worden.
	 * @param titel De titel/onderwerp van de mail.
	 * @param inhoud De inhoud van de mail exclusief footer, inclusief aanhef.
	 */
	private static function sendMail($ontvanger, $titel, $inhoud) {
		global $BESTUUR;

		// Stel nog wat waarden in die misschien parameters kunnen worden?
		$boekcomname = BOEKCOMNAME;
		$from = $BESTUUR['boekencommissaris']->getEmail();

		// Plak een afsluiting onder de mail.
		$tekst = <<<HEREDOC
$inhoud

Met vriendelijke groeten,

	$boekcomname

Boekencommissaris A-Eskwadraat ($from)

(dit bericht is automatisch aangemaakt door BoekenWeb)

HEREDOC;
		sendmail($from, $ontvanger, $titel, $tekst);
		return true;
	}
}
