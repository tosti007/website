<?php

use Symfony\Component\HttpFoundation\Response;

abstract class IDEAL_Controller
{
	static public function bankGekozen()
	{
		$transId = tryPar('transid');
		if (empty($transId))
		{
			$page = iDealView::geefGeneriekeMeldingEnStop(_('Waarschuwing'),
				_('Er is geen transactie-informatie meegegeven. Als u deze pagina '
				. 'ziet na het uitvoeren van een transactie is er iets goed mis. '
				. 'Neem aub contact op met bestuur@a-eskwadraat.nl.'));
			return new PageResponse($page);
		}

		//Klant heeft transactie gezien en wil nu betalen!
		$transactie = iDeal::geefVeilig($transId);
		if(!$transactie)
		{
			$page = iDealView::geefGeneriekeMeldingEnStop(_('Probleem met iDEAL'),_('Er is een fout opgetreden, probeer het later opnieuw!'));
			return new PageResponse($page);
		}

		//Start de transactie, maak gebruik van de Mollie-API
		$payment = $transactie->maakMollieTransactie();

		//Sla de verkregen transactieId op
		$transactie->setExterneId($payment->id);
		//Zet expiratietijd
		$transactie->setExpire(new DateTimeLocale("+15 minutes"));
		$transactie->opslaan();

		//Stuur de gebruiker door naar zijn bank
		spaceRedirect($payment->getPaymentUrl());
	}

	static public function klaar()
	{
		//De gebruiker komt terug van een transactie
		$trxid = tryPar('id');
		$ec = tryPar('ec');
		if(!$trxid || !$ec)
		{
			iDealView::geefGeneriekeMeldingEnStop(_('Waarschuwing'),
				_('Er is geen transactie-informatie meegegeven. Als u deze pagina '
				. 'ziet na het uitvoeren van een transactie is er iets goed mis. '
				. 'Neem aub contact op met bestuur@a-eskwadraat.nl.'));
		}
		//Check afkomst
		if($ec != iDeal::genereerEntranceCode())
		{
			iDealView::geefGeneriekeMeldingEnStop(_('Beveiligingswaarschuwing'),
				_('Deze transactie is niet vanaf deze computer uitgevoerd, '
				. 'daarom kunt u haar niet bekijken!'));
		}

		if($trxid && $ec && $transactie = Transactie::geef($trxid))
		{
			// Als we op de debug zitten hebben we niet de mogelijkheid tot de webhook
			if (DEBUG)
			{
				$payment = iDeal::getMolliePayment($transactie->getExterneID());
				$oudeStatus = $transactie->getStatus();
				$status = $transactie->haalStatusOp($payment);

				// We checken of de status is veranderd, want anders gaan we
				// transacties dubbel afhandelen.
				if ($status != $oudeStatus) {
					// Regel alle administratieve dingen af die bij dit artikel horen
					if (Transactie::isStatusGeslaagd($status)) {
						$transactie->handelAf();
					} elseif (Transactie::isStatusGefaald($status)) {
						$transactie->handelFaalAf();
					}
				}
			}

			if (tryPar('cleanhtml'))
			{
				$page = Page::getInstance('cleanhtml')
				    ->addHeadCss(Page::minifiedFile('a-eskwadraat.css', 'css'));
			}
			else
			{
			    $page = Page::getInstance();
			}

			$page->start(_('Resultaat iDEALTransactie'))
				->add(iDealView::toon($transactie))
				->end();
		}
		else
		{
			iDealView::geefGeneriekeMeldingEnStop(_('Waarschuwing'),
				_('Er is foutieve transactie-informatie meegegeven. Als u deze '
				. 'pagina ziet na het uitvoeren van een transactie is de transactie '
				. 'mislukt. Als er wel geld van u rekening is afgeschreven, '
				. 'wacht dan maximaal een half uur. Als u dan nog geen bevestiging '
				. 'gekregen hebt dat uw iDealtransactie gelukt is, '
				. 'neem dan aub contact op met bestuur@a-eskwadraat.nl.'));
		}
	}

	public static function beheer()
	{
		requireAuth('kasCom');

		$body = new HtmlElementCollection();
		$body->add(new HtmlParagraph(_("O machtige penningmeester (of KasCom), wat kan ik voor u betekenen?")));
		$paginaLijst = new HtmlList();

		$paginas = array();
		$paginas['alleTransacties'] = _("Bekijk alle transacties");
		if (hasAuth('bestuur')) {
			$paginas['KaartjeKwijt'] = _("Kaartje kwijt?");
		}

		foreach($paginas as $url=>$titel)
			$paginaLijst->add(new HtmlListItem(new HtmlAnchor($url, $titel)));

		$body->add($paginaLijst);

		$page = Page::getInstance()
			->start()
			->add($body)
			->end();
	}

	public static function alleTransacties()
	{
		requireAuth('kasCom');

		//Haal ze uit de db
		$id = tryPar("artikel", "*");
		if($id == "*")
			$id = null;

		$maand = tryPar('maand', date('m'));
		$jaar = tryPar('jaar', date('Y'));

		$transacties = iDealVerzameling::allemaal(tryPar("status"), $id, $maand, $jaar);

		$artikelen = ArtikelVerzameling::geefVanIDEAL();
		// Zet nieuwere artikelen bovenaan ipv onderaan
		// zodat de penny / kascom niet zo ver hoeft te scrollen
		// (dit is een snelle, niet echt nette, manier om te sorten op aflopende aanmaakdatum)
		$artikelen->reverse();

		if(tryPar("CSV"))
		{
			header("Content-Disposition: attachment; filename=ideal.csv");
			header("Content-type: text/csv; encoding=utf-8");
			iDealVerzamelingView::csv($transacties);
			exit();
		}
		else
		{
			//Maak een sjieke tabel, met een filter erboven
			iDealVerzamelingView::alleTransacties($artikelen, $transacties, tryPar("status"), $id, $maand, $jaar);
		}
	}

	static public function home()
	{
		if(DEBUG)
		{
			if(tryPar('t'))
			{
				$t = new iDeal('WWW',
					'BETALING',
					Persoon::getIngelogd(),
					1*tryPar('a',1),
					substr("Test de nieuwe versie van iDEAL", 0, 35),
					new DateTimeLocale(),
					"Testtransactie",
					Voorraad::geef(50),
					"bla",
					null
				);
				$t->setReturnURL(HTTPS_ROOT . "/www?status=");
				$t->setEmailadres("webcie@a-eskwadraat.nl");
				$t->setStatus('INACTIVE');

				//Naar de iDEALController
				$t->startTransactie();
			}
		}

		$page = Page::getInstance();
		$page->start(_("iDEAL"));
		$page->add(iDealView::algemeneInfo());

		$page->add(new HtmlBreak());

		$page->add(HtmlAnchor::button("?t=1&a=1", _("Vraag een transactie aan")));

		$page->end();
	}

	static public function nieuwePoging()
	{
		if(Token::processNamedForm() == 'nieuwePoging')
		{
			$transId = tryPar('transid');
			//Opnieuw transactie proberen
			//
			if(empty($transId) || !($transactie = iDeal::geefVeilig($transId)))
				self::geefGeneriekeMeldingEnStop(_('Probleem met iDEAL'),_('Er is een fout opgetreden, probeer het later opnieuw!'));

			$transactie = $transactie->nieuwePoging();
			$overzicht = iDealView::toonOverzicht($transactie);

			if(tryPar('cleanhtml'))
				$page = Page::getInstance('cleanhtml');
			else
				$page = Page::getInstance();

			$page->start(_('Nieuwe poging iDEAL betaling'))
				->add(new HtmlHeader(2, _('Nieuwe poging: Betalen via iDEAL')))
				->add($overzicht)
				->end();
		}

		self::home();
	}

	/**
	 * Formulier om een kaartje te genereren als iemand die kwijt is.
	 */
	public static function kaartjeKwijt()
	{
		if (!hasAuth('bestuur')) {
			spaceHTTP(403);
		}

		$page = Page::getInstance();

		$page->start();
		$transactieID = tryPar('transactieID');
		if ($transactieID) {
			$page->add(self::kaartjeKwijtVerwerk($transactieID));
		}
		$page->add(self::kaartjeKwijtBouwForm());

		$page->end();
	}

	/**
	 * Maakt het formulier waar je een transactie kan opgeven om de kaartjes te genereren.
	 */
	public static function kaartjeKwijtBouwForm()
	{
		$body = new HtmlElementCollection();

		$body->add(new HtmlHeader(1, _("Iemand is zijn kaartjes kwijt!")))
			->add(new HtmlParagraph(sprintf(_("Je hebt hiervoor een transactie-ID "
			. "nodig, wat te achterhalen is in %s of aan de gebruiker hopelijk "
			. "te ontfutselen is. LET OP: dit is dus niet het 16-cijferige ID "
			. "wat ING eraan geeft, maar het plusminus 5-cijferig A–EskwadraatID."),
			new HtmlAnchor("/Service/iDEALbeheer/alleTransacties",
				_("het overzicht van iDEALtransacties")))));

		$form = new HtmlForm('GET');
		$form->add($transactienr = HtmlInput::makeText('transactieID'))
			->add(HtmlInput::makeSubmitButton(_('Help die arme drommel!')));

		$body->add($form);
		return $body;
	}

	/**
	 * Verwerkt het formulier met transactienummer en toont de kaartjes die daarbij horen.
	 */
	public static function kaartjeKwijtVerwerk($transactieID = null)
	{
		// zoek transactie en kaartjes op, maar als er iets misgaat, schrijf dan een fout op
		$fout = "";
		if (!$transactieID) {
			$fout = _("Vergeet niet een transactie-ID in te vullen, anders wordt het nogal lastig zoeken!");
		} else {
			$transactie = Transactie::geef($transactieID);
			if (!$transactie) {
				$fout = _("Er is geen iDEAL-transactie met dat ID. Check even dat je wel het goede te pakken hebt!");
			} else {
				$kaartjes = iDealKaartjeCodeVerzameling::fromTransactie($transactie);
				if (!$kaartjes || $kaartjes->aantal() == 0) {
					$fout = _("De transactie is gevonden, maar er horen geen kaartjes bij.");
				}
			}
		}

		if ($fout) {
			$body = new HtmlElementCollection();
			$body->add(new HtmlHeader(1, _("Kaartjes zijn nog kwijt!")));
			$body->add(new HtmlParagraph($fout));
			$body->add(new HtmlParagraph(_("Ik heb dus geen kaartjes kunnen vinden. Probeer het nogmaals:")));

			return $body;
		}

		$body = new HtmlElementCollection();
		$body->add(new HtmlHeader(1, _("Kaartjes zijn teruggevonden!")));
		$body->add(new HtmlParagraph(_("De volgende kaartjes staan in de database bij deze transactie:")));
		foreach ($kaartjes as $kaartje) {
			$activiteit = $kaartje->getKaartje()->getActiviteit();

			$body->add($kaartjeDiv = new HtmlDiv());
			$kaartjeDiv->add(iDealKaartjeCodeView::viewInfo($kaartje));
			$kaartjeDiv->add(new HtmlImage(sprintf('/Service/iDEALbeheer/KaartjeQR?code=%s&actnaam=%s', $kaartje->getCode(), $activiteit->getTitel()), $kaartje->getCode()));
			$scanUrl = $activiteit->url() . sprintf('/KaartjeScan?code=%s', $kaartje->getCode());
			$kaartjeDiv->add(new HtmlAnchor($scanUrl, 'Scan dit kaartje!', sprintf('ScanKaartje("%s"); event.preventDefault();', $scanUrl)));
		}

		return $body;
	}

	/**
	 * Genereer een png-afbeelding met een iDEALkaartje.
	 * form-parameters: code (de geheime code op het kaartje)
	 *                  actnaam (de naam van de activiteit)
	 */
	static public function kaartjeQR() {
		global $pageEnded;

		$code = trim(trypar("code"));
		$actnaam = trim(trypar("actnaam", ""));

		// print die hap!
		header('Content-Type: image/png');
		echo iDealKaartjeCodeView::maakQR($code, $actnaam);

		// zorgt ervoor dat we een PNG krijgen ipv error 501
		$pageEnded = false;
	}

	static public function mollieWebhook()
	{
		global $WSW4DB;

		$mollieId = tryPar('id');
		$transactieId = tryPar('trxid');
		$hash = tryPar('hash', '');
        
        if(tryPar('testByMollie', null))
            die('OK');

		if(!($transactie = iDeal::geefUitExternID($mollieId)))
		{
			return responseUitStatusCode(404);
		}

		if($transactie->geefID() != $transactieId || $transactie->maakHash() !== $hash)
		{
			return responseUitStatusCode(404);
		}

		$payment = iDeal::getMolliePayment($mollieId);

		$oudeStatus = $transactie->getStatus(); //Dit was dus opgeslagen in de DB
		$status = $transactie->haalStatusOp($payment); //Krijg de huidige status van de transactie
		$klantInfo = iDealView::klantInfo($transactie);

		// We checken of de status is veranderd, want anders gaan we
		// transacties dubbel afhandelen.
		if ($status != $oudeStatus)
		{
			// Regel alle administratieve dingen af die bij dit artikel horen
			if (Transactie::isStatusGeslaagd($status))
			{
				// TODO: doe deze mail met templates ofzo.
				$mail = "
Hallo,

Er heeft een succesvolle transactie plaatsgevonden in ons iDEAL-systeem.

Transactie: " . iDealView::waardeTitel($transactie) . "
Omschrijving: " . TransactieView::waardeUitleg($transactie) . "
Bedrag: € " . number_format($transactie->getBedrag(),2) . "

Met vriendelijke groet,
De WebCie";
				sendmail("www@a-eskwadraat.nl", $transactie->getEmailadres(), "iDEAL-betaling A-Eskwadraat", $mail);
				//Regel alle administratieve dingen af die bij dit artikel horen

				$dbuser = 'mollie';
				$pass = secret::dbpasswords();
				$dbpass = $pass['mollie'];

				$WSW4DB = new dbpdo(WHOSWHO4_DB, '127.0.0.1', $dbuser, $dbpass);
				$transactie->handelAf();
			}
			else if (Transactie::isStatusGefaald($status))
			{
				//Klant op de hoogte stellen:
				sendmail("www@a-eskwadraat.nl",
					$transactie->getEmailadres(),
					_("iDEAL-betaling A-Eskwadraat"),
"Hallo,

Er heeft een mislukte transactie plaatsgevonden in ons iDEAL-systeem.

$klantInfo

U kunt contact opnemen met de organiserende commissie om het probleem op te lossen

Met vriendelijke groet,
De WebCie");

                //Handel de faal af:
				$dbuser = 'mollie';
				$pass = secret::dbpasswords();
				$dbpass = $pass['mollie'];

				$WSW4DB = new dbpdo(WHOSWHO4_DB, '127.0.0.1', $dbuser, $dbpass);
				$transactie->handelFaalAf();
			}
		}
       
	    return new Response();
	}
}
