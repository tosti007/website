<?php
define('BOEKEN_ANONIEM', 111);

abstract class Verkoop_Controller
{
	static public function index ()
	{
		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Verkoop/index.php");
		Page::getInstance()->end();
	}

	static public function venster ()
	{
		$personen = PersoonVerzameling::zoek(tryPar('contact'));
		$contact = null;
		if ($personen->aantal() == 1)
			$contact = $personen->first();
		if (tryPar('contact') == BOEKEN_ANONIEM)
			$contact = BOEKEN_ANONIEM;

		$kart = self::kartForm();
		if (tryPar('naarKart', false))
			self::naarKartForm($kart);
		if (tryPar('uitKart', false))
			self::uitKartForm($kart);
		$prijs = self::prijs($kart);
		$succes = false;
		if (($contact instanceof Contact || $contact == BOEKEN_ANONIEM) && count($kart) > 0 && tryPar('afrekenen', false))
			$succes = self::afrekenenForm($contact, $prijs, $kart);

		Page::getInstance('cleanhtml')->start();
		include("WhosWho4/Boekweb2/Views/Verkoop/venster.php");
		Page::getInstance('cleanhtml')->end();
	}

	static protected function kartForm ()
	{
		$kart = array();

		$data = tryPar('kart', array());
		foreach ($data as $id => $aantal)
			if (Voorraad::geef($id)->getAantal() >= $aantal)
				$kart[$id] = $aantal;

		return $kart;
	}

	static protected function naarKartForm (&$kart)
	{
		$id = (int) tryPar('naarKart');
		if (!is_null(Artikel::geef($id)))
			if (Voorraad::geef($id)->getAantal() >= tryPar('artikel_'.$id))
				$kart[$id] = tryPar('artikel_'.$id);
	}

	static protected function uitKartForm (&$kart)
	{
		$id = (int) tryPar('uitKart');
		unset($kart[$id]);
	}

	static protected function prijs ($kart)
	{
		$prijs = 0;
		foreach ($kart as $id => $aantal)
			$prijs += Voorraad::geef($id)->getVerkoopPrijs()->getPrijs() * $aantal;
		return $prijs;
	}

	static protected function afrekenenForm ($contact, $prijs, $kart)
	{
		$rekening = Register::getValue('pinRekening');
		$pin = new Pin('BOEKWEB', 'VERKOOP', (($contact instanceof Contact) ? $contact : null),
						$prijs, '', new DateTimeLocale(), $rekening, tryPar('pin'));

		if ($pin->valid())
		{
			$pin->opslaan();

			foreach ($kart as $id => $aantal)
			{
				$voorraad = Voorraad::geef($id);
				$verkoop = Verkoop::maakVerkoop($voorraad, new DateTimeLocale(), $aantal, $pin);
				$verkoop->opslaan();
			}
		}

		return true;
	}
}
