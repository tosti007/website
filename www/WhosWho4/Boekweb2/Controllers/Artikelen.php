<?php
declare(strict_types=1);

abstract class Artikelen_Controller
{
	static public function lijst ()
	{
		$colaproducten = ColaProductVerzameling::alle();

		$dibsproducten = DibsProductVerzameling::alle();

		$dictaten = DictaatVerzameling::alle();
		$dictaten->toAuteurVerzameling(); // Vul de cache alvast

		Page::getInstance()->start();
		include("WhosWho4/Boekweb2/Views/Artikelen/lijst.php");
		Page::getInstance()->end();
	}

	static public function toevoegen ()
	{
	}

	/**
	 * HTML-Endpoint om een hele spreadsheet met boeken in te voeren.
	 *
	 * Use case: De boekencommissaris heeft zojuist vakkendata geupload (#14 (closed))
	 * en wil aan alle docenten die vakken gaan geven, vragen of de informatie correct is.
	 * Bij deze informatie hoort ook welke boeken ze gebruiken in hun vak, dus dat uploadt de boekencommissaris.
	 * Daarna wordt een knopje getoond om meteen de mails de deur uit te duwen.
	 *
	 * @site{/Onderwijs/Boekweb/Artikel/BoekenCSV}
	 *
	 * @see Vak_Controller::vakkenCSV
	 * TODO: kunnen we gemeenschappelijke code een beetje goed extraheren?
	 * @see MailController::docentenmailing
	 */
	public static function boekenCSV()
	{
		global $logger;
		requireAuth('bestuur');

		if (Token::processNamedForm() == 'boekenCSV')
		{
			/**
			 * @var string[] $errors
			 * @var Verzameling $opTeSlaan
			 */
			// Het principe is dat de volgende functie een array met fouten in het bestand opspoort,
			// zodat we die aan de gebruiker kunnen melden en die ze kan fixen.
			// Als er geen errors zijn, zou de data correct moeten zijn en in de DB kunnen.
			list($errors, $opTeSlaan) = static::verwerkBoekenCSV((string)tryPar('csv', ''));
			if ($errors)
			{
				Page::addMeldingArray($errors, 'fout');
			}
			elseif (!$opTeSlaan->allValid())
			{
				// Check dat we toch echt valide data hebben gekregen,
				// om bugjes op te sporen
				// (bijvoorbeeld een nieuw verplicht veld waar processBoekenCSV geen rekening mee houdt).
				$logger->info(print_r($opTeSlaan->allErrors(), true));
				user_error("Geen error ontdekt in boeken-csv maar objecten zijn ongeldig!", E_USER_WARNING);
				Page::addMelding(_("Geen fout ontdekt tijdens het verwerken van het bestand, maar de uitvoer is ongeldig. De WebCie zou al op de hoogte gesteld moeten zijn."), 'fout');
			}
			else
			{
				$opTeSlaan->opslaan();
				return Page::responseRedirectMelding('/Onderwijs/Boekweb', _("De spreadsheet is succesvol in de databaas gezet!"));
			}
		}

		$page = new HTMLPage();
		$page->start();

		$page->add(new HtmlParagraph(_("Copypasta hieronder je spreadsheet met de boeken die bij vakken horen!")));
		$page->add($form = HtmlForm::named('boekenCSV'));
		$form->add(View::makeFormTextRow('csv', tryPar('csv'), _('Vul hier je csv in')));
		$form->add(new HtmlParagraph(_(
		// Voor een WebCie'er: je kan een voorbeeldspreadsheet vinden in de comments van GitLab issue #17,
		// die ik gebruikt heb om dit formulier te testen.
			"Heeft de Grote Boekencommissarisboekenspreadsheet jou niet bereikt? Hier een overzichtje:"
			. " Je kan tabs of komma's gebruiken om velden te scheiden."
			. " De eerste regel moet de veldnamen bevatten:"
			. " Vakcode,ISBN,Titel,Auteur,Editie,Uitgever,Verplicht?"
			. " (in jouw favoriete volgorde!)"
			. " Daarna op elke regel de informatie per vak."
			. " Datums zijn DD-MM-YYYY."
			. " Het boek wordt gekoppeld aan de eerstvolgende bekende jaargang van het vak."
		)));
		$form->add(HtmlInput::makeFormSubmitButton(_('Voer in')));

		return new PageResponse($page);
	}

	/**
	 * Verwerk een CSV-bestand met boekeninformatie tot DBObjecten die in de database kunnen.
	 *
	 * Het kan gebeuren dat er iets niet klopt aan de spreadsheet,
	 * dus we returnen een lijst menselijk leesbare foutmeldingen,
	 * en een Verzameling met objecten die corresponderen met de data in de spreadsheet.
	 *
	 * @param string $csv
	 *
	 * @return array
	 * Een array [$errors, $opTeSlaan].
	 * Hier is $errors een lijst van menselijk leesbare foutmeldingen,
	 * en $opTeSlaan een Verzameling.
	 * Indien er geen errors zijn, bevat $opTeSlaan alle data uit de spreadsheet als DBObjecten.
	 */
	private static function verwerkBoekenCSV(string $csv)
	{
		global $logger;

		$errors = [];
		$opTeSlaan = new Verzameling();

		// Merk op dat de code best lijkt op VakController::verwerkVakCSV, dit moet algemener kunnen!
		$regels = preg_split("/((\r?\n)|(\r\n?))/", $csv);
		if (count($regels) == 0)
		{
			$errors[] = _("Vergeet niet om data op te sturen, alstudank, blieftjewel!");
			return [$errors, $opTeSlaan];
		}
		$delimiterstukje = strpbrk($regels[0], ",\t");
		if (!$delimiterstukje)
		{
			$errors[] = _("De eerste regel bevat tab noch komma, heb je wel het goede bestand opgestuurd?");
			return [$errors, $opTeSlaan];
		}
		$delimiter = $delimiterstukje[0];
		$regels = array_map(function ($regel) use ($delimiter) {
			return str_getcsv($regel, $delimiter);
		}, $regels);

		// De eerste regel bevat de headers, maar we doen lowercase voor case-insensitivity.
		$header = array_shift($regels);
		$header = array_map('strtolower', $header);
		$logger->debug("Geparste CSV-header: " . print_r($header, true));

		// Velden: Vakcode,ISBN,Titel,Auteur,Editie,Uitgever,Verplicht?
		if (($vakcodeIndex = array_search('vakcode', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'vakcode');
		}
		if (($isbnIndex = array_search('isbn', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'isbn');
		}
		if (($titelIndex = array_search('titel', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'titel');
		}
		if (($auteurIndex = array_search('auteur', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'auteur');
		}
		if (($editieIndex = array_search('editie', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'editie');
		}
		if (($uitgeverIndex = array_search('uitgever', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'uitgever');
		}
		if (($verplichtIndex = array_search('verplicht?', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'verplicht?');
		}

		// We hebben alle voorgaande velden nodig!
		if (count($errors))
		{
			return [$errors, $opTeSlaan];
		}

		// Check of een regel alle velden bevat: is die langer dan max van alle indices?
		// Dus count($regel) >= $maxIndex + 1
		$maxIndex = max($vakcodeIndex, $isbnIndex, $titelIndex, $auteurIndex, $editieIndex, $uitgeverIndex, $verplichtIndex);

		foreach ($regels as $nummer => $regel)
		{
			// Als er velden aan het einde leeg zijn,
			// wil LibreOffice ze nog wel eens helemaal weglaten.
			// Vul ze dus op met lege strings.
			// Lengte moet strikt groter zijn dan de index, vandaar plus 1.
			$regel = array_pad($regel, $maxIndex + 1, '');

			// Dit boek hoort bij de eerstvolgende Jaargang van het vak met de gegeven code.
			$vakcode = $regel[$vakcodeIndex];
			$vak = Vak::geefDoorCode($vakcode);
			if (!$vak)
			{
				$errors[] = sprintf(_("Geen vak met code `%s' gevonden in de Databaas!"), $vakcode);
			}
			$jaargang = JaargangQuery::table()
				->whereProp('vak', $vak)
				->whereProp('datumBegin', '>=', new DateTimeLocale())
				->orderByAsc('datumBegin')
				->limit(1)
				->geef();
			if (!$jaargang)
			{
				$errors[] = sprintf(_("In de databaas staan geen komende jaargangen van het vak `%s'."), $vakcode);
			}

			$verplicht = strtolower($regel[$verplichtIndex]);
			if ($verplicht == 'verplicht' || $verplicht == 'ja' || $verplicht == 'j' || $verplicht == 'y')
			{
				$boekVerplicht = true;
			}
			elseif ($verplicht == 'aanbevolen' || $verplicht == 'nee' || $verplicht == 'n')
			{
				$boekVerplicht = false;
			}
			else
			{
				$errors[] = sprintf(_("Verplicht?: %s moet zijn 'Ja' of 'Nee'"), $verplicht);
			}

			if (count($errors))
			{
				// Als we errors hebben, dan gaan we ook niet proberen objecten aan te maken!
				continue;
			}

			$boek = new Boek();
			$boek->setNaam($regel[$titelIndex]);
			$boek->setAuteur($regel[$auteurIndex]);
			$boek->setDruk($regel[$editieIndex]);
			$boek->setIsbn($regel[$isbnIndex]);
			$boek->setUitgever($regel[$uitgeverIndex]);
			$opTeSlaan->voegtoe($boek);
			$boekgebruik = new JaargangArtikel($jaargang, $boek);
			$boekgebruik->setVerplicht($boekVerplicht);
			$opTeSlaan->voegtoe($boekgebruik);
		}

		return [$errors, $opTeSlaan];
	}
}
