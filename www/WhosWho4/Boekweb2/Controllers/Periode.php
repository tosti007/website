<?php

abstract class Periode_Controller {
	/**
	 * @brief Toon alle geconfigureerde periodes.
	 *
	 * @site{/Onderwijs/Boekweb/Periode/index.html}
	 */
	public static function overzicht() {
		$page = Page::getInstance();
		$page->start();

		$page->add(PeriodeView::nieuwForm());
		$page->add(PeriodeVerzamelingView::periodeOverzicht());

		$page->end();
	}

	/**
	 *  Handelt de variabele entry van een periode af.
	 *
	 * @site{/Onderwijs/Boekweb/Periode/ * /}
	 */
	static public function periodeEntry($args)
	{
		$per = Periode::geef($args[0]);
		if (!$per) return false;

		return array('name' => $args[0],
					'displayName' => PeriodeView::naam($per),
					'access' => true);
	}

	/**
	 *  Maak een nieuwe periode.
	 *
	 * @site{/Onderwijs/Boekweb/Periode/Nieuw}
	 */
	static public function periodeNieuw()
	{
		requireAuth('bestuur');

		$per = new Periode();
		$token = Token::processNamedForm();
		if ($token == "nieuwePeriode") {
			PeriodeView::processForm($per);
			if ($per->valid()) {
				$per->opslaan();
				Page::redirectMelding(BOEKWEBBASE."Periode", _("Nieuwe periode toegevoegd!"));
				return;
			}
		}
		$page = Page::getInstance()->start();
		$page->add(PeriodeView::nieuwForm($per));
		$page->end();
	}

	/**
	 *  Wijzig de periode met het variabele stukje ID.
	 *
	 * @site{/Onderwijs/Boekweb/Periode/ * /Wijzig}
	 */
	static public function periodeWijzig()
	{
		requireAuth('bestuur');

		$vfs = vfsVarEntryNames();

		if (!($per = Periode::geef($vfs[0])))
			spaceHttp(403);

		$token = Token::processNamedForm();
		if ($token == "wijzigPeriode") {
			PeriodeView::processForm($per);
			if ($per->valid()) {
				$per->opslaan();
				Page::redirectMelding(BOEKWEBBASE."Periode", _("Periode gewijzigd!"));
				return;
			}
		}
		$page = Page::getInstance()->start();
		$page->add(PeriodeView::wijzigForm($per));
		$page->end();
	}

	/**
	 *  Verwijder de periode met het variabele stukje ID.
	 *
	 * @site{/Onderwijs/Boekweb/Periode/ * /Verwijder}
	 */
	static public function periodeVerwijder()
	{
		requireAuth('bestuur');

		$vfs = vfsVarEntryNames();

		if (!($per = Periode::geef($vfs[0])))
			spaceHttp(403);

		$error = $per->verwijderen();

		if (!empty($error)) {
			Page::redirectMelding(BOEKWEBBASE."Periode", $error);
		} else {
			Page::redirectMelding(BOEKWEBBASE."Periode", _("Periode is verwijderd!"));
		}
	}
}
