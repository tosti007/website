<?php

setZeurmodus();

abstract class VoorraadController {
	/**
	 * @brief Overzichtpagina met alle voorraden in Boekweb2.0
	 *
	 * @site{/Onderwijs/Boekweb/Voorraad/index.html}
	 */
	public static function overzicht() {
		// verwerk get-data
		$maand = (int)tryPar('maand', date('m'));
		$jaar = (int)tryPar('jaar', date('Y'));

		if ($maand <= 0 || $maand > 12 || $jaar < 0 || $jaar >= 10000)
		{
			Page::addMelding(_("Geef wel een zinnige maand en/of jaar op joh!"), 'fout');
			$page = new HTMLPage();
			$page->start(_("Voorraadoverzicht"));
			$page->add(VoorraadVerzamelingView::overzichtForm(date('m'), date('Y'), false));
			return new PageResponse($page);
		}

		// laad de voorraden
		$voorraden = VoorraadQuery::table()->verzamel();
		return new PageResponse(VoorraadVerzamelingView::overzicht($voorraden, $maand, $jaar, false));
	}
};

?>
