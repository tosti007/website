<h1><?php echo DibsProductView::waardeNaam($dibsproduct); ?></h1>

<p>
	<?php printf(_("Klik %shier%s om terug naar de dibsproduct-pagina te gaan."), '<a href=\''.$dibsproduct->url().'\'>', '</a>'); ?>
</p>

<form method="post">

<?php if (isset($msg)) {
	foreach ($msg as $m) {
?><p><span class="negatief"><?php
		switch ($m) {
//				case BOEKEN_VERPLAATSFORM_VOORRAAD: echo _("Er ging iets mis met het verwerken van deze verplaatsing."); break;
		}
?></span></p><?php
	}
} ?>

<p>
	<ul>
		<li><?php echo _("Aantal:"); ?>
			<?php echo VoorraadView::waardeAantal($voorraad); ?></li>
		<li><?php echo _("Locatie:"); ?>
			<?php echo VoorraadView::waardeLocatie($voorraad); ?></li>
		<li><?php echo _("Verkoopprijs:"); ?>
			<?php echo VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs()); ?></li>
	</ul>
</p>

<table>
	<tr>
		<td><?php echo _("Hoeveel stuks wil je verplaatsen?"); ?></td>
		<td><input type="text" name="aantal" value="<?php echo $voorraad->getAantal(); ?>" /></td>
	</tr>
	<tr>
		<td><?php echo _("Waar wil je ze naar toe verplaatsen?"); ?></td>
		<td><ul><?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) { ?>
			<li><input type="radio" name="locatie" value="<?php echo $id; ?>" /><?php echo $label; ?></li>
			<?php } ?></ul></td>
	</tr>
</table>

<p><input type="submit" name="submit" value="<?php echo _("Verplaats de dibsproducten"); ?>" /></p>

</form>
