<h1><?php printf(_('Nieuw levering van "%s" toevoegen'), $dibsproduct->getNaam());?></h1>

<p>
	<?php printf(_('Klik %s om terug te gaan'), '<a href=\'' . $dibsproduct->url() .'\'>hier</a>');?>
</p>

<form method="post">

<?php if(isset($msg)) {
	foreach($msg as $m) {?>
		<p>
			<span class="negatief"><?php echo $m;?></span>
		</p>
<?php }
}?>

<table>
	<tr>
		<td><b><?php echo _('Naam:');?></b></td>
		<td><b><?php echo $dibsproduct->getNaam();?></b></td>
	</tr>
	<tr>
		<td><?php echo _('Hoeveel euros kost het?');?></td>
		<td><input type="number" name="prijs"></td>
	</tr>
	<tr>
		<td><?php echo _('Is het product verkoopbaar?');?></td>
		<td><ul><li><input type="radio" name="verkoopbaar" value="1" checked="true"/><?php echo _('Ja');?></li>
				<li><input type="radio" name="verkoopbaar" value="0"/><?php echo _('Nee');?></li></ul></td>
	</tr>
	<tr>
		<td><?php echo _('In welke voorraad komt het product?');?></td>
		<td><ul><?php foreach (VoorraadView::labelenumLocatieArray() as $id => $label) {?>
			<li><input type="radio" name="locatie" value="<?php echo $id;?>"/><?php echo $label;?></li>
			<?php } ?></ul></td>
	</tr>
	<tr>
		<td><?php echo _('Hoeveel worden er geleverd?');?></td>
		<td><input type="number" name="aantal"></td>
	</tr>
	<tr>
		<td><?php echo _('Voer eventueel een omschrijving in:');?></td>
		<td><input type="text" name="omschrijving"></td>
	</tr>
</table>

<p><input type="submit" name="submit" value="<?php echo _('Maak het type aan');?>"/></p>

</form>
