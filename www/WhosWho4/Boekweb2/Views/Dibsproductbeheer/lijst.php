<?php setlocale(LC_MONETARY, 'nl_NL.UTF-8');?>

<h1><?php echo _("Dibsproductenlijst"); ?></h1>

<?php if ($dibsproducten->aantal() == 0) { ?>
<span class="negatief"><?php echo _("Er zijn geen dibsproducten gevonden."); ?></span>
<?php } else { ?>
<table>
	<tr>
		<th><?php echo _('Naam');?></th>
		<th><?php echo _('Totaal in voorraad');?></th>
		<th><?php echo _('Totaal verkocht');?></th>
	</tr>
<?php foreach ($dibsproducten as $dp) {
	$dpmutaties = $dp->getVoorraadMutatieRelaties();?>
	<tr>
		<td><a href=<?php echo $dp->url();?>><?php echo ArtikelView::waardeNaam($dp);?></a></td>
<?php $sum = 0; $verkocht = 0; foreach($dpmutaties as $dpv) {
	$sum+=$dpv->getAantal();
	if($dpv instanceof Verkoop)
 		$verkocht+=$dpv->getAantal();
 	} ?>
		<td><?php echo $sum;?></td>
		<td><?php echo $verkocht;?></td>
	</tr>
<?php } ?>
</table>
<?php } ?>
<?php if(hasAuth('boekcom')) {?>
<h3><?php echo _('Boekcom-acties:'); ?></h3>
<a href='/Onderwijs/Boeken2/Dibsproductbeheer/Nieuw'>Maak een nieuw dibsproduct aan</a>
<?php } ?>
