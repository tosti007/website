<h1>Dibsproduct '<?php echo $dibsproduct->getNaam();?>'</h1>

<p>
	<?php printf(_('Klik %shier%s om terug te gaan naar de dibsproducten'), '<a href=\'/Onderwijs/Boeken2/Dibsproductbeheer/\'>', '</a>');?>
</p>

<?php if(hasAuth('boekcom')) {?>
<h3><?php echo _('Boekcom-acties');?></h3>
<p>
	<?php printf(_('Klik %shier%s om een nieuwe levering in te voeren'), '<a href=\'' . $dibsproduct->url() . '/Voorraad/Nieuw\'>', '</a>');?><br>
	<?php printf(_('Klik %shier%s om het mutatie overzicht te zien'), '<a href=\'' . $dibsproduct->url() . '/Voorraad/Mutaties\'>', '</a>');?>
</p>
<?php }?>

<p>
	<b><?php printf(_('Naam: %s'), $dibsproduct->getNaam());?></b><br>
	<b><?php printf(_('Kudos: %s'), $dibsproduct->getKudos());?></b>
</p>

<p>
	Hier volgt een overzicht van alle producten die in de voorraden zijn.
</p>

<?php $dibsverkoopbaar = $dibsproduct->getVerkoopbareVoorraadRelaties();?>
<?php $naam = null; foreach($dibsverkoopbaar as $voorraad) {?>
<?php if ($naam != $voorraad->getLocatie()) { ?>
<?php if (!is_null($naam)) { ?></table><?php } ?>
<h4><?php echo VoorraadView::waardeLocatie($voorraad); ?></h4>
<table>
	<tr>
		<th><?php echo _("Omschrijving");?></th>
		<th><?php echo _("Aantal"); ?></th>
		<th><?php echo _("Eerste mutatie"); ?></th>
		<th><?php echo _("Laatste mutatie"); ?></th>
		<th><?php echo _("Verkoopbaar"); ?></th>
		<th><?php echo _("Verkoopprijs"); ?></th>
		<th></th>
	</tr>
<?php $naam = $voorraad->getLocatie() ;} ?>
	<tr>
		<td><?php echo VoorraadView::waardeOmschrijving($voorraad);?></td>
		<td><?php echo VoorraadView::waardeAantal($voorraad); ?></td>
		<td><?php echo VoorraadView::waardeEersteMutatie($voorraad); ?></td>
		<td><?php echo VoorraadView::waardeLaatsteMutatie($voorraad); ?></td>
		<td><?php echo VoorraadView::waardeVerkoopbaar($voorraad); ?></td>
		<td><?php echo VerkoopPrijsView::waardePrijs($voorraad->getVerkoopPrijs()); ?></td>
		<td><?php if (hasAuth('boekcom')) { ?>
			(<a href='<?php echo $dibsproduct->url() . '/Voorraad/' . $voorraad->geefID() . '/Verplaatsen'; ?>'><?php echo _("verplaatsen"); ?></a>,
			<a href='<?php echo $dibsproduct->url() . '/Voorraad/' . $voorraad->geefID() . '/Toevoegen';?>'><?php echo _('toevoegen'); ?></a>)
			<?php }?></td>
	</tr>
<?php } ?>
</table>
