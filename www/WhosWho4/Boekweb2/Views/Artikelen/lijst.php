<h1><?php echo _("Alle artikelen"); ?></h1>

<h2><?php echo _("Colaproducten"); ?></h2>
<?php if ($colaproducten->aantal() > 0) { ?>
<table>
	<tr>
		<th><?php echo _("Naam"); ?></th>
		<th><?php echo _("Kudos"); ?></th>
		<th><?php echo _("Actief"); ?></th>
	</tr>
<?php foreach ($colaproducten as $product) { ?>
	<tr>
		<td><?php echo ColaProductView::detailsLink($product); ?></td>
		<td><?php echo ColaProductView::waardeKudos($product); ?></td>
		<td><?php echo ColaProductView::waardeActief($product); ?></td>
	</tr>
<?php } ?>
</table>
<?php } ?>

<h2><?php echo _("Dibsproducten"); ?></h2>
<?php if ($dibsproducten->aantal() > 0) { ?>
<table>
	<tr>
		<th><?php echo _("Naam"); ?></th>
		<th><?php echo _("Kudos"); ?></th>
	</tr>
<?php foreach ($dibsproducten as $product) { ?>
	<tr>
		<td><?php echo DibsProductView::detailsLink($product); ?></td>
		<td><?php echo DibsProductView::waardeKudos($product); ?></td>
	</tr>
<?php } ?>
</table>
<?php } ?>

<h2><?php echo _("Dictaten"); ?></h2>
<?php if ($dictaten->aantal() > 0) { ?>
<table>
	<tr>
		<th>Naam</th>
		<th>Auteur</th>
		<th>Versie</th>
		<th>Begin gebruik</th>
		<th>Einde gebruik</th>
		<th>Downloaden</th>
	</tr>
<?php foreach ($dictaten as $dictaat) { ?>
	<tr>
		<td><?php echo DictaatView::detailsLink($dictaat); ?></td>
		<td><?php echo PersoonView::naam($dictaat->getAuteur()); ?></td>
		<td><?php echo DictaatView::waardeUitgaveJaar($dictaat); ?></td>
		<td><?php echo DictaatView::waardeBeginGebruik($dictaat); ?></td>
		<td><?php echo DictaatView::waardeEindeGebruik($dictaat); ?></td>
		<td><?php echo DictaatView::downloadLink($dictaat); ?></td>
	</tr>
<?php } ?>
</table>
<?php } ?>
