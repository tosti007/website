<h1><?php echo DictaatView::waardeNaam($dictaat); ?></h1>

<p>
	<?php printf(_("Klik %shier%s om terug naar de dictaat-pagina te gaan."), '<a href=\''.$dictaat->url().'\'>', '</a>'); ?>
</p>
<p>
	<?php printf(_("Klik %shier%s om alle orders te zien."), '<a href=\''.$dictaat->url().'/Orders\'>', '</a>'); ?>
</p>

<h3><?php echo _("Wijzig waarde van order"); ?></h3>

<form method="post">

<?php if (isset($msg)) {
	foreach ($msg as $m) {
?><p><span class="negatief"><?php
		switch ($m) {
				case BOEKEN_WIJZIGWAARDEORDERFORM_ORDER: echo _("Er waren problemen met het wijzigen van de order."); break;
		}
?></span></p><?php
	}
} ?>

<p>
	<ul>
		<li><?php echo _("Datum geplaatst:") . OrderView::waardeDatumGeplaatst($order); ?></li>
		<li><?php echo _("Aantal:") . OrderView::waardeAantal($order); ?></li>
		<li><?php echo _("Leverancier:") . OrderView::waardeLeverancier($order); ?></li>
	</ul>
</p>

<p>
	<?php echo _("Wat is de nieuwe waarde van deze order?"); ?>
	<input type="text" name="waarde" />
</p>

<p>
	<input type="submit" name="submit" value="<?php echo _("Wijzig waarde"); ?>" />
</p>

</form>
