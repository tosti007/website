<?
/**
 * $Id$
 */
class VerkoopVerzameling
	extends VerkoopVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VerkoopVerzameling_Generated
	}

	static public function vanArtikel (Artikel $artikel)
	{
		return VoorraadMutatieQuery::table()
			->join('Voorraad')
			->whereString('VoorraadMutatie.overerving', 'Verkoop')
			->whereProp('Artikel', $artikel)
			->orderBy('VoorraadMutatie.wanneer')
			->verzamel();
	}

	static public function vanTransactie (Transactie $transactie)
	{
		return VerkoopQuery::table()
			->whereProp('Transactie', $transactie)
			->verzamel();
	}

	static public function vanString ($string, Transactie $transactie, $locatie)
	{
		if (!(preg_match('/^([0-9]+,[0-9]+;)*$/', $string) > 0)
			|| is_null($transactie))
			return NULL;
		$product_array = explode(';', $string);
		$v = new VerkoopVerzameling();

		foreach ($product_array as $product_string)
		{
			if (!empty($product_string))
			{
				$product_waarden = explode(',', $product_string, 2);
				$artikelID = $product_waarden[0];
				$artikel = ColaProduct::geef($artikelID);
				$voorraad = $artikel->getDefaultVoorraad();
				$verkoop = Verkoop::maakVerkoop($voorraad, new DateTimeLocale(), $product_waarden[1], $transactie);
				$v->voegtoe($verkoop);
			}
		}

		return $v;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
