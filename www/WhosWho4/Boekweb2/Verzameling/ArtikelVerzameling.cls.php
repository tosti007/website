<?
/**
 * $Id$
 */
class ArtikelVerzameling
	extends ArtikelVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // ArtikelVerzameling_Generated
	}

	static public function zoek ($arg)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `Artikel`.`artikelID` '
			. 'FROM `Artikel` '
			. 'LEFT JOIN `iDealKaartje` ON `iDealKaartje`.`artikelID` = `Artikel`.`artikelID` '
			. 'LEFT JOIN `Activiteit` ON `Activiteit`.`activiteitID` = `iDealKaartje`.`activiteit_activiteitID` '
			. 'LEFT JOIN `ActiviteitInformatie` ON `Activiteit`.`activiteitID` = `ActiviteitInformatie`.`activiteitID` '
			. 'LEFT JOIN `Boek` ON `Boek`.`artikelID` = `Artikel`.`artikelID` '
			. 'LEFT JOIN `AesArtikel` ON `AesArtikel`.`artikelID` = `Artikel`.`artikelID` '
			. 'LEFT JOIN `Commissie` ON `Commissie`.`commissieID` = `AesArtikel`.`commissie_commissieID` '
			. 'LEFT JOIN `Dictaat` ON `Dictaat`.`artikelID` = `Artikel`.`artikelID` '
			. 'WHERE `Artikel`.`naam` LIKE %c '
			. 'OR `Commissie`.`naam` LIKE %c '
			. 'OR `Commissie`.`login` LIKE %c '
			. 'OR `Commissie`.`email` LIKE %c '
			. 'OR `ActiviteitInformatie`.`titel_NL` LIKE %c '
			. 'OR `ActiviteitInformatie`.`titel_EN` LIKE %c '
			. 'OR `Boek`.`isbn` LIKE %c '
			. 'OR `Boek`.`auteur` LIKE %c '
			. 'OR `Boek`.`uitgever` LIKE %c '
			, $arg, $arg, $arg, $arg, $arg, $arg, $arg, $arg, $arg, $arg);

		return self::verzamel($ids);
	}

	static public function geefVanIDEAL()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT DISTINCT `voorraad_voorraadID` '
			. 'FROM `iDeal` '
			. 'WHERE `voorraad_voorraadID` IS NOT NULL ');

		$voorraden = VoorraadVerzameling::verzamel($ids);

		$artikelen = new ArtikelVerzameling();
		foreach ($voorraden as $v)
			$artikelen->voegtoe($v->getArtikel());

		return $artikelen;
	}

	static public function geefAlleBoekwebArtikelen($sorted = false, $soort = '')
	{
		$query = ArtikelQuery::table()
			->whereNotString('overerving', 'ColaProduct');

		if(!empty($sorted)) {
			$query->whereString('overerving', $soort);
		}

		if($sorted) {
			$query->orderByAsc('overerving');
		}

		return $query->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
