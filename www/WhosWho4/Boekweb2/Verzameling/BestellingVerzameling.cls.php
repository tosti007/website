<?
class BestellingVerzameling
	extends BestellingVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // BestellingVerzameling_Generated
	}

	public static function geefAlleBestellingen()
	{
		return BestellingQuery::table()->verzamel();
	}
}
