<?
/**
 * $Id$
 */
class VakVerzameling
	extends VakVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VakVerzameling_Generated
	}

	/*
	 *  Functie om te kunnen sorteren op code
	 */
	static public function sorteerOpCode($aID, $bID) {
		$a = Vak::geef($aID);
		$b = Vak::geef($bID);
		return strcasecmp($a->getCode(), $b->getCode());
	}

	static public function geefAlleVakken()
	{
		return VakQuery::table()->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
