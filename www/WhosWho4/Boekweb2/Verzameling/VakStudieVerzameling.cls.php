<?
class VakStudieVerzameling
	extends VakStudieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VakStudieVerzameling_Generated
	}

	public static function setStudies(Vak $vak, StudieVerzameling $studies)
	{
		$huidigeStudies = VakStudieVerzameling::fromVak($vak);
		$toekomstigeStudies = new VakStudieVerzameling();
		foreach ($studies as $key => $value) {
			$toekomstigeStudies->voegtoe(new VakStudie($vak, $value));
		}

		// Pak alle studies die wel in 'studies' zitten, maar niet in de DB
		$inserts = clone $toekomstigeStudies;
		$inserts->complement($huidigeStudies);

		// Pak alle studies die wel in de DB zitten, maar niet in ons lijstje
		$deletes = $huidigeStudies;
		$deletes->complement($toekomstigeStudies);

		if ($inserts != null && $inserts->aantal() > 0) $inserts->opslaan();
		if ($deletes != null && $deletes->aantal() > 0) $deletes->verwijderen();
	}
}
