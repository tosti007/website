<?
class iDealKaartjeVerzameling
	extends iDealKaartjeVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // iDealKaartjeVerzameling_Generated
	}

	static public function geefPotentieelScanbareKaartjes(Activiteit $activiteit)
	{
		return iDealKaartjeQuery::table()
			->whereProp('Activiteit', $activiteit)
			->whereProp('digitaalVerkrijgbaar', 'CODE')
			->verzamel();
	}

	static public function geefScanbareKaartjes(Activiteit $activiteit)
	{
		return iDealKaartjeQuery::table()
			->whereProp('Activiteit', $activiteit)
			->whereProp('digitaalVerkrijgbaar', 'CODE')
			->whereProp('magScannen', true)
			->verzamel();
	}

	/**
	 * @brief Geeft alle codes die bij de kaartjes in de verzameling horen.
	 *
	 * @return iDealKaartjeCodeVerzameling
	 */
	public function geefKaartjesCodes()
	{
		global $WSW4DB;

		$ids = array();
		foreach($this as $kaartje) {
			$idsTmp = $WSW4DB->q("COLUMN SELECT `iDealKaartjeCode`.`id` "
				. "FROM `iDealKaartjeCode` "
				. "WHERE `kaartje_artikelID` = %i"
				, $kaartje->geefID());
			$ids = array_merge($ids, $idsTmp);
		}

		return iDealKaartjeCodeVerzameling::verzamel($ids);
	}

	static public function vanActiviteit(Activiteit $act)
	{
		return iDealKaartjeQuery::table()
			->whereProp('Activiteit', $act)
			->verzamel();
	}
}
