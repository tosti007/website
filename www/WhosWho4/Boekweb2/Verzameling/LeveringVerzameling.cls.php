<?
/**
 * $Id$
 */
class LeveringVerzameling
	extends LeveringVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // LeveringVerzameling_Generated
	}

	/**
	 *  Maak het mogelijk om een LeveringVerzameling
	 * te sorteren op Levering::getWanneer.
	 *
	 *	@see Verzameling::sorteer()
	 */
	public static function sorteerOpWanneer($aID, $bID)
	{
		$a = Levering::geef($aID)->getWanneer();
		$b = Levering::geef($bID)->getWanneer();

		if($a > $b)
			return 1;
		if($a < $b)
			return -1;

		return 0;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
