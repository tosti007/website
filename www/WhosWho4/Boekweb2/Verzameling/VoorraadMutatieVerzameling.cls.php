<?
/**
 * $Id$
 */
class VoorraadMutatieVerzameling
	extends VoorraadMutatieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieVerzameling_Generated
	}

	public function totaal ()
	{
		$totaal = 0;
		foreach ($this as $mutatie)
			$totaal += $mutatie->getAantal();
		return $totaal;
	}

	static public function vanArtikel (Artikel $artikel)
	{
		return VoorraadQuery::table()
			->join('Voorraad')
			->whereProp('Voorraad.artikel', $artikel)
			->orderByAsc('wanneer')
			->verzamel();
	}

	static public function voorraadMutatiesTot (Voorraad $voorraad, $tot)
	{
		$tot = ($tot instanceof DateTimeLocale) ? $tot : new DateTimeLocale($tot);

		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $voorraad)
			->whereProp('wanneer', '<', $tot)
			->sum('aantal');
	}

	static public function vanVoorraad (Voorraad $voorraad)
	{
		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}

	static public function vanVoorraadVerkopen (Voorraad $voorraad)
	{
		return VoorraadMutatieQuery::table()
			->whereString('overerving', 'Verkoop')
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}

	static public function vanVoorraadVanTot (Voorraad $voorraad, $van = 0, $tot = NULL)
	{
		$van = ($van instanceof DateTimeLocale) ? $van : new DateTimeLocale($tot);
		$tot = ($tot instanceof DateTimeLocale) ? $tot : new DateTimeLocale($tot);

		return VoorraadMutatieQuery::table()
			->whereProp('Voorraad', $voorraad)
			->whereProp('wanneer', '>', $van)
			->whereProp('wanneer', '<', $tot)
			->verzamel();
	}

	static public function colaproductLeveringen ($van = 0, $tot = 0)
	{
		$van = ($van instanceof DateTimeLocale) ? $van : new DateTimeLocale($van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot : new DateTimeLocale($tot);

		return VoorraadMutatieQuery::table()
			->join('Levering')
			->join('Leverancier')
			->whereProp('Colaproducten', true)
			->whereProp('VoorraadMutatie.wanneer', '>=', $van)
			->whereProp('VoorraadMutatie.wanneer', '<=', $tot)
			->verzamel();
	}

	static public function colaproductVerkopen ($van, $tot)
	{
		$van = ($van instanceof DateTimeLocale) ? $van->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $van);
		$tot = ($tot instanceof DateTimeLocale) ? $tot->format('Y-m-d H:i:s') : date('Y-m-d H:i:s', $tot);

		return VoorraadMutatieQuery::table()
			->join('Voorraad')
			->join('Artikel')
			->whereString('VoorraadMutatie.overerving', 'Verkoop')
			->whereString('Artikel.overerving', 'ColaProduct')
			->whereProp('VoorraadMutatie.wanneer', '>=', $van)
			->whereProp('VoorraadMutatie.wanneer', '<=', $tot)
			->wherePropNotNull('Voorraad.omschrijving')
			->orderByAsc('wanneer')
			->verzamel();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
