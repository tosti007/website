<?
/**
 * $Id$
 */
class VoorraadVerzameling
	extends VoorraadVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VoorraadVerzameling_Generated
	}

	static public function voorradenVanArtikel (Artikel $artikel)
    {
		return VoorraadQuery::table()
			->whereProp('Artikel', $artikel)
			->orderByAsc('locatie')
			->verzamel();
    }

	static public function voorradenInBoekenhok (Artikel $artikel)
	{
		global $WSW4DB;
		$ids = $WSW4DB->q('COLUMN SELECT `t`.`i` FROM (SELECT `Voorraad`.`voorraadID` AS `i`, SUM(`VoorraadMutatie`.`aantal`) AS `s` FROM `VoorraadMutatie` '
						. 'LEFT JOIN `Voorraad` ON `VoorraadMutatie`.`voorraad_voorraadID` = `Voorraad`.`voorraadID` '
						. 'WHERE `Voorraad`.`artikel_artikelID` = %i AND `Voorraad`.`locatie` = \'boekenhok\''
						. 'GROUP BY `Voorraad`.`artikel_artikelID`) AS `t` '
						. 'WHERE `t`.`s` > 0',
						$artikel->geefID());
		return self::verzamel($ids);
	}

	static public function verkoopbaarVanArtikel (Artikel $artikel)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `Voorraad`.`voorraadID` FROM `Voorraad` '
						. 'LEFT JOIN ('
						.	'SELECT `VoorraadMutatie`.`voorraad_voorraadID`, SUM(`VoorraadMutatie`.`aantal`) AS `a` FROM `VoorraadMutatie` '
						.	'GROUP BY `VoorraadMutatie`.`voorraad_voorraadID`'
						. ') AS `t` ON `t`.`voorraad_voorraadID` = `Voorraad`.`voorraadID` '
						. 'WHERE `Voorraad`.`artikel_artikelID` = %i '
						. 'AND `t`.`a` > 0 '
						. 'AND `Voorraad`.`verkoopbaar` = 1 '
						. 'ORDER BY `Voorraad`.`locatie`',
						$artikel->geefID());

		return self::verzamel($ids);
	}

	static public function onverkoopbaarVanArtikel (Artikel $artikel)
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `Voorraad`.`voorraadID` FROM `Voorraad` '
						. 'LEFT JOIN ('
						.	'SELECT `VoorraadMutatie`.`voorraad_voorraadID`, SUM(`VoorraadMutatie`.`aantal`) AS `a` FROM `VoorraadMutatie` '
						.	'GROUP BY `VoorraadMutatie`.`voorraad_voorraadID`'
						. ') AS `t` ON `t`.`voorraad_voorraadID` = `Voorraad`.`voorraadID` '
						. 'WHERE `Voorraad`.`artikel_artikelID` = %i '
						. 'AND `t`.`a` > 0 '
						. 'AND `Voorraad`.`verkoopbaar` = 0 '
						. 'ORDER BY `Voorraad`.`locatie`',
						$artikel->geefID());

		return self::verzamel($ids);
	}

	public function getAantal ()
	{
		if ($this->aantal() == 0)
			return 0;

		return VoorraadMutatieQuery::table()
			->whereInProp('Voorraad', $this)
			->sum('aantal');
	}

	public function mutatiesPerVoorraad ($van, $tot)
	{
		$arr = array();
		foreach ($this as $voorraad)
		{
			$mutaties = VoorraadMutatieVerzameling::vanVoorraadVanTot($voorraad, $van, $tot);
			$arr[$voorraad->geefID()] = $mutaties;
		}
		return $arr;
	}

	static public function geefAlleColaproductVoorraden()
	{
		return VoorraadQuery::table()
			->join('Artikel')
			->whereString('Artikel.overerving', 'ColaProduct')
			->verzamel();
	}

	static public function alleVoorradenPerLocatie()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q('COLUMN SELECT `Voorraad`.`voorraadID` '
			. 'FROM `VoorraadMutatie` '
			. 'LEFT JOIN `Voorraad` ON `VoorraadMutatie`.`voorraad_voorraadID` = `Voorraad`.`voorraadID` '
			. 'GROUP BY `Voorraad`.`locatie` '
			. 'HAVING SUM(`VoorraadMutatie`.`aantal`) > 0 '
		);
		return self::verzamel($ids);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
