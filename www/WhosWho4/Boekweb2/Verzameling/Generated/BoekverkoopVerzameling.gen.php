<?
abstract class BoekverkoopVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BoekverkoopVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BoekverkoopVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze BoekverkoopVerzameling.
	 */
	public function toOpenerVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getOpenerContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze BoekverkoopVerzameling een LidVerzameling.
	 *
	 * @return LidVerzameling
	 * Een LidVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze BoekverkoopVerzameling.
	 */
	public function toSluiterVerzameling()
	{
		if($this->aantal() == 0)
			return new LidVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getSluiterContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getSluiterContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LidVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BoekverkoopVerzameling van Opener.
	 *
	 * @return BoekverkoopVerzameling
	 * Een BoekverkoopVerzameling die elementen bevat die bij de Opener hoort.
	 */
	static public function fromOpener($opener)
	{
		if(!isset($opener))
			return new BoekverkoopVerzameling();

		return BoekverkoopQuery::table()
			->whereProp('Opener', $opener)
			->verzamel();
	}
	/**
	 * @brief Maak een BoekverkoopVerzameling van Sluiter.
	 *
	 * @return BoekverkoopVerzameling
	 * Een BoekverkoopVerzameling die elementen bevat die bij de Sluiter hoort.
	 */
	static public function fromSluiter($sluiter)
	{
		if(!isset($sluiter))
			return new BoekverkoopVerzameling();

		return BoekverkoopQuery::table()
			->whereProp('Sluiter', $sluiter)
			->verzamel();
	}
}
