<?
abstract class OfferteVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de OfferteVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze OfferteVerzameling een ArtikelVerzameling.
	 *
	 * @return ArtikelVerzameling
	 * Een ArtikelVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze OfferteVerzameling.
	 */
	public function toArtikelVerzameling()
	{
		if($this->aantal() == 0)
			return new ArtikelVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getArtikelArtikelID()
			                      );
		}
		$this->positie = $origPositie;
		return ArtikelVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze OfferteVerzameling een LeverancierVerzameling.
	 *
	 * @return LeverancierVerzameling
	 * Een LeverancierVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze OfferteVerzameling.
	 */
	public function toLeverancierVerzameling()
	{
		if($this->aantal() == 0)
			return new LeverancierVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLeverancierContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LeverancierVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een OfferteVerzameling van Artikel.
	 *
	 * @return OfferteVerzameling
	 * Een OfferteVerzameling die elementen bevat die bij de Artikel hoort.
	 */
	static public function fromArtikel($artikel)
	{
		if(!isset($artikel))
			return new OfferteVerzameling();

		return OfferteQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();
	}
	/**
	 * @brief Maak een OfferteVerzameling van Leverancier.
	 *
	 * @return OfferteVerzameling
	 * Een OfferteVerzameling die elementen bevat die bij de Leverancier hoort.
	 */
	static public function fromLeverancier($leverancier)
	{
		if(!isset($leverancier))
			return new OfferteVerzameling();

		return OfferteQuery::table()
			->whereProp('Leverancier', $leverancier)
			->verzamel();
	}
}
