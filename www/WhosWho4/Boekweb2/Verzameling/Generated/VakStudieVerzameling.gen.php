<?
abstract class VakStudieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de VakStudieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze VakStudieVerzameling een VakVerzameling.
	 *
	 * @return VakVerzameling
	 * Een VakVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze VakStudieVerzameling.
	 */
	public function toVakVerzameling()
	{
		if($this->aantal() == 0)
			return new VakVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVakVakID()
			                      );
		}
		$this->positie = $origPositie;
		return VakVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze VakStudieVerzameling een StudieVerzameling.
	 *
	 * @return StudieVerzameling
	 * Een StudieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VakStudieVerzameling.
	 */
	public function toStudieVerzameling()
	{
		if($this->aantal() == 0)
			return new StudieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getStudieStudieID()
			                      );
		}
		$this->positie = $origPositie;
		return StudieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VakStudieVerzameling van Vak.
	 *
	 * @return VakStudieVerzameling
	 * Een VakStudieVerzameling die elementen bevat die bij de Vak hoort.
	 */
	static public function fromVak($vak)
	{
		if(!isset($vak))
			return new VakStudieVerzameling();

		return VakStudieQuery::table()
			->whereProp('Vak', $vak)
			->verzamel();
	}
	/**
	 * @brief Maak een VakStudieVerzameling van Studie.
	 *
	 * @return VakStudieVerzameling
	 * Een VakStudieVerzameling die elementen bevat die bij de Studie hoort.
	 */
	static public function fromStudie($studie)
	{
		if(!isset($studie))
			return new VakStudieVerzameling();

		return VakStudieQuery::table()
			->whereProp('Studie', $studie)
			->verzamel();
	}
}
