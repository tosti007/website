<?
abstract class JaargangArtikelVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de JaargangArtikelVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze JaargangArtikelVerzameling een JaargangVerzameling.
	 *
	 * @return JaargangVerzameling
	 * Een JaargangVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze JaargangArtikelVerzameling.
	 */
	public function toJaargangVerzameling()
	{
		if($this->aantal() == 0)
			return new JaargangVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getJaargangJaargangID()
			                      );
		}
		$this->positie = $origPositie;
		return JaargangVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze JaargangArtikelVerzameling een ArtikelVerzameling.
	 *
	 * @return ArtikelVerzameling
	 * Een ArtikelVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze JaargangArtikelVerzameling.
	 */
	public function toArtikelVerzameling()
	{
		if($this->aantal() == 0)
			return new ArtikelVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getArtikelArtikelID()
			                      );
		}
		$this->positie = $origPositie;
		return ArtikelVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een JaargangArtikelVerzameling van Jaargang.
	 *
	 * @return JaargangArtikelVerzameling
	 * Een JaargangArtikelVerzameling die elementen bevat die bij de Jaargang hoort.
	 */
	static public function fromJaargang($jaargang)
	{
		if(!isset($jaargang))
			return new JaargangArtikelVerzameling();

		return JaargangArtikelQuery::table()
			->whereProp('Jaargang', $jaargang)
			->verzamel();
	}
	/**
	 * @brief Maak een JaargangArtikelVerzameling van Artikel.
	 *
	 * @return JaargangArtikelVerzameling
	 * Een JaargangArtikelVerzameling die elementen bevat die bij de Artikel hoort.
	 */
	static public function fromArtikel($artikel)
	{
		if(!isset($artikel))
			return new JaargangArtikelVerzameling();

		return JaargangArtikelQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();
	}
}
