<?
abstract class VerkoopVerzameling_Generated
	extends VoorraadMutatieVerzameling
{
	/**
	 * @brief De constructor van de VerkoopVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieVerzameling
	}
	/**
	 * @brief Maak van deze VerkoopVerzameling een TransactieVerzameling.
	 *
	 * @return TransactieVerzameling
	 * Een TransactieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze VerkoopVerzameling.
	 */
	public function toTransactieVerzameling()
	{
		if($this->aantal() == 0)
			return new TransactieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTransactieTransactieID()
			                      );
		}
		$this->positie = $origPositie;
		return TransactieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VerkoopVerzameling van Transactie.
	 *
	 * @return VerkoopVerzameling
	 * Een VerkoopVerzameling die elementen bevat die bij de Transactie hoort.
	 */
	static public function fromTransactie($transactie)
	{
		if(!isset($transactie))
			return new VerkoopVerzameling();

		return VerkoopQuery::table()
			->whereProp('Transactie', $transactie)
			->verzamel();
	}
}
