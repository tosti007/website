<?
abstract class DictaatVerzameling_Generated
	extends ArtikelVerzameling
{
	/**
	 * @brief De constructor van de DictaatVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ArtikelVerzameling
	}
	/**
	 * @brief Maak van deze DictaatVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze DictaatVerzameling.
	 */
	public function toAuteurVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getAuteurContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een DictaatVerzameling van Auteur.
	 *
	 * @return DictaatVerzameling
	 * Een DictaatVerzameling die elementen bevat die bij de Auteur hoort.
	 */
	static public function fromAuteur($auteur)
	{
		if(!isset($auteur))
			return new DictaatVerzameling();

		return DictaatQuery::table()
			->whereProp('Auteur', $auteur)
			->verzamel();
	}
}
