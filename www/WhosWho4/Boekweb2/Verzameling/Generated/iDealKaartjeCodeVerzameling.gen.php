<?
abstract class iDealKaartjeCodeVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de iDealKaartjeCodeVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze iDealKaartjeCodeVerzameling een iDealKaartjeVerzameling.
	 *
	 * @return iDealKaartjeVerzameling
	 * Een iDealKaartjeVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze iDealKaartjeCodeVerzameling.
	 */
	public function toKaartjeVerzameling()
	{
		if($this->aantal() == 0)
			return new iDealKaartjeVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getKaartjeArtikelID()
			                      );
		}
		$this->positie = $origPositie;
		return iDealKaartjeVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze iDealKaartjeCodeVerzameling een TransactieVerzameling.
	 *
	 * @return TransactieVerzameling
	 * Een TransactieVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze iDealKaartjeCodeVerzameling.
	 */
	public function toTransactieVerzameling()
	{
		if($this->aantal() == 0)
			return new TransactieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getTransactieTransactieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getTransactieTransactieID()
			                      );
		}
		$this->positie = $origPositie;
		return TransactieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een iDealKaartjeCodeVerzameling van Kaartje.
	 *
	 * @return iDealKaartjeCodeVerzameling
	 * Een iDealKaartjeCodeVerzameling die elementen bevat die bij de Kaartje hoort.
	 */
	static public function fromKaartje($kaartje)
	{
		if(!isset($kaartje))
			return new iDealKaartjeCodeVerzameling();

		return iDealKaartjeCodeQuery::table()
			->whereProp('Kaartje', $kaartje)
			->verzamel();
	}
	/**
	 * @brief Maak een iDealKaartjeCodeVerzameling van Transactie.
	 *
	 * @return iDealKaartjeCodeVerzameling
	 * Een iDealKaartjeCodeVerzameling die elementen bevat die bij de Transactie hoort.
	 */
	static public function fromTransactie($transactie)
	{
		if(!isset($transactie))
			return new iDealKaartjeCodeVerzameling();

		return iDealKaartjeCodeQuery::table()
			->whereProp('Transactie', $transactie)
			->verzamel();
	}
}
