<?
abstract class AesArtikelVerzameling_Generated
	extends ArtikelVerzameling
{
	/**
	 * @brief De constructor van de AesArtikelVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ArtikelVerzameling
	}
	/**
	 * @brief Maak van deze AesArtikelVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze AesArtikelVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een AesArtikelVerzameling van Commissie.
	 *
	 * @return AesArtikelVerzameling
	 * Een AesArtikelVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new AesArtikelVerzameling();

		return AesArtikelQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
}
