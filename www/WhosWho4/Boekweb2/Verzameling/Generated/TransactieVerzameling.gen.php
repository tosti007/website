<?
abstract class TransactieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TransactieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TransactieVerzameling een ContactVerzameling.
	 *
	 * @return ContactVerzameling
	 * Een ContactVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TransactieVerzameling.
	 */
	public function toContactVerzameling()
	{
		if($this->aantal() == 0)
			return new ContactVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getContactContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getContactContactID()
			                      );
		}
		$this->positie = $origPositie;
		return ContactVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TransactieVerzameling van Contact.
	 *
	 * @return TransactieVerzameling
	 * Een TransactieVerzameling die elementen bevat die bij de Contact hoort.
	 */
	static public function fromContact($contact)
	{
		if(!isset($contact))
			return new TransactieVerzameling();

		return TransactieQuery::table()
			->whereProp('Contact', $contact)
			->verzamel();
	}
}
