<?
abstract class iDealKaartjeVerzameling_Generated
	extends ArtikelVerzameling
{
	/**
	 * @brief De constructor van de iDealKaartjeVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ArtikelVerzameling
	}
	/**
	 * @brief Maak van deze iDealKaartjeVerzameling een ActiviteitVerzameling.
	 *
	 * @return ActiviteitVerzameling
	 * Een ActiviteitVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze iDealKaartjeVerzameling.
	 */
	public function toActiviteitVerzameling()
	{
		if($this->aantal() == 0)
			return new ActiviteitVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getActiviteitActiviteitID()
			                      );
		}
		$this->positie = $origPositie;
		return ActiviteitVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een iDealKaartjeVerzameling van Activiteit.
	 *
	 * @return iDealKaartjeVerzameling
	 * Een iDealKaartjeVerzameling die elementen bevat die bij de Activiteit hoort.
	 */
	static public function fromActiviteit($activiteit)
	{
		if(!isset($activiteit))
			return new iDealKaartjeVerzameling();

		return iDealKaartjeQuery::table()
			->whereProp('Activiteit', $activiteit)
			->verzamel();
	}
}
