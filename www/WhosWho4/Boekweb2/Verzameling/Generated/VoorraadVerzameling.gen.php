<?
abstract class VoorraadVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de VoorraadVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze VoorraadVerzameling een ArtikelVerzameling.
	 *
	 * @return ArtikelVerzameling
	 * Een ArtikelVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze VoorraadVerzameling.
	 */
	public function toArtikelVerzameling()
	{
		if($this->aantal() == 0)
			return new ArtikelVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getArtikelArtikelID()
			                      );
		}
		$this->positie = $origPositie;
		return ArtikelVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een VoorraadVerzameling van Artikel.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die bij de Artikel hoort.
	 */
	static public function fromArtikel($artikel)
	{
		if(!isset($artikel))
			return new VoorraadVerzameling();

		return VoorraadQuery::table()
			->whereProp('Artikel', $artikel)
			->verzamel();
	}
}
