<?
abstract class LeverancierRetourVerzameling_Generated
	extends VoorraadMutatieVerzameling
{
	/**
	 * @brief De constructor van de LeverancierRetourVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieVerzameling
	}
	/**
	 * @brief Maak van deze LeverancierRetourVerzameling een OrderVerzameling.
	 *
	 * @return OrderVerzameling
	 * Een OrderVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze LeverancierRetourVerzameling.
	 */
	public function toOrderVerzameling()
	{
		if($this->aantal() == 0)
			return new OrderVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getOrderOrderID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getOrderOrderID()
			                      );
		}
		$this->positie = $origPositie;
		return OrderVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze LeverancierRetourVerzameling een LeverancierVerzameling.
	 *
	 * @return LeverancierVerzameling
	 * Een LeverancierVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze LeverancierRetourVerzameling.
	 */
	public function toLeverancierVerzameling()
	{
		if($this->aantal() == 0)
			return new LeverancierVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getLeverancierContactID()
			                      );
		}
		$this->positie = $origPositie;
		return LeverancierVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een LeverancierRetourVerzameling van Order.
	 *
	 * @return LeverancierRetourVerzameling
	 * Een LeverancierRetourVerzameling die elementen bevat die bij de Order hoort.
	 */
	static public function fromOrder($order)
	{
		if(!isset($order))
			return new LeverancierRetourVerzameling();

		return LeverancierRetourQuery::table()
			->whereProp('Order', $order)
			->verzamel();
	}
	/**
	 * @brief Maak een LeverancierRetourVerzameling van Leverancier.
	 *
	 * @return LeverancierRetourVerzameling
	 * Een LeverancierRetourVerzameling die elementen bevat die bij de Leverancier
	 * hoort.
	 */
	static public function fromLeverancier($leverancier)
	{
		if(!isset($leverancier))
			return new LeverancierRetourVerzameling();

		return LeverancierRetourQuery::table()
			->whereProp('Leverancier', $leverancier)
			->verzamel();
	}
}
