<?
abstract class BarcodeVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de BarcodeVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze BarcodeVerzameling een VoorraadVerzameling.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze BarcodeVerzameling.
	 */
	public function toVoorraadVerzameling()
	{
		if($this->aantal() == 0)
			return new VoorraadVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getVoorraadVoorraadID()
			                      );
		}
		$this->positie = $origPositie;
		return VoorraadVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een BarcodeVerzameling van Voorraad.
	 *
	 * @return BarcodeVerzameling
	 * Een BarcodeVerzameling die elementen bevat die bij de Voorraad hoort.
	 */
	static public function fromVoorraad($voorraad)
	{
		if(!isset($voorraad))
			return new BarcodeVerzameling();

		return BarcodeQuery::table()
			->whereProp('Voorraad', $voorraad)
			->verzamel();
	}
}
