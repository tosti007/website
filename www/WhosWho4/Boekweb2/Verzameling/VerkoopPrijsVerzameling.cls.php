<?
class VerkoopPrijsVerzameling
	extends VerkoopPrijsVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // VerkoopPrijsVerzameling_Generated
	}

	static public function geefAlleVanVoorraad(Voorraad $voorraad)
	{
		return VerkoopPrijsQuery::table()
			->whereProp('Voorraad', $voorraad)
			->orderByDesc('wanneer')
			->verzamel();
	}
}
