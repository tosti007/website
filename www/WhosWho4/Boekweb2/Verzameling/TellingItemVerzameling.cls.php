<?
class TellingItemVerzameling
	extends TellingItemVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // TellingItemVerzameling_Generated
	}

	static public function geefItemsVanTelling(Telling $telling)
	{
		return TellingItemQuery::table()
			->whereProp('Telling', $telling)
			->verzamel();
	}
}
