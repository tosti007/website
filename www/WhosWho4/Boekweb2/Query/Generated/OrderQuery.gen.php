<?
abstract class OrderQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de OrderQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een OrderQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw OrderQuery-object aan om er zeker van te zijn
	 * dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return OrderQuery
	 * Het OrderQuery-object waarvan meteen de db-table van het Order-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new OrderQuery();

		$query->tables('Order');

		return $query;
	}

	/**
	 * @brief Maakt een OrderVerzameling aan van de objecten die geselecteerd worden
	 * door de OrderQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return OrderVerzameling
	 * Een OrderVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Order', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Order-iterator aan van de objecten die geselecteerd worden door
	 * de OrderQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een OrderVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Order', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Order die geslecteeerd wordt door de OrderQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return OrderVerzameling
	 * Een OrderVerzameling verkregen door de query.
	 */
	public function geef($object = 'Order')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Order.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'overerving',
			'orderID',
			'artikel_artikelID',
			'leverancier_contactID',
			'waardePerStuk',
			'datumGeplaatst',
			'geannuleerd',
			'aantal',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Order';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Order een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Order een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Order'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Artikel' => array(
				'artikel_artikelID' => 'artikelID'
			),
			'Leverancier' => array(
				'leverancier_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Order.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Order.orderID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'orderid':
			$type = 'int';
			$field = 'orderID';
			break;
		case 'artikel':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Artikel; })))
					user_error('Alle waarden in de array moeten van type Artikel zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Artikel) && !($value instanceof ArtikelVerzameling))
				user_error('Value moet van type Artikel zijn', E_USER_ERROR);
			$field = 'artikel_artikelID';
			if(is_array($value) || $value instanceof ArtikelVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getArtikelID();
				$value = $new_value;
			}
			else $value = $value->getArtikelID();
			$type = 'int';
			break;
		case 'leverancier':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Leverancier; })))
					user_error('Alle waarden in de array moeten van type Leverancier zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Leverancier) && !($value instanceof LeverancierVerzameling))
				user_error('Value moet van type Leverancier zijn', E_USER_ERROR);
			$field = 'leverancier_contactID';
			if(is_array($value) || $value instanceof LeverancierVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'waardeperstuk':
			$type = 'float';
			$field = 'waardePerStuk';
			break;
		case 'datumgeplaatst':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'datumGeplaatst';
			break;
		case 'geannuleerd':
			$type = 'int';
			$field = 'geannuleerd';
			break;
		case 'aantal':
			$type = 'int';
			$field = 'aantal';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Order.'.$v;
			}
		} else {
			$field = 'Order.'.$field;
		}

		return array($field, $value, $type);
	}

}
