<?
abstract class VakQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de VakQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een VakQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw VakQuery-object aan om er zeker van te zijn dat
	 * we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VakQuery
	 * Het VakQuery-object waarvan meteen de db-table van het Vak-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new VakQuery();

		$query->tables('Vak');

		return $query;
	}

	/**
	 * @brief Maakt een VakVerzameling aan van de objecten die geselecteerd worden door
	 * de VakQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VakVerzameling
	 * Een VakVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Vak', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Vak-iterator aan van de objecten die geselecteerd worden door
	 * de VakQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VakVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Vak', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Vak die geslecteeerd wordt door de VakQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VakVerzameling
	 * Een VakVerzameling verkregen door de query.
	 */
	public function geef($object = 'Vak')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Vak.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'vakID',
			'code',
			'naam',
			'departement',
			'opmerking',
			'inTentamenLijsten',
			'tentamensOutdated',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Vak';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Vak een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Vak een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Vak'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Vak.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Vak.vakID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'vakid':
			$type = 'int';
			$field = 'vakID';
			break;
		case 'code':
			$type = 'string';
			$field = 'code';
			break;
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		case 'departement':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Vak::enumsDepartement()))
						user_error($value . ' is niet een geldige waarde voor departement', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Vak::enumsDepartement()))
				user_error($value . ' is niet een geldige waarde voor departement', E_USER_ERROR);
			$type = 'string';
			$field = 'departement';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'opmerking';
			break;
		case 'intentamenlijsten':
			$type = 'int';
			$field = 'inTentamenLijsten';
			break;
		case 'tentamensoutdated':
			$type = 'int';
			$field = 'tentamensOutdated';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Vak.'.$v;
			}
		} else {
			$field = 'Vak.'.$field;
		}

		return array($field, $value, $type);
	}

}
