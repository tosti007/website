<?
abstract class DictaatQuery_Generated
	extends ArtikelQuery
{
	/**
	 * @brief De constructor van de DictaatQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ArtikelQuery
	}
	/**
	 * @brief Maakt een DictaatQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw DictaatQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return DictaatQuery
	 * Het DictaatQuery-object waarvan meteen de db-table van het Dictaat-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new DictaatQuery();

		$query->tables('Dictaat');

		return $query;
	}

	/**
	 * @brief Maakt een DictaatVerzameling aan van de objecten die geselecteerd worden
	 * door de DictaatQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return DictaatVerzameling
	 * Een DictaatVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Dictaat', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Dictaat-iterator aan van de objecten die geselecteerd worden
	 * door de DictaatQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een DictaatVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Dictaat', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Dictaat die geslecteeerd wordt door de DictaatQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return DictaatVerzameling
	 * Een DictaatVerzameling verkregen door de query.
	 */
	public function geef($object = 'Dictaat')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Dictaat.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'artikelID',
			'auteur_contactID',
			'uitgaveJaar',
			'digitaalVerspreiden',
			'beginGebruik',
			'eindeGebruik',
			'copyrightOpmerking'
		);

		if(in_array($field, $varArray))
			return 'Dictaat';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Dictaat een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Artikel' => array(
				'Artikel.artikelID' => 'Dictaat.artikelID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Dictaat een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Dictaat'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Persoon' => array(
				'auteur_contactID' => 'contactID'
			),
			'Artikel' => array(
				'Artikel.artikelID' => 'Dictaat.artikelID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Dictaat.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Dictaat.artikelID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'auteur':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'auteur_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'uitgavejaar':
			$type = 'int';
			$field = 'uitgaveJaar';
			break;
		case 'digitaalverspreiden':
			$type = 'int';
			$field = 'digitaalVerspreiden';
			break;
		case 'begingebruik':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'beginGebruik';
			break;
		case 'eindegebruik':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'eindeGebruik';
			break;
		case 'copyrightopmerking':
			$type = 'string';
			$field = 'copyrightOpmerking';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Dictaat.'.$v;
			}
		} else {
			$field = 'Dictaat.'.$field;
		}

		return array($field, $value, $type);
	}

}
