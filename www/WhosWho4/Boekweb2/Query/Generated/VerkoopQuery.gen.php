<?
abstract class VerkoopQuery_Generated
	extends VoorraadMutatieQuery
{
	/**
	 * @brief De constructor van de VerkoopQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieQuery
	}
	/**
	 * @brief Maakt een VerkoopQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw VerkoopQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VerkoopQuery
	 * Het VerkoopQuery-object waarvan meteen de db-table van het Verkoop-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new VerkoopQuery();

		$query->tables('Verkoop');

		return $query;
	}

	/**
	 * @brief Maakt een VerkoopVerzameling aan van de objecten die geselecteerd worden
	 * door de VerkoopQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VerkoopVerzameling
	 * Een VerkoopVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Verkoop', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Verkoop-iterator aan van de objecten die geselecteerd worden
	 * door de VerkoopQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VerkoopVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Verkoop', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Verkoop die geslecteeerd wordt door de VerkoopQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VerkoopVerzameling
	 * Een VerkoopVerzameling verkregen door de query.
	 */
	public function geef($object = 'Verkoop')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Verkoop.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'voorraadMutatieID',
			'transactie_transactieID'
		);

		if(in_array($field, $varArray))
			return 'Verkoop';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Verkoop een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('VoorraadMutatie' => array(
				'VoorraadMutatie.voorraadMutatieID' => 'Verkoop.voorraadMutatieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Verkoop een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Verkoop'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Transactie' => array(
				'transactie_transactieID' => 'transactieID'
			),
			'Voorraad' => array(
				'voorraad_voorraadID' => 'voorraadID'
			),
			'VoorraadMutatie' => array(
				'VoorraadMutatie.voorraadMutatieID' => 'Verkoop.voorraadMutatieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Verkoop.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Verkoop.voorraadMutatieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'transactie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Transactie; })))
					user_error('Alle waarden in de array moeten van type Transactie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Transactie) && !($value instanceof TransactieVerzameling))
				user_error('Value moet van type Transactie zijn', E_USER_ERROR);
			$field = 'transactie_transactieID';
			if(is_array($value) || $value instanceof TransactieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getTransactieID();
				$value = $new_value;
			}
			else $value = $value->getTransactieID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Verkoop.'.$v;
			}
		} else {
			$field = 'Verkoop.'.$field;
		}

		return array($field, $value, $type);
	}

}
