<?
abstract class VakStudieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de VakStudieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een VakStudieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw VakStudieQuery-object aan om er zeker van
	 * te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VakStudieQuery
	 * Het VakStudieQuery-object waarvan meteen de db-table van het VakStudie-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new VakStudieQuery();

		$query->tables('VakStudie');

		return $query;
	}

	/**
	 * @brief Maakt een VakStudieVerzameling aan van de objecten die geselecteerd
	 * worden door de VakStudieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VakStudieVerzameling
	 * Een VakStudieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'VakStudie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een VakStudie-iterator aan van de objecten die geselecteerd worden
	 * door de VakStudieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VakStudieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'VakStudie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een VakStudie die geslecteeerd wordt door de VakStudieQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VakStudieVerzameling
	 * Een VakStudieVerzameling verkregen door de query.
	 */
	public function geef($object = 'VakStudie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van VakStudie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'vak_vakID',
			'studie_studieID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'VakStudie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als VakStudie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan VakStudie een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('VakStudie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Vak' => array(
				'vak_vakID' => 'vakID'
			),
			'Studie' => array(
				'studie_studieID' => 'studieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van VakStudie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'VakStudie.vak_vakID',
			'VakStudie.studie_studieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'vak':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Vak; })))
					user_error('Alle waarden in de array moeten van type Vak zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Vak) && !($value instanceof VakVerzameling))
				user_error('Value moet van type Vak zijn', E_USER_ERROR);
			$field = 'vak_vakID';
			if(is_array($value) || $value instanceof VakVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getVakID();
				$value = $new_value;
			}
			else $value = $value->getVakID();
			$type = 'int';
			break;
		case 'studie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Studie; })))
					user_error('Alle waarden in de array moeten van type Studie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Studie) && !($value instanceof StudieVerzameling))
				user_error('Value moet van type Studie zijn', E_USER_ERROR);
			$field = 'studie_studieID';
			if(is_array($value) || $value instanceof StudieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getStudieID();
				$value = $new_value;
			}
			else $value = $value->getStudieID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'VakStudie.'.$v;
			}
		} else {
			$field = 'VakStudie.'.$field;
		}

		return array($field, $value, $type);
	}

}
