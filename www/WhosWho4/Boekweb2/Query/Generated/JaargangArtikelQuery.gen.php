<?
abstract class JaargangArtikelQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de JaargangArtikelQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een JaargangArtikelQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw JaargangArtikelQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return JaargangArtikelQuery
	 * Het JaargangArtikelQuery-object waarvan meteen de db-table van het
	 * JaargangArtikel-object als table geset is.
	 */
	static public function table()
	{
		$query = new JaargangArtikelQuery();

		$query->tables('JaargangArtikel');

		return $query;
	}

	/**
	 * @brief Maakt een JaargangArtikelVerzameling aan van de objecten die geselecteerd
	 * worden door de JaargangArtikelQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return JaargangArtikelVerzameling
	 * Een JaargangArtikelVerzameling verkregen door de query
	 */
	public function verzamel($object = 'JaargangArtikel', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een JaargangArtikel-iterator aan van de objecten die geselecteerd
	 * worden door de JaargangArtikelQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een JaargangArtikelVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'JaargangArtikel', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een JaargangArtikel die geslecteeerd wordt door de
	 * JaargangArtikelQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return JaargangArtikelVerzameling
	 * Een JaargangArtikelVerzameling verkregen door de query.
	 */
	public function geef($object = 'JaargangArtikel')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van JaargangArtikel.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'jaargang_jaargangID',
			'artikel_artikelID',
			'schattingNodig',
			'opmerking',
			'verplicht',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'JaargangArtikel';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als JaargangArtikel een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan JaargangArtikel een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('JaargangArtikel'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Jaargang' => array(
				'jaargang_jaargangID' => 'jaargangID'
			),
			'Artikel' => array(
				'artikel_artikelID' => 'artikelID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van JaargangArtikel.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'JaargangArtikel.jaargang_jaargangID',
			'JaargangArtikel.artikel_artikelID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'jaargang':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Jaargang; })))
					user_error('Alle waarden in de array moeten van type Jaargang zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Jaargang) && !($value instanceof JaargangVerzameling))
				user_error('Value moet van type Jaargang zijn', E_USER_ERROR);
			$field = 'jaargang_jaargangID';
			if(is_array($value) || $value instanceof JaargangVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getJaargangID();
				$value = $new_value;
			}
			else $value = $value->getJaargangID();
			$type = 'int';
			break;
		case 'artikel':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Artikel; })))
					user_error('Alle waarden in de array moeten van type Artikel zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Artikel) && !($value instanceof ArtikelVerzameling))
				user_error('Value moet van type Artikel zijn', E_USER_ERROR);
			$field = 'artikel_artikelID';
			if(is_array($value) || $value instanceof ArtikelVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getArtikelID();
				$value = $new_value;
			}
			else $value = $value->getArtikelID();
			$type = 'int';
			break;
		case 'schattingnodig':
			$type = 'int';
			$field = 'schattingNodig';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'opmerking';
			break;
		case 'verplicht':
			$type = 'int';
			$field = 'verplicht';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'JaargangArtikel.'.$v;
			}
		} else {
			$field = 'JaargangArtikel.'.$field;
		}

		return array($field, $value, $type);
	}

}
