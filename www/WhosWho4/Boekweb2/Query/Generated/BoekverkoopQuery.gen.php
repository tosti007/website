<?
abstract class BoekverkoopQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de BoekverkoopQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een BoekverkoopQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw BoekverkoopQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return BoekverkoopQuery
	 * Het BoekverkoopQuery-object waarvan meteen de db-table van het
	 * Boekverkoop-object als table geset is.
	 */
	static public function table()
	{
		$query = new BoekverkoopQuery();

		$query->tables('Boekverkoop');

		return $query;
	}

	/**
	 * @brief Maakt een BoekverkoopVerzameling aan van de objecten die geselecteerd
	 * worden door de BoekverkoopQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return BoekverkoopVerzameling
	 * Een BoekverkoopVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Boekverkoop', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Boekverkoop-iterator aan van de objecten die geselecteerd
	 * worden door de BoekverkoopQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een BoekverkoopVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Boekverkoop', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Boekverkoop die geslecteeerd wordt door de BoekverkoopQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return BoekverkoopVerzameling
	 * Een BoekverkoopVerzameling verkregen door de query.
	 */
	public function geef($object = 'Boekverkoop')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Boekverkoop.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'verkoopdag',
			'volgnummer',
			'opener_contactID',
			'open',
			'sluiter_contactID',
			'pin1',
			'pin2',
			'kasverschil',
			'gesloten',
			'opmerking',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Boekverkoop';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Boekverkoop een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Boekverkoop een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Boekverkoop'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Lid' => array(
				'opener' => array(
					'opener_contactID' => 'contactID'
				),
				'sluiter' => array(
					'sluiter_contactID' => 'contactID'
				)
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Boekverkoop.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Boekverkoop.verkoopdag',
			'Boekverkoop.volgnummer'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'verkoopdag':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'verkoopdag';
			break;
		case 'volgnummer':
			$type = 'int';
			$field = 'volgnummer';
			break;
		case 'opener':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling))
				user_error('Value moet van type Lid zijn', E_USER_ERROR);
			$field = 'opener_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'open':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'open';
			break;
		case 'sluiter':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling) && !is_null($value))
				user_error('Value moet van type Lid of NULL zijn', E_USER_ERROR);
			$field = 'sluiter_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'pin1':
			$type = 'float';
			$field = 'pin1';
			break;
		case 'pin2':
			$type = 'float';
			$field = 'pin2';
			break;
		case 'kasverschil':
			$type = 'float';
			$field = 'kasverschil';
			break;
		case 'gesloten':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'gesloten';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'opmerking';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Boekverkoop.'.$v;
			}
		} else {
			$field = 'Boekverkoop.'.$field;
		}

		return array($field, $value, $type);
	}

}
