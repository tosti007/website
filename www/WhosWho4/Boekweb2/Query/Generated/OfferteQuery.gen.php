<?
abstract class OfferteQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de OfferteQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een OfferteQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw OfferteQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return OfferteQuery
	 * Het OfferteQuery-object waarvan meteen de db-table van het Offerte-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new OfferteQuery();

		$query->tables('Offerte');

		return $query;
	}

	/**
	 * @brief Maakt een OfferteVerzameling aan van de objecten die geselecteerd worden
	 * door de OfferteQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return OfferteVerzameling
	 * Een OfferteVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Offerte', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Offerte-iterator aan van de objecten die geselecteerd worden
	 * door de OfferteQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een OfferteVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Offerte', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Offerte die geslecteeerd wordt door de OfferteQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return OfferteVerzameling
	 * Een OfferteVerzameling verkregen door de query.
	 */
	public function geef($object = 'Offerte')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Offerte.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'offerteID',
			'artikel_artikelID',
			'leverancier_contactID',
			'waardePerStuk',
			'vanafDatum',
			'vervalDatum',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Offerte';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Offerte een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Offerte een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Offerte'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Artikel' => array(
				'artikel_artikelID' => 'artikelID'
			),
			'Leverancier' => array(
				'leverancier_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Offerte.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Offerte.offerteID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'offerteid':
			$type = 'int';
			$field = 'offerteID';
			break;
		case 'artikel':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Artikel; })))
					user_error('Alle waarden in de array moeten van type Artikel zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Artikel) && !($value instanceof ArtikelVerzameling))
				user_error('Value moet van type Artikel zijn', E_USER_ERROR);
			$field = 'artikel_artikelID';
			if(is_array($value) || $value instanceof ArtikelVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getArtikelID();
				$value = $new_value;
			}
			else $value = $value->getArtikelID();
			$type = 'int';
			break;
		case 'leverancier':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Leverancier; })))
					user_error('Alle waarden in de array moeten van type Leverancier zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Leverancier) && !($value instanceof LeverancierVerzameling))
				user_error('Value moet van type Leverancier zijn', E_USER_ERROR);
			$field = 'leverancier_contactID';
			if(is_array($value) || $value instanceof LeverancierVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'waardeperstuk':
			$type = 'float';
			$field = 'waardePerStuk';
			break;
		case 'vanafdatum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'vanafDatum';
			break;
		case 'vervaldatum':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'vervalDatum';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Offerte.'.$v;
			}
		} else {
			$field = 'Offerte.'.$field;
		}

		return array($field, $value, $type);
	}

}
