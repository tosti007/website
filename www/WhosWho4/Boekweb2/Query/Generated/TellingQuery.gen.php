<?
abstract class TellingQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de TellingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een TellingQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw TellingQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return TellingQuery
	 * Het TellingQuery-object waarvan meteen de db-table van het Telling-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new TellingQuery();

		$query->tables('Telling');

		return $query;
	}

	/**
	 * @brief Maakt een TellingVerzameling aan van de objecten die geselecteerd worden
	 * door de TellingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return TellingVerzameling
	 * Een TellingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Telling', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Telling-iterator aan van de objecten die geselecteerd worden
	 * door de TellingQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een TellingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Telling', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Telling die geslecteeerd wordt door de TellingQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return TellingVerzameling
	 * Een TellingVerzameling verkregen door de query.
	 */
	public function geef($object = 'Telling')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Telling.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'tellingID',
			'wanneer',
			'teller_contactID',
			'Opmerking',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Telling';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Telling een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Telling een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Telling'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Lid' => array(
				'teller_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Telling.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Telling.tellingID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'tellingid':
			$type = 'int';
			$field = 'tellingID';
			break;
		case 'wanneer':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'wanneer';
			break;
		case 'teller':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Lid; })))
					user_error('Alle waarden in de array moeten van type Lid zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Lid) && !($value instanceof LidVerzameling))
				user_error('Value moet van type Lid zijn', E_USER_ERROR);
			$field = 'teller_contactID';
			if(is_array($value) || $value instanceof LidVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'opmerking':
			$type = 'string';
			$field = 'Opmerking';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Telling.'.$v;
			}
		} else {
			$field = 'Telling.'.$field;
		}

		return array($field, $value, $type);
	}

}
