<?
abstract class TransactieQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de TransactieQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een TransactieQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw TransactieQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return TransactieQuery
	 * Het TransactieQuery-object waarvan meteen de db-table van het Transactie-object
	 * als table geset is.
	 */
	static public function table()
	{
		$query = new TransactieQuery();

		$query->tables('Transactie');

		return $query;
	}

	/**
	 * @brief Maakt een TransactieVerzameling aan van de objecten die geselecteerd
	 * worden door de TransactieQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return TransactieVerzameling
	 * Een TransactieVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Transactie', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Transactie-iterator aan van de objecten die geselecteerd worden
	 * door de TransactieQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een TransactieVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Transactie', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Transactie die geslecteeerd wordt door de TransactieQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return TransactieVerzameling
	 * Een TransactieVerzameling verkregen door de query.
	 */
	public function geef($object = 'Transactie')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Transactie.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'overerving',
			'transactieID',
			'rubriek',
			'soort',
			'contact_contactID',
			'bedrag',
			'uitleg',
			'wanneer',
			'status',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Transactie';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Transactie een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Transactie een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Transactie'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contact' => array(
				'contact_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Transactie.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Transactie.transactieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'transactieid':
			$type = 'int';
			$field = 'transactieID';
			break;
		case 'rubriek':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Transactie::enumsRubriek()))
						user_error($value . ' is niet een geldige waarde voor rubriek', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Transactie::enumsRubriek()))
				user_error($value . ' is niet een geldige waarde voor rubriek', E_USER_ERROR);
			$type = 'string';
			$field = 'rubriek';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Transactie::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Transactie::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'contact':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Contact; })))
					user_error('Alle waarden in de array moeten van type Contact zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Contact) && !($value instanceof ContactVerzameling) && !is_null($value))
				user_error('Value moet van type Contact of NULL zijn', E_USER_ERROR);
			$field = 'contact_contactID';
			if(is_array($value) || $value instanceof ContactVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'bedrag':
			$type = 'float';
			$field = 'bedrag';
			break;
		case 'uitleg':
			$type = 'string';
			$field = 'uitleg';
			break;
		case 'wanneer':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'wanneer';
			break;
		case 'status':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Transactie::enumsStatus()))
						user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Transactie::enumsStatus()))
				user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
			$type = 'string';
			$field = 'status';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Transactie.'.$v;
			}
		} else {
			$field = 'Transactie.'.$field;
		}

		return array($field, $value, $type);
	}

}
