<?
abstract class VoorraadQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de VoorraadQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een VoorraadQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw VoorraadQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VoorraadQuery
	 * Het VoorraadQuery-object waarvan meteen de db-table van het Voorraad-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new VoorraadQuery();

		$query->tables('Voorraad');

		return $query;
	}

	/**
	 * @brief Maakt een VoorraadVerzameling aan van de objecten die geselecteerd worden
	 * door de VoorraadQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Voorraad', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Voorraad-iterator aan van de objecten die geselecteerd worden
	 * door de VoorraadQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VoorraadVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Voorraad', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Voorraad die geslecteeerd wordt door de VoorraadQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VoorraadVerzameling
	 * Een VoorraadVerzameling verkregen door de query.
	 */
	public function geef($object = 'Voorraad')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Voorraad.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'voorraadID',
			'artikel_artikelID',
			'locatie',
			'waardePerStuk',
			'btw',
			'omschrijving',
			'verkoopbaar',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Voorraad';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Voorraad een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Voorraad een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Voorraad'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Artikel' => array(
				'artikel_artikelID' => 'artikelID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Voorraad.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Voorraad.voorraadID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'voorraadid':
			$type = 'int';
			$field = 'voorraadID';
			break;
		case 'artikel':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Artikel; })))
					user_error('Alle waarden in de array moeten van type Artikel zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Artikel) && !($value instanceof ArtikelVerzameling))
				user_error('Value moet van type Artikel zijn', E_USER_ERROR);
			$field = 'artikel_artikelID';
			if(is_array($value) || $value instanceof ArtikelVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getArtikelID();
				$value = $new_value;
			}
			else $value = $value->getArtikelID();
			$type = 'int';
			break;
		case 'locatie':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Voorraad::enumsLocatie()))
						user_error($value . ' is niet een geldige waarde voor locatie', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Voorraad::enumsLocatie()))
				user_error($value . ' is niet een geldige waarde voor locatie', E_USER_ERROR);
			$type = 'string';
			$field = 'locatie';
			break;
		case 'waardeperstuk':
			$type = 'float';
			$field = 'waardePerStuk';
			break;
		case 'btw':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Voorraad::enumsBtw()))
						user_error($value . ' is niet een geldige waarde voor btw', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Voorraad::enumsBtw()))
				user_error($value . ' is niet een geldige waarde voor btw', E_USER_ERROR);
			$type = 'string';
			$field = 'btw';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'verkoopbaar':
			$type = 'int';
			$field = 'verkoopbaar';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Voorraad.'.$v;
			}
		} else {
			$field = 'Voorraad.'.$field;
		}

		return array($field, $value, $type);
	}

}
