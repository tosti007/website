<?
abstract class BoekQuery_Generated
	extends ArtikelQuery
{
	/**
	 * @brief De constructor van de BoekQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ArtikelQuery
	}
	/**
	 * @brief Maakt een BoekQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw BoekQuery-object aan om er zeker van te zijn dat
	 * we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return BoekQuery
	 * Het BoekQuery-object waarvan meteen de db-table van het Boek-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new BoekQuery();

		$query->tables('Boek');

		return $query;
	}

	/**
	 * @brief Maakt een BoekVerzameling aan van de objecten die geselecteerd worden
	 * door de BoekQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return BoekVerzameling
	 * Een BoekVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Boek', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Boek-iterator aan van de objecten die geselecteerd worden door
	 * de BoekQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een BoekVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Boek', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Boek die geslecteeerd wordt door de BoekQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return BoekVerzameling
	 * Een BoekVerzameling verkregen door de query.
	 */
	public function geef($object = 'Boek')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Boek.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'artikelID',
			'isbn',
			'auteur',
			'druk',
			'uitgever'
		);

		if(in_array($field, $varArray))
			return 'Boek';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Boek een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Artikel' => array(
				'Artikel.artikelID' => 'Boek.artikelID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Boek een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Boek'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Artikel' => array(
				'Artikel.artikelID' => 'Boek.artikelID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Boek.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Boek.artikelID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'isbn':
			$type = 'string';
			$field = 'isbn';
			break;
		case 'auteur':
			$type = 'string';
			$field = 'auteur';
			break;
		case 'druk':
			$type = 'string';
			$field = 'druk';
			break;
		case 'uitgever':
			$type = 'string';
			$field = 'uitgever';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Boek.'.$v;
			}
		} else {
			$field = 'Boek.'.$field;
		}

		return array($field, $value, $type);
	}

}
