<?
abstract class iDealQuery_Generated
	extends GiroQuery
{
	/**
	 * @brief De constructor van de iDealQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // GiroQuery
	}
	/**
	 * @brief Maakt een iDealQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw iDealQuery-object aan om er zeker van te zijn
	 * dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return iDealQuery
	 * Het iDealQuery-object waarvan meteen de db-table van het iDeal-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new iDealQuery();

		$query->tables('iDeal');

		return $query;
	}

	/**
	 * @brief Maakt een iDealVerzameling aan van de objecten die geselecteerd worden
	 * door de iDealQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return iDealVerzameling
	 * Een iDealVerzameling verkregen door de query
	 */
	public function verzamel($object = 'iDeal', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een iDeal-iterator aan van de objecten die geselecteerd worden door
	 * de iDealQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een iDealVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'iDeal', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een iDeal die geslecteeerd wordt door de iDealQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return iDealVerzameling
	 * Een iDealVerzameling verkregen door de query.
	 */
	public function geef($object = 'iDeal')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van iDeal.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'transactieID',
			'titel',
			'voorraad_voorraadID',
			'externeID',
			'klantNaam',
			'klantStad',
			'expire',
			'entranceCode',
			'magOpvragenTijdstip',
			'emailadres',
			'returnURL'
		);

		if(in_array($field, $varArray))
			return 'iDeal';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als iDeal een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Giro' => array(
				'Giro.transactieID' => 'iDeal.transactieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan iDeal een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('iDeal'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Voorraad' => array(
				'voorraad_voorraadID' => 'voorraadID'
			),
			'Contact' => array(
				'contact_contactID' => 'contactID'
			),
			'Giro' => array(
				'Giro.transactieID' => 'iDeal.transactieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van iDeal.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'iDeal.transactieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'titel':
			$type = 'string';
			$field = 'titel';
			break;
		case 'voorraad':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Voorraad; })))
					user_error('Alle waarden in de array moeten van type Voorraad zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Voorraad) && !($value instanceof VoorraadVerzameling))
				user_error('Value moet van type Voorraad zijn', E_USER_ERROR);
			$field = 'voorraad_voorraadID';
			if(is_array($value) || $value instanceof VoorraadVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getVoorraadID();
				$value = $new_value;
			}
			else $value = $value->getVoorraadID();
			$type = 'int';
			break;
		case 'externeid':
			$type = 'string';
			$field = 'externeID';
			break;
		case 'klantnaam':
			$type = 'string';
			$field = 'klantNaam';
			break;
		case 'klantstad':
			$type = 'string';
			$field = 'klantStad';
			break;
		case 'expire':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'expire';
			break;
		case 'entrancecode':
			$type = 'string';
			$field = 'entranceCode';
			break;
		case 'magopvragentijdstip':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'magOpvragenTijdstip';
			break;
		case 'emailadres':
			$type = 'string';
			$field = 'emailadres';
			break;
		case 'returnurl':
			$type = 'string';
			$field = 'returnURL';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'iDeal.'.$v;
			}
		} else {
			$field = 'iDeal.'.$field;
		}

		return array($field, $value, $type);
	}

}
