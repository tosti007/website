<?
abstract class PinQuery_Generated
	extends TransactieQuery
{
	/**
	 * @brief De constructor van de PinQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // TransactieQuery
	}
	/**
	 * @brief Maakt een PinQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw PinQuery-object aan om er zeker van te zijn dat
	 * we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return PinQuery
	 * Het PinQuery-object waarvan meteen de db-table van het Pin-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new PinQuery();

		$query->tables('Pin');

		return $query;
	}

	/**
	 * @brief Maakt een PinVerzameling aan van de objecten die geselecteerd worden door
	 * de PinQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return PinVerzameling
	 * Een PinVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Pin', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Pin-iterator aan van de objecten die geselecteerd worden door
	 * de PinQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een PinVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Pin', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Pin die geslecteeerd wordt door de PinQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return PinVerzameling
	 * Een PinVerzameling verkregen door de query.
	 */
	public function geef($object = 'Pin')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Pin.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'transactieID',
			'rekening',
			'apparaat'
		);

		if(in_array($field, $varArray))
			return 'Pin';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Pin een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Transactie' => array(
				'Transactie.transactieID' => 'Pin.transactieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Pin een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Pin'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contact' => array(
				'contact_contactID' => 'contactID'
			),
			'Transactie' => array(
				'Transactie.transactieID' => 'Pin.transactieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Pin.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Pin.transactieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'rekening':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Pin::enumsRekening()))
						user_error($value . ' is niet een geldige waarde voor rekening', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Pin::enumsRekening()))
				user_error($value . ' is niet een geldige waarde voor rekening', E_USER_ERROR);
			$type = 'string';
			$field = 'rekening';
			break;
		case 'apparaat':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Pin::enumsApparaat()))
						user_error($value . ' is niet een geldige waarde voor apparaat', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Pin::enumsApparaat()))
				user_error($value . ' is niet een geldige waarde voor apparaat', E_USER_ERROR);
			$type = 'string';
			$field = 'apparaat';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Pin.'.$v;
			}
		} else {
			$field = 'Pin.'.$field;
		}

		return array($field, $value, $type);
	}

}
