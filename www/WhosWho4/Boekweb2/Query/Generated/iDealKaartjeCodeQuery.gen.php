<?
abstract class iDealKaartjeCodeQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de iDealKaartjeCodeQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een iDealKaartjeCodeQuery wat meteen gebruikt kan worden om
	 * methoden op toe te passen. We maken hier een nieuw iDealKaartjeCodeQuery-object
	 * aan om er zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return iDealKaartjeCodeQuery
	 * Het iDealKaartjeCodeQuery-object waarvan meteen de db-table van het
	 * iDealKaartjeCode-object als table geset is.
	 */
	static public function table()
	{
		$query = new iDealKaartjeCodeQuery();

		$query->tables('iDealKaartjeCode');

		return $query;
	}

	/**
	 * @brief Maakt een iDealKaartjeCodeVerzameling aan van de objecten die
	 * geselecteerd worden door de iDealKaartjeCodeQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return iDealKaartjeCodeVerzameling
	 * Een iDealKaartjeCodeVerzameling verkregen door de query
	 */
	public function verzamel($object = 'iDealKaartjeCode', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een iDealKaartjeCode-iterator aan van de objecten die geselecteerd
	 * worden door de iDealKaartjeCodeQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een iDealKaartjeCodeVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'iDealKaartjeCode', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een iDealKaartjeCode die geslecteeerd wordt door de
	 * iDealKaartjeCodeQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return iDealKaartjeCodeVerzameling
	 * Een iDealKaartjeCodeVerzameling verkregen door de query.
	 */
	public function geef($object = 'iDealKaartjeCode')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van iDealKaartjeCode.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'id',
			'kaartje_artikelID',
			'code',
			'transactie_transactieID',
			'gescand',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'iDealKaartjeCode';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als iDealKaartjeCode een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan iDealKaartjeCode een
	 * extensie is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden
	 * als het in meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('iDealKaartjeCode'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'iDealKaartje' => array(
				'kaartje_artikelID' => 'artikelID'
			),
			'Transactie' => array(
				'transactie_transactieID' => 'transactieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van iDealKaartjeCode.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'iDealKaartjeCode.id'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'id':
			$type = 'int';
			$field = 'id';
			break;
		case 'kaartje':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof iDealKaartje; })))
					user_error('Alle waarden in de array moeten van type iDealKaartje zijn', E_USER_ERROR);
			}
			else if(!($value instanceof iDealKaartje) && !($value instanceof iDealKaartjeVerzameling))
				user_error('Value moet van type iDealKaartje zijn', E_USER_ERROR);
			$field = 'kaartje_artikelID';
			if(is_array($value) || $value instanceof iDealKaartjeVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getArtikelID();
				$value = $new_value;
			}
			else $value = $value->getArtikelID();
			$type = 'int';
			break;
		case 'code':
			$type = 'string';
			$field = 'code';
			break;
		case 'transactie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Transactie; })))
					user_error('Alle waarden in de array moeten van type Transactie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Transactie) && !($value instanceof TransactieVerzameling) && !is_null($value))
				user_error('Value moet van type Transactie of NULL zijn', E_USER_ERROR);
			$field = 'transactie_transactieID';
			if(is_array($value) || $value instanceof TransactieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getTransactieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getTransactieID();
			$type = 'int';
			break;
		case 'gescand':
			$type = 'int';
			$field = 'gescand';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'iDealKaartjeCode.'.$v;
			}
		} else {
			$field = 'iDealKaartjeCode.'.$field;
		}

		return array($field, $value, $type);
	}

}
