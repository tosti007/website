<?
abstract class LeveringQuery_Generated
	extends VoorraadMutatieQuery
{
	/**
	 * @brief De constructor van de LeveringQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieQuery
	}
	/**
	 * @brief Maakt een LeveringQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw LeveringQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return LeveringQuery
	 * Het LeveringQuery-object waarvan meteen de db-table van het Levering-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new LeveringQuery();

		$query->tables('Levering');

		return $query;
	}

	/**
	 * @brief Maakt een LeveringVerzameling aan van de objecten die geselecteerd worden
	 * door de LeveringQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return LeveringVerzameling
	 * Een LeveringVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Levering', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Levering-iterator aan van de objecten die geselecteerd worden
	 * door de LeveringQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een LeveringVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Levering', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Levering die geslecteeerd wordt door de LeveringQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return LeveringVerzameling
	 * Een LeveringVerzameling verkregen door de query.
	 */
	public function geef($object = 'Levering')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Levering.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'voorraadMutatieID',
			'order_orderID',
			'leverancier_contactID'
		);

		if(in_array($field, $varArray))
			return 'Levering';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Levering een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('VoorraadMutatie' => array(
				'VoorraadMutatie.voorraadMutatieID' => 'Levering.voorraadMutatieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Levering een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Levering'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Order' => array(
				'order_orderID' => 'orderID'
			),
			'Leverancier' => array(
				'leverancier_contactID' => 'contactID'
			),
			'Voorraad' => array(
				'voorraad_voorraadID' => 'voorraadID'
			),
			'VoorraadMutatie' => array(
				'VoorraadMutatie.voorraadMutatieID' => 'Levering.voorraadMutatieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Levering.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Levering.voorraadMutatieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'order':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Order; })))
					user_error('Alle waarden in de array moeten van type Order zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Order) && !($value instanceof OrderVerzameling) && !is_null($value))
				user_error('Value moet van type Order of NULL zijn', E_USER_ERROR);
			$field = 'order_orderID';
			if(is_array($value) || $value instanceof OrderVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getOrderID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getOrderID();
			$type = 'int';
			break;
		case 'leverancier':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Leverancier; })))
					user_error('Alle waarden in de array moeten van type Leverancier zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Leverancier) && !($value instanceof LeverancierVerzameling))
				user_error('Value moet van type Leverancier zijn', E_USER_ERROR);
			$field = 'leverancier_contactID';
			if(is_array($value) || $value instanceof LeverancierVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Levering.'.$v;
			}
		} else {
			$field = 'Levering.'.$field;
		}

		return array($field, $value, $type);
	}

}
