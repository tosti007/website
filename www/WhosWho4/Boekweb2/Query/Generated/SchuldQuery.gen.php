<?
abstract class SchuldQuery_Generated
	extends TransactieQuery
{
	/**
	 * @brief De constructor van de SchuldQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // TransactieQuery
	}
	/**
	 * @brief Maakt een SchuldQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw SchuldQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return SchuldQuery
	 * Het SchuldQuery-object waarvan meteen de db-table van het Schuld-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new SchuldQuery();

		$query->tables('Schuld');

		return $query;
	}

	/**
	 * @brief Maakt een SchuldVerzameling aan van de objecten die geselecteerd worden
	 * door de SchuldQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return SchuldVerzameling
	 * Een SchuldVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Schuld', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Schuld-iterator aan van de objecten die geselecteerd worden
	 * door de SchuldQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een SchuldVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Schuld', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Schuld die geslecteeerd wordt door de SchuldQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return SchuldVerzameling
	 * Een SchuldVerzameling verkregen door de query.
	 */
	public function geef($object = 'Schuld')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Schuld.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'transactieID',
			'afgeschreven',
			'transactie_transactieID'
		);

		if(in_array($field, $varArray))
			return 'Schuld';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Schuld een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Transactie' => array(
				'Transactie.transactieID' => 'Schuld.transactieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Schuld een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Schuld'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Transactie' => array(
				'Transactie.transactieID' => 'Schuld.transactieID'
			),
			'Contact' => array(
				'contact_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Schuld.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Schuld.transactieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'afgeschreven':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'afgeschreven';
			break;
		case 'transactie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Transactie; })))
					user_error('Alle waarden in de array moeten van type Transactie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Transactie) && !($value instanceof TransactieVerzameling) && !is_null($value))
				user_error('Value moet van type Transactie of NULL zijn', E_USER_ERROR);
			$field = 'transactie_transactieID';
			if(is_array($value) || $value instanceof TransactieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getTransactieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getTransactieID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Schuld.'.$v;
			}
		} else {
			$field = 'Schuld.'.$field;
		}

		return array($field, $value, $type);
	}

}
