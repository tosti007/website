<?
abstract class VerplaatsingQuery_Generated
	extends VoorraadMutatieQuery
{
	/**
	 * @brief De constructor van de VerplaatsingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // VoorraadMutatieQuery
	}
	/**
	 * @brief Maakt een VerplaatsingQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw VerplaatsingQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return VerplaatsingQuery
	 * Het VerplaatsingQuery-object waarvan meteen de db-table van het
	 * Verplaatsing-object als table geset is.
	 */
	static public function table()
	{
		$query = new VerplaatsingQuery();

		$query->tables('Verplaatsing');

		return $query;
	}

	/**
	 * @brief Maakt een VerplaatsingVerzameling aan van de objecten die geselecteerd
	 * worden door de VerplaatsingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return VerplaatsingVerzameling
	 * Een VerplaatsingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Verplaatsing', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Verplaatsing-iterator aan van de objecten die geselecteerd
	 * worden door de VerplaatsingQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een VerplaatsingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Verplaatsing', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Verplaatsing die geslecteeerd wordt door de VerplaatsingQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return VerplaatsingVerzameling
	 * Een VerplaatsingVerzameling verkregen door de query.
	 */
	public function geef($object = 'Verplaatsing')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Verplaatsing.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'voorraadMutatieID',
			'tegenVoorraad_voorraadID'
		);

		if(in_array($field, $varArray))
			return 'Verplaatsing';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Verplaatsing een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('VoorraadMutatie' => array(
				'VoorraadMutatie.voorraadMutatieID' => 'Verplaatsing.voorraadMutatieID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Verplaatsing een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Verplaatsing'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Voorraad' => array(
				'tegenVoorraad' => array(
					'tegenVoorraad_voorraadID' => 'voorraadID'
				),
				'voorraad' => array(
					'voorraad_voorraadID' => 'voorraadID'
				)
			),
			'VoorraadMutatie' => array(
				'VoorraadMutatie.voorraadMutatieID' => 'Verplaatsing.voorraadMutatieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Verplaatsing.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Verplaatsing.voorraadMutatieID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'tegenvoorraad':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Voorraad; })))
					user_error('Alle waarden in de array moeten van type Voorraad zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Voorraad) && !($value instanceof VoorraadVerzameling))
				user_error('Value moet van type Voorraad zijn', E_USER_ERROR);
			$field = 'tegenVoorraad_voorraadID';
			if(is_array($value) || $value instanceof VoorraadVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getVoorraadID();
				$value = $new_value;
			}
			else $value = $value->getVoorraadID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Verplaatsing.'.$v;
			}
		} else {
			$field = 'Verplaatsing.'.$field;
		}

		return array($field, $value, $type);
	}

}
