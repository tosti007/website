<?
abstract class LeverancierQuery_Generated
	extends ContactQuery
{
	/**
	 * @brief De constructor van de LeverancierQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ContactQuery
	}
	/**
	 * @brief Maakt een LeverancierQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw LeverancierQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return LeverancierQuery
	 * Het LeverancierQuery-object waarvan meteen de db-table van het
	 * Leverancier-object als table geset is.
	 */
	static public function table()
	{
		$query = new LeverancierQuery();

		$query->tables('Leverancier');

		return $query;
	}

	/**
	 * @brief Maakt een LeverancierVerzameling aan van de objecten die geselecteerd
	 * worden door de LeverancierQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return LeverancierVerzameling
	 * Een LeverancierVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Leverancier', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Leverancier-iterator aan van de objecten die geselecteerd
	 * worden door de LeverancierQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een LeverancierVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Leverancier', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Leverancier die geslecteeerd wordt door de LeverancierQuery uit
	 * te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return LeverancierVerzameling
	 * Een LeverancierVerzameling verkregen door de query.
	 */
	public function geef($object = 'Leverancier')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Leverancier.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'contactID',
			'naam',
			'dictaten',
			'dictatenIntern',
			'dibsproducten',
			'colaproducten'
		);

		if(in_array($field, $varArray))
			return 'Leverancier';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Leverancier een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Contact' => array(
				'Contact.contactID' => 'Leverancier.contactID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Leverancier een extensie is.
	 * Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Leverancier'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Contact' => array(
				'Contact.contactID' => 'Leverancier.contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Leverancier.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Leverancier.contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'naam':
			$type = 'string';
			$field = 'naam';
			break;
		case 'dictaten':
			$type = 'int';
			$field = 'dictaten';
			break;
		case 'dictatenintern':
			$type = 'int';
			$field = 'dictatenIntern';
			break;
		case 'dibsproducten':
			$type = 'int';
			$field = 'dibsproducten';
			break;
		case 'colaproducten':
			$type = 'int';
			$field = 'colaproducten';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Leverancier.'.$v;
			}
		} else {
			$field = 'Leverancier.'.$field;
		}

		return array($field, $value, $type);
	}

}
