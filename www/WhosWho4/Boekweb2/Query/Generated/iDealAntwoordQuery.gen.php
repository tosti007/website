<?
abstract class iDealAntwoordQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de iDealAntwoordQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een iDealAntwoordQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw iDealAntwoordQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return iDealAntwoordQuery
	 * Het iDealAntwoordQuery-object waarvan meteen de db-table van het
	 * iDealAntwoord-object als table geset is.
	 */
	static public function table()
	{
		$query = new iDealAntwoordQuery();

		$query->tables('iDealAntwoord');

		return $query;
	}

	/**
	 * @brief Maakt een iDealAntwoordVerzameling aan van de objecten die geselecteerd
	 * worden door de iDealAntwoordQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return iDealAntwoordVerzameling
	 * Een iDealAntwoordVerzameling verkregen door de query
	 */
	public function verzamel($object = 'iDealAntwoord', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een iDealAntwoord-iterator aan van de objecten die geselecteerd
	 * worden door de iDealAntwoordQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een iDealAntwoordVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'iDealAntwoord', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een iDealAntwoord die geslecteeerd wordt door de iDealAntwoordQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return iDealAntwoordVerzameling
	 * Een iDealAntwoordVerzameling verkregen door de query.
	 */
	public function geef($object = 'iDealAntwoord')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van iDealAntwoord.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'ideal_transactieID',
			'vraag_activiteit_activiteitID',
			'vraag_vraagID',
			'vraag_activiteit_activiteitID',
			'antwoord',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'iDealAntwoord';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als iDealAntwoord een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan iDealAntwoord een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('iDealAntwoord'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'iDeal' => array(
				'ideal_transactieID' => 'transactieID'
			),
			'ActiviteitVraag' => array(
				'vraag_activiteit_activiteitID' => 'activiteit_activiteitID',
				'vraag_vraagID' => 'vraagID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van iDealAntwoord.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'iDealAntwoord.ideal_transactieID',
			'iDealAntwoord.vraag_activiteit_activiteitID',
			'iDealAntwoord.vraag_vraagID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'ideal':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof iDeal; })))
					user_error('Alle waarden in de array moeten van type iDeal zijn', E_USER_ERROR);
			}
			else if(!($value instanceof iDeal) && !($value instanceof iDealVerzameling))
				user_error('Value moet van type iDeal zijn', E_USER_ERROR);
			$field = 'ideal_transactieID';
			if(is_array($value) || $value instanceof iDealVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getTransactieID();
				$value = $new_value;
			}
			else $value = $value->getTransactieID();
			$type = 'int';
			break;
		case 'vraag':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof ActiviteitVraag; })))
					user_error('Alle waarden in de array moeten van type ActiviteitVraag zijn', E_USER_ERROR);
			}
			else if(!($value instanceof ActiviteitVraag) && !($value instanceof ActiviteitVraagVerzameling))
				user_error('Value moet van type ActiviteitVraag zijn', E_USER_ERROR);
			$field = array(
				'vraag_activiteit_activiteitID',
				'vraag_vraagID'
			);
			if(is_array($value) || $value instanceof ActiviteitVraagVerzameling)
			{
				foreach($value as $k => $v)
					$value[$k] = array(
				$v->getActiviteitActiviteitID(),
				$v->getVraagID()
					);
				if($value instanceof ActiviteitVraagVerzameling)
					$value = $value->getVerzameling();
			}
			else
				$value = array(
				$value->getActiviteitActiviteitID(),
				$value->getVraagID()
				);
			$type = array(
				'int',
				'int'				);
			break;
		case 'antwoord':
			$type = 'string';
			$field = 'antwoord';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'iDealAntwoord.'.$v;
			}
		} else {
			$field = 'iDealAntwoord.'.$field;
		}

		return array($field, $value, $type);
	}

}
