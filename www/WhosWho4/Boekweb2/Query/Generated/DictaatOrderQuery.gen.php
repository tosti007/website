<?
abstract class DictaatOrderQuery_Generated
	extends OrderQuery
{
	/**
	 * @brief De constructor van de DictaatOrderQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // OrderQuery
	}
	/**
	 * @brief Maakt een DictaatOrderQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw DictaatOrderQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return DictaatOrderQuery
	 * Het DictaatOrderQuery-object waarvan meteen de db-table van het
	 * DictaatOrder-object als table geset is.
	 */
	static public function table()
	{
		$query = new DictaatOrderQuery();

		$query->tables('DictaatOrder');

		return $query;
	}

	/**
	 * @brief Maakt een DictaatOrderVerzameling aan van de objecten die geselecteerd
	 * worden door de DictaatOrderQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return DictaatOrderVerzameling
	 * Een DictaatOrderVerzameling verkregen door de query
	 */
	public function verzamel($object = 'DictaatOrder', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een DictaatOrder-iterator aan van de objecten die geselecteerd
	 * worden door de DictaatOrderQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een DictaatOrderVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'DictaatOrder', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een DictaatOrder die geslecteeerd wordt door de DictaatOrderQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return DictaatOrderVerzameling
	 * Een DictaatOrderVerzameling verkregen door de query.
	 */
	public function geef($object = 'DictaatOrder')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van DictaatOrder.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'orderID',
			'kaftPrijs',
			'marge',
			'factuurverschil'
		);

		if(in_array($field, $varArray))
			return 'DictaatOrder';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als DictaatOrder een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Order' => array(
				'Order.orderID' => 'DictaatOrder.orderID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan DictaatOrder een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('DictaatOrder'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Artikel' => array(
				'artikel_artikelID' => 'artikelID'
			),
			'Leverancier' => array(
				'leverancier_contactID' => 'contactID'
			),
			'Order' => array(
				'Order.orderID' => 'DictaatOrder.orderID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van DictaatOrder.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'DictaatOrder.orderID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'kaftprijs':
			$type = 'float';
			$field = 'kaftPrijs';
			break;
		case 'marge':
			$type = 'float';
			$field = 'marge';
			break;
		case 'factuurverschil':
			$type = 'float';
			$field = 'factuurverschil';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'DictaatOrder.'.$v;
			}
		} else {
			$field = 'DictaatOrder.'.$field;
		}

		return array($field, $value, $type);
	}

}
