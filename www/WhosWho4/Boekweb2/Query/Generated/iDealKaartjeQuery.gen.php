<?
abstract class iDealKaartjeQuery_Generated
	extends ArtikelQuery
{
	/**
	 * @brief De constructor van de iDealKaartjeQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // ArtikelQuery
	}
	/**
	 * @brief Maakt een iDealKaartjeQuery wat meteen gebruikt kan worden om methoden op
	 * toe te passen. We maken hier een nieuw iDealKaartjeQuery-object aan om er zeker
	 * van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return iDealKaartjeQuery
	 * Het iDealKaartjeQuery-object waarvan meteen de db-table van het
	 * iDealKaartje-object als table geset is.
	 */
	static public function table()
	{
		$query = new iDealKaartjeQuery();

		$query->tables('iDealKaartje');

		return $query;
	}

	/**
	 * @brief Maakt een iDealKaartjeVerzameling aan van de objecten die geselecteerd
	 * worden door de iDealKaartjeQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return iDealKaartjeVerzameling
	 * Een iDealKaartjeVerzameling verkregen door de query
	 */
	public function verzamel($object = 'iDealKaartje', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een iDealKaartje-iterator aan van de objecten die geselecteerd
	 * worden door de iDealKaartjeQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een iDealKaartjeVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'iDealKaartje', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een iDealKaartje die geslecteeerd wordt door de iDealKaartjeQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return iDealKaartjeVerzameling
	 * Een iDealKaartjeVerzameling verkregen door de query.
	 */
	public function geef($object = 'iDealKaartje')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van iDealKaartje.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'artikelID',
			'activiteit_activiteitID',
			'autoInschrijven',
			'returnURL',
			'externVerkrijgbaar',
			'digitaalVerkrijgbaar',
			'magScannen'
		);

		if(in_array($field, $varArray))
			return 'iDealKaartje';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als iDealKaartje een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = array('Artikel' => array(
				'Artikel.artikelID' => 'iDealKaartje.artikelID'
			)
		);
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan iDealKaartje een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('iDealKaartje'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Activiteit' => array(
				'activiteit_activiteitID' => 'activiteitID'
			),
			'Artikel' => array(
				'Artikel.artikelID' => 'iDealKaartje.artikelID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van iDealKaartje.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'iDealKaartje.artikelID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'activiteit':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Activiteit; })))
					user_error('Alle waarden in de array moeten van type Activiteit zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Activiteit) && !($value instanceof ActiviteitVerzameling))
				user_error('Value moet van type Activiteit zijn', E_USER_ERROR);
			$field = 'activiteit_activiteitID';
			if(is_array($value) || $value instanceof ActiviteitVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getActiviteitID();
				$value = $new_value;
			}
			else $value = $value->getActiviteitID();
			$type = 'int';
			break;
		case 'autoinschrijven':
			$type = 'int';
			$field = 'autoInschrijven';
			break;
		case 'returnurl':
			$type = 'string';
			$field = 'returnURL';
			break;
		case 'externverkrijgbaar':
			$type = 'int';
			$field = 'externVerkrijgbaar';
			break;
		case 'digitaalverkrijgbaar':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, iDealKaartje::enumsDigitaalVerkrijgbaar()))
						user_error($value . ' is niet een geldige waarde voor digitaalVerkrijgbaar', E_USER_ERROR);
				}
			}
			else if(!in_array($value, iDealKaartje::enumsDigitaalVerkrijgbaar()))
				user_error($value . ' is niet een geldige waarde voor digitaalVerkrijgbaar', E_USER_ERROR);
			$type = 'string';
			$field = 'digitaalVerkrijgbaar';
			break;
		case 'magscannen':
			$type = 'int';
			$field = 'magScannen';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'iDealKaartje.'.$v;
			}
		} else {
			$field = 'iDealKaartje.'.$field;
		}

		return array($field, $value, $type);
	}

}
