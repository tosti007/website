<?

/**
 * $Id$
 */
abstract class PersoonView extends PersoonView_Generated {
	/** Enkele labels + opmerkingen voor de formulieren **/
	public static function labelVoornaam(Persoon $obj) {
		return _('Voornaam');
	}
	public static function labelBijnaam(Persoon $obj) {
		return _('Bijnaam');
	}
	public static function labelVoorletters(Persoon $obj) {
		return _('Voorletters');
	}
	public static function labelGeboortenamen(Persoon $obj) {
		return _('Geboortenamen');
	}
	public static function labelTussenvoegsels(Persoon $obj) {
		return _('Tussenvoegsels');
	}
	public static function labelAchternaam(Persoon $obj) {
		return _('Achternaam');
	}
	public static function labelTitelsPrefix(Persoon $obj) {
		return _('(Prefix) titels');
	}
	public static function labelTitelsPostfix(Persoon $obj) {
		return _('(Postfix) titels');
	}
	public static function labelGeslacht(Persoon $obj) {
		return _('Geslacht');
	}
	public static function labelenumGeslacht($value) {
		switch ($value) {
		case 'M':return _('Man');
		case 'V':return _('Vrouw');
		case '?':return _("Onbekend");
		}
		return parent::labelenumGeslacht($value);
	}
	public static function labelDatumGeboorte(Persoon $obj) {
		return _('Geboortedatum');
	}
	public static function labelOverleden(Persoon $obj) {
		return _('Overleden');
	}
	public static function labelGPGkey(Persoon $obj) {
		return _('GPG-key');
	}
	public static function labelRekeningnummer(Persoon $obj) {
		return _('Rekeningnummer');
	}
	public static function labelOpmerkingen(Persoon $obj) {
		return _('Opmerkingen');
	}
	public static function opmerkingVoornaamwoord() {
		return sprintf(_("Staat jouw voornaamwoord hier niet tussen? %s[VOC: link 'maak een nieuwe']"), new HtmlAnchor('/Service/Voornaamwoord/Nieuw', _("Maak een nieuwe!")));
	}

	public static function defaultWaardePersoon(Persoon $obj) {
		return self::makeLink($obj);
	}

	public static function naam(Contact $pers = null, $type = WSW_NAME_DEFAULT) {
		if (!$pers instanceof Persoon) {
			return;
		}

		// code is overgenomen uit: /www/space/whofuncs.php:getLidRow2Name()

		switch ($type) {
		case WSW_NAME_VOORNAAM:
			// wat als voornaam leeg is?
			$naam = $pers->getVoornaam();
			break;
		case WSW_NAME_FORMEEL:
			$naam = ($pers->getTitelsPrefix()
			. ' ' . $pers->getVoorletters()
			. ' ' . $pers->getTussenvoegsels()
			. ' ' . $pers->getAchternaam()
			. ' ' . $pers->getTitelsPostfix());
			break;
		case WSW_NAME_FORMEEL_LANG:
			// Prof. dr. Sebastiaan Johannes van Schaik MSc.
			$initia = ($pers->getGeboortenamen()
				? $pers->getGeboortenamen()
				: $pers->getVoorletters());
			$naam = ($pers->getTitelsPrefix()
			. ' ' . $initia
			. ' ' . $pers->getTussenvoegsels()
			. ' ' . $pers->getAchternaam()
			. ' ' . $pers->getTitelsPostfix());
			break;
		case WSW_NAME_INITIALEN:
			$spaces = preg_split('/\s+/'
				, ($pers->getVoornaam()
					. ' ' . $pers->getTussenvoegsels()
					. ' ' . $pers->getAchternaam())
			);
			$naam = '';
			foreach ($spaces as $joe)
			{
				$naam .= $joe[0];
			}

			break;
		case WSW_NAME_CRIMI:
			$naam = ($pers->getVoornaam()
			. ' ' . $pers->getTussenvoegsels()
			. ' ' . substr($pers->getAchternaam(), 0, 1) . '.');
			break;
		case WSW_NAME_LANG:
			$initia = ($pers->getGeboortenamen()
				? $pers->getGeboortenamen()
				: $pers->getVoorletters());
			$nick = ($pers->getBijnaam()
				? '"' . $pers->getBijnaam() . '"'
				: '');
			$vnaam = ($pers->getVoornaam() && $pers->getVoornaam() != $initia
				? '(' . $pers->getVoornaam() . ')'
				: '');
			$naam = ($pers->getTitelsPrefix()
			. ' ' . $initia
			. ' ' . $vnaam
			. ' ' . $nick
			. ' ' . $pers->getTussenvoegsels()
			. ' ' . $pers->getAchternaam()
			. ' ' . $pers->getTitelsPostfix());
			break;
		default:
			if ($type != WSW_NAME_DEFAULT) {
				user_error("Onbekend type '$type'!", E_USER_WARNING);
			}

			// geen voornaam, dan voorletters.
			$naam = $pers->getVoornaam();
			if (empty($naam)) {
				$naam = $pers->getVoorletters();
			}

			$naam .= ' ' . $pers->getTussenvoegsels()
			. ' ' . $pers->getAchternaam();
		}

		// dubbele spaties wegbokken
		return htmlspecialchars(preg_replace('/ +/', ' ', $naam));
	}

	/**
	 * Toont het formulier voor het wijzigen van een persoon; hoort bij Leden_Controller::wijzig()
	 */
	public static function wijzig($persoon, $show_error) {
		$page = Page::getInstance()
			->start(PersoonView::makeLink($persoon));

		$page->addFooterJS(Page::minifiedFile('knockout.js'))
			 ->addFooterJS(Page::minifiedFile('TelefoonNummers.js'));

		$page->add(PersoonView::wijzigForm('Wijzig', $persoon, $show_error));
		$page->end();
	}

	/**
	 * Toont het formulier voor het maken van een nieuw lid; hoort bij Leden_Controller::nieuw()
	 */
	public static function nieuw($persoon, $form, $msg, $fullForm = true) {
		$page = Page::getInstance()
			->start(_("Nieuw persoon"), 'leden');

		if ($msg !== null) {
			$page->add($msg);
		}

		if ($form) {
			if ($msg === null) {
				$page->add(PersoonView::nieuwForm('Nieuw', $persoon, false, $fullForm));
			} else {
				$page->add(PersoonView::nieuwForm('Nieuw', $persoon, true, $fullForm));
			}
		}

		$page->end();
	}

	/**
	 * Geef een formulier voor het aanmaken van een lid terug
	 */
	public static function nieuwForm($name = 'Nieuw', $obj = null, $show_error = true, $fullForm = true) {
		/*
			 * We gaan 4 divs maken:
			 * - Persoonsinformatie
			 * - Contactsinformatie
			 * - Opmerking
		*/

		/* Als er nog geen object is meegestuurd, dan maken we zelf wel iets **/
		if (is_null($obj)) {
			$obj = new Persoon();
		}

		/* Dit wordt het formulier **/
		$form = HtmlForm::named($name);
		if (!$fullForm) {
			$form->add(new HtmlDiv(_('Dit is het quickform, ga terug naar het ') . new HtmlAnchor('?quick=0', 'volledig formulier'), 'bs-callout bs-callout-warning'));
		} else {
			$form->add(new HtmlDiv(_('Dit is het volledige formulier, er is ook een ') . new HtmlAnchor('?quick=1', 'quickform') . "!", 'bs-callout bs-callout-warning'));
		}

		$form->add($div = new HtmlDiv(null, 'btn-group'));

		if ($obj->getInDB()) {
			$div->add(HtmlAnchor::button('Foto/Wijzig', _('Pasfoto aanpassen')));
		}

		/* Eerst de div voor persoonsinformatie in te vullen **/
		$form->add(PersoonView::wijzigPersoonPersooninformatieDiv($obj, $show_error, $fullForm));

		/* De div voor de contactsinformatie **/
		$form->add(PersoonView::wijzigPersoonContactinformatieDiv($obj, $show_error, $fullForm));

		/* Dan de div voor lidinformatie in te vullen **/
		if ($fullForm) {
			$form->add($div = new HtmlDiv());
			$div->add(new HtmlHeader(3, _("Opmerkingen")));
			$div->add(self::wijzigTR($obj, 'Opmerkingen', $show_error));
		}

		/* Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeSubmitButton(_("Maak de persoon aan!")));

		return $form;
	}

	/**
	 * Verwerk het nieuwe formulier voor zowel Persoon als Lid objecten
	 */
	public static function processNieuwForm($obj) {
		$view = $obj->getView();
		/* Haal alle data op **/
		if (!$data = tryPar($view::formobj($obj), NULL)) {
			return false;
		}

		/* Vul alle statische informatie in **/
		$view::processForm($obj);

		/* Verwerk PersoonVoorkeur **/
		PersoonVoorkeurView::processForm($obj->getVoorkeur());
	}

	/**
	 * Geef een formulier voor het wijzigen van een lid terug
	 */
	public static function wijzigForm($name = 'Wijzig', $obj = null, $show_error = true) {
		/* We gaan 3 divs maken:
		 * - Persoonsinformatie
		 * - Contactsinformatie + telefoonnummers + adressen
		 * - Opmerking
		 */

		// Als er nog geen object is meegestuurd, dan maken we zelf wel iets
		if (is_null($obj)) {
			$obj = new Persoon();
		}

		// Dit wordt het formulier
		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv(null, 'btn-group'));

		if ($obj->getInDB()) {
			$div->add(HtmlAnchor::button('Foto/Wijzig', _('Pasfoto aanpassen')));
		}

		$div->add(HtmlAnchor::button($obj->url() . '/WijzigAdres', _('Wijzig adressen')))
			->add(HtmlAnchor::button($obj->url() . '/WijzigStudies', _('Wijzig studies')));

		$form->add(self::wijzigPersoonPersooninformatieDiv($obj, $show_error));
		$form->add(self::wijzigPersoonContactinformatieDiv($obj, $show_error));
		$form->add(ContactTelnrVerzamelingView::wijzigTelnrsDiv($obj, $show_error));

		// Opmerking hier los in.. beetje lui zijn enzo.
		if (hasAuth('bestuur')) {
			$form->add($div = new HtmlDiv());
			$div->add(new HtmlHeader(3, _("Opmerking")));
			$div->add(self::wijzigTR($obj, 'Opmerkingen', $show_error));

			// En persoon->lid casting
			$div->add(HtmlAnchor::button('persoonToLid', _('Persoon omzetten naar lid')));
		}

		// Voeg ook een submit-knop toe
		$form->add(HtmlInput::makeSubmitButton(_("Wijzigen")));

		return $form;
	}

	/**
	 * Verwerk het wijzig formulier
	 */
	public static function processWijzigForm($obj) {
		/* Kloon het oude lid, om wijzigingen te bekijken **/
		$persOud = clone $obj;

		/* Haal alle data op **/
		if (!$data = tryPar(self::formobj($obj), null)) {
			return false;
		}

		/* Vul alle statische informatie in **/
		self::processForm($obj);

		if (count($persOud->difference($obj)) > 0 && !$obj instanceof Lid) {
			$obj->wijzigingen['Persoon'] = $persOud->difference($obj);
		}

		/* Verwerk telefoonnummer(s) **/
		ContactTelnrVerzamelingView::processWijzigTelnrs($obj, $data);

		/* Verwerk PersoonVoorkeur **/
		PersoonVoorkeurView::processForm($obj->getVoorkeur());
	}

	public static function makeLink(Persoon $pers, $caption = null, $link = null, $shop = 0) {
		$shopUrl = '';
		if ($shop == 1) {
			$shopUrl = '/Shop';
		} elseif ($shop == 2) {
			$shopUrl = '/UnShop';
		}

		if (is_null($caption)) {
			$caption = PersoonView::naam($pers);
		}

		if ($pers->magBekijken()) {
			if (is_null($link)) {
				$link = $pers->url() . $shopUrl;
			}
			$a = new HtmlAnchor($link, $caption, null, 'persoonLink');
			$fotoURL = ProfielFoto::fotoURL(ProfielFoto::zoekHuidig($pers), 'Thumbnail');
			$a->setAttribute('rel', $fotoURL);
			return $a;
		} else {
			return $caption;
		}

	}

	public static function makeAchievementsLink(Persoon $pers, $caption = null) {
		if (is_null($caption)) {
			$caption = PersoonView::naam($pers);
		}

		// if ($pers->magBekijken())
		// 	return new HtmlAnchor($pers->urlAchievements(), $caption);
		// else
		return $caption;
	}

	public static function makeIOULink(Persoon $pers) {
		$caption = PersoonView::naam($pers);
		return new HtmlAnchor($pers->urlIOU(), $caption);
	}

	public static function makeLinkWithEmail(Persoon $persoon) {
		return new HtmlSpan([
			self::makeLink($persoon), '&nbsp;(', self::makeEmailLink($persoon), ')']);
	}

	public static function evt_link($pers, $innerHTML) {
		if ($pers->magBekijken()) {
			return "<a href='/Leden/" . $pers->getContactID() . "'>" . $innerHTML . "</a>";
		} else {
			return $innerHTML;
		}
	}

	/**
	 *  Geef een div met de kop van deze persoon.
	 * Handig voor in een koppenblad (zie ook PersoonVerzameling::koppenblad())
	 * De opties zijn foto+naam of foto+lege regel, wat je bijvoorbeeld wilt voor het BMW-koppenblad van actieve leden.
	 *
	 * @param pers De persoon van wie het kop getoond moet worden.
	 * @param naamZichtbaar Zo nee, zie je onder de foto een lege regel. Zo ja, zie je onder de foto de naam van de persoon.
	 * @param printbaar Bool of het koppenblad in printbaar overzicht moet staan
	 *
	 * @returns Een HtmlDiv met een foto en mogelijk de naam van de persoon.
	 */
	public static function kop($pers, $naamZichtbaar = true, $printbaar = false) {
		$main = new HtmlDiv(null);

		// foto + naam die evt link is
		$foto = self::foto($pers, false, false, 'Thumbnail', $printbaar);
		$main->add(self::evt_link($pers, $foto));

		if ($naamZichtbaar) {
			$naam = PersoonView::naam($pers);
			$main->add(self::evt_link($pers, $naam));
		}

		return $main;
	}

	public static function alsArray($pers) {
		$result = ['voornaam' => $pers->getVoornaam()
			, 'bijnaam' => $pers->getBijnaam()
			, 'voorletters' => $pers->getVoorletters()
			, 'geboortenamen' => $pers->getGeboortenamen()
			, 'tussenvoegsels' => $pers->getTussenvoegsels()
			, 'achternaam' => $pers->getAchternaam()
			, 'titelsPrefix' => $pers->getTitelsPrefix()
			, 'titelsPostfix' => $pers->getTitelsPostfix()
			, 'geslacht' => $pers->getGeslacht()
			, 'datumGeboorte' => $pers->getDatumGeboorte()->format('d-m-Y')
			, 'overleden' => $pers->getOverleden()
			, 'GPGkey' => $pers->getGPGkey()
			, 'Rekeningnummer' => $pers->getRekeningnummer(),
		];

		return $result;
	}

	public static function alsCSVArray($pers) {
		$voornaam = PersoonView::waardeVoornaam($pers);
		if (empty($voornaam)) {
			$voornaam = PersoonView::waardeVoorletters($pers);
		}

		//bestuur ziet alles, met wat bonus lidinfo
		if (hasAuth('bestuur')) {
			$result = self::alsArray($pers);
			$result['studentnummer'] = ($pers instanceof Lid) ? $pers->getStudentNr() : '';
			$result['lidvan'] = ($pers instanceof Lid) ? LidView::waardeLidVan($pers) : '';
		} else
		//de rest krijgt alleen de naam
		{
			$result = ['voornaam' => $voornaam,
				'tussenvoegsels' => PersoonView::waardeTussenvoegsels($pers),
				'achternaam' => PersoonView::waardeAchternaam($pers),
			];
		}
		//Pak het adres of vul het anders op leegte
		$adres = ContactAdresView::adresArray($pers, 'THUIS');
		if ($adres) {
			$result = array_merge($result, $adres);
		} else {
			$result['straat1'] = '';
			$result['straat2'] = '';
			$result['huisnummer'] = '';
			$result['postcode'] = '';
			$result['woonplaats'] = '';
			$result['land'] = '';
		}
		//email en telefoonnummer komen niet uit de Persoontabel
		$result['email'] = PersoonView::waardeEmail($pers);
		$result['telefoonnummer'] = ContactTelnrView::waardeTelefoonnummer($pers->getPrefTelnr());

		return $result;
	}

	public static function alsBonusCSVArray($pers) {
		$voornaam = PersoonView::waardeVoornaam($pers);
		if (empty($voornaam)) {
			$voornaam = PersoonView::waardeVoorletters($pers);
		}

		$contactID = ($pers instanceof Lid) ? $pers->getcontactID() : '';

		$result = self::alsArray($pers);
		$result['studentnummer'] = ($pers instanceof Lid) ? $pers->getStudentNr() : '';
		$result['lidnummer'] = $contactID;
		$result['lidvan'] = ($pers instanceof Lid) ? LidView::waardeLidVan($pers) : '';
		$result['voornaam'] = $voornaam;
		$result['tussenvoegsels'] = PersoonView::waardeTussenvoegsels($pers);
		$result['achternaam'] = PersoonView::waardeAchternaam($pers);
		$result['email'] = PersoonView::waardeEmail($pers);
		$result['telefoonnummer'] = ContactTelnrView::waardeTelefoonnummer($pers->getPrefTelnr());
		$result['@foto'] = substr($contactID, -2) . "/lid$contactID.png";

		//Pak het adres of vul het anders op leegte
		$adres = ContactAdresView::adresArray($pers, 'THUIS');
		if ($adres) {
			$result = array_merge($result, $adres);
		} else {
			$result['straat1'] = '';
			$result['straat2'] = '';
			$result['huisnummer'] = '';
			$result['postcode'] = '';
			$result['woonplaats'] = '';
			$result['land'] = '';
		}

		//Zoek de studies uit en geef alleen de huidige in een mooie string terug.
		if ($pers instanceof Lid) //Personen hebben geen studieobjecten, dus niet-lid personen wil je ook niet aan LidStudieVerzameling::vanLid voeren.
		{
			$lidstudies = LidStudieVerzameling::vanLid($pers, 'huidige');
			$result['studie'] = StudieVerzamelingView::toString($lidstudies->toStudieVerzameling());
		}
		return $result;
	}

	public static function latex($pers) {
		$adres = ContactAdres::geefBySoort($pers, "THUIS");
		//Is er geen thuis adres dan pakken we het eerste andere adres dat mogelijk is
		if (!$adres) {
			$adres = $pers->getEersteAdres();
		}

		$arr = [
			PersoonView::waardeVoornaam($pers),
			PersoonView::waardeVoorletters($pers),
			PersoonView::waardeTussenvoegsels($pers),
			PersoonView::waardeAchternaam($pers),
			ContactTelnrView::waardeTelefoonnummer($pers->getPrefTelnr()),
			PersoonView::waardeEmail($pers),
			$adres ? $adres->getStraat1() . ' ' . $adres->getStraat2() . ' ' . $adres->getHuisnummer() : '',
			$adres ? $adres->getPostcode() . '  ' . $adres->getWoonplaats() . ' ' . $adres->getLand() : '',
			$adres ? $adres->kixcode() : '',
		];

		return '\\persoon{' . implode('}{', array_map('latexescape', $arr)) . "}\n";
	}

	public static function zoekenTR($pers) {
		$tablerow = new HtmlTableRow();

		foreach ($pers->velden('verzameling') as $veld) {
			$data = self::call('zoekenTD', $veld, $pers);
			if ($data) {
				$tablerow->addData($data);
			} else {
				$tablerow->addEmptyCell();
			}

		}
		return $tablerow;
	}

	protected static function zoekenTD(Persoon $pers, $veld) {
		return self::call('waarde', $veld, $pers);
	}

	/**
	 *  Geef de mededelingen over deze persoon in een array zoals missende
	 *   gegevens of jarig zijn.
	 */
	public static function mededelingen($pers) {
		global $auth;

		$now = new dateTimeLocale();

		$mededelingen = [];
		//missende gegevens, TODO: alle missende dingen verzamelen en daar een mooi geheel van maken
		if ($pers instanceof Lid && $pers->magWijzigen()) {
			if ($pers->getStudentNr() == null && $pers->getStudies()->huidige()->aantal() > 0) {
				if ($auth->getLidnr() != $pers->getContactID()) {
					$mededelingen[] = ['title' => _("Ontbrekende gegevens"),
						'mededeling' => sprintf(_("Deze persoon heeft geen studentnummer opgegeven."))];
				} else {
					$mededelingen[] = ['title' => _("Ontbrekende gegevens"),
						'mededeling' => sprintf(_("We missen belangrijke gegevens van je. Zorg dat je je studentnummer opgeeft zodat we kunnen controleren of je student bent."))];
				}
			}
		}
		//TODO: inconsistensies

		//Jarig
		if ($pers->getDatumGeboorte()->hasTime() && $pers->getDatumGeboorte()->format('d-m') == $now->format('d-m')) {
			$ingelogd = Persoon::getIngelogd();

			// Als je een jarig lid bekijkt (niet jezelf!)
			// krijg je een achievement
			if ($ingelogd && $ingelogd != $pers) {
				$persBadges = $ingelogd->getBadges();
				$persBadges->addBadge('jaloers');
			}
			if ($auth->getLidnr() != $pers->getContactID()) {
				$mededelingen[] = ['title' => _("Jarig!"),
					'mededeling' => sprintf(_("Deze persoon is vandaag %d jaar geworden!"), $pers->leeftijd())];
			} else {
				$mededelingen[] = ['title' => _("Jarig!"),
					'mededeling' => sprintf(_("Van harte gefeliciteerd met je %de verjaardag!"), $pers->leeftijd())];
			}
		}

		//Contactpersoon en niet spook
		if (ContactPersoon::vanPersoon($pers, "normaal") || ContactPersoon::vanPersoon($pers, "bedrijfscontact")) {
			$mededelingen[] = ['title' => _("Contactpersoon"), 'mededeling' => _("Deze persoon is een contactpersoon van A–Eskwadraat, bv van een organisatie of bedrijf.")];
		}

		return $mededelingen;
	}

	public static function mededelingenDiv($pers) {
		$medDiv = new HtmlDiv();
		$mededelingen = PersoonView::mededelingen($pers);

		if (count($mededelingen) > 0) {
			$medDiv->setAttribute('id', "persoonMededelingenDiv");
			foreach ($mededelingen as $mededeling) {
				$medDiv->add($divje = new HtmlDiv(null, 'bs-callout bs-callout-info'));
				$divje->add(new HtmlHeader(4, $mededeling['title']));
				$divje->add(new HtmlParagraph($mededeling['mededeling']));
			}
		}

		//Opmerkingen
		if (hasAuth("bestuur") && ($opm = $pers->getOpmerkingen())) {
			$medDiv->add(
				new HtmlDiv(
					new HtmlDiv(
						new HtmlStrong(PersoonView::waardeOpmerkingen($pers))
						, 'text-danger panel-body')
					, 'panel panel-default'
				)
			);
		}

		//Overleden?
		if (hasAuth("bestuur") && $pers->getOverleden()) {
			$medDiv->add($divje = new HtmlDiv(null, 'bs-callout bs-callout-danger'));
			$divje->add(new HtmlHeader(4, _("Deze persoon is overleden")));
		}
		return $medDiv;
	}

	public static function wijzigPersoonPersooninformatieDiv($obj, $show_error, $fullForm = true) {
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(3, _("Persoonsinformatie")));

		$div->add(self::wijzigTR($obj, 'Voornaam', $show_error));
		$div->add(self::wijzigTR($obj, 'Voorletters', $show_error));

		if ($fullForm) {
			$div->add(self::wijzigTR($obj, 'Bijnaam', $show_error));
			$div->add(self::wijzigTR($obj, 'Geboortenamen', $show_error));
		}

		$div->add(self::wijzigTR($obj, 'Tussenvoegsels', $show_error));
		$div->add(self::wijzigTR($obj, 'Achternaam', $show_error));

		if ($fullForm) {
			$div->add(self::wijzigTR($obj, 'TitelsPrefix', $show_error));
			$div->add(self::wijzigTR($obj, 'TitelsPostfix', $show_error));
			$div->add(self::wijzigTR($obj, 'Geslacht', $show_error));
			$div->add(self::wijzigTR($obj, 'Voornaamwoord', $show_error));
			$div->add(self::wijzigTR($obj, 'VoornaamwoordZichtbaar', $show_error));
			$div->add(self::wijzigTR($obj, 'DatumGeboorte', $show_error));
			//$div->add(self::wijzigTR($obj, 'GPGkey', $show_error));
			$div->add(self::wijzigTR($obj, 'Rekeningnummer', $show_error));
		}

		return $div;
	}

	public static function wijzigPersoonContactinformatieDiv($obj, $show_error, $fullForm = true) {
		$div = new HtmlDiv();

		/* De div voor de contactsinformatie **/
		$div->add(new HtmlHeader(3, _("Contact-informatie")));
		$div->add(self::wijzigTR($obj, 'Email', $show_error));

		if ($fullForm) {
			$div->add(self::wijzigTR($obj, 'Homepage', $show_error));
			$div->add(PersoonVoorkeurView::wijzigTR($obj->getVoorkeur(), 'Taal', $show_error));
		}

		return $div;
	}

	/**
	 * Toont een QR-code voor Google/Bing-maps
	 */
	public static function mapsQR(Contact $lid) {
		Page::getInstance()
			->start()
			->add(new HtmlHeader(2, LidView::makeLink($lid)))
			->add(ContactView::mapsQR($lid))
			->end();
	}

	public static function foto(Persoon $pers, $links = false, $anchor = true, $size = 'Medium', $printbaar = false) {
		$div = new HtmlDiv();
		$profielfoto = ProfielFoto::zoekHuidig($pers);
		$magZien = $pers->magFotoWordenBekeken();

		if ($profielfoto && $magZien) {
			$foto = $profielfoto->getMedia();
			$link = $foto->maakProfielThumbnail($pers, $anchor, $size, $printbaar);
			$link->addClass('thumbnail');
			$div->add($link);
		} else {
			// als de persoon de foto mag zien, laat de normale penguin zien
			// anders de 'no access penguin'
			$img = new HtmlImage(Media::penguinFoto(!$magZien), 'Penguin');
			$img->addClass('thumbnail');
			$img->setLazyLoad();
			if ($size == 'Medium') {
				$img->setCssStyle('height: 338px; width: auto;');
			} elseif ($size == 'Thumbnail') {
				$img->setCssStyle('height: 110px; width: auto;');
			}

			$div->add($img);
		}

		$div->add($knopdiv = new HtmlDiv());
		$knopdiv->setCssStyle('padding: 5px;');
		if ($pers->magWijzigen() && $links) {
			$knopdiv->add(HtmlAnchor::button($pers->url() . '/Foto/Wijzig', _('Wijzig de pasfoto')));
		}
		// zoek alle foto's en video's die deze persoon heeft gemaakt
		$media = MediaVerzameling::geefFotos('persoon', $pers);
		// als die minstens 1 zijn, kunnen we ze bekijken
		if ($links && count($media)) {
			$knopdiv->add(HtmlAnchor::button('/FotoWeb/Fotografen/' . $pers->geefID(), _('Bekijk gemaakte foto\'s')));
		}

		if ($links && $pers->heeftFotos()) {
			$knopdiv->add(HtmlAnchor::button($pers->url() . '/Fotos', _('Meer foto\'s')));
		}

		if ($links) {
			$kart = Kart::geef()->getPersonen();
			if ($kart->bevat($pers)) {
				$content = HtmlSpan::fa_stacked([
					HtmlSpan::fa('shopping-cart', _('Unshop'), 'fa-stack-1x text-muted'),
					HtmlSpan::fa('ban', _('Unshop'), 'fa-stack-2x text-danger'),
				]);
				$knopdiv->add(new HtmlSpan($a = PersoonView::makeLink($pers, $content, null, 2)));
				$a->setAttribute('onclick', "unShopLidAjax(" . $pers->geefID() . ", this); return false");
				$a->addClass('btn btn-primary');
			} else {
				$content = HtmlSpan::fa('shopping-cart', _('Shop'), 'text-muted');
				$knopdiv->add(new HtmlSpan($a = PersoonView::makeLink($pers, $content, null, 1)));
				$a->setAttribute('onclick', "shopLidAjax(" . $pers->geefID() . ", this); return false");
				$a->addClass('btn btn-primary');
			}
		}

		if ($links && count(MediaVerzameling::getTopratedByPersoon($pers)) > 0 && hasAuth('bestuur')) {
			$knopdiv->add($a = new HtmlAnchor('/Leden/' . $pers->geefID() . '/PasfotoVerwijderen', _('Pasfoto verwijderen')));
			$a->addClass('btn btn-danger btn-sm');
		}

		return $div;
	}

	public static function fotoUpload($lidnr) {
		global $BESTUUR;

		$page = new HtmlDiv();

		if (ProfielFoto::zoekNieuw(Persoon::geef($lidnr))) {
			$page->add(new HtmlDiv(_("Let op! Je hebt al een foto geüpload die "
				. "nog niet geaccepteerd is. Deze foto wordt verwijderd als je "
				. "een nieuwe pasfoto uploadt."), 'alert alert-warning'));
		}

		$fotoForm = HtmlForm::named('fotoForm');
		$fotoForm->add(HtmlInput::makeFile("userfile"));
		$fotoForm->add(HtmlInput::makeSubmitButton(_("Upload pasfoto")));

		$page->add(new HtmlDiv(new HtmlDiv(
			sprintf(_('Hier kan je je foto uploaden. Deze foto zal echter niet '
				. 'direct zichtbaar zijn op de website. Zij wordt nog handmatig gecheckt. '
				. 'Mail %s voor vragen. Let er wel op dat je foto een beetje op een '
				. 'pasfoto lijkt en je pasfoto mag maximaal 10MB zijn. We ondersteunen '
				. 'alleen png, jpeg of gif.')
				, new HtmlAnchor('mailto:' . $BESTUUR['secretaris']->getEmailAdres(), SECRNAME))
			, 'panel-body')
			, 'panel panel-default')
		)
			->add($fotoForm);

		return $page;
	}

	public static function overledenForm(Persoon $pers) {
		$form = HtmlForm::named('zetOverleden');
		$form->add(new HtmlParagraph("Iemand als overleden markeren heeft het volgende effect:"))
			->add($list = new HtmlList());
		$list->add(new HtmlListItem("Het adres wordt weggegooid, maar ter backup in de opmerkingen geplaatst"));
		$list->add(new HtmlListItem("Alle abonnementen worden verwijderd (email en post)"));
		$list->add(new HtmlListItem("De persoon wordt op niet-vindbaar voor reguliere oud-leden gezet."));

		$form->add(HtmlInput::makeSubmitButton(_("Zeker weten?")));
		$form->add(HtmlAnchor::button($pers->url(), "Haal me hier weg!"));
		return $form;
	}

	public static function pasfotoVerwijderenForm(Persoon $pers) {
		$form = HtmlForm::named('pasfotoVerwijderen');
		$form->add(new HtmlParagraph(sprintf(_("Weet je zeker dat je de pasfoto van %s wilt verwijderen?"), PersoonView::naam($pers))));
		$form->add(HtmlInput::makeSubmitButton(_("Ja")));
		return $form;
	}

	public static function achievements(Persoon $pers) {
		global $request, $BADGES;

		// Vang de hacker-achievement af
		if ($request->request->get('hacker')) {
			if ($pers == Persoon::getIngelogd()) {
				$persbadges = $pers->getBadges();
				$persbadges->addBadge('hacker');
			}
		}

		// Vang de dilbert-achievement af
		if ($request->request->get('dilbert') == "U8DuvlB9HB") {
			if ($pers == Persoon::getIngelogd()) {
				$persbadges = $pers->getBadges();
				$persbadges->addBadge('dilbert');
			}
		}

		if ($pers == Persoon::getIngelogd()) {
			$persbadges = $pers->getBadges();
			$persbadges->addBadge('badges');
		}

		// Check hier alle badges zodat het allemaal up-to-date is
		PersoonBadge::checkAllBadges($pers);

		$pagediv = new HtmlDiv();

		// Gebruikers met de juiste rechten kunnen achievements uitdelen.
		$magUitdelen = false;
		$form = HtmlForm::named('deelBadgeUit');
		$form->add(new HtmlSpan(_("Geef een achievement aan deze persoon: ")));
		foreach ($BADGES["onetime"] as $badgenaam => $badgeinfo) {
			// Heeft alleen zin als degene de badge nog niet heeft.
			if ($pers->hasBadge($badgenaam)) {
				continue;
			}
			if ($badgeinfo['beloonbaar'] && hasAuth($badgeinfo['beloonbaar'])) {
				$form->add(new HtmlButton('submit', $badgeinfo['titel'], "badgenaam", $badgenaam, 'btn btn-default'));
				$magUitdelen = true;
			}
		}
		if ($magUitdelen) {
			// wat spatiëring tussen formulieren en tabel
			$form->add(new HtmlBreak());
			$form->add(new HtmlBreak());
			$pagediv->addImmutable($form);
		}

		$persbadges = $pers->getBadges();
		$aantal = $persbadges->aantalBadges();

		// Genereer de achievementtabel iets eerder
		// zodat we meteen het aantal schaduwachievements tellen
		$ottable = new HtmlTable();
		$i = 0;
		$schaduwcount = 0;
		foreach ($BADGES['onetime'] as $badge => $badgeArray) {
			// verberg schaduwachievements als je die nog niet hebt
			if (!Persoon::getIngelogd() || (!Persoon::getIngelogd()->hasBadge($badge) && $badgeArray['schaduw'])) {
				if ($pers->hasBadge($badge)) {
					$schaduwcount++;
				}

				if (!hasAuth('god')) {
					continue;
				}
			}

			if ($i % 6 == 0) {
				$row = $ottable->addRow();
			}

			$row->add(PersoonBadgeView::toTableCell($badge, $badgeArray, $pers));
			$i++;
		}

		$pagediv->add(
			$aantalDiv = new HtmlDiv(sprintf((Persoon::getIngelogd() == $pers)
				? _("Je hebt al %s %s!")
				: _("Deze persoon heeft al %s %s!")
				, $aantal, ($aantal == 1 ? "achievement" : "achievements"))
				, 'bs-callout bs-callout-info')
		);

		if (($schaduwcount > 0) && (!hasAuth('god'))) {
			$aantalDiv->add($schaduwParagraph = new HtmlParagraph(
				sprintf(_("(aantal verborgen schaduwachievements: %d)"), $schaduwcount)
			));
			$schaduwParagraph->addClass('nietGehaald');
		}

		$pagediv->add(new HtmlHeader(3, _("Eenmalige achievements")));
		// Zie hierboven voor het genereren van deze tabel
		$pagediv->add($ottable);

		$pagediv->add(new HtmlHeader(3, _("Medailleachievements")));

		$pagediv->add($table = new HtmlTable());
		$table->setCSSStyle("width: 99%"); // fix autismepixel

		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->addHeader(_('Achievement'));
		$row->addHeader(_('Brons'))->addClass('center');
		$row->addHeader(_('Zilver'))->addClass('center');
		$row->addHeader(_('Goud'))->addClass('center');
		$row->addHeader(aesnaam())->addClass('center');

		foreach ($BADGES['bsgp'] as $key => $badge) {
			$tbody->add(PersoonBadgeView::makeBSGPRow($badge, $key, $pers));
		}

		// Dit formulier is voor het halen van de dilbert-achievement
		$form = HtmlForm::named('dilbert')
			->add(HtmlInput::makeText('dilbert', null, 0, null, 'dilbert'))
			->addClass('dilbertForm');
		$pagediv->add($form);

		$page = Page::getInstance()
			->addHeadCss(Page::minifiedFile('achievements', 'css'))
			->start(sprintf(_("Achievements van %s"), PersoonView::naam($pers)))
		// Dit stukje javascript is om ervoor te zorgen dat de dilbert-input
		// van het dilbertForm altijd geselecteerd wordt

		// Het is opzich een best vieze hack, maar werkt prima!
			->add(HtmlScript::makeJavascript('
$(function() {
	$("body").on("keydown", function(e) {
		if (e.key && e.key.length == 1)
			$("input[name=dilbert]").get(0).value += e.key;
		else if (e.key == "Enter")
			$("input[name=dilbert]").parent().submit();
	});
});
			'))
			->add($pagediv)
			->end();
	}

	/**
	 * Geef een HTMLObject om in een selectiedropdown te stoppen
	 * @param naam Formulier-naam van de checkbox
	 * @param figuur Iemand om te selecteren
	 * @param single Mag er maar eentje geselecteerd worden?
	 * @param reedsGeselecteerd Vink deze figuur alvast aan
	 */
	public static function selectieOptie($naam, $figuur, $single = false, $reedsGeselecteerd = false) {
		$divje = new HtmlDiv([], "PersoonSelectOptie");
		$divje->setAttribute("id", $figuur->geefId());

		if ($single) {
			$divje->add($input = HtmlInput::makeRadio($naam, $figuur->geefId(), $reedsGeselecteerd));
		} else {
			$divje->add($input = HtmlInput::makeCheckbox($naam . '[' . $figuur->geefId() . ']', $reedsGeselecteerd));
		}

		$input->setAttribute("onchange", "Form_defaultFormPersoonVerzameling_checkboxChecked();");
		$divje->add(new HtmlSpan(PersoonView::makeLink($figuur), "PersoonSelectieNaam"));
		$input->addClass('PersoonVerzamelingSelectieBox');

		return $divje;
	}

	public static function systeemAccount(Persoon $pers, $uid) {
		$cies = $pers->getCommissies();

		$page = Page::getInstance();
		$page->addFooterJS(Page::minifiedFile('ciecheck.js', 'js'));
		$page->start(sprintf(_('Systeemaccount van %s'), PersoonView::makeLink($pers)));

		$page->add($div = new HtmlDiv(null, 'lid-id', $pers->geefID()));
		$div->setCssStyle('display: none;');

		$page->add(new HtmlHeader(4, sprintf(_('Commissies waar %s in zit maar waarbij het '
			. 'systeemaccount niet gelinkt is'), PersoonView::makeLink($pers))));

		$page->add($welin = new HtmlTable(null, null, 'wel-in'));

		foreach ($cies as $cie) {
			$row = $welin->addRow();
			$row->addData(CommissieView::makeLink($cie));
			$row->addData(new HtmlDiv(null, 'rotating-plane rotating-plane-small to-check', 'plane-' . strtolower($cie->getLogin())));
		}

		$page->add(new HtmlHeader(4, sprintf(_('Commissies waar %s niet in zit maar waarbij het '
			. 'systeemaccount wel gelinkt is'), PersoonView::makeLink($pers))));

		$page->add($nietin = new HtmlTable(null, null, 'niet-in'));
		$nietin->addBody();

		$page->end();
	}

	public static function geefSysteemAccount(Persoon $pers) {
		$page = Page::getInstance();
		$page->start(sprintf('Geef %s systeemaccount', PersoonView::makeLink($pers)));

		$page->add(new HtmlParagraph(_('Als policy voor de naamgeving van de systeemaccounts '
			. 'van mensjes geldt het volgende: gebruik de voornaam van het mensje. '
			. 'Als die naam al bezet is, voeg dan de eerste letter van de achternaam toe. '
			. 'Als die naam al bezit is, voeg de tweede letter toe etc. Uitzondering: '
			. 'als de achternaam aan het account wordt toegevoegd komen van alle tussenvoegels '
			. 'precies de eerste letter in de accountnaam. Voorbeeld: het lid Jan '
			. 'van der Jan krijgt dan als accoutnaam janvdj.')));

		$page->add($form = HtmlForm::named('GeefSysteemAccount'));
		$form->add(self::makeFormStringRow('login', strtolower($pers->getVoornaam()), _('AccountNaam')));
		$form->add(HtmlInput::makeFormSubmitButton(_('Maak aan')));

		$page->end();
	}

	public static function detailsCiesInfoDiv(Persoon $persoon) {
		$body = new HtmlDiv();

		foreach ([_('Huidige') => false, _('Oude') => true] as $type => $huidig) {
			//bovenDiv
			$body->add($bovenDiv = new HtmlDiv(null, 'row'));

			//bovenDiv - cies
			if ($huidig) {
				$cies = $persoon->getOudeCommissies();
			} else {
				$cies = $persoon->getCommissies();
			}

			$soorten = ['CIE' => _('Commissies'), 'GROEP' => _('Groepen'), 'DISPUUT' => _('Disputen')];

			$aantal = 0;
			if ($cies->aantal() > 0) {
				foreach ($soorten as $soortId => $soortNaam) {
					if ($cies->aantalSoort($soortId) > 0) {
						$aantal++;
					}
				}
			}

			if ($aantal == 0) {
				$aantal++;
			}

			if ($cies->aantal() > 0) {
				foreach ($soorten as $soortId => $soortNaam) {
					if ($cies->aantalSoort($soortId) > 0) {
						$bovenDiv->add($divje = new HtmlDiv(null, 'col-md-' . (12 / ($aantal))));
						$divje->add(new HtmlHeader(3, sprintf('%s %s', $type, $soortNaam)));
						$divje->add(CommissieVerzamelingView::lijstMetSpreuken($cies, $persoon, null, $soortId));
					}
				}
			} elseif (!$huidig) {
				$bovenDiv->add($divje = new HtmlDiv(null, 'col-md-12'));
				$divje->add(new HtmlHeader(3, _('Commissies')));
				$divje->add(new HtmlEmphasis('Deze persoon zit momenteel in geen commissies.'));
			}
		}

		return $body;
	}

	public static function detailsActiviteitenInfoDiv(Persoon $persoon) {
		$body = new HtmlDiv();

		$body->add($actDiv = new HtmlDiv(null));

		$actToekomst = $persoon->getDeelgenomenActiviteiten(true, 'Toekomst');
		$actVerleden = $persoon->getDeelgenomenActiviteiten(true, 'Verleden');

		if ($actToekomst->aantal() > 0) {
			$actDiv->add(new HtmlHeader(3, _('Nog te bezoeken activiteiten (' . $actToekomst->aantal() . ')')));
			$actDiv->add(ActiviteitVerzamelingView::htmlLijst($actToekomst));
		}

		$actDiv->add(new HtmlHeader(3, _('Bezochte activiteiten (' . $actVerleden->aantal() . ')')));

		if ($actVerleden->aantal() > 0) {
			$actDiv->add(ActiviteitVerzamelingView::htmlLijst($actVerleden));
		} else {
			$actDiv->add(new HtmlEmphasis(_('Dit persoon heeft nog niet deelgenomen aan activiteiten.')));
		}

		return $body;
	}

	/**
	 * @brief Geef de waarde van het veld voornaamwoord.
	 *
	 * Voor persoonsinfo is een lijstje met vervoegingen genoeg.
	 *
	 * @param obj Het Persoon-object waarvan de waarde wordt verkregen.
	 * @return Een html-veilige string die de waarde van het veld voornaamwoord
	 * van het object obj representeert.
	 */
	public static function waardeVoornaamwoord(Persoon $obj) {
		//Als het object niet bestaat geven we niets terug
		if (!$obj->getVoornaamwoord()) {
			return NULL;
		}

		return VoornaamwoordView::vervoegingen($obj->getVoornaamwoord());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voornaamwoord.
	 *
	 * @see genericFormvoornaamwoord
	 * @param obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 * @return Een HtmlElement waarin de huidige waarde van het veld voornaamwoord
	 * staat en kan worden bewerkt. Indien voornaamwoord read-only is betreft
	 * het een statisch html-element.
	 */
	public static function formVoornaamwoord(Persoon $obj, $include_id = false) {
		// TODO SOEPMES: waarom doet generated code het fout?
		return VoornaamwoordView::genericDefaultForm(
			static::formobj($obj) . '[Voornaamwoord]',
			$obj->getVoornaamwoord()->geefID()
		);
	}

	public static function labelVoornaamwoordZichtbaar(Persoon $obj) {
		return _("Voornaamwoord zichtbaar?");
	}
}

// vim:sw=4:ts=4:tw=0:foldlevel=1
