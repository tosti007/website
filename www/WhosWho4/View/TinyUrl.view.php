<?

/**
 * $Id$
 */
abstract class TinyUrlView
	extends TinyUrlView_Generated
{
	public static function details($tinyUrl)
	{
		$pageDiv = new HtmlDiv();

		$pageDiv->add(new HtmlParagraph(_('Dit is de informatie die we hebben van deze TinyUrl.')))
				->add(TinyUrlView::viewInfo($tinyUrl));

		$page = Page::getInstance();
		$page->start(_('TinyUrl') . ': ' . $tinyUrl->geefID())
			->add($pageDiv)
			->add(new HtmlParagraph(
				_("Probeer de TinyUrl uit: " . new HtmlAnchor("/t/" . $tinyUrl->getTinyUrl(),
														 "http://a-es2.nl/t/" . $tinyUrl->getTinyUrl()))
				. new HtmlBreak(1) . _("Terug naar het overzicht: ") . new HtmlAnchor("/Service/Intern/TinyUrl",_("goan!"))));

		$page->end();
	}

	public static function wijzigForm($name, $obj = null, $show_error = true)
	{
		//Als we een TinyUrl aanpassen dan weten we het nummer en kunnen we dit
		// laten zien. Als het een nieuwe is dan moeten we nog even de TinyUrl
		// url genereren.
		$obj->getInDB()?$nr = $obj->geefID():$obj->setTinyUrl(TinyUrl::generateTinyUrl(5));

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($obj, 'TinyUrl', $show_error))
			  ->add(self::wijzigTR($obj, 'OutgoingUrl', $show_error))
			  ->add(self::wijzigTR($obj, 'Mailings', $show_error));

		/** Voeg ook een submit-knop toe **/
		if($obj->getInDB()) { 
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));
		} else {
			$form->add(HtmlInput::makeFormSubmitButton(_("Aanmaken!")));
		}
		return $form;
	}

	public static function makeLink(TinyUrl $tinyUrl)
	{
		if($tinyUrl->magWijzigen()){
			return new HtmlAnchor($tinyUrl->url(), $tinyUrl->geefID());
		} else {
			return $tinyUrl->geefID();
		}
	}

	public static function opmerkingOutgoingUrl()
	{
		return _("Begin met 'http(s)://'");
	}

	/**
	 *  Om hits netjes samen met de rest van het object te laten zien stoppen we
	 *   ze er gewoon bij! In de cls wordt het veld toegevoegd.
	 **/
	public static function labelHits(TinyUrl $obj)
	{
		return _('hits');
	}

	public static function waardeHits(TinyUrl $obj)
	{
		if($obj->magWijzigen())
			return new HtmlAnchor($obj->url() . "/Hitsoverzicht", $obj->amountOfHits());
		return (int) $obj->amountOfHits();
	}

	//Zefde verhaal voor gekoppelde mailings.
	public static function labelMailings(TinyUrl $obj)
	{
		return _('gekoppelde mailings');
	}

	public static function waardeMailings(TinyUrl $obj)
	{
		$tmurls = TinyMailingUrlVerzameling::vanUrl($obj);
		$array = array();
		foreach($tmurls as $tmurl){
			$array[] = new HtmlAnchor("/Service/MailingWeb/Mailings/" . $tmurl->getMailingMailingID(), $tmurl->getMailingMailingID());
		}
		return implode($array, ", ");
	}

	public static function opmerkingMailings(TinyUrl $obj)
	{
		return _("Gescheiden met een ,");
	}

	//Maak een string met all id's van mailings zodat deze aangepast kan worden.
	public static function formMailings(TinyUrl $obj, $include_id = false)
	{
		$mailings = TinyMailingUrlVerzameling::vanUrl($obj)->toMailingVerzameling();	
		$waarde = implode($mailings->keys(), ", ");
		$name = static::formveld($obj, "Mailings", null, $include_id);

		return HtmlInput::makeText($name, tryPar($name, $waarde), min(60, max(30, strlen($waarde))));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
