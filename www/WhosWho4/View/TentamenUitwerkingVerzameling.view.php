<?
abstract class TentamenUitwerkingVerzamelingView
	extends TentamenUitwerkingVerzamelingView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	public static function uitwerkingenPagina(Tentamen $tentamen, TentamenUitwerkingVerzameling $uitwerkingen)
	{
		global $filesystem;

		$page = Page::getInstance();
		$page->start();


		// Geef de naam/datum van het tentamen en de naam van het vak.
		$vakNaam = $tentamen->getVak()->getNaam();
		$page->add(new HtmlHeader(3, '<b>' . $vakNaam . '</b>'));

		$page->add(new HtmlHeader(4, '<b>Toets van ' . $tentamen->getDatum()->format('d-m-Y') . '</b>'));

		//Knop om uitwerking toe te voegen
		if(hasAuth('ingelogd'))
		{
			$page->add(HtmlAnchor::button($tentamen->url() . '/Uitwerkingen/Nieuw', _('Uitwerking toevoegen')));
		}

		$page->add($table = new HtmlTable());

		$table->add($tHead = new HtmlTableHead());
		$table->add($tBody = new HtmlTableBody());
		$tHead->add($row = new HtmlTableRow());
		$row->addHeader('Uploader');
		$row->addHeader('Wanneer');
		$row->addHeader('Uitwerking');
		$row->addHeader('Opmerking');

		//De tentamenbeheercomissie kan zien welke tentamens al gecontroleerd zijn en welke niet
		//Anderen kunnen alleen een lijst met gecontroleerde tentamens zien
		if(hasAuth('tbc'))
		{
			$row->addHeader('Gecontroleerd');
		}

		// Plaats alle uitwerkingen in de tabel
		foreach($uitwerkingen->filterZichtbaar() as $uitwerking)
		{
			$gecontroleerd = $uitwerking->getGecontroleerd();
			$tbc = hasAuth('tbc');

			$tBody->add($row = new HtmlTableRow());

			//Uploader
			if($uitwerking->getUploader() == null)
				$row->addData(_('<i>Tentamen beheer commissie</i>'));
			else
				$row->addData(TentamenUitwerkingView::waardeUploader($uitwerking));
			//Upload datum
			$row->addData(TentamenUitwerkingView::waardeWanneer($uitwerking));

			//Uitwerking pdf file
			$file = $uitwerking->getBestand();
			if($filesystem->has($file)) {
				$row->addData(new HtmlAnchor($uitwerking->url()
					, HtmlSpan::fa('file-pdf-o', _('pdf'))));
			} else {
				$row->addData('-');
			}

			//Opmerking
			$opmerking = TentamenUitwerkingView::waardeOpmerking($uitwerking);
			if($uitwerking->uploaderIsIngelogd() && !$gecontroleerd)
			{
				// Pas de opmerking aan voor de uploader, zodat deze kan zien dat z'n uitwerking nog verwerkt moet worden.
				$opmerking = $opmerking . _('<br><i>Nog niet gecontroleerd.</i>');
			}
			if($opmerking)
				$row->addData($opmerking);
			else
				$row->addData('-');

			$ingelogd = Persoon::getIngelogd();
			// Alleen zichtbaar voor tbc
			if(hasAuth('tbc'))
			{
				if($gecontroleerd)
					$row->addData(HtmlSpan::fa('check', _('Gecontroleerd'), 'text-success'));
				else
					$row->addData(HtmlSpan::fa('times', _('Niet gecontroleerd'), 'text-danger'));
			}
			// Alleen zichtbaar voor tbc of uploader
			if($tbc || $uitwerking->uploaderIsIngelogd())
			{
				$row->addData(new HtmlAnchor($uitwerking->wijzigURL(), HtmlSpan::fa('pencil', _('Wijzigen'))));
				$row->addData(new HtmlAnchor($uitwerking->verwijderURL(), HtmlSpan::fa('times', _('Verwijderen'), 'text-danger')));
			}
			else
			{
				// lege velden
				$row->addData('');
				$row->addData('');
			}
		}

		// Terug naar het vak
		$page->add(HtmlAnchor::button($tentamen->getVak()->url(), _('Terug')));

		$page->end();
	}

	/**
	 *  Laat een tabel zien met alle uitwerkingen die nog niet gecontroleerd zijn. Dit is puur voor de tbc.
	 */
	public static function uitwerkingenControleerPagina(TentamenUitwerkingVerzameling $uitwerkingen)
	{
		global $filesystem;

		if(!hasAuth('tbc'))
			spaceHttp(403);

		$page = Page::getInstance();
		$page->start();

		$page->add(new HtmlHeader(5, '<i>Deze pagina is slechts zichtbaar voor de TBC.</i>'));

		$page->add($table = new HtmlTable());
		$table->add($tHead = new HtmlTableHead());
		$table->add($tBody = new HtmlTableBody());
		$tHead->add($row = new HtmlTableRow());
		$row->addHeader('Uploader');
		$row->addHeader('Wanneer');
		$row->addHeader('Vak');
		$row->addHeader('Tentamen');
		$row->addHeader('Uitwerking');
		$row->addHeader('Opmerking');
		$row->addHeader('Gecontroleerd');

		// Plaats alle uitwerkingen in de tabel
		foreach($uitwerkingen->filterZichtbaar() as $uitwerking)
		{
			// Controlleer of gebruiker tbc is
			$gecontroleerd = $uitwerking->getGecontroleerd();

			$tBody->add($row = new HtmlTableRow());
			//Uploader
			if($uitwerking->getUploader() == null)
				$row->addData(_('<i>Tentamen beheer commissie</i>'));
			else
				$row->addData(TentamenUitwerkingView::waardeUploader($uitwerking));
			//Upload datum
			$row->addData($uitwerking->getTentamen()->getDatum()->format('Y-m-d'));
			//Vaknaam
			$row->addData($uitwerking->getTentamen()->getVak()->getNaam());

			$tentamen = $uitwerking->getTentamen();
			$tentamenFile = $tentamen->getBestand();
			if($filesystem->has($tentamenFile)) {
				$row->addData(new HtmlAnchor($tentamen->url()
					, HtmlSpan::fa('file-pdf-o', _('pdf'))));
			} else {
				$row->addData('-');
			}

			$uitwerkingFile = $uitwerking->getBestand();
			if($filesystem->has($uitwerkingFile)) {
				$row->addData(new HtmlAnchor($uitwerking->url()
					, HtmlSpan::fa('file-pdf-o', _('pdf'))));
			} else {
				$row->addData('-');
			}

			$opmerking = TentamenUitwerkingView::waardeOpmerking($uitwerking);
			if($opmerking)
				$row->addData($opmerking);
			else
				$row->addData('-');

			if($gecontroleerd)
				$row->addData(HtmlSpan::fa('check', _('Gecontroleerd'), 'text-success'));
			else
				$row->addData(HtmlSpan::fa('times', _('Niet gecontroleerd'), 'text-danger'));
			$row->addData(new HtmlAnchor($uitwerking->wijzigURL() . '/?sender=controleren', HtmlSpan::fa('pencil', _('Wijzigen'))));
			$row->addData(new HtmlAnchor($uitwerking->verwijderURL() . '/?sender=controleren', HtmlSpan::fa('times', _('Verwijderen'), 'text-danger')));

		}

		// Terug naar alle vakken
		$page->add(HtmlAnchor::button('/Onderwijs/Tentamens', _('Terug')));

		$page->end();
	}

}
