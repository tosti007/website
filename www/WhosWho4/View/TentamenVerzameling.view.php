<?
/**
 * $Id$
 */
abstract class TentamenVerzamelingView
	extends TentamenVerzamelingView_Generated
{
	/*
	 * Maakt van een tentamenverzameling een mooie tabel
	 * voor bijvoorbeeld bij een vak
	 * @param tents De tentamenverzameling om een tabel bij te maken
	 */
	static public function toTable(TentamenVerzameling $tents)
	{
		global $filesystem;

		$table = new HtmlTable();
		$table->add($head = new HtmlTableHead())
			->add($body = new HtmlTableBody());

		$head->add($row = new HtmlTableRow());
		$row->addHeader(_('Datum'));
		$row->addHeader(_('Naam'));
		$row->addHeader(_('Tentamen'));
		$row->addHeader(_('Uitwerkingen'));
		$row->addHeader(_('Opmerkingen'));
		if(hasAuth('ingelogd'))
			$row->addHeader(_('Upload<br>Uitwerking'));

		foreach($tents as $t) {
			$body->add($row = new HtmlTableRow());
			$row->addData(TentamenView::waardeDatum($t));
			$row->addData($t->getNaam());

			$file = $t->getBestand();
			if($t->getTentamen() && $filesystem->has($file)) {
				$row->addData(new HtmlAnchor($t->url()
					, HtmlSpan::fa('file-pdf-o', _('pdf'))));
			} else {
				$row->addData('-');
			}

			$aantalUitwerkingen = $t->geefAantalUitwerkingenZichtbaar();

			if($aantalUitwerkingen == 0)
				$row->addData(new HtmlSpan('-'));
			else
				$row->addData(new HtmlAnchor($t->url() . '/Uitwerkingen', $aantalUitwerkingen));

			$row->addData($t->getOpmerking());

			if(hasAuth('ingelogd'))
				$row->addData(new HtmlAnchor($t->url() . '/Uitwerkingen/Nieuw', HtmlSpan::fa('plus-circle', 'Uitwerking Toevoegen')));


			if(hasAuth('tbc')) {
				$row->addData(new HtmlAnchor($t->wijzigURL()
					, HtmlSpan::fa('pencil', _('Wijzigen'))));
				$row->addData(new HtmlAnchor($t->verwijderURL()
					, HtmlSpan::fa('times', _('Verwijderen'), 'text-danger')));
			}
		}

		return $table;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
