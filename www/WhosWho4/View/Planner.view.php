<?

/**
 * $Id$
 */
abstract class PlannerView
	extends PlannerView_Generated
{
	/** Enkele labels + opmerkingen voor de formulieren **/
	static public function labelTitel(Planner $obj)
	{
		return _("Titel van de planner");
	}
	static public function labelOmschrijving(Planner $obj)
	{
		return _("Omschrijving");
	}
	static public function labelMail(Planner $obj)
	{
		return _("Ontvang mail bij wijzigingen");
	}
	static public function opmerkingMail ()
	{
		return _("Als je dit aanvinkt ontvang je mail zodra een deelnemer de planner heeft ingevuld.");
	}

	static public function overzichtTableRow (Planner $planner)
	{
		$row = new HtmlTableRow();
		$row->addData(PlannerView::makeLink($planner));
		if($planner->getPersoon())
			$row->addData(PersoonView::makeLink($planner->getPersoon()));
		else
			$row->addData(_("Onbekend"));

		if(!$planner->getDeelnemers()->bevat(Persoon::getIngelogd()))
			$row->addEmptyCell();
		else
		{
			$ingevuld = $planner->ingevuld(Persoon::getIngelogd());
			switch ($ingevuld)
			{
			case 2:
				$row->addData(_('Ja'), 'wel');
				break;
			case 1:
				$row->addData(_('Deels'), 'deels');
				break;
			case 0:
			default:
				$row->addData(_('Nee'), 'niet');
			}
		}

		$data = $planner->getData();
		$data->sorteer("momentBegin");
		$row->addData($data->first()->getMomentBegin()->format("Y-m-d"));
		$row->addData($data->last()->getMomentBegin()->format("Y-m-d"));
		return $row;
	}

	/**
	 * Maak een klikbare link
	 */
	static public function makeLink (Planner $planner, $caption = null)
	{
		if (!$caption)
			$caption = self::waardeTitel($planner);
		return new HtmlAnchor($planner->url(), $caption);
	}

	/**
	 * Geef een formulier om een nieuwe planner aan te maken
	 */
	static public function nieuwForm ($name = 'PlannerNieuw', $obj = null
		, $show_error = true, $datesInPast = false)
	{
		/**
		 * We gaan 3 divs maken
		 * - statische informatie
		 * - deelnemers
		 * - data
		 */

		/** Als er nog geen object is meegestuurd, dan maken we zelf wel iets **/
		$nieuw = !tryPar('submit');
		if (is_null($obj))
			$obj = new Planner();

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		$form->add(new HtmlDiv(_('Hieronder kan je een nieuwe planner aanmaken.'
			. 'Vul eerst de algemene informatie in, selecteer dan deelnemers en '
			. 'data en maak vervolgens de planner aan.')
		, 'bs-callout bs-callout-info'));

		/** Eerst de div voor statische informatie in te vullen **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Algemeen")));
		$div->add(self::wijzigTR($obj, 'Titel', $show_error))
			  ->add(self::wijzigTR($obj, 'Omschrijving', $show_error));
// todo: implementeer dit
//			  ->add(self::wijzigTR($obj, 'Mail', $show_error));

		/** Dan de div voor deelnemers te kiezen **/
		$alGeselecteerd = array_keys(array_filter(tryPar('LidVerzameling', array())));
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Deelnemers")))
			->add(new HtmlParagraph(_('Selecteer alle deelnemers')))
			->add(new HtmlSpan((!$nieuw && empty($alGeselecteerd)) ? _("Vergeet niet om deelnemers te selecteren") : '', 'text-danger strongtext'))
			->add(self::defaultFormPersoonVerzameling(PersoonVerzameling::verzamel($alGeselecteerd), "LidVerzameling", false, true));

		/** Dan de data **/
		$data = array_keys( array_filter(tryPar('DatetimeVerzameling', array())) );
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Data")))
			->add(new HtmlSpan((!$nieuw && empty($data)) ? _("Vergeet niet om data te selecteren") : '', 'text-danger strongtext'))
			->add(self::defaultFormDatetimeVerzameling(null, null, $data))
			->add(new HtmlSpan($datesInPast ? 'Er zijn data in het verleden' : '','text-danger strongtext'));

		/** Als laatste nog een plek voor extra informatie**/
		
		$form->add(new HtmlHeader(3, _("Extra informatie")))
			 ->add(new HtmlParagraph(_("Wil je nog extra informatie invoeren? Dit is je kans!")))
		     ->add(new HtmlSpan(array(
			new HtmlTextarea("ExtraInfo"),
		)));

		/** Knopje om de deelnemers te mailen **/
		$form->add(new HtmlSpan(array(
				HtmlInput::makeCheckbox('stuurmail', true),
				new HtmlSpan(_("Mail de deelnemers over de nieuwe planner."))
		)));
		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeSubmitButton(_("Maak planner aan!")));

		$page = Page::getInstance()
			->start(_('Nieuwe planner'))
			->add($form)
			->end();
	}

	/**
	 * Verwerk het nieuwe formulier
	 */
	static public function processNieuwForm (Planner $planner, PlannerDataVerzameling $plannerdata, PlannerDeelnemerVerzameling $deelnemers, &$refToDatesInPast = null)
	{
		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($planner), NULL)){
			return false;
		}

		/** Vul alle statische informatie in **/
		$planner->setPersoon(Persoon::getIngelogd());
		self::processForm($planner);

		/** Verwerk alle data **/
		foreach (tryPar('DatetimeVerzameling', array()) as $datum => $derp){
			if ($derp)
			{
				if(time() > strtotime($datum)){
					$refToDatesInPast = true;
				}

				$plannerdata->voegtoe($pd = new PlannerData());
				$pd->setPlanner($planner)
				   ->setMomentBegin($datum);

				/** Verwerk alle deelnemers **/
				$dataDeelnemers = tryPar('LidVerzameling');

				if (isset($dataDeelnemers)){
					foreach ($dataDeelnemers as $id => $derp){
						if ($derp && $id != Persoon::getIngelogd()->geefID()){
							$deelnemers->voegtoe(new PlannerDeelnemer($pd, Lid::geef($id)));
						}
					}
				}
				$deelnemers->voegtoe(new PlannerDeelnemer($pd, Persoon::getIngelogd()));
			}
		}
	}

	/**
	 * Toon het overzicht van een planner
	 */
	static public function details (Planner $planner)
	{
		$div = new HtmlDiv();
		$div->setAttribute('style', 'overflow-x:auto;');

		$data = $planner->getData();
		$leden = $planner->getDeelnemers()->sorteer("MijEerst");
		PlannerDeelnemerVerzameling::cache($data, $leden);

		$div->add($table = new HtmlTable(null, 'overzicht sortable'));

		$thead = $table->addHead();
		$tbody = $table->addBody();

		$topRow = $thead->addRow();
		$topRow->addHeader(_("Data"));

		foreach ($leden as $lid)
		{
			$lidNaam = PersoonView::naam($lid,
				$leden->aantal() < 12 ? WSW_NAME_CRIMI : WSW_NAME_INITIALEN
			);
			$topRow->addHeader(
				$anchor = LidView::makeLink($lid, $lidNaam),
				'sorttable_nosort'
			);
			if($anchor instanceof HtmlAnchor)
				$anchor->setAttribute('title', PersoonView::naam($lid));
		}

		$topRow->addHeader(_("Totaal (voor/tegen)"));
		$topRow->addHeader(_("Aantal gereserveerde computers (van de 14)"));
		$topRow->addHeader(_("Activiteiten"), 'sorttable_nosort');

		foreach ($data as $datum)
		{
			$row = $tbody->addRow();
			$row->addHeader(
				PlannerDataView::waardeMomentBegin($datum),
				'sorttable_nosort'
			);

			foreach ($leden as $lid)
			{
				$deelnemer = PlannerDeelnemer::geef($datum, $lid);

				// Als het deelnemerobject niet in de db staat is er dus ergens
				// iets mis gegaan, zet m dan alsnog in de db
				if(!$deelnemer) {
					$deelnemer = new PlannerDeelnemer($datum, $lid);
					$deelnemer->opslaan();
				}

				switch ($deelnemer->getAntwoord())
				{
					case 'ONBEKEND':
						$row->addData(_("?"), 'onbekend');
						break;
					case 'LIEVER_NIET':
						$row->addData(_("+-"), 'liever_niet');
						break;
					case 'JA':
						$row->addData(_("+"), 'ja');
						break;
					case 'NEE':
						$row->addData(_("-"), 'nee');
						break;
				}
			}

			$voor = $datum->getTotaalVoor($leden);
			$tegen = $datum->getTotaalTegen($leden);
			$lieverNiet = $datum->getTotaalLieverNiet($leden);
			$promoveerLink = hasAuth('actief')
				? new HtmlAnchor($planner->url() . '/' . $datum->geefID()
					. '/Promoveren',
					_('(Promoveer)')
				) : '';

			$cell = $row->addData(array($voor . ' / ' . $tegen, $promoveerLink));

			// Hack om sorttable goed te laten sorteren.
			// Dit zorgt er als het goed is voor, dat 'liever niet' ook telt als
			// een 'voor', maar dat data met meer 'voor' wel hoger staan.
			$score = ($voor - $tegen) * $leden->aantal()
				+ ($voor - $tegen + $lieverNiet);
			$cell->setAttribute("sorttable_customkey", (string)$score);

			$activiteiten = ActiviteitVerzameling::getActiviteitenMetBegindatum($datum->getMomentBegin(), 1, 'ALLE');

			$totaalComputers = 0;
			if ($activiteiten->aantal() > 0)
			{
				$tekst = array();
				foreach($activiteiten as $activiteit)
				{
					$computers = $activiteit->getAantalComputers();
					$cies = $activiteit->getCommissies()->geefArrayString();
					$tekst[] = implode(', ', $cies) . ': ' . $computers;
					$totaalComputers += $computers;
				}
				$cell = $row->addData(implode('<br>', $tekst));
				$cell->setAttribute("sorttable_customkey", (string)$totaalComputers);
			}
			else
				$row->addData("0");

			$alleActiviteiten = ActiviteitVerzameling::getActiviteitenMetBegindatum($datum->getMomentBegin(), 0, 'ALLE');
			$alleActiviteiten->filterBekijken();
			$row->addData(ActiviteitVerzamelingView::klikbaarKommaLijst($alleActiviteiten));
		}

		return $div;
	}

	public static function wijzigPlanner(Planner $planner, $show_error = false)
	{
		$all = new HtmlDiv();

		$all->add($form = HtmlForm::named('wijzigPlannerData'));

		$form->add($div = new HtmlDiv());
		$div->add(self::wijzigTR($planner, 'Titel', $show_error))
			->add(self::wijzigTR($planner, 'Omschrijving', $show_error));

		$div->add(HtmlInput::makeFormSubmitButton('Wijzig deze gegevens'));

		$all->add($form = HtmlForm::named('wijzigPlanner'));
		$form->add($div = new HtmlDiv());

		$deelnemers = $planner->getDeelnemers();
		$alGeselecteerd = PersoonVerzameling::verzamel(array_keys(array_filter(tryPar('LidVerzameling', array()))));
		$deelnemers->union($deelnemers);

		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Deelnemers")))
			->add(new HtmlParagraph(_('Selecteer alle deelnemers')))
			->add(self::defaultFormPersoonVerzameling(PersoonVerzameling::verzamel($deelnemers->keys()), "LidVerzameling", false, true));

		$data = array_merge($planner->getData()->toArrayMomentBegin(),
							array_keys(array_filter(tryPar('DatetimeVerzameling', array()))));
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Data")))
			->add(self::defaultFormDatetimeVerzameling(null, null, $data));

		$form->add(new HtmlSpan(array(
			HtmlInput::makeCheckbox('stuurmail', True),
			 new HtmlSpan(_("Mail de nieuwe deelnemers over de planner."))
		)));
		$form->add(new HtmlBreak())->add(HtmlInput::makeSubmitButton('Wijzigen'));

		return $all;
	}

	public static function processWijzigPlanner(Planner $planner)
	{
		$plannerdata = new PlannerDataVerzameling();

		$data = array_keys(array_filter(tryPar('DatetimeVerzameling', array())));
		foreach ($data as $datum)
		{
				$plannerdata->voegtoe($pd = new PlannerData());
				$pd->setPlanner($planner)
				   ->setMomentBegin($datum);
		}

		$leden = array_keys(array_filter(tryPar('LidVerzameling', array())));

		$planner->setData($plannerdata)
				->setDeelnemers(PersoonVerzameling::verzamel($leden));
	}

	/**
	 * Verwijder een planner;
	 */
	static public function verwijder($plan)
	{
		$page = Page::getInstance()
			 ->start()
			 ->add(new HtmlHeader(2, _("Verwijderen") . ' ' . new HtmlAnchor($plan->url(), PlannerView::waardeTitel($plan))));

		$form = HtmlForm::named('VerwijderPlanner');
		$form->add(new HtmlParagraph(_("Weet je het zeker?")))
			 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Jazeker!"))))
			 ->add(new HtmlAnchor($plan->url(), _("Ga terug")));
		$page->add($form)
			 ->end();
	}

	/**
	 * Verwijder jezelf uit de planner;
	 */
	static public function zelfVerwijder($plan)
	{
		$page = Page::getInstance()
			 ->start()
			 ->add(new HtmlHeader(2, _("Jezelf verwijderen uit") . ' ' . new HtmlAnchor($plan->url(), PlannerView::waardeTitel($plan))));

		$form = HtmlForm::named('VerwijderJezelf');
		$form->add(new HtmlParagraph(_("Weet je het zeker?")))
			 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Jazeker!"))))
			 ->add(new HtmlAnchor($plan->url(), _("Ga terug")));
		$page->add($form)
			 ->end();
	}


	/**
	 * Geef het schermpje waar je extra info kan invullen voor de herinnering.
	 */
	static public function herinneren($plan)
	{
		$page = Page::getInstance()
			->start()
			->add(new HtmlHeader(2, _("Herinnering versturen voor") . ' ' . new HtmlAnchor($plan->url(), PlannerView::waardeTitel($plan))));

		$form = HtmlForm::named('ExtraInfo');
		$form->add(new HtmlParagraph(_("Wil je nog extra informatie toevoegen? Dit is je kans!")))
			->add(new HtmlSpan(array(
			new HtmlTextarea("ExtraInfo"),
		)))
			->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Versturen!"))))
			->add(new HtmlAnchor($plan->url(), _("Ga terug")));
		$page->add($form);
		return $page;
	}
}



// vim:sw=4:ts=4:tw=0:foldlevel=1
