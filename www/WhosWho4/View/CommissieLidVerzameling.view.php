<?

/**
 * $Id$
 */
abstract class CommissieLidVerzamelingView
	extends CommissieLidVerzamelingView_Generated
{
	// Basic kale view van de gehele collectie.
	static public function view($cieleden){

		echo "<pre>\n";
		print_r($cieleden);
		echo "</pre>\n";

	}

	static public function lijst($cieleden)
	{
		$lijst = array();
		$cieleden->toPersoonVerzameling(); // cache: 1x querien
		foreach($cieleden as $cielid)
		{
			$lijst[] = CommissieLidView::persoonNaam($cielid);
		}

		return $lijst;
	}

	static public function htmlLijst($cieleden, $bonus = false)
	{
		$lijst = new HtmlList();
		$cieleden->toPersoonVerzameling(); // cache: 1x querien
		foreach($cieleden as $cielid)
		{
			if($bonus && $cielid->magBekijken())
				$lijst->add(CommissieLidView::htmlLijstRegelKlikbaarBonus($cielid));
			elseif($cielid->magBekijken())
				$lijst->add(CommissieLidView::htmlLijstRegelKlikbaar($cielid));
			else
				$lijst->add(CommissieLidView::htmlLijstRegel($cielid));
		}
		return $lijst;
	}

	/**
	 *  Geef de leden van een commissie in tabelvorm weer.
	 *
	 * Dit zorgt o.a. voor een mooie tabelheader, sortering en keuze voor
	 * velden die we tonen.
	 *
	 * @param cieleden Een CommissieLidVerzameling om te tonen.
	 * @param status Indien "oud" wordt ook datumEind getoond (keuze uit "huidige" of "oud")
	 * @param bonus Toon wijzig/verwijderlinkjes indien de gebruiker dat mag.
	 */
	static public function htmlTabel($cieleden, $status = "huidige", $bonus = false)
	{
		$table = new HtmlTable(null, 'sortable');

		$table->add($thead = new HtmlTableHead());

		$row = $thead->addRow();
		$row->addHeader(_("CommissieLid"));
		$row->addHeader(_("Begindatum"));
		if($status !== "huidige")
			$row->addHeader(_("Einddatum"));
		$row->addHeader(_("Functie"));
		$row->addHeader(_("Spreuk"));

		$table->add($tbody = new HtmlTableBody());

		$cieleden->toPersoonVerzameling(); // cache: 1x querien
		if ($status !== "huidige") {
			$cieleden->sorteer("datumEindDanLidNaam");
		} else {
			$cieleden->sorteer("datumBeginDanLidNaam");
		}

		foreach($cieleden as $cielid)
		{
			$row = $tbody->addRow();

			$row->addData(CommissieLidView::waardePersoon($cielid));
			$begin = CommissieLidView::waardeDatumBegin($cielid);
		    $begin_iso = CommissieLidView::waardeDatumBeginISO($cielid);
            $begin = $begin === null ? "1 jan 1970" : $begin;
			$row->addChild(HtmlTableDataCell::maakGesorteerde($begin, $begin_iso));

			if($status !== "huidige"){
				$eind = CommissieLidView::waardeDatumEind($cielid);
				$eind_iso = CommissieLidView::waardeDatumEindISO($cielid);	
				$row->addChild(HtmlTableDataCell::maakGesorteerde($eind, $eind_iso));
			}
			$row->addData(CommissieLidView::waardeFunctie($cielid));
			$row->addData(CommissieLidView::waardeSpreuk($cielid));

			// De extra addData in de else zodat de tabel er niet lelijk
			// uitziet als je niet iedereen mag bewerken
			if($bonus && $cielid->magBekijken() && $cielid->magWijzigen()) {
				$row->addData(sprintf("(%s, %s)",
					new HtmlAnchor('Leden/' . $cielid->getPersoonContactID() . '/Wijzig', _("wijzig")),
					new HtmlAnchor('Leden/' . $cielid->getPersoonContactID() . '/Verwijder', _("verwijder"))));
			} else {
				$row->addData();
			}

			if(hasAuth('bestuur') && $status == 'huidige')
			{
				$row->addData(new HtmlDiv(null, 'rotating-plane rotating-plane-small to-check', 'plane-' . $cielid->getPersoonContactID()));
			}
		}

		return $table;
	}

	//Maak leuke tabel met naam + commissie.
	public static function tableLijst($cieleden)
	{
		$div = new HtmlDiv();
		$cieleden->toPersoonVerzameling(); // cache: 1x querien
		$cieleden->toCommissieVerzameling();

		$table = new HtmlTable();
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->addHeader(_("Naam"));
		$row->addHeader(_("Commissie"));

		foreach($cieleden as $cielid){
			$cie = $cielid->getCommissie();
			$persoon = $cielid->getPersoon();

			$row = $tbody->addRow();
			$row->addData(new HtmlAnchor($persoon->url(), PersoonView::naam($persoon)));
			$row->addData(new HtmlAnchor($cie->url(), CommissieView::waardeNaam($cie)));
		}
		$div->add($table);

		return $div;
	}


	/**
	 *  We maken eerst een pagina, gooien er wat commissiedingen boven en 
	 *  maken het koppenblad.
	 */
	static public function commissieKoppenBlad()
	{
		$cie = Commissie::inputToCie(vfsVarEntryName());
		if(tryPar('oud', null)) {
			$cieleden = $cie->leden('oud');
		} else {
			$cieleden = $cie->leden();
		}
		if($cieleden->aantal() == 0)
			$cieleden = $cie->leden('alle');

		$page = Page::getInstance();
		$page->start($cie->getNaam() . " koppenblad");

		$cieKoppenPaginaDiv = new HtmlDiv();

		$cieKoppenDiv = new HtmlDiv();

		$cieKoppenDiv->add(PersoonVerzamelingView::koppen($cieleden));

		$cieKoppenPaginaDiv->add($cieKoppenDiv);

		$page->add($cieKoppenPaginaDiv);

		Page::getInstance()->end();

	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
