<?

/**
 * $Id$
 */
abstract class DonateurVerzamelingView
	extends DonateurVerzamelingView_Generated
{
	/** @name View: wiewatTR @{ **/
	public static function viewWiewatTable($donas)
	{
		$tbl = new HtmlTable();

		$tbl->add($thead = new HtmlTableHead());
		$tbl->add($tbody = new HtmlTableBody());

		$donas->toPersoonVerzameling(); // cachen
		$donas->sorteer('naam');

		$velden = array('Persoon', 'JaarBegin', 'Almanak', 'AvNotulen', 'Jaarverslag',
						'Studiereis', 'Symposium', 'Vakid', 'Donatie', 'Machtiging','Totaal');

		if($donas->aantal() > 0)
		{
			$thead->add(DonateurView::wiewatTR(NULL, $velden));

			foreach($donas as $dona)
			{
				$tbody->add(DonateurView::wiewatTR($dona, $velden));
			}

		}
		return $tbl;
	}
	public static function viewWiewatTex($donas)
	{
		$donas->toPersoonVerzameling(); // cachen
		$donas->sorteer('naam');

		$output = DonateurView::latexCommand(NULL);
		foreach($donas as $dona)
		{
			$latex = DonateurView::latexCommand($dona);
			if (is_null($latex))
			{
				$persoon = $dona->getPersoon();
				print_error(sprintf('%s (%d) heeft geen adres!', PersoonView::naam($persoon), ContactView::waardeContactID($persoon)));
			}

			$output .= $latex;
		}
		return $output;
	}
	public static function toCSV(DonateurVerzameling $verz)
	{
		$velden = array("Wie", "Van", "Tot", "Almanak", "AV-Notulen per Post", "Jaarverslag", "Studierijs", "Symposium",
						"VakIdioot", "Donatie", "Machtiging", "Totaal");
			
		$response = new CSVResponse([$velden], "donateuren.csv");	
		foreach($verz as $dona){
			$data = array(
				"Wie" => PersoonView::naam($dona->getPersoon()),
				"Van" => $dona->getJaarBegin(),
				"Tot" => ($dona->getJaarEind() ? $dona->getJaarEind() : 'nu'),
				"Almanak" => (int) $dona->getAlmanak(),
				"AV-Notulen per Post" => $dona->getAvNotulen(),
				"Jaarverslag" => (int) $dona->getJaarverslag(),
				"Studierijs" => (int) $dona->getStudiereis(),
				"Symposium" => (int) $dona->getSymposium(),
				"VakIdioot" => (int) $dona->getVakid(),
				"Donatie" => $dona->getDonatie(),
				"Machtiging" => (int) $dona->getMachtiging(),
				"Totaal" => $dona->totaalbedrag()
			);

			$response->addRow($data);			
		}
		return $response;
	}
	//@}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
