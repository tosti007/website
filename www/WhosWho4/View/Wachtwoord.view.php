<?

/**
 * $Id$
 */
abstract class WachtwoordView
	extends WachtwoordView_Generated
{
	public static function wijzigForm(Wachtwoord $ww, $meldingen)
	{
		if(!$ww->heeftWachtwoord()) {
			$meldingen[] = _('Er is nog geen wachtwoord ingesteld.');
		}

		foreach($meldingen as $m) {
			Page::addMelding($m, 'fout');
		}

		$page = Page::getInstance();
		$page->start(_("Login/wachtwoord wijzigen"));
		$page->setLastMod($ww);

		$page->add(new HtmlParagraph($ww->heeftWachtwoord() ? _('Als je alleen de login wilt wijzigen laat dan de twee velden voor een nieuw wachtwoord leeg.').' '._('Ter extra verificatie dien je je huidige wachtwoord in te voeren.') : '', 'bs-callout bs-callout-info'));

		$page->add($form = HtmlForm::named("WachtwoordWijzig"));

		$form->add(self::makeFormStringRow('loginnaam', $ww->getLogin(), _('Login')));
		$form->add(self::makeFormPasswordRow('password', _('Nieuw wachtwoord')));
		$form->add(self::makeFormPasswordRow('password2', _('Bevestig wachtwoord')));
		$form->add(self::makeFormPasswordRow('verify', _('Verificatie')));

		$form->add(HtmlInput::makeFormSubmitButton(_('Wijzig')));

		$page->end();
	}

	public static function requestLogin($state, $melding = NULL, $error = NULL)
	{
		$page = Page::getInstance();
		$page->start();

		if(!empty($error)) {
			$page->add(new HtmlDiv($error, 'alert alert-danger'));
		}

		switch($state) {
		case 'ident':
			if($melding)
				$page->add($melding);
			else {
				self::requestLoginID();
				$page->add(new HtmlHR());
			}
			// NO break
		case 'auth':
			self::requestLoginAuth();
			break;
		case 'edit':
			self::requestLoginWijzig();
			break;
		case 'done':
			self::requestLoginKlaar();
			break;
		}

		$page->end();
	}
	private static function requestLoginID()
	{
		global $BESTUUR;

		$form = HtmlForm::named('ident');
		$form->addClass('form-inline');
		$form->add(
			HtmlInput::makeText('voorwie', NULL, 30)
		,	HtmlInput::makeSubmitButton(_('Volgende stap'))
		);

		Page::getInstance()->add(
			new HtmlHeader(2, _('Identificatie'))
		,	new HtmlParagraph(
				_("Vul hier je studentnummer, lidnummer of e-mailadres in, als je nog geen login hebt of je wachtwoord vergeten bent."))
		,	new HtmlParagraph(sprintf(
				_("Als je studentnummer niet werkt, of wij hebben een e-mailadres geregistreerd dat je niet (meer) kan lezen,  stuur dan een mailtje naar de %s of kom even langs in de kamer.")
				, '<a href="mailto:'.$BESTUUR['secretaris']->getEmailAdres().'">Secretaris</a>'))
		,	new HtmlParagraph(
				_("Je kan alleen een login aanvragen als je lid of oud-lid bent."))
		,	$form
		);
	}
	private static function requestLoginAuth()
	{
		$form = HtmlForm::named('auth');
		$form->addClass('form-inline');
		$form->add(
			HtmlInput::makeText('magic', NULL, 30)
		,	HtmlInput::makeSubmitButton(_('Volgende stap'))
		);

		Page::getInstance()->add(
			new HtmlHeader(2, _('Authenticatie'))
		,	new HtmlParagraph(
				_("Vul de per mail ontvangen magische code hieronder in:"))
		,	$form
		);
	}
	private static function requestLoginWijzig()
	{
		$page = Page::getInstance();

		$ww = Wachtwoord::geefMagic(trim(tryPar('magic')));

		$tbl[0][] = _('Login:');
		$tbl[0][] = HtmlInput::makeText('loginnaam', $ww->getLogin(), 20);
        $tbl[1][] = _("Nieuw wachtwoord:");
        $tbl[1][] = HtmlInput::makePassword('password');
        $tbl[2][] = _("Bevestig wachtwoord:");
        $tbl[2][] = HtmlInput::makePassword('password2');
		$tbl[3][] = '';
		$tbl[3][] = HtmlInput::makeSubmitButton(_('Volgende stap'));

		$form = HtmlForm::named('edit');
		$form->addClass('form-inline');
		$form->add(
			HtmlInput::makeHidden('magic', tryPar('magic'))
		,	HtmlTable::fromArray($tbl)
		);


		$page->add(
			new HtmlHeader(2, _("Een login en/of een nieuw wachtwoord aanvragen"))
			, new HtmlParagraph( _("Wijzig hieronder eventueel je login, en/of vul een nieuw wachtwoord in.  Als je geen wachtwoord invult, blijft je wachtwoord onveranderd."))
			, $form
		);
	}
	private static function requestLoginKlaar()
	{
		Page::getInstance()->add(
		  new HtmlHeader(2, _('Login / Wachtwoord gewijzigd'))
		, new HtmlParagraph(_("Je login en/of wachtwoord is succesvol gewijzigd."))
		);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
