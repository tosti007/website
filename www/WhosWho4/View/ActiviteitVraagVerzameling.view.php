<?

/**
 * $Id$
 */
abstract class ActiviteitVraagVerzamelingView
	extends ActiviteitVraagVerzamelingView_Generated
{

	/**
	 *  Geef het formulier om de vragen van een activiteit te wijzigen.
	 * Op de pagina hoort ook nog wat javascript die de knop addVraag afhandelt.
	 * @param vragen De vragen die op dit moment bij de activiteit horen.
	 * @param obj De activiteit waarvoor de vragen zijn.
	 * @param show_error Of er fouten zijn om te tonen.
	 * @returns Een HtmlDiv met de formulierdata.
	 */
	public static function wijzigForm(ActiviteitVraagVerzameling $vragen, ActiviteitInformatie $obj, $show_error)
	{
		$tDiv = new HtmlDiv();
		$tDiv->add(new HtmlHR());

		$jsonArray = array();
		foreach($vragen as $vraag) {
			$arr = array();
			$arr['id'] = $vraag->geefID();
			$arr['type'] = $vraag->getType();
			$arr['vraag_NL'] = $vraag->getVraag('nl');
			$arr['vraag_EN'] = $vraag->getVraag('en');
			$arr['opties'] = $vraag->getOpties();
			$arr['errors'] = $vraag->getErrors();
			$jsonArray[] = $arr;
		}
		$tDiv->add($script = HtmlScript::makeJavascript('var vragenJSON = '.json_encode($jsonArray)));

		$tDiv->add($vragenDiv = new HtmlList(false, $vraagWrapper = new HtmlListItem(null, 'ui-state-default'), 'actvragenlijst'));

		$vragenDiv->setAttribute('data-bind', 'foreach: vragen');
		// TODO: we noteren momenteel nog geen volgorde in de database van alle vragen,
		// waardoor we de huidig bestaande vragen dus niet kunnen permuteren...
		// Als dit wel kan, dan kunnen we de volgende regel gebruiken ipv bovenstaande regel:
		// $vragenDiv->setAttribute('data-bind', 'sortable: { data: vragen, connectClass: \'ko_vragen\' }');

		$vraag = new ActiviteitVraag($obj);
		$vraag->setFakeID(1);

		$vraagWrapper->add($tr = ActiviteitVraagView::wijzigTR($vraag, 'vraag', $show_error, true));
		$tr->getChild(1)->getChild(0)->getChild(1)
			->setAttribute('data-bind', 'value: vraag_NL, '
				. 'attr: {name: name_NL, '
					. 'placeholder: "'.sprintf(_('Vraag %s[VOC: nummer] aan deelnemer-NL'), '"+($index()+1)+"').'"}');
		$tr->getChild(1)->getChild(1)->getChild(1)
			->setAttribute('data-bind', 'value: vraag_EN, '
				. 'attr: {name: name_EN, '
					. 'placeholder: "'.sprintf(_('Vraag %s[VOC: nummer] aan deelnemer-EN'), '"+($index()+1)+"').'"}');
		$tr->getChild(0)->setAttribute('data-bind', 'text: "'.sprintf(_('Vraag %s[VOC: nummer] aan deelnemer'), '"+($index()+1)+"').'"');
        $vraagWrapper->add($tr = ActiviteitVraagView::wijzigTR($vraag, 'type', $show_error, true));
		$selectbox = $tr->getChild(1)->getChild(0);
		$selectbox->setNoDefaultClasses();
		$selectbox->addClass('form-control');
        $selectbox->setAttribute('data-bind', 'value: type, attr: {name: name_type}');

		$vraagWrapper->add($optiesDiv = new HtmlDiv());
		$optiesDiv->setAttribute('data-bind',
			'visible: type() == "ENUM", sortable: { data: opties, connectClass: \'ko_opties\' }');

		$optiesDiv->add($div = new HtmlDiv(null, 'form-group'));

		// Een vraagoptie ziet er zo uit:
		$div->add($label = new HtmlLabel('optie', 'Optie', 'col-sm-2 control-label'));
		$label->setAttribute('data-bind' ,'text: "'.sprintf(_('Optie %s'), '"+($index()+1)'));
		// Een invoer veld:
		$div->add(new HtmlDiv($inputGroup = new HtmlDiv(null, 'input-group'), 'col-sm-10'));
		$inputGroup->add($optieInput = self::genericDefaultFormString('name', '', ''));
		$optieInput->setAttribute('data-bind', 'value: optie, '
			. 'attr: {placeholder: "'.sprintf(_('Optie %s'), '"+($index()+1)').', '
				. 'name: "ActiviteitVraag["+($parent.id())+"][Opties]["+($index())+"]"}');
		// Een 'verwijder optie' knop
		$inputGroup->add(new HtmlSpan($button = HtmlInput::makeButton('verwijder', _('Verwijder')), 'input-group-btn'));
		$button->setAttribute('data-bind', 'click: $parent.removeOptie');

		$vraagWrapper->add(new HtmlDiv($actions = new HtmlDiv(null, 'btn-group col-sm-offset-2 col-sm-10'), 'form-group'));
		$actions->add($button = HtmlInput::makeButton('removeVraag', _('Vraag verwijderen')));
		$button->setAttribute('data-bind', 'click: $parent.removeVraag');
		$actions->add($button = HtmlInput::makeButton('addOptie', _('Optie toevoegen')));
		$button->setAttribute('data-bind', 'click: addOptie, visible: type() == "ENUM"');

		$tDiv->add(new HtmlDiv(new HtmlDiv(
			$button = HtmlInput::makeButton('addVraag', _('Vraag toevoegen')),
			'col-sm-offset-2 col-sm-10'
		), 'form-group'));
		$button->setAttribute('data-bind', 'click: addVraag');

		return $tDiv;
	}

	/**
	 *  Pas de ActiviteitVerzameling aan aan de hand van het formulier.
	 * De verzameling wordt in-place geupdated.
	 * Eventuele nieuwe vragen worden toegevoegd, eventuele oude vragen worden weggegooid.
	 * @param vragen Een verzameling met oude activiteitvragen.
	 * @param act De activiteit waarvan de vragen gewijzigd moeten worden.
	 * @param vraagdata Een array met requestdata die uit het formulier afkomstig is.
	 * @returns De nieuwe verzameling.
	 */
	public static function processWijzigForm(ActiviteitVraagVerzameling $vragen, ActiviteitInformatie $act, $vraagdata)
	{
		if (!$vraagdata)
			$vraagdata = array();

		// maak een verzameling van alle vraag id's in vraagdata
		$bestaatVraag = array();
		foreach ($vraagdata as $id => $vData) {
			// De underscore geeft aan dat je een bestaand ID te pakken hebt.
			$name_parts = explode('_', $id);
			$bestaandeVraag = count($name_parts) > 1;

			$heeftTekst = trim($vData['Vraag']['nl']) != '' || trim($vData['Vraag']['en']) != '';

			// Zoek de bijbehorende vraag op, of maak hem aan
			$vraag = NULL;
			if ($bestaandeVraag) {
				if ($vragen->bevat($id))
					$vraag = ActiviteitVraag::geef($act, $name_parts[1]);
				else
					Page::addMelding(_("Een vraag claimt te bestaan maar bestaat helemaal niet!"), "fout");
			} else if ($heeftTekst) {
				$vraag = new ActiviteitVraag($act);
			}

			if ($vraag == NULL)
				continue;

			if (!$heeftTekst && $bestaandeVraag) {
				// Gooi oude vragen weg als ze geen tekst meer hebben.
				$vragen->verwijder($vraag->geefID());
				$vraag->verwijderen();
			} else {
				ActiviteitVraagView::processForm($vraag, "viewWijzig", $vData);
				$vraag->opslaan();
				$vragen->voegToe($vraag);
				$bestaatVraag[$vraag->geefID()] = true;
			}
		}

		foreach ($vragen as $vraag) {
			if (array_key_exists($vraag->geefID(), $bestaatVraag))
				continue;
			$vragen->verwijder($vraag->geefID());
			$vraag->verwijderen();
		}

		return $vragen;
	}
}

