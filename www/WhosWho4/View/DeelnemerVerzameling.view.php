<?

/**
 * $Id$
 */
abstract class DeelnemerVerzamelingView
	extends DeelnemerVerzamelingView_Generated
{
	/**
	 * Toon een korte lijst met deelnemers
	 */
	static public function kort(DeelnemerVerzameling $deelnemers)
	{
		$personen = $deelnemers->toPersoonVerzameling();
		return PersoonVerzamelingView::kort($personen);
	}

	/**
	 * Toont alle deelnemers simpel
	 */
	static public function simpel(DeelnemerVerzameling $deelnemers, Activiteit $act = null)
	{
		if ($deelnemers->aantal() > 0) {
			$cie_string = "";
			//zoek alle deelnemers die in de organiserende commissies zitten
			$cieleden = new PersoonVerzameling();
			foreach($act->getCommissies() as $cie){
				$soort = strtolower($cie->getSoort());
				$cieleden->union($cie->leden());
				$cie_string .= $cieleden->aantal() . ' ' . strtolower($cie->getSoort()) . 'leden';
			}

			$cieleden->intersection($deelnemers->toPersoonVerzameling());

			if ($act->getCommissies()->aantal() > 0)
			{
				$cieSoort = $act->getCommissies()->first()->getSoort();
			} else {
				$cieSoort = "CIE";
			}

			$tekstDeelnemers = ($deelnemers->aantal() == 1) ? "deelnemer" : "deelnemers";
			$tekstCommissieDeelnemers = ($cieleden->aantal() == 1) ? "lid" : "leden";

			switch ($cieSoort)
			{
				case "GROEP":
					$tekstCommissieSoort = "groeps";
					break;
				case "DISPUUT":
					$tekstCommissieSoort = "dispuut";
					break;
				case "CIE":
				default:
					$tekstCommissieSoort = "commissie";
					break;
			}

			$deelnemers_container = new HtmlDiv(sprintf(_("%d %s, waarvan %d %s%s: "),
										 $deelnemers->aantal(), $tekstDeelnemers, $cieleden->aantal(), $tekstCommissieSoort, $tekstCommissieDeelnemers));
		} else {
			$deelnemers_container = new HtmlDiv(_("Nog geen inschrijvingen."));
		}

		$deelnemers_container->add($deelnemers_div = new HtmlDiv(null, 'row'));
		$deelnemers_div->add(new HtmlDiv($ul = new HtmlList(), 'col-md-6'));

		$deelnemers->sorteer("naam");

		foreach ($deelnemers as $deelnemer)
		{
			$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);
			$ul->addChild(PersoonView::makeLink($deelnemer->getPersoon()));
		}

		return $deelnemers_container;
	}

	/**
	 * Toont alle deelnemers uitgebreid adhv een activiteit icm met een bewerkingsformulier
	 */
	static public function uitgebreid(Activiteit $act, $form = null, $prefix = '', $selectAll = false)
	{
		if(is_null($form)) {
			$div = new HtmlDiv();
		} else {
			$div = HtmlForm::named($form)
				->setAttribute('name', 'DeelnemersLijst')
				->add(HtmlInput::makeHidden('Deelnemers[Actie]', ''));
			$div->addClass('form-inline');
                        $div ->add($par = new HtmlDiv(HtmlAnchor::button($act->url() ,_("Terug naar de activiteitpagina"))));
			$div->add(new HtmlParagraph(HtmlAnchor::button('Inschrijven', _("Nieuwe deelnemer(s) inschrijven"))));
		}

		if($act->deelnemers()->aantal() > 0)
		{
			$anchorArray = array(
				HtmlAnchor::button('#', _("koppenblad bekijken"), null, null, 'koppenbladDeelnemers'),
				HtmlAnchor::button('#', _("in kart stoppen"), null, null, 'kartDeelnemers'),
				HtmlAnchor::button('DeelnemersToCSV', _("CSV")));
			$div->add(self::lijst($act->deelnemers(), $form, $prefix,$selectAll));
			if(!is_null($form))
				$div->add(new HtmlParagraph(array(
						  HtmlAnchor::button('#', _("Selecteer iedereen"), null, null, 'selDeelnemers'),
						  '/',
						  HtmlAnchor::button('#', _("De-selecteer iedereen"), null, null, 'deselDeelnemers')
						  )))
					->add(new HtmlParagraph(array(
						  _("Acties met de geselecteerde deelnemers: "),
						  implode(', ', $anchorArray))));
		}
		else
			$div->add(new HtmlSpan(_("Geen inschrijvingen gevonden."), 'italic'));

		return $div;
	}

	/**
	 * Toont een lijst met alle deelnemers
	 */
	static public function lijst(DeelnemerVerzameling $deelnemers, $form = null, $prefix = null, $selectAll = false)
	{

		$vragen = ActiviteitVraagVerzameling::vanActiviteit($deelnemers->first()->getActiviteit());
		$table = new HtmlTable();
		$row = $table->addRow();

		if (!is_null($form)) {
			$row->addEmptyCell();
		}
		$row->addEmptyCell();
		if($vragen->aantal())
			$row->addEmptyCell();
		$row->addHeader(_("Naam"));
		$row->addHeader(DeelnemerView::labelMomentInschrijven());
		$row->addHeader(ContactTelnrView::labelTelefoonnummer());
		foreach($vragen as $vraag){
			$row->addHeader(ActiviteitVraagView::waardeVraag($vraag));
		}

		$deelnemers->toPersoonVerzameling(); // cachen

		foreach($deelnemers as $deelnemer)
		{
			$table->add($row = new HtmlTableRow());
			if (!is_null($form)) {
				$row->addData(HtmlInput::makeCheckbox($prefix . '[Geselecteerde][' . $deelnemer->getPersoon()->geefID() . ']', $selectAll));
			}
			$row->addData(DeelnemerView::uitschrijfKnop($deelnemer));
			if($vragen->aantal())
				$row->addData(DeelnemerView::wijzigKnop($deelnemer));
			$row->addData(PersoonView::makeLink($deelnemer->getPersoon()));
			$row->addData(DeelnemerView::waardeMomentInschrijven($deelnemer));
			$row->addData(ContactTelnrView::waardeTelefoonnummer($deelnemer->getPersoon()->getPrefTelnr()));

			// sorteer antwoorden op vraag
			$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);
			$antwoordPerVraag = array();
			foreach($antwoorden as $antwoord){
				$antwoordPerVraag[$antwoord->getVraagVraagID()] = $antwoord;
			}

			// houd de volgorden van vragen aan zoals ze in de header terechtkomen
			foreach ($vragen as $vraag)
			{
				$vraagID = $vraag->getVraagID();
				if (array_key_exists($vraagID, $antwoordPerVraag)) {
					$row->addData(DeelnemerAntwoordView::waardeAntwoord($antwoordPerVraag[$vraagID]));
				} else {
					// geen antwoord gegeven, maar we moeten de tabel wel opvullen
					$row->addData('');
				}
			}
		}
		$div = new HtmlDiv($table, 'noOverflow');
		return $div;
	}

	static public function DeelnemersToCSV()
	{
		header("Content-Disposition: attachment; filename=ActiviteitDeelnemers.csv");
		header("Content-type: text/csv; encoding=utf-8");

		$entryData = vfsVarEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);
		$deelnemers = $act->deelnemers();
		$csvarray = array();
		$vragen = ActiviteitVraagVerzameling::vanActiviteit($deelnemers->first()->getActiviteit());
		$headers = array();
		foreach($vragen as $vraag){
			$headers[] = ActiviteitVraagView::waardeVraag($vraag);
		}
		foreach($deelnemers as $deelnemer)
		{
			$data = PersoonView::alsCSVArray($deelnemer->getPersoon());
			unset( $data['bijnaam'], $data['geboortenamen'], $data['voorletters'], $data['straat1'], $data['straat2'], $data['huisnummer'], $data['postcode'], $data['woonplaats'], $data['land'], $data['studentnummer'], $data['lidvan'], $data['titelsPrefix'], $data['titelsPostfix'], $data['overleden'], $data['datumGeboorte'], $data['GPGkey'], $data['Rekeningnummer'], $data['email']);
			$data['Moment van inschrijven'] = DeelnemerView::waardeMomentInschrijven($deelnemer);
			$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);

			$i = 0;
			foreach($antwoorden as $antwoord){
				$data[$headers[$i]] = DeelnemerAntwoordView::waardeAntwoord($antwoord);
				$i++;
			}
			//Vul overige (lege) headers in
			while($i < count($headers))
			{
				$data[$headers[$i]] = "";
				$i++;
			}
			$csvarray[] = $data;
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		if(count($csvarray) > 0){
			fputcsv($output, array_keys($csvarray[0]));
		} else {
			fwrite($output, _("Geen data."));
		}
		
		//Print alle inhoud voor de CSV
		foreach($csvarray as $per){
			fputcsv($output, $per);
		}

		fclose($output);
		exit();
	}

	/**
	 * Formulier om nieuwe deelnemers in te schrijven bij een bekende activiteit
	 */
	static public function nieuwForm($name, $obj = null, $show_error = true, $selected = array(), Activiteit $act)
	{
		$cieleden = new PersoonVerzameling();
		$cies = $act->getCommissies();
		foreach($cies as $cie) {
			$cieleden->union(PersoonVerzameling::vanCommissie($cie));
		}

		$form = HtmlForm::named($name);
		$form->addClass('form-inline');
		$form->add(self::defaultFormPersoonVerzameling(null, 'LidVerzameling'))
			 ->add(self::vragenLijst($act))
			 ->add(new HtmlParagraph(HtmlInput::makeSubmitButton(_("Schrijf in!"))));
		return $form;
	}

	/**
	 * Verwerk het nieuwe formulier bij een bekende activiteit
	 */
	static public function processNieuwForm(DeelnemerVerzameling $verzameling, Activiteit $act)
	{
		global $request;

		/** Haal alle data op **/
		$data = tryPar('LidVerzameling', NULL);

		if(!$data) {
			return false;
		}


		$vragen = ActiviteitVraagVerzameling::vanActiviteit($act);
		$antwoorden = new DeelnemerAntwoordVerzameling();
		$antwoordText = array();
		foreach($vragen as $vraag)
		{
			$id = $vraag->getVraagID();
			$antwoordText[$id] = array(trypar("Antwoord[$id]",''), $vraag);
		}

		/** Verwerk ids **/
		// todo dit moet gegeneraliseerd worden
		$ids = array();
		$data = $request->request->get("LidVerzameling");
		foreach($data as $id => $derp)
		{
			if($derp)
				$ids[] = array($id, $act->geefID());
		}

		/** Maak de deelnemers aan **/
		$nver = DeelnemerVerzameling::verzamel($ids);
		foreach($ids as $id)
		{
			if(!$nver->bevat($id)) {
				$nver->voegtoe($deelnemer = new Deelnemer($id[0], $id[1]));
				$deelnemer->setMomentInschrijven(date("Y-m-d H:i:s"));

				$antwoorden = new DeelnemerAntwoordVerzameling();
				foreach($antwoordText as $antwoordCombo)
				{
					$antwoord = new DeelnemerAntwoord($deelnemer, $antwoordCombo[1]);
					if($antwoordCombo[0] == null) {
						DeelnemerAntwoordView::processForm($antwoord, 'viewWijzig', '');
					} else {
						DeelnemerAntwoordView::processForm($antwoord, 'viewWijzig', array("Antwoord" => $antwoordCombo[0]));
					}
					$antwoorden->voegToe($antwoord);
				}

				$deelnemer->setAntwoorden($antwoorden);
			}
		}
		$verzameling->union($nver);
	}

	/**
	 *  /brief Geef een tabel terug waarin alle vragen van een activiteit worden
	 *	gesteld op z'n manier dat het compatible is met een deelnemerverzameling.
	 *  
	 *  /param act activiteit waarvan je de lijst wil maken.
	 **/
	public static function vragenLijst($act)
	{
		$vragen = $act->getVragen();

		$div = new HtmlDiv();
		$table = new HtmlTable();
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->addHeader(_("Vragen en opmerkingen bij inschrijving"));//?

		foreach($vragen as $vraag)
		{
			$row = $tbody->addRow();
			$row->addData(ActiviteitVraagView::waardeVraag($vraag));
			$row = $tbody->addRow();
			//TODO dit om de een of andere manier vervangen door DeelnemerAntwoordView::formAntwoord
			if($vraag->getType() == 'ENUM')
			{
				$opties = $vraag->getOpties();
				$opties = array(null => _('-- Selecteer een antwoord --')) + $opties;
				$row->addData(HtmlSelectbox::fromArray('Antwoord['.$vraag->geefID().']', $opties, array()));
			}
			else
			{
				$row->addData(HtmlInput::makeText("Antwoord[". $vraag->getVraagID() . "]",null,null,null,$vraag->getVraagID()));
			}
		}
		$div->add($table);
		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
