<?

/**
 * $Id$
 */
abstract class CommissieView
	extends CommissieView_Generated
{
	//Laat de infoPagina zien van de cie uit de url.
	static public function infoPagina($cie, $bestaatLogin = true)
	{
		$page = static::pageLinks($cie);

		if(hasAuth('bestuur'))
			$page->addFooterJs(Page::minifiedFile('cielid', 'js'));

		$page->addFooterJS(Page::minifiedFile('profielJS', 'js'));

		$page->start(self::waardeNaam($cie), null, $cie->getThema());

		$page->add($div = new HtmlDiv(null, 'cie-id', $cie->geefID()));
		$div->setCssStyle('display: none;');

		$cieInfoPaginaDiv = new HtmlDiv();

		if(self::waardeOmschrijving($cie) != "") {
			$cieInfoPaginaDiv->add(new HtmlDiv(self::waardeOmschrijving($cie), 'bs-callout bs-callout-info'));
		}

		$cieInfoPaginaDiv->add(new HtmlDiv($col = new HtmlDiv(null, 'col-lg-12 col-sm-12'), 'row'));

		// Zoek hier de profielfoto als die bestaat...
		$commissieFoto = CommissieFoto::zoekHuidig($cie);

		if($commissieFoto && $commissieFoto->magBekijken())
		{
			$foto = $commissieFoto->getMedia();
			$url_medium = $foto->url() . '/Medium';
			$url_origineel = $foto->url() . '/Origineel';

			// Maak de cards aan
			$col->add($hc = new HtmlDiv(null, 'card hovercard'));
			$hc->add($cbc = new HtmlDiv(null, 'card-background'));

			// ..en voeg de foto toe as achtergrond...
			$cbc->add(new HtmlImage($url_medium, 'profielfoto', _('Profielfoto'), 'card-bkimg'));

			// En als avatar
			$hc->add(new HtmlDiv($img = new HtmlImage($url_medium, 'profielfoto', _('Profielfoto')), 'useravatar'));
			$img->setId('profielFoto');

			$cieInfoPaginaDiv->add($modal = new HtmlDiv(null, 'profielfoto-modal', 'profielFotoModal'));
			$modal->add(new HtmlSpan('&times', 'close', 'close-modal'));
			$modal->add($img = new HtmlImage($url_origineel, 'profielFoto', _('Profielfoto'), 'profielfoto-modal-content'));
			$img->setId('modal-image');
			$modal->add(new HtmlDiv(CommissieView::waardeNaam($cie), null, 'caption'));
		}

		// Maak de buttongroup voor de tabs aan
		$col->add($pref = new HtmlDiv(null, 'btn-pref btn-group btn-group-justified'));

		// Maak de well voor de tabs
		$col->add($well = new HtmlDiv(null, 'well'));
		$well->add($tab_content = new HtmlDiv(null, 'tab-content'));

		// Voeg de persoonsinformatie toe
		$pref->add($btn = new HtmlAnchor('#tab1'
			, new HtmlDiv(
				array(
					HtmlSpan::fa('address-card-o',static::commissieInformatieLabel($cie))
					, new HtmlBreak()
					, static::commissieInformatieLabel($cie)
				)
			)
			, null
			, 'btn btn-primary'
			, 'info'));
		$btn->setAttribute('data-toggle', 'tab');

		$tab_content->add($infoDiv = new HtmlDiv(null, 'tab-pane fade in active', 'tab1'));
		$infoDiv->add(self::cieInfoDiv($cie, $bestaatLogin));

		$pref->add($btn = new HtmlAnchor('#tab2'
			, new HtmlDiv(
				array(
					HtmlSpan::fa('users', _('Leden'))
					, new HtmlBreak()
					, _('Leden')
				)
			)
			, null
			, 'btn btn-default'
			, 'info'));
		$btn->setAttribute('data-toggle', 'tab');

		$tab_content->add($ledenDiv = new HtmlDiv(null, 'tab-pane fade in', 'tab2'));
		$ledenDiv->add(self::cieLedenDiv($cie));

		$pref->add($btn = new HtmlAnchor('#tab3'
			, new HtmlDiv(
				array(
					HtmlSpan::fa('archive', _('Activiteiten'))
					, new HtmlBreak()
					, _('Activiteiten')
				)
			)
			, null
			, 'btn btn-default'
			, 'info'));
		$btn->setAttribute('data-toggle', 'tab');

		$tab_content->add($actDiv = new HtmlDiv(null, 'tab-pane fade in', 'tab3'));
		$actDiv->add(self::cieActsDiv($cie));

		//Categorielijst
		if($cie->getCategorie())
		{
			$pref->add($btn = new HtmlAnchor('#tab4'
				, new HtmlDiv(
					array(
						HtmlSpan::fa('recycle', _('Verwante commissies'))
						, new HtmlBreak()
						, _('Verwante commissies')
					)
				)
				, null
				, 'btn btn-default'
				, 'info'));
			$btn->setAttribute('data-toggle', 'tab');

			$tab_content->add($catDiv = new HtmlDiv(null, 'tab-pane fade in', 'tab4'));
			$catDiv->add(self::cieCatDiv($cie));
		}

		$page->add($cieInfoPaginaDiv)
			 ->end();

	}

	static public function cieInfoDiv($cie, $bestaatLogin = true)
	{
		global $auth;

		$rowDiv = new HtmlDiv();

		$rowDiv->add(new HtmlDiv(new HtmlDiv($table = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

		if(CommissieView_Generated::waardeEmailZichtbaar($cie)=='ja') {
			$table->add(self::infoTR($cie, 'Email'));
		}

		$table->add(self::infoTR($cie, 'Website'))
			  ->add(self::infoTR($cie, 'DatumBegin'))
			  ->add(self::infoTR($cie, 'DatumEind'));

		if(hasAuth('bestuur')) {
			$table->add(self::infoTR($cie, 'Login'));
		}

		if(hasAuth('bestuur') && !$bestaatLogin) {
			$rowDiv->add(HtmlAnchor::button($cie->systeemUrl() . '/GeefSysteemAccount', _('Geef een systeemaccount')));
		}

		if (hasAuth('ingelogd'))
		{
			$inKart = Kart::geef()->getCies()->bevat($cie);
			$rowDiv->add(self::makeShopLink($cie, !$inKart));
		}

		if(Persoon::getIngelogd()) {
			$fotos = MediaVerzameling::getTopratedByCie($cie, 4);

			$fotoUploadUrl = $cie->systeemUrl() . '/FotosUploaden';
			$fotoUploadText = _("Foto's invoeren");

			if(sizeof($fotos) != 0) {
				$rowDiv->add(new HtmlHeader(3, _('Foto\'s') . ($auth->hasCieAuth($cie->getCommissieID()) ? ' '.new HtmlSmall(new HtmlAnchor($fotoUploadUrl, $fotoUploadText)) : '')));
				$rowDiv->add($cieInfoFotos = new HtmlDiv());
				$cieInfoFotos->add(MediaVerzamelingView::fotoLijstCie($fotos, $cie));
			} else if($auth->hasCieAuth($cie->getCommissieID())) {
				$a = HtmlAnchor::button($fotoUploadUrl, $fotoUploadText);
				$rowDiv->add($a);
			}
		}

		return $rowDiv;
	}

	static public function cieLedenDiv($cie)
	{
		$cieLedenDiv = new HtmlDiv();

		if(CommissieView::ledenTabel($cie, 'huidige', true))
		{
			if(hasAuth('lid'))
			{
				$cieLedenDiv->add($h = new HtmlHeader(3, new HtmlAnchor("/Leden/?lidnrs=" .
					CommissieView::cieLedenNrs($cie, 'huidige'), new HtmlSpan(_("Huidige leden"), 'strongtext'))));
				$h->add(new HtmlSmall(new HtmlAnchor($cie->systeemUrl()."/Leden/Koppenblad",_("Koppenblad bekijken"))));
			} else {
				$cieLedenDiv->add(new HtmlHeader(3, new HtmlSpan(_("Huidige leden"), 'strongtext')));
			}
			$cieLedenDiv->add(CommissieView::ledenTabel($cie, 'huidige', hasAuth('lid')));
		}

		if(hasAuth('bestuur'))
		{
			$cieLedenDivP = new HtmlParagraph();
			$cieLedenDivP->add(HtmlAnchor::button("Leden/Nieuw",_("Leden toevoegen")));
			$cieLedenDiv->add($cieLedenDivP);
		}

		if(CommissieView::ledenTabel($cie, 'oud', true))
		{
			if(hasAuth('lid'))
			{
				$cieLedenDiv->add($h = new HtmlHeader(3, new HtmlAnchor("/Leden/?lidnrs=" .
					CommissieView::cieLedenNrs($cie, 'oud'), new HtmlSpan(_("Oud-leden"), 'strongtext'))));
				$h->add(new HtmlSmall(new HtmlAnchor($cie->systeemUrl()."/Leden/Koppenblad?oud=true",_("Koppenblad bekijken"))));
			} else {
				$cieLedenDiv->add(new HtmlHeader(3, new HtmlSpan(_("Oud-leden"), 'strongtext')));
			}
			$cieLedenDiv->add(CommissieView::ledenTabel($cie, 'oud', hasAuth('lid')));
		}

		return $cieLedenDiv;
	}

	static public function cieActsDiv($cie)
	{
		global $auth;

		$rowDiv = new HtmlDiv(null, 'row');
		$rowDiv->add($actDiv = new HtmlDiv(null, 'col-md-6'));

		$actDiv->add($h = new HtmlHeader(3, new HtmlSpan(_("Activiteiten"), 'strongtext')));

		if($auth->hasCieAuth($cie->getCommissieID()))
		{
			$h->add(new HtmlSmall(new HtmlAnchor("/Activiteiten/Toevoegen?Activiteit[commissies][0]=".$cie->getCommissieID(),_("Activiteit toevoegen"))));
		}

		$komendeActs = $cie->activiteitenByLogin();
		$komendeActs->union($cie->activiteitenByLogin('huidige'));
		if ($komendeActs->aantal() > 0)
		{
			$actDiv->add(CommissieView::activiteiten($cie, 'komend', $komendeActs));
		}

		$oudeActiviteiten = $cie->activiteitenByLogin('oud');
		$oudeActsView = CommissieView::activiteiten($cie, 'oud', $oudeActiviteiten);
		if($oudeActsView)
		{
			$rowDiv->add($actOudDiv = new HtmlDiv(null, 'col-md-6'));

			$actOudDiv->add(new HtmlHeader(3, new HtmlSpan(_("Oude activiteiten [" . $oudeActiviteiten->aantal() . "]"), 'strongtext')));

			$actOudDiv->add($oudeActsView);
		}

		return $rowDiv;
	}

	static public function cieCatDiv($cie)
	{
		$catDiv = new HtmlDiv();

		$ciesVerwant = $cie->getCategorie()->commissies();
		$ciesVerwant->verwijder($cie);
		$ciesVerwant->sorteer("recentst");

		$catDiv->add(new HtmlHeader(3, new HtmlSpan(_("Verwante Commissies"), 'strongtext')));

		$catDiv->add(CommissieView::commissies($ciesVerwant));

		return $catDiv;
	}

	static public function wijzigFoto(Commissie $cie)
	{
		$page = Page::getInstance()->start(sprintf(_('Wijzig foto van %s[VOC: cienaam]'), CommissieView::waardeNaam($cie)));

		$fotoForm = HtmlForm::named('fotoForm');
		$fotoForm->add(HtmlInput::makeFile("userfile"));
		$fotoForm->add(HtmlInput::makeSubmitButton(_("Upload commissiefoto")));

		$page->add(new HtmlDiv(new HtmlDiv(
				_('Hier kan je de foto voor de commissie uploaden. Let er wel op dat je foto '
				 .' maximaal 10MB mag zijn. We ondersteunen aleen png, jpeg of gif.')
				, 'panel-body')
				, 'panel panel-default')
			)
			->add($fotoForm);
		$page->end();
	}

	static public function verwijderFoto($cie)
	{
		$page = Page::getInstance()->start(sprintf(_('Verwijder foto van %s[VOC: cienaam]'), CommissieView::waardeNaam($cie)));

		$form = HtmlForm::named('fotoVerwijderen');
		$form->add(new HtmlDiv(sprintf(_("Weet je zeker dat je de foto van %s wilt verwijderen?"), CommissieView::waardeNaam($cie)), 'alert alert-warning'));
		$form->add(HtmlInput::makeSubmitButton(_("Ja")));

		$page->add($form);

		$page->end();
	}

	static public function alsArray($cie){

		$result = array("cienr" => self::waardeCommissieID($cie)
				,"cienaam" => self::waardeNaam($cie)
				,"soort" => self::waardeSoort($cie)
				,"ciecategorie" => $cie->getCategorieCategorieID()
				,"thema" => self::waardeThema($cie)
				,"omschrijving" => self::waardeOmschrijving($cie)
				,"begindatum" => self::waardeDatumBegin($cie)
				,"einddatum" => self::waardeDatumEind($cie)
				,"login" => self::waardeLogin($cie)
				,"email" => self::waardeEmail($cie)
				,"homepage" => self::waardeHomepage($cie)
				);

		return $result;

	}

	static public function overzichtLijstRegel(Commissie $cie, $inKart)
	{
		$div = new HtmlDiv(null, 'thumbnail');
		$div->setCssStyle('height: 100%');

		$commissieFoto = CommissieFoto::zoekHuidig($cie);

		if($commissieFoto && $commissieFoto->magBekijken())
		{
			$foto = $commissieFoto->getMedia();
			$url = $foto->url() . '/Medium';

			$div->add(new HtmlDiv($img = new HtmlImage($url, CommissieView::waardeNaam($cie)), 'cie-thumbnail'));
			$img->setLazyLoad();
			$img->setCssStyle('position: absolute');
		}
		else if(hasAuth('ingelogd'))
		{
			$div->add(new HtmlDiv($red = new HtmlDiv(), 'cie-thumbnail'));
			$red->setCssStyle('width: 100%; height: 100%; background-color: #97001e;');
		}

		$div->add($caption = new HtmlDiv(null, 'caption'));

		$caption->add(new HtmlHeader(4, new HtmlAnchor($cie->url(), self::waardeNaam($cie))));
		// Eigenlijk alleen een anchor als er meer is dan /Info, dus check ook op publisher: TODO.
		//$row->addData(($cie->getHomepage()?new HtmlAnchor($cie->url(),self::waardeNaam($cie)): self::waardeNaam($cie)));
		$caption->add(new HtmlParagraph(self::waardeOmschrijving($cie)));

		$caption->add($knoppenPar = new HtmlParagraph());
		$caption->add($par = new HtmlParagraph());
		$par->setCssStyle('height: 30px');
		$knoppenPar->setCssStyle('position: absolute; bottom: 0;');
		$knoppenPar->add(new HtmlAnchor("/Vereniging/Commissies/".self::waardeLogin($cie)."/Info",
				HtmlSpan::fa('info-circle', 'info', 'fa-2x')));
		if (hasAuth('ingelogd'))
		{
			$knoppenPar->add(self::makeShopLink($cie, !$inKart, true, false));
		}

		return $div;
	}

	/*
	 *  Handig voor het maken van lidverzamelingurl.
	 **/

	static public function cieLedenNrs($cie, $status = "huidige")
	{
		$nrs = array();

		$cleden = $cie->leden($status);
		foreach($cleden as $clid)
		{
			$nrs[] = $clid->geefID();
		} 
		return implode(',', $nrs);
	}

	static public function ledenLijstArray($cie, $status = "huidige", $bonus = false)
	{
		$cleden = $cie->cieleden($status);

		//Is de lijst leeg?
		if(!$cleden->first()){
		 return 'nee';
		}

		return CommissieLidVerzamelingView::lijst($cleden, $bonus);
	}

	/*
	 *	 Maakt ledenlijst
	 *	Geeft een lijst van leden, in dit geval als li lijst.
	 *	@param status mag 'oud' of 'huidige' zijn
	 *	@param cie commissie
	 *  @param bonus willlen we wel of geen bonusopties (als dat mag)
	 **/
	static public function ledenLijst($cie, $status = "huidige", $bonus = false)
	{
		$cleden = $cie->cieleden($status);
		//Is de lijst leeg?
		if(!$cleden->first()) return null;

		return CommissieLidVerzamelingView::htmlLijst($cleden, $bonus);
	}

	/*
	 *	 Maakt ledentabel
	 *	Geeft een HtmlTable van leden.
	 *	@param status mag 'oud' of 'huidige' zijn
	 *	@param cie commissie
	 *  @param bonus willlen we wel of geen bonusopties (als dat mag)
	 **/
	static public function ledenTabel($cie, $status = "huidige", $bonus = false)
	{
		$cleden = $cie->cieleden($status);
		//Is de lijst leeg?
		if(!$cleden->first()) return null;

		return CommissieLidVerzamelingView::htmlTabel($cleden, $status, $bonus);
	}

	static public function activiteiten($cie, $status = 'komend', ActiviteitVerzameling $acts)
	{
	//	$acts = $cie->activiteitenByLogin($status);
		CommissieActiviteitVerzameling::vanActiviteiten($acts); // cache

		//Is de lijst leeg?
		if ($acts->aantal() == 0)
			return;

		$acts->sorteer('beginMoment', $status == 'oud');

		return ActiviteitVerzamelingView::htmlLijst($acts);
	}

	static public function commissies($cies)
	{
		//TODO: htmlcontainerdingen
		$returnLijst = new HtmlList();

		foreach($cies as $cie)
		{
			$returnLijst->add(new HtmlListItem(CommissieView::makeLink($cie)));
		}

		return $returnLijst;
	}

	static public function kartTableRow (Commissie $cie, $prefix, $geselecteerd)
	{
		$row = new HtmlTableRow();

		$row->addData(HtmlInput::makeCheckbox($prefix . '[GeselecteerdeCie][' . $cie->geefID() . ']', $geselecteerd));
		$row->addData(self::makeLink($cie));
		$row->addData(self::waardeEmail($cie));

		return $row;
	}

	static public function defaultWaardeCommissie(Commissie $cie)
	{
		return self::makeLink($cie);
	}


	static public function commissieInformatieLabel(Commissie $cie)
	{
		switch($cie->getSoort()){
			case 'CIE':
				return 'Commissie-informatie';
				break; 
			
			case 'DISPUUT':
				return 'Dispuutinformatie';
				break;

			case 'GROEP':
				return 'Groepsinformatie';
				break;
		}
	}
	static public function makeLink(Commissie $cie, $caption = null, $naarInfo = true){
		if (!$caption) $caption = self::waardeNaam($cie);
		if($naarInfo)
			return new HtmlAnchor($cie->getUrlBase(), $caption);
		return new HtmlAnchor($cie->url(), $caption);
	}

	static public function makeShopLink (Commissie $cie, $shop, $icon = false, $button = true)
	{
		$id = $cie->geefID();
		$cieurl = $cie->url();
		$icon = $icon ? '1' : '0';

		if ($cie->magBekijken())
		{
			if ($shop)
			{
				$content = $icon ? HtmlSpan::fa('shopping-cart', _('Shop'), 'text-muted fa-2x') : _('Shop');
				$a = new HtmlAnchor($cieurl . '/Shop', $content);
				$a->setAttribute('onclick', "shopCieAjax($id, $icon, this); return false");
			}
			else
			{
				$content = $icon ? HtmlSpan::fa_stacked(array(
					HtmlSpan::fa('shopping-cart', _('Unshop'), 'fa-stack-1x text-muted fa-2x'),
					HtmlSpan::fa('ban', _('Unshop'), 'fa-stack-2x text-danger fa-2x')
				)) : _('Unshop');
				$a = new HtmlAnchor($cieurl . '/Unshop', $content);
				$a->setAttribute('onclick', "unShopCieAjax($id, $icon, this); return false");
			}
			return $a;
		}
		return new HtmlSpan($caption);
	}

	public static function latex (Commissie $cie)
	{
		$arr = self::alsArray($cie);
		return '\\cie{'.implode('}{', array_map('latexescape', array_values($arr)))."}\n";
	}

	public static function geefSysteemAccount(Commissie $cie)
	{
		$page = Page::getInstance();
		$page->start(sprintf(_('Geef %s een systeemaccount'), CommissieView::waardeNaam($cie)));

		$page->add($form = HtmlForm::named('GeefSysteemAccount'));
		$form->add(self::makeFormStringRow('login', $cie->getLogin(), _('AccountNaam')));

		$form->add(HtmlInput::makeFormSubmitButton(_('Maak aan')));
		$page->end();
	}

	public static function labelenumSoort($value)
	{
		switch ($value)
		{
			case 'CIE': return _('Commissie');
			case 'GROEP': return _('Groep');
			case 'DISPUUT': return _('Dispuut');
		}
		return parent::labelenumSoort($value);
	}
	public static function labelDatumBegin(Commissie $obj = null)
	{
		return _('Begindatum');
	}
	public static function labelDatumEind(Commissie $obj = null)
	{
		return _('Einddatum');
	}
	public static function labelEmail(Commissie $obj = null) {
		return _('Email');
	}
	public static function labelWebsite(Commissie $obj = null) {
		return _('Website');
	}

	public static function waardeWebsite(Commissie $obj) {
		return new HtmlAnchor($obj->url(), $obj->url());
	}

	/**
	 * @brief Geef een HtmlAnchor met de mailto:-url voor de commissie.
	 *
	 * @param obj De Commissie om de link voor te maken.
	 * @param caption De linktekst waar je op kan klikken. Default: e-mailadres van de cie.
	 *
	 * @return Een HtmlAnchor.
	 */
	public static function waardeEmailMailto(Commissie $obj, $caption=null) {
		if (is_null($caption))
		{
			$caption = static::waardeEmail($obj);
		}
		return new HtmlAnchor('mailto:'.$obj->getEmail(), $caption);
	}

	//Voor controller, override dingen zodat het er beter dan standaard uitziet.
// todo: gebruik de standaardform functies

	static public function opmerkingLogin()
	{
		return _('De login van de commissie of een fictief hier op lijkende login. bv: "betadag commissies 2013-2014" -> betadag1314, "Feestcommissie" -> feest.');
	}

	static public function formCategorie(Commissie $obj, $include_id = false)
	{
		// Geef een dropdownbox met behoud van de catID's en voeg een lege
		// entry toe om de categorie te kunnen verwijderen.
		return HtmlSelectbox::fromArray("Commissie[Categorie]"
					, CommissieCategorie::categorieArray() + array(null => "")
					, $obj->getCategorieCategorieID()
					);
	}

	static public function defaultForm($obj)
	{
		if($obj)
			return self::defaultFormCommissieVerzameling($obj, 'commissie', $obj->geefID(), false, true);
		else
			return self::defaultFormCommissieVerzameling($obj, 'commissie', null, false, true);
	}

   /**
    * @brief Geef het label van het veld emailZichtbaar.
    *
    * @param obj Het Commissie-object waarvoor een veldlabel nodig is.
    * @return Een string die het veld emailZichtbaar labelt.
    */
    public static function labelEmailZichtbaar(Commissie $obj)
    {
        return _('Emailadres zichtbaar?');
    }

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
