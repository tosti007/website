<?

/**
 * $Id$
 */
abstract class PersoonVerzamelingView
	extends PersoonVerzamelingView_Generated
{
	/**
	 * Toont een snelzoekformulier; hoort bij Leden_Controller::zoeken()
	 */
	static public function simpelzoeken ($leden, $str)
	{
		$container = null;
		if ($leden !== null) {
			$leden->sorteer('naam');
			$container = self::lijstToonEnkel($leden);
		}

		$page = Page::getInstance();

		if(!hasAuth('ingelogd'))
		{
			$page->start(_('Leden'));
			$page->add(new HtmlParagraph(_('Welkom op het ledenzoeksysteem van A&ndash;Eskwadraat! Als je lid bent, kun je inloggen op deze site om andere leden op te zoeken of om je eigen gegevens te wijzigen.  Ook oud-leden kunnen inloggen.')));
			$page->add(new HtmlParagraph(sprintf(_('Nog geen lid? Zie <a href="%s">waarom lid worden?</a>'), '/Leden/Lidworden')));
			$page->add(new HtmlParagraph(sprintf(_('Geen login of wachtwoord vergeten? Zie <a href="%s">login aanvragen</a>.'), '/Service/Intern/Logindata')));
			$page->end();
			return;
		}

		$form = new HtmlForm();
		$form->addClass('form-inline');
		$form->setName('PersoonVerzameling')
			->add($input_group = new HtmlDiv(null, 'input-group'))
			 ->setToken(null); //geen token voor deze post-form

		$input_group->add(new HtmlSpan(new HtmlSpan(null, 'fa fa-search'), 'input-group-addon'))
			->add(HtmlInput::makeSearch('LidVerzamelingZoeken', ($str) ? $str : '')->setAttribute('data-searchurl', '/Ajax/Persoon/Zoek'))
			->add(new HtmlDiv(HtmlInput::makeSubmitButton(_('Zoeken')), 'input-group-btn'));

		$page->addFooterJS(Page::minifiedFile('PersoonZoeken.js'))
			 ->addFooterJS(Page::minifiedFile('profielJS.js'))
			 ->start()
			 ->add(new HtmlParagraph(_("Zoek hier naar je favoriete lid!")))
			 ->add($form);
		$page->add(new HtmlForm());

		if(hasAuth('bestuur')) 
			$page->add(new HtmlParagraph(sprintf(_("Omdat je %s bent, kun je iedereen vinden. Dus bv ook niet-leden, overledenen en contactpersonen. Deze laatste worden in italic weerggeven."), hasAuth('god') ? _('god') : _('bestuurslid')), 'bs-callout bs-callout-warning'));

		$page->add(new HtmlDiv(_("De gegevens in deze database zijn bedoeld om A–Eskwadraatleden te zoeken en uitsluitend toegankelijk voor leden (en in beperkte mate oud-leden) van A–Eskwadraat. Geautomatiseerde en ongeautoriseerde toegang is verboden."), 'bs-callout bs-callout-danger'))
			 ->add(new HtmlDiv($container, null, 'Leden_zoekContainer'))
			 ->add(new HtmlParagraph(tipbox(_("Wil je specifiek op commissie of persoon zoeken? Gebruik 'cie:_naam_' of 'persoon:_naam_'!"), false)))
			 ->end();
	}

	public static function lijst (PersoonVerzameling $personen, $form = null, $geselecteerd = array(), $prefix = '', $intro = null, $dili = false)
	{
		if ($personen->aantal() == 0)
		{
			if(is_null($form))
				return new HtmlSpan(_("Geen resultaten gevonden."));
			return new HtmlSpan(_("Shop mensen in je kart om toe te voegen."));
		}

		$body = new HtmlElementCollection();

		$body->add($table = new HtmlTable(null, 'sortable'));

		$thead = $table->addHead();
		$tbody = $table->addBody();

		$trow = $thead->addRow();

		if (!is_null($form))
		{
			$checkbox = HtmlInput::makeCheckbox($prefix . '[InverteerPersonen]', True, NULL, 'InverteerPersonen');
			$checkbox->setAttribute('onclick', 'Kart_Overzicht_Inverteer_Personen()');
			$checkbox->setAttribute('title', _('(De)selecteer iedereen'));
			$trow->addData($checkbox, 'sorttable_nosort');
		}

		$trow->makeHeaderFromArray(array(_("Voornaam"),_("Tussenvoegsels"),
			_("Achternaam"),_("Telefoonnummer"),_("Email")));

		//Voor Bijnaleden is het handig als er ook een studie bij staat
		if($intro == true || $dili)
		{
			$trow->addData(_("Studie(s)"));
		}

		if (!$dili && is_null($form))
			$trow->addData(_("Winkelen"));

		$kart = Kart::geef()->getPersonen();
		foreach ($personen as $persoon)
		{
			$row = $tbody->addRow();
			if(!($persoon instanceof Lid))
				$row->setCssStyle('font-style: italic');
			elseif(!$persoon->isHuidigLid())
				$row->setCssStyle('font-style: italic');


			if(ContactPersoon::vanPersoon($persoon, "normaal") || ContactPersoon::vanPersoon($persoon, "bedrijfscontact"))
				$row->addClass("bedrijfscontact");

			if (!is_null($form))
				$row->addData(HtmlInput::makeCheckbox($prefix . '[Geselecteerde][' . $persoon->geefID() . ']', in_array($persoon->geefID(), $geselecteerd)));

			//De voornaam krijgt de link, dus mag niet leeg zijn!
			$voornaam = PersoonView::waardeVoornaam($persoon);
			if(empty($voornaam))
				$voornaam = PersoonView::waardeVoorletters($persoon);

			//Voor Introdeelnemers is de links anders
			if($intro == true)
			{   //Leden die een extra/nieuwe studie gaan doen en al lid zijn, hebben vaak geen groepje nodig
				if($persoon->getLidToestand() == 'BIJNALID')
				{
					$row->addData(IntroDeelnemerView::makeLink($persoon, $voornaam));
				}
				else
				{
					$row->addData(PersoonView::makeLink($persoon, $voornaam).' (Is al lid!!)');
				}
			}
			else if($dili)
			{
				$row->addData( DiliView::makeLink($persoon));
			}
			else
			{
				$row->addData(PersoonView::makeLink($persoon, $voornaam));
			}
			$row->addData(PersoonView::waardeTussenvoegsels($persoon));
			$row->addData(PersoonView::waardeAchternaam($persoon));
			$row->addData(ContactTelnrView::waardeTelefoonnummer($persoon->getPrefTelnr()));
			$row->addData(PersoonView::waardeEmail($persoon));

			//Voor Bijnaleden is het handig als er ook een studie bij staat
			if($intro == true || $dili)
			{
				$row->addData(LidStudieVerzamelingView::simpel($persoon->getStudies()));
			}

			if(!$dili && is_null($form) && $persoon->magBekijken())
			{
				if($kart->bevat($persoon))
				{
					$row->addData($shoplink = PersoonView::makeLink($persoon, 'UnShop', null, 2));
					$shoplink->setAttribute('onclick',"unShopLidAjax(" . $persoon->geefID() . ", this); return false");
				}
				else
				{
					$row->addData($shoplink = PersoonView::makeLink($persoon, 'Shop', null, 1));
					$shoplink->setAttribute('onclick',"shopLidAjax(" . $persoon->geefID() . ", this); return false");
				}
			}
		}
		return $body;
	}

	public static function lijstToonEnkel(PersoonVerzameling $personen, $form = null, $geselecteerd = array(), $prefix = '')
	{
		$return = new HtmlElementCollection();
		$return->add(self::lijst($personen, $form, $geselecteerd, $prefix));
		if($personen->aantal() == 1)
		{
			//Als het een lid is gaan we verder met een lid object.
			$persoonid = $personen->first()->geefID();
			if(Lid::geef($persoonid)){
				$figuur = Lid::geef($persoonid);
			} else {
				$figuur = Persoon::geef($persoonid);
			}

			if($figuur->magBekijken())
			{
				$view = $figuur->getView();
				$div = new HtmlDiv();
				$div->setId('leden')->add($view::details($figuur, true));
				$return->add($div);
			}
		}
		return $return;
	}

	public static function kort (PersoonVerzameling $personen)
	{
		$namen = array();
		foreach ($personen as $persoon)
			$namen[] = PersoonView::makeLink($persoon);
		return implode(', ', $namen);
	}

	/**
	 *  Maak een HtmlDiv met klasse "koppenlijst" waar een koppenblad in terecht komt.
	 *
	 * @param personen De PersoonVerzameling (of andere iterable van personen) waarvan je het koppenblad wilt zien
	 * @param naamZichtbaar Bool of de namen bij het koppenblad zichtbaar zijn
	 * @param printbaar Bool of het koppenblad in printbaar formaat moet komen
	 *
	 * @return Een HtmlDiv met het gewenste koppenblad erin.
	 */
	public static function koppen($personen, $naamZichtbaar = true, $printbaar = false)
	{
		$personen->sorteer('Naam');

		// plek in de row
		$x = 0;

		if($printbaar)
		{
			$koppenblad = new HtmlTable();

			foreach ($personen as $pers) {
				// aan nieuwe rij begonnen
				if ($x % 6 == 0) {
					$row = $koppenblad->addRow();
				}

				$dif = PersoonView::kop($pers, $naamZichtbaar, $printbaar);
				$row->addData($dif);
				$x = ($x + 1);
			}
		}
		else
		{
			$koppenblad = $rows = new HtmlDiv(null, 'container-fluid koppenlijst');

			foreach ($personen as $pers) {
				// aan nieuwe rij begonnen
				if ($x % 6 == 0) {
					$koppen = new HtmlDiv(null, 'row');
					$rows->add($koppen);
				}

				if ($x % 2 == 0) {
					$koppenCol = new HtmlDiv($koppen = new HtmlDiv(null, 'row'), 'col-sm-4');
					$rows->add($koppenCol);
				}

				$dif = PersoonView::kop($pers, $naamZichtbaar, $printbaar);
				$koppen->add($dif->addClass('col-xs-6'));
				$x = ($x + 1);
			}
		}

		return $koppenblad;
	}

	/**
	 *  Maak een HtmlDiv met klasse "koppenlijst" waar een printbaar koppenblad in terecht komt.
	 * TODO voor MSO'ers: gebruik een of ander handig programmeerpatroon om dit samen te voegen met koppen()
	 * @param personen De PersoonVerzameling (of andere iterable van personen) waarvan je het koppenblad wilt zien
	 * @param namenZichtbaar Zo nee, zijn de namen verborgen (handig voor BMW'ers die namen moeten oefenen!)
	 * @return Een HtmlDiv met het gewenste koppenblad erin.
	 */
	public static function koppenPrintbaar($personen, $namenZichtbaar=true) {
		$personen->sorteer('Naam');

		// plek in de row
		$x = 0;

		$koppenblad = new HtmlDiv(null, 'container-fluid koppenlijst');
		$koppenblad->add($rows = new HtmlTable());

		foreach ($personen as $pers) {
			// aan nieuwe rij begonnen
			if ($x % 5 == 0) {
				$koppen = new HtmlTableRow();
				$rows->add($koppen);
			}

			$dif = PersoonView::kop($pers, $namenZichtbaar);
			$koppen->addData($dif);
			$x++;
		}
		return $koppenblad;
	}

	public static function latex($personen)
	{
		$latexarray = array();
		foreach ($personen as $pers) {
			 $latexarray[] = PersoonView::latex($pers);
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		fwrite($output, "%voornaam voorletter achternaam telnr1 email straat+huisno postcode+plaats kixcode\n");
		foreach($latexarray as $latexline)
		{
			fwrite($output, $latexline);
		}

		fclose($output);

	}

	/**
	 * Geef de latexcode geschikt voor het BMW-koppenblad van actieve leden.
	 * Output alles direct naar de browser voor downloaden.
	 * 
	 * Vanwege de fantastische documentatie van het originele script,
	 * (i.e. de eerste zin van deze comment, en "Dus.")
	 * kan ik de rest van het proces niet doorvertellen.
	 * @param personen De verzameling van personen waar het BMW-koppenblad van moet zijn.
	 */
	public static function bmwlatex($personen)
	{
		$latexarray = array();
		foreach ($personen as $pers) {
			$latexarray[] = "\bmwkop" .
					"{" . latexescape($pers->getVoornaam()) . "}" .
					"{" . latexescape($pers->getTussenvoegsels() . " " . $pers->getAchternaam()) . "}" .
					"{" . $pers->geefID() . "}" .
					"{" . $pers->getFotoNaam() . "}" .
					"\n";
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		fwrite($output, "% voornaam   achternaam   lidnr   foto\n");
		foreach($latexarray as $latexline)
		{
			fwrite($output, $latexline);
		}

		fclose($output);

	}

	// CSV view van lijst personen, met bonus wordt er extra informatie gegeven.
	// Dit ten behoeven van bv de almanak
	static public function csv($pers, $bonus = false){

		$csvarray = array();
		foreach($pers as $per){
			if($bonus){
				$csvarray[] = PersoonView::alsBonusCSVArray($per);
			} else {
				$csvarray[] = PersoonView::alsCSVArray($per);
			}
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		if(count($csvarray) > 0){
			fputcsv($output, array_keys($csvarray[0]));
		} else {
			fwrite($output, _("Geen data."));
		}

		//Print alle inhoud voor de CSV
		foreach($csvarray as $per){
			fputcsv($output, $per);
		}

		fclose($output);

	}

	public static function etiketten($personen)
	{
		//Postscript etiketten
		user_error('501 Not Implemented', E_USER_ERROR);
	}

	public static function telefoonlijst($personen)
	{
		user_error('501 Not Implemented', E_USER_ERROR);
	}

	/**
	 *  Het uitgebreide zoek form waarmee hele groepen van personen kunnen
	 *  worden gevonden. Dit form werkt met get zodat javascript niet nodig is.
	 *  Er zitten geen tokens in daar dit het form een stuk lastiger maakt en
	 *  het in dit geval geen cross site scripting aanvallen kan voorkomen: ze
	 *  zijn niet uberhaupt niet nodig aangezien dit form alleen maar data
	 *  ophaalt.
	 *  
	 *  Het form maakt gebruik van een paar hulpfuncties die er o.a. voor zorgen
	 *  dat geposte info wordt gebruikt waar mogelijk en om een index te maken
	 *  voor grote verzamelingen, zonder javascript.
	 **/
	public static function uitgebreidzoeken()
	{
		$content = new HtmlDiv();

		//Maak een get form aan zonder tokens
		$form = new HtmlForm('get');
		//$form->addClass('form-inline');

		$formtable = new HtmlDiv();

		/* TODO, dit moet kan/moet ooit nog eens worden toegevoegd.
		$formtable->add(new HtmlTableRow(array(
			new HtmlTableDataCell(_("Persoonsgegevens: ")),
			new HtmlTableDataCell(HtmlInput::makeText('simpelzoeken','!TODO!')),
			new HtmlTableDataCell(_("Hiermee kan je zoeken in de groep"))
			)));
		*/

		$formtable->add(self::makeFormEnumRow('groep', PersoonVerzameling::groeperingsMogelijkhedenArray(), tryPar('groep'), _('Groep')));

		/**
		 *  Vanaf hier de verschillende criteria die ook op de groepen kunnen worden losgelaten.
		 */

		$formtable->add(new HtmlHeader(3, _("Lid-toestand")));

		$formtable->add(self::makeFormBoolRow('lid', tryPar('lid', false), _("Lid")))
				  ->add(self::makeFormBoolRow('oudlid', tryPar('oudlid', false), _("Oud-lid")))
				  ->add(self::makeFormBoolRow('boekenkoper', tryPar('boekenkoper', false), _("Boekenkoper")))
				  ->add(self::makeFormBoolRow('bijnalid', tryPar('bijnalid', false), _("Bijna-lid")));

		$formtable->add(new HtmlHeader(3, _("Studiefase")));

		$formtable->add(self::makeFormBoolRow('bachelor', tryPar('bachelor', false), _("Bachelor")))
				  ->add(self::makeFormBoolRow('master', tryPar('master', false), _("Master**")))
				  ->add(self::makeFormBoolRow('bal', tryPar('bal', false), _("BAL")))
				  ->add(self::makeFormBoolRow('exchange', tryPar('exchange', false), _("Exchange")));

		$formtable->add(new HtmlHeader(3, _("Studie")));

		$formtable->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeCheckbox("ica")->setValueFromForm("on") . "(" . HtmlInput::makeCheckbox("icaactief")->setValueFromForm("off") .")* Informatica", 'col-sm-offset-2 col-sm-10'), 'form-group'))
				  ->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeCheckbox("gt")->setValueFromForm("on") . "(" . HtmlInput::makeCheckbox("gtactief")->setValueFromForm("off") .")* Gametechnologie", 'col-sm-offset-2 col-sm-10'), 'form-group'))
				  ->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeCheckbox("iku")->setValueFromForm("on") . "(" . HtmlInput::makeCheckbox("ikuactief")->setValueFromForm("off") .")* Informatiekunde", 'col-sm-offset-2 col-sm-10'), 'form-group'))
				  ->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeCheckbox("na")->setValueFromForm("on") . "(" . HtmlInput::makeCheckbox("naactief")->setValueFromForm("off") .")* Natuurkunde", 'col-sm-offset-2 col-sm-10'), 'form-group'))
				  ->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeCheckbox("wis")->setValueFromForm("on") . "(" . HtmlInput::makeCheckbox("wisactief")->setValueFromForm("off") .")* Wiskunde", 'col-sm-offset-2 col-sm-10'), 'form-group'))
				  ->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeCheckbox("wit")->setValueFromForm("on") . "(" . HtmlInput::makeCheckbox("witactief")->setValueFromForm("off") .")* Wiskunden en Toepassingen", 'col-sm-offset-2 col-sm-10'), 'form-group'));

		$formtable->add(new HtmlHR());

		$formtable->add(self::makeFormStringRow('lidvan', tryPar('lidvan'), _("Lid geworden in jaar")));

		//Geslacht is raar en een spook.
		//De geslacht enum heeft een vierde spookoptie die uitgebreid zoeken sloopt en op een of andere manier ook de default optie is.
		//gooi deze dus eerst weg
		$geslachtarray = array('0' => null) + PersoonView::labelenumGeslachtArray();
		unset($geslachtarray['']);

		$formtable->add(self::makeFormEnumRow('geslacht', $geslachtarray, tryPar('geslacht'), _("Geslacht")));

		$formtable->add(self::makeFormBoolRow('bounces', tryPar('bounces'), _("Alleen met bounces***")));

		$formtable->add(self::makeFormEnumRow('sortering', PersoonVerzameling::sorteerMogelijkheden(), tryPar('sortering'), _("Sorteer op")));

		$formtable->add(self::makeFormBoolRow('aflopend', tryPar('aflopend'), _("Sorteer aflopend")));

		$formtable->add(self::makeFormEnumRow('output', array( "html"  => "On screen html",
			"csv"   => "CSV",
			"csv++"   => "CSV++",
			"latex" => "LaTeX"
		), tryPar('output'), _("Outputformaat")));

		// Submit button
		$formtable->add(HtmlInput::makeFormSubmitButton('Zoeken', 'uitgebreidzoeken'));

		$form->add($formtable);

		$informatie = new HtmlDiv();

		$content->add($form)
				->add(new HtmlHR())
				->add($informatie);

		return $content;
	}

	//Genereer tabel met jarigen in een maand
	public static function jarigPerMaandTabel($maandNr, $maandNaam, $jarigen)
	{
		global $MAANDEN;

		$body = new HtmlElementCollection();

		//bouw maanden-navigatie-menu
		$maandselect = new HtmlParagraph();
		foreach($MAANDEN as $nr => $mnd )
		{
			if ($maandNr == $nr )
				$maandselect->add('[' . substr($mnd,0,3) . "]\n");
			else
				$maandselect->add(new HtmlAnchor('/Leden/Verjaardagen?maand=' . ucfirst($MAANDEN[$nr]), '[' . substr($mnd,0,3) . ']'));
		}

		$body->add($maandselect)
			 ->add(new HtmlHeader(2, $maandNaam));

		$prevdag = '-1';
		$table1 = array();
		$table2 = array();
		$table3 = array();
		$i = 1;
		$curtable = 1;
		foreach($jarigen as $persoon)
		{
			$dag = $persoon->getDatumGeboorte()->format('d');
			$jaar = $persoon->getDatumGeboorte()->format('Y');
			if($dag != $prevdag)
			{
				$tr = array(array($dag, $a = new HtmlAnchor(null)), PersoonView::makeLink($persoon, PersoonView::naam($persoon)) . " ($jaar)");
				$a->setName('dag' . $dag);
			}
			else
			{
				$tr = array("", PersoonView::makeLink($persoon, PersoonView::naam($persoon)) . " ($jaar)");
			}
			if($dag == $prevdag) {
				if($curtable == 1)
					$table1[] = $tr;
				elseif($curtable == 2)
					$table2[] = $tr;
				elseif($curtable == 3)
					$table3[] = $tr;
			} else {
				if($i < $jarigen->aantal()/3)
				{
					$table1[] = $tr;
					$curtable = 1;
				}
				elseif($i < (2*$jarigen->aantal())/3)
				{
					$table2[] = $tr;
					$curtable = 2;
				}
				else
				{
					$table3[] = $tr;
					$curtable = 3;
				}
			}
			$prevdag = $dag;
			$i++;
		}

		$body->add($div = new HtmlDiv(null, 'row'));
		$div->add(new HtmlDiv(HtmlTable::fromArray($table1), 'col-md-4'));
		$div->add(new HtmlDiv(HtmlTable::fromArray($table2), 'col-md-4'));
		$div->add(new HtmlDiv(HtmlTable::fromArray($table3), 'col-md-4'));

		//Tip van de WebCie!
		if ( hasAuth('actief') )
		{
			$body->add(tipbox(sprintf(
			_('Automatisch een mailtje krijgen als er actieve leden jarig zijn? Abonneer je op %sjarig%s.'),
			'<a href="http://lists.A-Eskwadraat.nl/mailman/listinfo/jarig">',
			'</a>'), false));
		}

		$body->add($maandselect);

		return $body;
	}

	/**
	 *  Maak een tabel met naamfixsuggesties en een form om ze door te voeren.
	 **/

	public static function naamFixTabel(PersoonVerzameling $personen)
	{
		$div = new HtmlDiv();
		$form = HtmlForm::named("naamFix");
		$table = new HtmlTable();
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("Voornaam"), _("Voorletters"), _("Tussenvoegsels"),
			_("Achternaam"), _("->"), _("Voornaam"), _("Voorletters"), _("Tussenvoegsels"),
			_("Achternaam"), _("Doorvoeren"), ""));

		foreach($personen as $persoon){
			$voorstelnaam = Persoon::naamsuggestie($persoon);

			$row = $tbody->addRow();
			$row->addData(PersoonView::waardeVoornaam($persoon));
			$row->addData(PersoonView::waardeVoorletters($persoon));
			$row->addData(PersoonView::waardeTussenvoegsels($persoon));
			$row->addData(PersoonView::waardeAchternaam($persoon));
			$row->addData("->");
			$row->addData($voorstelnaam['voornaam']);
			$row->addData($voorstelnaam['voorletters']);
			$row->addData($voorstelnaam['tussenvoegsels']);
			$row->addData($voorstelnaam['achternaam']);
			$row->addData(HtmlInput::makeCheckbox($persoon->geefID()));
			$row->addData(new HtmlAnchor($persoon->url() . "/Wijzig",
				HtmlSpan::fa('pencil', _('Wijzigen'))));
		}

		$form->add($table)
			->add(HtmlInput::makeSubmitButton("Doorvoeren"));
		$div->add($form);

		return $div;
	}

	public static function naamMetLidNrArray($personen)
	{
		$array = array();
		foreach($personen as $persoon){
			$array[$persoon->getContactID()] = PersoonView::naam($persoon) . ' (' . $persoon->geefID() . ')';
		}
		return $array;

	}

	public static function htmlSelectBox($obj, PersoonVerzameling $personen, $veld, $selected = array(), $multiple = false, $emptyoption = false) {
		if($selected instanceof PersoonVerzameling)
			$selected = $selected->keys();

		$options = array();

		if($emptyoption)
			$options[0] = "";

		foreach($personen as $persoon)
		{
			$options[$persoon->geefID()] = PersoonView::naam($persoon);
		}

		$name = self::formveld($obj, $veld);
		$sel = HtmlSelectbox::fromArray($name, $options, $selected);
		if($multiple) {
			$sel->setMultiple();
		} else {
			$sel->setSize(1);
		}
		return $sel;
	}

	//Selectbox voor de getagte leden van fotoweb
	public static function maakPersonenSelect($personenids) {
		$select = array();
		foreach(array_filter($personenids) as $p) {
			$select[$p] = PersoonView::naam(Persoon::geef($p));
		}

		$selected = tryPar('personen',array());

		$box = HtmlSelectbox::fromArray('personen',$select,$selected,5);
		$box->setMultiple();
		return $box;
	}

	public static function simpel($personen)
	{
		$result = '';

		foreach($personen as $persoon){
			if(!empty($result)) $result .= ', ';
			$result .= PersoonView::naam($persoon);
		}
		return $result;
	}

	static public function achievementsAantal()
	{
		global $BADGES, $CACHING;

		$aantallen = PersoonBadgeVerzameling::geefAlleAantallenVanBadges();

		$page = Page::getInstance()->start();

		$page->add(new HtmlDiv(_("De A-Eskwadraatwebsite heeft achievements "
			. "voor iedereen die ingelogd is. Er zijn verschillende soorten "
			. "achievements te halen. Kijk vooral rond om te zien hoe je de "
			. "verschillende achievements haalt."), 'alert alert-info'));
		$page->add(new HtmlDiv(new HtmlDiv(sprintf(_("Hier zie je per achievement hoe "
			. "vaak die gehaald is. Wil je zelf weten hoeveel achievements je "
			. "gehaald hebt? Kijk dan %s."),
				new HtmlAnchor(
					'/Leden/' . Persoon::getIngelogd()->geefID() . '/Achievements',
					_("hier"))), 'panel-body'), 'panel panel-deafult'));

		// laat wat debug-info zien: wat je precies moet doen
		// bijvoorbeeld wat telt als intro-cie
		if (hasAuth('bestuur')) {
			$page->add(new HtmlHeader(3, _("Specifieke eisen")));

			// loop alle verschillende achievements op cie-basis af
			$alleCies = CommissieVerzameling::allemaal();
			$page->add(new HtmlParagraph(_("De volgende achievements krijg je "
				. "als je in een van de genoemde commissies zit:")));
			
			$page->add($paraIba = new HtmlParagraph($BADGES['onetime']['iba']['titel'] . ": "));
			$page->add($paraBestuur = new HtmlParagraph($BADGES['onetime']['bestuur']['titel'] . ": "));
			$page->add($paraEc = new HtmlParagraph($BADGES['onetime']['ec']['titel'] . ": "));
			$page->add($paraAr = new HtmlParagraph($BADGES['onetime']['ar']['titel'] . ": "));
			$page->add($paraAlmanak = new HtmlParagraph($BADGES['onetime']['almanak']['titel'] . ": "));
			$page->add($paraReis = new HtmlParagraph($BADGES['onetime']['reis']['titel'] . ": "));
			$page->add($paraIntro = new HtmlParagraph($BADGES['onetime']['intro']['titel'] . ": "));
			$page->add($paraSympo = new HtmlParagraph($BADGES['onetime']['sympo']['titel'] . ": "));
			$page->add($paraSupermentor = new HtmlParagraph($BADGES['onetime']['supermentor']['titel'] . ": "));
			$page->add($paraMedezeggenschap = new HtmlParagraph($BADGES['onetime']['medezeggenschap']['titel'] . ": "));
			$page->add($paraBier = new HtmlParagraph($BADGES['onetime']['bier']['titel'] . ": "));
	
			foreach ($alleCies as $cie) {
				if (PersoonBadge::checkCie($cie, 12)) {
					$paraIba->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 1)) {
					$paraBestuur->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 7)) {
					$paraEc->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 18)) {
					$paraAr->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 4)) {
					$paraAlmanak->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 6)) {
					$paraReis->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 8)) {
					$paraIntro->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 9)) {
					$paraSympo->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 19)) {
					$paraSupermentor->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 24)) {
					$paraMedezeggenschap->add(CommissieView::makeLink($cie));
				} elseif (PersoonBadge::checkCie($cie, 25)) {
					$paraBier->add(CommissieView::makeLink($cie));
				}
			}
		}

		$page->add(new HtmlHeader(3, _("Eenmalige achievements")));
		$page->add($ottable = new HtmlTable());
		$i = 0;

		foreach($BADGES['onetime'] as $key => $badge)
		{
			// verberg schaduwachievements als je die nog niet hebt
			if (!hasAuth('god') && !Persoon::getIngelogd()->hasBadge($key) && $badge['schaduw']) {
				continue;
			}
			if($i % 6 == 0) {
				$ottable->add($row = new HtmlTableRow());
			}
			$row->addData($div = new HtmlDiv())
				->setCssStyle("width: 16.67%; text-align: center; border: 1px solid #ddd !important;");

			$aantal = $aantallen[$key];

			$div->add(new HtmlDiv(PersoonBadgeView::badgeNaam($badge), 'bold'));
			$div->add($aantalDiv = new HtmlDiv(new HtmlAnchor('?details=' . $key, $aantal)));
			$aantalDiv->setCssStyle('font-size: 40px;');
			$div->add(new HtmlDiv($badge['omschrijving']));
			$i++;
		}

		$page->add(new HtmlHeader(3, _("Medailleachievements")));

		$page->add($table = new HtmlTable());
		$table->setCSSStyle("width: 99%"); // fix autismepixel

		$table->add($head = new HtmlTableHead());
		$table->add($body = new HtmlTableBody());

		$head->add($row = new HtmlTableRow());
		$row->addHeader(_("Achievement"));
		$row->addHeader(_("Brons"))->setCssStyle("text-align: center;");
		$row->addHeader(_("Zilver"))->setCssStyle("text-align: center;");
		$row->addHeader(_("Goud"))->setCssStyle("text-align: center;");
		$row->addHeader(aesnaam())->setCssStyle("text-align: center;");

		foreach($BADGES['bsgp'] as $key => $badge)
		{
			if($badge['multi'] == 'short') {
				$multi = array(1, 2, 3, 4);
			} else {
				$multi = array(1, 2, 5, 10);
			}
			$body->add($row = new HtmlTableRow());
			$row->addData($badge['omschrijving'])
				->setCssStyle('vertical-align: middle; font-weight: bold;');

			foreach($multi as $k => $m)
			{
				$m = $badge['value'] * $m;

				$aantal = $aantallen[$key.'_'.$m];

				$row->addData($aantalDiv = new HtmlDiv(new HtmlAnchor('?details=' . $key . '&level=' . $k, $aantal)))
					->setCssStyle("text-align: center; border: 1px solid #ddd !important;");
				$aantalDiv->setCssStyle('font-size: 40px;');
			}
		}

		$page->end();
	}

	static public function achievementsAantalDetails($badgeNaam, $onetime, $level = 0)
	{
		global $BADGES;

		$personen = PersoonBadgeVerzameling::geefAllePersonenMetBadge($badgeNaam);
		$personen->sorteer('naam');

		if($onetime) {
			$badge = $BADGES['onetime'][$badgeNaam];
		} else {
			$badge = $BADGES['bsgp'][$badgeNaam];
			if($badge['multi'] == 'short') {
				$multi = array(1, 2, 3, 4);
			} else {
				$multi = array(1, 2, 5, 10);
			}
			$badgeNaam = $badgeNaam . "_" . ($multi[$level] * $badge['value']);
		}

		$page = Page::getInstance()
			->start(sprintf(_("Details van badge %s")
					, ($onetime)
						? $badge['titel']
						: $badge['omschrijving']
					)
				);

		$page->add(HtmlAnchor::button('/Leden/Achievements', _("Terug naar alle achievements")));

		if($badgeNaam == 'virus')
		{
			$page->add(new HtmlHeader(3, _('Aantal keer behaald door de tijd')));
			$handle = fopen('/var/log/www/virus-achievement.log', 'r');
			if ($handle)
			{
				$results = array();
				while (($line = fgets($handle)) !== false)
				{
					$str_exp = explode(' ', $line);
					$results["$str_exp[0] $str_exp[1]"] = $str_exp[2];
				}

				fclose($handle);

				$min_date = strtotime(key($results));
				end($results);
				$max_date = strtotime(key($results));

				$dates = range($min_date, $max_date, ($max_date - $min_date)/49);

				foreach($dates as $date)
					$aantal[$date] = 0;

				$nodes = array();
				$edges = array();

				foreach($results as $d => $res)
				{
					$from_to = explode('->', trim(preg_replace('/\s+/', ' ', $res)));

					if(!in_array($from_to[0], $nodes))
						$nodes[] = $from_to[0];
					if(!in_array($from_to[1], $nodes))
						$nodes[] = $from_to[1];

					if(!array_key_exists($from_to[0], $edges))
						$edges[$from_to[0]] = array();
					$edges[$from_to[0]][] = $from_to[1];

					$modified = strtotime($d);
					foreach($dates as $date)
					{
						if($date < $modified)
							continue;

						$aantal[$date]++;
					}
				}

				$labels = array();
				foreach(array_keys($aantal) as $key) {
					$key = new DateTimeLocale($key);
					$labels[] = $key->format("Y-m-d");
				}

				$node_str = '';
				foreach($nodes as $n)
				{
	  				$node_str .= '{ "id": "'.$n.'", "name": "'.$n.'", "group": 1 },'."\n";
				}

				$edges_str = '';
				foreach($edges as $key => $to)
				{
					foreach($to as $t)
					{
						$edges_str .= '{ "source": "'.$key.'", "target": "'.$t.'", "value": 4},'."\n";
					}
				}

				$page->addFooterJS(Page::minifiedFile('Chart.js', 'js'));
				$page->add(maakGrafiek($labels, array('Aantal' => array_values($aantal))));
			}
		}

		$page->add(new HtmlHeader(4, _("Mensen die deze achievement hebben")));

		$page->add($list = new HtmlList());

		foreach($personen as $pers) {
			if($pers->hasBadge($badgeNaam)) {
				$list->addChild(PersoonView::makeLink($pers));
			}
		}

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
