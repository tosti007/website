<?

/**
 * $Id$
 */
abstract class CommissieCategorieView
	extends CommissieCategorieView_Generated
{
	public static function makeLink(CommissieCategorie $cieCat)
	{
		return new HtmlAnchor($cieCat->url(), CommissieCategorieView::waardeNaam($cieCat));
	}

	public static function waardeCommissies(CommissieCategorie $cieCat)
	{
		$ul = new HtmlList();

		$cies = $cieCat->getCommissies(tryPar('oud', false));

		foreach($cies as $cie)
		{
			$ul->addChild(CommissieView::makeLink($cie));
		}

		return $ul;
	}

	public static function wijzig(CommissieCategorie $cieCat, $show_error = false, $nieuw = false)
	{
		$page = Page::getInstance();
		if($nieuw)
			$page->start(_('Nieuwe categorie'));
		else
			$page->start(_('Categorie wijzigen'));

		$page->add($form = HtmlForm::named('Categorie'));
		$form->add(self::createForm($cieCat, $show_error));
		$form->add(HtmlInput::makeFormSubmitButton(_('Doe')));

		$page->end();
	}

	public static function categorie(CommissieCategorie $cieCat)
	{
		$page = Page::getInstance();

		if($cieCat->magWijzigen()) {
			$page->setEditUrl($cieCat->url() . 'Wijzigen');
		}
		if($cieCat->magVerwijderen()) {
			$page->setDeleteUrl($cieCat->url() . 'Verwijderen');
		}

		$page->start(CommissieCategorieView::waardeNaam($cieCat));

		$page->add(self::viewInfo($cieCat));

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
