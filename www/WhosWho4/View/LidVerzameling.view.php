<?

/**
 * $Id$
 */
abstract class LidVerzamelingView
	extends LidVerzamelingView_Generated
{

	static public function maakLedenAf($leden)
	{
		$page = new HtmlDiv();

		if ($leden->aantal() <= 0) {
			$page->add(new HtmlHeader(3, "Kies mensen om lid-af te maken:"));
			$page->add($selectieForm = HtmlForm::named('afmaakSelectie'));
			$selectieForm->addClass('form-inline');
			$selectieForm->add(View::defaultFormPersoonVerzameling());
			$selectieForm->add(HtmlInput::makeSubmitButton("Selecteer deze mensen!"));
			return $page;
		}

		if($leden->aantal() > 300) {
			$page->add(new HtmlDiv(new HtmlParagraph(_("Er zijn meer dan 300 leden om af te maken, dat kan ik niet aan! Ik verwerk nu slechts de eerste 300.")), 'alert alert-danger'));
		}

		$page->add($lidForm = HtmlForm::named('maakLidAf'));

		//Sorteren gaat goed zolang mensen geen 2 studies hebben, dus nog een TODO voor later.
		$lidFormTable = new HtmlTable(null, 'sortable');
		$lidFormTable->add(new HtmlTableHead(new HtmlTableRow(array(new HtmlTableHeaderCell(),
						new HtmlTableHeaderCell(_("Lid")),
						new HtmlTableHeaderCell(_("Studie")),
						new HtmlTableHeaderCell(_("Status")),
						new HtmlTableHeaderCell(_("Opmerkingen")),
						))));

		$teller = 0;
		//Laat alle info van alle leden in de selectie zien
		foreach($leden as $lid){
			$teller++;
			if($teller > 300)
				break;
			$lidStudies = $lid->getStudies();
			$aantalRows = max(1, $lidStudies->aantal());
			foreach (LidView::lidStudieTRsWijzig($lid, true) as $tr) {
				$lidFormTable->add($tr);
			}
		}

		$lidForm->add($lidFormTable);

		$datum = (coljaar()+1)."-08-31";

		$lidForm->add(new HtmlParagraph(array(_('Datum van lidaf'), HtmlInput::makeDate('lidafdatum', $datum))));

		//Geef dit mee zodat we bij de verwerking deze makkelijk na kunnen lopen.
		$lidForm->add(HtmlInput::makeHidden('lidnrs', implode(',',$leden->keys())));

		$lidForm->add(HtmlInput::makeSubmitButton(_("Voer wijzigingen door")));

		$page->add(new HtmlParagraph(array(
					new HtmlHeader(3,_("Uitleg")),
					_(	"Met de <i>geselecteerde leden</i> gebeurt het volgende:"),
					new HtmlList(false, array(
						new HtmlListItem(_("De studie-info wordt geüpdatet: studiestatus aanpassingen zonder einddatum
								krijgen einddatum 'onbekend' en data in de toekomst kunnen ook.")),
						new HtmlListItem(_("Ze krijgen de status oudlid per ingevulde datum.")),
						new HtmlListItem(_("Ze ontvangen een e-mail hierover.")))))))
				->add(new HtmlParagraph(array(
					new HtmlHeader(4,_("Uitzonderingen")),
					_(	"Het lid krijgt geen einddatum indien het lid nog een actieve
					studie heeft <i>of</i> het lid heeft al een einddatum, dan wordt de oude
					gehandhaafd."))))
				->add(new HtmlParagraph(array(
					new HtmlHeader(3,_("Legenda")),
					_("grijze achtergrond: Dit lid studeert deze studie niet actief."))));

		return $page;
	}

	static public function actieveJarigen($leden, $limiet = 13,$koppen = 0)
	{
		$page = new HtmlDiv();
		$form = HtmlForm::named('jarigLimiet');
		$form->addClass('form-inline');
		$form->add(_('De volgende leden zijn jarig binnen nu en '))
					->add(HtmlInput::makeNumber('limiet',$limiet))
					->add(_('dagen!'))
					->add(HtmlInput::makeSubmitButton(_("Verander het aantal dagen!"),'submit_limiet'));
		$form->add(new HtmlBreak())
			 ->add(new HtmlSpan("De weergave:"))
		     ->add(HtmlSelectbox::fromArray('koppen', array(
                '0' => _("Lijstje"),
				'1' => _("Koppenblad")
				), (int) $koppen, 0, true));
		$form->add(new HtmlBreak());
		$form->add(new HtmlBreak());
		if($koppen == 1)
		{
			$form->add(PersoonVerzamelingView::koppen($leden));
		}
		else
		{
			$table = new HtmlTable();
			$thead = $table->addHead();
			$tbody = $table->addBody();

			$row = $thead->addRow();
			$row->addHeader(_("Geboortedatum"));
			$row->addHeader(_("Naam"));
			$row->addHeader(_("Adres"));
			$row->addHeader(_("Commissies"));

			foreach($leden as $lid){
				$commissieString = CommissieVerzamelingView::kommaLijst(CommissieVerzameling::vanPersoon($lid));

				$row = $tbody->addRow();
				$row->addData(LidView::waardeDatumGeboorte($lid));
				$row->addData(PersoonView::makeLink($lid));
				$row->addData(ContactAdresView::adresString($lid,'THUIS'));
				$row->addData($commissieString);
			}

			$form->add($table);

			$form->add('Kart:')
				->add(HtmlInput::makeSubmitButton(_('Shop ze er allemaal in!'),'submit_kart'));
		}

		$page->add($form);
		return $page;
	}

	static public function geluksvogels($geluksvogels, $printable = false)
	{
		$div = new HtmlDiv();

		$form = new HtmlForm('GET');

		$form->add(new HtmlSpan(_("De geluksvogels op de datum:")));
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeDate('datum'));
		$form->add(HtmlInput::makeSubmitButton(_("ga!")));

		$bijnaleden = new LidVerzameling();
		$leden = new LidVerzameling();
		foreach($geluksvogels as $lid)
		{
			if($lid->getLidToestand() == "BIJNALID")
			{
				$bijnaleden->voegtoe($lid);
			}
			else
			{
				$leden->voegtoe($lid);
			}
		}

		$div->add($form);
		if($printable) {
			$div->add(new HtmlHeader(2,"De ".$leden->aantal()." minderjarige leden:"));
			$div->add($table = new HtmlTable());
			$i = 0;
			foreach($leden as $lid) {
				if($i%5 == 0)
					$table->add($row = new HtmlTableRow());
				$row->addData(PersoonView::kop($lid, true, true));
				$i++;
				$i%=5;
			}
			$div->add(new HtmlHeader(2,"De ".$bijnaleden->aantal()." bijnaleden:"));
			$div->add($table = new HtmlTable());
			$i = 0;
			foreach($bijnaleden as $lid) {
				if($i%5 == 0)
					$table->add($row = new HtmlTableRow());
				$row->addData(PersoonView::kop($lid, true, true));
				$i++;
				$i%=5;
			}
		} else {
			$div->add(new HtmlHeader(2,"De ".$leden->aantal()." minderjarige leden:")) 
				->add(PersoonVerzamelingView::koppen($leden))
				->add(new HtmlHeader(2,"De ".$bijnaleden->aantal()." bijnaleden:")) 
				->add(PersoonVerzamelingView::koppen($bijnaleden));
		}
		return $div;
	}

	/**
	 *  Geef de table row voor het massaal modificeren van een enkel veld.
	 * Als je het aantal velden hier aanpast,
	 * fix dan ook de TableHeader die massamodificatieForm genereert.
	 * @see massamodificatieForm
	 * @param veld Het veld van het Lid-object dat we gaan aanpassen.
	 * @returns een HtmlTR klaar om in een table te gooien, of false als het veld onaanpasbaar blijkt.
	 */
	static public function massamodificatieTR($veld) {
		// we doen hier een paar keer self::call om te parametriseren in veldnaam
		// maar misschien bestaat dat hele veld niet (i.e. Naam), dus check hier of dat eigenlijk kan
		if (!method_exists('LidView', "label$veld") || !method_exists('LidView', "genericForm$veld")) {
			return false;
		}

		$row = new HtmlTableRow();
		// label
		// O grote WebCievoorouders, waarom moet label$veld() altijd een object hebben?
		$row->addData(LidView::call("label", $veld, Persoon::getIngelogd()));
		// vinkje
		$row->addData(HtmlInput::makeCheckbox("PasAan[".$veld."]"));

		// van
		$row->addData(LidView::call("genericForm", $veld, "Van[".$veld."]"));
		// naar
		$row->addData(LidView::call("genericForm", $veld, "Naar[".$veld."]"));

		return $row;
	}

	/**
	 *  Maak de inhoud van een massabewerkingsformulier voor leden.
	 * @see processMassamodificatieForm
	 * @returns Een HtmlElement met verscheidene inputs, die processMassamodificatieForm aankan.
	 */
	static public function massamodificatieForm() {
		$divje = new HtmlDiv();

		// Voor de ledenselectie
		$divje->add(new HtmlParagraph(_("Bepaal eerst welke leden je massaal wilt "
			. "modificeren. Dit zouden bijvoorbeeld alle introdeelnemers kunnen "
			. "zijn, of degenen met een gegeven lidnummer. Met je Kart kun je dit "
			. "effici&#235;nter doen. LET OP: (Contact-)personen tellen niet als leden!")));
		$divje->add(self::defaultFormPersoonVerzameling());

		// Voor de waarden
		$divje->add(new HtmlParagraph(_("Is het gelukt om iedereen te selecteren, "
			. "dan moet je aangeven wat er precies moet veranderen. Vink het vak"
			. "je direct naast een veld aan als je die wilt aanpakken, zet de "
			. "voormalige waarde links neer en de nieuwe waarde rechts.")));
		$divje->add($table = new HtmlTable());
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$header = $thead->addRow();
		// Controleer dat dit correspondeert met massamodificatieTR!
		$header->addHeader(_("Als het veld"));
		$header->addHeader(_("aangevinkt is en"));
		$header->addHeader(_("de waarde _ heeft,"));
		$header->addHeader(_("maak het dan _."));

		// een TR per veld
		foreach (Lid::velden() as $veld) {
			$tr = self::massamodificatieTR($veld);
			if ($tr) {
				$tbody->add($tr);
			}
		}

		return $divje;
	}

	/**
	 *  Verwerk de inhoud van een massabewerkingsformulier voor leden.
	 * @see massamodificatieForm
	 * @returns Een htmlelement met de resultaten.
	 */
	static public function processMassamodificatieForm() {
		$pasAan = tryPar("PasAan", array());
		$van = tryPar("Van", array());
		$naar = tryPar("Naar", array());
		$lid_ids = array_filter(tryPar("PersoonVerzameling", array()));
		$leden = LidVerzameling::verzamel(array_keys($lid_ids));

		// houd ook wat stats bij
		$veldenCount = 0; // # aangevinkte velden
		$ledenCount = $leden->aantal(); // # aangevinkte leden
		$matchesCount = 0; // totaal # aangepaste waarden
		foreach (Lid::velden() as $veld) {
			$aangevinkt = $pasAan[$veld];
			if (!$aangevinkt) {
				continue;
			}
			$veldenCount++;
			foreach ($leden as $lid) {
				$origineel = self::callobj($lid, "get", $veld);
				if ($origineel == $van[$veld]) {
					$origineel = self::callobj($lid, "set", $veld, $naar[$veld]);
					$matchesCount++;
				}
			}
		}
		// pas later opslaan, dat scheelt weer kwerries
		foreach ($leden as $lid) {
			$lid->opslaan();
		}

		return new HtmlParagraph(sprintf(_("Er waren %d velden en %d leden geselecteerd, met in totaal %d veranderde waarden. Ga maar eens na hoeveel extra tijd dat zou kosten om handmatig te doen!"), $veldenCount, $ledenCount, $matchesCount));
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
