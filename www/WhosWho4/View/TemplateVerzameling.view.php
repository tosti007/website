<?
abstract class TemplateVerzamelingView
	extends TemplateVerzamelingView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in TemplateVerzamelingView.
	 *
	 * @param obj Het TemplateVerzameling-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTemplateVerzameling(TemplateVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

	// TODO: maak dit een algemene methode in een VerzamelingView-basisklasse.
	/**
	 * @brief Toon alle (gegeven) templates in een mooi tabelletje.
	 * @param pers Indien NULL, laad ze allemaal uit de databaas.
	 * @returns Een HtmlElement.
	 */
	public static function templateOverzicht(TemplateVerzameling $templates = null) {
		if (is_null($templates)) {
			$templates = TemplateQuery::table()->verzamel();
		}

		if ($templates->aantal() <= 0)
		{
			return new HtmlParagraph(_("Geen resultaten gevonden."));
		}

		$tabel = new HtmlTable();
		$tabel->add(new HtmlTableHead($row = new HtmlTableRow()));
		$row->addHeader(_("Naam"));
		$row->addHeader(_("Beschrijving"));
		$row->addHeader(); // wijzig
		$row->addHeader(); // verwijder

		foreach ($templates as $template) {
			$templateID = $template->geefID();
			$row = new HtmlTableRow();
			$row->addData(TemplateView::waardeLabel($template));
			$row->addData(TemplateView::waardeBeschrijving($template));
			$row->addData(HtmlAnchor::button($template->wijzigURL(), _("Wijzig")));
			$row->addData(HtmlAnchor::button($template->verwijderURL(), _("Verwijder")));

			$tabel->addImmutable($row);
		}

		return $tabel;
	}
}
