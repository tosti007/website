<?
/**
 * $Id$
 */
abstract class PersoonVoorkeurView
	extends PersoonVoorkeurView_Generated
{

	public static function labelTaal(PersoonVoorkeur $obj)
	{
		return _('Voorkeurstaal');
	}

	public static function labelEnumTaal($value)
	{
		switch ($value)
		{
		case 'GEEN':
			return _('Geen voorkeur');
		case 'NL':
			return _('Nederlands');
		case 'EN':
			return _('Engels');
		}
	}

	public static function labelEnumBugSortering($value)
	{
		switch($value)
		{
		case 'ASC':
			return _('oplopend');
		case 'DESC':
			return _('aflopend');
		}
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
