<?
/**
 * $Id$
 */
abstract class PersoonKartView
	extends PersoonKartView_Generated
{
	static public function kartOverzicht() {
		$div = new HtmlDiv();

		$persoonkarts = PersoonKartVerzameling::geefVanPersoon(Persoon::getIngelogd());

		if($persoonkarts->aantal() > 0)
		{
			$div->add(new HtmlHeader(3,_('Opgeslagen karts:')));
			$div->add($table = new HtmlTable());
			$tbody = $table->addBody();

			foreach($persoonkarts as $pk)
			{
				$row = $tbody->addRow();
				$row->addData(new HtmlAnchor($pk->url(),PersoonKartView::waardeNaam($pk)));
				$row->addData($group = new HtmlDiv(null, 'btn-group'));
				$group->add(HtmlAnchor::button('KartLaden?naam='.$pk->getNaam(), _('Laden'), null, 'btn-sm'));
				$group->add(HtmlAnchor::button('KartLaden?naam='.$pk->getNaam().'&toevoegen=true', _('Bij de huidige kart stoppen'), null, 'btn-sm'));
				$group->add(HtmlAnchor::button('KartVerwijderen?naam='.$pk->getNaam(), _('Verwijderen'), null, 'btn-sm btn-danger'));
			}
		}

		return $div;
	}

	/**
	 *   Laat de info van een opgeslagen kart zien
	 *
	 *  @param pk de persoonkart die we willen laten zien
	 */
	static public function info(PersoonKart $pk)
	{
		$page = Page::getInstance()->start(sprintf(_("Info van de kart '%s'"),PersoonKartView::waardeNaam($pk)));

		$page->add($div = new HtmlDiv(null, 'row'));
		$div->addChildren(self::maakOverzicht($pk));
		$div->makeImmutable();

		$page->end();
	}

	/**
	 *   Geeft het form terug waarmee we een kart kunnen opslaan
	 *
	 *	@param msg de foutmelding als die er eventueel is
	 */
	public static function opslaanForm ($msg = null)
	{
		$form = HtmlForm::named('KartOpslaan');
		$form->addClass('form-inline');
		$form->addChildren(array(
			_('Naam van de kart:'),
			HtmlInput::makeText('naam'),
			$div = new HtmlDiv(null, 'row')
		));

		$div->addChildren(self::maakOverzicht(Kart::geef()));
		$div->makeImmutable();

		$form->add(HtmlInput::makeSubmitButton('Sla op!'));

		// Return de form
		return $form;
	}

	/**
	 * Maakt een HTML-array met kolommen die weergeefbaar zijn en een overzicht
	 * van de inhoud van de kart geven.
	 * @param $kartAchtige de kart die weergegeven moet worden, en mag van het
	 *                     type Kart of PersoonKart zijn.
	 * @return een array van divs die 'col-md-$n$' hebben zodanig dat alles
	 wordt opgevuld indien de kart niet leeg is.
	 */
	private static function maakOverzicht($kartAchtige)
	{
		$ret = array();

		// Haal alle verzamelingen behorende bij de persoonkart op
		$personen = $kartAchtige->getPersonen();
		$cies = $kartAchtige->getCies();
		$media = $kartAchtige->getMedia();

		$rownrs = ($personen->aantal() != 0) + ($cies->aantal() != 0) + ($media->aantal() != 0);
		if ($rownrs == 0) {
			// TODO: behandel de edge-case van een lege kart.
			return $ret;
		}
		$colWidth = 12 / $rownrs;

		// overzicht van alle personen in de persoonkart
		if($personen->aantal() != 0) {
			$persdiv = new HtmlDiv(null, 'col-md-' . $colWidth);
			$persdiv->add(new HtmlHeader(3, _('De volgende personen zitten in je kart:')));
			$persdiv->add($list = new HtmlList());
			foreach($personen as $p)
				$list->add(new HtmlListItem(new HtmlAnchor($p->url(), PersoonView::naam($p))));
			$ret[] = $persdiv;
		}

		// overzicht van all cies in de persoonkart
		if($cies->aantal() != 0) {
			$ciediv = new HtmlDiv(null, 'col-md-' . $colWidth);
			$ciediv->add(new HtmlHeader(3, _('De volgende cies zitten in je kart:')));
			$ciediv->add($list = new HtmlList());
			foreach($cies as $c)
				$list->add(new HtmlListItem(new HtmlAnchor($c->url(), CommissieView::waardeNaam($c))));
			$ret[] = $ciediv;
		}

		// overzicht van alle media in de persoonkart
		if($media->aantal() != 0) {
			$mediadiv = new HtmlDiv(null, 'col-md-' . $colWidth);
			$mediadiv->add(new HtmlHeader(3, _('De volgende foto\'s/films zitten in je kart:')));
			$mediadiv->add($list = new HtmlList());
			foreach($media as $m)
				$list->add(new HtmlListItem(new HtmlAnchor($m->url(), $m->getMediaID())));
			$ret[] = $mediadiv;
		}

		return $ret;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
