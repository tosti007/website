<?

/**
 * $Id$
 */
abstract class ContactAdresView
	extends ContactAdresView_Generated
{
	static public function labelStraat1(ContactAdres $obj)
	{
		return _('Straat');
	}

	static public function labelStraat2(ContactAdres $obj)
	{
		return _('Toevoeging');
	}

	static public function labelHuisnummer(ContactAdres $obj)
	{
		return _('Huisnummer');
	}

	static public function labelPostcode(ContactAdres $obj)
	{
		return _('Postcode');
	}

	static public function labelWoonplaats(ContactAdres $obj)
	{
		return _('Plaats');
	}

	static public function labelLand(ContactAdres $obj)
	{
		return _('Land');
	}

	static public function labelSoort(ContactAdres $obj)
	{
		return _('Soort');
	}

	public static function labelenumSoort($value)
	{
		switch ($value)
		{
		case 'THUIS': return _('Thuisadres');
		case 'OUDERS': return _('Adres ouders');
		case 'WERK': return _('Adres werk');
		case 'POST': return _('Postadres');
		case 'BEZOEK': return _('Bezoekadres');
		case 'FACTUUR': return _('Factuuradres');
		}
		return parent::labelenumSoort($value);
	}

	/**
	 *  Als het een bedrijf is willen we maar bepaalde soorten adressen. Om
	 *  hier op te checken moet er wel een object worden meegegeven.
	 **/
	public static function formSoort(ContactAdres $obj, $include_id = false)
	{
		if($obj->getContact() instanceof Organisatie)
		{
			$veld = 'Soort';
			$name = self::formveld($obj, $veld, null, $include_id);
			$enums = array('POST','BEZOEK','FACTUUR');
			$waarde = self::callobj($obj, 'get', $veld);

			$select = new HtmlSelectbox($name);
			if (!in_array($waarde, $enums))
			{
				$label = self::call('labelenum', $veld, $waarde);
				$select->addOption($waarde, $label);
			}
			foreach ($enums as $enum)
			{
				$label = self::call('labelenum', $veld, $enum);
				$select->addOption($enum, $label);
			}
			$select->select(tryPar($name, $waarde));

			return $select;
		}

		return parent::defaultFormEnum($obj, 'Soort', $include_id);
	}

	static public function detailsDiv (ContactAdres $adres)
	{
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(4, self::waardeSoort($adres) . ":"))
			->add(self::waardeStraat1($adres), " ", self::waardeHuisnummer($adres), new HtmlBreak());
		$adres2 = $adres->getStraat2();
		if(!empty($adres2))
			$div->add(self::waardeStraat2($adres), new HtmlBreak());
		$div->add(self::waardePostcode($adres), " ", self::waardeWoonplaats($adres), new HtmlBreak())
			->add(self::waardeLand($adres));

		return $div;
	}

	public static function geefAdresDiv($contact, $type)
	{
		$div = null;
		
		$adres = ContactAdres::geefBySoort($contact, $type);
		if($adres != null)
		{
			$div = new HtmlDiv();
			$div->add(new HtmlHeader(4, ucfirst(strtolower($type)) . ":"));
			$div->add($adres->getStraat1(), " ", $adres->getHuisNummer(), new HtmlBreak());
			$adres2 = $adres->getStraat2();
			if(!empty($adres2))
				$div->add($adres->getStraat2(), new HtmlBreak());
			$div->add($adres->getPostcode(), " ", $adres->getWoonplaats(), new HtmlBreak());
			$div->add($adres->getLand());
		}
		return $div;
	}
	
	public static function toString($adres)
	{
		return	   ($adres->getStraat1()?$adres->getStraat1() . ' ':'')
				 . ($adres->getStraat2()?$adres->getStraat2() . ' ':'')
				 . $adres->getHuisnummer() . "\r\n"
				 . ($adres->getPostcode()?$adres->getPostcode() . ', ':'')
				 . $adres->getWoonplaats()
				 . ($adres->getLand()?', '.$adres->getLand():'');
	}

	public static function adresString($contact, $type)
	{
		$adres = ContactAdres::geefBySoort($contact, $type);
		if(is_null($adres))
		{
			return "";
		}
		return ($adres->getStraat1()?$adres->getStraat1() . ' ':'')
				 . ($adres->getStraat2()?$adres->getStraat2() . ' ':'')
				 . ($adres->getHuisnummer()?', '.$adres->getHuisnummer():'')
				 . ($adres->getPostcode()?', '.$adres->getPostcode():'')
				 . ($adres->getWoonplaats()?', '.$adres->getWoonplaats():'')
				 . ($adres->getLand()?', '.$adres->getLand():'');
	}

	public static function adresArray($contact, $type = '')
	{
		if($type)
			$adres = ContactAdres::geefBySoort($contact, $type);
		else
			$adres = ContactAdres::eersteBijContact($contact);

		if($adres)
		{
			return array(
				'straat1' => $adres->getStraat1(),
				'straat2' => $adres->getStraat2(),
				'huisnummer' => $adres->getHuisnummer(),
				'postcode' => $adres->getPostcode(),
				'woonplaats' => $adres->getWoonplaats(),
				'land'=> ($adres->getLand()? $adres->getLand():'NL')
				);
		}
		return null;
	}

	public static function plainAdres(ContactAdres $adres)
	{
		$string = self::waardeStraat1($adres)." ".self::waardeHuisnummer($adres)."\n"
					.self::waardePostcode($adres)." ".self::waardeWoonplaats($adres);
		return $string;
	}

	public static function latexAdres(ContactAdres $adres)
	{
		$string = self::waardeStraat1($adres)."~".self::waardeHuisnummer($adres);
		if ($adres->getStraat2())
		{
			$string .= "\\\\" . self::waardeStraat2($adres);
		}
		$string .= "\\\\" . self::waardePostcode($adres)."~~".self::waardeWoonplaats($adres);
		if($adres->getLand())
			$string .= "\\\\".self::waardeLand($adres);
		return $string;
	}

	public static function tableTr(ContactAdres $adres, $routelink = false)
	{
		$contact = $adres->getContact();

		if($contact instanceof Organisatie)
		{
			$wijziglink = "(".new HtmlAnchor($contact->url()."/".'WijzigAdres','wijzig').")";
		}

		$tr = self::infoTRData(
			ContactAdresView::waardeSoort($adres),
			ContactAdresView::waardeStraat1($adres) .' '. ContactAdresView::waardeStraat2($adres) .' '. ContactAdresView::waardeHuisnummer($adres)
			. new HtmlBreak() . ContactAdresView::waardePostcode($adres) .' '. ContactAdresView::waardeWoonplaats($adres)
			. new HtmlBreak() . ContactAdresView::waardeLand($adres)
			. ($routelink?" (".new HtmlAnchor(ContactAdresView::googleMapsRouteUrl($adres), "route") .")":null)
			. $wijziglink
			);
		return $tr;
	}

	static public function googleMapsRouteUrl($adres, $wandel = true){

		$url = 'http://maps.google.nl/maps'
			. '?saddr='.urlencode('Princetonplein 5, 3584 Utrecht, Netherlands')
			. '&daddr='.urlencode($adres->getStraat1()
					." " .$adres->getHuisNummer()
					.", ".$adres->getPostcode()
					." " .$adres->getWoonplaats()
					.", ".$adres->getLand()
					) . ($wandel?'&dirflg=w':'') ; //dit geeft aan dat het om een wandelroute gaat

		return $url;
	}

	/**
	 *  Toon de bing maps route van A-Eskwadraat naar de Persoon voor mensen windows Phones enzo.
	 *  Info at http://www.bing.com/community/site_blogs/b/maps/archive/2008/04/10/live-search-maps-api.aspx
	 */

	static public function bingMapsRouteUrl($adres, $wandel = true){

		$url = 'http://www.bing.com/maps/default.aspx?v=2'
			. '&rtp='. urlencode('adr.Princetonplein, 3584 Utrecht, Netherlands') . '~'
			. 'adr.'.urlencode($adres->getStraat1()
					." " .$adres->getHuisNummer()
					.", ".$adres->getPostcodeCijfers() //Normale postcode eet hij niet :(
					." " .$adres->getWoonplaats()
					.", ".($adres->getLand() != null? $adres->getLand():'Netherlands')
					);

		return $url;

	}

	/**
	 * Toont een QR-code voor Google/Bing-maps
	 */
	static public function mapsQR (ContactAdres $adres)
	{

		$urlGoogle = urlencode(self::googleMapsRouteUrl($adres));
		$urlBing = urlencode(self::bingMapsRouteUrl($adres));

		$div1 = new HtmlDiv();
		$div1->add(new HtmlHeader(2, "Google Maps"));
		$div1->add(new HtmlImage("https://chart.googleapis.com/chart?cht=qr&chl=$urlGoogle&choe=UTF-8&chs=400x400", 'QR code om te scannen voor googlemaps'));
		$div1->setAttribute('style', 'float:left');
		$div2 = new HtmlDiv();
		$div2->add(new HtmlHeader(2, "Bing Maps"));
		$div2->add(new HtmlImage("https://chart.googleapis.com/chart?cht=qr&chl=$urlBing&choe=UTF-8&chs=400x400", 'QR code om te scannen voor bingmaps'));
		$div2->setAttribute('style', 'float:right');
		$par = new HtmlParagraph("Je kunt deze QR-code scannen met je mobiele telefoon. De linker verwijst je door naar de Google Maps app (als je deze geïnstalleerd hebt), of naar de webpagina van Google Maps. De rechter stuurt je door naar Bing Maps.");
		$par->setAttribute('style', 'clear:both');

		$div = new HtmlDiv();
		$div->add($div1)->add($div2)->add($par);

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
