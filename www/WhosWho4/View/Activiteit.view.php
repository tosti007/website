<?

/**
 * $Id$
 */
abstract class ActiviteitView
	extends ActiviteitView_Generated
{
	/**
	 *  Geeft een pagina met de koppen van de ingeschreven personen
	 *
	 * @param PersoonVerzameling $personen De personen om het koppenblad van te maken
	 * @param Activiteit $act De activiteit om het koppenblad bij te maken
	 * @return HTMLPage|Page
	 */
	public static function koppenBlad(PersoonVerzameling $personen, Activiteit $act)
	{
		$page = (new HTMLPage())
			 ->start(_("Koppenblad voor") . " " . ActiviteitView::titel($act))
			 ->add($par = new HtmlDiv(HtmlAnchor::button($act->url(), _("Terug naar de activiteitpagina"))));

		if($act->magWijzigen()) {
			$par->add(HtmlAnchor::button($act->deelnemersURL(), _("Terug naar de deelnemersbeheerpagina")));
		}

		$page->add(PersoonVerzamelingView::koppen($personen));
		return $page;
	}

	/**
	 *  Schrijf jezelf in; hoort bij Activiteit_Controller::inschrijven()
	 *
	 * @param Activiteit $act De activiteit om bij in te schrijven
	 * @return HTMLPage|Page
	 */
	static public function inschrijven (Activiteit $act)
	{
		return (new HTMLPage())
			 ->start(null, 'activiteiten')
			 ->add(new HtmlHeader(2, _("Inschrijven") . ' ' . ActiviteitView::makeLink($act)));
	}

	/**
	 *  Schrijf jezelf uit; hoort bij Activiteit_Controller::uitschrijven()
	 *
	 * @param Activiteit $act De activiteit om bij uit te schrijven
	 * @param string $msg Een string als boodschap
	 * @return HTMLPage|Page
	 */
	static public function uitschrijven (Activiteit $act, $msg = '')
	{
		return (new HTMLPage())
			 ->start(null, 'activiteiten')
			 ->add(new HtmlHeader(2, _("Uitschrijven") . ' ' . ActiviteitView::makeLink($act)))
			 ->add($msg);
	}

	/**
	 *   Laat de vragen zien die bij het inschrijven voor deze activiteit horen.
	 *
	 * @param Activiteit $act De activiteit om de vragen bij te laten zien
	 * @param Deelnemer $dlnr De deelnemer die de vragen invult
	 * @param bool $nieuw Een bool of het om een nieuwe deelnemer gaat
	 * @param bool $show_error Bool of errors moeten worden laten zien
	 * @return HTMLPage|Page
	 */
	static public function vragen(Activiteit $act, Deelnemer $dlnr, $nieuw = true, $show_error = false)
	{
		$antwoorden = $dlnr->getAntwoorden();
		$naam = PersoonView::naam($dlnr->getPersoon());

		$page = (new HTMLPage())
			 ->start($nieuw?_("Inschrijven"):_("Wijzig inschrijving van $naam voor")
			 	. ' ' . $act, 'activiteiten');

		$form = HtmlForm::named($nieuw?'vragenLijst':'deelnemerWijzig')
			->add($div = new HtmlDiv());

		// Moeten we we vragen stellen?
		if($dlnr->wijzigenHeeftZin())
		{
			$page->add(new HtmlParagraph(_("Hieronder kan je opmerkingen en/of vragen voor je inschrijving invullen.")));

			foreach($antwoorden as $antwoord)
			{
				$div->add(new HtmlDiv(array(
					new HtmlLabel('vraag', _('Vraag'), 'col-sm-2 control-label'),
					new HtmlDiv(ActiviteitVraagView::waardeVraag($antwoord->getVraag()), 'actvraag col-sm-10')
				), 'form-group'));
				$div->add(DeelnemerAntwoordView::wijzigTR($antwoord, "antwoord", $show_error, true));
			}
			$form->add(HtmlInput::makeFormSubmitButton($nieuw?_("Dit zijn mijn antwoorden en schrijf me in!")
														:_("Voer de aanpassingen door!")));
		} else {
			$div->add(HtmlInput::makeFormSubmitButton($nieuw?_("Schrijf me in!")
														:_("Er is niets aan te passen!")));
		}

		$page->add($form);

		return $page;
	}

	/**
	 *  Geeft een uitgebreide lijst van de deelnemers; hoort bij Activiteit_Controller::deelnemers()
	 *
	 * @param Activiteit $act De activiteit om de deelnemerslijst te laten zien
	 * @return HTMLPage|Page
	 */
	static public function deelnemers (Activiteit $act)
	{
		$page = (new HTMLPage())
			 ->addFooterJS(Page::minifiedFile('Activiteit.js'))
			 ->start(sprintf(_("Deelnemers %s"), ActiviteitView::titel($act)), 'activiteiten');
		$page->add(DeelnemerVerzamelingView::uitgebreid($act, 'ActDeelnemers', 'Deelnemers',true));
		return $page;
	}

	/**
	 *  Schrijft deelnemers uit; hoort bij Activiteit_Controller::deelnemersUitschrijven()
	 *
	 * @param Activiteit $act De activiteit om de deelenemers uit te schrijven
	 * @param DeelnemerVerzameling $deelnemers De deelnemerverzameling om uit te schrijven
	 * @return HTMLPage|Page|\Symfony\Component\HttpFoundation\Response
	 */
	static public function deelnemersUitschrijven (Activiteit $act, DeelnemerVerzameling $deelnemers)
	{
		if ($deelnemers->aantal() == 0)
		{
			return Page::responseRedirectMelding($act->deelnemersURL(), _('Je hebt niemand geselecteerd!'));
		}

		$page = (new HTMLPage())
			 ->addFooterJS(Page::minifiedFile('Activiteit.js'))
			 ->start(_("Deelnemers uitschrijven") . ' ' . ActiviteitView::makeLink($act));

		$page->add(new HtmlParagraph(_("Je wilt de volgende personen uitschrijven: ") . DeelnemerVerzamelingView::kort($deelnemers)))
			 ->add($form = HtmlForm::named('ActDeelnemers'));
		foreach ($deelnemers as $deelnemer)
			$form->add(HtmlInput::makeHidden('Deelnemers[Geselecteerde][' . $deelnemer->getPersoonContactID() . ']', '1'));
		$form->add(new HtmlParagraph(HtmlInput::makeCheckbox('Deelnemers[check]')
					->setCaption(_("Ik weet het heel zeker!"))))
			 ->add(HtmlInput::makeHidden('Deelnemers[Actie]', 'uitschrijven'))
			 ->add(new HtmlDiv(HtmlInput::makeSubmitButton(_("Volgende"), 'check')));
		return $page;
	}

	/**
	 *  Shop deelnemers in je kart; hoort bij Activiteit_Controller::deelnemersKart()
	 *
	 * @param Activiteit $act De activiteit waarvan je de deelnemers shopt
	 * @return HTMLPage|Page
	 */
	static public function deelnemersKart (Activiteit $act)
	{
		$page = (new HTMLPage())
			 ->addFooterJS(Page::minifiedFile('Activiteit.js'))
			 ->start(null, 'activiteiten')
			 ->add(new HtmlHeader(2, _("Deelnemers") . ' ' . ActiviteitView::makeLink($act)));
		return $page;
	}

	/**
	 *  Schrijf deelnemers in; hoort bij Activiteit_Controller::deelnemersInschrijven()
	 *
	 * @param Activiteit $act De activiteit waarvoor je deelnemers in schrijft
	 * @return HTMLPage|Page
	 */
	static public function deelnemersInschrijven (Activiteit $act)
	{
		return (new HTMLPage())
			 ->addFooterJS(Page::minifiedFile('Activiteit.js'))
			 ->start(sprintf(_("Deelnemers %s inschrijven"), ActiviteitView::makeLink($act)))
			 ->add(DeelnemerVerzamelingView::nieuwForm('ActDeelnemersInschrijven', null, null, array(), $act));
	}

	/**
	 *  Verwijder een activiteit; hoort bij Activiteit_Controller::verwijder()
	 *
	 * @param Activiteit $act De activiteit om te verwijderen
	 *
	 * @return HtmlDiv Een div met daarin het form om de act te verwijderen
	 */
	static public function verwijderForm($act)
	{
		$div = new HtmlDiv();

		$div->add(parent::verwijderForm($act));

		$div->add(new HtmlParagraph(
			 _("Deze handeling is permanent en het is niet mogelijk het effect ongedaan te maken.")));

		return $div;
	}

	/*
	 *  Geeft een list-item van de act
	 *
	 * @param act De activiteit
	 *
	 * @return De list-item
	 */
	static public function lijstRegel($act)
	{
		$li = new HtmlListItem($span = new HtmlSpan(array(
			new HtmlSpan("[" . self::waardeMomentBegin($act, 'lijst'). "]", 'smalltext'),
			ActiviteitView::titel($act)
			)));
		if ($act->getToegang() == 'PRIVE')
		{
			$span->addClass('internAct');
		}
		return $li;
	}

	/*
	 *  Geeft een list-item van de act die ook nog eens klikbaar is
	 *
	 * @param act De activiteit
	 *
	 * @return De list-item
	 */
	static public function lijstRegelKlikbaar($act)
	{
		$li = new HtmlListItem($span = new HtmlSpan(
			new HtmlAnchor($act->url(), array(
				new HtmlSpan("[" . self::waardeMomentBegin($act, 'lijst'). "]", 'smalltext'),
				ActiviteitView::titel($act)
			))));
		if ($act->getToegang() == 'PRIVE')
		{
			$span->addClass('internAct');
		}
		return $li;
	}

	/**
	 *  De defaultwaarde van de activiteit
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return Een link naar de activiteit
	 */
	static public function defaultWaardeActiviteit(Activiteit $act)
	{
		return self::makeLink($act);
	}

	/**
	 *  Maakt een link van een activiteit
	 *
	 * @param Activiteit $act De activiteit om de link van te krijgen
	 * @param string|null $caption Een eventuele string wat de caption van de link moet zijn
	 * @param bool $inc_date Een bool of de date geinclude moet worden
	 *
	 * @return HtmlAnchor Een htmlAnchor naar de act
	 */
	static public function makeLink(Activiteit $act, $caption = null, $inc_date = false)
	{
		if(!$caption)
		{
			$caption = (($inc_date)
					? $act->getMomentBegin()->strftime('%a %e %b %y: ')
					: '')
				. ActiviteitView::titel($act);
		}
		return new HtmlAnchor($act->url(), $caption);
	}

	/**
	 *  Handelt de data af voor het inschrijven van deelnemers
	 */
	public static function processInschrijfForm()
	{
		$vragen = ActiviteitVraagVerzameling::vanActiviteit($act);
		$antwoordData = tryPar(self::formobj(new DeelnemerAntwoord($act)), NULL);

			foreach($vragen as $vraag)
			{
				$antwoord = DeelnemerAntwoord::geef($pers->geefID(), $act->geefID(),
						$vraag->getActiviteitActiviteitID(), $vraag->getVraagID());
				if(!$antwoord) { 
					$antwoord = new DeelnemerAntwoord(array($pers->geefID(), $act->geefID()),
						array($vraag->getActiviteitActiviteitID(), $vraag->getVraagID()));
				}
				DeelnemerAntwoordView::processForm($antwoord, "viewWijzig", $antwoordData[$antwoord->geefID()]);
			}
	}

	/**
	 *  Maakt een pagina voor het activeren van scanbare kaartjes
	 *
	 * @param Activiteit $act De activiteit om de kaartjes van te activeren
	 * @param bool $existScanbare Of er al scanbare kaartjes bestaan
	 * @param bool $totalMagScannen Of er al gescand mag worden
	 * @return HTMLPage
	 */
	static public function activeerKaartjes(Activiteit $act, $existScanbare = false, $totalMagScannen = true)
	{
		$page = new HTMLPage();

		if(!$existScanbare && $totalMagScannen)
		{
			$page->start(_("Geen kaartjes gevonden!"));

			$page->add(new HtmlParagraph(_("Er zijn helemaal geen kaartjes "
					. "om te activeren!"), 'text-danger strongtext'));

			$page->add(HtmlAnchor::button($act->url(), _("Terug naar de activiteit")));
		}
		else if($existScanbare && !$totalMagScannen)
		{
			$page->start(_("Deactiveer kaartjes"));

			$page->add(new HtmlParagraph(_("Er zijn kaartjes bij deze activiteit "
				. "die scanbaar zijn en er zijn kaartjes die niet scanbaar zijn. "
				. "deactiveer eerst de kaartjes die scanbaar zijn!")));

			$page->add($form = HtmlForm::named('deactiveer'));
			$form->add(HtmlInput::makeSubmitButton(_("Deactiveer alle kaartjes")));
		}
		else if($totalMagScannen && $existScanbare)
		{
			$page->start(_("Alle kaartjes al geactiveerd"));

			$page->add(new HtmlParagraph(_("Alle kaartjes zijn al geactiveerd!")));
			$page->add(new HtmlParagraph(_("Als je alle kaartjes wilt deactiveren "
				. "moet je op de knop hieronder klikken.")));

			$page->add($form = HtmlForm::named('deactiveer'));
			$form->add(HtmlInput::makeSubmitButton(_("Deactiveer alle kaartjes")));

			$page->add(new HtmlParagraph(_("Voor als je het ook vergeten bent "
				. "is hieronder de master-QR-code voor je :).")));

			$page->add(new HtmlImage("data:image/png;base64,"
				. base64_encode($act->getMasterQRCode()), _('QR-code')));
		}
		else
		{
			$page->start(_("Activeer kaartjes om te scannen!"));

			$page->add(new HtmlParagraph(_("Weet je zeker dat je de kaartjes "
					. "van deze activiteit wilt activeren zodat ze gescand kunnen worden?")));
			$page->add(new HtmlParagraph(_("Vergeet ook niet om de onderstaande "
					. "master-QR-code uit te printen voor het scannen!")));

			$page->add(new HtmlImage("data:image/png;base64,"
				. base64_encode($act->getMasterQRCode()), _('QR-code')));

			$page->add($form = HtmlForm::named('activeer'));
			$form->add(HtmlInput::makeSubmitButton(_("Ja")));
		}

		return $page;
	}

	/**
	 *  Maakt een maindiv met informatie bij de activiteit
	 *
	 * @param Activiteit $act De activiteit om de maindiv bij te maken
	 *
	 * @return HtmlDiv De maindiv
	 */
	static public function maakMainDiv(Activiteit $act)
	{
		$class = get_class($act);
		$viewClass = $class . "View";
		switch($class)
		{
		case 'Activiteit':
			return new HtmlDiv();
			break;
		default:
			return $viewClass::maakMainDiv($act);
			break;
		}
	}

	public static function getDetails(Activiteit $act, $isAjax)
	{
		$content = array();

		$fotos = array();
		if (Persoon::getIngelogd()) {
			$aantal = 6;
			$fotos = MediaVerzameling::getTopratedByAct($act, 6);
		}

		$buttonBar = new HtmlDiv(null, 'btn-group');
		if (sizeof($fotos) == 0) {
			$buttonBar->add(HtmlAnchor::button($act->fotoUrl() . 'Uploaden', _("Foto's uploaden")));
		} // anders, laten we wat leuke foto's zien verderop bij maakFotoDiv

		$buttonBar->add(self::addToCalendarButton($act));

		// if ($buttonBar->hasHtmlChildren()) {
		$content[] = $buttonBar;
		// }

		$content[] = self::maakMainDiv($act);
		$content[] = self::maakFotoDiv($act, $fotos);
		if (!$isAjax) {
			$content[] = self::maakKaartjesDiv($act);
		}
		$content[] = self::maakDeelnemersDiv($act);
		return $content;
	}

	/**
	 *  Toont de activiteitpagina; hoort bij Activiteit_Controller::details()
	 *
	 * @param act De activiteit om de details van te laten zien
	 */
	static public function details ($act)
	{
		/** Toon de activiteit **/
		$content = self::getDetails($act, false);

		// Laat in de titel informatie zien of iets intern is of niet.
		$titel = self::titel($act);
		$titel .= ($act->getToegang() == 'PRIVE' ? ' (intern)' : '');

		/** Spuug alles uit **/
		return static::pageLinks($act)
			->start(sprintf("$titel %s", ActiviteitInformatieView::computerReserveringIcon($act, true)))
			->add($content);
	}

	/**
	 *  Maakt een div met algemene act-informatie
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return HtmlDiv Een div met de activiteit-informatie
	 */
	static public function maakInfoDiv(Activiteit $act)
	{
		$info = $act->getInformatie();
		$div = new HtmlDiv($rowDiv = new HtmlDiv(null, 'row'));

		$rowDiv->add(new HtmlDiv(
			new HtmlDiv(
				new HtmlDiv(ActiviteitInformatieView::waardeWerftekst($info), 'panel-body act-beschrijving')
				, 'panel panel-default')
			, 'col-md-8'));

		$rowDiv->add($infoTlDiv = new HtmlDiv(null, 'col-md-4'));
		$infoTlDiv->add($inf = new HtmlList());
		
		//als er een poster is, show die dan
		if($act->posterPath())
			{
				global $filesystem;
				$posterlink = new HtmlAnchor($act->url() . '/Actposter');
				//kijk of een medium grootte bestaat, zoja laad die dan in:
				if($filesystem->has(explode(".", $act->posterPath())[0] . "medium.jpg")){
					$posterlink->add($img = new HtmlImage($act->url() . '/ActposterMedium', 'Actposter'));
				}
				//laad anders de gigantische versie in:
				else{
					$posterlink->add($img = new HtmlImage($act->url() . '/Actposter', 'Actposter'));
				}
				$img->setCssStyle('width: auto; height: auto; max-width: 100%; max-height: 100%;');
				$inf->add($posterlink);
			}

		$inf->add(new HtmlListItem(array(
				new HtmlSpan(self::labelCommissies($act) . ':'),
				new HtmlSpan(CommissieVerzamelingView::kommaLijst($act->getCommissies()))
				)))
			->add(new HtmlListItem(array(
				new HtmlSpan(_("Begin: ")),
				new HtmlSpan(self::waardeMomentBegin($act))
			)));

		if (!is_null($act->getMomentEind()))
			$inf->add(new HtmlListItem(array(
					new HtmlSpan(_("Eind: ")),
					new HtmlSpan(self::waardeMomentEind($act))
				)));
		if (!is_null($info->getHomepage()))
			$inf->add(new HtmlListItem(array(
					new HtmlSpan(ActiviteitInformatieView::labelHomepage($info) . ':'),
					new HtmlSpan(ActiviteitInformatieView::waardeHomepage($info))
					)));
		if (!is_null($info->getLocatie()))
			$inf->add(new HtmlListItem(
						array(new HtmlSpan(ActiviteitInformatieView::labelLocatie($info) . ':')
							, new HtmlSpan(ActiviteitInformatieView::waardeLocatie($info))
					 )));
		if (!is_null($info->getPrijs()))
			$inf->add(new HtmlListItem(array(
					new HtmlSpan(ActiviteitInformatieView::labelPrijs($info) . ':'),
					new HtmlSpan(ActiviteitInformatieView::waardePrijs($info))
					)));
		if ($info->getAantalComputers() > 0)
		{
			$inf->add(new HtmlListItem(array(
					new HtmlSpan(ActiviteitInformatieView::labelAantalComputers($info) . ':'),
					new HtmlSpan(ActiviteitInformatieView::waardeAantalComputers($info)),
					)));
		}

		$inf->add(new HtmlListItem(array(
				new HtmlSpan(ActiviteitInformatieView::labelActsoort($info) . ':'),
				new HtmlSpan(ActiviteitInformatieView::waardeActsoort($info))
				)));

		if ($info->getCategorie()) {
			$inf->add(new HtmlListItem(array(
					new HtmlSpan(ActiviteitInformatieView::labelCategorie($info) . ':'),
					new HtmlSpan(ActiviteitInformatieView::waardeCategorie($info))
					)));
		}

		if ($info->getMaxDeelnemers())
		{
			$inf->add(new HtmlListItem(array(
					new HtmlSpan(ActiviteitInformatieView::labelMaxDeelnemers($info) . ':'),
					new HtmlSpan(ActiviteitInformatieView::waardeMaxDeelnemers($info))
				)));
		}

		
		$poster = array(
			new HtmlSpan(ActiviteitInformatieView::labelPoster($info) . ':'),
			new HtmlSpan($act->posterPath() ? "Ja" : "Nee")		
		);
		if ($act->magWijzigen()) {
			array_push($poster, new HtmlAnchor($act->url() . '/WijzigPoster', '(wijzig)'));
		}
		$inf->add(new HtmlListItem($poster));
		
		

		return $div;
	}

	/**
	 *  Maakt een div met eventuele fotos erin en een foto-toevoeg-knop
	 *
	 * @param act De activiteit
	 * @param aantal Het aantal fotos om te laten zien
	 *
	 * @return De fotodiv
	 */
	public static function maakFotoDiv($act, $fotos)
	{
		if (!Persoon::getIngelogd() || sizeof($fotos) == 0)
			return null;

		$foto = new HtmlDiv($h = new HtmlHeader(3, _("Foto's")));
		$h->add(new HtmlSmall(new HtmlAnchor($act->fotoUrl() . 'Uploaden', _("Foto's uploaden"))));
		$foto->add(MediaVerzamelingView::fotoLijst($fotos));
		return $foto;
	}

	/**
	 *  Maakt een div met eventuele kaartjes en een kaartje-toevoeg-knop
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return HtmlDiv De kaartjesdiv
	 */
	static public function maakKaartjesDiv(Activiteit $act)
	{
		$div = new HtmlDiv();

		$iDealkaartjes = $act->getIdealKaartjes();

		$externverkrijgbaar = false;
		$aantalbeschikbaar = 0;

		foreach($iDealkaartjes as $k)
		{
			$voorraden = $k->getVoorraden();

			if($k->getExternVerkrijgbaar())
				$externverkrijgbaar = true;

			foreach($voorraden as $v)
				if($v->getVerkoopbaar())
					$aantalbeschikbaar += $v->getAantal();
		}

		if(($iDealkaartjes->aantal() != 0) && ($externverkrijgbaar || (!$externverkrijgbaar && hasAuth('ingelogd'))) && ($aantalbeschikbaar != 0))
		{
			$div->add(new HtmlHeader(3, _('Kaartjes zijn digitaal beschikbaar: ')));
			$div->add(new HtmlDiv(new HtmlDiv(sprintf(_('Kaartjes kopen via %s (er zijn nog %s kaartje beschikbaar)')
				, new HtmlAnchor($act->url() . '/KaartjeKopen', _('iDeal'))
				, $aantalbeschikbaar)
			, 'panel-body'), 'panel panel-default'));
		}

		if($act->magWijzigen() && $act->getToegang() != 'PRIVE' && $iDealkaartjes->aantal() != 0)
		{
			$div->add($panel = new HtmlDiv(null, 'panel panel-default'));
			$panel->add(new HtmlDiv(_('Kaartbeheer'), 'panel-heading'));

			$panel->add($body = new HtmlDiv(null, 'panel-body'));

			if($act->getVragen()->aantal() > 0)
				$body->add(HtmlAnchor::button($act->url() . '/iDealAntwoorden', _('Bekijk antwoorden bij iDeal')));

			if (hasAuth('bestuur')) {
				$body->add(HtmlAnchor::button($act->url() . '/ScanActiveren', _('Scanbare kaartjes activeren')));
				$body->add(HtmlAnchor::button($act->url() . '/Scannen', _('Kaartjes scannen')));
			}

			$panel->add($table = new HtmlTable(null, 'sortable'));
			$thead = $table->addHead();
			$tbody = $table->addBody();

			$row = $thead->addRow();
			$row->makeHeaderFromArray(array(_('ID'), _('Naam'), _('Controleren via'),
				_('Verkoopbaar via site?'), _('Auto-inschrijven'), _('Return-URL')));

			foreach($iDealkaartjes as $iDealkaartje)
			{
				$row = $tbody->addRow();
				$row->addData(new HtmlAnchor($iDealkaartje->url(), iDealKaartjeView::waardeArtikelID($iDealkaartje)));
				$row->addData(iDealKaartjeView::waardeNaam($iDealkaartje));
				$row->addData(iDealKaartjeView::waardeDigitaalVerkrijgbaar($iDealkaartje));
				$row->addData(iDealKaartjeView::waardeExternVerkrijgbaar($iDealkaartje));
				$row->addData(iDealKaartjeView::waardeAutoInschrijven($iDealkaartje));
				$row->addData(iDealKaartjeView::waardeReturnURL($iDealkaartje));
			}
		}

		if(hasAuth('bestuur') && $act->getToegang() != 'PRIVE')
			$div->add(HtmlAnchor::button($act->url() . '/KaartjesToevoegen?iDealKaartje[activiteit]=' . $act->geefID(), _('Kaartjes toevoegen')));

		return $div;
	}

	/**
	 *  Maakt een div met deelnemers en beheer-knoppen
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return HtmlDiv De deelnemersdiv
	 */
	static public function maakDeelnemersDiv(Activiteit $act)
	{
		global $auth;
		$div = new HtmlDiv();

		if($lid = Persoon::getIngelogd()) {
			$deelnemers = $act->deelnemers();
			$deelnemers->toPersoonVerzameling(); // cache: 1x alle leden querien

			$div->add($h = new HtmlHeader(3, _("Deelnemers")));
			if(hasAuth('ingelogd')) {
				$h->add(new HtmlSmall(new HtmlAnchor($act->deelnemersURL() . '/Koppenblad'
					, _("Bekijk koppenblad"))));
			}

			$div->add($links = new HtmlDiv(null, 'btn-group'));

			if(($act->uitschrijfbaar() || $act->magWijzigen()) && $act->isDeelnemer($lid)) {
				$links->add(HtmlAnchor::button($act->url() . '/Uitschrijven', _("Schrijf me toch maar uit!")));
			}

			if(($act->inschrijfbaar() || ($act->magWijzigen() && !$auth->hasCieAuth(PROMOCIE))) && !$act->isDeelnemer($lid)) {
				if(!$act->inschrijfbaar()) {
					$links->add(HtmlAnchor::button($act->url() . '/Inschrijven',
						_("Schrijf me in! ") . new HtmlStrong(_("Let op! Activiteit is niet officieel inschrijfbaar"))));
				} else {
					$links->add(HtmlAnchor::button($act->url() . '/Inschrijven', _("Schrijf me in!")));
				}
			}

			if($act->magWijzigen()) {
				$links->add(HtmlAnchor::button($act->deelnemersURL()
					, _("Beheer deelnemerslijst")));
			}

			if($act->getInschrijfbaar() || $deelnemers->aantal() > 0)
				$div->add(DeelnemerVerzamelingView::simpel($deelnemers, $act));
		}

		return $div;
	}
	
	static public function wijzigPoster(Activiteit $act)
	{
		$page = (new HTMLPage())->start(_("Wijzig de activiteitenposter"));

		$path = $act->posterPath();
		if ($path == NULL) {
			$page->add(new HtmlParagraph(_("Er is nog geen poster ingesteld voor deze activiteit."), "text-center"));
		} else {
			$page->add(new HtmlDiv(array(
				new HtmlParagraph(_("Huidige poster:")),
				$img = new HtmlImage($act->url() . "/ActPoster", _("Huidige activiteitenposter"), _("Huidige activiteitenposter"))
			), "poster-preview"));

			$img->setLazyLoad();

			/* $page->add(new HtmlParagraph(_("Dit is de huidige poster ingesteld bij deze activiteit:"), "text-center"));
		
			$page->add($img = new HtmlImage($act->url() . "/ActPoster", _("Huidige activiteitenposter"), _("Huidige activiteitenposter"), "center-block"));
			$img->setCssStyle("max-width: 100%"); */
		}
		$page->add($form = new HtmlForm());
		$form->setAttribute("enctype", "multipart/form-data");
		$form->add($div = new HtmlDiv());
		$div->add($fileInput = HtmlInput::makeFile("poster", ACTPOSTERS_MAX_FILE_SIZE, false, "center-block"));
		$fileInput->setCssStyle("margin-top: 1em; display: block;");
		$div->add(HtmlInput::makeHidden("pasPosterAan","aye"));
		$div->add($submit = HtmlInput::makeSubmitButton(_("Wijzig poster!"), "submit", "center-block"));
		$submit->setCssStyle("margin-top:.5em;");

		return $page;
	}

	public static function addToCalendarButton(Activiteit $act)
	{
		$info = $act->getInformatie();

		$naam = urlencode(self::titel($act));
		$locatie = urlencode($info->getLocatie());
		$details = urlencode(strip_tags($info->getWerftekst()));
		$detailsEnLink = urlencode(ActiviteitInformatieView::getIcsWerftekst($act));

		// Google Calendar
		$gmomentBegin = $act->getMomentBegin()->kalenderFormaat('gcalendar');
		$gmomentEinde = $act->getMomentEind()->kalenderFormaat('gcalendar');
		$gurl = "https://www.google.com/calendar/event?action=TEMPLATE&text=$naam&dates=$gmomentBegin/$gmomentEinde&details=$detailsEnLink&location=$locatie";
		$gbutton = new HtmlAnchor($gurl, _('Voeg aan Google Calendar toe'));
		$gbutton->setAttribute('target', '_blank');
		
		// Outlook
		$omomentBegin = $act->getMomentBegin()->kalenderFormaat('outlook');
		$omomentEinde = $act->getMomentEind()->kalenderFormaat('outlook');
		$ourl = "https://outlook.live.com/owa/?path=/calendar/action/compose&rru=addevent&subject=$naam&startdt=$omomentBegin&enddt=$omomentEinde&body=$details&location=$locatie";
		$obutton = new HtmlAnchor($ourl, _('Voeg aan Outlook toe'));
		$obutton->setAttribute('target', '_blank');

		$ibutton = new HtmlAnchor($act->url() . "/ics", _('Genereer iCal-bestand'));
		$ibutton->setAttribute('target', '_blank');

		$dropdown = new HtmlButton('button', array(
			_('Voeg aan kalender toe'),
			new HtmlSpan('', 'caret')
		), null, null, 'btn btn-primary');
		$dropdown->setAttribute('onclick', 'event.stopPropagation(); $(this).next().toggle();');
		$dropdown->setAttribute('data-toggle', 'dropdown');
		$dropdownMenu = new HtmlList(false, array(
			new HtmlListItem($gbutton, null, null, false),
			new HtmlListItem($obutton, null, null, false),
			new HtmlListItem($ibutton, null, null, false)
		), 'dropdown-menu');
		return new HtmlDiv(array($dropdown, $dropdownMenu), 'btn-group');
	}

	/**
	 *  Geeft een RSS-item terug van een activiteit
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return Rss_Item Het rss-item
	 */
	static public function rssItem (Activiteit $act)
	{
		$info = $act->getInformatie();
		$item = new Rss_Item();
		$item->setTitle($act->getMomentBegin()->strftime('%a %e %b %y: ')
						. self::titel($act))
			->setDescription(ActiviteitInformatieView::waardeWerftekst($info))
			->setPubdate($act->getMomentBegin()->format(DateTime::RSS))
			->setLink(HTTPS_ROOT . $act->url());
		return $item;
	}

	public static function ics(Activiteit $act) {
		$page = new Ics_Page();
		$page
			->start(null, _("Alle aankomende activiteiten"))
			->add(self::VCalendarEvent($act));
		return $page;
	}

	/**
	 *  Geeft een VCalendar-event terug van een activiteit
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return VCalendar_Event Het vcalender-item
	 */
	static public function VCalendarEvent (Activiteit $act)
	{
		$info = $act->getInformatie();

		$begin = $act->getMomentBegin()->kalenderFormaat('vcalendar');
		$einde = $act->getMomentEind()->kalenderFormaat('vcalendar');
		$gewijzigd = $act->getGewijzigdWanneer()->kalenderFormaat('vcalendar');

		$item = new VCalendar_Event();
		$item->setDescription(ActiviteitInformatieView::getIcsWerftekst($act))
			->setDTStart($begin)
			->setDTEnd($einde)
			->setDTStamp($gewijzigd)
			->setOrganizer(CommissieVerzamelingView::kommaLijst($act->getCommissies(), false))
			->setSummary(self::titel($act))
			->setLocation($info->getLocatie())
			->setUID($act->geefID())
			->setUrl(HTTPS_ROOT . $act->url());
		return $item;
	}

	/**
	 *  Geeft de titel van de activiteit terug
	 *
	 * @param Activiteit $act De activiteit
	 *
	 * @return De titel als string
	 */
	static public function titel (Activiteit $act)
	{
		$class = get_class($act);
		switch($class)
		{
		case 'Activiteit':
			throw new BadMethodCallException('Een activiteit moet een extensie hebben!');
			break;
		case 'ActiviteitInformatie':
			return ActiviteitInformatieView::waardeTitel($act);
			break;
		case 'ActiviteitHerhaling':
			$act = $act->getInformatie();
			return sprintf("%s (%s)", ActiviteitInformatieView::waardeTitel($act), _("Herhaling"));
		}
		return static::defaultWaardeString($act, 'Titel');
	}

	/**
	 *  Geeft het aantalcomputers terug
	 *
	 * @param Activiteit $obj De activiteit
	 *
	 * @return Het aantal computers als string
	 */
	public static function aantalComputers(Activiteit $obj)
	{
		switch(get_class($obj))
		{
		case 'Activiteit':
			throw new BadMethodCallException('Een activiteit moet een extensie hebben!');
			break;
		case 'ActiviteitInformatie':
			return ActiviteitInformatieView::waardeAantalComputers($obj);
			break;
		case 'ActiviteitHerhaling':
			return ActiviteitInformatieView::waardeAantaLcomputers($obj->getInformatie());
			break;
		}
	}

	/** waarde-functies **/
	public static function waardeMomentBegin(Activiteit $obj, $form = 'default')
	{
		switch ($form)
		{
			case 'lijst': return $obj->getMomentBegin()->strftime("%e %b '%g");
			default: return self::outputDateTime($obj->getMomentBegin(), '%a %e %b %Y, %R');
		}
	}
	public static function waardeMomentEind(Activiteit $obj)
	{
		return self::outputDateTime($obj->getMomentEind(), '%a %e %b %Y, %R');
	}

	/** label-functies **/
	public static function labelToegang(Activiteit $obj)
	{
		return _('Zichtbaar');
	}
	public static function labelenumToegang($value)
	{
		switch ($value)
		{
			case 'PUBLIEK': return _('Openbaar');
			case 'PRIVE': return _('Intern');
		}
		return parent::labelenumToegang($value);
	}
	public static function labelMomentBegin(Activiteit $obj)
	{
		return _('Begin van de activiteit');
	}
	public static function labelMomentEind(Activiteit $obj)
	{
		return _('Einde van de activiteit');
	}
	static public function labelCommissies($obj)
	{
		$hasCies = 0;
		$hasDisps = 0;
		$hasGrps = 0;

		foreach($obj->getCommissies() as $cie)
		{
			switch($cie->getSoort())
			{
			case 'CIE':
				$hasCies++;
				break;
			case 'GROEP':
				$hasGrps++;
				break;
			case 'DISPUUT':
				$hasDisps++;
				break;
			}
		}

		$str = array();

		if ($hasCies > 1)
			$str[] = _('Commissies');
		else if ($hasCies == 1)
			$str[] = _('Commissie');

		if ($hasDisps > 1)
			$str[] = _('Disputen');
		else if ($hasDisps == 1)
			$str[] = _('Dispuut');

		if ($hasGrps > 1)
			$str[] = _('Groepen');
		else if ($hasGrps == 1)
			$str[] = _('Groep');

		return implode(', ', $str);
	}

	/**
	 *  Mapping van een activiteitsoort naar een symbool
	 *
	 * Deze functie mapt een activiteitsoort ('UU', 'ZUS', 'EXT', etc.) naar
	 * een symboolnummer. By default wordt het symbool voor
	 * A-Eskwadraatactiviteit geretourneerd.
	 *
	 * @param act De activiteit
	 *
	 * @return De symboolcode van de activiteitsoort van deze activiteit
	 **/
	static public function soortSymbool(Activiteit $act)
	{
		switch($act->getActsoort()) {
		case 'UU':		return '9679';
		case 'ZUS':		return '10012';
		case 'LAND':	return '9681';
		case '.COM':	return '8364';
		case 'EXT':		return '9762';
		case 'AES2':
		default:		return '9673';
		}
	}

	/**
	 *  Mapping van activiteitsoort naar omschrijving van die
	 * activiteitsoort
	 *
	 * Deze functie verzorgt een mapping van een soort activiteit (bijv. 'UU',
	 * 'ZUS', etc.) naar een uitgebreide omschrijving van die soort. Deze
	 * omschrijving is uiteraard onderhevig aan vertaling middels gettext
	 *
	 * @param act De activiteit
	 *
	 * @return Een string met de omschrijving van de activiteitsoort
	 **/
	static public function soortNaam(Activiteit $act)
	{
		switch($act->getActsoort()) {
		case 'UU':		return _('Activiteit van de Universiteit Utrecht');
		case 'ZUS':		return _('Activiteit georganiseerd door een zustervereniging van A–Eskwadraat');
		case 'LAND':	return _('Landelijke activiteit');
		case '.COM':	return _('Arbeidsmarktori&euml;ntatieactiviteit');
		case 'EXT':		return _('Externe activiteit');
		case 'AES2':
		default:		return _('A–Eskwadraatactiviteit');
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
