<?

/**
 * $Id$
 */
abstract class DeelnemerAntwoordView
	extends DeelnemerAntwoordView_Generated
{
	public static function labelDeelnemer(DeelnemerAntwoord $obj)
	{
		return _('Deelnemer');
	}
	public static function labelVraag(DeelnemerAntwoord $obj)
	{
		return _('Vraag');
	}
	public static function labelAntwoord(DeelnemerAntwoord $obj)
	{
		return _('Antwoord');
	}
	public static function formAntwoord(DeelnemerAntwoord $obj, $include_id = false)
	{
		if($obj->getVraag()->getType() == 'ENUM')
		{
			$name = 'DeelnemerAntwoord['.$obj->geefID().'][Antwoord]';
			$opties = $obj->getVraag()->getOpties();
			$opties = array(null => _('-- Selecteer een antwoord --')) + $opties;
			return HtmlSelectbox::fromArray($name, $opties, $obj->getAntwoord() ? array_search($obj->getAntwoord(), $opties) : array());
		}
		else
		{
			return static::defaultFormText($obj, 'Antwoord', null, $include_id);
		}
	}

	static public function processForm($obj, $velden = 'viewWijzig', $data = null, $include_id = false)
	{
		parent::processForm($obj, $velden, $data, $include_id);

		if($obj->getVraag()->getType() == 'ENUM')
		{
			$opties = $obj->getVraag()->getOpties();

			if(isset($data['Antwoord']) && isset($opties[$data['Antwoord']]))
				$obj->setAntwoord($opties[$data['Antwoord']]);
			else
				$obj->setAntwoord('');
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
