<?

abstract class BackgroundView
{

	/**
	 * @brief Maakt een formulier voor het toevoegen van een achtergrond
	 *
	 * @param msg De eventuele foutmelding
	 */
	static public function toevoegen($msg) {
		$page = Page::getInstance()->start(_("Achtergrond toevoegen"));

		if($msg)
			$page->add($msg);

		$form = HtmlForm::named('toevoegenbackground');
		$form->add($div = new HtmlDiv());
		$div->add($table = new HtmlTable());
		$table->add($tbody = new HtmlTableBody());

		$tbody->add($row = new HtmlTableRow());
		$row->addData(_('Foto ID'));
		$row->addData(HtmlInput::makeText('fotoId'));

		$form->add(HtmlInput::makeSubmitbutton(_('Toevoegen!')));

		$page->add(new HtmlParagraph(_('Hieronder kan je een achtergrondfoto toevoegen. Denk eraan dat je de code uit FotoWeb kopieert en in het veld zet zonder het hekje (#)')));

		$page->add($form);

		$page->add(new HtmlHR());

		$list = new HtmlList();
		$list->add(new HtmlListItem(new HtmlAnchor("/Background/", _('Terug naar overzicht'))));
		$list->add(new HtmlListItem(new HtmlAnchor("/Activiteiten/Fotos", _('FotoWeb'))));
		$page->add($list);

		$page->end();
	}
}
