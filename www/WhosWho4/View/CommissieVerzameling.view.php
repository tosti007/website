<?

/**
 * $Id$
 */
abstract class CommissieVerzamelingView
	extends CommissieVerzamelingView_Generated
{
	// Lijstje ,-seperated van commissies
	static public function kommaLijst (CommissieVerzameling $cies, $klikbaar = true)
	{
		if ($klikbaar)
			return implode(', ', CommissieVerzamelingView::klikbaarLijst($cies));

		$array = array();
		foreach ($cies as $cie)
			$array[] = CommissieView::waardeNaam($cie);
		return implode(', ', $array);
	}
	static public function kommaLoginLijst (CommissieVerzameling $cies, $klikbaar = true)
	{
		if ($klikbaar)
			return implode(', ', CommissieVerzamelingView::klikbaarLijst($cies));

		$array = array();
		foreach ($cies as $cie)
			if ($klikbaar)
				$array[] = CommissieView::makeLink($cie, CommissieView::waardeLogin($cie));
			else
				$array[] = CommissieView::waardeLogin($cie);
		return implode(', ', $array);
	}

	static public function overzichtLijst($cies)
	{
		if (!$cies || !$cies->aantal())
			return new HtmlEmphasis(_('Geen resultaten gevonden'));

		$kartCies = Kart::geef()->getCies();

		$div = new HtmlDiv();

		$i = 0;
		foreach($cies as $cie) {
			if(($i % 4) == 0)
			{
				$div->add($row = new HtmlDiv(null, 'row is-table-row'));

				$div->add(new HtmlBreak());
			}
			$row->add($cieDiv = new HtmlDiv(null, 'col-sm-3'));
			$cieDiv->add(CommissieView::overzichtLijstRegel($cie, $kartCies->bevat($cie)));
			$i++;
		}

		return $div;
	}

	/* Geeft een unordened list met links uit een verzameling commissies*/
	static public function lijstMetSpreuken(CommissieVerzameling $cies, Persoon $persoon, $class = null, $soort = 'ALLES')
	{
		$lijst = new HtmlList();
		if ($class != null)
			$lijst->addClass($class);

		$cieLidVerzameling = $persoon->getCommissieLids(false);
		foreach ($cies as $cie)
		{
			if($soort == 'ALLES' || $soort == $cie->getSoort())
			{
				$div = new HtmlDiv();
				$div->add($a = CommissieView::makeLink($cie));
				foreach($cieLidVerzameling as $cieLid) {
					if($cieLid->getCommissie() == $cie) {
						if($cieLid->getSpreukZichtbaar() && strlen($cieLid->getSpreuk()) > 0) {
							$div->add(new HtmlSmall(new HtmlEmphasis($cieLid->getSpreuk())));
						}
						break;
					}
				}
				$lijst->add($div);
			}
		}
		return $lijst;
	}

	/**
	 * Geeft een array met anchors terug
	 **/
	static public function klikbaarLijst ($cies)
	{
		$a = array();
		foreach ($cies as $cie)
			$a[] = CommissieView::makeLink($cie);
		return $a;
	}

	static public function kartLijst (CommissieVerzameling $cies, $prefix)
	{
		$kartCies = Kart::geef()->getCies();

		$table = new HtmlTable(NULL, 'sortable');
		$table->add($head = new HtmlTableHead());
		$table->add($body = new HtmlTableBody());

		$head->add($row = new HtmlTableRow());
		$checkbox = HtmlInput::makeCheckbox($prefix . '[InverteerCies]', True, NULL, 'InverteerCies');
		$checkbox->setAttribute('onclick', 'Kart_Overzicht_Inverteer_Cies()');
		$checkbox->setAttribute('title', _('(De)selecteer alle commissies'));
		$row->addData($checkbox, 'sorttable_nosort');
		$row->makeHeaderFromArray(array(
			_('Naam'),
			_('Email')));

		foreach ($cies as $cie)
		{
			$body->add(CommissieView::kartTableRow($cie, $prefix, $kartCies->bevat($cie)));
		}

		return $table;
	}

	// CSV view van lijst cies
	static public function csv($cies){

		foreach($cies as $cie){
			$csvarray[] = CommissieView::alsArray($cie);
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		fputcsv($output, array_keys($csvarray[0]));

		//Print alle inhoud voor de CSV
		foreach($csvarray as $cie){
			fputcsv($output, $cie);
		}

		fclose($output);

	}

	static public function latex (CommissieVerzameling $cies)
	{
		$latexarray = array();
		foreach ($cies as $cie)
		{
			 $latexarray[] = CommissieView::latex($cie);
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		fwrite($output, "%naam soort categorie thema omschrijving begindatum einddatum login email homepage\n");
		foreach($latexarray as $latexline)
		{
			fwrite($output, $latexline);
		}

		fclose($output);

	}

	/**
	 *  Maak een overzicht van alle huidige cies/groepen/disputen. We maken een
	 *  tabel met 3 datacellen in de breedte en gaan zo verder naar beneden.
	 */
	static public function alleLedenOverzicht($cies)
	{
		$table = new HtmlDiv();

		$aantal = $cies->aantal();

		$aantalrows = ceil($aantal/4);
		for($x = 0; $x < $aantalrows; $x++)
		{
			$row = new HtmlDiv(null, 'row');

			for($y = 1; $y <= 4 && $cies->valid(); $y++) //check valid voor positie in verzameling.
			{
				$cell = new HtmlDiv(null, 'col-md-3');

				$cie_c = $cies->current();
				$cell->add(new HtmlHeader(4, CommissieView::makeLink($cies->current())));
				$cell->add(CommissieView::ledenLijst($cies->current(), 'huidige'));
				$row->add($cell);
				$cies->next();
			}
			$table->add($row);
		}

		return $table;
	}

	static public function maakCsvArrayAlleLeden($cies){
		$aantal = $cies->aantal();
		$result = array();
		$commissieLijst = array();
		
		foreach($cies as $cie){
			$leden[CommissieView::waardeNaam($cie)] = CommissieView::ledenLijstArray($cies->current(), 'huidige');

			$commissieLijst[] = CommissieView::waardeNaam($cie);
		}


		for($i = 0; $i < 30; $i++){
			foreach($commissieLijst as $com){
				if(isset($leden[$com][$i])){
					$result[$i][$com] = $leden[$com][$i];
				}else{
					$result[$i][$com] = "noprdepopr";
				}
			}
		}

		return $result;
	}

	static public function maakCommissieSelect($commissieids) {
		$cies = array();
		foreach(array_filter($commissieids) as $c) {
			if(array_key_exists($c,$cies))
				continue;
			$cies[$c] = CommissieView::waardeNaam(Commissie::geef($c));
		}

		$selected = tryPar('commissies',array());

		$box = HtmlSelectbox::fromArray('commissies',$cies,$selected,5);
		$box->setMultiple();
		return $box;
	}

	static public function maakCommissieSelectBox ($naam, CommissieVerzameling $commissies, Commissie $geselecteerd = NULL, $legeWaarde = False)
	{
		$opts = array('' => '&lt;alle commissies&gt;');
		foreach ($commissies as $c)
		{
			$opts[$c->geefID()] = CommissieView::waardeNaam($c);
		}

		$geselecteerdID = $geselecteerd ? $geselecteerd->geefID() : NULL;
		$box = HtmlSelectbox::fromArray($naam, $opts, $geselecteerdID);
		return $box;
	}

	static public function maakCommissieBugSelectBox($naam, Commissie $geselecteerd = null)
	{
		$cies = Persoon::getIngelogd()->getCommissiesMetBugs();

		if($cies->aantal() <= 1) {
			return null;
		}

		$opts = array();
		foreach ($cies as $cie) {
			$opts[$cie->geefID()] = CommissieView::waardeNaam($cie);
		}

		if (empty($opts)) return false;
		return HtmlSelectbox::fromArray($naam, $opts, $geselecteerd ? $geselecteerd->geefID() : NULL);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
