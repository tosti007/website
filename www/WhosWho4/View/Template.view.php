<?
abstract class TemplateView
	extends TemplateView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in TemplateView.
	 *
	 * @param obj Het Template-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTemplate(Template $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef een HtmlAnchor om de template te wijzigen.
	 *
	 * @param template De naam van een template.
	 * @param text De tekst waar je op kan klikken.
	 *
	 * @return Een HTMLElement met de wijziglink.
	 */
	public static function wijzigLink($template, $text="Wijzig") {
		self::maakLoader();
		
		return self::$loader->wijzigLink($template, $text);
	}
}
