<?
/**
 * $Id$
 */
abstract class TentamenView
	extends TentamenView_Generated
{
	/**
	 * De tentamenbank homepage
	 *
	 * @param params de velden die ingevuld als mensen willen zoeken op tentamens
	 * @param tbc_studies Een array van de studies supported door de tbc
	 */
	public static function home($params, $tbc_studies) {
		$alleStudies = 'alles';

		$studie = $params['studie'];
		$toonOutdated = $params['outdated'];
		$zoektekst = $params['zoektekst'];
		$faseBA = $params['fase_ba'];
		$faseMA = $params['fase_ma'];

		// Zet wat algemeen geblaat neer
		$page = Page::getInstance()->start(_('Tentamens'));
		$page->add($infoDiv = new HtmlDiv(new HtmlParagraph(sprintf(
				_("Welkom op de pagina van de Tentamen Beheer Commissie! In deze "
				. "tentamenbank kun je digitale versies van allerlei tentamens "
				. "vinden. Mocht je zelf nog papieren tentamens hebben die niet "
				. "op de site staan: lever ze in! Dan zorgen wij dat ze online "
				. "komen. Heb je nog uitwerkingen die nog niet online staan? Deze "
				. "kan je zelf online zetten! Heb je nog uitwerkingen van een "
				. "tentamen waarvan het tentamen nog niet online staat? Stuur dan "
				. "een mail naar [VOC:e-mailadres van de TBC]%s. Dan zorgen wij "
				. "ervoor dat we hem online zetten.")
				, new HtmlAnchor('mailto:tbc@a-eskwadraat.nl', _('De TentamenBeheerCommissie'))
		)), "bs-callout bs-callout-info"));
		$infoDiv->add(new HtmlParagraph(sprintf(
				_("Mocht je wiskunde studeren en je mist hier nog wat tentamens, "
				. "kijk dan ook even in het [VOC:tentamenarchief van de UU]%s.")
				, new HtmlAnchor('http://students.uu.nl/beta/wiskunde/onderwijs/praktische-informatie-en-archief', _('tentamenarchief van de UU'))
		)));

		$page->add(new HtmlDiv(new HtmlParagraph(_('Maak hieronder een keuze voor welke studie je een tentamen wilt opzoeken:'))));

		$studie_options = array_merge(array($alleStudies => 'Alle tentamens'), $tbc_studies);

		// Maak het form aan om de studie te selecteren
		$page->add(new HtmlDiv($form = new HtmlForm("POST", null, true)));
		$form->setAttribute('name', 'studieselect');

		$form->add(new HtmlDiv(array(
			new HtmlLabel('studie', "Studie", "col-sm-2 control-label"),
			new HtmlDiv(HtmlSelectbox::fromArray('studie', $studie_options, array($studie), 1), "col-sm-10")
		), "form-group"));

		$form->add(new HtmlDiv(array(
			new HtmlLabel('zoektekst', _("Zoek op vakcode/naam"), "col-sm-2 control-label"),
			new HtmlDiv(HtmlInput::makeText("zoektekst", $zoektekst), "col-sm-10")
		), "form-group"));

		$form->add(new HtmlDiv(array(
			new HtmlDiv(new HtmlDiv(array(
				new HtmlLabel(null, array(
					HtmlInput::makeCheckbox('fase_ba', $faseBA),
					_("Bachelor vakken"),
					"&nbsp;"
				)),
				new HtmlLabel(null, array(
					HtmlInput::makeCheckbox('fase_ma', $faseMA),
					_("Master vakken")
				))
			), 'checkbox'), 'col-sm-offset-2 col-sm-10')
		), "form-group"));

		$form->add(self::makeFormBoolRow('outdated', null, "Toon ook de verouderde vakken"));
		$form->add(new HtmlDiv(new HtmlDiv(HtmlInput::makeSubmitButton("Zoek!"), "col-sm-offset-2 col-sm-10"), "form-group"));

		$page->add(new HtmlHR());

		$tentamens = TentamenVerzameling::vanStudieAantal($studie === $alleStudies ? null : strtoupper($studie), $toonOutdated, $zoektekst, $faseBA, $faseMA);

		$page->add(new HtmlDiv(new HtmlParagraph(new HtmlEmphasis(
			_('N.B. Docenten kunnen door de jaren heen verschillende '
			. 'accenten leggen binnen de stof, vertrouw daarom niet alleen op '
			. 'oude tentamens om je vak te halen!')))));

		if(hasAuth('tbc') && TentamenUitwerkingVerzameling::getAantalNietGecontroleerd() > 0)
		{
			$page->add(HtmlAnchor::button('/Onderwijs/Vak/Controleren', _('Uitwerkingen controleren')));
		}

		if(hasAuth('tbc')) {
			$page->add(HtmlAnchor::button('/Onderwijs/Vak/Nieuw', _("Vak toevoegen")));
		}

		// Begin de table met tentamens
		$page->add($table = new HtmlTable(null, 'sortable'));
		$table->add(new HtmlTableHead($head = new HtmlTableRow()))
			  ->add($tbody = new HtmlTableBody());

		$head->addHeader(_("Vakcode"));
		$head->addHeader(_("Vaknaam"));
		$head->addHeader(_("Aantal tentamens"));
		$head->addHeader(_("Opmerking"));

		foreach($tentamens as $t) {
			$vak = Vak::geef($t['vakID']);

			$tbody->add($row = new HtmlTableRow());
			$row->addData(new HtmlAnchor($vak->url(), $vak->getCode()));
			$row->addData($vak->getNaam());
			$row->addData($t['aantal']);
			$row->addData($vak->getOpmerking());
		}

		$page->end();
	}

	/*
	 *  Functie om een form voor het wijzigen van een tentamen te geven
	 *
	 * @param tent Het tentamen om te wijzigen
	 * @param msg Een dubbele array met mogelijke foutmeldingen
	 * @param toevoegen Of er een nieuw object gemaakt moet worden
	 * @param show_error Of de error moet worden laten zien
	 */
	static public function wijzigen(Tentamen $tent, $toevoegen = false, $show_error = true) {

		if($toevoegen) {
			$page = Page::getInstance()->start(_("Tentamen toevoegen"));
		} else {
			$page = Page::getInstance()->start(_('Wijzigen')
				. ' ' .TentamenView::waardeNaam($tent) 
				. ' ' . VakView::waardeNaam($tent->getVak()) 
				. ' ' . TentamenView::waardeDatum($tent));
		}

		$page->add(HtmlAnchor::button($tent->getVak()->url(), 'Terug'));

		if($toevoegen)
			$form = HtmlForm::named('toevoegentent');
		else
			$form = HtmlForm::named('wijzigtent');

		$form->setAttribute('enctype', "multipart/form-data");
		$page->add($form);

		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($tent, 'naam', $show_error));
		$div->add(self::wijzigTR($tent, 'datum', $show_error));
		$div->add(self::wijzigTR($tent, 'oud', $show_error));
		$div->add(self::wijzigTR($tent, 'opmerking', $show_error));

		// Hierzo stoppen we het file-upload-veld in het form
		$div->add(self::wijzigTR($tent, 'bestand', $show_error));

		if($toevoegen)
			$form->add(HtmlInput::makeFormSubmitbutton(_('Toevoegen!')));
		else
			$form->add(HtmlInput::makeFormSubmitbutton(_('Wijzigen!')));
		$page->end();
	}

	static public function labelBestand()
	{
		return _('Bestand');
	}

	static public function opmerkingBestand()
	{
		return '';
	}

	static public function formBestand()
	{
		return HtmlInput::makeFile('file', 1);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
