<?
abstract class PapierMolenVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PapierMolenVerzamelingView.
	 *
	 * @param PapierMolenVerzameling $obj Het PapierMolenVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePapierMolenVerzameling(PapierMolenVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
