<?
abstract class PlannerDataVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PlannerDataVerzamelingView.
	 *
	 * @param PlannerDataVerzameling $obj Het PlannerDataVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePlannerDataVerzameling(PlannerDataVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
