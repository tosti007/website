<?
abstract class LidView_Generated
	extends PersoonView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LidView.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLid(Lid $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld lidToestand.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lidToestand labelt.
	 */
	public static function labelLidToestand(Lid $obj)
	{
		return 'LidToestand';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld lidToestand.
	 *
	 * @param string $value Een enum-waarde van het veld lidToestand.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumLidToestand($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld lidToestand horen.
	 *
	 * @see labelenumLidToestand
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld lidToestand
	 * representeren.
	 */
	public static function labelenumLidToestandArray()
	{
		$soorten = array();
		foreach(Lid::enumsLidToestand() as $id)
			$soorten[$id] = LidView::labelenumLidToestand($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld lidToestand.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lidToestand van het object
	 * obj representeert.
	 */
	public static function waardeLidToestand(Lid $obj)
	{
		return static::defaultWaardeEnum($obj, 'LidToestand');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lidToestand.
	 *
	 * @see genericFormlidToestand
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidToestand staat en kan
	 * worden bewerkt. Indien lidToestand read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLidToestand(Lid $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'LidToestand', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lidToestand. In
	 * tegenstelling tot formlidToestand moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formlidToestand
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidToestand staat en kan
	 * worden bewerkt. Indien lidToestand read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLidToestand($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'LidToestand', Lid::enumslidToestand());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * lidToestand bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lidToestand representeert.
	 */
	public static function opmerkingLidToestand()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lidMaster.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lidMaster labelt.
	 */
	public static function labelLidMaster(Lid $obj)
	{
		return 'LidMaster';
	}
	/**
	 * @brief Geef de waarde van het veld lidMaster.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lidMaster van het object obj
	 * representeert.
	 */
	public static function waardeLidMaster(Lid $obj)
	{
		return static::defaultWaardeBool($obj, 'LidMaster');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lidMaster.
	 *
	 * @see genericFormlidMaster
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidMaster staat en kan
	 * worden bewerkt. Indien lidMaster read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLidMaster(Lid $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'LidMaster', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lidMaster. In
	 * tegenstelling tot formlidMaster moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formlidMaster
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidMaster staat en kan
	 * worden bewerkt. Indien lidMaster read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLidMaster($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'LidMaster');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * lidMaster bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lidMaster representeert.
	 */
	public static function opmerkingLidMaster()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lidExchange.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lidExchange labelt.
	 */
	public static function labelLidExchange(Lid $obj)
	{
		return 'LidExchange';
	}
	/**
	 * @brief Geef de waarde van het veld lidExchange.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lidExchange van het object
	 * obj representeert.
	 */
	public static function waardeLidExchange(Lid $obj)
	{
		return static::defaultWaardeBool($obj, 'LidExchange');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lidExchange.
	 *
	 * @see genericFormlidExchange
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidExchange staat en kan
	 * worden bewerkt. Indien lidExchange read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLidExchange(Lid $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'LidExchange', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lidExchange. In
	 * tegenstelling tot formlidExchange moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formlidExchange
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidExchange staat en kan
	 * worden bewerkt. Indien lidExchange read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLidExchange($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'LidExchange');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * lidExchange bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lidExchange representeert.
	 */
	public static function opmerkingLidExchange()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lidVan.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lidVan labelt.
	 */
	public static function labelLidVan(Lid $obj)
	{
		return 'LidVan';
	}
	/**
	 * @brief Geef de waarde van het veld lidVan.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lidVan van het object obj
	 * representeert.
	 */
	public static function waardeLidVan(Lid $obj)
	{
		return static::defaultWaardeDate($obj, 'LidVan');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lidVan.
	 *
	 * @see genericFormlidVan
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidVan staat en kan worden
	 * bewerkt. Indien lidVan read-only is betreft het een statisch html-element.
	 */
	public static function formLidVan(Lid $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'LidVan', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lidVan. In
	 * tegenstelling tot formlidVan moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlidVan
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidVan staat en kan worden
	 * bewerkt. Indien lidVan read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLidVan($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'LidVan');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lidVan
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lidVan representeert.
	 */
	public static function opmerkingLidVan()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld lidTot.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld lidTot labelt.
	 */
	public static function labelLidTot(Lid $obj)
	{
		return 'LidTot';
	}
	/**
	 * @brief Geef de waarde van het veld lidTot.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld lidTot van het object obj
	 * representeert.
	 */
	public static function waardeLidTot(Lid $obj)
	{
		return static::defaultWaardeDate($obj, 'LidTot');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld lidTot.
	 *
	 * @see genericFormlidTot
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidTot staat en kan worden
	 * bewerkt. Indien lidTot read-only is betreft het een statisch html-element.
	 */
	public static function formLidTot(Lid $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'LidTot', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld lidTot. In
	 * tegenstelling tot formlidTot moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlidTot
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld lidTot staat en kan worden
	 * bewerkt. Indien lidTot read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLidTot($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'LidTot');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld lidTot
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld lidTot representeert.
	 */
	public static function opmerkingLidTot()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld studentnr.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld studentnr labelt.
	 */
	public static function labelStudentnr(Lid $obj)
	{
		return 'Studentnr';
	}
	/**
	 * @brief Geef de waarde van het veld studentnr.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld studentnr van het object obj
	 * representeert.
	 */
	public static function waardeStudentnr(Lid $obj)
	{
		return static::defaultWaardeString($obj, 'Studentnr');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld studentnr.
	 *
	 * @see genericFormstudentnr
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studentnr staat en kan
	 * worden bewerkt. Indien studentnr read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formStudentnr(Lid $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Studentnr', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld studentnr. In
	 * tegenstelling tot formstudentnr moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formstudentnr
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studentnr staat en kan
	 * worden bewerkt. Indien studentnr read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormStudentnr($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Studentnr', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * studentnr bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld studentnr representeert.
	 */
	public static function opmerkingStudentnr()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opzoekbaar.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opzoekbaar labelt.
	 */
	public static function labelOpzoekbaar(Lid $obj)
	{
		return 'Opzoekbaar';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld opzoekbaar.
	 *
	 * @param string $value Een enum-waarde van het veld opzoekbaar.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumOpzoekbaar($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld opzoekbaar horen.
	 *
	 * @see labelenumOpzoekbaar
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld opzoekbaar
	 * representeren.
	 */
	public static function labelenumOpzoekbaarArray()
	{
		$soorten = array();
		foreach(Lid::enumsOpzoekbaar() as $id)
			$soorten[$id] = LidView::labelenumOpzoekbaar($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld opzoekbaar.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opzoekbaar van het object obj
	 * representeert.
	 */
	public static function waardeOpzoekbaar(Lid $obj)
	{
		return static::defaultWaardeEnum($obj, 'Opzoekbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opzoekbaar.
	 *
	 * @see genericFormopzoekbaar
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opzoekbaar staat en kan
	 * worden bewerkt. Indien opzoekbaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpzoekbaar(Lid $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Opzoekbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opzoekbaar. In
	 * tegenstelling tot formopzoekbaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopzoekbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opzoekbaar staat en kan
	 * worden bewerkt. Indien opzoekbaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpzoekbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Opzoekbaar', Lid::enumsopzoekbaar());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opzoekbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opzoekbaar representeert.
	 */
	public static function opmerkingOpzoekbaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inAlmanak.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inAlmanak labelt.
	 */
	public static function labelInAlmanak(Lid $obj)
	{
		return 'InAlmanak';
	}
	/**
	 * @brief Geef de waarde van het veld inAlmanak.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inAlmanak van het object obj
	 * representeert.
	 */
	public static function waardeInAlmanak(Lid $obj)
	{
		return static::defaultWaardeBool($obj, 'InAlmanak');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inAlmanak.
	 *
	 * @see genericForminAlmanak
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inAlmanak staat en kan
	 * worden bewerkt. Indien inAlmanak read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formInAlmanak(Lid $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'InAlmanak', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inAlmanak. In
	 * tegenstelling tot forminAlmanak moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see forminAlmanak
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inAlmanak staat en kan
	 * worden bewerkt. Indien inAlmanak read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormInAlmanak($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'InAlmanak');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * inAlmanak bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inAlmanak representeert.
	 */
	public static function opmerkingInAlmanak()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld planetRSS.
	 *
	 * @param Lid $obj Het Lid-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld planetRSS labelt.
	 */
	public static function labelPlanetRSS(Lid $obj)
	{
		return 'PlanetRSS';
	}
	/**
	 * @brief Geef de waarde van het veld planetRSS.
	 *
	 * @param Lid $obj Het Lid-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld planetRSS van het object obj
	 * representeert.
	 */
	public static function waardePlanetRSS(Lid $obj)
	{
		return static::defaultWaardeString($obj, 'PlanetRSS');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld planetRSS.
	 *
	 * @see genericFormplanetRSS
	 *
	 * @param Lid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld planetRSS staat en kan
	 * worden bewerkt. Indien planetRSS read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPlanetRSS(Lid $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'PlanetRSS', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld planetRSS. In
	 * tegenstelling tot formplanetRSS moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formplanetRSS
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld planetRSS staat en kan
	 * worden bewerkt. Indien planetRSS read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPlanetRSS($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'PlanetRSS', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * planetRSS bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld planetRSS representeert.
	 */
	public static function opmerkingPlanetRSS()
	{
		return NULL;
	}
}
