<?
abstract class PersoonVerzamelingView_Generated
	extends ContactVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonVerzamelingView.
	 *
	 * @param PersoonVerzameling $obj Het PersoonVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonVerzameling(PersoonVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
