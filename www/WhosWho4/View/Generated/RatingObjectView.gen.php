<?
abstract class RatingObjectView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in RatingObjectView.
	 *
	 * @param RatingObject $obj Het RatingObject-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeRatingObject(RatingObject $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld ratingObjectID.
	 *
	 * @param RatingObject $obj Het RatingObject-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld ratingObjectID labelt.
	 */
	public static function labelRatingObjectID(RatingObject $obj)
	{
		return 'RatingObjectID';
	}
	/**
	 * @brief Geef de waarde van het veld ratingObjectID.
	 *
	 * @param RatingObject $obj Het RatingObject-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ratingObjectID van het object
	 * obj representeert.
	 */
	public static function waardeRatingObjectID(RatingObject $obj)
	{
		return static::defaultWaardeInt($obj, 'RatingObjectID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * ratingObjectID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ratingObjectID representeert.
	 */
	public static function opmerkingRatingObjectID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld rating.
	 *
	 * @param RatingObject $obj Het RatingObject-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld rating labelt.
	 */
	public static function labelRating(RatingObject $obj)
	{
		return 'Rating';
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @param RatingObject $obj Het RatingObject-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld rating van het object obj
	 * representeert.
	 */
	public static function waardeRating(RatingObject $obj)
	{
		return static::defaultWaardeInt($obj, 'Rating');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld rating.
	 *
	 * @see genericFormrating
	 *
	 * @param RatingObject $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rating staat en kan worden
	 * bewerkt. Indien rating read-only is betreft het een statisch html-element.
	 */
	public static function formRating(RatingObject $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'Rating', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld rating. In
	 * tegenstelling tot formrating moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formrating
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld rating staat en kan worden
	 * bewerkt. Indien rating read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormRating($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'Rating');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld rating
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld rating representeert.
	 */
	public static function opmerkingRating()
	{
		return NULL;
	}
}
