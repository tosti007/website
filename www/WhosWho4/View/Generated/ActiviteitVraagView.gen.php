<?
abstract class ActiviteitVraagView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitVraagView.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitVraag(ActiviteitVraag $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld activiteit.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld activiteit labelt.
	 */
	public static function labelActiviteit(ActiviteitVraag $obj)
	{
		return 'Activiteit';
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld activiteit van het object obj
	 * representeert.
	 */
	public static function waardeActiviteit(ActiviteitVraag $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getActiviteit())
			return NULL;
		return ActiviteitView::defaultWaardeActiviteit($obj->getActiviteit());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * activiteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld activiteit representeert.
	 */
	public static function opmerkingActiviteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vraagID.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld vraagID labelt.
	 */
	public static function labelVraagID(ActiviteitVraag $obj)
	{
		return 'VraagID';
	}
	/**
	 * @brief Geef de waarde van het veld vraagID.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vraagID van het object obj
	 * representeert.
	 */
	public static function waardeVraagID(ActiviteitVraag $obj)
	{
		return static::defaultWaardeInt($obj, 'VraagID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vraagID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vraagID representeert.
	 */
	public static function opmerkingVraagID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld type.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld type labelt.
	 */
	public static function labelType(ActiviteitVraag $obj)
	{
		return 'Type';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld type.
	 *
	 * @param string $value Een enum-waarde van het veld type.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumType($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld type horen.
	 *
	 * @see labelenumType
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld type
	 * representeren.
	 */
	public static function labelenumTypeArray()
	{
		$soorten = array();
		foreach(ActiviteitVraag::enumsType() as $id)
			$soorten[$id] = ActiviteitVraagView::labelenumType($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld type.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld type van het object obj
	 * representeert.
	 */
	public static function waardeType(ActiviteitVraag $obj)
	{
		return static::defaultWaardeEnum($obj, 'Type');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld type.
	 *
	 * @see genericFormtype
	 *
	 * @param ActiviteitVraag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is betreft het een statisch html-element.
	 */
	public static function formType(ActiviteitVraag $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Type', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld type. In tegenstelling
	 * tot formtype moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtype
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld type staat en kan worden
	 * bewerkt. Indien type read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormType($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Type', ActiviteitVraag::enumstype());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld type
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld type representeert.
	 */
	public static function opmerkingType()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vraag.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld vraag labelt.
	 */
	public static function labelVraag(ActiviteitVraag $obj)
	{
		return 'Vraag';
	}
	/**
	 * @brief Geef de waarde van het veld vraag.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vraag van het object obj
	 * representeert.
	 */
	public static function waardeVraag(ActiviteitVraag $obj)
	{
		return static::defaultWaardeString($obj, 'Vraag');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vraag.
	 *
	 * @see genericFormvraag
	 *
	 * @param ActiviteitVraag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vraag staat en kan worden
	 * bewerkt. Indien vraag read-only is betreft het een statisch html-element.
	 */
	public static function formVraag(ActiviteitVraag $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Vraag', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Vraag', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vraag. In
	 * tegenstelling tot formvraag moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formvraag
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vraag staat en kan worden
	 * bewerkt. Indien vraag read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormVraag($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Vraag', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Vraag', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vraag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vraag representeert.
	 */
	public static function opmerkingVraag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opties.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld opties labelt.
	 */
	public static function labelOpties(ActiviteitVraag $obj)
	{
		return 'Opties';
	}
	/**
	 * @brief Geef de waarde van het veld opties.
	 *
	 * @param ActiviteitVraag $obj Het ActiviteitVraag-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opties van het object obj
	 * representeert.
	 */
	public static function waardeOpties(ActiviteitVraag $obj)
	{
		return static::defaultWaardeText($obj, 'Opties');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opties.
	 *
	 * @see genericFormopties
	 *
	 * @param ActiviteitVraag $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opties staat en kan worden
	 * bewerkt. Indien opties read-only is betreft het een statisch html-element.
	 */
	public static function formOpties(ActiviteitVraag $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opties', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opties. In
	 * tegenstelling tot formopties moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formopties
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opties staat en kan worden
	 * bewerkt. Indien opties read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormOpties($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opties', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld opties
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opties representeert.
	 */
	public static function opmerkingOpties()
	{
		return NULL;
	}
}
