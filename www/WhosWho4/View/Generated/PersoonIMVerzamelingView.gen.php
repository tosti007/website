<?
abstract class PersoonIMVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonIMVerzamelingView.
	 *
	 * @param PersoonIMVerzameling $obj Het PersoonIMVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonIMVerzameling(PersoonIMVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
