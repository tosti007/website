<?
abstract class CommissieLidView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CommissieLidView.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCommissieLid(CommissieLid $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(CommissieLid $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(CommissieLid $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(CommissieLid $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(CommissieLid $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumBegin.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld datumBegin labelt.
	 */
	public static function labelDatumBegin(CommissieLid $obj)
	{
		return 'DatumBegin';
	}
	/**
	 * @brief Geef de waarde van het veld datumBegin.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumBegin van het object obj
	 * representeert.
	 */
	public static function waardeDatumBegin(CommissieLid $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumBegin.
	 *
	 * @see genericFormdatumBegin
	 *
	 * @param CommissieLid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumBegin(CommissieLid $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumBegin', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumBegin. In
	 * tegenstelling tot formdatumBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumBegin staat en kan
	 * worden bewerkt. Indien datumBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumBegin representeert.
	 */
	public static function opmerkingDatumBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumEind.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld datumEind labelt.
	 */
	public static function labelDatumEind(CommissieLid $obj)
	{
		return 'DatumEind';
	}
	/**
	 * @brief Geef de waarde van het veld datumEind.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumEind van het object obj
	 * representeert.
	 */
	public static function waardeDatumEind(CommissieLid $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumEind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumEind.
	 *
	 * @see genericFormdatumEind
	 *
	 * @param CommissieLid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDatumEind(CommissieLid $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumEind', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumEind. In
	 * tegenstelling tot formdatumEind moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formdatumEind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumEind staat en kan
	 * worden bewerkt. Indien datumEind read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDatumEind($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumEind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumEind bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumEind representeert.
	 */
	public static function opmerkingDatumEind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld spreuk.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld spreuk labelt.
	 */
	public static function labelSpreuk(CommissieLid $obj)
	{
		return 'Spreuk';
	}
	/**
	 * @brief Geef de waarde van het veld spreuk.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld spreuk van het object obj
	 * representeert.
	 */
	public static function waardeSpreuk(CommissieLid $obj)
	{
		return static::defaultWaardeString($obj, 'Spreuk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld spreuk.
	 *
	 * @see genericFormspreuk
	 *
	 * @param CommissieLid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld spreuk staat en kan worden
	 * bewerkt. Indien spreuk read-only is betreft het een statisch html-element.
	 */
	public static function formSpreuk(CommissieLid $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Spreuk', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld spreuk. In
	 * tegenstelling tot formspreuk moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formspreuk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld spreuk staat en kan worden
	 * bewerkt. Indien spreuk read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSpreuk($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Spreuk', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld spreuk
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld spreuk representeert.
	 */
	public static function opmerkingSpreuk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld spreukZichtbaar.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld spreukZichtbaar labelt.
	 */
	public static function labelSpreukZichtbaar(CommissieLid $obj)
	{
		return 'SpreukZichtbaar';
	}
	/**
	 * @brief Geef de waarde van het veld spreukZichtbaar.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld spreukZichtbaar van het
	 * object obj representeert.
	 */
	public static function waardeSpreukZichtbaar(CommissieLid $obj)
	{
		return static::defaultWaardeBool($obj, 'SpreukZichtbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld spreukZichtbaar.
	 *
	 * @see genericFormspreukZichtbaar
	 *
	 * @param CommissieLid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld spreukZichtbaar staat en
	 * kan worden bewerkt. Indien spreukZichtbaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formSpreukZichtbaar(CommissieLid $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'SpreukZichtbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld spreukZichtbaar. In
	 * tegenstelling tot formspreukZichtbaar moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formspreukZichtbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld spreukZichtbaar staat en
	 * kan worden bewerkt. Indien spreukZichtbaar read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormSpreukZichtbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'SpreukZichtbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * spreukZichtbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld spreukZichtbaar representeert.
	 */
	public static function opmerkingSpreukZichtbaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld functie.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld functie labelt.
	 */
	public static function labelFunctie(CommissieLid $obj)
	{
		return 'Functie';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld functie.
	 *
	 * @param string $value Een enum-waarde van het veld functie.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumFunctie($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld functie horen.
	 *
	 * @see labelenumFunctie
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld functie
	 * representeren.
	 */
	public static function labelenumFunctieArray()
	{
		$soorten = array();
		foreach(CommissieLid::enumsFunctie() as $id)
			$soorten[$id] = CommissieLidView::labelenumFunctie($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld functie.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld functie van het object obj
	 * representeert.
	 */
	public static function waardeFunctie(CommissieLid $obj)
	{
		return static::defaultWaardeEnum($obj, 'Functie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld functie.
	 *
	 * @see genericFormfunctie
	 *
	 * @param CommissieLid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld functie staat en kan
	 * worden bewerkt. Indien functie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formFunctie(CommissieLid $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Functie', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld functie. In
	 * tegenstelling tot formfunctie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formfunctie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld functie staat en kan
	 * worden bewerkt. Indien functie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormFunctie($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Functie', CommissieLid::enumsfunctie());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld functie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld functie representeert.
	 */
	public static function opmerkingFunctie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld functieNaam.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld functieNaam labelt.
	 */
	public static function labelFunctieNaam(CommissieLid $obj)
	{
		return 'FunctieNaam';
	}
	/**
	 * @brief Geef de waarde van het veld functieNaam.
	 *
	 * @param CommissieLid $obj Het CommissieLid-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld functieNaam van het object
	 * obj representeert.
	 */
	public static function waardeFunctieNaam(CommissieLid $obj)
	{
		return static::defaultWaardeString($obj, 'FunctieNaam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld functieNaam.
	 *
	 * @see genericFormfunctieNaam
	 *
	 * @param CommissieLid $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld functieNaam staat en kan
	 * worden bewerkt. Indien functieNaam read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formFunctieNaam(CommissieLid $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'FunctieNaam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld functieNaam. In
	 * tegenstelling tot formfunctieNaam moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formfunctieNaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld functieNaam staat en kan
	 * worden bewerkt. Indien functieNaam read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormFunctieNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'FunctieNaam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * functieNaam bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld functieNaam representeert.
	 */
	public static function opmerkingFunctieNaam()
	{
		return NULL;
	}
}
