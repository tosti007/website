<?
abstract class KartVoorwerpView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in KartVoorwerpView.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeKartVoorwerp(KartVoorwerp $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld kartVoorwerpID.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld kartVoorwerpID labelt.
	 */
	public static function labelKartVoorwerpID(KartVoorwerp $obj)
	{
		return 'KartVoorwerpID';
	}
	/**
	 * @brief Geef de waarde van het veld kartVoorwerpID.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kartVoorwerpID van het object
	 * obj representeert.
	 */
	public static function waardeKartVoorwerpID(KartVoorwerp $obj)
	{
		return static::defaultWaardeInt($obj, 'KartVoorwerpID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * kartVoorwerpID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kartVoorwerpID representeert.
	 */
	public static function opmerkingKartVoorwerpID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoonKart.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoonKart labelt.
	 */
	public static function labelPersoonKart(KartVoorwerp $obj)
	{
		return 'PersoonKart';
	}
	/**
	 * @brief Geef de waarde van het veld persoonKart.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoonKart van het object
	 * obj representeert.
	 */
	public static function waardePersoonKart(KartVoorwerp $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoonKart())
			return NULL;
		return PersoonKartView::defaultWaardePersoonKart($obj->getPersoonKart());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoonKart.
	 *
	 * @see genericFormpersoonKart
	 *
	 * @param KartVoorwerp $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoonKart staat en kan
	 * worden bewerkt. Indien persoonKart read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoonKart(KartVoorwerp $obj, $include_id = false)
	{
		return static::waardePersoonKart($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoonKart. In
	 * tegenstelling tot formpersoonKart moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formpersoonKart
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoonKart staat en kan
	 * worden bewerkt. Indien persoonKart read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoonKart($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * persoonKart bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoonKart representeert.
	 */
	public static function opmerkingPersoonKart()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(KartVoorwerp $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(KartVoorwerp $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld persoon.
	 *
	 * @see genericFormpersoon
	 *
	 * @param KartVoorwerp $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPersoon(KartVoorwerp $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld persoon. In
	 * tegenstelling tot formpersoon moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formpersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld persoon staat en kan
	 * worden bewerkt. Indien persoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('Persoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld commissie.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld commissie labelt.
	 */
	public static function labelCommissie(KartVoorwerp $obj)
	{
		return 'Commissie';
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld commissie van het object obj
	 * representeert.
	 */
	public static function waardeCommissie(KartVoorwerp $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCommissie())
			return NULL;
		return CommissieView::defaultWaardeCommissie($obj->getCommissie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld commissie.
	 *
	 * @see genericFormcommissie
	 *
	 * @param KartVoorwerp $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCommissie(KartVoorwerp $obj, $include_id = false)
	{
		return CommissieView::defaultForm($obj->getCommissie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld commissie. In
	 * tegenstelling tot formcommissie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcommissie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld commissie staat en kan
	 * worden bewerkt. Indien commissie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCommissie($name, $waarde=NULL)
	{
		return CommissieView::genericDefaultForm('Commissie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * commissie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld commissie representeert.
	 */
	public static function opmerkingCommissie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld media.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld media labelt.
	 */
	public static function labelMedia(KartVoorwerp $obj)
	{
		return 'Media';
	}
	/**
	 * @brief Geef de waarde van het veld media.
	 *
	 * @param KartVoorwerp $obj Het KartVoorwerp-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld media van het object obj
	 * representeert.
	 */
	public static function waardeMedia(KartVoorwerp $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMedia())
			return NULL;
		return MediaView::defaultWaardeMedia($obj->getMedia());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld media.
	 *
	 * @see genericFormmedia
	 *
	 * @param KartVoorwerp $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld media staat en kan worden
	 * bewerkt. Indien media read-only is betreft het een statisch html-element.
	 */
	public static function formMedia(KartVoorwerp $obj, $include_id = false)
	{
		return MediaView::defaultForm($obj->getMedia());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld media. In
	 * tegenstelling tot formmedia moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formmedia
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld media staat en kan worden
	 * bewerkt. Indien media read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormMedia($name, $waarde=NULL)
	{
		return MediaView::genericDefaultForm('Media');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld media
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld media representeert.
	 */
	public static function opmerkingMedia()
	{
		return NULL;
	}
}
