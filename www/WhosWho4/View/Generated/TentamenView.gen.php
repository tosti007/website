<?
abstract class TentamenView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TentamenView.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTentamen(Tentamen $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(Tentamen $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(Tentamen $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vak.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vak labelt.
	 */
	public static function labelVak(Tentamen $obj)
	{
		return 'Vak';
	}
	/**
	 * @brief Geef de waarde van het veld vak.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vak van het object obj
	 * representeert.
	 */
	public static function waardeVak(Tentamen $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVak())
			return NULL;
		return VakView::defaultWaardeVak($obj->getVak());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vak.
	 *
	 * @see genericFormvak
	 *
	 * @param Tentamen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vak staat en kan worden
	 * bewerkt. Indien vak read-only is betreft het een statisch html-element.
	 */
	public static function formVak(Tentamen $obj, $include_id = false)
	{
		return static::waardeVak($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vak. In tegenstelling
	 * tot formvak moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formvak
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vak staat en kan worden
	 * bewerkt. Indien vak read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormVak($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vak
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vak representeert.
	 */
	public static function opmerkingVak()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datum.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datum labelt.
	 */
	public static function labelDatum(Tentamen $obj)
	{
		return 'Datum';
	}
	/**
	 * @brief Geef de waarde van het veld datum.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datum van het object obj
	 * representeert.
	 */
	public static function waardeDatum(Tentamen $obj)
	{
		return static::defaultWaardeDate($obj, 'Datum');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datum.
	 *
	 * @see genericFormdatum
	 *
	 * @param Tentamen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datum staat en kan worden
	 * bewerkt. Indien datum read-only is betreft het een statisch html-element.
	 */
	public static function formDatum(Tentamen $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'Datum', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datum. In
	 * tegenstelling tot formdatum moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formdatum
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datum staat en kan worden
	 * bewerkt. Indien datum read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormDatum($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'Datum');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld datum
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datum representeert.
	 */
	public static function opmerkingDatum()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(Tentamen $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(Tentamen $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param Tentamen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(Tentamen $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld oud.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld oud labelt.
	 */
	public static function labelOud(Tentamen $obj)
	{
		return 'Oud';
	}
	/**
	 * @brief Geef de waarde van het veld oud.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld oud van het object obj
	 * representeert.
	 */
	public static function waardeOud(Tentamen $obj)
	{
		return static::defaultWaardeBool($obj, 'Oud');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld oud.
	 *
	 * @see genericFormoud
	 *
	 * @param Tentamen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld oud staat en kan worden
	 * bewerkt. Indien oud read-only is betreft het een statisch html-element.
	 */
	public static function formOud(Tentamen $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Oud', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld oud. In tegenstelling
	 * tot formoud moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formoud
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld oud staat en kan worden
	 * bewerkt. Indien oud read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormOud($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Oud');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld oud
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld oud representeert.
	 */
	public static function opmerkingOud()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tentamen.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tentamen labelt.
	 */
	public static function labelTentamen(Tentamen $obj)
	{
		return 'Tentamen';
	}
	/**
	 * @brief Geef de waarde van het veld tentamen.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tentamen van het object obj
	 * representeert.
	 */
	public static function waardeTentamen(Tentamen $obj)
	{
		return static::defaultWaardeBool($obj, 'Tentamen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tentamen.
	 *
	 * @see genericFormtentamen
	 *
	 * @param Tentamen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tentamen staat en kan
	 * worden bewerkt. Indien tentamen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTentamen(Tentamen $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Tentamen', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tentamen. In
	 * tegenstelling tot formtentamen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtentamen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tentamen staat en kan
	 * worden bewerkt. Indien tentamen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTentamen($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Tentamen');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tentamen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tentamen representeert.
	 */
	public static function opmerkingTentamen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld opmerking.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld opmerking labelt.
	 */
	public static function labelOpmerking(Tentamen $obj)
	{
		return 'Opmerking';
	}
	/**
	 * @brief Geef de waarde van het veld opmerking.
	 *
	 * @param Tentamen $obj Het Tentamen-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld opmerking van het object obj
	 * representeert.
	 */
	public static function waardeOpmerking(Tentamen $obj)
	{
		return static::defaultWaardeText($obj, 'Opmerking');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld opmerking.
	 *
	 * @see genericFormopmerking
	 *
	 * @param Tentamen $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOpmerking(Tentamen $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Opmerking', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld opmerking. In
	 * tegenstelling tot formopmerking moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formopmerking
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld opmerking staat en kan
	 * worden bewerkt. Indien opmerking read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOpmerking($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Opmerking', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * opmerking bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld opmerking representeert.
	 */
	public static function opmerkingOpmerking()
	{
		return NULL;
	}
}
