<?
abstract class VoornaamwoordVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VoornaamwoordVerzamelingView.
	 *
	 * @param VoornaamwoordVerzameling $obj Het VoornaamwoordVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoornaamwoordVerzameling(VoornaamwoordVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
