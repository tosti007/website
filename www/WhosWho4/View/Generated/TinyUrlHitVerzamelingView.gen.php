<?
abstract class TinyUrlHitVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TinyUrlHitVerzamelingView.
	 *
	 * @param TinyUrlHitVerzameling $obj Het TinyUrlHitVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTinyUrlHitVerzameling(TinyUrlHitVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
