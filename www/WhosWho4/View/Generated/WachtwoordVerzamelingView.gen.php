<?
abstract class WachtwoordVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in WachtwoordVerzamelingView.
	 *
	 * @param WachtwoordVerzameling $obj Het WachtwoordVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeWachtwoordVerzameling(WachtwoordVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
