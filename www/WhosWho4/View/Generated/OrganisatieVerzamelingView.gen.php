<?
abstract class OrganisatieVerzamelingView_Generated
	extends ContactVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in OrganisatieVerzamelingView.
	 *
	 * @param OrganisatieVerzameling $obj Het OrganisatieVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeOrganisatieVerzameling(OrganisatieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
