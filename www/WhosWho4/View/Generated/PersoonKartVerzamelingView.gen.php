<?
abstract class PersoonKartVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonKartVerzamelingView.
	 *
	 * @param PersoonKartVerzameling $obj Het PersoonKartVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonKartVerzameling(PersoonKartVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
