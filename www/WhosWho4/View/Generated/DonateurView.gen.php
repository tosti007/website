<?
abstract class DonateurView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DonateurView.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDonateur(Donateur $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Donateur $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Donateur $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld anderPersoon.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld anderPersoon labelt.
	 */
	public static function labelAnderPersoon(Donateur $obj)
	{
		return 'AnderPersoon';
	}
	/**
	 * @brief Geef de waarde van het veld anderPersoon.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld anderPersoon van het object
	 * obj representeert.
	 */
	public static function waardeAnderPersoon(Donateur $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getAnderPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getAnderPersoon());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld anderPersoon.
	 *
	 * @see genericFormanderPersoon
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld anderPersoon staat en kan
	 * worden bewerkt. Indien anderPersoon read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAnderPersoon(Donateur $obj, $include_id = false)
	{
		return PersoonView::defaultForm($obj->getAnderPersoon());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld anderPersoon. In
	 * tegenstelling tot formanderPersoon moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formanderPersoon
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld anderPersoon staat en kan
	 * worden bewerkt. Indien anderPersoon read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAnderPersoon($name, $waarde=NULL)
	{
		return PersoonView::genericDefaultForm('AnderPersoon');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * anderPersoon bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld anderPersoon representeert.
	 */
	public static function opmerkingAnderPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aanhef.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld aanhef labelt.
	 */
	public static function labelAanhef(Donateur $obj)
	{
		return 'Aanhef';
	}
	/**
	 * @brief Geef de waarde van het veld aanhef.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aanhef van het object obj
	 * representeert.
	 */
	public static function waardeAanhef(Donateur $obj)
	{
		return static::defaultWaardeString($obj, 'Aanhef');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aanhef.
	 *
	 * @see genericFormaanhef
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aanhef staat en kan worden
	 * bewerkt. Indien aanhef read-only is betreft het een statisch html-element.
	 */
	public static function formAanhef(Donateur $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Aanhef', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aanhef. In
	 * tegenstelling tot formaanhef moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formaanhef
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aanhef staat en kan worden
	 * bewerkt. Indien aanhef read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormAanhef($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Aanhef', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld aanhef
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aanhef representeert.
	 */
	public static function opmerkingAanhef()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld jaarBegin.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld jaarBegin labelt.
	 */
	public static function labelJaarBegin(Donateur $obj)
	{
		return 'JaarBegin';
	}
	/**
	 * @brief Geef de waarde van het veld jaarBegin.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld jaarBegin van het object obj
	 * representeert.
	 */
	public static function waardeJaarBegin(Donateur $obj)
	{
		return static::defaultWaardeInt($obj, 'JaarBegin');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld jaarBegin.
	 *
	 * @see genericFormjaarBegin
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaarBegin staat en kan
	 * worden bewerkt. Indien jaarBegin read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formJaarBegin(Donateur $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'JaarBegin', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld jaarBegin. In
	 * tegenstelling tot formjaarBegin moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formjaarBegin
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaarBegin staat en kan
	 * worden bewerkt. Indien jaarBegin read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormJaarBegin($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'JaarBegin');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * jaarBegin bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld jaarBegin representeert.
	 */
	public static function opmerkingJaarBegin()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld jaarEind.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld jaarEind labelt.
	 */
	public static function labelJaarEind(Donateur $obj)
	{
		return 'JaarEind';
	}
	/**
	 * @brief Geef de waarde van het veld jaarEind.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld jaarEind van het object obj
	 * representeert.
	 */
	public static function waardeJaarEind(Donateur $obj)
	{
		return static::defaultWaardeInt($obj, 'JaarEind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld jaarEind.
	 *
	 * @see genericFormjaarEind
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaarEind staat en kan
	 * worden bewerkt. Indien jaarEind read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formJaarEind(Donateur $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'JaarEind', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld jaarEind. In
	 * tegenstelling tot formjaarEind moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formjaarEind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaarEind staat en kan
	 * worden bewerkt. Indien jaarEind read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormJaarEind($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'JaarEind');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * jaarEind bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld jaarEind representeert.
	 */
	public static function opmerkingJaarEind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld donatie.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld donatie labelt.
	 */
	public static function labelDonatie(Donateur $obj)
	{
		return 'Donatie';
	}
	/**
	 * @brief Geef de waarde van het veld donatie.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld donatie van het object obj
	 * representeert.
	 */
	public static function waardeDonatie(Donateur $obj)
	{
		return static::defaultWaardeMoney($obj, 'Donatie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld donatie.
	 *
	 * @see genericFormdonatie
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld donatie staat en kan
	 * worden bewerkt. Indien donatie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formDonatie(Donateur $obj, $include_id = false)
	{
		return static::defaultFormMoney($obj, 'Donatie', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld donatie. In
	 * tegenstelling tot formdonatie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formdonatie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld donatie staat en kan
	 * worden bewerkt. Indien donatie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormDonatie($name, $waarde=NULL)
	{
		return static::genericDefaultFormMoney($name, $waarde, 'Donatie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld donatie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld donatie representeert.
	 */
	public static function opmerkingDonatie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld machtiging.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld machtiging labelt.
	 */
	public static function labelMachtiging(Donateur $obj)
	{
		return 'Machtiging';
	}
	/**
	 * @brief Geef de waarde van het veld machtiging.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld machtiging van het object obj
	 * representeert.
	 */
	public static function waardeMachtiging(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'Machtiging');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld machtiging.
	 *
	 * @see genericFormmachtiging
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld machtiging staat en kan
	 * worden bewerkt. Indien machtiging read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMachtiging(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Machtiging', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld machtiging. In
	 * tegenstelling tot formmachtiging moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmachtiging
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld machtiging staat en kan
	 * worden bewerkt. Indien machtiging read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMachtiging($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Machtiging');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * machtiging bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld machtiging representeert.
	 */
	public static function opmerkingMachtiging()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld machtigingOud.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld machtigingOud labelt.
	 */
	public static function labelMachtigingOud(Donateur $obj)
	{
		return 'MachtigingOud';
	}
	/**
	 * @brief Geef de waarde van het veld machtigingOud.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld machtigingOud van het object
	 * obj representeert.
	 */
	public static function waardeMachtigingOud(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'MachtigingOud');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld machtigingOud.
	 *
	 * @see genericFormmachtigingOud
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld machtigingOud staat en kan
	 * worden bewerkt. Indien machtigingOud read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMachtigingOud(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'MachtigingOud', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld machtigingOud. In
	 * tegenstelling tot formmachtigingOud moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmachtigingOud
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld machtigingOud staat en kan
	 * worden bewerkt. Indien machtigingOud read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMachtigingOud($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'MachtigingOud');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * machtigingOud bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld machtigingOud representeert.
	 */
	public static function opmerkingMachtigingOud()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld almanak.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld almanak labelt.
	 */
	public static function labelAlmanak(Donateur $obj)
	{
		return 'Almanak';
	}
	/**
	 * @brief Geef de waarde van het veld almanak.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld almanak van het object obj
	 * representeert.
	 */
	public static function waardeAlmanak(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'Almanak');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld almanak.
	 *
	 * @see genericFormalmanak
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld almanak staat en kan
	 * worden bewerkt. Indien almanak read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAlmanak(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Almanak', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld almanak. In
	 * tegenstelling tot formalmanak moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formalmanak
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld almanak staat en kan
	 * worden bewerkt. Indien almanak read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAlmanak($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Almanak');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld almanak
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld almanak representeert.
	 */
	public static function opmerkingAlmanak()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld avNotulen.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld avNotulen labelt.
	 */
	public static function labelAvNotulen(Donateur $obj)
	{
		return 'AvNotulen';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld avNotulen.
	 *
	 * @param string $value Een enum-waarde van het veld avNotulen.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumAvNotulen($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld avNotulen horen.
	 *
	 * @see labelenumAvNotulen
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld avNotulen
	 * representeren.
	 */
	public static function labelenumAvNotulenArray()
	{
		$soorten = array();
		foreach(Donateur::enumsAvNotulen() as $id)
			$soorten[$id] = DonateurView::labelenumAvNotulen($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld avNotulen.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld avNotulen van het object obj
	 * representeert.
	 */
	public static function waardeAvNotulen(Donateur $obj)
	{
		return static::defaultWaardeEnum($obj, 'AvNotulen');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld avNotulen.
	 *
	 * @see genericFormavNotulen
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld avNotulen staat en kan
	 * worden bewerkt. Indien avNotulen read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAvNotulen(Donateur $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'AvNotulen', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld avNotulen. In
	 * tegenstelling tot formavNotulen moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formavNotulen
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld avNotulen staat en kan
	 * worden bewerkt. Indien avNotulen read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAvNotulen($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'AvNotulen', Donateur::enumsavNotulen());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * avNotulen bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld avNotulen representeert.
	 */
	public static function opmerkingAvNotulen()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld jaarverslag.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld jaarverslag labelt.
	 */
	public static function labelJaarverslag(Donateur $obj)
	{
		return 'Jaarverslag';
	}
	/**
	 * @brief Geef de waarde van het veld jaarverslag.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld jaarverslag van het object
	 * obj representeert.
	 */
	public static function waardeJaarverslag(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'Jaarverslag');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld jaarverslag.
	 *
	 * @see genericFormjaarverslag
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaarverslag staat en kan
	 * worden bewerkt. Indien jaarverslag read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formJaarverslag(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Jaarverslag', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld jaarverslag. In
	 * tegenstelling tot formjaarverslag moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formjaarverslag
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld jaarverslag staat en kan
	 * worden bewerkt. Indien jaarverslag read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormJaarverslag($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Jaarverslag');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * jaarverslag bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld jaarverslag representeert.
	 */
	public static function opmerkingJaarverslag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld studiereis.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld studiereis labelt.
	 */
	public static function labelStudiereis(Donateur $obj)
	{
		return 'Studiereis';
	}
	/**
	 * @brief Geef de waarde van het veld studiereis.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld studiereis van het object obj
	 * representeert.
	 */
	public static function waardeStudiereis(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'Studiereis');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld studiereis.
	 *
	 * @see genericFormstudiereis
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studiereis staat en kan
	 * worden bewerkt. Indien studiereis read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formStudiereis(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Studiereis', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld studiereis. In
	 * tegenstelling tot formstudiereis moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formstudiereis
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld studiereis staat en kan
	 * worden bewerkt. Indien studiereis read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormStudiereis($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Studiereis');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * studiereis bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld studiereis representeert.
	 */
	public static function opmerkingStudiereis()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld symposium.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld symposium labelt.
	 */
	public static function labelSymposium(Donateur $obj)
	{
		return 'Symposium';
	}
	/**
	 * @brief Geef de waarde van het veld symposium.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld symposium van het object obj
	 * representeert.
	 */
	public static function waardeSymposium(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'Symposium');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld symposium.
	 *
	 * @see genericFormsymposium
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld symposium staat en kan
	 * worden bewerkt. Indien symposium read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formSymposium(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Symposium', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld symposium. In
	 * tegenstelling tot formsymposium moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formsymposium
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld symposium staat en kan
	 * worden bewerkt. Indien symposium read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormSymposium($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Symposium');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * symposium bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld symposium representeert.
	 */
	public static function opmerkingSymposium()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vakid.
	 *
	 * @param Donateur $obj Het Donateur-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vakid labelt.
	 */
	public static function labelVakid(Donateur $obj)
	{
		return 'Vakid';
	}
	/**
	 * @brief Geef de waarde van het veld vakid.
	 *
	 * @param Donateur $obj Het Donateur-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vakid van het object obj
	 * representeert.
	 */
	public static function waardeVakid(Donateur $obj)
	{
		return static::defaultWaardeBool($obj, 'Vakid');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld vakid.
	 *
	 * @see genericFormvakid
	 *
	 * @param Donateur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vakid staat en kan worden
	 * bewerkt. Indien vakid read-only is betreft het een statisch html-element.
	 */
	public static function formVakid(Donateur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Vakid', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld vakid. In
	 * tegenstelling tot formvakid moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formvakid
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld vakid staat en kan worden
	 * bewerkt. Indien vakid read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormVakid($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Vakid');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vakid
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vakid representeert.
	 */
	public static function opmerkingVakid()
	{
		return NULL;
	}
}
