<?
abstract class PlannerDeelnemerVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PlannerDeelnemerVerzamelingView.
	 *
	 * @param PlannerDeelnemerVerzameling $obj Het PlannerDeelnemerVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePlannerDeelnemerVerzameling(PlannerDeelnemerVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
