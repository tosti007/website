<?
abstract class TentamenVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TentamenVerzamelingView.
	 *
	 * @param TentamenVerzameling $obj Het TentamenVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTentamenVerzameling(TentamenVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
