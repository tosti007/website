<?
abstract class LidStudieVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in LidStudieVerzamelingView.
	 *
	 * @param LidStudieVerzameling $obj Het LidStudieVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeLidStudieVerzameling(LidStudieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
