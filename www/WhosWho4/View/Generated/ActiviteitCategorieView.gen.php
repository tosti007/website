<?
abstract class ActiviteitCategorieView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitCategorieView.
	 *
	 * @param ActiviteitCategorie $obj Het ActiviteitCategorie-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitCategorie(ActiviteitCategorie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld actCategorieID.
	 *
	 * @param ActiviteitCategorie $obj Het ActiviteitCategorie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld actCategorieID labelt.
	 */
	public static function labelActCategorieID(ActiviteitCategorie $obj)
	{
		return 'ActCategorieID';
	}
	/**
	 * @brief Geef de waarde van het veld actCategorieID.
	 *
	 * @param ActiviteitCategorie $obj Het ActiviteitCategorie-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld actCategorieID van het object
	 * obj representeert.
	 */
	public static function waardeActCategorieID(ActiviteitCategorie $obj)
	{
		return static::defaultWaardeInt($obj, 'ActCategorieID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * actCategorieID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld actCategorieID representeert.
	 */
	public static function opmerkingActCategorieID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param ActiviteitCategorie $obj Het ActiviteitCategorie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(ActiviteitCategorie $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param ActiviteitCategorie $obj Het ActiviteitCategorie-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(ActiviteitCategorie $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param ActiviteitCategorie $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(ActiviteitCategorie $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Titel', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Titel', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Titel', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Titel', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
}
