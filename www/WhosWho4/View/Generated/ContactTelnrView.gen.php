<?
abstract class ContactTelnrView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactTelnrView.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactTelnr(ContactTelnr $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld telnrID.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld telnrID labelt.
	 */
	public static function labelTelnrID(ContactTelnr $obj)
	{
		return 'TelnrID';
	}
	/**
	 * @brief Geef de waarde van het veld telnrID.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld telnrID van het object obj
	 * representeert.
	 */
	public static function waardeTelnrID(ContactTelnr $obj)
	{
		return static::defaultWaardeInt($obj, 'TelnrID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld telnrID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld telnrID representeert.
	 */
	public static function opmerkingTelnrID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contact.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld contact labelt.
	 */
	public static function labelContact(ContactTelnr $obj)
	{
		return 'Contact';
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contact van het object obj
	 * representeert.
	 */
	public static function waardeContact(ContactTelnr $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContact())
			return NULL;
		return ContactView::defaultWaardeContact($obj->getContact());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contact.
	 *
	 * @see genericFormcontact
	 *
	 * @param ContactTelnr $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formContact(ContactTelnr $obj, $include_id = false)
	{
		return static::waardeContact($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contact. In
	 * tegenstelling tot formcontact moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formcontact
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormContact($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld contact
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contact representeert.
	 */
	public static function opmerkingContact()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld telefoonnummer.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld telefoonnummer labelt.
	 */
	public static function labelTelefoonnummer(ContactTelnr $obj)
	{
		return 'Telefoonnummer';
	}
	/**
	 * @brief Geef de waarde van het veld telefoonnummer.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld telefoonnummer van het object
	 * obj representeert.
	 */
	public static function waardeTelefoonnummer(ContactTelnr $obj)
	{
		return static::defaultWaardeString($obj, 'Telefoonnummer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld telefoonnummer.
	 *
	 * @see genericFormtelefoonnummer
	 *
	 * @param ContactTelnr $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld telefoonnummer staat en
	 * kan worden bewerkt. Indien telefoonnummer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTelefoonnummer(ContactTelnr $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Telefoonnummer', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld telefoonnummer. In
	 * tegenstelling tot formtelefoonnummer moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtelefoonnummer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld telefoonnummer staat en
	 * kan worden bewerkt. Indien telefoonnummer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTelefoonnummer($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Telefoonnummer', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * telefoonnummer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld telefoonnummer representeert.
	 */
	public static function opmerkingTelefoonnummer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(ContactTelnr $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(ContactTelnr::enumsSoort() as $id)
			$soorten[$id] = ContactTelnrView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(ContactTelnr $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param ContactTelnr $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(ContactTelnr $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', ContactTelnr::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorkeur.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld voorkeur labelt.
	 */
	public static function labelVoorkeur(ContactTelnr $obj)
	{
		return 'Voorkeur';
	}
	/**
	 * @brief Geef de waarde van het veld voorkeur.
	 *
	 * @param ContactTelnr $obj Het ContactTelnr-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorkeur van het object obj
	 * representeert.
	 */
	public static function waardeVoorkeur(ContactTelnr $obj)
	{
		return static::defaultWaardeBool($obj, 'Voorkeur');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voorkeur.
	 *
	 * @see genericFormvoorkeur
	 *
	 * @param ContactTelnr $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorkeur staat en kan
	 * worden bewerkt. Indien voorkeur read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoorkeur(ContactTelnr $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Voorkeur', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voorkeur. In
	 * tegenstelling tot formvoorkeur moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoorkeur
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorkeur staat en kan
	 * worden bewerkt. Indien voorkeur read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoorkeur($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Voorkeur');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorkeur bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorkeur representeert.
	 */
	public static function opmerkingVoorkeur()
	{
		return NULL;
	}
}
