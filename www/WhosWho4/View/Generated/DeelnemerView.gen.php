<?
abstract class DeelnemerView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DeelnemerView.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDeelnemer(Deelnemer $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(Deelnemer $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(Deelnemer $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld activiteit.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld activiteit labelt.
	 */
	public static function labelActiviteit(Deelnemer $obj)
	{
		return 'Activiteit';
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld activiteit van het object obj
	 * representeert.
	 */
	public static function waardeActiviteit(Deelnemer $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getActiviteit())
			return NULL;
		return ActiviteitView::defaultWaardeActiviteit($obj->getActiviteit());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * activiteit bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld activiteit representeert.
	 */
	public static function opmerkingActiviteit()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld momentInschrijven.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld momentInschrijven labelt.
	 */
	public static function labelMomentInschrijven(Deelnemer $obj)
	{
		return 'MomentInschrijven';
	}
	/**
	 * @brief Geef de waarde van het veld momentInschrijven.
	 *
	 * @param Deelnemer $obj Het Deelnemer-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld momentInschrijven van het
	 * object obj representeert.
	 */
	public static function waardeMomentInschrijven(Deelnemer $obj)
	{
		return static::defaultWaardeDatetime($obj, 'MomentInschrijven');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld momentInschrijven.
	 *
	 * @see genericFormmomentInschrijven
	 *
	 * @param Deelnemer $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentInschrijven staat en
	 * kan worden bewerkt. Indien momentInschrijven read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formMomentInschrijven(Deelnemer $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'MomentInschrijven', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld momentInschrijven. In
	 * tegenstelling tot formmomentInschrijven moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formmomentInschrijven
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld momentInschrijven staat en
	 * kan worden bewerkt. Indien momentInschrijven read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormMomentInschrijven($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'MomentInschrijven');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * momentInschrijven bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld momentInschrijven representeert.
	 */
	public static function opmerkingMomentInschrijven()
	{
		return NULL;
	}
}
