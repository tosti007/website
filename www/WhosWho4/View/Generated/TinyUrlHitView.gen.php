<?
abstract class TinyUrlHitView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TinyUrlHitView.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTinyUrlHit(TinyUrlHit $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld id.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld id labelt.
	 */
	public static function labelId(TinyUrlHit $obj)
	{
		return 'Id';
	}
	/**
	 * @brief Geef de waarde van het veld id.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld id van het object obj
	 * representeert.
	 */
	public static function waardeId(TinyUrlHit $obj)
	{
		return static::defaultWaardeInt($obj, 'Id');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld id
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld id representeert.
	 */
	public static function opmerkingId()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tinyUrl.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tinyUrl labelt.
	 */
	public static function labelTinyUrl(TinyUrlHit $obj)
	{
		return 'TinyUrl';
	}
	/**
	 * @brief Geef de waarde van het veld tinyUrl.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tinyUrl van het object obj
	 * representeert.
	 */
	public static function waardeTinyUrl(TinyUrlHit $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTinyUrl())
			return NULL;
		return TinyUrlView::defaultWaardeTinyUrl($obj->getTinyUrl());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tinyUrl.
	 *
	 * @see genericFormtinyUrl
	 *
	 * @param TinyUrlHit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tinyUrl staat en kan
	 * worden bewerkt. Indien tinyUrl read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTinyUrl(TinyUrlHit $obj, $include_id = false)
	{
		return TinyUrlView::defaultForm($obj->getTinyUrl());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tinyUrl. In
	 * tegenstelling tot formtinyUrl moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtinyUrl
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tinyUrl staat en kan
	 * worden bewerkt. Indien tinyUrl read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTinyUrl($name, $waarde=NULL)
	{
		return TinyUrlView::genericDefaultForm('TinyUrl');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld tinyUrl
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tinyUrl representeert.
	 */
	public static function opmerkingTinyUrl()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld host.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld host labelt.
	 */
	public static function labelHost(TinyUrlHit $obj)
	{
		return 'Host';
	}
	/**
	 * @brief Geef de waarde van het veld host.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld host van het object obj
	 * representeert.
	 */
	public static function waardeHost(TinyUrlHit $obj)
	{
		return static::defaultWaardeString($obj, 'Host');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld host.
	 *
	 * @see genericFormhost
	 *
	 * @param TinyUrlHit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld host staat en kan worden
	 * bewerkt. Indien host read-only is betreft het een statisch html-element.
	 */
	public static function formHost(TinyUrlHit $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Host', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld host. In tegenstelling
	 * tot formhost moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formhost
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld host staat en kan worden
	 * bewerkt. Indien host read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormHost($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Host', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld host
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld host representeert.
	 */
	public static function opmerkingHost()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld hitTime.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld hitTime labelt.
	 */
	public static function labelHitTime(TinyUrlHit $obj)
	{
		return 'HitTime';
	}
	/**
	 * @brief Geef de waarde van het veld hitTime.
	 *
	 * @param TinyUrlHit $obj Het TinyUrlHit-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld hitTime van het object obj
	 * representeert.
	 */
	public static function waardeHitTime(TinyUrlHit $obj)
	{
		return static::defaultWaardeDatetime($obj, 'HitTime');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld hitTime.
	 *
	 * @see genericFormhitTime
	 *
	 * @param TinyUrlHit $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld hitTime staat en kan
	 * worden bewerkt. Indien hitTime read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formHitTime(TinyUrlHit $obj, $include_id = false)
	{
		return static::defaultFormDatetime($obj, 'HitTime', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld hitTime. In
	 * tegenstelling tot formhitTime moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formhitTime
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld hitTime staat en kan
	 * worden bewerkt. Indien hitTime read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormHitTime($name, $waarde=NULL)
	{
		return static::genericDefaultFormDatetime($name, $waarde, 'HitTime');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld hitTime
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld hitTime representeert.
	 */
	public static function opmerkingHitTime()
	{
		return NULL;
	}
}
