<?
abstract class VoornaamwoordView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in VoornaamwoordView.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeVoornaamwoord(Voornaamwoord $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld woordID.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld woordID labelt.
	 */
	public static function labelWoordID(Voornaamwoord $obj)
	{
		return 'WoordID';
	}
	/**
	 * @brief Geef de waarde van het veld woordID.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld woordID van het object obj
	 * representeert.
	 */
	public static function waardeWoordID(Voornaamwoord $obj)
	{
		return static::defaultWaardeInt($obj, 'WoordID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld woordID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld woordID representeert.
	 */
	public static function opmerkingWoordID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(Voornaamwoord $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Soort', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Soort', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Soort', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Soort', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld onderwerp.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld onderwerp labelt.
	 */
	public static function labelOnderwerp(Voornaamwoord $obj)
	{
		return 'Onderwerp';
	}
	/**
	 * @brief Geef de waarde van het veld onderwerp.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld onderwerp van het object obj
	 * representeert.
	 */
	public static function waardeOnderwerp(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Onderwerp');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld onderwerp.
	 *
	 * @see genericFormonderwerp
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld onderwerp staat en kan
	 * worden bewerkt. Indien onderwerp read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOnderwerp(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Onderwerp', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Onderwerp', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld onderwerp. In
	 * tegenstelling tot formonderwerp moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formonderwerp
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld onderwerp staat en kan
	 * worden bewerkt. Indien onderwerp read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOnderwerp($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Onderwerp', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Onderwerp', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * onderwerp bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld onderwerp representeert.
	 */
	public static function opmerkingOnderwerp()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld voorwerp.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld voorwerp labelt.
	 */
	public static function labelVoorwerp(Voornaamwoord $obj)
	{
		return 'Voorwerp';
	}
	/**
	 * @brief Geef de waarde van het veld voorwerp.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld voorwerp van het object obj
	 * representeert.
	 */
	public static function waardeVoorwerp(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Voorwerp');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld voorwerp.
	 *
	 * @see genericFormvoorwerp
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorwerp staat en kan
	 * worden bewerkt. Indien voorwerp read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formVoorwerp(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Voorwerp', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Voorwerp', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld voorwerp. In
	 * tegenstelling tot formvoorwerp moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formvoorwerp
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld voorwerp staat en kan
	 * worden bewerkt. Indien voorwerp read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormVoorwerp($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Voorwerp', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Voorwerp', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * voorwerp bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld voorwerp representeert.
	 */
	public static function opmerkingVoorwerp()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bezittelijk.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld bezittelijk labelt.
	 */
	public static function labelBezittelijk(Voornaamwoord $obj)
	{
		return 'Bezittelijk';
	}
	/**
	 * @brief Geef de waarde van het veld bezittelijk.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bezittelijk van het object
	 * obj representeert.
	 */
	public static function waardeBezittelijk(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Bezittelijk');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bezittelijk.
	 *
	 * @see genericFormbezittelijk
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bezittelijk staat en kan
	 * worden bewerkt. Indien bezittelijk read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBezittelijk(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Bezittelijk', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Bezittelijk', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bezittelijk. In
	 * tegenstelling tot formbezittelijk moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbezittelijk
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bezittelijk staat en kan
	 * worden bewerkt. Indien bezittelijk read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBezittelijk($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Bezittelijk', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Bezittelijk', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bezittelijk bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bezittelijk representeert.
	 */
	public static function opmerkingBezittelijk()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld wederkerend.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld wederkerend labelt.
	 */
	public static function labelWederkerend(Voornaamwoord $obj)
	{
		return 'Wederkerend';
	}
	/**
	 * @brief Geef de waarde van het veld wederkerend.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld wederkerend van het object
	 * obj representeert.
	 */
	public static function waardeWederkerend(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Wederkerend');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld wederkerend.
	 *
	 * @see genericFormwederkerend
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wederkerend staat en kan
	 * worden bewerkt. Indien wederkerend read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWederkerend(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Wederkerend', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Wederkerend', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld wederkerend. In
	 * tegenstelling tot formwederkerend moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formwederkerend
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld wederkerend staat en kan
	 * worden bewerkt. Indien wederkerend read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWederkerend($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Wederkerend', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Wederkerend', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * wederkerend bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld wederkerend representeert.
	 */
	public static function opmerkingWederkerend()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld ouder.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld ouder labelt.
	 */
	public static function labelOuder(Voornaamwoord $obj)
	{
		return 'Ouder';
	}
	/**
	 * @brief Geef de waarde van het veld ouder.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld ouder van het object obj
	 * representeert.
	 */
	public static function waardeOuder(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Ouder');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld ouder.
	 *
	 * @see genericFormouder
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ouder staat en kan worden
	 * bewerkt. Indien ouder read-only is betreft het een statisch html-element.
	 */
	public static function formOuder(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Ouder', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Ouder', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld ouder. In
	 * tegenstelling tot formouder moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formouder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld ouder staat en kan worden
	 * bewerkt. Indien ouder read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormOuder($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Ouder', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Ouder', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld ouder
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld ouder representeert.
	 */
	public static function opmerkingOuder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kind.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld kind labelt.
	 */
	public static function labelKind(Voornaamwoord $obj)
	{
		return 'Kind';
	}
	/**
	 * @brief Geef de waarde van het veld kind.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kind van het object obj
	 * representeert.
	 */
	public static function waardeKind(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Kind');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kind.
	 *
	 * @see genericFormkind
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kind staat en kan worden
	 * bewerkt. Indien kind read-only is betreft het een statisch html-element.
	 */
	public static function formKind(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Kind', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Kind', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kind. In tegenstelling
	 * tot formkind moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formkind
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kind staat en kan worden
	 * bewerkt. Indien kind read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormKind($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Kind', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Kind', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld kind
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kind representeert.
	 */
	public static function opmerkingKind()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld brusje.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld brusje labelt.
	 */
	public static function labelBrusje(Voornaamwoord $obj)
	{
		return 'Brusje';
	}
	/**
	 * @brief Geef de waarde van het veld brusje.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld brusje van het object obj
	 * representeert.
	 */
	public static function waardeBrusje(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Brusje');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld brusje.
	 *
	 * @see genericFormbrusje
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld brusje staat en kan worden
	 * bewerkt. Indien brusje read-only is betreft het een statisch html-element.
	 */
	public static function formBrusje(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Brusje', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Brusje', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld brusje. In
	 * tegenstelling tot formbrusje moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbrusje
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld brusje staat en kan worden
	 * bewerkt. Indien brusje read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBrusje($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Brusje', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Brusje', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld brusje
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld brusje representeert.
	 */
	public static function opmerkingBrusje()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kozijn.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld kozijn labelt.
	 */
	public static function labelKozijn(Voornaamwoord $obj)
	{
		return 'Kozijn';
	}
	/**
	 * @brief Geef de waarde van het veld kozijn.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kozijn van het object obj
	 * representeert.
	 */
	public static function waardeKozijn(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Kozijn');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kozijn.
	 *
	 * @see genericFormkozijn
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kozijn staat en kan worden
	 * bewerkt. Indien kozijn read-only is betreft het een statisch html-element.
	 */
	public static function formKozijn(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Kozijn', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Kozijn', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kozijn. In
	 * tegenstelling tot formkozijn moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formkozijn
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kozijn staat en kan worden
	 * bewerkt. Indien kozijn read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormKozijn($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Kozijn', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Kozijn', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld kozijn
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kozijn representeert.
	 */
	public static function opmerkingKozijn()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kindGrootouder.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld kindGrootouder labelt.
	 */
	public static function labelKindGrootouder(Voornaamwoord $obj)
	{
		return 'KindGrootouder';
	}
	/**
	 * @brief Geef de waarde van het veld kindGrootouder.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kindGrootouder van het object
	 * obj representeert.
	 */
	public static function waardeKindGrootouder(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'KindGrootouder');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kindGrootouder.
	 *
	 * @see genericFormkindGrootouder
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kindGrootouder staat en
	 * kan worden bewerkt. Indien kindGrootouder read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKindGrootouder(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'KindGrootouder', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'KindGrootouder', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kindGrootouder. In
	 * tegenstelling tot formkindGrootouder moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formkindGrootouder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kindGrootouder staat en
	 * kan worden bewerkt. Indien kindGrootouder read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKindGrootouder($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'KindGrootouder', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'KindGrootouder', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * kindGrootouder bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kindGrootouder representeert.
	 */
	public static function opmerkingKindGrootouder()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld kleinkindOuder.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld kleinkindOuder labelt.
	 */
	public static function labelKleinkindOuder(Voornaamwoord $obj)
	{
		return 'KleinkindOuder';
	}
	/**
	 * @brief Geef de waarde van het veld kleinkindOuder.
	 *
	 * @param Voornaamwoord $obj Het Voornaamwoord-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld kleinkindOuder van het object
	 * obj representeert.
	 */
	public static function waardeKleinkindOuder(Voornaamwoord $obj)
	{
		return static::defaultWaardeString($obj, 'KleinkindOuder');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld kleinkindOuder.
	 *
	 * @see genericFormkleinkindOuder
	 *
	 * @param Voornaamwoord $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kleinkindOuder staat en
	 * kan worden bewerkt. Indien kleinkindOuder read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formKleinkindOuder(Voornaamwoord $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'KleinkindOuder', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'KleinkindOuder', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld kleinkindOuder. In
	 * tegenstelling tot formkleinkindOuder moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formkleinkindOuder
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld kleinkindOuder staat en
	 * kan worden bewerkt. Indien kleinkindOuder read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormKleinkindOuder($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'KleinkindOuder', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'KleinkindOuder', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * kleinkindOuder bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld kleinkindOuder representeert.
	 */
	public static function opmerkingKleinkindOuder()
	{
		return NULL;
	}
}
