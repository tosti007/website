<?
abstract class ContactMailingListView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactMailingListView.
	 *
	 * @param ContactMailingList $obj Het ContactMailingList-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactMailingList(ContactMailingList $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld contact.
	 *
	 * @param ContactMailingList $obj Het ContactMailingList-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld contact labelt.
	 */
	public static function labelContact(ContactMailingList $obj)
	{
		return 'Contact';
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @param ContactMailingList $obj Het ContactMailingList-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contact van het object obj
	 * representeert.
	 */
	public static function waardeContact(ContactMailingList $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContact())
			return NULL;
		return ContactView::defaultWaardeContact($obj->getContact());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld contact
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contact representeert.
	 */
	public static function opmerkingContact()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld mailingList.
	 *
	 * @param ContactMailingList $obj Het ContactMailingList-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld mailingList labelt.
	 */
	public static function labelMailingList(ContactMailingList $obj)
	{
		return 'MailingList';
	}
	/**
	 * @brief Geef de waarde van het veld mailingList.
	 *
	 * @param ContactMailingList $obj Het ContactMailingList-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld mailingList van het object
	 * obj representeert.
	 */
	public static function waardeMailingList(ContactMailingList $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getMailingList())
			return NULL;
		return MailingListView::defaultWaardeMailingList($obj->getMailingList());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * mailingList bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld mailingList representeert.
	 */
	public static function opmerkingMailingList()
	{
		return NULL;
	}
}
