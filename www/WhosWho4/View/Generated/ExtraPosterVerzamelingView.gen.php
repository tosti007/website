<?
abstract class ExtraPosterVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ExtraPosterVerzamelingView.
	 *
	 * @param ExtraPosterVerzameling $obj Het ExtraPosterVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeExtraPosterVerzameling(ExtraPosterVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
