<?
abstract class DeelnemerAntwoordView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in DeelnemerAntwoordView.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeDeelnemerAntwoord(DeelnemerAntwoord $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld deelnemer.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld deelnemer labelt.
	 */
	public static function labelDeelnemer(DeelnemerAntwoord $obj)
	{
		return 'Deelnemer';
	}
	/**
	 * @brief Geef de waarde van het veld deelnemer.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld deelnemer van het object obj
	 * representeert.
	 */
	public static function waardeDeelnemer(DeelnemerAntwoord $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getDeelnemer())
			return NULL;
		return DeelnemerView::defaultWaardeDeelnemer($obj->getDeelnemer());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * deelnemer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld deelnemer representeert.
	 */
	public static function opmerkingDeelnemer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld vraag.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld vraag labelt.
	 */
	public static function labelVraag(DeelnemerAntwoord $obj)
	{
		return 'Vraag';
	}
	/**
	 * @brief Geef de waarde van het veld vraag.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld vraag van het object obj
	 * representeert.
	 */
	public static function waardeVraag(DeelnemerAntwoord $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getVraag())
			return NULL;
		return ActiviteitVraagView::defaultWaardeActiviteitVraag($obj->getVraag());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld vraag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld vraag representeert.
	 */
	public static function opmerkingVraag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld antwoord.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld antwoord labelt.
	 */
	public static function labelAntwoord(DeelnemerAntwoord $obj)
	{
		return 'Antwoord';
	}
	/**
	 * @brief Geef de waarde van het veld antwoord.
	 *
	 * @param DeelnemerAntwoord $obj Het DeelnemerAntwoord-object waarvan de waarde
	 * wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld antwoord van het object obj
	 * representeert.
	 */
	public static function waardeAntwoord(DeelnemerAntwoord $obj)
	{
		return static::defaultWaardeString($obj, 'Antwoord');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld antwoord.
	 *
	 * @see genericFormantwoord
	 *
	 * @param DeelnemerAntwoord $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld antwoord staat en kan
	 * worden bewerkt. Indien antwoord read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAntwoord(DeelnemerAntwoord $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Antwoord', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld antwoord. In
	 * tegenstelling tot formantwoord moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formantwoord
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld antwoord staat en kan
	 * worden bewerkt. Indien antwoord read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAntwoord($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Antwoord', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * antwoord bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld antwoord representeert.
	 */
	public static function opmerkingAntwoord()
	{
		return NULL;
	}
}
