<?
abstract class ActiviteitHerhalingVerzamelingView_Generated
	extends ActiviteitVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitHerhalingVerzamelingView.
	 *
	 * @param ActiviteitHerhalingVerzameling $obj Het
	 * ActiviteitHerhalingVerzameling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitHerhalingVerzameling(ActiviteitHerhalingVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
