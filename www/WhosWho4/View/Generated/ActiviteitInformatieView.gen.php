<?
abstract class ActiviteitInformatieView_Generated
	extends ActiviteitView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitInformatieView.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitInformatie(ActiviteitInformatie $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld titel.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld titel labelt.
	 */
	public static function labelTitel(ActiviteitInformatie $obj)
	{
		return 'Titel';
	}
	/**
	 * @brief Geef de waarde van het veld titel.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld titel van het object obj
	 * representeert.
	 */
	public static function waardeTitel(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeString($obj, 'Titel');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld titel.
	 *
	 * @see genericFormtitel
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is betreft het een statisch html-element.
	 */
	public static function formTitel(ActiviteitInformatie $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Titel', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Titel', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld titel. In
	 * tegenstelling tot formtitel moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formtitel
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld titel staat en kan worden
	 * bewerkt. Indien titel read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTitel($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Titel', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Titel', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld titel
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld titel representeert.
	 */
	public static function opmerkingTitel()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld werftekst.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld werftekst labelt.
	 */
	public static function labelWerftekst(ActiviteitInformatie $obj)
	{
		return 'Werftekst';
	}
	/**
	 * @brief Geef de waarde van het veld werftekst.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld werftekst van het object obj
	 * representeert.
	 */
	public static function waardeWerftekst(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeHtml($obj, 'Werftekst');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld werftekst.
	 *
	 * @see genericFormwerftekst
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld werftekst staat en kan
	 * worden bewerkt. Indien werftekst read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWerftekst(ActiviteitInformatie $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormHtml($obj, 'Werftekst', 'nl', $include_id),
			'en' => static::defaultFormHtml($obj, 'Werftekst', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld werftekst. In
	 * tegenstelling tot formwerftekst moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formwerftekst
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld werftekst staat en kan
	 * worden bewerkt. Indien werftekst read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWerftekst($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormHtml($name, $waarde, 'Werftekst', 'nl'),
			'en' => static::genericDefaultFormHtml($name, $waarde, 'Werftekst', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * werftekst bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld werftekst representeert.
	 */
	public static function opmerkingWerftekst()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld homepage.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld homepage labelt.
	 */
	public static function labelHomepage(ActiviteitInformatie $obj)
	{
		return 'Homepage';
	}
	/**
	 * @brief Geef de waarde van het veld homepage.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld homepage van het object obj
	 * representeert.
	 */
	public static function waardeHomepage(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeUrl($obj, 'Homepage');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld homepage.
	 *
	 * @see genericFormhomepage
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld homepage staat en kan
	 * worden bewerkt. Indien homepage read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formHomepage(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormUrl($obj, 'Homepage', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld homepage. In
	 * tegenstelling tot formhomepage moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formhomepage
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld homepage staat en kan
	 * worden bewerkt. Indien homepage read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormHomepage($name, $waarde=NULL)
	{
		return static::genericDefaultFormUrl($name, $waarde, 'Homepage');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * homepage bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld homepage representeert.
	 */
	public static function opmerkingHomepage()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld locatie.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld locatie labelt.
	 */
	public static function labelLocatie(ActiviteitInformatie $obj)
	{
		return 'Locatie';
	}
	/**
	 * @brief Geef de waarde van het veld locatie.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld locatie van het object obj
	 * representeert.
	 */
	public static function waardeLocatie(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeString($obj, 'Locatie');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld locatie.
	 *
	 * @see genericFormlocatie
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld locatie staat en kan
	 * worden bewerkt. Indien locatie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formLocatie(ActiviteitInformatie $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Locatie', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Locatie', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld locatie. In
	 * tegenstelling tot formlocatie moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formlocatie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld locatie staat en kan
	 * worden bewerkt. Indien locatie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormLocatie($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Locatie', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Locatie', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld locatie
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld locatie representeert.
	 */
	public static function opmerkingLocatie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld prijs.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld prijs labelt.
	 */
	public static function labelPrijs(ActiviteitInformatie $obj)
	{
		return 'Prijs';
	}
	/**
	 * @brief Geef de waarde van het veld prijs.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld prijs van het object obj
	 * representeert.
	 */
	public static function waardePrijs(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeString($obj, 'Prijs');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld prijs.
	 *
	 * @see genericFormprijs
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prijs staat en kan worden
	 * bewerkt. Indien prijs read-only is betreft het een statisch html-element.
	 */
	public static function formPrijs(ActiviteitInformatie $obj, $include_id = false)
	{
		return array(
			'nl' => static::defaultFormString($obj, 'Prijs', 'nl', $include_id),
			'en' => static::defaultFormString($obj, 'Prijs', 'en', $include_id)
		);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld prijs. In
	 * tegenstelling tot formprijs moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formprijs
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld prijs staat en kan worden
	 * bewerkt. Indien prijs read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormPrijs($name, $waarde=NULL)
	{
		return array(
			'nl' => static::genericDefaultFormString($name, $waarde, 'Prijs', 'nl'),
			'en' => static::genericDefaultFormString($name, $waarde, 'Prijs', 'en'),
		);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld prijs
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld prijs representeert.
	 */
	public static function opmerkingPrijs()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld inschrijfbaar.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld inschrijfbaar labelt.
	 */
	public static function labelInschrijfbaar(ActiviteitInformatie $obj)
	{
		return 'Inschrijfbaar';
	}
	/**
	 * @brief Geef de waarde van het veld inschrijfbaar.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld inschrijfbaar van het object
	 * obj representeert.
	 */
	public static function waardeInschrijfbaar(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeBool($obj, 'Inschrijfbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld inschrijfbaar.
	 *
	 * @see genericForminschrijfbaar
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inschrijfbaar staat en kan
	 * worden bewerkt. Indien inschrijfbaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formInschrijfbaar(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Inschrijfbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld inschrijfbaar. In
	 * tegenstelling tot forminschrijfbaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see forminschrijfbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld inschrijfbaar staat en kan
	 * worden bewerkt. Indien inschrijfbaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormInschrijfbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Inschrijfbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * inschrijfbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld inschrijfbaar representeert.
	 */
	public static function opmerkingInschrijfbaar()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld stuurMail.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld stuurMail labelt.
	 */
	public static function labelStuurMail(ActiviteitInformatie $obj)
	{
		return 'StuurMail';
	}
	/**
	 * @brief Geef de waarde van het veld stuurMail.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld stuurMail van het object obj
	 * representeert.
	 */
	public static function waardeStuurMail(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeBool($obj, 'StuurMail');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld stuurMail.
	 *
	 * @see genericFormstuurMail
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld stuurMail staat en kan
	 * worden bewerkt. Indien stuurMail read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formStuurMail(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'StuurMail', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld stuurMail. In
	 * tegenstelling tot formstuurMail moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formstuurMail
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld stuurMail staat en kan
	 * worden bewerkt. Indien stuurMail read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormStuurMail($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'StuurMail');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * stuurMail bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld stuurMail representeert.
	 */
	public static function opmerkingStuurMail()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumInschrijvenMax.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumInschrijvenMax labelt.
	 */
	public static function labelDatumInschrijvenMax(ActiviteitInformatie $obj)
	{
		return 'DatumInschrijvenMax';
	}
	/**
	 * @brief Geef de waarde van het veld datumInschrijvenMax.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumInschrijvenMax van het
	 * object obj representeert.
	 */
	public static function waardeDatumInschrijvenMax(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumInschrijvenMax');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumInschrijvenMax.
	 *
	 * @see genericFormdatumInschrijvenMax
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumInschrijvenMax staat
	 * en kan worden bewerkt. Indien datumInschrijvenMax read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formDatumInschrijvenMax(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumInschrijvenMax', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumInschrijvenMax.
	 * In tegenstelling tot formdatumInschrijvenMax moeten naam en waarde meegegeven
	 * worden, en worden niet uit het object geladen.
	 *
	 * @see formdatumInschrijvenMax
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumInschrijvenMax staat
	 * en kan worden bewerkt. Indien datumInschrijvenMax read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormDatumInschrijvenMax($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumInschrijvenMax');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumInschrijvenMax bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumInschrijvenMax representeert.
	 */
	public static function opmerkingDatumInschrijvenMax()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld datumUitschrijven.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld datumUitschrijven labelt.
	 */
	public static function labelDatumUitschrijven(ActiviteitInformatie $obj)
	{
		return 'DatumUitschrijven';
	}
	/**
	 * @brief Geef de waarde van het veld datumUitschrijven.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld datumUitschrijven van het
	 * object obj representeert.
	 */
	public static function waardeDatumUitschrijven(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeDate($obj, 'DatumUitschrijven');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld datumUitschrijven.
	 *
	 * @see genericFormdatumUitschrijven
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumUitschrijven staat en
	 * kan worden bewerkt. Indien datumUitschrijven read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formDatumUitschrijven(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormDate($obj, 'DatumUitschrijven', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld datumUitschrijven. In
	 * tegenstelling tot formdatumUitschrijven moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formdatumUitschrijven
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld datumUitschrijven staat en
	 * kan worden bewerkt. Indien datumUitschrijven read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormDatumUitschrijven($name, $waarde=NULL)
	{
		return static::genericDefaultFormDate($name, $waarde, 'DatumUitschrijven');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * datumUitschrijven bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld datumUitschrijven representeert.
	 */
	public static function opmerkingDatumUitschrijven()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld maxDeelnemers.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld maxDeelnemers labelt.
	 */
	public static function labelMaxDeelnemers(ActiviteitInformatie $obj)
	{
		return 'MaxDeelnemers';
	}
	/**
	 * @brief Geef de waarde van het veld maxDeelnemers.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld maxDeelnemers van het object
	 * obj representeert.
	 */
	public static function waardeMaxDeelnemers(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeInt($obj, 'MaxDeelnemers');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld maxDeelnemers.
	 *
	 * @see genericFormmaxDeelnemers
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld maxDeelnemers staat en kan
	 * worden bewerkt. Indien maxDeelnemers read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formMaxDeelnemers(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'MaxDeelnemers', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld maxDeelnemers. In
	 * tegenstelling tot formmaxDeelnemers moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formmaxDeelnemers
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld maxDeelnemers staat en kan
	 * worden bewerkt. Indien maxDeelnemers read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormMaxDeelnemers($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'MaxDeelnemers');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * maxDeelnemers bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld maxDeelnemers representeert.
	 */
	public static function opmerkingMaxDeelnemers()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld actsoort.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld actsoort labelt.
	 */
	public static function labelActsoort(ActiviteitInformatie $obj)
	{
		return 'Actsoort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld actsoort.
	 *
	 * @param string $value Een enum-waarde van het veld actsoort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumActsoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld actsoort horen.
	 *
	 * @see labelenumActsoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld actsoort
	 * representeren.
	 */
	public static function labelenumActsoortArray()
	{
		$soorten = array();
		foreach(ActiviteitInformatie::enumsActsoort() as $id)
			$soorten[$id] = ActiviteitInformatieView::labelenumActsoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld actsoort.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld actsoort van het object obj
	 * representeert.
	 */
	public static function waardeActsoort(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Actsoort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld actsoort.
	 *
	 * @see genericFormactsoort
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actsoort staat en kan
	 * worden bewerkt. Indien actsoort read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formActsoort(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Actsoort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld actsoort. In
	 * tegenstelling tot formactsoort moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formactsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld actsoort staat en kan
	 * worden bewerkt. Indien actsoort read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormActsoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Actsoort', ActiviteitInformatie::enumsactsoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * actsoort bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld actsoort representeert.
	 */
	public static function opmerkingActsoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld onderwijs.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld onderwijs labelt.
	 */
	public static function labelOnderwijs(ActiviteitInformatie $obj)
	{
		return 'Onderwijs';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld onderwijs.
	 *
	 * @param string $value Een enum-waarde van het veld onderwijs.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumOnderwijs($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld onderwijs horen.
	 *
	 * @see labelenumOnderwijs
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld onderwijs
	 * representeren.
	 */
	public static function labelenumOnderwijsArray()
	{
		$soorten = array();
		foreach(ActiviteitInformatie::enumsOnderwijs() as $id)
			$soorten[$id] = ActiviteitInformatieView::labelenumOnderwijs($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld onderwijs.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld onderwijs van het object obj
	 * representeert.
	 */
	public static function waardeOnderwijs(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeEnum($obj, 'Onderwijs');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld onderwijs.
	 *
	 * @see genericFormonderwijs
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld onderwijs staat en kan
	 * worden bewerkt. Indien onderwijs read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formOnderwijs(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Onderwijs', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld onderwijs. In
	 * tegenstelling tot formonderwijs moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formonderwijs
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld onderwijs staat en kan
	 * worden bewerkt. Indien onderwijs read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormOnderwijs($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Onderwijs', ActiviteitInformatie::enumsonderwijs());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * onderwijs bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld onderwijs representeert.
	 */
	public static function opmerkingOnderwijs()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld aantalComputers.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld aantalComputers labelt.
	 */
	public static function labelAantalComputers(ActiviteitInformatie $obj)
	{
		return 'AantalComputers';
	}
	/**
	 * @brief Geef de waarde van het veld aantalComputers.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld aantalComputers van het
	 * object obj representeert.
	 */
	public static function waardeAantalComputers(ActiviteitInformatie $obj)
	{
		return static::defaultWaardeInt($obj, 'AantalComputers');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld aantalComputers.
	 *
	 * @see genericFormaantalComputers
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantalComputers staat en
	 * kan worden bewerkt. Indien aantalComputers read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAantalComputers(ActiviteitInformatie $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'AantalComputers', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld aantalComputers. In
	 * tegenstelling tot formaantalComputers moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formaantalComputers
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld aantalComputers staat en
	 * kan worden bewerkt. Indien aantalComputers read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormAantalComputers($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'AantalComputers');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * aantalComputers bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld aantalComputers representeert.
	 */
	public static function opmerkingAantalComputers()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld categorie.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvoor het
	 * veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld categorie labelt.
	 */
	public static function labelCategorie(ActiviteitInformatie $obj)
	{
		return 'Categorie';
	}
	/**
	 * @brief Geef de waarde van het veld categorie.
	 *
	 * @param ActiviteitInformatie $obj Het ActiviteitInformatie-object waarvan de
	 * waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld categorie van het object obj
	 * representeert.
	 */
	public static function waardeCategorie(ActiviteitInformatie $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getCategorie())
			return NULL;
		return ActiviteitCategorieView::defaultWaardeActiviteitCategorie($obj->getCategorie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld categorie.
	 *
	 * @see genericFormcategorie
	 *
	 * @param ActiviteitInformatie $obj Het object waarvoor een formulieronderdeel
	 * nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formCategorie(ActiviteitInformatie $obj, $include_id = false)
	{
		return ActiviteitCategorieView::defaultForm($obj->getCategorie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld categorie. In
	 * tegenstelling tot formcategorie moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formcategorie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld categorie staat en kan
	 * worden bewerkt. Indien categorie read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormCategorie($name, $waarde=NULL)
	{
		return ActiviteitCategorieView::genericDefaultForm('Categorie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * categorie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld categorie representeert.
	 */
	public static function opmerkingCategorie()
	{
		return NULL;
	}
}
