<?
abstract class ActiviteitInformatieVerzamelingView_Generated
	extends ActiviteitVerzamelingView
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitInformatieVerzamelingView.
	 *
	 * @param ActiviteitInformatieVerzameling $obj Het
	 * ActiviteitInformatieVerzameling-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitInformatieVerzameling(ActiviteitInformatieVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
