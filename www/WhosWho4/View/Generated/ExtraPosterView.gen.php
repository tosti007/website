<?
abstract class ExtraPosterView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ExtraPosterView.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeExtraPoster(ExtraPoster $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld extraPosterID.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld extraPosterID labelt.
	 */
	public static function labelExtraPosterID(ExtraPoster $obj)
	{
		return 'ExtraPosterID';
	}
	/**
	 * @brief Geef de waarde van het veld extraPosterID.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld extraPosterID van het object
	 * obj representeert.
	 */
	public static function waardeExtraPosterID(ExtraPoster $obj)
	{
		return static::defaultWaardeInt($obj, 'ExtraPosterID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * extraPosterID bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld extraPosterID representeert.
	 */
	public static function opmerkingExtraPosterID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld naam.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld naam labelt.
	 */
	public static function labelNaam(ExtraPoster $obj)
	{
		return 'Naam';
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld naam van het object obj
	 * representeert.
	 */
	public static function waardeNaam(ExtraPoster $obj)
	{
		return static::defaultWaardeString($obj, 'Naam');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld naam.
	 *
	 * @see genericFormnaam
	 *
	 * @param ExtraPoster $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is betreft het een statisch html-element.
	 */
	public static function formNaam(ExtraPoster $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Naam', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld naam. In tegenstelling
	 * tot formnaam moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formnaam
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld naam staat en kan worden
	 * bewerkt. Indien naam read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormNaam($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Naam', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld naam
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld naam representeert.
	 */
	public static function opmerkingNaam()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beschrijving.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld beschrijving labelt.
	 */
	public static function labelBeschrijving(ExtraPoster $obj)
	{
		return 'Beschrijving';
	}
	/**
	 * @brief Geef de waarde van het veld beschrijving.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beschrijving van het object
	 * obj representeert.
	 */
	public static function waardeBeschrijving(ExtraPoster $obj)
	{
		return static::defaultWaardeString($obj, 'Beschrijving');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beschrijving.
	 *
	 * @see genericFormbeschrijving
	 *
	 * @param ExtraPoster $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beschrijving staat en kan
	 * worden bewerkt. Indien beschrijving read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeschrijving(ExtraPoster $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Beschrijving', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beschrijving. In
	 * tegenstelling tot formbeschrijving moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeschrijving
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beschrijving staat en kan
	 * worden bewerkt. Indien beschrijving read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeschrijving($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Beschrijving', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beschrijving bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beschrijving representeert.
	 */
	public static function opmerkingBeschrijving()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld zichtbaar.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld zichtbaar labelt.
	 */
	public static function labelZichtbaar(ExtraPoster $obj)
	{
		return 'Zichtbaar';
	}
	/**
	 * @brief Geef de waarde van het veld zichtbaar.
	 *
	 * @param ExtraPoster $obj Het ExtraPoster-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld zichtbaar van het object obj
	 * representeert.
	 */
	public static function waardeZichtbaar(ExtraPoster $obj)
	{
		return static::defaultWaardeBool($obj, 'Zichtbaar');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld zichtbaar.
	 *
	 * @see genericFormzichtbaar
	 *
	 * @param ExtraPoster $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld zichtbaar staat en kan
	 * worden bewerkt. Indien zichtbaar read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formZichtbaar(ExtraPoster $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'Zichtbaar', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld zichtbaar. In
	 * tegenstelling tot formzichtbaar moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formzichtbaar
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld zichtbaar staat en kan
	 * worden bewerkt. Indien zichtbaar read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormZichtbaar($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'Zichtbaar');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * zichtbaar bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld zichtbaar representeert.
	 */
	public static function opmerkingZichtbaar()
	{
		return NULL;
	}
}
