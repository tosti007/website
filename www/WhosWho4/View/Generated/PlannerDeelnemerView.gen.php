<?
abstract class PlannerDeelnemerView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PlannerDeelnemerView.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePlannerDeelnemer(PlannerDeelnemer $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld plannerData.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld plannerData labelt.
	 */
	public static function labelPlannerData(PlannerDeelnemer $obj)
	{
		return 'PlannerData';
	}
	/**
	 * @brief Geef de waarde van het veld plannerData.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld plannerData van het object
	 * obj representeert.
	 */
	public static function waardePlannerData(PlannerDeelnemer $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPlannerData())
			return NULL;
		return PlannerDataView::defaultWaardePlannerData($obj->getPlannerData());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * plannerData bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld plannerData representeert.
	 */
	public static function opmerkingPlannerData()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(PlannerDeelnemer $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(PlannerDeelnemer $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld antwoord.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld antwoord labelt.
	 */
	public static function labelAntwoord(PlannerDeelnemer $obj)
	{
		return 'Antwoord';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld antwoord.
	 *
	 * @param string $value Een enum-waarde van het veld antwoord.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumAntwoord($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld antwoord horen.
	 *
	 * @see labelenumAntwoord
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld antwoord
	 * representeren.
	 */
	public static function labelenumAntwoordArray()
	{
		$soorten = array();
		foreach(PlannerDeelnemer::enumsAntwoord() as $id)
			$soorten[$id] = PlannerDeelnemerView::labelenumAntwoord($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld antwoord.
	 *
	 * @param PlannerDeelnemer $obj Het PlannerDeelnemer-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld antwoord van het object obj
	 * representeert.
	 */
	public static function waardeAntwoord(PlannerDeelnemer $obj)
	{
		return static::defaultWaardeEnum($obj, 'Antwoord');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld antwoord.
	 *
	 * @see genericFormantwoord
	 *
	 * @param PlannerDeelnemer $obj Het object waarvoor een formulieronderdeel nodig
	 * is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld antwoord staat en kan
	 * worden bewerkt. Indien antwoord read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formAntwoord(PlannerDeelnemer $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Antwoord', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld antwoord. In
	 * tegenstelling tot formantwoord moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formantwoord
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld antwoord staat en kan
	 * worden bewerkt. Indien antwoord read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormAntwoord($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Antwoord', PlannerDeelnemer::enumsantwoord());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * antwoord bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld antwoord representeert.
	 */
	public static function opmerkingAntwoord()
	{
		return NULL;
	}
}
