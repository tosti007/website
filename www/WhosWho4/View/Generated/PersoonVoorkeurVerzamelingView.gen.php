<?
abstract class PersoonVoorkeurVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonVoorkeurVerzamelingView.
	 *
	 * @param PersoonVoorkeurVerzameling $obj Het PersoonVoorkeurVerzameling-object
	 * waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonVoorkeurVerzameling(PersoonVoorkeurVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
