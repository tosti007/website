<?
abstract class ContactAdresView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ContactAdresView.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeContactAdres(ContactAdres $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld adresID.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld adresID labelt.
	 */
	public static function labelAdresID(ContactAdres $obj)
	{
		return 'AdresID';
	}
	/**
	 * @brief Geef de waarde van het veld adresID.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld adresID van het object obj
	 * representeert.
	 */
	public static function waardeAdresID(ContactAdres $obj)
	{
		return static::defaultWaardeInt($obj, 'AdresID');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld adresID
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld adresID representeert.
	 */
	public static function opmerkingAdresID()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld contact.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld contact labelt.
	 */
	public static function labelContact(ContactAdres $obj)
	{
		return 'Contact';
	}
	/**
	 * @brief Geef de waarde van het veld contact.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld contact van het object obj
	 * representeert.
	 */
	public static function waardeContact(ContactAdres $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getContact())
			return NULL;
		return ContactView::defaultWaardeContact($obj->getContact());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld contact.
	 *
	 * @see genericFormcontact
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formContact(ContactAdres $obj, $include_id = false)
	{
		return static::waardeContact($obj);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld contact. In
	 * tegenstelling tot formcontact moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formcontact
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld contact staat en kan
	 * worden bewerkt. Indien contact read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormContact($name, $waarde=NULL)
	{
		return $waarde;
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld contact
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld contact representeert.
	 */
	public static function opmerkingContact()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld soort.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld soort labelt.
	 */
	public static function labelSoort(ContactAdres $obj)
	{
		return 'Soort';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld soort.
	 *
	 * @param string $value Een enum-waarde van het veld soort.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumSoort($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld soort horen.
	 *
	 * @see labelenumSoort
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld soort
	 * representeren.
	 */
	public static function labelenumSoortArray()
	{
		$soorten = array();
		foreach(ContactAdres::enumsSoort() as $id)
			$soorten[$id] = ContactAdresView::labelenumSoort($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld soort van het object obj
	 * representeert.
	 */
	public static function waardeSoort(ContactAdres $obj)
	{
		return static::defaultWaardeEnum($obj, 'Soort');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld soort.
	 *
	 * @see genericFormsoort
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is betreft het een statisch html-element.
	 */
	public static function formSoort(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Soort', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld soort. In
	 * tegenstelling tot formsoort moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formsoort
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld soort staat en kan worden
	 * bewerkt. Indien soort read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormSoort($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Soort', ContactAdres::enumssoort());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld soort
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld soort representeert.
	 */
	public static function opmerkingSoort()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld straat1.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld straat1 labelt.
	 */
	public static function labelStraat1(ContactAdres $obj)
	{
		return 'Straat1';
	}
	/**
	 * @brief Geef de waarde van het veld straat1.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld straat1 van het object obj
	 * representeert.
	 */
	public static function waardeStraat1(ContactAdres $obj)
	{
		return static::defaultWaardeString($obj, 'Straat1');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld straat1.
	 *
	 * @see genericFormstraat1
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld straat1 staat en kan
	 * worden bewerkt. Indien straat1 read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formStraat1(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Straat1', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld straat1. In
	 * tegenstelling tot formstraat1 moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstraat1
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld straat1 staat en kan
	 * worden bewerkt. Indien straat1 read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormStraat1($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Straat1', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld straat1
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld straat1 representeert.
	 */
	public static function opmerkingStraat1()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld straat2.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld straat2 labelt.
	 */
	public static function labelStraat2(ContactAdres $obj)
	{
		return 'Straat2';
	}
	/**
	 * @brief Geef de waarde van het veld straat2.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld straat2 van het object obj
	 * representeert.
	 */
	public static function waardeStraat2(ContactAdres $obj)
	{
		return static::defaultWaardeString($obj, 'Straat2');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld straat2.
	 *
	 * @see genericFormstraat2
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld straat2 staat en kan
	 * worden bewerkt. Indien straat2 read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formStraat2(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Straat2', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld straat2. In
	 * tegenstelling tot formstraat2 moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formstraat2
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld straat2 staat en kan
	 * worden bewerkt. Indien straat2 read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormStraat2($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Straat2', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld straat2
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld straat2 representeert.
	 */
	public static function opmerkingStraat2()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld huisnummer.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld huisnummer labelt.
	 */
	public static function labelHuisnummer(ContactAdres $obj)
	{
		return 'Huisnummer';
	}
	/**
	 * @brief Geef de waarde van het veld huisnummer.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld huisnummer van het object obj
	 * representeert.
	 */
	public static function waardeHuisnummer(ContactAdres $obj)
	{
		return static::defaultWaardeString($obj, 'Huisnummer');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld huisnummer.
	 *
	 * @see genericFormhuisnummer
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld huisnummer staat en kan
	 * worden bewerkt. Indien huisnummer read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formHuisnummer(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Huisnummer', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld huisnummer. In
	 * tegenstelling tot formhuisnummer moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formhuisnummer
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld huisnummer staat en kan
	 * worden bewerkt. Indien huisnummer read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormHuisnummer($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Huisnummer', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * huisnummer bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld huisnummer representeert.
	 */
	public static function opmerkingHuisnummer()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld postcode.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld postcode labelt.
	 */
	public static function labelPostcode(ContactAdres $obj)
	{
		return 'Postcode';
	}
	/**
	 * @brief Geef de waarde van het veld postcode.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld postcode van het object obj
	 * representeert.
	 */
	public static function waardePostcode(ContactAdres $obj)
	{
		return static::defaultWaardeString($obj, 'Postcode');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld postcode.
	 *
	 * @see genericFormpostcode
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld postcode staat en kan
	 * worden bewerkt. Indien postcode read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formPostcode(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Postcode', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld postcode. In
	 * tegenstelling tot formpostcode moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formpostcode
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld postcode staat en kan
	 * worden bewerkt. Indien postcode read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormPostcode($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Postcode', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * postcode bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld postcode representeert.
	 */
	public static function opmerkingPostcode()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld woonplaats.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld woonplaats labelt.
	 */
	public static function labelWoonplaats(ContactAdres $obj)
	{
		return 'Woonplaats';
	}
	/**
	 * @brief Geef de waarde van het veld woonplaats.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld woonplaats van het object obj
	 * representeert.
	 */
	public static function waardeWoonplaats(ContactAdres $obj)
	{
		return static::defaultWaardeString($obj, 'Woonplaats');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld woonplaats.
	 *
	 * @see genericFormwoonplaats
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld woonplaats staat en kan
	 * worden bewerkt. Indien woonplaats read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formWoonplaats(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Woonplaats', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld woonplaats. In
	 * tegenstelling tot formwoonplaats moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formwoonplaats
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld woonplaats staat en kan
	 * worden bewerkt. Indien woonplaats read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormWoonplaats($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Woonplaats', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * woonplaats bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld woonplaats representeert.
	 */
	public static function opmerkingWoonplaats()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld land.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld land labelt.
	 */
	public static function labelLand(ContactAdres $obj)
	{
		return 'Land';
	}
	/**
	 * @brief Geef de waarde van het veld land.
	 *
	 * @param ContactAdres $obj Het ContactAdres-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld land van het object obj
	 * representeert.
	 */
	public static function waardeLand(ContactAdres $obj)
	{
		return static::defaultWaardeString($obj, 'Land');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld land.
	 *
	 * @see genericFormland
	 *
	 * @param ContactAdres $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld land staat en kan worden
	 * bewerkt. Indien land read-only is betreft het een statisch html-element.
	 */
	public static function formLand(ContactAdres $obj, $include_id = false)
	{
		return static::defaultFormString($obj, 'Land', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld land. In tegenstelling
	 * tot formland moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formland
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld land staat en kan worden
	 * bewerkt. Indien land read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLand($name, $waarde=NULL)
	{
		return static::genericDefaultFormString($name, $waarde, 'Land', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld land
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld land representeert.
	 */
	public static function opmerkingLand()
	{
		return NULL;
	}
}
