<?
abstract class PersoonBadgeVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonBadgeVerzamelingView.
	 *
	 * @param PersoonBadgeVerzameling $obj Het PersoonBadgeVerzameling-object waarvan
	 * de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonBadgeVerzameling(PersoonBadgeVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
