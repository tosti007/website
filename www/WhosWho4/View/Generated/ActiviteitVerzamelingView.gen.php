<?
abstract class ActiviteitVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ActiviteitVerzamelingView.
	 *
	 * @param ActiviteitVerzameling $obj Het ActiviteitVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitVerzameling(ActiviteitVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
