<?
abstract class PersoonBadgeView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonBadgeView.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvan de waarde kregen moet
	 * worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonBadge(PersoonBadge $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(PersoonBadge $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(PersoonBadge $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld badges.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld badges labelt.
	 */
	public static function labelBadges(PersoonBadge $obj)
	{
		return 'Badges';
	}
	/**
	 * @brief Geef de waarde van het veld badges.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld badges van het object obj
	 * representeert.
	 */
	public static function waardeBadges(PersoonBadge $obj)
	{
		return static::defaultWaardeText($obj, 'Badges');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld badges.
	 *
	 * @see genericFormbadges
	 *
	 * @param PersoonBadge $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld badges staat en kan worden
	 * bewerkt. Indien badges read-only is betreft het een statisch html-element.
	 */
	public static function formBadges(PersoonBadge $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Badges', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld badges. In
	 * tegenstelling tot formbadges moeten naam en waarde meegegeven worden, en worden
	 * niet uit het object geladen.
	 *
	 * @see formbadges
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld badges staat en kan worden
	 * bewerkt. Indien badges read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormBadges($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Badges', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld badges
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld badges representeert.
	 */
	public static function opmerkingBadges()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld last.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvoor het veldlabel nodig
	 * is.
	 *
	 * @return string
	 * Een string die het veld last labelt.
	 */
	public static function labelLast(PersoonBadge $obj)
	{
		return 'Last';
	}
	/**
	 * @brief Geef de waarde van het veld last.
	 *
	 * @param PersoonBadge $obj Het PersoonBadge-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld last van het object obj
	 * representeert.
	 */
	public static function waardeLast(PersoonBadge $obj)
	{
		return static::defaultWaardeText($obj, 'Last');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld last.
	 *
	 * @see genericFormlast
	 *
	 * @param PersoonBadge $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld last staat en kan worden
	 * bewerkt. Indien last read-only is betreft het een statisch html-element.
	 */
	public static function formLast(PersoonBadge $obj, $include_id = false)
	{
		return static::defaultFormText($obj, 'Last', null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld last. In tegenstelling
	 * tot formlast moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formlast
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld last staat en kan worden
	 * bewerkt. Indien last read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormLast($name, $waarde=NULL)
	{
		return static::genericDefaultFormText($name, $waarde, 'Last', null);
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld last
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld last representeert.
	 */
	public static function opmerkingLast()
	{
		return NULL;
	}
}
