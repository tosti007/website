<?
abstract class CSPReportVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in CSPReportVerzamelingView.
	 *
	 * @param CSPReportVerzameling $obj Het CSPReportVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeCSPReportVerzameling(CSPReportVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
