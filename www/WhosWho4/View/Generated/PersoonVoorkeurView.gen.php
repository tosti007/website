<?
abstract class PersoonVoorkeurView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in PersoonVoorkeurView.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvan de waarde kregen
	 * moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardePersoonVoorkeur(PersoonVoorkeur $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld persoon.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld persoon labelt.
	 */
	public static function labelPersoon(PersoonVoorkeur $obj)
	{
		return 'Persoon';
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld persoon van het object obj
	 * representeert.
	 */
	public static function waardePersoon(PersoonVoorkeur $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getPersoon())
			return NULL;
		return PersoonView::defaultWaardePersoon($obj->getPersoon());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld persoon
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld persoon representeert.
	 */
	public static function opmerkingPersoon()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld taal.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld taal labelt.
	 */
	public static function labelTaal(PersoonVoorkeur $obj)
	{
		return 'Taal';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld taal.
	 *
	 * @param string $value Een enum-waarde van het veld taal.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumTaal($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld taal horen.
	 *
	 * @see labelenumTaal
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld taal
	 * representeren.
	 */
	public static function labelenumTaalArray()
	{
		$soorten = array();
		foreach(PersoonVoorkeur::enumsTaal() as $id)
			$soorten[$id] = PersoonVoorkeurView::labelenumTaal($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld taal.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld taal van het object obj
	 * representeert.
	 */
	public static function waardeTaal(PersoonVoorkeur $obj)
	{
		return static::defaultWaardeEnum($obj, 'Taal');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld taal.
	 *
	 * @see genericFormtaal
	 *
	 * @param PersoonVoorkeur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld taal staat en kan worden
	 * bewerkt. Indien taal read-only is betreft het een statisch html-element.
	 */
	public static function formTaal(PersoonVoorkeur $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'Taal', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld taal. In tegenstelling
	 * tot formtaal moeten naam en waarde meegegeven worden, en worden niet uit het
	 * object geladen.
	 *
	 * @see formtaal
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld taal staat en kan worden
	 * bewerkt. Indien taal read-only is, betreft het een statisch html-element.
	 */
	public static function genericFormTaal($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'Taal', PersoonVoorkeur::enumstaal());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld taal
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld taal representeert.
	 */
	public static function opmerkingTaal()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld bugSortering.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld bugSortering labelt.
	 */
	public static function labelBugSortering(PersoonVoorkeur $obj)
	{
		return 'BugSortering';
	}
	/**
	 * @brief Geef de view-waarde van een enum-waarde van het veld bugSortering.
	 *
	 * @param string $value Een enum-waarde van het veld bugSortering.
	 *
	 * @return string
	 * Een html-veilige string die value representeert.
	 */
	public static function labelenumBugSortering($value)
	{
		return $value;
	}
	/**
	 * @brief Geef alle view-waarde die bij het enum-veld bugSortering horen.
	 *
	 * @see labelenumBugSortering
	 *
	 * @return string[]
	 * Een array met html-veilige strings die de enum-waarden van het veld bugSortering
	 * representeren.
	 */
	public static function labelenumBugSorteringArray()
	{
		$soorten = array();
		foreach(PersoonVoorkeur::enumsBugSortering() as $id)
			$soorten[$id] = PersoonVoorkeurView::labelenumBugSortering($id);
		return $soorten;
	}
	/**
	 * @brief Geef de waarde van het veld bugSortering.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld bugSortering van het object
	 * obj representeert.
	 */
	public static function waardeBugSortering(PersoonVoorkeur $obj)
	{
		return static::defaultWaardeEnum($obj, 'BugSortering');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld bugSortering.
	 *
	 * @see genericFormbugSortering
	 *
	 * @param PersoonVoorkeur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bugSortering staat en kan
	 * worden bewerkt. Indien bugSortering read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBugSortering(PersoonVoorkeur $obj, $include_id = false)
	{
		return static::defaultFormEnum($obj, 'BugSortering', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld bugSortering. In
	 * tegenstelling tot formbugSortering moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbugSortering
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld bugSortering staat en kan
	 * worden bewerkt. Indien bugSortering read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBugSortering($name, $waarde=NULL)
	{
		return static::genericDefaultFormEnum($name, $waarde, 'BugSortering', PersoonVoorkeur::enumsbugSortering());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * bugSortering bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld bugSortering representeert.
	 */
	public static function opmerkingBugSortering()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld favoBugCategorie.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld favoBugCategorie labelt.
	 */
	public static function labelFavoBugCategorie(PersoonVoorkeur $obj)
	{
		return 'FavoBugCategorie';
	}
	/**
	 * @brief Geef de waarde van het veld favoBugCategorie.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld favoBugCategorie van het
	 * object obj representeert.
	 */
	public static function waardeFavoBugCategorie(PersoonVoorkeur $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getFavoBugCategorie())
			return NULL;
		return BugCategorieView::defaultWaardeBugCategorie($obj->getFavoBugCategorie());
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld favoBugCategorie.
	 *
	 * @see genericFormfavoBugCategorie
	 *
	 * @param PersoonVoorkeur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld favoBugCategorie staat en
	 * kan worden bewerkt. Indien favoBugCategorie read-only is betreft het een
	 * statisch html-element.
	 */
	public static function formFavoBugCategorie(PersoonVoorkeur $obj, $include_id = false)
	{
		return BugCategorieView::defaultForm($obj->getFavoBugCategorie());
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld favoBugCategorie. In
	 * tegenstelling tot formfavoBugCategorie moeten naam en waarde meegegeven worden,
	 * en worden niet uit het object geladen.
	 *
	 * @see formfavoBugCategorie
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld favoBugCategorie staat en
	 * kan worden bewerkt. Indien favoBugCategorie read-only is, betreft het een
	 * statisch html-element.
	 */
	public static function genericFormFavoBugCategorie($name, $waarde=NULL)
	{
		return BugCategorieView::genericDefaultForm('FavoBugCategorie');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * favoBugCategorie bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld favoBugCategorie representeert.
	 */
	public static function opmerkingFavoBugCategorie()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld tourAfgemaakt.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvoor het veldlabel
	 * nodig is.
	 *
	 * @return string
	 * Een string die het veld tourAfgemaakt labelt.
	 */
	public static function labelTourAfgemaakt(PersoonVoorkeur $obj)
	{
		return 'TourAfgemaakt';
	}
	/**
	 * @brief Geef de waarde van het veld tourAfgemaakt.
	 *
	 * @param PersoonVoorkeur $obj Het PersoonVoorkeur-object waarvan de waarde wordt
	 * verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tourAfgemaakt van het object
	 * obj representeert.
	 */
	public static function waardeTourAfgemaakt(PersoonVoorkeur $obj)
	{
		return static::defaultWaardeBool($obj, 'TourAfgemaakt');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld tourAfgemaakt.
	 *
	 * @see genericFormtourAfgemaakt
	 *
	 * @param PersoonVoorkeur $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tourAfgemaakt staat en kan
	 * worden bewerkt. Indien tourAfgemaakt read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formTourAfgemaakt(PersoonVoorkeur $obj, $include_id = false)
	{
		return static::defaultFormBool($obj, 'TourAfgemaakt', $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld tourAfgemaakt. In
	 * tegenstelling tot formtourAfgemaakt moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formtourAfgemaakt
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld tourAfgemaakt staat en kan
	 * worden bewerkt. Indien tourAfgemaakt read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormTourAfgemaakt($name, $waarde=NULL)
	{
		return static::genericDefaultFormBool($name, $waarde, 'TourAfgemaakt');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * tourAfgemaakt bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tourAfgemaakt representeert.
	 */
	public static function opmerkingTourAfgemaakt()
	{
		return NULL;
	}
}
