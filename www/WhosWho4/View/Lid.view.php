<?

/**
 * $Id$
 */
abstract class LidView
	extends LidView_Generated
{
	/**
	 * Toont het formulier voor het maken van een nieuw lid; hoort bij Leden_Controller::nieuw()
	 */
	static public function nieuw ($lid, $form, $msg, $fullForm = true)
	{
		$page = Page::getInstance()
			 ->start(_("Nieuw lid"), 'leden');
		
		if ($msg !== null)
			$page->add($msg);
		if ($form)
			if ($msg === null)
				$page->add(LidView::nieuwForm('Nieuw', $lid, false, $fullForm));
			else
				$page->add(LidView::nieuwForm('Nieuw', $lid, true, $fullForm));
		$page->end();
	}

	/**
	 * Toont het formulier voor het wijzigen van een lid; hoort bij Leden_Controller::wijzig()
	 */
	static public function wijzig ($lid, $show_error)
	{
		$page = Page::getInstance()
			 ->start(LidView::makeLink($lid));

		$page->addFooterJS(Page::minifiedFile('knockout.js'))
			 ->addFooterJS(Page::minifiedFile('TelefoonNummers.js'));

		$page->add(LidView::wijzigForm('Wijzig', $lid, $show_error));
		$page->end();
	}

	/**
	 *  Geeft een string met de toestand van een lid terug;
	 *	  [Status] [(lid sinds lidVanDatum] . [ tot lidAfDatum)]
	 **/
	public static function lidToestandString($lid)
	{
		$status = $lid->getLidToestand();
		$lidVan = $lid->getLidVan()->hasTime() ? LidView::waardeLidVan($lid) : _('[VOC:lidVan-datum]onbekend');
		$return = LidView::labelenumLidToestand($status);

		if($status == 'OUDLID')
		{
			$lidTot = $lid->getLidTot()->hasTime() ? LidView::waardeLidTot($lid) : _('[VOC:lidTot-datum]onbekend');
			$return = $return . ' ' . sprintf(_('(lid van %s[VOC:datum] tot %s[VOC:datum])'),
					$lidVan, $lidTot);
		}
		elseif($status !== 'BIJNALID')
		{
			$return = $return . ' ' . sprintf(_('(lid sinds %s[VOC:datum])'), $lidVan);
		}

		if(hasAuth('bestuur')) {
			$return .= "<i>";
			if($lid->getLidMaster()) {
				$return .= _("[VOC: lidtoestand], masterlid");
			}
			if($lid->getLidExchange()) {
				$return .= _("[VOC: lidtoestand], exchange-student");
			}
			$return .= "</i>";
		}

		return $return;
	}

	/** Enkele labels + opmerkingen voor de formulieren **/
	public static function labelLidToestand(Lid $obj)
	{
		return _('Type lid');
	}
	public static function labelenumLidToestand($value)
	{
		switch ($value)
		{
			case 'BIJNALID': return _('Bijna lid');
			case 'LID': return _('Normaal lid');
			case 'OUDLID': return _('Oud-lid');
			case 'BAL': return _('Bijzonder actief lid');
			case 'LIDVANVERDIENSTE': return _('Lid van verdienste');
			case 'ERELID': return _('Ere-lid');
			case 'GESCHORST': return _('Geschorst');
			case 'BOEKENKOPER': return _('Boekenkoper');
			case 'BEESTJE': return _('[VOC: lidToestand]Beestje');
		}
		return parent::labelenumLidToestand($value);
	}
	public static function labelLidMaster(Lid $obj)
	{
		return _('Master-lid');
	}
	public static function labelLidExchange(Lid $obj)
	{
		return _('Exchange-student');
	}
	public static function labelLidVan(Lid $obj)
	{
		return _('Lid vanaf');
	}
	public static function labelLidTot(Lid $obj)
	{
		return _('Lid tot');
	}
	public static function labelStudentnr(Lid $obj)
	{
		return _('Student-nummer');
	}
	public static function labelOpzoekbaar(Lid $obj)
	{
		return _('Opzoekbaar');
	}
	public static function labelenumOpzoekbaar($value)
	{
		switch ($value)
		{
			case 'A': return _('[VOC: opzoekbaar]Voor alle (oud-)leden');
			case 'J': return _('[VOC: opzoekbaar]Voor alle huidige leden');
			case 'N': return _('[VOC: opzoekbaar]Voor niemand');
			case 'CIE': return _('[VOC: opzoekbaar]Voor mijn commissies');
		}
		return parent::labelenumOpzoekbaar($value);
	}
	public static function labelInAlmanak(Lid $obj)
	{
		return _('Opnemen in almanak');
	}
	public static function labelPlanerRSS(Lid $obj)
	{
		return _('Planet RSS');
	}

	/**
	 * Geef een formulier voor het aanmaken van een lid terug
	 */
	static public function nieuwForm ($name = 'Nieuw', $obj = null, $show_error = true, $fullForm = true)
	{
		/**
		 * We gaan 4 divs maken:
		 * - Persoonsinformatie
		 * - Contactsinformatie
		 * - Lidinformatie
		 * - Studies
		 */

		/** Als er nog geen object is meegestuurd, dan maken we zelf wel iets **/
		if (is_null($obj))
			$obj = new Lid();

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		if(!$fullForm)
			$form->add(new HtmlDiv(sprintf(_('Dit is het quickform, ga terug '
				. 'naar het %s[VOC: volledig formulier]')
				, new HtmlAnchor('?quick=0', _('volledig formulier')))
			, 'bs-callout bs-callout-warning'));
		else
			$form->add(new HtmlDiv(sprintf(_('Dit is het volledige formulier, '
				. 'er is ook een %s[VOC: quickform]!')
				, new HtmlAnchor('?quick=1', 'quickform'))
			, 'bs-callout bs-callout-warning'));

		$form->add(new HtmlDiv(_('Wil je een boekenkoper of een ander niet-lid '
				. 'aanmaken? Druk dan hier:') . " " .
				new HtmlAnchor('/Leden/Administratie/NieuwPersoon', _('nieuwpersoonformulier'))
			, 'bs-callout bs-callout-warning'));

		/** Eerst de div voor persoonsinformatie in te vullen **/
		$form->add(PersoonView::wijzigPersoonPersooninformatieDiv($obj, $show_error, $fullForm));

		/** De div voor de contactsinformatie **/
		$form->add(PersoonView::wijzigPersoonContactinformatieDiv($obj, $show_error, $fullForm));

		/** Dan de div voor lidinformatie in te vullen **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Lidgegevens")));
		$div->add(self::wijzigTR($obj, 'LidToestand', $show_error));
		$div->add(self::wijzigTR($obj, 'LidMaster', $show_error));
		$div->add(self::wijzigTR($obj, 'LidExchange', $show_error));
		$div->add(self::wijzigTR($obj, 'LidVan', $show_error));
		if($fullForm)
			$div->add(self::wijzigTR($obj, 'LidTot', $show_error));
		$div->add(self::wijzigTR($obj, 'Studentnr', $show_error));
		if($fullForm)
		{
			$div->add(self::wijzigTR($obj, 'Opzoekbaar', $show_error));
			$div->add(self::wijzigTR($obj, 'InAlmanak', $show_error));
			$div->add(self::wijzigTR($obj, 'PlanetRSS', $show_error));
			$div->add(self::wijzigTR($obj, 'Opmerkingen', $show_error));
		}

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Maak het lid aan!")));

		return $form;
	}

	/**
	 * Geef een formulier voor het wijzigen van een lid terug
	 */
	static public function wijzigForm ($name = 'Wijzig', $obj = null, $show_error = true)
	{
		/**
		 * We gaan 4 divs maken:
		 * - Persoonsinformatie
		 * - Contactsinformatie + telefoonnummers + adressen
		 * - Studies
		 * - Lidinformatie
		 */

		/** Als er nog geen object is meegestuurd, dan maken we zelf wel iets **/
		if (is_null($obj))
			$obj = new Lid();

		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		$form->add($div = new HtmlDiv(null, 'btn-group'));
		if($obj->getInDB())
			$div->add(HtmlAnchor::button('Foto/Wijzig', _('Pasfoto aanpassen')));
		$div->add(HtmlAnchor::button($obj->url() . '/WijzigAdres', _('Wijzig adressen')))
			->add(HtmlAnchor::button($obj->url() . '/WijzigStudies', _('Wijzig studies')));

		$form->add(self::wijzigPersoonPersooninformatieDiv($obj, $show_error));
		$form->add(self::wijzigPersoonContactinformatieDiv($obj, $show_error));
		$form->add(ContactTelnrVerzamelingView::wijzigTelnrsDiv($obj, $show_error));

		/** De div voor de abonnementen **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Abonnementen")));

		if(hasAuth('bestuur'))
			$mailingsBeschikbaar = MailingListVerzameling::alle();
		else
			$mailingsBeschikbaar = MailingListVerzameling::bereikbaar();

		$mailingsAangevinkt = MailingListVerzameling::vanContact($obj);
		$mailingUL = new HtmlList();
		//Vakidioot
		$check = HtmlInput::makeCheckbox('Vakid', $obj->getVakIdOpsturen());
		$check->setCaption(_("Vakidioot per post"));
		$mailingUL->add(new HtmlListItem($check));
		//Aes2Roots
		$check = HtmlInput::makeCheckbox('aes2roots', $obj->getAes2rootsOpsturen());
		$check->setCaption(_("A-Es2Roots"));
		$mailingUL->add(new HtmlListItem($check . " ". _("[VOC:A-es2roots](ontvang je alleen als je oud-lid bent)")));

		//Emaillijsten
		foreach($mailingsBeschikbaar as $mailingList)
		{
			if($mailingList->getVerborgen() || $mailingList->getQuery())
				continue;
			$check = HtmlInput::makeCheckbox('Mailing[' . $mailingList->getMailingListID() . ']', isset($mailingsAangevinkt[$mailingList->getMailingListID()]));
			$check->setCaption($mailingList->getNaam());
			$mailingUL->add(new HtmlListItem($check));
		}
		$div->add($mailingUL);

		/** Dan de div voor lidinformatie in te vullen **/
		if (hasAuth('bestuur'))
		{
			$form->add($div = new HtmlDiv());
			$div->add(new HtmlHeader(3, _("Lidgegevens")));
			$div->add(self::wijzigTR($obj, 'LidToestand', $show_error))
				->add(self::wijzigTR($obj, 'LidMaster', $show_error))
				->add(self::wijzigTR($obj, 'LidExchange', $show_error))
				->add(self::wijzigTR($obj, 'LidVan', $show_error))
				->add(self::wijzigTR($obj, 'LidTot', $show_error))
				->add(self::wijzigTR($obj, 'Studentnr', $show_error))
				->add(self::wijzigTR($obj, 'Opzoekbaar', $show_error))
				->add(self::wijzigTR($obj, 'InAlmanak', $show_error))
				->add(self::wijzigTR($obj, 'Opmerkingen', $show_error));
			$div->add($anchor = new HtmlAnchor());
			$anchor->setName('Opmerkingen');

			//En lid->persoon casting
			$div->add(new HtmlDiv(new HtmlDiv(HtmlAnchor::button('lidToPersoon', _('Lid omzetten naar persoon')), 'col-sm-offset-2 col-sm-10'), 'form-group'));
		}
		else
		{
			$form->add($div = new HtmlDiv());
			$div->add(new HtmlHeader(3, _("Lidgegevens")));
			$div->add(self::wijzigTR($obj, 'Opzoekbaar', $show_error))
				->add(self::wijzigTR($obj, 'InAlmanak', $show_error));
		}

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));

		return $form;
	}

	/**
	 * Verwerk het wijzig formulier
	 */
	static public function processWijzigForm ($obj)
	{
		/** Kloon het oude lid, om wijzigingen te bekijken **/
		$lidOud = clone $obj;

		/** Vul alle statische informatie in **/
		parent::processWijzigForm ($obj);
		self::processForm($obj);
		
		if($obj->isBijnaLid())
		{
			$adressenFiguur = $obj->getAdressen();

			$adressen = new ContactAdresVerzameling();
			$adressenCur = new ContactAdresVerzameling();

			foreach($adressenFiguur as $adres) {
				$clone = clone($adres);
				$adressen->voegtoe($clone);
				$adressenCur->voegtoe($clone);
			}
			if($adressen->aantal() == 0)
			{
				$nieuw = new ContactAdres($obj);
				$adressen->voegtoe($nieuw);
			}
			ContactAdresVerzamelingView::processWijzigAdres($adressen);
			//TODO: Dit moet dus gevalideerd en opgeslagen worden
		}
		
		if(count($lidOud->difference($obj)) > 0)
			$obj->wijzigingen['Lid'] = $lidOud->difference($obj);

		/** Verwerk abonnementen **/
		$obj->setVakIdOpsturen(tryPar('Vakid')); //Vakidioot
		$obj->setAes2rootsOpsturen(tryPar('aes2roots'));
		$wilMailingIds = array_filter(tryPar('Mailing', array()), function($x){return $x=='on';});
		$wilMailingKeys = array();
		foreach($wilMailingIds as $mailingId=>$on)
			$wilMailingKeys[] = array($obj->geefID(), $mailingId);
		$mailings = ContactMailingListVerzameling::verzamel($wilMailingKeys); //pakt de mailings die het lid wil én al heeft

		foreach($wilMailingIds as $mailingId=>$on)
		{
			if(!$mailings->bevat(array($obj->geefID(), $mailingId)))
			{
				$mailings->voegtoe(new ContactMailingList($obj->geefID(), $mailingId));
			}
		}
		$mailings->opslaan();

		global $WSW4DB;
		$nietWeggooien = array_merge(array_keys($wilMailingIds), MailingListVerzameling::nietMeenemen()->keys());
		$WSW4DB->q('DELETE FROM `ContactMailingList`'
					. ' WHERE `contact_contactID` = %i'
					. (($mailings->aantal() > 0) ? ' AND (`mailingList_mailingListID`) NOT IN (%Ai)' : '%_'),
					$obj->getContactID(),
					$nietWeggooien);// Emile: "Wie heeft deze lelijk code in de view gezet?
	}

	/**
	 * Toon een top25 lijst.
	 */
	public static function toonTop25Lijst($opties, $wat, $data)
	{
		$page = Page::getInstance();
		$page->start(_('Top 25 Leden'), 'leden');

		$page->add(_('Selecteer een veld:'));
		$page->add(new HtmlParagraph($form = new HtmlForm('get')));
		$form->addClass('form-inline');

		$form->add($inputGroup = new HtmlDiv(null, 'input-group'));

		$inputGroup->add($box = HtmlSelectbox::fromArray('lijst', $opties, $wat, 1));
		$box->setAutoSubmit();

		$inputGroup->add(new HtmlDiv(HtmlInput::makeSubmitButton('gogogo', 'toon'), 'input-group-btn'));

		$page->add(new HtmlDiv(new HtmlDiv($tbl = new HtmlTable(), "col-md-3"), "row"));
		$row = $tbl->addRow();
		$row->addHeader('#');
		$row->addHeader($opties[$wat]);

		foreach($data as $dataentry)
			{
				$row = $tbl->addRow();
				if(in_array(tryPar('lijst'), ['nuactieve zonder', 'nuactieve met', 'alltime actief zonder', 'alltime actief met','actief/niet actief']))
				{
					$row->addData($dataentry['a'])->setAttribute('align', 'right');
					$row->addData(self::makeLink(Persoon::geef($dataentry['b'])));
				}
				elseif(tryPar('lijst') == 'achievements met' || tryPar('lijst') == 'achievements zonder')
				{
					$row->addData(self::makeAchievementsLink(Persoon::geef($dataentry['b']), $dataentry['a']))->setAttribute('align', 'right');
					$row->addData(self::makeLink(Persoon::geef($dataentry['b'])));
				}
				else
				{
					$row->addData($dataentry['a'])->setAttribute('align', 'right');
					$row->addData($dataentry['b']);
				}
			}
		$page->end();
	}

/**
 * Genereer een stamboompagina voor het gegeven lid
 */

	static public function stamboom(Lid $lid)
	{
		$page = Page::getInstance();
		$page->addFooterJS(Page::minifiedFile('Stamboom.js'));
		$page->start(sprintf(_("Stamboom van %s"), PersoonView::naam($lid)));
		$page->add($a = HtmlAnchor::button('#', _('Klap alles uit')));
		$a->addClass('stamboom-expand-all btn btn-primary');
		$page->add(new HtmlHeader(3, _("Ouders")));
		$page->add(self::lidStamboom($lid, 0, true, true));
		$page->add(new HtmlHeader(3, _("Kindjes")));
		$page->add(self::lidStamboomRev($lid, 0, true, true));
		$page->end();
	}

/**
 * Genereer een div'je met de voorouders van het gegeven lid
 * @param lid Het lid waarvan je de boom zoekt
 * @param level Hoe diep we in de boom zitten
 * @param first Doe formattering alsof je helemaal onderaan de boom zit
 * @param uitgeklapt Laat voorouders van deze knoop meteen zien
 */

	static public function lidStamboom(Lid $lid, $level = 0, $first = false, $uitgeklapt = false)
	{
		$mg = $lid->mentorgroepen();
 
		$mentoren = new LidVerzameling();
		foreach($mg as $m) {
			$mentoren->union($m->mentoren());
		}

		$div = new HtmlDiv();

		if($first)
			$arrow = "";
		else
			$arrow = "<- ";

		$d = ' ';
		foreach($mg as $m) {
			$d .= sprintf(_("(%s) "), IntroGroepView::maakLink($m));
		}

		$laatVooroudersZien = $level < 3;

		if($mentoren->aantal() != 0) {
			if ($laatVooroudersZien) {
				if ($uitgeklapt) {
					$anch = new HtmlAnchor('#', _('Klap in'));
				} else {
					$anch = new HtmlAnchor('#', _('Klap uit'));
				}
				$anch->addClass('stamboom-expand');
				$anch->setCssStyle('font-weight: bold;');
			} else {
				$anch = new HtmlAnchor('#', _('Laden'));
				$anch->addClass('stamboom-fetch');
				$anch->setCssStyle('font-weight: bold;');
				$anch->setId($lid->geefID());
			}
			$div->add($a = new HtmlSpan($arrow . PersoonView::makeLink($lid) . $d . sprintf(" (%s)", $anch)));
		} else {
			$div->add($a = new HtmlSpan($arrow . PersoonView::makeLink($lid) . $d));
		}

		$div->add($newdiv = new HtmlDiv(null, 'stamboom-expandable'));
		$newdiv->setCssStyle('margin-left: 3%; border-left: dashed thin black;');

		if(!$uitgeklapt)
			$newdiv->setCssStyle($newdiv->getAttribute('style') . ' display: none');

		if ($laatVooroudersZien) {
			foreach($mentoren as $m) {
				$newdiv->add(self::lidStamboom($m, $level+1, false));
			}
		}
		return $div;
	}


/**
 * Genereer een div'je met de afstammelingen van het gegeven lid
 * @param lid Het lid waarvan je de boom zoekt
 * @param level Hoe diep we in de boom zitten
 * @param first Doe formattering alsof je helemaal onderaan de boom zit
 * @param uitgeklapt Laat kinderen van deze knoop meteen zien
 */
	static public function lidStamboomRev(Lid $lid, $level = 0, $first = false, $uitgeklapt = false)
	{
		$mg = $lid->mentorVan();

		$div = new HtmlDiv();

		if($first)
			$arrow = "";
		else
			$arrow = "-> ";

		$laatVooroudersZien = $level < 3;

		if($mg->aantal() != 0) {
			if ($laatVooroudersZien) {
				if ($uitgeklapt) {
					$anch = new HtmlAnchor('#', _('Klap in'));
				} else {
					$anch = new HtmlAnchor('#', _('Klap uit'));
				}
				$anch->addClass('stamboom-expand');
				$anch->setCssStyle('font-weight: bold;');
			} else {
				$anch = new HtmlAnchor('#', _('Laden'));
				$anch->addClass('stamboom-fetchRev');
				$anch->setCssStyle('font-weight: bold;');
				$anch->setId($lid->geefID());
			}

			$div->add($a = new HtmlSpan($arrow . PersoonView::makeLink($lid) . sprintf(" (%s)", $anch)));
		} else {
			$div->add($a = new HtmlSpan($arrow . PersoonView::makeLink($lid)));
		}

		$kindjes = new LidVerzameling();
		foreach($mg as $m) {
			$kindjes->union($m->kindjes());
		}

		$div->add($newdiv = new HtmlDiv(null, 'stamboom-expandable'));
		$newdiv->setCssStyle('margin-left: 3%; border-left: dashed thin black;');

		if(!$uitgeklapt)
			$newdiv->setCssStyle($newdiv->getAttribute('style') . ' display: none');

		foreach($mg as $m) {
			$mentoren = $m->mentoren();
			$mentoren->verwijder($lid->geefID());
			$mentornamen = array();
			foreach($mentoren as $mentor) {
				$mentornamen[]= PersoonView::makeLink($mentor);
			}

			if (!$mentornamen) {
				$metString = "";
			} else {
				$metString = sprintf(_("[VOC: mentor] (met %s)"), implode(_(' en '), $mentornamen));
			}
			$newdiv->add(new HtmlDiv(sprintf(new HtmlStrong(_("[VOC: mentor]Groepje %s")), IntroGroepView::maakLink($m)) . $metString));
			if ($laatVooroudersZien) {
				foreach($m->kindjes() as $k) {
					$newdiv->add(self::lidStamboomRev($k, $level+1, false));
				}
			}
		}

		return $div;
	}

	/**
	 *  Wijzigformulieronderdeel voor de studies van een lid.
	 * Maakt een array van TR's, eentje per studie van het lid. Handig om mensen lid-af te maken!
	 * @param lid Het lid warvan de studies moeten worden gewijzigd
	 * @param maakAlumnus Vink ook bij studerende leden het alumnus-vakje aan
	 */
	public static function lidStudieTRsWijzig($lid, $maakAlumnus = false) {
		$result = array();

		$lidStudies = $lid->getStudies();
		$aantalRows = max(1, $lidStudies->aantal());

		$lidRowBovenste = $lidRow = new HtmlTableRow();

		if($lid->getLidTot()->hasTime()||$lid->getLidToestand() == 'OUDLID')
		{
			$lidRow->addData();
			$lidRow->addData(PersoonView::makeLink($lid));
			$lidRow->addData();
			$lidRow->addData();
			$lidRow->addData(new HtmlDiv(_('Is al lidaf gemaakt!'), 'text-danger strongtext'));
			$result[] = $lidRow;

			return $result;
		}

		//Check of lid zelf al heeft aangegeven dat die gestopt is, nog geen lidaf datum heeft en ook nog niet oud-lid is.
			$gestopt = false;
		foreach($lidStudies as $lidstudie){
			if($lidstudie->getStatus() != 'STUDEREND' && !$lid->getLidTot()->hasTime() && !$lid->getLidToestand() == 'OUDLID')
				$gestopt = true;
		}

		$t1 = $lidRow->addData(HtmlInput::makeCheckbox($lid->getContactID(), $gestopt));
		$t2 = $lidRow->addData(array(PersoonView::makeLink($lid),
					new HtmlBreak(1), $lid->getStudentNr(),
					new HtmlBreak(1), $lid->getLidVan()->format('Y-m-d'),
					new HtmlBreak(1), $lid->getContactID(),
					($lid->getLidTot()->hasTime()||$lid->getLidToestand() == 'OUDLID'? _(' is al lidaf gemaakt!'):'')));

		$t1->setAttribute('rowspan', $aantalRows);
		$t2->setAttribute('rowspan', $aantalRows);

		//Bij meerdere studies moeten de x > 1de studies op een eigen row.
		$nrStudies = 0;

		//$lidStudies->rewind();
		foreach($lidStudies as $lidstudie){
			if($nrStudies > 0)
				$lidRow = new HtmlTableRow();

			$t3 = $lidRow->addData(array($lidstudie->getStudie()->getNaam(),
						new HtmlBreak(), $lidstudie->getStatus(),
						new HtmlBreak(), $lidstudie->getStudie()->getFase(),
						new HtmlBreak(), $lidstudie->getDatumBegin()->strftime('%Y-%m-%d'),
						new HtmlBreak(), $lidstudie->getDatumEind()->strftime('%Y-%m-%d')
						));

			// Vul een nuttige datum in als er nog geen einddatum bestaat.
			if ($lidstudie->getStatus() == 'STUDEREND') {
				$eindDatum = new DateTimeLocale('now');
			} else {
				$eindDatum = $lidstudie->getdatumEind();
			}

			$t4 = $lidRow->addData(array(
						HtmlInput::makeRadio($lid->getContactID() .'-'. $lidstudie->getLidStudieID(),'STUDEREND',
							(!$maakAlumnus && $lidstudie->getStatus() == 'STUDEREND'), false,
							$lid->getContactID() .'-'. $lidstudie->getLidStudieID().'studerend'),
						_('studerend'), new HtmlBreak(),
						HtmlInput::makeRadio($lid->getContactID() .'-'. $lidstudie->getLidStudieID(),'ALUMNUS',
							(($maakAlumnus && $lidstudie->getStatus() == 'STUDEREND') || $lidstudie->getStatus() == 'ALUMNUS'), false,
							$lid->getContactID() .'-'. $lidstudie->getLidStudieID().'alumnus'),
						_('alumnus'), new HtmlBreak(),
						HtmlInput::makeRadio($lid->getContactID() .'-'. $lidstudie->getLidStudieID(),'GESTOPT',
							($lidstudie->getStatus() == 'GESTOPT'), false,
							$lid->getContactID() .'-'. $lidstudie->getLidStudieID().'gestopt'),
						_('gestopt'), new HtmlBreak(), sprintf(_('eind: %s[VOC: datum einde studie]'),
						HtmlInput::makeDate($lid->getContactID() .'-'. $lidstudie->getLidStudieID().'-eind'),
							$eindDatum->strftime('%Y-%m-%d') )
						));

			//Extra kleur voor beter overzicht bij studiestatus
			if($lidstudie->getStatus() != 'STUDEREND')
				$t4->setAttribute('style','background: lightgray');

			//Let op, voeg hier al de row toe als er studies zijn!
			$result[] = $lidRow;
			$nrStudies++;
		}

		if($nrStudies == 0){
			$cell = $lidRow->addData(_('Dit lid heeft geen studies!'));
			$cell->setAttribute('colspan', 2);
			$result[] = $lidRow;
		}

		$opmerkingen = LidView::waardeOpmerkingen($lid);
		$toestand = $lid->getLidToestand();
		if($toestand == 'BAL' || $toestand == 'ERELID' || $toestand == 'LIDVANVERDIENSTE')
			$opmerkingen .= new HtmlSpan($toestand, 'waarschuwing');
		$cell = $lidRowBovenste->addData($opmerkingen);
		$cell->setAttribute('rowspan', $aantalRows); //align netjes met de rij

		return $result;
	}

	static public function detailsStudiesInfoDiv(Lid $lid)
	{
		$body = new HtmlDiv();

		if($lid != Persoon::getIngelogd()) {
            $relatie = Persoon::getIngelogd()->geefRelatieMet($lid);
            if($relatie === False)
                $body->add(new HtmlParagraph(_('Deze persoon is geen familie van je.'), 'alert alert-info'));
            else
			    $body->add(new HtmlParagraph(sprintf(_('Deze persoon is jouw %s.'), $relatie), 'alert alert-info'));
		} else {
			$body->add(new HtmlParagraph(_('Dit ben je zelf.'), 'alert alert-info'));
		}

		$body->add($h = new HtmlHeader(3, _('Studies')));

		if($lid->magWijzigen())
		{
			$h->add(new HtmlSmall(new HtmlAnchor($lid->url() . '/WijzigStudies', _('Wijzig'))));
		}

		$studies = $lid->getStudies();

		$body->add(new HtmlDiv(LidStudieVerzamelingView::kort($studies), 'detail'));
		$body->add(HtmlAnchor::button($lid->url() . '/Stamboom', _("Bekijk stamboom")));

		//Mentor geweest van:
		$groepen = $lid->mentorVan();
		if ($groepen->aantal() > 0)
			$body->add(new HtmlHeader(3, _('Mentor van:')))->add(IntroGroepVerzamelingView::toonGroepen($groepen));


		return $body;
	}

	static public function makeTourJS(Lid $lid)
	{
		$js = '
(function() {
	$(function() {
		console.log("bla");
		var $demo, duration, remaining, tour;
		$demo = $("#demo");
		duration = 5000;
		remaining = duration;
		var persId = $("#tour-id").html();
		tour = new Tour({
			backdrop: true,
			backdropContainer: "body",
			backdropPadding: 0,
			template: \'';

		$template = new HtmlDiv(null, 'popover tour');
		$template->add(new HtmlDiv(null, 'arrow'));
		$template->add($h = new HtmlHeader(3, null));
		$h->setNoDefaultClasses();
		$h->addClass('popover-title');
		$template->add(new HtmlDiv(null, 'popover-content'));
		$template->add($navigation = new HtmlDiv(null, 'popover-navigation'));
		$navigation->add($btn_group = new HtmlDiv(null, 'btn-group'));
		$btn_group->add($btn = new HtmlButton('button', _('« Vorige')));
		$btn->addClass('btn btn-default btn-sm');
		$btn->setAttribute('data-role', 'prev');
		$btn_group->add($btn = new HtmlButton('button', _('Volgende »')));
		$btn->addClass('btn btn-default btn-sm');
		$btn->setAttribute('data-role', 'next');

		$navigation->add($btn = new HtmlButton('button', _('Beëndig tour')));
		$btn->addClass('btn btn-default btn-sm');
		$btn->setAttribute('data-role', 'end');

		$js .= trim(preg_replace('/\s+/', ' ', $template->makeHtml())) . '\',';

		$js .= '
			onEnd: function() {
				$.get("/Ajax/Lid/TourAfgemaakt");
				return;
			},
			debug: true,
			steps: [
				{
					path: "/Home",
					title: "'._('Welkom bij de tour van de A-Eskwadraatsite').'",
					content: "'.sprintf(_('Je krijgt een tour van de %ssite '
							. 'omdat je actief lid bent. Als je al weet wat je '
							. 'allemaal kunt doen op de %ssite, kan '
							. 'je deze tour gerust over slaan.'), aesnaam(), aesnaam()).'",
					orphan: true,
				},
				{
					path: "/Home",
					title: "'._('Welkom bij de tour van de A-Eskwadraatsite').'",
					content: "'._('We gaan je hiermee verschillende aspecten '
							. 'van de site laten zien. Op naar de volgende pagina!').'",
					orphan: true,
				},
				{
					path: "'.$lid->url().'",
					title: "'._('Je eigen lidpagina').'",
					content: "'._('Dit is je eigen lidpagina. Hier staat al je '
							. 'eigen informatie op een rijtje.').'",
					orphan: true,
				},
				{
					path: "'.$lid->url().'",
					element: ".fa.fa-pencil",
					placement: "bottom",
					title: "'._('Wijzigen').'",
					content: "'._('Hier kan je je eigen persoonsinformatie wijzigen.').'",
				},
				{
					path: "'.$lid->url().'",
					element: "a[href=\'WijzigAdres\']",
					placement: "right",
					title: "'._('Wijzigen').'",
					content: "'._('Je adressen kan je hier wijzigen.').'",
				},
				{
					path: "'.$lid->url().'",
					element: "a[href=\'WijzigStudies\']",
					placement: "top",
					title: "'._('Wijzigen').'",
					content: "'._('Je studies kan je hier wijzigen.').'",
				},
				{
					path: "/Activiteiten/",
					title: "'._('Activiteitenoverzicht').'",
					content: "'._('Dit is het activiteitenoverzicht. Hier '
							. 'verschijnen alle komende activiteiten die '
							. aesnaam() . ' organiseert.').'",
					orphan: true,
				},
				{
					path: "/Activiteiten/",
					title: "'._('Activiteitenoverzicht').'",
					content: "'._('Nieuwe activiteiten kan je aanmaken via de '
							. 'link \'Nieuwe activiteit\' in het menuutje in de '
							. 'balk bovenaan de pagina in de tab \'Activiteiten\'.').'",
					orphan: true,
				},
				{
					path: "/Activiteiten/Planner/",
					title: "'._('Planneroverzicht').'",
					content: "'._('Dit is het overzicht van alle planners die '
							. 'jij kan zien. Hiermee kan je met je commissie een '
							. 'nieuwe activiteit of vergadering plannen. Of '
							. 'gezellig met je vrienden een uitje plannen!').'",
					orphan: true,
				},
				{
					path: "/Activiteiten/Planner/",
					element: "a[href=\'Nieuw\']",
					placement: "right",
					title: "'._('Nieuwe planner').'",
					content: "'._('Gebruik deze knop om een nieuwe planner aan te maken (dûh!).').'",
				},
				{
					path: "'.IOUBASE.'",
					title: "'._('I-Owe-U').'",
					content: "'._('Dit is je persoonlijke I-Owe-U-overzicht. '
							. 'I-Owe-U kan je gebruiken om met je commissies de '
							. 'kosten te verdelen als je bijvoorbeeld samen gegeten hebt.').'",
					orphan: true,
				},
				{
					path: "'.IOUBASE.'",
					title: "'._('I-Owe-U').'",
					content: "'._('Als je meerdere bonnen invoert (dus je moet '
							. 'meerdere keren geld verdelen) wordt dit autmatisch '
							. 'verrekend met nog openstaande schulden.').'",
					orphan: true,
				},
				{
					path: "'.IOUBASE.'",
					element: "a[href=\'/Leden/I-Owe-U/Bon/Nieuw\']",
					placement: "bottom",
					title: "'._('Nieuwe Bon').'",
					content: "'._('Hiermee voeg je een nieuwe bon toe om zo geld te verdelen.').'",
				},
				{
					path: "'.IOUBASE.'",
					element: "a[href=\'/Leden/I-Owe-U/Betaling/Nieuw\']",
					placement: "bottom",
					title: "'._('Nieuwe verrekening').'",
					content: "'._('Als je mensen hun geld hebt overgemaakt moet '
							. 'je hiervoor een nieuwe verrekening invoeren.').'",
				},
				{
					path: "'.IOUBASE.'",
					element: "a[href=\'/Leden/I-Owe-U/Verzoeken\']",
					placement: "bottom",
					title: "'._('Wanbetalers mailen').'",
					content: "'._('Als stomme mensen je nog steeds niet terugbetaald '
							. 'hebt kan je ze daar vriendelijk aan herinneren '
							. 'door ze een verzoek tot betaling te mailen.').'",
				},
				{
					path: "'.TENT_HOME.'",
					title: "'._('Tentamens').'",
					content: "'._('Dit is de tentamenpagina. Hier kan je zoeken '
							. 'op een vak en alle oude tentamens bekijken.').'",
					orphan: true,
				},
				{
					path: "'.TENT_HOME.'",
					title: "'._('Tentamens').'",
					content: "'._('Je kan zelf ook uitwerkingen voor tentamens '
							. 'uploaden om zo je medestudent te helpen!').'",
					orphan: true,
				},
				{
					path: "'.PAPIERMOLENBASE.'",
					title: "'._('Papiermolen').'",
					content: "'._('Bij de papiermolen kunnen studenten zelf '
							. 'tweedehands boeken verkopen.').'",
					orphan: true,
				},
				{
					path: "'.PAPIERMOLENBASE.'",
					element: "a[href=\'/Onderwijs/PapierMolen/Overzicht\']",
					placement: "bottom",
					title: "'._('Overzicht').'",
					content: "'._('Bekijk het overzicht van alle tweedehands '
							. 'boeken die nu beschikbaar zijn...').'",
				},
				{
					path: "'.PAPIERMOLENBASE.'",
					element: "a[href=\'/Onderwijs/PapierMolen/Nieuw\']",
					placement: "bottom",
					title: "'._('Boek plaatsen').'",
					content: "'._('...of plaats zelf een boek in de verkoop!').'",
				},
				{
					path: "/Carriere/Vacaturebank/",
					title: "'._('Vacaturebank').'",
					content: "'._('Dit is de vacaturebank. Hier staan regelmatig '
							. 'vacatures van bedrijven voor bijbaantjes en '
							. 'startersfuncties.').'",
					orphan: true,
				},
				{
					path: "/Service/Intern/Handleidingen/",
					title: "'._('Handleidingen').'",
					content: "'._('Een handige pagina is de handleidingenpagina. '
							. 'Hier staan, zoals de naam zegt, verscheidenen '
							. 'handleidingen, van een handleiding voor actieve '
							. 'leden tot HAUD (Het Afkoringen-Uitleg-Document).').'",
					orphan: true,
				},
				{
					path: "'.PUBLISHERBASE.'",
					title: "'._('Publisher').'",
					content: "'._('Met publisher kan je een html-pagina maken '
							. 'voor je commissie of voor een activiteit. Neem '
							. 'bijvoorbeeld eens een keertje een kijkje op de '
							. 'pagina van de AfterpartCie om te kijken wat er '
							. 'allemaal mee mogelijk is!').'",
					orphan: true,
				},
				{
					path: "'.BUGWEBBASE.'",
					title: "'._('Bugweb').'",
					content: "'._('Dit is bugweb. Hier houden de technische '
							. 'commissies bij wat er allemaal voor een problemen '
							. 'opgelost moeten worden. Kijk rond waar ze allemaal '
							. 'mee bezig zijn...').'",
					orphan: true,
				},
				{
					path: "'.BUGWEBBASE.'",
					element: \'a[href="/Service/Bugweb/Melden"]\',
					placement: "bottom",
					title: "'._('Bug melden').'",
					content: "'._('...of meld zelf een bug als je iets bent '
							. 'tegengekomen wat kapot is of je een leuke suggestie hebt.').'",
				},
				{
					path: "/Service/Kart/",
					title: "'._('Kart').'",
					content: "'._('De kart is bedoeld om mensen of commissies '
							. 'in te stoppen. Vervolgens kan je met alles in je '
							. 'kart een actie doen zoals iedereen een mail sturen, '
							. 'een koppenblad bekijken of ze allemaal exporteren '
							. 'als bestand. Handig als je gegevens van veel mensen '
							. 'nodig hebt voor een activiteit.').'",
					orphan: true,
				},
				{
					path: "'.DOCUBASE.'",
					title: "'._('Docuweb').'",
					content: "'._('Tenslotte is er nog Docuweb. Hier plaatst het '
							. 'bestuur belangrijke stukken zoals notulen van '
							. 'vergaderingen. Een goed actief lid houdt ook controle '
							. 'op het bestuur!').'",
					orphan: true,
				},
				{
					path: "/Home",
					title: "'._('Eind').'",
					content: "'._('We hopen dat je heel wat wijzer geworden bent '
							. 'van deze tour. Als er nog vragen zijn of suggesties, '
							. 'mail de webcie. Neem vooral ook zelf nog een kijkje '
							. 'op de rest van de site om te kijken wat er nog meer mogelijk is!').'",
					orphan: true,
				},
		]
		}).init();
		tour.start();
	});

}).call(this);';
		return $js;
	}

	static public function defaultWaardeLid(Lid $lid)
	{
		return PersoonView::defaultWaardePersoon($lid);
	}

	static public function labelStudie()
	{
		return _('Studie');
	}

	static public function opmerkingStudie()
	{
		return '';
	}

	static public function formStudie(Lid $obj, $include_id = false)
	{
		return HtmlSelectbox::fromArray('studie',
			   LidStudie::alleStudiesArray(),
					(string)tryPar('studie'), 1, false);
	}

	static public function wijzigBijnaLid ($lid, $adres, $betaling, $show_error = false)
	{
		$page = Page::getInstance()
			 ->addHeadJS('/Minified/js/WijzigBijnaLid.js')
			 ->start(LidView::makeLink($lid));
		$page->add(LidView::wijzigBijnaLidForm($lid, $adres, $betaling, $show_error));
		$page->end();
	}

	static public function wijzigBijnaLidForm ($lid, $adres, $betaling, $show_error = true)
	{
		$form = HtmlForm::named('WijzigBijnaLid');

		// Algemene info
		$form->add(new HtmlParagraph(sprintf(_('Hier moet je je eigen gegevens controleren. Velden met een rood sterretje erbij zijn verplicht om in te vullen. Je kan ten alle tijden deze gegevens aanpassen op de Website van %s'), aesnaam()), 'bs-callout bs-callout-info'));

		$form->add(self::wijzigTR($lid, 'Voornaam', $show_error))
			 ->add(self::wijzigTR($lid, 'Voorletters', $show_error))
			 ->add(self::wijzigTR($lid, 'Tussenvoegsels', $show_error))
			 ->add(self::wijzigTR($lid, 'Achternaam', $show_error))
			 ->add(self::wijzigTR($lid, 'Geslacht', $show_error))
			 ->add(self::wijzigTR($lid, 'Voornaamwoord', $show_error))
			 ->add(self::wijzigTR($lid, 'Datumgeboorte', $show_error))
			 ->add(self::wijzigTR($lid, 'Email', $show_error))
			 ->add(self::wijzigTR($lid, 'Opzoekbaar', $show_error))
			 ->add(PersoonVoorkeurView::wijzigTR($lid->getVoorkeur(), 'Taal', $show_error))
			 ->add(self::wijzigTR($lid, 'InAlmanak', $show_error));

		$form->add(new HtmlHR());

		$form->add(ContactAdresView::wijzigTR($adres, 'Straat1', $show_error))
			 ->add(ContactAdresView::wijzigTR($adres, 'Straat2', $show_error))
			 ->add(ContactAdresView::wijzigTR($adres, 'Huisnummer', $show_error))
			 ->add(ContactAdresView::wijzigTR($adres, 'Postcode', $show_error))
			 ->add(ContactAdresView::wijzigTR($adres, 'Woonplaats', $show_error))
			 ->add(ContactAdresView::wijzigTR($adres, 'Land', $show_error));

		$form->add(new HtmlHR());

		// We gaan ervan uit dat we geen extra telefoonnummers willen hebben of
		// telefoonnummers willen weghalen
		$telnrs = $lid->getTelefoonNummers();
		$i = 0;
		foreach($telnrs as $telnr) {
			$form->add(self::makeFormStringRow('telnr'.($i++), $telnr->getTelefoonNummer(), ContactTelnrView::labelTelefoonnummer($telnr)));
		}

		$form->add(new HtmlHR());

		$form->add($div = new HtmlDiv());

		//Vakidioot, kennelijk werkt de wijzigTR hiervoor niet goed :S
		$div->add(
			self::makeFormBoolRow('Vakid'
			, $lid->getVakIdOpsturen()
			, _('Vakidioot per post')
			, sprintf(_('De Vakidioot is het verenigingsblad van %s wat je zes keer per jaar ontvangt als je hierop geabonneerd bent. Vink dit aan als je de nummers op papier wilt ontvangen.'), aesnaam()))
		);

		// We doen hier een hardcoding van de mailinglists omdat de namen van de lists letterlijk
		// 'Activiteitenmailinglist' zijn oid, dat staat een beetje vreemd voor
		// de bijnaleden. Ook willen we niet alle lists die beschikbaar zijn hier
		// hebben, de Engelse variant van de actmailing hoeft hier bv niet.
		$mailingsAangevinkt = MailingListVerzameling::vanContact($lid);

		$div->add(self::makeFormBoolRow('Mailing[15]'
			, isset($mailingsAangevinkt[15])
			, _('Vakidioot per e-mail')
			, sprintf(_('De Vakidioot is het verenigingsblad van %s wat je zes keer per jaar ontvangt als je hierop geabonneerd bent. Vink dit aan als je de nummers per e-mail wilt ontvangen.'), aesnaam()))
		);

		$div->add(self::makeFormBoolRow('Mailing[7]'
			, isset($mailingsAangevinkt[7])
			, _('Activiteitenmailing')
			, _('De activiteitenmailing wordt iedere week naar je toe gemaild als je hierop geabonneerd bent. Van de activiteitenmailing wordt er een Nederlandse en een Engelse versie')));

		$div->add(self::makeFormBoolRow('Mailing[1]'
			, isset($mailingsAangevinkt[1])
			, _('Stages- & banenmailing')
			, _('De stages- & banenmailing wordt gemiddeld twee keer per maand verstuurd met daarin informatie over arbeidsmarktoriëntatie van verschillende bedrijven.')));

		// Indien toepasselijk, voeg een betalingsform toe.
		if (Dili_Controller::inschrijvingBetalen()) {
			$form->add($betaaldiv = new HtmlDiv());

			$betaaldiv->add(new HtmlHR());

			$betaaldiv->add(LidmaatschapsBetalingView::wijzigTR($betaling, 'soort', $show_error));

			// Betaalopties als de gebruiker machtiging selecteert.
			// JavaScript zorgt ervoor dat deze disabled zijn als ze een andere soort kiezen.
			$betaaldiv->add($machtigdiv = new HtmlDiv(null, null, 'machtigingform'));
			$machtigdiv->add(LidmaatschapsBetalingView::wijzigTR($betaling, 'rekeningnummer', $show_error));
			$machtigdiv->add(LidmaatschapsBetalingView::wijzigTR($betaling, 'rekeninghouder', $show_error));
		}

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton(_("Wijzigen")));

		return $form;
	}

	static public function processWijzigBijnaLidForm ($lid, $adres, $telnrs)
	{
		self::processForm($lid);

		$lid->setVakidOpsturen(tryPar('vakidOpsturen', $lid->getVakidOpsturen()));

		ContactAdresView::processForm($adres);

		$i = 0;
		foreach($telnrs as $telnr) {
			$telnr->setTelefoonnummer(tryPar('telnr'.($i++), $telnr->getTelefoonNummer()));
		}
	}

	static public function formOpzoekbaar(Lid $obj, $include_id = false)
	{
		if(!$obj->isBijnaLid()) {
			return parent::formOpzoekbaar($obj, $include_id);
		}

		// Voor bijnaleden heeft de optie 'Alleen voor commissie' niet veel nut
		// dus dan halen we die weg zodat het niet rare vragen oplevert bij het
		// controleren van de gegevens door bijnaleden tijdens de intro.
		$veld = 'Opzoekbaar';
		$selected = false;

		$name = self::formveld($obj, $veld, null, $include_id);
		$enums = self::callobj($obj, 'enums', $veld);
		$waarde = self::callobj($obj, 'get', $veld);

		if($waarde != 'CIE') {
			$pos = array_search('CIE', $enums);
			unset($enums[$pos]);
		}

		return self::genericDefaultFormEnum($name, $waarde, $veld, $enums, $selected);
	}

	static public function opmerkingOpzoekbaar()
	{
		return sprintf(_('Leden zijn standaard opzoekbaar voor alle (oud-)leden met een inlog op de %ssite. Je gegevens zijn op geen enkele manier opzoekbaar voor derden.'), aesnaam());
	}
}

// vim:sw=4:ts=4:tw=0:foldlevel=1
