<?

/**
 * $Id$
 */
abstract class DeelnemerView
	extends DeelnemerView_Generated
{
	/**
	 *  Verwerk een hele verzameling antwoorden van een deelnemer, als er geen antwoord
	 *  is gegeven slaan we de lege string op. Geen antwoord opslaan is natuurlijk
	 *  veel netter maar maakt de implementatie een stuk meer werk en heeft een
	 *  slechtere bang/buck ratio.
	 */
	public static function processAntwoordForm($dlnr)
	{
		$antwoorden = $dlnr->getAntwoorden();
		foreach($antwoorden as $antwoord)
		{
			$antwoordData = tryPar(self::formobj($antwoord, true), NULL);
			if($antwoordData['Antwoord'] == NULL)
			{
				DeelnemerView::processForm($antwoord, 'viewWijzig', '');
				//$antwoorden->verwijder($antwoord);
			} else {
				DeelnemerAntwoordView::processForm($antwoord, 'viewWijzig', $antwoordData);
			}
		}
	}

	//Geef een link naar waar je deze deelnemer kan wijzigen.
	public static function wijzigKnop($deelnemer)
	{
		return HtmlAnchor::button(
			$deelnemer->wijzigURL(),
			_('Wijzigen'),
			null,
			'btn-sm'
		);
	}

	public static function uitschrijfKnop($deelnemer)
	{
		// TODO
		return HtmlAnchor::button(
			$deelnemer->uitschrijfURL(),
			_('Uitschrijven'),
			null,
			'btn-sm'
		);
	}

	public static function labelPersoon(Deelnemer $obj)
	{
		return _('Persoon');
	}
	public static function labelActiviteit(Deelnemer $obj)
	{
		return _('Activiteit');
	}
	public static function labelMomentInschrijven(Deelnemer $obj = NULL)
	{
		return _('Moment van inschrijven');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
