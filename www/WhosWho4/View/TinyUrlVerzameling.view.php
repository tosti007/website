<?

/**
 * $Id$
 */
abstract class TinyUrlVerzamelingView
	extends TinyUrlVerzamelingView_Generated
{

	static public function lijst(TinyUrlVerzameling $verz)
	{
		$div = new HtmlDiv();

		$div->add($table = new HtmlTable(null, 'sortable'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("ID"), _("TinyUrl"), _("OutgoingUrl"), _("Hits"),
			_("# koppelingen")));

		foreach($verz as $tinyUrl)
		{
			$row = $tbody->addRow();
			$row->addData(TinyUrlView::makeLink($tinyUrl));
			$row->addData(TinyUrlView::waardeTinyUrl($tinyUrl));
			$row->addData(TinyUrlview::waardeOutgoingUrl($tinyUrl));
			$row->addData(TinyUrlview::waardeHits($tinyUrl));
			$row->addData(TinyMailingUrlVerzameling::vanUrl($tinyUrl)->aantal());
		}

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
