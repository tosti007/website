<?
abstract class DocuBestandView
	extends DocuBestandView_Generated
{
	/*
	 *  De functie voor het geven van een wijzigform van het object.
	 *        Kan ook gebruikt worden om nieuwe objecten aan te maken
	 *
	 * @param docu Het docubestand om te wijzigen
	 * @param nieuw Een bool om aan te geven of het een nieuw object betreft
	 * @param msg Een met de foutmeldingen
	 * @param show_error Bool met of de errors moeten worden weergegeven
	 */
	static public function wijzigForm (DocuBestand $docu, $nieuw = false, $show_error = true)
	{
		if($nieuw)
			$page = Page::getInstance()->start(_("Nieuw bestand toevoegen"));
		else
			$page = Page::getInstance()->start(sprintf(_("Wijzig %s"),
				DocuBestandView::waardeTitel($docu)));

		// Maak het form aan waar we alles in stoppen
		$page->add($form = HtmlForm::named("DocuWijzig"));
		$form->setAttribute('enctype', 'multipart/form-data');
		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($docu, 'datum', $show_error));
		$div->add(self::wijzigTR($docu, 'titel', $show_error));
		$div->add(self::wijzigTR($docu, 'categorie', $show_error));
		$div->add(self::wijzigTR($docu, 'bestand', $show_error));

		if($nieuw)
			$form->add(HtmlInput::makeFormSubmitButton(_("Voeg toe")));
		else
			$form->add(HtmlInput::makeFormSubmitButton(_("Wijzig")));

		$page->end();
	}

	static public function opmerkingCategorie()
	{
		return '';
	}

	static public function formCategorie(DocuBestand $obj, $include_id = false)
	{
		$select = new HtmlSelectbox("categorie");

		$cats = DocuCategorieVerzameling::getCategorieen();
		foreach($cats as $id => $cat)
			$select->add(new HtmlSelectboxOption($id,
				DocuCategorieView::waardeOmschrijving($cat)));

		$select->select(tryPar('DocuBestand[Categorie]',
			($obj->getCategorie() ? $obj->getCategorie()->geefID() : NULL)));

		return $select;
	}

	static public function labelBestand()
	{
		return _('Bestand');
	}

	static public function opmerkingBestand()
	{
		return '';
	}

	static public function formBestand()
	{
		return HtmlInput::makeFile("bestand");
	}
}
