<?

/**
 * $Id$
 */
abstract class ContactAdresVerzamelingView
	extends ContactAdresVerzamelingView_Generated
{
	/**
	 * Toont het formulier voor het wijzigen van een adres van een persoon
	 */
	static public function wijzigAdres(ContactAdresVerzameling $adressen, $show_error = false)
	{
		$contact = $adressen->last()->getContact();

		if($contact instanceof Persoon) {
			$naam = PersoonView::naam($contact);
		} elseif($contact instanceof Bedrijf) {
			$naam = BedrijfView::naam($contact);
		} else {
			$naam = sprintf(" contact %s", $contact->geefID());
		}

		$page = new HTMLPage();
		$page->start(sprintf("Adres wijzigen van %s", $naam), 'leden');
		$page->add(ContactAdresVerzamelingView::wijzigAdresForm('Wijzig', $adressen, $show_error));
		return $page;
	}

	/**
	 * Geef een formulier voor het wijzigen van adressen terug
	 */
	static public function wijzigAdresForm ($name = 'Wijzig', ContactAdresVerzameling $adressen, $show_error = true)
	{
		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		$form->add(self::wijzigAdressenDiv($adressen, $show_error));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeSubmitButton(_("Wijzigen")));

		return $form;
	}

	/*
	 * Geeft een div met alle velden voor het wijzigen van adressen
	 */
	public static function wijzigAdressenDiv(ContactAdresVerzameling $adressen, $show_error, $allow_new = true)
	{
		$div = new HtmlDiv();

		/** De adressen **/
		$div->add($idi = new HtmlDiv());

		$first = true;
		foreach ($adressen as $adres)
		{
			$idi->add($iidiv = new HtmlDiv());

			if($allow_new && $adres == $adressen->last())
				$iidiv->add(self::makeFormBoolRow('NieuwAdres', tryPar('NieuwAdres')
				, _('Nieuw adres toevoegen')));

			$iidiv->add(new HtmlHeader(4, ContactAdresView::waardeSoort($adres)));
			$iidiv->add(ContactAdresView::wijzigTR($adres, 'Soort', $show_error, true))
				  ->add(ContactAdresView::wijzigTR($adres, 'Straat1', $show_error, true))
				  ->add(ContactAdresView::wijzigTR($adres, 'Straat2', $show_error, true))
				  ->add(ContactAdresView::wijzigTR($adres, 'Huisnummer', $show_error, true))
				  ->add(ContactAdresView::wijzigTR($adres, 'Postcode', $show_error, true))
				  ->add(ContactAdresView::wijzigTR($adres, 'Woonplaats', $show_error, true))
				  ->add(ContactAdresView::wijzigTR($adres, 'Land', $show_error, true));

			if($adres != $adressen->last())
			{
				$iidiv->add(self::makeFormBoolRow('ContactAdres['.$adres->geefID().'][verwijder]'
					, tryPar('ContactAdres['.$adres->geefID().'][verwijder]')
					, _('Verwijder dit adres')));

				$iidiv->add(new HtmlHR());
			}
			$first = false;
		}

		return $div;

	}

	public static function processWijzigAdres(ContactAdresVerzameling $adressen)
	{
		foreach($adressen as $adres) {
			ContactAdresView::processForm($adres, 'viewWijzig', null, true);
		}
	}

	public static function verwijderAdres($pers) {
		$page = new HTMLPage();
		$page->start();
		// Toon formulier voor verwijderen
		$page->add(self::verwijderAdresForm($pers));
		return $page;
	}

	private static function verwijderAdresForm($pers) {
		$adresForm = HtmlForm::named('verwijderAdresForm');
		$adresForm->add(new HtmlHeader(2, _('Kies het adres dat verwijderd moet worden: ')));
		$adressen = $pers->getAdressen();
		if ($adressen->aantal() == 0) {
			$adresForm->add(new HtmlParagraph(_("Er zijn geen adressen die verwijderd kunnen worden!")));
			return $adresForm;
		}

		foreach ($adressen as $adres) {
			$adresDiv = ContactAdresView::detailsDiv($adres);
			$adresDiv->add(HtmlInput::makeRadio('soort', $adres->getSoort()))
				->add(new HtmlSpan(_("Deze weggooien")));
			$adresForm->add($adresDiv);
		}

		$adresForm->add(new HtmlParagraph(_("Door op verwijder adres te drukken krijgt het lid/persoon bericht dat zijn adres niet meer klopt, ook wordt het adres verwijderd en in de opmerkingen gezet om het eventueel terug te zetten.")));
		$adresForm->add(HtmlInput::makeSubmitButton(_('Verwijder adres')));
		return $adresForm;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
