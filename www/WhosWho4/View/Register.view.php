<?
abstract class RegisterView
	extends RegisterView_Generated
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in RegisterView.
	 *
	 * @param obj Het Register-object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeRegister(Register $obj)
	{
		return self::defaultWaarde($obj);
	}

}
