<?
abstract class PersoonBadgeView
	extends PersoonBadgeView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 *  Maakt van een badge-naam een string van de titel
	 *
	 * @param string De badgenaam
	 *
	 * @return De badge-titel als string
	 */
	public static function toString($string)
	{
		global $BADGES;

		if(array_key_exists($string, $BADGES['onetime'])) {
			return $BADGES['onetime'][$string]['titel'];
		} else {
			$exploded = explode('_', $string);
			$value = array_pop($exploded);
			$string = implode('_', $exploded);
			if(array_key_exists($string, $BADGES['bsgp'])) {
				$soort = ($value / $BADGES['bsgp'][$string]['value']);
				if($BADGES['bsgp'][$string]['multi'] == 'long') {
					$soortArray = array(1 => 'brons', 2 => 'zilver', 5 => 'goud', 10 => aesnaam());
				} else {
					$soortArray = array(1 => 'brons', 2 => 'zilver', 3 => 'goud', 4 => aesnaam());
				}
				return $BADGES['bsgp'][$string]['omschrijving'] . " " . $soortArray[$soort];
			}
		}
	}

	public static function badgeNaam($badgeArray) {
		$naam = $badgeArray['titel'];
		if ($badgeArray['schaduw'] && hasAuth('god')) {
			$naam .= ' (schaduw)';
		}
		return $naam;
	}

	/**
	 *  Maakt een tablecell voor een badge voor gebruik bij het weergeven
	 *  van de onetime achievements
	 *
	 * @param badge De badgenaam
	 * @param badgeArray De badgeArray met de informatie
	 * @param pers De persoon waarbij de badge hoort
	 *
	 * @return Een tablecell met daarin een overzicht van de achievement
	 */
	public static function toTableCell($badge, $badgeArray, $pers)
	{
		$cell = new HtmlTableDataCell($div = new HtmlDiv(),'badge-cell');

		$div->add(new HtmlDiv(PersoonBadgeView::badgeNaam($badgeArray), 'bold'));

		$div->add($img = new HtmlImage('/Layout/Images/Icons/aesrood.png'
			, PersoonBadgeView::toString($badge)
			, ''
			, PersoonBadgeView::toString($badge)));
		$img->addClass('achievementImg');

		$div->add(new HtmlDiv($badgeArray['omschrijving']));

		if(!$pers->hasBadge($badge)) {
			$div->addClass('nietGehaald');
		}

		return $cell;
	}

	/**
	 *  maakt een tablerow voor een bsgp-achievement voor het achievement-
	 *  overzicht
	 *
	 * @param badge De achievement-info als array
	 * @param key De naam van de achievement
	 * @param pers De persoon waarbij de achievements horen
	 *
	 * @return Een tablerow met de achievements
	 */
	public static function makeBSGPRow($badge, $key, $pers)
	{
		if($badge['multi'] == 'long')
			$multiplier = array(1 => 'brons', 2 => 'zilver', 5 => 'goud', 10 => 'aesrood');
		elseif($badge['multi'] == 'short')
			$multiplier = array(1 => 'brons', 2 => 'zilver', 3 => 'goud', 4 => 'aesrood');

		$row = new HtmlTableRow();
		$row->addData($badge['omschrijving'])->addClass('badge-info omschrijving');

		// Tel hoeveel voor deze badge de persoon al gehaald heeft
		$count = PersoonBadge::badgeBSGPCount($key, $pers);

		foreach($multiplier as $multi => $img)
		{
			$keyMult = $key . '_' . ($badge['value'] * $multi);

			$row->addData(
				array($img = new HtmlImage('/Layout/Images/Icons/' . $img . '.png'
						, PersoonBadgeView::toString($keyMult)
						, ''
						, PersoonBadgeView::toString($keyMult))
					, $prog = new HtmlDiv())
			)->addClass('badge-cell');

			if($count < $badge['value'] * $multi)
			{
				$prog->addClass('progress');
				$prog->add($div = new HtmlDiv(null, 'progress-bar progress-bar-partial'));

				// Zet hier een round omheen omdat css niet met kommagetallen
				// om kan gaan O.o
				$width = round(100 * min(1, $count / ($badge['value'] * $multi)));
				$div->setCssStyle("width: " . $width . "%;");

				$div->add(sprintf("%s/%s", $count, $badge['value'] * $multi));

				$img->addClass('nietGehaald achievementImg');
			}
			else
			{
				$prog->addClass('progress');

				$prog->add($div = new HtmlDiv(null, 'progress-bar progress-bar-success'));

				$div->add(sprintf("%s/%s", $badge['value'] * $multi, $badge['value'] * $multi));

				$img->addClass('achievementImg');
			}
		}

		return $row;
	}
}
