<?

/**
 * $Id$
 */
abstract class OrganisatieVerzamelingView
	extends OrganisatieVerzamelingView_Generated
{

	static public function htmlList($orgs)
	{
		if (!$orgs || !$orgs->aantal())
			return new HtmlEmphasis(_('Geen resultaten gevonden'));

		$page = new HtmlDiv();

		$table = new HtmlTable(null, 'sortable');
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->addHeader(_("Naam"));
		$row->addHeader(_("Soort"));
		$row->addheader(_("Email"));
		$row->addHeader(_("Website"));
		$row->addHeader(_("Omschrijving"));

		foreach($orgs as $org){
			$row = $tbody->addRow();
			$row->addData(OrganisatieView::waardeNaamUrl($org));
			$row->addData(OrganisatieView::waardeSoort($org));
			$row->addData(OrganisatieView::waardeEmail($org));
			$row->addData(OrganisatieView::waardeHomepage($org));
			$row->addData(OrganisatieView::waardeOmschrijving($org));
		}

		$page->add($table);

		return $page;
	}

	static public function simpel($orgs)
	{
		$page = new HtmlDiv();

		$table = new HtmlTable();
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->addHeader(_("Naam"));
		$row->addHeader(_("Website"));
		$row->addHeader(_("Omschrijving"));

		foreach($orgs as $org){
			$row = $tbody->addRow();
			$row->addData(OrganisatieView::waardeNaamUrl($org));
			$row->addData(OrganisatieView::waardeHomepage($org));
			$row->addData(OrganisatieView::waardeOmschrijving($org));
		}

		$page->add($table);

		return $page;
	}

	static public function csv($orgs)
	{
		$result = "naam;soort;contactpersoon;functie;telefoon;email;www;straat;nr;postcode;plaats;bezoek_straat;bezoek_snr;bezoek_spostcode;bezoek_splaats;omschrijving" . "\n";
		foreach($orgs as $org){
			$result .= OrganisatieView::csvRegel($org) . "\n";
		}
		return $result;
	}

	static public function latex($orgs)
	{
		$result = "%naam   contactpers   adres,pc+plaats   telefoon    KiX" . "\n";
		foreach($orgs as $org){
			$result .= OrganisatieView::latexRegel($org);
		}
		return $result;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
