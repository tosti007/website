<?
abstract class ActiviteitCategorieView
	extends ActiviteitCategorieView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het categorieobject waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeActiviteitCategorie(ActiviteitCategorie $obj)
	{
		return self::waardeTitel($obj);
	}

	/**
	 *  Geef een formulier om een nieuwe categorie te maken.
	 * Indien de ActiviteitCategorie NULL is, wordt die automagisch aangemaakt.
	 * @param obj Het ActiviteitCategorie waar de inhoud gezet moet worden.
	 * @returns Een HTML-formulier om in je pagina te zetten.
	 */
	public static function nieuwForm(ActiviteitCategorie $obj = null) {
		if (is_null($obj)) {
			$obj = new ActiviteitCategorie("Generieke categorie", "Generic category");
		}

		$form = HtmlForm::named("nieuweCategorie", "/Activiteiten/Categorie/Nieuw");
		$form->add(self::createForm($obj));
		$form->add(HtmlInput::makeSubmitButton(_("Nieuwe categorie")));
		return $form;
	}

	/**
	 *  Geef een formulier om een categorie te wijzigen.
	 * @param obj Het ActiviteitCategorie waar de inhoud gezet moet worden.
	 * @returns Een HTML-formulier om in je pagina te zetten.
	 */
	public static function wijzigForm(ActiviteitCategorie $obj) {
		$form = HtmlForm::named("wijzigCategorie");
		$form->add(self::createForm($obj));
		$form->add(HtmlInput::makeSubmitButton(_("Wijzig categorie")));
		return $form;
	}

	/**
	 *  Geef een formulier om een categorie te selecteren.
	 * @param selectedCat De defaultcategorie die al geselecteerd is, of NULL.
	 * @param name De naam van dit formulieronderdeel, of "ActiviteitCategorie".
	 * @param magNull Sta ook toe dat je geen categorie selecteert.
	 * @returns Een HtmlInput-achtig formulieronderdeel.
	 */
	public static function selectieForm($selectedCat=NULL, $name="ActiviteitCategorie", $magNull=false) {
		if (!$selectedCat) {
			// Geef ook een optie om (nog?) geen categorie te hebben.
			$selectboxen = array("" => _("(geen categorie)"));
			$selectedCats = array("");
		} else {
			if ($magNull) {
				$selectedCats = array("");
			} else {
				$selectedCats = array();
			}
			$selectedCats = array($selectedCat->geefId());
		}
		$allCats = ActiviteitCategorieQuery::table()->verzamel();
		foreach ($allCats as $catID => $cat) {
			$selectboxen[$catID] = self::waardeTitel($cat);
		}
		return HtmlSelectbox::fromArray($name, $selectboxen, $selectedCats);
	}
}
