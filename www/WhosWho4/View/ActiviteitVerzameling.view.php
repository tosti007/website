<?

/**
 * $Id$
 */
abstract class ActiviteitVerzamelingView
	extends ActiviteitVerzamelingView_Generated
{
	/**
	 *  Toont aankomende activiteiten; hoort bij Activiteiten_Controller::aankomend()
	 *
	 * @param ActiviteitVerzameling activiteiten De activiteiten die je wilt tonen
	 * @param bool $matrix Een bool of het overzicht in matrix moet
	 * @return HTMLPage|Page
	 */
	static public function aankomend (ActiviteitVerzameling $activiteiten, $matrix)
	{
		global $request;

		$page = (new HTMLPage())
			->addFooterJS(Page::MinifiedFile('agenda','js'))
			->addHeadCSS(Page::MinifiedFile('activiteiten','css'))
			->start(_("Aankomende activiteiten"), 'activiteiten')
			->add(new HtmlDiv(sprintf(_('Welkom op de activiteitenpagina! Wil '
			. 'jij altijd op de hoogte blijven van al onze activiteiten? Voeg '
			. 'dan nu de %s toe aan jouw Calendar-app! Vergeet je ook niet voor '
			. 'activiteiten in te schrijven als je aanwezig zult zijn.')
			, new HtmlAnchor('/Activiteiten/Agenda/ics', _('activiteitenagenda')))
		, 'alert alert-info'));

		// Maak de knoppen en filter-optie aan het begin van de pagina
		// We stoppen alles in een form zodat de selectbox klein is en
		// op dezelfde regel komt. Ook posten we naar een speciefieke url
		// om alle hashes uit de url weg te halen
		$page->add(array($form = new HtmlForm('GET'), new HtmlBreak()));
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeHidden('matrix', tryPar('matrix', 0)));

		$query_array = array_merge($request->query->all(), array('matrix' => ($matrix ? '0' : '1')));
		$swapLabel = ($matrix ? _("Bekijk in normaal overzicht") : _("Bekijk in matrixoverzicht"));

		$form->add(new HtmlDiv(array(
			HtmlAnchor::button('?' . http_build_query($query_array), $swapLabel),
			HtmlAnchor::button('/Activiteiten/Afgelopen', _("Bekijk ook oude activiteiten"))
		), "btn-group"));
		static::filterForm($form);

		//Genereert de daadwerkelijke agenda.
		$page->add(ActiviteitVerzamelingView::pimpAgenda($activiteiten, $matrix, true));
		return $page;
	}

	/**
	 *  Toont archief van activiteiten; hoort bij Activiteiten_Controller::archief()
	 *
	 * @param ActiviteitVerzameling $activiteiten De activiteiten waarvan je het archief wilt
	 * @param bool $matrix Bool of het in matrixoverzicht moet
	 * @param bool $colyear String welk collegejaar je bekijkt
	 * @return HTMLPage|Page
	 */
	static public function archief (ActiviteitVerzameling $activiteiten, $matrix, $colyear)
	{
		global $request;

		$header = "Afgelopen Activiteiten Collegejaar ". $colyear." - " . ($colyear+1);

		$page = (new HTMLPage())
			->addFooterJS(Page::MinifiedFile('agenda','js'))
			->addHeadCSS(Page::MinifiedFile('activiteiten','css'))
			->start($header, 'activiteit');

		// Maak de knoppen en filter-optie aan het begin van de pagina
		// We stoppen alles in een form zodat de selectbox klein is en
		// op dezelfde regel komt. Ook posten we naar een speciefieke url
		// om alle hashes uit de url weg te halen
		$page->add(array($form = new HtmlForm('GET', '/Activiteiten/Afgelopen'), new HtmlBreak()));
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeHidden('matrix', tryPar('matrix', 0)));

		$query_array = array_merge($request->query->all(), array('matrix' => ($matrix ? '0' : '1')));
		$swapLabel = ($matrix ? _("Bekijk in normaal overzicht") : _("Bekijk in matrixoverzicht"));

		$form->add(new HtmlDiv(array(
			HtmlAnchor::button('?' . http_build_query($query_array), $swapLabel),
			HtmlAnchor::button('/Activiteiten/', _("Bekijk ook komende activiteiten"))
		), "btn-group"));

		$colyearSelectbox = new HtmlSelectbox('colyear', 'colyear');
		//Zorg ervoor dat je geen data in de toekomst kan selecteren
		$maxJaar = date('Y');
		if(date("n") <= COLJAARSWITCHMAAND) $maxJaar--;
 
		for($i = 1988; $i <= $maxJaar; $i++) {
			$colyearSelectbox->addOption($i, $i . ' - ' . ($i+1));
		}
		$colyearSelectbox->select($colyear)->setAutoSubmit();
		$form->add(array(
			new HtmlLabel('colyear', '&nbsp;' . _("Toon archiefjaar: ")),
			$colyearSelectbox
		));

		static::filterForm($form);
		$page->add(ActiviteitVerzamelingView::pimpAgenda($activiteiten, $matrix, false));
		return $page;
	}

	/**
	 *  Geeft alle computerreserveringen; hoort bij Activiteiten_Controller::computerReserveringen()
	 *
	 * @param ActiviteitVerzameling activiteiten De activiteitVerzameling waarvan je de reserveringen wilt
	 * @param DateTimeLocale beginDatum De begindatum van de periode waarin je bekijkt
	 * @param DateTimeLocale eindDatum De einddatum van de periode waarin je bekijkt
	 * @return Page
	 */
	static public function computerReserveringen (ActiviteitVerzameling $activiteiten, DateTimeLocale $beginDatum, DateTimeLocale $eindDatum)
	{
		$beginDatumTekst = self::outputDate($beginDatum->format('Y-m-d'));
		$eindDatumTekst = self::outputDate($eindDatum->format('Y-m-d'));

		$div = new HtmlDiv(NULL, NULL, 'activiteiten');
		if($activiteiten->aantal() == 0) {
			$div->add(new HtmlSpan(sprintf(_('Geen computerreserveringen tussen %s en %s.')
				, $beginDatumTekst
				, $eindDatumTekst)));
		} else {
			$div->add(new HtmlParagraph(sprintf(_('Op deze pagina staan alle '
				. 'computerreserveringen tussen %s en %s.')
				, $beginDatumTekst
				, $eindDatumTekst)));

			$tbl = new HtmlTable();

			$thead = $tbl->addHead();
			$tbody = $tbl->addBody();

			$row = $thead->addRow();
			$row->makeHeaderFromArray(array(_('Wat'), _('Wie'), _('Begindatum')
				, _('Einddatum'), _('Aantal computers')));

			/* Al deze activiteiten in de tabel stoppen */
			foreach($activiteiten as $activiteit)
			{
				$datumBegin = $activiteit->getMomentBegin()->format('Y-m-d');

				$tbody->add($row = new HtmlTableRow());

				$commissies = $activiteit->getCommissies();
				$row->addData(new HtmlAnchor($activiteit->url(), ActiviteitView::titel($activiteit)));
				$row->addData(CommissieVerzamelingView::kommaLijst($commissies));
				$row->addData(ActiviteitView::waardeMomentBegin($activiteit));
				$row->addData(ActiviteitView::waardeMomentEind($activiteit));
				$row->addData(ActiviteitView::aantalComputers($activiteit));
			}
			$div->add($tbl);
		}

		return (new HTMLPage())
			->start(_("Computerreserveringen in de werkkamer"))
			->add($div);
	}

	/**
	 *  Maakt een lijst van activiteiten die klikbaar zijn, gescheiden door kommas
	 *
	 * @param acts De activiteiten waarvan je de lijst wil maken
	 *
	 * @return Een span met daarin de lijst
	 */
	static public function klikbaarKommaLijst($acts)
	{
		$result = array();
		foreach($acts as $act){
			$result[] = ActiviteitView::makeLink($act);
		}
		return new HtmlSpan(implode(', ', $result));
	}

	/**
	 *  Maakt een htmllijst van een sloot activiteiten
	 *
	 * @param acts De activiteiten waarvan je de lijst wilt
	 *
	 * @return Een HtmlEmphasis als er geen acts zijn en een HtmlList anders
	 */
	static public function htmlLijst($acts)
	{
		if (!$acts || !$acts->aantal())
			return new HtmlEmphasis(_('Geen resultaten gevonden'));

		$result = new HtmlList();

		foreach($acts as $act){
			$result->add(ActiviteitView::lijstRegelKlikbaar($act));
		}
		return $result;
	}

	/**
	 *  Toont legenda
	 *
	 * @return De div met de legenda
	 */
	static public function legendaAgenda ()
	{
		$legenda = new HtmlDiv();
		$legenda->add(new HtmlHeader(4, _("Legenda")));

		$legendaTable = new HtmlTable();

		$row = $legendaTable->addRow();
		$row->addData("&#9673;", 'actsoort actsoort_aes2');
		$row->addData(_("A–Eskwadraatactiviteit"));
		$row = $legendaTable->addRow();
		$row->addData("&#9679;", 'actsoort actsoort_uu');
		$row->addData(_("Activiteit van de Universiteit Utrecht"));
		$row = $legendaTable->addRow();
		$row->addData("&#10012;", 'actsoort actsoort_zus');
		$row->addData(_("Activiteit georganiseerd door een zustervereniging"));
		$row = $legendaTable->addRow();
		$row->addData("&#9684;", 'actsoort actsoort_land');
		$row->addData(_("Landelijke activiteit"));
		$row = $legendaTable->addRow();
		$row->addData("&#8364;", 'actsoort actsoort_.com');
		$row->addData(_("Arbeidsmarktori&eumlntatieactiviteit"));
		$row = $legendaTable->addRow();
		$row->addData("&#9762;", 'actsoort actsoort_ext');
		$row->addData(_("Externe Activiteit"));
		$legenda->add($legendaTable);

		return $legenda;
	}

	/**
	 *  Returneert een html-container van een activiteitenverzameling afhankelijk van de gekozen stijl
	 *
	 * @param acts De activiteiten waarvan je de container wilt
	 * @param matrix Een bool of het in matrix moet
	 * @param toonDeelnemers Een bool of de deelnemers getoond moeten worden
	 *
	 * @return De html-container
	 */
	static public function pimpAgenda (ActiviteitVerzameling $acts, $matrix = false, $toonDeelnemers)
	{
		if ($acts->aantal() == 0)
			$agendaTable = new HtmlSpan(_("Er zijn geen activiteiten gevonden."));
		else
		{
			if($matrix) {
				$agendaTable = ActiviteitVerzamelingView::matrixAgenda($acts);
			} else {
				$agendaTable = ActiviteitVerzamelingView::lijstAgenda($acts, $toonDeelnemers);
			}
		}
		return $agendaTable;
	}

	/**
	 *  Returneert een overzicht met alle activiteiten in lijst-form
	 *
	 * @param acts De activiteiten waarvan je het overzicht wilt
	 * @param toonDeelnemers Bool of deelnemers moeten worden laten zien
	 *
	 * @return Een htmldiv met het act-overzicht
	 */
	static public function lijstAgenda (ActiviteitVerzameling $acts, $toonDeelnemers = true)
	{
		$agendaDiv = new HtmlDiv(null, 'row', 'act-row');

		// Voeg de divs toe aan de pagina
		$agendaDiv->add($actLijstDiv = new HtmlDiv(null, 'col-md-4', 'act-table-div'));
		$agendaDiv->add($actGrootDiv = new HtmlDiv(null, 'col-md-8', 'act-omschrijving'));

		// de activiteiten staan in een tabel
		$actLijstDiv->add($actTable = new HtmlTable(null, null, 'act-table'));

		$oudMaand = $oudJaar = 0;

		foreach($acts as $act)
		{
			//Bij nieuwe maand of jaar -> geef header
			$date = $act->getMomentBegin();
			if($date->format('m') != $oudMaand || $date->format('Y') != $oudJaar)
			{
				//Sla de oude datum op
				$oudMaand = $date->format('m');
				$oudJaar = $date->format('Y');

				$row = $actTable->addRow();
				$row->addClass('maand-row');
				$cell = $row->addData(new HtmlHeader(3, ucwords($date->strftime('%B %Y'))));
				$cell->setAttribute('colspan', 2);
			}

			// De act-row
			$row = $actTable->addRow();

			$date = $act->getMomentBegin();
			$einddatum = $act->getMomentEind();

			$datumtekst = $date->strftime('%a %e %k:%M');

			// Als de einddag niet hetezelfde is als de begin dag, geef dan
			// expliciet de einddag ook weer
			if ($einddatum->format('d-m-Y') != $date->format('d-m-Y'))
				$datumtekst .= new HtmlBreak() . ' &ndash; ' . $einddatum->strftime('%a %e');

			$intern = ($act->getToegang() == "PRIVE"?" (intern)":"");
			$internClass = ($act->getToegang() == "PRIVE"?"internAct": null);

			$row->addData($datumtekst, "maanddag");

			$cell = new HtmlTableDataCell();

			if($act->posterPath())
				$cell->add($aRow = new HtmlDiv(new HtmlDiv($aDiv = new HtmlDiv(null, 'actHeader ' . $internClass), 'col-xs-8'), 'row'));
			else
				$cell->add($aDiv = new HtmlDiv(null, 'actHeader ' . $internClass));

			$aDiv->add(new HtmlHeader(4, ActiviteitView::titel($act) . $intern, 'acttitle'));
			$aDiv->add(new HtmlParagraph(new HtmlEmphasis(
				CommissieVerzamelingView::kommalijst($act->getCommissies(), false)
			)));

			// Voeg de div met actdata toe zodat deze gekoppied kan worden
			// naar de rechterdiv of voor gebruik op mobiele pagina's
			$aDiv->add($actData = new HtmlDiv(null, 'act-data', $act->geefID()));
			$actData->setCssStyle('display: none;');

			if($act->posterPath())
			{
				global $filesystem;
				//kijk of een thumbnail bestaat, zoja laad die dan in:
				if($filesystem->has(explode(".", $act->posterPath())[0] . "thumbnail.jpg")){
					$aRow->add($aDiv = new HtmlDiv(null, 'col-xs-4'));
					$aDiv->add($img = new HtmlImage($act->url() . '/ActposterThumbnail', 'Actposter'));
				}
				//laad anders de gigantische versie in:
				else{
					$aRow->add($aDiv = new HtmlDiv(null, 'col-xs-4'));
					$aDiv->add($img = new HtmlImage($act->url() . '/Actposter', 'Actposter'));
				}
				$img->setCssStyle('width: auto; height: auto; max-width: 100%; max-height: 100%;');
			}

			$row->addImmutable($cell);
		}

		return $agendaDiv;
	}

	/**
	 *  Returneert een tabel met alle activiteiten in matrix-form
	 *
	 * @param acts De activiteitVerzameling
	 *
	 * @return Een Htmldiv met daarin het matrix-overzicht
	 */
	static public function matrixAgenda (ActiviteitVerzameling $acts)
	{
		$agendaDiv = new HtmlDiv();

		$acts->sorteer('BeginMoment');

		//yy-m-d
		$first_date = clone $acts->first()->getMomentBegin();
		$first_date->modify('-2 hour');
		$end_date = clone $acts->last()->getMomentBegin();
		$end_date->modify('+1 day');

		// alles in de cache
		$actcies = CommissieActiviteitVerzameling::vanActiviteiten($acts);
		$actcies->toCommissieVerzameling();

		$last_date = new Datetime('1980-01-01');
		for($date = $first_date; $date < $end_date; $date->modify('+1 day'))
		{
			if($last_date->format('Y') == 1980 || $last_date->format('n') != $date->format('n'))
			{
				if($last_date->format('Y') != 1980 && $date->format('N') != 1)
				{
					for($i = 7; $i >= $date->format('N'); $i--)
						$currentRow->addEmptyCell('filler');
				}

				$last_date = clone $date;

				$agendaDiv->add($currentDiv = new HtmlDiv());
				$currentDiv->add(new HtmlHeader(3, $date->strftime('%B %Y')))
					->add($currentTable = new HtmlTable(null, 'matrix'));

				$thead = $currentTable->addHead();
				$tbody = $currentTable->addBody();

				$row = $thead->addRow();
				$row->makeHeaderFromArray(array("",_("Maandag"),_("Dinsdag"),
					_("Woensdag"),_("Donderdag"),_("Vrijdag")));
				$row->addHeader(_("Zaterdag"), 'weekend');
				$row->addHeader(_("Zondag"), 'weekend');

				if($date->format('N') != 1)
				{
					$currentRow = $tbody->addRow();
					$currentRow->addData($date->format('W'));
					for ($i = 1; $i < $date->format('N'); $i++)
						$currentRow->addEmptyCell('filler');
				}
			}

			if($date->format('N') == 1)
			{
				$tbody->add($currentRow = new HtmlTableRow());
				$currentRow->addData($date->format('W'));
			}

			if ($date->format('N') < 6)
				$cell = $currentRow->addData(new HtmlSpan($date->format('j')));
			else
				$cell = $currentRow->addData(new HtmlSpan($date->format('j')), 'weekend');

			$cell->add($list = new HtmlList());
			foreach($acts->subActiviteitenOpDag($date) as $act)
				$list->add(new HtmlListItem(ActiviteitView::makeLink($act) .' '. ActiviteitInformatieView::computerReserveringIcon($act, true),
					'actsoort_' . strtolower($act->getActsoort())
				));
		}

		if($last_date->format('YY') != 1980 && $date->format('w') != 1)
		{
			for($i = 7; $i >= $date->format('N'); $i--)
				$currentRow->addEmptyCell('filler');
		}

		return $agendaDiv;
	}

	/**
	 *  Geeft een ICS-bestand van aankomende activiteiten; hoort bij Activiteiten_Controller::ics()
	 *
	 * @param ActiviteitVerzameling activiteiten De activiteiten waarvan je het ics-bestand wilt
	 * @return Page
	 */
	static public function ics (ActiviteitVerzameling $activiteiten)
	{
		return (new ICSPage())
			->start(null, _("Alle aankomende activiteiten"))
			->add(ActiviteitVerzamelingView::VCalendarEvent($activiteiten));
	}

	/**
	 *  Geeft een RSS-bestand van aankomende activiteiten; hoort bij Activiteiten_Controller::rss()
	 *
	 * @param ActiviteitVerzameling $activiteiten De Activiteiten waarvan je het rss-bestand wilt
	 * @return Page
	 */
	static public function rss (ActiviteitVerzameling $activiteiten)
	{
		return (new RSSPage())
			->start(_("Agenda A–Eskwadraat"), _("Alle aankomende activiteiten"))
			->add(ActiviteitVerzamelingView::rssItem($activiteiten));
	}

	/**
	 *  Geeft een array van RSS-items terug
	 *
	 * @param acts De activiteitVerzameling waarvan je rss-items wilt
	 *
	 * @return De rss-items als arrau
	 */
	static public function rssItem (ActiviteitVerzameling $acts)
	{
		$output = array();
		foreach ($acts as $act)
		{
			$output[] = ActiviteitView::rssItem($act);
		}
		return $output;
	}

	/**
	 *  Geeft een array van VCalendar-events terug
	 *
	 * @param acts De activiteitVerzameling
	 *
	 * @return De VCalender-events in een array
	 */
	static public function VCalendarEvent (ActiviteitVerzameling $acts)
	{
		$output = array();
		foreach ($acts as $act)
		{
			$output[] = ActiviteitView::VCalendarEvent($act);
		}
		return $output;
	}

	/**
	 *  Retourneert een tabel met alle activiteiten in lijst-form die geschikt is voor de activiteitenmailgenerator
	 *
	 * @param acts De activiteitVerzameling
	 *
	 * @return De table
	 */
	static public function lijstAgendaActMail(ActiviteitVerzameling $acts)
	{
		$tabel = new HtmlTable(null, 'lijst');
		$headerRow = new HtmlTableRow(null, 'uitgebreid');
		$headerRow->makeHeaderFromArray(array('NL', 'Titel', 'EN', 'Title', 'Tijd/Bewerken?'));
		$tabel->add(new HtmlTableHead($headerRow))
			->add($tbody = new HtmlTableBody());
		$rows = array();

		foreach ($acts as $act)
		{
			$box1NL = new HtmlElementCollection();
			$box1NL->add(new HtmlParagraph($checkNLTitel = HtmlInput::makeCheckBox('actTitelNL[]')));
			$checkNLTitel->setValue($act->geefID());
			$box1NL->add(new HtmlParagraph($checkNLTekst = HtmlInput::makeCheckBox('actTekstNL[]')));
			$checkNLTekst->setValue($act->geefID());

			$box1EN = new HtmlElementCollection();
			$box1EN->add(new HtmlParagraph($checkENTitel = HtmlInput::makeCheckBox('actTitelEN[]')));
			$checkENTitel->setValue($act->geefID());
			$box1EN->add(new HtmlParagraph($checkENTekst = HtmlInput::makeCheckBox('actTekstEN[]')));
			$checkENTekst->setValue($act->geefID());

			if($act->getMomentBegin()->getTimeStamp() < strtotime("next monday +1 month"))
			{
				if($act->getMomentBegin()->getTimeStamp() < strtotime("next monday +1 week"))
				{
					$checkNLTekst->setChecked();
					$checkENTekst->setChecked();
				}
				$checkNLTitel->setChecked();
				$checkENTitel->setChecked();
			}

			$box2EN = new HtmlElementCollection();
			$box2NL = new HtmlElementCollection();

			$info = $act->getInformatie();

			$box2NL->add(new HtmlStrong($act->getTitel('nl')), new HtmlParagraph($info->getWerftekst('nl')));
			$box2NL->add(new HtmlParagraph(array(new HtmlEmphasis("Locatie: "), ActiviteitInformatieView::waardeLocatie($info))));
			$box2EN->add(new HtmlStrong($act->getTitel('en')), new HtmlParagraph($info->getWerftekst('en')));

			$row = $tbody->addRow();
			$row->addData($box1NL);
			$row->addData($box2NL);
			$row->addData($box1EN);
			$row->addData($box2EN);
			$row->addData(new HtmlParagraph(array(
				ActiviteitView::waardeMomentBegin($act),
				new HtmlBreak(),
				" -- ",
				new HtmlBreak(),
				ActiviteitView::waardeMomentEind($act),
				new HtmlBreak(),
				new HtmlAnchor($act->url() . '/Wijzig', new HtmlImage('/Layout/Images/Buttons/edit.png', 'Bewerk')),
			)), 'uitgebreid');


		}

		return $tabel;
	}

	/**
	 *  Maakt een pseudo-actmail
	 *
	 * @param acts De activiteitVerzameling
	 * @param metTekst Bool of er tekst in de mail moet
	 * @param lang Welke taal
	 *
	 * @return de pseudo-mail
	 */
	public static function pseudoActMail(ActiviteitVerzameling $acts, $metTekst, $lang)
	{
		if($lang == 'en')
			setlocale(LC_TIME, 'en_GB');
		else
			setlocale(LC_TIME, 'nl_NL');

		$koptekst = "--actmail:Geleuter--\n\n\n--actmail:LedenLoterijWinnaar--\n";
		if($lang == 'nl')
			$koptekst .= "Het lid van de week is: #####. Die kan wat lekkers ophalen in de A–Eskwadraatkamer.";
		else
			$koptekst .= "The member of the week is: #####. They can get a sweet treat at the A–Eskwadraat room.";
		$koptekst .= "\n\n--actmail:Agenda--\n\n";

		$bodytekst = '';

		foreach($acts as $act)
		{
			$act = $act->getInformatie();
			if(in_array($act->getActiviteitID(), $metTekst))
				$koptekst .= '* ';
			else
				$koptekst .= '  ';

			//Geef de datumstring mooi weer
			$datumstr = $act->getMomentBegin()->strftime("%d");
			if($act->getMomentBegin()->strftime("%d") == $act->getMomentEind()->strftime("%d"))
				$datumstr .= ' ' . $act->getMomentBegin()->strftime("%b");
			else
			{
				if($act->getMomentBegin()->strftime("%b") == $act->getMomentEind()->strftime("%b"))
					$datumstr .= '-' . $act->getMomentEind()->strftime("%d") . ' ' . $act->getMomentBegin()->strftime("%b");
				else
					$datumstr .= ' ' . $act->getMomentBegin()->strftime("%b") . '-' . $act->getMomentEind()->strftime("%d %b");

			}
			$koptekst .= $datumstr . '|' . htmlspecialchars($act->getTitel($lang)) . "\n";

			//Voor in de body
			if(in_array($act->getActiviteitID(), $metTekst))
			{
				$bodytekst .= "\n\n--actmail:Activiteit--\n";
				$bodytekst .= "> " . htmlspecialchars($act->getTitel($lang)) . " <\n";

				//Geef de datumstring mooi weer
				$datumstr = $act->getMomentBegin()->strftime("%a %d"); //Maa 02
				if($act->getMomentBegin()->strftime("%d") == $act->getMomentEind()->strftime("%d"))
					$datumstr .= ' ' . $act->getMomentBegin()->strftime("%b %H:%M") . ' - ' . $act->getMomentEind()->strftime("%H:%M"); //Jan 12:00 - 14:00
				else
				{
					$datumstr .= ' ' . $act->getMomentBegin()->strftime("%b %H:%M") . ' - ' . $act->getMomentEind()->strftime("%a %d %b %H:%M");
				}

				$bodytekst .= ">> " . CommissieVerzamelingView::kommalijst($act->getCommissies(), false);
				$bodytekst .= ', '. $datumstr;
				$locatie = ActiviteitInformatieView::waardeLocatie($act);
				if(!empty($locatie))
					$bodytekst .= ' @ ' . $locatie;
				$bodytekst .= " <<";

				$bodytekst .=  "\n>>>" . $act->url() . "<<<\n\n";
				$bodytekst .= $act->getWerfTekst($lang);
			}
		}
		return $koptekst . $bodytekst;
	}

	/**
	 *  Maakt een act-overzicht van een verzameling van cies
	 *
	 * @param CommissieVerzameling $cieVerzameling De commissieverzameling waarvan je de acts wilt
	 * @param ActiviteitVerzameling $actVerzameling De actverzameling die nog extra toegevoegd wordt
	 * @return HTMLPage|Page
	 */
	public static function vanCommissiesLijst(CommissieVerzameling $cieVerzameling, ActiviteitVerzameling $actVerzameling)
	{
		$page = (new HTMLPage())->start();
		$body = new HtmlDiv();
		$body->add(new HtmlHeader(2, _("Activiteiten van ") . CommissieVerzamelingView::kommaLijst($cieVerzameling)));
		$actVerzameling->sorteer('beginMoment', true);
		if($actVerzameling->aantal())
			$body->add(ActiviteitVerzamelingView::htmlLijst($actVerzameling));
		else
			$body->add(new HtmlParagraph(_("Er zijn geen activiteiten gevonden")));
		$page->add($body);
		return $page;
	}

	/**
	 *  Maakt een algemene foto-overzicht van activiteiten
	 *
	 * @param ActiviteitVerzameling $acts De acitiviteiten die in het foto-overzicht komen
	 */
	public static function overzichtMetPage(ActiviteitVerzameling $acts)
	{
		$page = (new HTMLPage())->start(_("Foto-overzicht"));

		$page->add($div = new HtmlDiv());

		$div->add($linkdiv = new HtmlDiv(null, 'bs-callout bs-callout-info'));
		$linkdiv->add(sprintf(_("Klik %s voor meer informatie over FotoWeb "), new HtmlAnchor('/FotoWeb',_("hier"))));

		if($jaar = tryPar('jaar',null))
		{
			$jaren = ActiviteitVerzameling::geefAlleCollegeJarenMetFotos();
			$linkdiv->add($divv = new HtmlDiv());

			foreach ($jaren as $thisjaar)
			{
				if (strlen($thisjaar) != 4) continue; // ongeldig jaar

				if ($thisjaar > coljaar()) continue; // toekomst

				$thisTitle = "$thisjaar-" . ($thisjaar + 1);

				if($jaar == $thisjaar)
					$divv->add(new HtmlSpan(new HtmlStrong($thisTitle)));
				else
					$divv->add(new HtmlAnchor("/Activiteiten/Fotos?jaar=$thisjaar",$thisTitle));
			}
		}
		else
			$linkdiv->add(sprintf(_(" of bekijk foto's van %s."), new HtmlAnchor('?jaar='.coljaar(),_("eerdere activiteiten"))));

		$div->add(self::overzicht($acts));

		$page->end();
	}

	public static function overzicht($acts)
	{
		$div = new HtmlDiv();

		foreach($acts as $a)
		{
			$div->add(new HtmlHR());
			$div->add($anch = new HtmlHeader(4, null));
			$anch->add(new HtmlAnchor($a->fotoUrl(),ActiviteitView::titel($a)));
			$anch->add(new HtmlSmall($a->getMomentBegin()->strftime('%A %e %B %Y') . ', ' . CommissieVerzamelingView::kommaLijst($a->getCommissies())));

			if(Persoon::getIngelogd())
			{
				$fotos = $a->getRangedFotos();

				$div->add($row = new HtmlDiv(null, 'row row-centered'));

				foreach($fotos as $f)
				{
					$row->add($d = new HtmlDiv($f->maakThumbnail(), 'col-xs-2 col-centered'));
					$d->setCssStyle('text-align: center;');
				}
			}
		}

		$div->setAttribute('style','text-align: center');

		return $div;
	}

	static public function maakActiviteitSelect($activiteitids) {
		$acts = array();
		foreach(array_filter($activiteitids) as $a) {
			if(array_key_exists($a[0],$acts))
				continue;
			$acts[$a[0]] = $a[1];
		}

		$selected = tryPar('activiteiten',array());

		$box = HtmlSelectbox::fromArray('activiteiten',$acts,$selected,5);
		$box->setMultiple();
		return $box;
	}

	/**
	 *  Geef een formulieronderdeel om activiteiten te filteren.
	 *
	 * Wordt gebruikt in de activiteitenagenda en archief.
	 * @see aankomend
	 * @see archief
	 *
	 * @returns Een formulieronderdeel om in het form bovenaan de agenda te zetten.
	 */
	private static function filterForm($form) {
		$form->add(new HtmlLabel('catFilter', '&nbsp;' . _('Filter activiteiten:')));
		$catFilter = ActiviteitCategorieView::selectieForm(tryPar('ActiviteitCategorie'), "catFilter", true);
		$catFilter->setId('catFilter')
			->setAutoSubmit(true)
			->select(tryPar('catFilter', ''));
		$form->addImmutable($catFilter);

		if(hasAuth('actief')) {
			$form->add($select = new HtmlSelectbox('filter'));

			$select->addOption('alle', _('Alles'));
			$select->addOption('PUBLIEK', _('Openbaar'));
			$select->addOption('PRIVE', _('Intern'));
			$select->setAutoSubmit(true);
			$select->select(tryPar('filter', 'alle'));
		}
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
