<?

/**
 * $Id$
 */
abstract class DiliView
	extends View
{
	static public function makeLink (Persoon $pers, $caption = null)
	{
		if (is_null($caption))
			$caption = PersoonView::naam($pers);

		$a = new HtmlAnchor( '/Leden/Intro/DiLI/Formulier?lidid=' . $pers->getContactID(), $caption, null, 'persoonLink');
		$profielfoto = ProfielFoto::zoekHuidig($pers);
		$a->setAttribute('rel', ProfielFoto::fotoURL($profielfoto, 'Thumbnail'));
		return $a;
	}

	static public function loginPage()
	{
		$page = Page::getInstance()->start();

		$page->add(new HtmlParagraph(
			_('Op deze pagina kan je inloggen met de magische activatiecode die je gekregen hebt. '
			. 'Als je ingelogd bent moet je meteen je eigen gegevens controleren.')
		, 'bs-callout bs-callout-info'));

		$page->add($form = HtmlForm::named('diliinlog'));

		$form->add(View::makeFormStringRow('lidnr', null, _('Lidnummer')));
		$form->add(View::makeFormStringRow('code', null, _('Activatiecode')));
		$form->add(HtmlInput::makeFormSubmitButton(_('Login')));

		$page->end();
	}

	static public function betalingOverzicht(LidmaatschapsBetalingVerzameling $betalingen)
	{
		$page = Page::getInstance()->start();

		$page->add(new HtmlParagraph(sprintf(_('Er zijn op dit moment %s betalingen gevonden.'), $betalingen->aantal()), 'alert alert-info'));

		$page->add($table = new HtmlTable());

		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();

		$row->makeHeaderFromArray([ 'ID', _('Lid'), _('Rekeningnummer'), _('Rekeninghouder'), _('iDealtransactie') ]);

		foreach($betalingen as $betaling) {
			$row = $tbody->addRow();

			$row->addData($betaling->geefID());
			$row->addData(PersoonView::makeLink($betaling->getLid()));
			$row->addData(LidmaatschapsBetalingView::waardeRekeningnummer($betaling));
			$row->addData(LidmaatschapsBetalingView::waardeRekeninghouder($betaling));
			$row->addData(($betaling->getIdeal()) ? $betaling->getIdeal()->geefID() : null);
		}

		$page->end();
	}
}
