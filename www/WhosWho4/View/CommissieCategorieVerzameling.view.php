<?

/**
 * $Id$
 */
abstract class CommissieCategorieVerzamelingView
	extends CommissieCategorieVerzamelingView_Generated
{
	public static function categorieen(CommissieCategorieVerzameling $verz)
	{
		$page = Page::getInstance()->start(_('Commissiecategorieën'));

		$page->add($btn_group = new HtmlDiv(null, 'btn-group'));
		$btn_group->add(HtmlAnchor::button('/Vereniging/Commissies/Categorie/Nieuw', _('Nieuwe categorie')));

		if(tryPar('oud', false))
			$btn_group->add(HtmlAnchor::button('?oud=', _('Bekijk alleen met huidige cies')));
		else
			$btn_group->add(HtmlAnchor::button('?oud=true', _('Bekijk ook met oude cies')));

		$page->add($table = new HtmlTable());
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(['Categorie', 'Commissies']);

		foreach($verz as $cieCat)
		{
			$row = $tbody->addRow();
			$row->addData(CommissieCategorieView::makeLink($cieCat));
			$row->addData(CommissieCategorieView::waardeCommissies($cieCat));
		}

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
