<?
abstract class ActiviteitInformatieView
	extends ActiviteitInformatieView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 *  Geef een icon terug als er een reservering is, mogelijk als link naar de reserveringen.
	 *
	 * @param act De activiteitinformatie waarvoor we het icoontje hebben
	 * @param link Een bool of we een link willen of niet
	 *
	 * @return NULL als er geen reserveringen zijn, anders het icoontje
	 */
	static public function computerReserveringIcon(Activiteit $act, $link = false)
	{
		if($act instanceof ActiviteitHerhaling)
			$act = $act->getInformatie();
		if($act->getAantalComputers() > 0)
		{
			$text = sprintf(_("Er zijn %d computers gereserveerd"), ActiviteitInformatieView::waardeAantalComputers($act));
			$result = HtmlSpan::fa('laptop', $text);
			if($link)
				$result = new HtmlAnchor("/Activiteiten/Computerreserveringen", $result);
			return $result;
		} else {
			return null;
		}
	}

	public static function getIcsWerftekst(Activiteit $act)
	{
		$info = $act->getInformatie();

		$description = self::waardeWerftekst($info);

		$link = new HtmlAnchor(HTTPS_ROOT . $act->url(), 'bekijk activiteit');
		$description .= " (" . $link->makeHtml() . ")";

		// return str_replace("\n", "", $description);
		return $description;
	}

	/**
	 *  Maakt de maindiv met informatie van een activiteitinformatie aan.
	 *
	 * @param act De activiteitinformatie
	 *
	 * @return De maindiv
	 */
	static public function maakMainDiv($act)
	{
		$div = new HtmlDiv();

		if($act->hasHerhalingsChildren())
		{
			$div->add($callout = new HtmlDiv(null, 'bs-callout bs-callout-info'));
			$callout->add(sprintf(_('Deze activiteit wordt herhaald. Bekijk de '
				. 'herhalingen %s.'), new HtmlAnchor($act->url() . '/Herhalingen', _('hier'))));
		}

		$div->add(self::maakInfoDiv($act));

		return $div;
	}

	/**
	 *  Maakt een nieuwe pagina voor het wijzigen of toevoegen van een activiteit
	 *
	 * @param Activiteit $act De ActiviteitInformatie die nieuw is of gewijzigd wordt
	 * @param CommissieActiviteitVerzameling $cieActs De CommissieActiviteitVerzameling die bij act hoort
	 * @param ActiviteitVraagVerzameling $vragen De ActiviteitVraagVerzameling die bij act hoort
	 * @param bool $nieuw Een bool die aangeeft of het een nieuwe act is of niet
	 * @param bool $show_error Een bool die aangeeft of errors moeten worden laten zien
	 * @return Page;
	 */
	static public function wijzigen(
		Activiteit $act, CommissieActiviteitVerzameling $cieActs, ActiviteitVraagVerzameling $vragen,
		$nieuw = false, $show_error = false)
	{
		$page = (new HTMLPage())
			 ->addFooterJS(Page::minifiedFile('knockout.js'))
			 ->addFooterJS(Page::minifiedFile('moment.js'))
			 ->addFooterJS(Page::minifiedFile('Activiteit.js'))
			 ->addFooter(HtmlScript::makeJavascript('var zichtbaarJSON = "'.$act->getToegang().'";'))
			 ->addFooter(HtmlScript::makeJavascript('var momentBeginJSON = "'.$act->getMomentBegin().'";'))
			 ->addFooter(HtmlScript::makeJavascript('var momentEindJSON = "'.$act->getMomentEind().'";'));

		if($nieuw)
			$page->start(_("Nieuwe activiteit"), 'activiteiten');
		else
			$page->start(sprintf(_("%s wijzigen"), self::waardeTitel($act)));

		$form = ActiviteitInformatieView::wijzigForm($act, $cieActs, $vragen, $nieuw, $show_error);
		$page->add($form);

		return $page;
	}

	/**
	 *  Geeft een formulier voor het wijzigen of toevoegen van een ActiviteitInformatie terug
	 *
	 * @param obj De ActiviteitInformatie om te wijzigen of toe te voegen
	 * @param cieActs De CommissieActiviteitVerzameling die bij act hoort
	 * @param vragen De ActiviteitVraagVerzameling die bij act hoort
	 * @param nieuw Een bool die aangeeft of het een nieuwe act is of niet
	 * @param show_error Een bool die aangeeft of errors moeten worden laten zien
	 *
	 * @return Het formulier om te wijzigen of toe te voegen
	 */
	static public function wijzigForm ($obj, $cieActs, $vragen, $nieuw = false, $show_error = false)
	{
		/** Bepaal welk formulier getoond moet worden **/
		$name = $nieuw ? 'ActNieuw' : 'ActWijzig';
		/** Dit wordt het formulier **/
		$form = HtmlForm::named($name);

		/**
		 * We gaan 3 divs maken:
		 * - statische informatie
		 * - datum
		 * - inschrijvingen
		 */

		/** Eerst de div voor statische informatie in te vullen **/
		$form->add($div = new HtmlDiv());

		if($obj->hasHerhalingsChildren())
		{
			$div->add($callout = new HtmlDiv(null, 'bs-callout bs-callout-warning'));
			$callout->add(_('Deze activiteit heeft herhalingen. Je past nu dus '
				. 'de informatie van alle herhalingen ook aan.'));
		}
		else if($obj instanceof ActivteitInformatie)
		{
			$div->add($callout = new HtmlDiv(null, 'bs-callout bs-callout-warning'));
			$callout->add(_('Deze activiteit is een herhaling. Je past nu dus '
				. 'alleen de herhaling aan.'));
		}

		$div->add(new HtmlHeader(3, _("Informatie")));

		$div->add(self::wijzigTR($obj, 'Commissies', $show_error))
			->add(self::wijzigTR($obj, 'Titel', $show_error))
			->add(self::wijzigTR($obj, 'Werftekst', $show_error))
			->add(self::wijzigTR($obj, 'Locatie', $show_error))
			->add(self::wijzigTR($obj, 'Prijs', $show_error))
			->add(self::wijzigTR($obj, 'Homepage', $show_error))
			->add($t = self::wijzigTR($obj, 'Toegang', $show_error))
			->add(self::wijzigTR($obj, 'Actsoort', $show_error))
			->add(self::wijzigTR($obj, 'Categorie', $show_error))
			->add(self::wijzigTR($obj, 'Onderwijs', $show_error))
			->add(self::wijzigTR($obj, 'AantalComputers', $show_error));

		$t->getChild(1)->getChild(0)->setAttribute('data-bind', 'value: zichtbaar');

		/** Dan de div voor data in te vullen **/
		$form->add($div = new HtmlDiv());
		$div->add(new HtmlHeader(3, _("Data")));

		if($nieuw)
		{
			$div->add(self::makeFormBoolRow('ActiviteitInformatie[Herhalend][check]'
				, tryPar('ActiviteitInformatie[Herhalend][check]')
				, _('Herhalende activiteit?')));
		}

		$div->add($tblNee = new HtmlDiv(null, null, 'toevoegen_Nee'))
			->add($tblWel = new HtmlDiv(null, null, 'toevoegen_Wel'));

		if($nieuw)
			if (tryPar('ActiviteitInformatie[Herhalend][check]'))
				$tblNee->setAttribute('style', 'display: none');
			else
				$tblWel->setAttribute('style', 'display: none');

		$tblNee->add($mb = self::wijzigTR($obj, 'MomentBegin', $show_error))
			->add($me = self::wijzigTR($obj, 'MomentEind', $show_error));

		$mb->getChild(1)->getChild(0)->setAttribute('data-bind', 'value: momentBegin');
		$me->getChild(1)->getChild(0)->setAttribute('data-bind', 'value: momentEind');

		if($nieuw)
		{

			$tblWel->add(self::wijzigTR($obj, 'HerhalendVanaf', $show_error))
				->add(self::wijzigTR($obj, 'HerhalendTot', $show_error))
				->add(self::wijzigTR($obj, 'HerhalendBegin', $show_error))
				->add(self::wijzigTR($obj, 'HerhalendEind', $show_error))
				->add(self::wijzigTR($obj, 'HerhalendNumweek', $show_error))
				->add(self::wijzigTR($obj, 'HerhalendWeek', $show_error));
		}

		/** Als laatste de div om inschrijvingen te regelen **/
		$form->add($div = new HtmlDiv());
		$div->setAttribute('data-bind', 'visible: zichtbaar() == "PUBLIEK"');
		$div->add(new HtmlHeader(3, _("Inschrijvingen")))
			->add(self::makeFormBoolRow('ActiviteitInformatie[inschrijfbaar][check]'
			, tryPar('ActiviteitInformatie[inschrijfbaar][check]', $obj->getInschrijfbaar())
			, _('Openbaar inschrijfbaar via de site?')));

		$div->add($tDiv = new HtmlDiv(null, null, 'Activiteit_Wijzig_Inschrijfbaar'));

		if (!tryPar('ActiviteitInformatie[inschrijfbaar][check]', $obj->getInschrijfbaar()))
			$tDiv->setAttribute('style', 'display: none');

		$tDiv->add(self::wijzigTR($obj, 'StuurMail', $show_error))
			 ->add(self::wijzigTR($obj, 'DatumInschrijvenMax', $show_error))
			 ->add(self::wijzigTR($obj, 'DatumUitschrijven', $show_error))
			 ->add(self::wijzigTR($obj, 'MaxDeelnemers', $show_error));

		$tDiv->add(ActiviteitVraagVerzamelingView::wijzigForm($vragen, $obj, $show_error));

		$div->add($vragenDiv = new HtmlDiv(null, null, 'Activiteit_Vragen'));

		$vragenDiv->add(new HtmlHeader(4, _('Vragen toevoegen voor iDeal')));
		$vragenDiv->add(ActiviteitVraagVerzamelingView::wijzigForm($vragen, $obj, $show_error));

		/** Voeg ook een submit-knop toe **/
		$form->add(HtmlInput::makeFormSubmitButton($nieuw ? _("Maak aan") : _("Wijzig de activiteit!")));

		return $form;
	}

	/**
	 *  Verwerk het wijzig formulier van een ActiviteitInformatie
	 *
	 * @param act De Activiteit waarvan de data moet worden ingevuld
	 * @param cieActs Een CommissieActiviteitVerzameling die bij act hoort
	 * @param vragen Een ActiviteitVraagVerzameling die bij act hoort
	 * @param herhalingen Een ActiviteitVerzameling die bij act hoort voor de herhalingen
	 *
	 * @return False, als er geen data is om te verwerken
	 */
	static public function processWijzigForm (Activiteit $act
		, CommissieActiviteitVerzameling $cieActs
		, ActiviteitVraagVerzameling $vragen
		, ActiviteitVerzameling $herhalingen)
	{
		// TODO: haal $vragen weg want dit wordt niet gebruikt. Gebruik
		// processWijzigFormVragen om het vragen-deel van deze activiteit te
		// verwerken

		global $auth;

		/** Haal alle data op **/
		if (!$data = tryPar(self::formobj($act), NULL))
			return false;

		/** Vul alle statische informatie in **/
		self::processForm($act);

		/** Verwerk de geselecteerde commissies **/
		if (isset($data['commissies']))
		{
			$commissies = CommissieVerzameling::verzamel($data['commissies']);
			// laat de gebruiker alleen cies toevoegen waar die zelf in zit (tenzij promocie, die mogen alles aanpassen)
			foreach ($commissies as $commissie) {
				if ($auth->hasCieAuth($commissie->getCommissieID()) || hasAuth('promocie')) {
					$cieActs->voegtoe(new CommissieActiviteit($commissie, $act));
				}
			}
		}

		/** Verwerk een herhalende activiteit **/
		if (@$data['Herhalend']['check'])
		{
			$act->setMomentBegin(null);
			$act->setMomentEind(null);

			if ($data['Herhalend']['numweek'] == 0)
				return;

			$begin = new DateTimeLocale($data['Herhalend']['vanaf']);
			$einde = new DateTimeLocale($data['Herhalend']['tot']);

			$first = true;
			for ($dag = clone($begin); $dag <= $einde; $dag->add(new DateInterval('P1D')))
			{
				$week_offset = ($dag->format('W') - $begin->format('W')) % 52;
				if ($week_offset % (int)$data['Herhalend']['numweek'] != 0)
					continue;

				$d = $dag->format('N');
				if (($d == 1 && !$data['HerhalendOp']['ma']) ||
					($d == 2 && !$data['HerhalendOp']['di']) ||
					($d == 3 && !$data['HerhalendOp']['wo']) ||
					($d == 4 && !$data['HerhalendOp']['do']) ||
					($d == 5 && !$data['HerhalendOp']['vr']) ||
					($d == 6 && !$data['HerhalendOp']['za']) ||
					($d == 7 && !$data['HerhalendOp']['zo']))
					continue;

				if ($first)
				{
					$newAct = $act;
				}
				else
				{
					$newAct = new ActiviteitHerhaling($act);
					$newAct->setToegang($act->getToegang());
				}
				$momentBegin = clone $dag;
				$tijdBegin = new DateTimeLocale($data['Herhalend']['begin']);
				$momentBegin->add(new DateInterval($tijdBegin->format('\P\TH\Hi\Ms\S')));
				$momentEinde = clone $dag;
				$tijdEind = new DateTimeLocale($data['Herhalend']['eind']);
				$momentEinde->add(new DateInterval($tijdEind->format('\P\TH\Hi\Ms\S')));

				$newAct->setMomentBegin($momentBegin)
					   ->setMomentEind($momentEinde);

				if($first)
					$first = false;
				else
				{
					$herhalingen->voegtoe($newAct);
					if (isset($data['commissies']))
					{
						$commissies = CommissieVerzameling::verzamel($data['commissies']);
						// laat de gebruiker alleen cies toevoegen waar die zelf in zit
						foreach ($commissies as $commissie) {
							if ($auth->hasCieAuth($commissie->getCommissieID())) {
								$cieActs->voegtoe(new CommissieActiviteit($commissie, $newAct));
							}
						}
					}
				}
			}
		}

		/** Verwerk inschrijvingen **/
		if (@$data['inschrijfbaar']['check'])
			$act->setInschrijfbaar(true);
		else
			$act->setInschrijfbaar(false);
	}

	static public function processWijzigFormVragen (Activiteit $act
		, ActiviteitVraagVerzameling $vragen)
	{
		/** Verwerk vragen **/
		$vraagdata = tryPar(self::formobj(new ActiviteitVraag($act)), NULL);
		ActiviteitVraagVerzamelingView::processWijzigForm($vragen, $act, $vraagdata);
	}

	/** De waarde-functies die bij de actinfo horen **/
	static public function waardeKorteTitel (ActiviteitInformatie $act)
	{
		return static::defaultWaardeString($act, 'Titel');
	}

	static public function waardeTitel (ActiviteitInformatie $act)
	{
		return static::defaultWaardeString($act, 'Titel');
	}

	public static function waardeHomepage(ActiviteitInformatie $obj)
	{
		$caption = $url = externeSiteLink($obj->getHomepage());
		if($url[0] == '/')
			$caption = 'www.A-Eskwadraat.nl' . $caption;
		return new HtmlAnchor($url, $caption);
	}

	/** De labels die bij de actinfo horen **/
	public static function labelTitel(ActiviteitInformatie $obj)
	{
		return _('Titel van de activiteit');
	}
	public static function labelWerftekst(ActiviteitInformatie $obj)
	{
		return _('Omschrijving');
	}
	public static function labelPrijs(ActiviteitInformatie $obj)
	{
		return _('Toegangsprijs');
	}
	public static function labelHomepage(ActiviteitInformatie $obj)
	{
		return _('Aanvullende website');
	}
	public static function labelActsoort(ActiviteitInformatie $obj)
	{
		return _('Organisator');
	}
	public static function labelPoster(ActiviteitInformatie $obj)
	{
		return $obj->posterPath() ? new HtmlAnchor($obj->url() . "/ActPoster", 'Poster') : _('Poster');
	}
	public static function labelenumActsoort($value)
	{
		switch ($value)
		{
			case 'AES2': return aesnaam();
			case 'UU': return _('Universiteit Utrecht');
			case 'LAND': return _('Landelijke activiteit');
			case 'ZUS': return _('Zustervereniging');
			case '.COM': return _('Bedrijf');
			case 'EXT': return _('Extern');
		}
		return parent::labelenumActsoort($value);
	}
	public static function labelOnderwijs(ActiviteitInformatie $obj)
	{
		return _('Agenda');
	}
	public static function labelenumOnderwijs($value)
	{
		switch ($value)
		{
			case 'J': return _('Onderwijs');
			case 'N': return _('Normaal');
			case 'B': return _('Beide');
		}
		return parent::labelenumOnderwijs($value);
	}
	public static function labelAantalComputers(ActiviteitInformatie $obj)
	{
		return _('Aantal computers reserveren');
	}
	static public function labelStuurMail(ActiviteitInformatie$obj)
	{
		return _('Stuur een mailtje als leden zich inschrijven');
	}
	public static function labelDatumInschrijvenMax(ActiviteitInformatie $obj)
	{
		return _('Uiterste inschrijf-datum');
	}
	public static function labelDatumUitschrijven(ActiviteitInformatie $obj)
	{
		return _('Uiterste uitschrijf-datum');
	}
	public static function labelMaxDeelnemers(ActiviteitInformatie $obj)
	{
		return _('Maximum aantal deelnemers');
	}
	static public function labelVraag(ActiviteitInformatie $obj)
	{
		return _("Work in progress");
	}
	static public function labelHerhalendVanaf(ActiviteitInformatie $obj)
	{
		return _("Herhalen vanaf");
	}
	static public function labelHerhalendTot(ActiviteitInformatie $obj)
	{
		return _("Herhalen tot");
	}
	static public function labelHerhalendBegin(ActiviteitInformatie $obj)
	{
		return _("De begintijd");
	}
	static public function labelHerhalendEind(ActiviteitInformatie $obj)
	{
		return _("De eindtijd");
	}
	static public function labelHerhalendNumweek(ActiviteitInformatie $obj)
	{
		return _("Herhalen om de hoeveel weken");
	}
	static public function labelHerhalendWeek($obj)
	{
		return _("Herhalen op");
	}

	/** Opmerkingen **/
	public static function opmerkingOnderwijs()
	{
		return _("Op welke agenda moet deze activiteit komen?");
	}
	static public function opmerkingVraag ()
	{
		return _("Work in progress");
	}
	static public function opmerkingMaxDeelnemers ()
	{
		return _("0 betekent zonder limiet");
	}
	static public function opmerkingAantalComputers ()
	{	
		$link = new HtmlSpan(array($anch = new HtmlAnchor("/Activiteiten/Computerreserveringen", _("Bekijk hier computerreserveringen"))
				, new HtmlBreak()
				, new HtmlSpan(_('Let op, computerreserveringen zijn pas vanaf 17:00 geldig'))));
		$anch->setAttribute('target', '_blank');
		return $link;
	}
	static public function opmerkingHerhalendVanaf ()
	{
		return '';
	}
	static public function opmerkingHerhalendTot ()
	{
		return '';
	}
	static public function opmerkingHerhalendBegin ()
	{
		return '';
	}
	static public function opmerkingHerhalendEind ()
	{
		return '';
	}
	static public function opmerkingHerhalendNumweek ()
	{
		return _("'1' is iedere, '2' is om-de-week, etc");
	}
	static public function opmerkingHerhalendWeek ()
	{
		return _("Selecteer minstens 1 dag");
	}
	static public function opmerkingCommissies()
	{
		return NULL;
	}

	/** Form-functies **/
	public static function formAantalComputers(ActiviteitInformatie $obj, $include_id = false)
	{
		// maak de gebruiker het wat makkelijker door een minimum in te stellen
		// het maximum is wat lastiger te verzinnen, dus laten we nog even vrij
		return static::defaultFormInt($obj, 'AantalComputers', 0, null, $include_id);
	}
	static public function formVraag ()
	{
			return _("Work in progress");
	}
	static public function formHerhalendVanaf ()
	{
		return HtmlInput::makeDate('ActiviteitInformatie[Herhalend][vanaf]', tryPar('ActiviteitInformatie[Herhalend][vanaf]'));
	}
	static public function formHerhalendTot ()
	{
		return HtmlInput::makeDate('ActiviteitInformatie[Herhalend][tot]', tryPar('ActiviteitInformatie[Herhalend][tot]'));
	}
	static public function formHerhalendBegin ()
	{
		return HtmlInput::makeTime('ActiviteitInformatie[Herhalend][begin]', tryPar('ActiviteitInformatie[Herhalend][begin]'));
	}
	static public function formHerhalendEind ()
	{
		return HtmlInput::makeTime('ActiviteitInformatie[Herhalend][eind]', tryPar('ActiviteitInformatie[Herhalend][eind]'));
	}
	static public function formHerhalendNumweek ()
	{
		return HtmlInput::makeNumber('ActiviteitInformatie[Herhalend][numweek]', tryPar('ActiviteitInformatie[Herhalend][numweek]', 1), 1, 8);
	}
	static public function formHerhalendWeek ()
	{
		return new HtmlList(false, array(
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][ma]', tryPar('ActiviteitInformatie[HerhalendOp][ma]')),
				new HtmlSpan(_("maandag")))),
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][di]', tryPar('ActiviteitInformatie[HerhalendOp][di]')),
				new HtmlSpan(_("dinsdag")))),
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][wo]', tryPar('ActiviteitInformatie[HerhalendOp][wo]')),
				new HtmlSpan(_("woensdag")))),
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][do]', tryPar('ActiviteitInformatie[HerhalendOp][do]')),
				new HtmlSpan(_("donderdag")))),
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][vr]', tryPar('ActiviteitInformatie[HerhalendOp][vr]')),
				new HtmlSpan(_("vrijdag")))),
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][za]', tryPar('ActiviteitInformatie[HerhalendOp][za]')),
				new HtmlSpan(_("zaterdag")))),
			new HtmlSpan(array(
				HtmlInput::makeCheckbox('ActiviteitInformatie[HerhalendOp][zo]', tryPar('ActiviteitInformatie[HerhalendOp][zo]')),
				new HtmlSpan(_("zondag"))))
		));
	}

	static public function formCommissies(ActiviteitInformatie $obj)
	{
		return self::defaultFormCommissieVerzameling($obj, 'commissies', $obj->getCommissies(), true);
	}

	static public function formCategorie(ActiviteitInformatie $obj, $include_id = false)
	{
		return ActiviteitCategorieView::selectieForm($obj->getCategorie(), self::formObj($obj) . "[Categorie]");
	}
}
