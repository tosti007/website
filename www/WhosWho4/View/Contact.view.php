<?

/**
 * $Id$
 */
abstract class ContactView
	extends ContactView_Generated
{
	static public function naam (Contact $contact = null, $type = WSW_NAME_DEFAULT)
	{
		if ($contact instanceof Persoon)
			return PersoonView::naam($contact, $type);
		elseif ($contact instanceof Organisatie)
			return OrganisatieView::waardeNaam($contact);
		else
			return _('onbekend contact');
	}

	public static function labelEmail(Contact $obj)
	{
		return _("Email");
	}
	public static function labelHomepage(Contact $obj)
	{
		return _("Homepage");
	}

	public static function makeEmailLink (Contact $con)
	{
		if ($con->getEmail() == null)
			return new HtmlSpan(_('Dit contact heeft geen emailadres.'));
		return new HtmlAnchor('mailto:' . $con->getEmail(), self::waardeEmail($con));
	}

	public static function makeHomepageLink (Contact $con)
	{
		if ($con->getHomepage() == null)
			return new HtmlSpan(_('Dit contact heeft geen homepage.'));
		return new HtmlAnchor($con->getHomepage(), self::waardeHomepage($con));
	}

	public static function bounceDiv($contact)
	{

		$bounces = MailingBounceVerzameling::vanContact($contact);

		$div = new HtmlDiv();
		$div->add( $contact->getContactID() . _(' heeft de volgende bounces;'))
			->add( new HtmlBreak(1));

		if($bounces->aantal() == 0) {
				$div->add(_('Helemaal geen!'));
		} else {
			$list = new HtmlList();
			foreach($bounces as $bounce){
				if(hasAuth('bestuur')) {
					$span = new HtmlSpan(new HtmlAnchor($contact->url() . '/Bounces?verwijder=' . $bounce->geefID(),
						HtmlSpan::fa('times', _('Verwijderen'), 'text-danger')));
				} else {
					$span = null;
				}
				$list->add(new HtmlListItem(array($span, 
					new HtmlAnchor('#' .$bounce->getMailingBounceID(),MailingBounceView::lineDescription($bounce))
				)));
			}
			$div->add($list);

			foreach($bounces as $bounce){
				$header = new HtmlAnchor();
				$header->setName($bounce->getMailingBounceID());
				$div->add(new HtmlHeader(4, $header . MailingBounceView::lineDescription($bounce)));
				$div->add(new HtmlPre(htmlspecialchars($bounce->getBounceContent())));
			}
		}

		return $div;
	}

	public static function labelvakidOpsturen(Contact $obj)
	{
		return _('Vakidioot (per post) opsturen');
	}

	public static function labelaes(Contact $obj)
	{
		return _('A-Es2Roots opsturen');
	}

	public static function waardeHomepage(Contact $obj)
	{
		$caption = $url = externeSiteLink($obj->getHomepage());
		if($url[0] == '/')
			$caption = 'www.A-Eskwadraat.nl' . $caption;
		return new HtmlAnchor($url, $caption);
	}

	static public function defaultWaardeContact(Contact $obj)
	{
		if($obj instanceof Persoon) {
			return PersoonView::makeLink($obj);
		}
		return self::naam($obj);
	}

	/**
	 * Verwerk de formulieronderdelen die te maken hebben met het adres van dit Contact.
	 * Handiger dan de code van ContactController::processWijzigAdres overnemen.
	 *
	 * @param obj Het Contactobject waar het adressenformulier bij hoort.
	 * @returns False als er een fout is gebeurd, anders true
	 */
	public static function processWijzigAdres(Contact $obj) {
		$adressen = $obj->getAdressen();

		ContactAdresVerzamelingView::processWijzigAdres($adressen);

		// Gooi de adressen weg als dat moet
		$adresData = tryPar('ContactAdres');
		foreach ($adressen as $i => $adres) {
			if ((bool)$adresData[$adres->geefID()]['verwijder']) {
				$adressen->verwijder($i);
				if ($error = $adres->verwijderen()) {
					return false;
				}
			}
		}

		// Voor het geval dat er een nieuw adres in het formulier staat
		$nieuw = new ContactAdres($obj);
		// Als je een veld niet hebt ingevuld, geeft processForm fouten
		// dus we gaan ervan uit dat je hem niet wil toevoegen
		$nieuwToevoegen = ContactAdresView::processForm($nieuw);
		if ($nieuwToevoegen) {
			$adressen->voegtoe($nieuw);
		}

		// Het is gelukt!
		if ($adressen->allValid()) {
			$adressen->opslaan();
			return true;
		} else {
			return false;
		}
	}

	static public function details(Contact $contact, $noPage = false)
	{
		global $session;

		$body = new HtmlDiv(null, null, 'profiel');

		//Als we een willekeurig lid hebben laat dan nieuwe knop zien
		if(tryPar('willekeurig')) {
			$body->add($form = new HtmlForm('POST', '/Leden/Willekeurig'));
			$form->add(HtmlInput::makeHidden('willekeurigCounter', $session->get('willekeurigCounter')));
			$form->add(HtmlInput::makeSubmitButton(_('Ander willekeurig lid')));
		}

		if($contact instanceof Persoon)
		{
			$body->add(PersoonView::mededelingenDiv($contact));
		}

		$body->add(new HtmlDiv($col = new HtmlDiv(null, 'col-lg-12 col-sm-12'), 'row'));

		// Maak de cards aan
		$col->add($hc = new HtmlDiv(null, 'card hovercard'));
		$hc->add($cbc = new HtmlDiv(null, 'card-background'));

		// Zoek hier de profielfoto als die bestaat...
		$profielfoto = ProfielFoto::zoekHuidig($contact);
		$magZien = $contact->magFotoWordenBekeken();
		if ($profielfoto && $magZien) {
			$foto = $profielfoto->getMedia();
			$url_medium = $foto->url() . '/Medium';
			$url_origineel = $foto->url() . '/Origineel';
		} else {
			$url_medium = Media::penguinFoto(!$magZien);
			$url_origineel = Media::penguinFoto(!$magZien);
		}

		// ..en voeg de foto toe as achtergrond...
		$cbc->add(new HtmlImage($url_medium, 'profielfoto', _('Profielfoto'), 'card-bkimg'));

		// En als avatar
		$hc->add(new HtmlDiv($img = new HtmlImage($url_medium, 'profielfoto', _('Profielfoto')), 'useravatar'));
		$img->setId('profielFoto');

		$body->add($modal = new HtmlDiv(null, 'profielfoto-modal', 'profielFotoModal'));
		$modal->add(new HtmlSpan('&times', 'close', 'close-modal'));
		$modal->add($img = new HtmlImage($url_origineel, 'profielFoto', _('Profielfoto'), 'profielfoto-modal-content'));
		$img->setId('modal-image');
		$modal->add(new HtmlDiv(self::naam($contact), null, 'caption'));

		// Maak de buttongroup voor de tabs aan
		$col->add($pref = new HtmlDiv(null, 'btn-pref btn-group btn-group-justified'));

		// Maak de well voor de tabs
		$col->add($well = new HtmlDiv(null, 'well'));
		$well->add($tab_content = new HtmlDiv(null, 'tab-content'));

		// Voeg de persoonsinformatie toe
		$pref->add($btn = new HtmlAnchor('#tab1'
			, new HtmlDiv(
				array(
					HtmlSpan::fa('user-circle', _('Persoonsinformatie'))
					, new HtmlBreak()
					, _('Persoonsinformatie')
				)
			)
			, null
			, 'btn btn-primary'
			, 'info'));
		$btn->setAttribute('data-toggle', 'tab');

		$tab_content->add($info_div = new HtmlDiv(null, 'tab-pane fade in active', 'tab1'));

		$info_div->add(ContactView::detailsPersoonInfoDiv($contact));

		// Alleen leden kunnen studeren en dus de tab studie krijgen
		if($contact instanceof Lid)
		{
			$pref->add($btn = new HtmlAnchor('#tab2'
				, new HtmlDiv(
					array(
						HtmlSpan::fa('book', _('Studies & mentorgroepen'))
						, new HtmlBreak()
						, _('Studies & mentorgroepen')
					)
				)
				, null
				, 'btn btn-default'
				, 'info'));
			$btn->setAttribute('data-toggle', 'tab');

			$tab_content->add($studie_div = new HtmlDiv(null, 'tab-pane fade in', 'tab2'));

			$studie_div->add(LidView::detailsStudiesInfoDiv($contact));
		}

		// Alleen personen kunnen in cies zitten en dus de tab studie krijgen
		if($contact instanceof Persoon)
		{
			$pref->add($btn = new HtmlAnchor('#tab3'
				, new HtmlDiv(
					array(
						HtmlSpan::fa('group', _('Commissies, groepen en disputen'))
						, new HtmlBreak()
						, _('Commissies, groepen en disputen')
					)
				)
				, null
				, 'btn btn-default'
				, 'info'));
			$btn->setAttribute('data-toggle', 'tab');

			$tab_content->add($cie_div = new HtmlDiv(null, 'tab-pane fade in', 'tab3'));

			$cie_div->add(PersoonView::detailsCiesInfoDiv($contact));
		}

		// Alleen personen kunnen studeren en dus de tab studie krijgen
		if($contact instanceof Persoon)
		{
			$pref->add($btn = new HtmlAnchor('#tab4'
				, new HtmlDiv(
					array(
						HtmlSpan::fa('archive', _('Bezochte activiteiten'))
						, new HtmlBreak()
						, _('Bezochte activiteiten')
					)
				)
				, null
				, 'btn btn-default'
				, 'info'));
			$btn->setAttribute('data-toggle', 'tab');

			$tab_content->add($act_div = new HtmlDiv(null, 'tab-pane fade in', 'tab4'));

			$act_div->add(PersoonView::detailsActiviteitenInfoDiv($contact));
		}

		if($contact instanceof Lid && hasAuth('bestuur'))
		{
			$body->add($script = new HtmlScript());
			$script->setAttribute('src', Page::minifiedFile('persoonuid.js', 'js'));
		}

		if($noPage)
			return $body;

		$page = Page::getInstance()
			->addFooterJS(Page::minifiedFile('profielJS', 'js'))
			->start(ContactView::naam($contact))
			->add($body)
			->end();
	}

	public static function detailsPersoonInfoDiv($contact)
	{
		global $auth;

		$body = new HtmlDiv();

		$body->add($btn_group = new HtmlDiv(null, 'btn-group'));

		$media = MediaVerzameling::geefFotos('lid', $contact);

		if(count($media) != 0)
		{
			$btn_group->add(HtmlAnchor::button('/FotoWeb/Fotografen/' . $contact->geefID(), _('Bekijk gemaakte foto\'s')));
		}

		if($contact->heeftfotos())
		{
			$btn_group->add(HtmlAnchor::button('/Leden/' . $contact->geefID() . '/Fotos', _('Meer foto\'s')));
		}

		$kart = Kart::geef()->getPersonen();

		if($kart->bevat($contact))
		{
			$content = HtmlSpan::fa_stacked(array(
				HtmlSpan::fa('shopping-cart', _('Unshop'), 'fa-stack-1x text-muted'),
				HtmlSpan::fa('ban', _('Unshop'), 'fa-stack-2x text-danger')
			));
			$btn_group->add($a = new HtmlAnchor('#', $content, "unShopLidAjax(" . $contact->geefID() . ", this); return false"));
			$a->addClass('btn btn-primary');
		}
		else
		{
			$content = HtmlSpan::fa('shopping-cart', _('Shop'), 'text-muted');
			$btn_group->add($a = new HtmlAnchor('#', $content, "shopLidAjax(" . $contact->geefID() . ", this); return false"));
			$a->addClass('btn btn-primary');
		}

		$body->add($btn_group = new HtmlDiv(null, 'btn-group'));

		if($contact->magWijzigen())
		{
			$btn_group->add(HtmlAnchor::button('/Leden/' . $contact->geefID() . '/Foto/Wijzig', _('Wijzig de pasfoto')));
		}

		if(count(MediaVerzameling::getTopratedByPersoon($contact)) > 0 && hasAuth('bestuur'))
		{
			$btn_group->add(HtmlAnchor::button('/Leden/' . $contact->geefID() . '/PasfotoVerwijderen', _('Pasfoto verwijderen'), null, null,null, 'danger'));
		}

		$body->add($h = new HtmlHeader(3, _('Persoonsinformatie')));

		if($contact->magWijzigen())
		{
			$h->add(new HtmlSmall(new HtmlAnchor($contact->url() . '/Wijzig', _('Wijzig'))));
		}

		// Maak panel waar standard info in komt te staan
		$body->add(new HtmlDiv(new HtmlDiv($tbl = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

		// De naam
		$tbl->add(self::infoTRData(_('Naam'), ContactView::naam($contact, WSW_NAME_LANG)));

		// Alleen personen of hoger kunnen een geboortedatum hebben,
		// overleden zijn of voornaamwoorden hebben
		if($contact instanceof Persoon)
		{
			$tbl->add(self::infoTRData(_('Geboortedatum'), PersoonView::waardeDatumGeboorte($contact)));
			// Check of het voornaamwoord wel mag laten zien
			if ($contact->getVoornaamwoordZichtbaar() && $contact != Persoon::getIngelogd()){
				$tbl->add(self::infoTRData(_('Voornaamwoord'), PersoonView::waardeVoornaamwoord($contact)));
			}

			if(hasAuth('bestuur'))
			{
				$tbl->add(self::infoTRData(_('Overleden')
					, new HtmlAnchor($contact->url() . '/Overleden', PersoonView::waardeOverleden($contact))));
			}
		}

		// Email
		$tbl->add(self::infoTRData(_('Email'), self::makeEmailLink($contact)));

		// Mailingbounces
		if (MailingBounceVerzameling::vanContact($contact)->aantal() > 0 && $contact->magWijzigen() && $contact instanceof Persoon)
		{
			$tbl->add(self::infoTRData(sprintf(_('Persoon heeft %s!'), new HtmlAnchor($contact->url() . '/Bounces', 'bounces'))
				, self::makeEmailLink($contact)));
		}

		// Homepage
		if (strlen($contact->getHomepage()) > 0)
		{
			$tbl->add(self::infoTRData(_('Site'), self::waardeHomepage($contact)));
		}

		// Geef het rekeningnummer een class zodat het met js met een dubbelklik
		// in 1 keer geselecteerd kan worden
		if($contact->magWijzigen() && strlen($contact->getRekeningNummer()) > 0 && $contact instanceof Persoon)
		{
			$tbl->add(self::infoTRData(_('Rekeningnummer')
				, $reknr = new HtmlSpan(PersoonView::waardeRekeningNummer($contact))));
			$reknr->addClass('rekeningnummer');
		}

		// Alle telefoonnummers
		foreach($contact->getTelefoonNummers() as $telnr)
		{
			$tbl->add(self::infoTRData(ContactTelnrView::waardeSoort($telnr)
				, ContactTelnrView::waardeTelefoonnummer($telnr)));
		}

		// Als er badges zijn om weer te gegeven, geef ze weer
		if(($persBadges = $contact->getBadges()) && $contact instanceof Persoon)
		{
			$achievementsSpan = _('Achievements');

			if ($contact->magBekijken())
			{
				$achievementsSpan = new HtmlAnchor($contact->urlAchievements(), $achievementsSpan);
			}

			$tbl->add(self::infoTRData($achievementsSpan,
				PersoonView::makeAchievementsLink($contact, $persBadges->aantalBadges())
			));
		}

		// Vakidioot en roots
		if($contact->magWijzigen())
		{
			$tbl->add(self::infoTRData(_('Vakidioot: ')
					, $contact->getVakIdOpsturen() ? _('Ja') : _('Nee')
				))
				->add(self::infoTRData(new HtmlAnchor('/Leden/Alumni/', 'A-Es2roots')
					, $contact->getAes2rootsOpsturen() ? sprintf('%s %s', _('Ja'), _('(ontvang je alleen als je oud-lid bent)'))
							: _('Nee')
				));
		}

		// Systeemaccount
		if(hasAuth('bestuur') && $contact instanceof Lid)
		{
			$tbl->add(self::infoTRData(new HtmlAnchor($contact->url() . '/SysteemAccount', _('Systeemaccount'))
				, new HtmlSpan(new HtmlDiv(null, 'rotating-plane rotating-plane-small'), 'uid', 'plane-' . $contact->geefID())));
		}

		// Einde persoonsinformatie


		// Adressen

		$body->add($h = new HtmlHeader(3, _('Adressen')));

		if($contact->magWijzigen())
		{
			$h->add(new HtmlSmall(new HtmlAnchor($contact->url() . '/WijzigAdres', _('Wijzig adressen'))));
		}

		$body->add(new HtmlDiv(new HtmlDiv($tbl = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

		foreach ($contact->getAdressen() as $adres)
		{
			$tbl->add(self::infoTRData(ContactAdresView::waardeSoort($adres)
				, $div = new HtmlDiv()));
			$div->add(ContactAdresView::waardeStraat1($adres))
				->add(ContactAdresView::waardeHuisnummer($adres))
				->add(new HtmlBreak())
				->add(ContactAdresView::waardePostcode($adres))
				->add(ContactAdresView::waardeWoonplaats($adres))
				->add(new HtmlBreak())
				->add(ContactAdresView::waardeLand($adres));

			$div->add($p = new HtmlParagraph());
			$mapsLink = new HtmlAnchor($contact->url() . '/Route', _('Route'));
			$mapsLink->setAttribute('target', '_blank');
			$mapsQrLink = new HtmlAnchor($contact->url() . '/MapsQR', _('Google/Bing Maps QR'));
			$p->add("[$mapsLink, $mapsQrLink]");
		}

		// Einde adressen


		// Lidinformatie

		if($contact instanceof Lid)
		{
			$body->add($h = new HtmlHeader(3, _('Lidinformatie')));

			//TODO de lidinfowijzigen op een aparte pagina
			/*if($contact->magWijzigen())
			{
				$h->add(new HtmlSmall(sprintf('%s', new HtmlAnchor('WijzigLidInfo', _('Wijzig lidinformatie')))));
			}*/

			$body->add(new HtmlDiv(new HtmlDiv($tbl = new HtmlDiv(null, 'info-table'), 'panel-body'), 'panel panel-default'));

			$tbl->add(self::infoTRData(_('Lidnummer'), LidView::waardeContactID($contact)))
				->add(self::infoTRData(_('Lidtoestand'), LidView::lidToestandString($contact)));

			if($contact->magWijzigen())
			{
				$tbl->add(self::infoTRData(LidView::labelOpzoekbaar($contact)
					, LidView::labelEnumOpzoekbaar($contact->getOpzoekbaar())));

				if (hasAuth('bestuur'))
				{
					if ($dibsinfo = DibsInfo::geef($contact))
					{
						$tbl->add(self::infoTRData(
							new HtmlAnchor($dibsinfo->url(), _('DiBS-informatie'))
							, sprintf('%s %s', DibsInfoView::waardeKudos($dibsinfo), _('kudos'))
						));
					}
					else
					{
						$tbl->add(self::infoTRData(_('DiBS-informatie')
							, new HtmlAnchor(DIBSBASE . '/DibsInfo/Nieuw?persoonID=' . LidView::waardeContactID($contact), _('Activeer'))));
					}
				}

				$mailings = MailingListVerzameling::vanContact($contact);

				if($mailings->aantal() > 0)
				{
					$mailingUL = new HtmlList();
					foreach($mailings as $mailing)
					{
							$mailingUL->add(new HtmlListItem($mailing->getNaam()));
					}
					$tbl->add(self::infoTRData(_('Geabonneerd op'), $mailingUL));
				}
				else
					$tbl->add(self::infoTRData(_('Geabonneerd op')
						, ($contact->getContactID() == $auth->getLidnr())
							? _('Je bent niet geabonneerd op mailings')
							: _('Dit persoon is niet geabonneerd op mailings')));
			}

			// Bouw actie-knopjes.
			$body->add($p = new HtmlDiv(null, 'btn-group'));

			if (hasAuth('bestuur') || $contact == Persoon::getIngelogd())
			{
				$p->add(HtmlAnchor::button($contact->url() . '/Exporteer', _("Exporteer persoonsgegevens")));
			}

			if(hasAuth('bestuur'))
			{
				if($contact->isHuidigLid())
				{
					$p->add(HtmlAnchor::button('/Leden/Administratie/LidAf?lidnrs=' . $contact->geefID() , _('Maak lid-af')));
				}

				$p->add(HtmlAnchor::button('/Leden/'.$contact->geefID().'/VerwijderAdres' , _('Verwijder adres')));
				$p->add(HtmlAnchor::button('/Leden/'.$contact->geefID().'/WijzigWachtwoord' , _('Wijzig login/wachtwoord')));

				$dona = Donateur::geef($contact->geefID());

				if(!$dona)
				{
					$p->add(HtmlAnchor::button('/Leden/'.$contact->geefID().'/maakDonateur' , _('Maak donateur')));
				}
				else
				{
					$p->add(HtmlAnchor::button('/Leden/'.$contact->geefID().'/maakDonateur' , _('Wijzig donateursgegevens')));
				}
			}
		}

		return $body;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
