<?
abstract class PapierMolenView
	extends PapierMolenView_Generated
{
	/**
	 *  Geeft de defaultWaarde van het object terug.
	 * Er wordt aangeraden deze functie te overschrijven in View.
	 *
	 * @param obj Het -object waarvan de waarde kregen moet worden.
	 * @return Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaarde( $obj)
	{
		return self::defaultWaarde($obj);
	}

	static public function papierMolen(PapierMolen $pm)
	{
		$page = Page::getInstance();

		if($pm->getLid() == Persoon::getIngelogd() || hasAuth('bestuur')) {
			$page->setEditURL($pm->url() . "Wijzig");
			$page->setDeleteURL($pm->url() . "Verwijder");
		}

		$page->start(_("Aangeboden boek"));

		$page->add(self::viewInfo($pm));

		$page->add($p = new HtmlDiv(null, 'btn-group'));
		$p->add(HtmlAnchor::button(PAPIERMOLENBASE . "Overzicht", _("Ga terug naar het overzicht")));
		if($pm->getLid() == Persoon::getIngelogd() || hasAuth('bestuur')) {
			$p->add(HtmlAnchor::button($pm->url() . "Verwijder", _("Intrekken")));
			$p->add(HtmlAnchor::button($pm->url() . "Wijzig", _("Aanpassen")));
		}

		$page->end();
	}

	static public function contact(PapierMolen $pm)
	{
		$page = Page::getInstance()->start(_("Contactformulier"));

		$page->add(new HtmlParagraph(_("Via dit formulier kun je contact opnemen "
			. "met de aanbieder. Je kunt er geen lege mailtjes mee versturen.")));

		$page->add(new HtmlDiv($li = new HtmlList(), 'col-md-6'));
		$li->addChild(sprintf("%s: %s", _("Aan"), LidView::naam($pm->getLid())));
		$li->addChild(sprintf("%s: %s &#60;%s&#62;", _("Van")
			, LidView::naam(Persoon::getIngelogd()), LidView::waardeEmail(Persoon::getIngelogd())));
		$li->addChild(sprintf("%s: %s / %s", _("Betreft")
			, self::waardeEan($pm), self::waardeTitel($pm)));

		$page->add($form = HtmlForm::named("contactFormulier"));
		$form->add($textarea = HtmlTextarea::withContent('', 'bericht'));
		$textarea->setLarge();
		$form->add(HtmlInput::makeSubmitButton(_("Verstuur")));

		$page->end();
	}

	static public function wijzigForm(PapierMolen $pm, $nieuw = false, $show_error = false)
	{
		$page = Page::getInstance();
		if($nieuw)
			$page->start(_("Nieuw boek aanbieden"));
		else
			$page->start(_("Aangeboden boek wijzigen"));

		$page->add($form = HtmlForm::named($nieuw ? 'nieuwForm' : 'wijzigForm'));

		$form->add(self::wijzigTR($pm, 'EAN', $show_error));
		$form->add(self::wijzigTR($pm, 'Titel', $show_error));
		$form->add(self::wijzigTR($pm, 'Auteur', $show_error));
		$form->add(self::wijzigTR($pm, 'Druk', $show_error));
		$form->add(self::wijzigTR($pm, 'Prijs', $show_error));
		$form->add(self::wijzigTR($pm, 'Studie', $show_error));
		$form->add(self::wijzigTR($pm, 'Opmerking', $show_error));
		$form->add(self::wijzigTR($pm, 'Verloopdatum', $show_error));

		$form->add(HtmlInput::makeSubmitButton(_("Voer in")));

		$page->end();
	}

	static public function verwijder(PapierMolen $pm)
	{
		$page = Page::getInstance()->start(_("Aangeboden boek verwijdere"));

		$page->add(self::verwijderForm($pm));

		$page->end();
	}

	static public function processWijzigForm($obj)
	{
		if(!$data = tryPar(self::formobj($obj), null))
			return false;

		self::processForm($obj);
	}

	static public function labelPrijs(PapierMolen $obj)
	{
		return _("Vraagprijs");
	}

	static public function labelEan(PapierMolen $obj)
	{
		return _("ISBN");
	}

	static public function waardeLid(PapierMolen $obj)
	{
		return sprintf("%s, %s", parent::waardeLid($obj)
			, new HtmlAnchor($obj->url() . 'Contact', _("Contactformulier")));
	}

	static public function waardeStudie(PapierMolen $obj)
	{
		global $STUDIENAMEN;
		return $STUDIENAMEN[strtolower($obj->getStudie())];
	}

	static public function opmerkingEan()
	{
		return _("Het ISBN van het boek (bv 9781234567890)");
	}

	static public function opmerkingPrijs()
	{
		return _("In euro's");
	}

	static public function opmerkingVerloopdatum()
	{
		return _("Mag maximaal 1 jaar na vandaag zijn");
	}

	static public function formStudie(PapierMolen $obj, $include_id = false)
	{
		global $STUDIENAMEN;

		$select = new HtmlSelectbox('PapierMolen[Studie]');

		foreach($STUDIENAMEN as $id => $studie)
		{
			if($id == 'gt')
				$id = 'ga';
			$select->addOption($id, $studie);
		}

		return $select;
	}

	static public function formVerloopdatum(PapierMolen $obj, $include_id = false)
	{
		$input = parent::formVerloopdatum($obj, $include_id = false);
		$date = new DateTimeLocale();
		if($obj->getVerloopdatum()->format('Y-m-d') === $date->format('Y-m-d'))
		{
			$date->modify('+1 year');
			$input->setValue($date->format('Y-m-d'));
		}
		return $input;
	}
}
