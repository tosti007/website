<?

/**
 * $Id$
 */
abstract class MededelingVerzamelingView
	extends MededelingVerzamelingView_Generated
{
	/**
		 Retourneert een HtmlElementCollection met een overzicht van de
		gegeven nieuwsberichten
	**/
	public static function maakOverzicht(MededelingVerzameling $mededelingen, $toonDetails = false)
	{

		if ($toonDetails)
		{
			$table = new HtmlTable(null, null, 'mededelingTable');
			$thead = $table->addHead();
			$tbody = $table->addBody();

			$row = $thead->addRow();
			$row->addHeader(_('Omschrijving'));
			$row->addHeader(_('Doelgroep'));
			$row->addHeader(_('Prioriteit'));
			$row->addHeader(_('Begindatum'));
			$row->addHeader(_('Einddatum'));

			foreach($mededelingen as $med)
			{
				$row = $tbody->addRow();

				if(hasAuth('lid'))
				{
					$row->addData(MededelingView::waardeOmschrijvingLink($med));
				}
				else
				{
					$row->addData(MededelingView::waardeOmschrijving($med));
				}

				$row->addData(MededelingView::waardeDoelgroep($med));
				$row->addData(MededelingView::waardePrioriteit($med));
				$cell = $row->addData(MededelingView::waardeDatumBegin($med));
				$cell->setAttribute('data-order', $med->getDatumBegin()->getTimestamp());
				$cell = $row->addData(MededelingView::waardeDatumEind($med));
				$cell->setAttribute('data-order', $med->getDatumEind()->getTimestamp());
			}
			$table->setCssStyle('display: none;');
			return $table;
		}
		else
		{
			$res = new HtmlElementCollection();
			foreach ($mededelingen as $mededeling)
			{
				$mdiv = MededelingView::maakMededelingDiv($mededeling);
				$res->add($mdiv);
			}
			return $res;
		}
	}

	/**
	 * Geeft een RSS-bestand van aankomende mededelingen; hoort bij Mededeling_Controller::rss()
	 */
	static public function rss ($mededelingen)
	{
		$page = Page::getInstance('rss')
			->start(_("Nieuws A–Eskwadraat"), _("De laatste nieuwsberichten"))
			->add(MededelingVerzamelingView::rssItem($mededelingen))
			->end();
	}

	/**
	 * Geeft een array van RSS-items terug
	 */
	static public function rssItem (MededelingVerzameling $mededelingen)
	{
		$output = array();
		foreach ($mededelingen as $m)
			$output[] = MededelingView::rssItem($m);
		return $output;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
