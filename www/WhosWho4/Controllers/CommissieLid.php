<?php

abstract class CommissieLidController
{
	/**
	 * @site{/Vereniging/Commissies/ * /Leden/ * /}
	 */
	static public function isCommissieLid($args)
	{
		$persoon = Persoon::geef($args[0]);
		if(!$persoon)
			return false;

		$cielid = CommissieLid::geef($persoon, Commissie::inputToCie($args[2]));
		if(!$cielid)
			return false;

		return array('name' => (int)$args[0], 'access' => True);
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Leden/ * /Info}
	 */
	static function infoPagina()
	{
		$entry = vfsVarEntryNames();
		$cielid = Commissielid::geef(Persoon::geef($entry[0]), Commissie::inputToCie($entry[1]));
		return new PageResponse(CommissieLidView::infoPagina($cielid));
	}


	/**
	 * @TODO: wijzigPagina en nieuwCommissieLid mergen op een *MOOIE* manier. Het is nu poep.
	 *
	 * @site{/Vereniging/Commissies/ * /Leden/ * /Wijzig}
	 */
	static function wijzigPagina()
	{
		$entry = vfsVarEntryNames();
		$cielid = Commissielid::geef(Persoon::geef($entry[0])->getContactID()
			, Commissie::inputToCie($entry[1])->geefID());

		if(!$cielid->magWijzigen()){
			spaceHTTP(403);
		}

		$show_error = false;

		if(Token::processNamedForm() == 'CieLidWijzig')
		{
			$oudeEindDatum = $cielid->getDatumEind();//->format('Y-m-d');
			$oudeBeginDatum = $cielid->getDatumBegin();

			CommissieLidView::processForm($cielid);
			if($cielid->valid())
			{
				$nieuweEindDatum = $cielid->getDatumEind();
				$nieuweBeginDatum = $cielid->getDatumBegin();

				if (!hasAuth('bestuur')
					&& $oudeEindDatum->hasTime()
					&& $oudeEindDatum < new DateTimeLocale('now')
					&& $oudeEindDatum < $nieuweEindDatum)
				{
					Page::addMelding(
						_('Je bent al uit de commissie. Je kan je niet '
						. 'terugplaatsen. Vraag een bestuurslid :).'),
						'fout');
					$show_error = true;
				}
				else
				{
					$cielid->opslaan();//Opslaan die hap

					if($nieuweBeginDatum != $oudeBeginDatum
						|| $nieuweEindDatum != $oudeEindDatum)
					{
						if(!DEBUG && !DEMO)
						{
							$meldingen = array();
							// weghalen/toevoegen op het systeem
							if($nieuweBeginDatum < new DateTimeLocale('now')
									&& $nieuweEindDatum < new DateTimeLocale('now'))
								$meldingen = $cielid->haalOpSysteemWeg();
							else if($nieuweBeginDatum > new DateTimeLocale('now')
									&& $nieuweEindDatum < new DateTimeLocale('now'))
								$meldingen = $cielid->voegOpSysteemToe();
							else if($nieuweBeginDatum > new DateTimeLocale('now')
									&& $nieuweEindDatum > new DateTimeLocale('now'))
								$meldingen = $cielid->haalOpSysteemWeg();

							foreach($meldingen as $soort => $melding)
								Page::addMeldingArray($melding, $soort);
						}
						else
							Page::addMelding(_('Op live zou het lid nu ook zijn toegevoegd/weggehaald '
								. 'op \'Het Systeem\', maar omdat je niet op live zit gebeurt '
								. 'dat nu niet'), 'waarschuwing');
					}

					gooiCacheEntrysWeg(); // gecachete menuus weggooien voor rechten enzo
					Page::redirectMelding($cielid->getCommissie()->systeemUrl().'/Info',
						_("De aanpassingen zijn opgeslagen, profit!"));
				}
			}
			else
			{
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		return new PageResponse(CommissieLidView::wijzigForm($cielid, false, $show_error));
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Leden/Nieuw}
	 */
	static function nieuwCommissieLid()
	{
		$entry = vfsVarEntryNames();
		$cie = Commissie::inputToCie($entry[0]);

		if(!$cie || !$cie->magWijzigen())
			spaceHttp(403);

		$show_error = false;

		$cieLid = new CommissieLid(new Persoon(), $cie);

		if(Token::ProcessNamedForm() == 'CieLidNieuw')
		{
			// Kijk wie we willen toevoegenen maak daar een verzameling van
			$newCieLeden = trypar('CieLid','ContactID');
			$cieLidVerzameling = new CommissieLidVerzameling();

			$toegevoegd = false;
			foreach($newCieLeden as $id => $value)
			{
				if(!$value) continue;

				// Het lid moet wel bestaan!
				$lid = Persoon::geef($id);
				if(!$lid)
				{
					Page::addMelding(
						sprintf(_('Lid met id %s bestaat niet'), $id), 'fout');
					$show_error = true;
					continue;
				}

				// Als het commissielid al bestaat passen we gewoon die aan
				$CieLid = CommissieLid::geef($lid, $cie);
				if($CieLid) {
					$CieLid->setDatumEind(NULL);
					$cieLid = $CieLid;
				} else {
					$cieLid = new CommissieLid($lid, $cie);
				}

				CommissieLidView::processForm($cieLid);
				$cieLidVerzameling->voegtoe($cieLid);
				$toegevoegd = true;
			}

			// Als er niemand geset is om toe te voegen, voegen we toch cieLid
			// toe en verwerken we het formulier zodat we dit kunnen gebruiken
			// om te laten zien wat de fouten zijn
			if(!$toegevoegd)
			{
				CommissieLidView::processForm($cieLid);
				$cieLidVerzameling->voegtoe($cieLid);
			}

			// Alles opslaan indien dat mag
			if($cieLidVerzameling->allValid() && !$show_error)
			{
				$cieLidVerzameling->opslaan();

				// We willen alleen op live de koppeling met het systeem maken
				if(!DEBUG && !DEMO && hasAuth('bestuur'))
				{
					$cies = CommissieVerzameling::opSysteemInvloedHebbenden();

					foreach($cieLidVerzameling as $cielid)
					{
						if ($cies->hasLid($cielid->getPersoon()))
						{
							Page::addMelding(_('Omdat dit lid in de sysop, webcie of het bestuur zit '
								. 'wordt dit lid niet automatisch in de cie op \'Het Systeem\' gestopt'), 'waarschuwing');
						}
						else
						{
							if($cielid->getDatumBegin() > new DateTimeLocale('now'))
								Page::addMelding(_('De begindatum van een nieuw commissielid '
								. 'is later dan vandaag, dus het commissielid wordt later '
								. 'automatisch toegevoegd op \'Het Systeem\'.'), 'waarschuwing');
							else
							{
								$meldingen = $cielid->voegOpSysteemToe();

								foreach($meldingen as $soort => $melding)
									Page::addMeldingArray($melding, $soort);
							}
						}
					}
				}
				else
					Page::addMelding(_('Op live zou het lid nu ook zijn toegevoegd '
						. 'op \'Het Systeem\', maar omdat je niet op live zit gebeurt '
						. 'dat nu niet'), 'waarschuwing');

				gooiCacheEntrysWeg(); // gecachete menuus weggooien voor rechten enzo
				Page::redirectMelding($cie->systeemUrl().'/Info#tab2'
					, _('De aanpassingen zijn opgeslagen!'));
			}
			else
			{
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		CommissieLidView::wijzigForm($cieLid, true, $show_error);
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Verwijder}
	 */
	static function verwijderPagina()
	{
		$entry = vfsVarEntryNames();

		if(!$persoon = Persoon::geef($entry[0]))
			spaceHttp(403);

		if(!$cie = Commissie::cieByLogin($entry[1]))
			spaceHttp(403);

		if(!$cielid = Commissielid::geef($persoon, $cie))
			spaceHttp(403);

		if(Token::processNamedForm() == 'CieLidVerwijder')
		{
			// We slaan alvast de url van de cie op
			$cieUrl = $cielid->getCommissie()->systeemUrl();

			$meldingen = array();
			// Verwijder het commissielid van het systeem
			$weggehaaldOpSysteem = false;

			if (hasAuth('bestuur'))
			{
				if (CommissieVerzameling::opSysteemInvloedHebbenden()->hasLid(
						$cielid->getPersoon())) {
					Page::addMelding(_('Omdat dit lid in de sysop, webcie of het bestuur zit '
						. 'wordt dit lid niet automatisch uit de cie op \'Het Systeem\' gehaald'), 'waarschuwing');
				} else if (DEBUG || DEMO) {
					Page::addMelding(_('Op live zou het lid nu ook zijn verwijderd '
							. 'op \'Het Systeem\', maar omdat je niet op live zit gebeurt '
							. 'dat nu niet'), 'waarschuwing');
				} else {
					// alleen op de live-site halen we cieleden ook van het
					// systeem af.
					// Op debug/demo-paginas worden nog wel de leden uit de cie
					// op de site gehaald.
					$meldingen = $cielid->haalOpSysteemWeg();
					$weggehaaldOpSysteem = true;
				}
			}

			$returnVal = $cielid->verwijderen();
			if(!is_null($returnVal))
			{
				if ($weggehaaldOpSysteem) {
					// Weer toevoegen op het systeem, want er is iets fout gegaan
					$cielid->voegOpSysteemToe();
				}

				Page::addMelding('Lid kon niet verwijderd worden uit de commissie op het systeem.', 'fout');
			}
			else
			{
				foreach($meldingen as $soort => $melding)
					Page::addMeldingArray($melding, $soort);
				Page::redirectMelding($cieUrl, _('CommissieLid is verwijderd!'));
			}
		}

		$page = CommissieLidView::pageLinks($cielid);
		$page->start(_('Commissielid verwijderen'));

		$page->add(new HtmlParagraph(sprintf(_('Weet je zeker dat je %s[VOC: persoonNaam] '
			. 'wilt verwijderen uit %s[VOC: commissienaam]?'),
			PersoonView::naam($cielid->getPersoon()),
			$cielid->getCommissie()->getNaam())));

		$form = HtmlForm::named('CieLidVerwijder');

		$form->add(HtmlInput::makeSubmitButton(_('Ja! Weg ermee!')));
		$page->add($form);

		$page->end();

	}

	/**
	 * @site{/Ajax/Cie/systeemCheck}
	 */
	static public function systeemCheck()
	{
		requireAuth('bestuur');

		$cielid = CommissieLid::geef(tryPar('lidid'), tryPar('cieid'));

		if(!$cielid)
			spaceHttp(403);

		$cieLogin = $cielid->getCommissie()->getLogin();
		$url = $cielid->getCommissie()->systeemUrl() . '/Leden/'
			. $cielid->getPersoon()->geefID() . '/systeemFix';

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['code' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$return = NULL;

		if(!$conn) {
			$return = array('code' => "FOUT", 'lidid' => tryPar('lidid'), 'url' => $url);
		} else {
			$uid = $conn->getUid($cielid->getPersoon());
			if($uid) {
				if($conn->zitInCommissie($uid, $cielid->getCommissie())) {
					$return = array('code' => "OK", 'lidid' => tryPar('lidid'));
				} else {
					$return = array('code' => "FOUT", 'lidid' => tryPar('lidid'), 'url' => $url);
				}
			} else {
				$email = $cielid->getPersoon()->getEmail();
				if($conn->zitInForward($email, $cielid->getCommissie())) {
					$return = array('code' => "WAARSCHUWING", 'lidid' => tryPar('lidid'), 'url' => $url);
				} else {
					$return = array('code' => "FOUT", 'lidid' => tryPar('lidid'), 'url' => $url);
				}
			}
		}
		return new JSONResponse($return);
	}

	/**
	 * @site{/Vereniging/Commissies/ * /Leden/ * /SysteemFix}
	 */
	static public function systeemFix()
	{
		requireAuth('bestuur');

		$entry = vfsVarEntryNames();

		if(!$persoon = Persoon::geef($entry[0]))
			spaceHttp(403);

		if(!$cie = Commissie::cieByLogin($entry[1]))
			spaceHttp(403);

		if (CommissieVerzameling::opSysteemInvloedHebbenden()->bevat($cie))
		{
			Page::getInstance()->start(_('Mag niet!'))
				->add(_('Omdat je deze actie probeert toe te passen op een lid uit de Cie ' . CommissieView::waardeNaam($cie) . ', mag dit niet! Je mag deze Cie namelijk niet aanpassen op het systeem via de site.'))
				->end();
			return;
		}

		if(!$cielid = Commissielid::geef($persoon, $cie))
			spaceHttp(403);

		try
		{
			$conn = new SysteemApi();

			switch(Token::processNamedForm())
			{
			case 'VoegSysteemAccountToeHaalEmailWeg':
				if(!DEBUG && !DEMO)
				{
					$email = $persoon->getEmail();
					$conn->haalEmailWegUitCie($email, $cie);
				}
			case 'VoegSysteemAccountToe':
			case 'VoegEmailToe':
				if(!DEBUG && !DEMO)
				{
					$meldingen = $cielid->voegOpSysteemToe();
					foreach($meldingen as $type => $melding)
						Page::addMeldingArray($melding, $type);
				}
				else
					Page::addMelding(_('Op live zou het lid nu ook zijn toegevoegd '
						. 'op \'Het Systeem\', maar omdat je niet op live zit gebeurt '
						. 'dat nu niet'), 'waarschuwing');

				Page::redirectMelding($cie->systeemUrl(), _('Gelukt!'));
				break;
			}

			$uid = null;
			$inCie = false;
			$emailInCie = false;

			$uid = $conn->getUid($persoon);

			if($uid)
			{
				if($conn->zitInCommissie($uid, $cie))
					$inCie = true;
			}

			$email = $cielid->getPersoon()->getEmail();

			if($conn->zitInForward($email, $cie))
				$emailInCie = true;
		}
		catch (IPALigtEruitException $e)
		{
			Page::redirectMelding($cielid->url(),
				_('Er is geen verbinding met ldap mogelijk, schop de Sysop!'), 'fout'
			);
		}

		CommissieLidView::systeemFix($cielid, $uid, $inCie, $emailInCie);
	}
}
