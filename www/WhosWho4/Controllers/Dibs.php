<?php

use Symfony\Component\HttpFoundation\Response;

/**
 * Class Dibs_Controller
 *
 * Controller voor DiBS-administratie.
 * De tablet wordt in de DibsTablet_Controller geregeld.
 */
final class Dibs_Controller extends Controller
{

	public function home()
	{
		return $this->redirect(DIBSBASE . '/' . $this->getIngelogdID());
	}

	/**
	 * Variabele entry voor het opzoeken van dibsgegevens voor personen.
	 *
	 * @param array $args array(0 => $lidnr). Indien $args[0] == "Ingelogd", wordt dit vervangen met het huidige
	 *     ingelogde lidnr.
	 *
	 * @return array
	 */
	public function persoonEntry($args)
	{
		if (!$this->hasAuth('ingelogd'))
		{
			return ['name' => ''
				, 'displayName' => ''
				, 'access' => false];
		}

		// Omdat scripts wel ingelogd-auth hebben maar geen $this->getIngelogd checken we dat
		// in het bijzonder de benamite-integrity-checks
		if (strtolower($args[0]) == "ingelogd" && !is_null($this->getIngelogd()))
		{
			$lidnr = $this->getIngelogdID();
		}
		else
		{
			$lidnr = $args[0];
		}
		$info = DibsInfo::geef($lidnr);
		if (!$info)
		{
			$pers = Persoon::geef($lidnr);
			return ['name' => $lidnr
				, 'displayName' => _('DiBS-informatie over ') . $lidnr
				, 'access' => hasAuth('bestuur') || (Persoon::getIngelogd() == $pers)];
		}
		else
		{
			return ['name' => $info->getPersoonContactID()
				, 'displayName' => _('DiBS-informatie over ') . $info->getPersoonContactID()
				, 'access' => $info->magBekijken()];
		}
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function persoon()
	{
		//Haal de DibsInfo op
		$id = $this->getEntryName();
		$info = DibsInfo::geef($id);

		//Heeft de persoon DibsInfo? Zo nee, dan kan die daarom vragen
		//en geactiveerd worden
		if (!$info)
		{
			if (!($persoon = Persoon::geef($id)))
			{
				$this->geenToegang();
			}

			$page = DibsInfoView::toonGeenDibs($persoon);
			return $this->pageResponse($page);

			//Heeft lid wel DibsInfo, open de infopagina
		}

		$toonTransacties = $this->hasAuth('god') || $this->getIngelogd() == $info->getPersoon();
		$page = DibsInfoView::toon($info, $toonTransacties);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function persoonPassen()
	{
		$id = $this->getEntryName();
		if (!($info = DibsInfo::geef($id)))
		{
			$this->geenToegang();
		}

		$persoon = $info->getPersoon();
		$passen = PasVerzameling::getPersoonPassen($persoon);
		$page = PasVerzamelingView::persoon($passen, $persoon);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function persoonTransacties()
	{
		$id = $this->getEntryName();
		if (!($info = DibsInfo::geef($id)))
		{
			$this->geenToegang();
		}

		$persoon = Persoon::geef($id);
		$transacties = DibsTransactieVerzameling::getPersoonTransacties($persoon);
		$page = DibsTransactieVerzamelingView::persoon($transacties, $info);
		return $this->pageResponse($page);
	}

	/**
	 * @param $args
	 *
	 * @return array
	 */
	public function pasEntry($args)
	{
		$pas = Pas::geef($args[0]);
		if (!$pas)
		{
			return ['name' => ''
				, 'displayName' => ''
				, 'access' => false];
		}

		return ['name' => $pas->getPasID()
			, 'displayName' => _('Passen')
			, 'access' => $pas->magBekijken()];
	}

	/**
	 * @return PageResponse
	 */
	public function passenOverzicht()
	{
		$passen = PasVerzameling::getPassen();
		$page = PasVerzamelingView::overzicht($passen);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function toevoegenPas()
	{
		//Haal de Dibsinfo op
		$persoonID = $this->getEntryName();
		if (!($info = DibsInfo::geef($persoonID)))
		{
			$this->geenToegang();
		}

		$show_error = false;

		//Maak nieuwe pas
		$newobj = new Pas();
		$newobj->setPersoon(Persoon::geef($persoonID));

		if (Token::processNamedForm() == 'Nieuwe Pas')
		{
			PasView::processForm($newobj);
			if ($newobj->valid('nieuw'))
			{
				$info = DibsInfo::geef($newobj->getPersoon());
				//Bij checkPasID is al gekeken of de pas niet al bij iemand anders hoort
				$bestaandepas = Pas::geefRfidTag($newobj->getRfidTag());

				//Alles opslaan
				if ($bestaandepas)
				{
					$bestaandepas->setActief(true);
				}
				else
				{
					$newobj->opslaan();
				}

				Page::redirectMelding($info->url(), $bestaandepas
					? _('Gelukt! De actieve pas is met succes gewijzigd.')
					: _('Gelukt! De nieuwe pas is met succes toegevoegd.')
				);
			}
			else
			{
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		$page = PasView::toevoegen($newobj, $show_error);

		return $this->pageResponse($page);
	}

	/**
	 * @throws HTTPStatusException
	 *
	 * @return Response
	 */
	public function wijzigActiefPas()
	{
		//Haal de data op
		$id = $this->getEntryName();
		$pas = Pas::geef($id);
		if (!$pas)
		{
			$this->geenToegang();
		}

		//Als er geen info bij de pas hoort: error!
		$info = DibsInfo::geef($pas->getPersoon());
		if (!$info)
		{
			throw new LogicException('persoon ' . $pas->getPersoonContactID() . ' heeft een pas maar is niet geactiveerd voor dibs');
		}

		if ($pas->getActief())
		{
			$pas->setActief(false);
		}
		else
		{
			$pas->setActief(true);
		}

		$pas->opslaan();
		return Page::responseRedirectMelding($info->url('Passen'), _('Gelukt!'));
	}

	/**
	 * @throws HTTPStatusException
	 * @return Response
	 */
	public function verwijderPas()
	{
		//Haal de data op
		$id = $this->getEntryName();
		$pas = Pas::geef($id);
		if (!$pas)
		{
			$this->geenToegang();
		}

		//Als er geen info bij de pas hoort: error!
		$info = DibsInfo::geef($pas->getPersoon());
		if (!$info)
		{
			throw new LogicException('persoon ' . $pas->getPersoonContactID() . ' heeft een pas maar is niet geactiveerd voor dibs');
		}

		//Als de pas al in gebruik is mag hij niet verwijderd worden!
		if ($pas->isGebruikt())
		{
			return Page::responseRedirectMelding($info->url('Passen'), _('De pas kan niet verwijderd worden omdat deze al in gebruik is.'));
		}

		$returnVal = $pas->verwijderen();
		if (!is_null($returnVal))
		{
			return Page::responseRedirectMelding($info->url('Passen'), $returnVal);
		}

		return Page::responseRedirectMelding($info->url('Passen'), _('Gelukt!'));
	}

	/**
	 * @param array $args
	 * De entrynamen van de variabele entry.
	 *
	 * @return array
	 */
	public function transactieEntry($args)
	{
		$transactie = DibsTransactie::geef($args[0]);
		if (!$transactie)
		{
			return ['name' => ''
				, 'displayName' => ''
				, 'access' => false];
		}

		return ['name' => $transactie->getTransactieID()
			, 'displayName' => _('Transactie ') . $transactie->getTransactieID()
			, 'access' => $transactie->magBekijken()];
	}

	/**
	 * @return Response
	 * @throws HTTPStatusException
	 */
	public function transactie()
	{
		$id = $this->getEntryName();
		$transactie = DibsTransactie::geef($id);

		if ($transactie instanceof DibsTransactie)
		{
			$page = DibsTransactieView::toon($transactie);
			return $this->pageResponse($page);
		}

		// Zonder geldige transactie gooien we error 403.
		$this->geenToegang();
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function transactieOverzicht()
	{
		// verwerk get-data
		$maand = (int)$this->getRequestParam('maand', date('m'));
		$jaar = (int)$this->getRequestParam('jaar', date('Y'));

		if ($maand <= 0 || $maand > 12 || $jaar < 0 || $jaar >= 10000)
		{
			$this->responseUitStatus(501);
		}

		$van = new DateTimeLocale("$jaar-$maand-01");
		$tot = clone $van;
		$tot->add(new DateInterval("P1M"));
		// laad de transacties
		$transacties = DibsTransactieVerzameling::getTransacties($van, $tot);
		$page = DibsTransactieVerzamelingView::dibsTransactieoverzicht($transacties, $maand, $jaar);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function toevoegenTransactie()
	{
		//Haal de data op
		$persoonID = $this->getEntryName();
		if (!($info = DibsInfo::geef($persoonID)))
		{
			$this->geenToegang();
		}

		$error = null;

		//Maak een nieuwe transactie
		$newobj = DibsTransactieView::processNieuwForm();
		if (Token::processNamedForm() == 'Nieuwe Transactie' && $newobj)
		{
			//Probeer de transactie op te slaan
			if (!($error = $info->nieuweTransactie($newobj)))
			{
				$info->opslaan();
				Page::redirectMelding($info->url(), _('Transactie is met succes toegevoegd.'));
			}
		}

		if (!$newobj)
		{
			$newobj = new DibsTransactie('WWW', 'VERKOOP', Persoon::geef($persoonID), 0, '', new DateTimeLocale(), 'BVK');
		}

		$page = DibsTransactieView::toevoegen($newobj, $error);
		return $this->pageResponse($page);
	}

	/**
	 * Verwerp het toevoegen van een nieuwe transactie.
	 *
	 * @param DibsTransactie $newobj
	 *
	 * @return bool|Response
	 */
	// TODO: wordt volgens `rg` nergens gebruikt.
	public function processToevoegenTransactie(DibsTransactie $newobj)
	{
		try
		{
			$newobj->opslaan();
		}
		catch (LogicException $e)
		{
			return false;
		}
		return Page::responseRedirectMelding(DibsInfo::geef($newobj->getPersoon())->url(), _('Transactie succesvol toegevoegd!'));
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function toevoegenIDEAL()
	{
		//Haal de data op
		$persoonID = $this->getEntryName();
		if (!($info = DibsInfo::geef($persoonID)))
		{
			if (!($persoon = Persoon::geef($persoonID)))
			{
				$this->geenToegang();
			}

			$page = DibsInfoView::toonGeenDibs($persoon);
			return $this->pageResponse($page);
		}

		if (!Voorraad::geef(50))
		{
			$page = Page::getInstance()->start();
			$page->add(new HtmlParagraph("Het is op dit moment niet mogelijk om kudos via iDeal te kopen.", "text-danger strongtext"));
			return $this->pageResponse($page);
		}

		$newobj = new DibsTransactie('WWW', 'VERKOOP', Persoon::geef($persoonID), 0, '', new DateTimeLocale(), 'IDEAL');
		$processedtr = DibsTransactieView::processNieuwIdealForm($newobj);

		if (Token::processNamedForm() == 'IDEAL Transactie' && $newobj)
		{
			if (!($error = $processedtr->check()))
			{
				$voorraad = Voorraad::geef(50);
				//Als alles goed is, maak een nieuwe iDEALTransactie aan en start de iDEALprocedure
				$kudos = -1 * $processedtr->getKudos();
				$persoon = Persoon::geef($persoonID);

				$ideal = new iDeal("BOEKWEB", "VERKOOP", $persoon
					, ($kudos + iDeal::transactiekosten()->getCenten()) / 100, $kudos
					. " kudos kopen met iDeal", new DateTimeLocale(), "iDealtransactie voor kudos"
					, $voorraad, EVENEMENTENREKENING, null);
				$ideal->setEmailadres($persoon->getEmail());
				$ideal->setStatus("INACTIVE");
				$ideal->setReturnURL('/Leden/Dibs/' . $persoonID);

				$ideal->startTransactie();
			}
			else
			{
				Page::redirectMelding($info->url(), _('Je mag niet zoveel kudos erbij kopen.'));
			}
		}

		$page = DibsTransactieView::toevoegenIdeal($newobj);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 */
	public function colaproductOverzicht()
	{
		$colaproducten = ColaProductVerzameling::getColaproducten();
		$page = ColaProductVerzamelingView::overzicht($colaproducten);
		return $this->pageResponse($page);
	}

	/**
	 * @throws Exception
	 */
	public function transactieCSV()
	{
		$maand = (int)$this->getRequestParam('maand', date('m'));
		$jaar = (int)$this->getRequestParam('jaar', date('Y'));

		if (1 > $maand || $maand > 12 || DIBS_STARTJAAR > $jaar)
		{
			Page::redirectMelding('/Leden/Dibs/Transacties', _('Uh Oh! Je wil een niet bestaande maand opvragen!'));
		}

		$maandnaam = date('F', mktime(0, 0, 0, $maand));

		$van = new DateTimeLocale("$jaar-$maand-01");
		$tot = clone $van;
		$tot->add(new DateInterval("P1M"));
		// laad de transacties
		$transacties = DibsTransactieVerzameling::getTransacties($van, $tot);

		//Doe de csvtsjak
		header("Content-Disposition: attachment; filename=transacties-$maandnaam-$jaar.csv");
		header("Content-type: text/csv; encoding=utf-8");
		DibsTransactieVerzamelingView::csv($transacties);
		exit();
	}

	/**
	 * @param string[] $args
	 * De namen van de variabele entries.
	 *
	 * @return array
	 */
	public function colaproductEntry($args)
	{
		$colaproduct = ColaProduct::geef($args[0]);
		if (!$colaproduct)
		{
			return ['name' => ''
				, 'displayName' => ''
				, 'access' => false];
		}

		return ['name' => $colaproduct->getArtikelID()
			, 'access' => true];
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function colaproduct()
	{
		$id = $this->getEntryName();
		$colaproduct = ColaProduct::geef($id);

		if ($colaproduct instanceof ColaProduct)
		{
			$page = ColaProductView::toon($colaproduct);
			return $this->pageResponse($page);
		}

		$this->geenToegang();
	}

	/**
	 * Toon de pagina om een nieuw ColaProduct toe te voegen.
	 *
	 * @return Response
	 */
	public function toevoegenColaproduct()
	{
		return $this->colaProductForm(new ColaProduct());
	}

	/**
	 * @return bool|Response
	 */
	public function wijzigenColaproduct()
	{
		$id = $this->getEntryName();
		$obj = ColaProduct::geef($id);

		if (!$obj)
		{
			return false;
		}
		return $this->colaProductForm($obj);
	}

	/**
	 * @brief Verwerk het nieuw/wijzigformulier voor colaproducten.
	 *
	 * Zorgt voor updates in het Colaproduct en het bijbehorende Voorraad.
	 *
	 * @param ColaProduct $obj Het product om in te lezen.
	 *
	 * @return Response
	 */
	public function colaProductForm(ColaProduct $obj)
	{
		$show_error = false;

		if (Token::processNamedForm() == 'Nieuw Colaproduct')
		{
			$vals = ColaProductView::processNieuwColaProductForm($obj);
			if (!$obj->getInDB() && $vals['Prijs'] <= 0)
			{
				Page::addMelding(_('De prijs moet positief zijn.'), 'fout');
				$show_error = true;
			}
			else
			{
				if (!$obj->valid('nieuw'))
				{
					Page::addMelding(_('Er waren fouten.'), 'fout');
					$show_error = true;
				}
				else
				{
					$voorraad = $obj->getDefaultVoorraad();
					$setprijs = false;
					if (!$obj->getInDB())
					{
						$setprijs = true;
					}

					if (!$voorraad)
					{
						$voorraad = new Voorraad($obj, 'VIRTUEEL', $vals['Prijs'], 0);
					}
					$voorraad->setVerkoopbaar($vals['Verkoopbaar']);
					if (!$voorraad->valid())
					{
						// TODO: dit is niet heel gebruikersvriendelijk
						// maar aan de andere kant ziet 1 bestuurslid dit...
						Page::addMelding(_('Er waren fouten bij het aanmaken van de voorraad:'), 'fout');
						foreach ($voorraad->getErrors() as $veld => $msg)
						{
							if ($msg)
							{
								Page::addMelding($veld . ": " . $msg);
							}
						}
						$show_error = true;
					}
					else
					{
						$obj->opslaan();
						$voorraad->opslaan();
						if ($setprijs)
						{
							$vp = new VerkoopPrijs($voorraad, new DateTimeLocale(), $vals['Prijs']);
							$vp->opslaan();
						}
						Page::redirectMelding($obj->url(), _('Het is gelukt.'));
					}
				}
			}
		}

		$page = ColaProductView::toevoegen($obj, $show_error);
		return $this->pageResponse($page);
	}

	public function wijzigVerkoopbaarColaproduct()
	{
		$id = $this->getEntryName();
		$product = ColaProduct::geef($id);
		if (!$product)
		{
			return false;
		}

		$product->setVerkoopbaar($product->getVerkoopbaar() ? false : true);
		if (!$product->valid())
		{
			Page::addMelding(_('Er mogen maar max 8 producten tegelijk verkoopbaar zijn!'));
		}
		else
		{
			$product->opslaan();
		}

		return $this->redirect(DIBSBASE . '/Colaproducten');
	}

	public function wijzigVolgordeColaproduct()
	{
		$id = $this->getEntryName();
		$product = ColaProduct::geef($id);
		if (!$product)
		{
			return false;
		}

		$r = (int)$this->getRequestParam('r', null);

		$product->opslaan($r);

		return $this->redirect(DIBSBASE . '/Colaproducten');
	}

	public function toevoegenBarcode()
	{
		$id = $this->getEntryName();
		$product = ColaProduct::geef($id);
		if (!$product)
		{
			return false;
		}

		$voorraad = $product->getDefaultVoorraad();

		$msg = null;

		$newobj = new Barcode();

		if (Token::processNamedForm() == 'Nieuwe Barcode')
		{
			$barcode = $this->getRequestParam('barcode', null);
			$newobj = new Barcode($voorraad, $barcode);
			if ($newobj->valid() && !(Barcode::getByBarcode($barcode)))
			{
				$newobj->opslaan();
				Page::redirectMelding($product->url(), _('Het is gelukt.'));
			}
			else
			{
				$msg = new HtmlSpan(_('Er waren fouten, of de barcode bestaat al.'), 'text-danger strongtext');
			}
		}

		$page = ColaProductView::toevoegenBarcode($newobj, $msg);

		return $this->pageResponse($page);
	}

	public function uploadPlaatje()
	{
		$id = $this->getEntryName();
		$product = ColaProduct::geef($id);
		if (!$product)
		{
			return false;
		}

		if (Token::processNamedForm() == 'plaatjeForm')
		{
			global $logger;
			$logger->debug('plaatjeForm');
			return $this->confirmUploadPlaatje($product);
		}

		$page = new HTMLPage();
		$page->start(_('Plaatje uploaden'));

		$pagediv = ColaProductView::uploadPlaatje($product);
		$page->add($pagediv);

		return $this->pageResponse($page);
	}

	public function zoekenDibsInfo()
	{
		$dibsers = null;
		if ($str = $this->getRequestParam('DibsInfoVerzamelingZoeken', false))
		{
			$dibsers = DibsInfoVerzameling::zoek($str);
		}

		$page = DibsInfoVerzamelingView::zoeken($dibsers, $str);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function quickform()
	{
		$this->requireAuth('bestuur');

		$id = (int)$this->getRequestParam('ID', null);

		$dibsers = null;

		if ($str = $this->getRequestParam('DibsInfoVerzamelingZoekenQuickform', false))
		{
			$dibsers = DibsInfoVerzameling::zoekQuickform($str);
		}

		$page = DibsInfoVerzamelingView::quickform($dibsers, $str, $id);
		return $this->pageResponse($page);
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function toevoegenQuickform()
	{
		$persoonID = (int)$this->getRequestParam('persoonID', 0);
		if ($persoonID == 0)
		{
			$this->responseUitStatus(501);
		}

		//Als de persoon al een DibsInfo heeft ga terug
		if (DibsInfo::geef($persoonID))
		{
			Page::redirectMelding(DIBSBASE . '/Quickform',
				_('Deze persoon kan al dibsen; het quickform is voor leden die nog niet kunnen dibsen.'));
		}

		//Maak de nieuwe objecten aan
		$dibsinfo = new DibsInfo($persoonID);

		$pas = new Pas();
		$pas->setPersoon($persoonID);

		$show_error = false;

		DibsInfoView::processQuickform($dibsinfo, $pas);
		if (Token::processNamedForm() == 'Nieuw Quickform')
		{
			if ($dibsinfo->valid('nieuw') && $pas->valid())
			{
				//Als alles klopt, sla op
				$pas->opslaan();
				$dibsinfo->opslaan();

				Page::redirectMelding(DIBSBASE . '/Quickform?ID=' . $persoonID,
					sprintf(_('%s kan nu DiBSen.'), PersoonView::naam($dibsinfo->getPersoon())));
			}
			else
			{
				$show_error = true;
				if (!$pas->valid())
				{
					Page::addMelding(_('Er waren fouten bij de pas'), 'fout');
				}
				if (!$dibsinfo->valid('nieuw'))
				{
					Page::addMelding(_('Er waren fouten bij de dibsinfo'), 'fout');
				}
			}
		}

		$page = DibsInfoView::toevoegenQuickform($dibsinfo, $show_error);
		return $this->pageResponse($page);
	}

	/**
	 * Test of het plaatje van het ColaProduct goed is upgeload.
	 *
	 * Het plaatje wordt upgeload naar $_FILES['userfile']
	 *
	 * @param ColaProduct $product Het product om een plaatje te geven.
	 *
	 * @return Response
	 */
	public function confirmUploadPlaatje(ColaProduct $product)
	{
		// Deze pagina is voor het aangeven van errors.
		$page = new HTMLPage();
		$page->start(_('Plaatje uploaden'));
		$gaTerug = new HtmlAnchor($product->url('UploadPlaatje'), _("Probeer het opnieuw"));

		$userfile = $_FILES["userfile"];

		if ($userfile == null)
		{
			$page->add(_("Niets geupload, misschien is je bestand te groot"));
			return new PageResponse($page);
		}

		//Geef alle mogelijke errors
		switch ($userfile["error"])
		{
			case UPLOAD_ERR_OK:
				break;
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				$page->add(new HtmlParagraph(_('Je bestand was te groot, je bestand mag maximaal 100KB zijn')))->add($gaTerug);
				return new PageResponse($page);
			case UPLOAD_ERR_PARTIAL:
				$page->add(new HtmlParagraph(_('Er is alleen een gedeelte geupload')))->add($gaTerug);
				return new PageResponse($page);
			case UPLOAD_ERR_NO_FILE:
				$page->add(new HtmlParagraph(_('Er is geen file geupload')))->add($gaTerug);
				return new PageResponse($page);
			case UPLOAD_ERR_CANT_WRITE:
				$page->add(new HtmlParagraph(_('Er kan niet naar de server geschreven worden')))->add($gaTerug);
				return new PageResponse($page);
			default:
				$page->add(new HtmlParagraph(_('Er ging iets mis maar ik weet zelf ook even niet wat het is')))->add($gaTerug);
				return new PageResponse($page);
		}

		//Het bestand moet een .jpg zijn.
		$image_properties = getImageSize($userfile['tmp_name']);
		if (!($image_properties[2] == IMAGETYPE_JPEG))
		{
			$page->add(new HtmlParagraph(_("Verkeerd bestandsformaat (" . $userfile['type'] . "). Het moet een jpg zijn.")))->add($gaTerug);
			return new PageResponse($page);
		}
		//corrigeer de naam naar artikelid.jpg
		$userfile["name"] = $product->getArtikelID() . '.jpg';
		//haal het vfs tevoorschijn voor de dibstabletplaatjes:
		$vfs = hrefToEntry('/Leden/Dibs/ColaProductPlaatjes');
		//maak een md5 hash voor de bestandsnaam:
		$md5 = md5_file($userfile["tmp_name"]);

		global $ALLOWED_FILE_TYPES;
		$type = $ALLOWED_FILE_TYPES['jpg'];

		//verwijder de entry als deze al bestaat:
		if ($vfs->hasChild($userfile["name"]))
		{
			$del = $vfs->getChild($userfile["name"]);
			vfs::del($del);
		}

		// Debug-bestanden worden opgeslagen met een .debug extensie.
		if (!move_uploaded_file($userfile["tmp_name"],
			"/srv/http/www/benamite_files/$md5" . (DEBUG ? '.debug' : '')))
		{
			user_error('File upload failed...!', E_USER_ERROR);
		}

		//voeg de file toe:
		$file = vfs::add(
			array('name' => $userfile["name"]
			, 'type' => ENTRYUPLOADED
			, 'parent' => $vfs->getId()
			, 'visible' => false
			, 'displayName' => $userfile["name"]
			, 'special' => "$md5 $type"
			));

		// Alles is goed gegaan, dus we gaan de RIP je executie van de page op veilig zetten.
		spaceFinished();
		return Page::responseRedirectMelding('/Leden/Dibs/Colaproducten/' . $product->getArtikelId(), _('Uploaden gelukt!'), 'succes');
	}

	/**
	 * @return PageResponse
	 * @throws HTTPStatusException
	 */
	public function productenOverzicht()
	{
		// verwerk get-data
		$maand = (int)$this->getRequestParam('maand', date('m'));
		$jaar = (int)$this->getRequestParam('jaar', date('Y'));

		if ($maand <= 0 || $maand > 12 || $jaar < 0 || $jaar >= 10000)
		{
			$this->responseUitStatus(501);
		}

		// laad de transacties
		$voorraden = VoorraadVerzameling::geefAlleColaproductVoorraden();
		return $this->pageResponse(VoorraadVerzamelingView::overzicht($voorraden, $maand, $jaar));
	}

	/**
	 * Poep een CSV-bestand uit met het overzicht van alle colaproductverkopen.
	 *
	 * TODO: poept uit naar stdout en doet dan exit()
	 *
	 * @throws HTTPStatusException
	 */
	public function productenOverzichtCSV()
	{
		// verwerk get-data
		$maand = (int)$this->getRequestParam('maand', date('m'));
		$jaar = (int)$this->getRequestParam('jaar', date('Y'));

		if (1 > $maand || $maand > 12 || DIBS_STARTJAAR > $jaar)
		{
			$this->responseUitStatus(501);
		}

		$maandnaam = date('F', mktime(0, 0, 0, $maand));

		$van = new DateTimeLocale("$jaar-$maand-01");
		$tot = clone $van;
		$tot->add(new DateInterval("P1M"));
		// laad de transacties
		$voorraden = VoorraadVerzameling::geefAlleColaproductVoorraden();
		$mutaties = $voorraden->mutatiesPerVoorraad($van, $tot);

		//Doe de csvtsjak
		header("Content-Disposition: attachment; filename=verkochtecolaproducten-$maandnaam-$jaar.csv");
		header("Content-type: text/csv; encoding=utf-8");

		$csvarray = [];
		foreach ($mutaties as $voorraadID => $voorraadmutaties)
		{
			$artikel = Voorraad::geef($voorraadID)->getArtikel();
			$csvarray[] = ['naam' => ArtikelView::waardeNaam($artikel),
				'aantal' => -$voorraadmutaties->totaal()];
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		if (count($csvarray) > 0)
		{
			fputcsv($output, array_keys($csvarray[0]));
		}
		else
		{
			fwrite($output, _("Geen data."));
		}

		//Print alle inhoud voor de CSV
		foreach ($csvarray as $t)
		{
			fputcsv($output, $t);
		}

		fclose($output);
		exit();
	}

	/**
	 * @return PageResponse
	 */
	public function statistieken()
	{
		$page = DibsInfoView::statistieken();
		return $this->pageResponse($page);
	}

	public function limieten()
	{
		$omloop = DibsInfoVerzameling::getTotaalKudos();
		//hier moeten de limieten uit de registry gehaald worden
		$aeslimiet = (int)Register::getValue('aesLimiet');
		$aeskudos = (int)Register::getValue('aesKudos');
		$lidlimiet = (int)Register::getValue('lidLimiet');
		$page = Page::getInstance()
			->start()
			->add(new HtmlParagraph(new HtmlStrong(sprintf(_('Kudos in omloop: %d/%d (A&ndash;Eskwadraatlimiet; %s)'), $omloop, $aeslimiet, new HtmlAnchor(DIBSBASE . '/Limieten/Verander?id=aesLimiet', _('verhoog'))))
				. new HtmlBreak()
				. new HtmlStrong(sprintf(_('Kudos in voorraad: %s'), $aeskudos))))
			->add(new HtmlParagraph(_('De A&ndash;Eskwadraatlimiet is het maximale aantal kudos dat leden in totaal in bezit mogen hebben. Het aantal kudos in voorraad is het aantal dat nu nog aan leden verkocht kan worden.')))
			->add(new HtmlHR())
			->add(new HtmlParagraph(new HtmlStrong(sprintf(_('Lidlimiet: %d (%s)'), $lidlimiet, new HtmlAnchor(DIBSBASE . '/Limieten/Verander?id=lidLimiet', _('Verhoog'))))))
			->add(new HtmlParagraph(_('De lidlimiet is het maximale aantal kudos dat een enkel lid in bezit mag hebben.')));

		return $this->pageResponse($page);
	}

	public function veranderLimieten()
	{
		$id = $this->getRequestParam('id', null);
		$aeslimiet = (int)Register::getValue('aesLimiet');
		$aeskudos = (int)Register::getValue('aesKudos');
		$lidlimiet = (int)Register::getValue('lidLimiet');

		//Haal de data op als het formulier is ingevuld en kijk of het klopt
		$lim = $this->getRequestParam($id, 0);
		if (Token::processNamedForm() == $id)
		{
			if ($id == 'aesLimiet')
			{
				$limiet = $aeslimiet;
			}
			else
			{
				$limiet = $lidlimiet;
			}

			if ($lim <= $limiet)
			{
				Page::addMelding(_("De nieuwe limiet is zo kleiner dan de oude! Dat kan natuurlijk niet!"));
			}
			else
			{
				if ($id == 'aesLimiet')
				{
					$diff = $lim - $aeslimiet;
					Register::updateRegister('aesLimiet', $lim);
					Register::updateRegister('aesKudos', ($aeskudos + $diff));
				}
				elseif ($id == 'lidLimiet')
				{
					Register::updateRegister('lidLimiet', $lim);
				}
				else
				{
					Page::redirectMelding(DIBSBASE . '/Limieten', _('Er was een fout.'));
				}
				Page::redirectMelding(DIBSBASE . '/Limieten', _('Gelukt'));
			}
		}

		//Maak het formulier
		$form = HtmlForm::named($id);
		$idlim = $id;
		$form->setAttribute('onsubmit', "var data = document.getElementById('$idlim').value;
			return confirm('Weet je zeker dat je de $idlim wilt zetten op ' + data + '?')");

		$form->add($div = new HtmlDiv());
		$div->add($table = new HtmlTable());
		$table->add($tbody = new HtmlTableBody());

		$row = new HtmlTableRow(new HtmlTableDataCell("Huidige $id:"));
		$row->add(new HtmlTableDataCell(($id == 'aesLimiet') ? $aeslimiet : $lidlimiet));
		$tbody->add($row);

		if ($id == 'aesLimiet')
		{
			$row = new HtmlTableRow(new HtmlTableDataCell(_('Huidige kudos in voorraad:')));
			$row->add(new HtmlTableDataCell($aeskudos));
			$tbody->add($row);
		}

		$row = new HtmlTableRow(new HtmlTableDataCell(_('Nieuwe limiet') . ':'));
		$row->add(new HtmlTableDataCell(new HtmlInput($id, null, $id)));
		$tbody->add($row);

		$form->add(HtmlInput::makeSubmitButton(_('Verhoog')));

		//Maak de pagina aan
		$page = Page::getInstance()
			->start($id)
			->add(new HtmlSpan(_('Let op: je kan de limiet niet meer terug veranderen als je hem eenmaal gewijzigd hebt!'), 'text-danger strongtext'))
			->add(new HtmlBreak())
			->add(new HtmlBreak())
			->add($form);

		return $this->pageResponse($page);
	}

	/**
	 * @site {/Leden/Dibs/OudeDibs}
	 */

	static public function verwijderOudeKudos()
	{
		 	
		if(Token::processNamedForm() == 'oudeKudosVerwijderen')
		{	
			$tijd = getdate();
			$tot = new DateTimeLocale("$tijd[year]-$tijd[mon]-$tijd[mday]");
			$tot->sub(new DateInterval('P5Y'));
			$laatsteTransacties = DibsTransactieVerzameling::getLaatsteTransacties($tot);

			foreach ($laatsteTransacties as $transactie) {
				$info = DibsInfo::geef($transactie->getContactContactID());
				if ($info->getKudos() > 0) {
					$persoon = $transactie->getPersoon();
					$kudos = $info->getKudos();
					$uitleg = "Vijf jaar niet gebruikt";
					$t = new DibsTransactie('WWW', 'VERKOOP', $persoon, (1 * $kudos) / EURO_TO_KUDOS, $uitleg, new DateTimeLocale(), 'WWW');
					$info->nieuweTransactie($t);
					$info->opslaan();
					$waarde = $kudos + Register::getValue('oudeDibs');
					Register::updateRegister('oudeDibs', $waarde);
				}
			}

			return Page::responseRedirectMelding('/Leden/Dibs/Statistieken', _("De inactieve Dibs zijn overgezet"));
			
		}
		return DibsInfoView::oudeKudosVerwijderen();
	}
}

