<?php
/**
 * /Leden/lid_id/WijzigWachtwoord       -> ::wijzig()
 * /Service/Intern/Logindata            -> ::requestLogin
 */
abstract class Wachtwoord_Controller
{
	/**
	 *	 Wachtwoord wijzigen
	 */
	public static function wijzig()
	{
		$ww = Wachtwoord::geefOfNieuw(vfsVarEntryName());

		if(!$ww->magWijzigen())
			spaceHTTP(403);

		WachtwoordView::wijzigForm($ww, static::processWijzigForm($ww));
	}
	private static function processWijzigForm(Wachtwoord $ww)
	{
		global $auth;
		//Check of de gebruiker deze actie wel initieert
		if(Token::processNamedForm() != "WachtwoordWijzig")
			return array();
		
		$login = tryPar('loginnaam', $ww->getLogin());
		$pass1 = tryPar('password');
		$pass2 = tryPar('password2');

		//Geen wijzigingen?
		if($login == $ww->getLogin() && empty($pass1) && empty($pass2))
		{
			Page::addMelding(_("Er is niets gewijzigd"), 'fout');
			return array();
		}

		if(!$verify = trypar('verify', false))
			return array(_("Geen verificatiewachtwoord opgegeven."));

		$wwcheck = Wachtwoord::geef($auth->getLidnr());
		if(!$wwcheck->authenticate($verify))
			return array(_("Verificatiewachtwoord fout."));

		return static::processWijzigWachtwoord($ww);
	}
	private static function processWijzigWachtwoord(Wachtwoord $ww, $viaMagic = false)
	{
		$login = tryPar('loginnaam', $ww->getLogin());
		$pass1 = tryPar('password');
		$pass2 = tryPar('password2');

		if($viaMagic && !$pass1)
			return array(_("Je wachtwoord mag niet leeg zijn."));
		if($pass1 != $pass2)
			return array(_("Er zijn twee verschillende wachtwoorden ingevoerd."));

		$ww->setLogin($login);

		// als je alleen je login wijzigt doen we niets met het wachtwoord
		if(!empty($pass1))
			$ww->wijzigWachtwoord($pass1, $viaMagic);

		if(!$ww->valid())
			return $ww->getErrors();

		$ww->removeMagic();
		$ww->opslaan();

		Page::addMelding(_("Je gegevens zijn gewijzigd"));

		return array();
	}

	public static function requestLogin_menu()
	{
		global $auth;
		if(hasAuth('ingelogd')) {
			return array( 'url' => '/Leden/'.$auth->getLidnr().'/WijzigWachtwoord'
						, 'displayName' => _('Wachtwoord wijzigen')
						);
		}

		return array( 'url' => '/Service/Intern/Logindata'
					, 'displayName' => _('Login aanvragen')
					);
	}

	/**
	 *	 Login aanvragen / vergeten.
	 */
	public static function requestLogin_content($rest)
	{
		global $BESTUUR, $auth;

		if(!urlIsEmpty($rest))
			spaceHTTP(404);

		if(hasAuth('ingelogd'))	// eej...  je bent ingelogd!
			spaceRedirect('/Leden/'.$auth->getLidnr().'/WijzigWachtwoord');

		$lid = NULL;
		$error = NULL;
		$melding = NULL;

		$state = Token::processNamedForm();
		switch($state) {
		case 'ident':		// er is identificatie ingevuld voor de aanvraag
			$voorwie = trim(tryPar('voorwie'));
			if(empty($voorwie)) break;

			$leden = static::bepaalLid($voorwie);
			if ($leden->aantal() === 2) {
				//Zorg dat jan en allemaal niet zonder moeite alle e-mailadressen kan plunderen en mensen kan spammen
				sleep(2);
				$error = _("Zoeken mislukt, aangezien er niet &eacute&eacuten lid hiermee geidentificeerd kan worden.
							Mail de Secretaris om te vragen wat er aan de hand is:\n") . $BESTUUR['secretaris']->getEmailAdres();
				break;
			}
			if ($leden->aantal() === 0 || !in_array($leden->first()->getLidToestand(), array('LID','OUDLID','BAL','LIDVANVERDIENSTE','ERELID'))) {
				//Zorg dat jan en allemaal niet zonder moeite alle e-mailadressen kan plunderen en mensen kan spammen
				sleep(2);
				$error = _("Zoeken mislukt, geef het correcte studentnummer op waarmee je bij ons bekend bent als (oud-)lid,
							of mail de Secretaris:\n") . $BESTUUR['secretaris']->getEmailAdres();
				break;
			}

			$lid = $leden->first();
			$ww = Wachtwoord::geefOfNieuw($lid->getContactID());
			if(!$ww->sendMagicMail()) {
				$error = _("Helaas, er is geen e-mailadres bekend van dit (oud-)lid.
							Kom even langs op de kamer, daar kan iemand jouw
							e-mailadres toevoegen.");
				break;
			}
			$melding = sprintf(
				_("Er is je nu een e-mailtje gestuurd naar %s met daarin een code.")
				, '***'.substr($lid->getEmail(), 3));
			break;
		case 'auth':		// er is een magische code ingevuld
		case 'edit':		// wijzigen van het wachtwoord mbv magic
			$magic = trim(tryPar('magic'));
			if(!$magic) {
				$error = _("Je bent vergeten de code in te vullen.");
				$state = ($state == 'auth' ? 'ident' : $state);
				break;
			}

			$ww = Wachtwoord::geefMagic($magic);
			if(!$ww) {
				$error = _("Helaas, de code die je opgaf is niet correct of
							verlopen.  Vraag opnieuw een login aan, of mail de
							Secretaris.");
				$state = ($state == 'auth' ? 'ident' : $state);
				break;
			}

			if ($auth->isGodAchtig($ww->getPersoonContactID()) || $auth->isBestuur($ww->getPersoonContactID()))
			{
				$error = _("Bestuursleden of goden mogen niet via mail een nieuw wachtwoord aanvragen.");
				$state = 'ident';
				break;
			}

			if($state != 'edit') {		// 'auth' succesvol doorlopen
				$state = 'edit';		// door naar 'edit'
				break;
			}

			$error = self::processWijzigWachtwoord($ww, true); //true geeft aan dat een niet ingelogd lid wachtwoorden mag wijzigen
			if(empty($error)) {
				$state = 'done';
			}
			break;
		case NULL:			// geen post data
		default:			// huh wat is dat voor form??
			$state = 'ident';
		}

		WachtwoordView::requestLogin($state, $melding, $error);
	}


	/**
	 * Geef een LidVerzameling van iedereen die de gegeven identificerende waarde heeft.
	 *
	 * De identificatie kan zijn:
	 *  * een 7-cijferig getal (of een f voorop): studentnummer
	 *  * een ander getal: lidnummer
	 *  * een string met een @ erin: e-mailadres
	 *
	 * Het kan voorkomen dat er meerdere leden overeenkomen met dezelfde waarde, dus we returnen een LidVerzameling.
	 *
	 * @param string $identificatie Een (vrijwel) uniek identificerende waarde.
	 * @return LidVerzameling Alle leden die overeenkomen met de gegeven identificerende waarde.
	 */
	public static function bepaalLid($identificatie)
	{
		$identificatie = trim($identificatie);
		$leden = new LidVerzameling();
		if (!empty($identificatie)) {
			if (preg_match("/^[f0-9][0-9]{6}$/i", $identificatie)) {
				// studentnummer
				return Lid::geefLedenMetStudentnummer($identificatie);
			}

			if (preg_match("/^[0-9]+$/", $identificatie)) {
				// lidnummer
				$lid = Lid::geef((int) $identificatie);
				if ($lid) {
					$leden->voegtoe($lid);
				}
			} else if(strpos($identificatie, "@")) {
				// heeft een @ && [0]!=@
				$lid = Lid::zoekOpEmail($identificatie);
				if ($lid && !($lid instanceof Lid)) {
					$leden->voegtoe($lid);
				}
			}
		}
		return $leden;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
