<?php
/**
 * /Leden/Organisatie		-> (overzicht) basis pagina
 *  /Nieuw					-> (nieuw) maak een nieuwe organisatie
 *  /contact_id				-> (details) toont de profiel pagina van een organisatie
 *   /Wijzig				-> (wijzig) wijzig het geselecteerde organisatie
 *   /Verwijder				-> (verwijder) verwijder een organisatie
 */

abstract class Organisatie_Controller
{
	static public function voorLeden()
	{
		$body = new HtmlElementCollection();

		$link = new HtmlAnchor('http://www.uu.nl/NL/Informatie/studenten/studentenleven/overorganisaties/Pages/faculteitenstudie.aspx', _("verenigingen binnen Utrecht"));

		$body->add(new HtmlHeader(1, _("Contacten")))
			 ->add(new HtmlParagraph(_("A–Eskwadraat onderhoudt contacten met allerlei ") . $link . '.'))
			 ->add(new HtmlParagraph(_("De drie landelijke organisaties waar A–Eskwadraat lid van is zijn:")))
			 ->add($list = new HtmlList());

		$list->add(new HtmlListItem(new HtmlAnchor("http://www.gewis.nl/wiso/", "WISO") . _(" voor Wiskunde en Informatica")))
			 ->add(new HtmlListItem(new HtmlAnchor("http://www.verenigingspin.nl", "SPIN") . _(" voor Natuurkunde")))
			 ->add(new HtmlListItem(new HtmlAnchor("http://www.snic.nl/", "SNiC") . _(", de stichting voor het nationale informaticacongres")));

		$nl = OrganisatieVerzameling::byType("ZUS_NL");
		$utrecht = OrganisatieVerzameling::byType("ZUS_UU");

		$body->add(new HtmlHeader(3, _("Zusterverenigingen in Nederland")));
		$body->add(OrganisatieVerzamelingView::simpel($nl));

		$body->add(new HtmlHeader(3, _("Utrechtse Verenigingen")));
		$body->add(OrganisatieVerzamelingView::simpel($utrecht));

		$page = Page::getInstance()->start(_('Zusjes en contacten'))
			->add($body)
			->end();
	}

	static public function overzicht()
	{
		//Vul de organisatie (sub)set
		if(trypar('type')){
			$type = trypar('type');

			//We willen geen bedrijven op deze plek, dat is iets voor spookweb.
			if($type == "Alle"){
				$orgs = OrganisatieVerzameling::byNotType("BEDRIJF");
			} else {
				$orgs = OrganisatieVerzameling::byType($type);
			}
		}

		if(trypar('resulttype')){
			$resulttype = trypar('resulttype');
			switch ($resulttype) {
				case 'csv':
					header("Content-Disposition: attachment; filename=organisaties_$type.csv");
					header("Content-type: text/csv; encoding=utf-8");

					echo OrganisatieVerzamelingView::csv($orgs);
					die;
					break;
				case 'latex':
					header("Content-Disposition: attachment; filename=organisaties_$type.tex");
					header("Content-type: text/tex; encoding=utf-8");

					echo OrganisatieVerzamelingView::latex($orgs);
					die;
					break;
				default:
					break;
			} 
		}

		$page = Page::getInstance();

		$page->start(); 

		$infoDiv = new HtmlDiv();

		$infoDiv->add(new HtmlParagraph(_('Op deze pagina kan je organisatie waar A–Eskwadraat contact mee heeft zoeken, bekijken en beheren.')
								. new HtmlBreak(1) . _('Op termijn komen hier ook nog bedrijven bij')))
				->add(new HtmlList(false, array(
						new HtmlAnchor('/Leden/Organisatie/Nieuw',_('Nieuwe organisatie invoeren'))
						)))
				->add(new HtmlParagraph(_('Hieronder kan je een verzameling organisaties zien, klik er op voor meer info of selecteer een specifiekere set.')))
				->add(new HtmlParagraph())
				->add(new HtmlList('', array(
						new HtmlListItem(new htmlAnchor('?type=ZUS_UU',_('Zusjes Utrecht'))),
						new HtmlListItem(new htmlAnchor('?type=ZUS_NL',_('Zusjes Nederland'))),
						new HtmlListItem(new htmlAnchor('?type=UU',_('Mensen en onderdelen van de Universiteit Utrecht'))),
						new HtmlListItem(new htmlAnchor('?type=VAKGROEP',_('Vakgroepen'))),
						new HtmlListItem(new htmlAnchor('?type=OG_MED',_('Medezeggenschap en overleggroepen'))),
						new HtmlListItem(new htmlAnchor('?type=OVERIG',_('Overige'))),
						new HtmlListItem(new htmlAnchor('?type=Alle',_('Allemaal')) . " (excl. bedrijven)")
						)))
				->add(new HtmlParagraph(sprintf(_('Heb je vragen? Mail de webcie of maak een %s aan!'),
								new HtmlAnchor('/Service/Bugs/Nieuw',_('bug')))));

		$page->add($infoDiv);

		//Als er een subset is laat deze dan zien.
		if(@$orgs) {
			$page->add(new HtmlParagraph(_('Ook te krijgen in ') . new HtmlAnchor("?type=$type&resulttype=csv",'csv') . 
						' en ' . new HtmlAnchor("?type=$type&resulttype=latex",'latex') .'.'));
			$page->add(OrganisatieVerzamelingView::HtmlList($orgs));	
		}

		$page->end();
	}

	/**
	 * Returneert een array met het geselecteerd organisatie
	 * $args = array(0 => 'org_id')
	 */
	static public function organisatieEntry ($args)
	{
		$org = Organisatie::geef($args[0]);
		if (!$org) return false;

		return array( 'name' => $org->geefID()
					, 'displayName' => $org->getNaam()
					, 'access' => $org->magBekijken());
	}

	/**
	 * Toon de detailspagina van een organisatie
	 */
	static public function details ()
	{
		/** Laad de gekozen organisatie **/
		$orgid = (int) vfsVarEntryName();
		$org = Organisatie::geef($orgid);
		
		if(!$org->magBekijken())
			spaceHTTP(403);

		/** Ga naar de view **/
		OrganisatieView::details($org);
	}

	/**
	 * Maak een nieuwe organisatie aan
	 */
	static public function nieuw ()
	{
		self::wijzig(new Organisatie());
	}

	/**
	 * Wijzig een organisatie
	 */
	static public function wijzig($org = null)
	{
		if(is_null($org)){
			$orgid = (int) vfsVarEntryName();
			$org = Organisatie::geef($orgid);
		}
		if (!$org->magWijzigen())
			spaceHttp(403);

		/** Buffer voor de output **/
		$form = true;
		$msg = null;

		/** Verwerk formulier-data **/

		if (Token::processNamedForm() == 'OrganisatieWijzig')
		{
			OrganisatieView::processWijzigForm($org);
			if ($org->valid())
			{
				$org->opslaan();
				Page::redirectMelding($org->url(), _("De gegevens zijn opgeslagen!"));
			}
			else
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
		}

		/** Ga naar de view **/
		return new PageResponse(OrganisatieView::wijzig($org, $form, $msg));
	}


	/**
	 * Verwijder een organisatie
	 */

	static public function verwijder ()
	{
		/** Verwerk get-data **/
		$orgid = (int) vfsVarEntryName();
		$org = Organisatie::geef($orgid);

		/** Buffer voor berichten **/
		$msg = null;

		/** Verwerk formulier-data **/
		switch(Token::processNamedForm()) {
		case 'OrganisatieVerwijder':
			foreach(ContactPersoonVerzameling::metConstraints($org) as $cpers) //Gooi alle aanverwanten dingen eerst weg.
			{
				$pers = $cpers->getPersoon();
				$cpers->verwijderen();
				$pers->verwijderen();
			}
			foreach(ContactAdresVerzameling::vanContact($org) as $adr)
			{
				$adr->verwijderen();
			}
			foreach(ContactTelnrVerzameling::vanContact($org) as $telnr)
			{
				$telnr->verwijderen();
			}
			$org->verwijderen();
			Page::redirectMelding('/Leden/organisatie/', _("De organisatie met zijn contactpersonen is nu verwijderd!"));
			break;

		default:
			break;
		}
			/** Ga naar de view **/
			OrganisatieView::verwijder($org, $msg);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
