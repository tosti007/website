<?php

abstract class Dili_Controller {
	/**
	 * @brief Toon de pagina waar de DiLI gestart kan worden.
	 *
	 * @site{/Leden/Intro/Dili/index.html}
	 */
	public static function inschrijfmodus() {
		global $session;

		$page = Page::getInstance()->start("Digitale ledeninschrijving");

		if((DEBUG || DEMO) && hasAuth('god') && tryPar('degradeer', '0') == '1')
		{
			$lidanoniem = Lid::geef(111);
			$lidanoniem->setLidToestand('BIJNALID');
			$lidanoniem->opslaan();
		}

		if (hasAuth('bestuur')) {
			$page->add(HtmlAnchor::button("../", HtmlSpan::fa('arrow-left', 'Terug')->makeHtml() . "&nbsp;Terug naar de introsmurf"));

			$page->add(new HtmlParagraph("Hier kun je de digitale ledenadministratie openen of sluiten."));
			$page->add(new HtmlParagraph("Als de inschrijving open is, kunnen bijna-leden lid worden."));

			$page->add($panel = new HtmlDiv(null, 'panel panel-default'));
			$panel->add(new HtmlDiv("Activatiecodes", 'panel-heading'));
			$panel->add($panelBody = new HtmlDiv(null, 'panel-body'));
			$panelBody->add(new HtmlParagraph("Bijna-leden hebben eerst een activatiecode nodig om lid te worden. Deze gebruiken we om ze uniek te kunnen identificeren (ze hebben natuurlijk nog geen loginnaam)."));
			$panelBody->add($actions = new HtmlDiv(null, 'btn-group'));
			$actions->setCssStyle('margin: .5em;');
			$actions->add(HtmlAnchor::button("Codes", "Beheer activatiecodes"));
			$actions->add(HtmlAnchor::button("CodesPrintbaar", "Print activatiecodes"));
			$panelBody->add(new HtmlParagraph("Als het goed is, krijgen alle smurfjes een activatiecode van hun mentormama of mentorpapa of mentoroverig. Het is de taak van de Intro om alle mentoren de lijst te geven."));

			if (static::inschrijvingOpen()) {
				$statusTekst = _("De inschrijving is open.");
				$url = DILI_BASE . "InschrijvingSluiten";
				$buttonTekst = _("Sluit de inschrijving!");
			} else {
				$statusTekst = _("De inschrijving is gesloten.");
				$url = DILI_BASE . "InschrijvingOpenen";
				$buttonTekst = _("Open de inschrijving!");
			}

			if((DEBUG || DEMO) && hasAuth('god')) {
				$page->add($button = HtmlAnchor::button("?degradeer=1", "Degradeer Lid Anoniem"));
				$button->setCssStyle('margin: 0 1em 1.5em 1em;');
			}

			$page->add($panel = new HtmlDiv(null, 'panel panel-default'));
			$panel->add(new HtmlDiv("Inschrijfmodus", 'panel-heading'));
			$panel->add($panelBody = new HtmlDiv(null, 'panel-body'));

			$panelBody->add(new HtmlParagraph($statusTekst));
			$panelBody->add($form = new HtmlForm('POST', $url));
			$form->setCssStyle('margin-top: .5em;');

			// Er zijn best wel wat betaalmethoden die niet altijd op hetzelfde moment kunnen,
			// dus we laten het bestuur kiezen wat ze willen toestaan.
			if (!static::inschrijvingOpen()) {
				$form->add(new HtmlParagraph(_('Hoe mogen bijnaleden betalen voor hun lidmaatschap?')));
				foreach (LidmaatschapsBetaling::enumsSoort() as $soort)
				{
					$labelSoort = LidmaatschapsBetalingView::labelenumSoort($soort);
					$form->add(new HtmlDiv(HtmlInput::makeCheckbox("betalen[{$soort}]", TRUE, null, null,
						$labelSoort)));
				}
			}

			$form->add(HtmlInput::makeSubmitButton($buttonTekst));

			$page->add($panel = new HtmlDiv(null, 'panel panel-default'));
			$panel->add(new HtmlDiv("Betalingen", 'panel-heading'));
			$panel->add($panelBody = new HtmlDiv(null, 'panel-body'));
			$panelBody->add(new HtmlParagraph(_("Afhankelijk van de vinkjes hierboven, worden inschrijvende leden gevraagd om een machtiging/iDEALtransactie. Voor alle ingeschreven leden houden we bij hoe de inschrijving is betaald, wat je hieronder kan bekijken.")));
			$panelBody->add($actions = new HtmlDiv(null, 'btn-group'));
			$actions->setCssStyle('margin: .5em;');
			$actions->add(HtmlAnchor::button("Betalingen", _("Betaaloverzicht")));
			// TODO: maak dit iets minder hardcoded...
			$actions->add(HtmlAnchor::button(
				"/Onderwijs/Boekweb/Artikel/150/Voorraad/118/",
				_("Check AUB ook eventjes dat er lidmaatschappen beschikbaar zijn")
			));
			$panelBody->add(new HtmlParagraph(_("De iDEAL-transactie werkt op basis van een artikel in Boekenweb. Dat artikel zou niet op moeten raken in de komende tijd, maar check dit altijd voor de inschrijvingen!")));
		} else if (static::inschrijvingOpen()) {
			$lid = Lid::getIngelogdLid();

			if (!is_null($lid) && !static::lidInschrijfbaar($lid)) {
				$msg =
					_("Je bent al lid! Als je denkt dat dit niet klopt, spreek"
					 . " dan een intro- of bestuurslid aan.");
				$page->add(HtmlDiv::makeMelding($msg, 'fout'));
			} else {
				// TODO: zorg dat je een inlogpagine krijgt als je geen bestuur bent.
				// Stop dit maar wel in een aparte viewfunctie, anders wordt deze
				// methode wel erg lang.
			}
		}

		$page->end();
	}

	/**
	 * @brief Open de inschrijving voor bijna-leden.
	 *
	 * @site{/Leden/Intro/Dili/InschrijvingOpenen}
	 */
	public static function inschrijvingOpenen() {
		global $request;

		requireAuth('bestuur');

		if ($request->getMethod() != 'POST') {
			// We staan geen andere methods dan POST toe.
			spaceHttp(405);
		}

		Register::updateRegister('inschrijvingOpen', true);

		// Update alle betalingsmethoden.
		foreach (LidmaatschapsBetaling::enumsSoort() as $soort)
		{
			$toestand = (bool)tryPar("betalen[{$soort}]");
			Register::updateRegister('inschrijvingBetalen_' . $soort, $toestand);
		}

		Page::redirectMelding(DILI_BASE, _("De inschrijving is succesvol geopend!"));
	}

	/**
	 * @brief Zorg ervoor dat de inschrijving gesloten is.
	 *
	 * @site{/Leden/Intro/Dili/InschrijvingSluiten}
	 */
	public static function inschrijvingSluiten() {
		// TODO: het zou mooi zijn als dit een functie was met parameter: toestand
		// dan samenvoegen met inschrijvingOpenen
		global $request;

		requireAuth('bestuur');

		if ($request->getMethod() != 'POST') {
			// We staan geen andere methods dan POST toe.
			spaceHttp(405);
		}

		Register::updateRegister('inschrijvingOpen', false);
		Page::redirectMelding(DILI_BASE, _("De inschrijving is succesvol gesloten."));
	}

	/**
	 * @brief Pagina om bijnaleden + activatiecodes te tonen.
	 *
	 * @site{/Leden/Intro/Dili/Codes}
	 */
	public static function activatiecodes() {
		global $request;

		requireAuth('intro');

		$codelozen = ActivatieCodeVerzameling::bijnaledenZonderCode();

		if ($request->getMethod() == 'POST') {
			$successen = 0;
			foreach ($codelozen as $bijnalid) {
				$code = ActivatieCode::genereer($bijnalid);
				$code->opslaan();
				$successen++;
			}

			Page::addMelding(sprintf(_("Er zijn %d activatiecodes aangemaakt."),
				$successen
			));
		}

		$page = Page::getInstance()->start();

		$statusTekst = sprintf(_("Op dit moment zijn er %d bijnaleden zonder activatiecode."),
			$codelozen->aantal()
		);

		$page->add(new HtmlParagraph($statusTekst));
		$page->add($form = new HtmlForm('POST'));
		$form->add($actions = new HtmlDiv(null, 'btn-group'));
		$actions->add(HtmlAnchor::button("./", HtmlSpan::fa('arrow-left', 'Terug')->makeHtml() . "&nbsp;Terug naar het DiLi huis"));
		$actions->add(HtmlInput::makeSubmitButton(_("Genereer codes voor de codelozen! (Dit duurt een tijdje, geduld AUB!)")));
		$actions->add(HtmlAnchor::button(DILI_BASE . "CodesPrintbaar", _("Print die codes!")));

		$page->add(ActivatieCodeVerzamelingView::tabel());

		$page->end();
	}

	/**
	 * @brief Geef een printbaar overzicht van de activatiecodes.
	 *
	 * Hier geven we een pagina aan codes per mentorgroepje,
	 * zodat de mentoren ze kunnen uitdelen aan hun kindjes.
	 *
	 * We gaan ervan uit dat (bijna) iedereen een code heeft,
	 * en de intro eventuele buiten-de-bootvallers wel kan fixen.
	 *
	 * @site{/Leden/Intro/Dili/CodesPrintbaar}
	 */
	public static function codesPrintbaar() {
		$css = <<<EOF
.groep {
	page-break-after: always;
	column-count: 2;
	column-gap: 0;
}

.flex-container {
	display: flex;
	break-inside: avoid-column;
}

.lid {
	width: 100%;
	padding: 1em;
}

/* maak een mooie collapsed border */
.groep, .lid { border: 1px solid #000; }
.groep { border-width: 1px 0 0 1px; }
.lid { border-width: 0 1px 1px 0; }

.lid :not(:last-child) {
	page-break-after: never;
}

.lid code {
	font-size: 1.5em;
}
EOF;
		
		$page = Page::getInstance('cleanhtml');
		$page->addHeadCSSInline($css);
		$page->start();

		foreach (IntroGroepVerzameling::groepenVanJaar(date('Y')) as $mentorgroep) {
			$page->add(new HtmlHeader(1, sprintf(_("%s[VOC: aesnaam]activatiecode"),
				aesnaam())));
			$page->add(new HtmlHeader(2, _("Mentorgroep ") . IntroGroepView::waardeNaam($mentorgroep)));
			$page->add(new HtmlParagraph(sprintf(
				_("Op deze pagina staan de activatiecodes voor het mentorgroepje."
				. " Deze code gebruik je om lid te worden van %s[VOC:aesnaam],"
				. " dus let op dat je de juiste code hebt. Code kwijt? Vraag de intro!"),
				aesnaam()
			)));
			$page->add($groepDiv = new HtmlDiv(null, 'groep'));
			foreach ($mentorgroep->kindjes() as $kindje) {
				$groepDiv->add($flexDiv = new HtmlDiv(null, 'flex-container'));
				$flexDiv->add($lidDiv = new HtmlDiv(null, 'lid'));

				$lidDiv->add(new HtmlSpan(
					sprintf(
						_("%s[VOC: naam mentorkindje]"), LidView::waardeNaam($kindje)
					)
				));
				$lidDiv->add(new HtmlBreak());

				$lidDiv->add(new HtmlSpan(_("Lidnummer: ")));
				$lidDiv->add(new HtmlCode($kindje->geefId()));
				$lidDiv->add(new HtmlBreak());

				$lidDiv->add(new HtmlSpan(_("Activatiecode: ")));
				// Verkrijg een code, of geef aan dat deze niet bestaat.
				$code = ActivatieCode::fromLid($kindje);
				if (is_null($code)) {
					$waardeCode = "(geen code beschikbaar)";
				} else {
					$waardeCode = ActivatieCodeView::waardeCode($code);
				}
				$lidDiv->add(new HtmlCode($waardeCode));
			}
		}

		$page->end();
	}

	/**
	 * @brief Pagina om betalingen te kunnen zien.
	 *
	 * @site{/Leden/Intro/Dili/Betalingen}
	 */
	public static function betalingen() {
		global $request;

		requireAuth('bestuur');

		// we checken in het begin of we een csv-bestand moeten uitpoepen,
		// want anders zijn we al een pagina in elkaar aan het zetten
		$csvEditie = (bool)tryPar("submitCSV", false);
		if ($csvEditie) {
			header("Content-Disposition: attachment; filename=machtigingen.csv");
			header("Content-type: text/csv; encoding=utf-8");
			$file = fopen('php://output', 'r+');
			LidmaatschapsBetalingVerzamelingView::toCSV($file);
			return spaceFinished();
		}

		$page = Page::getInstance()->start();

		$page->add($form = new HtmlForm('POST'));
		$form->add($actions = new HtmlDiv(null, 'btn-group'));
		$actions->add(HtmlAnchor::button("./", HtmlSpan::fa('arrow-left', 'Terug')->makeHtml() . "&nbsp;Terug naar het DiLi huis"));
		// om dit uit te poepen in csv-vorm
		$form->add(HtmlAnchor::button("?submitCSV=1", _("Poep een CSV-bestand uit"), "submitCSV"));
		$page->add(LidmaatschapsBetalingVerzamelingView::tabel());

		$page->end();
	}

	/**
	 * @brief Geef een formulier om (bijna)-leden te zoeken voor de DiLI.
	 *
	 * @returns Een HtmlObject om in een pagina neer te zetten.
	 */
	private static function inschrijfZoekform() {
		$zoekstring = tryPar('LidVerzamelingZoeken', '');

		$form = new HtmlForm();
		$form->add(new HtmlParagraph(_("Vul hier je naam in om je gegevens bij te werken:"))); // TODO
		if(DEBUG || DEMO)
		{
			$randomlidID = 111;
			if(Lid::geef($randomlidID)->getLidToestand() != 'BIJNALID')
				$form->add(new HtmlParagraph(HtmlAnchor::button('/Leden/Intro/DiLI/?degradeer=1', _("Degradeer Lid Anoniem!"))));
			else
				$form->add(new HtmlParagraph(HtmlAnchor::button('/Leden/Intro/DiLI/Formulier?lidid=' . $randomlidID, _("Schrijf Lid Anoniem in!"))));
		}
		$form->addClass('form-inline');
		$form->setName('PersoonVerzameling')
			->add($input_group = new HtmlDiv(null, 'input-group'))
			->setToken(null); //geen token voor deze post-form

		$input_group->add(new HtmlSpan(new HtmlSpan(null, 'fa fa-search'), 'input-group-addon'))
			->add(HtmlInput::makeSearch('LidVerzamelingZoeken', $zoekstring)->setAttribute('data-searchurl', '/Ajax/Dili/Zoek'))
			->add(new HtmlDiv(HtmlInput::makeSubmitButton(_('Zoeken')), 'input-group-btn'));
			
		$form->add(new HtmlDiv(null, null, 'Leden_zoekContainer'));
		
		$page = Page::getInstance();
		
		$page->addFooterJS(Page::minifiedFile('PersoonZoeken.js'));

		return $form;
	}

	/**
	 * @brief Laat leden inloggen met lidnummer + code.
	 *
	 * @site{/Leden/Intro/Dili/Login}
	 */
	public static function login()
	{
		global $session;

		if(hasAuth('ingelogd') || !self::inschrijvingOpen()) {
			spaceHttp(403);
		}

		if(Token::processNamedForm() == 'diliinlog') {
			$lidnr = tryPar('lidnr');
			$code = tryPar('code');

			if(!$lidnr || !$code) {
				Page::addMelding(_('Je moet wel iets invullen bij zowel het lidnummer als de activatiecode'), 'fout');
			} else {
				$lid = ActivatieCode::toLid($code);

				if(!$lid || $lid->geefID() != $lidnr) {
					Page::addMelding(_('Geen geldige combinatie van lidnummer en activatiecode ingevoerd.'), 'fout');
				} else {
					// Juiste combo lidnr en code, log lid in

					// Voorkom session fixation aanval
					$session->migrate(true);

					// inloggen gelukt, zet session variabelen op basis daarvan.
					$session->set('mylidnr', $lidnr);
					$session->set('veilig', true);

					Page::redirectMelding($lid->url().'/Wijzig', _('Inloggen gelukt!'));
				}
			}
		}

		DiliView::loginPage();
	}

	public static function betalingOverzicht()
	{
		$betalingen = LidmaatschapsBetalingVerzameling::alleBetalingen();

		DiliView::betalingOverzicht($betalingen);
	}

	/**
	 * @brief De betaalpagina voor inschrijvingen.
	 *
	 * Betaalt voor het huidig ingelogd lid, wat een bijnalid moet wezen.
	 *
	 * @site{/Leden/Intro/Dili/Betalen}
	 */
	public static function ideal() {
		$lid = Persoon::getIngelogd();
		if ($lid == null || !self::lidInschrijfbaar($lid)) {
			spaceHTTP(403);
		}

		$voorraad = Voorraad::geef(LIDMAATSCHAP_VOORRAAD);
		$verkoopPrijs = $voorraad->getVerkoopPrijs()->getPrijs();
		$ideal = new iDeal("BOEKWEB", "VERKOOP", $lid, $verkoopPrijs,
			"Lidmaatschap A–Eskwadraat", new DateTimeLocale(),
			"iDealtransactie lidmaatschap A–Eskwadraat",
			$voorraad,
			EVENEMENTENREKENING, null
		);

		$ideal->setEmailAdres($lid->getEmail());
		$ideal->setStatus('INACTIVE');
		$ideal->setReturnURL(DILI_BASE . 'Betaald');

		// Sla de iDEALtransactie op bij de betaling.
		$betaling = LidmaatschapsBetaling::geef($lid);
		if (is_null($betaling) || $betaling->getSoort() != 'IDEAL') {
			Page::redirectMelding($lid->url() . '/Wijzig',
				_("Selecteer eerst hoe je wilt betalen, voordat je aan IDEAL begint"),
				'fout'
			);
		}
		$betaling->setIdeal($ideal);
		$betaling->opslaan();

		$ideal->startTransactie();
	}

	/**
	 * @brief De pagina die geslaagde betalingen afrondt.
	 *
	 * Maakt het huidige bijnalid een echt lid.
	 *
	 * @site{/Leden/Intro/Dili/Betaald}
	 */
	public static function betaald() {
		// We checken niet op lidstatus,
		// want de webhook zou al gebeurd kunnen zijn
		// voordat de pagina geladen wordt.

		// Log uit en stuur door naar de inlogpagina.
		// TODO: de redirect uit logout moet mooier kunnen
		$redirectEntry = hrefToEntry('/Leden/Intro/Dili/Login')->getId();
		Page::redirectMelding('/space/auth/logout.php?parent=' . $redirectEntry,
			_("De transactie wordt nu verwerkt. Zodra dat gelukt is, word je "
		   . "automatisch lid. Dit duurt meestal maar een paar seconden."));
	}

	/**
	 * @brief Moeten bijnaleden een machtiging / IDEALtransactie doen bij het inschijven?
	 *
	 * @returns Een bool of betalingen nodig zijn.
	 */
	public static function inschrijvingBetalen() {
		$entry = Register::getValue('inschrijvingBetalen', true);
		// Zie ook comment bij inschrijvingOpen
		return filter_var($entry, FILTER_VALIDATE_BOOLEAN);
	}

	/**
	 * @brief Heeft de gebruiker autorisatie om een inschrijving te doen?
	 *
	 * @returns Een bool of er een inschrijving mag gebeuren.
	 */
	public static function inschrijvingOpen() {
		$entry = Register::getValue('inschrijvingOpen', false);

		// Dit ziet er misschien belachelijk uit, maar consider the following:
		// https://stackoverflow.com/questions/4775294/parsing-a-string-into-a-boolean-value-in-php
		// Lang leve PHP.
		return filter_var($entry, FILTER_VALIDATE_BOOLEAN);
	}

	/**
	 * @brief Is het zinnig om dit bijnalid in te schrijven?
	 *
	 * @returns Een bool of dit bijnalid ingeschreven kan worden.
	 */
	private static function lidInschrijfbaar($bijnalid) {
		return $bijnalid->getLidToestand() == 'BIJNALID';
	}
}
