<?php

use Space\Bedrag;
use Space\GeenChocolaException;

abstract class IOU_Controller
{

	/**
	 *  Geeft de overzichtspagina van het ingelogde lid weer
	 */
	static public function overzicht()
	{
		$persoon = Persoon::getIngelogd();
		if(!$persoon)
			return false;
		$page = Page::getInstance()->start();

		$page->add(IOUView::menu());

		//$page->add(new HtmlHeader(2,_('Persoonlijk IOU-overzicht')));

		//$page->add(new HtmlHeader(4,_("Persoonlijk financieel overzicht")));

		$debcredarray = IOUBetalingVerzameling::getBalans(Persoon::getIngelogd());

		$page->add($rowdiv = new HtmlDiv(null, 'row'));

		$rowdiv->add($divrechts = new HtmlDiv(null, 'col-sm-6'));
		$divrechts->add(new HtmlHeader(3, _("Alle mensen die jou nog geld moeten betalen:")));

		$rowdiv->add($divlinks = new HtmlDiv(null, 'col-sm-6'));
		$divlinks->add(new HtmlHeader(3, _("Alle mensen die jij nog geld moet betalen:")));

		$credtotaal = 0;
		$debtotaal = 0;

		// header voor sorteerknopjes
		$header = new HtmlTableHead();
		$row = $header->addRow();
		$row->addHeader(_("Naam"));
		$row->addHeader(_("Bedrag"));

		$divrechts->add($cred = new HtmlTable(array($header), "sortable"));
		$divlinks->add($deb = new HtmlTable(array($header), "sortable"));

		foreach($debcredarray as $key => $dc) {
			if(round($dc,2) < 0) {
				$dc = abs(round($dc,2));
				$row = $deb->addRow();
				$pers = Persoon::geef($key);
				$d = new HtmlDiv(PersoonView::waardeRekeningnummer($pers), 'rekeningnummer');
				$d->setCssStyle('display: initial');
				$row->addData(
					PersoonView::makeLink($pers, null, IOUBASE.$key)
					. " " .
					(($pers->getRekeningnummer())
						? "(" . $d . ")"
						: "")
					);
				$cell = $row->addData(Money::addPrice($dc));
				$cell->setAttribute('align','right');
				$debtotaal += $dc;
			} elseif(round($dc,2) > 0) {
				$row = $cred->addRow();
				$pers = Persoon::geef($key);
				$row->addData(PersoonView::makeLink($pers, null, IOUBASE.$key));
				$cell = $row->addData(Money::addPrice(round($dc,2)));
				$cell->setAttribute('align','right');
				$credtotaal += $dc;
			}
		}

		// gebruik een footer om de totalen ongesorteerd te laten
		$foot = $cred->addFoot();
		$row = $foot->addRow();
		$cell = $row->addData(_("Totaal te ontvangen:"));
		$cell->setAttributes(array('align' => 'right', 'valign' => 'bottom'));
		$cell = $row->addData(Money::addPrice($credtotaal));
		$cell->setAttributes(array('align' => 'right', 'style' => 'border-top: double black;'));

		$foot = $deb->addFoot();
		$row = $foot->addRow();
		$cell = $row->addData(_("Totaal aan schulden:"));
		$cell->setAttributes(array('align' => 'right', 'valign' => 'bottom'));
		$cell = $row->addData(Money::addPrice($debtotaal));
		$cell->setAttributes(array('align' => 'right', 'style' => 'border-top: double black;'));

		$divrechts->add(HtmlAnchor::button(IOUBASE . 'Kwijtschelden', _('Al deze schulden kwijtschelden')));

		$page->end();
	}

	/** 
	 *  De persoonEntry-functie
	 */
	static public function persoonEntry($args)
	{
		if (!hasAuth('ingelogd'))
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);

		$pers = Persoon::geef($args[0]);
		if(!$pers) {
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);
		} else {
			return array( 'name' => $args[0]
						, 'displayName' => sprintf(_('Bonnen met %s')
												, PersoonView::naam($pers))
						, 'access' => true);
		}
	}

	/**
	 *  De controller-functie die de overzichtspagina van de relatie
	 * tussen het ingelogde lid en een persoon aanroept
	 */
	static public function persoonOverzicht()
	{
		$id = vfsVarEntryName();
		
		$pers = Persoon::geef($id);

		$ingelogd = Persoon::getIngelogd();

		$betalingen = IOUBetalingVerzameling::alleVanTweePersonen($ingelogd, $pers);

		IOUBetalingVerzamelingView::persoonOverzicht($ingelogd, $pers, $betalingen);
	}

	/**
	 *  De bonEntry-functie
	 */
	static public function bonEntry($args)
	{
		global $auth;

		if (!hasAuth('ingelogd'))
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);

		$bon = IOUBon::geef($args[0]);
		if(!$bon)
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);

		$betrokkenen = $bon->getBetrokkenen();

		// Alleen als het ingelogd lid betrokken is bij de bon mag die de bon 
		// bekijken
		if(in_array($auth->getLidnr()	,$betrokkenen) || hasAuth('god')) {
			return array( 'name' => $args[0]
						, 'displayName' => sprintf(_("IOU-bon #%s"), $args[0])
						, 'access' => true);
		} else {
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);
		}
	}

	/**
	 *  Controllerfunctie de de overzichtspagina van een bon aanroept
	 */
	static public function bon()
	{
		$id = vfsVarEntryName();

		$bon = IOUBon::geef($id);

		IOUBonView::bon($bon);
	}

	/**
	 *  De betalingsEntry-functie
	 */
	static public function betalingEntry($args)
	{
		if (!hasAuth('ingelogd'))
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);

		$betaling = IOUBetaling::geef($args[0]);
		// Als de betaling niet bestaat of als de betaling bij een ingevoerde
		// bon hoort, mag die niet worden laten zien
		if(!$betaling || $betaling->getBon())
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);

		// Als de ingelogde persoon geen deel uitmaakt van de betaling mag de
		// betaling niet worden laten zien
		if(Persoon::getIngelogd() == $betaling->getVan() || Persoon::getIngelogd() == $betaling->getNaar()) {
			return array( 'name' => $args[0]
						, 'displayName' => sprintf(_("IOU-betaling #%s"), $args[0])
						, 'access' => true);
		} else {
			return array( 'name' => ''
						, 'displayName' => ''
						, 'access' => false);
		}
	}

	/**
	 *  De functie die de pagina met het betalingsoverzicht aanroept
	 */
	static public function betaling()
	{
		$id = vfsVarEntryName();

		$betaling = IOUBetaling::geef($id);

		IOUBetalingView::betaling($betaling);
	}

	/**
	
	 *  Controllerfunctie voor het aanroepen van het form voor nieuwe
	 * verrekeningen alsmede het processen daarvan
	 */
	static public function betalingNieuw()
	{
		if(!($persoon = Persoon::getIngelogd()))
			return false;

		$msg = array();

		if(Token::processNamedForm() == 'NieuweBetaling') {
			$check = tryPar('check');
			$idarray = array();
			if (is_array($check) || $check instanceof Traversable) {
				foreach($check as $id => $val) {
					if($val == "on")
						$idarray[] = $id;
				}
			}

			if(sizeof($idarray) == 0) {
				$msg[] = _("Je hebt niemand geselecteerd om een verrekening mee in te voeren");
			} else {
				$verz = new IOUBetalingVerzameling();
				$naararr = tryPar('naar');
				foreach($idarray as $id) {
					$pers = Persoon::geef($id);
					if(!$pers) {
						$msg[] = _("Je mag geen verrekening met een niet bestaand persoon invoeren");
						continue;
					}
					if($pers == $persoon) {
						$msg[] = _("Je mag geen verrekening aan jezelf toevoegen");
						continue;
					}
					$bedrag = tryPar('bedrag_'.$id);
					$bedrag = Money::parseMoney($bedrag);
					if($bedrag == 0) {
						$msg[] = sprintf(_("Je hebt bij de verrekening met %s 0 euro ingevuld")
							, PersoonView::naam($pers));
						continue;
					} elseif($bedrag < 0) {
						$msg[] = sprintf(_("Je hebt bij de verrekening met %s een negatief bedrag ingevuld")
							, PersoonView::naam($pers));
						continue;
					}
					if($naararr[$id] == 'ingelogd')
						$bedrag = -1 * $bedrag;
					$omschrijving = trim(tryPar('text_'.$id));
					$omschr = sprintf(_('%s heeft %s € %s betaald.')
						, PersoonView::naam($persoon)
						, PersoonView::naam($pers)
						, Money::addPrice($bedrag, false, true));
					if($omschrijving) {
						$omschr .= "\n".$omschrijving;
					}
					$betaling = new IOUBetaling();
					$betaling->setVan($pers);
					$betaling->setNaar($persoon);
					$betaling->setBedrag($bedrag);
					$betaling->setOmschrijving($omschr);
					$verz->voegtoe($betaling);
				}
				if(sizeof($msg) == 0) {
					$verz->opslaan();
                    $verz->mailen();
					Page::redirectMelding(IOUBASE,_("Alle verrekenigen zijn succesvol ingevoerd"));
				}
			}
		}

		$debcredarray = IOUBetalingVerzameling::getBalans($persoon);

		IOUBetalingView::betalingNieuw($debcredarray, $msg);
	}

	static public function bonNieuw()
	{
		global $auth;

		$kart = Kart::geef();

		$betrokkenCie	= tryPar('betrokkenCie');
		$omschrijving	= trim(tryPar('bonOmschrijving'));
		$verdeelover	= tryPar('bonVerdeelOver', array());
		$verdeelbedrag	= Money::parseMoney(tryPar('bonVerdeelBedrag', 0));
		$tebetalen		= tryPar('bonTebetalen', array());
		$ingelegd		= tryPar('bonIngelegd', array());

		$betrokkenen = $kart->getPersonen();
		$betrokkenen->union($bla = PersoonVerzameling::verzamel(array_keys(array_filter($tebetalen, "strlen") + array_diff($verdeelover, array("0")) + array_filter($ingelegd, "strlen"))));
		$betrokkenen->voegtoe(Persoon::getIngelogd());
		$cies = $kart->getCies();
		foreach($cies as $cie) {
			$betrokkenen->union($cie->leden());
		}

		// verwijder niet geselecteerde mensen uit tebetalen en ingelegd
		// check ook gelijk dat er geen negatieve bedragen ingevoerd zijn
		$melding = false;
		$valid = true;
		$isMax = false;
		$meldingen = array();
		$tebetalenTot = $verdeelbedrag;
		$ingelegdTot  = 0;

		foreach($tebetalen as $lidnr => $bedrag)
		{
			$bedrag = Money::parseMoney($bedrag, true);
			$tebetalen[$lidnr] = $bedrag;
			if(!$betrokkenen->bevat($lidnr) || $bedrag == 0)
				unset($tebetalen[$lidnr]);
			$tebetalenTot += $bedrag;
			if($bedrag < 0)
				$melding = true;
			if($bedrag >= 1000000)
				$isMax = true;
		}

		foreach($ingelegd as $lidnr => $bedrag)
		{
			$bedrag = Money::parseMoney($bedrag, true);
			$ingelegd[$lidnr] = $bedrag;
			if(!$betrokkenen->bevat($lidnr) || $bedrag == 0)
				unset($ingelegd[$lidnr]);
			$ingelegdTot += $bedrag;
			if($bedrag < 0)
				$melding = true;
			if($bedrag >= 1000000)
				$isMax = true;
		}

		// niet stiekum geld verdelen over niet betrokken mensen
		foreach($verdeelover as $lidnr => $janee)
		{
			if(!$betrokkenen->bevat($lidnr) || $janee != 'on')
				unset($verdeelover[$lidnr]);
		}

		// Er moeten geldige bedragen ingevuld zijn
		if($melding)
			$valid = false;
		if($isMax)
			$valid = false;

		if(Token::processNamedForm() == 'nieuweBon')
		{
			// Er moet een omschrijving zijn
			if(empty($omschrijving))
				$valid = false;

			// De totalen moeten kloppen
			if(Money::isSignificant(abs($tebetalenTot - $ingelegdTot)))
				$valid = false;
			elseif(!Money::isSignificant($tebetalenTot))
				$valid = false;

			// Als er een verdeelbedrag is moet dat bedrag verdeeld worden
			if(empty($verdeelover) && Money::isSignificant($verdeelbedrag))
				$valid = false;

			// Er moeten minstens 2 personen betrokken zijn
			if(sizeof(array_keys($tebetalen + $ingelegd + $verdeelover)) <= 1)
				$valid = false;

			// Je moet zelf betrokken zijn
			if(!in_array($auth->getLidnr(), array_keys($tebetalen+$ingelegd+$verdeelover)))
				$valid = false;

			if($valid && tryPar('nieuwebon'))
			{
				// Yes! alles ok, invoeren die bon...
				$bon = new IOUBon(Persoon::getIngelogd());
				$bon->setOmschrijving($omschrijving);

				$tebetalen = IOUBetalingVerzameling::verdeelBedrag($verdeelbedrag,
						array_keys($verdeelover), $tebetalen, $ingelegd);

				$splitverz = new IOUSplitVerzameling();
				foreach($tebetalen as $lidnr => $bedrag) {
					$split = new IOUSplit($bon, Persoon::geef($lidnr));
					$split->setMoetbetalen($bedrag);
					$splitverz->voegtoe($split);
				}
				foreach($ingelegd as $lidnr => $bedrag) {
					$split = null;
					foreach($splitverz as $s) {
						if($s->getPersoon() == Persoon::geef($lidnr)) {
							$split = $s;
							break;
						}
					}
					if(is_null($split)) {
						$split = new IOUSplit($bon, Persoon::geef($lidnr));
						$split->setHeeftbetaald($bedrag);
						$splitverz->voegtoe($split);
					} else {
						$split->setHeeftbetaald($bedrag);
					}
				}

				$wie = array_unique(array_merge(array_keys($verdeelover),array_keys($tebetalen),array_keys($ingelegd)));
				$betalingen = $bon->berekenResultaat($wie, $ingelegd, $tebetalen);

				if($betalingen->aantal() != 0) {
					$bon->opslaan();
					$splitverz->opslaan();
					$betalingen->opslaan();
					$bon->mailen();
					Page::redirect($bon->url(), 0);
				}
				Page::redirectMelding(IOUBASE, _('Voor deze bon hoeft er niet met geld geschoven te worden!'));
			}
		}

		// Voeg de mensen die niets met de bon te maken hebben maar die wel nog
		// bij alle mogelijke mensen staan ook toe om ervoor te zorgen dat mensen
		// die per ongeluk een foutje maken niet iedereen opnieuw hoeven te selecteren
		$verzids = tryPar('LidVerzameling', array());
		$verzids = array_filter($verzids);
		if(sizeof($verzids) != 0) {
			$betrokkenen->union(PersoonVerzameling::verzamel(array_keys($verzids)));
		}

		IOUBonView::bonNieuw($betrokkenCie, $betrokkenen, $kart, $omschrijving, $verdeelover, $verdeelbedrag, $tebetalen, $ingelegd);
	}

	static public function bonWijzigen()
	{
		$id = vfsVarEntryName();

		$bon = IOUBon::geef($id);

		if(!$bon->magWijzigen())
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm() == 'wijzigenBon') {
			IOUBonView::processForm($bon);
			if($bon->valid()) {
				$bon->opslaan();
				Page::redirectMelding($bon->url(), _('Wijzigen gelukt!'));
			} else {
				Page::addMelding(_('Er waren fouten'), 'fout');
			}
		}

		IOUBonView::bonWijzigen($bon, $show_error);
	}

	/**
	 * @site{/Leden/I-Owe-U/Bon/ * /Verwijder}
	 */
	static public function bonVerwijderen()
	{
		$id = vfsVarEntryName();

		if(!($bon = IOUBon::geef($id)))
			spaceHttp(403);

		$show_error = false;

		if(!$bon->magVerwijderen())
			spaceHttp(403);

		if(Token::processNamedForm() == 'IOUBonVerwijderen')
		{
			$bon->mailen(true);
			$bon->verwijderen();
			Page::redirectMelding(IOUBASE, _('Bon verwijderd!'));
		}

		IOUBonView::bonVerwijderen($bon, $show_error);
	}

	static public function verzoeken()
	{
		$lidInf = Persoon::getIngelogd();

		$balans = IOUBetalingVerzameling::getBalans($lidInf);

		$naam		= PersoonView::naam($lidInf);
		$tennamevan	= trim(tryPar('mailTNV', $naam));
		$rekeningnr	= trim(tryPar('mailRekeningnr', $lidInf->getRekeningnummer()));
		$mensjes	= tryPar('mailMensjes[]');

		$mailtekst	= _('Beste %s,

Volgens de "I Owe You"-administratie van %s
(zie %s) moet jij
nog € %s betalen. Dit bericht is op verzoek van %s
verzonden met de vraag of je het bedrag binnenkort wilt
voldoen op: bank- of girorekeningnummer %s
t.n.v. %s

Als je dit bedrag hebt betaald is het verstandig om dat in de
"I Owe You" te administreren door een verrekening in te voeren,
dit kun je doen via de volgende link:
%s

Alvast hartelijk dank!

A–Eskwadraat "I Owe You",
namens %s');

		$melding = array();
		$gemaild = false;
		if(Token::processNamedForm() == 'betalenKreng') {
			if(empty($tennamevan)) {
				$melding[] = _('Je hebt niet ingevuld is wie de rekeninghouder is!');
			}
			if(empty($rekeningnr)) {
				$melding[] = _('Je hebt je rekeningnummer niet ingevuld!');
			}
			if(empty($melding))
			{
				$from = '"IOU A–Eskwadraat" <iou@a-eskwadraat.nl>';
				foreach($mensjes as $index => $persoon)
				{
					if(round($balans[$index],2) == 0 || !$persoon) {
						unset($mensjes[$index]);
						continue;
					}
					$ander = Persoon::geef($index);

					sendmail($from
						, $ander->getContactID()
						, _('Verzoek tot betaling I Owe You')
						, sprintf($mailtekst
								, PersoonView::naam($ander, WSW_NAME_VOORNAAM)
								, htmlspecialchars($naam)
								, HTTPS_ROOT.IOUBASE.$lidInf->getContactID()
								, Money::addPrice($balans[$index], false, true)
								, PersoonView::naam($lidInf, WSW_NAME_VOORNAAM)
								, htmlspecialchars($rekeningnr)
								, htmlspecialchars($tennamevan)
								, HTTPS_ROOT.IOUBASE."Betaling/Nieuw?check[".$lidInf->getContactID()."]=on#".$lidInf->getContactID()
								, $naam
								)
						);
					$gemaild = true;
				}
				if(!$gemaild) {
					$melding[] = _('Je hebt niemand geselecteerd!');
				} else {
					Page::redirectMelding(IOUBASE, _("De wanbetalers zijn gemaild!"));
				}
			}
		}

		IOUView::verzoekenPagina($balans, $rekeningnr, $tennamevan, $mailtekst, $naam, $lidInf, $gemaild);
	}

	/**
	 * @site{/Leden/I-Owe-U/Kwijtschelden}
	 */
	public static function kwijtschelden()
	{
		$persoon = Persoon::getIngelogd();
		if(!$persoon)
			return false;

		$debcredarray = IOUBetalingVerzameling::getBalans(Persoon::getIngelogd());

		if(Token::processNamedForm() == 'kwijtschelden')
		{
			$betalingen = new IOUBetalingVerzameling();
			foreach($debcredarray as $key => $dc)
			{
				if(round($dc,2) > 0)
				{
					$pers = Persoon::geef($key);
					$betaling = new IOUBetaling();
					$betaling->setVan($pers)
							 ->setNaar($persoon)
							 ->setBedrag(-$dc)
							 ->setOmschrijving(sprintf(_('€ %s kwijtgescholden'), round($dc, 2)));
					$betalingen->voegtoe($betaling);
				}
			}

			if($betalingen->allValid())
			{
				$betalingen->opslaan();

				foreach($betalingen as $betaling)
				{
					$betaling->mailen();
				}

				Page::redirectMelding(IOUBASE, _('Alle schulden kwijtgescholden'));
			}
		}

		$credtotaal = 0;

		$header = new HtmlTableHead();
		$header->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell(_("Naam")));
		$row->add(new HtmlTableHeaderCell(_("Bedrag"), "sorttable_numeric"));

		$cred = new HtmlTable(array($header), "sortable");

		$cred_exists = false;

		foreach($debcredarray as $key => $dc) {
			if(round($dc,2) > 0) {
				$cred_exists = true;

				$cred->add($row = new HtmlTableRow());
				$pers = Persoon::geef($key);
				$row->add(new HtmlTableDataCell(
					new HtmlAnchor(IOUBASE.$key
						, PersoonView::naam($pers))));
				$row->add($cell = new HtmlTableDataCell(
					Money::addPrice(round($dc,2))));
				$cell->setAttribute('align','right');
				$credtotaal += $dc;
			}
		}

		// gebruik een footer om de totalen ongesorteerd te laten
		$cred->add($foot = new HtmlTableFoot());
		$foot->add($row = new HtmlTableRow());
		$row->add($cell = new HtmlTableDataCell(_("Totaal te ontvangen:")));
		$cell->setAttributes(array('align' => 'right', 'valign' => 'bottom'));
		$row->add($cell = new HtmlTableDataCell(Money::addPrice($credtotaal)));
		$cell->setAttributes(array('align' => 'right', 'style' => 'border-top: double black;'));

		$page = Page::getInstance()->start();

		$page->add(IOUView::menu());

		if($cred_exists)
		{
			$page->add(new HtmlHeader(3, _("Weet je zeker dat je de volgende schulden wilt kwijtschelden?")));

			$page->add($cred);

			$page->add($form = HtmlForm::named('kwijtschelden'));
			$form->add(HtmlInput::makeSubmitButton(_('Jazeker')));
		}
		else
		{
			$page->add(new HtmlEmphasis(_('Er zijn geen schulden om kwijt te schelden!')));
		}

		$page->end();
	}
}
