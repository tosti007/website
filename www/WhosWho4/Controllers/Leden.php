<?php
/**
 * /Leden                    -> (zoeken) geeft een simpel zoekformulier
 *  /UitgebreidZoeken        -> (uitgebreidzoeken) uitgebreid zoeken formulier
 *  /Nieuw                    -> (nieuw) maak een nieuw lid
 *  /NieuwPersoon            -> (nieuwpersoon) maak een nieuw persoon
 *  /lid_id                    -> (details) toont de profiel pagina van een lid/persoon
 *   /Wijzig                -> (wijzig) wijzig het geselecteerde lid/persoon
 *   /Verwijder                -> (verwijder) verwijder een lid/persoon
 *  /Foto
 *   /Wijzig                -> (wijzigFoto) wijzig de pasfoto van een lid/persoon
 */

abstract class Leden_Controller
{
	/**
	 * Check of we het snelzoeken-formulier willen weergeven of geselecteerde leden
	 * todo: deze constructie eruit slopen
	 */
	static public function leden()
	{
		global $uriRef;

		if (!hasAuth('ingelogd') || !tryPar('lidnrs'))
		{
			/** Kijk of er gezocht is **/
			$leden = null;
			if ($str = tryPar('LidVerzamelingZoeken', false))
			{
				$leden = LidVerzameling::zoek($str);
			}

			/** Ga gelijk naar de view **/
			PersoonVerzamelingView::simpelzoeken($leden, $str);
		}
		else
		{
			$lidnummers = trypar('lidnrs');
			//Maak een array met unieke lidnrs om een verzameling van te maken.
			$leden = PersoonVerzameling::verzamel(array_unique(explode(",", $lidnummers)));

			//Omdat we niet altijd html terug gaan geven maken we eerst een prepage voor makkelijkere logica
			$prepage = new HtmlDiv();

			//Moeten we iets met de kart?
			switch (Token::processNamedForm())
			{
				case 'SelectToKart':
					//We gaan er voor nu van uit dat de token genoeg beveiliging is, anders moeten we meer gaan posten.
					Kart::geef()->voegPersonenToe($leden);
					$kartdiv = new HtmlDiv();
					$kartdiv->setCSSStyle("border:5px;solid black;margin:10px;");
					$kartdiv->add(_('Alles is zo mogelijk aan je kart toegevoegd, check het '))
						->add(new HtmlAnchor('/Service/Kart/', _('hier')));
					$prepage->add($kartdiv);
					break;
				default:
					break;
			}

			$headPage = new HtmlDiv();

			//Pak de url en haal de view er uit voor het maken van nieuwe viewurls.
			$urlparams = $uriRef['params'];
			unset($urlparams['view']);

			$params = array();
			foreach ($urlparams as $key => $value)
			{
				$params[] = urlencode($key) . '=' . urlencode($value);
			}
			$viewurl = "/Leden/?" . implode('&', $params);


			$headPage->add("Selecteer hoe je de verzameling wil bekijken.")
				->add(new HtmlBreak())
				->add(new HtmlAnchor($viewurl . '&view=lijst', _('Lijst')) . ', ')
				->add(new HtmlAnchor($viewurl . '&view=csv', "CSV") . ', ')
				->add(new HtmlBreak());

			//Omdat lijst de default is kunnen we hooguit onbekende views krijgen.
			switch ($view = trypar('view'))
			{
				case 'csv':
					sendfile("resultaat.csv");
					PersoonVerzamelingView::csv($leden);
					exit;
					break;
				case 'lijst':
				default:
					$headPage->add(PersoonVerzamelingView::lijst($leden));
					break;
			}

			$headPage->add(new HtmlBreak());

			$kartForm = HtmlForm::named('SelectToKart');
			//$kartForm->add(HtmlInput::makeHidden('personen', $personen));
			$kartForm->add(HtmlInput::makeSubmitButton("Stop in kart"));

			$headPage->add("Je kan ook alles in je kart stoppen.");
			$headPage->add($kartForm);

			$prepage->add($headPage);

			if ($view != 'csv')
			{ //Bij csv geven we geen html terug, dus probeer dat ook niet.
				$page = Page::getInstance();
				$page->start('Ledenverzameling');
				$page->add($prepage);
				$page->end();
			}
		}
	}

	/**
	 *  Simpel zoeken is heel goed in het vinden van leden op basis van persoongegevens
	 *  maar het bestuur wil ook kunnen zoeken op typen personen om zo groepen te krijgen.
	 *  Als we een dergelijke groep mensen hebben kunnen we vervolgens de simpele zoek er
	 *  op los laten om binnen de groep te zoeken naar iemand.
	 *
	 * @return Geeft een uitgebreid-zoekformulier.
	 */
	static public function uitgebreidzoeken()
	{
		global $request;

		$persverz = new PersoonVerzameling();

		$page = Page::getInstance();

		if (trypar('uitgebreidzoeken') == 'Zoeken')
		{
			//Vul de constraints door juist te tryparren.
			$constraints = array();
			foreach (PersoonVerzameling::uitgebreidzoekenCriteriaArray() as $constraintsarray)
			{
				$constraintSubArray = array();
				foreach ($constraintsarray as $constraint)
				{
					$constraintSubArray[$constraint] = trypar($constraint);
				}
				$constraints[] = $constraintSubArray;
			}

			//Welke groep mensen willen we?
			$groep = requirePar('groep');

			//Afhankelijk van de vorm van output moeten we op verschillende manieren de data ophalen.
			switch (trypar('output'))
			{
				case 'csv'  :
					//csv output
					header("Content-Disposition: attachment; filename=personen_$groep.csv");
					header("Content-type: text/csv; encoding=utf-8");
					$result = PersoonVerzameling::uitgebreidZoeken($groep, $constraints, true);
					PersoonVerzamelingView::csv(PersoonVerzameling::verzamel($result[1]));
					die; //Nu moet je wel handmatig de pagina refreshen..  TODO
					break;
				case 'csv++'  :
					//csv++ output
					header("Content-Disposition: attachment; filename=personen_$groep.csv");
					header("Content-type: text/csv; encoding=utf-8");
					$result = PersoonVerzameling::uitgebreidZoeken($groep, $constraints, true);
					PersoonVerzamelingView::csv(PersoonVerzameling::verzamel($result[1]), true);
					die;
					break;
				case 'latex':
					header("Content-Disposition: attachment; filename=personen_$groep.tex");
					header("Content-type: text/tex; encoding=utf-8");
					$result = PersoonVerzameling::uitgebreidZoeken($groep, $constraints, true);
					PersoonVerzamelingView::latex(PersoonVerzameling::verzamel($result[1]));
					die;
					break;
				case 'kart':
					$result = PersoonVerzameling::uitgebreidZoeken($groep, $constraints, true);
					$kart = Kart::geef();
					$kart->voegPersonenToe(PersoonVerzameling::verzamel($result[1]));
					spaceRedirect('/Service/Kart');
					break;
				default:
					//Voor "normale" html output
					$result = PersoonVerzameling::uitgebreidZoeken($groep, $constraints);
					$persverz = PersoonVerzameling::verzamel($result[1]);
					break;
			}
		}

		$page->start('Uitgebreid zoeken')
			->add(PersoonVerzamelingView::uitgebreidzoeken());

		//Laat een lijst met mensen zien als die er zijn.
		if ($persverz->aantal() > 0)
		{
			$page->add("Aantal gevonden personen: " . $result[0]);
			$page->add(new HtmlBreak());
			$url = $request->query->all();
			$url["output"] = "kart";
			$page->add(_("Shop alle $result[0] personen in je kart: ") . new HtmlAnchor('?' . http_build_query($url), "Shop!"));
			if ($result[0] > 50)
			{
				$page->add(limietLinks($result[0], 50));
			}
			$page->add(PersoonVerzamelingView::lijst($persverz));
		}

		//TODO: dit mooier maken met CSS.
		$page->add(array(
			new HtmlHeader(3, _("Informatie over Uitgebreid zoeken")),
			new HtmlParagraph(_("Met uitgebreid zoeken kan je op groepen mensen zoeken in de database. Als je alle studie gerelateerde opties niet aanvinkt (dus BA,MA en studie hokjes) dan worden ook mensen gevonden waarvan wij geen studie informatie hebben. Ook wordt er gebluft dat alle leden van verdienste en ereleden ook oudlid zijn, dus deze worden gevonden zodra het vakje 'Oudlid' is aangevinkt.")),
			new HtmlParagraph(_("* Zoek alleen op mensen waarvan deze studie op dit moment actief is.")),
			new HtmlParagraph(_("** PAS OP, deze data is nu nog onbetrouwbaar!!!")),
			new HtmlParagraph(_("*** Als dit niet aangevinkt is dan wordt er geheel niet op bounces gelet."))
		));
		$page->end();
	}

	/**
	 *  Maak een nieuw persoon aan
	 */
	static public function nieuwPersoon()
	{
		$object = new Persoon();

		/** Buffer voor de output **/
		$form = true;
		$msg = null;
		$view = $object->getView();

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'Nieuw')
		{
			$view::processNieuwForm($object);
			if ($object->valid())
			{
				$object->opslaan();
				Page::redirectMelding($object->url(), "De persoon is nu aangemaakt!");
			}
			else
			{
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
			}
		}

		/** Ga naar de view **/
		$fullForm = !tryPar('quick');
		$view::nieuw($object, $form, $msg, $fullForm);
	}

	/**
	 * Maak een nieuw lid aan
	 */
	static public function nieuw()
	{
		$object = new Lid();

		/** Buffer voor de output **/
		$form = true;
		$msg = null;
		$view = $object->getView();

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'Nieuw')
		{
			$view::processNieuwForm($object);
			if ($object->valid())
			{
				$object->opslaan();
				$object->defaultLidInstelling();

				Page::redirectMelding($object->url(), "Het lid is nu aangemaakt!");
			}
			else
			{
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
			}
		}

		/** Ga naar de view **/
		$fullForm = !tryPar('quick');
		$view::nieuw($object, $form, $msg, $fullForm);
	}

	/**
	 * Returneert een array met het geselecteerd persoon
	 *
	 * Als het lid_id gelijk is aan "Ingelogd", laat dan de huidig ingelogde persoon zien.
	 * (handig voor o.a. documentatie)
	 *
	 * @param args array(0 => 'Lid_id')
	 */
	static public function LidEntry($args)
	{
		global $request;

		// special case: "Ingelogd" redirect naar het huidig ingelogde lid
		if (strtolower($args[0]) == "ingelogd")
		{
			$pers = Persoon::getIngelogd();
		}
		else
		{
			$pers = Persoon::geef($args[0]);
		}

		if (!$pers)
		{
			return array('name' => ''
			, 'displayName' => ''
			, 'access' => false);
		}

		//Als we een plaatje opvragen laat dan een 403 plaatje zien. (plaatje.php checkt zelf de rechten)
		$access = false;
		if ($pers->magBekijken() || substr($request->server->get('REQUEST_URI'), -15) == "/Foto/Duimnagel")
		{
			$access = true;
		}

		return array('name' => $pers->geefID()
		, 'displayName' => PersoonView::naam($pers)
		, 'access' => $access);
	}

	/**
	 * Toon de detailspagina van een lid
	 */
	static public function details()
	{
		/** Laad het gekozen lid **/
		$persoonid = (int)vfsVarEntryName();

		//Als het een lid is gaan we verder met een lid object.
		if (Lid::geef($persoonid))
		{
			$figuur = Lid::geef($persoonid);
		}
		else
		{
			$figuur = Persoon::geef($persoonid);
		}

		if (!$figuur->magBekijken())
		{
			spaceHTTP(403);
		}

		/** Kijk of het lid de virus-achiement heeft **/
		if ($figuur->hasBadge('virus'))
		{
			$pers = Persoon::getIngelogd();
			$pb = $pers->getBadges();
			if (!$pers->hasBadge('virus'))
			{
				$pb->addBadge('virus');
				$fp = fopen('/var/log/www/virus-achievement.log', 'a+');
				$date = new DateTimeLocale();
				fwrite($fp, $date->format('Y-m-d H:i:s') . " " . $figuur->geefID() . "->" . $pers->geefID() . "\n");
				fclose($fp);
			}
		}

		/** Ga naar de view van het betreffende figuur **/
		$view = $figuur->getView();
		$view::details($figuur);
	}

	/**
	 * Wijzig een lid
	 */
	static public function wijzig()
	{
		/** Laad het gekozen lid **/
		$id = (int)vfsVarEntryName();
		if (Lid::geef($id))
		{
			$isLid = TRUE;
			$figuur = Lid::geef($id);
		}
		else
		{
			$isLid = FALSE;
			$figuur = Persoon::geef($id);
		}

		if (!$figuur->magWijzigen())
		{
			spaceHttp(403);
		}

		// We splitsen hier uit in bijnaleden en normale figuren
		// omdat het formalier van bijnaleden wat simpeler is
		if ($isLid && $figuur->isBijnaLid())
		{
			$ingelogd = Persoon::getIngelogd();
			// Het bijnalidinschrijfformulier tonen we alleen aan dat bijnalid zelf.
			if ($figuur == $ingelogd)
			{
				self::wijzigBijnaLid($figuur);
			}
			elseif ($figuur->magWijzigen())
			{
				self::wijzigFiguur($figuur);
			}
			else
			{
				spaceHTTP(403);
			}
		}
		else
		{
			self::wijzigFiguur($figuur);
		}
	}

	static public function wijzigFiguur($figuur)
	{
		global $BESTUUR;

		$view = $figuur->getView();

		$show_error = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'Wijzig')
		{
			$view::processWijzigForm($figuur);
			if ($figuur->valid())
			{
				//Stuur verschillen door naar Secretaris
				if (!($figuur instanceof Lid) || !$figuur->isBijnaLid())
				{
					$verschil = $figuur->wijzigingen;
					if (!empty($verschil) && !hasAuth('bestuur')
						&& !ContactPersoon::vanPersoon($figuur, "bedrijfscontact")
						&& !ContactPersoon::vanPersoon($figuur, "normaal")) //Negeer wijzigingen van bestuur en hoger, en van sommige CP's
					{
						$verschiltekst = "Geachte Secretaris,\r\n\r\n" . "Van " . PersoonView::naam($figuur) . " (" . get_class($figuur) . " #" . $figuur->geefID() . ") zijn de volgende velden door " . PersoonView::naam(Persoon::getIngelogd()) . " gewijzigd:\r\n\r\n";
						foreach ($verschil as $object => $veld)
						{
							$verschiltekst .= $object . "\r\n";
							foreach ($veld as $naam => $val)
							{
								$verschiltekst .= "\t$naam\r\n\t\tOud:\t" . View::maakString($val[0]) . "\r\n\t\tNieuw:\t" . View::maakString($val[1]) . "\r\n\r\n";
							}
						}
						sendmail(WWWEMAIL, $BESTUUR['secretaris']->getEmail(), "Gegevens gewijzigd", $verschiltekst . "Groetjes, de website");
					}
				}

				$figuur->opslaan();
				Page::redirectMelding($figuur->url(), _("De gegevens zijn gewijzigd!"));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		/** Ga naar de view **/
		$view::wijzig($figuur, $show_error);
	}

	static public function wijzigBijnaLid($figuur)
	{
		$adressen = $figuur->getAdressen();

		if (!$adressen)
		{
			$adressen = new ContactAdresVerzameling();
		}

		// We gaan ervan uit dat een bijnalid precies 1 adres heeft
		if ($adressen->aantal() == 0)
		{
			$adres = new ContactAdres($figuur);
		}
		else
		{
			$adres = $adressen->first();
		}

		$betaling = LidmaatschapsBetaling::geef($figuur);
		if (!$betaling)
		{
			if (Dili_Controller::inschrijvingBetalen())
			{
				$betaling = new LidmaatschapsBetaling($figuur, 'IDEAL');
			}
			else
			{
				$betaling = new LidmaatschapsBetaling($figuur, 'BOEKVERKOOP');
			}
		}

		// Dit is een heel lange functie geworden,
		// TODO: refactor dit in wat fijnere stukjes
		$show_error = false;

		if (Token::processNamedForm() == 'WijzigBijnaLid')
		{
			// Er is nog niets fout...
			$error = false;

			$telnrs = $figuur->getTelefoonNummers();

			// We roepen hier met adres en telnrs aan zodat we die kunnen
			// wijzigen in de process-functie omdat we ze niet meteen in het
			// figuur-object op kunnen slaan
			LidView::processWijzigBijnaLidForm($figuur, $adres, $telnrs);

			LidmaatschapsBetalingView::processForm($betaling);

			// Je moet wel een geldige betaalsoort opgeven!
			$betaalsoort = $betaling->getSoort();
			if (array_search($betaalsoort, LidmaatschapsBetaling::beschikbareEnumsSoort()) === false)
			{
				Page::addMelding(_("Selecteer de manier waarop je wilt betalen"), 'fout');
				$error = true;
			}

			// Sla alles op als het goed is
			if (!$error && $figuur->valid() && $adres->valid() && $telnrs->allValid() && $betaling->valid())
			{
				$figuur->opslaan();
				$adres->opslaan();
				$telnrs->opslaan();
				$betaling->opslaan();

				// Hier passen we de mailinglists aan, omdat die als many-to-many worden
				// opgeslagen is dit een beetje lelijk
				$toekomstMailingLists = tryPar('Mailing', []);

				$mailingList = ContactMailingListVerzameling::vanContact($figuur);

				// Haal eerst de huidige contactmailinglists weg die we niet meer willen
				foreach ($mailingList as $ml)
				{
					if (!in_array($ml->geefID(), array_keys($toekomstMailingLists)))
					{
						$mailingList->verwijder($ml->geefID());
						$ml->verwijderen();
					}
				}

				// Voeg de nieuwe contactmailinglists toe die we wel willen
				foreach (tryPar('Mailing') as $key => $mail)
				{
					$ml = new ContactMailingList($figuur, $key);
					if (!$mailingList->bevat($ml))
					{
						$ml->opslaan();
						$mailingList->voegtoe($ml);
					}
				}

				// Als het lid wil betalen met IDEAL, stuur door naar DiLI voor betalen.
				if ($betaalsoort == 'IDEAL')
				{
					Page::redirect(DILI_BASE . 'Betalen');
				}
				else
				{
					// Maar anders wordt dit door het bestuur geregeld.
					$figuur->bijnaLidToLid('INGESCHREVEN');
					Dili_Controller::betaald();
				}
			}
			else
			{
				// Laat zien dat er fouten waren
				Page::addMelding(_('Er waren fouten'), 'fout');
				$show_error = true;
			}
		}

		// Roep de view aan, ook met adres omdat we het adres meteen willen aanpassen
		LidView::wijzigBijnaLid($figuur, $adres, $betaling, $show_error);
	}

	/**
	 * Wijzig de studies van een lid
	 */
	static public function wijzigStudies()
	{
		global $BESTUUR;

		/** Laad het gekozen lid **/
		$id = (int)vfsVarEntryName();
		if (Lid::geef($id))
		{
			$figuur = Lid::geef($id);
		}
		else
		{
			$figuur = Persoon::geef($id);
		}

		$view = $figuur->getView();

		if (!$figuur->magWijzigen())
		{
			spaceHttp(403);
		}

		$fouten = false;
		$studiesFiguur = $figuur->getStudies();

		$studies = new LidStudieVerzameling();
		$studiesCur = new LidStudieVerzameling();

		foreach ($studiesFiguur as $studie)
		{
			$clone = clone($studie);
			$studies->voegtoe($clone);
			$studiesCur->voegtoe($clone);
		}

		$nieuw = new LidStudie($figuur);
		$studies->voegtoe($nieuw);

		if (Token::processNamedForm() == 'Wijzig')
		{
			LidStudieVerzamelingView::processWijzigStudie($studies);

			$verschil = array();

			// Bekijk de verschillen
			foreach ($studiesFiguur as $i => $studie)
			{
				$dataadr = tryPar('LidStudie');

				if ((bool)$dataadr[$studie->geefID()]['verwijder'])
				{
					$verschil['Verwijderde studie #' . $studie->geefID()] =
						array('gehele studie:' => array(LidStudieView::toString($studie), ''));
				}
				else
				{
					$wijzigingen = $studie->difference($studies[$i]);

					if ($wijzigingen)
					{
						$verschil['Studie #' . $studie->geefID()] = $wijzigingen;
					}
				}
			}

			if ($studies->allValid())
			{
				$verschil['Nieuwe studie'] = array('geheel adres:' => array('', LidStudieView::toString($studies->last())));
			}
			elseif (!$studiesCur->allValid() || tryPar('NieuwStudie', null))
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$fouten = true;
			}

			if (!$fouten)
			{
				//Stuur verschillen door naar Secretaris
				if (!empty($verschil) && !hasAuth('bestuur')
					&& !ContactPersoon::vanPersoon($figuur, "bedrijfscontact")
					&& !ContactPersoon::vanPersoon($figuur, "normaal")) //Negeer wijzigingen van bestuur en hoger, en van sommige CP's
				{
					$verschiltekst = "Geachte Secretaris,\r\n\r\n" . "Van " . PersoonView::naam($figuur) . " (" . get_class($figuur) . " #" . $figuur->geefID() . ") zijn de volgende velden door " . PersoonView::naam(Persoon::getIngelogd()) . " gewijzigd:\r\n\r\n";

					foreach ($verschil as $object => $veld)
					{
						$verschiltekst .= $object . "\r\n";

						foreach ($veld as $naam => $val)
						{
							$verschiltekst .= "\t$naam\r\n\t\tOud:\t" . View::maakString($val[0]) . "\r\n\t\tNieuw:\t" . View::maakString($val[1]) . "\r\n\r\n";
						}
					}
					sendmail(WWWEMAIL, $BESTUUR['secretaris']->getEmail(), "Gegevens gewijzigd", $verschiltekst . "Groetjes, de website");
				}

				$returnVal = array();

				// Verwijder de studies die verwijderd moeten worden
				foreach ($studiesCur as $i => $studie)
				{
					$dataadr = tryPar('LidStudie');

					if ((bool)$dataadr[$studie->geefID()]['verwijder'])
					{

						$studies->verwijder($i);
						$studiesCur->verwijder($i);

						$studie_errors = $studie->verwijderen();
						if (!is_null($studie_errors))
						{
							foreach ($studie_errors as $error)
							{
								$returnVal[] = $error;
							}
						}
					}
				}

				if (empty($returnVal))
				{
					if (tryPar('NieuwStudie', null))
					{
						$studies->opslaan();
					}
					else
					{
						$studiesCur->opslaan();
					}

					if (!empty($verschil))
					{
						Page::redirectMelding($figuur->url(), _("De gegevens zijn gewijzigd!"));
					}
					else
					{
						Page::redirectMelding($figuur->url(), _("Er is niks gewijzgd!"));
					}
				}
				else
				{
					Page::addMeldingArray($returnVal, 'fout');
				}
			}
		}

		/** Ga naar de view **/
		LidStudieVerzamelingView::wijzigStudies($studies, $fouten);
	}

	/**
	 * Verwijder een lid
	 */
	static public function verwijder()
	{
		/** Verwerk get-data **/
		$id = (int)vfsVarEntryName();
		if (Lid::geef($id))
		{
			$figuur = Lid::geef($id);
		}
		else
		{
			$figuur = Persoon::geef($id);
		}

		$view = $figuur->getView();

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == get_class($figuur) . 'Verwijderen')
		{
			$returnVal = $figuur->verwijderen();
			if (!is_null($returnVal))
			{
				Page::addMelding($returnVal, 'fout');
			}
			else
			{
				Page::redirectMelding(WSWHOME, _("Het figuur is verwijderd!"));
			}
		}

		/** Ga naar de view **/
		$page = Page::getInstance();
		$page->start(_("Verwijderen") . ' ' . $view::naam($figuur))
			->add($view::verwijderForm($figuur))
			->add(new HtmlParagraph(_("Let op: je verwijdert ook meteen alle dingen gerelateerd aan dit figuur!
				Ook kan je iemand niet verwijderen als deze commissielid, deelnemer, donateur, mentor is (geweest)
					 of aan een financiele transactie is gekoppeld.
				")))
			->end();
	}

	/**
	 *  Toon de google maps route van A-Eskwadraat naar de Persoon
	 */
	static public function routeNaar()
	{
		$contact = Contact::geef((int)vfsVarEntryName());
		$adres = $contact->getEersteAdres();

		if ($adres)
		{
			spaceRedirect(ContactAdresView::googleMapsRouteUrl($adres));
		}
		else
		{
			spaceHttp(403);
		}
	}

	/**
	 *  Toon de google maps QR-code van A-Eskwadraat naar de Persoon (om met je telefoon te scannen!)
	 */
	static public function mapsQR()
	{
		$page = Page::getInstance();
		$page->start();
		$persoon = Contact::geef((int)vfsVarEntryName());
		$page->add(ContactAdresView::mapsQR($persoon->getEersteAdres()));
		$page->end();
	}

	/**
	 *    Top 25 lijst selectie.
	 */
	static public function top25()
	{
		global $WSW4DB;
		$opties = array('voornaam' => _('voornaam')
		, 'achternaam' => _('achternaam')
		, 'geboortedatum' => _('geboortedatum')
		, 'voornaamwoord' => _('voornaamwoord')
		, 'straat' => _('straat')
		, 'woonplaats' => _('woonplaats')
		, 'tussenvoegsels' => _('tussenvoegsels')
		, 'nuactieve zonder' => _('huidig actieve leden')
		, 'nuactieve met' => _('huidig actieve leden + (oud)bestuur')
		, 'alltime actief zonder' => _('actiefste (oud)leden')
		, 'alltime actief met' => _('actiefste (oud)leden + (oud)bestuur')
		, 'achievements zonder' => _('achievements')
		, 'achievements met' => _('achievements + (oud)bestuur')
		, 'actief/niet actief' => _('actiefste niet-actieve leden')
		);

		$wat = strtolower(tryPar('lijst'));
		if (!array_key_exists($wat, $opties))
		{
			$wat = 'voornaam';
		}

		$veld = $wat;
		switch ($veld)
		{
			case 'geboortedatum':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `datumGeboorte` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `Persoon` USING(`contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. '   AND `datumGeboorte` IS NOT NULL'
					. '   AND `datumGeboorte` != ""'
					. ' GROUP BY `datumGeboorte`'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
				);
				break;
			case 'voornaam':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `voornaam` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `Persoon` USING(`contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. '   AND `voornaam` IS NOT NULL'
					. '   AND `voornaam` != ""'
					. ' GROUP BY `voornaam`'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
				);
				break;
			case 'achternaam':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `achternaam` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `Persoon` USING(`contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. '   AND `achternaam` IS NOT NULL'
					. '   AND `achternaam` != ""'
					. ' GROUP BY `achternaam`'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
				);
				break;
			case 'voornaamwoord':
				$rawData = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `voornaamwoord_woordID` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `Persoon` USING(`contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. ' GROUP BY b'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
				);
				$data = array();
				foreach ($rawData as $key => $value)
				{
					$woord = Voornaamwoord::geef($value['b']);
					if ($woord)
					{
						$data[$key] =
							['a' => $value['a']
								, 'b' => VoornaamwoordView::defaultWaardeVoornaamwoord($woord)
							];
					}
				}
				break;
			case 'tussenvoegsels':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `tussenvoegsels` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `Persoon` USING(`contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. '   AND `tussenvoegsels` IS NOT NULL'
					. '   AND `tussenvoegsels` != ""'
					. ' GROUP BY `tussenvoegsels`'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
				);
				break;
			case 'straat':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `straat1` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `ContactAdres`'
					. ' ON (`Lid`.`contactID` = `ContactAdres`.`contact_contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. '   AND `straat1` IS NOT NULL'
					. '   AND `straat1` != ""'
					. '   AND `soort` = %s'
					. ' GROUP BY `straat1`'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
					, 'THUIS'
				);
				break;
			case 'woonplaats':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `woonplaats` as "b"'
					. ' FROM `Lid`'
					. ' LEFT JOIN `ContactAdres`'
					. ' ON (`Lid`.`contactID` = `ContactAdres`.`contact_contactID`)'
					. ' WHERE ' . WSW4_WHERE_LID
					. '   AND `woonplaats` IS NOT NULL'
					. '   AND `woonplaats` != ""'
					. '   AND `soort` = %s'
					. ' GROUP BY `woonplaats`'
					. ' ORDER BY a DESC, b ASC LIMIT 25'
					, 'THUIS'
				);
				break;
			case 'nuactieve met':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `persoon_contactID` as "b"'
					. 'FROM `CommissieLid`'
					. 'LEFT JOIN `Lid` ON `persoon_contactID`=`contactID`'
					. 'LEFT JOIN `Persoon` USING(`contactID`)'
					. 'LEFT JOIN `Commissie` ON `commissie_commissieID`=`commissieID`'
					. 'WHERE ' . WSW4_WHERE_LID . ' AND (`CommissieLid`.`datumEind` IS NULL OR `CommissieLid`.`datumEind` > CURDATE()) AND `soort`="CIE" AND (Commissie.datumEind IS NULL OR Commissie.datumEind > CURDATE())'
					. 'GROUP BY `persoon_contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
			case 'nuactieve zonder':
				// Een bijzonder lelijke manier om te kijken of iemand bestuur gedaan
				// heeft door te kijken of die de achievement 'bestuur' heeft
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `CommissieLid`.`persoon_contactID` as "b"'
					. 'FROM `CommissieLid`'
					. 'LEFT JOIN `Lid` ON `persoon_contactID`=`contactID`'
					. 'LEFT JOIN `Persoon` USING(`contactID`)'
					. 'LEFT JOIN `Commissie` ON `commissie_commissieID`=`commissieID`'
					. 'LEFT JOIN `PersoonBadge` ON `PersoonBadge`.`persoon_contactID`=`contactID` '
					. 'WHERE ' . WSW4_WHERE_LID . ' AND (`CommissieLid`.`datumEind` IS NULL OR `CommissieLid`.`datumEind` > CURDATE()) AND `soort`="CIE" AND (Commissie.datumEind IS NULL OR Commissie.datumEind > CURDATE()) '
					. 'AND `badges` NOT LIKE "%%bestuur%%" '
					. 'GROUP BY `CommissieLid`.`persoon_contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
			case 'alltime actief met':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `persoon_contactID` as "b"'
					. 'FROM `CommissieLid`'
					. 'LEFT JOIN `Lid` ON `persoon_contactID`=`contactID`'
					. 'LEFT JOIN `Persoon` USING(`contactID`)'
					. 'LEFT JOIN `Commissie` ON `commissie_commissieID`=`commissieID`'
					. 'WHERE  `soort`="CIE" '
					. 'GROUP BY `persoon_contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
			case 'alltime actief zonder':
				// Een bijzonder lelijke manier om te kijken of iemand bestuur gedaan
				// heeft door te kijken of die de achievement 'bestuur' heeft
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `CommissieLid`.`persoon_contactID` as "b"'
					. 'FROM `CommissieLid`'
					. 'LEFT JOIN `Lid` ON `CommissieLid`.`persoon_contactID`=`contactID`'
					. 'LEFT JOIN `Persoon` USING(`contactID`)'
					. 'LEFT JOIN `Commissie` ON `commissie_commissieID`=`commissieID`'
					. 'LEFT JOIN `PersoonBadge` ON `PersoonBadge`.`persoon_contactID`=`contactID` '
					. 'WHERE  `soort`="CIE" '
					. 'AND `badges` NOT LIKE "%%bestuur%%" '
					. 'GROUP BY `CommissieLid`.`persoon_contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
			case 'achievements met':
				$data = $WSW4DB->q('TABLE SELECT LENGTH(`badges`) - LENGTH(REPLACE(`badges`, ",", "" ))+1 as "a", `persoon_contactID` as "b" '
					. 'FROM `PersoonBadge` '
					. 'GROUP BY `persoon_contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
			case 'achievements zonder':
				$data = $WSW4DB->q('TABLE SELECT LENGTH(`badges`) - LENGTH(REPLACE(`badges`, ",", "" ))+1 as "a", `persoon_contactID` as "b" '
					. 'FROM `PersoonBadge` '
					. 'WHERE `badges` NOT LIKE "%%bestuur%%" '
					. 'GROUP BY `persoon_contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
			case 'actief/niet actief':
				$data = $WSW4DB->q('TABLE SELECT COUNT(*) as "a", `Deelnemer`.`persoon_contactID` as "b"'
					. 'FROM `Deelnemer`'
					. 'LEFT JOIN `Lid` ON `Deelnemer`.`persoon_contactID`=`contactID`'
					. 'LEFT JOIN `Persoon` USING (`contactID`)'
					. 'LEFT JOIN `PersoonBadge` ON `PersoonBadge`.`persoon_contactID`=`contactID` '
					. 'WHERE  `badges` NOT LIKE "%%commissie_lid%%" '
					. 'GROUP BY `Persoon`.`contactID` ORDER BY a DESC, b ASC LIMIT 25');
				break;
		}

		LidView::toonTop25Lijst($opties, $wat, $data);
	}

	/**
	 *  Laat leden nieuwe pasfotos uploaden.
	 */

	public static function wijzigFoto()
	{
		global $FW_EXTENSIONS_FOTO, $filesystem, $BESTUUR;

		$lidnr = (int)vfsVarEntryName();

		if (!($lid = Persoon::geef($lidnr)))
		{
			spaceHttp(403);
		}

		//Je mag alleen jezelf aanpassen of je moet bestuursrechten hebben.
		if (Persoon::getIngelogd() != $lid && !hasAuth('bestuur'))
		{
			spaceHttp(403);
		}

		$show_error = false;

		if (Token::processNamedForm() == 'fotoForm')
		{
			$userfile = $_FILES["userfile"];

			$mediafile = Media::uploaden($userfile, true);

			if (is_array($mediafile) && count($mediafile) != 0)
			{
				Page::addMeldingArray($mediafile, 'fout');
			}
			else
			{
				// Als er al een pasfoto met status NIEUW is, haal die dan eerst weg
				$huidig = ProfielFoto::zoekNieuw($lid);
				if ($huidig)
				{
					$foto = $huidig->getMedia();
					$returnVal = $huidig->verwijderen();
					if (!is_null($returnVal))
					{
						user_error($returnVal, E_USER_ERROR);
					}
					$foto->verwijderVanFilesystem();
					$returnVal = $foto->verwijderen();
					if (!is_null($returnVal))
					{
						user_error($returnVal, E_USER_ERROR);
					}
				}

				$profielfoto = new ProfielFoto(Persoon::geef($lidnr), $mediafile);
				$profielfoto->setStatus('NIEUW');
				$profielfoto->opslaan();

				$tekst = sprintf(_("Beste %s"), SECRNAME) . ",\n\n"
					. sprintf(_("We hebben een foto van %s (%d) mogen ontvangen.
Zou je deze foto willen controleren via %s
mits de foto voldoet aan de eisen.

Met vriendelijke groet,
De WebCie"),
						PersoonView::naam(Lid::geef($lidnr)),
						$lidnr,
						HTTPS_ROOT . '/Leden/Administratie/Pasfotos'
					);

				sendmail(_('De Website') . ' <' . WWWEMAIL . '>', $BESTUUR['secretaris']->getEmail(), _('Er is een foto geüpload'), $tekst);

				Page::redirectMelding($lid->url(), _('Bedankt voor je upload. We zullen je upload spoedig verwerken op de website.'));
			}
		}

		$page = Page::getInstance();
		$page->start('Foto uploaden');

		$page->add(PersoonView::fotoUpload($lidnr));

		$page->end();
	}

	/** Shop functies **/
	public static function shop()
	{
		$lidnr = (int)vfsVarEntryName();
		$lid = Lid::geef($lidnr);
		if (!$lid->magBekijken())
		{
			spaceHttp(403);
		}

		Kart::geef()->voegPersoonToe($lidnr);
		spaceRedirect('/Service/Kart');
	}

	public static function unShop()
	{
		$lidnr = (int)vfsVarEntryName();
		$lid = Lid::geef($lidnr);
		Kart::geef()->verwijderPersoon($lidnr);
		spaceRedirect('/Service/Kart');
	}


	//Geef alle bounces weer van een lid.
	public static function bounces()
	{
		$lidnr = (int)vfsVarEntryName();
		$lid = Lid::geef($lidnr);

		if (!$lid->magWijzigen())
		{
			spaceHTTP(403);
		}

		$page = Page::getInstance();

		if (($verwijderID = tryPar('verwijder', null)) && hasAuth('bestuur'))
		{
			$bounce = MailingBounce::geef($verwijderID);
			if (!$bounce)
			{
				Page::addMelding(sprintf(_('Bounce %s bestaat niet!'), $verwijderID), 'fout');
			}
			else
			{
				$contact = $bounce->getContact();
				if (!$contact == $lid)
				{
					Page::addMelding(sprintf(_('Bounce %s hoort niet bij dit contact'), $verwijderID), 'fout');
				}
				else
				{
					$returnVal = $bounce->verwijderen();
					if (!is_null($returnVal))
					{
						Page::addMeldingArray($returnVal, 'fout');
					}
					else
					{
						Page::addMelding(_('Bounce succesvol verwijderd! Yeah!'), 'succes');
					}
				}
			}
		}

		$page->start();

		$page->add(LidView::bounceDiv($lid));

		$page->end();
	}

	public static function willekeurig()
	{
		global $session, $request;

		$lid = Lid::geefWillekeurig();

		// Houd hier een counter bij voor het halen van de 
		// no-life achievement; als je 100x achter elkaar
		// een willekeurig lid zoekt
		if ($request->request->has('willekeurigCounter'))
		{
			$session->set('willekeurigCounter', $request->request->get('willekeurigCounter') + 1);
		}
		else
		{
			$session->set('willekeurigCounter', 1);
		}
		if ($session->get('willekeurigCounter') == 100)
		{
			$pers = Persoon::getIngelogd();
			if ($pers)
			{
				$persBadges = $pers->getBadges();
				$persBadges->addBadge('nolife');
			}
		}
		spaceRedirect('/Leden/' . $lid->geefID() . '?willekeurig=true');
	}

	static public function verjaardagen()
	{
		global $MAANDEN; // 1=>januari, etc..

		// Als we het koppenblad willen zien laat dan alleen de jarigen
		// van vandaag zien
		if (tryPar('koppenblad', null) === 'true')
		{
			if (!hasAuth('ingelogd'))
			{
				spaceHttp(403);
			}

			$page = Page::getInstance()->start(_("Koppenblad van de jarigen"));

			$page->add(new HtmlHeader(4, _("Deze mensen zijn vandaag jarig!")));
			$vandaagJarig = PersoonVerzameling::jarig();
			$page->add(PersoonVerzamelingView::koppen($vandaagJarig));

			$page->add(new HtmlBreak());

			$page->add(HtmlAnchor::button('/Home', _("Terug naar de homepagina")));

			$page->end();
		}
		else
		{
			//Laat de gevraagde maand zien, of de huidige
			$maandNaam = strtolower(tryPar('maand', ''));
			if (empty($maandNaam) || !($maandNr = array_search($maandNaam, $MAANDEN)))
			{
				$maandNr = strftime('%m');
			}
			$maandNaam = ucfirst(strftime('%B', mktime(0, 0, 0, $maandNr))); //normalizeer naam

			//start pagina
			Page::getInstance()->start(_('Verjaardagen') . ': ' . $maandNaam);
			$body = new HtmlElementCollection();

			$jarigen = PersoonVerzameling::jarigInMaand($maandNr);
			$body->add(PersoonVerzamelingView::jarigPerMaandTabel($maandNr, $maandNaam, $jarigen));

			Page::getInstance()->add($body)->end();
		}
	}

	static public function lidToPersoon()
	{
		if (!hasAuth('bestuur'))
		{
			spaceHTTP('403');
		}

		$lidnr = (int)vfsVarEntryName();
		$lid = Lid::geef($lidnr);

		if ($lid)
		{
			if (Token::processNamedForm() == 'lidtopersoon')
			{
				$returnVal = Persoon::lidToPersoon($lid);

				if ($returnVal instanceof Persoon)
				{
					Page::redirectMelding($returnVal->url(), _("Het lid is omgezet naar een persoon."));
				}
				else
				{
					Page::addMeldingArray($returnVal, 'fout');
				}
			}

			$page = Page::getInstance();
			$page->start(_("Lid -> Persoon"));


			if ($lid->mentorVan()->aantal() > 0)
			{
				$page->add(new HtmlParagraph(_("Heu? Dit (oud-)lid is nog mentor van een of meerdere mentorgroepen en is dus echt lid (geweest) 
					en kan je daarom niet omzetten naar een Persoon object.	Wil je iets van de status van dit lid veranderen dan zal je
					waarschijnlijk alleen de lidstatus nodig hebben.")));
			}
			else
			{
				$lidnr = $lid->geefID();

				$form = HtmlForm::named("lidtopersoon");
				$form->add(HtmlInput::makeSubmitButton(_("Ik weet het zeker")));

				$div = new HtmlDiv();
				if (IntroDeelnemer::geef($lidnr))
				{
					$div->add(new HtmlParagraph(_('Let op: dit lid is nog IntroDeelnemer! Deze informatie wordt nu ook automatisch verwijderd!'), 'error'));
				}

				$div->add(new HtmlParagraph(sprintf(_("Je staat op het punt om lid #%d om te zetten naar een persoon (zoals bv een boekenkoper).
					Bij deze stap gaat *ALLE* lidinformatie permanent verloren, dus denk goed na of je dit wilt doen!"), $lidnr)))
					->add($form);
				$page->add($div);
			}
		}
		else
		{
			$page->add(new HtmlParagraph(_("Heu? Dit is helemaal geen Lid object meer!")));
		}

		$page->end();
	}

	static public function persoonToLid()
	{
		if (!hasAuth('bestuur'))
		{
			spaceHTTP('403');
		}

		$lidnr = (int)vfsVarEntryName();
		$pers = Persoon::geef($lidnr);


		if (Token::processNamedForm() == 'persoontolid')
		{
			$lid = Lid::persoonToLid($pers);

			Page::redirectMelding($lid->url() . "/Wijzig", _("De persoon is omgezet naar een lid, je moet nu de lidgegevens invullen."));
		}

		$page = Page::getInstance();
		$page->start("Persoon -> Lid");

		if (!Lid::geef($lidnr))
		{

			$lidnr = $pers->geefID();

			$form = HtmlForm::named("persoontolid");
			$form->add(HtmlInput::makeSubmitButton(_("Ik weet het zeker")));

			$div = new HtmlDiv();
			$div->add(new HtmlParagraph(_("Je staat op het punt om persoon #$lidnr om te zetten naar een lid (zoals bv een bijna-lid of bal).
								 Bij deze stap moet je vervolgens nog wel lidinformatie invullen, hiervoor wordt je automagische doorverwezen.")))
				->add($form);
			$page->add($div);
		}
		else
		{
			$page->add(new HtmlParagraph(_("Je kan niet een lid naar een lid omzetten, Japie!")));
		}

		$page->end();
	}

	public static function overleden()
	{
		if (!hasAuth('bestuur'))
		{
			spaceHTTP('403');
		}

		$lidnr = (int)vfsVarEntryName();

		$pers = Lid::geef($lidnr); //Probeer een lid te krijgen, anders een persoon
		if (!$pers)
		{
			$pers = Persoon::geef($lidnr);
		}

		if (!$pers)
		{
			spaceHTTP(404);
		}

		if (Token::processNamedForm() == 'zetOverleden')
		{
			$pers->setOverleden(true);
			$pers->setVakidOpsturen(false);
			$pers->setAes2rootsOpsturen(false);

			$abonnementen = ContactMailingListVerzameling::vanContact($pers);
			$returnVal = $abonnementen->verwijderen();
			if (!is_null($returnVal))
			{
				user_error($returnVal, E_USER_ERROR);
			}

			$opm = $pers->getOpmerkingen();
			if ($opm)
			{
				$opm .= " \n";
			}
			$opm .= sprintf("Gemarkeerd als overleden op %s door #%d. \n", date('Y-m-d'), Lid::getIngelogd()->geefID());

			$adressen = $pers->getAdressen();
			foreach ($adressen as $adres)
			{
				$plainAdres = ContactAdresView::toString($adres);
				$opm .= "Oud adres: " . $plainAdres . " \n";
				$returnVal = $adres->verwijderen();
				if (!is_null($returnVal))
				{
					user_error($returnVal, E_USER_ERROR);
				}
			}

			$pers->setOpmerkingen($opm);

			if ($pers instanceof Lid)
			{
				$pers->setOpzoekbaar('N');
				$pers->setLidToestand('OUDLID');
			}


			$pers->opslaan();

			Page::redirectMelding($pers->url(), _("De persoon is nu gemarkeerd als overleden. Je kan nog de lid-af-datum aanpassen."));
		}

		$page = Page::getInstance()->start(_('Deze persoon is gemarkeerd als overleden'));
		if ($pers->getOverleden())
		{
			$page->add(new HtmlParagraph(_("Mail de WebCie als je dit ongedaan wilt maken.")));
		}
		else
		{
			$page->add(PersoonView::overledenForm($pers));
		}

		$page->end();
	}

	static public function pasfotoVerwijderen()
	{
		if (!hasAuth('bestuur'))
		{
			spaceHTTP('403');
		}

		$lidnr = (int)vfsVarEntryName();

		$pers = Persoon::geef($lidnr); //Probeer een lid te krijgen, anders een persoon
		if (!$pers)
		{
			spaceHTTP(403);
		}

		$foto = ProfielFoto::zoekHuidig($pers);
		if (!$foto)
		{
			spaceHttp(403);
		}

		if (Token::processNamedForm() == 'pasfotoVerwijderen')
		{
			$foto = ProfielFoto::zoekHuidig($pers);
			$media = $foto->getMedia();

			$foto->verwijderen();
			$media->verwijderen();
			Page::redirectMelding($pers->url(), _("Pasfoto is met succes verwijderd! Hoezee!"));
		}

		$page = Page::getInstance()->start();
		$page->add(PersoonView::pasfotoVerwijderenForm($pers));
		$page->end();
	}

	static public function stamboom()
	{
		if (!hasAuth('ingelogd'))
		{
			spaceHTTP('403');
		}

		$id = vfsVarEntryName();
		if (!$pers = Lid::geef($id))
		{
			spaceHTTP('403');
		}

		// Als je je eigen stamboom bekijkt krijg je
		// een achievement
		if ($pers == Persoon::getIngelogd())
		{
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('stamboom');
		}

		LidView::stamboom($pers);
	}

	// De controllerfunctie voor het openen van de achievementpagina
	static public function achievements()
	{
		global $BADGES;

		if (!hasAuth('ingelogd'))
		{
			spaceHTTP('403');
		}

		$id = vfsVarEntryName();
		if (!$pers = Lid::geef($id))
		{
			spaceHTTP('403');
		}

		// Probeer iemand een achievement te geven via het form,
		// mits de achievement bestaat en je de juiste auth hebt.
		$form = Token::processNamedForm();
		if ($form == 'deelBadgeUit')
		{
			$badgenaam = tryPar("badgenaam");
			if (array_key_exists($badgenaam, $BADGES["onetime"]))
			{
				$beloonAuth = $BADGES["onetime"][$badgenaam]["beloonbaar"];
				// Als het beloonbaar is en je de juiste auth hebt, geef de achievement.
				if ($beloonAuth && hasAuth($beloonAuth))
				{
					$persBadges = $pers->getBadges();
					$persBadges->addBadge($badgenaam);
				}
			}
		}

		PersoonView::achievements($pers);
	}

	static public function achievementsAantal()
	{
		global $BADGES;

		if (!hasAuth('ingelogd'))
		{
			spaceHttp('403');
		}

		if ($badge = tryPar('details'))
		{
			if (isset($BADGES['onetime'][$badge]))
			{
				PersoonVerzamelingView::achievementsAantalDetails($badge, true);
			}
			else
			{
				PersoonVerzamelingView::achievementsAantalDetails($badge, false, tryPar('level'));
			}
		}
		else
		{
			PersoonVerzamelingView::achievementsAantal();
		}
	}

	/**
	 * Pagina die de duimnagellidfoto van een lid laat zien
	 */
	static public function duimnagel()
	{
		if (!hasAuth('ingelogd'))
		{
			spaceHttp(403);
		}

		$id = vfsVarEntryName();
		if (!$pers = Persoon::geef($id))
		{
			spaceHttp(403);
		}

		$profielfoto = ProfielFoto::zoekHuidig($pers);
		$magZien = $pers->magFotoWordenBekeken();
		if ($profielfoto && $magZien)
		{
			$foto = $profielfoto->getMedia();
			return Media::passthruMediaFile('thumbnail', $foto->geefID());
		}
		else
		{
			$foto = Media::penguinFoto(!$magZien);
			return sendfilefromfs($foto, 'Duimnagel', '/');
		}
	}

	/**
	 * Wijzig de adressen van een contact.
	 */
	static public function wijzigAdres()
	{
		global $BESTUUR;

		/** Laad het gekozen contact **/
		$id = (int)vfsVarEntryName();
		$figuur = Contact::geef($id);

		$view = $figuur->getView();

		if (!$figuur->magWijzigen())
		{
			spaceHttp(403);
		}

		// Clone de adressen van het figuur zodat we later kunnen kijken
		// wat er gewijzigd is. Als we de clones opslaan overschrijven ze
		// de originele entries in de db dus dat goed
		$adressenFiguur = $figuur->getAdressen();

		$adressen = new ContactAdresVerzameling();
		$adressenCur = new ContactAdresVerzameling();

		foreach ($adressenFiguur as $adres)
		{
			$clone = clone($adres);
			$adressen->voegtoe($clone);
			$adressenCur->voegtoe($clone);
		}

		$nieuw = new ContactAdres($figuur);
		$adressen->voegtoe($nieuw);

		/** Buffer voor de output **/
		$fouten = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'Wijzig')
		{
			ContactAdresVerzamelingView::processWijzigAdres($adressen);

			$verschil = array();

			// Bekijk de verschillen
			foreach ($adressenFiguur as $i => $adres)
			{
				$dataadr = tryPar('ContactAdres');

				if ((bool)$dataadr[$adres->geefID()]['verwijder'])
				{
					$verschil['Verwijderd adres ' . ContactAdresView::waardeSoort($adres) . '  #' . $adres->geefID()] =
						array('geheel adres:' => array(ContactAdresView::toString($adres), ''));
				}
				else
				{
					$wijzigingen = $adres->difference($adressen[$i]);

					if ($wijzigingen)
					{
						$verschil['Adres ' . ContactAdresView::waardeSoort($adres) . '  #' . $adres->geefID()] = $wijzigingen;
					}
				}
			}

			if ($adressen->allValid())
			{
				$verschil['Nieuw adres ' . ContactAdresView::waardeSoort($adressen->last())] = array('geheel adres:' => array('', ContactAdresView::toString($adressen->last())));
			}
			elseif (!$adressenCur->allValid() || tryPar('NieuwAdres', null))
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$fouten = true;
			}

			if (!$fouten)
			{
				//Stuur verschillen door naar Secretaris
				if (!empty($verschil) && !hasAuth('bestuur')
					&& $figuur instanceof Persoon
					&& !ContactPersoon::vanPersoon($figuur, "bedrijfscontact")
					&& !ContactPersoon::vanPersoon($figuur, "normaal")) //Negeer wijzigingen van bestuur en hoger, en van sommige CP's
				{
					$verschiltekst = "Geachte Secretaris,\r\n\r\n" . "Van " . PersoonView::naam($figuur) . " (" . get_class($figuur) . " #" . $figuur->geefID() . ") zijn de volgende velden door " . PersoonView::naam(Persoon::getIngelogd()) . " gewijzigd:\r\n\r\n";

					foreach ($verschil as $object => $veld)
					{
						$verschiltekst .= $object . "\r\n";

						foreach ($veld as $naam => $val)
						{
							$verschiltekst .= "\t$naam\r\n\t\tOud:\t" . View::maakString($val[0]) . "\r\n\t\tNieuw:\t" . View::maakString($val[1]) . "\r\n\r\n";
						}
					}
					sendmail(WWWEMAIL, $BESTUUR['secretaris']->getEmail(), "Gegevens gewijzigd", $verschiltekst . "Groetjes, de website");
				}

				// Verwijder de adressen die verwijderd moeten worden
				foreach ($adressenCur as $i => $adres)
				{
					$dataadr = tryPar('ContactAdres');

					if ((bool)$dataadr[$adres->geefID()]['verwijder'])
					{
						$adressen->verwijder($i);
						$returnVal = $adres->verwijderen();
						if (!is_null($returnVal))
						{
							user_error($returnVal, E_USER_ERROR);
						}
					}
				}

				if (tryPar('NieuwAdres', null))
				{
					$adressen->opslaan();
				}
				else
				{
					$adressenCur->opslaan();
				}

				if (!empty($verschil))
				{
					Page::redirectMelding($figuur->url(), _("De gegevens zijn gewijzigd!"));
				}
				else
				{
					Page::redirectMelding($figuur->url(), _("Er is niks gewijzgd!"));
				}
			}
		}

		/** Ga naar de view **/
		ContactAdresVerzamelingView::wijzigAdres($adressen, $fouten);
	}

	static public function systeemAccount()
	{
		requireAuth('bestuur');

		$id = vfsVarEntryName();
		if (!$pers = Persoon::geef($id))
		{
			spaceHttp(403);
		}

		try
		{
			$conn = new SysteemApi();
			$uid = $conn->getUid($pers);
		}
		catch (IPALigtEruitException $e)
		{
			Page::addMelding(_("Kan geen verbinding maken met het systeem."), 'fout');
			$uid = null;
		}

		if (!$uid)
		{
			self::geefSysteemAccount();
		}
		else
		{
			PersoonView::systeemAccount($pers, $uid);
		}
	}

	static public function geefSysteemAccount()
	{
		requireAuth('bestuur');

		$id = vfsVarEntryName();
		if (!$pers = Persoon::geef($id))
		{
			spaceHttp(403);
		}

		if (Token::processNamedForm() == 'GeefSysteemAccount')
		{
			$login = tryPar('login');

			try
			{
				$conn = new SysteemApi();

				if ($conn->aliasCheck($login))
				{
					if (!DEBUG && !DEMO)
					{
						$page = Page::getInstance();
						$page->addFooterJs(Page::minifiedFile('persoonadd', 'js'));
						$page->start(_('Persoonadd'));

						$page->add($div = new HtmlDiv($pers->geefID(), 'persoon-id'));
						$div->setCssStyle('display: none;');
						$page->add($div = new HtmlDiv(tryPar('login'), 'login-name'));
						$div->setCssStyle('display: none;');
						$page->add($div = new HtmlDiv($pers->url(), 'persoonurl'));
						$div->setCssStyle('display: none;');

						$page->add(new HtmlParagraph(null, 'paragraph'));

						$page->add(new HtmlDiv(null, 'rotating-plane'));

						$page->end();
						return;
					}
				}
				else
				{
					Page::addMelding(_('De accountnaam mag niet gebruikt worden'), 'fout');
				}
			}
			catch (IPALigtEruitException $e)
			{
				Page::addMelding(_("Kan geen verbinding maken met het systeem."), 'fout');
			}
		}

		PersoonView::geefSysteemAccount($pers);
	}

	static public function mensjeaddUser()
	{
		requireAuth('bestuur');
		if (DEBUG || DEMO)
		{
			spaceHttp(403);
		}

		if (!$persoon = Persoon::geef(tryPar('id')))
		{
			spaceHttp(403);
		}

		$login = tryPar('login');

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$output = $conn->mensjeaddUser($persoon, $login);

		if (!$output)
		{
			$return = array('status' => 'OK', 'fout' => '');
		}
		else
		{
			$return = array('status' => 'FOUT', 'fout' => $output);
		}
		return new JSONResponse($return);
	}

	static public function mensjeaddHomedir()
	{
		requireAuth('bestuur');
		if (DEBUG || DEMO)
		{
			spaceHttp(403);
		}

		if (!$persoon = Persoon::geef(tryPar('id')))
		{
			spaceHttp(403);
		}

		$login = tryPar('login');

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$output = $conn->mensjeaddHomedir($persoon, $login);

		if (!$output)
		{
			$return = array('status' => 'OK', 'fout' => '');
		}
		else
		{
			$return = array('status' => 'FOUT', 'fout' => $output);
		}
		return new JSONResponse($return);
	}

	static public function mensjeaddPassword()
	{
		requireAuth('bestuur');
		if (DEBUG || DEMO)
		{
			spaceHttp(403);
		}

		if (!$pers = Persoon::geef(tryPar('id')))
		{
			spaceHttp(403);
		}

		$login = tryPar('login');

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$output = $conn->mensjeaddPassword($login);
		return new JSONResponse($output);
	}

	static public function mensjeaddSendmail()
	{
		requireAuth('bestuur');

		if (DEBUG || DEMO)
		{
			spaceHttp(403);
		}

		if (!$pers = Persoon::geef(tryPar('id')))
		{
			spaceHttp(403);
		}

		$login = tryPar('login');

		$page = new Text_Page();
		$page->start();

		sendmail('webcie@a-eskwadraat.nl', 'sysop@a-eskwadraat.nl', 'Nieuw account ' . $login . ' aangemaakt', 'Best Sysop,

Er is door ' . PersoonView::naam(Persoon::getIngelogd()) . ' een nieuw account ' . $login . ' aangemaakt voor het lid ' . PersoonView::naam($pers) . '.

Groetjes
De website');

		$page->end();
	}

	static public function userCheck()
	{
		requireAuth('bestuur');
		if (!$pers = Persoon::geef(tryPar('lidid')))
		{
			spaceHttp(403);
		}

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$uid = $conn->getUid($pers);

		if ($uid)
		{
			$return = array('status' => 'OK', 'lidid' => $pers->geefID(), 'uid' => $uid);
		}
		else
		{
			$return = array('status' => 'FOUT', 'lidid' => $pers->geefID());
		}
		return new JSONResponse($return);
	}

	static public function cieCheck()
	{
		requireAuth('bestuur');

		if (!$pers = Persoon::geef(tryPar('lidid')))
		{
			spaceHttp(403);
		}

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return new JSONResponse(['status' => 'FOUT', 'fout' => 'geen verbinding met LDAP']);
		}

		$cies = $conn->getAllCies($pers);

		return new JSONResponse(array('status' => 'OK', 'cies' => $cies));
	}

	static public function resetWachtwoord()
	{
		requireAuth('bestuur');

		$id = vfsVarEntryName();
		if (!$pers = Persoon::geef($id))
		{
			spaceHttp(403);
		}

		try
		{
			$conn = new SysteemApi();
		}
		catch (IPALigtEruitException $e)
		{
			return Page::responseRedirectMelding($pers->url(),
				_("Kan geen verbinding maken met Het Systeem™."));
		}

		$uid = $conn->getUid($pers);

		if (!$uid)
		{
			spaceHttp(403);
		}

		if (CommissieVerzameling::opSysteemInvloedHebbenden()->hasLid($pers))
		{
			$page = Page::getInstance()->start(_('Mag niet!'));
			$page->add(new HtmlParagraph(_('Omdat dit lid in de sysop, webcie of het bestuur zit '
				. 'mag je het wachtwoord niet resetten!')));
			$page->end();
		}
		else
		{

			$page = Page::getInstance();
			$page->addFooterJs(Page::minifiedFile('resetwachtwoord', 'js'));
			$page->start(sprintf(_('Reset wachtwoord van %s'), PersoonView::makeLink($pers)));

			$page->add($div = new HtmlDiv($pers->geefID(), 'persoon-id'));
			$div->setCssStyle('display: none;');
			$page->add($div = new HtmlDiv($uid, 'login-name'));
			$div->setCssStyle('display: none;');
			$page->add($div = new HtmlDiv($pers->url(), 'persoonurl'));
			$div->setCssStyle('display: none;');

			$page->add(new HtmlParagraph(null, 'paragraph'));

			$page->add($div = new HtmlDiv(null, 'rotating-plane'));
			$div->setCssStyle('display: none;');

			$page->end();
		}
	}

	/**
	 * @site{/Leden/ * /OpgelosteBugs}
	 */
	static public function opgelosteBugs()
	{
		requireAuth('ingelogd');

		$persId = vfsVarEntryName();
		if (!($persoon = Persoon::geef($persId)))
		{
			spaceHttp(403);
		}

		$verz = BugVerzameling::geefOpgelosteBugsVanPersoon($persoon);

		Page::getInstance()
			->addHeadCSS(Page::minifiedFile('style_bugweb.css', 'css'))
			->start()
			->add(BugVerzamelingView::tabel($verz, true, "LaatstGewijzigd"))
			->end();
	}

	/**
	 * @site{/Leden/ * /Exporteer}
	 */
	public static function exporteer()
	{
		// Laad het gekozen lid
		$persoonid = (int)vfsVarEntryName();
		$figuur = Persoon::geef($persoonid);
		// Voor exporteren moet je als jezelf zijn ingelogd,
		// of bestuur zijn. (TODO: of misschien beter godrechten?)
		if (!($figuur && ($figuur == Persoon::getIngelogd() || hasAuth('bestuur'))))
		{
			return responseUitStatusCode(403);
		}

		// Doe dit alleen na een druk op de exportknop,
		// om niet al te veel overbodige downloads te veroorzaken.
		if (Token::processNamedForm("lidexport"))
		{
			$data = $figuur->exportJSON();
			$response = new JSONResponse($figuur->exportJSON());
			// Zorg ervoor dat die gedownload worden.
			$response->headers->set(
				'Content-disposition',
				'attachment; filename=export-lid' . $persoonid . '.json'
			);
			return $response;
		}

		// Geef uitleg over export en een downloadknop.
		$page = PersoonView::pageLinks($figuur);
		$page->start();
		$page->add(new HtmlParagraph(sprintf(
			_("Je kan hier alle gegevens exporteren die %s[VOC: aesnaam] van je heeft."
				. " %s[VOC: 'Op de A-Eskwiki'] staat een beschrijving van het bestandsformaat."
				. " Neem contact op met %s[VOC: 'de WebCie'] als hier nog iets ontbreekt."
				. " Als je veel data hebt, kan het even duren..."),
			aesnaam(),
			new HtmlAnchor(
				"https://mediawiki.a-eskwadraat.nl/wiki/index.php/JSON-export#Format",
				_("Op de A-Eskwiki")
			),
			CommissieView::waardeEmailMailto(Commissie::cieByLogin('webcie'), _("de WebCie"))
		)));
		$page->add(HtmlForm::named("lidexport")
			->add(HtmlInput::makeSubmitButton(_("Geef mij die data!")))
			->add(HtmlAnchor::button($figuur->url(), _("Ga terug")))
		);
		return new PageResponse($page);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
