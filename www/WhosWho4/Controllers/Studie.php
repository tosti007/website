<?php

abstract class Studie_Controller
{

	static public function studieEntry ($args)
	{
		$studie = Studie::geef($args[0]);
		if (!$studie instanceof Studie)
			spaceHttp(403);

		return array( 'name' => $studie->geefID()
					, 'displayName' => StudieView::waardeNaam($studie)
					, 'access' => true);
	}

	static public function studie ()
	{
		$id = (int)vfsVarEntryName();
		$studie = Studie::geef($id);
		$teruglink = new HtmlParagraph(
			HtmlAnchor::button('/Leden/Studie', _('Terug naar de lijst'))
		);
		$div = StudieView::weergaveDiv($studie);
		$page = StudieView::pageLinks($studie);
		$page->start(StudieView::waardeNaam($studie))
			->add($div)
			->add($teruglink)
			->end();
	}

	static public function studieOverzicht ()
	{
		$studies = StudieVerzameling::alleStudies();
		$tabel = StudieVerzamelingView::tabel($studies);
		Page::getInstance()
			->start(_('Studies'))
			->add($tabel)
			->end();
	}

	static public function studiePersonen ()
	{
		$id = (int)vfsVarEntryName();
		$studie = Studie::geef($id);
		$teruglink = new HtmlParagraph(
			HtmlAnchor::button('/Leden/Studie', _('Terug naar de lijst'))
		);
		$personen = $studie->getStudenten();
		$tabel = PersoonVerzamelingView::lijst($personen);
		Page::getInstance()
			->start(_('Studenten'))
			->add($teruglink)
			->add(new HtmlHeader(2, sprintf(_('Studenten %s'), StudieView::waardeNaam($studie))))
			->add($tabel)
			->end();

	}

	static public function studieWijzigen()
	{
		$id = (int)vfsVarEntryName();
		$studie = Studie::geef($id);
		if(!$studie)
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm() == 'wijzigStudie') {
			StudieView::processForm($studie);
			if($studie->valid()) {
				$studie->opslaan();
				Page::redirectMelding($studie->url(), _('Naam gewijzigd!'));
			}
		}

		StudieView::wijzigStudie($studie, $show_error);
	}
}

