<?php
/**
 * /TinyUrl		-> (TinyUrlOverzicht) geeft een overzicht van alle TinyUrls
 *	/id			-> (TinyUrlDetails) geeft informatie over een TinyUrl
 *	  /Wijzig	-> (TinyUrlWijzig) wijzig een TinyUrl
 *	  /Verwijder	-> (TinyUrlVerwijder) verwijder een TinyUrl
 *	/Nieuw		-> (TinyUrlNieuw) maak een nieuwe TinyUrl
 */

abstract class TinyUrl_Controller
{

	static public function tinyUrlOverzicht()
	{
		//Zoek uit hoeveel er eigenlijk zijn, is nodig voor de lijst.
		// Pak vervolgens wat je nodig hebt, zo mogelijk een subset (limit).
		$tinyUrlsTotaal = TinyUrlVerzameling::totaalAantal();
		$tinyUrls = TinyUrlVerzameling::geefAlle(trypar("limit"));
		$page = Page::getInstance();
		$page->start(_("A–Eskwadraat's kleine-URLen-service"));

		$page->add(new HtmlParagraph(_("Hieronder staat een lijst van alle TinyUrls die er in het systeem zitten.
						Je kan ook een nieuwe aanmaken:") . " " . ($a = new HtmlAnchor("Nieuw", _("Nieuw")))))
			->add(limietLinks($tinyUrlsTotaal[0], 50))
			->add(TinyUrlVerzamelingView::lijst($tinyUrls))
			->add(new HtmlHeader(3,_("Wat doen TinyUrls?")) . 
				new HtmlParagraph(_("TinyUrls lijken heel erg op shortcuts. Elke TinyUrl valt onder a-es2.nl/t/*****
						 en stuurt de bezoeker meteen door naar een andere pagina die vrij in te stellen is.
						 Hiermee kunnen bijvoorbeeld moeilijke lange urls verkort worden, kan de doelurl verstopt
						 worden en houdt het systeem bij hoe vaak er op de link is gedrukt.")));

		$page->end();
	}

	/****************************************
	 *	Functies voor losse TinyUrls	*
	 ****************************************/

	static public function tinyUrlDetails()
	{
		$entryData = vfsVarEntryNames();
		$turl = TinyUrl::geef($entryData[0]);

		TinyUrlView::pagelinks($turl);
		TinyUrlView::details($turl);
	}

	static public function tinyUrlWijzig()
	{
		$entryData = vfsVarEntryNames();
		$turl = TinyUrl::geef($entryData[0]);

		TinyUrlView::pagelinks($turl);
		$page = Page::getInstance();

		$show_error = false;

		if (Token::processNamedForm() == "TinyUrlWijzig")
		{
			TinyUrlView::processForm($turl);
			if($turl->valid()){
				$turl->opslaan();
				Page::redirectMelding($turl->url(), _("De TinyUrl is nu gewijzigd!"));
			} else {
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		$page->start(sprintf(_('TinyUrl #%s[VOC:tinyurlid] informatie'), $turl->geefID()))
			->add(TinyUrlView::wijzigForm("TinyUrlWijzig", $turl, $show_error))
			->end();
	}

	static public function tinyUrlNieuw()
	{
		$turl = new TinyUrl();
		$page = Page::getInstance();

		$show_error = false;

		if (Token::processNamedForm() == "TinyUrlNieuw")
		{
			TinyUrlView::processForm($turl);
			if($turl->valid()){
				$turl->opslaan();
				Page::redirectMelding($turl->url()
					, _('De TinyUrl is nu aangemaakt!'));
			} else {
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}
		$page->start(_('Nieuwe TinyUrl'))
			->add(TinyUrlView::wijzigForm("TinyUrlNieuw", $turl, $show_error))
			->end();
	}

	static public function tinyUrlVerwijder()
	{
		$entryData = vfsVarEntryNames();
		$turl = TinyUrl::geef($entryData[0]);

		$page = Page::getInstance();

		/** Buffer voor berichten **/
		$msg = null;

		if (Token::processNamedForm() == "TinyUrlVerwijder")
		{
			$nr = $turl->geefID();
			$returnVal = $turl->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding("/Service/Intern/TinyUrl/", _("TinyUrl #$nr is nu verwijderd!"));
			}
		}

		$page->start();
		TinyUrlView::pagelinks($turl);

		$form = HtmlForm::named("TinyUrlVerwijder");
		$form->add(new HtmlParagraph(sprintf(_("Hiermee verwijder je TinyUrl #%s, weet je dat zeker?"),$turl->geefID())))
			->add(HtmlInput::makeSubmitButton(_("Verwijderen")));

		$page->add($form);

		$page->end();
	}

	public static function hitsOverzicht()
	{
		$entryData = vfsVarEntryNames();
		$tinyUrl = TinyUrl::geef($entryData[0]);

		TinyUrlView::pagelinks($tinyUrl);

		$page = Page::getInstance();
		$page->addFooterJS(Page::minifiedFile('Chart.js'));
		$page->start();

		$dataset = array('Hits gedurende de eerste twee weken' => array_values($tinyUrl->arrayOfHits("eersteweek")));
		$page->add(maakGrafiek(array_keys($tinyUrl->arrayOfHits("eersteweek")), $dataset));
		$dataset = array('Hits gedurende de eerste 35 uur' => array_values($tinyUrl->arrayOfHits("eerstedag")));
		$page->add(maakGrafiek(array_keys($tinyUrl->arrayOfHits("eerstedag")), $dataset));

		$page->end();
	}


	/****************************************
	 *	Algemene functies    		*
	 ****************************************/

	/**
	 *	Check of een TinyUrl wel degelijk bestaat.
	 **/
	public static function checkUrlEntry($args)
	{
		$turl = TinyUrl::geef($args[0]);

		if(!$turl) 
			return false;

		return array('name' => $turl->geefID(),
					'displayName' => $turl->geefID(),
					'access' => true);
	}
}
