<?php
/**
 * /wijzigAdres 	-> wijzigt het adres van het betreffende object
 */

abstract class Contact_Controller
{
	/**
	 * Wijzig de adressen van een contact.
	 */
	static public function wijzigAdres() {
		global $BESTUUR;

		/** Laad het gekozen contact **/
		$id = (int) vfsVarEntryName();
		$figuur = Contact::geef($id);

		$view = $figuur->getView();

		if (!$figuur->magWijzigen()) {
			return responseUitStatusCode(403);
		}

		// Clone de adressen van het figuur zodat we later kunnen kijken
		// wat er gewijzigd is. Als we de clones opslaan overschrijven ze
		// de originele entries in de db dus dat goed
		$adressenFiguur = $figuur->getAdressen();

		$adressen = new ContactAdresVerzameling();
		$adressenCur = new ContactAdresVerzameling();

		foreach($adressenFiguur as $adres) {
			$clone = clone($adres);
			$adressen->voegtoe($clone);
			$adressenCur->voegtoe($clone);
		}

		$nieuw = new ContactAdres($figuur);
		$adressen->voegtoe($nieuw);

		/** Buffer voor de output **/
		$fouten = false;

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'Wijzig')
		{
			ContactAdresVerzamelingView::processWijzigAdres($adressen);

			$verschil = array();

			// Bekijk de verschillen
			foreach($adressenFiguur as $i => $adres) {
				$dataadr = tryPar('ContactAdres');

				if((bool)$dataadr[$adres->geefID()]['verwijder']) {
					$verschil['Verwijderd adres ' . ContactAdresView::waardeSoort($adres) . '  #' . $adres->geefID()] =
						array('geheel adres:' => array(ContactAdresView::toString($adres), ''));
				} else {
					$wijzigingen = $adres->difference($adressen[$i]);

					if($wijzigingen) {
						$verschil['Adres ' . ContactAdresView::waardeSoort($adres) . '  #' . $adres->geefID()] = $wijzigingen;
					}
				}
			}

			if ($adressen->allValid()) {
				$verschil['Nieuw adres ' . ContactAdresView::waardeSoort($adressen->last())] = array('geheel adres:' => array('', ContactAdresView::toString($adressen->last())));
			} elseif(!$adressenCur->allValid() || tryPar('NieuwAdres', null)) {
				Page::addMelding(_("Er waren fouten."), 'fout');
				$fouten = true;
			}

			if(!$fouten) {
				//Stuur verschillen door naar Secretaris
				if(!empty($verschil) && !hasAuth('bestuur') 
					&& $figuur instanceof Persoon
					&& !ContactPersoon::vanPersoon($figuur, "bedrijfscontact")
					&& !ContactPersoon::vanPersoon($figuur, "normaal")) //Negeer wijzigingen van bestuur en hoger, en van sommige CP's
				{
					$verschiltekst = "Geachte Secretaris,\r\n\r\n" . "Van " . PersoonView::naam($figuur) . " (".get_class($figuur)." #"  . $figuur->geefID() . ") zijn de volgende velden door ". PersoonView::naam(Persoon::getIngelogd()) ." gewijzigd:\r\n\r\n";

					foreach($verschil as $object=>$veld){
						$verschiltekst .= $object."\r\n";

						foreach($veld as $naam=>$val){
							$verschiltekst .= "\t$naam\r\n\t\tOud:\t".View::maakString($val[0])."\r\n\t\tNieuw:\t".View::maakString($val[1])."\r\n\r\n";
						}
					}
					sendmail(WWWEMAIL, $BESTUUR['secretaris']->getEmailAdres(), "Gegevens gewijzigd", $verschiltekst . "Groetjes, de website");
				}

				$returnVal = NULL;
				// Verwijder de adressen die verwijderd moeten worden
				foreach($adressenCur as $i => $adres) {
					$dataadr = tryPar('ContactAdres');

					if((bool)$dataadr[$adres->geefID()]['verwijder']) {
						$adressen->verwijder($i);
						$returnVal = $adres->verwijderen();
					}
				}

				if(is_null($returnVal)) {
					if(tryPar('NieuwAdres', null)) {
						$adressen->opslaan();
					} else {
						$adressenCur->opslaan();
					}

					if(!empty($verschil)) {
						return Page::responseRedirectMelding($figuur->url(), _("De gegevens zijn gewijzigd!"));
					} else {
						return Page::responseRedirectMelding($figuur->url(), _("Er is niks gewijzgd!"));
					}
				} else {
					Page::addMeldingArray($returnVal, 'fout');
				}
			}
		}

		/** Ga naar de view **/
		$page = ContactAdresVerzamelingView::wijzigAdres($adressen, $fouten);
		return new PageResponse($page);
	}

	public static function verwijderAdres()
	{
		global $BESTUUR;

		$contact = Contact::geef((int) vfsVarEntryName());
		if (!$contact || !hasAuth('bestuur')) {
			return responseUitStatusCode(403);
		}

		if (Token::processNamedForm() == 'verwijderAdresForm' && $soort = tryPar('soort')) {
			$naam = PersoonView::naam($contact);
			if (!is_null($naam)) {
				// Heb een aanhef "Beste $blabla," bij personen
				// Heb een aanhef "Beste," bij contacten
				$naam = " " . $naam;
			}

			$adres = ContactAdres::geefBySoort($contact, $soort);
			$adrestekst = ContactAdresView::toString($adres);

			$mail = "
== English version below ==

Beste$naam,

Wij hebben recent post van u retour gegekregen. Uw  meest recente adres wat bij ons bekend is, is:

$adrestekst

U kunt uw gegevens aanpassen op http://www.a-eskwadraat.nl" . $contact->url() . ", wij
stellen het op prijs als u dit doet. U kunt ook uw nieuwe adres gegevens
doorgeven door een reply te sturen op deze e-mail.

Met vriendelijke groet,

" . SECRNAME . "
Studievereniging A–Eskwadraat


======================== English ============================

Dear$naam,

Recently we have had some returned mail from you. Your last known address is:

$adrestekst

We would appreciate it if you would update your data. You can do so at
http://www.a-eskwadraat.nl" . $contact->url() . " or by replying this e-mail.

Kind regards,

" . SECRNAME . "
Study association A–Eskwadraat
";
			$returnVal = $adres->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				if ($contact instanceof Persoon) {
					$contact->setOpmerkingen(trim($contact->getOpmerkingen() . "\r\nOud adres: " . $adrestekst));
				}
				sendmail($BESTUUR['secretaris']->getEmail(), $contact->geefID(), "Verkeerde adresgegevens A–Eskwadraat", $mail);
				$contact->opslaan();
				return responseRedirect($contact->url());
			}
		}

		$page = ContactAdresVerzamelingView::verwijderAdres($contact);
		return new PageResponse($page);
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
