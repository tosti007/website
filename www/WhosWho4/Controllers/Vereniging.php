<?php

abstract class Vereniging_Controller
{
	static public function homepagina()
	{
		global $request, $WSW4DB;

		if($ingelogd = Persoon::getIngelogd())
		{
			return self::homepagina_ingelogd($ingelogd);
		}

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('home.css', 'css'));
		$page->addHeadCSS(Page::minifiedFile('timeline.css', 'css'));
		$page->addFooterJS(Page::minifiedFile('home-carousel.js', 'js'));
		$page->addFooterJS(Page::minifiedFile('home-carousel2.js', 'js'));
		$page->addFooterJS(Page::minifiedFile('home2.js', 'js'));
		$page->start('Home');

		$page->add('
<svg width="0" height="0">
  <defs>
    <clipPath id="clipping-left" clipPathUnits="objectBoundingBox">
      <polygon points="0 0, 1 0, 0.86 1, 0 1" />
    </clipPath>
  </defs>
</svg>
<svg width="0" height="0">
  <defs>
    <clipPath id="clipping-middle" clipPathUnits="objectBoundingBox">
      <polygon points="0.11 0, 1 0, 0.89 1, 0 1" />
    </clipPath>
  </defs>
</svg>
<svg width="0" height="0">
  <defs>
    <clipPath id="clipping-right" clipPathUnits="objectBoundingBox">
      <polygon points="0.14 0, 1 0, 1 1, 0 1" />
    </clipPath>
  </defs>
</svg>
		');

		$page->add($divvv = new HtmlDiv(null, null, 'fullpage'));
		$divvv->add($headDiv = new HtmlDiv(null, 'section full-height cover full-window section-header'));

		$headDiv->add($coverImg = new HtmlDiv(null, 'cover-image'));
		$headDiv->add(new HtmlDiv(new HtmlDiv(null, 'progressbar'), 'progressbar-container'));
		$coverImg->add($carousel = new HtmlDiv(null, 'coursel slide', 'myCarousel'));
		$carousel->add($carInner = new HtmlDiv(null, 'carousel-inner'));

		if(DEBUG)
			$directory = '/srv/fotos/Background-Debug/';
		else
			$directory = '/srv/fotos/Background/';

		$scanned_directory = scandir($directory);
		$scanned_directory = array_diff($scanned_directory, array('.', '..'));

		$first = true;
		foreach($scanned_directory as $image) {
			$fotoid = str_replace(".jpg", "", $image);
			$carInner->add($img = new HtmlImage("/Background/$fotoid/", 'home.jpg'));
			$img->setId('large-'.$fotoid);
			$img->addClass('resload');
			$carInner->add($div = new HtmlDiv(null, 'item '.($first ? 'active' : ''), 'small-'.$fotoid));
			$div->setCssStyle("background-image: url(/Background/$fotoid/);");

			if($first) {
				$first = false;
			}
		}

		$headDiv->add($tDiv = new HtmlDiv(null, 'container-fluid'));
		$tDiv->add($div = new HtmlDiv(null, 'content'));
		$div->add($img = new HtmlImage('/Layout/Images/aes2logo-rood.png', 'logo'));
		$img->addClass('logo');
		$div->add($h = new HtmlHeader(2, null));
		$h->add(_('Studievereniging <nobr>A&ndash;Eskwadraat</nobr>'));
		$h->setNoDefaultClasses();
		$div->add(new HtmlSpan(_('Gametechnologie &ndash; Informatica &ndash; Informatiekunde &ndash; Natuurkunde &ndash; Wiskunde')));

		$divvv->add($div = new HtmlDiv(null, 'section'));
		$div->add($nDiv = new HtmLDiv(null, 'text-area div-1'));
		$nDiv->add($container = new HtmlDiv(null, 'container'));
		$container->setCssStyle('height: 100%; display: flex; flex-direction: column;');
		$container->add($row = new HtmlDiv(null, 'row'));
		$row->add($colDiv = new HtmlDiv(null, 'col-sm-12'));
		$colDiv->add($div = new HtmlDiv());
		$div->add($h = new HtmlHeader(1, _('Door het organiseren van activiteiten en het hebben van een gezelligheidskamer brengt A&ndash;Eskwadraat studenten en onderwijs dichter bij elkaar. Ontdek wat A&ndash;Eskwadraat jou als student te bieden heeft!')));
		$h->setNoDefaultClasses();

		$container->add($row = new HtmlDiv(null, 'row icoontjes-row'));
		$row->add($div = new HtmlDiv(null, 'col-md-3 icoontje'));
		$div->add($a = new HtmlAnchor('/Onderwijs/Tentamens', null));
		$a->add('<i class="fa fa-graduation-cap fa-5x"></i>');
		$a->add(new HtmlDiv($h = new HtmlHeader(3, _('Tentamens'))));
		$h->setNoDefaultClasses();
		$row->add($div = new HtmlDiv(null, 'col-md-3 icoontje'));
		$div->add($a = new HtmlAnchor('/Onderwijs/Boeken/', null));
		$a->add('<i class="fa fa-book fa-5x"></i>');
		$a->add(new HtmlDiv($h = new HtmlHeader(3, _('Boeken en dictaten'))));
		$h->setNoDefaultClasses();
		$row->add($div = new HtmlDiv(null, 'col-md-3 icoontje'));
		$div->add($a = new HtmlAnchor('/Activiteiten', null));
		$a->add('<i class="fa fa-calendar fa-5x"></i>');
		$a->add(new HtmlDiv($h = new HtmlHeader(3, _('Activiteitenagenda'))));
		$h->setNoDefaultClasses();
		$row->add($div = new HtmlDiv(null, 'col-md-3 icoontje'));
		$div->add($a = new HtmlAnchor('/Vereniging/Contact', null));
		$a->add('<i class="fa fa-address-card fa-5x"></i>');
		$a->add(new HtmlDiv($h = new HtmlHeader(3, _('Contact'))));
		$h->setNoDefaultClasses();

		$divvv->add($fotosDiv = new HtmlDiv(null, 'section full-height fotosdiv'));

		$fotosDiv->add($left = new HtmlDiv(null, 'foto foto-left hovereffect'));
		$fotosDiv->add($middle = new HtmlDiv(null, 'foto foto-middle hovereffect'));
		$fotosDiv->add($right = new HtmlDiv(null, 'foto foto-right hovereffect'));
		// De url van de clip-path moet inline, anders werkt t niet
		$left->setCssStyle('clip-path: url("#clipping-left");');
		$middle->setCssStyle('clip-path: url("#clipping-middle");');
		$right->setCssStyle('clip-path: url("#clipping-right");');

		$left->add($img = new HtmlImage('/Layout/Images/scholieren.jpg', 'Voor scholieren', _('Voor scholieren'), 'img-responsive'));
		$left->add(new HtmlDiv(null, 'overlay'));
		$left->add(new HtmlAnchor('/Vereniging/Scholieren',
			array($h = new HtmlHeader(2, _('Voor scholieren'))
			, $h2 = new HtmlHeader(3, _('A&ndash;Eskwadraat heeft veel te bieden voor scholieren. Ontdek hier wat A&ndash;Eskwadraat voor jou te bieden heeft.'))
			), null, 'overlay2'));
		$h->setNoDefaultClasses();
		$h2->setNoDefaultClasses();

		$middle->add($img = new HtmlImage('/Layout/Images/bedrijven.jpg', 'Voor bedrijven', _('Voor bedrijven'), 'img-responsive'));
		$middle->add(new HtmlDiv(null, 'overlay'));
		$middle->add(new HtmlAnchor('/Vereniging/Bedrijven',
			array($h = new HtmlHeader(2, _('Voor bedrijven'))
			, $h2 = new HtmlHeader(3, _('A&ndash;Eskwadraat heeft veel te bieden voor bedrijven. Ontdek hier wat A&ndash;Eskwadraat voor jou te bieden heeft.'))
			), null, 'overlay2'));
		$h->setNoDefaultClasses();
		$h2->setNoDefaultClasses();

		$right->add($img = new HtmlImage('/Layout/Images/docenten.jpg', 'Voor docenten', _('Voor docenten'), 'img-responsive'));
		$right->add(new HtmlDiv(null, 'overlay'));
		$right->add(new HtmlAnchor('/Vereniging/Docenten',
			array($h = new HtmlHeader(2, _('Voor docenten'))
			, $h2 = new HtmlHeader(3, _('A&ndash;Eskwadraat heeft veel te bieden voor docenten. Ontdek hier wat A&ndash;Eskwadraat voor jou te bieden heeft.'))
			), null, 'overlay2'));
		$h->setNoDefaultClasses();
		$h2->setNoDefaultClasses();

		$divvv->add($div = new HtmlDiv(null, 'section full-height'));
		$div->add($nDiv = new HtmLDiv(null, 'text-area full-height'));
		$nDiv->add(new HtmlDiv($h = new HtmlHeader(2, _('Vacatures')), 'title'));
		$h->setNoDefaultClasses();
		$nDiv->add($container = new HtmlDiv(null, 'container-fluid'));
		$container->add($row = new HtmlDiv(null, 'row vacature-row'));

		$vacatures = VacatureVerzameling::getRandom();

		foreach($vacatures as $vac)
		{
			$bedrijf = $vac->getContractOnderdeel()->getContract()->getBedrijf();

			$row->add($div = new HtmlDiv($inhoud = new HtmlAnchor($vac->url(), null, null, 'vacature'), 'col-md-3 icoontje vacature-icoontje'));
			$inhoud->add($h = new HtmlHeader(3, VacatureView::waardeTitel($vac)));
			$h->setNoDefaultClasses();

			//$panel->add(VacatureView::waardeInhoud($vac));

			$inhoud->add($img = new HtmlImage($bedrijf->getLogo(), _('bedrijfslogo')));
		}

		$divvv->add($div = new HtmlDiv(null, 'section cover full-height div-4'));
		$div->add($cover = new HtmlDiv(null, 'cover-image'));
		$cover->add(new HtmlDiv(new HtmlDiv(null, 'progressbar'), 'progressbar-container'));
		$cover->add($carousel = new HtmlDiv(null, 'coursel slide', 'myCarousel2'));
		$carousel->add($carInner = new HtmlDiv(null, 'carousel-inner'));

		$carInner->add($img = new HtmlImage('/Layout/Images/home5.jpg', 'home5.jpg'));
		$img->setId('large-5');
		$img->addClass('resload');
		$carInner->add($div2 = new HtmlDiv(null, 'item active', 'small-5'));
		$div2->setCssStyle('background-image: url(\'/Layout/Images/home_lowres5.jpg\');');

		$carInner->add($img = new HtmlImage('/Layout/Images/home6.jpg', 'home6.jpg'));
		$img->setId('large-6');
		$img->addClass('resload');
		$carInner->add($div2 = new HtmlDiv(null, 'item', 'small-6'));
		$div2->setCssStyle('background-image: url(\'/Layout/Images/home_lowres6.jpg\');');

		$carInner->add($img = new HtmlImage('/Layout/Images/home7.jpg', 'home7.jpg'));
		$img->setId('large-7');
		$img->addClass('resload');
		$carInner->add($div2 = new HtmlDiv(null, 'item', 'small-7'));
		$div2->setCssStyle('background-image: url(\'/Layout/Images/home_lowres7.jpg\');');

		$carInner->add($img = new HtmlImage('/Layout/Images/home8.jpg', 'home8.jpg'));
		$img->setId('large-8');
		$img->addClass('resload');
		$carInner->add($div2 = new HtmlDiv(null, 'item', 'small-8'));
		$div2->setCssStyle('background-image: url(\'/Layout/Images/home_lowres8.jpg\');');

		$div->add($grad = new HtmlDiv(null, 'div-4-grad'));
		$grad->setCssStyle('top: 0px;');
		$div->add($nDiv = new HtmlAnchor('/Vereniging/Commissies/Vacatures', null, null, 'text-area'));
		$nDiv->setNoDefaultClasses();
		$nDiv->setCssStyle('position: initial !important');
		$nDiv->add($container = new HtmlDiv(null, 'container'));
		$container->add($row = new HtmlDiv(null, 'row'));
		$row->add($colDiv = new HtmlDiv(null, 'col-md-5 col-md-offset-7 col-sm-6 col-sm-offset-6'));
		$colDiv->add($div = new HtmlDiv());
		$div->add($h = new HtmlHeader(1, _('A&ndash;Eskwadraat organiseert veel activiteiten voor haar leden, mede mogelijk gemaakt door alle commissies die A&ndash;Eskwadraat heeft')));
		$h->setNoDefaultClasses();
		$div->add($h = new HtmlHeader(3, _('Lijkt het je ook leuk om activiteiten te organiseren of om A&ndash;Eskwadraat te organiseren? Neem dan een kijkje bij onze commissievacatures!')));
		$h->setNoDefaultClasses();

		$divvv->add($div = new HtmlDiv(null, 'section full-height'));
		$div->add($nDiv = new HtmLDiv(null, 'text-area full-height'));
		//$nDiv->add(new HtmlDiv($h = new HtmlHeader(2, _('Komende activiteiten')), 'title'));
		//$h->setNoDefaultClasses();
		$nDiv->add($container = new HtmlDiv(null, 'container'));

		$container->setCssStyle('height: 100%');

		$container->add($wrapper = new HtmlDiv(null, 'visible-md visible-lg', 'timeline'));
		$wrapper->setCssStyle('display: none;');

		$wrapper->add(new HtmlDiv($a = new HtmlAnchor('#', new HtmlSpan(null, 'fa fa-chevron-up')), 'prev-arrow'));
		$a->setNoDefaultClasses();
		$a->setId('prev');

		//De derde variabele van tijdsSpan staat standaard op toegang PUBLIEK
		$weekacts = ActiviteitVerzameling::geefEerstAankomendePubliekeActs()->filterBekijken();

		$uldate = new HtmlList(false, null, null, 'dates');
		$uldate->setNoDefaultClasses();

		$ulissues = new HtmlList(false, null, null, 'issues');
		$ulissues->setNoDefaultClasses();

		foreach($weekacts as $act)
		{
			$li = new HtmlListItem(array(/*$h = new HtmlHeader(3, ActiviteitView::titel($act)),*/
				new HtmlAnchor('#' . $act->geefID(), ActiviteitView::waardeMomentBegin($act)),
				$d = new HtmlDiv(null, 'disk')));
			$h->setNoDefaultClasses();

			$li->add(new HtmlDiv(ActiviteitView::titel($act), 'titeldiv'));

			$li->setNoDefaultClasses();
			$uldate->add($li);

			$info = $act->getInformatie();
			$h = new HtmlHeader(3, new HtmlAnchor($act->url(), ActiviteitView::titel($act)));
			$h->setNoDefaultClasses();
			$li = new HtmlListItem(array(new HtmlDiv($h, 'title'),
				new HtmlDiv(new HtmlDiv(new HtmlParagraph(ActiviteitInformatieView::waardeWerftekst($info)), 'panel-body'), 'panel panel-default')));
			$li->setNoDefaultClasses();
			$li->setId($act->geefID());
			$ulissues->add($li);
		}

		$wrapper->add($uldate);
		$wrapper->add($ulissues);

		$wrapper->add(new HtmlDiv($a = new HtmlAnchor('#', new HtmlSpan(null, 'fa fa-chevron-down')), 'next-arrow'));
		$a->setNoDefaultClasses();
		$a->setId('next');

		$container->add($wrapper = new HtmlDiv(null, 'visible-xs visible-sm'));

		//De derde variabele van tijdsSpan staat standaard op toegang PUBLIEK
		$weekacts = ActiviteitVerzameling::geefEerstAankomendePubliekeActs()->filterBekijken();

		$uldate = new HtmlList(false, null, null, 'dates-small');
		$uldate->setNoDefaultClasses();

		$ulissues = new HtmlList();
		$ulissues->setNoDefaultClasses();

		foreach($weekacts as $act)
		{
			$li = new HtmlListItem($a = new HtmlAnchor('#', ActiviteitView::waardeMomentBegin($act)));
			$h->setNoDefaultClasses();

			$a->add(new HtmlDiv(ActiviteitView::titel($act), 'titeldiv'));

			$info = $act->getInformatie();
			$li->add($div = new HtmlDiv(new HtmlDiv(new HtmlParagraph(ActiviteitInformatieView::waardeWerftekst($info)), 'panel-body'), 'panel panel-default act-info'));

			$li->setNoDefaultClasses();
			$uldate->add($li);
		}

		$wrapper->add($uldate);
		//$wrapper->add($ulissues);

		$divvv->add($div = new HtmlDiv(null, 'section'));
		$div->add($nDiv = new HtmLDiv(null, 'text-area'));
		$nDiv->add($container = new HtmlDiv(null, 'container-fluid'));
		$container->add($row = new HtmlDiv(null, 'row'));

		$row->add($ldiv = new HtmlDiv(null, 'col-md-6'));

		$time = new DateTimeLocale();
		if (DEBUG && isset($_GET['date'])) {
			$time = new DateTimeLocale($_GET['date']);
		}
		$timestamp = $time->getTimestamp();

		if ($ingelogd) {
			// Link naar verjaardagenpagina
			global $MAANDEN;
			$ldiv->add(new HtmlDiv(new HtmlHeader(2, sprintf("%s %s",
				new HtmlAnchor('/Leden/Verjaardagen?maand=' . ucfirst($MAANDEN[date('n', $timestamp)]) ."#dag".date('d', $timestamp), _('Vandaag jarig')),
				new HtmlSmall(new HtmlAnchor('/Leden/Verjaardagen?koppenblad=true', _("Bekijk koppenblad")))))
				, 'title'));		

			$vandaagJarig = PersoonVerzameling::jarig();

			if($vandaagJarig->aantal() == 0 && date('m-d', $timestamp) != '02-10')
			{
				$ldiv->add(new HtmlParagraph(_("Niemand!")));
			}
			else
			{
				$ldiv->add($jarigul = new HtmlList());

				if(date('m-d', $timestamp) == '02-10') // De vereniging is jarig!
					$jarigul->addChild(new HtmlParagraph(sprintf('%s (%s)', new HtmlAnchor('/', 'A&ndash;Eskwadraat'), date('Y') - 1971)));

				foreach($vandaagJarig as $jarige)
				{
					if($jarige->magBekijken())
						// Link maken en leeftijd tonen
						$jarigul->addChild(new HtmlParagraph(sprintf('%s (%s)', PersoonView::makeLink($jarige)
							, $jarige->leeftijd())));
					else
						$jarigul->addChild(new HtmlParagraph(new HtmlEmphasis(PersoonView::naam($jarige))));
				}
			}
		}
		
		$row->add($ldiv = new HtmlDiv(null, 'col-md-12'));
		$ldiv->add(new HtmlDiv($h = new HtmlHeader(2, _('Cijfers')), 'title'));
		$nDiv->add($container = new HtmlDiv(null, 'container'));
		
		$row->add($div = new HtmlDiv(null, 'col-sm-3'));
		$div->add($d = new HtmlDiv(null, 'description'));
		$d->add($h = new HtmlHeader(3, _('Leden')));
		$h->setNoDefaultClasses();
		$h->addClass('big-text');
		$aantal = $WSW4DB->q('VALUE SELECT COUNT(*) FROM `Lid` WHERE ' . WSW4_WHERE_NORMAALLID);
		$d->add(new HtmlDiv($aantal, 'number'));

		$row->add($div = new HtmlDiv(null, 'col-sm-3'));
		$div->add($d = new HtmlDiv(null, 'description'));
		$jaar = (int)colJaar();
		$d->add($h = new HtmlHeader(3, sprintf(_('Activiteiten %d-%d'), $jaar-1, $jaar)));
		$h->setNoDefaultClasses();
		$h->addClass('big-text');
		$aantal = $WSW4DB->q('VALUE SELECT COUNT(DISTINCT `activiteitID`) FROM `Activiteit` '
			. 'WHERE momentBegin < %s AND momentBegin > %s AND `toegang` = "PUBLIEK"',
			$jaar ."-8-31", $jaar-1 ."-8-31");
		$d->add(new HtmlDiv($aantal, 'number'));
		
		$row->add($div = new HtmlDiv(null, 'col-sm-3'));
		$div->add($d = new HtmlDiv(null, 'description'));
		$d->add($h = new HtmlHeader(3, _('Actieve commissies')));
		$h->setNoDefaultClasses();
		$h->addClass('big-text');
		$aantal = $WSW4DB->q('VALUE SELECT COUNT(`commissieID`) FROM `Commissie` '
			. 'WHERE soort = "CIE" AND (datumEind IS NULL OR datumEind >= NOW())');
		$d->add(new HtmlDiv($aantal, 'number'));

		$row->add($div = new HtmlDiv(null, 'col-sm-3'));
		$div->add($d = new HtmlDiv(null, 'description'));
		$d->add($h = new HtmlHeader(3, _('Foto\'s')));
		$h->setNoDefaultClasses();
		$h->addClass('big-text');
		$aantal = $WSW4DB->q('VALUE SELECT COUNT(`mediaID`) FROM `Media` WHERE `soort` = "FOTO"');
		$d->add(new HtmlDiv($aantal, 'number'));

		return new PageResponse($page);
	}

	private static function homepagina_ingelogd($ingelogd)
	{
		global $request;

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('home.css', 'css'));
		$page->addFooterJS(Page::minifiedFile('home.js', 'js'));
		$page->start('Home');

		// Content boven mededelingen
		$page->add($rowdiv = new HtmlDiv(null, 'row'));
		$coldiv1 = new HtmlDiv(null, "col-md-6");

		// Laat persoonlijke mededelingen zien
		$coldiv1->add(self::showMededelingen());

		// Voeg een random vacature toe
		$coldiv1->add(self::randomVacature());

		// Zoek de tijd uit - mogelijk uit de GET!
		$time = new DateTimeLocale();
		if (DEBUG && $request->query->get('date')) {
			$time = new DateTimeLocale($request->query->get('date'));
		}
		$timestamp = $time->getTimestamp();

		// show aan komende activiteiten
		$coldiv2 = self::showAankomendeActiviteiten($timestamp, $time);
		//show iba
		self::showIBA($coldiv2);
		// toon jarigen
		$coldiv2->add(self::showVandaagJarig(new HtmlDiv(), $ingelogd, $timestamp, $time, 'HtmlTable', 'htmlTableContainer'));

		// Toevoegen aan de pagina
 		$rowdiv->add($coldiv2);
 		$rowdiv->add($coldiv1);

		return new PageResponse($page);
	}

	static public function showJumbotron(){
		$div = new HtmlDiv(null, "jumbotron jumbotron-main");
		$div->add($img = HtmlSpan::fa('chevron-up', _('Verberg'), 'jumbotron-arrow text-muted'));
		$div->setCssStyle('background-color: #ddd; margin-top: 20px;');
		$div->add($textdiv = new HtmlDiv(null, 'jumbotron-home'));
		$textdiv->add(new HtmlHeader(1, _('Welkom bij A&ndash;Eskwadraat')));

		$textdiv->add(new HtmlParagraph(
			_('A&ndash;Eskwadraat is de vereniging voor studenten van de studies Gametechnologie, Informatica, Informatiekunde, Natuurkunde en Wiskunde aan de Universiteit Utrecht.')
		));

		// Handige links
		$div->add(new HtmlParagraph(array(
			HtmlAnchor::button('/Boeken', _('Boekenafdeling'), null, 'btn-lg', null, 'danger'),
			HtmlAnchor::button(TENT_HOME, _('Oefententamens'), null, 'btn-lg', null, 'danger'),
			HtmlAnchor::button('/Carriere/Vacaturebank/', _('Vacaturebank'), null, 'btn-lg', null, 'danger'),
			HtmlAnchor::button('/Activiteiten/Fotos', _('Foto\'s'), null, 'btn-lg', null, 'danger'),
			HtmlAnchor::button('/Activiteiten', _('Activiteitenagenda'), null, 'btn-lg', null, 'danger')
		), 'button-home btn-group'));
		return $div;
	}

	static public function showIBA($coldiv2)
	{
		$coldiv2->add(new HtmlHeader(3, $a = new HtmlAnchor('https://iba.a-eskwadraat.nl', _('IBA-blog'))));
		$a->addClass("ibabloglink");
		$coldiv2->add(new HtmlDiv(new HtmlDiv(new HtmlEmphasis(
				sprintf(_('Op de IBA-blog leggen de verschillende commissies van IBA verscheidenen '
						. 'technische aspecten uit die we bij %s gebruiken.'), aesnaam())
			), 'panel-body'), 'panel panel-default'));
		$coldiv2->add($ibablog = new HtmlDiv(null, 'ibablog'));
		$ibablog->add($div = new HtmlDiv($span = HtmlSpan::fa('circle-o-notch', _('Wachten'), 'fa-spin fa-3x fa-fw')));
		$span->setCssStyle('color: black !important;');
		$div->setCssStyle('text-align: center;');
	}

	static private function dateCalculation($timestamp){
		// $curdag loopt van ma==0 t/m vr==4, en za/zo (5, 6)
		$curday = (date('w', $timestamp)+6) % 7;
		// zet startday in op maandag
		$startday = strtotime("-$curday days", $timestamp);
		// als zaterdag of zondag
		if( $curday >= 5 ) {
			// print tekst 'komende week', en de acts van maandag t/m za/zo
			$startday = strtotime("+7 days", $startday);
			$kopje = _('Komende week %s');
		} else {
			// anders, print tekst 'deze week', en de acts van ma t/m zo
			$kopje = _('Deze week %s');
		}
		// we doen een t/m zonder tijd, dus slechts 6 en niet 7 dagen optellen
		$endday = strftime("%Y-%m-%d", strtotime('+6 days', $startday));
		$startday = strftime("%Y-%m-%d", $startday);

		return array('curday' => $curday, 'startday' => $startday, 'endday' => $endday, 'kopje' => $kopje);
	}

	static public function showAankomendeActiviteiten($timestamp, $time, $css = ""){
		$r = self::dateCalculation($timestamp);

		//De derde variabele van tijdsSpan staat standaard op toegang PUBLIEK
		$weekacts = ActiviteitVerzameling::tijdsSpan($r['startday'], $r['endday'])->filterBekijken();

		// cache
		$cieacts = CommissieActiviteitVerzameling::vanActiviteiten($weekacts);
		$cieacts->toCommissieVerzameling();

		// 'deze week <a>te doen</a>' of 'komende week <a>te doen</a>'
		$coldiv2 = new HtmlDiv(null, 'col-md-6');
		$coldiv2->add(new HtmlHeader(3, sprintf($r['kopje'], new HtmlAnchor("/Activiteiten", _('te doen')))));

		// Overzicht van activiteiten in eigen tabelletje
		$tbl_acts = new HtmlTable();

		$coldiv2->add($tbl_acts);
		if($weekacts->aantal() == 0) {
			$tbl_acts->addRow()->addData(new HtmlEmphasis(_("geen activiteiten...")));
		} else {
			$prevdagnaam = '';
			foreach($weekacts as $act)
			{
				$begin = $act->getMomentBegin();
				$eind = $act->getMomentEind();
				$tbl_acts->add($curr_tr = new HtmlTableRow());

				$curr_tr->add($td_dag = new HtmlTableDataCell());
				$td_dag->setAttribute("title", $begin->strftime('%e %b %Y'));
				$dagnaam = $begin->strftime('%a');
				if($begin->strftime('%a') != $eind->strftime('%a'))
					$dagnaam = '<span style="white-space: nowrap;">' . $begin->strftime('%a') . "-" . $eind->strftime('%a') . "</span>";
				if($dagnaam == $prevdagnaam) {
					$td_dag->add('&nbsp;');
				} else {
					//Als vandaag za of zo is, worden de activiteiten van aankomende week laten zien, dus hoeft het niet onderstreept te worden
					$td_dag->add($dagnaam);
					if($time->format('Y-m-d') === $begin->format('Y-m-d') && $r['curday'] <= 4) {
						$td_dag->addClass('huidigedag');
					}
				}

				$td_act = $curr_tr->addData();
				$td_act->add(new HtmlAnchor($act->url(), str_cut(ActiviteitView::titel($act), 30)));

				if($act->getActsoort() == "AES2") {
					$td_act->setAttribute("colspan", 2);
				} else {
					// Extra kolom met een icoontje
					$td_acttype = $curr_tr->addData();
					$td_acttype->addClass("actsoort actsoort_".strtolower($act->getActSoort()));
					$td_acttype->setAttribute("title", ActiviteitView::soortNaam($act));
					$td_acttype->add("&#" . ActiviteitView::soortSymbool($act) . ";");
				}
				$prevdagnaam = $dagnaam;
			}
		}

		return $coldiv2;
	}

	static public function showMededelingen(){
		// Relevante mededelingen ophalen uit de database
		$mededelingen = MededelingVerzameling::huidige();
		if($mededelingen->aantal() > 0) {
			$div_mdd = new HtmlDiv();
			$div_mdd->add(new HtmlHeader(3, _('Nieuws')));
			$div_mdd->add(MededelingVerzamelingView::maakOverzicht($mededelingen));
			return $div_mdd;
		} else {
			return new HtmlEmphasis(_('Er is op dit moment geen nieuws!'));
		}
	}

	static public function showVandaagJarig($jarigdiv, $ingelogd, $timestamp, $time, $parentContainer = 'HtmlList', $childContainer = 'htmlListItemContainer', $css = ""){
		$vandaagJarig = PersoonVerzameling::jarig();

		if ($ingelogd) {
			// Link naar verjaardagenpagina
			global $MAANDEN;
			$jarigdiv->add(new HtmlHeader(3, sprintf("%s %s",
				new HtmlAnchor('/Leden/Verjaardagen?maand=' . ucfirst($MAANDEN[date('n', $timestamp)]) ."#dag".date('d', $timestamp), _('Vandaag jarig')),
				new HtmlSmall(new HtmlAnchor('/Leden/Verjaardagen?koppenblad=true', _("Bekijk koppenblad"))))));
		} else {
			// Geen link naar verjaardagen
			$jarigdiv->add(new HtmlHeader(3, _('Vandaag jarig')));
		}

		if(date('m-d', $timestamp) == '02-10') {
			// de vereniging is jarig!
			$jarigdiv->add(new HtmlHeader(4, 
				new HtmlAnchor('/', 'A&ndash;Eskwadraat'),
				' (' . (date('Y') - 1971) . ')'
			));
		}

		if($vandaagJarig->aantal() == 0) {
			$jarigdiv->add(_("Niemand!"));
		} else {
			$jarigdiv->add($jarigul = new $parentContainer());
			if($css){
				$jarigul->addClass($css);
			}

			foreach($vandaagJarig as $jarige)
			{
				if($jarige->magBekijken()) {
					// Link maken en leeftijd tonen
					$jarigul->add(self::$childContainer(array(
						PersoonView::makeLink($jarige)
						, ' ('.$jarige->leeftijd().')'
						, new HtmlBreak())));
				} else {
					$jarigul->add(self::$childContainer(new HtmlEmphasis(PersoonView::naam($jarige))));
				}
			}
		}

		return $jarigdiv;
	}

	static private function htmlListItemContainer($data){
		return new HtmlListItem($data);
	}

	static private function htmlTableContainer($data){
		return new HtmlTableRow(new HtmlTableDataCell($data));
	}

	static public function ibablog()
	{
		$page = Page::getInstance('cleanhtml')->start();
		//RSS for IBA blog
		$xml=('https://iba.a-eskwadraat.nl/feed');

		$xmlDoc = new DOMDocument();

		if(@$xmlDoc->load($xml) !== false) {

			$page->add($rssdiv = new HtmlDiv());
			$channel=$xmlDoc->getElementsByTagName('channel')->item(0);

			//get and output "<item>" elements
			$x=$xmlDoc->getElementsByTagName('item');

			$rssdiv->add($rsstable = new HtmlTable());
			$limit = 3;

			for ($i=0; $i<$limit; $i++) {
				$item_title=$x->item($i)->getElementsByTagName('title')
					->item(0)->childNodes->item(0)->nodeValue;
				$item_link=$x->item($i)->getElementsByTagName('link')
					->item(0)->childNodes->item(0)->nodeValue;
				$rsstable->add(new HtmlTableRow($cell=new HtmlTableDataCell(new HtmlHeader(4, new HtmlAnchor($item_link, $item_title)))));
				if($i == 0) {
					$item_desc=$x->item($i)->getElementsByTagName('description')
						->item(0)->childNodes->item(0)->nodeValue;
					$cell->add(new HtmlParagraph($item_desc));
				}
			}
		}
		$page->end();
	}

	static public function randomVacature()
	{
		$vacatures = VacatureVerzameling::metConstraints();
		// Engelstaligen kunnen geen Nederlands maar wel andersom
		if (getLang() == "en") {
			$vacatures = $vacatures->filter("heeftInleiding", true);
		}
		$vacatures->shuffle();
		$vacature = $vacatures->first();

		$div = new HtmlDiv();
		$div->add(new HtmlHeader(3, new HtmlAnchor("/Carriere/Vacaturebank/" . $vacature->geefID(), VacatureView::waardeTitel($vacature))));

		$div->add($divRow = new HtmlDiv(null, 'row'));
		$divRow->add($divOmschrijving = new HtmlDiv(null, 'col-md-8'));
		$divRow->add($divLogo = new HtmlDiv(null, 'col-md-4'));

		$bedrijf = Bedrijf::vanVacature($vacature);

		$divOmschrijving->add(VacatureView::waardeInleiding($vacature));
		$divLogo->add(new HtmlAnchor("/Carriere/Vacaturebank/" . $vacature->geefID(),
			$img = new HtmlImage($bedrijf->getLogo(), $bedrijf->getNaam(), 'Bedrijfslogo', 'bedrijfslogo')));
		$img->setCssStyle('max-width: 100%');
		$img->setLazyLoad();

		$div->add(new HtmlHeader(4, sprintf(_("Wil je meer vacatures bekijken? Kijk dan %s."),
			new HtmlAnchor("/Carriere/Vacaturebank/", _("hier")))));

		return $div;
	}
}
