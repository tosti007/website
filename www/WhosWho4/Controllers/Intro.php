<?php
/**
 * /intro			-> (main) index-pagina
 */
abstract class Intro_Controller
{
	/**
	 * @site {/Leden/Intro/Aanmelden}
	 */
	public static function externAanmelden()
	{
		if (DEBUG && tryPar('debugmodus'))
		{
			global $request;
			//Debug request aangezien we niet zelf bij het form kunnen.
			$dataArr = [];
			$dataArr['geslacht'] = 'M';
			$dataArr['email'] = 'bob-jan@witbaard.nl';
			$dataArr['voorletters'] = 'B';
			$dataArr['achternaam'] = 'Witbaard';
			$dataArr['straat'] = 'bleekstraat';
			$dataArr['postcode'] = 'm';
			$dataArr['plaats'] = 'Witbaarder';
			$dataArr['mobiel'] = '4231';
			$dataArr['telefoonnummer'] = '1233423';
			$dataArr['kamp'] = '1';
			$dataArr['vega'] = '0';

			$request->request->add($dataArr);
		}

		$email = tryPar('email', null);
		if (empty($email))
		{
			$result = [
				'status' => 'error',
				'errors' => ['Email' => 'Vul een e-mailadres in.'],
			];
			return new JSONResponse($result);
		}
		elseif (Contact::zoekOpEmail(tryPar('email')))
		{
			$result = [
				'status' => 'error',
				'errors' => ['Email' => 'Dit e-mailadres bestaat al in ons systeem.'],
			];
			return new JSONResponse($result);
		}

		//Zoek uit of we dingen met de reservelijst moeten doen voordat
		// we potentieel dingen met deze inschrijving gaan doen.
		$kampgangers = IntroDeelnemerVerzameling::alleBijnaLeden("true");
		$reservelijst = false;
		$reservelijst = $kampgangers->aantal() >= MAXAANTALKAMP;

		/* Buffer voor berichten **/
		$form = true;
		$msg = null;

		/* Verwerk formulier-data **/
		$lid = new Lid();
		$lid->requireEmail()
			->setVoornaam(tryPar('voornaam'))
			->setVoorletters(tryPar('voorletters'))
			->setTussenvoegsels(tryPar('tussenvoegsels'))
			->setAchternaam(tryPar('achternaam'))
			->setVoornaamwoord(Voornaamwoord::uitGeslacht(tryPar('geslacht')))
			->setDatumGeboorte(tryPar('geboortedatum'))
			->setEmail(tryPar('email'))
			->setStudentnr(tryPar('studentnummer'))
			->setLidVan(EJBV);

		$adres = new ContactAdres($lid);
		$adres->setStraat1(tryPar('straat'))
			->setHuisnummer(tryPar('huisnummer'))
			->setPostcode(tryPar('postcode'))
			->setWoonplaats(tryPar('plaats'))
			->setLand(tryPar('land'));

		$tel1 = new ContactTelnr($lid);
		$tel1->setSoort('MOBIEL')
			->setTelefoonnummer(tryPar('mobiel'));

		$tel2 = new ContactTelnr($lid);
		$tel2->setSoort('OUDERS')
			->setTelefoonnummer(tryPar('telefoonnummer'));

		$studieVerzameling = new LidStudieVerzameling();
		$studies = (is_array(tryPar('studie'))) ? tryPar('studie') : [];
		foreach ($studies as $wat)
		{
			$studie = new LidStudie($lid, true);
			$studie->setStudie(Studie::zoek($wat, 'BA'));
			$studieVerzameling->voegtoe($studie);
		}

		$deel = new IntroDeelnemer($lid);
		$deel->setKamp(tryPar('kamp'))
			->setVega(tryPar('vega'))
			->setAllergie(strlen(tryPar('allergie')) > 2)
			->setAllergieen(tryPar('allergie'))
			->setVoedselBeperking(tryPar('voedselbeperking'))
			->setIntroOpmerkingen(tryPar('opmerkingen'))
			->setMagischeCode(hash("sha256", Crypto::getRandomBytes(42)));

		if (!$lid->valid() || !$adres->valid() || !$tel1->valid() || !$tel2->valid() || !$studieVerzameling->allValid() || !$deel->valid())
		{
			$tel_errors = [];
			if (!$tel1->valid())
			{
				$tel_errors['Mobiel'] = 'dit is een verplicht veld';
			}

			if (!$tel2->valid())
			{
				$tel_errors['Nood-nummer'] = 'dit is een verplicht veld';
			}

			$errors = array_filter(array_merge(
				$lid->getErrors(),
				$adres->getErrors(),
				$tel_errors,
				$studieVerzameling->allErrors(),
				$deel->getErrors()
			));

			$result = [
				'status' => 'error',
				'errors' => $errors,
			];
			return new JSONResponse($result);
		}

        //Moet deze persoon op de reservelijst komen? Zet er dan een opmerking bij.
        if ($reservelijst && $deel->getKamp())
        {
            $deel->setIntroOpmerkingen("*Staat op de reservelijst* " . $deel->getIntroOpmerkingen());
        }

        $adressen = $lid->getAdressen();
        $adressen->voegtoe($adres);
        $telnrs = $lid->getTelefoonnummers();
        $telnrs->voegtoe($tel1);
        $telnrs->voegtoe($tel2);

        $lid->setVakidOpsturen(true);
        $lid->setOpzoekbaar('A');
        $lid->setInAlmanak(true);
        $lid->opslaan();

        $studieVerzameling->opslaan();
        $deel->opslaan();

        $mailings = new ContactMailingListVerzameling();
        $mailings->voegtoe(new ContactMailingList($lid->geefID(), 1)); //Stage en banen
        $mailings->voegtoe(new ContactMailingList($lid->geefID(), 7)); //Activiteiten NL
        $mailings->voegtoe(new ContactMailingList($lid->geefID(), 9)); //Boekenmail

        $huidigeMailings = MailingListVerzameling::vanContact($lid);
        $wilMailingKeys = [];
        foreach ($huidigeMailings as $huidigeMailing)
        {
            $wilMailingKeys[] = [$lid->geefID(), $huidigeMailing->geefID()];
        }

        //Filter de mailings die je al hebt er uit als dat mogelijk is.
        if ($wilMailingKeys)
        {
            $mailings->complement(ContactMailingListVerzameling::verzamel($wilMailingKeys));
        }

        $mailings->opslaan();

        //Als de deelnemer niet op kamp wil kunnen we de mail dat de inschrijving voltooid is nu al sturen.
        //anders wordt deze pas gestuurd wanneer de iDeal betaling is voltooid.
        if(!$deel->getKamp()) {
            IntroDeelnemer::introMail($lid, $deel, $reservelijst);


            if (strlen($deel->getIntroOpmerkingen()) > 1) {
                sendmail('www-data@a-eskwadraat.nl', 'bachelor@intro-utrecht.nl', 'Inschrijving [' . $lid->geefID() . '] heeft vragen/opmerkingen',
                        'Beste intro,' . "\n\n" .
                        'Er heeft zich iemand ingeschreven met nog een vraag/opmerking:' . "\n" .
                        $deel->getIntroOpmerkingen() . "\n\n" .
                        'Handelen jullie het verder af?' . "\n\n" .
                        'Met vriendelijke groeten,' . "\n" .
                        'De introsmurf', $lid->getEmail());
            }
        }

        $result = [
            'status' => 'ok',
            'inschrijfcode' => $deel->getMagischeCode(),
        ];
        return new JSONResponse($result);
    }

    /**
     * @brief Begin een iDealbetaling voor het kamp.
     *
     * Heeft twee get-parameters:
     *  * cleanhtml: (bool) of de iDealtransactie op een CleanPage getoond wordt
     *  * inschrijfcode: (string) de magische code van de deelnemer die wil betalen.
     *
     * @site{/Leden/Intro/iDeal}
     */
    public static function iDeal()
    {
        global $logger;

        $deelnemer = IntroDeelnemer::geefUitMagischeCode(tryPar('inschrijfcode'));
        if (!$deelnemer || $deelnemer->getBetaald() != "NEE")
        {
            return responseUitStatusCode(403);
        }

        // We hebben het lid nodig om de lidstatus aan te passen na afloop.
        $lid = $deelnemer->getLid();

        $voorraad = Voorraad::geef(Register::getValue('introKampVoorraad'));
        if (!$voorraad)
        {
            $logger->error('Registerwaarde "introKampVoorraad" is geen geldig Voorraad-id!');
            die("Er is iets misgegaan, neem contact op met de WebCie");
        }
        $verkoopPrijs = $voorraad->getVerkoopPrijs()->getPrijs();

        $ideal = new iDeal("BOEKWEB", "VERKOOP", $lid, $verkoopPrijs,
                "Introductiekamp", new DateTimeLocale(), "iDealtransactie introkamp"
                , $voorraad, EVENEMENTENREKENING, null);

        $ideal->setEmailAdres($lid->getEmail());
        $ideal->setStatus('INACTIVE');
        $ideal->setReturnURL('http://bachelor.intro-utrecht.nl/?succes=kamp');

        $ideal->startTransactie();
	}

	/**
	 * Toont statistieken over komende introdeelnemers
	 */
	public static function main()
	{
		$kamp = tryPar("kamp", 'void');
		$vega = tryPar("vega", 'void');
		$aanwezig = tryPar("aanwezig", 'void');
		$allergie = tryPar("allergie", 'void');
		$betaald = tryPar("betaald", 'ALLES');
		$opmvar = tryPar("opmvar", '1');
		$opmerk = tryPar("opmerk", 'void');
		$algopm = tryPar("algopm", 'void');

		$deelnemers = IntroDeelnemerVerzameling::alleBijnaLeden($kamp, $vega, $aanwezig, $allergie, $betaald, $opmerk);

		$output = new HtmlDiv();
		$output->add($form = new HtmlForm('get'));
		$form->addClass('form-inline');
		$form->add(IntroDeelnemerVerzamelingView::shortForm($kamp, $vega, $aanwezig, $allergie, $betaald, $opmvar, $opmerk));
		$output->add(new HtmlParagraph(('(Toon Deelnemers heeft alleen functionaliteit als Javascript niet aan staat)')))
			->add(new HtmlHeader(4, "Ga naar de " . new HtmlAnchor("Inschrijfmodus", "Inschrijfmodus")))
			->add(IntroDeelnemerVerzamelingView::toonDeelnemers($deelnemers, $opmvar));

		$page = Page::getInstance()
			->setBrand('intro')
			->start(_("Introsmurf"), 'intro')
			->add(new HtmlSpan([
				new HtmlSpan(new HtmlStrong(_("Toevoegen")))
				, HtmlAnchor::button("NieuweDeelnemer", "Deelnemer")
				, HtmlAnchor::button("NieuweDeelnemerMaandag", "Deelnemer op Maandag")
				, HtmlAnchor::button("NieuweCluster", "Cluster")
				, HtmlAnchor::button("NieuweMentorGroep", "Mentorgroep")
				, new HtmlBreak()
				, HtmlAnchor::button("IntroToCSV", "CSV")
				, HtmlAnchor::button("Inschrijfmodus", "Inschrijfmodus"),
			]))
			->add(IntroDeelnemerVerzamelingView::introstats())
			->add($output)
			->end();

	}

	public static function IntrotoCSV()
	{

		header("Content-Disposition: attachment; filename=IntroDeelnemers.csv");
		header("Content-type: text/csv; encoding=utf-8");

		//Retourneert de Lid-objecten van alle introdeelnemers
		$deelnemers = IntroDeelnemerVerzameling::alleBijnaLeden();
		$csvarray = [];
		foreach ($deelnemers as $deel)
		{
			$persoon = Persoon::geef($deel->geefID());
			$data = PersoonView::alsBonusCSVArray($persoon);
			//De volgende velden worden niet geset in het inschrijfformulier dus hoeven niet in de csv, lidvan is nutteloze data.
			unset($data['overleden'], $data['bijnaam'], $data['titelsPrefix'], $data['titelsPostfix'], $data['GPGkey'], $data['geboortenamen'], $data['Rekeningnummer'], $data['@foto'], $data['lidvan'], $data['straat2']);

			//Introdeelnemerdata
			$data['Mentorgroep'] = '';
			$lid = $deel->getLid();
			if (!!$lid)
			{
				$groepen = $lid->mentorgroepen();
				if (!is_null($groepen->last()))
				{
					$data['Mentorgroep'] = IntroGroepView::waardeNaam($groepen->last());
				}
			}
			$data['Aanwezig'] = IntroDeelnemerView::waardeAanwezig($deel);
			$data['Op kamp'] = IntroDeelnemerView::waardeKamp($deel);
			$data['Vega'] = IntroDeelnemerView::waardeVega($deel);
			$data['Betaald'] = IntroDeelnemerView::waardeBetaald($deel);
			$data['tel ouders'] = ContactTelnrView::waardeTelefoonnummer(ContactTelnrVerzameling::vanContact($persoon, "OUDERS")->first());
			$data['Allergie'] = IntroDeelnemerView::waardeAllergie($deel);
			$data['Allergie Opmerking'] = IntroDeelnemerView::waardeAllergieen($deel);
			$data['Voedsel Beperkingen'] = IntroDeelnemerView::waardeVoedselBeperking($deel);
			$data['Algemene Opmerkingen'] = IntroDeelnemerView::waardeIntroOpmerkingen($deel);

			$csvarray[] = $data;
		}

		//Schrijf direct alles weg (niet stdout).
		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		if (count($csvarray) > 0)
		{
			fputcsv($output, array_keys($csvarray[0]));
		}
		else
		{
			fwrite($output, _("Geen data."));
		}

		//Print alle inhoud voor de CSV
		foreach ($csvarray as $per)
		{
			fputcsv($output, $per);
		}

		fclose($output);
		exit();
	}

	public static function bekijkDeel(IntroDeelnemer $deel)
	{
		$page = IntroDeelnemerView::pageLinks($deel);
		$page
			->setBrand('intro')
			->start(_("Introsmurf"), 'intro')
			->add(IntroDeelnemerView::bekijkDeel($deel))
			->end();
	}

	public static function deelEntry($arg)
	{
		$deel = IntroDeelnemer::geef((int) $arg[0]);
		if (!$deel instanceof IntroDeelnemer)
		{
			return false;
		}

		return ['name' => $deel->geefID()
			, 'displayName' => PersoonView::naam(Persoon::geef($deel->geefID()))
			, 'access' => true,
		];
	}

	public static function deelDetails()
	{
		self::bekijkDeel(IntroDeelnemer::geef((int) vfsVarEntryName()));
	}

	public static function nieuweDeelnemer()
	{
		$show_error = false;

		$lid = new Lid();
		$adres = new ContactAdres($lid);
		$tel1 = new ContactTelnr($lid, 'THUIS');
		$tel2 = new ContactTelnr($lid, 'OUDERS');
		$deel = new IntroDeelnemer($lid);
		$stu1 = new LidStudie($lid, true);
		$stu2 = new LidStudie($lid, true);
		$stu3 = new LidStudie($lid, true);

		if (Token::processNamedForm() == 'nieuweDeelnemer')
		{
			IntroDeelnemerView::processNieuwForm($deel, $adres, $tel1, $tel2, $stu1, $stu2, $stu3);
			if ($deel->valid() && $lid->valid() && $adres->valid() && $tel1->valid())
			{
				$lid->opslaan();
				$deel->opslaan();
				$adres->opslaan();
				$tel1->opslaan();
				$stu1->opslaan();
				if ($stu2->getStudie())
				{
					$stu2->opslaan();
				}
				if ($stu3->getStudie())
				{
					$stu3->opslaan();
				}
				if ($tel2->getTelefoonnummer() !== "")
				{
					$tel2->opslaan();
				}
				IntroDeelnemer::introMail($lid, $deel);
				Page::redirect($deel->url(), 0);
				$form = false;
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}
		$page = IntroDeelnemerView::toevoegen($deel, $adres, $tel1, $tel2, $stu1, $stu2, $stu3, $show_error);
		return new PageResponse($page);
	}

	public static function nieuweDeelnemerMaandag()
	{
		$show_error = false;

		$lid = new Lid();
		$deel = new IntroDeelnemer($lid);
		$stu1 = new LidStudie($lid, true);
		$stu2 = new LidStudie($lid, true);
		$stu3 = new LidStudie($lid, true);
		$tel1 = new ContactTelnr($lid, 'THUIS');
		$tel2 = new ContactTelnr($lid, 'OUDERS');

		if (Token::processNamedForm() == 'nieuweDeelnemerMaandag')
		{
			IntroDeelnemerView::processMaandagNieuwForm($lid, $deel, $stu1, $stu2, $stu3, $tel1, $tel2);

			$groep = IntroGroep::geef(tryPar('introgroepen'));

			if (!$groep)
			{
				Page::addMelding(_('Vul een correct mentorgroepje in!'), 'fout');
			}

			if ($deel->valid() && $lid->valid() && $tel1->valid() && $groep)
			{
				$deel->setAanwezig(true);
				$lid->opslaan();
				$lid->defaultLidInstelling();
				$deel->opslaan();
				$tel1->opslaan();
				$stu1->opslaan();
				if ($stu2->getStudie())
				{
					$stu2->opslaan();
				}
				if ($stu3->getStudie())
				{
					$stu3->opslaan();
				}
				if ($tel2->getTelefoonnummer() !== "")
				{
					$tel2->opslaan();
				}

				$naamgroep = IntroGroepView::waardeNaam($stu1->getGroep());
				if ($naamgroep == "")
				{
					$tekst = " is aangemaakt en als aanwezig aangevinkt.";
				}
				else
				{
					$tekst = " is aangemaakt, in mentorgroep " . $naamgroep . " gestopt en als aanwezig aangevinkt.";
				}
				Page::redirectMelding($deel->url(), new HtmlAnchor($deel->url(), PersoonView::naam($lid)) . $tekst);

			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		$page = Page::getInstance();
		$page->setBrand('intro');
		$page->start();
		$page->add(IntroDeelnemerView::maandagForm($lid, $deel, $tel1, $tel2, $show_error));
		$page->end();
	}

	public static function toonIntroMail()
	{
		$page = Page::getInstance()
			->setBrand('intro')
			->start(_("Introsmurf"), 'intro')
			->add(new HtmlSpan(IntroDeelnemer::introMail(null, null)))
			->end();
	}

	public static function wijzigDeel()
	{
		requireAuth('intro');

		/* Laad het gekozen introlid **/
		$lidid = (int) vfsVarEntryName();
		$lid = Lid::geef($lidid); // Zelfs bijnaleden zijn lidobjecten...
		$deel = IntroDeelnemer::geef($lidid);

		$show_error = false;

		/* Verwerk formulier-data **/

		if (Token::processNamedForm() == 'DeelWijzig')
		{
			IntroDeelnemerView::processWijzigForm($lid, $deel);
			if ($lid->valid() && $deel->valid())
			{
				$lid->opslaan();
				$deel->opslaan();
				return Page::responseRedirectMelding('/Leden/Intro/Deelnemer/' . $lidid, _("De gegevens zijn gewijzigd! Je wordt doorgestuurd."));
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		/* Ga naar de view **/
		return new PageResponse(IntroDeelnemerView::wijzig($lid, $deel, $show_error));
	}

	public static function verwijderDeel()
	{
		$deelID = (int) vfsVarEntryName();
		$deel = IntroDeelnemer::geef($deelID);

		switch (Token::processNamedForm())
		{
		case 'IntroDeelnemerVerwijderen':
			$lid = $deel->getLid();
			$naam = PersoonView::naam($lid);

			$returnVal = $lid->verwijderen(); //De lidverwijderfunctie gooit ook de introdeelnemer weg
			if (!is_null($returnVal))
				{
				Page::addMeldingArray($returnVal, 'fout');
			}
				else
				{
				Page::redirectMelding("/Leden/Intro/ToonIntrodeelnemers", '"' . $naam . _('" is verwijderd uit de Databaas.'));
			}
			return;
		case NULL:
		default:
			break;
		}

		$page = IntroDeelnemerView::pageLinks($deel);
		$page->setBrand('intro');
		$page->start();
		$page->add(IntroDeelnemerView::verwijderForm($deel));
		$page->end();
	}
	public static function nieuweCluster()
	{
		requireAuth('intro');
		$show_error = true;

		/* Verwerk formulier-data **/
		$newClu = new IntroCluster();

		if (Token::processNamedForm() == 'nieuweCluster')
		{
			IntroClusterView::processNieuwForm($newClu);
			if ($newClu->valid('set'))
			{
				$newClu->opslaan();
				$id = $newClu->geefID();
				Page::redirect("/Leden/intro/Cluster/$id/", 0);
				$form = false;
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}

		/* Ga naar de view **/
		IntroClusterView::toevoegen($newClu, $show_error);
	}

	public static function clusterEntry($arg)
	{
		$cluster = IntroCluster::geef($arg[0]);
		if (!$cluster instanceof IntroCluster)
		{
			return false;
		}

		if (!$cluster->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		return ['name' => $cluster->geefID()
			, 'displayName' => $cluster->getNaam()
			, 'access' => true,
		];
	}

	public static function clusterDetails()
	{
		$cluster = IntroCluster::geef((int) vfsVarEntryName());

		if (!$cluster->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		return self::bekijkCluster($cluster);
	}

	protected static function bekijkCluster(IntroCluster $clu)
	{
		$page = IntroClusterView::pageLinks($clu);
		$page
			->setBrand('intro')
			->start(_("Introsmurf"), 'intro')
			->add(IntroClusterView::bekijkCluster($clu))
			->end();
	}

	public static function wijzigCluster()
	{
		requireAuth('intro');

		$cluid = (int) vfsVarEntryName();
		$clu = IntroCluster::geef($cluid);

		if (!$clu->magWijzigen())
		{
			return responseUitStatusCode(403);
		}

		$editClu = clone $clu;

		$msg = null;
		$form = true;
		$errors = false;

		IntroClusterView::processWijzigForm($editClu);
		if (Token::processNamedForm() == 'WijzigClu')
		{
			if ($editClu->valid())
			{
				$clu = $editClu;
				$clu->opslaan();
				Page::redirect('/Leden/intro/', 0);
				$msg = new HtmlSpan(_("De cluster is nu gewijzigd! Je wordt doorgestuurd."), 'text-success strongtext');
				$form = false;
			}
			else
			{
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
			}
		}

		return new PageResponse(IntroClusterView::wijzig($editClu, $form, $msg));
	}

	public static function verwijderCluster()
	{
		$cluid = (int) vfsVarEntryName();
		$clu = IntroCluster::geef($cluid);

		$msg = null;

		if ((bool) tryPar('cluid', false))
		{
			$clu->verwijderen();
			$msg = new HtmlSpan(_("De cluster is nu verwijderd!"), 'text-success strongtext');
			$clu = null;
		}
		elseif (!is_null(tryPar('check')))
		{
			$msg = new HtmlSpan(_("U wordt teruggestuurd naar de cluster!"));
			Page::redirect('../' . $cluid, 0);
		}
		IntroClusterView::verwijderCluster($clu, $msg);
	}

	/**
	 * Pagina voor de mentorgroepjes van een gegeven jaar.
	 * Parameters worden eerst nog ge-trypar'd als ze null zijn.
	 * @param jaar Het jaar om te bekijken, of null voor de recentste.
	 * @param gesorteerd Of de mentorgroepjes verder gesorteerd moeten worden.
	 */
	public static function bekijkjaar(int $jaar = null, bool $gesorteerd = null)
	{
		if (is_null($jaar))
		{
			$jaar = tryPar('jaar', max(IntroClusterVerzameling::jarenMetCluster()));
		}
		if (is_null($gesorteerd))
		{
			$gesorteerd = tryPar('gesorteerd', false);
		}

		$clusters = IntroClusterVerzameling::getClusterDitJaar($jaar);
		$output = new HtmlDiv();
		$output->add($form = new HtmlForm('get'));

		$form->add(IntroClusterVerzamelingView::shortForm($jaar, $gesorteerd));

		$page = Page::getInstance()
			->setBrand('intro')
			->start(_("Introsmurf"), 'intro')
			->add($output)
			->add(IntroClusterVerzamelingView::bekijkClusters($clusters, $gesorteerd))
			->add(new HtmlSpan(_('De mentorgroeplozen:')
				. new HtmlBreak()
				. IntroDeelnemerVerzamelingView::bekijkMentorGroepLozen($jaar)))
			->end();
	}

	public static function groepEntry($arg)
	{
		$grp = IntroGroep::geef((int) $arg[0]);

		if (!$grp instanceof IntroGroep)
		{
			return false;
		}

		if (!$grp->magBekijken())
		{
			return false;
		}

		return ['name' => $grp->geefID()
			, 'displayName' => $grp->getNaam()
			, 'access' => true,
		];
	}

	public static function groepDetails()
	{
		$grp = IntroGroep::geef((int) vfsVarEntryName());

		if (!$grp->magBekijken())
		{
			return responseUitStatusCode(403);
		}

		return new PageResponse(IntroGroepView::toonGroep($grp));
	}

	public static function verwijderGroep()
	{
		$grpid = (int) vfsVarEntryName();
		$grp = IntroGroep::geef($grpid);

		if (!$grp->magVerwijderen())
		{
			return responseUitStatusCode(403);
		}

		$msg = null;

		if ((bool) tryPar('grpid', false))
		{
			$grp->verwijderen();
			$msg = new HtmlSpan(_("De groep is nu verwijderd!"), 'text-success strongtext');
			$grp = null;
		}
		elseif (!is_null(tryPar('check')))
		{
			$msg = new HtmlSpan(_("U wordt teruggestuurd naar de groep!"));
			Page::redirect('../' . $grpid, 0);
		}
		IntroGroepView::verwijderGroep($grp, $msg);
	}

	public static function nieuweGroep()
	{
		requireAuth('intro');

		$show_error = false;

		$newGroep = new IntroGroep();

		if (Token::processNamedForm() == 'WijzigGrp')
		{
			IntroGroepView::processNieuwForm($newGroep);
			if ($newGroep->valid('set'))
			{
				$newGroep->opslaan();

				$mentoren = LidVerzameling::verzamel(array_keys(array_filter(trypar("Mentoren", []))));
				$kindjes = LidVerzameling::verzamel(array_keys(array_filter(trypar("Kindjes", []))));
				$loners = LidVerzameling::verzamel(array_keys(array_filter(trypar("Loners", []))));
				$kindjes->union($loners);

				foreach ($mentoren as $mentorPersoon)
				{
					$mentor = new Mentor($mentorPersoon, $newGroep);
					$mentor->opslaan();
				}
				foreach ($kindjes as $kindje)
				{
					$studies = $kindje->getStudies(true);
					foreach ($studies as $studie)
					{
						//Dit gaat fout als we een masterintroweb hebben (2x per jaar!)
						if ($studie->getDatumBegin()->format('Y') == date('Y'))
						{
							$studie->setGroep($newGroep->geefID());
							$studie->opslaan();
						}
					}
				}
				Page::redirect($newGroep->url(), 0);
			}
			else
			{
				Page::addMelding(_("Er waren fouten."), 'fout');
				$show_error = true;
			}
		}
		$page = IntroGroepView::wijzig($newGroep, true, $show_error);
		return new PageResponse($page);
	}

	public static function wijzigGroep()
	{
		requireAuth('intro');

		$grpid = (int) vfsVarEntryName();
		$grp = IntroGroep::geef($grpid);
		$grpjaar = IntroClusterView::waardeJaar($grp->getCluster());

		if (!hasAuth('bestuur') && $grpjaar != date('Y'))
		{
			spaceHttp(403);
		}

		$editGrp = clone $grp;

		$msg = null;
		$errors = false;

		if (Token::processNamedForm() == 'WijzigGrp')
		{
			IntroGroepView::processWijzigForm($editGrp);

			if ($editGrp->valid())
			{
				$grp = $editGrp;

				$cluster = $grp->getCluster();
				$jaar = $cluster->getJaar();

				//Dump de mentoren en kinderen
				foreach ($grp->mentorVerzameling() as $m)
				{
					$m->verwijderen();
				}
				$kindjes = $grp->kindjes();
				foreach ($kindjes as $kindje)
				{
					$studies = $kindje->getStudies();
					foreach ($studies as $studie)
					{
						if ($studie->getGroepGroepID() == $grp->getGroepID())
						{
							$studie->setGroep(null);
							$studie->opslaan();
						}
					}
				}
				//En zet daarna de aangevinkte weer terug!
				$mentoren = LidVerzameling::verzamel(array_keys(array_filter(trypar("Mentoren", []))));
				$kindjes = LidVerzameling::verzamel(array_keys(array_filter(trypar("Kindjes", []))));
				$loners = LidVerzameling::verzamel(array_keys(array_filter(trypar("Loners", []))));
				$kindjes->union($loners);

				foreach ($mentoren as $mentorPersoon)
				{
					$mentor = new Mentor($mentorPersoon, $grp);
					$mentor->opslaan();
				}
				foreach ($kindjes as $kindje)
				{
					$studies = $kindje->getStudies();
					foreach ($studies as $studie)
					{
						if ($studie->getDatumBegin()->format('Y') == $jaar)
						{
							$studie->setGroep($grp->geefID());
							$studie->opslaan();
						}
					}
				}

				$grp->opslaan();

				Page::redirect($grp->url(), 0);
				$msg = new HtmlSpan(_("De groep is nu gewijzigd! Je wordt doorgestuurd."), 'text-success strongtext');
			}
			else
			{
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
			}
		}

		return new PageResponse(IntroGroepView::wijzig($editGrp, false, $msg));
	}

	public static function inschrijfmodus()
	{
		if (Token::processNamedForm() == "aanwezig")
		{
			$deelnemer = IntroDeelnemer::geef(trypar("lidnr"));
			$deelnemer->setAanwezig(1);

			if (tryPar('submit') == 'Aanwezig + Gepind')
			{
				$deelnemer->setBetaald('PIN');
			}

			$deelnemer->opslaan();

			Page::redirect("#{$deelnemer->geefID()}", 0);
		}

		$opmvar = tryPar("opmvar", '1');

		$deelnemers = IntroDeelnemerVerzameling::alleBijnaLeden();

		$page = Page::getInstance();
		$page->start("Inschrijfmodus");
		$page->setBrand('intro');
		$page->add(new HtmlParagraph(sprintf(_("Zoek iemand op met <code>Ctrl + f</code> en druk op het lidnummer om iemand op aanwezig te zetten. Of terug naar %s."),
			new HtmlAnchor("ToonIntrodeelnemers", _("het overzicht")))));
		$page->add(IntroDeelnemerVerzamelingView::toonDeelnemers($deelnemers, $opmvar, true));
		$page->end();
	}
}
