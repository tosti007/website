<?php

abstract class Docuweb_Controller
{
	/*
	 *  Controllerfunctie voor het overzicht van docuweb
	 *
	 * @site{/Vereniging/Docu/index.html}
	 */
	static public function home()
	{
		global $WSW4DB;

		// Alleen leden mogen dit zien
		if(!hasAuth('lid'))
			return false;

		$categorie = tryPar('cat', null);
		if($categorie)
			$categorie = DocuCategorie::geef($categorie);

		$cats = array( '' => 'Alle');
		$res = DocuCategorieVerzameling::getCategorieen();
		foreach($res as $cat) {
			$cats[$cat->getId()] = $cat->getOmschrijving();
		}

		$limit = tryPar('limit', '0_0');
		$offset = explode('_', $limit);
		if($offset[0] < 0)
			$offset[0] = 0;

		$docs = DocuBestandVerzameling::getBestanden($categorie);

		// Stuur alles naar de view toe
		DocuBestandVerzamelingView::overzicht($categorie, $cats, $docs, $offset[0]);
	}

	/**
	 * @site{/Vereniging/Docu/ * /}
	 */
	static public function docuEntry($args)
	{
		$id = $args[0];
		if($docu = DocuBestand::geef($id))
			$naam = $docu->getTitel();
		else
			return false;
		if(hasAuth('lid'))
			return array('name' => $id,
				'displayName' => $naam,
				'access' => true);
		else
			return false;
	}

	/*
	 *  Controllerfunctie voor het ophalen van een bestand
	 *
	 * @site{/Vereniging/Docu/ * /index.html}
	 */
	static public function bekijk()
	{
		global $filesystem;

		$id = vfsVarEntryName();
		if(!$docu = DocuBestand::geef($id))
			return false;

		// Als men een bestand bekijkt krijgt men een achievement
		if($pers = Persoon::getIngelogd()) {
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('docuweb');
		}

		$bestand = $docu->getBestand();
		if($filesystem->has($bestand)) {
			return sendfilefromfs($bestand);
		} else {
			// Als het bestand niet geopend kan worden laat dan een
			// nette foutmelding zien
			$page = Page::getInstance()->start(_("Fout!"));
			$page->add(new HtmlDiv(_("Er is een fout opgetreden bij het "
				."ophalen van het bestand! Onze excuses voor het ongemak, De "
				."Webcie."), 'alert alert-danger'));
			$page->end();
		}
	}

	//TODO: deze functie samenvoegen met Docu_Controller:toevoegen
	/*
	 *  Controllerfunctie voor het wijzigen van een bestand
	 *
	 * @site{/Vereniging/Docu/ * /Wijzig}
	 */
	static public function wijzig()
	{
		global $filesystem;

		if(!hasAuth('bestuur'))
			return false;

		$id = vfsVarEntryName();
		if(!$docu = DocuBestand::geef($id))
			return false;

		$show_error = false;

		if(Token::processNamedForm() == 'DocuWijzig') {
			DocuBestandView::processForm($docu);

			$cat = DocuCategorie::geef(tryPar("categorie"));

			if(!$cat) {
				Page::addMelding(_("Categorieid onbekend!"), 'fout');
				$show_error = true;
			} else {
				$docu->setCategorie($cat);
			}

			// Als er een bestand is geupload doe dingen
			if($_FILES["bestand"]["tmp_name"] && !$show_error) {
				if(!is_uploaded_file($_FILES["bestand"]['tmp_name'])) {
					Page::addMelding(_("Het uploaden van het bestand is niet goed gegaan!"), 'fout');
					$show_error = true;
				} elseif(!$show_error) {
					// zet de extensie goed
					$extensieArray = explode('.', $_FILES["bestand"]["name"]);
					$extensie = array_pop($extensieArray);
					$docu->setExtensie($extensie);
				}

				if($docu->valid() && !$show_error) {
					// sla op waar nodig
					$docu->opslaan();

					if(!DEBUG && !DEMO) {
						$stream = fopen($_FILES['bestand']['tmp_name'], 'r+');
						if ($filesystem->has($docu->getBestand())) {
							$filesystem->updateStream($docu->getBestand(), $stream);
						} else {
							$filesystem->writeStream($docu->getBestand(), $stream);
						}
						fclose($stream);
					}

					Page::redirectMelding(DOCUBASE, _("Wijzigen gelukt!"));
				}
			} elseif($docu->valid() && !$show_error) {
				$docu->opslaan();
				Page::redirectMelding(DOCUBASE, _("Wijzigen gelukt!"));
			} else {
				Page::addMelding(_("Er waren fouten"), 'fout');
				$show_error = true;
			}
		}

		// Stuur alles naar de view toe
		DocuBestandView::wijzigForm($docu, false, $show_error);
	}

	//TODO: deze functie samenvoegen met Docu_Controller:wijzigen
	/*
	 *  Controllerfunctie voor het toevoegen van een nieuw bestand
	 *
	 * @site{/Vereniging/Docu/Toevoegen}
	 */
	static public function toevoegen()
	{
		global $filesystem;

		if(!hasAuth('bestuur'))
			return false;

		$show_error = false;
		$docu = new DocuBestand();

		if(Token::processNamedForm() == 'DocuWijzig') {
			// verwerk het formulier
			DocuBestandView::processForm($docu);

			$cat = DocuCategorie::geef(tryPar("categorie"));

			if(!$cat) {
				Page::addMelding(_("Categorieid onbekend!"), 'fout');
				$show_error = true;
			} else {
				$docu->setCategorie($cat);
			}

			// als het uploaden van het bestand goed gegaan is zetten
			// we de extensie goed
			if(!is_uploaded_file($_FILES["bestand"]['tmp_name']) && !$show_error) {
				Page::addMelding(_("Het uploaden van het bestand is niet goed gegaan!"), 'fout');
				$show_error = true;
			} else {
				$extensieArray = explode('.', $_FILES["bestand"]["name"]);
				$extensie = array_pop($extensieArray);
				$docu->setExtensie($extensie);
			}

			if($docu->valid() && !$show_error) {
				// sla het nieuwe object goed op
				$docu->opslaan();
				$id = $docu->geefID();

				if(!DEBUG && !DEMO) {
					$stream = fopen($_FILES['bestand']['tmp_name'], 'r+');
					$filesystem->writeStream($docu->getBestand(), $stream);
					fclose($stream);
				}
				Page::redirectMelding(DOCUBASE, _("Toevoegen gelukt!"));
			} else {
				Page::addMelding(_("Er waren fouten"), 'fout');
				$show_error = true;
			}
		}

		// stuur alles door naar de view
		DocuBestandView::wijzigForm($docu, true, $show_error);
	}

	/*
	 *  Controllerfunctie voor het verwijderen van een docubestand
	 *
	 * @site{/Vereniging/Docu/ * /Verwijder}
	 */
	static public function verwijder()
	{
		if(!hasAuth('bestuur'))
			return false;

		$id = vfsVarEntryName();
		if(!$docu = DocuBestand::geef($id))
			return false;

		if(Token::processNamedForm() == "DocuBestandVerwijderen") {
			// verwijder het object uit de db
			$returnVal = $docu->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(DOCUBASE, _("Verwijderen gelukt!"));
			}
		}

		$page = Page::getInstance()->start(sprintf(_("Verwijderen van %s"), 
			DocuBestandView::waardeTitel($docu)));
		$page->add(DocuBestandView::verwijderForm($docu));
		$page->end();
	}

	/*
	 *  Controllerfunctie voor het categorieoverzicht
	 *
	 * @site{/Vereniging/Docu/Categorie}
	 */
	static public function categorie()
	{
		if(!hasAuth('bestuur'))
			return false;

		$msg = null;

		// Als iemand een nieuwe categorie toevoegd verwerken we hier een 
		// formulier
		if(Token::processNamedForm() == 'nieuw') {
			$cat = new DocuCategorie();
			$cat->setOmschrijving(tryPar('omschrijving', null));
			if($cat->valid()) {
				$cat->opslaan();
				Page::redirectMelding(DOCUBASE . "/Categorie", _("Toevoegen gelukt!"));
			} else {
				$msg = _("Kan niets toevoegen zonder naam.");
			}
		}

		$cats = DocuCategorieVerzameling::getCategorieen();

		// pomp alles naar de view
		DocuCategorieVerzamelingView::overzicht($cats, $msg);
	}
}
