<?php

/**
 * $Id$
 *
 *  /                                   -> home()
 *  /contactID                          -> donateur()
 *      /Wijzig                         -> wijzig()
 *      /Verwijder                      -> verwijder()
 *  /Huidige                            -> huidige()
 *	    /Adreslijst                     -> adresLijstHuidige()
 *  /Verslagen/verslag                  -> adresLijstVerslag()
 *  /Factureren                         -> facturen()
 *  /Statistieken                       -> statistieken()
 */
abstract class DonateurController
{
	public static function home()
	{
		$page = Page::getInstance();
		$page->start(_('Donateurs'));

		if(hasAuth('bestuur'))
		{
			$stats = Donateur::statistieken();
			$huidig = $stats['huidig'];

			$page->add(new HtmlParagraph(_('Op deze pagina kun je lijsten opvragen van'
					.' alle donateurs en zij die bepaalde voorwerpen willen'
					.' ontvangen. Het aanmaken van een nieuwe donateur gaat via de lidpagina.')));
			$page->add(new HtmlDiv(new HtmlDiv($list = new HtmlList(), 'col-md-4'), 'row'));
			$list->addChild(new HtmlAnchor('Huidige', sprintf(_("Alle %d huidige donateurs"), $huidig)) . ' ('. new HtmlAnchor('Huidige/Adreslijst', _('Adreslijst')). ')');
			$list->addChild(new HtmlAnchor('Allemaal', _('Alle (oud) donateurs')));

			$list = new HtmlList();
			foreach (DonateurVerzameling::ontvangerSoorten() as $id => $arr)
			{
				$list->add(new HtmlAnchor("Verslagen/".$id, $arr['naam'])
							. " (". $arr['verzameling']->aantal() .")");
			}
			$page->add(
				  new HtmlParagraph(array(
					  _('Adressen van donateurs die het volgende willen:')
					, $list))
				, new HtmlDiv(array(
					  HtmlAnchor::button('Factureren', _('Factuur-TeXinput'))
					, HtmlAnchor::button('Statistieken', _('Statistieken'))))
				);
		}

		$page->end();
	}

	public static function donateur()
	{
		$dona = Donateur::geef(vfsVarEntryName());
		if(!$dona) spaceHTTP(404);

		$page = DonateurView::pageLinks($dona);

		$page->setLastMod($dona);
		$page->start(PersoonView::naam($dona->getPersoon()));
		$page->add(DonateurView::viewInfo($dona, true)
				 , HtmlAnchor::button('..', _('Donateurshome'))
				 );
		$page->end();
	}

	public static function wijzig()
	{
		$opgeslagen = false;

		$dona = Donateur::geef(vfsVarEntryName());
		if(!$dona) {
			$dona = new Donateur(vfsVarEntryName());
		}
		$persoon = $dona->getPersoon();

		$show_error = false;

		if (Token::processNamedForm() == 'DonaWijzig')
		{
			DonateurView::processForm($dona);
			$ander = $dona->getAnderPersoon();
			if (!is_null($ander) && ($anderDona = Donateur::geef($ander->getContactID())) !== NULL && $anderDona != $dona) {
				Page::addMelding(_('Je tweede persoon is al een donateur (geweest), ruim dit eerst op.'), 'fout');
			} else {
				if($dona->valid())
				{
					$dona->opslaan();
					$opgeslagen = true;
				}
				else
				{
					$show_error = true;
					Page::addMelding(_('Er waren fouten.'), 'fout');
				}
			}
		}

		if($dona->getInDB()) {
			$header = _('Donateursgegevens wijzigen');
			$knop   = _('Wijzig');
		}
		else {
			$header = sprintf(_('%s donateur maken')
						, PersoonView::naam($persoon));
			$knop   = _('Aanmaken');
		}

		$page = Page::getInstance();

		if($opgeslagen) {
			$page->addMelding(_('De gegevens zijn successvol gewijzigd.'), 'succes');
		}

		$page->setLastMod($dona);
		$page->start($header);

		$page->add(HtmlAnchor::button($persoon->url(), sprintf(_('Naar lidpagina van %s[VOC: lidnaam]'), PersoonView::naam($persoon))));
		$page->add(HtmlAnchor::button('/Leden/Donateurs/Huidige', _('Naar donateuroverzicht')));
		$page->add(new HtmlBreak());
		$page->add(new HtmlBreak());

		$form = HtmlForm::named('DonaWijzig');
		$form->setAttribute('onsubmit', 'return Form_onDonaWijzigSubmit(this);');

		$form->add(DonateurView::createForm($dona, $show_error, 'viewWijzig')
				 , HtmlInput::makeFormSubmitButton($knop));
		$page->add($form);

		$page->end();
	}

	public static function verwijder()
	{
		/** Verwerk get-data **/
		$donaid = (int) vfsVarEntryName();
		$dona = Donateur::geef($donaid);

		switch(Token::processNamedForm()) 
		{
			case 'DonateurVerwijderen':
				$naam = DonateurView::waardePersoon($dona);
				$returnVal = $dona->verwijderen();//weghalen die hap
				if(!is_null($returnVal)) {
					Page::addMeldingArray($returnVal, 'fout');
				} else {
					Page::redirectMelding("/Leden/Donateurs/", sprintf(_("De donateursgegevens van %s zijn verwijderd uit de Databaas."), $naam));
				}
				return;
			case NULL:
			default:
				break;
		}

		$page = Page::getInstance();
		$page->start();
		$page->add(DonateurView::verwijderForm($dona));
		$page->end();
	}

	public static function huidige()
	{
		$donas = DonateurVerzameling::huidige();
		
		$csvEditie = (bool)tryPar("submitCSV", false);

		if($csvEditie) {
			$csv = DonateurVerzamelingView::toCSV($donas);
			$csv->send();
			return;
		}

		$page = Page::getInstance();
		$page->start(_("Donateurs"));
		$page->add(new HtmlParagraph(
			sprintf(_('In totaal %d donateurs gevonden.'), $donas->aantal())
				. ' (' . new HtmlAnchor('Adreslijst', _('adreslijst')) . ')'
				. ' (' . new HtmlAnchor('?submitCSV=1', _('CSV Export')) . ')'
			)
			, DonateurVerzamelingView::viewWiewatTable($donas)
		);
		$page->end();
	}
	public static function allemaal()
	{
		$donas = DonateurVerzameling::allemaal();

		$page = Page::getInstance();
		$page->start();
		$page->add(new HtmlHeader(2, _('Donateurs'))
			, new HtmlParagraph(
				sprintf(_('In totaal %d donateurs gevonden.'), $donas->aantal())
				)
			, DonateurVerzamelingView::viewWiewatTable($donas)
			);
		$page->end();
	}

	private static function adresLijstTexOutput($donas)
	{
		$output = DonateurVerzamelingView::viewWiewatTex($donas);

		$page = Page::getInstance('text');
		$page->start();
		$page->add($output);
		$page->end();
	}
	public static function adresLijstHuidige()
	{
		self::adresLijstTexOutput(DonateurVerzameling::huidige());
	}
	public static function adresLijstAllemaal()
	{
		self::adresLijstTexOutput(DonateurVerzameling::allemaal());
	}
	public static function adresLijstVerslag()
	{
		$soort = vfsVarEntryName();
		$ontvangers = DonateurVerzameling::ontvangerSoorten();
		if (!array_key_exists($soort, $ontvangers))
		{
			// Wordt ook gecheckt in verslagEntry.
			return responseUitStatusCode(403);
		}
		return self::adresLijstTexOutput($ontvangers[$soort]['verzameling']);
	}

	public static function donateurEntry($args)
	{
		$dona = Donateur::geef($args[0]);
		if(!$dona) return False;

		$p = $dona->getPersoon();

		return array( 'name' => $p->getContactID()
					, 'displayName' => PersoonView::naam($p)
					, 'access' => hasAuth('bestuur')
					);
	}

	public static function verslagEntry($args)
	{
		$verslag = $args[0];
		$verslagen = DonateurVerzameling::ontvangerSoorten();

		if(!array_key_exists($verslag, $verslagen))
		{
			// Deze check wordt ook voor de zekerheid nog eens gedaan in
			// adresLijstVerslag
			return false;
		}

		return array( 'name' => $verslag
					, 'displayName' => 'Verslagen'
					, 'access' => hasAuth('bestuur')
					);
	}

	public static function statistieken()
	{
		$page = Page::getInstance();
		$page->start(_('Statistieken'));

		$stats = Donateur::statistieken();
		$rs = array();

		$i = 0;
		$tbl = array();
		$tbl[$i  ][0] = _('Aantal donateurs:');
		$tbl[$i++][1] = $stats['huidig'];
		$tbl[$i  ][0] = _('Aantal samendonerenden:');
		$tbl[$i++][1] = $stats['samen'];
		$tbl[$i  ][0] = _('Totaal donatie:');
		$tbl[$i++][1] = Money::addPrice($stats['tot']);
		$tbl[$i  ][0] = _('Gemiddelde donatie:');
		$tbl[$i++][1] = Money::addPrice($stats['gem']);
		$tbl[$i  ][0] = _('Donatieinterval:');
		$tbl[$i++][1] = Money::addPrice($stats['min'])
						.' - '.Money::addPrice($stats['max'], false);
		for($i = 0; $i < count($tbl); $i++) {
			$tbl[$i][1] = new HtmlTableDataCell($tbl[$i][1], 'right');
		}
		$page->add(new HtmlParagraph($table = HtmlTable::fromArray($tbl)));

		$table = new HtmlTable();
		foreach (DonateurVerzameling::ontvangerSoorten() as $key => $ontvangers)
		{
			$row = $table->addRow();
			$row->addData($ontvangers['naam']);
			$row->addData($ontvangers['verzameling']->aantal(), 'right');
		}
		$page->add(new HtmlParagraph($table));

		$page->end();
	}

	public static function facturen()
	{
		global $DONAKOSTEN;
		
		$verslagen = DonateurVerzameling::ontvangerSoorten();

		$token = Token::useToken();
		$donas = DonateurVerzameling::huidige();
		$donas->toPersoonVerzameling(); // cachen
		$donas->sorteer('naam');

		$fail = false;
		$buffer = '';
		foreach($donas as $dona)
		{
			$persoon = $dona->getPersoon();
			$ander = $dona->getAnderPersoon();

			$contact = ContactAdres::geefBySoort($persoon, "THUIS");
			if(!$contact)
			{
				$fail = true;
				break;
			}

			$buffer .= '\donateur{'.PersoonView::naam($persoon);
			if ($ander != NULL)
			{
				$buffer .= ' en '.PersoonView::naam($ander);
			}
			$buffer .= '}';

			$buffer .= '{'.$contact->getStraat1()
				. '~'.$contact->getHuisnummer()
				. '\\\\'.$contact->getPostcode()
				. '~'.$contact->getWoonplaats().'}'
				. '{\\post{Donatie}{'.$dona->getDonatie().'}}';
			$buffer .= '{\\'.(!$dona->getMachtiging()?'geen':'')."machtiging}\n";
		}

		if(!$fail)
		{
			sendfile('dona-factuur.tex');
			echo $buffer;
			exit();
		}
		
		//Bij dikke faal
		Page::getInstance()->start()
				->add(new HtmlHeader(2, _('Facturen')))
				->add(new HtmlSpan(sprintf(_("Fout! De donateur %s (lid #%d) heeft geen adres, los dit eerst op!"), PersoonView::naam($persoon), $persoon->geefID()), 'text-danger strongtext'))
				->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
