<?php
/**
 * /Planner					-> (overzicht) geeft een overzicht van alle open planners mbt ingelogd lid
 *  /Nieuw					-> (nieuw) maak een nieuwe planner aan
 *  /planner_id				-> (details) details van een planner
 *   /Invullen				-> (invullen) invullen van de planner
 *    /plannerData_id		-> 404
 *     /Promoveren			-> (promoveren) promoveer een planner
 */

require_once('WhosWho4/Controllers/Activiteiten.php');

abstract class Planner_Controller
{
	/**
	 * Geeft een overzicht van alle open planner mbt de ingelogde persoon
	 */
	static public function overzicht ()
	{

		/** Haal alle open planners op, of alleen dat van de persoon **/
		// Merk op dat dit niet zomaar hetzelfde is als ->filterBekijken:
		// bestuur mag ook alle planners zien maar willen we niet lastig vallen
		// met echt *alle* planners.
		if(hasAuth('god')) {
			$planners = PlannerVerzameling::getOpenPlanners();
		} else {
			$pers = Persoon::getIngelogd();
			$planners = PlannerVerzameling::getOpenPlanners($pers);
		}

		/** Spuug alles uit **/
		$page = Page::getInstance()
			->start(_("Open planners"), 'planner')
			->add(new HtmlDiv(array(
				new HtmlSpan(HtmlAnchor::button('Nieuw', _("Maak een nieuwe planner aan")))
				)))
			->add(PlannerVerzamelingView::toonOverzicht($planners));
		$page->end();
	}

	/**
	 * Maak een nieuwe planner aan
	 */
	static public function nieuw ()
	{
		$datesInPast = false;

		$show_error = false;

		$newPlanner = new Planner();

		/** Verwerk formulier-data op basis van de state van het form **/			

		if (Token::processNamedForm() == 'PlannerNieuw')
		{
			PlannerView::processNieuwForm($newPlanner
				, $data = new PlannerDataVerzameling()
				, $deelnemers = new PlannerDeelnemerVerzameling()
				, $datesInPast);
			if ($newPlanner->valid() && $data->aantal() > 0 && $deelnemers->aantal() > 0 && !$datesInPast)
			{
				$newPlanner->opslaan();
				$data->opslaan();
				$deelnemers->opslaan();

				//Mail de deelnemers indien gewenst
				$mailResultaat = '';
				if(tryPar('stuurmail'))
				{
					$lidMail = Persoon::getIngelogd()->getEmail();
					if($lidMail)
					{
						// Mail maker van planner niet, die hoort al te weten dat er een planner is.
						$mailResultaat = $newPlanner->mailDeelnemers(NULL, true, tryPar("ExtraInfo"));
					}
				}
				if($pers = Persoon::getIngelogd()) {
					$persBadges = $pers->getBadges();
					$persBadges->addBadge('planner');
				}
				Page::redirectMelding($newPlanner->url(), _("De planner is nu aangemaakt!") . "<br>" . $mailResultaat);
			}
			else
			{
				Page::addMelding(_('Er waren fouten.'), 'fout');
				$show_error = true;
			}
		}

		PlannerView::nieuwForm('PlannerNieuw', $newPlanner, $show_error);
	}

	/**
	 * Returneert een array met geselecteerde planner
	 * $args = array(0 => 'planner_id')
	 */
	static public function plannerEntry ($args)
	{
		$planner = Planner::geef($args[0]);
		if (!$planner) return false;

		return array('name' => $planner->geefID(),
					'displayName' => $planner->getTitel(),
					'access' => $planner->magBekijken());
	}

	/**
	 * Toon details van een planner
	 */
	static public function details ()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);
		$lid = Persoon::getIngelogd();
		$titel = new HtmlDiv(new HtmlDiv(PlannerView::waardeOmschrijving($plan), 'panel-body'), 'panel panel-default');

		//Maak tabelletje voor de mogelijk dingen die je er mee kan doen, al staat een deel bij de pagelinks... niet super consistent dus
		$acties = new HtmlDiv();
		$acties->add(new HtmlHeader(3, _("Acties met deze planner:")));
		$acties->add(new HtmlDiv(new HtmlDiv($actieTable = new HtmlTable(array()), 'col-md-6'), 'row'));
		$actieTable->add(new HtmlTableRow(array(
			new HtmlTableDataCell(_("Pas mijn antwoorden aan:")),
			new HtmlTableDataCell(new HtmlAnchor($plan->url() . '/Invullen',
				HtmlSpan::fa('pencil', _('Aanpassen')) ))
			)));
		$actieTable->add(new HtmlTableRow(array(
			new HtmlTableDataCell(_("Stuur de deelnemers een reminder:")),
			new HtmlTableDataCell(new HtmlAnchor($plan->url() . '/Herinneren',
				HtmlSpan::fa('send', _('Mail')) ))
			)));
		if($plan->getDeelnemers()->bevat($lid)){

			$actieTable->add(new HtmlTableRow(array(
				new HtmlTableDataCell(_("Verwijder jezelf uit deze planner")),
				new HtmlTableDataCell(new HtmlAnchor($plan->url() . '/Zelfverwijder',
					HtmlSpan::fa('close', _('Zelf weg')) ))
			)));
		}
		/** Spuug alles uit **/
		$page = Page::getInstance();

		if ($plan->magWijzigen())
			$page->setEditUrl($plan->url() . '/Wijzig');
		if ($plan->magVerwijderen())
			$page->setDeleteUrl($plan->url() . '/Verwijder');

		$page->start(PlannerView::waardeTitel($plan), 'planner')
			->setLastmodified($plan->getGewijzigdWanneer())
			->setLastmodifier($plan->getGewijzigdWie())
			->add($titel)
			->add($acties)
			->add(new HtmlHR())
			->add(PlannerView::details($plan))
			->end();
	}

	/**
	 * Wijzig de planner
	 */
	static public function wijzigPlanner ()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);

		$title = PlannerView::waardeTitel($plan);
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(2, $title));

		$show_error = false;

		switch(Token::processNamedForm()) {
		case 'wijzigPlanner':
			// om te kijken wat het verschil in deelnemers is
			$oudeDeelnemers = $plan->getDeelnemers();

			PlannerView::processWijzigPlanner($plan);

			if($plan->getData()->aantal() <= 0) {
				$div->add(new HtmlSpan(_('Hola! Zonder data kunnen we niets plannen!'),'text-danger strongtext'));
			} else {
				$plan->opslaan();

				// bepaal of er nog nieuwe deelnemers bijkomen zodat we ze kunnen mailen
				$mailResultaat = '';
				if(tryPar('stuurmail')) {
					$nieuweDeelnemers = $plan->getDeelnemers();

					// bepaal het verschil (moet zo omdat difference() symmetrisch is en in-place)
					$verschil = new LidVerzameling();
					foreach($nieuweDeelnemers as $nieuwe) {
						if (!$oudeDeelnemers->bevat($nieuwe)) {
							$verschil->voegtoe($nieuwe);
						}
					}

					// stuur ze een mailtje
					if ($verschil->aantal() > 0) {
						$lidMail = Persoon::getIngelogd()->getEmail();
						if($lidMail)
						{
							$mailResultaat = $plan->mailDeelnemers($verschil, true);
						}
					}
				}

				Page::redirectMelding($plan->url(), _("De planner is gewijzigd!") . " " . $mailResultaat);
			}
			break;
		case 'wijzigPlannerData':
			PlannerView::processForm($plan);

			if($plan->valid()) {
				$plan->opslaan();
				Page::redirectMelding($plan->url(), _("De planner is gewijzigd!"));
			} else {
				Page::addMelding(_('Er waren fouten'), 'fout');
				$show_error = true;
			}
			break;
		}

		$div->add(PlannerView::wijzigPlanner($plan, $show_error));

		$page = Page::getInstance()
			->start($title, 'planner')
			->setLastmodified($plan->getGewijzigdWanneer())
			->setLastmodifier($plan->getGewijzigdWie())
			->add($div)
			->end();
	}

	public static function mailDeelnemers()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);
		$resultaat = $plan->mailDeelnemers();
		Page::redirectMelding($plan->url(), $resultaat);
	}

	public static function verwijder()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);
		
		if(!$plan->magVerwijderen())
			spaceHttp(403);

		/** Verwerk formulier-data **/
		if(Token::processNamedForm() == 'VerwijderPlanner')
		{
			$returnVal = $plan->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding('/Activiteiten/Planner/', _("Planner verwijderd"));
			}
		}

		PlannerView::verwijder($plan);
	}

	/** 
	* Kick jezelf de planner uit
	*/
	public static function zelfVerwijder()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);
		$lid = Persoon::getIngelogd();

		if(!$plan->getDeelnemers()->bevat($lid)){
			spaceHttp(403);
		}
		/** Verwerk formulier-data **/
		
		if(Token::processNamedForm() == 'VerwijderJezelf')
		{
			
			$plan->zelfVerwijderen();
			$plan->opslaan();
			$plan->mailIemandIsWeggegaan($lid);
			Page::redirectMelding('/Activiteiten/Planner/', _("Je hebt jezelf eruit gezet"));	
		}

		PlannerView::zelfVerwijder($plan);
	}

	/**
	 * Vul de planner in
	 */
	static public function invullen ()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);

		/** Prepareerd output **/
		$body = new HtmlDiv();

		/** Verwerk formulier-data **/
		PlannerDeelnemerVerzamelingView::processInvullenForm($verzameling = $plan->deelnemer(Persoon::getIngelogd()));
		if (Token::processNamedForm() == 'PlanInvullen')
		{
			// todo mail indien aangegeven
			$verzameling->opslaan();

			if($plan->isIngevuldDoorIedereen()){
				$plan->mailEigenaarPlannerIsIngevuldDoorIedereen();
			}		

			Page::redirectMelding($plan->url(), _("De antwoorden zijn opgeslagen!"));

			

		}
		else
			$body->add(PlannerDeelnemerVerzamelingView::invullenForm('PlanInvullen', $verzameling));


		

		/** Spuug alles uit **/
		$div = new HtmlDiv();
		$div->add($header = new HtmlHeader(2, _("Invullen ")))
			->add($body);
		$header->add(PlannerView::makeLink($plan));

		$page = Page::getInstance()
			->addFooterJS(Page::minifiedFile("Planner.js"))
			->start(_("Planner invullen"))
			->add($div)
			->end();

	}

	/**
	 * Returneert een array met geselecteerde plannerData
	 * $args = array(0 => 'plannerData_id')
	 */
	static public function plannerDataEntry ($args)
	{
		$plannerData = PlannerData::geef($args[0]);
		if (!$plannerData) return false;

		return array('name' => $plannerData->geefID(),
					'displayName' => PlannerDataView::waardeMomentBegin($plannerData),
					'access' => true);
	}

	/**
	 * Promoveren van een plan tot activiteit
	 */
	static public function promoveren ()
	{
		/** Controleer in welke stap we zitten **/
		if ($actid = tryPar('planPromoveer[ActId]', null))
		{
			self::promoveer2($actid, true);
			return;
		}

		$show_error = false;

		/** Laad de gekozen planner **/
		$planDataId = (int) vfsVarEntryName();
		$planData = PlannerData::geef($planDataId);
		$plan = $planData->getPlanner();

		/** Prepareerd output **/
		$body = new HtmlDiv();

		/** Verwerk formulier-data **/
		$newAct = new ActiviteitInformatie();
		$eindMoment = clone $planData->getMomentBegin();
		$eindMoment->add(new DateInterval('PT1H')); //vergadering duurt standaard een uurtje
		$newAct->setTitel($plan->getTitel('nl'), 'nl')
			   ->setTitel($plan->getTitel('en'), 'en')
			   ->setWerftekst($plan->getOmschrijving('nl'), 'nl')
			   ->setWerftekst($plan->getOmschrijving('en'), 'en')
			   ->setMomentBegin($planData->getMomentBegin())
			   ->setMomentEind($eindMoment)
			   ->setToegang('PRIVE');
		$cieActs = new CommissieActiviteitVerzameling();
		$vragen = new ActiviteitVraagVerzameling();
		$herhalingen = new ActiviteitVerzameling();
		if (Token::processNamedForm() == 'ActNieuw')
		{
			if (Activiteiten_Controller::verwerkActiviteitToevoegen($newAct, $cieActs, $vragen, $herhalingen)) {
				self::promoveer2($newAct->geefID(), false);
				return;
			} else {
				// jammer dan.
				$show_error = true;
			}
		}

		$body->add(ActiviteitInformatieView::wijzigForm($newAct, $cieActs, $vragen, true, $show_error));

		/** Spuug alles uit **/
		$div = new HtmlDiv();
		$div->add($body);

		$page = Page::getInstance()
			 ->addFooterJS(Page::minifiedFile('Activiteit.js'))
			->start(sprintf(_("Planner promoveren stap 1/2 %s"), PlannerView::waardeTitel($plan)))
			->add($div)
			->end();
	}

	/**
	 * Schrijf deelnemers in voor een net gepromoveerde planner
	 * (wordt aangeroepen via promoveer()
	 */
	static public function promoveer2 ($actid, $continue)
	{
		/** Laad de gekozen planner + act **/
		$planDataId = (int) vfsVarEntryName();
		$planData = PlannerData::geef($planDataId);
		$plan = $planData->getPlanner();
		$act = Activiteit::geef($actid);

		/** Prepareerd output **/
		$body = new HtmlDiv();

		/** Verwerk formulier-data **/
		$verzameling = new DeelnemerVerzameling();
		$persVerzameling = $planData->personenVoor();
		DeelnemerVerzamelingView::processNieuwForm($verzameling = new DeelnemerVerzameling(), $act);
		if ($continue && Token::processNamedForm() == 'PlanPromoveer2')
		{
			$verzameling->opslaan();
			Page::redirectMelding($act->url(), _("De activiteit is nu aangemaakt!"));
		}
		else
			$body->add(DeelnemerVerzamelingView::nieuwForm('PlanPromoveer2', $verzameling, false, $persVerzameling, $act)->add(HtmlInput::makeHidden('planPromoveer[ActId]', $act->geefID())))
				 ->add(new HtmlParagraph(_("Bovenstaande leden zijn de leden die in je kart zitten en de leden die aangegeven hebben om op die dag aanwezig te kunnen zijn.")));

		/** Spuug alles uit **/
		$div = new HtmlDiv();
		$div->add(new HtmlHeader(2, _("Promoveren stap 2/2 ") . ' ' . PlannerView::waardeTitel($plan)))
			->add($body);

		$page = Page::getInstance()
			->start(_("Planner promoveren"))
			->add($div)
			->end();
	}

	/**
	 * Verstuur een herinnering om de planner in te vullen.
	 */
	static public function herinner()
	{
		/** Laad de gekozen planner **/
		$planid = (int) vfsVarEntryName();
		$plan = Planner::geef($planid);
		$lid = Persoon::getIngelogd();

		if(!$plan->getDeelnemers()->bevat($lid)){
			spaceHttp(403);
		}
		/** Verwerk formulier-data **/
		
		if(Token::processNamedForm() == 'ExtraInfo')
		{
			$plan->mailDeelnemers(NULL, true, tryPar("ExtraInfo"));
			Page::redirectMelding('/Activiteiten/Planner/' . $planid, _("Herinnering succesvol verstuurd!"));
		}

		return new PageResponse(PlannerView::herinneren($plan));
	}
}
