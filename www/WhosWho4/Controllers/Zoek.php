<?php

abstract class Zoek_Controller
{

	static public function zoek ()
	{
		$q = tryPar('q');

		$div = new HtmlDiv(NULL, 'search');
		if (!$q)
			$div->add(new HtmlParagraph(_('Vul een zoekterm in')));

		$form = new HtmlForm('GET');
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeSearch('q', $q ? $q : '')->addClass('text'))
			 ->add(HtmlInput::makeSubmitButton(_('Zoeken')));
		$div->add($form);

		if ($q)
		{
			$body = new HtmlElementCollection();
			$body->add(new HtmlHeader(2, sprintf(_('Resultaten voor %s'), $q)));

			if (hasAuth('ingelogd'))
			{
				$personen = PersoonVerzameling::zoek($q);
				$nPersonen = $personen->aantal();

				$hideablediv = new HtmlHideableDiv(!$nPersonen);
				$body->add(new HtmlHeader(3, ($nPersonen ? $hideablediv->makeButton() . new HtmlSpan('&nbsp;') : '') . _('Personen')));
				$hideablediv->add(PersoonVerzamelingView::lijst($personen));
				$body->add($hideablediv);
			}

			$activiteiten = ActiviteitVerzameling::zoek($q);
			$nActiviteiten = $activiteiten->aantal();

			$hideablediv = new HtmlHideableDiv(!$nActiviteiten);
			$body->add(new HtmlHeader(3, ($nActiviteiten ? $hideablediv->makeButton() . new HtmlSpan('&nbsp;') : '') . _('Activiteiten')));
			$hideablediv->add(ActiviteitVerzamelingView::htmlLijst($activiteiten))
				->addClass('activiteiten');
			$body->add($hideablediv);

			$cies = CommissieVerzameling::zoek($q);
			$nCies = $cies->aantal();

			$hideablediv = new HtmlHideableDiv(!$nCies);
			$body->add(new HtmlHeader(3, ($nCies ? $hideablediv->makeButton() . new HtmlSpan('&nbsp;')  : '') . _('Commissies, groepen en disputen')));
			$hideablediv->add(CommissieVerzamelingView::overzichtLijst($cies));
			$body->add($hideablediv);

			$mededelingen = MededelingVerzameling::zoek($q);
			$nMededelingen = $mededelingen->aantal();

			$hideablediv = new HtmlHideableDiv(!$nMededelingen);
			$body->add(new HtmlHeader(3, ($nMededelingen ? $hideablediv->makeButton() . new HtmlSpan('&nbsp;')  : '') . _('Nieuws')));
			$hideablediv->add(MededelingVerzamelingView::maakOverzicht($mededelingen, true));
			$body->add($hideablediv);

			if (hasAuth('bestuur'))
			{
				$organisaties = OrganisatieVerzameling::zoek($q);
				$nOrganisaties = $organisaties->aantal();

				$hideablediv = new HtmlHideableDiv(!$nOrganisaties);
				$body->add(new HtmlHeader(3, ($nOrganisaties ? $hideablediv->makeButton() . new HtmlSpan('&nbsp;')  : '') . _('Organisaties')));
				$hideablediv->add(OrganisatieVerzamelingView::htmlList($organisaties));
				$body->add($hideablediv);

				$artikelen = ArtikelVerzameling::zoek($q);
				$nArtikelen = $artikelen->aantal();

				$hideablediv = new HtmlHideableDiv(!$nArtikelen);
				$body->add(new HtmlHeader(3, ($nArtikelen ? $hideablediv->makeButton() . new HtmlSpan('&nbsp;')  : '') . _('Artikelen')));
				$hideablediv->add(ArtikelVerzamelingView::tabel($artikelen));
				$body->add($hideablediv);
			}

			$div->add($body);
		}

		Page::getInstance()
			->addHeadCss(Page::minifiedFile('activiteiten.css', 'css'))
			->start(_('Zoeken'))
			->add($div)
			->end();
	}

}

