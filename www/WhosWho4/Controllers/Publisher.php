<?php

abstract class Publisher_Controller
{
	/**
	 *  Controllerfunctie voor de publisherhome-pagina
	 */
	static public function home()
	{
		global $uriRef;

		requireAuth('actief');

		if($url = tryPar('url'))
		{
			$vfs = hrefToEntry($url);
			if(!$vfs)
				spaceHttp(403);

			Page::redirect($vfs->publisherUrl());
		}

		PublisherView::home();
	}

	/**
	 *  Handelt het toevoegen van een cie of act af
	 */
	static public function toevoegen()
	{
		requireAuth('actief');

		// Handel het formulier af
		if(Token::processNamedForm() == 'add')
		{
			$cieAct = tryPar('add');
			$name = tryPar('name');

			// Getparams moeten wel allebei geset zijn
			if(!$cieAct || !$name)
				spaceHttp(403);

			switch($cieAct)
			{
			case 'cie':
				$ciedata = Commissie::cieByLogin($name);

				// De commissie moet te wijzigen zijn
				if(!CommissieVerzameling::wijzigbare()->bevat($ciedata))
					spaceHttp(403);

				// De entry moet niet al bestaan
				$entry = hrefToEntry('/Vereniging/Commissies/'.$name);
				if($entry && ($entry->getType() != ENTRYVARIABLE ? $entry->getId() : NULL))
					Page::redirect($entry->publisherUrl());

				$entry = hrefToEntry('/Vereniging/Commissies/');

				// Sla op
				$newdir = vfs::add(array
					( 'name' => $name
					, 'displayName' => $ciedata->getNaam()
					, 'type' => ENTRYDIR
					, 'parent' => $entry->getId()
					, 'special' => '/space/benamite/hooks/commissies.php:commissiewsw'
					, 'visible' => FALSE
				)	);

				Page::redirectMelding($newdir->publisherUrl(), _('Entry aangemaakt'));
				break;
			case 'act':
				global $INDEX;

				// De act moet bestaan en/of gewijzigd kunnen worden
				$actdata =  Activiteit::geef($name);
				if(!$actdata)
					spaceHttp(403);
				if (!$actdata->magWijzigen())
					spaceHttp(403);

				// De act moet niet al bestaan
				$entry = hrefToEntry($actdata->url());
				if( $entry
						&& $entry->getType() == ENTRYVARIABLE
						&& $entry->getSpecial() == '/WhosWho4/Controllers/Activiteiten.php:Activiteiten_Controller:commissieActiviteitNaamEntry'
						? $entry->getId()
						: NULL)
						Page::redirect($entry->publisherUrl());

				$actsRef = benamiteHrefToReference('/Activiteiten/*');
				$actsEntry = $actsRef['entry'];

				// Maak een dir voor de act aan
				$actEntry = vfs::add(array
					( 'name' => $name
					, 'displayName' => $actdata->getTitel()
					, 'type' => ENTRYDIR
					, 'parent' => $actsEntry->getId()
					, 'visible' => FALSE
					, 'special' => ''
				)	);
				// Maak een variabele entr voor de act, deze komt overeen met de
				// variabale entry waar normaal de naam van de act in staat
				$subdir1 = vfs::add(array
					( 'name' => $actdata->getTitel()
					, 'displayName' => $actdata->getTitel()
					, 'type' => ENTRYVARIABLE
					, 'parent' => $actEntry->getId()
					, 'visible' => FALSE
					, 'special' => '/WhosWho4/Controllers/Activiteiten.php:Activiteiten_Controller:commissieActiviteitNaamEntry'
				)	);
				// Maak een automatische doorverwijzigngspagina
				$subdir2 = vfs::add(array
					( 'name' => $INDEX
					, 'displayName' => 'Doorsturen'
					, 'type' => ENTRYINCLUDE
					, 'parent' => $actEntry->getId()
					, 'visible' => FALSE
					, 'special' => '/WhosWho4/Controllers/Activiteiten.php:Activiteiten_Controller:activiteitNaamForward'
				)	);

				Page::redirectMelding($subdir1->publisherUrl(), _('Entry aangemaakt'));
				break;
			}
		}

		return false;
	}

	/**
	 *  De entry-functie van een index
	 */
	static public function publisherEntry($args)
	{
		global $request;

		requireAuth('actief');

		if(!is_numeric($args[0]))
		{
			// We verwachten een entry-id, maar als we een URL krijgen kunnen we de id zelf verzinnen.
			$str = str_replace(PUBLISHERBASE, '/', $request->server->get('PATH_INFO'));
			$vfs = hrefToEntry($str);

			if($vfs) {
				// Vertel aan benamite dat de resterende URL gelijk is aan het entry-id.
				return [
					'rewrite' => $vfs->getID(),
					'access' => true,
				];
			} else {
				// De pagina bestaat niet, dus we sterven onszelf dood.
				return array('name' => $args[0]
					, 'displayName' => "Publisher"
					, 'access' => false
				);
			}
		}

		$vfs = vfs::get($args[0]);

		if(!$vfs)
			return false;

		// De vfs moet te wijzigen zijn door de user
		return array('name' => $vfs->getId()
			, 'displayName' => $vfs->getName()
			, 'access' => hasPublisherAuth($vfs->publisherRealUrl()));
	}

	/**
	 *  De entry-functie van een index voor tijdens editten
	 */
	static public function editEntry($args)
	{
		global $request;

		requireAuth('actief');

		$vfs = vfs::get($args[1]);

		if(!$vfs || !($id = vfs::exists($args[0], $vfs->getParent()->getId()))) {
			spaceHttp(403);
		}

		Page::redirect(PUBLISHERBASE . $id);
	}

	/**
	 *  Geeft een pagina voor een entry
	 */
	static public function publisher()
	{
		requireAuth('actief');
		$id = vfsVarEntryName();
		$vfs = vfs::get($id);

		if(!$vfs)
			spaceHttp(403);

		// Maak onderscheid tussen dirs en niet-dirs
		if($vfs->isDir())
			self::publisherDir($vfs);
		else
			self::publisherFile($vfs);
	}

	/**
	 *  Maakt een pagina voor een entry-dir
	 *
	 * @param vfs De vfs-dir
	 */
	static public function publisherDir($vfs)
	{
		// Handel de verschillende formulieren af
		switch(Token::processNamedForm())
		{
		case 'bestandAanmaken':
			$newname = tryPar('newname');
			$newtype = ENTRYFILE;

			// Haal de laatste / van de naam
			if(substr($newname, -1) == '/')
			{
				$newtype = ENTRYDIR;
				$newname = substr($newname, 0, -1);
			}

			// /-es mogen niet in de naam voorkomen
			$newfile = str_replace('/', '', $newname);

			// De entry moet niet al bestaan
			if($vfs->hasChild($newfile))
			{
				Page::addMelding(sprintf(_("Er bestaat al een entry met de naam '%s'!"), $newfile), 'fout');
				break;
			}

			// Voeg het bestand toe
			$file = vfs::add(
						array   ( 'name'    => $newfile
								, 'type'    => $newtype
								, 'parent'  => $vfs->getId()
								, 'visible' => true
								, 'displayName'     => 'Hier de titel van de pagina'
								, 'displayName_eng' => 'The title of the page goes here'
								));
			Page::addMelding(_('Toevoegen gelukt!'), 'succes');
			break;
		case 'mapAanmaken':
			$newdir = tryPar('mapname');
			$newtype = ENTRYDIR;

			// De naam van een map mag geen '/'-es hebben
			if(strpos($newdir, '/') !== false)
			{
				Page::addMelding(sprintf(_("Je mag geen '/' in de naam hebben!")), 'fout');
				break;
			}
			// De map-naam mag niet al bestaan
			if($vfs->hasChild($newdir))
			{
				Page::addMelding(sprintf(_("Er bestaat al een map met de naam '%s'!"), $newdir), 'fout');
				break;
			}
			// Voeg toe
			$dir = vfs::add(array
				( 'name' => $newdir
				, 'displayName' => $newdir
				, 'type' => $newtype
				, 'parent' => $vfs->getId()
				, 'visible' => FALSE
			)	);
			Page::addMelding(_('Toevoegen gelukt!'), 'succes');
			break;
		case 'bestandUploaden':
			// er moet wel een bestand geupload worden
			if (empty($_FILES['fileupload']['name'])) {
				Page::addMelding(_('Er is geen bestand meegestuurd'), 'fout');
				break;
			}

			$filename = $_FILES['fileupload']['name'];

			// We moeten wel een extensie uit de naam kunnen halen
			$pos = strrpos($filename, '.');
			$type = ($pos === False ? '' : strtolower(substr($filename, $pos+1)));

			// Het type bestand moet wel toegestaan zijn om te uploaden
			global $ALLOWED_FILE_TYPES;
			if (!isset($ALLOWED_FILE_TYPES[$type]))
			{
				if ( $type == 'html' || $type == 'htm' )
				{
					Page::addMelding(_('Je kunt HTML bestanden niet uploaden, '
						. 'die moet je via de publisher toevoegen en bewerken.'), 'fout');
					break;
				}

				Page::addMelding(sprintf(_('Onbekend filetype \'%s\', kan dit '
					. 'bestand nog niet opslaan. Voer een bug in als je dit '
					. 'toch nodig hebt.', 'fout'),
				htmlspecialchars($type)));
				break;
			}
			$type = $ALLOWED_FILE_TYPES[$type];

			$filetmp = $_FILES['fileupload']['tmp_name'];

			if(filesize($filetmp) == 0)
			{
				Page::addMelding(sprintf(_("'%s' is leeg!"),
					$_FILES['fileupload']['name']), 'fout');
				break;
			}

			// De entry moet niet al bestaan
			if($vfs->hasChild($filename)) {
				Page::addMelding(sprintf(_('Er bestaat al een entry met de naam \'%s\'!')
					, $filename), 'fout');
				break;
			}

			// We maken een md5-hash voor de bestandsnaam op de fs
			$md5 = md5_file($filetmp);

			// We staan debug-bestanden op met een .debug-extensie om er zeker van
			// te zijn dat er geen overlap is met live. Deze bestanden worden
			// door de cron autmatisch verwijderd
			if (!move_uploaded_file($filetmp,
				"/srv/http/www/benamite_files/$md5".(DEBUG?'.debug':'')))
				user_error('File upload failed...!', E_USER_ERROR);

			// Opslaan
			$file = vfs::add(
				array   ( 'name' => $filename
				, 'type' => ENTRYUPLOADED
				, 'parent' => $vfs->getId()
				, 'visible' => false
				, 'displayName' => $filename
				, 'special' => "$md5 $type"
			));
			Page::addMelding(_('Toevoegen gelukt!'), 'succes');
			break;
		}

		PublisherView::publisherDirPage($vfs);
	}

	/**
	 *  Maakt de pagina voor alles wat niet een dir is
	 *
	 * @param vfs De vfs
	 */
	static public function publisherFile($vfs)
	{
		// Alles wat niet een file is sturen we door naar de echte locatie
		switch($vfs->getType())
		{
		case ENTRYLINK:
		case ENTRYINCLUDE:
		case ENTRYUPLOADED:
		case ENTRYHOOK:
			Page::redirect($vfs->url());
			break;
		case ENTRYFILE:
			// Een file sturen we door naar de edit-pagina
			Page::redirect($vfs->publisherUrl() . 'Edit');
		default:
			break;
		}
	}

	/**
	 *  Maakt de edit-pagina voor publisher bestanden
	 */
	static public function edit()
	{
		requireAuth('actief');
		$id = vfsVarEntryName();
		$vfs = vfs::get($id);

		if(!$vfs)
			spaceHttp(403);

		if(!hasPublisherAuth($vfs->publisherRealUrl()))
			spaceHttp(403);

		$show_error = false;

		// Handel het formulier af
		if(Token::processNamedForm() == 'edit')
		{
			$displayName = tryPar('displayName');
			$vfs->setDisplayName($displayName);

			// Er moet wel content zijn om op te slaan!
			$content = tryPar('content');
			if(!$content)
				Page::addMelding(_('Er wordt geen content opgeslagen!'), 'fout');
			else
			{
				// Hackt #7706 goed.
				// TODO: dit moet beter kunnen!
				$content = preg_replace('/>\s*</', "><", $content);

				// Zorg ervoor dat er geen andere xml-errors in de weg zitten.
				$vroeger_intern = libxml_use_internal_errors(true);
				$errors = libxml_get_errors();
				if (count($errors) > 0) {
					user_error("libxml heeft errors maar die zijn niet opgeruimd", E_USER_WARNING);
				}
				libxml_clear_errors();
				// Stop het in een volwaardig HTML-bestand en check op errors.
				$html =
					'<!DOCTYPE HTML PUBLIC "-//AES//DTD HTML//EN//1.0"'
					. ' "'.LIBDIR.'/sgml/aes.dtd">'."\n"
					. "<html>\n"
					. '<head>'
					.  '<title>title</title>'
					.  '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'
					. "</head>\n"
					. "<body>\n"
					. $content
					. "</body>\n</html>";

				// Dit klaagt dat sommige tags niet kloppen met de HTML spec,
				// dus even later filteren we die er weer uit.
				$document = new DOMDocument('1.0', 'utf-8');
				$valid = $document->loadHTML($html);
				$valid = $valid && $document->validate();

				// Maak errors dood als ze zeggen dat de a-estags ongeldig zijn.
				$aestags = "act|actfotos|cie|ciefotos|foto|lid";

				$errors = libxml_get_errors();
				foreach($errors as $e)
				{
					if (preg_match("/Tag (" . $aestags . ") invalid/", $e->message) === 1) {
						continue;
					}

					// Produceer een mooie toonbare errorcode.
					$niveau = _("Onbekend");
					switch ($e->level) {
						case LIBXML_ERR_WARNING:
							$niveau = _("Waarschuwing");
							break;
						case LIBXML_ERR_ERROR:
							$niveau = _("Fout");
							break;
						case LIBXML_ERR_FATAL:
							$niveau = _("Fatale fout");
							break;
					}
					// De regel is 4 meer want we stoppen er onze HTML omheen.
					if ($e->file) {
						$plek = sprintf("%s:%d:%d", $e->file, $e->line, $e->column);
					} else {
						$plek = sprintf("%d:%d", $e->line - 4, $e->column);
					}
					$bericht = htmlspecialchars(trim($e->message));
					$melding = sprintf(_("[VOC: regel van een tekstbestand]%s op regel %s: %s"), $niveau, $plek, $bericht);
					Page::addMelding($melding, 'waarschuwing');
					$show_error = true;
				}


				// Reset alle andere errors.
				libxml_clear_errors();
				libxml_use_internal_errors($vroeger_intern);

				if ($document && !$show_error)
				{
					Page::addMelding(_("De pagina is bijgewerkt, hoera!"), 'succes');
					$vfs->setContent($content);
				}
			}
		}

		PublisherView::editPage($vfs, $show_error);
	}

	/**
	 *  Toggled de visibility van een entry
	 */
	static public function toggleVisibility()
	{
		requireAuth('actief');
		$id = vfsVarEntryName();
		$vfs = vfs::get($id);

		if(!$vfs)
			spaceHttp(403);

		if(!hasPublisherAuth($vfs->publisherRealUrl()))
			spaceHttp(403);

		$vfs->setVisible(!$vfs->isVisible());

		Page::redirectMelding($vfs->getParent()->publisherUrl(), _('Gelukt!'));
	}

	/**
	 *  Hernoemt een entry
	 */
	static public function rename()
	{
		requireAuth('actief');
		$id = vfsVarEntryName();
		$vfs = vfs::get($id);

		if(!$vfs)
			spaceHttp(403);

		if(!hasPublisherAuth($vfs->publisherRealUrl()))
			spaceHttp(403);

		// Handel het formulier af
		if(Token::processNamedForm() == 'rename')
		{
			$newname = tryPar('newname_'.$vfs->getId());

			// De entry moet wel bestaan
			// TODO: bestaat het bestand al?
			if(!$vfs)
				Page::addMelding(sprintf(_("Entry (%d) is niet hernoemd, want deze bestaat niet."), $rename), 'fout');
			else
			{
				$vfs->setName($newname);

				Page::addMelding(_('Gelukt!'), 'succes');
			}
		}

		Page::redirect($vfs->getParent()->publisherUrl());
	}

	/**
	 *  Verwijderd een entry, let op de 'wil je dit zeker'-check wordt door js gedaan
	 */
	static public function delete()
	{
		requireAuth('actief');
		$id = vfsVarEntryName();
		$vfs = vfs::get($id);

		if(!$vfs)
			spaceHttp(403);

		if(!hasPublisherAuth($vfs->publisherRealUrl()))
			spaceHttp(403);

		// Een niet-lege dir mag je niet deleten
		if($vfs->isDir() && sizeof($vfs->getChildrenIds()) != 0)
		{
			Page::addMelding(_('De map bevat nog bestanden, gooi deze eerst weg!'), 'fout');
			Page::redirect($vfs->getParent()->publisherUrl());
		}

		vfs::del($vfs);
		Page::redirectMelding($vfs->getParent()->publisherUrl(), _('Gelukt!'));
	}

	/**
	 *  Verplaats een bestand omhoog in de rangorde
	 */
	static public function up()
	{
		self::move('u');
	}

	/**
	 *  Verplaats een bestand omlaag in de rangorde
	 */
	static public function down()
	{
		self::move('d');
	}

	/**
	 *  Verplaats een bestand in de rangorde
	 *
	 * @param dir De richting, mag 'u' of 'd' zijn
	 */
	static public function move($dir = 'u')
	{
		requireAuth('actief');
		$id = vfsVarEntryName();
		$vfs = vfs::get($id);

		if(!$vfs)
			spaceHttp(403);

		if(!hasPublisherAuth($vfs->publisherRealUrl()))
			spaceHttp(403);

		switch($dir)
		{
		case 'u':
			$vfs->getParent()->moveChildUp($vfs);
			break;
		case 'd':
			$vfs->getParent()->moveChildDown($vfs);
			break;
		}

		Page::redirectMelding($vfs->getParent()->publisherUrl(), _('Gelukt!'));
	}

	static public function contentEntry($args)
	{
		return array(
			'name' => $args[0],
			'displayName' => $args[0],
			'access' => true
		);
	}

	static public function content()
	{
		global $uriRef;
		$ids = vfsVarEntryNames();

		$vfs = vfs::get($ids[1]);

		$child = $vfs->getParent()->getChild($ids[0]);

		if(!$child)
			spaceHttp(403);

		self::publisherFile($child);
	}
}
