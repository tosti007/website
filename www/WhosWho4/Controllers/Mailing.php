<?php
/**
 * /Mailing			-> (mailing) toon een mailformulier
 */

abstract class Mailing_Controller
{
	/**
	 * Toon een mailformulier
	 */
	public static function nieuw ()
	{
		/** Buffer voor berichten **/
		$form = true;
		$msg = null;

		/** Verwerk formulier-data **/
		$mail = new Email();
		if (Token::processNamedForm() == 'Mail')
		{
			$data = tryPar('Mail', array());
			$mail->setFrom(Contact::geef($data['from']))
				 ->setReplyTo($data['replyto'])
				 ->setSubject($data['subject'])
				 ->setBody($data['body']);

			if ($mail->valid())
			{
				$form = false;
				$msg = new HtmlSpan(_("De mail is verzonden."), 'text-success strongtext');
			}
			else
				$msg = new HtmlSpan(_("Er waren fouten."), 'text-danger strongtext');
		}

		/** Ga naar de view **/
		EMailingView::nieuw($mail, $form, $msg);
	}
}
