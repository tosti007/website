<?php

abstract class CommissieVerzamelingController {

	/**
	 * @site{/Vereniging/Commissies/index.html}
	 */
	static public function home()
	{
		$page = Page::getInstance();
		$page->start(_("Commissie, groepen en disputen"));

		$page->add(new HtmlDiv(sprintf(_('Hieronder vind je een overzicht van commissies, '
			. 'disputen en groepen binnen A&ndash;Eskwadraat die op dit moment '
			. 'bezig zijn met het organiseren van activiteiten en andere bezigheden '
			. 'in naam van de vereniging. Naast deze commissies zijn er door het '
			. 'jaar heen ook andere soorten commissies en groepen die bezig zijn. '
			. 'Voor een overzicht over alle commissies binnen %s, kun je het %s raadplegen. '
			. 'Op de %s staat een aantal commissies die op zoek zijn naar leden.  ' 
            . 'Heb je interesse in een commissie? Stuur dan een mailtje naar actief@a-eskwadraat.nl!')
			, aesnaam(), new HtmlAnchor(Register::getValue('commissieBoekjeURL'), _('commissieboekje'))
			, new HtmlAnchor('/Vereniging/Commissies/Vacatures', _('vacaturepagina')))
		, 'alert alert-info'));

		$cies = CommissieVerzameling::huidige();
		$cies->sorteer("naam");
		$page->add(new HtmlHeader(3, _("Commissies")))
			 ->add(CommissieVerzamelingView::overzichtLijst($cies));

		$cies = CommissieVerzameling::huidige('DISPUUT');
		$cies->sorteer("naam");
		$page->add(new HtmlHeader(3, _("Disputen")))
			 ->add(new HtmlParagraph(_("Een dispuut is een groepering (oud-)leden wiens activiteiten verwant zijn aan, maar niet vallen onder de vereniging.")))
			 ->add(CommissieVerzamelingView::overzichtLijst($cies));

		$cies = CommissieVerzameling::huidige('GROEP');
		$cies->sorteer("naam");
		$page->add(new HtmlHeader(3, _("Groepen")))
			 ->add(new HtmlParagraph(_("Een groep is een willekeurige samenstelling van leden.")))
			 ->add(CommissieVerzamelingView::overzichtLijst($cies));

		$page->end();
	}

	/**
	 * @site{/Vereniging/Commissies/alleLedenCsv}
	 */
	static public function alleLedenCSV(){
		$soort = trypar('type');
		$soorten = array(
			'groep' => _("groepen"),
			'dispuut' => _("disputen"),
			'cie' => _("commissies"),
			'ALLE' => _("commissies, -groepen en -disputen"),
		);

		if(!in_array($soort, array_keys($soorten))){ 
			$soort = 'ALLE';
		}

		$cies = CommissieVerzameling::huidige($soort);

		$csvarray = CommissieVerzamelingView::maakCsvArrayAlleLeden($cies);


		header("Content-Disposition: attachment; filename=personen.csv");
		header("Content-type: text/csv; encoding=utf-8");

		$output = fopen('php://output', 'r+');

		//Plaats alle array keys aan het begin van de CSV
		if(count($csvarray) > 0){
			fputcsv($output, array_keys($csvarray[0]));
		} else {
			fwrite($output, _("Geen data."));
		}

		//Print alle inhoud voor de CSV
		foreach($csvarray as $per){
			fputcsv($output, $per);
		}

		fclose($output);
		die;

	}

	/**
	 * @site{/Vereniging/Commissies/Leden}
	 */
	static public function alleLeden()
	{
		$printableKoppenblad = tryPar('printableKoppenblad', null);
		$namenZichtbaar = tryPar('namenZichtbaar', null);

		$soort = trypar('type');
		$soorten = array(
			'groep' => _("groepen"),
			'dispuut' => _("disputen"),
			'cie' => _("commissies"),
			'ALLE' => _("commissies, -groepen en -disputen"),
		);

		if(!in_array($soort, array_keys($soorten))){ 
			$soort = 'ALLE';
		}

		// De commissies / groepen / disputen waarvan we de leden willen zien
		$cies = CommissieVerzameling::huidige($soort);

		if($printableKoppenblad) {
			$leden = $cies->leden()->toPersoonVerzameling();

			$page = Page::getInstance('cleanhtml')->start();
			$page->add(new HtmlHeader(2, sprintf(_("De %d[VOC: hoeveelheid leden] actieve leden van %s[VOC: commissiesoort]:"), $leden->aantal(), $soorten[$soort])));

			$page->add(PersoonVerzamelingView::koppenPrintbaar($leden, $namenZichtbaar));

			$page->end();
		} else {
			$page = Page::getInstance();
			$page->start(sprintf(_("Actieve A-Eskwadraat%s[VOC: commissiesoort bv groep, cie etc]"), $soorten[$soort]));

			// Selecteer wat je wilt zien mbv dit formulier
			$page->add(new HtmlSpan(_("Maak je keuze uit: ")))
				->add($form = new HtmlForm('get'));
			$form->setNoDefaultClasses();
			$form->addClass('form-horizontal');

			// Dropdown voor de soort commissies
			$form->add($soortenBox = new HtmlSelectbox('type'));
			$soorten['ALLE'] = _("alle"); //voor de leesbaarheid
			$geselecteerd = "ALLE";
			foreach($soorten as $key => $soortval)
			{
				$soortenBox->add($option = new HtmlSelectboxOption($key, $soortval));
				$option->setSelected($soort == $key);
			}

			// Weergaveopties
			$form->add(HtmlInput::makeCheckbox("printableKoppenblad", False, null, null, _("Bekijk in printbaar koppenblad")));
			$form->add(new HtmlBreak());
			$form->add(HtmlInput::makeCheckbox("namenZichtbaar", True, null, null, _("Namen koppenblad zichtbaar")));
			$form->add(new HtmlBreak());

			// Submit die hap!
			$form->add(HtmlInput::makeSubmitbutton("Bekijk!"));

			// En laat dat zien
			$page->add(CommissieVerzamelingView::alleLedenOverzicht($cies));

			$page->end();
		}
	}

}
