<?php

abstract class Televisie_Controller
{
	static public function toonTelevisie ()
	{
		Page::getInstance('cleanhtml')->start();
		include('WhosWho4/Views/Televisie/toon2.php');
		Page::getInstance('cleanhtml')->end();
		/*
		//Vanaf vandaag 0:00
		$van = new DateTimeLocale('this week');
		$van->setTime(0,0);
		$tot = clone $van;
		$tot->modify("+7 days");

		$acts = ActiviteitVerzameling::getActiviteiten($van, $tot);

		$promos = array();
		for ($date = $van; $date < $tot; $date->modify("+1 days")) {
			$promo = $acts->subActiviteitenOpDag($date);
			if ($promo->aantal() > 0) {
				$promos[] = array('date' => clone $date, 'promos' => $promo);
			}
		}

		Page::getInstance('cleanhtml')->start();
		include('WhosWho4/Views/Televisie/toon.php');
		Page::getInstance('cleanhtml')->end();
		*/
	}

	public static function tvPosters()
	{
		//Vanaf vandaag 0:00
		$van = new DateTimeLocale('this week');
		$van->setTime(0, 0);
		$tot = clone $van;
		$tot->modify("+7 days");

		$acts = ActiviteitVerzameling::getActiviteiten($van, $tot);
		$eps = ExtraPosterVerzameling::posters(TRUE);

		$strippedActs = array();
		foreach ($acts as $id => $act) {
			$strippedActs[] = array(
				'id' => $id,
				'naam' => "" . $act,
				'pad' => $act->url() . "/ActPoster",
				'heeftPoster' => !!$act->posterPath(),
				'momentBegin' => $act->getMomentBegin()->format("Y-m-d H:i:s"),
				'momentEind' => $act->getMomentEind()->format("Y-m-d H:i:s"),
				'beschrijving' => "" . $act
			);
		}

		$strippedEps = array();
		foreach ($eps as $id => $ep) {
			$strippedEps[] = array(
				'id' => $id,
				'naam' => $ep->getNaam(),
				'pad' => '/Service/ExtraPoster/' . $ep->getExtraPosterID(),
				'beschrijving' => $ep->getBeschrijving()
			);
		}

/*
		// Voor testen van de samenvoeging van activiteitenposters en extraposters:
		function fakeArr($val, $nr) {
			$ret = array();
			while ($nr--) {
				$ret[] = array('value' => $val);
			}
			return $ret;
		}

		$strippedActs = fakeArr('A', 0);
		$strippedEps = fakeArr('B', 0);
*/

		/**
		 * We willen deze twee arrays samenvoegen:
		 * zoals:
		 * A = [ a1, a2, a3, a4, a5 ]
		 * B = [ b1, b2, b3 ]
		 * Dan C = [ a1, b1, a2, b2, a3, b3, a4, a5 ]
		 *
		 * OF:
		 * A = [ a1 ]
		 * B = [ b1, b2, b3 ]
		 * Dan C = [ b1, a1, b2, b3 ]
		 */

		$n = count($strippedActs);
		$m = count($strippedEps);
		$itAct = 0;
		$itEp = 0;
		$ret = array();
		$addAct = function ($times = 1) use (&$strippedActs, &$itAct, &$ret) {
			while ($times--) {
				$obj = $strippedActs[$itAct++];
				$obj['type'] = 'activiteit';
				$ret[] = $obj;
			}
		};
		$addEp = function ($times = 1) use (&$strippedEps, &$itEp, &$ret) {
			while ($times--) {
				$obj = $strippedEps[$itEp++];
				$obj['type'] = 'extra-poster';
				$ret[] = $obj;
			}
		};

		if ($n >= $m) {
			/**
			 * In dit geval hebben we meer posters van activiteiten dan extra posters
			 * Om de $perTime(+0 of +1) activiteiten komt een extraposter,
			 * In de eerste $numTime gevallen is dit $perTime + 1,
			 * In de andere gevallen $perTime
			 */
			$perTime = (int) floor($n / ($m + 1));
			$numExtra = $n % ($m + 1);
			for ($i = 0; $i < $m; $i++) {
				$addAct($perTime + ($i < $numExtra ? 1 : 0));
				$addEp();
			}
			$addAct($perTime);
		} else {
			// In dit geval doen we hetzelfde maar dan de rollen omgedraaid.
			$perTime = (int) floor($m / ($n + 1));
			$numExtra = $m % ($n + 1);
			for ($i = 0; $i < $n; $i++) {
				$addEp($perTime + ($i < $numExtra ? 1 : 0));
				$addAct();
			}
			$addEp($perTime);
		}
		if ($n != $itAct || $m != $itEp) {
			user_error("WARNING: iterators are not at the end of the array: " . $itAct . "/" . $n . ", " . $itEp . "/" . $m, E_USER_ERROR);
		}

		return new JSONResponse($ret);
	}
}
