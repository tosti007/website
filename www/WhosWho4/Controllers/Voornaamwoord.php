<?php

abstract class Voornaamwoord_Controller {
	/**
	 * @brief Geef een pagina met overzicht van alle voornaamwoorden.
	 *
	 * Heeft ook een mooi nieuwform.
	 *
	 * @site{/Service/Voornaamwoord/index.html}
	 */
	static public function overzicht() {
		$tabel = VoornaamwoordVerzamelingView::tabel();

		$page = new Html_Page();
		$page->start();

		$page->add($panel = new HtmlPanel(_("Beschikbare voornaamwoorden")));
		$panel->add(new HtmlParagraph(_("Je kan een van de volgende voornaamwoorden gebruiken voor jezelf:")));
		$panel->add($tabel);

		// Iedereen mag hun eigen voornaamwoorden aanmaken.
		if (hasAuth('ingelogd')) {
			$page->add($panel = new HtmlPanel(_("Maak nieuw voornaamwoord aan")));
			$panel->add($form = VoornaamwoordView::nieuwForm());
		}

		/*
		return new PageResponse($page);
		 */
		$page->end();
	}

	/**
	 * Handelt de variabele entry van een voornaamwoord af.
	 *
	 * @site{/Service/Voornaamwoord/ * /}
	 */
	static public function voornaamwoordEntry($args)
	{
		$woord = Voornaamwoord::geef($args[0]);
		if (!$woord) return false;

		return array('name' => $args[0],
					'displayName' => $woord,
					'access' => true);
	}

	/**
	 * @brief Stel de sitebeheerders op de hoogte dat een voornaamwoord gecheckt moet worden.
	 *
	 * Stuurt een mailtje naar de secretaris met de vraag of dit voornaamwoord klopt.
	 *
	 * Gaat ervan uit dat er een Persoon is ingelogd.
	 */
	static private function meldWijziging(Voornaamwoord $woord) {
		global $BESTUUR;

		$tekst = sprintf(_("Beste %s[VOC: secretaris]"), SECRNAME).",\n\n"
			.sprintf(_("We hebben een voornaamwoord van %s (%d) [VOC: naam en lidnr]
mogen ontvangen. Zou je willen checken dat hieronder niets beledigends staat?

> %s

Klopt er niets van, dan kun je het woord op %s [VOC: link naar pagina] wijzigen of verwijderen.

Genderneutrale groetjes van
De WebCie"),
			PersoonView::naam(Persoon::getIngelogd()),
			Persoon::getIngelogd()->geefID(),
			VoornaamwoordView::defaultWaardeVoornaamwoord($woord),
			HTTPS_ROOT . '/Service/Voornaamwoord'
		);

		sendmail(_('De Website') . ' <'.WWWEMAIL.'>', $BESTUUR['secretaris']->getEmail(), _('Er is een nieuw voornaamwoord'), $tekst);
	}

	/**
	 *  Maak een nieuw voornaamwoord.
	 *
	 * @site{/Service/Voornaamwoord/Nieuw}
	 */
	static public function nieuw()
	{
		// Iedereen mag hun eigen voornaamwoorden aanmaken.
		if (!hasAuth('ingelogd')) {
			return spaceHTTP(403);
		}

		$woord = new Voornaamwoord();
		$token = Token::processNamedForm();
		if ($token == "nieuwVoornaamwoord") {
			VoornaamwoordView::processForm($woord);
			if ($woord->valid()) {
				$woord->opslaan();
				if (!hasAuth('promocie')) {
					Voornaamwoord_Controller::meldWijziging($woord);
				}
				return Page::redirectMelding("/Service/Voornaamwoord", _("Nieuw voornaamwoord toegevoegd!"));
			}
		}
		$page = new Html_Page();
		$page->start();
		$page->add(VoornaamwoordView::nieuwForm($woord, $woord->valid()));
		$page->end();
		/*
		return new PageResponse($page);
		 */
	}

	/**
	 *  Wijzig het voornaamwoord met het variabele stukje ID.
	 *
	 * @site{/Service/Voornaamwoord/ * /Wijzig}
	 */
	static public function wijzig()
	{
		// Je wilt niet dat andere personen last hebben van iemand
		// die zomaar hun voornaamwoorden wijzigt, dus auth >= promocie
		if (!hasAuth('promocie')) {
			return spaceHTTP(403);
		}

		$vfs = vfsVarEntryNames();

		if (!($woord = Voornaamwoord::geef($vfs[0]))) {
			return spaceHTTP(403);
		}

		$token = Token::processNamedForm();
		if ($token == "wijzigVoornaamwoord") {
			VoornaamwoordView::processForm($woord);
			if ($woord->valid()) {
				$woord->opslaan();
				return Page::redirectMelding("/Service/Voornaamwoord", _("Voornaamwoord gewijzigd!"));
			}
		}
		$page = Page::getInstance()->start();
		$page->add(VoornaamwoordView::wijzigForm($woord));
		$page->end();
	}

	/**
	 *  Verwijder het voornaamwoord met het variabele stukje ID.
	 *
	 * @site{/Service/Voornaamwoord/ * /Verwijder}
	 */
	static public function verwijder()
	{
		// Je wilt niet dat andere personen last hebben van iemand
		// die zomaar hun voornaamwoorden verwijdert, dus auth >= promocie
		if (!hasAuth('promocie')) {
			return spaceHTTP(403);
		}

		$vfs = vfsVarEntryNames();

		if (!($woord = Voornaamwoord::geef($vfs[0]))) {
			return spaceHTTP(403);
		}

		$error = $woord->verwijderen();

		if (!empty($error)) {
			return Page::redirectMelding("/Service/Voornaamwoord", $error);
		} else {
			return Page::redirectMelding("/Service/Voornaamwoord", _("Voornaamwoord is verwijderd!"));
		}
	}

	/**
	 * @brief Stel dit voornaamwoord in voor de huidig ingelogde Persoon.
	 *
	 * @site{/Service/Voornaamwoord/ * /Kies}
	 */
	static public function kies()
	{
		// Je moet wel ingelogd zijn.
		$persoon = Persoon::getIngelogd();
		if (!$persoon) {
			return spaceHTTP(403);
		}

		$vfs = vfsVarEntryNames();

		if (!($woord = Voornaamwoord::geef($vfs[0]))) {
			return spaceHTTP(403);
		}

		$persoon->setVoornaamwoord($woord);

		// Dit zou geen error horen te geven, dus we doen gewoon opslaan.
		// (Geeft het wel een error, mag de WebCie uit de toekomst dit fixen.)
		$persoon->opslaan();

		return Page::redirectMelding("/Service/Voornaamwoord", _("Voornaamwoord gekozen!"));
	}
}
