<?php
abstract class Boekweb_Controller
{
	static public function artikeloverzicht()
	{
		$q = tryPar('artikel', '');
		$artikelen = ArtikelVerzameling::geefAlleBoekwebArtikelen(true, $q);

		$page = Page::getInstance()->start(_("Artikelen"));
		$page->add(new HtmlAnchor(BOEKWEBBASE . 'Artikel/Nieuw', _("Maak een nieuw artikel aan")));
		$page->add(ArtikelVerzamelingView::tabel($artikelen));
		$page->end();
	}

	static public function artikelEntry($args)
	{
		$artikel = Artikel::geef($args[0]);
		if(!$artikel)
			return array( 'name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array( 'name' => $artikel->geefID()
			, 'displayName' => _("Artikel")
			, 'access' => true);
	}

	static public function artikel()
	{
		if (!hasAuth('bestuur'))
			spaceHttp(403);
		$id = vfsVarEntryName();
		$artikel = Artikel::geef($id);
		if(!$artikel)
			spaceHttp(403);

		ArtikelView::artikel($artikel);
	}

	static public function artikelToevoegen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$artikelstr = strtolower(tryPar('artikel'));
		switch($artikelstr) {
		case 'boek':
			$artikel = new Boek();
			break;
		case 'aesartikel':
			$artikel = new AesArtikel();
			break;
		case 'idealkaartje':
			Page::redirectMelding('/Activiteiten', _("Selecteer een activiteit om een kaartje bij te maken."));
			break;
		case 'dibsproduct':
			$artikel = new DibsProduct();
			break;
		case 'dictaat':
			$artikel = new Dictaat();
			break;
		default:
			$artikel = new Artikel();
			break;
		}

		$show_error = false;
		if(Token::processNamedForm() == 'artikelForm') {
			$class = get_class($artikel) . 'View';
			if($a = $class::processNieuwForm($artikel))
				$artikel = $a;
			if($artikel->valid()) {
				$artikel->opslaan();
				Page::redirectMelding($artikel->url(), _("Artikel aangemaakt! Hoezee!"));
			} else {
				Page::addMelding(_("Er zijn fouten"), 'fout');
				$show_error = true;
			}
		}

		ArtikelView::wijzigForm($artikel, true, $show_error);
	}

	static public function artikelWijzigen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$id = vfsVarEntryName();
		$artikel = Artikel::geef($id);
		if(!$artikel)
			spaceHttp(403);

		$show_error = false;
		if(Token::processNamedForm() == 'artikelForm') {
			$class = get_class($artikel) . 'View';
			$class::processWijzigForm($artikel);
			if($artikel->valid()) {
				$artikel->opslaan();
				Page::redirectMelding($artikel->url(), _("Artikel gewijzigd! Hoezee!"));
			} else {
				Page::addMelding(_("Er zijn fouten"), 'fout');
				$show_error = true;
			}
		}

		ArtikelView::wijzigForm($artikel, false, $show_error);
	}

	static public function artikelVerwijderen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$id = vfsVarEntryName();
		$artikel = Artikel::geef($id);
		if(!$artikel)
			spaceHttp(403);

		$class = get_class($artikel) . "View";
		if(Token::processNamedForm() == get_class($artikel) . 'Verwijderen') {
			$naam = ArtikelView::waardeNaam($artikel);
			$returnVal = $artikel->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding(BOEKWEBBASE . '/Artikel', sprintf(_("Artikel %s[VOC: artikelnaam] is verwijderd"), $naam));
			}
		}

		$page = Page::getInstance()->start(sprintf(_("Verwijderen van %s[VOC: artikelnaam]"), ArtikelView::waardeNaam($artikel)));
		$page->add($class::verwijderForm($artikel));
		$page->end();
	}

	static public function iDealKaartjesToevoegen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$vfs = vfsVarEntryNames();

		if (!($activiteit = Activiteit::geef($vfs[1])))
			spaceHttp(404);

		$show_error = false;
		$msg = array();

		$iDealKaartje = new iDealKaartje($activiteit);

		if(Token::processNamedForm() == 'iDealKaartjeToevoegen') {
			$iDealKaartje = iDealKaartjeView::processNieuwForm($iDealKaartje);

			if($iDealKaartje->getExternVerkrijgbaar()
				&& $iDealKaartje->getDigitaalVerkrijgbaar() == 'NAMENLIJST')
				$msg[] = _("DigitaalVerkrijgbaar mag niet NAMENLIJST zijn terwijl "
				. "de kaartjes ook voor externen verkrijgbaar zijn!");

			$voorraad = new Voorraad($iDealKaartje, 'VIRTUEEL', tryPar('verkoopprijs'), 0);
			$voorraad->setOmschrijving('digitale editie')
				->setVerkoopbaar(true);

			if(!tryPar('verkoopprijs') || tryPar('verkoopprijs') <= 0)
				$msg[] = _("Er is een verkeerde prijs ingevoerd.");
			else {
				$prijs = new VerkoopPrijs($voorraad, new DateTimeLocale(), tryPar('verkoopprijs'));
				if (!$prijs->valid())
					$msg[] = _("Er is een verkeerde prijs ingevoerd.");
			}

			$levering = new Levering($voorraad, new DateTimeLocale(), tryPar('aantal'), null,
				Leverancier::geef(AESKWLEVERANCIER));
			if (!$levering->valid())
				$msg[] = _("Er is een verkeerde levering ingevoerd.");

			if (count($msg) == 0 && $iDealKaartje->valid() && $voorraad->valid()
				&& $prijs->valid() && $levering->valid())
			{
				$iDealKaartje->opslaan();
				$voorraad->opslaan();
				$prijs->opslaan();
				$levering->opslaan();

				Page::redirectMelding($activiteit->url(),
					_("Digitale kaartjes zijn nu beschikbaar."));
			} else if(count($msg) > 0) {
				foreach($msg as $m) {
					Page::addMelding($m, 'fout');
				}
				$show_error = true;
			} else {
				$show_error = true;
			}
		}

		iDealKaartjeView::toevoegen($iDealKaartje, $activiteit, $show_error);
	}

	static public function voorraadEntry($args)
	{
		$voorraad = Voorraad::geef($args[0]);
		$artikel = Artikel::geef($args[2]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			return array( 'name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array( 'name' => $voorraad->geefID()
			, 'displayName' => _("Voorraad")
			, 'access' => hasAuth('bestuur'));
	}

	static public function voorraad()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		VoorraadView::details($voorraad);
	}

	static public function voorraadToevoegen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$id = vfsVarEntryName();
		$artikel = Artikel::geef($id);
		if(!$artikel)
			spaceHttp(403);

		$voorraad = new Voorraad($artikel, 'VIRTUEEL', NULL, 'NULL');
		$show_error = false;
		if(Token::processNamedForm() == 'voorraadForm') {
			$voorraad = VoorraadView::processNieuwForm($voorraad);
			if($voorraad->valid()) {
				$voorraad->opslaan();
				Page::redirectMelding($voorraad->url(), _("Toevoegen van de voorraad gelukt!"));
			} else {
				$show_error = true;
				Page::addMelding(_("Er waren fouten"), 'fout');
			}
		}

		VoorraadView::wijzigForm($voorraad, true, $show_error);
	}

	static public function voorraadWijzigen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		$show_error = false;

		if(Token::processNamedForm() == 'voorraadForm') {
			VoorraadView::processWijzigForm($voorraad);
			if($voorraad->valid()) {
				$voorraad->opslaan();
				Page::redirectMelding($voorraad->url(), _("Wijzigen van de voorraad gelukt!"));
			} else {
				$show_error = true;
				Page::addMelding(_("Er waren fouten"), 'fout');
			}
		}

		VoorraadView::wijzigForm($voorraad, false, $show_error);
		p(403);
	}

	static public function voorraadVerwijderen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		if(Token::processNamedForm() == 'VoorraadVerwijderen') {
			$id = $voorraad->geefID();
			$returnVal = $voorraad->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding($artikel->url(), sprintf(_("Voorraad %s[VOC: voorraadID] is verwijderd"), $id));
			}
		}

		$page = Page::getInstance()->start(sprintf(_("Verwijderen van %s[VOC: voorraadID]"), $voorraad->geefID()));
		$page->add(VoorraadView::verwijderForm($voorraad));
		$page->end();
	}

	static public function voorraadLeveringToevoegen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		$show_error = false;
		$levering = new Levering($voorraad);

		if(Token::processNamedForm() == 'leveringForm')
		{
			$levering = LeveringView::processNieuwForm($levering);
			if($levering->valid()) {
				$order = $levering->getOrder();
				if (!is_null($order) && !$order->valid()) {
					Page::addMelding(_("Er is een fout in de order!"), 'fout');
					$show_error = true;
				}
			} else {
				Page::addMelding(_("Er is een fout in de levering!"), 'fout');
				$show_error = true;
			}

			if ($show_error) {
				Page::addMelding(_("Er waren fouten"), 'fout');
			} else {
				if (!is_null($order)) {
					$order->opslaan();
				}
				$levering->opslaan();
				Page::redirectMelding($voorraad->url(), _("Toevoegen van de levering gelukt!"));
			}
		}

		LeveringView::wijzigForm($levering, $show_error);
	}

	static public function voorraadVerplaatsen()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		$show_error = false;
		$tegenvoorraad = new Voorraad($artikel);
		$verplaatsing = new Verplaatsing($voorraad, new DateTimeLocale(), 0, $tegenvoorraad);

		if(Token::processNamedForm() == 'verplaatsenForm')
		{
			$verplaatsing = VerplaatsingView::processNieuwForm($verplaatsing);
			if($verplaatsing->valid()) {
				$verplaatsing->opslaan();
				Page::addMelding(_("Verplaatsen naar andere voorraad gelukt!"));
				Page::redirectMelding($voorraad->getArtikel()->url(),
					_('Let erop dat de nieuwe voorraad niet dezelfde verkoopprijs '
					. 'hoeft te hebben en/of niet dezelfde verkoopbaarheid hoeft te hebben.'));
			} else {
				$show_error = true;
				Page::addMelding(_("Er waren fouten"), 'fout');
			}
		}

		VerplaatsingView::wijzigForm($verplaatsing, $show_error);
	}

	static public function voorraadGeschiedenisVerkoopPrijs()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		$vps = VerkoopPrijsVerzameling::geefAlleVanVoorraad($voorraad);

		VerkoopPrijsVerzamelingView::voorraadGeschiedenis($vps, $voorraad);
	}

	static public function voorraadWijzigVerkoopPrijs()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$ids = vfsVarEntryNames();
		$voorraad = Voorraad::geef($ids[0]);
		$artikel = Artikel::geef($ids[1]);

		if(!$voorraad || !$artikel || $voorraad->getArtikel() != $artikel)
			spaceHttp(403);

		$show_error = false;
		$vp = new VerkoopPrijs($voorraad, new DateTimeLocale());

		if(Token::processNamedForm() == 'verkoopprijsForm')
		{
			$vp = VerkoopPrijsView::processNieuwForm($vp);
			if($vp->valid()) {
				$vp->opslaan();
				Page::redirectMelding($voorraad->url(), _("Wijzigen van de verkoopprijs gelukt!"));
			} else {
				$show_error = true;
				Page::addMelding(_("Er waren fouten"), 'fout');
			}
		}

		VerkoopPrijsView::wijzigForm($vp, $show_error);
	}

	static public function bestellingOverzicht() {
		if (!hasAuth('bestuur'))
			spaceHttp(403);

		$bestellingen = BestellingVerzameling::geefAlleBestellingen();
		BestellingVerzamelingView::BestellingOverzicht($bestellingen);
	}

	static public function kaartjeKopen()
	{
		$return = array();

		$vfs = vfsVarEntryNames();

		if (!($activiteit = Activiteit::geef($vfs[1])))
			spaceHttp(404);

		$iDealkaartjeVerzameling = $activiteit->getIdealKaartjes();
		if ($iDealkaartjeVerzameling->aantal() == 0) {
			Page::redirectMelding($activiteit->url(),
				_("Deze activiteit heeft geen kaartjes!"));
			return;
		}

		// We maken hier een Dummy-iDeal aan om antwoorden aan te koppelen
		$ideal = new iDeal("BOEKWEB", "VERKOOP", Persoon::getIngelogd(), 0,
			'Kaartjes', new DateTimeLocale(),
			'',
			$iDealkaartjeVerzameling->first()->getVoorraden()->first(),
			EVENEMENTENREKENING, null);

		$antwoorden = $ideal->getAntwoorden();

		if(Token::processNamedForm() == 'IDEAL Transactie')
		{
			$voorraad = Voorraad::geef(tryPar('kaartje'));
			if(!$voorraad)
				$return[] = _("Je hebt nog geen kaartje geselecteerd!");

			if(sizeof($return) == 0) {
				$iDealkaartje = $voorraad->getArtikel();
				if(is_null(Persoon::getIngelogd()) && !($iDealkaartje->getExternVerkrijgbaar()))
					$return[] = _("Je mag dit kaartje helemaal niet kopen!");

				if($iDealkaartje->getActiviteit() != $activiteit)
					$return[] = _("Je mag dit kaartje helemaal niet kopen!");

				if(!tryPar('email'))
					$return[] = _("Je hebt geen e-mailadres ingevuld!");
				elseif(checkEmailFormat(tryPar('email')) == 0)
					$return[] = _("Je hebt geen geldig e-mailadres ingevuld!");
				elseif(tryPar('email') != tryPar('emailVerificatie'))
					$return[] = _("De twee e-mailadressen komen niet overeen");

				$aantal = tryPar('aantal');
				if($aantal <= 0) {
					$aantal = tryPar('aantalKopen');
					if($aantal <= 0)
						$return[] = _("Je moet minstens 1 kaartje kopen");
				}
				if($aantal > $voorraad->getAantal())
					$return[] = _("Er zijn niet zoveel kaartjes beschikbaar");

				if($aantal > 1 && $iDealkaartje->isPerPersoon()) {
					$return[] = _("Omdat dit kaartje per persoon geldt, "
						. "is het mogelijk om maar 1 kaartje te kopen.");
				}

				$ideal = new iDeal("BOEKWEB", "VERKOOP", Persoon::getIngelogd(),
					$aantal * $voorraad->getVerkoopPrijs()->getPrijs() +
						iDeal::transactiekosten()->alsFloat(),
					"Kaartjes " . $iDealkaartje->getNaam(), new DateTimeLocale(),
					"IDEALTransactie voor kaartjes", $voorraad, EVENEMENTENREKENING, null);
				$antwoorden = $ideal->getAntwoorden();

				if($antwoorden->aantal() > 0)
					iDealAntwoordVerzamelingView::processAntwoordForm($antwoorden);

				if(sizeof($return) == 0 && $antwoorden->allValid())
				{
					$persoon = Persoon::getIngelogd();
					$ideal->setEmailAdres(tryPar('email'));
					$ideal->setReturnURL($iDealkaartje->getReturnURL());
					$ideal->setStatus('INACTIVE');
					$ideal->opslaan();
					$antwoorden->opslaan();

					$ideal->startTransactie();
				}
				else if(sizeof($return) == 0)
				{
					foreach($antwoorden as $antwoord)
					{
						if($antwoord->valid())
							continue;

						Page::addMelding(sprintf('%s %s:', _('Fout bij vraag'),
							ActiviteitVraagView::waardeVraag($antwoord->getVraag())), 'fout');

						foreach($antwoord->getErrors() as $error)
							if($error)
								Page::addMelding($error, 'fout');
					}
				}
				else
					Page::addMeldingArray($return, 'fout');

			}
		}

		iDealKaartjeView::kaartjeKopen($iDealkaartjeVerzameling, $activiteit, $antwoorden);
	}

	static public function kaartjeScan()
	{
		$vfs = vfsVarEntryNames();

		if (!($activiteit = Activiteit::geef($vfs[1])))
			spaceHttp(404);

		$kaartjes = iDealKaartjeVerzameling::geefScanbareKaartjes($activiteit);

		if($kaartjes->aantal() != 0) {
			header("Content-type: text/json");

			$code = tryPar('code', NULL);
			echo "{";
			if(is_null($code) || empty($code)) {
				echo "\"valid\": false, \"error\": \"Geen code meegestuurd!\", \"color\": \"#FF0000\"";
			} else {
				$kcodes = $kaartjes->geefKaartjesCodes();

				$gevonden = false;
				foreach($kcodes as $kcode) {
					if($kcode->getCode() === $code && !($kcode->getGescand())) {
						echo "\"valid\": true, \"error\": \"\", \"color\":\"#00FF00\"";
						$kcode->setGescand(true);
						$kcode->opslaan();
						$gevonden = true;
						break;
					} else if($kcode->getCode() === $code) {
						echo "\"valid\": false, \"error\": \"Is al gescand!\", \"color\": \"#FF0000\"";
						$gevonden = true;
						break;
					}
				}
				if(!$gevonden) {
					echo "\"valid\": false, \"error\": \"Code bestaat niet!\", \"color\": \"#FF0000\"";
				}
			}

			echo "}";

			die();
		} else if(!hasAuth('bestuur')) {
			spaceHttp(403);
		} else {
			if(Token::processNamedForm() == 'activeer') {
				$kaartjes = $activiteit->getIdealKaartjes();
				foreach($kaartjes as $kaartje) {
					$kaartje->setMagScannen(true);
				}
				$kaartjes->opslaan();

				Page::redirectMelding($activiteit->url(), _("De kaartjes kunnen nu gescand worden!"));
			}

			$page = Page::getInstance()->start(_("Activeer kaartjes om te scannen"));

			$page->add(new HtmlParagraph(_("Weet je zeker dat je de kaartjes van deze activiteit wilt activeren zodat ze gescand kunnen worden?")));
			$page->add($form = HtmlForm::named('activeer'));
			$form->add(HtmlInput::makeSubmitButton(_("Ja")));

			$page->end();
		}
	}

	static public function offerteOverzicht()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$offertes = OfferteVerzameling::geefAlleOffertes();
		OfferteVerzamelingView::offerteOverzicht($offertes);
	}
	static public function verkoopGeschiedenis()
	{
		if(!hasAuth('bestuur')) {
			spaceHttp(403);
		}

		$verkopen = BoekverkoopVerzameling::geefBoekverkopen();

		BoekverkoopVerzamelingView::verkoopGeschiedenis($verkopen);
	}

	static public function verkoopEntry($args)
	{
		$verkoop = Boekverkoop::geefLaatsteBoekverkoop($args[0]);
		if(!$verkoop)
			return array( 'name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array( 'name' => $verkoop->getVerkoopdag()
			, 'displayName' => _("Transactie")
			, 'access' => hasAuth('bestuur'));
	}

	static public function verkoopView()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);
		$id = vfsVarEntryName();

		$verkopen = BoekverkoopVerzameling::geefAlleVanDag($id);

		$transacties = new TransactieVerzameling();
		foreach($verkopen as $verkoop) {
			$transacties->union(TransactieVerzameling::getTransactiesVanVerkoop($verkoop));
		}

		BoekverkoopVerzamelingView::boekverkoop($verkopen, $transacties);
	}

	static public function bekijkTransacties()
	{
		if(!hasAuth('bestuur')) {
			spaceHttp(403);
		}

		$begin = new DateTimeLocale();
		$eind = new DateTimeLocale();
		$eind->add(new DateInterval('P1D'));

		$begin = new DateTimeLocale(tryPar('begin'));
		$eind = new DateTimeLocale(tryPar('eind'));

		if(!is_null(tryPar('rubriek', null))) {
			$rubrieken = Transactie::enumsRubriek();
			$rubriek_id = tryPar('rubriek');
			$rubriek = $rubrieken[$rubriek_id];
		} else {
			$rubriek = $rubriek_id = NULL;
		}
		if(!is_null(tryPar('soort', null))) {
			$soorten = Transactie::enumsSoort();
			$soort_id = tryPar('soort');
			$soort = $soorten[$soort_id];
		} else {
			$soort = $soort_id = NULL;
		}

		if($begin > $eind) {
			Page::addMelding(_("De begintijd mag niet eerder zijn dan de eindtijd"), 'fout');
			$transacties = new TransactieVerzameling();
		} else {
			$transacties = TransactieVerzameling::geefAlleVanBeginEnEindEnRubriekEnSoort($begin, $eind, $rubriek, $soort);
		}

		TransactieVerzamelingView::overzicht($transacties, $begin, $eind, $rubriek_id, $soort_id);
	}

	static public function transactieEntry($args)
	{
		$transactie = Transactie::geef($args[0]);
		if(!$transactie)
			return array( 'name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array( 'name' => $transactie->geefID()
			, 'displayName' => _("Transactie")
			, 'access' => hasAuth('bestuur'));
	}

	static public function transactie()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$id = vfsVarEntryName();
		$transactie = Transactie::geef($id);
		if(!$transactie)
			spaceHttp(403);

		TransactieView::transactie($transactie);
	}

	static public function verkoopOverzicht()
	{
		if(!hasAuth('bestuur')) {
			spaceHttp(403);
		}

		$begin = new DateTimeLocale('today');
		$eind = new DateTimeLocale('tomorrow');

		$transacties = TransactieVerzameling::geefAlleVanBeginEnEind($begin, $eind);

		TransactieVerzamelingView::verkoopOverzicht($transacties, $begin, $eind);
	}

	static public function boekHome()
	{
		$page = Page::getInstance()->start(_('Boekweb'));

		$page->add($form = HtmlForm::named('BoekZoek', BOEKWEBBASE . 'Zoeken'));
		$form->addClass('form-inline');
		$form->add(HtmlInput::makeText('zoekStr'))
			->add(HtmlInput::makeSubmitButton(_('Zoek')));

		$page->add($rowdiv = new HtmlDiv(null, 'row'));
		$rowdiv->add($div = new HtmlDiv(null, 'col-md-6'));

		$div->add(new HtmlHeader(3, _("Voor de verkopers")));
		$div->add($list = new HtmlList());
		$verkoop = Boekverkoop::geefLaatsteBoekverkoop(date('Y-m-d'));
		if(!is_null($verkoop) && (is_null($verkoop->getGesloten()) || $verkoop->getGesloten() == new DateTimeLocale())) {
			$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Verkoop/Sluitverkoop', _('Sluit boekverkoop (TODO)')));
			$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Verkoop/', _('Verkoop-menu (TODO)')));
		} else {
			$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Verkoop/Openverkoop', _('Open boekverkoop (TODO)')));
			$list->addChild(_("Verkoop-menu"));
		}
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Transactie', _('Toon alle transacties (TODO)')));

		$begin = new DateTimeLocale();
		$eind = new DateTimeLocale();
		$eind->add(new DateInterval('P1D'));

		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Transactie/?begin=' . $begin->format('Y-m-d') . '&eind=' . $eind->format('Y-m-d'), _('Toon alle transacties van vandaag (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Transactie/Perproduct', _('Toon verkopen per product (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Verkoop/Overzicht', _('Toon verkoopoverzicht (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Verkoop/Kasverschillen', _('Toon kasverschillen van vandaag (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Telling/', _('Bekijk alle tellingen (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Telling/DoeTelling', _('Doe een voorraadtelling (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Grootboeken', _('Grootboeken (TODO)')));

		$rowdiv->add($div = new HtmlDiv(null, 'col-md-6'));
		$div->add(new HtmlHeader(3, _('Voor leden')));
		$div->add($list = new HtmlList());

		$list->addChild(new HtmlAnchor(BOEKWEBBASE, _('Alle artikelen die direct verkrijgbaar zijn zonder te bestellen (TODO)')));
		$list->addChild(array(new HtmlAnchor(PAPIERMOLENBASE, _('Papiermolen'))
			, _('tweedehands boekenmarkt')));

		$page->add(new HtmlHeader(2, _("Voor de boekencommissaris")));
		$page->add($rowdiv = new HtmlDiv(null, 'row'));

		$rowdiv->add($div = new HtmlDiv(null, 'col-md-3'));
		$div->add(new HtmlHeader(3, _("Algemeen")));
		$div->add($list = new HtmlList());
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Boekenlijst', _('Boekenlijsten (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Verkoop/Geschiedenis', _('Geschiedenis boekverkopen (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Docentenmail', _('Docenten mailen')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Periode', _('Lesperiodes invoeren')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Constants', _('Constants (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Stats', _('Statistieken (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'LoyverseExport', _('Exporteer lidgegevens naar Loyverse')));

		$rowdiv->add($div = new HtmlDiv(null, 'col-md-3'));
		$div->add(new HtmlHeader(3, _("Voorraad")));
		$div->add($list = new HtmlList());
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Voorraden', _('Alle voorraden (TODO)')));

		$rowdiv->add($div = new HtmlDiv(null, 'col-md-3'));
		$div->add(new HtmlHeader(3, _('Leveringen')));
		$div->add($list = new HtmlList());

		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Levering/Invoeren', _('Levering invoeren (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Levering/', _('Leveringen tonen')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Leverancier/', _('Leveranciers bekijken')));

		$rowdiv->add($div = new HtmlDiv(null, 'col-md-3'));
		$div->add(new HtmlHeader(3, _("Artikelen")));
		$div->add($list = new HtmlList());
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Artikel/', _('Artikeloverzicht (TODO)')));
		$list->addChild(new HtmlAnchor(BOEKWEBBASE . 'Artikel/Nieuw', _('Artikel invoeren')));
		$list->addChild(new HtmlAnchor('/Onderwijs/Vak/Nieuw', _('Vak invoeren')));
		$list->addChild(new HtmlAnchor('/Onderwijs/Vak/NieuwJaargang', _('Jaargang invoeren')));
		$list->addChild(new HtmlAnchor('/Onderwijs/Vak/NieuwCSV', _('CSV met vakken invoeren')));

		$page->end();
	}

	static public function perProduct()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$res = ArtikelView::processZoekForm();

		VoorraadMutatieVerzamelingView::perProduct($res);
	}

	static public function doeTelling()
	{
		if(!hasAuth('bestuur'))
			spaceHttp(403);

		$artikelen = ArtikelVerzameling::geefAlleBoekwebArtikelen();

		if(Token::processNamedForm() == "Telling") {
			$voorraden = tryPar('voorraad', array());

			if(sizeof($voorraden) == 0) {
				Page::addMelding(_("Er zijn geen voorraden geteld jôh!"), 'fout');
			} else {
				$telling = new Telling(new DateTimeLocale(), Persoon::getIngelogd());
				$telling->setOpmerking(tryPar('opmerking'));
				$telling->opslaan();

				foreach($voorraden as $voorraadID => $aantal) {
					$voorraad = Voorraad::geef($voorraadID);

					$tellingItem = new TellingItem($telling, $voorraad);
					$tellingItem->setAantal($aantal);
					$tellingItem->opslaan();
				}

				Page::redirectMelding(BOEKWEBBASE, _("Teltje ingevoerd!"));
			}
		}

		TellingView::doeTelling($artikelen);
	}

	static public function zoeken()
	{
		requireAuth('bestuur');

		$artikelen = new ArtikelVerzameling();

		if(Token::processNamedForm() == 'BoekZoek')
		{
			$str = tryPar('zoekStr');
			if(!$str = tryPar('zoekStr'))
				Page::addMelding(_('Je moet wel op iets zoeken!'), 'fout');
			else
			{
				$artikelen = ArtikelVerzameling::zoek($str);
			}
		}

		ArtikelView::zoeken($artikelen);
	}
}
