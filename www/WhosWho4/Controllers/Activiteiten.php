<?php

use Intervention\Image\ImageManagerStatic;
use Symfony\Component\HttpFoundation\Response;

/**
 * /Activiteiten			-> (activiteiten) bepaal naar welke functie geredirect moet worden
 *  /Aankomend				-> (aankomend) aankomende activiteiten
 *   /ics					-> (ics) ics bestand van aankomende activiteiten
 *   /rss					-> (rss) rss feed van aankomende activiteiten
 *  /Archief				-> (archief) afgelopen activiteiten
 *  /Toevoegen				-> (toevoegen) een nieuwe activiteit aanmaken
 *	/Computer reserveren	-> (computerReserveren) een handig overzicht van de gereserveerde computers
 *  /act_id					-> (details) details van een activiteit
 *   /Inschrijven			-> (inschrijven) schrijf jezelf in voor een activiteit
 *   /Uitschrijven			-> (uitschrijven) schrijf jezelf uit voor een activiteit
 *   /Deelnemers			-> (deelnemers) details over de deelnemers van een activiteit + uitschrijven + kart
 *    /Toevoegen			-> (deelnemersInschrijven) deelnemers inschrijven
 *   /Wijzig				-> (wijzig) wijzig een activiteit
 *    /Promoveer			-> (promoveer) promoveer een herhalende activiteit naar hoofdactiviteit
 *   /Verwijder				-> (verwijder) verwijder een activiteit
 *   /Herhalingen			-> (herhalingen) toon alle herhalende activiteiten
 */

final class Activiteiten_Controller extends Controller
{
	/**
	 * Print de aankomende activiteiten op het scherm
	 *
	 * @site{/Activiteiten/index.html}
	 */
	public function activiteiten ()
	{
		/** Stel vast of er ingelogd is **/
		$pers = $this->getIngelogd();

		if(is_null($pers))
			$detail = 'publiek';

		/** Verwerk data **/
		$matrix = $this->getRequestParam('matrix', 0);
		$filter = $this->getRequestParam('filter', 'alle');
		$catFilter = $this->getRequestParam('catFilter', '');
		$cat = ActiviteitCategorie::geef($catFilter);

		//Vanaf vandaag 0:00
		$van = new DateTimeLocale();
		$van->setTime(0,0);
		$tot = clone $van;
		$tot->modify("+1 year");

		/** Laad alle activiteiten **/
		$activiteiten = ActiviteitVerzameling::getActiviteiten($van, $tot, $filter,
			'', '', 0, $cat)->filterBekijken();

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitVerzamelingView::aankomend($activiteiten, $matrix));
	}

	/**
	 * Het ICS-bestand van alle aankomende activiteiten
	 *
	 * @site{/Activiteiten/Agenda/ics}
	 */
	public function ics ()
	{
		/** Bepaal de range **/
		$van = new DateTimeLocale();
		$van->setTime(0,0);
		$tot = clone $van;
		$tot->modify("+1 year");

		/** Laad alle activiteiten **/
		$activiteiten = ActiviteitVerzameling::getActiviteiten($van, $tot)->filterBekijken();

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitVerzamelingView::ics($activiteiten));
	}

	public function icsActiviteit()
	{
		global $logger;
		$ids = $this->getEntryNames();
		$act = Activiteit::geef($ids[1]);
		return $this->pageResponse(ActiviteitView::ics($act));
	}

	/**
	 * De RSS-feed van alle aankomende activiteiten
	 *
	 * @site{/Activiteiten/Agenda/rss}
	 */
	public function rss ()
	{
		/** Laad alle activiteiten **/
		$activiteiten = ActiviteitVerzameling::getActiviteiten(
				time(), strtotime(time() . ' +1 year'));

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitVerzamelingView::rss($activiteiten));
	}

	/**
	 * Print de afgelopen activiteiten op het scherm
	 *
	 * @site{/Activiteiten/Afgelopen}
	 */
	public function archief ()
	{
		/** Verwerk get-data **/
		$matrix		= $this->getRequestParam('matrix', 0);
		$colyear 	= (int) $this->getRequestParam('colyear', colJaar());
		$filter = $this->getRequestParam('filter', 'alle');
		$catFilter = $this->getRequestParam('catFilter', '');
		$cat = ActiviteitCategorie::geef($catFilter);

		if($colyear < 0 || $colyear > date('Y'))
			return $this->redirectMelding("/Activiteiten/Afgelopen",
				sprintf(_("Het archief van collegejaar %s[VOC: collegejaar] bestaat niet"), $colyear));

		$van = strtotime($colyear . '-' . COLJAARSWITCHMAAND . '-1');
		$tot = strtotime(($colyear + 1) . '-' . COLJAARSWITCHMAAND . '-1');
		if($tot > time())
			$tot = time();
		$colyear = date('Y', $van); //Voorkom inconsequenties door parsen van bv `02

		/** Laad alle activiteiten **/
		$activiteiten = ActiviteitVerzameling::getActiviteiten(
				$van, $tot, $filter, '', '', 0, $cat)->reverse()->filterBekijken();

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitVerzamelingView::archief($activiteiten, $matrix, $colyear));
	}

	/**
	 * Voeg een nieuwe activiteit toe
	 *
	 * @site{/Activiteiten/Toevoegen}
	 * @throws HTTPStatusException
	 */
	public function toevoegen ()
	{
		if(!$this->getIngelogd()->getCommissies())
			$this->geenToegang();

		$show_error = false;

		/** Verwerk formulier-data **/
		$newAct = new ActiviteitInformatie();
		$newAct->setMomentEind(new DateTimeLocale("+1 hour"));

		$cieActs = new CommissieActiviteitVerzameling();
		$vragen = new ActiviteitVraagVerzameling();
		$herhalingen = new ActiviteitVerzameling();
		if (Token::processNamedForm() == 'ActNieuw')
		{
			if ($this->verwerkActiviteitToevoegen($newAct, $cieActs, $vragen, $herhalingen)){
				return $this->redirectMelding($newAct->url(), _("De activiteit is nu aangemaakt!"));
			} else {
				// jammer dan.
				$show_error = true;
			}
		}

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitInformatieView::wijzigen($newAct, $cieActs, $vragen, true, $show_error));
	}

	/**
	 * Verwerkt het activiteit toevoegen
	 *
	 * @param ActiviteitInformatie $newAct
	 * @param CommissieActiviteitVerzameling $cieActs
	 * @param ActiviteitVraagVerzameling $vragen
	 * @param ActiviteitVerzameling $herhalingen
	 * @return boolean of het activiteit aanmaken gelukt is. Zoniet, moeten fouten
	 *         getoond worden ($show_error)
	 */
	public function verwerkActiviteitToevoegen(
			ActiviteitInformatie $newAct,
			CommissieActiviteitVerzameling $cieActs,
			ActiviteitVraagVerzameling $vragen,
			ActiviteitVerzameling $herhalingen)
	{
		ActiviteitInformatieView::processWijzigForm($newAct, $cieActs, $vragen, $herhalingen);
		if ($newAct->valid('nieuw')
			&& $herhalingen->allValid()
			&& $cieActs->aantal() != 0 && $cieActs->allValid())
		{
			//Vragen kunnen alleen worden toegevoegd als de activiteit al bestaat
			//Sla de activiteit dus op en check of de vragen valid zijn
			$newAct->opslaan();
			ActiviteitInformatieView::processWijzigFormVragen($newAct, $vragen);
			if($vragen->allValid()){
				$vragen->opslaan();
				$herhalingen->opslaan();
				$cieActs->opslaan();

				// Voeg de achievement toe van "activiteit aanmaken"
				if($pers = $this->getIngelogd()) {
					$persBadges = $pers->getBadges();
					$persBadges->addBadge('activiteit');
				}
				return true;
			}
			else{
				$newAct->verwijderen();
			}
		}
		else
		{
			Page::addMelding(_("Er waren fouten:"), 'fout');
			if (!$herhalingen->allValid())
				Page::addMelding(_("De herhalingsactiviteiten bevatten een fout"), 'fout');
			if (!$cieActs->allValid())
				Page::addMelding(_("De commissies bij de activiteit bevatten een fout"), 'fout');
		}
		if($this->getRequestParam('ActiviteitInformatie[Herhalend][check]'))
		{
			$vanaf = new DateTimeLocale($this->getRequestParam('ActiviteitInformatie[Herhalend][vanaf]'));
			$tot = new DateTimeLocale($this->getRequestParam('ActiviteitInformatie[Herhalend][tot]'));
			if($vanaf > $tot)
				Page::addMelding(_("'Herhalen vanaf' moet eerder zijn dan 'Herhalen tot'"), 'fout');

			$begin = new DateTimeLocale($this->getRequestParam('ActiviteitInformatie[Herhalend][begin]'));
			$eind = new DateTimeLocale($this->getRequestParam('ActiviteitInformatie[Herhalend][eind]'));
			if($begin > $eind)
				Page::addMelding(_("De begintijd moet eerder zijn dan de eindtijd"), 'fout');

			$dag = $this->getRequestParam('ActiviteitInformatie[HerhalendOp]');
			if($dag['ma'] === "0"
				&& $dag['di'] === "0"
				&& $dag['wo'] === "0"
				&& $dag['do'] === "0"
				&& $dag['vr'] === "0"
				&& $dag['za'] === "0"
				&& $dag['zo'] === "0")
				Page::addMelding(_("Er moet minstens een dag voor de herhaling geselecteerd zijn"), 'fout');

			if($this->getRequestParam('ActiviteitInformatie[Herhalend][numweek]') <= 0)
				Page::addMelding(_("Er moet een positief aantal weken ingevuld worden"), 'fout');
		}
		if(!$herhalingen->allValid())
		{
			$begin = $newAct->getMomentBegin();
			$eind = $newAct->getMomentEind();
			$max = $newAct->getMaxAantalComputers($begin, $eind);
			foreach($herhalingen as $her)
			{
				$begin = $her->getMomentBegin();
				$eind = $her->getMomentEind();
				$max = min($max, $her->getInformatie()->getMaxAantalComputers($begin, $eind));
			}

			Page::addMelding(sprintf(_('Er kan een maximum van %s[VOC:aantal computers] gereserveerd worden'), $max), 'fout');
		}
		if(!$vragen->allValid())
		{
			Page::addMelding(_("Er staan fouten in de vragen"), 'fout');
		}
		if($cieActs->aantal() == 0 || !$cieActs->allValid())
		{
			Page::addMelding(_("Er moet minstens 1 commissie verantwoordelijk zijn"), 'fout');
		}
		// Er is iets fout gegaan.
		return false;
	}

	/**
	 * Wijzig een activiteit
	 *
	 * @site{/Activiteiten/ * / * / * /Wijzig/index.html}
	 * @throws HTTPStatusException
	 */
	public function wijzig ()
	{
		/** Laad de gekozen activiteit **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);

		if (!$act || !$act->magWijzigen())
			$this->geenToegang();

		$show_error = false;

		$cieActs = $act->getActiviteitCommissies();
		$vragen = $act->getVragen();
		$herhalingen = $act->getHerhalingsChildren();

		/** Verwerk formulier-data **/
		if (Token::processNamedForm() == 'ActWijzig')
		{
			if($act instanceof ActiviteitInformatie)
			{
				ActiviteitInformatieView::processWijzigForm($act,
					$cieActs = new CommissieActiviteitVerzameling(),
					$vragen,
					$herhalingen);
				ActiviteitInformatieView::processWijzigFormVragen($act, $vragen);

				if ($act->valid()
					&& $cieActs->aantal() != 0 && $cieActs->allValid()
					&& $vragen->allValid()
					&& $herhalingen->allValid())
				{
					// Verwerk commissies
					$cieActsOud = $act->getActiviteitCommissies();
					foreach($cieActsOud as $cieAct)
					{
						if(!$cieActs->bevat($cieAct))
						{
							$cieActsOud->verwijder($cieAct);
							$cieAct->verwijderen();
						}
					}
					$cieActs->complement($cieActsOud);
					$cieActs->opslaan();

					$vragen->opslaan();

					$act->opslaan();

					return $this->redirectMelding($act->url(), _("De activiteit is nu gewijzigd!"));
				}
				else
				{
					Page::addMelding(_("Er waren fouten"), 'fout');
					$show_error = true;
				}
				if(!$herhalingen->allValid())
				{
					$begin = $act->getMomentBegin();
					$eind = $act->getMomentEind();
					$max = $act->getMaxAantalComputers($begin, $eind);
					foreach($herhalingen as $her)
					{
						$begin = $her->getMomentBegin();
						$eind = $her->getMomentEind();
						$max = min($max, $her->getMaxAantalComputers($begin, $eind));
					}

					Page::addMelding(sprintf(_('Er kan een maximum van %s[VOC:aantal computers] gereserveerd worden'), $max), 'fout');
				}
				if(!$vragen->allValid())
				{
					Page::addMelding(_("Er staan fouten in de vragen"), 'fout');
					foreach($vragen as $vraag)
						foreach($vraag->getErrors() as $error)
						{
							if(is_array($error))
							{
								foreach($error as $e)
								{
									if($e)
										Page::addMelding($e, 'fout');
								}
							}
							else if($error)
								Page::addMelding($error, 'fout');
						}
				}
				if($cieActs->aantal() == 0 || !$cieActs->allValid())
				{
					Page::addMelding(_("Er moet minstens 1 commissie verantwoordelijk zijn"), 'fout');
				}
			}
			else if($act instanceof ActiviteitHerhaling)
			{
				ActiviteitHerhalingView::processWijzigForm($act);
				if($act->valid())
				{
					$act->opslaan();

					return $this->redirectMelding($act->url(), _("De activiteit is nu gewijzigd!"));
				}
				else
				{
					Page::addMelding(_("Er waren fouten"), 'fout');
					$show_error = true;
				}
			}
		}

		/** Ga naar de view **/
		if($act instanceof ActiviteitInformatie)
			$page = ActiviteitInformatieView::wijzigen($act, $cieActs, $vragen, false ,$show_error);
		else
			$page =ActiviteitHerhalingView::wijzigen($act, $show_error);
		return $this->pageResponse($page);
	}

	/**
	 * @site{/Activiteiten/ * / * / * /WijzigPoster}
	 * @return PageResponse
	 * @throws HTTPStatusException
	 * @throws \League\Flysystem\FileExistsException
	 * @throws \League\Flysystem\FileNotFoundException
	 */
	public function wijzigPoster()
	{
		global $request;

		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);

		if (!$act || !$act->magWijzigen())
			$this->geenToegang();

		if ($request->request->get("pasPosterAan")) {
			$result = $this->processPoster($act);
			Page::addMelding($result["message"], $result["result"]);
		}
		return $this->pageResponse(ActiviteitView::wijzigPoster($act));
	}

	/**
	 *  Geeft een overzicht in een tabel van alle computerreserveringen van komend jaar
	 *
	 * @site{/Activiteiten/Computerreserveringen}
	 * @return PageResponse
	 * @throws Exception
	 */
	public function computerReserveringen ()
	{
		/* Toon vanaf vandaag tot over een jaar */
		$beginDatum = new DateTimeLocale('today');
		$eindDatum = clone $beginDatum;
		$eindDatum->add(new DateInterval('P1Y'));

		/* Activiteiten ophalen met minstens 1 computerreservering */
		$activiteiten = ActiviteitVerzameling::getActiviteiten($beginDatum, $eindDatum, 'ALLE', '', '', 1);

		/* Ten slotte header en tabel op de pagina weergeven */
		return $this->pageResponse(ActiviteitVerzamelingView::computerReserveringen($activiteiten, $beginDatum, $eindDatum));
	}

	/**
	 * Dit geeft het plaatje van de poster wat bij deze activiteit hoort.
	 *
	 * @site{/Activiteiten/ * / * / * /ActPoster}
	 */
	public function poster()
	{
		$ids = $this->getEntryNames();
		$act = Activiteit::geef($ids[1]);
		$file = $act->posterPath();
		if ($file === NULL) {
			// Geen bestand gevonden
			$page = (new HTMLPage())->start(_("Fout!"));
			$page->add(new HtmlParagraph(_('Die poster bestaat niet.'), "waarschuwing"));
			return $this->pageResponse($page);
		} else {
			// Stuur het mooie activiteitenposterplaatje
			return sendfilefromfs($file);
		}
	}
	
	/**
	 * Dit geeft een resized thumbnail van de poster wat bij deze activiteit hoort.
	 *
	 * @site{/Activiteiten/ * / * / * /ActPosterGroot}
	 */
	 public function posterGroot()
	{
		$ids = $this->getEntryNames();
		$act = Activiteit::geef($ids[1]);
		$file = $act->posterPath();
		
		if ($file === NULL) {
			// Geen bestand gevonden
			// TODO: waarom hier geen error 404?
			// TODO SOEPMES: waarom deze codeduplicatie?
			$page = (new HTMLPage())->start(_("Fout!"));
			$page->add(new HtmlParagraph(_('Die poster bestaat niet.'), "waarschuwing"));
			return $this->pageResponse($page);
		} else {
			//probeer de grote poster te krijgen:
			try {
				$groot = explode(".", $file)[0] . "groot.jpg";
				// Stuur de grote poster:
				return sendfilefromfs($groot);
			}
			//Als het niet lukt, stuur dan de normale poster:
			catch(Exception $e) {
				return sendfilefromfs($file);
			}
		}
	}
	
	/**
	 * Dit geeft een resized thumbnail van de poster wat bij deze activiteit hoort.
	 *
	 * @site{/Activiteiten/ * / * / * /ActPosterMedium}
	 */
	 public function posterMedium()
	{
		$ids = $this->getEntryNames();
		$act = Activiteit::geef($ids[1]);
		$file = $act->posterPath();
		
		if ($file === NULL) {
			// Geen bestand gevonden
			$page = (new HTMLPage())->start(_("Fout!"));
			$page->add(new HtmlParagraph(_('Die poster bestaat niet.'), "waarschuwing"));
			return $this->pageResponse($page);
		} else {
			try {
				$med = explode(".", $file)[0] . "medium.jpg";
				// Stuur de medium poster:
				return sendfilefromfs($med);
			}
			//Als het niet lukt, stuur dan de normale poster:
			catch(Exception $e) {
				return sendfilefromfs($file);
			}
		}
	}
	
	/**
	 * Dit geeft een resized thumbnail van de poster wat bij deze activiteit hoort.
	 *
	 * @site{/Activiteiten/ * / * / * /ActPosterThumbnail}
	 */
	 public function posterThumbnail()
	{
		$ids = $this->getEntryNames();
		$act = Activiteit::geef($ids[1]);
		$file = $act->posterPath();
		
		if ($file === NULL) {
			// Geen bestand gevonden
			$page = (new HTMLPage())->start(_("Fout!"));
			$page->add(new HtmlParagraph(_('Die poster bestaat niet.'), "waarschuwing"));
			return $this->pageResponse($page);
		} else {
			try {
				$thumb = explode(".", $file)[0] . "thumbnail.jpg";
				// Stuur de thumbnail:
				return sendfilefromfs($thumb);
			}
			//Als het niet lukt, stuur dan de normale poster:
			catch(Exception $e) {
				return sendfilefromfs($file);
			}
		}
	}

	/**
	 * Schrijf jezelf in
	 *
	 * @site{/Activiteiten/ * / * / * /Inschrijven}
	 */
	public function inschrijven ()
	{
		/** Stel vast of er ingelogd is **/
		$pers = $this->getIngelogd();

		/** Laad de gekozen activiteit **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);
		$deelnemer = new Deelnemer($pers, $act);

		if (Token::processNamedForm() == 'vragenLijst')
		{
			DeelnemerView::processAntwoordForm($deelnemer);
			$vragenBeantwoord = true;
		} else {
			// als geen antwoorden nodig zijn kun je mensen meteen inschrijven
			$vragenBeantwoord = $deelnemer->getAntwoorden()->aantal() == 0;
		}

		if ($vragenBeantwoord) {
			/** Controle inschrijven **/
			if ($act->isDeelnemer($pers))
				Page::addMelding(_('Je bent al ingeschreven.'), 'fout');
			elseif (!$act->inschrijfbaar() && !$act->magAltijdInschrijven())
				Page::addMelding(_('Deze activiteit is nu niet inschrijfbaar.'), 'fout');
			else
			{
				if($deelnemer->getAntwoorden()->allValid())
				{
					/** Begin met inschrijven **/
					$deelnemer->setMomentInschrijven(date("Y-m-d H:i:s"));
					$deelnemer->opslaan();
					if($act->getInformatie()->getStuurMail())
						$this->mailCie($act, "in", $deelnemer);
					return $this->redirectMelding($act->url(), _("Je bent nu ingeschreven!"));
				}
				else
				{
					return $this->pageResponse(ActiviteitView::vragen($act, $deelnemer, true, true));
				}
			}
		}
		else
		{
			/** Laat vragenformulier zien **/
			return $this->pageResponse(ActiviteitView::vragen($act, $deelnemer));
		}

		/** Stuur naar de view **/
		return $this->pageResponse(ActiviteitView::inschrijven($act));
	}

	/**
	 * Schrijf jezelf uit
	 *
	 * @site{/Activiteiten/ * / * / * /Uitschrijven}
	 */
	public function uitschrijven ()
	{
		/** Stel vast of er ingelogd is **/
		$pers = $this->getIngelogd();

		/** Laad de gekozen activiteit **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);

		/** Begin met uitschrijven **/
		if (!$act->isDeelnemer($pers))
			Page::addMelding(_('Je bent niet ingeschreven.'), 'fout');
		elseif (!$act->uitschrijfbaar() && !$act->magWijzigen())
			Page::addMelding(_('De uiterlijke uitschrijfdatum is verstreken.'), 'fout');
		else
		{
			$deelnemer = $act->deelnemer($pers);
			$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);
			foreach($antwoorden as $antwoord){
				$antwoord->verwijderen();
			}
			if($act->getInformatie()->getStuurMail())
				$this->mailCie($act, "uit", $deelnemer);
			$act->deelnemer($pers)->verwijderen();
			return $this->redirectMelding($act->url(), _("Je bent nu uitgeschreven!"));
		}

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitView::uitschrijven($act));
	}

	/**
	 * Print alle deelnemers voor een geselecteerde activiteit op het scherm
	 * todo voeg een csv stijl toe + koppenblad + voeg zaken zoals uitschrijven toe
	 *
	 * @site{/Activiteiten/ * / * / * /Deelnemers/index.html}
	 * @throws HTTPStatusException
	 */
	public function deelnemers ()
	{
		/** Laad de gekozen activiteit **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);
		if (!$act->magWijzigen())
			$this->geenToegang();

		/** Verwerk mogelijke formulier-data **/
		if (Token::processNamedForm() == 'ActDeelnemers')
		{
			$data = $this->getRequestParam('Deelnemers');
			switch ($data['Actie'])
			{
				case 'uitschrijven':
					return $this->deelnemersUitschrijven($data, $act);
				case 'kart':
					return $this->deelnemersKart($data);
				case 'koppenblad':
					return $this->deelnemersKoppenblad($data, $act);
				default:
					Page::addMelding(_('Er waren fouten.'), 'fout');
					break;
			}
		}
		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitView::deelnemers($act));
	}

	/**
	 * Schrijf deelnemers uit
	 * Submethode van deelnemers()
	 * @param $data
	 * @param Activiteit $act
	 * @return PageResponse|Response
	 */
	public function deelnemersUitschrijven ($data, Activiteit $act)
	{
		/** Verwerk formulier-data **/
		$ids = array();
		foreach ($data['Geselecteerde'] as $id => $derp)
			if ($derp)
				$ids[] = array($id, $act->geefID());
		$deelnemers = DeelnemerVerzameling::verzamel($ids);
		if (isset($data['check']))
		{
			if ((bool) $data['check'])
			{
				if($act->getInformatie()->getStuurMail())
					$this->mailCie($act, "uit", $deelnemers);
				foreach($deelnemers as $deelnemer){
					$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);
					foreach($antwoorden as $antwoord){
						$antwoord->verwijderen();
					}
					$deelnemer->verwijderen();
				}
				return $this->redirectMelding($act->deelnemersURL(), _("Je hebt nu de deelnemers uitgeschreven!"));
			}
			else
			{
				return $this->redirectMelding($act->deelnemersURL(), _("Er zijn geen deelnemers uitgeschreven!"));
			}
		}

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitView::deelnemersUitschrijven($act, $deelnemers));
	}

	/**
	 * Shop deelnemers in je kart
	 * Submethode van deelnemers()
	 * @param $data
	 * @return Response
	 */
	public function deelnemersKart ($data)
	{
		/** Verwerk formulier-data **/
		// todo dit moet gegeneraliseerd worden
		$ids = array();
		foreach ($data['Geselecteerde'] as $id => $derp)
			if ($derp)
				$ids[] = $id;
		Kart::geef()->voegPersonenToe($ids);

		/** Stuur door **/
		return $this->redirectMelding('/Service/Kart', _("De geselecteerden mensen zitten nu in je kart."));
	}

	/**
	 * Laat koppenblad zien van geselecteerde deelnemers
	 * @param $data
	 * @param $act
	 * @return PageResponse
	 */
	public function deelnemersKoppenblad($data, $act)
	{
		if(!$data instanceof DeelnemerVerzameling){
			$ids = array();
			foreach ($data['Geselecteerde'] as $id => $derp)
				if ($derp)
					$ids[] = $id;
			$personen = PersoonVerzameling::verzamel($ids);
		} else {
			$personen = $data->toPersoonVerzameling();
		}

		return $this->pageResponse(ActiviteitView::koppenBlad($personen, $act));
	}

	/**
	 *    Een Javascriptloze versie van het koppenblad.
	 *
	 * @site{/Activiteiten/ * / * / * /Deelnemers/Koppenblad}
	 *
	 * @throws HTTPStatusException
	 */
	public function deelnemersAlleKoppenblad()
	{
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);
		if(!$act)
			$this->geenToegang();
		return $this->deelnemersKoppenblad($deelnemers = $act->deelnemers(), $act);
	}

	/**
	 * Schrijf nieuwe deelnemers in
	 *
	 * @site{/Activiteiten/ * / * / * /Deelnemers/Inschrijven}
	 * @throws HTTPStatusException
	 */
	public function deelnemersInschrijven ()
	{
		/** Laad de gekozen activiteit **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);
		if (!$act->magWijzigen())
			$this->geenToegang();
		$hact = $act;

		/** Verwerk formulier-data **/
		DeelnemerVerzamelingView::processNieuwForm($verzameling = new DeelnemerVerzameling(), $hact);
		if (Token::processNamedForm() == 'ActDeelnemersInschrijven')
		{
			if($verzameling->aantal() > 0)
			{
				if($verzameling->aantal() + $act->deelnemers()->aantal() > $act->getMaxDeelnemers() && $act->getMaxDeelnemers() > 0)
				{
					return $this->redirectMelding($act->deelnemersURL(), _("De hoeveelheid deelnemers overschrijdt het maximum aantal deelnemers!"));
				}
				$verzameling->opslaan();
				if($act->getInformatie()->getStuurMail())
					$this->mailCie($act, "in", $verzameling);

				return $this->redirectMelding($act->deelnemersURL(), _("Je hebt nu de deelnemers ingeschreven!"));
			} else {
				return $this->redirectMelding($act->deelnemersURL(), _("Je moet wel iemand selecteren Willem!"));
			}
		}

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitView::deelnemersInschrijven($act));
	}

	/**
	 *	/brief Pas een deelnemer aan. Op dit moment is dit beperkt tot het
	 *	 aanpassen van de vragen/opmerkingen van een deelenemer.
	 *
	 * @site{/Activiteiten/ * / * / * /Deelnemers/ * /Wijzig}
	 */
	public function deelnemerWijzigen()
	{
		$entryData = $this->getEntryNames();

		$deelnemer = Deelnemer::geef($entryData[0], $entryData[2]);

		if(Token::processNamedForm() == 'deelnemerWijzig')
		{
			DeelnemerView::ProcessAntwoordForm($deelnemer);
			if($deelnemer->valid())
			{
				$deelnemer->opslaan();

				// Alleen mailen als er vragen zijn
				if ($deelnemer->getAntwoorden()->aantal() > 0 && $deelnemer->getActiviteit()->getInformatie()->getStuurMail())
				{
					$this->mailCie($deelnemer->getActiviteit(), "wijzig", $deelnemer);
				}
				return $this->redirectMelding($deelnemer->getActiviteit()->url(), _("De gegevens zijn aangepast!"));
			}
		}
		//Geef false mee zodat duidelijk is dat het hier om een wijziging gaat.
		return $this->pageResponse(ActiviteitView::vragen($deelnemer->getActiviteit(), $deelnemer, false));
	}

	/**
	 * @site{/Activiteiten/ * / * / * /Deelnemers/ * /Uitschrijven}
	 */
	public function deelnemerUitschrijven()
	{
		$entryData = $this->getEntryNames();

		$act = Activiteit::geef($entryData[2]);

		$deelnemer = Deelnemer::geef($entryData[0], $entryData[2]);

		$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);
		foreach($antwoorden as $antwoord)
		{
			$antwoord->verwijderen();
		}
		$deelnemer->verwijderen();
		return $this->redirectMelding($act->deelnemersURL(), _("De deelnemer is uitgeschreven"));
	}

	/**
	 * Slaat een poster die geupload is en in $_FILES['poster'] zit op, op de goede plek.
	 * @param Activiteit $act
	 * @return array
	 * @throws \League\Flysystem\FileExistsException
	 * @throws \League\Flysystem\FileNotFoundException
	 */
	private function processPoster(Activiteit $act)
	{
		global $FW_EXTENSIONS_FOTO, $filesystem;

		$actPoster = $_FILES['poster'];
		if (!isset($actPoster)) {
			return array("result" => "fout", "message" => _("Er was geen file-upload formulier"));
		}
		if ($actPoster['error'] == 2 || ($actPoster['size'] && $actPoster['size'] > ACTPOSTERS_MAX_FILE_SIZE)) {
			return array("result" => "fout", "message" => _("Je activiteitenposter is te groot. Het maximum is 10 MB"));
		}
		if($actPoster['error'] && $actPoster['error'] != 4) {
			return array(
				"result" => "fout",
				"message" => _("Er waren fouten met het uploaden.\nZie http://php.net/manual/en/features.file-upload.errors.php voor info van error code") . $actPoster['error']
			);
		}

		if ($actPoster['error'] != 4) {
			$dotIndex = strrpos($actPoster['name'], '.');
			if ($dotIndex === false) {
				return array(
					"result" => "fout",
					"message" => _("Dit bestandstype is niet toegestaan.")
				);
			}
			$usedExt = substr($actPoster['name'], $dotIndex + 1);
			if (!in_array($usedExt, $FW_EXTENSIONS_FOTO)) {
				return array(
					"result" => "fout",
					"message" => _("Dit bestandstype is niet toegestaan.")
				);
			}
		}

		// We gooien de oude poster eerst weg
		$path = $act->posterPath(null, true, false);
		if ($path !== NULL && !$filesystem->delete($path)) {
			return array("result" => "fout", "message" => _("De oude poster kon niet weggemieterd worden."));
		}

		if ($actPoster['error'] == 4) {
			// Er is geen bestand gestuurd, dus we nemen aan dat de huidige poster weggegooid moet worden.
			return array("result" => "succes", "message" => _("De poster is weggemieterd!"));
		}

		$path = $act->posterPath(array($usedExt), FALSE);
		$stream = fopen($actPoster['tmp_name'], 'r+');
		$res = $filesystem->writeStream($path, $stream);
		fclose($stream);
		// Page::addMelding("We voegen een poster toe bij " . $usedExt . "  op " . $path);
		if (!$res) {
			return array("result" => "fout", "message" => _("Het bestand kon niet opgeslagen worden."));
		}
		
		//maak thumbnails:
		Activiteiten_Controller::createThumbnail("groot", $act->posterPath());
		Activiteiten_Controller::createThumbnail("medium", $act->posterPath());
		Activiteiten_Controller::createThumbnail("thumbnail", $act->posterPath());

		return array("result" => "succes", "message" => _("De poster is aangepast!"));
	}


	/**
	 * Creert een cache-versie van het origineel van de poster
	 * het gegeven formaat ("groot", "medium", "thumbnail")
	 * @param $formaat
	 * @param $originalFile
	 */
	public function createThumbnail($formaat, $originalFile)
	{
		global $filesystem;

		// Hardcoded waardes groottes van bestanden
		if ($formaat == "thumbnail") {
			$maxwidth = 134;
			$maxheight = 100;
		} elseif ($formaat == "medium") {
			$maxwidth = 450;
			$maxheight = 338;
		} elseif ($formaat == "groot") {
			$maxwidth = 900;
			$maxheight = 676;
		} else {
			user_error('Ongeldig formaat opgegeven voor Foto->createCacheFile(...)', E_USER_ERROR);
		}
		
		$exten = explode(".", $originalFile);

		$cacheFile = $exten[0] . $formaat . ".jpg";

		if (!$filesystem->has($originalFile)) {
			user_error("Kan geen cache ($formaat) maken van poster, de originele poster bestaat niet", E_USER_ERROR);
		}

		$originalFile = FILESYSTEM_PREFIX . $originalFile;

		$image = ImageManagerStatic::make($originalFile);

		if ($image->height() > $image->width()) {
			$tmp = $maxwidth;
			$maxwidth = $maxheight;
			$maxheight = $tmp;
			$maxwidth = $image->width() * ($maxheight / $image->height());
		}
		else
		{
			$maxheight = $image->height() * ($maxwidth / $image->width());
		}

		$image->resize($maxwidth, $maxheight)->save(FILESYSTEM_PREFIX . $cacheFile);
	}

	/**
	 * @site{/Activiteiten/ * / * / * /Wijzig/Promoveer}
	 * @throws HTTPStatusException
	 */
	public function promoveer ()
	{
		/** Verwerk get-data **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);
		if (!$act || !$act->magWijzigen() || !($act instanceof ActiviteitHerhaling))
			$this->geenToegang();

		/** Verwerk formulier-data **/
		$inp = (bool) $this->getRequestParam('actid', false);
		if ($inp)
		{
			$vragen = new ActiviteitVraagVerzameling();
			$cieActs = new CommissieActiviteitVerzameling();
			$newAct = new ActiviteitInformatie();
			$act->promoveer($newAct, $cieActs, $vragen);
			if($newAct->valid())
			{
				$newAct->opslaan();
				$cieActs->opslaan();
				$vragen->opslaan();
				$act->verwijderen();
				return $this->redirectMelding($newAct->url(), _("De activiteit is nu gepromoveerd!"));
			} else {
				// Geef een lijstje met alle fouten die zijn opgetreden.
				// TODO SOEPMES: hier zou een mooie functie voor kunnen bestaan
				Page::addMelding(_("Er waren fouten bij het promoveren:"), 'fout');
				foreach ($newAct->getErrors() as $veld => $err) {
					if (!$err) {
						continue;
					}
					Page::addMelding($err, 'fout');
				}
			}
		}
		elseif (!is_null($this->getRequestParam('check')))
		{
			return $this->redirectMelding($act->url(), _("De activiteit is niet gepromoveerd!"));
		}

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitHerhalingView::promoveer($act));
	}

	/**
	 * @site{/Activiteiten/ * / * / * /Verwijder}
	 */
	public function verwijder ()
	{
		/** Verwerk get-data **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);

		switch(Token::processNamedForm()) 
		{
			case 'ActiviteitInformatieVerwijderen':
				$returnValue = $act->verwijderen();

				if(!is_null($returnValue)) {
					Page::addMeldingArray($returnValue, "fout");
				} else {
					return $this->redirectMelding("/Activiteiten/", _('De activiteit is verwijderd uit de Databaas.'));
				}
				break;
			case 'ActiviteitHerhalingVerwijderen':
				$parent = $act->getInformatie();
				$returnValue = $act->verwijderen();
				if(!is_null($returnValue))
					Page::addMeldingArray($returnValue, 'fout');
				else
					return $this->redirectMelding($parent->url(), _('De herhalende activiteit is verwijderd.'));
				break;
			default:
				break;
		}

		$classView = get_class($act) . 'View';

		/** Ga naar de view **/
		$page = (new HTMLPage());
		$page->start();
		$page->add($classView::verwijderForm($act));
		return $this->pageResponse($page);
	}

	/**
	 * @site{/Activiteiten/ * / * / * /Herhalingen}
	 * @throws HTTPStatusException
	 */
	public function herhalingen ()
	{
		/** Verwerk get-data **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);

		$children = $act->getHerhalingsChildren();
		if (!$children->aantal())
			$this->geenToegang();

		$body = new HtmlDiv();
		$body->setId('activiteiten');
		$body->add(new HtmlParagraph(sprintf(_('Alle herhalingen van %s[VOC: titel van activiteit]'), new HtmlAnchor($act->url(), ActiviteitView::titel($act)))))
			 ->add(ActiviteitVerzamelingView::htmlLijst($children));

		return $this->pageResponse((new HTMLPage())
			->start(_('Herhalingen'))
			->add($body));
	}

	/**
	 * Returneert een array met geselecteerde activiteit
	 * $args = array(0 => 'act_id')
	 * @site{/Activiteiten/ * / * /}
	 * @param $args
	 * @return array|bool|Response
	 */
	public function commissieActiviteitEntry ($args)
	{
		$act = Activiteit::geef($args[0]);
		if (!$act) return false;

		$cies = explode('_',$args[1]);

		$bevat = true;
		foreach($cies as $c) {
			if (!$act->getCommissies()->bevat(Commissie::cieByLogin($c))) {
				$bevat = false;
				break;
			}
		}

		// Redirect naar de juiste URL op basis van het activiteit-ID.
		if (!$bevat)
		{
			return [
				'access' => $act->magBekijken(),
				'name' => $act->geefID(),
				'rewrite' => $act->url(),
			];
		}

		return array('name' => $act->geefID(),
					'displayName' => "null",
					'access' => $act->magBekijken());
	}

	/**
	 * Returneert een array met geselecteerde commissie-activiteit
	 * $args = array(0 => 'cie_input')
	 * @site{/Activiteiten/ * / * / * /}
	 * @param $args
	 * @return array|bool
	 */
	public function commissieActiviteitNaamEntrySuper($args)
	{
		return $this->commissieActiviteitNaamEntry($args);
	}
	public function commissieActiviteitNaamEntry ($args)
	{
		$act = Activiteit::geef($args[1]);
 		if (!$act) return false;

		return array('name' => $act->getActURLActnaam(),
					'displayName' => "null",
					'access' => true);
	}

	/**
	 *	Retourneer een array met de geselecteerde deelnemer als dat mag.
	 *
	 *	@site{/Activiteiten/ * / * / * /Deelnemers/ * /}
	 **/
	public function deelnemerActEntry($args)
	{
		$dlnr = Deelnemer::geef($args[0],$args[3]);
		if(!$dlnr)
			return false;

		return array('name' => $dlnr->getPersoonContactID(),
				'displayname' => PersoonView::naam($dlnr->getPersoon()),
				'access' => $dlnr->magWijzigen());
	}

	/**
	 * Deze hook zorgt ervoor dat je publisherpagina's onder een activiteit kan maken.
	 *
	 * Wat er namelijk gebeurt is dat we de hook instantiëren voor elke activiteit
	 * die een publisherpagina wilt.
	 * Dus heb je in /Activiteiten de variabele entry /*
	 * en een entry /$id voor elke activiteit met publisherpagina's.
	 * Indien je dan de standaardpagina's wilt, die normaal onder de variabele entry staan,
	 * dan gaat deze hook opnieuw alles door benamite halen vanaf de variabele entry.
	 * (Dit moet beter kunnen!)
	 * @param $rest
	 * @return Response
	 */
	public function commissieActiviteitNaamEntry_content($rest)
	{
		global $space;
		global $uriRef, $INDEX;

		$requestURI = $uriRef['request'];
		$entry = $uriRef['entry'];

		if(!$rest)
		{
			//Zoek de index.html en gebruik die
			if($child = $entry->getChild($INDEX))
			{
				$child->display();
			}
			//Redirect anders naar de /Info pagina
			$uriRef['remaining'] = 'Info';
		}
		// we komen van /Activiteiten/*/ACTID/*
		// en willen naar /Activiteiten/*/*/*/
		$down = $entry->getParent()->getParent();
		$up = $down->getChild('*');
		assert($up->checkUrl($entry->getParent()->getName()),
			"de variabele activiteitenentry accepteert de waarden van de php-haak");
		$up = $up->getChild('*');
		assert($up->checkUrl($entry->getName()),
			"de variabele activiteitenentry accepteert de waarden van de php-haak");

		// <Dubbel met commissies.php::commissiewsw4_content>
		$target = hrefToReference($uriRef['remaining'], $uriRef['params'], $up, null, $uriRef['rewrite']);
		return $space->getResponse($requestURI, $target);
	}

	/**
	 * @site{/Activiteiten/ * / CommissieActiviteiten}
	 * @throws HTTPStatusException
	 */
	public function commissieInfo()
	{
		$cies = explode('+', trim($this->getEntryName(), '+'));
		$cieVerzameling = new CommissieVerzameling();
		foreach($cies as $cie)
		{
			if($cie = Commissie::inputToCie($cie))
				$cieVerzameling->voegtoe($cie);
		}

		//Laat de activiteiten van deze commissie zien (Activiteiten/bestuur)
		if($cieVerzameling->aantal() == 0)
			$this->geenToegang();

		return $this->pageResponse(ActiviteitVerzamelingView::vanCommissiesLijst($cieVerzameling, ActiviteitVerzameling::vanCommissies($cieVerzameling)));
	}

	/**
	 * @brief Stuur de gebruiker door naar een url waar de slug in voorkomt.
	 *
	 * Bijvoorbeeld `/Activiteiten/bestuur/1234` redirect naar `/Activiteiten/bestuur/1234/Titel`.
	 * (Een slug is een (verkorte) menselijk begrijpbare naam voor iets uit de databaas,
	 * die je kan gebruiken in de plaats van een ID.)
	 *
	 * @site{/Activiteiten/ * / * /index.html}
	 * @throws HTTPStatusException
	 */
	public function activiteitNaamForward()
	{
		global $uriRef;

		// Lees de entrynaam uit van de huidige entry,
		// want ivm publisher komen we niet altijd via een variabele entry.
		// TODO SOEPMES: dit moet beter kunnen...
		$actidEntry = $uriRef['entry']->getParent();
		$actid = (int) $actidEntry->getName();
		$act = Activiteit::geef($actid);
		if (!$act)
		{
			$this->geenToegang();
		}

		// Redirect naar de officiële url.
		return $this->redirect($act->url());
	}

	/**
	 * Print een geselecteerde activiteit op het scherm
	 *
	 * @site{/Activiteiten/ * / * / * /Info}
	 */
	public function details()
	{
		/** Laad de gekozen activiteit **/
		$entryData = $this->getEntryNames();
		$actid = (int) $entryData[1];
		$act = Activiteit::geef($actid);

		/** Ga naar de view **/
		return $this->pageResponse(ActiviteitView::details($act));
	}

	/**
	 *    Mail de cie relevante wijzigingen die 3e doen.
	 *    //TODO: meer taalafhankelijk maken.
	 * @param Activiteit $act
	 * @param $wat
	 * @param $wie
	 * @return bool
	 */
	public function mailCie(Activiteit $act, $wat, $wie)
	{
		//Zorg dat we met een deelnemerverzameling werken.
		if($wie instanceof Deelnemer)
		{
			$dlnr = $wie;
			$wie = new DeelnemerVerzameling();
			$wie->voegToe($dlnr);
		} elseif(!($wie instanceof DeelnemerVerzameling)) {		
			return false;
		} 

		$cies = $act->getCommissies();
		$ciesString = CommissieVerzamelingView::kommaLijst($cies, false);
		$ciesMailStringArray = array();
		foreach($cies as $cie)
		{
			$ciesMailStringArray[] .= $cie->getNaam().' <'.$cie->getEmail().'>';
		}
		$ciesMailString = implode($ciesMailStringArray,',');

		//Als er meer dan 1 cie is dan krijgt de www de bounces en "acts accordingly".
		if($cies->aantal() > 1)
		{
			$ciesBounceString = "www";
		} else {
			$cies->rewind();
			$ciesBounceString = $cies->current()->getNaam().' <'.$cies->current()->getEmail().'>';
		}

		switch($wat) {
			default:
			case 'in':
				$intro = "Via de site zijn de volgende mensen ingeschreven voor";
				$subj = "Inschrijvingen voor ";
				break;
			case 'uit':
				$intro = "Via de site zijn de volgende mensen UITgeschreven voor";
				$subj = "Uitschrijvingen voor ";
				break;
			case 'wijzig';
				$intro = "Via de site zijn de volgende inschrijvingen GEWIJZIGD voor";
				$subj = "Inschrijvingen gewijzigd voor ";
		}

		$titel = ActiviteitView::titel($act);
		if($act instanceof ActiviteitHerhaling)
		{
			$titel .= " (op " . ActiviteitView::waardeMomentBegin($act) . ")";
		}

		$tekst = "Beste ".$ciesString.
			",\n\n$intro\n".$titel.":\n\n";

		foreach($wie as $deelnemer)
		{
			$tekst .= " * ". PersoonView::naam($deelnemer->getPersoon())
				. " (". $deelnemer->getPersoonContactID() .")\n";
		}

		$antwoorden = DeelnemerAntwoordVerzameling::vanDeelnemer($deelnemer);
		if($antwoorden->aantal() > 0 && $wat != 'uit')
		{
			$tekst .= "\n" . _("De volgende opmerkingen en/of antwoorden zijn ingevuld:")."\n";
			foreach($antwoorden as $antwoord)
			{
				$tekst .= " * " . $antwoord->getVraag()->getVraag() . "\n";
				$tekst .= "\t" . $antwoord->getAntwoord() . "\n";
			}
		}
		$tekst .= "\nZie: http://www.A-Eskwadraat.nl" .
			$act->url() . "/Info\n\n";
		//Voeg het aantal deelnemers in de mail toe als we met in- of uitschrijvingen te maken hebben.
		$maxaantal = $act->getMaxDeelnemers();
		$aantal = $act->deelnemers()->aantal();
		if($wat==="uit")
		{
			$aantal --;//Want we gaan iemand uischrijven.
		}

		if($maxaantal == 0)
		{
			$maxtekst = ".";
		}
		else
		{
			$maxtekst = sprintf(_(' [VOC: deze string komt aan het einde van een '
				. 'mailtje als een deelnemer zich ingeschreven heeft en er een '
				. 'maximum aantal deelmeners is]van de maximaal %s[VOC: aantal '
				. 'deelnemers] deelnemers'), $maxaantal);
		}
		if($wat==="in"||$wat==="uit")
			$tekst .= sprintf(_("Het aantal deelnemers is nu %s[VOC: aantal deelnemers]"), $aantal) . $maxtekst."\n";


		$tekst .= "\nGroeten,\nA–Eskwadraat Activiteitenpagina's  i.s.m. " . PersoonView::naam(Persoon::getIngelogd());

		sendmail('www.A-Eskwadraat.nl <www-data@a-eskwadraat.nl>',
			$ciesMailString,
			$subj.$act->getTitel(), $tekst, $ciesMailString, null, 
			$ciesBounceString);
	}

	/**
	 * @site{/Activiteiten/ * / * / * /ScanActiveren}
	 * @throws HTTPStatusException
	 */
	public function activeerVoorScannen()
	{
		$this->requireAuth('bestuur');

		$vfs = $this->getEntryNames();

		if (!($activiteit = Activiteit::geef($vfs[1])))
			$this->geenToegang();

		$kaartjes = iDealKaartjeVerzameling::geefPotentieelScanbareKaartjes($activiteit);

		$name = Token::processNamedForm();
		switch($name)
		{
		case 'activeer':
			foreach($kaartjes as $kaartje)
			{
				$kaartje->setMagScannen(true);
			}
			$kaartjes->opslaan();

			return $this->redirectMelding($activiteit->url(), _("De kaartjes kunnen nu gescand worden!"));
			break;
		case 'deactiveer':
			foreach($kaartjes as $kaartje)
			{
				$kaartje->setMagScannen(false);
			}
			$kaartjes->opslaan();

			return $this->redirectMelding($activiteit->url(), _("De kaartjes kunnen nu niet meer gescand worden!"));
			break;
		}

		$totalMagScannen = true;
		$existScanbare = false;
		foreach($kaartjes as $kaartje)
		{
			if($kaartje->getMagScannen())
			{
				$existScanbare = true;
			}
			else
			{
				$totalMagScannen = false;
			}
		}

		return $this->pageResponse(ActiviteitView::activeerKaartjes($activiteit, $existScanbare, $totalMagScannen));
	}

	/**
	 * Endpoint voor de app om iDealkaartjes te kunnen scannen.
	 *
	 * @site /Activiteiten/ * / * / * /ScanControle
	 * @throws HTTPStatusException
	 */
	public function scanControle()
	{
		$vfs = $this->getEntryNames();

		if (!($activiteit = Activiteit::geef($vfs[1])))
			$this->geenToegang();

		$kaartjes = iDealKaartjeVerzameling::geefScanbareKaartjes($activiteit);

		if($kaartjes->aantal() != 0)
		{
			$code = $this->getRequestParam('code', NULL);
			echo "{";
			if(is_null($code) || empty($code))
			{
				echo "\"valid\": false, \"error\": \"Geen code meegestuurd!\", \"color\": \"#FF0000\"";
			}
			else if(!isset($_SESSION['magScannen'.$activiteit->geefID()])
				|| !$_SESSION['magScannen'.$activiteit->geefID()])
			{
				if(hash("sha512", QRCODE_SALT . $code) ==
					hash("sha512", QRCODE_SALT .
						hash("sha512", QRCODE_SALT . $activiteit->geefID())))
				{
					$_SESSION['magScannen'.$activiteit->geefID()] = true;
					echo "\"valid\": true, \"error\": \"OK je mag nu scannen\", \"color\": \"#00FF00\", \"magScannen\": true";
				}
				else
				{
					echo "\"valid\": false, \"error\": \"Verkeerde code meegestuurd!\", \"color\": \"#FF0000\"";
				}
			}
			else
			{
				$kcodes = $kaartjes->geefKaartjesCodes();

				$gevonden = false;
				foreach($kcodes as $kcode)
				{
					if($kcode->getCode() === $code && !($kcode->getGescand()))
					{
						echo "\"valid\": true, \"error\": \"Succes!\", \"color\":\"#00FF00\"";
						$kcode->setGescand(true);
						$kcode->opslaan();
						$gevonden = true;
						break;
					}
					else if($kcode->getCode() === $code)
					{
						echo "\"valid\": false, \"error\": \"Is al gescand!\", \"color\": \"#FF0000\"";
						$gevonden = true;
						break;
					}
				}

				if(!$gevonden)
				{
					echo "\"valid\": false, \"error\": \"Code bestaat niet!\", \"color\": \"#FF0000\"";
				}
			}

			echo "}";

			die();
		}
	}

	/**
	 * Heeft deze activiteit een publisher-entry?
	 *
	 * Dit wil je vooral weten om te bepalen wat er in geef_ajax getoond wordt.
	 *
	 * @param Activiteit $act De activiteit waarvan we de publisherentry willen testen.
	 *
	 * @return bool
	 */
	private function heeftPublisherContent(Activiteit $act)
	{
		// TODO: dit is een (lelijke) hack, dit moet mooier kunnen
		$entry = hrefToEntry($act->url() . '/index.html');
		if (!$entry)
		{
			return false;
		}

		try
		{
			$entry->getSpecialFunc();
			return false;
		}
		catch (\Space\Benamite\NoSpecialFunctionException $e)
		{
			// Dit is blijkbaar publisher-content,
			// omdat het een vfsEntryFile is.
			return true;
		}
	}

	/**
	 *  Geeft een act-weergave terug voor gebruik in ajax
	 *
	 * @site{/Ajax/Activiteiten/geef_ajax}
	 * @throws HTTPStatusException
	 */
	public function geef_ajax()
	{
		$id = $this->getRequestParam('activiteitID');

		$act = Activiteit::geef($id);
		if(!$act)
		{
			$this->geenToegang();
		}

		// Check of de activiteit publisher-content heeft:
		// dit vervangt de "default"-weergave van een activiteit.
		if ($this->heeftPublisherContent($act))
		{
			$entry = hrefToEntry($act->url() . '/index.html');
			return new Response($entry->getContent());
		}

		$title = array(ActiviteitView::makeLink($act));
		if ($act->magWijzigen()) {
			$title[] = '&nbsp;';
			$title[] = new HtmlAnchor($act->wijzigURL(), HtmlSpan::fa('pencil', _('Aanpassen')));
		}
		if ($act->magVerwijderen()) {
			$title[] = '&nbsp;';
			$title[] = new HtmlAnchor($act->verwijderURL(), HtmlSpan::fa('times', _('Verwijderen')));
		}
		$page = new CleanHtml_Page();
		$page->start();
		$page->add(new HtmlHeader(2, $title))
			->add(ActiviteitView::getDetails($act, true));
		return $this->pageResponse($page);
	}

	/**
	 *  Geeft de antwoorden die bij idealverkopen horen weer
	 *
	 * @site{/Activiteiten/ * / * / * /iDealAntwoorden}
	 * @throws HTTPStatusException
	 */
	public function iDealAntwoorden()
	{
		$this->requireAuth('actief');

		$vfs = $this->getEntryNames();

		if (!($activiteit = Activiteit::geef($vfs[1])))
			$this->geenToegang();

		if (!$activiteit->magWijzigen()) {
			$this->geenToegang();
		}

		$vragen = $activiteit->getVragen();
		if($vragen->aantal() == 0)
			$this->geenToegang();

		$kaartjes = $activiteit->getIdealKaartjes();
		if($kaartjes->aantal() == 0)
			$this->geenToegang();

		$antwoorden = $activiteit->getIdealAntwoorden();

		if($this->getRequestParam('csv', null) === 'true')
		{
			header('Content-Disposition: attachment; filename=antwoorden.csv');
			header('Content-type: text/csv; encoding=utf-8');
			$file = fopen('php://output', 'r+');
			iDealAntwoordVerzamelingView::toCSV($antwoorden, $file);
			fclose($file);
			exit();
		}
		else
			return $this->pageResponse(iDealAntwoordVerzamelingView::antwoorden($antwoorden));
	}

	/**
	 *  Handelt de variabele entry van een activiteitcategorie af.
	 *
	 * @site{/Activiteiten/Categorie/ * /}
	 */
	public function categorieEntry($args)
	{
		$cat = ActiviteitCategorie::geef($args[0]);
		if (!$cat) return false;

		return array('name' => $args[0],
					'displayName' => ActiviteitCategorieView::waardeTitel($cat),
					'access' => true);
	}

	/**
	 * @brief Toont het aantal unieke deelnemers binnen een bepaalde categorie in een collegejaar. Verder laat het ook alle activiteiten zien in dat jaar.
	 * @site{/Activiteiten/Categorie/ * /index.html}
	 * @throws HTTPStatusException
	 */
	// Overgenomen van de functie actieveEerstejaars()
	public function categorieTelling() {
		$this->requireAuth('bestuur');

		$page = (new HTMLPage())->start();

		// Welk collegejaar gaat het over?
		$jaar = $this->getRequestParam('jaar', colJaar());

		// Formuliertje om het jaar te selecteren
		$page->add($form = new HtmlForm("GET"));
		$form->add(new HtmlParagraph(_("Selecteer van welk collegejaar je de activiteiten in deze categorie wil zien:")));
		$form->add(HtmlInput::makeNumber("jaar", $jaar));
		$form->add(HtmlInput::makeSubmitButton(_("Toon die activiteiten!")));

		$begindatum = new DateTimeLocale($jaar . "-08-01"); // begin iets voor september
		$einddatum = new DateTimeLocale(($jaar + 1) . "-08-01");

		//TODO: Maak functie die collegejaren omzet naar begindata

		$catnummer=ActiviteitCategorie::geef(vfsVarEntryName());


		$activiteiten = ActiviteitVerzameling::getActiviteiten($begindatum, $einddatum, 'PUBLIEK',
				'', '', 0, $catnummer)->filterBekijken();

		$uniek = $activiteiten->uniekeDeelnemerAantal();

		$page->add(new HtmlParagraph(sprintf(
			_("Het aantal unieke leden in deze categorie in dit jaar is: %d"),
			$uniek
		)));

		$page->add(ActiviteitVerzamelingView::htmlLijst($activiteiten));

		return $this->pageResponse($page);
	}


	/**
	 *  Toon alle activiteitcategorieën in een handig overzicht.
	 *
	 * Bevat ook een mooi nieuwform.
	 *
	 * @site{/Activiteiten/Categorie/index.html}
	 * @throws HTTPStatusException
	 */
	public function categorieOverzicht()
	{
		$this->requireAuth('bestuur');

		$page = (new HTMLPage())->start();

		$page->add(ActiviteitCategorieView::nieuwForm());

		$page->add(ActiviteitCategorieVerzamelingView::categorieOverzicht());

		return $this->pageResponse($page);
	}

	/**
	 *  Maak een nieuwe activiteitcategorie.
	 *
	 * @site{/Activiteiten/Categorie/Nieuw}
	 * @throws HTTPStatusException
	 */
	public function categorieNieuw()
	{
		$this->requireAuth('bestuur');

		$cat = new ActiviteitCategorie();
		$token = Token::processNamedForm();
		if ($token == "nieuweCategorie") {
			ActiviteitCategorieView::processForm($cat);
			if ($cat->valid()) {
				$cat->opslaan();
				return $this->redirectMelding("/Activiteiten/Categorie", _("Nieuwe categorie toegevoegd!"));
			}
		}
		$page = (new HTMLPage())->start();
		$page->add(ActiviteitCategorieView::nieuwForm($cat));
		return $this->pageResponse($page);
	}

	/**
	 *  Wijzig de categorie met het variabele stukje ID.
	 *
	 * @site{/Activiteiten/Categorie/ * /Wijzig}
	 * @throws HTTPStatusException
	 */
	public function categorieWijzig()
	{
		$this->requireAuth('bestuur');

		$vfs = $this->getEntryNames();

		if (!($cat = ActiviteitCategorie::geef($vfs[0])))
			$this->geenToegang();

		$token = Token::processNamedForm();
		if ($token == "wijzigCategorie") {
			ActiviteitCategorieView::processForm($cat);
			if ($cat->valid()) {
				$cat->opslaan();
				return $this->redirectMelding("/Activiteiten/Categorie", _("Categorie gewijzigd!"));
			}
		}
		$page = (new HTMLPage())->start();
		$page->add(ActiviteitCategorieView::wijzigForm($cat));
		return $this->pageResponse($page);
	}

	/**
	 *  Verwijder de categorie met het variabele stukje ID.
	 *
	 * @site{/Activiteiten/Categorie/ * /Verwijder}
	 * @throws HTTPStatusException
	 */
	public function categorieVerwijder()
	{
		$this->requireAuth('bestuur');

		$vfs = $this->getEntryNames();

		if (!($cat = ActiviteitCategorie::geef($vfs[0])))
			$this->geenToegang();

		$error = $cat->verwijderen();

		if (!empty($error)) {
			return $this->redirectMelding("/Activiteiten/Categorie", $error);
		} else {
			return $this->redirectMelding("/Activiteiten/Categorie", _("Categorie is verwijderd!"));
		}
	}
}
