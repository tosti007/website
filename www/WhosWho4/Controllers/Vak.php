<?php

use Symfony\Component\HttpFoundation\Response;

abstract class Vak_Controller
{
	/*
	 *  Geeft een overzichtspagina van alle vakken weer
	 *
	 * @site{/Onderwijs/Vak}
	 * @return Response
	 */
	static public function vakOverzicht() {
		requireAuth('bestuur');

		$vakken = VakVerzameling::geefAlleVakken();
		$vakken->sorteer('code');
		$page = VakVerzamelingView::VakOverzicht($vakken);
		return new PageResponse($page);
	}

	static public function vakEntry($args) {
		$vak = Vak::geef($args[0]);
		if(!$vak)
			return array( 'name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array( 'name' => $vak->geefID()
			, 'displayName' => _("Vak")
			, 'access' => true);
	}

	/**
	 * Geeft een overzichtspagina met informatie van een vak
	 *
	 * @site{/Onderwijs/Vak/ * /index.html}
	 *
	 * @return Response
	 * @throws ResponseException
	 */
	public static function vak()
	{
		$vakid = vfsVarEntryName();

		if(!($vak = Vak::geef($vakid)))
		{
			return responseUitStatusCode(403);
		}

		$jaargangen = JaargangVerzameling::geefVanVak($vak);
		$tents = TentamenVerzameling::getAlleVanVak($vak);

		$page = VakView::vakInfo($vak, $jaargangen, $tents);
		return new PageResponse($page);
	}

	/**
	 * Een pagina voor het toevoegen van een vak
	 *
	 * @site{/Onderwijs/Vak/Nieuw}
	 *
	 * @return Response
	 * @throws ResponseException
	 */
	static public function vakToevoegen()
	{
		// Merk op dat hasAuth('bestuur') impliceert hasAuth('tbc').
		requireAuth('tbc');

		return self::pasVakAan(new Vak(), _("Vak toevoegen"), 'vaktoevoegen', _("Succes! Vak toegevoegd"));
	}

	/**
	 * Een pagina voor het wijzigen van een vak
	 *
	 * @site{/Onderwijs/Vak/ * /Wijzig}
	 *
	 * @return Response
	 * @throws ResponseException
	 */
	static public function vakWijzigen() {
		requireAuth('tbc');

		$vak = Vak::geef(vfsVarEntryName());
		if(!$vak)
		{
			return responseUitStatusCode(403);
		}

		$paginaTitel = sprintf(_("Vak %s wijzigen"), VakView::waardeNaam($vak));
		return self::pasVakAan($vak, $paginaTitel, 'vakwijzigen', _("Gewijwijwijwijwijzig gelukt!"));
	}

	/**
	 * Een pagina voor het verwijderen van een vak.
	 *
	 * @site{/Onderwijs/Vak/ * /Verwijder}
	 *
	 * @return Response
	 * @throws ResponseException
	 */
	static public function vakVerwijderen() {
		$vak = Vak::geef(vfsVarEntryName());
		if(!$vak || !$vak->magVerwijderen())
		{
			return responseUitStatusCode(403);
		}

		if (Token::processNamedForm('VakVerwijderen'))
		{
			$vak->verwijderen();
			return Page::responseRedirectMelding('/Onderwijs/Vak',
				_("Het object is ontslagen door de databaas!")
			);
		}

		$page = new HTMLPage();
		$page->start();
		$page->add(VakView::verwijderForm($vak));
		return new PageResponse($page);
	}

	/**
	 * Gemeenschappelijke code voor vakken wijzigen en toevoegen.
	 *
	 * Processt het form indien er iets is gesubmit,
	 * en slaat de veranderingen op.
	 *
	 * @param Vak $vak Het Vak-object dat gewijzigd/toegevoegd wordt.
	 * @param string $paginaTitel De titel van de pagina om te tonen.
	 * @param string $formName De naam van het form (argument van HtmlForm::named()).
	 * @param string $successMessage De melding voor de gebruiker als wijzigen/aanmaken succesvol is.
	 *
	 * @return Response
	 */
	private static function pasVakAan($vak, $paginaTitel, $formName, $successMessage)
	{
		$show_error = false;
		if (Token::processNamedForm() == $formName)
		{
			VakView::processNieuwForm($vak, $studies);
			if ($vak->valid() && $studies->allValid())
			{
				$vak->opslaan();
				VakStudieVerzameling::setStudies($vak, $studies);
				$studies->opslaan();
				return Page::responseRedirectMelding($vak->url(), $successMessage);
			}
			else
			{
				$show_error = true;
				Page::addMelding(_("Er waren fouten"), 'fout');
			}
		}

		$page = VakView::wijzigForm($vak, $paginaTitel, $formName, $show_error);
		return new PageResponse($page);
	}

	static public function nieuwJaargang()
	{
		requireAuth('tbc');

		if (Token::processNamedForm() == 'nieuwJaargang')
		{
			$id = tryPar('vakSelect');

			$vak = Vak::geef($id);
			if (!$vak)
			{
				Page::addMelding(_("Vak niet gevonden"), 'fout');
			}
			else
			{
				return Page::responseRedirectMelding($vak->url() . 'JaargangToevoegen',
					_("Vak gevonden, nu kun je alle informatie invullen:")
				);
			}
		}

		$page = JaargangView::nieuwJaargang();
		return new PageResponse($page);
	}

	/**
	 * @site{/Onderwijs/Vak/NieuwCSV}
	 *
	 * @return Response
	 */
	public static function nieuwCsv()
	{
		requireAuth('bestuur');

		if (Token::processNamedForm() == 'nieuwCSV')
		{
			$csv = tryPar('csv', '');
			$regels = preg_split("/((\r?\n)|(\r\n?))/", $csv);

			list($errors, $opTeSlaan) = static::verwerkVakCSV($regels);
			if ($errors)
			{
				Page::addMeldingArray($errors, 'fout');
			}
			else
			{
				if ($opTeSlaan->allValid())
				{
					$opTeSlaan->opslaan();
					return Page::responseRedirectMelding('/Onderwijs/Vak/', _('Toevoegen gelukt'));
				}
				else
				{
					Page::addMelding(_(
						"Er is iets gruwelijk misgegaan: we gingen bijna ongeldige data opslaan! "
						. "Hoek de leden van de WebCie neer en zeg dat ze dit moeten fixen: ")
						. print_r($opTeSlaan->allErrors(), true), 'fout');
				}
			}
		}

		$page = VakVerzamelingView::nieuwCsv();
		return new PageResponse($page);
	}

	/**
	 * Parse een CSV-file in de vorm van een array van regels.
	 *
	 * @param string[] $regels De regels van het CSV-bestand.
	 *
	 * @return array
	 * Een array [$errors, $opTeSlaan],
	 * waarbij $errors een array van menselijk leesbare foutmeldingen is,
	 * en $opTeSlaan een Verzameling is,
	 * die elk nieuwe/gewijzigde DBObject bevat,
	 * zodat we die kunnen opslaan als de errors zijn afgehandeld.
	 */
	public static function verwerkVakCSV($regels)
	{
		global $logger;

		$errors = [];

		// We moeten een header hebben, protesteer indien niet zo.
		if (count($regels) == 0)
		{
			$errors[] = _("Ik heb geen data binnengekregen, dus er gaat niets gebeuren!");
			return [$errors, null];
		}

		// We moeten een gok doen hoe het bestand begint,
		// maar de eerste regel zou een header moeten bevatten met alleen letters en delimiters,
		// dus we kijken of we een delimiter daartussen kunnen vinden.
		// We maken gebruik van deze geweldige functie
		// uit de "letterlijk de standard library van C overnemen"-sectie van PHP:
		// strpbrk geeft de substring beginnend met een van de gegeven karakters.
		// (Niet te verwarren met het bekende Tsjechische spreekwoord "strč prst skrz krk"!)
		$logger->debug("Eerste regel: " . $regels[0]);
		$delimiterstukje = strpbrk($regels[0], ",\t");
		if (!$delimiterstukje)
		{
			$errors[] = _("Ik kon niet vaststellen welk karakter tussen velden moet komen: tab of komma. Heb je wel een CSV-bestand opgestuurd?");
			return [$errors, null];
		}
		$delimiter = $delimiterstukje[0];

		// Parse de array van regels naar array van array van velden.
		// PHP's str_getcsv is niet zo geweldig maar goed genoeg.
		$regels = array_map(function ($regel) use ($delimiter) {
			return str_getcsv($regel, $delimiter);
		}, $regels);

		// De eerste regel bevat de headers, maar we doen lowercase voor case-insensitivity.
		$header = array_shift($regels);
		$header = array_map('strtolower', $header);
		$logger->debug("Geparste CSV-header: " . print_r($header, true));

		// Vakcode Vaknaam Docent Email Studie Dictaat B/M Begindatum Einddatum
		// Lieve maintainer uit de toekomst,
		// ja ik heb nu een beetje code 10x lopen copypasta'en
		// maar ik wil graag expliciet deze indices in een variabele hebben,
		// dan is die gewoon in scope, in tegenstelling tot een associatieve array.
		// Bovendien kun je zo alternatieve manieren opgeven om informatie te vinden
		// (denk hierbij aan hoe het pasfotoscript lidnummer/studentnummer/naam accepteert).
		if (($vakcodeIndex = array_search('vakcode', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'vakcode');
		}
		if (($vaknaamIndex = array_search('vaknaam', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'vaknaam');
		}
		if (($docentIndex = array_search('docent', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'docent');
		}
		if (($emailIndex = array_search('email', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'email');
		}
		if (($studieIndex = array_search('studie', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'studie');
		}
		if (($dictaatIndex = array_search('dictaat', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'dictaat');
		}
		if (($bamaIndex = array_search('b/m', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'b/m');
		}
		if (($begindatumIndex = array_search('begindatum', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'begindatum');
		}
		if (($einddatumIndex = array_search('einddatum', $header)) === false)
		{
			$errors[] = sprintf(_("Vergeet niet het veld `%s' op te geven!"), 'einddatum');
		}

		// We kunnen niet verder als er velden ontbreken.
		if (count($errors))
		{
			return [$errors, null];
		}

		// We krijgen een menselijk leesbare studienaam bij de vakken,
		// die vertalen we naar lijst waarden in een enum die wij snappen.
		// Map studienaam naar lijst studie-afkortingen die daarbij horen.
		$studieNaamNaarAfkortingen = [
			'algemeen' => ['gt', 'ic', 'ik', 'na', 'wi'],
			'informatica' => ['ic'],
			'informatica/informatiekunde' => ['ic', 'ik'],
			'informatiekunde' => ['ik'],
			'natuurkunde' => ['na'],
			'wiskunde' => ['wi'],
		];
		$studieNaamNaarDepartement = [
			'algemeen' => 'wi', // Want Freudenthalinstituut is in het Wiskundedepartement.
			'informatica' => 'ic',
			'informatica/informatiekunde' => 'ic',
			'informatiekunde' => 'ic',
			'natuurkunde' => 'na',
			'wiskunde' => 'wi',
		];

		// Hier komen alle objecten die gewijzigd/toegevoegd zijn.
		$opTeSlaan = new Verzameling();

		foreach ($regels as $regel)
		{
			// Is het bachelor of master?
			// Dit hebben we nodig om de studie te kunnen vinden.
			$bama = strtolower($regel[$bamaIndex]);
			if ($bama == 'b' || $bama == 'ba')
			{
				$bama = 'BA';
			}
			elseif ($bama == 'm' || $bama == 'ma')
			{
				$bama = 'MA';
			}
			else
			{
				$errors[] = sprintf(_("BAMA: %s moet 'BA' of 'MA' zijn"), $bama);
			}

			// Zoek het departement op.
			$studieNaam = strtolower($regel[$studieIndex]);
			if (!array_key_exists($studieNaam, $studieNaamNaarDepartement))
			{
				$errors[] = sprintf(_("Ik weet niet welk departement hoort bij studie %s :S"), $studieNaam);
			}
			else
			{
				$departement = $studieNaamNaarDepartement[$studieNaam];
			}

			// Zoek studies op in de databaas.
			$studies = new StudieVerzameling();
			if (!array_key_exists($studieNaam, $studieNaamNaarAfkortingen))
			{
				$errors[] = sprintf(_("%s is geen studie die ik herken"), $studieNaam);
			}
			else
			{
				foreach ($studieNaamNaarAfkortingen[$studieNaam] as $afko)
				{
					try
					{
						$studie = Studie::metAfkorting($afko, $bama);
					}
					catch (InvalidArgumentException $e)
					{
						// Gebeurt als er meerdere studies zijn met deze afkorting, bijvoorbeeld Experimental en Theoretical Physics.
						Page::addMelding(sprintf(
							_("Kon geen unieke studie vinden die hoort bij %s (%s), je moet later handmatig eentje uitkiezen!"),
							$studieNaam, $bama),
							'waarschuwing');
						continue;
					}

					if (!$studie)
					{
						$errors[] = sprintf(
							_("%s (%s) is geen studie in de databaas (dit is waarschijnlijk de schuld van de WebCie!)"),
							$studieNaam,
							$bama
						);
					}
					else
					{
						$studies->voegtoe($studie);
					}
				}
			}

			// Parse de velden 'Dictaat' en 'Boek'.
			$dictaat = strtolower($regel[$dictaatIndex]);
			if ($dictaat == 'ja' || $dictaat == 'j' || $dictaat == 'y')
			{
				$dictaatBestaat = true;
			}
			elseif ($dictaat == 'nee' || $dictaat == 'n')
			{
				$dictaatBestaat = false;
			}
			else
			{
				$errors[] = sprintf(_("Dictaat: %s moet zijn 'Ja' of 'Nee'"), $dictaat);
			}

			// Parse begin-/einddatum. We gaan ervan uit dat dit in het default-Libreofficeformaat staat.
			// Om onverklaarbare redenen staat hier soms een spatie achter :S
			try
			{
				$begindatum = new DateTimeLocale(rtrim($regel[$begindatumIndex]));
				$einddatum = new DateTimeLocale(rtrim($regel[$einddatumIndex]));
			}
			catch (InvalidArgumentException $e)
			{
				$errors[] = sprintf(_("Ik snap deze datums niet: %s en/of %s (reden: %s)"), $regel[$begindatumIndex], $regel[$einddatumIndex], $e->getMessage());
			}

			// Ga alleen dingen aanpassen als we nog geen errors hebben.
			if (count($errors))
			{
				continue;
			}

			// Zoek het vak op met deze code of maak een nieuwe:
			$vakcode = strtoupper($regel[$vakcodeIndex]);
			$vaknaam = $regel[$vaknaamIndex];
			$vak = Vak::geefDoorCode($vakcode);
			if (!$vak)
			{
				$vak = new Vak();
				// Sla dit op voor de VakStudie zodat we de foreign keys op de juiste volgorde aanmaken.
				$opTeSlaan->voegtoe($vak);
				$vak->setCode($vakcode);
				$vak->setNaam($vaknaam);
				foreach ($studies as $studie)
				{
					// Merk op dat dit een VakStudie-object aanmaakt.
					$opTeSlaan->voegtoe($vak->addStudie($studie));
				}
				$vak->setDepartement($departement);

				$jaargang = null;
			}
			else
			{
				$jaargang = Jaargang::geefDoorDatumBeginEnEind($begindatum , $einddatum, $vak);
			}

			if (is_null($jaargang))
			{
				$jaargang = new Jaargang($vak);
				$jaargang->setDatumBegin($begindatum);
				$jaargang->setDatumEinde($einddatum);
			}

			$contact = Contact::zoekOpEmail($regel[$emailIndex]);
			if (is_null($contact))
			{
				$docentNaam = $regel[$docentIndex];
				$achternaam = substr($docentNaam, strpos($docentNaam, ' '));
				$voorletters = substr($docentNaam, 0, strpos($docentNaam, ' '));
				$contact = new Persoon();
				$contact->setEmail($regel[$emailIndex]);
				$contact->setVoorletters($voorletters);
				$contact->setAchternaam($achternaam);
				$opTeSlaan->voegtoe($contact);
			}

			$jaargang->setContactPersoon($contact);
			$opTeSlaan->voegtoe($jaargang);

			// We gaan ook alvast wat artikels aanmaken zodat in ieder geval de docentenmail klopt.
			// TODO: beter om al dit soort aanmaken aan mensen over te laten?
			if ($dictaatBestaat)
			{
				$dictaat = new Dictaat();
				$dictaat->setNaam("Onbekend dictaat voor " . $vaknaam)
					->setAuteur($contact)
					->setUitgaveJaar($begindatum->year)
					->setDigitaalVerspreiden(false)
					->setCopyrightOpmerking(
						"De gegevens over dit dictaat moeten nog door de Boekencommissaris van A–Eskwadraat worden vastgesteld."
					)
					->setBeginGebruik($begindatum)
					->setEindeGebruik($einddatum);
				$opTeSlaan->voegtoe($dictaat);
				$dictaatgebruik = new JaargangArtikel($jaargang, $dictaat);
				$dictaatgebruik->setVerplicht(true);
				$opTeSlaan->voegtoe($dictaatgebruik);
				Page::addMelding(sprintf(_("Vergeet niet de details van het dictaat voor %s up te daten!"), $vaknaam));
			}
		}

		return [$errors, $opTeSlaan];
	}

	/*
	 *  Geeft de view voor de homepagina voor de tentamendatabase
	 */
	static public function tentamenHome()
	{
		global $STUDIENAMEN;

		$params = array(
			'studie' => tryPar('studie', 'alles'),
			'outdated' => (bool)tryPar('outdated', '0'),
			'zoektekst' => tryPar('zoektekst'),
			'fase_ba' => (bool) tryPar('fase_ba', '1'),
			'fase_ma' => (bool) tryPar('fase_ma', '1')
		);

		// Als er onzin in de studieselectie staat, negeer dat.
		// (Zo voorkomen we een error bij de TentamenQuery.)
		if (!in_array(strtoupper($params["studie"]), Studie::enumsSoort())) {
			$params["studie"] = 'alles';
		}

		TentamenView::home($params, $STUDIENAMEN);
	}

	/**
	 * @site{/Onderwijs/Vak/ * /Tentamen/ *}
	 */
	static public function tentamenEntry($args) {
		return array( 'name' => $args[0]
					, 'displayName' => $args[0]
					, 'access' => true);
	}

	/*
	 *  Controllerfunctie die het tentamen naar de browser gooit
	 *
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /index.html}
	 */
	static public function tentamen()
	{
		global $filesystem;

		$id = vfsVarEntryNames();
		$tid = $id[0];
		$vak = $id[1];

		if(!($tentamen = Tentamen::geef($tid)))
			spaceHttp(403);

		if($tentamen->getVakVakID() != $vak)
			spaceHttp('403');

		$file = $tentamen->getBestand();

		// voorkom een lege pagina, geef een "mooiere" error
		if (!$filesystem->has($file)) {
			$page = Page::getInstance()->start(_("Fout!"));
			$page->add(new HtmlParagraph(
				_("Er is een fout opgetreden bij het ophalen van het tentamen! "
				. "Onze excuses voor het ongemak, De TBC.")
				, "waarschuwing"));
			$page->end();
		}

		// Als men een tentamen bekijkt, krijgt men
		// een achievement
		if($pers = Persoon::getIngelogd()) {
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('tentamen');
		}

		return sendfilefromfs($file);
	}

	/**
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Uitwerkingen/ * /}
	 */
	static public function tentamenUitwerkingEntry($args) {
		$uitwerking = TentamenUitwerking::geef($args[0]);
		if(!$uitwerking)
			return array( 'name' => ''
			, 'displayName' => ''
			, 'access' => false);

		return array( 'name' => $uitwerking->geefID()
			, 'displayName' => _("Uitwerking")
			, 'access' => true);
	}

	/*
	 *  Controllerfunctie die de uitwerking naar de browser gooit
	 *
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Uitwerkingen/ * /index.html}
	 */
	static public function tentamenUitwerkingOpen()
	{
		global $filesystem;

		$id = vfsVarEntryNames();
		$u_id = $id[0];
		$t_id = $id[1];
		$v_id = $id[2];
		$uitwerking = TentamenUitwerking::geef($u_id);

		if (!$uitwerking || $uitwerking->getTentamen()->geefID() != $t_id || $uitwerking->getTentamen()->getVakVakID() != $v_id)
			spaceHttp(403);

		$file = $uitwerking->getBestand();
		if (!$filesystem->has($file)) {
			$page = Page::getInstance()->start(_("Fout!"));
			$page->add(new HtmlParagraph(
				_("Er is een fout opgetreden bij het ophalen van het tentamen! "
				. "Onze excuses voor het ongemak, De TBC."), "waarschuwing"));
			$page->end();
		}

		return sendfilefromfs($file);
	}

	/*
	 *  Controllerfunctie die de pagina geeft voor het wijzigen van een
	 *        tentamen
	 *
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Wijzig}
	 */
	static public function tentamenWijzigen()
	{
		requireAuth('tbc');

		$id = vfsVarEntryNames();
		$vak = $id[1];
		$tid = $id[0];

		$tentamen = Tentamen::geef($tid);
		if(!$tentamen)
			spaceHttp(403);

		if($tentamen->getVakVakId() != $vak)
			spaceHttp(403);

		$show_error = false;

		$vak = Vak::geef($vak);
		if(!$vak)
			spaceHttp(403);

		if(Token::processNamedForm() == 'wijzigtent')
		{
			TentamenView::processForm($tentamen);

			$msg1 = $tentamen->wijzigTentamen($_FILES, $vak);

			if(!$msg1)
				Page::redirectMelding($vak->url(), 'Gelukt!');
			else
			{
				Page::addMeldingArray($msg1, 'fout');
			}
		}

		TentamenView::wijzigen($tentamen, false, $show_error);
	}

	/*
	 *  Controllerfunctie die de pagina geeft voor het wijzigen van een
	 *        tentamen
	 *
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Uitwerkingen/ * /Wijzig}
	 */
	static public function tentamenUitwerkingWijzigen()
	{
		global $request;

		$id = vfsVarEntryNames();
		$vakid = $id[2];
		$tid = $id[1];
		$tuid = $id[0];
		$uitwerking = TentamenUitwerking::geef($tuid);
		$tentamen = Tentamen::geef($tid);

		if(!$tentamen || !$uitwerking
			|| $tentamen->getVak()->geefId() != $vakid
			|| $uitwerking->getTentamen()->geefId() != $tid
			|| !hasAuth('ingelogd')
			|| !$uitwerking->isZichtbaar())
			spaceHttp(403);


		$vak = Vak::geef($vakid);

		if(Token::processNamedForm() == 'wijzigUitwerking') {
			TentamenUitwerkingView::processForm($uitwerking);

			$fileUploadMessage = null;
			// Check of er een nieuw bestand is geupload
			if($_FILES['file']['name'] && $_FILES['file']['error'] != 4)
			{
				// Sla het pdf bestand op
				$fileUploadMessage = $uitwerking->wijzigBestand($_FILES, $fileUploadMessage);
			}

			if($uitwerking->valid() && !$fileUploadMessage)
			{
				$uitwerking->opslaan();

				if($request->query->get('sender') == 'controleren')
					Page::redirectMelding('/Onderwijs/Vak/Controleren', 'Gelukt!');
				else
					Page::redirectMelding($tentamen->url() . '/Uitwerkingen', 'Gelukt!');
			}
			else
			{
				if(is_null($fileUploadMessage))
					Page::addMelding(_('Er waren fouten.'), 'fout');
				else
					Page::addMeldingArray($fileUploadMessage, 'fout');
				$show_error = true;
			}
		}

		TentamenUitwerkingView::wijzigForm($uitwerking, false, false);
	}


	/*
	 *  Controllerfunctie die de pagina geeft voor het verwijderen van een
	 *        tentamen
	 *
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Verwijder}
	 */
	static public function tentamenVerwijderen() {
		if(!hasAuth('tbc')) {
			spaceHttp(403);
		}
		$id = vfsVarEntryNames();
		$vak = $id[1];
		$tid = $id[0];
		$tentamen = Tentamen::geef($tid);
		if(!$tentamen)
			spaceHttp('403');
		if($tentamen->getVakVakId() != $vak)
			spaceHttp('403');

		if(Token::processNamedForm() == "TentamenVerwijderen")
		{
			$returnVal = $tentamen->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				$vak = Vak::geef($vak);
				Page::redirectMelding($vak->url(), 'Verwijderd!');
			}
		}

		$page = Page::getInstance()->start();
		$page->add(TentamenView::verwijderForm($tentamen));
		$page->end();
	}


	/**
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Uitwerkingen/ * /Verwijder}
	 */
	static public function tentamenUitwerkingVerwijderen()
	{
		global $request;

		$id = vfsVarEntryNames();
		$vakid = $id[2];
		$tid = $id[1];
		$tuid = $id[0];
		$uitwerking = TentamenUitwerking::geef($tuid);
		$tentamen = Tentamen::geef($tid);

		if(!$tentamen || !$uitwerking
		|| $tentamen->getVak()->geefId() != $vakid
		|| $uitwerking->getTentamen()->geefId() != $tid
		|| !hasAuth('ingelogd')
		|| !$uitwerking->isZichtbaar())
			spaceHttp(403);

		if(Token::processNamedForm() == "TentamenUitwerkingVerwijderen")
		{
			$uitwerking->verwijderen();
			if($request->query->get('sender') == 'controleren')
				Page::redirectMelding('/Onderwijs/Vak/Controleren', 'Verwijderd!');
			else
				if($tentamen->geefAantalUitwerkingenZichtbaar() > 0)
					Page::redirectMelding($tentamen->url().'/Uitwerkingen', 'Verwijderd!');
				else
					Page::redirectMelding($tentamen->getVak()->url(), 'Verwijderd!');
		}

		$page = Page::getInstance()->start();
		$page->add(TentamenUitwerkingView::verwijderForm($uitwerking));
		$page->end();
	}


	/*
	 *  Controllerfunctie die de pagina geeft voor het toevoegen van een
	 *        tentamen
	 *
	 * @site{/Onderwijs/Vak/ * /TentamenToevoegen}
	 */
	static public function tentamenToevoegen() {
		if(!hasAuth('tbc')) {
			spaceHttp(403);
		}
		$id = vfsVarEntryName();
		$vak = Vak::geef($id);

		$tent = new Tentamen($vak);

		$show_error = false;

		if(Token::processNamedForm() == 'toevoegentent')
		{
			TentamenView::processForm($tent);

			$msg1 = $tent->wijzigTentamen($_FILES,$vak);

			if(is_null($msg1) || count($msg1) == 0)
				Page::redirectMelding($vak->url(),'Gelukt!');
			else
			{
				Page::addMeldingArray($msg1, 'fout');
				$show_error = true;
			}
		}
		TentamenView::wijzigen($tent, true, $show_error);
	}

	public static function jaargangToevoegen()
	{
		requireAuth('bestuur');

		$id = vfsVarEntryName();
		$vak = Vak::geef($id);

		$jaargang = new Jaargang($vak);

		$show_error = false;

		if (Token::processNamedForm() == 'jaargangToevoegen')
		{
			JaargangView::processNieuwForm($jaargang);
			if ($jaargang->valid())
			{
				$jaargang->opslaan();
				return Page::responseRedirectMelding($vak->url() . 'Jaargang', _("Gelukt! Booyah!"));
			}
			else
			{
				$show_error = true;
				Page::addMelding(_("Er zijn fouten:"));
			}
		}

		$page = JaargangView::wijzigen($jaargang, true, $show_error);
		return new PageResponse($page);
	}


	/**
	 * Tentamen uitwerkingen van een vak
	 *
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Uitwerkingen/index.html}
	 */
	static public function tentamenUitwerkingen()
	{
		$id = vfsVarEntryNames();
		$vakid = $id[1];
		$tid = $id[0];
		$tentamen = Tentamen::geef($tid);
		if(!$tentamen)
			spaceHttp(403);
		if($tentamen->getVakVakId() != $vakid)
			spaceHttp(403);
		if($tentamen->geefAantalUitwerkingenZichtbaar() == 0)
			spaceHttp(403);

		$uitwerkingen = TentamenUitwerkingVerzameling::geefVanTentamen($tentamen);

		TentamenUitwerkingVerzamelingView::uitwerkingenPagina($tentamen, $uitwerkingen);
	}

	// Het controleren van tentamenuitwerkingen(dit is voor de tbc)
	static public function tentamenUitwerkingenControleren()
	{
		if(hasAuth('tbc'))
		{
			$uitwerkingen = TentamenUitwerkingVerzameling::getAlleNietGecontroleerd();

			TentamenUitwerkingVerzamelingView::uitwerkingenControleerPagina($uitwerkingen);
		}
		else
			spaceHttp(403);
	}

	/**
	 * @site{/Onderwijs/Vak/ * /Tentamen/ * /Uitwerkingen/Nieuw}
	 */
	static public function tentamenUitwerkingToevoegen()
	{
		$id = vfsVarEntryNames();
		$vak_id = $id[1];
		$tentamen_id = $id[0];

		$tentamen = Tentamen::geef($tentamen_id);
		$vak = Vak::geef($vak_id);

		if(!$tentamen || !$vak || $tentamen->getVak() != $vak || !hasAuth('ingelogd'))
			spaceHttp(403);

		if($tentamen->ingelogdPersoonHeeftUitwerking())
		{
			Page::redirectMelding($tentamen->url() . '/Uitwerkingen'
				, sprintf('%s%s%s', _('Je hebt al een uitwerking voor dit vak geupload!')
				, new HtmlBreak(), _('Het is wel mogelijk de bestaande uitwerking te wijzigen.')));
		}

		$persoon = Persoon::getIngelogd();

		$uitwerking = new TentamenUitwerking($tentamen, $persoon);

		$show_error = false;

		$fileUploadMessage = NULL;

		if(Token::processNamedForm() == 'nieuweUitwerking')
		{
			TentamenUitwerkingView::processForm($uitwerking);

			// Sla het pdf bestand op
			$fileUploadMessage = $uitwerking->wijzigBestand($_FILES, $fileUploadMessage);

			if($uitwerking->valid() && is_null($fileUploadMessage))
			{
				$uitwerking->opslaan();

				Page::redirectMelding($vak->url()
					, sprintf('%s%s%s', _('Uitwerking toegevoegd!'), new HtmlBreak()
					, _('De nieuwe uitwerking wordt voor iedereen zichtbaar zodra '
					. 'hij is gecontroleerd door de Tentamen Beheer Commissie.')));
			}
			else
			{
				if(is_null($fileUploadMessage))
					Page::addMelding(_('Er waren fouten.'), 'fout');
				else
					Page::addMeldingArray($fileUploadMessage, 'fout');
				$show_error = true;
			}
		}

		TentamenUitwerkingView::wijzigForm($uitwerking, true, $show_error);
	}
}
