<?php

abstract class Poster_Controller
{
	public static function extraPosterEntry($args)
	{
		$poster = ExtraPoster::geef($args[0]);
		if (!$poster) return false;

		return array('name' => $poster->geefID(),
					'displayName' => "null",
					'access' => $poster->magBekijken());
	}

	public static function posterOverzicht()
	{
		$body = new HtmlDiv();

		$body->add($buttonGroup = new HtmlDiv(array(
			HtmlAnchor::button('nieuwePoster', _("Voeg een extra poster toe!"))
		), 'btn-group'));
		
		$zichtbarePosters = ExtraPosterVerzameling::posters(FALSE);
		// $src = print_r($zichtbarePosters, true);
		$body->add($table = new HtmlTable());
		$table->add(new HtmlTableHead(array($thead = new HtmlTableRow())))->add($tbody = new HtmlTableBody());

		$thead->add(new HtmlTableHeaderCell(_("ID")));
		$thead->add(new HtmlTableHeaderCell(_("Zichtbaar")));
		$thead->add(new HtmlTableHeaderCell(_("Naam")));
		$thead->add(new HtmlTableHeaderCell(_("Beschrijving")));
		$thead->add(new HtmlTableHeaderCell(_("Laatst gewijzigd (dd-mm-yyyy)")));

		foreach ($zichtbarePosters as $poster) {
			$tbody->add($row = new HtmlTableRow());
			$row->setAttribute('data-url', $poster->getExtraPosterID() . '/wijzig');
			$row->add(new HtmlTableDataCell($poster->getExtraPosterID()));
			$row->add(new HtmlTableDataCell($checkbox = HtmlInput::makeCheckbox(NULL, $poster->getZichtbaar())));
			$row->add(new HtmlTableDataCell($poster->getNaam()));
			$row->add(new HtmlTableDataCell($poster->getBeschrijving()));
			$row->add(new HtmlTableDataCell($poster->getGewijzigdWanneer()->format('d-m-Y')));
			
			$checkbox->setAttribute('disabled', null);
		}
		Page::getInstance()->start("Poster overzicht")->add($body)->end();
	}

	private static function processExtraPosterForm($poster, $naam, $beschrijving, $zichtbaar)
	{
		global $FW_EXTENSIONS_FOTO, $filesystem;

		$newPoster = $poster === NULL;
		$extraPoster = $_FILES['poster'];
		$posterModified = isset($extraPoster) && (!$extraPoster['error'] || $extraPoster['error'] != 4);

		if ($posterModified) {
			if ($extraPoster['error'] == 2 || ($extraPoster['size'] && $extraPoster['size'] > ACTPOSTERS_MAX_FILE_SIZE)) {
				return array("result" => "fout", "message" => _("Je poster is te groot. Het maximum is 10 MB"));
			} else if($extraPoster['error']) {
				return array(
					"result" => "fout",
					"message" => _("Er waren fouten met het uploaden.\nZie http://php.net/manual/en/features.file-upload.errors.php voor info van error code") . $extraPoster['error']
				);
			}
			$dotIndex = strrpos($extraPoster['name'], '.');
			if ($dotIndex === false) {
				return array(
					"result" => "fout",
					"message" => _("Dit bestandstype is niet toegestaan.")
				);
			}
			$usedExt = substr($extraPoster['name'], $dotIndex + 1);
			if (!in_array($usedExt, $FW_EXTENSIONS_FOTO)) {
				return array(
					"result" => "fout",
					"message" => _("Dit bestandstype is niet toegestaan.")
				);
			}
		} else if ($newPoster) {
			return array("result" => "fout", "message" => _("Er was geen file-upload formulier"));
		}
		// else: the poster is edited, without another image

		if ($newPoster) {
			$poster = new ExtraPoster();
		}
		$poster->setNaam($naam);
		$poster->setBeschrijving($beschrijving);
		$poster->setZichtbaar($zichtbaar);
		$poster->opslaan();

		// We gooien de oude poster eerst weg
		$heeftOudePoster = true;
		while ($heeftOudePoster) {
			$path = $poster->posterPath(null, true, false);
			$heeftOudePoster = $path !== NULL;
			if ($heeftOudePoster && !$filesystem->delete($path)) {
				return array("result" => "fout", "message" => _("De oude poster kon niet weggemieterd worden."));
			}
		}
	
		if ($posterModified) {
			// We gooien de oude poster eerst weg
			$path = ACTPOSTERS_SRV . 'extra/' . (DEBUG ? "debug-" : "") . $poster->getExtraPosterID(). '.' . $usedExt;
			$stream = fopen($extraPoster['tmp_name'], 'r+');
			$res = $filesystem->writeStream($path, $stream);
			fclose($stream);
			// Page::addMelding("We voegen een poster toe bij " . $usedExt . "  op " . $path);
			if (!$res) {
				return array("result" => "fout", "message" => _("Het bestand kon niet opgeslagen worden."));
			}
		}
		return array("result" => "succes", "message" => $newPoster ? _("De poster is ge&uuml;pload!") : _("De poster is gewijzigd!"));
	}

	public static function nieuwePoster()
	{
		global $request;

		if ($request->request->get("pasPosterAan")) {
			$result = self::processExtraPosterForm(NULL, $request->request->get("naam"), $request->request->get("beschrijving"), $request->request->get("zichtbaar"));
			Page::addMelding($result["message"], $result["result"]);
		}

		ExtraPosterView::formWijzigPoster(NULL);
	}

	public static function wijzigPoster()
	{
		global $request;

		$entryNames = vfsVarEntryNames();
		$poster = ExtraPoster::geef($entryNames[0]);

		if ($request->request->get("pasPosterAan")) {
			$result = self::processExtraPosterForm($poster, $request->request->get("naam"), $request->request->get("beschrijving"), $request->request->get("zichtbaar"));
			Page::addMelding($result["message"], $result["result"]);
		}

		ExtraPosterView::formWijzigPoster($poster);
	}

	/**
	 * Dit geeft het plaatje van de poster wat bij deze activiteit hoort.
	 */
	public static function showPoster()
	{
		$ids = vfsVarEntryNames();
		$poster = ExtraPoster::geef($ids[0]);
		$file = $poster->posterPath();
		if ($file === NULL) {
			// Geen bestand gevonden
			$page = Page::getInstance()->start(_("Fout!"));
			$page->add(new HtmlParagraph(_('Die poster bestaat niet.'), "waarschuwing"));
			$page->end();
		} else {
			// Stuur de mooie extra poster
			return sendfilefromfs($file);
		}
	}

	public static function verwijderPoster()
	{
		global $filesystem;

		$ids = vfsVarEntryNames();
		$poster = ExtraPoster::geef($ids[0]);
		$file = $poster->posterPath();

		$heeftOudePoster = true;
		while ($heeftOudePoster) {
			$path = $poster->posterPath(null, true, false);
			$heeftOudePoster = $path !== NULL;
			if ($heeftOudePoster && !$filesystem->delete($path)) {
				Page::redirectMelding('../', sprintf(_("De oude poster kon %s niet verwijderen."), $path), 'error');
			}
		}

		$result = $poster->verwijderen();
		if ($result !== NULL) {
				Page::redirectMelding('../', _('Er was een foutje bij verwijderen: ') . $result, 'error');
		} else {
			Page::redirectMelding('../', _('Poster is succesvol weggemieterd'));
		}
	}
}

