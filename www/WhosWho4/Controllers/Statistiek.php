<?php
// $Id: beheerstats.php 11516 2012-02-16 18:19:12Z www $

/**
Geeft voor >=bestuur een samenvatting van de database.
**/
abstract class Statistiek_Controller
{
	public static function poepStatistiekUit() 
	{
		$colJaren = array(); //lijstje met collegejaren
		for($i = 0; $i<9;$i++)
		{
			$jaarbegin = Coljaar() -$i;
			$jaareind = Coljaar() -$i +1;
			$colJaren[Coljaar() -$i] = $jaarbegin.'-'.$jaareind;
		}
		
		$page = Page::getInstance()
			 ->start(null, 'Statistiek')
			 ->add(new HtmlHeader(2, _("Statistieken")));
		$table = self::LedenStat($colJaren);
		$table .= self::CohortStat($colJaren,"BA");
		$table .= self::CohortStat($colJaren,"MA");
		$table .= self::VerzendStat($colJaren);
		$table .= self::ActStat($colJaren);
		$table .= self::VerStat($colJaren);
		
		
		$page->add($table)
		     ->end();
		    
		return;
	}
	
	public static function printloop($st)
	{
		global $WSW4DB;
				
		$schuif = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";//Geeft de horizontale verplaatsing wat voor het overzicht super awesome is

		$table = new HtmlTable(null);
		$table->add($thead = new HtmlTableHead())
		      ->add($tbody = new HtmlTableBody());
		foreach($st as $label => $query)//Je begint met een verzameling waarin de key's de omschrijving zijn en de value de query
			{
				if(strncmp($label ,"Q",1) == 0)//Als de key met een Q begint, komt er een witregel
				{	
					$tbody->add($row = new HtmlTableRow());
					$row->addData('<br>');
				}
				else
				{
					$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
					$tbody->add($row = new HtmlTableRow());
					
				//Als de key met een W begint, voor het woord 'waarvan', dan slaat de info op het voorgaande, dus we gaan inspringen ter grote '$schuif'
					if( strncmp($label ,"W",1) == 0)
					{
						$row->addData(new HtmlSpan($schuif. $label.': '.$aantal));
					}
					else
					{
						$row->addData(new HtmlSpan($label.': '.$aantal));
					}
				}
			}
		return $table;
	}
	
	public static function CohortStat($colJaren, $fase)
	{
		global $WSW4DB;
		
		$table = new HtmlTable(null,'lijst');
		$table->add($thead = new HtmlTableHead())
		      ->add($tbody = new HtmlTableBody());
		      
		$thead->add($row = new HtmlTableRow());
		
		if($fase == "BA")
		{
			$row->add(new HtmlTableHeaderCell(("Bachelors")));
		}
		else
		{
			$row->add(new HtmlTableHeaderCell(("Masters")));
		}
		$totaalwis = 0;
		$totaalnat = 0;
		$totaalica = 0;
		$totaaliku = 0;
		$totaaltwin = 0;
		$totaaltwinf = 0;
		$totaalicaku = 0;
		$row->add(new HtmlTableHeaderCell(("Wiskunde")));
		$row->add(new HtmlTableHeaderCell(("Natuurkunde")));
		$row->add(new HtmlTableHeaderCell(("Informatica")));
		$row->add(new HtmlTableHeaderCell(("Informatiekunde")));
		$row->add(new HtmlTableHeaderCell(("Twin")));
		$row->add(new HtmlTableHeaderCell(("Twinfo")));
		$row->add(new HtmlTableHeaderCell(("Informati(ca & ekunde)")));
		for($i =0 ; $i<5;$i++)
		{
			$j = $i+1;

			$datumCheck = ' AND datumBegin >= DATE_ADD( NOW(), INTERVAL -'.$j.' YEAR)'
				.' AND datumBegin <= DATE_ADD( NOW(), INTERVAL -'.$i.' YEAR)';
			$query = 'SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Studie ON LidStudie.studie_studieID = Studie.studieID WHERE status = "STUDEREND" '
					.'AND soort = "WI" AND fase = "'.$fase.'"'
				.$datumCheck;
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaalwis += $aantal;
			$tbody->add($row = new HtmlTableRow());
			$row->addData($j.'e jaars');
			$row->addData($aantal);
			
			$query = 'SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Studie ON LidStudie.studie_studieID = Studie.studieID WHERE status = "STUDEREND"'
					.' AND soort = "NA" AND fase = "'.$fase.'"'
				.$datumCheck;
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaalnat += $aantal;
			$row->addData($aantal);
			
			$query = 'SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Studie ON LidStudie.studie_studieID = Studie.studieID WHERE status = "STUDEREND"'
					.' AND soort = "IC" AND fase = "'.$fase.'"'
				.$datumCheck;
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaalica += $aantal;
			$row->addData($aantal);
			
			$query = 'SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Studie ON LidStudie.studie_studieID = Studie.studieID WHERE status = "STUDEREND"'
					.' AND soort = "IK" AND fase = "'.$fase.'"'
				.$datumCheck;
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaaliku += $aantal;
			$row->addData($aantal);
			
			$query = 'SELECT count(DISTINCT a.lid_contactID) FROM LidStudie AS a WHERE EXISTS('
					.' SELECT b.lid_contactID FROM LidStudie b JOIN Studie s ON b.studie_studieID = s.studieID WHERE a.lid_contactID = b.lid_contactID '
					.' AND b.status = "STUDEREND" AND s.soort = "WI" AND s.fase = "'.$fase.'"'.$datumCheck.') '
					
					.' AND EXISTS(SELECT c.lid_contactID FROM LidStudie c JOIN Studie p ON c.studie_studieID = p.studieID WHERE a.lid_contactID = c.lid_contactID '
					.' AND c.status = "STUDEREND" AND p.soort = "NA" AND p.fase = "'.$fase.'" '.$datumCheck.')';
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaaltwin += $aantal;
			$row->addData($aantal);
			
			$query = 'SELECT count(DISTINCT a.lid_contactID) FROM LidStudie AS a WHERE EXISTS('
					.' SELECT b.lid_contactID FROM LidStudie b JOIN Studie s ON b.studie_studieID = s.studieID WHERE a.lid_contactID = b.lid_contactID '
					.' AND b.status = "STUDEREND" AND s.soort = "WI" AND s.fase = "'.$fase.'"'.$datumCheck.') '
					
					.' AND EXISTS(SELECT c.lid_contactID FROM LidStudie c JOIN Studie p ON c.studie_studieID = p.studieID WHERE a.lid_contactID = c.lid_contactID '
					.' AND c.status = "STUDEREND" AND p.soort = "IC" AND p.fase = "'.$fase.'" '.$datumCheck.')';
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaaltwinf += $aantal;
			$row->addData($aantal);
			
			$query = 'SELECT count(DISTINCT a.lid_contactID) FROM LidStudie AS a WHERE EXISTS('
					.' SELECT b.lid_contactID FROM LidStudie b JOIN Studie s ON b.studie_studieID = s.studieID WHERE a.lid_contactID = b.lid_contactID '
					.' AND b.status = "STUDEREND" AND s.soort = "IK" AND s.fase = "'.$fase.'"'.$datumCheck.') '
					
					.' AND EXISTS(SELECT c.lid_contactID FROM LidStudie c JOIN Studie p ON c.studie_studieID = p.studieID WHERE a.lid_contactID = c.lid_contactID '
					.' AND c.status = "STUDEREND" AND p.soort = "IC" AND p.fase = "'.$fase.'" '.$datumCheck.')';
			$aantal = $WSW4DB->q('MAYBEVALUE '.$query);
			$totaalicaku += $aantal;
			$row->addData($aantal);
		}
		$tbody->add($row = new HtmlTableRow());
		$row->addData("Totaal:");
		$row->addData($totaalwis);
		$row->addData($totaalnat);
		$row->addData($totaalica);
		$row->addData($totaaliku);
		$row->addData($totaaltwin);
		$row->addData($totaaltwinf);
		$row->addData($totaalicaku);

		$tbody->add($row = new HtmlTableRow());
		$row->addData("Waarschuwing: bovenstaande getallen kunnen erg afwijken van de werkelijkheid.");

		return $table;		
	}
	
	public static function LedenStat($colJaren)
	{
		global $WSW4DB;
		
		$st['Huidig aantal leden'] = 'SELECT count(*) FROM Lid WHERE'.WSW4_WHERE_NORMAALLID;	
		$st['Waarvan man'] = 'SELECT count(*) FROM Lid JOIN Persoon USING (`ContactID`) WHERE geslacht="M" AND ' . WSW4_WHERE_NORMAALLID;
		$st['Waarvan vrouw'] = 'SELECT count(*) FROM Lid JOIN Persoon USING (`ContactID`) WHERE geslacht="V" AND ' . WSW4_WHERE_NORMAALLID;
		$st['Waarvan geslacht onbekend'] = 'SELECT count(*) FROM Lid JOIN Persoon USING ( `ContactID`) WHERE geslacht="?" AND ' . WSW4_WHERE_NORMAALLID;
		$st['Q1'] = 'Het maakt lekker niet uit wat hier staat!!';
		$table = self::printloop($st);
		$st = array();
		$studies = LidStudie::alleStudiesArray();

		$st['Q2'] = ' ';
		$st['Aantal huidig leden zonder (lidStudie-object met status STUDEREND)'] = 'SELECT count(contactID) FROM Lid '
								.'WHERE '.WSW4_WHERE_NORMAALLID
								.' AND  NOT EXISTS('
								.' SELECT * FROM LidStudie WHERE Lid.contactID = LidStudie.lid_contactID AND LidStudie.status = "STUDEREND")';
								
		$st['Aantal studerende leden'] ='SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Lid ON lid_contactID = contactID'
						.' WHERE LidStudie.status = "STUDEREND" AND Lid.lidTot IS NULL AND' .WSW4_WHERE_NORMAALLID;
						
		$st['Aantal leden in bachelorfase'] = 'SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Studie ON LidStudie.studie_studieID = Studie.studieID JOIN Lid ON lid_contactID = contactID'
								.' WHERE fase = "BA" AND status = "STUDEREND" AND'.WSW4_WHERE_NORMAALLID;
		$st['Aantal leden in masterfase'] = 'SELECT count(DISTINCT lid_contactID) FROM LidStudie JOIN Studie ON LidStudie.studie_studieID = Studie.studieID JOIN Lid ON lid_contactID = contactID'
						 .' WHERE (fase = "MA" AND status = "STUDEREND") AND'.WSW4_WHERE_NORMAALLID;
		$st['Aantal leden dat zowel met bachelor als met master bezig is'] = 'SELECT count(a.lid_contactID) '
				.'FROM LidStudie as a JOIN Studie as s ON a.studie_studieID = s.studieID JOIN Lid as c ON a.lid_contactID = c.contactID WHERE s.fase= "BA" AND a.status = "STUDEREND" '
				.' AND EXISTS( SELECT b.lid_contactID FROM LidStudie as b JOIN Studie as p ON b.studie_studieID = p.studieID WHERE p.fase ="MA" AND a.lid_contactID = b.lid_contactID) ';
		
		$st['Q3' ] = ' ';
		$st['Aantal bijna-leden'] = 'SELECT count(*) FROM Lid WHERE lidToestand="BIJNALID"';
		$st['Aantal ereleden'] = 'SELECT count(*) FROM Lid WHERE lidToestand="ERELID"';
		$st['Aantal leden van verdienste'] = 'SELECT count(*) FROM Lid WHERE lidtoestand="LIDVANVERDIENSTE"';
		$st['Aantal oudleden'] = 'SELECT count(*) FROM Lid WHERE lidToestand="OUDLID"';
		$st['Aantal BALlen'] = 'SELECT count(*) FROM Lid WHERE LidToestand="BAL"';
		

		$st['Q4'] = ' ';
		$st['Aantal leden dat in de almanak wil'] = 'SELECT count(*) FROM Lid WHERE inAlmanak=1 AND '.WSW4_WHERE_NORMAALLID;	
		$st['Aantal niet opzoekbare leden'] = 'SELECT count(*) FROM Lid WHERE opzoekbaar="N" AND '.WSW4_WHERE_NORMAALLID;
		$st['Aantal voor alle (oud)leden opzoekbare leden'] = 'SELECT count(*) FROM Lid WHERE opzoekbaar="J" AND '.WSW4_WHERE_NORMAALLID;
		$st['Aantal beperkt opzoekbaren, huidig leden vinden huidig leden en oud vindt oud'] = 'SELECT count(*) FROM Lid WHERE opzoekbaar="A" AND '.WSW4_WHERE_NORMAALLID;
		$st['Q5'] = ' ';
		
		$st['Huidig aantal leden met websitelogin'] = 'SELECT count(*) FROM Lid JOIN Wachtwoord ON Lid.contactID = Wachtwoord.persoon_contactID'
				.' WHERE (wachtwoord IS NOT NULL OR cryptHash IS NOT NULL) AND '.WSW4_WHERE_NORMAALLID;
		$current = colJaar() + 1;
		for($i =0 ; $i<3;$i++)
		{
			$eindJaar = $current."-8-31"; 
			$current -=1;
			$beginJaar = $current."-8-31";
			$st['Waarvan de laatste login in het collegejaar '.$colJaren[$current]] = "SELECT COUNT(*) FROM Lid "
				." RIGHT JOIN Wachtwoord ON Lid.contactID = Wachtwoord.persoon_contactID "
				." WHERE (wachtwoord IS NOT NULL OR cryptHash IS NOT NULL) "
				." AND laatsteLogin < '$eindJaar'" 
				." AND laatsteLogin >= '$beginJaar'"
				." AND ".WSW4_WHERE_NORMAALLID;
				
		}
		$st['Q6'] = ' ';
		
		$table .= self::printloop($st);
		$st = array();
		$current = colJaar();
		for($i =0 ; $i<5;$i++)
			{
				$beginJaar = $current . "-8-31"; // Je moet lid zijn vanaf eerder dan deze dag en tot later dan deze datum
				$st['Aantal leden in '.$colJaren[$current]] = 'SELECT COUNT(*) FROM Lid WHERE '
					." lidToestand != 'BEESTJE' AND lidToestand != 'BIJNALID'" // Zijn geen leden
					." AND lidVan IS NOT NULL AND lidVan < '$beginJaar'" 
					." AND (( lidToestand != 'OUDLID' AND lidTot IS NULL) OR lidTot >= '$beginJaar') "; //Oudleden met lidVan zonder lidTot datum, rekenen we niet mee, dit zijn er 1170, waarvan er twee een lidVan datum van na 2001-09-01 hebben. 
				$current -=1;
			}
		return $table;
		
		}
		
	public static function ActStat($colJaren)
	{	
		$current = colJaar();
		$table = " ";
		for($i =0 ; $i<4;$i++){
			$maand = COLJAARSWITCHMAAND;
			$laatsteDeelname = $current +1 . "-8-31";
			$eersteDeelname = $current ."-8-31";
			$st = array();
			$st['Aantal activiteiten in '.$colJaren[$current]] =
				'SELECT count(DISTINCT activiteitID) FROM Activiteit '
				." WHERE momentBegin < '$laatsteDeelname' AND momentBegin > '$eersteDeelname'";
			$st['Waarvan openbaar'] = $st['Aantal activiteiten in '.$colJaren[$current]]." AND toegang ='PUBLIEK'";	
			$st['Waarvan inschrijfbaar']= $st['Aantal activiteiten in '.$colJaren[$current] ] 
				." AND inschrijfbaar = 1";
			$st['Waarvan niet inschrijfbaar']= $st['Aantal activiteiten in '.$colJaren[$current] ] 
				." AND inschrijfbaar = 0";
			$st['Waarvan een totaal aantal unieke deelnemers'] = 
				  'SELECT count(DISTINCT persoon_contactID) '
				.' FROM Deelnemer JOIN Activiteit ON Deelnemer.activiteit_activiteitID=Activiteit.activiteitID'
				." WHERE momentBegin < '$laatsteDeelname' AND momentEind > '$eersteDeelname' ";
			$st['Waarvan studerend informatica'] = 
				'SELECT count(DISTINCT persoon_contactID) '
				.' FROM Deelnemer JOIN Activiteit ON Deelnemer.activiteit_activiteitID=Activiteit.activiteitID'
				.' JOIN LidStudie ON Deelnemer.persoon_contactID = LidStudie.lid_contactID '
				." WHERE momentBegin < '$laatsteDeelname' AND momentEind > '$eersteDeelname'"
				.' AND studie_studieID = "1"';
			$st['Waarvan studerend informatiekunde'] = 
				'SELECT count(DISTINCT persoon_contactID) '
				.' FROM Deelnemer JOIN Activiteit ON Deelnemer.activiteit_activiteitID=Activiteit.activiteitID'
				.' JOIN LidStudie ON Deelnemer.persoon_contactID = LidStudie.lid_contactID '
				." WHERE momentBegin < '$laatsteDeelname' AND momentEind > '$eersteDeelname'"
				.' AND (studie_studieID = "2")';
			$st['Waarvan studerend informati(ca + ekunde)'] = 
				'SELECT count(DISTINCT persoon_contactID) '
				.' FROM Deelnemer JOIN Activiteit ON Deelnemer.activiteit_activiteitID=Activiteit.activiteitID'
				.' JOIN LidStudie ON Deelnemer.persoon_contactID = LidStudie.lid_contactID '
				." WHERE momentBegin < '$laatsteDeelname' AND momentEind > '$eersteDeelname'"
				.' AND (studie_studieID = "1" OR studie_studieID = "2")';
			$st['Waarvan studerend natuurkunde'] = 
				'SELECT count(DISTINCT persoon_contactID) '
				.' FROM Deelnemer JOIN Activiteit ON Deelnemer.activiteit_activiteitID=Activiteit.activiteitID'
				.' JOIN LidStudie ON Deelnemer.persoon_contactID = LidStudie.lid_contactID '
				." WHERE momentBegin < '$laatsteDeelname' AND momentEind > '$eersteDeelname'"
				.' AND (studie_studieID = "3")';
			$st['Waarvan studerend wiskunde'] = 
				'SELECT count(DISTINCT persoon_contactID) '
				.' FROM Deelnemer JOIN Activiteit ON Deelnemer.activiteit_activiteitID=Activiteit.activiteitID'
				.' JOIN LidStudie ON Deelnemer.persoon_contactID = LidStudie.lid_contactID '
				." WHERE momentBegin < '$laatsteDeelname' AND momentEind > '$eersteDeelname'"
				.' AND (studie_studieID = "4")';
				
			$current -=1;

			$table .= self::printloop($st);
		}
		$st = array();
		$st['Q'] = ' ';
		$table .= self::printloop($st);
		     
		return $table;
	}
	public static function VerzendStat($colJaren)
	{
		$schuif = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";//Geeft de horizontale verplaatsing wat voor het overzicht super awesome is
		
		$table = new HtmlTable(null);
		$table->add($tbody = new HtmlTableBody());
		      
		$mailLijst = MailingListVerzameling::beschikbaar();
		$tbody->add($row = new HtmlTableRow());
		$row->addData(new HtmlSpan('Aantal zielen per mailinglijst:'));
		foreach($mailLijst as $lijst)
		{
			$naam = $lijst->getNaam();
			$tbody->add($row = new HtmlTableRow());
			$row->addData(new HtmlSpan($schuif . $naam.': '.count($lijst->abonneeLidnrs())));
		}
		$st['Aantal leden waarvan de afgelopen maanden een mail is gebounced'] = 'SELECT count(DISTINCT contactID) FROM Lid JOIN MailingBounce'
							      .' ON Lid.contactID = MailingBounce.contact_contactID '
							      .' WHERE'.WSW4_WHERE_NORMAALLID;
		$st['Q1'] = ' ';
		$st['Aantal leden met VakIdioot-abonnement'] = 'SELECT count(*) FROM Contact JOIN Lid USING (contactID)'
									.' WHERE vakidOpsturen = 1 AND '.WSW4_WHERE_LID;
		//Ook de SpoCie kan deze pagina bekijken.
		if(hasAuth('bestuur'))
			$st['Aantal donateur met VakIdioot-abonnement'] = 'SELECT count(*) FROM Donateur WHERE vakid = 1';
		$st['Aantal overigen met VakIdioot-abonnement'] = 'SELECT count(*) FROM Contact WHERE vakidOpsturen = 1'
								 .' AND (overerving = "Organisatie" OR overerving = "Bedrijf")';
		$st['Aantal leden met aes2Roots-abonnement'] = 'SELECT count(*) FROM Contact JOIN Lid USING (contactID)'
									.' WHERE aes2rootsOpsturen = 1 AND '.WSW4_WHERE_LID;
		$table .= self::printloop($st);
		return $table;
	}
	
	public static function VerStat($colJaren)
	{
		$st['Aantal leden van 17 of jonger'] = 'SELECT count(*) FROM Persoon JOIN Lid USING (`contactID`) WHERE datumGeboorte IS NOT NULL'
							.' AND datumGeboorte >= DATE_ADD( NOW(), INTERVAL -18 YEAR) '
							.' AND '.WSW4_WHERE_NORMAALLID;
		for($i = 18; $i <26;$i++)
		{
			$j = $i +1;
			$st['Aantal leden met leeftijd '.$i] = ' SELECT count(*) FROM Persoon JOIN Lid USING (`contactID`) '
								.' WHERE datumGeboorte IS NOT NULL'
								.' AND datumGeboorte > DATE_ADD( NOW(), INTERVAL -'. $j .' YEAR) '
								.' AND datumGeboorte <= DATE_ADD(NOW(), INTERVAL -'. $i.' YEAR)'
								.' AND '.WSW4_WHERE_NORMAALLID;
		}
		$st['Aantal leden van 26 of ouder'] = 'SELECT count(*) FROM Persoon JOIN Lid USING (`contactID`) WHERE datumGeboorte IS NOT NULL'
							.' AND datumGeboorte < DATE_ADD( NOW(), INTERVAL -26 YEAR) '
							.' AND '.WSW4_WHERE_NORMAALLID;
		$st['Aantal leden met onbekende geboortedatum'] = 'SELECT count(*) FROM Persoon JOIN Lid USING (`contactID`) WHERE datumGeboorte IS NULL'
									.' AND '.WSW4_WHERE_NORMAALLID;
		$st['Q2'] = ' ';
		$st['Aantal actieve commisies'] = 'SELECT count(commissieID)  FROM Commissie WHERE soort = "CIE" '
							.'AND ( datumEind IS NULL OR datumEind >= NOW())';
		$st['Aantal actieve groepen'] = 'SELECT count(commissieID)  FROM Commissie WHERE soort = "GROEP" '
							.'AND ( datumEind IS NULL OR datumEind >= NOW())';
		$st['Aantal actieve dispuut'] = 'SELECT count(commissieID)  FROM Commissie WHERE soort = "DISPUUT" '
							.'AND ( datumEind IS NULL OR datumEind >= NOW())';
		if(hasAuth('bestuur'))
			$st['Aantal donateurs'] = 'SELECT count(persoon_contactID) FROM Donateur WHERE (jaarEind IS NULL OR jaarEind >= NOW())';
		$st['Q2'] = ' ';
		$st['Het jongste lid is geboren op'] = 'SELECT datumGeboorte FROM Persoon JOIN Lid USING (`contactID`) WHERE '.WSW4_WHERE_NORMAALLID
						.' ORDER BY datumGeboorte DESC LIMIT 1';						
		$st['Het oudste lid is geboren op'] = 'SELECT datumGeboorte FROM Persoon JOIN Lid USING (`contactID`) WHERE '.WSW4_WHERE_NORMAALLID
						.' AND datumGeboorte IS NOT NULL ORDER BY datumGeboorte ASC LIMIT 1';
		
		$st["Aantal foto's in fotoweb2"] = 'SELECT count(mediaID) FROM Media WHERE soort = "FOTO"';
		$st["Aantal filmpjes in fotoweb2"] = 'SELECT count(mediaID) FROM Media WHERE soort = "FILM"';
		$st['Aantal collecties in fotoweb2'] = 'SELECT count(collectieID) FROM Collectie';
		
		$table = self::printloop($st);
		return $table;
	}
}
