<?php

abstract class FotoWeb_Controller
{
	/**
	 *  Geeft de fotoweb-home pagina terug
	 *
	 * @site{/FotoWeb/index.html}
	 */
	static public function home()
	{
		// Haal de hoeveelheid fotos op zodat we dat kunnen laten zien
		$fotocount = MediaVerzameling::telAlle();
		$untagged = MediaVerzameling::getTotaalAantalUntagged();
		$notlocked = MediaVerzameling::getTotaalAantalNotlocked();

		$page = Page::getInstance()->start('FotoWeb');

		$page->add($div = new HtmlDiv());

		// De welkomsttekst van de pagina
		$div->add(new HtmlDiv(new HtmlDiv(
			_("FotoWeb is het digitale fotoarchief van A-Eskwadraat. Het is"
			. " in staat om een rating van foto's op te slaan te geven, een "
			. "fotograaf te specificeren en om foto's te koppelen aan "
			. "activiteiten, personen en collecties. Op dit moment zijn er "
			. $fotocount . " foto's te zien! Er zijn nog "
			. new HtmlAnchor('/FotoWeb/Untagged',$untagged)
			. " foto's waar (nog) geen mensen op getagd zijn en er zijn nog "
			. new HtmlAnchor('/FotoWeb/Notlocked',$notlocked)
			. " foto's waar (nog) geen ViCie taglock op is gezet."), 'panel-body'), 'panel panel-default'))
			->add(new HtmlDiv(
				_("Vanaf deze pagina kun je verschillende onderdelen van "
				. "FotoWeb gebruiken. Heb je vragen, suggesties of opmerkingen?"
				. " Mail de WebCie!"), 'bs-callout bs-callout-info'))
				->add(new HtmlHeader(3,_('Laatste 3 activiteiten met foto\'s')));

		$acts = ActiviteitVerzameling::geefLaatsteMetFoto(3);

		$div->add(ActiviteitVerzamelingView::overzicht($acts));

		$div->add(new HtmlHeader(3,_("Personen zoeken")));

		// Als er mensen in de kart zitten, zeg dan dat ze de foto's daarvan
		// kunnen bekijken
		$kart = Kart::geef();
		if($kart->getPersonen()->aantal() == 0) {
			$div->add(new HtmlSpan(
				sprintf(_("Op dit moment zitten er geen leden in je %s. "
				. "Je zult er een paar in moeten shoppen om foto's "
				. "te zoeken.")
				, new HtmlAnchor('/Service/Kart','kart'))));
		} elseif($kart->getPersonen()->aantal() == 1) {
			$div->add(new HtmlSpan(
				sprintf(_("Op dit moment zit er één lid in je %s. Klik %s "
				. "om de foto's van deze persoon te bekijken")
				, new HtmlAnchor('/Service/Kart','kart')
				, new HtmlAnchor($kart->getPersonen()->first()->fotoUrl()
				, 'hier'))));
		} else {
			$div->add(new HtmlSpan(
				sprintf(_("Op dit moment zitten er %s leden in je %s. Er zijn "
				. "%s foto's gevonden waar ze allemaal op staan!")
				, $kart->getPersonen()->aantal()
				, new HtmlAnchor('/Service/Kart','kart')
				, new HtmlAnchor('/FotoWeb/Zoeken?showKartPersonen=true'
				, count(MediaVerzameling::geefSelectieKartPersonen())))));
		}

		// De informatie over de collecties
		$div->add(new HtmlHeader(3,_('Collecties')))
			->add(new HtmlSpan(
				sprintf(_("Klik %s om naar het collectie-overzicht te gaan")
				, new HtmlAnchor("/FotoWeb/Collecties",'hier'))))
				->add(new HtmlHeader(3,_("Statistieken")));

		// Als de persoon ingelogd is kan die de statistieken over de leden
		// in fotoweb bekijken
		if(Persoon::getIngelogd()) {
			$div->add($div2 = new HtmlDiv(null, 'row'));

			// Haal de top-leden voor de statistieken op van de laatste
			// twee jaar
			$getagde = TagVerzameling::getTopGetagde(10,2);
			$taggers = TagVerzameling::getTopTaggers(10,2);
			$fotografen = MediaVerzameling::getTopFotografen(10,2);

			$div2->add($row = new HtmlDiv(null, 'col-md-4'));
			$row->add($h = new HtmlHeader(4, null));
			$h->add(_("Top-taggers"));
			$h->add(new HtmlSmall(_("tags geplaatst in de laatste twee jaar")));


			$row->add($tagtable = new HtmlTable());
			$tagtable->setAttribute('align','center');

			$i = 0;
			foreach($taggers as $t) {
				if(!$t[0])
					continue;
				$i++;
				$persoon = Persoon::geef($t[0]);
				$tagtable->add(new HtmlTableRow(
					array(new HtmlTableDataCell($i . ". ")
					, new HtmlTableDataCell(
						new HtmlAnchor($persoon->fotoUrl()
						, PersoonView::naam($persoon)))
						, new HtmlTableDataCell($t[1]))));
			}

			$div2->add($row = new HtmlDiv(null, 'col-md-4'));
			$row->add($h = new HtmlHeader(4, null));
			$h->add(_("Meest getagd"));
			$h->add(new HtmlSmall(_("op foto's van de laatste twee jaar")));

			$row->add($tagtable = new HtmlTable());
			$tagtable->setAttribute('align','center');

			$i = 0;
			foreach($getagde as $t) {
				if(!$t[0])
					continue;
				$i++;
				$tagtable->add(new HtmlTableRow(
					array(new HtmlTableDataCell($i . ". ")
					, new HtmlTableDataCell(
						PersoonView::makeLink(Persoon::geef($t[0])))
						, new HtmlTableDataCell($t[1]))));
			}

			$div2->add($row = new HtmlDiv(null, 'col-md-4'));
			$row->add($h = new HtmlHeader(4, null));
			$h->add(_("Top-fotografen"));
			$h->add(new HtmlSmall(_("van foto's van de laatste twee jaar")));

			$row->add($tagtable = new HtmlTable());
			$tagtable->setAttribute('align','center');

			$i = 0;
			foreach($fotografen as $t) {
				if(!$t[0])
					continue;
				$i++;
				$persoon = Persoon::geef($t[0]);
				$tagtable->add(new HtmlTableRow(
					array(new HtmlTableDataCell($i . ". ")
					, new HtmlTableDataCell(
						new HtmlAnchor($persoon->fotograafUrl()
						, PersoonView::naam($persoon)))
						, new HtmlTableDataCell($t[1]))));
			}

			$div->add($div2 = new HtmlDiv(null, 'row'));

			// Haal de top-leden op
			$getagde = TagVerzameling::getTopGetagde(5);
			$taggers = TagVerzameling::getTopTaggers(5);
			$fotografen = MediaVerzameling::getTopFotografen(5);

			$div2->add($row = new HtmlDiv(null, 'col-md-4'));
			$row->add($h = new HtmlHeader(4, null));
			$h->add(_("Top-taggers"));
			$h->add(new HtmlSmall(_("aller tijden")));

			$row->add($tagtable = new HtmlTable());
			$tagtable->setAttribute('align','center');

			$i = 0;
			foreach($taggers as $t) {
				if(!$t[0])
					continue;
				$i++;
				$persoon = Persoon::geef($t[0]);
				$tagtable->add(new HtmlTableRow(
					array(new HtmlTableDataCell($i . ". ")
					, new HtmlTableDataCell(
						new HtmlAnchor($persoon->fotoUrl()
						, PersoonView::naam($persoon)))
						, new HtmlTableDataCell($t[1]))));
			}

			$div2->add($row = new HtmlDiv(null, 'col-md-4'));
			$row->add($h = new HtmlHeader(4, null));
			$h->add(_("Meest getagd"));
			$h->add(new HtmlSmall(_("aller tijden")));

			$row->add($tagtable = new HtmlTable());
			$tagtable->setAttribute('align','center');

			$i = 0;
			foreach($getagde as $t) {
				if(!$t[0])
					continue;
				$i++;
				$tagtable->add(new HtmlTableRow(
					array(new HtmlTableDataCell($i . ". ")
					, new HtmlTableDataCell(
						PersoonView::makeLink(Persoon::geef($t[0])))
						, new HtmlTableDataCell($t[1]))));
			}

			$div2->add($row = new HtmlDiv(null, 'col-md-4'));
			$row->add($h = new HtmlHeader(4, null));
			$h->add(_("Top-fotografen"));
			$h->add(new HtmlSmall(_("aller tijden")));

			$row->add($tagtable = new HtmlTable());
			$tagtable->setAttribute('align','center');

			$i = 0;
			foreach($fotografen as $t) {
				if(!$t[0])
					continue;
				$i++;
				$persoon = Persoon::geef($t[0]);
				$tagtable->add(new HtmlTableRow(
					array(new HtmlTableDataCell($i . ". ")
					, new HtmlTableDataCell(
						new HtmlAnchor($persoon->fotograafUrl()
						, PersoonView::naam($persoon)))
						, new HtmlTableDataCell($t[1]))));
			}
		} else {
			$div->add(new HtmlDiv(_("Je moet ingelogd zijn om de statistieken "
				. "te kunnen bekijken."), 'bs-callout bs-callout-danger'));
		}
		$page->end();
	}

	/**
	 *  DE ajax-pagina voor fotoweb!
	 *
	 * @site{/FotoWeb/Ajax}
	 */
	static public function ajaxFotoweb() {
		if(!hasAuth('ingelogd')) {
			spaceHttp(403);
		}

		// Start de pagina
		$page = Page::getInstance('cleanhtml')->start();

		// Haal het ding op wat we willen doen
		$elem = tryPar('ajaxElem',null);
		switch($elem) {
		case 'mentorgroepjaar':
			//We willen alle mentorgroepen van een bepaald jaar terug
			$jaar = tryPar('jaar');
			$groepen = IntroGroepVerzameling::groepenVanJaar($jaar);
			foreach($groepen as $g) {
				$page->add("<option value=".$g->getGroepID().">".$g->getNaam()."</option>");
			}
			break;
		case 'foto':
			// We gaan dingen met een enkele foto doen
			$id = tryPar('id',null);
			if(!($foto = Media::geef($id)))
				return false;
			if(!$foto->magBekijken())
				return false;
			if(tryPar('editFormSubmit',false) == true) {
				if(!$foto->magWijzigen())
					break;
				$foto->setFotograaf(Persoon::geef(tryPar('fotograaf',null)));
				$foto->setOmschrijving(tryPar('omschrijving',null));
				$foto->setGemaakt(tryPar('datetime') . " " . tryPar('Timedatetime'));
				$foto->opslaan();
				break;
			}
			//We willen een foto-overzicht terug
			if(tryPar('rate',null)) {
				$ingelogd = Persoon::getIngelogd();
				//Heeft de user niet al een keertje de foto geratet?
				if(!($rating = Rating::geef($id,$ingelogd->getContactID()))) {
					//We geven een rating aan de foto
					$rating = new Rating($foto,$ingelogd);
					$rating->setRating(tryPar('value'));
					$rating->opslaan();
					$foto->updateRating();
					$foto->opslaan();
				} else {
					// Anders passen we de huidige rating gaan
					$rating->setRating(tryPar('value'));
					$rating->opslaan();
					$foto->updateRating();
					$foto->opslaan();
				}
			}
			if(tryPar('toggleLock', null)) {
				if(hasAuth('vicie')) {
					$foto->setLock(!($foto->getLock()));
					$foto->opslaan();
				}
			}
			if($foto->getLock() == 0) {
				//Als de foto niet gelockt is...
				if($deltag = tryPar('delTag',null)) {
					//Haal een tag weg
					if($tag = Tag::geef($deltag)) {
						if($tagarea = $tag->getTagArea())
							$tagarea->verwijderen();
						$tag->verwijderen();
					}
				}
				if(tryPar('setTag',null)) {
					//Zet een tag neer van een persoon...
					if($personenarray = tryPar('persoon',null)) {
						$personen = array();
						foreach($personenarray as $persoon) {
							if($persoon = Persoon::geef($persoon)) {
								$tag = new Tag();
								$tag->setMedia($foto);
								$tag->setPersoon($persoon);
								$tag->setTagger(Persoon::getIngelogd());
								if(!$foto->getTagPersoon($persoon))
									$tag->opslaan();
							}
						}
					}
					//...commissie...
					if($cie = tryPar('commissie',null)) {
						if($cie = Commissie::geef($cie)) {
							$tag = new Tag();
							$tag->setMedia($foto);
							$tag->setCommissie($cie);
							$tag->setTagger(Persoon::getIngelogd());
							if(!$foto->getTagCommissie($cie))
								$tag->opslaan();
						}
					}
					//...of mentorgroep
					if($mg = tryPar('mentorgroep',null)) {
						if($mg = IntroGroep::geef($mg)) {
							$tag = new Tag();
							$tag->setMedia($foto);
							$tag->setIntroGroep($mg);
							$tag->setTagger(Persoon::getIngelogd());
							if(!$foto->getTagMentorGroep($mg))
								$tag->opslaan();
						}
					}
				}
				if($setCollectie = tryPar('setCollectie',null)) {
					//We stoppen de foto in een collectie
					$collectie = Collectie::geef($setCollectie);
					if($collectie && !$foto->getTagCollectie($collectie)) {
						$tag = new Tag();
						$tag->setMedia($foto);
						$tag->setCollectie($collectie);
						$tag->setTagger(Persoon::getIngelogd());
						$tag->opslaan();
					}
				}
				if($delcollectie = tryPar('delCollectie',null)) {
					//We halen de foto uit een collectie
					if($collectie = $foto->getTagCollectie(Collectie::geef($delcollectie))) {
						$collectie->verwijderen();
					}
				}
			}
			//Geef het foto-overzicht terug
			$page->add(MediaView::maakFotoDiv($foto));
			break;
		}
		$page->end();
	}

	/**
	 *  Geeft de pagina met alle activiteiten die foto's hebben terug
	 *		  van een gegeven jaar
	 *
	 * @site{/Activiteiten/Fotos}
	 */
	public static function activiteiten() {
		$jaar = tryPar('jaar',null);
		if($jaar) {
			$acts = ActiviteitVerzameling::geefVanJaarMetFoto($jaar);
		} else {
			$acts = ActiviteitVerzameling::geefLaatsteMetFoto(8);
		}
		ActiviteitVerzamelingView::overzichtMetPage($acts);
	}

	/**
	 *  Standaard die een overzichtspagina geeft afhankelijk van de selecties
	 *
	 * @param titel De titel van de pagina
	 * @param type Het type object wat meegegeven is als string
	 * @param object Het eventuele object om de fotos bij te geven (blijft leeg bij notlocked en untagged)
	 */
	static public function paginaControllerFunctie($titel, $type, $object = null)
	{
		if($id = tryPar('delFoto', null)) {
			$media = Media::geef($id);
			if($media && $media->magWijzigen()) {
				$tags = $media->getTags();
				$ratings = $media->getAllRatings();
				$tags->verwijderen();
				$ratings->verwijderen();
				$media->verwijderVanFilesystem();
				$media->verwijderen();
				Page::redirectMelding('Fotos', _("Gelukt!"));
			} else {
				$page = Page::getInstance()->start(_("Fout"));
				$page->add(new HtmlSpan(sprintf(_("De foto bij het id %d bestaat niet!"), $id), 'text-danger strongtext'));
				$page->end();
			}
		}

		if($id = tryPar('rotateLFoto', null)) {
			$media = Media::geef($id);
			if($media && $media->magWijzigen()) {
				$media->rotate(90);
				Page::redirectMelding('Fotos', _("Gelukt!"));
			} else {
				$page = Page::getInstance()->start(_("Fout"));
				$page->add(new HtmlSpan(sprintf(_("De foto bij het id %d bestaat niet!"), $id), 'text-danger strongtext'));
				$page->end();
			}
		}

		if($id = tryPar('rotateRFoto', null)) {
			$media = Media::geef($id);
			if($media && $media->magWijzigen()) {
				$media->rotate(270);
				Page::redirectMelding('Fotos', _("Gelukt!"));
			} else {
				$page = Page::getInstance()->start(_("Fout"));
				$page->add(new HtmlSpan(sprintf(_("De foto bij het id %d bestaat niet!"), $id), 'text-danger strongtext'));
				$page->end();
			}
		}

		$fotos = MediaVerzameling::geefFotos($type, $object);

		// Als er geen foto's zijn, dan is dat het enige wat de user te
		// zien krijgt
		if(sizeof($fotos) == 0) {
			$page = Page::getInstance()->start(_("Geen foto's"));
			$page->add(new HtmlSpan(_("Er zijn geen foto's gevonden")));
			$page->end();
		} else {
			// Roep maakPagina aan die de pagina voor ons gaat maken
			MediaVerzamelingView::maakPagina($fotos, $titel);
		}
	}

	/**
	 *  De fotoentry-functie
	 *
	 * @site{/FotoWeb/Media/ *}
	 */
	static public function fotoEntry($args) {
		if(!Persoon::getIngelogd())
			return false;

		$id = Media::geef($args[0]);
		if($id)
			return array('name' => $args[0], 'displayName' => 'Foto', 'access' => true);
		else
			return array('name' => '','displayName' => '', 'access' => false);
	}

	/**
	 *  De fotosizeentry-functie
	 *
	 * @site{/FotoWeb/Media/ * / *}
	 */
	static public function fotoSizeEntry($args) {
		if(!Persoon::getIngelogd())
			return false;

		$size = strtolower($args[0]);
		$allowed = array('', 'groot', 'origineel', 'medium', 'thumbnail');

		if(in_array($size, $allowed)) {
			$id = $args[1];
			// FIXME: Als het bestand geen JPG is, is dit natuurlijk nogal onhandig voor de gebruiker!
			$filename = $id . "_" . $size . ".jpg";
			header("Content-Disposition: inline; filename=\"" . $filename . "\"");

			return array('name' => $args[0], 'displayName' => 'Foto', 'access' => true);
		} else {
			// FIXME: Ondanks de falseness van `access` krijg je nu toch de foto te zien?
			// Ook `return false` werkt niet, dus het zal wel onzin van Benamite zijn.
			return array('name' => '','displayName' => '', 'access' => false);
		}
	}

	/**
	 *  Geeft de pagina voor alle fotos van personen in je kart
	 *
	 * @site{/FotoWeb/Zoeken}
	 */
	static public function zoeken() {
		if(!tryPar('showKartPersonen',null))
			return false;
		self::paginaControllerFunctie("Foto's van de personen in je kart");
	}

	/**
	 *  Geeft de pagina van foto's van een activiteit
	 *
	 * @site{/Activiteiten/ * / * / * /Fotos}
	 */
	static public function activiteit() {
		$ids = vfsVarEntryNames();
		if(!($act = Activiteit::geef($ids[1])))
			return false;
		self::paginaControllerFunctie(ActiviteitView::makeLink($act), 'activiteit', $act);
	}

	/**
	 *  Geeft de pagina voor alle niet-getagde fotos
	 *
	 * @site{/FotoWeb/Untagged}
	 */
	static public function untagged() {
		self::paginaControllerFunctie('Untagged', 'untagged');
	}

	/**
	 *  Geeft de pagina met alle niet-gelockte fotos
	 *
	 * @site{/FotoWeb/Notlocked}
	 */
	static public function notlocked() {
		self::paginaControllerFunctie('Notlocked', 'notlocked');
	}

	/**
	 *  Geeft de pagina voor alle foto's bij een fotograaf
	 */
	static public function fotograaf() {
		$id = vfsVarEntryName();
		if(!($fotograaf = Persoon::geef($id)))
			return false;
		self::paginaControllerFunctie(sprintf(_('Fotograaf %s'), PersoonView::makeLink($fotograaf)), 'fotograaf', $fotograaf);
	}

	/**
	 *  De fotograafentry-functie
	 *
	 * @site{/FotoWeb/Fotografen/ * /index.html}
	 */
	static public function fotograafEntry($args) {
		$fotograaf = Persoon::geef($args[0]);
		if($fotograaf)
			return array('name' => $args[0], 'displayName' => PersoonView::naam($fotograaf), 'access' => true);
		else
			return array('name' => '','displayName' => '', 'access' => false);
	}

	/**
	 *  Geeft de pagina van foto's bij een commissie
	 *
	 * @site{/Vereniging/Commissies/ * /Fotos}
	 */
	static public function commissie() {
		$id = vfsVarEntryName();
		if(!($cie = Commissie::cieByLogin($id)))
			return false;
		self::paginaControllerFunctie(CommissieView::makeLink($cie), 'commissie', $cie);
	}

	/**
	 *  Geeft de foto-pagina bij een persoon
	 *
	 * @site{/Leden/ * /Fotos}
	 */
	static public function persoon() {
		$id = vfsVarEntryName();
		if(!($persoon = Persoon::geef($id)))
			return false;
		self::paginaControllerFunctie(PersoonView::makeLink($persoon), 'Lid', $persoon);
	}

	/**
	 *  Geeft de foto-pagina bij een mentorgroep
	 *
	 * @site{/Leden/Intro/Groep/ * /Fotos}
	 */
	static public function mentorgroep() {
		$id = vfsVarEntryName();
		if(!($grp = IntroGroep::geef($id)))
			return false;
		self::paginaControllerFunctie('Mentorgroep', 'mentorgroep', $grp);
	}

	/**
	 *  Geeft de pagina met alle collecties erop
	 *
	 * @site{/FotoWeb/Collecties/index.html}
	 */
	static public function collecties() {
		CollectieVerzamelingView::collectieLijst();
	}

	/**
	 *  De collectie-entry-functie
	 *
	 * @site{/FotoWeb/Collecties/ *}
	 */
	static public function collectieEntry($args) {
		$collectie = Collectie::geef($args[0]);
		if($collectie)
			return array('name' => $args[0], 'displayName' => CollectieView::waardeNaam($collectie), 'access' => true);
		else
			return array('name' => '','displayName' => '', 'access' => false);
	}

	/**
	 *  Geeft de info-pagina van een collectie
	 *
	 * @site{/FotoWeb/Collecties/ * /index.html}
	 */
	static public function collectieInfo() {
		$id = vfsVarEntryName();
		if(!($collectie = Collectie::geef($id)))
			return false;
		CollectieView::collectieInfo($collectie);
	}

	/**
	 *  Geeft de foto-pagina bij een collectie
	 *
	 * @site{/FotoWeb/Collecties/ * /Fotos}
	 */
	static public function collectieFotos() {
		$id = vfsVarEntryName();
		if(!($collectie = Collectie::geef($id)))
			return false;
		self::paginaControllerFunctie(_('Collectie ').CollectieView::waardeNaam($collectie), 'collectie', $collectie);
	}

	/**
	 *  Verwijdert een collectie
	 *
	 * @site{/FotoWeb/Collecties/ * /Verwijder}
	 */
	static public function collectieVerwijderen() {
		$id = vfsVarEntryName();
		if(!($collectie = Collectie::geef($id)))
			return false;
		if(Token::processNamedForm() == "CollectieVerwijderen") {
			$returnVal = $collectie->verwijderen();
			if(!is_null($returnVal)) {
				Page::addMeldingArray($returnVal, 'fout');
			} else {
				Page::redirectMelding('/FotoWeb/Collecties','Collectie met succes verwijderd');
			}
		}
		$page = Page::getInstance()->start();
		$page->add(CollectieView::verwijderForm($collectie));
		$page->end();
	}

	/**
	 *  Pagina voor het toevoegen van een collectie
	 *
	 * @site{/FotoWeb/Collecties/ * /Toevoegen}
	 */
	static public function collectieToevoegen()
	{
		$collectie = new Collectie();

		$show_error = false;

		if(Token::processNamedForm() == 'toevoegen')
		{
			CollectieView::processNieuwForm($collectie);

			if($collectie->valid())
			{
				$collectie->opslaan();
				Page::redirectMelding($collectie->url(),_('Collectie met succes aangemaakt'));
			}
			else
			{
				Page::addMelding(_('Er waren fouten'), 'fout');
				$show_error = true;
			}
		}

		CollectieView::collectieWijzigen($collectie, true, $show_error);
	}

	/**
	 *  Pagina voor het wijzigen van een collectie
	 *
	 * @site{/FotoWeb/Collecties/ * /Wijzig}
	 */
	static public function collectieWijzigen()
	{
		$id = vfsVarEntryName();

		if(!($collectie = Collectie::geef($id)))
			return false;

		$show_error = false;

		if(Token::processNamedForm() == 'wijzigen')
		{
			CollectieView::processNieuwForm($collectie);

			if($collectie->valid())
			{
				$collectie->opslaan();
				Page::redirectMelding($collectie->url(),_('Collectie met succes gewijzigd'));
			}
			else
			{
				Page::addMelding(_('Er waren fouten'), 'fout');
				$show_error = true;
			}
		}

		CollectieView::collectieWijzigen($collectie, false, $show_error);
	}

	/**
	 *  Geeft de pagina voor het uploaden van fotos bij een activiteit
	 *
	 * @site{/Activiteiten/ * / * / * /FotosUploaden}
	 */
	static public function activiteitNieuw() {
		$actnr = VfsVarEntryNames();
		$act = Activiteit::geef($actnr[1]);
		if(!$act)
			return false;
		self::fotosUploaden($act,null);
	}

	/**
	 *  Geeft de pagina voor het uploaden van fotos bij een cie
	 *
	 * @site{/Vereniging/Commissies/ * /FotosUploaden}
	 */
	static public function commissieNieuw() {
		$cieinlog = VfsVarEntryName();
		$cie = Commissie::cieByLogin($cieinlog);
		if(!$cie)
			return false;
		self::fotosUploaden(null,$cie);
	}

	/**
	 *  De functie voor het uploaden van fotos
	 *
	 * @param act De activiteit waar fotos bij moeten worden geüpload
	 * @param cie De commissie waar fotos bij moeten worden geüpload
	 */
	static public function fotosUploaden($act = null, $cie = null)
	{
		requireAuth('lid');

		if($act)
			$head_cieact = sprintf(_("Activiteit: %s"), $act->getTitel());
		elseif($cie)
			$head_cieact = sprintf(_("Commissie: %s"), $cie->getNaam());

		// begin met wat standaardpaginaonderdelen
		$page = Page::getInstance();
		$page->addHeadCss(Page::minifiedFile('dropzone.css', 'css'));
		$page->addFooterJS(Page::minifiedFile('dropzone.js', 'js'));
		$page->addFooterJS(Page::minifiedFile('fotowebUploaden.js', 'js'));

		// produceer een waarschuwing want je wilt eigenlijk niet foto's direct bij een cie online zetten
		if ($cie && !tryPar("reallycie", false)){
			$page->addMelding(sprintf('%s %s'
				, new HtmlStrong(_('Let op!'))
				, _('Je staat op het punt om foto\'s bij een '
				. 'commissie online te zetten. Het is te doen gebruikelijk '
				. 'foto\'s van activiteiten niet bij de commissie online te '
				. 'zetten, maar via de activiteitpagina. '
				. 'Ga alleen verder als je zeker weet dat foto\'s bij de '
				. 'commissie thuishoren, en niet bij een activiteit van deze '
				. 'commissie.')));
		}

		$page->start(sprintf(_("Foto's en video's uploaden (%s)"), $head_cieact));

		if(!is_null($act)) {
			$page->add(new HtmlParagraph(HtmlAnchor::button($act->url() . '/Fotos', _('Bekijk de geüploade foto\'s'))));
		}

		$page->add(MediaView::creativeCommons());

		$page->add($form = new HtmlForm());
		$form->add(MediaView::makeUploadForm($act, $cie));

		$form->add(HtmlInput::makeHidden('act', !is_null($act) ? $act->geefID() : null));
		$form->add(HtmlInput::makeHidden('cie', !is_null($cie) ? $cie->geefID() : null));
		$form->add(new HtmlDiv(HtmlInput::makeFormFile('file'), 'fallback'));
		$form->add(new HtmlDiv(null, 'upload-form col-sm-10 col-sm-offset-2'));

		$form->add(HtmlInput::makeFormButton('uploaden', _('Uploaden')));

		$page->end();
		return;
	}

	/**
	 * @site{/FotoWeb/Uploaden}
	 */
	static public function uploaden()
	{
		$act = tryPar('act', null);
		$cie = tryPar('cie', null);

		$fouten = array();

		if(is_null($act) && is_null($cie))
			$fouten[] = _('Stuur geldige act of cie mee');

		if(!($act = Activiteit::geef($act)) && !($cie = Commissie::geef($cie)))
			$fouten[] = _('Stuur geldige act of cie mee');

		if(count($fouten) != 0)
		{
			header("HTTP/1.1 500");
			echo implode("\n", $fouten);
			exit();
		}

		$mediafile = Media::uploaden($_FILES['file'], false, $act);

		if(is_array($mediafile) && count($mediafile) != 0)
		{
			header("HTTP/1.1 500");
			echo implode("\n", $fouten);
			exit();
		}

		$fotograaflidnr = tryPar('fotograaf');
		$collectiontagid = tryPar('collectie');
		if (is_numeric($fotograaflidnr)) {
			$mediafile->setFotograaf($fotograaflidnr);
			$mediafile->opslaan();
		}

		if($collectiontagid)
		{
			$tag = new Tag();
			$tag->setMedia($mediafile);
			$tag->setCollectie(Collectie::geef($collectiontagid));
			$tag->setTagger(Persoon::getIngelogd());
			$tag->opslaan();
		}

		if($cie)
		{
			$cietag = new Tag();
			$cietag->setMedia($mediafile);
			$cietag->setCommissie($cie);
			$cietag->setTagger(Persoon::getIngelogd());
			$cietag->opslaan();
		}
		exit();
	}

	/**
	 * @site{/FotoWeb/Fotografen/index.html}
	 */
	static public function fotografen() {
		$page = Page::getInstance()->start('Fotografen');
		$page->add(new HtmlHeader(2,_('Fotografen')));
		$page->add(new HtmlSpan(_("Hieronder vind je een lijst van alle fotografen die in fotoweb staan.")));
		$fotografen = MediaVerzameling::getAllFotografen();
		$table = new HtmlTable();
		$table->add(new HtmlTableRow(array(new HtmlTableHeaderCell(_('Fotograaf')),new HtmlTableHeaderCell(_("Aantal gemaakte foto's")))));
		foreach($fotografen as $f) {
			$table->add($row = new HtmlTableRow());
			if(is_null($f[0]))
				continue;
			$persoon = Persoon::geef($f['fotograaf_contactID']);
			$row->add(new HtmlTableDataCell(new HtmlAnchor($persoon->fotograafUrl(),PersoonView::naam($persoon))));
			$row->add(new HtmlTableDataCell($f['counted']));
		}
		$page->add($table);
		$page->end();
	}

	/**
	 * Maakt de pagina waarmee je fotos naar scratch kan exporteren
	 *
	 * @site{/FotoWeb/Exporteren}
	 */
	static public function exportInterface(){
		requireAuth("ingelogd");
		$jsonmediaids = tryPar('mediaids',null);
		if(!$jsonmediaids) {
			return responseUitStatusCode(400);
		}
		$mediaids = json_decode($jsonmediaids);

		$msg = null;

		if(Token::processNamedForm() == 'exporteren') {
			$location = htmlspecialchars(tryPar("location", null));
			$fotograafsubdirs = htmlspecialchars(tryPar("fotograafsubdirs", false));

			$errors = array();
			$warnings = array();
			if (strpos($location,"/scratch/") !== 0) {
				// Directory is geen subdir van /scratch
				$errors[] = "&quot;$location&quot; is geen subdirectory onder /scratch!";
			} elseif (!is_dir($location)){
				$errors[] = "&quot;$location&quot; is geen directory!";
			} elseif (!is_writable($location)){
				$errors[] = "&quot;$location&quot; is niet schrijfbaar voor de webserver. Kies een locatie
					die wel schrijfbaar is (maak bijvoorbeeld een directory op <em>scratch</em> en maak deze
					schrijfbaar voor alle gebruikers met het volgende commando: <em>chmod a+w /scratch/mijndir</em>)";
			} else {
				// ok: exporteren maar!
				$fotos = MediaVerzameling::verzamel($mediaids);

				// Namen van fotografen uitzoeken?
				if ($fotograafsubdirs){
					$lidnrs = array();
					foreach ($fotos as $foto){
						$lidnrs[] = $foto->getFotograafContactID();
					}
					$lidNamen = array();
					foreach(PersoonVerzameling::verzamel($lidnrs) as $p) {
						$lidNamen[$p->getContactID()] = PersoonView::naam($p);
					}
				}

				$exportCounter = 0;
				foreach ($fotos as $foto){
					$mediaid = $foto->getMediaId();
					$src = FILESYSTEM_PREFIX . Media::geefAbsLoc("Origineel", $mediaid);
					$datum = $foto->getGemaakt();


					$dstfilename = strftime("%Y-%m-%d_%Hu%Mm%Ss.jpg", strtotime($datum->format("Y-m-d H:i:s")));
					if ($fotograafsubdirs){
						$fotograaf = $foto->getFotograaf();
						if (!$fotograaf){
							$fotograafNaam = "onbekend";
						} else {
							$fotograafNaam = $lidNamen[$fotograaf->getContactID()];
							$fotograafNaam = str_replace(" ", "", strtolower($fotograafNaam));
						}
						$dstdir = $location . "/" . $fotograafNaam;
						if (!file_exists($dstdir)){
							if (mkdir($dstdir) === false){
								$warnings[] = "Kan &quot;$dstdir&quot; niet aanmaken, foto &quot;$dstfilename&quot; niet aangemaakt";
								continue; // volgende foto
							} elseif (@chmod($dstdir, 0777) === false){
								$warnings[] = "Kan &quot;$dstdir&quot; niet chmodden naar 0777 (a+rwx)";
							}
						}
					} else {
						$dstdir = $location;
					}

					$dst = $dstdir . "/" . $dstfilename;

					if (file_exists($dst) || true ){
						// extensie afstrippen
						$noext = substr($dstfilename, 0, strlen($dstfilename) - 4);

						// goede suffix zoeken
						for ($suffix = 1; $suffix <= 1000; $suffix++){
							$newfilename = $noext . "-" . $suffix . ".jpg";
							$newdst = $dstdir . "/" . $newfilename;

							if (!file_exists($newdst)){
								$dst = $newdst;
								break;
							}
						}
					}

					if (copy($src, $dst) === false){
						$warnings[] = "Kopieren van &quot;$src&quot; naar &quot;$dst&quot; is niet gelukt";
						continue; // volgende foto
					}
					if (chmod($dst, 0666) === false){
						$warnings[] = "chmod van &quot;$dst&quot; (0666, a+rw) is niet gelukt";
					}

					$exportCounter++;
				}

			}

			if (sizeof($errors) > 0 || sizeof($warnings) > 0){
				$msg = '<span class="waarschuwing">Error: ' .
					implode($errors, "<br/>") .
					implode($warnings, "<br/>") .
					'</span><br/><br/>';
			} elseif (isset($exportCounter)){
				$msg = "<h3>Er zijn $exportCounter bestanden gekopieerd (van de " . $fotos->aantal() . ")</h3>" .
					"Je kunt de foto's nu gaan bekijken op de door jou opgegeven locatie:<br/>" .
					"<strong>$location</strong><br/><br/>";
				Page::redirectMelding('/FotoWeb',$msg);
			}
		}

		$totalSize = 0;
		foreach ($mediaids as $mediaid){
			$fotopath = FILESYSTEM_PREFIX . Media::geefAbsLoc("original", $mediaid);
			$totalSize = $totalSize + @filesize($fotopath);
		}
		if ($totalSize != 0){
			$totalSize = round($totalSize / 1024 / 1024, 2);
		}

		$page = Page::getInstance()->start();
		if($msg) {
			$page->add(new HtmlSpan($msg,'text-danger strongtext'));
		}
		$page->add(new HtmlSpan("Hoi! Je staat op het punt om de onderstaande foto's te exporteren. Het zijn er exact " .
			sizeof($mediaids) . " en ze zijn in totaal $totalSize megabytes groot. " .
			"De foto's worden gekopieerd naar een directory onder /scratch op de A-Eskwadraatcomputers, " .
			'je kunt hieronder de exacte locatie opgeven. Vervolgens kun je bijvoorbeeld ' .
			'<a href="http://www.winscp.net">WinSCP</a> gebruiken om de fotos ' .
			'naar je eigen computer te kopi&euml;ren.<br><br>'));

		$page->add($form = HtmlForm::named('exporteren'));
		$form->setMethod("POST");
		$form->add(HtmlInput::makeHidden('mediaids',$jsonmediaids));
		$form->add('<table width="500" align="center">' .
			'<tr><td>' .
			"Foto's&nbsp;exporteren&nbsp;naar:&nbsp;" .
			'</td><td>' .
			HtmlInput::makeText("location", tryPar("location", "/scratch/"), 40) .
			'</td></tr>' .
			'<tr><td>&nbsp;</td><td>' .
			HtmlInput::makeCheckbox("fotograafsubdirs", tryPar("fotograafsubdirs", false), null, null, "Foto's groeperen per fotograaf<br/>") .
			'</td></tr>' .
			'<tr><td>&nbsp;</td><td>' .
			'<input type="submit" value="Huppakee, doen!">' .
			'</td></tr>' .
			'</table>');

		$page->add("<br/><br/>");

		// Print de te exporteren foto's per 30, zonder icoontjes voor kartshoppen, exporteren en premiumselectie
		//			echo FotoInterfaceFactory::makeFotosMatrix(30, "Te exporteren foto's", $this->getFilteredMediaIds(), false, false, false);*/
		$page->end();
	}
}
