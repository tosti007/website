<?php

abstract class Bugweb_Controller
{
	/**
	 * @site{/Service/Bugweb/ *}
	 */
	static public function bugEntry($args)
	{
		$bug = Bug::geef($args[0]);
		if (!($bug instanceof Bug))
		{
			return false;
		}
		return array(
			'name' => $bug->geefID(),
			'displayName' => BugView::waardeTitel($bug),
			'access' => $bug->magBekijken()
		);
	}

	/**
	 * @site{/Service/Bugweb/Bericht/ *}
	 */
	static public function bugBerichtEntry($args)
	{
		$bericht = BugBericht::geef($args[0]);
		if (!($bericht instanceof BugBericht))
		{
			return false;
		}
		return array(
			'name' => $bericht->geefID(),
			'displayName' => BugBerichtView::waardeTitel($bericht),
			'access' => $bericht->magBekijken()
		);
	}

	/**
	 * @site{/Service/Bugweb/Categorie/ *}
	 */
	static public function categorieEntry($args)
	{
		$categorie = BugCategorie::geef($args[0]);
		if (!($categorie instanceof BugCategorie))
		{
			return false;
		}
		return array(
			'name' => $categorie->geefID(),
			'displayName' => BugCategorieView::waardeNaam($categorie),
			'access' => $categorie->magBekijken()
		);
	}

	/**
	 * @site{/Service/Bugweb/ * /index.html}
	 */
	static public function toonBug()
	{
		global $request;

		$bugID = vfsVarEntryName();
		$bug = Bug::geef($bugID);
		if (!$bug instanceof Bug)
		{
			spaceHttp(403);
		}

		if (!$bug->magBekijken())
		{
			spaceHttp(403);
		}

		$omgekeerd = false;
		$ingelogd = Persoon::getIngelogd();
		if ($ingelogd)
		{
			$voorkeur = $ingelogd->getVoorkeur();
			$omgekeerd = $voorkeur->getBugSortering() == 'DESC';
		}

		$toon_fouten = false;
		$msg = NULL;

		$titel = tryPar('BugBericht[Titel]', $bug->getTitel());
		$categorieID = tryPar('BugBericht[Categorie]', $bug->getCategorieBugCategorieID());
		$prioriteit = tryPar('BugBericht[Prioriteit]', $bug->getPrioriteit());
		$status = tryPar('BugBericht[Status]', $bug->getStatus());
		$niveau = tryPar('BugBericht[Niveau]', $bug->getNiveau());
		$level = tryPar('BugBericht[Level]', $bug->getLevel());
		$bericht = tryPar('BugBericht[Bericht]', '');
		$toewijzingIDs = tryPar('BugBericht[Toewijzingen]', array());

		if (Token::processNamedForm('NieuwBugBericht'))
		{
			$nieuwBerichtTime = new DateTimeLocale(tryPar('nieuwBerichtTime'));

			$laatsteBericht = $bug->getLaatsteBericht();

			if ($nieuwBerichtTime < $laatsteBericht->getMoment())
			{
				Page::addMelding(_('Er is een nieuw reactie binnengekomen sinds '
					. 'je dit bericht geschreven hebt! Let goed op '
					. 'wat je wilt schrijven!'), 'waarschuwing');
			}
			else
			{
				$moment = new DateTimeLocale();
				$categorie = BugCategorie::geef($categorieID);
				$toewijzingen = PersoonVerzameling::verzamel($toewijzingIDs);

				if (!$titel)
				{
					$msg = new HtmlSpan(_('Vul een titel in.'), 'text-danger strongtext');
					$titel = $bug->getTitel();
				}
				elseif (!$categorie)
				{
					$msg = new HtmlSpan(_('Categorie niet gevonden.'), 'text-danger strongtext');
					$categorieID = $bug->getCategorieBugCategorieID();
				}
				elseif (!in_array($prioriteit, Bug::enumsPrioriteit()))
				{
					$msg = new HtmlSpan(_('Ongeldige prioriteit.'), 'text-danger strongtext');
					$prioriteit = $bug->getPrioriteit();
				}
				elseif (!in_array($status, Bug::enumsStatus()))
				{
					$msg = new HtmlSpan(_('Ongeldige status.'), 'text-danger strongtext');
					$status = $bug->getStatus();
				}
				elseif (!in_array($niveau, Bug::enumsNiveau()))
				{
					$msg = new HtmlSpan(_('Ongeldige niveau.'), 'text-danger strongtext');
					$niveau = $bug->getNiveau();
				}
				elseif (!in_array($level, Bug::enumsLevel()))
				{
					$msg = new HtmlSpan(_('Ongeligde moeilijkheid.'), 'text-danger strongtext');
					$level = $bug->getLevel();
				}
				else
				{
					$bugBericht = BugBericht::nieuwBugBericht($bug, $ingelogd, $titel, $categorie, $status, $prioriteit, $toewijzingen, $niveau, $bericht, $moment, null, $level);
					if (!($bugBericht instanceof BugBericht))
					{
						$msg = new HtmlSpan(_('Vul een bericht in.'), 'text-danger strongtext');
					}
					elseif (!$bugBericht->valid())
					{
						$msg = new HtmlSpan(_('Er waren fouten.'), 'text-danger strongtext');
						$toon_fouten = true;
					}
					else
					{
						$bugBericht->opslaan();

						$bug->voegBugBerichtToe($bugBericht);
						$bug->opslaan();

						Page::addMelding(_('De reactie is verstuurd.'), 'succes');
					}
				}
			}
		}

		$page = static::getBugwebPage()
			->addFooterJS(Page::minifiedFile('knockout.js', 'js'))
			->addFooterJS(Page::minifiedFile('bugbericht.js', 'js'))
			->start(sprintf(_('Bug %s[VOC:bugid]'), BugView::waardeBugID($bug)));

		if ($request->server->get('HTTP_REFERER', null) || tryPar('referer'))
		{
			$page->add(HtmlAnchor::button(tryPar('referer', $request->server->get('HTTP_REFERER')), _('Terug naar waar je vandaan kwam')));
		}

		if ($msg)
		{
			Page::addMelding($msg, 'fout');
		}


		$viewBug = BugView::toon($bug);
		$viewReacties = BugView::toonReacties($bug, $omgekeerd);

		if ($bug->magReageren())
		{
			$viewNieuwBericht = BugBerichtView::nieuwBericht($bug
				, $titel
				, $categorieID
				, $prioriteit
				, $status
				, $bericht
				, $toewijzingIDs
				, $niveau
				, $level
				, $toon_fouten
			);

			if ($omgekeerd)
			{
				$page->add($viewBug);
				$page->add(new HtmlHR());
				$page->add($viewNieuwBericht);
				$page->add($viewReacties);
			}
			else
			{
				$page->add($viewBug);
				$page->add($viewReacties);
				$page->add(new HtmlHR());
				$page->add($viewNieuwBericht);
			}
		}
		else
		{
			$page->add($viewBug);
			$page->add($viewReacties);
		}
		$page->end();
	}

	/**
	 * @site{/Service/Bugweb/ * /Volg}
	 */
	static public function volgBug()
	{
		self::setVolgBug(true);
	}

	/**
	 * @site{/Service/Bugweb/ * / Ontvolg}
	 */
	static public function ontvolgBug()
	{
		self::setVolgBug(false);
	}

	static private function setVolgBug($value)
	{
		$bugID = vfsVarEntryName();
		$bug = Bug::geef($bugID);
		if (!$bug instanceof Bug)
		{
			spaceHttp(403);
		}

		if (!$bug->magBekijken())
		{
			spaceHttp(403);
		}

		$bug->setVolger(Persoon::getIngelogd(), $value);
		Page::redirectMelding($bug->url(), $value
			? _('Je volgt de bug nu.')
			: _('Je volgt de bug nu niet.'));
	}

	/**
	 * @site{/Service/Bugweb/InverteerBugSortering}
	 */
	static public function inverteerBugSortering()
	{
		global $request;

		$voorkeur = Persoon::getIngelogd()->getVoorkeur();
		$voorkeur->inverteerBugSortering();
		$voorkeur->opslaan();

		$melding = sprintf(_('Bugs worden vanaf nu %s[VOC: sortering, cq aflopend etc] gesorteerd.'),
			PersoonVoorkeurView::waardeBugSortering($voorkeur));

		if ($request->server->get('HTTP_REFERER', null))
		{
			Page::redirectMelding($request->server->get('HTTP_REFERER'), $melding);
		}
		else
		{
			Page::redirectMelding(BUGWEBBASE, $melding);
		}
	}

	/**
	 * @site{/Service/Bugweb/Melden}
	 */
	static public function nieuweBug()
	{
		// We zijn nu read-only, dus we sturen altijd een (zo goed als) lege
		// pagina.
		static::getBugwebPage()
			->start("Tijdelijk niet beschikbaar")
			->end();
		return;

		$categorieID = tryPar('categorieID');
		$categorie = BugCategorie::geef($categorieID) ?: NULL;

		// handig voor feedbacklinks
		$pagina = tryPar('pagina');
		$bericht = tryPar('bericht');

		$tmpBug = new Bug(new DateTimeLocale(), $categorie);
		$msg = _('Voer hieronder een nieuwe bug in. Let hierbij op dat je de juiste categorie selecteert, en controleer of er niet al een bug over hetzelfde onderwerp bestaat.');
		$toon_fouten = false;

		if (Token::processNamedForm('NieuweBug'))
		{
			$ingelogd = Persoon::getIngelogd();

			BugView::processForm($tmpBug);
			$titel = $tmpBug->getTitel();
			$categorieID = $tmpBug->getCategorieBugCategorieID();
			$status = $tmpBug->getStatus();
			$prioriteit = $tmpBug->getPrioriteit();
			$niveau = $tmpBug->getNiveau();
			$level = $tmpBug->getLevel();
			$moment = new DateTimeLocale();
			$bericht = tryPar('Bug[Bericht]');

			$categorie = BugCategorie::geef($categorieID) ?: NULL;
			if (!$categorie)
			{
				Page::addMelding(_('Categorie niet gevonden.'), 'fout');
				$toon_fouten = true;
			}
			elseif (!$bericht)
			{
				Page::addMelding(_('Vul een bericht in.'), 'fout');
				$toon_fouten = true;
			}
			else
			{
				$bug = Bug::nieuweBug($ingelogd, $titel, $categorie, $status, $prioriteit, $niveau, $level, $bericht, $moment);
				if ($bug->valid())
				{
					$bug->opslaan();
					Page::redirectMelding($bug->url(), _('De bug is gemeld!'));
				}
				else
				{
					Page::addMelding(_('Er waren fouten.'), 'fout');
					$toon_fouten = true;
				}
			}
		}
		else
		{
			// probeer een beginnetje te maken aan het bericht
			if (!$bericht && $pagina)
			{
				$tmpBug->setBericht(sprintf(_("Gemeld op URL: %s[VOC: url]"), $pagina));
			}
		}

		static::getBugwebPage()
			->addFooterJS(Page::minifiedFile('knockout.js', 'js'))
			->addFooterJS(Page::minifiedFile('bugbericht.js', 'js'))
			->start(_('Nieuwe bug melden'))
			->add(new HtmlDiv($msg, 'bs-callout bs-callout-info'))
			->add(BugView::nieuweBug($tmpBug, $bericht, $toon_fouten))
			->end();
	}

	/**
	 * @site{/Service/Bugweb/Willekeurig}
	 */
	static public function willekeurig()
	{
		$bug = Bug::geefWillekeurig();

		Page::redirect($bug->url());
	}

	/**
	 * @site{/Service/Bugweb/Bericht/ * /VerifieerEmail}
	 */
	static public function bugBerichtVerifieerEmail()
	{
		$bugBerichtID = vfsVarEntryName();
		$bugBericht = BugBericht::geef($bugBerichtID);

		$deze = !empty(tryPar('deze'));
		$alle = !empty(tryPar('alle'));

		if ($deze == $alle)
		{
			Page::redirectMelding($bugBericht->getBug()->url(), _("Je moet kiezen: of alleen dit bugbericht of alle"), "fout");
			return;
		}

		if ($deze)
		{
			$berichten = array($bugBericht);
		}
		else
		{
			$mail = $bugBericht->getMelderEmail();
			$berichten = BugBerichtQuery::table()->whereProp("melderEmail", $mail)->verzamel();
		}
		foreach ($berichten as $bericht)
		{
			$bericht->verifieerEmail(Persoon::getIngelogd());
			$bericht->opslaan();
		}
		Page::redirectMelding($bugBericht->getBug()->url(), _('Je bent geverifieerd!'));
	}

	/**
	 * @site{/Service/Bugweb/Categorie/ * /index.html}
	 */
	static public function toonBugCategorie()
	{
		$categorieID = vfsVarEntryName();
		$categorie = BugCategorie::geef($categorieID);
		if (!$categorie instanceof BugCategorie)
		{
			spaceHttp(403);
		}

		$page = static::getBugwebPage()
			->setEditUrl($categorie->url() . '/Wijzig');
		if ($categorie->magVerwijderen())
		{
			$page->setDeleteUrl($categorie->url() . '/Verwijder');
		}
		$page->start(BugCategorieView::waardeNaam($categorie))
			->add(BugCategorieView::toon($categorie))
			->end();
	}

	/**
	 * @site{/Service/Bugweb/Categorie/index.html}
	 */
	static public function bugCategorieOverzicht()
	{
		$cies = BugCategorieVerzameling::geefAlleBugCommissies();

		$magInactieveTonen = false;
		foreach ($cies as $cie)
		{
			if ($cie->magWijzigen())
			{
				$aantalInactieve = $cie->getAantalInactieveBugCategorieen();
				if ($aantalInactieve > 0)
				{
					$magInactieveTonen = true;
					break;
				}
			}
		}

		$toonInactieve = $magInactieveTonen && tryPar('toonalles', false);
		$magAanmaken = hasAuth('actief');

		static::getBugwebPage()
			->start(_('Overzicht bugcategorie&#235;n'))
			->add(BugCategorieVerzamelingView::overzicht($cies, $toonInactieve, $magInactieveTonen, $magAanmaken))
			->end();
	}

	/**
	 * @site{/Service/Bugweb/Categorie/Nieuw}
	 */
	static public function nieuweBugCategorie()
	{
		$msg = '';
		$toon_fouten = false;
		$categorie = new BugCategorie();
		$categorie->setActief(true);

		if (Token::processNamedForm('NieuweBugCategorie'))
		{
			$cie = Commissie::geef(tryPar('BugCategorie[Commissie]'));
			if ($cie instanceof Commissie && $cie->magWijzigen())
			{
				BugCategorieView::processForm($categorie, 'nieuw');

				if ($categorie->valid())
				{
					$categorie->opslaan();

					Page::redirectMelding($categorie->url(),
						_('De categorie is aangemaakt!'));
				}
				else
				{
					$toon_fouten = true;
				}
			}
			else
			{
				$msg = _('Commissie niet gevonden.');
			}
		}

		$form = HtmlForm::named('NieuweBugCategorie');
		$form->add(BugCategorieView::createForm($categorie, $toon_fouten, 'nieuw'));
		$form->add(HtmlInput::makeFormSubmitButton(_('Maak aan')));

		$page = static::getBugwebPage()->start(_('Nieuwe categorie aanmaken'));
		$page->add($form)->end();
	}

	/**
	 * @site{/Service/Bugweb/Categorie/ * /Wijzig}
	 */
	static public function wijzigBugCategorie()
	{
		$categorieID = vfsVarEntryName();
		$categorie = BugCategorie::geef($categorieID);
		if (!$categorie instanceof BugCategorie
			|| !$categorie->magWijzigen())
		{
			spaceHttp(403);
		}

		$toon_fouten = false;

		if (Token::processNamedForm('WijzigBugCategorie'))
		{
			BugCategorieView::processForm($categorie);

			if ($categorie->valid())
			{
				$categorie->opslaan();

				Page::redirectMelding($categorie->url(),
					_('De categorie is gewijzigd.'));
			}
			else
			{
				$toon_fouten = true;
			}
		}

		$form = HtmlForm::named('WijzigBugCategorie');
		$form->add(BugCategorieView::createForm($categorie, $toon_fouten));
		$form->add(HtmlInput::makeSubmitButton(_('Wijzig')));

		static::getBugwebPage()
			->start(_('Wijzig categorie'))
			->add(new HtmlHeader(2, sprintf(_('Wijzig de categorie %s[VOC: categorienaam]'),
				BugCategorieView::waardeNaam($categorie))))
			->add($form)
			->end();
	}

	/**
	 * @site{/Service/Bugweb/Categorie/ * /Verwijder}
	 */
	static public function verwijderBugCategorie()
	{
		$categorieID = vfsVarEntryName();
		$categorie = BugCategorie::geef($categorieID);
		if (!$categorie instanceof BugCategorie
			|| !$categorie->magVerwijderen())
		{
			spaceHttp(403);
		}

		$returnVal = $categorie->verwijderen();

		if (!is_null($returnVal))
		{
			Page::redirectMelding(BUGWEBBASE . '/Categorie',
				$returnVal);
		}
		else
		{
			Page::redirectMelding(BUGWEBBASE . '/Categorie',
				_('De categorie is verwijderd.'));
		}
	}

	/**
	 * @site{/Service/Bugweb/Categorie/ * /WijzigActief}
	 */
	static public function wijzigActiefBugCategorie()
	{
		$categorieID = vfsVarEntryName();
		$categorie = BugCategorie::geef($categorieID);
		if (!$categorie instanceof BugCategorie
			|| !$categorie->magWijzigen())
		{
			spaceHttp(403);
		}

		if ($categorie->getActief())
		{
			if ($categorie->kanDeactiveren())
			{
				$categorie->setActief(false);
				$categorie->opslaan();
				$melding = _('De categorie is gedeactiveerd.');
			}
			else
			{
				$melding = _('De categorie bevat nog open bugs! Deze moeten worden gesloten voordat de categorie kan worden gedeactiveerd.');
			}
		}
		else
		{
			$categorie->setActief(true);
			$categorie->opslaan();
			$melding = _('De categorie is heractiveerd.');
		}

		Page::redirectMelding($categorie->url(), $melding);
	}

	/**
	 * @site{/Service/Bugweb/index.html}
	 */
	static public function home()
	{
		$categorieID = tryPar('categorieID');
		$categorie = BugCategorie::geef($categorieID) ?: NULL;
		$ingelogd = Persoon::getIngelogd();

		if (tryPar('commissieID'))
		{
			$commissie = Commissie::geef(tryPar('commissieID')) ?: NULL;
		}
		elseif ($ingelogd)
		{
			$commissie = $ingelogd->getCommissiesMetBugs()->first();
		}
		else
		{
			$commissie = NULL;
		}

		if ($categorie && (!$ingelogd || !$categorie->getCommissie()->hasLid($ingelogd)))
		{
			$categorie = NULL;
		}

		if ($commissie && (!$ingelogd || !$commissie->hasLid($ingelogd)))
		{
			$commissie = NULL;
		}

		if ($ingelogd)
		{
			$titel = _('Persoonlijk bugoverzicht');
			$voorkeur = $ingelogd->getVoorkeur();
			if ($categorie)
			{
				$voorkeur->setFavoBugCategorie($categorie);
				$voorkeur->opslaan();
			}
			else if (($favo = $voorkeur->getFavoBugCategorie()) instanceof BugCategorie)
			{
				$categorie = $favo;
			}
		}
		else
		{
			$titel = _('Bugoverzicht');
		}

		static::getBugwebPage()
			->start($titel)
			->add(BugVerzamelingView::toonInfoPagina($ingelogd, $categorie, $commissie))
			->end();
	}

	/**
	 * @site{/Service/Bugweb/Zoeken}
	 */
	static public function zoeken()
	{
		global $request, $uriRef;

		$bugsAantal = 0;
		$tabel = '';
		$zoekterm = tryPar('zoekterm', '');
		$categorieIDs = tryPar('categorie');
		if ($categorieIDs)
		{
			if (is_array($categorieIDs))
			{
				$categorieen = BugCategorieQuery::table()
					->whereInProp('bugCategorieID', $categorieIDs)->verzamel();
			}
			else
			{
				// Probeer of dit per ongeluk een losse categorie is.
				$categorieen = new BugCategorieVerzameling();
				$categorie = BugCategorie::geef($categorieIDs);
				if ($categorie)
				{
					$categorieen->voegToe($categorie);
				}
			}
		}
		else
		{
			$categorieen = new BugCategorieVerzameling();
		}
		$commissieID = tryPar('commissie');
		$commissie = Commissie::geef($commissieID) ?: NULL;
		$status = tryPar('status');
		$offset = (int)tryPar('limit', '0_0');
		$prioriteit = tryPar('prioriteit');

		$offset = explode('_', $offset);
		$offset = (int)$offset[0];
		if ($offset < 0)
		{
			$offset = 0;
		}

		$prioriteiten = array();
		if ($prioriteit)
		{
			$prioriteitArray = explode(' ', $prioriteit);
			if (count($prioriteitArray) != 2)
			{
				spaceHttp(501);
			}

			$prioriteitComp = $prioriteitArray[0];
			$prioriteitInt = (int)$prioriteitArray[1];
			if (!in_array($prioriteitComp, array('=', '>=', '<=')) || !is_int($prioriteitInt) || $prioriteitInt <= 0 || $prioriteitInt > count(Bug::enumsPrioriteit()))
			{
				spaceHttp(501);
			}
			// Deze verwacht een getal vanaf 0.
			$prioriteiten = Bug::prioriteiten($prioriteitComp, $prioriteitInt - 1);
		}

		if ($zoekterm || $categorieen->aantal() > 0 || $commissie || $prioriteit || ($status && tryPar('submit')))
		{
			$bugsAantal = BugVerzameling::zoeken($zoekterm, $categorieen, $commissie, $status, $prioriteiten, null, true);
			$bugs = BugVerzameling::zoeken($zoekterm, $categorieen, $commissie, $status, $prioriteiten, $offset);
			$tabel = BugVerzamelingView::tabel($bugs, true);
		}

		$zoekForm = BugVerzamelingView::zoekForm($zoekterm, $categorieen, $commissie, $status, $prioriteit);

		$prevparams = array_merge($request->query->all(), array("offset" => max($offset - BUGWEB_BUGS_BIJ_ZOEKEN, 0)));
		$prev_query_string = http_build_query($prevparams);
		$nextparams = array_merge($request->query->all(), array("offset" => $offset + BUGWEB_BUGS_BIJ_ZOEKEN));
		$next_query_string = http_build_query($nextparams);

		static::getBugwebPage()
			->start(_('Zoeken naar bugs'))
			->add(new HtmlDiv($zoekForm))
			->add($bugsAantal ? new HtmlParagraph(sprintf(_('%d[VOC: aantal bugs] bugs gevonden'), $bugsAantal)) : NULL)
			->add($bugsAantal ? limietLinks($bugsAantal, BUGWEB_BUGS_BIJ_ZOEKEN) : NULL)
			->add($tabel)
			->end();
	}

	/**
	 * @site{/Service/Bugweb/NieuwFromForm}
	 */
	static protected function getBugwebPage()
	{
		$readOnlyMsg =
			"Momenteel is de WebCie aan het migreren naar een nieuw bugsysteem.
			Hierom is BugWeb in read only-modus gezet.</br>
			Je kan issues nu melden op onze <a href='https://gitlab.com/iba-aes/webcie/website/issues'>GitLabpagina</a> (hiervoor moet je een account maken bij GitLab).</br>
			Heb je een heel urgente bug?
			Dan kan je deze ook sturen naar naar <a href='mailto:webcie@a-eskwadraat.nl'>webcie@a-eskwadraat.nl</a>.";

		$page = Page::getInstance()
			->addHeadCSS(Page::minifiedFile('style_bugweb.css', 'css'))
			->setBrand('bugweb');

		if (hasAuth('god')) {
			// Shitty easter egg voor WebCie'rs.
			// Oftewel: gratis materiaal voor "kijk wat voor rare shit er in deze codebase zit"-blogposts.
			$page->addMelding('「BugWeb」はすでに死んでいる！！！！！', 'fout');
			$page->addMelding('NANI?!?!');
		} else {
			$page->addMelding($readOnlyMsg, 'fout');
		}

		return $page;
	}

	/**
	 * @site{/Service/Bugweb/Bericht/ *}
	 */
	static public function bugFromForm()
	{
		if (!trypar("Omschrijving"))
		{
			return;
		}

		$bug = Bug::nieuweBug(Persoon::getIngelogd()
			, "Bug gemeld via feedbackformulier"
			, BugCategorie::geef(58)
			, 'OPEN'
			, 'GEMIDDELD'
			, 'OPEN'
			, 'Gemeld op URL: ' . tryPar("URL") . "\n\n" . tryPar("Omschrijving")
			, new DateTimeLocale());

		$bug->opslaan();
	}

	/**
	 * @site{/Service/Bugweb/Grafieken}
	 */
	static public function grafieken()
	{
		$page = Page::getInstance()
			->addFooterJS(Page::minifiedFile('Chart.js'))
			->start();

		$cieArray = array(61, 42, 43, 8);

		foreach ($cieArray as $cie)
		{
			$cie = Commissie::geef($cie);
			$page->add(new HtmlHeader(3, CommissieView::waardeNaam($cie)));
			$page->add($cie->maakBugGrafiek());
		}

		$page->end();
	}

	/**
	 * @site{/Service/Bugweb/Statistieken}
	 */
	static public function statistieken()
	{
		global $bugStatusSets;

		$page = Page::getInstance();
		$page->addHeadCSS(Page::minifiedFile('style_bugweb.css', 'css'));
		$page->start(_("Bugstatistieken"));

		$page->add(array(
			new HtmlHeader(4, "Huidige commit van de website: "),
			// FIXME gebruik alsjeblieft geen command substitution
			new HtmlPre(htmlspecialchars(`git show --summary`))
		));

		$page->add(new HtmlHeader(3, _("Categorie&#235;n")));

		$cies = BugCategorieVerzameling::geefAlleBugCommissies();

		$page->add($table = new HtmlTable(null, 'sortable'));
		$thead = $table->addHead();
		$tbody = $table->addBody();

		$row = $thead->addRow();
		$row->makeHeaderFromArray(array(_("Open"), _("Gesloten"), _("Opgelost"),
			_("Totaal"), _("Categorie")));

		$showStatuses = array("OPENS", "GESLOTENS", "OPGELOSTS");

		foreach ($cies as $cie)
		{
			$row = $tbody->addRow();
			$row->addClass('cie-bug-rij');
			$zoekStr = BUGWEBBASE . "/Zoeken?zoekterm=&categorie=&commissie[]=" . $cie->getCommissieID() . "&prioriteit=&submit=Zoeken";

			foreach ($showStatuses as $statuses)
			{
				// Zie constants.php -> $bugStatusSets
				$bugs = BugVerzameling::zoeken('', null, $cie, $statuses, Bug::enumsPrioriteit(), null, true);
				$row->addData(new HtmlAnchor($zoekStr . "&status=" . $statuses, $bugs));
			}

			$bugsTotaal = BugVerzameling::zoeken('', null, $cie, 'ALLE', Bug::enumsPrioriteit(), null, true);
			$row->addData($bugsTotaal);
			$row->addData(new HtmlAnchor($zoekStr . "&status=ALLE", CommissieView::waardeNaam($cie)));

			$cats = BugCategorieVerzameling::geefVanCommissie($cie);

			foreach ($cats as $cat)
			{
				$row = $tbody->addRow();
				$zoekStr = BUGWEBBASE . "/Zoeken?zoekterm=&categorie=" . $cat->getBugCategorieID() . "&commissie=&prioriteit=&submit=Zoeken";

				foreach ($showStatuses as $statuses)
				{
					$bcvz = new BugCategorieVerzameling();
					$bcvz->voegToe($cat);
					$bugs = BugVerzameling::zoeken('', $bcvz, null, $statuses, Bug::enumsPrioriteit(), null, true);
					$row->addData(new HtmlAnchor($zoekStr . "&status=$statuses", $bugs));
				}

				$bcvz = new BugCategorieVerzameling();
				$bcvz->voegToe($cat);
				$bugsTotaal = BugVerzameling::zoeken('', $bcvz, null, 'ALLE', Bug::enumsPrioriteit(), null, true);
				$row->addData($bugsTotaal);
				$row->addData(new HtmlAnchor($zoekStr . '&status=ALLE', BugCategorieView::waardeNaam($cat)));
			}
		}

		$page->add($rowdiv = new HtmlDiv($div1 = new HtmlDiv(null, 'col-md-4'), 'row'));

		$div1->add(new HtmlHeader(3, _("Statussen")));
		$div1->add($table = new HtmlTable());

		$statussen = BugVerzameling::geefStatussenMetAantal();
		foreach ($statussen as $status => $aantal)
		{
			// de status van een bug zoals gebruikers hem zien
			$statusNaam = BugBerichtView::labelEnumStatus($status);

			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData(new HtmlAnchor(BUGWEBBASE . '/Zoeken?zoekterm=&categorie=&commissie=&status=' . $status . '&prioriteit=&submit=Zoeken', $statusNaam));
		}

		$rowdiv->add($div2 = new HtmlDiv(null, 'col-md-4'));

		$div2->add(new HtmlHeader(3, _("Prioriteiten")))
			->add($table = new HtmlTable());

		$prioriteiten = BugVerzameling::geefPrioriteitenMetAantal();
		$count = 1;
		foreach ($prioriteiten as $prio => $aantal)
		{
			// prioriteit zoals de gebruiker het ziet
			$prioNaam = BugBerichtView::labelEnumPrioriteit($prio);

			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData(new HtmlAnchor(BUGWEBBASE . '/Zoeken?zoekterm=&categorie=&commissie=&status=&prioriteit=%3D+' . $count . '&submit=Zoeken', $prioNaam));
			$count++;
		}

		$page->add(new HtmlDiv($div2 = new HtmlDiv(null, 'col-md-8'), 'row'));
		$div2->add(new HtmlHeader(3, _("Nog steeds niet opgelost")))
			->add($table = new HtmlTable());

		$nietopgelost = BugVerzameling::geefNogNietOpgelost();
		foreach ($nietopgelost as $bug)
		{
			$row = $table->addRow();
			$row->addData(new HtmlAnchor($bug->url(), $bug->geefID()));
			$row->addData(BugView::waardeMoment($bug));
			$row->addData(BugView::waardeTitel($bug));
		}

		$page->add($rowdiv = new HtmlDiv($div1 = new HtmlDiv(null, 'col-md-4'), 'row'));

		$div1->add(new HtmlHeader(3, _("Meeste bugs gemeld")));
		$div1->add($table = new HtmlTable());

		$melders = BugVerzameling::geefMeldersMetAantal();
		foreach ($melders as $id => $aantal)
		{
			if (empty($id) || is_null($id))
			{
				$persnaam = _("Onbekend");
			}
			else
			{
				$persnaam = PersoonView::naam(Persoon::geef($id));
			}
			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData($persnaam);
		}

		$rowdiv->add($div2 = new HtmlDiv(null, 'col-md-4'));

		$div2->add(new HtmlHeader(3, _("Meeste bugs opgelost")))
			->add($table = new HtmlTable());

		$fixers = BugToewijzingVerzameling::geefFixersMetAantal();
		foreach ($fixers as $id => $aantal)
		{
			if (empty($id) || is_null($id))
			{
				$persnaam = _("Onbekend");
			}
			else
			{
				$persnaam = PersoonView::naam(Persoon::geef($id));
				$persnaam = new HtmlAnchor(Persoon::geef($id)->url() . '/OpgelosteBugs', $persnaam);
			}
			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData($persnaam);
		}

		$rowdiv->add($div3 = new HtmlDiv(null, 'col-md-4'));

		$div3->add(new HtmlHeader(3, _("Meest gereageerd")))
			->add($table = new HtmlTable());

		$comm = BugBerichtVerzameling::geefCommentatorsMetAantal();
		foreach ($comm as $id => $aantal)
		{
			if (empty($id) || is_null($id))
			{
				$persnaam = _("Onbekend");
			}
			else
			{
				$persnaam = PersoonView::naam(Persoon::geef($id));
			}
			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData($persnaam);
		}

		$page->add($rowdiv = new HtmlDiv($div1 = new HtmlDiv(null, 'col-md-4'), 'row'));

		$div1->add(new HtmlHeader(3, _("Meeste bugs gevolgd")));
		$div1->add($table = new HtmlTable());

		$volgers = BugVolgerVerzameling::geefVolgersMetAantal();
		foreach ($volgers as $id => $aantal)
		{
			if (empty($id) || is_null($id))
			{
				$persnaam = _("Onbekend");
			}
			else
			{
				$persnaam = PersoonView::naam(Persoon::geef($id));
			}
			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData($persnaam);
		}

		$rowdiv->add($div2 = new HtmlDiv(null, 'col-md-4'));
		$div2->add(new HtmlHeader(3, _("Meest gevolgde bugs")));
		$div2->add($table = new HtmlTable());

		$gevolgd = BugVerzameling::geefGevolgdMetAantal();
		foreach ($gevolgd as $id => $aantal)
		{
			$bug = Bug::geef($id);
			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData(new HtmlAnchor($bug->url(), BugView::waardeTitel($bug)));
		}

		$rowdiv->add($div2 = new HtmlDiv(null, 'col-md-4'));
		$div2->add(new HtmlHeader(3, _("Meest gereageerde bugs")));
		$div2->add($table = new HtmlTable());

		$gereageerd = BugVerzameling::geefGereageerdMetAantal();
		foreach ($gereageerd as $id => $aantal)
		{
			$bug = Bug::geef($id);
			$row = $table->addRow();
			$row->addData($aantal);
			$row->addData(new HtmlAnchor($bug->url(), BugView::waardeTitel($bug)));
		}

		$page->end();
	}

	/**
	 * @site{/Service/Bugweb/Webhook/Push}
	 *
	 * Dit is een speciaal endpoint waar een Git host een POST-request naar
	 * kan sturen wanneer er naar de repository van de website gepusht wordt.
	 * Er wordt dan verwacht dat de body JSON bevat die info over de gepushte
	 * commits etc. bevat. Op dit moment moet dat dan volgens het format dat
	 * Gitea/Gogs gebruikt.
	 */
	static public function pushWebhook()
	{
		global $session;
		global $GITWEBHOOKNR;
		global $WSW4DB;

		if ($_SERVER['REQUEST_METHOD'] !== 'POST')
		{
			spaceHttp(405);
		} // 405 - Method Not Allowed

		$body = file_get_contents('php://input');
		$data = json_decode($body, true);

		if ($data['secret'] != GIT_WEBHOOK_SECRET)
		{
			spaceHttp(403);
		}

		Page::setVoorkeur('text'); // Dit moet ook om de een of andere reden.
		$page = Page::getInstance('text');
		$page->start();

		// We moeten even handmatig inloggen als het special account voor webhooks.
		$session->set('mylidnr', $GITWEBHOOKNR);
		// Omdat we ook rechten in de DB nodig hebben, moeten we de verbinding
		// daarmee opnieuw opzetten. Beetje vies, maar ja, kan niet echt anders.
		$WSW4DB = new dbpdo(WHOSWHO4_DB, '127.0.0.1', getDatabaseUsername(), sqlpass());

		// $ref ziet er uit als 'refs/heads/$branch', dus we pakken gewoon het
		// laatste stuk daarvan. Ik weet niet of er ooit naar iets anders dan
		// 'refs/heads' gepusht kan worden.
		$ref = explode('/', $data['ref']);
		$branch = end($ref);

		self::checkBugsGesloten($data['commits'], $branch);
		// TODO: dit misschien implementeren?
		// checkBugsGenoemd($commits);

		$page->end();
	}

	/**
	 * @brief Check of er in een verzameling bugs een bug gesloten moet worden.
	 *
	 * @param commits Een array van associatieve arrays met info over
	 *        de gepushte commits, in het format wat Gitea teruggeeft.
	 * @param branch De naam van de branch waar naar gepusht is.
	 */
	static function checkBugsGesloten($commits, $branch)
	{
		// We pakken een tekstpagina om debug output terug te kunnen geven.
		Page::setVoorkeur('text'); // Dit moet ook om de een of andere reden.
		$page = Page::getInstance('text');

		switch ($branch)
		{
			case GIT_LIVE_BRANCH:
				$status = 'OPGELOST';
				break;

			default:
				$status = 'GECOMMIT';
				break;
		}

		foreach ($commits as $commit)
		{
			$hash = $commit['id'];
			$author = $commit['author'];
			$message = $commit['message'];

			$authorDesc = $author['name'] . " <" . $author['email'] . ">";
			$commitDesc = "^$hash " . $commit['timestamp'] . " " . $author['username'];

			$tekst = "Deze bug is zojuist in ons versiebeheersysteem " . strtolower($status) . " door\n";
			$tekst .= "$authorDesc\n";
			$tekst .= "\n";
			$tekst .= "Op de website is deze verandering nog niet zichtbaar,\n";
			$tekst .= "die moet eerst geüpdatet worden. Dit zou spoedig moeten gebeuren.\n";
			$tekst .= "\n";
			$tekst .= "$commitDesc\n";
			$tekst .= "Op branch: $branch\n";
			$tekst .= "$message\n";
			$tekst .= "Bedankt voor het melden!\n";

			preg_match_all(BugBerichtView::BUG_CLOSE_REGEX, $message, $matches);
			foreach ($matches[0] as $match)
			{
				preg_match_all("/#?([0-9]+)/", $match, $bugs);
				foreach ($bugs[1] as $bugnr)
				{
					$bug = Bug::geef((int)$bugnr);
					if ($bug != null)
					{
						$bericht = BugBericht::vanGitCommit($bug, $tekst, $hash, $author['email'], $status);
						if (!is_null($bericht))
						{
							$bericht->opslaan();
							$bug->voegBugBerichtToe($bericht);
							$bug->opslaan();

							$page->add("Feestje! Bug $bugnr is " . strtolower($status) . "!");
						}
					}
					else
					{
						$page->add("Hey! Bug $bugnr is helemaal geen bug!");
					}
				}
			}
		}
	}

	/**
	 * @site{/Service/Bugweb/Rss}
	 */
	static public function rss()
	{
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `bugID` FROM `Bug` ORDER BY `bugID` DESC LIMIT 25");

		$bugs = BugVerzameling::verzamel($ids);
		$bugs->filterBekijken();
		BugVerzamelingView::rss($bugs);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
