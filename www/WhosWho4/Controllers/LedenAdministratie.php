<?php
/**
 * $Id$
 *
 * /Administratie/			-> Geeft basispagina met links
 *  /Pasfotos				-> checkUploadedFotos()
 *  /LidAf					-> lidaf($leden)
 *  /JarigeActieveLeden		-> jarigeActieveLeden()
 *  /Contacten				-> contactenOverzicht()
 */

abstract class LedenAdministratie_Controller
{

	public static function main(){

		$page = Page::getInstance();
		$page->start(_('Ledenadministratie'));

		$page->add(new HtmlDiv(array(
			new HtmlParagraph(_('Op deze pagina kan je je leden beheren met powertools en andere handige functies.')),
			new HtmlList(false, array(
				new HtmlAnchor('Nieuw',_('Nieuw lid invoeren')),
				new HtmlAnchor('NieuwPersoon', _('Nieuwe persoon aanmaken')) . " " . _('(voor boekenkopers, docenten, etc...)'),
				new HtmlAnchor('Lidaf',_('Lid-af maken van leden')),
				new HtmlAnchor('ZoekOudLeden',_('Vind mensen die lid-af gemaakt moeten worden')) . ' ' . _('aan de hand van een lijst met studentnummers'),
				new HtmlAnchor('VakidiootAdressen',_('Adressenlijst voor de Vakidioot genereren (leden, organisaties en bedrijfscontacten)')),
				new HtmlAnchor('Aes2rootsAdressen',_('Adressenlijst voor A-Es2 Roots genereren')) . ' ' . _('(excl. donateurs en huidige leden)'),
				new HtmlAnchor('JarigeActieveLeden',_('Jarige actieve leden tonen')),
				new HtmlAnchor('Pasfotos',_('Geüploade pasfotos controleren')),
				new HtmlAnchor('Organisatie',_('Organisaties beheren')),
				new HtmlAnchor('Studies',_('Overzicht van studies')),
				new HtmlAnchor('Naamfix',_('Fix potentieel verkeerde namen')),
				new HtmlAnchor('Geluksvogels',_('Overzicht van de geluksvogels van A-Eskwadraat (leden onder de 18 jaar)')),
				new HtmlAnchor('Massamodificatie',_('Bewerk veel lidgegevens tegelijk')),
				new HtmlAnchor('ActieveEerstejaars',_('Zoek actieve eerstejaars van een bepaald jaar')),
				)),
			new HtmlParagraph(sprintf(_('Heb je vragen? Mail de webcie of maak een %s aan!'),
								new HtmlAnchor('/Service/Bugs/Nieuw',_('bug'))))
			)));
			
		$page->end();
	}

   /**
	*   Laat de secretaris checken of er foto's moeten worden goedgekeurd.
	*/

	public static function checkUploadedFotos()
	{
		global $BESTUUR;

		requireAuth('bestuur');

		if(Token::processNamedForm('accepteer'))
		{
			$pasFotoTeksten=array();

			$pasFotoTeksten['inleiding_afwijzing']=
				_("De foto die je recentelijk hebt ingestuurd voldeed niet aan onze eisen.\n");
			$pasFotoTeksten['reden_Identiteit']=_("Je staat niet op de foto");
			$pasFotoTeksten['reden_Zichtbaar']=_("Je bent niet zichtbaar genoeg.");

			$pasFotoTeksten['eind_afwijzing']=
			_("\nJe kan alsnog een goede foto insturen. Maar vooralsnog blijven we je oude foto gebruiken.");

			$pasFotoTeksten['accepteer']=
			_("De foto die je recentelijk hebt ingestuurd is geaccepteerd. Zij staat nu op
			de website, en zal ook gebruikt worden voor de almanak, als je daarin wordt
			opgenomen.");

			$pasFotoTeksten['signatuur'] =  _("\n\nMet vriendelijke groet,\n\n").SECRNAME
				._("\nSecretaris A-Eskwadraat");

			$lid = Persoon::geef(tryPar('persoonID'));

			if(!$lid)
				spaceHttp(403);

			$profielfoto = ProfielFoto::zoekNieuw($lid);
			$pasFotoTeksten['aanhef'] = sprintf(_("Beste %s,\n\n"), PersoonView::naam($lid));

			$upload = tryPar('upload');
			if(isset($upload) && $upload == 'Accepteer')
			{
				Lid::accepteerUploadedFoto($profielfoto);
				$mail = $pasFotoTeksten['aanhef'].$pasFotoTeksten['accepteer'].$pasFotoTeksten['signatuur'];
				sendmail($BESTUUR['secretaris']->getEmail(),$lid->getContactID(),
					_('Je foto is geaccepteerd'), $mail);

				Page::addMelding(sprintf(_('De pasfoto van %s is geaccepteerd'), PersoonView::naam($lid)), 'success');
			}
			elseif (isset($upload) && $upload != 'Accepteer')
			{
				//doe een case mail uploader waarom de foto niet geaccepteerd is.

				if($upload ==  'Overig') {
					$mailreden=requirePar('reden');
				} else {
					$mailreden=$pasFotoTeksten['reden_'.$upload];
				}

				$mailstart = $pasFotoTeksten['aanhef'] . $pasFotoTeksten['inleiding_afwijzing'];
				$maileind = $pasFotoTeksten['eind_afwijzing'].$pasFotoTeksten['signatuur'];

				sendmail($BESTUUR['secretaris']->getEmail(), $lid->getContactID(),
					_('Je foto is afgewezen'),$mailstart . $mailreden . $maileind);

				Lid::afwijzenUploadedFoto($profielfoto);
				Page::addMelding(sprintf(_('De pasfoto van %s is geweigerd'), PersoonView::naam($lid)), 'fout');
			}
		}

		$profielfoto = ProfielFoto::getFirstNieuw();
		ProfielFotoView::pasfotoAdministratie($profielfoto);
	}

	/*
	 * Laat het batch formulier lidaf zien
	 * 
	 * $lidnrs is een array van $lidnr => $lidnaam (format van kart)
	 */
	public static function lidaf($leden = null)
	{
		if(!hasAuth('bestuur'))
			spaceHTTP(403);

		if($leden === null) {
			$leden = LidVerzameling::verzamel(explode(',', tryPar('lidnrs', '')));

			// haal mensen uit checkboxen
			foreach(trypar("PersoonVerzameling", array()) as $lidNr => $vinkje) {
				if ($vinkje)
					$leden->voegtoe(Lid::geef($lidNr));
			}
		}

		$page = Page::getInstance();

		//Kijk of we iets moeten verwerken
		switch(Token::processNamedForm()) {
			case 'kiesLidAf':
				break;
			case 'maakLidAf':
				$page->start(_("Leden") . " > " . _("Admin") . " > " . _("leden-af bevestiging"));
				$page->add(new HtmlHeader(2, _('Wijzigingen in leden')));

				try { $lidafDatum = new DateTimeLocale(requirePar('lidafdatum')); }
				catch(Exception $e) { 
					$page->add( new HtmlParagraph(_("Kan dit niet omzetten naar een DateTimeLocale: "
						 . requirePar('lidafdatum'), 'waarschuwing'))); 
				}

				$lidnrs = array_unique(explode(",",requirePar('lidnrs')));
				$leden = LidVerzameling::verzamel($lidnrs);

				$nietstedoen = true;
				foreach ($leden as $lid)
				{
					$lidDiv = new HtmlDiv();
					$lidLine = new HtmlAnchor($lid->url(),PersoonView::naam($lid));
					$lidNr = $lid->getContactID();

					// als lid niet geselecteerd: niets mee doen.
					if(tryPar($lidNr) != 'on')
						continue;

					$nietstedoen = false;
					//controleer voor wijzigingen in studies en update db
					$lidStudies = $lid->getStudies();
					foreach($lidStudies as $lidStudie)
					{
						$key = $lidStudie->getLidStudieID();

						//Status
						$studiestatus = tryPar($lidNr.'-'.$key);
						$lidStudie->setStatus($studiestatus);
						$lidDiv->add(array($lidLine . ': studiestatus '. $key .' >> '. $studiestatus, new HtmlBreak()));

						//Datum
						$eind = tryPar($lidNr.'-'.$key.'-eind');
						$lidStudie->setDatumEind($eind);
						$lidDiv->add(array($lidLine . ': studieeinddatum '. $key .' >> '. $eind, new HtmlBreak()));
						
						//Force commit??
						$lidStudie->opslaan();
					}

					//Email
					$mail = $lid->getEmail();
					if (empty($mail))
						$lidDiv->add(array('Er is geen emailadres gevonden en geen lidafmaak-mail verstuurd', new HtmlBreak()));
						
					//Maak nu ook het lid af als dit nodig is.
					if($lid->getStudies(true)->aantal() == 0 && ($lid->getLidToestand() == "LID" || $lid->getLidToestand() == "BAL")) {
						$lid->maakLidaf($lidafDatum);
						$lidDiv->add(array($lidLine . ': Mailingabonnementen en vakidabonnement verwijderd ', new HtmlBreak()));
						$lidDiv->add(array($lidLine . ': lidaf datum >> '. $lidafDatum->format('Y-m-d'), new HtmlBreak()));
					} elseif($lid->getStudies(true)->aantal() != 0)  {
						$lidDiv->add(array($lidLine . ': lidaf datum >> NIET doorgevoerd: er zijn nog actieve studies!', new HtmlBreak()));
					} elseif($lid->getLidToestand() == "OUDLID")  {
						$lidDiv->add(array($lidLine . ': lidaf datum >> NIET doorgevoerd: lid heeft al status oud-lid!', new HtmlBreak()));
					} else {
						$page->add(new HtmlParagraph(_("Er is iets misgegaan met lidnr " . $lid->getContactID()), 'waarschuwing'));
					}

					//Force commit??
					$lid->opslaan();
					$page->add($lidDiv);
				}

				//Geef bonus informatie of een warning.
				if($nietstedoen){
					$page->add(new HtmlParagraph(_("Je hebt niemand geselecteerd dus
						 ik heb geen enkel idee wat ik moet doen!"), 'waarschuwing'));
				} else {
					$page->add(new HtmlParagraph(array(
						 _('Als een lid niet lid-af is gemaakt dan kan het volgende aan de hand zijn;')
						, new HtmlList(false, array( _('Het lid heeft nog steeds een actieve studie')
								,_('Het lid heeft al een lid-tot datum')))
						)));
				}

				$page->end();
			default:
				break;
		}

		$page->start(_('Leden lid-af maken'));

		$page->add(LidVerzamelingView::maakLedenAf($leden));

		$page->end();
	}

	/**
	 *  Functie voor het weergeven van de komende jarige actieve leden met cieinfo.
	 **/
	public static function jarigeActieveLeden()
	{
		$page = Page::getInstance();

		$page->start(_("Leden") . " > " . _("Admin") . " > " . _("Jarige actieve leden"));

		//Kijk of de limiet/periode moeten aanpassen
		switch(Token::processNamedForm()) {
			case 'jarigLimiet': 
				$limiet = intval(tryPar('limiet', 0));
				if(!$limiet || abs($limiet)>366) $limiet=13;
				$koppen = tryPar('koppen');
				break;
			default:
				$limiet = 13;
				$koppen = 0;
				break;
		}
		$actieveLeden = LidVerzameling::actief();
		$jarigeLeden = PersoonVerzameling::jarig($limiet);
		$jarigeLeden->intersection($actieveLeden);
		if(trypar('submit_kart')){ //Buiten token om, dus iets minder secure.
			Kart::geef()->voegPersonenToe($jarigeLeden);
			$page->add(new HtmlParagraph(_('De leden zijn in je kart geshopt!')));
		}
		$page->add(LidVerzamelingView::actieveJarigen($jarigeLeden,$limiet,$koppen));
		$page->end();
	}

	public static function VakidiootAdressen()
	{
		$contacten = ContactVerzameling::vakidioten();
		$contactpersonen = ContactPersoonVerzameling::vakidioten();

		header("Content-Disposition: attachment; filename=vakid-adressen.csv");
		header("Content-type: text/csv; encoding=utf-8");

		echo 'naam;tav;straat;huisnummer;postcode;woonplaats'."\n";

		if($contacten !== null)
			foreach($contacten as $contact)
			{
				$adres = $contact->getEersteAdres();
				if(!$adres) continue;

				if($postadres = ContactAdres::geefBySoort($contact, 'POST'))
					$adres = $postadres;

				if($contact instanceof Lid)
				{
					$string = '"' . PersoonView::naam($contact) . '";';
					$string .= '"";'; //Persoon heeft geen TAV
					$string.= '"' . ($adres->getStraat1()	?: '')
								  . ($adres->getStraat2()	? " " . $adres->getStraat2() : '') . '";'
							. '"' . ($adres->getHuisnummer()?: '') . '";'
							. '"' . ($adres->getPostcode()	?: '') . '";'
							. '"' . ($adres->getWoonplaats()?: '') . '";'
							. '"' . ($adres->getLand()		?: '') . '"';
					$string .= "\n";
					echo $string;
					continue;
				}
				if($contact instanceof Organisatie) //Het is een instantie/bedrijf/zusje/etc
				{
					$string = '"' . $contact->getNaam() . '";';
					$orgcontactpersonen = ContactPersoonVerzameling::metConstraints($contact);
					$string .= '"';
					if($orgcontactpersonen->aantal() > 0)
						$string .= PersoonView::naam(Persoon::geef($orgcontactpersonen->first()->getPersoonContactID())); //Wat als er meerdere contactpersonen zijn?
					$string .= '";'; 

					$string.= '"' . ($adres->getStraat1()	?: '')
								  . ($adres->getStraat2()	? "\n" . $adres->getStraat2() : '') . '";'
							. '"' . ($adres->getHuisnummer()?: '') . '";'
							. '"' . ($adres->getPostcode()	?: '') . '";'
							. '"' . ($adres->getWoonplaats()?: '') . '";'
							. '"' . ($adres->getLand()		?: '') . '"';

					$string .= "\n";
					echo $string;
				}

			}

		//Bij contactpersonen is de specifieke koppeling tussen de persoon en de organiatie
		// van belang. BV als een persoon op meerdere plekken een vakid kan krijgen of als
		// meerdere mensen bij 1 bedrijf een vakid willen.
		if($contactpersonen !== null)
			foreach($contactpersonen as $contactpersoon)
			{
				$organisatie = $contactpersoon->getOrganisatie($contactpersoon);
				$contact = $contactpersoon->getPersoon($contactpersoon);

				$adres = $organisatie->getEersteAdres();
				if(!$adres) continue;

				if($postadres = ContactAdres::geefBySoort($organisatie, 'POST'))
					$adres = $postadres;

				if(isBedrijfscontact($contactpersoon))
				{
					$string = '"' . $organisatie->getNaam() . '";';
					$string.= '"' . PersoonView::naam($contact). '";'; 

					$string.= '"' . ($adres->getStraat1()	?: '')
								  . ($adres->getStraat2()	? "\n" . $adres->getStraat2() : '') . '";'
							. '"' . ($adres->getHuisnummer()?: '') . '";'
							. '"' . ($adres->getPostcode()	?: '') . '";'
							. '"' . ($adres->getWoonplaats()?: '') . '";'
							. '"' . ($adres->getLand()		?: '') . '"';

					$string .= "\n";
					echo $string;

				}
			}

		die;
	}

	public static function Aes2rootsAdressen()
	{
		$contacten = ContactVerzameling::aes2roots();
		$contactpersonen = null;

		header("Content-Disposition: attachment; filename=aes2roots-adressen.csv");
		header("Content-type: text/csv; encoding=utf-8");

		echo 'naam;tav;straat;huisnummer;postcode;woonplaats'."\n";

		if($contacten !== null)
			foreach($contacten as $contact)
			{
				$adres = $contact->getEersteAdres();
				if(!$adres) continue;

				if($postadres = ContactAdres::geefBySoort($contact, 'POST'))
					$adres = $postadres;

				if($contact instanceof Lid)
				{
					$string = '"' . PersoonView::naam($contact) . '";';
					$string .= '"";'; //Persoon heeft geen TAV
					$string.= '"' . ($adres->getStraat1()	?: '')
								  . ($adres->getStraat2()	? " " . $adres->getStraat2() : '') . '";'
							. '"' . ($adres->getHuisnummer()?: '') . '";'
							. '"' . ($adres->getPostcode()	?: '') . '";'
							. '"' . ($adres->getWoonplaats()?: '') . '";'
							. '"' . ($adres->getLand()		?: '') . '"';
					$string .= "\n";
					echo $string;
					continue;
				}
				if($contact instanceof Organisatie) //Het is een instantie/bedrijf/zusje/etc
				{
					$string = '"' . $contact->getNaam() . '";';
					$orgcontactpersonen = ContactPersoonVerzameling::metConstraints($contact);
					$string .= '"';
					if($orgcontactpersonen->aantal() > 0)
						$string .= PersoonView::naam(Persoon::geef($orgcontactpersonen->first()->getPersoonContactID())); //Wat als er meerdere contactpersonen zijn?
					$string .= '";'; 

					$string.= '"' . ($adres->getStraat1()	?: '')
								  . ($adres->getStraat2()	? "\n" . $adres->getStraat2() : '') . '";'
							. '"' . ($adres->getHuisnummer()?: '') . '";'
							. '"' . ($adres->getPostcode()	?: '') . '";'
							. '"' . ($adres->getWoonplaats()?: '') . '";'
							. '"' . ($adres->getLand()		?: '') . '"';

					$string .= "\n";
					echo $string;
				}

			}

		//Bij contactpersonen is de specifieke koppeling tussen de persoon en de organiatie
		// van belang. BV als een persoon op meerdere plekken een vakid kan krijgen of als
		// meerdere mensen bij 1 bedrijf een vakid willen.
		if($contactpersonen !== null)
			foreach($contactpersonen as $contactpersoon)
			{
				$organisatie = $contactpersoon->getOrganisatie($contactpersoon);
				$contact = $contactpersoon->getPersoon($contactpersoon);

				$adres = $organisatie->getEersteAdres();
				if(!$adres) continue;

				if($postadres = ContactAdres::geefBySoort($organisatie, 'POST'))
					$adres = $postadres;

				if(isBedrijfscontact($contactpersoon))
				{
					$string = '"' . $organisatie->getNaam() . '";';
					$string.= '"' . PersoonView::naam($contact). '";'; 

					$string.= '"' . ($adres->getStraat1()	?: '')
								  . ($adres->getStraat2()	? "\n" . $adres->getStraat2() : '') . '";'
							. '"' . ($adres->getHuisnummer()?: '') . '";'
							. '"' . ($adres->getPostcode()	?: '') . '";'
							. '"' . ($adres->getWoonplaats()?: '') . '";'
							. '"' . ($adres->getLand()		?: '') . '"';

					$string .= "\n";
					echo $string;

				}
			}

		die;
	}
	
	public static function oudLedenZoeken()
	{
		global $WSW4DB;

		Page::getInstance()->start();
		
		$body = new HtmlElementCollection();
		
		$studentNummers = tryPar('studentnummers');
		if(!$studentNummers || Token::processNamedForm() != 'oudledenzoeken')
		{
			$body->add(new HtmlHeader(2, _("Zoek naar oud-leden")));

			if(Kart::geef()->getLeden()->aantal() > 0)
			{
				$body->add($div = new HtmlDiv(null, 'waarschuwing'));
				$div->add(array(
					new HtmlParagraph(new HtmlStrong(_("Waarschuwing: je ledenkart is niet leeg."))),
					new HtmlParagraph(_("Dit betekent dat je de oud-leden die je hier gaat vinden mengt met leden die je nu in je kart hebt zitten.")),
					new HtmlAnchor('/Service/Kart/', _("Ga naar je kart."))
				));
			}

			$body->add(new HtmlParagraph(_("Vul studentnummers in van leden die nog wel studeren.")));
			$form = HtmlForm::named('oudledenzoeken');
			$form->add(new HtmlTextarea('studentnummers'));
			$form->add(HtmlInput::makeSubmitButton(_('Gaan met de banaan!')));
			$body->add($form);
		}
		else
		{
			$body->add(new HtmlHeader(2, _("Resultaat")));

			//Explodeer die lijst, haal whitespace weg en neem de lege en entries niet mee, laat dubbele weg
			$studentnrs = array_unique(array_filter(array_map('trim', preg_split("/[\s,;]+/", $studentNummers))));

			//Pluk alle normale leden uit de database
			$lidnrs = $WSW4DB->q('KEYVALUETABLE SELECT `studentnr`, `contactID` as lidnr '
							   .'FROM `Lid` '
							   .'WHERE `lidToestand`="LID" '
							   .'AND `lidTot` IS NULL '
							   .'ORDER BY `studentnr` ASC');
			$notfound =	array();
			$found = array();

			//Haal alle studerenden uit de ledenlijst
			//We werken uiteraard met lidnrs en niet met lidobjecten, veel te traag!
			foreach($studentnrs as $studnr)
			{
				if(array_key_exists($studnr, $lidnrs)) 
				{
					unset($lidnrs[$studnr]);
				} 
				else 
				{
					// houdt ook bij welke studerenden niet in de ledenDB voorkomen
					$notfound[] = $studnr;
				}
			}

			//Wie nu nog in de ledenlijst zit, studeert blijkbaar niet!
			//Stop ze in de kart.
			Kart::geef()->voegPersonenToe(PersoonVerzameling::verzamel($lidnrs));

			// Vertel wie wel in de database zit maar niet meer studeert (dus lid af maken)
			if(count($lidnrs) > 0)
			{
				$body->add(new HtmlParagraph(sprintf(
					_("Er zijn %s leden in de ledendatabase waarvan je zojuist niet het studentnummer hebt ingevoerd. Deze mensen hebben geen bijzondere status, en nog geen lid-af-datum. Ze zitten nu in de kart om lid-af gemaakt te worden, maar let op: Misschien staat er wel een opmerking bij hen met een reden waarom ze lid moeten blijven!"), new HtmlStrong(count($lidnrs))
				)));
				$body->add(new HtmlParagraph(sprintf(
					//VOC: Kart & lid-af
					_("Bekijk de %s, of maak ze direct %s."),
					new HtmlAnchor('/Service/Kart', _("kart")), new HtmlAnchor('/Leden/Administratie/Lidaf', _("lid-af"))
				)));
			} else {
				$body->add(new HtmlParagraph(_("Ongelooflijk, er zijn geen leden in de database die daar niet zouden horen. Goed werk, Secretaris!")));
			}

			$body->add(new HtmlHR());

			// Vertel wie wel studeert maar niet in de ledendatabase zit
			if(count($notfound) > 0)
			{
				$body->add(new HtmlParagraph(array(
					new HtmlStrong(_('Er zijn mensen die wel studeren maar niet in de ledenadministratie staan.')),
					_('Dit kan omdat ze niet lid zijn, lid-af zijn of een bijzondere toestand hebben.'),
					' ',
					_('De volgende studentnrs zijn niet gevonden:'),
					implode('; ', $notfound)
				)));
			}
			else
			{
				$body->add(new HtmlParagraph(_("Wowie, iedereen die je ingevoerd hebt is ook daadwerkelijk lid!")));
			}
		}
		
		$body->add(new HtmlHR());
		
		//Vertel wie geen studentnummer heeft (ook ballen)
		$ledenZonderNummer = $WSW4DB->q('COLUMN SELECT `contactID`'
					.	'FROM `Lid` '
					.	'WHERE `studentnr` IS NULL '
					.	'AND '. WSW4_WHERE_NORMAALLID
					);

		$body->add(new HtmlParagraph(
			sprintf(_("%d leden hebben geen studentnr dus worden niet meegenomen in dit proces."), count($ledenZonderNummer))
		));
		$body->add(new HtmlAnchor('/Leden/?lidnrs='.implode(',', $ledenZonderNummer),
			_('Klik hier om de leden zonder studentnr te bekijken.')));
		
		// Vertel welke leden niet studeren maar wel een bijzondere status hebben
		$notselect = $WSW4DB->q('COLUMN SELECT `contactID` '
							.'FROM `Lid` '
							.'WHERE `lidToestand` != "LID" AND ' . WSW4_WHERE_LID);

		$body->add(new HtmlParagraph(
			sprintf(_("%d leden hebben een bijzondere status en worden daarom <i>niet</i> meegenomen in dit proces."), count($notselect))
		));
		$body->add(new HtmlAnchor("/Leden/?lidnrs="	. implode(',',$notselect), _("Bekijk deze leden")));
		
		Page::getInstance()->add($body)->end();
	}

	public static function naamfix()
	{
		requireAuth('bestuur');

		$page = Page::getInstance();

		$checkNaam = PersoonVerzameling::checkNaam();

		//Check voor alle dingen die we potentieel willen verbeteren of dit ook moet gebeuren
		// en doe dit zo nodig.
		$totaal = 0;
		foreach($checkNaam as $persoon)
		{
			if(trypar($persoon->geefID())){
				$nieuweNaam = Persoon::naamsuggestie($persoon);
				$persoon->setVoornaam($nieuweNaam['voornaam'])
					->setVoorletters($nieuweNaam['voorletters'])
					->setTussenvoegsels($nieuweNaam['tussenvoegsels'])
					->setAchternaam($nieuweNaam['achternaam'])
					->opslaan();
				$totaal++;
			}
		}

		if($totaal)
			Page::addMelding(_("Er zijn $totaal personen aangepast."));

		//Refresh wat er nog aan fouten overblijft.
		$checkNaam = PersoonVerzameling::checkNaam();

		$page->start()
			->add(new HtmlParagraph("Onderstaande lijst bevalt alle namen van personen in de database die mogelijk een probleem hebben in hun naam. Vervolgens wordt er een voorstel voor een fix gedaan die je aan kan vinken als je deze wilt doorvoeren."))
			->add(PersoonVerzamelingView::naamFixTabel($checkNaam));

		$page->end();
	}

    public static function geluksvogels()
    {
		global $uriRef;

        requireAuth('bestuur');

        $geluksvogels = LidVerzameling::geluksvogels(tryPar('datum', NULL));

		if(tryPar('printable', null)) {
			$page = Page::getInstance('cleanhtml')->start();
			$page->add(LidVerzamelingView::geluksvogels($geluksvogels, true));
			$page->end();
		} else {
			$page = Page::getInstance()->start();

			$uriRef['params']["printable"] = "true";
			$page->add(new HtmlAnchor($uriRef['request'] . '?' . http_build_query($uriRef['params']), _('Bekijk ook in printbaar overzicht')));

			$page->add(LidVerzamelingView::geluksvogels($geluksvogels));

			$page->end();
		}
    }

	/**
	 *  Een formulier maken en verwerken om veel leden tegelijk te bewerken.
	 */
	public static function massamodificatie()
	{
		requireAuth('bestuur');

		$page = Page::getInstance()->start();
		$page->add(new HtmlParagraph(_("Deze massabewerkingstool doet precies wat jij aan de tool vertelt wat er moet gebeuren. Let op dat niet altijd is wat je wilt zeggen, en dat niet altijd is wat je bedoelt, en dat niet altijd is wat je wilt dat er gebeurt. Als je niet oppast heb je zo honderden lidgegevens foutgemaakt. Bezint eer ge massamodificeert!"), 'waarschuwing'));

		if (Token::processNamedForm() == 'massamodificatie') {
			$page->add(new HtmlHeader(3, _("De uitkomsten zijn bekend!")));
			$page->add(LidVerzamelingView::processmassamodificatieForm());
		}

		$page->add(new HtmlHeader(3, _("Start een nieuwe modificatie")));
		$page->add(new HtmlParagraph(_("Dit is een tool waarmee je een verzameling leden in een keer aan kan passen. Het voordeel is dat lidmaken aan de hand van een lijstje lidnummers, of een foute lidvanaf-datum in een keer opgelost kan worden. We checken op de vorige waarde zodat je bijvoorbeeld niet per ongeluk oud-leden opnieuw lid maakt.")));
		$form = HtmlForm::named('massamodificatie');
		$form->add(LidVerzamelingView::massamodificatieForm());
		$form->add(HtmlInput::makeSubmitButton(_("Ik weet heel zeker dat dit klopt!")));
		$page->add($form);

		$page->end();
	}

	/**
	 *  Zoek alle eerstejaars van het gegeven jaar die actief zijn geworden.
	 */
	public static function actieveEerstejaars()
	{
		requireAuth('bestuur');

		$page = Page::getInstance()->start();

		// Welk collegejaar gaat het over?
		$jaar = trypar('jaar', colJaar());
		// Wat voor soort actieve leden gaat het over?
		$alleenCies = trypar('alleenCies', false);

		// Formuliertje om het jaar te selecteren
		$page->add($form = new HtmlForm("GET"));
		$form->add(new HtmlParagraph(_("Selecteer van welk collegejaar je de actief geworden eerstejaars wilt zien:")));
		$form->add(HtmlInput::makeNumber("jaar", $jaar));
		$form->add(new HtmlSpan(_("Groepen en disputen tellen niet mee: ")));
		$form->add(HtmlInput::makeCheckbox("alleenCies", $alleenCies));
		$form->add(HtmlInput::makeSubmitButton(_("Toon die eerstejaars!")));

		$begindatum = new DateTimeLocale($jaar . "-08-01"); // begin iets voor september
		$einddatum = new DateTimeLocale(($jaar + 1) . "-08-01");

		//TODO: Maak functie die collegejaren omzet naar begindata

		$kwerrie = CommissieLidQuery::table()
			->join("Lid", "persoon_contactID", "Lid.contactID", "INNER JOIN")
			->join("Commissie", NULL, NULL, "INNER JOIN")
			->whereProp("CommissieLid.datumBegin", ">=", $begindatum)
			->whereProp("CommissieLid.datumBegin", "<", $einddatum)
			->whereProp("Lid.lidVan", ">=", $begindatum)
			->whereProp("Lid.lidVan", "<", $einddatum);
		if ($alleenCies) {
			$kwerrie = $kwerrie->whereString("Commissie.soort", "CIE");
		}
		$actieveLeden = $kwerrie->verzamel()->toPersoonVerzameling();

		$page->add(new HtmlParagraph(_("Alle lidnummers, om makkelijk te copypasten:")));
		$page->add(new HtmlParagraph(implode(", ", $actieveLeden->keys())));

		$page->add(LidVerzamelingView::lijst($actieveLeden));

		$page->end();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
