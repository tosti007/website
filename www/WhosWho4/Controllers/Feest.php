<?php

abstract class Feest_Controller
{

	/**
	 * Controller die ervoor zorgt dat je de webcie-almanakstukje achievement krijgt
	 * en redirect daarna naar een publisherpagina
	 */
	static public function feestje()
	{
		$pers = Persoon::getIngelogd();
		if($pers)
		{
			$persBadges = $pers->getBadges();
			$persBadges->addBadge('almanakstukje');
		}

		Page::redirect('/Vereniging/Commissies/www/feestje.html');
	}
}
