<?php

use Symfony\Component\HttpFoundation\Response;

abstract class DibsTablet_Controller
{
	/**
	 * @brief Inlogpagina voor de dibstabletgebruiker.
	 *
	 * Je kan inloggen met loginnaam en wachtwoord,
	 * of door de pas van een god/bestuur te scannen.
	 *
	 * @site{/space/auth/dibstabletlogin.php}
	 */
	public static function login()
	{
		global $auth;
		global $session;
		global $DIBSTABLETNR;


		if (!DEBUG && !$auth->hasTabletAgent())
		{
			return responseUitStatusCode(403);
		}

		// construeer nextpage, waarheen we weer redirecten na het inloggen
		$nextpage = HTTPS_ROOT . tryPar('next');

		$lidnr = null;

		// hier gebeurt het daadwerkelijke checken, resultaat is het lidnr
		// bij succes
		$ww = Wachtwoord::geefLogin(tryPar('loginnaam'));
		if ($ww)
		{
			$lidnr = $ww->authenticate(tryPar('password'));
		}

		if (!is_null($lidnr))
		{
			// wachtwoord klopt niet, hoort de pas bij 1 van de bestuursleden/goden?
			$pas = Pas::geefRfidTag(tryPar('password', ''));
			if (!is_null($pas))
			{
				$persoon = $pas->getPersoonContactId();
				if ($auth->isGodAchtig($persoon) || $auth->isBestuur($persoon))
				{
					// Als iemand van bestuur of webcie zijn DiBS-tag scant bij de
					// tablet, dan kan de tablet door DiBS'ers worden gebruikt
					// totdat de tablet weer wordt gereboot
					$lidnr = $DIBSTABLETNR;
				}
			}

			if ($lidnr != $DIBSTABLETNR)
			{
				return responseRedirect('/Leden/Dibs/DibsTablet/ToonTablet?loginfout=true');
			}
		}

		// Voorkom session fixation aanval
		$session->migrate(true);

		// inloggen gelukt, zet session variabelen op basis daarvan.
		$session->set('mylidnr', $lidnr);
		$session->set('veilig', true);

		return responseRedirect($nextpage);
	}

	static public function tabletGooiCookiesWeg()
	{
		global $session;

		$session->invalidate();

		return responseRedirect('/Leden/Dibs/DibsTablet/ToonTablet');
	}

	static public function toonTablet()
	{
		global $auth;

		if (!DEBUG && !$auth->hasTabletAgent())
		{
			spaceHttp(403);
		}

		if (!hasAuth('tablet'))
		{
			Page::getInstance('cleanhtml')->start();
			include('WhosWho4/Views/DibsTablet/login.php');
			Page::getInstance('cleanhtml')->end();
		}

		$trigger = 'onclick';

		$producten = ColaProductVerzameling::getActieveColaProducten();
		if ($producten->aantal() == 0)
		{
			echo "Geen producten gevonden\n";
			exit(1);
		}

		Page::getInstance('cleanhtml')->start();
		include('WhosWho4/Views/DibsTablet/toon.php');
		Page::getInstance('cleanhtml')->end();
	}


	// Hier onder zijn allemaal AJAX-requests die gedaan worden vanuit de ToonTablet pagina:

	static public function checkPas()
	{
		global $request;

		if (is_null($request->request->get('rfid')))
		{
			return new JSONResponse(array('exit' => '2', 'msg' => 'geen geldige postdata'));
		}

		$rfid = $request->request->get('rfid');

		if (!($pas = Pas::geefRfidTag($rfid)))
		{
			return new JSONResponse(array('exit' => '1', 'msg' => 'Geen geldige pas!'));
		}

		if (!$pas->getActief())
		{
			return new JSONResponse(array('exit' => '1', 'msg' => 'Geen geldige pas!'));
		}

		$persoonID = $pas->getPersoonContactID();
		$info = DibsInfo::geef($persoonID);
		if (!$info)
		{
			return new JSONResponse(array('exit' => '2', 'msg' => "Persoon $persoonID nog niet geactiveerd voor DiBS!"));
		}

		$persoon = Persoon::geef($persoonID);
		$naam = $persoon->getVoornaam();
		$kudos = $info->getKudos();

		return new JSONResponse(array('exit' => '0', 'lidnr' => "$persoonID", 'pasID' => $pas->getPasID(), 'naam' => "$naam", 'kudos' => "$kudos"));
	}

	static public function checkBarcode()
	{
		global $request;

		if (is_null($request->request->get('barcode')))
		{
			return new JSONResponse(array('exit' => '3', 'msg' => 'geen geldige postdata'));
			return;
		}

		$code = $request->request->get('barcode');

		if ($code == '')
		{
			return new JSONResponse(array('exit' => '3', 'msg' => 'geen geldige postdata'));
		}
		elseif (strlen($code) != 13 && strlen($code) != 8)
		{
			return new JSONResponse(array('exit' => '3', 'msg' => 'geen geldige postdata'));
		}
		elseif (Pas::geefRfidTag($code))
		{
			return new JSONResponse(array('exit' => '3', 'msg' => 'Er bestaat al een rfid-tag met deze code!'));
		}
		elseif (!($product = ColaProduct::geefDoorBarcode($code)))
		{
			return new JSONResponse(array('exit' => '1', 'msg' => 'Geen geldige barcode!'));
		}

		$voorraad = $product->getDefaultVoorraad();
		if (!($voorraad->getVerkoopbaar()))
		{
			return new JSONResponse(array('exit' => '3', 'msg' => 'Deze barcode hoort bij een niet-verkoopbaar product'));
		}

		// als alles is goed gegaan:
		$artikelID = $product->getArtikelID();
		return new JSONResponse(array('exit' => '0', 'artikelid' => "$artikelID"));
	}

	static public function addBarcode()
	{
		global $request;

		if (is_null($request->request->get('barcode')) || is_null($request->request->get('productid')))
		{
			return new JSONResponse(array('exit' => '2', 'msg' => 'geen geldige postdata'));
		}

		$code = $request->request->get('barcode');
		$productid = $request->request->get('productid');

		if ($product = ColaProduct::geefDoorBarcode($code))
		{
			return new JSONResponse(array('exit' => '1', 'msg' => 'De barcode is plots toegevoegd!'));
		}

		$artikel = Artikel::geef($productid);
		$voorraad = $artikel->getDefaultVoorraad();
		$barcode = new Barcode($voorraad, $code);
		$barcode->opslaan();

		return new JSONResponse(array('exit' => '0', 'msg' => 'Alles klopt!'));
	}

	static public function maakTransactie()
	{
		global $request;

		if (is_null($request->request->get('lidnr'))
			|| is_null($request->request->get('pasID'))
			|| is_null($request->request->get('kudos'))
			|| is_null($request->request->get('producten')))
		{
			return new JSONResponse(array('exit' => '20', 'msg' => 'geen geldige postdata'));
		}
		$lidnr = (int)$request->request->get('lidnr');
		$pasnr = (int)$request->request->get('pasID');
		$kudos = (int)$request->request->get('kudos');
		$producten = $request->request->get('producten');

		$bron = 'TABLET';

		$pas = Pas::geef($pasnr);

		if ($kudos <= 0)
		{
			return new JSONResponse(array('exit' => '30', 'msg' => 'transactie met niet-positief aantal kudos'));
		}
		elseif (!($persoon = Persoon::geef($lidnr)) || !($info = DibsInfo::geef($persoon)))
		{
			return new JSONResponse(array('exit' => '40', 'msg' => 'lid niet gevonden'));
		}
		elseif (!$pas)
		{
			return new JSONResponse(array('exit' => '50', 'msg' => 'pas komt niet overeen'));
		}
		elseif (!($pas->getActief()))
		{
			return new JSONResponse(array('exit' => '60', 'msg' => 'pas is niet actief'));
		}

		$transactie = new DibsTransactie('COLAKAS'
			, 'VERKOOP'
			, $persoon
			, $kudos / EURO_TO_KUDOS
			, ''
			, new DateTimeLocale()
			, 'TABLET'
			, $pas);
		if (!($error = $info->nieuweTransactie($transactie)))
		{
			$info->opslaan();
			$verkopen = VerkoopVerzameling::vanString($producten, $transactie, 'VIRTUEEL');
			if (!$verkopen->allValid())
			{
				user_error('error in verkopenvalidatie: ' . print_r($verkopen->allErrors(), TRUE), E_USER_ERROR);
			}
			$verkopen->opslaan();
		}
		else
		{
			return new JSONResponse(array('exit' => 50, 'msg' => $error));
		}

		return new JSONResponse(array('exit' => '0'));
	}

	static public function loguit()
	{
		// TODO SOEPMES: wat is nou eigenlijk de bedoeling van deze functie?
		// Lijkt me dat de tablet zelf ook wel requestdata kan vergeten...

		global $request;

		if (is_null($request->request->get('lidnr')))
		{
			return new JSONResponse(array('exit' => '10'));
		}

		// TODO: waarom is deze code zo dubbelop?
		$lidID = $request->request->get('lidnr');
		if ($lidID == 'undefined' || $lidID == '0')
		{
			// Nothing to do here!
			return new JSONResponse(array('exit' => '0'));
		}
		else
		{
			return new JSONResponse(array('exit' => '0'));
		}
	}

	static public function tabletJavascript()
	{
		header("Content-type: text/javascript; encoding=utf-8");
		ob_start();
		include('WhosWho4/Views/DibsTablet/DibsTablet.js');
		return new Response(ob_get_clean());
	}
}
