<?php

abstract class CSP_Controller
{
	public static function report()
	{
		global $request;

		//De browser post de data naar deze pagina, maar niet als een querystring maar als directe data
		//Daarom gaan we de input stream uitlezen
		$json = file_get_contents('php://input');
		if ($json === false) 
		{
			//Er is niets gepost -> bad request
			spaceHttp(400);
		}

		$csp = json_decode($json, true);
		if (is_null($csp) || !$csp['csp-report']) 
		{
			//Geen valide JSON
			spaceHttp(400);
		}

		//Steel de gebruiker zijn privacy
		if(!$request->server->get('REMOTE_HOST')) {
			$rhost = gethostbyaddr($request->server->get('HTTP_X_REAL_IP'));
		} else {
			$rhost = $request->server->get('HTTP_X_REAL_IP');
		}

		$report = new CSPReport();

		$report->setIp("$rhost" . " (" . $request->server->get('HTTP_X_REAL_IP') .")");
		$report->setUserAgent($request->server->get('HTTP_USER_AGENT') ? $reqeust->server->get('HTTP_USER_AGENT') : 'unknown');

		$report->setDocumentUri(		$csp['csp-report']['document-uri'] 		 ?: 'none');
		$report->setReferrer(			$csp['csp-report']['referrer'] 			 ?: 'none');
		$report->setBlockedUri(			$csp['csp-report']['blocked-uri'] 		 ?: 'none');
		$report->setViolatedDirective(	$csp['csp-report']['violated-directive'] ?: 'none');
		$report->setOriginalPolicy(		$csp['csp-report']['original-policy'] 	 ?: 'none');

		$report->opslaan();

		exit();
	}

}
