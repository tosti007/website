<?
function wsw4_autoload($class)
{
	switch($class)
	{
		case 'Constraint':
			require_once('WhosWho4/Basis/Constraint.cls.php');
			break;
		case 'ConstraintVerzameling':
			require_once('WhosWho4/Basis/ConstraintVerzameling.cls.php');
			break;
		case 'ConstraintVerzameling_Generated':
			require_once('WhosWho4/Basis/Generated/ConstraintVerzameling.gen.php');
			break;
		case 'Constraint_Generated':
			require_once('WhosWho4/Basis/Generated/Constraint.gen.php');
			break;
		case 'DBObject':
			require_once('WhosWho4/Basis/DBObject.abs.php');
			break;
		case 'DBObject_Generated':
			require_once('WhosWho4/Basis/Generated/DBObject.gen.php');
			break;
		case 'Entiteit':
			require_once('WhosWho4/Basis/Entiteit.abs.php');
			break;
		case 'Entiteit_Generated':
			require_once('WhosWho4/Basis/Generated/Entiteit.gen.php');
			break;
		case 'Query':
			require_once('WhosWho4/Basis/Query.cls.php');
			break;
		case 'QueryBuilder':
			require_once('WhosWho4/Basis/QueryBuilder.cls.php');
			break;
		case 'QueryBuilder_Generated':
			require_once('WhosWho4/Basis/Generated/QueryBuilder.gen.php');
			break;
		case 'Query_Generated':
			require_once('WhosWho4/Basis/Generated/Query.gen.php');
			break;
		case 'Verzameling':
			require_once('WhosWho4/Basis/Verzameling.cls.php');
			break;
		case 'Verzameling_Generated':
			require_once('WhosWho4/Basis/Generated/Verzameling.gen.php');
			break;
		case 'View':
			require_once('WhosWho4/Basis/View.abs.php');
			break;
		case 'View_Generated':
			require_once('WhosWho4/Basis/Generated/View.gen.php');
			break;

		case 'AesArtikel':
			require_once('WhosWho4/Boekweb2/DBObject/AesArtikel.cls.php');
			break;
		case 'AesArtikelQuery':
			require_once('WhosWho4/Boekweb2/Query/AesArtikel.query.php');
			break;
		case 'AesArtikelQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/AesArtikelQuery.gen.php');
			break;
		case 'AesArtikelVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/AesArtikelVerzameling.cls.php');
			break;
		case 'AesArtikelVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/AesArtikelVerzameling.view.php');
			break;
		case 'AesArtikelVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/AesArtikelVerzamelingView.gen.php');
			break;
		case 'AesArtikelVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/AesArtikelVerzameling.gen.php');
			break;
		case 'AesArtikelView':
			require_once('WhosWho4/Boekweb2/View/AesArtikel.view.php');
			break;
		case 'AesArtikelView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/AesArtikelView.gen.php');
			break;
		case 'AesArtikel_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/AesArtikel.gen.php');
			break;
		case 'Artikel':
			require_once('WhosWho4/Boekweb2/DBObject/Artikel.cls.php');
			break;
		case 'ArtikelQuery':
			require_once('WhosWho4/Boekweb2/Query/Artikel.query.php');
			break;
		case 'ArtikelQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/ArtikelQuery.gen.php');
			break;
		case 'ArtikelVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/ArtikelVerzameling.cls.php');
			break;
		case 'ArtikelVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/ArtikelVerzameling.view.php');
			break;
		case 'ArtikelVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/ArtikelVerzamelingView.gen.php');
			break;
		case 'ArtikelVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/ArtikelVerzameling.gen.php');
			break;
		case 'ArtikelView':
			require_once('WhosWho4/Boekweb2/View/Artikel.view.php');
			break;
		case 'ArtikelView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/ArtikelView.gen.php');
			break;
		case 'Artikel_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Artikel.gen.php');
			break;
		case 'Barcode':
			require_once('WhosWho4/Boekweb2/DBObject/Barcode.cls.php');
			break;
		case 'BarcodeQuery':
			require_once('WhosWho4/Boekweb2/Query/Barcode.query.php');
			break;
		case 'BarcodeQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/BarcodeQuery.gen.php');
			break;
		case 'BarcodeVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/BarcodeVerzameling.cls.php');
			break;
		case 'BarcodeVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/BarcodeVerzameling.view.php');
			break;
		case 'BarcodeVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BarcodeVerzamelingView.gen.php');
			break;
		case 'BarcodeVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/BarcodeVerzameling.gen.php');
			break;
		case 'BarcodeView':
			require_once('WhosWho4/Boekweb2/View/Barcode.view.php');
			break;
		case 'BarcodeView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BarcodeView.gen.php');
			break;
		case 'Barcode_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Barcode.gen.php');
			break;
		case 'Bestelling':
			require_once('WhosWho4/Boekweb2/DBObject/Bestelling.cls.php');
			break;
		case 'BestellingQuery':
			require_once('WhosWho4/Boekweb2/Query/Bestelling.query.php');
			break;
		case 'BestellingQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/BestellingQuery.gen.php');
			break;
		case 'BestellingVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/BestellingVerzameling.cls.php');
			break;
		case 'BestellingVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/BestellingVerzameling.view.php');
			break;
		case 'BestellingVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BestellingVerzamelingView.gen.php');
			break;
		case 'BestellingVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/BestellingVerzameling.gen.php');
			break;
		case 'BestellingView':
			require_once('WhosWho4/Boekweb2/View/Bestelling.view.php');
			break;
		case 'BestellingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BestellingView.gen.php');
			break;
		case 'Bestelling_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Bestelling.gen.php');
			break;
		case 'Boek':
			require_once('WhosWho4/Boekweb2/DBObject/Boek.cls.php');
			break;
		case 'BoekQuery':
			require_once('WhosWho4/Boekweb2/Query/Boek.query.php');
			break;
		case 'BoekQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/BoekQuery.gen.php');
			break;
		case 'BoekVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/BoekVerzameling.cls.php');
			break;
		case 'BoekVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/BoekVerzameling.view.php');
			break;
		case 'BoekVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BoekVerzamelingView.gen.php');
			break;
		case 'BoekVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/BoekVerzameling.gen.php');
			break;
		case 'BoekView':
			require_once('WhosWho4/Boekweb2/View/Boek.view.php');
			break;
		case 'BoekView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BoekView.gen.php');
			break;
		case 'Boek_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Boek.gen.php');
			break;
		case 'Boekverkoop':
			require_once('WhosWho4/Boekweb2/DBObject/Boekverkoop.cls.php');
			break;
		case 'BoekverkoopQuery':
			require_once('WhosWho4/Boekweb2/Query/Boekverkoop.query.php');
			break;
		case 'BoekverkoopQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/BoekverkoopQuery.gen.php');
			break;
		case 'BoekverkoopVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/BoekverkoopVerzameling.cls.php');
			break;
		case 'BoekverkoopVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/BoekverkoopVerzameling.view.php');
			break;
		case 'BoekverkoopVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BoekverkoopVerzamelingView.gen.php');
			break;
		case 'BoekverkoopVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/BoekverkoopVerzameling.gen.php');
			break;
		case 'BoekverkoopView':
			require_once('WhosWho4/Boekweb2/View/Boekverkoop.view.php');
			break;
		case 'BoekverkoopView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/BoekverkoopView.gen.php');
			break;
		case 'Boekverkoop_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Boekverkoop.gen.php');
			break;
		case 'Dictaat':
			require_once('WhosWho4/Boekweb2/DBObject/Dictaat.cls.php');
			break;
		case 'DictaatOrder':
			require_once('WhosWho4/Boekweb2/DBObject/DictaatOrder.cls.php');
			break;
		case 'DictaatOrderQuery':
			require_once('WhosWho4/Boekweb2/Query/DictaatOrder.query.php');
			break;
		case 'DictaatOrderQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/DictaatOrderQuery.gen.php');
			break;
		case 'DictaatOrderVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/DictaatOrderVerzameling.cls.php');
			break;
		case 'DictaatOrderVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/DictaatOrderVerzameling.view.php');
			break;
		case 'DictaatOrderVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/DictaatOrderVerzamelingView.gen.php');
			break;
		case 'DictaatOrderVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/DictaatOrderVerzameling.gen.php');
			break;
		case 'DictaatOrderView':
			require_once('WhosWho4/Boekweb2/View/DictaatOrder.view.php');
			break;
		case 'DictaatOrderView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/DictaatOrderView.gen.php');
			break;
		case 'DictaatOrder_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/DictaatOrder.gen.php');
			break;
		case 'DictaatQuery':
			require_once('WhosWho4/Boekweb2/Query/Dictaat.query.php');
			break;
		case 'DictaatQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/DictaatQuery.gen.php');
			break;
		case 'DictaatVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/DictaatVerzameling.cls.php');
			break;
		case 'DictaatVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/DictaatVerzameling.view.php');
			break;
		case 'DictaatVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/DictaatVerzamelingView.gen.php');
			break;
		case 'DictaatVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/DictaatVerzameling.gen.php');
			break;
		case 'DictaatView':
			require_once('WhosWho4/Boekweb2/View/Dictaat.view.php');
			break;
		case 'DictaatView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/DictaatView.gen.php');
			break;
		case 'Dictaat_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Dictaat.gen.php');
			break;
		case 'Giro':
			require_once('WhosWho4/Boekweb2/DBObject/Giro.cls.php');
			break;
		case 'GiroQuery':
			require_once('WhosWho4/Boekweb2/Query/Giro.query.php');
			break;
		case 'GiroQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/GiroQuery.gen.php');
			break;
		case 'GiroVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/GiroVerzameling.cls.php');
			break;
		case 'GiroVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/GiroVerzameling.view.php');
			break;
		case 'GiroVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/GiroVerzamelingView.gen.php');
			break;
		case 'GiroVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/GiroVerzameling.gen.php');
			break;
		case 'GiroView':
			require_once('WhosWho4/Boekweb2/View/Giro.view.php');
			break;
		case 'GiroView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/GiroView.gen.php');
			break;
		case 'Giro_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Giro.gen.php');
			break;
		case 'Jaargang':
			require_once('WhosWho4/Boekweb2/DBObject/Jaargang.cls.php');
			break;
		case 'JaargangArtikel':
			require_once('WhosWho4/Boekweb2/DBObject/JaargangArtikel.cls.php');
			break;
		case 'JaargangArtikelQuery':
			require_once('WhosWho4/Boekweb2/Query/JaargangArtikel.query.php');
			break;
		case 'JaargangArtikelQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/JaargangArtikelQuery.gen.php');
			break;
		case 'JaargangArtikelVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/JaargangArtikelVerzameling.cls.php');
			break;
		case 'JaargangArtikelVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/JaargangArtikelVerzameling.view.php');
			break;
		case 'JaargangArtikelVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/JaargangArtikelVerzamelingView.gen.php');
			break;
		case 'JaargangArtikelVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/JaargangArtikelVerzameling.gen.php');
			break;
		case 'JaargangArtikelView':
			require_once('WhosWho4/Boekweb2/View/JaargangArtikel.view.php');
			break;
		case 'JaargangArtikelView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/JaargangArtikelView.gen.php');
			break;
		case 'JaargangArtikel_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/JaargangArtikel.gen.php');
			break;
		case 'JaargangQuery':
			require_once('WhosWho4/Boekweb2/Query/Jaargang.query.php');
			break;
		case 'JaargangQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/JaargangQuery.gen.php');
			break;
		case 'JaargangVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/JaargangVerzameling.cls.php');
			break;
		case 'JaargangVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/JaargangVerzameling.view.php');
			break;
		case 'JaargangVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/JaargangVerzamelingView.gen.php');
			break;
		case 'JaargangVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/JaargangVerzameling.gen.php');
			break;
		case 'JaargangView':
			require_once('WhosWho4/Boekweb2/View/Jaargang.view.php');
			break;
		case 'JaargangView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/JaargangView.gen.php');
			break;
		case 'Jaargang_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Jaargang.gen.php');
			break;
		case 'Leverancier':
			require_once('WhosWho4/Boekweb2/DBObject/Leverancier.cls.php');
			break;
		case 'LeverancierQuery':
			require_once('WhosWho4/Boekweb2/Query/Leverancier.query.php');
			break;
		case 'LeverancierQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/LeverancierQuery.gen.php');
			break;
		case 'LeverancierRetour':
			require_once('WhosWho4/Boekweb2/DBObject/LeverancierRetour.cls.php');
			break;
		case 'LeverancierRetourQuery':
			require_once('WhosWho4/Boekweb2/Query/LeverancierRetour.query.php');
			break;
		case 'LeverancierRetourQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/LeverancierRetourQuery.gen.php');
			break;
		case 'LeverancierRetourVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/LeverancierRetourVerzameling.cls.php');
			break;
		case 'LeverancierRetourVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/LeverancierRetourVerzameling.view.php');
			break;
		case 'LeverancierRetourVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/LeverancierRetourVerzamelingView.gen.php');
			break;
		case 'LeverancierRetourVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/LeverancierRetourVerzameling.gen.php');
			break;
		case 'LeverancierRetourView':
			require_once('WhosWho4/Boekweb2/View/LeverancierRetour.view.php');
			break;
		case 'LeverancierRetourView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/LeverancierRetourView.gen.php');
			break;
		case 'LeverancierRetour_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/LeverancierRetour.gen.php');
			break;
		case 'LeverancierVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/LeverancierVerzameling.cls.php');
			break;
		case 'LeverancierVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/LeverancierVerzameling.view.php');
			break;
		case 'LeverancierVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/LeverancierVerzamelingView.gen.php');
			break;
		case 'LeverancierVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/LeverancierVerzameling.gen.php');
			break;
		case 'LeverancierView':
			require_once('WhosWho4/Boekweb2/View/Leverancier.view.php');
			break;
		case 'LeverancierView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/LeverancierView.gen.php');
			break;
		case 'Leverancier_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Leverancier.gen.php');
			break;
		case 'Levering':
			require_once('WhosWho4/Boekweb2/DBObject/Levering.cls.php');
			break;
		case 'LeveringQuery':
			require_once('WhosWho4/Boekweb2/Query/Levering.query.php');
			break;
		case 'LeveringQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/LeveringQuery.gen.php');
			break;
		case 'LeveringVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/LeveringVerzameling.cls.php');
			break;
		case 'LeveringVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/LeveringVerzameling.view.php');
			break;
		case 'LeveringVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/LeveringVerzamelingView.gen.php');
			break;
		case 'LeveringVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/LeveringVerzameling.gen.php');
			break;
		case 'LeveringView':
			require_once('WhosWho4/Boekweb2/View/Levering.view.php');
			break;
		case 'LeveringView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/LeveringView.gen.php');
			break;
		case 'Levering_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Levering.gen.php');
			break;
		case 'Offerte':
			require_once('WhosWho4/Boekweb2/DBObject/Offerte.cls.php');
			break;
		case 'OfferteQuery':
			require_once('WhosWho4/Boekweb2/Query/Offerte.query.php');
			break;
		case 'OfferteQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/OfferteQuery.gen.php');
			break;
		case 'OfferteVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/OfferteVerzameling.cls.php');
			break;
		case 'OfferteVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/OfferteVerzameling.view.php');
			break;
		case 'OfferteVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/OfferteVerzamelingView.gen.php');
			break;
		case 'OfferteVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/OfferteVerzameling.gen.php');
			break;
		case 'OfferteView':
			require_once('WhosWho4/Boekweb2/View/Offerte.view.php');
			break;
		case 'OfferteView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/OfferteView.gen.php');
			break;
		case 'Offerte_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Offerte.gen.php');
			break;
		case 'Order':
			require_once('WhosWho4/Boekweb2/DBObject/Order.cls.php');
			break;
		case 'OrderQuery':
			require_once('WhosWho4/Boekweb2/Query/Order.query.php');
			break;
		case 'OrderQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/OrderQuery.gen.php');
			break;
		case 'OrderVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/OrderVerzameling.cls.php');
			break;
		case 'OrderVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/OrderVerzameling.view.php');
			break;
		case 'OrderVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/OrderVerzamelingView.gen.php');
			break;
		case 'OrderVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/OrderVerzameling.gen.php');
			break;
		case 'OrderView':
			require_once('WhosWho4/Boekweb2/View/Order.view.php');
			break;
		case 'OrderView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/OrderView.gen.php');
			break;
		case 'Order_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Order.gen.php');
			break;
		case 'Periode':
			require_once('WhosWho4/Boekweb2/DBObject/Periode.cls.php');
			break;
		case 'PeriodeQuery':
			require_once('WhosWho4/Boekweb2/Query/Periode.query.php');
			break;
		case 'PeriodeQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/PeriodeQuery.gen.php');
			break;
		case 'PeriodeVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/PeriodeVerzameling.cls.php');
			break;
		case 'PeriodeVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/PeriodeVerzameling.view.php');
			break;
		case 'PeriodeVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/PeriodeVerzamelingView.gen.php');
			break;
		case 'PeriodeVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/PeriodeVerzameling.gen.php');
			break;
		case 'PeriodeView':
			require_once('WhosWho4/Boekweb2/View/Periode.view.php');
			break;
		case 'PeriodeView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/PeriodeView.gen.php');
			break;
		case 'Periode_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Periode.gen.php');
			break;
		case 'Pin':
			require_once('WhosWho4/Boekweb2/DBObject/Pin.cls.php');
			break;
		case 'PinQuery':
			require_once('WhosWho4/Boekweb2/Query/Pin.query.php');
			break;
		case 'PinQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/PinQuery.gen.php');
			break;
		case 'PinVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/PinVerzameling.cls.php');
			break;
		case 'PinVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/PinVerzameling.view.php');
			break;
		case 'PinVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/PinVerzamelingView.gen.php');
			break;
		case 'PinVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/PinVerzameling.gen.php');
			break;
		case 'PinView':
			require_once('WhosWho4/Boekweb2/View/Pin.view.php');
			break;
		case 'PinView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/PinView.gen.php');
			break;
		case 'Pin_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Pin.gen.php');
			break;
		case 'Schuld':
			require_once('WhosWho4/Boekweb2/DBObject/Schuld.cls.php');
			break;
		case 'SchuldQuery':
			require_once('WhosWho4/Boekweb2/Query/Schuld.query.php');
			break;
		case 'SchuldQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/SchuldQuery.gen.php');
			break;
		case 'SchuldVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/SchuldVerzameling.cls.php');
			break;
		case 'SchuldVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/SchuldVerzameling.view.php');
			break;
		case 'SchuldVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/SchuldVerzamelingView.gen.php');
			break;
		case 'SchuldVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/SchuldVerzameling.gen.php');
			break;
		case 'SchuldView':
			require_once('WhosWho4/Boekweb2/View/Schuld.view.php');
			break;
		case 'SchuldView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/SchuldView.gen.php');
			break;
		case 'Schuld_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Schuld.gen.php');
			break;
		case 'Telling':
			require_once('WhosWho4/Boekweb2/DBObject/Telling.cls.php');
			break;
		case 'TellingItem':
			require_once('WhosWho4/Boekweb2/DBObject/TellingItem.cls.php');
			break;
		case 'TellingItemQuery':
			require_once('WhosWho4/Boekweb2/Query/TellingItem.query.php');
			break;
		case 'TellingItemQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/TellingItemQuery.gen.php');
			break;
		case 'TellingItemVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/TellingItemVerzameling.cls.php');
			break;
		case 'TellingItemVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/TellingItemVerzameling.view.php');
			break;
		case 'TellingItemVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/TellingItemVerzamelingView.gen.php');
			break;
		case 'TellingItemVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/TellingItemVerzameling.gen.php');
			break;
		case 'TellingItemView':
			require_once('WhosWho4/Boekweb2/View/TellingItem.view.php');
			break;
		case 'TellingItemView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/TellingItemView.gen.php');
			break;
		case 'TellingItem_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/TellingItem.gen.php');
			break;
		case 'TellingQuery':
			require_once('WhosWho4/Boekweb2/Query/Telling.query.php');
			break;
		case 'TellingQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/TellingQuery.gen.php');
			break;
		case 'TellingVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/TellingVerzameling.cls.php');
			break;
		case 'TellingVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/TellingVerzameling.view.php');
			break;
		case 'TellingVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/TellingVerzamelingView.gen.php');
			break;
		case 'TellingVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/TellingVerzameling.gen.php');
			break;
		case 'TellingView':
			require_once('WhosWho4/Boekweb2/View/Telling.view.php');
			break;
		case 'TellingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/TellingView.gen.php');
			break;
		case 'Telling_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Telling.gen.php');
			break;
		case 'Transactie':
			require_once('WhosWho4/Boekweb2/DBObject/Transactie.cls.php');
			break;
		case 'TransactieQuery':
			require_once('WhosWho4/Boekweb2/Query/Transactie.query.php');
			break;
		case 'TransactieQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/TransactieQuery.gen.php');
			break;
		case 'TransactieVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/TransactieVerzameling.cls.php');
			break;
		case 'TransactieVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/TransactieVerzameling.view.php');
			break;
		case 'TransactieVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/TransactieVerzamelingView.gen.php');
			break;
		case 'TransactieVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/TransactieVerzameling.gen.php');
			break;
		case 'TransactieView':
			require_once('WhosWho4/Boekweb2/View/Transactie.view.php');
			break;
		case 'TransactieView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/TransactieView.gen.php');
			break;
		case 'Transactie_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Transactie.gen.php');
			break;
		case 'Vak':
			require_once('WhosWho4/Boekweb2/DBObject/Vak.cls.php');
			break;
		case 'VakQuery':
			require_once('WhosWho4/Boekweb2/Query/Vak.query.php');
			break;
		case 'VakQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VakQuery.gen.php');
			break;
		case 'VakStudie':
			require_once('WhosWho4/Boekweb2/DBObject/VakStudie.cls.php');
			break;
		case 'VakStudieQuery':
			require_once('WhosWho4/Boekweb2/Query/VakStudie.query.php');
			break;
		case 'VakStudieQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VakStudieQuery.gen.php');
			break;
		case 'VakStudieVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VakStudieVerzameling.cls.php');
			break;
		case 'VakStudieVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VakStudieVerzameling.view.php');
			break;
		case 'VakStudieVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VakStudieVerzamelingView.gen.php');
			break;
		case 'VakStudieVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VakStudieVerzameling.gen.php');
			break;
		case 'VakStudieView':
			require_once('WhosWho4/Boekweb2/View/VakStudie.view.php');
			break;
		case 'VakStudieView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VakStudieView.gen.php');
			break;
		case 'VakStudie_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/VakStudie.gen.php');
			break;
		case 'VakVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VakVerzameling.cls.php');
			break;
		case 'VakVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VakVerzameling.view.php');
			break;
		case 'VakVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VakVerzamelingView.gen.php');
			break;
		case 'VakVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VakVerzameling.gen.php');
			break;
		case 'VakView':
			require_once('WhosWho4/Boekweb2/View/Vak.view.php');
			break;
		case 'VakView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VakView.gen.php');
			break;
		case 'Vak_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Vak.gen.php');
			break;
		case 'Verkoop':
			require_once('WhosWho4/Boekweb2/DBObject/Verkoop.cls.php');
			break;
		case 'VerkoopPrijs':
			require_once('WhosWho4/Boekweb2/DBObject/VerkoopPrijs.cls.php');
			break;
		case 'VerkoopPrijsQuery':
			require_once('WhosWho4/Boekweb2/Query/VerkoopPrijs.query.php');
			break;
		case 'VerkoopPrijsQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VerkoopPrijsQuery.gen.php');
			break;
		case 'VerkoopPrijsVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VerkoopPrijsVerzameling.cls.php');
			break;
		case 'VerkoopPrijsVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VerkoopPrijsVerzameling.view.php');
			break;
		case 'VerkoopPrijsVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VerkoopPrijsVerzamelingView.gen.php');
			break;
		case 'VerkoopPrijsVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VerkoopPrijsVerzameling.gen.php');
			break;
		case 'VerkoopPrijsView':
			require_once('WhosWho4/Boekweb2/View/VerkoopPrijs.view.php');
			break;
		case 'VerkoopPrijsView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VerkoopPrijsView.gen.php');
			break;
		case 'VerkoopPrijs_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/VerkoopPrijs.gen.php');
			break;
		case 'VerkoopQuery':
			require_once('WhosWho4/Boekweb2/Query/Verkoop.query.php');
			break;
		case 'VerkoopQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VerkoopQuery.gen.php');
			break;
		case 'VerkoopVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VerkoopVerzameling.cls.php');
			break;
		case 'VerkoopVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VerkoopVerzameling.view.php');
			break;
		case 'VerkoopVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VerkoopVerzamelingView.gen.php');
			break;
		case 'VerkoopVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VerkoopVerzameling.gen.php');
			break;
		case 'VerkoopView':
			require_once('WhosWho4/Boekweb2/View/Verkoop.view.php');
			break;
		case 'VerkoopView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VerkoopView.gen.php');
			break;
		case 'Verkoop_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Verkoop.gen.php');
			break;
		case 'Verplaatsing':
			require_once('WhosWho4/Boekweb2/DBObject/Verplaatsing.cls.php');
			break;
		case 'VerplaatsingQuery':
			require_once('WhosWho4/Boekweb2/Query/Verplaatsing.query.php');
			break;
		case 'VerplaatsingQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VerplaatsingQuery.gen.php');
			break;
		case 'VerplaatsingVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VerplaatsingVerzameling.cls.php');
			break;
		case 'VerplaatsingVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VerplaatsingVerzameling.view.php');
			break;
		case 'VerplaatsingVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VerplaatsingVerzamelingView.gen.php');
			break;
		case 'VerplaatsingVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VerplaatsingVerzameling.gen.php');
			break;
		case 'VerplaatsingView':
			require_once('WhosWho4/Boekweb2/View/Verplaatsing.view.php');
			break;
		case 'VerplaatsingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VerplaatsingView.gen.php');
			break;
		case 'Verplaatsing_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Verplaatsing.gen.php');
			break;
		case 'Voorraad':
			require_once('WhosWho4/Boekweb2/DBObject/Voorraad.cls.php');
			break;
		case 'VoorraadMutatie':
			require_once('WhosWho4/Boekweb2/DBObject/VoorraadMutatie.cls.php');
			break;
		case 'VoorraadMutatieQuery':
			require_once('WhosWho4/Boekweb2/Query/VoorraadMutatie.query.php');
			break;
		case 'VoorraadMutatieQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VoorraadMutatieQuery.gen.php');
			break;
		case 'VoorraadMutatieVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VoorraadMutatieVerzameling.cls.php');
			break;
		case 'VoorraadMutatieVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VoorraadMutatieVerzameling.view.php');
			break;
		case 'VoorraadMutatieVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VoorraadMutatieVerzamelingView.gen.php');
			break;
		case 'VoorraadMutatieVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VoorraadMutatieVerzameling.gen.php');
			break;
		case 'VoorraadMutatieView':
			require_once('WhosWho4/Boekweb2/View/VoorraadMutatie.view.php');
			break;
		case 'VoorraadMutatieView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VoorraadMutatieView.gen.php');
			break;
		case 'VoorraadMutatie_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/VoorraadMutatie.gen.php');
			break;
		case 'VoorraadQuery':
			require_once('WhosWho4/Boekweb2/Query/Voorraad.query.php');
			break;
		case 'VoorraadQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/VoorraadQuery.gen.php');
			break;
		case 'VoorraadVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/VoorraadVerzameling.cls.php');
			break;
		case 'VoorraadVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/VoorraadVerzameling.view.php');
			break;
		case 'VoorraadVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VoorraadVerzamelingView.gen.php');
			break;
		case 'VoorraadVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/VoorraadVerzameling.gen.php');
			break;
		case 'VoorraadView':
			require_once('WhosWho4/Boekweb2/View/Voorraad.view.php');
			break;
		case 'VoorraadView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/VoorraadView.gen.php');
			break;
		case 'Voorraad_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/Voorraad.gen.php');
			break;
		case 'iDeal':
			require_once('WhosWho4/Boekweb2/DBObject/iDeal.cls.php');
			break;
		case 'iDealAntwoord':
			require_once('WhosWho4/Boekweb2/DBObject/iDealAntwoord.cls.php');
			break;
		case 'iDealAntwoordQuery':
			require_once('WhosWho4/Boekweb2/Query/iDealAntwoord.query.php');
			break;
		case 'iDealAntwoordQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/iDealAntwoordQuery.gen.php');
			break;
		case 'iDealAntwoordVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/iDealAntwoordVerzameling.cls.php');
			break;
		case 'iDealAntwoordVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/iDealAntwoordVerzameling.view.php');
			break;
		case 'iDealAntwoordVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealAntwoordVerzamelingView.gen.php');
			break;
		case 'iDealAntwoordVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/iDealAntwoordVerzameling.gen.php');
			break;
		case 'iDealAntwoordView':
			require_once('WhosWho4/Boekweb2/View/iDealAntwoord.view.php');
			break;
		case 'iDealAntwoordView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealAntwoordView.gen.php');
			break;
		case 'iDealAntwoord_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/iDealAntwoord.gen.php');
			break;
		case 'iDealKaartje':
			require_once('WhosWho4/Boekweb2/DBObject/iDealKaartje.cls.php');
			break;
		case 'iDealKaartjeCode':
			require_once('WhosWho4/Boekweb2/DBObject/iDealKaartjeCode.cls.php');
			break;
		case 'iDealKaartjeCodeQuery':
			require_once('WhosWho4/Boekweb2/Query/iDealKaartjeCode.query.php');
			break;
		case 'iDealKaartjeCodeQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/iDealKaartjeCodeQuery.gen.php');
			break;
		case 'iDealKaartjeCodeVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/iDealKaartjeCodeVerzameling.cls.php');
			break;
		case 'iDealKaartjeCodeVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/iDealKaartjeCodeVerzameling.view.php');
			break;
		case 'iDealKaartjeCodeVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealKaartjeCodeVerzamelingView.gen.php');
			break;
		case 'iDealKaartjeCodeVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/iDealKaartjeCodeVerzameling.gen.php');
			break;
		case 'iDealKaartjeCodeView':
			require_once('WhosWho4/Boekweb2/View/iDealKaartjeCode.view.php');
			break;
		case 'iDealKaartjeCodeView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealKaartjeCodeView.gen.php');
			break;
		case 'iDealKaartjeCode_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/iDealKaartjeCode.gen.php');
			break;
		case 'iDealKaartjeQuery':
			require_once('WhosWho4/Boekweb2/Query/iDealKaartje.query.php');
			break;
		case 'iDealKaartjeQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/iDealKaartjeQuery.gen.php');
			break;
		case 'iDealKaartjeVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/iDealKaartjeVerzameling.cls.php');
			break;
		case 'iDealKaartjeVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/iDealKaartjeVerzameling.view.php');
			break;
		case 'iDealKaartjeVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealKaartjeVerzamelingView.gen.php');
			break;
		case 'iDealKaartjeVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/iDealKaartjeVerzameling.gen.php');
			break;
		case 'iDealKaartjeView':
			require_once('WhosWho4/Boekweb2/View/iDealKaartje.view.php');
			break;
		case 'iDealKaartjeView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealKaartjeView.gen.php');
			break;
		case 'iDealKaartje_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/iDealKaartje.gen.php');
			break;
		case 'iDealQuery':
			require_once('WhosWho4/Boekweb2/Query/iDeal.query.php');
			break;
		case 'iDealQuery_Generated':
			require_once('WhosWho4/Boekweb2/Query/Generated/iDealQuery.gen.php');
			break;
		case 'iDealVerzameling':
			require_once('WhosWho4/Boekweb2/Verzameling/iDealVerzameling.cls.php');
			break;
		case 'iDealVerzamelingView':
			require_once('WhosWho4/Boekweb2/View/iDealVerzameling.view.php');
			break;
		case 'iDealVerzamelingView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealVerzamelingView.gen.php');
			break;
		case 'iDealVerzameling_Generated':
			require_once('WhosWho4/Boekweb2/Verzameling/Generated/iDealVerzameling.gen.php');
			break;
		case 'iDealView':
			require_once('WhosWho4/Boekweb2/View/iDeal.view.php');
			break;
		case 'iDealView_Generated':
			require_once('WhosWho4/Boekweb2/View/Generated/iDealView.gen.php');
			break;
		case 'iDeal_Generated':
			require_once('WhosWho4/Boekweb2/DBObject/Generated/iDeal.gen.php');
			break;

		case 'Bug':
			require_once('WhosWho4/Bugweb/DBObject/Bug.cls.php');
			break;
		case 'BugBericht':
			require_once('WhosWho4/Bugweb/DBObject/BugBericht.cls.php');
			break;
		case 'BugBerichtQuery':
			require_once('WhosWho4/Bugweb/Query/BugBericht.query.php');
			break;
		case 'BugBerichtQuery_Generated':
			require_once('WhosWho4/Bugweb/Query/Generated/BugBerichtQuery.gen.php');
			break;
		case 'BugBerichtVerzameling':
			require_once('WhosWho4/Bugweb/Verzameling/BugBerichtVerzameling.cls.php');
			break;
		case 'BugBerichtVerzamelingView':
			require_once('WhosWho4/Bugweb/View/BugBerichtVerzameling.view.php');
			break;
		case 'BugBerichtVerzamelingView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugBerichtVerzamelingView.gen.php');
			break;
		case 'BugBerichtVerzameling_Generated':
			require_once('WhosWho4/Bugweb/Verzameling/Generated/BugBerichtVerzameling.gen.php');
			break;
		case 'BugBerichtView':
			require_once('WhosWho4/Bugweb/View/BugBericht.view.php');
			break;
		case 'BugBerichtView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugBerichtView.gen.php');
			break;
		case 'BugBericht_Generated':
			require_once('WhosWho4/Bugweb/DBObject/Generated/BugBericht.gen.php');
			break;
		case 'BugCategorie':
			require_once('WhosWho4/Bugweb/DBObject/BugCategorie.cls.php');
			break;
		case 'BugCategorieQuery':
			require_once('WhosWho4/Bugweb/Query/BugCategorie.query.php');
			break;
		case 'BugCategorieQuery_Generated':
			require_once('WhosWho4/Bugweb/Query/Generated/BugCategorieQuery.gen.php');
			break;
		case 'BugCategorieVerzameling':
			require_once('WhosWho4/Bugweb/Verzameling/BugCategorieVerzameling.cls.php');
			break;
		case 'BugCategorieVerzamelingView':
			require_once('WhosWho4/Bugweb/View/BugCategorieVerzameling.view.php');
			break;
		case 'BugCategorieVerzamelingView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugCategorieVerzamelingView.gen.php');
			break;
		case 'BugCategorieVerzameling_Generated':
			require_once('WhosWho4/Bugweb/Verzameling/Generated/BugCategorieVerzameling.gen.php');
			break;
		case 'BugCategorieView':
			require_once('WhosWho4/Bugweb/View/BugCategorie.view.php');
			break;
		case 'BugCategorieView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugCategorieView.gen.php');
			break;
		case 'BugCategorie_Generated':
			require_once('WhosWho4/Bugweb/DBObject/Generated/BugCategorie.gen.php');
			break;
		case 'BugQuery':
			require_once('WhosWho4/Bugweb/Query/Bug.query.php');
			break;
		case 'BugQuery_Generated':
			require_once('WhosWho4/Bugweb/Query/Generated/BugQuery.gen.php');
			break;
		case 'BugToewijzing':
			require_once('WhosWho4/Bugweb/DBObject/BugToewijzing.cls.php');
			break;
		case 'BugToewijzingQuery':
			require_once('WhosWho4/Bugweb/Query/BugToewijzing.query.php');
			break;
		case 'BugToewijzingQuery_Generated':
			require_once('WhosWho4/Bugweb/Query/Generated/BugToewijzingQuery.gen.php');
			break;
		case 'BugToewijzingVerzameling':
			require_once('WhosWho4/Bugweb/Verzameling/BugToewijzingVerzameling.cls.php');
			break;
		case 'BugToewijzingVerzamelingView':
			require_once('WhosWho4/Bugweb/View/BugToewijzingVerzameling.view.php');
			break;
		case 'BugToewijzingVerzamelingView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugToewijzingVerzamelingView.gen.php');
			break;
		case 'BugToewijzingVerzameling_Generated':
			require_once('WhosWho4/Bugweb/Verzameling/Generated/BugToewijzingVerzameling.gen.php');
			break;
		case 'BugToewijzingView':
			require_once('WhosWho4/Bugweb/View/BugToewijzing.view.php');
			break;
		case 'BugToewijzingView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugToewijzingView.gen.php');
			break;
		case 'BugToewijzing_Generated':
			require_once('WhosWho4/Bugweb/DBObject/Generated/BugToewijzing.gen.php');
			break;
		case 'BugVerzameling':
			require_once('WhosWho4/Bugweb/Verzameling/BugVerzameling.cls.php');
			break;
		case 'BugVerzamelingView':
			require_once('WhosWho4/Bugweb/View/BugVerzameling.view.php');
			break;
		case 'BugVerzamelingView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugVerzamelingView.gen.php');
			break;
		case 'BugVerzameling_Generated':
			require_once('WhosWho4/Bugweb/Verzameling/Generated/BugVerzameling.gen.php');
			break;
		case 'BugView':
			require_once('WhosWho4/Bugweb/View/Bug.view.php');
			break;
		case 'BugView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugView.gen.php');
			break;
		case 'BugVolger':
			require_once('WhosWho4/Bugweb/DBObject/BugVolger.cls.php');
			break;
		case 'BugVolgerQuery':
			require_once('WhosWho4/Bugweb/Query/BugVolger.query.php');
			break;
		case 'BugVolgerQuery_Generated':
			require_once('WhosWho4/Bugweb/Query/Generated/BugVolgerQuery.gen.php');
			break;
		case 'BugVolgerVerzameling':
			require_once('WhosWho4/Bugweb/Verzameling/BugVolgerVerzameling.cls.php');
			break;
		case 'BugVolgerVerzamelingView':
			require_once('WhosWho4/Bugweb/View/BugVolgerVerzameling.view.php');
			break;
		case 'BugVolgerVerzamelingView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugVolgerVerzamelingView.gen.php');
			break;
		case 'BugVolgerVerzameling_Generated':
			require_once('WhosWho4/Bugweb/Verzameling/Generated/BugVolgerVerzameling.gen.php');
			break;
		case 'BugVolgerView':
			require_once('WhosWho4/Bugweb/View/BugVolger.view.php');
			break;
		case 'BugVolgerView_Generated':
			require_once('WhosWho4/Bugweb/View/Generated/BugVolgerView.gen.php');
			break;
		case 'BugVolger_Generated':
			require_once('WhosWho4/Bugweb/DBObject/Generated/BugVolger.gen.php');
			break;
		case 'Bug_Generated':
			require_once('WhosWho4/Bugweb/DBObject/Generated/Bug.gen.php');
			break;

		case 'ColaProduct':
			require_once('WhosWho4/Dibs/DBObject/ColaProduct.cls.php');
			break;
		case 'ColaProductQuery':
			require_once('WhosWho4/Dibs/Query/ColaProduct.query.php');
			break;
		case 'ColaProductQuery_Generated':
			require_once('WhosWho4/Dibs/Query/Generated/ColaProductQuery.gen.php');
			break;
		case 'ColaProductVerzameling':
			require_once('WhosWho4/Dibs/Verzameling/ColaProductVerzameling.cls.php');
			break;
		case 'ColaProductVerzamelingView':
			require_once('WhosWho4/Dibs/View/ColaProductVerzameling.view.php');
			break;
		case 'ColaProductVerzamelingView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/ColaProductVerzamelingView.gen.php');
			break;
		case 'ColaProductVerzameling_Generated':
			require_once('WhosWho4/Dibs/Verzameling/Generated/ColaProductVerzameling.gen.php');
			break;
		case 'ColaProductView':
			require_once('WhosWho4/Dibs/View/ColaProduct.view.php');
			break;
		case 'ColaProductView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/ColaProductView.gen.php');
			break;
		case 'ColaProduct_Generated':
			require_once('WhosWho4/Dibs/DBObject/Generated/ColaProduct.gen.php');
			break;
		case 'DibsInfo':
			require_once('WhosWho4/Dibs/DBObject/DibsInfo.cls.php');
			break;
		case 'DibsInfoQuery':
			require_once('WhosWho4/Dibs/Query/DibsInfo.query.php');
			break;
		case 'DibsInfoQuery_Generated':
			require_once('WhosWho4/Dibs/Query/Generated/DibsInfoQuery.gen.php');
			break;
		case 'DibsInfoVerzameling':
			require_once('WhosWho4/Dibs/Verzameling/DibsInfoVerzameling.cls.php');
			break;
		case 'DibsInfoVerzamelingView':
			require_once('WhosWho4/Dibs/View/DibsInfoVerzameling.view.php');
			break;
		case 'DibsInfoVerzamelingView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/DibsInfoVerzamelingView.gen.php');
			break;
		case 'DibsInfoVerzameling_Generated':
			require_once('WhosWho4/Dibs/Verzameling/Generated/DibsInfoVerzameling.gen.php');
			break;
		case 'DibsInfoView':
			require_once('WhosWho4/Dibs/View/DibsInfo.view.php');
			break;
		case 'DibsInfoView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/DibsInfoView.gen.php');
			break;
		case 'DibsInfo_Generated':
			require_once('WhosWho4/Dibs/DBObject/Generated/DibsInfo.gen.php');
			break;
		case 'DibsProduct':
			require_once('WhosWho4/Dibs/DBObject/DibsProduct.cls.php');
			break;
		case 'DibsProductQuery':
			require_once('WhosWho4/Dibs/Query/DibsProduct.query.php');
			break;
		case 'DibsProductQuery_Generated':
			require_once('WhosWho4/Dibs/Query/Generated/DibsProductQuery.gen.php');
			break;
		case 'DibsProductVerzameling':
			require_once('WhosWho4/Dibs/Verzameling/DibsProductVerzameling.cls.php');
			break;
		case 'DibsProductVerzamelingView':
			require_once('WhosWho4/Dibs/View/DibsProductVerzameling.view.php');
			break;
		case 'DibsProductVerzamelingView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/DibsProductVerzamelingView.gen.php');
			break;
		case 'DibsProductVerzameling_Generated':
			require_once('WhosWho4/Dibs/Verzameling/Generated/DibsProductVerzameling.gen.php');
			break;
		case 'DibsProductView':
			require_once('WhosWho4/Dibs/View/DibsProduct.view.php');
			break;
		case 'DibsProductView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/DibsProductView.gen.php');
			break;
		case 'DibsProduct_Generated':
			require_once('WhosWho4/Dibs/DBObject/Generated/DibsProduct.gen.php');
			break;
		case 'DibsTransactie':
			require_once('WhosWho4/Dibs/DBObject/DibsTransactie.cls.php');
			break;
		case 'DibsTransactieQuery':
			require_once('WhosWho4/Dibs/Query/DibsTransactie.query.php');
			break;
		case 'DibsTransactieQuery_Generated':
			require_once('WhosWho4/Dibs/Query/Generated/DibsTransactieQuery.gen.php');
			break;
		case 'DibsTransactieVerzameling':
			require_once('WhosWho4/Dibs/Verzameling/DibsTransactieVerzameling.cls.php');
			break;
		case 'DibsTransactieVerzamelingView':
			require_once('WhosWho4/Dibs/View/DibsTransactieVerzameling.view.php');
			break;
		case 'DibsTransactieVerzamelingView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/DibsTransactieVerzamelingView.gen.php');
			break;
		case 'DibsTransactieVerzameling_Generated':
			require_once('WhosWho4/Dibs/Verzameling/Generated/DibsTransactieVerzameling.gen.php');
			break;
		case 'DibsTransactieView':
			require_once('WhosWho4/Dibs/View/DibsTransactie.view.php');
			break;
		case 'DibsTransactieView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/DibsTransactieView.gen.php');
			break;
		case 'DibsTransactie_Generated':
			require_once('WhosWho4/Dibs/DBObject/Generated/DibsTransactie.gen.php');
			break;
		case 'Pas':
			require_once('WhosWho4/Dibs/DBObject/Pas.cls.php');
			break;
		case 'PasQuery':
			require_once('WhosWho4/Dibs/Query/Pas.query.php');
			break;
		case 'PasQuery_Generated':
			require_once('WhosWho4/Dibs/Query/Generated/PasQuery.gen.php');
			break;
		case 'PasVerzameling':
			require_once('WhosWho4/Dibs/Verzameling/PasVerzameling.cls.php');
			break;
		case 'PasVerzamelingView':
			require_once('WhosWho4/Dibs/View/PasVerzameling.view.php');
			break;
		case 'PasVerzamelingView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/PasVerzamelingView.gen.php');
			break;
		case 'PasVerzameling_Generated':
			require_once('WhosWho4/Dibs/Verzameling/Generated/PasVerzameling.gen.php');
			break;
		case 'PasView':
			require_once('WhosWho4/Dibs/View/Pas.view.php');
			break;
		case 'PasView_Generated':
			require_once('WhosWho4/Dibs/View/Generated/PasView.gen.php');
			break;
		case 'Pas_Generated':
			require_once('WhosWho4/Dibs/DBObject/Generated/Pas.gen.php');
			break;

		case 'Collectie':
			require_once('WhosWho4/Fotoweb/DBObject/Collectie.cls.php');
			break;
		case 'CollectieQuery':
			require_once('WhosWho4/Fotoweb/Query/Collectie.query.php');
			break;
		case 'CollectieQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/CollectieQuery.gen.php');
			break;
		case 'CollectieVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/CollectieVerzameling.cls.php');
			break;
		case 'CollectieVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/CollectieVerzameling.view.php');
			break;
		case 'CollectieVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/CollectieVerzamelingView.gen.php');
			break;
		case 'CollectieVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/CollectieVerzameling.gen.php');
			break;
		case 'CollectieView':
			require_once('WhosWho4/Fotoweb/View/Collectie.view.php');
			break;
		case 'CollectieView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/CollectieView.gen.php');
			break;
		case 'Collectie_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/Collectie.gen.php');
			break;
		case 'CommissieFoto':
			require_once('WhosWho4/Fotoweb/DBObject/CommissieFoto.cls.php');
			break;
		case 'CommissieFotoQuery':
			require_once('WhosWho4/Fotoweb/Query/CommissieFoto.query.php');
			break;
		case 'CommissieFotoQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/CommissieFotoQuery.gen.php');
			break;
		case 'CommissieFotoVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/CommissieFotoVerzameling.cls.php');
			break;
		case 'CommissieFotoVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/CommissieFotoVerzameling.view.php');
			break;
		case 'CommissieFotoVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/CommissieFotoVerzamelingView.gen.php');
			break;
		case 'CommissieFotoVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/CommissieFotoVerzameling.gen.php');
			break;
		case 'CommissieFotoView':
			require_once('WhosWho4/Fotoweb/View/CommissieFoto.view.php');
			break;
		case 'CommissieFotoView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/CommissieFotoView.gen.php');
			break;
		case 'CommissieFoto_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/CommissieFoto.gen.php');
			break;
		case 'Media':
			require_once('WhosWho4/Fotoweb/DBObject/Media.cls.php');
			break;
		case 'MediaQuery':
			require_once('WhosWho4/Fotoweb/Query/Media.query.php');
			break;
		case 'MediaQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/MediaQuery.gen.php');
			break;
		case 'MediaVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/MediaVerzameling.cls.php');
			break;
		case 'MediaVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/MediaVerzameling.view.php');
			break;
		case 'MediaVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/MediaVerzamelingView.gen.php');
			break;
		case 'MediaVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/MediaVerzameling.gen.php');
			break;
		case 'MediaView':
			require_once('WhosWho4/Fotoweb/View/Media.view.php');
			break;
		case 'MediaView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/MediaView.gen.php');
			break;
		case 'Media_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/Media.gen.php');
			break;
		case 'ProfielFoto':
			require_once('WhosWho4/Fotoweb/DBObject/ProfielFoto.cls.php');
			break;
		case 'ProfielFotoQuery':
			require_once('WhosWho4/Fotoweb/Query/ProfielFoto.query.php');
			break;
		case 'ProfielFotoQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/ProfielFotoQuery.gen.php');
			break;
		case 'ProfielFotoVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/ProfielFotoVerzameling.cls.php');
			break;
		case 'ProfielFotoVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/ProfielFotoVerzameling.view.php');
			break;
		case 'ProfielFotoVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/ProfielFotoVerzamelingView.gen.php');
			break;
		case 'ProfielFotoVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/ProfielFotoVerzameling.gen.php');
			break;
		case 'ProfielFotoView':
			require_once('WhosWho4/Fotoweb/View/ProfielFoto.view.php');
			break;
		case 'ProfielFotoView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/ProfielFotoView.gen.php');
			break;
		case 'ProfielFoto_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/ProfielFoto.gen.php');
			break;
		case 'Rating':
			require_once('WhosWho4/Fotoweb/DBObject/Rating.cls.php');
			break;
		case 'RatingQuery':
			require_once('WhosWho4/Fotoweb/Query/Rating.query.php');
			break;
		case 'RatingQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/RatingQuery.gen.php');
			break;
		case 'RatingVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/RatingVerzameling.cls.php');
			break;
		case 'RatingVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/RatingVerzameling.view.php');
			break;
		case 'RatingVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/RatingVerzamelingView.gen.php');
			break;
		case 'RatingVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/RatingVerzameling.gen.php');
			break;
		case 'RatingView':
			require_once('WhosWho4/Fotoweb/View/Rating.view.php');
			break;
		case 'RatingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/RatingView.gen.php');
			break;
		case 'Rating_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/Rating.gen.php');
			break;
		case 'Tag':
			require_once('WhosWho4/Fotoweb/DBObject/Tag.cls.php');
			break;
		case 'TagArea':
			require_once('WhosWho4/Fotoweb/DBObject/TagArea.cls.php');
			break;
		case 'TagAreaQuery':
			require_once('WhosWho4/Fotoweb/Query/TagArea.query.php');
			break;
		case 'TagAreaQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/TagAreaQuery.gen.php');
			break;
		case 'TagAreaVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/TagAreaVerzameling.cls.php');
			break;
		case 'TagAreaVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/TagAreaVerzameling.view.php');
			break;
		case 'TagAreaVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/TagAreaVerzamelingView.gen.php');
			break;
		case 'TagAreaVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/TagAreaVerzameling.gen.php');
			break;
		case 'TagAreaView':
			require_once('WhosWho4/Fotoweb/View/TagArea.view.php');
			break;
		case 'TagAreaView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/TagAreaView.gen.php');
			break;
		case 'TagArea_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/TagArea.gen.php');
			break;
		case 'TagQuery':
			require_once('WhosWho4/Fotoweb/Query/Tag.query.php');
			break;
		case 'TagQuery_Generated':
			require_once('WhosWho4/Fotoweb/Query/Generated/TagQuery.gen.php');
			break;
		case 'TagVerzameling':
			require_once('WhosWho4/Fotoweb/Verzameling/TagVerzameling.cls.php');
			break;
		case 'TagVerzamelingView':
			require_once('WhosWho4/Fotoweb/View/TagVerzameling.view.php');
			break;
		case 'TagVerzamelingView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/TagVerzamelingView.gen.php');
			break;
		case 'TagVerzameling_Generated':
			require_once('WhosWho4/Fotoweb/Verzameling/Generated/TagVerzameling.gen.php');
			break;
		case 'TagView':
			require_once('WhosWho4/Fotoweb/View/Tag.view.php');
			break;
		case 'TagView_Generated':
			require_once('WhosWho4/Fotoweb/View/Generated/TagView.gen.php');
			break;
		case 'Tag_Generated':
			require_once('WhosWho4/Fotoweb/DBObject/Generated/Tag.gen.php');
			break;

		case 'IOUBetaling':
			require_once('WhosWho4/IOU/DBObject/IOUBetaling.cls.php');
			break;
		case 'IOUBetalingQuery':
			require_once('WhosWho4/IOU/Query/IOUBetaling.query.php');
			break;
		case 'IOUBetalingQuery_Generated':
			require_once('WhosWho4/IOU/Query/Generated/IOUBetalingQuery.gen.php');
			break;
		case 'IOUBetalingVerzameling':
			require_once('WhosWho4/IOU/Verzameling/IOUBetalingVerzameling.cls.php');
			break;
		case 'IOUBetalingVerzamelingView':
			require_once('WhosWho4/IOU/View/IOUBetalingVerzameling.view.php');
			break;
		case 'IOUBetalingVerzamelingView_Generated':
			require_once('WhosWho4/IOU/View/Generated/IOUBetalingVerzamelingView.gen.php');
			break;
		case 'IOUBetalingVerzameling_Generated':
			require_once('WhosWho4/IOU/Verzameling/Generated/IOUBetalingVerzameling.gen.php');
			break;
		case 'IOUBetalingView':
			require_once('WhosWho4/IOU/View/IOUBetaling.view.php');
			break;
		case 'IOUBetalingView_Generated':
			require_once('WhosWho4/IOU/View/Generated/IOUBetalingView.gen.php');
			break;
		case 'IOUBetaling_Generated':
			require_once('WhosWho4/IOU/DBObject/Generated/IOUBetaling.gen.php');
			break;
		case 'IOUBon':
			require_once('WhosWho4/IOU/DBObject/IOUBon.cls.php');
			break;
		case 'IOUBonQuery':
			require_once('WhosWho4/IOU/Query/IOUBon.query.php');
			break;
		case 'IOUBonQuery_Generated':
			require_once('WhosWho4/IOU/Query/Generated/IOUBonQuery.gen.php');
			break;
		case 'IOUBonVerzameling':
			require_once('WhosWho4/IOU/Verzameling/IOUBonVerzameling.cls.php');
			break;
		case 'IOUBonVerzamelingView':
			require_once('WhosWho4/IOU/View/IOUBonVerzameling.view.php');
			break;
		case 'IOUBonVerzamelingView_Generated':
			require_once('WhosWho4/IOU/View/Generated/IOUBonVerzamelingView.gen.php');
			break;
		case 'IOUBonVerzameling_Generated':
			require_once('WhosWho4/IOU/Verzameling/Generated/IOUBonVerzameling.gen.php');
			break;
		case 'IOUBonView':
			require_once('WhosWho4/IOU/View/IOUBon.view.php');
			break;
		case 'IOUBonView_Generated':
			require_once('WhosWho4/IOU/View/Generated/IOUBonView.gen.php');
			break;
		case 'IOUBon_Generated':
			require_once('WhosWho4/IOU/DBObject/Generated/IOUBon.gen.php');
			break;
		case 'IOUSplit':
			require_once('WhosWho4/IOU/DBObject/IOUSplit.cls.php');
			break;
		case 'IOUSplitQuery':
			require_once('WhosWho4/IOU/Query/IOUSplit.query.php');
			break;
		case 'IOUSplitQuery_Generated':
			require_once('WhosWho4/IOU/Query/Generated/IOUSplitQuery.gen.php');
			break;
		case 'IOUSplitVerzameling':
			require_once('WhosWho4/IOU/Verzameling/IOUSplitVerzameling.cls.php');
			break;
		case 'IOUSplitVerzamelingView':
			require_once('WhosWho4/IOU/View/IOUSplitVerzameling.view.php');
			break;
		case 'IOUSplitVerzamelingView_Generated':
			require_once('WhosWho4/IOU/View/Generated/IOUSplitVerzamelingView.gen.php');
			break;
		case 'IOUSplitVerzameling_Generated':
			require_once('WhosWho4/IOU/Verzameling/Generated/IOUSplitVerzameling.gen.php');
			break;
		case 'IOUSplitView':
			require_once('WhosWho4/IOU/View/IOUSplit.view.php');
			break;
		case 'IOUSplitView_Generated':
			require_once('WhosWho4/IOU/View/Generated/IOUSplitView.gen.php');
			break;
		case 'IOUSplit_Generated':
			require_once('WhosWho4/IOU/DBObject/Generated/IOUSplit.gen.php');
			break;

		case 'ActivatieCode':
			require_once('WhosWho4/Intro/DBObject/ActivatieCode.cls.php');
			break;
		case 'ActivatieCodeQuery':
			require_once('WhosWho4/Intro/Query/ActivatieCode.query.php');
			break;
		case 'ActivatieCodeQuery_Generated':
			require_once('WhosWho4/Intro/Query/Generated/ActivatieCodeQuery.gen.php');
			break;
		case 'ActivatieCodeVerzameling':
			require_once('WhosWho4/Intro/Verzameling/ActivatieCodeVerzameling.cls.php');
			break;
		case 'ActivatieCodeVerzamelingView':
			require_once('WhosWho4/Intro/View/ActivatieCodeVerzameling.view.php');
			break;
		case 'ActivatieCodeVerzamelingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/ActivatieCodeVerzamelingView.gen.php');
			break;
		case 'ActivatieCodeVerzameling_Generated':
			require_once('WhosWho4/Intro/Verzameling/Generated/ActivatieCodeVerzameling.gen.php');
			break;
		case 'ActivatieCodeView':
			require_once('WhosWho4/Intro/View/ActivatieCode.view.php');
			break;
		case 'ActivatieCodeView_Generated':
			require_once('WhosWho4/Intro/View/Generated/ActivatieCodeView.gen.php');
			break;
		case 'ActivatieCode_Generated':
			require_once('WhosWho4/Intro/DBObject/Generated/ActivatieCode.gen.php');
			break;
		case 'IntroCluster':
			require_once('WhosWho4/Intro/DBObject/IntroCluster.cls.php');
			break;
		case 'IntroClusterQuery':
			require_once('WhosWho4/Intro/Query/IntroCluster.query.php');
			break;
		case 'IntroClusterQuery_Generated':
			require_once('WhosWho4/Intro/Query/Generated/IntroClusterQuery.gen.php');
			break;
		case 'IntroClusterVerzameling':
			require_once('WhosWho4/Intro/Verzameling/IntroClusterVerzameling.cls.php');
			break;
		case 'IntroClusterVerzamelingView':
			require_once('WhosWho4/Intro/View/IntroClusterVerzameling.view.php');
			break;
		case 'IntroClusterVerzamelingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/IntroClusterVerzamelingView.gen.php');
			break;
		case 'IntroClusterVerzameling_Generated':
			require_once('WhosWho4/Intro/Verzameling/Generated/IntroClusterVerzameling.gen.php');
			break;
		case 'IntroClusterView':
			require_once('WhosWho4/Intro/View/IntroCluster.view.php');
			break;
		case 'IntroClusterView_Generated':
			require_once('WhosWho4/Intro/View/Generated/IntroClusterView.gen.php');
			break;
		case 'IntroCluster_Generated':
			require_once('WhosWho4/Intro/DBObject/Generated/IntroCluster.gen.php');
			break;
		case 'IntroDeelnemer':
			require_once('WhosWho4/Intro/DBObject/IntroDeelnemer.cls.php');
			break;
		case 'IntroDeelnemerQuery':
			require_once('WhosWho4/Intro/Query/IntroDeelnemer.query.php');
			break;
		case 'IntroDeelnemerQuery_Generated':
			require_once('WhosWho4/Intro/Query/Generated/IntroDeelnemerQuery.gen.php');
			break;
		case 'IntroDeelnemerVerzameling':
			require_once('WhosWho4/Intro/Verzameling/IntroDeelnemerVerzameling.cls.php');
			break;
		case 'IntroDeelnemerVerzamelingView':
			require_once('WhosWho4/Intro/View/IntroDeelnemerVerzameling.view.php');
			break;
		case 'IntroDeelnemerVerzamelingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/IntroDeelnemerVerzamelingView.gen.php');
			break;
		case 'IntroDeelnemerVerzameling_Generated':
			require_once('WhosWho4/Intro/Verzameling/Generated/IntroDeelnemerVerzameling.gen.php');
			break;
		case 'IntroDeelnemerView':
			require_once('WhosWho4/Intro/View/IntroDeelnemer.view.php');
			break;
		case 'IntroDeelnemerView_Generated':
			require_once('WhosWho4/Intro/View/Generated/IntroDeelnemerView.gen.php');
			break;
		case 'IntroDeelnemer_Generated':
			require_once('WhosWho4/Intro/DBObject/Generated/IntroDeelnemer.gen.php');
			break;
		case 'IntroGroep':
			require_once('WhosWho4/Intro/DBObject/IntroGroep.cls.php');
			break;
		case 'IntroGroepQuery':
			require_once('WhosWho4/Intro/Query/IntroGroep.query.php');
			break;
		case 'IntroGroepQuery_Generated':
			require_once('WhosWho4/Intro/Query/Generated/IntroGroepQuery.gen.php');
			break;
		case 'IntroGroepVerzameling':
			require_once('WhosWho4/Intro/Verzameling/IntroGroepVerzameling.cls.php');
			break;
		case 'IntroGroepVerzamelingView':
			require_once('WhosWho4/Intro/View/IntroGroepVerzameling.view.php');
			break;
		case 'IntroGroepVerzamelingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/IntroGroepVerzamelingView.gen.php');
			break;
		case 'IntroGroepVerzameling_Generated':
			require_once('WhosWho4/Intro/Verzameling/Generated/IntroGroepVerzameling.gen.php');
			break;
		case 'IntroGroepView':
			require_once('WhosWho4/Intro/View/IntroGroep.view.php');
			break;
		case 'IntroGroepView_Generated':
			require_once('WhosWho4/Intro/View/Generated/IntroGroepView.gen.php');
			break;
		case 'IntroGroep_Generated':
			require_once('WhosWho4/Intro/DBObject/Generated/IntroGroep.gen.php');
			break;
		case 'LidmaatschapsBetaling':
			require_once('WhosWho4/Intro/DBObject/LidmaatschapsBetaling.cls.php');
			break;
		case 'LidmaatschapsBetalingQuery':
			require_once('WhosWho4/Intro/Query/LidmaatschapsBetaling.query.php');
			break;
		case 'LidmaatschapsBetalingQuery_Generated':
			require_once('WhosWho4/Intro/Query/Generated/LidmaatschapsBetalingQuery.gen.php');
			break;
		case 'LidmaatschapsBetalingVerzameling':
			require_once('WhosWho4/Intro/Verzameling/LidmaatschapsBetalingVerzameling.cls.php');
			break;
		case 'LidmaatschapsBetalingVerzamelingView':
			require_once('WhosWho4/Intro/View/LidmaatschapsBetalingVerzameling.view.php');
			break;
		case 'LidmaatschapsBetalingVerzamelingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/LidmaatschapsBetalingVerzamelingView.gen.php');
			break;
		case 'LidmaatschapsBetalingVerzameling_Generated':
			require_once('WhosWho4/Intro/Verzameling/Generated/LidmaatschapsBetalingVerzameling.gen.php');
			break;
		case 'LidmaatschapsBetalingView':
			require_once('WhosWho4/Intro/View/LidmaatschapsBetaling.view.php');
			break;
		case 'LidmaatschapsBetalingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/LidmaatschapsBetalingView.gen.php');
			break;
		case 'LidmaatschapsBetaling_Generated':
			require_once('WhosWho4/Intro/DBObject/Generated/LidmaatschapsBetaling.gen.php');
			break;
		case 'Mentor':
			require_once('WhosWho4/Intro/DBObject/Mentor.cls.php');
			break;
		case 'MentorQuery':
			require_once('WhosWho4/Intro/Query/Mentor.query.php');
			break;
		case 'MentorQuery_Generated':
			require_once('WhosWho4/Intro/Query/Generated/MentorQuery.gen.php');
			break;
		case 'MentorVerzameling':
			require_once('WhosWho4/Intro/Verzameling/MentorVerzameling.cls.php');
			break;
		case 'MentorVerzamelingView':
			require_once('WhosWho4/Intro/View/MentorVerzameling.view.php');
			break;
		case 'MentorVerzamelingView_Generated':
			require_once('WhosWho4/Intro/View/Generated/MentorVerzamelingView.gen.php');
			break;
		case 'MentorVerzameling_Generated':
			require_once('WhosWho4/Intro/Verzameling/Generated/MentorVerzameling.gen.php');
			break;
		case 'MentorView':
			require_once('WhosWho4/Intro/View/Mentor.view.php');
			break;
		case 'MentorView_Generated':
			require_once('WhosWho4/Intro/View/Generated/MentorView.gen.php');
			break;
		case 'Mentor_Generated':
			require_once('WhosWho4/Intro/DBObject/Generated/Mentor.gen.php');
			break;

		case 'Mailing':
			require_once('WhosWho4/Mailings/DBObject/Mailing.cls.php');
			break;
		case 'MailingBounce':
			require_once('WhosWho4/Mailings/DBObject/MailingBounce.cls.php');
			break;
		case 'MailingBounceQuery':
			require_once('WhosWho4/Mailings/Query/MailingBounce.query.php');
			break;
		case 'MailingBounceQuery_Generated':
			require_once('WhosWho4/Mailings/Query/Generated/MailingBounceQuery.gen.php');
			break;
		case 'MailingBounceVerzameling':
			require_once('WhosWho4/Mailings/Verzameling/MailingBounceVerzameling.cls.php');
			break;
		case 'MailingBounceVerzamelingView':
			require_once('WhosWho4/Mailings/View/MailingBounceVerzameling.view.php');
			break;
		case 'MailingBounceVerzamelingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingBounceVerzamelingView.gen.php');
			break;
		case 'MailingBounceVerzameling_Generated':
			require_once('WhosWho4/Mailings/Verzameling/Generated/MailingBounceVerzameling.gen.php');
			break;
		case 'MailingBounceView':
			require_once('WhosWho4/Mailings/View/MailingBounce.view.php');
			break;
		case 'MailingBounceView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingBounceView.gen.php');
			break;
		case 'MailingBounce_Generated':
			require_once('WhosWho4/Mailings/DBObject/Generated/MailingBounce.gen.php');
			break;
		case 'MailingList':
			require_once('WhosWho4/Mailings/DBObject/MailingList.cls.php');
			break;
		case 'MailingListQuery':
			require_once('WhosWho4/Mailings/Query/MailingList.query.php');
			break;
		case 'MailingListQuery_Generated':
			require_once('WhosWho4/Mailings/Query/Generated/MailingListQuery.gen.php');
			break;
		case 'MailingListVerzameling':
			require_once('WhosWho4/Mailings/Verzameling/MailingListVerzameling.cls.php');
			break;
		case 'MailingListVerzamelingView':
			require_once('WhosWho4/Mailings/View/MailingListVerzameling.view.php');
			break;
		case 'MailingListVerzamelingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingListVerzamelingView.gen.php');
			break;
		case 'MailingListVerzameling_Generated':
			require_once('WhosWho4/Mailings/Verzameling/Generated/MailingListVerzameling.gen.php');
			break;
		case 'MailingListView':
			require_once('WhosWho4/Mailings/View/MailingList.view.php');
			break;
		case 'MailingListView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingListView.gen.php');
			break;
		case 'MailingList_Generated':
			require_once('WhosWho4/Mailings/DBObject/Generated/MailingList.gen.php');
			break;
		case 'MailingQuery':
			require_once('WhosWho4/Mailings/Query/Mailing.query.php');
			break;
		case 'MailingQuery_Generated':
			require_once('WhosWho4/Mailings/Query/Generated/MailingQuery.gen.php');
			break;
		case 'MailingTemplate':
			require_once('WhosWho4/Mailings/DBObject/MailingTemplate.cls.php');
			break;
		case 'MailingTemplateQuery':
			require_once('WhosWho4/Mailings/Query/MailingTemplate.query.php');
			break;
		case 'MailingTemplateQuery_Generated':
			require_once('WhosWho4/Mailings/Query/Generated/MailingTemplateQuery.gen.php');
			break;
		case 'MailingTemplateVerzameling':
			require_once('WhosWho4/Mailings/Verzameling/MailingTemplateVerzameling.cls.php');
			break;
		case 'MailingTemplateVerzamelingView':
			require_once('WhosWho4/Mailings/View/MailingTemplateVerzameling.view.php');
			break;
		case 'MailingTemplateVerzamelingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingTemplateVerzamelingView.gen.php');
			break;
		case 'MailingTemplateVerzameling_Generated':
			require_once('WhosWho4/Mailings/Verzameling/Generated/MailingTemplateVerzameling.gen.php');
			break;
		case 'MailingTemplateView':
			require_once('WhosWho4/Mailings/View/MailingTemplate.view.php');
			break;
		case 'MailingTemplateView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingTemplateView.gen.php');
			break;
		case 'MailingTemplate_Generated':
			require_once('WhosWho4/Mailings/DBObject/Generated/MailingTemplate.gen.php');
			break;
		case 'MailingVerzameling':
			require_once('WhosWho4/Mailings/Verzameling/MailingVerzameling.cls.php');
			break;
		case 'MailingVerzamelingView':
			require_once('WhosWho4/Mailings/View/MailingVerzameling.view.php');
			break;
		case 'MailingVerzamelingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingVerzamelingView.gen.php');
			break;
		case 'MailingVerzameling_Generated':
			require_once('WhosWho4/Mailings/Verzameling/Generated/MailingVerzameling.gen.php');
			break;
		case 'MailingView':
			require_once('WhosWho4/Mailings/View/Mailing.view.php');
			break;
		case 'MailingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/MailingView.gen.php');
			break;
		case 'Mailing_Generated':
			require_once('WhosWho4/Mailings/DBObject/Generated/Mailing.gen.php');
			break;
		case 'Mailing_MailingList':
			require_once('WhosWho4/Mailings/DBObject/Mailing_MailingList.cls.php');
			break;
		case 'Mailing_MailingListQuery':
			require_once('WhosWho4/Mailings/Query/Mailing_MailingList.query.php');
			break;
		case 'Mailing_MailingListQuery_Generated':
			require_once('WhosWho4/Mailings/Query/Generated/Mailing_MailingListQuery.gen.php');
			break;
		case 'Mailing_MailingListVerzameling':
			require_once('WhosWho4/Mailings/Verzameling/Mailing_MailingListVerzameling.cls.php');
			break;
		case 'Mailing_MailingListVerzamelingView':
			require_once('WhosWho4/Mailings/View/Mailing_MailingListVerzameling.view.php');
			break;
		case 'Mailing_MailingListVerzamelingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/Mailing_MailingListVerzamelingView.gen.php');
			break;
		case 'Mailing_MailingListVerzameling_Generated':
			require_once('WhosWho4/Mailings/Verzameling/Generated/Mailing_MailingListVerzameling.gen.php');
			break;
		case 'Mailing_MailingListView':
			require_once('WhosWho4/Mailings/View/Mailing_MailingList.view.php');
			break;
		case 'Mailing_MailingListView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/Mailing_MailingListView.gen.php');
			break;
		case 'Mailing_MailingList_Generated':
			require_once('WhosWho4/Mailings/DBObject/Generated/Mailing_MailingList.gen.php');
			break;
		case 'TinyMailingUrl':
			require_once('WhosWho4/Mailings/DBObject/TinyMailingUrl.cls.php');
			break;
		case 'TinyMailingUrlQuery':
			require_once('WhosWho4/Mailings/Query/TinyMailingUrl.query.php');
			break;
		case 'TinyMailingUrlQuery_Generated':
			require_once('WhosWho4/Mailings/Query/Generated/TinyMailingUrlQuery.gen.php');
			break;
		case 'TinyMailingUrlVerzameling':
			require_once('WhosWho4/Mailings/Verzameling/TinyMailingUrlVerzameling.cls.php');
			break;
		case 'TinyMailingUrlVerzamelingView':
			require_once('WhosWho4/Mailings/View/TinyMailingUrlVerzameling.view.php');
			break;
		case 'TinyMailingUrlVerzamelingView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/TinyMailingUrlVerzamelingView.gen.php');
			break;
		case 'TinyMailingUrlVerzameling_Generated':
			require_once('WhosWho4/Mailings/Verzameling/Generated/TinyMailingUrlVerzameling.gen.php');
			break;
		case 'TinyMailingUrlView':
			require_once('WhosWho4/Mailings/View/TinyMailingUrl.view.php');
			break;
		case 'TinyMailingUrlView_Generated':
			require_once('WhosWho4/Mailings/View/Generated/TinyMailingUrlView.gen.php');
			break;
		case 'TinyMailingUrl_Generated':
			require_once('WhosWho4/Mailings/DBObject/Generated/TinyMailingUrl.gen.php');
			break;

		case 'Bedrijf':
			require_once('WhosWho4/Spookweb2/DBObject/Bedrijf.cls.php');
			break;
		case 'BedrijfQuery':
			require_once('WhosWho4/Spookweb2/Query/Bedrijf.query.php');
			break;
		case 'BedrijfQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/BedrijfQuery.gen.php');
			break;
		case 'BedrijfStudie':
			require_once('WhosWho4/Spookweb2/DBObject/BedrijfStudie.cls.php');
			break;
		case 'BedrijfStudieQuery':
			require_once('WhosWho4/Spookweb2/Query/BedrijfStudie.query.php');
			break;
		case 'BedrijfStudieQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/BedrijfStudieQuery.gen.php');
			break;
		case 'BedrijfStudieVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/BedrijfStudieVerzameling.cls.php');
			break;
		case 'BedrijfStudieVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/BedrijfStudieVerzameling.view.php');
			break;
		case 'BedrijfStudieVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/BedrijfStudieVerzamelingView.gen.php');
			break;
		case 'BedrijfStudieVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/BedrijfStudieVerzameling.gen.php');
			break;
		case 'BedrijfStudieView':
			require_once('WhosWho4/Spookweb2/View/BedrijfStudie.view.php');
			break;
		case 'BedrijfStudieView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/BedrijfStudieView.gen.php');
			break;
		case 'BedrijfStudie_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/BedrijfStudie.gen.php');
			break;
		case 'BedrijfVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/BedrijfVerzameling.cls.php');
			break;
		case 'BedrijfVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/BedrijfVerzameling.view.php');
			break;
		case 'BedrijfVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/BedrijfVerzamelingView.gen.php');
			break;
		case 'BedrijfVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/BedrijfVerzameling.gen.php');
			break;
		case 'BedrijfView':
			require_once('WhosWho4/Spookweb2/View/Bedrijf.view.php');
			break;
		case 'BedrijfView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/BedrijfView.gen.php');
			break;
		case 'Bedrijf_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/Bedrijf.gen.php');
			break;
		case 'Bedrijfsprofiel':
			require_once('WhosWho4/Spookweb2/DBObject/Bedrijfsprofiel.cls.php');
			break;
		case 'BedrijfsprofielQuery':
			require_once('WhosWho4/Spookweb2/Query/Bedrijfsprofiel.query.php');
			break;
		case 'BedrijfsprofielQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/BedrijfsprofielQuery.gen.php');
			break;
		case 'BedrijfsprofielVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/BedrijfsprofielVerzameling.cls.php');
			break;
		case 'BedrijfsprofielVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/BedrijfsprofielVerzameling.view.php');
			break;
		case 'BedrijfsprofielVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/BedrijfsprofielVerzamelingView.gen.php');
			break;
		case 'BedrijfsprofielVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/BedrijfsprofielVerzameling.gen.php');
			break;
		case 'BedrijfsprofielView':
			require_once('WhosWho4/Spookweb2/View/Bedrijfsprofiel.view.php');
			break;
		case 'BedrijfsprofielView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/BedrijfsprofielView.gen.php');
			break;
		case 'Bedrijfsprofiel_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/Bedrijfsprofiel.gen.php');
			break;
		case 'Contract':
			require_once('WhosWho4/Spookweb2/DBObject/Contract.cls.php');
			break;
		case 'ContractQuery':
			require_once('WhosWho4/Spookweb2/Query/Contract.query.php');
			break;
		case 'ContractQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/ContractQuery.gen.php');
			break;
		case 'ContractVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/ContractVerzameling.cls.php');
			break;
		case 'ContractVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/ContractVerzameling.view.php');
			break;
		case 'ContractVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/ContractVerzamelingView.gen.php');
			break;
		case 'ContractVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/ContractVerzameling.gen.php');
			break;
		case 'ContractView':
			require_once('WhosWho4/Spookweb2/View/Contract.view.php');
			break;
		case 'ContractView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/ContractView.gen.php');
			break;
		case 'Contract_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/Contract.gen.php');
			break;
		case 'Contractonderdeel':
			require_once('WhosWho4/Spookweb2/DBObject/Contractonderdeel.cls.php');
			break;
		case 'ContractonderdeelQuery':
			require_once('WhosWho4/Spookweb2/Query/Contractonderdeel.query.php');
			break;
		case 'ContractonderdeelQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/ContractonderdeelQuery.gen.php');
			break;
		case 'ContractonderdeelVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/ContractonderdeelVerzameling.cls.php');
			break;
		case 'ContractonderdeelVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/ContractonderdeelVerzameling.view.php');
			break;
		case 'ContractonderdeelVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/ContractonderdeelVerzamelingView.gen.php');
			break;
		case 'ContractonderdeelVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/ContractonderdeelVerzameling.gen.php');
			break;
		case 'ContractonderdeelView':
			require_once('WhosWho4/Spookweb2/View/Contractonderdeel.view.php');
			break;
		case 'ContractonderdeelView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/ContractonderdeelView.gen.php');
			break;
		case 'Contractonderdeel_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/Contractonderdeel.gen.php');
			break;
		case 'Interactie':
			require_once('WhosWho4/Spookweb2/DBObject/Interactie.cls.php');
			break;
		case 'InteractieQuery':
			require_once('WhosWho4/Spookweb2/Query/Interactie.query.php');
			break;
		case 'InteractieQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/InteractieQuery.gen.php');
			break;
		case 'InteractieVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/InteractieVerzameling.cls.php');
			break;
		case 'InteractieVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/InteractieVerzameling.view.php');
			break;
		case 'InteractieVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/InteractieVerzamelingView.gen.php');
			break;
		case 'InteractieVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/InteractieVerzameling.gen.php');
			break;
		case 'InteractieView':
			require_once('WhosWho4/Spookweb2/View/Interactie.view.php');
			break;
		case 'InteractieView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/InteractieView.gen.php');
			break;
		case 'Interactie_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/Interactie.gen.php');
			break;
		case 'Vacature':
			require_once('WhosWho4/Spookweb2/DBObject/Vacature.cls.php');
			break;
		case 'VacatureQuery':
			require_once('WhosWho4/Spookweb2/Query/Vacature.query.php');
			break;
		case 'VacatureQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/VacatureQuery.gen.php');
			break;
		case 'VacatureStudie':
			require_once('WhosWho4/Spookweb2/DBObject/VacatureStudie.cls.php');
			break;
		case 'VacatureStudieQuery':
			require_once('WhosWho4/Spookweb2/Query/VacatureStudie.query.php');
			break;
		case 'VacatureStudieQuery_Generated':
			require_once('WhosWho4/Spookweb2/Query/Generated/VacatureStudieQuery.gen.php');
			break;
		case 'VacatureStudieVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/VacatureStudieVerzameling.cls.php');
			break;
		case 'VacatureStudieVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/VacatureStudieVerzameling.view.php');
			break;
		case 'VacatureStudieVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/VacatureStudieVerzamelingView.gen.php');
			break;
		case 'VacatureStudieVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/VacatureStudieVerzameling.gen.php');
			break;
		case 'VacatureStudieView':
			require_once('WhosWho4/Spookweb2/View/VacatureStudie.view.php');
			break;
		case 'VacatureStudieView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/VacatureStudieView.gen.php');
			break;
		case 'VacatureStudie_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/VacatureStudie.gen.php');
			break;
		case 'VacatureVerzameling':
			require_once('WhosWho4/Spookweb2/Verzameling/VacatureVerzameling.cls.php');
			break;
		case 'VacatureVerzamelingView':
			require_once('WhosWho4/Spookweb2/View/VacatureVerzameling.view.php');
			break;
		case 'VacatureVerzamelingView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/VacatureVerzamelingView.gen.php');
			break;
		case 'VacatureVerzameling_Generated':
			require_once('WhosWho4/Spookweb2/Verzameling/Generated/VacatureVerzameling.gen.php');
			break;
		case 'VacatureView':
			require_once('WhosWho4/Spookweb2/View/Vacature.view.php');
			break;
		case 'VacatureView_Generated':
			require_once('WhosWho4/Spookweb2/View/Generated/VacatureView.gen.php');
			break;
		case 'Vacature_Generated':
			require_once('WhosWho4/Spookweb2/DBObject/Generated/Vacature.gen.php');
			break;

		case 'Activiteit':
			require_once('WhosWho4/DBObject/Activiteit.cls.php');
			break;
		case 'ActiviteitCategorie':
			require_once('WhosWho4/DBObject/ActiviteitCategorie.cls.php');
			break;
		case 'ActiviteitCategorieQuery':
			require_once('WhosWho4/Query/ActiviteitCategorie.query.php');
			break;
		case 'ActiviteitCategorieQuery_Generated':
			require_once('WhosWho4/Query/Generated/ActiviteitCategorieQuery.gen.php');
			break;
		case 'ActiviteitCategorieVerzameling':
			require_once('WhosWho4/Verzameling/ActiviteitCategorieVerzameling.cls.php');
			break;
		case 'ActiviteitCategorieVerzamelingView':
			require_once('WhosWho4/View/ActiviteitCategorieVerzameling.view.php');
			break;
		case 'ActiviteitCategorieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitCategorieVerzamelingView.gen.php');
			break;
		case 'ActiviteitCategorieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ActiviteitCategorieVerzameling.gen.php');
			break;
		case 'ActiviteitCategorieView':
			require_once('WhosWho4/View/ActiviteitCategorie.view.php');
			break;
		case 'ActiviteitCategorieView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitCategorieView.gen.php');
			break;
		case 'ActiviteitCategorie_Generated':
			require_once('WhosWho4/DBObject/Generated/ActiviteitCategorie.gen.php');
			break;
		case 'ActiviteitHerhaling':
			require_once('WhosWho4/DBObject/ActiviteitHerhaling.cls.php');
			break;
		case 'ActiviteitHerhalingQuery':
			require_once('WhosWho4/Query/ActiviteitHerhaling.query.php');
			break;
		case 'ActiviteitHerhalingQuery_Generated':
			require_once('WhosWho4/Query/Generated/ActiviteitHerhalingQuery.gen.php');
			break;
		case 'ActiviteitHerhalingVerzameling':
			require_once('WhosWho4/Verzameling/ActiviteitHerhalingVerzameling.cls.php');
			break;
		case 'ActiviteitHerhalingVerzamelingView':
			require_once('WhosWho4/View/ActiviteitHerhalingVerzameling.view.php');
			break;
		case 'ActiviteitHerhalingVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitHerhalingVerzamelingView.gen.php');
			break;
		case 'ActiviteitHerhalingVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ActiviteitHerhalingVerzameling.gen.php');
			break;
		case 'ActiviteitHerhalingView':
			require_once('WhosWho4/View/ActiviteitHerhaling.view.php');
			break;
		case 'ActiviteitHerhalingView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitHerhalingView.gen.php');
			break;
		case 'ActiviteitHerhaling_Generated':
			require_once('WhosWho4/DBObject/Generated/ActiviteitHerhaling.gen.php');
			break;
		case 'ActiviteitInformatie':
			require_once('WhosWho4/DBObject/ActiviteitInformatie.cls.php');
			break;
		case 'ActiviteitInformatieQuery':
			require_once('WhosWho4/Query/ActiviteitInformatie.query.php');
			break;
		case 'ActiviteitInformatieQuery_Generated':
			require_once('WhosWho4/Query/Generated/ActiviteitInformatieQuery.gen.php');
			break;
		case 'ActiviteitInformatieVerzameling':
			require_once('WhosWho4/Verzameling/ActiviteitInformatieVerzameling.cls.php');
			break;
		case 'ActiviteitInformatieVerzamelingView':
			require_once('WhosWho4/View/ActiviteitInformatieVerzameling.view.php');
			break;
		case 'ActiviteitInformatieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitInformatieVerzamelingView.gen.php');
			break;
		case 'ActiviteitInformatieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ActiviteitInformatieVerzameling.gen.php');
			break;
		case 'ActiviteitInformatieView':
			require_once('WhosWho4/View/ActiviteitInformatie.view.php');
			break;
		case 'ActiviteitInformatieView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitInformatieView.gen.php');
			break;
		case 'ActiviteitInformatie_Generated':
			require_once('WhosWho4/DBObject/Generated/ActiviteitInformatie.gen.php');
			break;
		case 'ActiviteitQuery':
			require_once('WhosWho4/Query/Activiteit.query.php');
			break;
		case 'ActiviteitQuery_Generated':
			require_once('WhosWho4/Query/Generated/ActiviteitQuery.gen.php');
			break;
		case 'ActiviteitVerzameling':
			require_once('WhosWho4/Verzameling/ActiviteitVerzameling.cls.php');
			break;
		case 'ActiviteitVerzamelingView':
			require_once('WhosWho4/View/ActiviteitVerzameling.view.php');
			break;
		case 'ActiviteitVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitVerzamelingView.gen.php');
			break;
		case 'ActiviteitVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ActiviteitVerzameling.gen.php');
			break;
		case 'ActiviteitView':
			require_once('WhosWho4/View/Activiteit.view.php');
			break;
		case 'ActiviteitView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitView.gen.php');
			break;
		case 'ActiviteitVraag':
			require_once('WhosWho4/DBObject/ActiviteitVraag.cls.php');
			break;
		case 'ActiviteitVraagQuery':
			require_once('WhosWho4/Query/ActiviteitVraag.query.php');
			break;
		case 'ActiviteitVraagQuery_Generated':
			require_once('WhosWho4/Query/Generated/ActiviteitVraagQuery.gen.php');
			break;
		case 'ActiviteitVraagVerzameling':
			require_once('WhosWho4/Verzameling/ActiviteitVraagVerzameling.cls.php');
			break;
		case 'ActiviteitVraagVerzamelingView':
			require_once('WhosWho4/View/ActiviteitVraagVerzameling.view.php');
			break;
		case 'ActiviteitVraagVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitVraagVerzamelingView.gen.php');
			break;
		case 'ActiviteitVraagVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ActiviteitVraagVerzameling.gen.php');
			break;
		case 'ActiviteitVraagView':
			require_once('WhosWho4/View/ActiviteitVraag.view.php');
			break;
		case 'ActiviteitVraagView_Generated':
			require_once('WhosWho4/View/Generated/ActiviteitVraagView.gen.php');
			break;
		case 'ActiviteitVraag_Generated':
			require_once('WhosWho4/DBObject/Generated/ActiviteitVraag.gen.php');
			break;
		case 'Activiteit_Generated':
			require_once('WhosWho4/DBObject/Generated/Activiteit.gen.php');
			break;
		case 'CSPReport':
			require_once('WhosWho4/DBObject/CSPReport.cls.php');
			break;
		case 'CSPReportQuery':
			require_once('WhosWho4/Query/CSPReport.query.php');
			break;
		case 'CSPReportQuery_Generated':
			require_once('WhosWho4/Query/Generated/CSPReportQuery.gen.php');
			break;
		case 'CSPReportVerzameling':
			require_once('WhosWho4/Verzameling/CSPReportVerzameling.cls.php');
			break;
		case 'CSPReportVerzamelingView':
			require_once('WhosWho4/View/CSPReportVerzameling.view.php');
			break;
		case 'CSPReportVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/CSPReportVerzamelingView.gen.php');
			break;
		case 'CSPReportVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/CSPReportVerzameling.gen.php');
			break;
		case 'CSPReportView':
			require_once('WhosWho4/View/CSPReport.view.php');
			break;
		case 'CSPReportView_Generated':
			require_once('WhosWho4/View/Generated/CSPReportView.gen.php');
			break;
		case 'CSPReport_Generated':
			require_once('WhosWho4/DBObject/Generated/CSPReport.gen.php');
			break;
		case 'Commissie':
			require_once('WhosWho4/DBObject/Commissie.cls.php');
			break;
		case 'CommissieActiviteit':
			require_once('WhosWho4/DBObject/CommissieActiviteit.cls.php');
			break;
		case 'CommissieActiviteitQuery':
			require_once('WhosWho4/Query/CommissieActiviteit.query.php');
			break;
		case 'CommissieActiviteitQuery_Generated':
			require_once('WhosWho4/Query/Generated/CommissieActiviteitQuery.gen.php');
			break;
		case 'CommissieActiviteitVerzameling':
			require_once('WhosWho4/Verzameling/CommissieActiviteitVerzameling.cls.php');
			break;
		case 'CommissieActiviteitVerzamelingView':
			require_once('WhosWho4/View/CommissieActiviteitVerzameling.view.php');
			break;
		case 'CommissieActiviteitVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/CommissieActiviteitVerzamelingView.gen.php');
			break;
		case 'CommissieActiviteitVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/CommissieActiviteitVerzameling.gen.php');
			break;
		case 'CommissieActiviteitView':
			require_once('WhosWho4/View/CommissieActiviteit.view.php');
			break;
		case 'CommissieActiviteitView_Generated':
			require_once('WhosWho4/View/Generated/CommissieActiviteitView.gen.php');
			break;
		case 'CommissieActiviteit_Generated':
			require_once('WhosWho4/DBObject/Generated/CommissieActiviteit.gen.php');
			break;
		case 'CommissieCategorie':
			require_once('WhosWho4/DBObject/CommissieCategorie.cls.php');
			break;
		case 'CommissieCategorieQuery':
			require_once('WhosWho4/Query/CommissieCategorie.query.php');
			break;
		case 'CommissieCategorieQuery_Generated':
			require_once('WhosWho4/Query/Generated/CommissieCategorieQuery.gen.php');
			break;
		case 'CommissieCategorieVerzameling':
			require_once('WhosWho4/Verzameling/CommissieCategorieVerzameling.cls.php');
			break;
		case 'CommissieCategorieVerzamelingView':
			require_once('WhosWho4/View/CommissieCategorieVerzameling.view.php');
			break;
		case 'CommissieCategorieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/CommissieCategorieVerzamelingView.gen.php');
			break;
		case 'CommissieCategorieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/CommissieCategorieVerzameling.gen.php');
			break;
		case 'CommissieCategorieView':
			require_once('WhosWho4/View/CommissieCategorie.view.php');
			break;
		case 'CommissieCategorieView_Generated':
			require_once('WhosWho4/View/Generated/CommissieCategorieView.gen.php');
			break;
		case 'CommissieCategorie_Generated':
			require_once('WhosWho4/DBObject/Generated/CommissieCategorie.gen.php');
			break;
		case 'CommissieLid':
			require_once('WhosWho4/DBObject/CommissieLid.cls.php');
			break;
		case 'CommissieLidQuery':
			require_once('WhosWho4/Query/CommissieLid.query.php');
			break;
		case 'CommissieLidQuery_Generated':
			require_once('WhosWho4/Query/Generated/CommissieLidQuery.gen.php');
			break;
		case 'CommissieLidVerzameling':
			require_once('WhosWho4/Verzameling/CommissieLidVerzameling.cls.php');
			break;
		case 'CommissieLidVerzamelingView':
			require_once('WhosWho4/View/CommissieLidVerzameling.view.php');
			break;
		case 'CommissieLidVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/CommissieLidVerzamelingView.gen.php');
			break;
		case 'CommissieLidVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/CommissieLidVerzameling.gen.php');
			break;
		case 'CommissieLidView':
			require_once('WhosWho4/View/CommissieLid.view.php');
			break;
		case 'CommissieLidView_Generated':
			require_once('WhosWho4/View/Generated/CommissieLidView.gen.php');
			break;
		case 'CommissieLid_Generated':
			require_once('WhosWho4/DBObject/Generated/CommissieLid.gen.php');
			break;
		case 'CommissieQuery':
			require_once('WhosWho4/Query/Commissie.query.php');
			break;
		case 'CommissieQuery_Generated':
			require_once('WhosWho4/Query/Generated/CommissieQuery.gen.php');
			break;
		case 'CommissieVerzameling':
			require_once('WhosWho4/Verzameling/CommissieVerzameling.cls.php');
			break;
		case 'CommissieVerzamelingView':
			require_once('WhosWho4/View/CommissieVerzameling.view.php');
			break;
		case 'CommissieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/CommissieVerzamelingView.gen.php');
			break;
		case 'CommissieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/CommissieVerzameling.gen.php');
			break;
		case 'CommissieView':
			require_once('WhosWho4/View/Commissie.view.php');
			break;
		case 'CommissieView_Generated':
			require_once('WhosWho4/View/Generated/CommissieView.gen.php');
			break;
		case 'Commissie_Generated':
			require_once('WhosWho4/DBObject/Generated/Commissie.gen.php');
			break;
		case 'Contact':
			require_once('WhosWho4/DBObject/Contact.cls.php');
			break;
		case 'ContactAdres':
			require_once('WhosWho4/DBObject/ContactAdres.cls.php');
			break;
		case 'ContactAdresQuery':
			require_once('WhosWho4/Query/ContactAdres.query.php');
			break;
		case 'ContactAdresQuery_Generated':
			require_once('WhosWho4/Query/Generated/ContactAdresQuery.gen.php');
			break;
		case 'ContactAdresVerzameling':
			require_once('WhosWho4/Verzameling/ContactAdresVerzameling.cls.php');
			break;
		case 'ContactAdresVerzamelingView':
			require_once('WhosWho4/View/ContactAdresVerzameling.view.php');
			break;
		case 'ContactAdresVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ContactAdresVerzamelingView.gen.php');
			break;
		case 'ContactAdresVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ContactAdresVerzameling.gen.php');
			break;
		case 'ContactAdresView':
			require_once('WhosWho4/View/ContactAdres.view.php');
			break;
		case 'ContactAdresView_Generated':
			require_once('WhosWho4/View/Generated/ContactAdresView.gen.php');
			break;
		case 'ContactAdres_Generated':
			require_once('WhosWho4/DBObject/Generated/ContactAdres.gen.php');
			break;
		case 'ContactMailingList':
			require_once('WhosWho4/DBObject/ContactMailingList.cls.php');
			break;
		case 'ContactMailingListQuery':
			require_once('WhosWho4/Query/ContactMailingList.query.php');
			break;
		case 'ContactMailingListQuery_Generated':
			require_once('WhosWho4/Query/Generated/ContactMailingListQuery.gen.php');
			break;
		case 'ContactMailingListVerzameling':
			require_once('WhosWho4/Verzameling/ContactMailingListVerzameling.cls.php');
			break;
		case 'ContactMailingListVerzamelingView':
			require_once('WhosWho4/View/ContactMailingListVerzameling.view.php');
			break;
		case 'ContactMailingListVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ContactMailingListVerzamelingView.gen.php');
			break;
		case 'ContactMailingListVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ContactMailingListVerzameling.gen.php');
			break;
		case 'ContactMailingListView':
			require_once('WhosWho4/View/ContactMailingList.view.php');
			break;
		case 'ContactMailingListView_Generated':
			require_once('WhosWho4/View/Generated/ContactMailingListView.gen.php');
			break;
		case 'ContactMailingList_Generated':
			require_once('WhosWho4/DBObject/Generated/ContactMailingList.gen.php');
			break;
		case 'ContactPersoon':
			require_once('WhosWho4/DBObject/ContactPersoon.cls.php');
			break;
		case 'ContactPersoonQuery':
			require_once('WhosWho4/Query/ContactPersoon.query.php');
			break;
		case 'ContactPersoonQuery_Generated':
			require_once('WhosWho4/Query/Generated/ContactPersoonQuery.gen.php');
			break;
		case 'ContactPersoonVerzameling':
			require_once('WhosWho4/Verzameling/ContactPersoonVerzameling.cls.php');
			break;
		case 'ContactPersoonVerzamelingView':
			require_once('WhosWho4/View/ContactPersoonVerzameling.view.php');
			break;
		case 'ContactPersoonVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ContactPersoonVerzamelingView.gen.php');
			break;
		case 'ContactPersoonVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ContactPersoonVerzameling.gen.php');
			break;
		case 'ContactPersoonView':
			require_once('WhosWho4/View/ContactPersoon.view.php');
			break;
		case 'ContactPersoonView_Generated':
			require_once('WhosWho4/View/Generated/ContactPersoonView.gen.php');
			break;
		case 'ContactPersoon_Generated':
			require_once('WhosWho4/DBObject/Generated/ContactPersoon.gen.php');
			break;
		case 'ContactQuery':
			require_once('WhosWho4/Query/Contact.query.php');
			break;
		case 'ContactQuery_Generated':
			require_once('WhosWho4/Query/Generated/ContactQuery.gen.php');
			break;
		case 'ContactTelnr':
			require_once('WhosWho4/DBObject/ContactTelnr.cls.php');
			break;
		case 'ContactTelnrQuery':
			require_once('WhosWho4/Query/ContactTelnr.query.php');
			break;
		case 'ContactTelnrQuery_Generated':
			require_once('WhosWho4/Query/Generated/ContactTelnrQuery.gen.php');
			break;
		case 'ContactTelnrVerzameling':
			require_once('WhosWho4/Verzameling/ContactTelnrVerzameling.cls.php');
			break;
		case 'ContactTelnrVerzamelingView':
			require_once('WhosWho4/View/ContactTelnrVerzameling.view.php');
			break;
		case 'ContactTelnrVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ContactTelnrVerzamelingView.gen.php');
			break;
		case 'ContactTelnrVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ContactTelnrVerzameling.gen.php');
			break;
		case 'ContactTelnrView':
			require_once('WhosWho4/View/ContactTelnr.view.php');
			break;
		case 'ContactTelnrView_Generated':
			require_once('WhosWho4/View/Generated/ContactTelnrView.gen.php');
			break;
		case 'ContactTelnr_Generated':
			require_once('WhosWho4/DBObject/Generated/ContactTelnr.gen.php');
			break;
		case 'ContactVerzameling':
			require_once('WhosWho4/Verzameling/ContactVerzameling.cls.php');
			break;
		case 'ContactVerzamelingView':
			require_once('WhosWho4/View/ContactVerzameling.view.php');
			break;
		case 'ContactVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ContactVerzamelingView.gen.php');
			break;
		case 'ContactVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ContactVerzameling.gen.php');
			break;
		case 'ContactView':
			require_once('WhosWho4/View/Contact.view.php');
			break;
		case 'ContactView_Generated':
			require_once('WhosWho4/View/Generated/ContactView.gen.php');
			break;
		case 'Contact_Generated':
			require_once('WhosWho4/DBObject/Generated/Contact.gen.php');
			break;
		case 'Deelnemer':
			require_once('WhosWho4/DBObject/Deelnemer.cls.php');
			break;
		case 'DeelnemerAntwoord':
			require_once('WhosWho4/DBObject/DeelnemerAntwoord.cls.php');
			break;
		case 'DeelnemerAntwoordQuery':
			require_once('WhosWho4/Query/DeelnemerAntwoord.query.php');
			break;
		case 'DeelnemerAntwoordQuery_Generated':
			require_once('WhosWho4/Query/Generated/DeelnemerAntwoordQuery.gen.php');
			break;
		case 'DeelnemerAntwoordVerzameling':
			require_once('WhosWho4/Verzameling/DeelnemerAntwoordVerzameling.cls.php');
			break;
		case 'DeelnemerAntwoordVerzamelingView':
			require_once('WhosWho4/View/DeelnemerAntwoordVerzameling.view.php');
			break;
		case 'DeelnemerAntwoordVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/DeelnemerAntwoordVerzamelingView.gen.php');
			break;
		case 'DeelnemerAntwoordVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/DeelnemerAntwoordVerzameling.gen.php');
			break;
		case 'DeelnemerAntwoordView':
			require_once('WhosWho4/View/DeelnemerAntwoord.view.php');
			break;
		case 'DeelnemerAntwoordView_Generated':
			require_once('WhosWho4/View/Generated/DeelnemerAntwoordView.gen.php');
			break;
		case 'DeelnemerAntwoord_Generated':
			require_once('WhosWho4/DBObject/Generated/DeelnemerAntwoord.gen.php');
			break;
		case 'DeelnemerQuery':
			require_once('WhosWho4/Query/Deelnemer.query.php');
			break;
		case 'DeelnemerQuery_Generated':
			require_once('WhosWho4/Query/Generated/DeelnemerQuery.gen.php');
			break;
		case 'DeelnemerVerzameling':
			require_once('WhosWho4/Verzameling/DeelnemerVerzameling.cls.php');
			break;
		case 'DeelnemerVerzamelingView':
			require_once('WhosWho4/View/DeelnemerVerzameling.view.php');
			break;
		case 'DeelnemerVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/DeelnemerVerzamelingView.gen.php');
			break;
		case 'DeelnemerVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/DeelnemerVerzameling.gen.php');
			break;
		case 'DeelnemerView':
			require_once('WhosWho4/View/Deelnemer.view.php');
			break;
		case 'DeelnemerView_Generated':
			require_once('WhosWho4/View/Generated/DeelnemerView.gen.php');
			break;
		case 'Deelnemer_Generated':
			require_once('WhosWho4/DBObject/Generated/Deelnemer.gen.php');
			break;
		case 'DocuBestand':
			require_once('WhosWho4/DBObject/DocuBestand.cls.php');
			break;
		case 'DocuBestandQuery':
			require_once('WhosWho4/Query/DocuBestand.query.php');
			break;
		case 'DocuBestandQuery_Generated':
			require_once('WhosWho4/Query/Generated/DocuBestandQuery.gen.php');
			break;
		case 'DocuBestandVerzameling':
			require_once('WhosWho4/Verzameling/DocuBestandVerzameling.cls.php');
			break;
		case 'DocuBestandVerzamelingView':
			require_once('WhosWho4/View/DocuBestandVerzameling.view.php');
			break;
		case 'DocuBestandVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/DocuBestandVerzamelingView.gen.php');
			break;
		case 'DocuBestandVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/DocuBestandVerzameling.gen.php');
			break;
		case 'DocuBestandView':
			require_once('WhosWho4/View/DocuBestand.view.php');
			break;
		case 'DocuBestandView_Generated':
			require_once('WhosWho4/View/Generated/DocuBestandView.gen.php');
			break;
		case 'DocuBestand_Generated':
			require_once('WhosWho4/DBObject/Generated/DocuBestand.gen.php');
			break;
		case 'DocuCategorie':
			require_once('WhosWho4/DBObject/DocuCategorie.cls.php');
			break;
		case 'DocuCategorieQuery':
			require_once('WhosWho4/Query/DocuCategorie.query.php');
			break;
		case 'DocuCategorieQuery_Generated':
			require_once('WhosWho4/Query/Generated/DocuCategorieQuery.gen.php');
			break;
		case 'DocuCategorieVerzameling':
			require_once('WhosWho4/Verzameling/DocuCategorieVerzameling.cls.php');
			break;
		case 'DocuCategorieVerzamelingView':
			require_once('WhosWho4/View/DocuCategorieVerzameling.view.php');
			break;
		case 'DocuCategorieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/DocuCategorieVerzamelingView.gen.php');
			break;
		case 'DocuCategorieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/DocuCategorieVerzameling.gen.php');
			break;
		case 'DocuCategorieView':
			require_once('WhosWho4/View/DocuCategorie.view.php');
			break;
		case 'DocuCategorieView_Generated':
			require_once('WhosWho4/View/Generated/DocuCategorieView.gen.php');
			break;
		case 'DocuCategorie_Generated':
			require_once('WhosWho4/DBObject/Generated/DocuCategorie.gen.php');
			break;
		case 'Donateur':
			require_once('WhosWho4/DBObject/Donateur.cls.php');
			break;
		case 'DonateurQuery':
			require_once('WhosWho4/Query/Donateur.query.php');
			break;
		case 'DonateurQuery_Generated':
			require_once('WhosWho4/Query/Generated/DonateurQuery.gen.php');
			break;
		case 'DonateurVerzameling':
			require_once('WhosWho4/Verzameling/DonateurVerzameling.cls.php');
			break;
		case 'DonateurVerzamelingView':
			require_once('WhosWho4/View/DonateurVerzameling.view.php');
			break;
		case 'DonateurVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/DonateurVerzamelingView.gen.php');
			break;
		case 'DonateurVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/DonateurVerzameling.gen.php');
			break;
		case 'DonateurView':
			require_once('WhosWho4/View/Donateur.view.php');
			break;
		case 'DonateurView_Generated':
			require_once('WhosWho4/View/Generated/DonateurView.gen.php');
			break;
		case 'Donateur_Generated':
			require_once('WhosWho4/DBObject/Generated/Donateur.gen.php');
			break;
		case 'ExtraPoster':
			require_once('WhosWho4/DBObject/ExtraPoster.cls.php');
			break;
		case 'ExtraPosterQuery':
			require_once('WhosWho4/Query/ExtraPoster.query.php');
			break;
		case 'ExtraPosterQuery_Generated':
			require_once('WhosWho4/Query/Generated/ExtraPosterQuery.gen.php');
			break;
		case 'ExtraPosterVerzameling':
			require_once('WhosWho4/Verzameling/ExtraPosterVerzameling.cls.php');
			break;
		case 'ExtraPosterVerzamelingView':
			require_once('WhosWho4/View/ExtraPosterVerzameling.view.php');
			break;
		case 'ExtraPosterVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/ExtraPosterVerzamelingView.gen.php');
			break;
		case 'ExtraPosterVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/ExtraPosterVerzameling.gen.php');
			break;
		case 'ExtraPosterView':
			require_once('WhosWho4/View/ExtraPoster.view.php');
			break;
		case 'ExtraPosterView_Generated':
			require_once('WhosWho4/View/Generated/ExtraPosterView.gen.php');
			break;
		case 'ExtraPoster_Generated':
			require_once('WhosWho4/DBObject/Generated/ExtraPoster.gen.php');
			break;
		case 'KartVoorwerp':
			require_once('WhosWho4/DBObject/KartVoorwerp.cls.php');
			break;
		case 'KartVoorwerpQuery':
			require_once('WhosWho4/Query/KartVoorwerp.query.php');
			break;
		case 'KartVoorwerpQuery_Generated':
			require_once('WhosWho4/Query/Generated/KartVoorwerpQuery.gen.php');
			break;
		case 'KartVoorwerpVerzameling':
			require_once('WhosWho4/Verzameling/KartVoorwerpVerzameling.cls.php');
			break;
		case 'KartVoorwerpVerzamelingView':
			require_once('WhosWho4/View/KartVoorwerpVerzameling.view.php');
			break;
		case 'KartVoorwerpVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/KartVoorwerpVerzamelingView.gen.php');
			break;
		case 'KartVoorwerpVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/KartVoorwerpVerzameling.gen.php');
			break;
		case 'KartVoorwerpView':
			require_once('WhosWho4/View/KartVoorwerp.view.php');
			break;
		case 'KartVoorwerpView_Generated':
			require_once('WhosWho4/View/Generated/KartVoorwerpView.gen.php');
			break;
		case 'KartVoorwerp_Generated':
			require_once('WhosWho4/DBObject/Generated/KartVoorwerp.gen.php');
			break;
		case 'Lid':
			require_once('WhosWho4/DBObject/Lid.cls.php');
			break;
		case 'LidQuery':
			require_once('WhosWho4/Query/Lid.query.php');
			break;
		case 'LidQuery_Generated':
			require_once('WhosWho4/Query/Generated/LidQuery.gen.php');
			break;
		case 'LidStudie':
			require_once('WhosWho4/DBObject/LidStudie.cls.php');
			break;
		case 'LidStudieQuery':
			require_once('WhosWho4/Query/LidStudie.query.php');
			break;
		case 'LidStudieQuery_Generated':
			require_once('WhosWho4/Query/Generated/LidStudieQuery.gen.php');
			break;
		case 'LidStudieVerzameling':
			require_once('WhosWho4/Verzameling/LidStudieVerzameling.cls.php');
			break;
		case 'LidStudieVerzamelingView':
			require_once('WhosWho4/View/LidStudieVerzameling.view.php');
			break;
		case 'LidStudieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/LidStudieVerzamelingView.gen.php');
			break;
		case 'LidStudieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/LidStudieVerzameling.gen.php');
			break;
		case 'LidStudieView':
			require_once('WhosWho4/View/LidStudie.view.php');
			break;
		case 'LidStudieView_Generated':
			require_once('WhosWho4/View/Generated/LidStudieView.gen.php');
			break;
		case 'LidStudie_Generated':
			require_once('WhosWho4/DBObject/Generated/LidStudie.gen.php');
			break;
		case 'LidVerzameling':
			require_once('WhosWho4/Verzameling/LidVerzameling.cls.php');
			break;
		case 'LidVerzamelingView':
			require_once('WhosWho4/View/LidVerzameling.view.php');
			break;
		case 'LidVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/LidVerzamelingView.gen.php');
			break;
		case 'LidVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/LidVerzameling.gen.php');
			break;
		case 'LidView':
			require_once('WhosWho4/View/Lid.view.php');
			break;
		case 'LidView_Generated':
			require_once('WhosWho4/View/Generated/LidView.gen.php');
			break;
		case 'Lid_Generated':
			require_once('WhosWho4/DBObject/Generated/Lid.gen.php');
			break;
		case 'Mededeling':
			require_once('WhosWho4/DBObject/Mededeling.cls.php');
			break;
		case 'MededelingQuery':
			require_once('WhosWho4/Query/Mededeling.query.php');
			break;
		case 'MededelingQuery_Generated':
			require_once('WhosWho4/Query/Generated/MededelingQuery.gen.php');
			break;
		case 'MededelingVerzameling':
			require_once('WhosWho4/Verzameling/MededelingVerzameling.cls.php');
			break;
		case 'MededelingVerzamelingView':
			require_once('WhosWho4/View/MededelingVerzameling.view.php');
			break;
		case 'MededelingVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/MededelingVerzamelingView.gen.php');
			break;
		case 'MededelingVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/MededelingVerzameling.gen.php');
			break;
		case 'MededelingView':
			require_once('WhosWho4/View/Mededeling.view.php');
			break;
		case 'MededelingView_Generated':
			require_once('WhosWho4/View/Generated/MededelingView.gen.php');
			break;
		case 'Mededeling_Generated':
			require_once('WhosWho4/DBObject/Generated/Mededeling.gen.php');
			break;
		case 'Organisatie':
			require_once('WhosWho4/DBObject/Organisatie.cls.php');
			break;
		case 'OrganisatieQuery':
			require_once('WhosWho4/Query/Organisatie.query.php');
			break;
		case 'OrganisatieQuery_Generated':
			require_once('WhosWho4/Query/Generated/OrganisatieQuery.gen.php');
			break;
		case 'OrganisatieVerzameling':
			require_once('WhosWho4/Verzameling/OrganisatieVerzameling.cls.php');
			break;
		case 'OrganisatieVerzamelingView':
			require_once('WhosWho4/View/OrganisatieVerzameling.view.php');
			break;
		case 'OrganisatieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/OrganisatieVerzamelingView.gen.php');
			break;
		case 'OrganisatieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/OrganisatieVerzameling.gen.php');
			break;
		case 'OrganisatieView':
			require_once('WhosWho4/View/Organisatie.view.php');
			break;
		case 'OrganisatieView_Generated':
			require_once('WhosWho4/View/Generated/OrganisatieView.gen.php');
			break;
		case 'Organisatie_Generated':
			require_once('WhosWho4/DBObject/Generated/Organisatie.gen.php');
			break;
		case 'PapierMolen':
			require_once('WhosWho4/DBObject/PapierMolen.cls.php');
			break;
		case 'PapierMolenQuery':
			require_once('WhosWho4/Query/PapierMolen.query.php');
			break;
		case 'PapierMolenQuery_Generated':
			require_once('WhosWho4/Query/Generated/PapierMolenQuery.gen.php');
			break;
		case 'PapierMolenVerzameling':
			require_once('WhosWho4/Verzameling/PapierMolenVerzameling.cls.php');
			break;
		case 'PapierMolenVerzamelingView':
			require_once('WhosWho4/View/PapierMolenVerzameling.view.php');
			break;
		case 'PapierMolenVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PapierMolenVerzamelingView.gen.php');
			break;
		case 'PapierMolenVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PapierMolenVerzameling.gen.php');
			break;
		case 'PapierMolenView':
			require_once('WhosWho4/View/PapierMolen.view.php');
			break;
		case 'PapierMolenView_Generated':
			require_once('WhosWho4/View/Generated/PapierMolenView.gen.php');
			break;
		case 'PapierMolen_Generated':
			require_once('WhosWho4/DBObject/Generated/PapierMolen.gen.php');
			break;
		case 'Persoon':
			require_once('WhosWho4/DBObject/Persoon.cls.php');
			break;
		case 'PersoonBadge':
			require_once('WhosWho4/DBObject/PersoonBadge.cls.php');
			break;
		case 'PersoonBadgeQuery':
			require_once('WhosWho4/Query/PersoonBadge.query.php');
			break;
		case 'PersoonBadgeQuery_Generated':
			require_once('WhosWho4/Query/Generated/PersoonBadgeQuery.gen.php');
			break;
		case 'PersoonBadgeVerzameling':
			require_once('WhosWho4/Verzameling/PersoonBadgeVerzameling.cls.php');
			break;
		case 'PersoonBadgeVerzamelingView':
			require_once('WhosWho4/View/PersoonBadgeVerzameling.view.php');
			break;
		case 'PersoonBadgeVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonBadgeVerzamelingView.gen.php');
			break;
		case 'PersoonBadgeVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PersoonBadgeVerzameling.gen.php');
			break;
		case 'PersoonBadgeView':
			require_once('WhosWho4/View/PersoonBadge.view.php');
			break;
		case 'PersoonBadgeView_Generated':
			require_once('WhosWho4/View/Generated/PersoonBadgeView.gen.php');
			break;
		case 'PersoonBadge_Generated':
			require_once('WhosWho4/DBObject/Generated/PersoonBadge.gen.php');
			break;
		case 'PersoonIM':
			require_once('WhosWho4/DBObject/PersoonIM.cls.php');
			break;
		case 'PersoonIMQuery':
			require_once('WhosWho4/Query/PersoonIM.query.php');
			break;
		case 'PersoonIMQuery_Generated':
			require_once('WhosWho4/Query/Generated/PersoonIMQuery.gen.php');
			break;
		case 'PersoonIMVerzameling':
			require_once('WhosWho4/Verzameling/PersoonIMVerzameling.cls.php');
			break;
		case 'PersoonIMVerzamelingView':
			require_once('WhosWho4/View/PersoonIMVerzameling.view.php');
			break;
		case 'PersoonIMVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonIMVerzamelingView.gen.php');
			break;
		case 'PersoonIMVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PersoonIMVerzameling.gen.php');
			break;
		case 'PersoonIMView':
			require_once('WhosWho4/View/PersoonIM.view.php');
			break;
		case 'PersoonIMView_Generated':
			require_once('WhosWho4/View/Generated/PersoonIMView.gen.php');
			break;
		case 'PersoonIM_Generated':
			require_once('WhosWho4/DBObject/Generated/PersoonIM.gen.php');
			break;
		case 'PersoonKart':
			require_once('WhosWho4/DBObject/PersoonKart.cls.php');
			break;
		case 'PersoonKartQuery':
			require_once('WhosWho4/Query/PersoonKart.query.php');
			break;
		case 'PersoonKartQuery_Generated':
			require_once('WhosWho4/Query/Generated/PersoonKartQuery.gen.php');
			break;
		case 'PersoonKartVerzameling':
			require_once('WhosWho4/Verzameling/PersoonKartVerzameling.cls.php');
			break;
		case 'PersoonKartVerzamelingView':
			require_once('WhosWho4/View/PersoonKartVerzameling.view.php');
			break;
		case 'PersoonKartVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonKartVerzamelingView.gen.php');
			break;
		case 'PersoonKartVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PersoonKartVerzameling.gen.php');
			break;
		case 'PersoonKartView':
			require_once('WhosWho4/View/PersoonKart.view.php');
			break;
		case 'PersoonKartView_Generated':
			require_once('WhosWho4/View/Generated/PersoonKartView.gen.php');
			break;
		case 'PersoonKart_Generated':
			require_once('WhosWho4/DBObject/Generated/PersoonKart.gen.php');
			break;
		case 'PersoonQuery':
			require_once('WhosWho4/Query/Persoon.query.php');
			break;
		case 'PersoonQuery_Generated':
			require_once('WhosWho4/Query/Generated/PersoonQuery.gen.php');
			break;
		case 'PersoonRating':
			require_once('WhosWho4/DBObject/PersoonRating.cls.php');
			break;
		case 'PersoonRatingQuery':
			require_once('WhosWho4/Query/PersoonRating.query.php');
			break;
		case 'PersoonRatingQuery_Generated':
			require_once('WhosWho4/Query/Generated/PersoonRatingQuery.gen.php');
			break;
		case 'PersoonRatingVerzameling':
			require_once('WhosWho4/Verzameling/PersoonRatingVerzameling.cls.php');
			break;
		case 'PersoonRatingVerzamelingView':
			require_once('WhosWho4/View/PersoonRatingVerzameling.view.php');
			break;
		case 'PersoonRatingVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonRatingVerzamelingView.gen.php');
			break;
		case 'PersoonRatingVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PersoonRatingVerzameling.gen.php');
			break;
		case 'PersoonRatingView':
			require_once('WhosWho4/View/PersoonRating.view.php');
			break;
		case 'PersoonRatingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonRatingView.gen.php');
			break;
		case 'PersoonRating_Generated':
			require_once('WhosWho4/DBObject/Generated/PersoonRating.gen.php');
			break;
		case 'PersoonVerzameling':
			require_once('WhosWho4/Verzameling/PersoonVerzameling.cls.php');
			break;
		case 'PersoonVerzamelingView':
			require_once('WhosWho4/View/PersoonVerzameling.view.php');
			break;
		case 'PersoonVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonVerzamelingView.gen.php');
			break;
		case 'PersoonVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PersoonVerzameling.gen.php');
			break;
		case 'PersoonView':
			require_once('WhosWho4/View/Persoon.view.php');
			break;
		case 'PersoonView_Generated':
			require_once('WhosWho4/View/Generated/PersoonView.gen.php');
			break;
		case 'PersoonVoorkeur':
			require_once('WhosWho4/DBObject/PersoonVoorkeur.cls.php');
			break;
		case 'PersoonVoorkeurQuery':
			require_once('WhosWho4/Query/PersoonVoorkeur.query.php');
			break;
		case 'PersoonVoorkeurQuery_Generated':
			require_once('WhosWho4/Query/Generated/PersoonVoorkeurQuery.gen.php');
			break;
		case 'PersoonVoorkeurVerzameling':
			require_once('WhosWho4/Verzameling/PersoonVoorkeurVerzameling.cls.php');
			break;
		case 'PersoonVoorkeurVerzamelingView':
			require_once('WhosWho4/View/PersoonVoorkeurVerzameling.view.php');
			break;
		case 'PersoonVoorkeurVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PersoonVoorkeurVerzamelingView.gen.php');
			break;
		case 'PersoonVoorkeurVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PersoonVoorkeurVerzameling.gen.php');
			break;
		case 'PersoonVoorkeurView':
			require_once('WhosWho4/View/PersoonVoorkeur.view.php');
			break;
		case 'PersoonVoorkeurView_Generated':
			require_once('WhosWho4/View/Generated/PersoonVoorkeurView.gen.php');
			break;
		case 'PersoonVoorkeur_Generated':
			require_once('WhosWho4/DBObject/Generated/PersoonVoorkeur.gen.php');
			break;
		case 'Persoon_Generated':
			require_once('WhosWho4/DBObject/Generated/Persoon.gen.php');
			break;
		case 'Planner':
			require_once('WhosWho4/DBObject/Planner.cls.php');
			break;
		case 'PlannerData':
			require_once('WhosWho4/DBObject/PlannerData.cls.php');
			break;
		case 'PlannerDataQuery':
			require_once('WhosWho4/Query/PlannerData.query.php');
			break;
		case 'PlannerDataQuery_Generated':
			require_once('WhosWho4/Query/Generated/PlannerDataQuery.gen.php');
			break;
		case 'PlannerDataVerzameling':
			require_once('WhosWho4/Verzameling/PlannerDataVerzameling.cls.php');
			break;
		case 'PlannerDataVerzamelingView':
			require_once('WhosWho4/View/PlannerDataVerzameling.view.php');
			break;
		case 'PlannerDataVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PlannerDataVerzamelingView.gen.php');
			break;
		case 'PlannerDataVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PlannerDataVerzameling.gen.php');
			break;
		case 'PlannerDataView':
			require_once('WhosWho4/View/PlannerData.view.php');
			break;
		case 'PlannerDataView_Generated':
			require_once('WhosWho4/View/Generated/PlannerDataView.gen.php');
			break;
		case 'PlannerData_Generated':
			require_once('WhosWho4/DBObject/Generated/PlannerData.gen.php');
			break;
		case 'PlannerDeelnemer':
			require_once('WhosWho4/DBObject/PlannerDeelnemer.cls.php');
			break;
		case 'PlannerDeelnemerQuery':
			require_once('WhosWho4/Query/PlannerDeelnemer.query.php');
			break;
		case 'PlannerDeelnemerQuery_Generated':
			require_once('WhosWho4/Query/Generated/PlannerDeelnemerQuery.gen.php');
			break;
		case 'PlannerDeelnemerVerzameling':
			require_once('WhosWho4/Verzameling/PlannerDeelnemerVerzameling.cls.php');
			break;
		case 'PlannerDeelnemerVerzamelingView':
			require_once('WhosWho4/View/PlannerDeelnemerVerzameling.view.php');
			break;
		case 'PlannerDeelnemerVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PlannerDeelnemerVerzamelingView.gen.php');
			break;
		case 'PlannerDeelnemerVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PlannerDeelnemerVerzameling.gen.php');
			break;
		case 'PlannerDeelnemerView':
			require_once('WhosWho4/View/PlannerDeelnemer.view.php');
			break;
		case 'PlannerDeelnemerView_Generated':
			require_once('WhosWho4/View/Generated/PlannerDeelnemerView.gen.php');
			break;
		case 'PlannerDeelnemer_Generated':
			require_once('WhosWho4/DBObject/Generated/PlannerDeelnemer.gen.php');
			break;
		case 'PlannerQuery':
			require_once('WhosWho4/Query/Planner.query.php');
			break;
		case 'PlannerQuery_Generated':
			require_once('WhosWho4/Query/Generated/PlannerQuery.gen.php');
			break;
		case 'PlannerVerzameling':
			require_once('WhosWho4/Verzameling/PlannerVerzameling.cls.php');
			break;
		case 'PlannerVerzamelingView':
			require_once('WhosWho4/View/PlannerVerzameling.view.php');
			break;
		case 'PlannerVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/PlannerVerzamelingView.gen.php');
			break;
		case 'PlannerVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/PlannerVerzameling.gen.php');
			break;
		case 'PlannerView':
			require_once('WhosWho4/View/Planner.view.php');
			break;
		case 'PlannerView_Generated':
			require_once('WhosWho4/View/Generated/PlannerView.gen.php');
			break;
		case 'Planner_Generated':
			require_once('WhosWho4/DBObject/Generated/Planner.gen.php');
			break;
		case 'RatingObject':
			require_once('WhosWho4/DBObject/RatingObject.cls.php');
			break;
		case 'RatingObjectQuery':
			require_once('WhosWho4/Query/RatingObject.query.php');
			break;
		case 'RatingObjectQuery_Generated':
			require_once('WhosWho4/Query/Generated/RatingObjectQuery.gen.php');
			break;
		case 'RatingObjectVerzameling':
			require_once('WhosWho4/Verzameling/RatingObjectVerzameling.cls.php');
			break;
		case 'RatingObjectVerzamelingView':
			require_once('WhosWho4/View/RatingObjectVerzameling.view.php');
			break;
		case 'RatingObjectVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/RatingObjectVerzamelingView.gen.php');
			break;
		case 'RatingObjectVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/RatingObjectVerzameling.gen.php');
			break;
		case 'RatingObjectView':
			require_once('WhosWho4/View/RatingObject.view.php');
			break;
		case 'RatingObjectView_Generated':
			require_once('WhosWho4/View/Generated/RatingObjectView.gen.php');
			break;
		case 'RatingObject_Generated':
			require_once('WhosWho4/DBObject/Generated/RatingObject.gen.php');
			break;
		case 'Register':
			require_once('WhosWho4/DBObject/Register.cls.php');
			break;
		case 'RegisterQuery':
			require_once('WhosWho4/Query/Register.query.php');
			break;
		case 'RegisterQuery_Generated':
			require_once('WhosWho4/Query/Generated/RegisterQuery.gen.php');
			break;
		case 'RegisterVerzameling':
			require_once('WhosWho4/Verzameling/RegisterVerzameling.cls.php');
			break;
		case 'RegisterVerzamelingView':
			require_once('WhosWho4/View/RegisterVerzameling.view.php');
			break;
		case 'RegisterVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/RegisterVerzamelingView.gen.php');
			break;
		case 'RegisterVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/RegisterVerzameling.gen.php');
			break;
		case 'RegisterView':
			require_once('WhosWho4/View/Register.view.php');
			break;
		case 'RegisterView_Generated':
			require_once('WhosWho4/View/Generated/RegisterView.gen.php');
			break;
		case 'Register_Generated':
			require_once('WhosWho4/DBObject/Generated/Register.gen.php');
			break;
		case 'Studie':
			require_once('WhosWho4/DBObject/Studie.cls.php');
			break;
		case 'StudieQuery':
			require_once('WhosWho4/Query/Studie.query.php');
			break;
		case 'StudieQuery_Generated':
			require_once('WhosWho4/Query/Generated/StudieQuery.gen.php');
			break;
		case 'StudieVerzameling':
			require_once('WhosWho4/Verzameling/StudieVerzameling.cls.php');
			break;
		case 'StudieVerzamelingView':
			require_once('WhosWho4/View/StudieVerzameling.view.php');
			break;
		case 'StudieVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/StudieVerzamelingView.gen.php');
			break;
		case 'StudieVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/StudieVerzameling.gen.php');
			break;
		case 'StudieView':
			require_once('WhosWho4/View/Studie.view.php');
			break;
		case 'StudieView_Generated':
			require_once('WhosWho4/View/Generated/StudieView.gen.php');
			break;
		case 'Studie_Generated':
			require_once('WhosWho4/DBObject/Generated/Studie.gen.php');
			break;
		case 'Template':
			require_once('WhosWho4/DBObject/Template.cls.php');
			break;
		case 'TemplateQuery':
			require_once('WhosWho4/Query/Template.query.php');
			break;
		case 'TemplateQuery_Generated':
			require_once('WhosWho4/Query/Generated/TemplateQuery.gen.php');
			break;
		case 'TemplateVerzameling':
			require_once('WhosWho4/Verzameling/TemplateVerzameling.cls.php');
			break;
		case 'TemplateVerzamelingView':
			require_once('WhosWho4/View/TemplateVerzameling.view.php');
			break;
		case 'TemplateVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/TemplateVerzamelingView.gen.php');
			break;
		case 'TemplateVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/TemplateVerzameling.gen.php');
			break;
		case 'TemplateView':
			require_once('WhosWho4/View/Template.view.php');
			break;
		case 'TemplateView_Generated':
			require_once('WhosWho4/View/Generated/TemplateView.gen.php');
			break;
		case 'Template_Generated':
			require_once('WhosWho4/DBObject/Generated/Template.gen.php');
			break;
		case 'Tentamen':
			require_once('WhosWho4/DBObject/Tentamen.cls.php');
			break;
		case 'TentamenQuery':
			require_once('WhosWho4/Query/Tentamen.query.php');
			break;
		case 'TentamenQuery_Generated':
			require_once('WhosWho4/Query/Generated/TentamenQuery.gen.php');
			break;
		case 'TentamenUitwerking':
			require_once('WhosWho4/DBObject/TentamenUitwerking.cls.php');
			break;
		case 'TentamenUitwerkingQuery':
			require_once('WhosWho4/Query/TentamenUitwerking.query.php');
			break;
		case 'TentamenUitwerkingQuery_Generated':
			require_once('WhosWho4/Query/Generated/TentamenUitwerkingQuery.gen.php');
			break;
		case 'TentamenUitwerkingVerzameling':
			require_once('WhosWho4/Verzameling/TentamenUitwerkingVerzameling.cls.php');
			break;
		case 'TentamenUitwerkingVerzamelingView':
			require_once('WhosWho4/View/TentamenUitwerkingVerzameling.view.php');
			break;
		case 'TentamenUitwerkingVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/TentamenUitwerkingVerzamelingView.gen.php');
			break;
		case 'TentamenUitwerkingVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/TentamenUitwerkingVerzameling.gen.php');
			break;
		case 'TentamenUitwerkingView':
			require_once('WhosWho4/View/TentamenUitwerking.view.php');
			break;
		case 'TentamenUitwerkingView_Generated':
			require_once('WhosWho4/View/Generated/TentamenUitwerkingView.gen.php');
			break;
		case 'TentamenUitwerking_Generated':
			require_once('WhosWho4/DBObject/Generated/TentamenUitwerking.gen.php');
			break;
		case 'TentamenVerzameling':
			require_once('WhosWho4/Verzameling/TentamenVerzameling.cls.php');
			break;
		case 'TentamenVerzamelingView':
			require_once('WhosWho4/View/TentamenVerzameling.view.php');
			break;
		case 'TentamenVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/TentamenVerzamelingView.gen.php');
			break;
		case 'TentamenVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/TentamenVerzameling.gen.php');
			break;
		case 'TentamenView':
			require_once('WhosWho4/View/Tentamen.view.php');
			break;
		case 'TentamenView_Generated':
			require_once('WhosWho4/View/Generated/TentamenView.gen.php');
			break;
		case 'Tentamen_Generated':
			require_once('WhosWho4/DBObject/Generated/Tentamen.gen.php');
			break;
		case 'TinyUrl':
			require_once('WhosWho4/DBObject/TinyUrl.cls.php');
			break;
		case 'TinyUrlHit':
			require_once('WhosWho4/DBObject/TinyUrlHit.cls.php');
			break;
		case 'TinyUrlHitQuery':
			require_once('WhosWho4/Query/TinyUrlHit.query.php');
			break;
		case 'TinyUrlHitQuery_Generated':
			require_once('WhosWho4/Query/Generated/TinyUrlHitQuery.gen.php');
			break;
		case 'TinyUrlHitVerzameling':
			require_once('WhosWho4/Verzameling/TinyUrlHitVerzameling.cls.php');
			break;
		case 'TinyUrlHitVerzamelingView':
			require_once('WhosWho4/View/TinyUrlHitVerzameling.view.php');
			break;
		case 'TinyUrlHitVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/TinyUrlHitVerzamelingView.gen.php');
			break;
		case 'TinyUrlHitVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/TinyUrlHitVerzameling.gen.php');
			break;
		case 'TinyUrlHitView':
			require_once('WhosWho4/View/TinyUrlHit.view.php');
			break;
		case 'TinyUrlHitView_Generated':
			require_once('WhosWho4/View/Generated/TinyUrlHitView.gen.php');
			break;
		case 'TinyUrlHit_Generated':
			require_once('WhosWho4/DBObject/Generated/TinyUrlHit.gen.php');
			break;
		case 'TinyUrlQuery':
			require_once('WhosWho4/Query/TinyUrl.query.php');
			break;
		case 'TinyUrlQuery_Generated':
			require_once('WhosWho4/Query/Generated/TinyUrlQuery.gen.php');
			break;
		case 'TinyUrlVerzameling':
			require_once('WhosWho4/Verzameling/TinyUrlVerzameling.cls.php');
			break;
		case 'TinyUrlVerzamelingView':
			require_once('WhosWho4/View/TinyUrlVerzameling.view.php');
			break;
		case 'TinyUrlVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/TinyUrlVerzamelingView.gen.php');
			break;
		case 'TinyUrlVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/TinyUrlVerzameling.gen.php');
			break;
		case 'TinyUrlView':
			require_once('WhosWho4/View/TinyUrl.view.php');
			break;
		case 'TinyUrlView_Generated':
			require_once('WhosWho4/View/Generated/TinyUrlView.gen.php');
			break;
		case 'TinyUrl_Generated':
			require_once('WhosWho4/DBObject/Generated/TinyUrl.gen.php');
			break;
		case 'Voornaamwoord':
			require_once('WhosWho4/DBObject/Voornaamwoord.cls.php');
			break;
		case 'VoornaamwoordQuery':
			require_once('WhosWho4/Query/Voornaamwoord.query.php');
			break;
		case 'VoornaamwoordQuery_Generated':
			require_once('WhosWho4/Query/Generated/VoornaamwoordQuery.gen.php');
			break;
		case 'VoornaamwoordVerzameling':
			require_once('WhosWho4/Verzameling/VoornaamwoordVerzameling.cls.php');
			break;
		case 'VoornaamwoordVerzamelingView':
			require_once('WhosWho4/View/VoornaamwoordVerzameling.view.php');
			break;
		case 'VoornaamwoordVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/VoornaamwoordVerzamelingView.gen.php');
			break;
		case 'VoornaamwoordVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/VoornaamwoordVerzameling.gen.php');
			break;
		case 'VoornaamwoordView':
			require_once('WhosWho4/View/Voornaamwoord.view.php');
			break;
		case 'VoornaamwoordView_Generated':
			require_once('WhosWho4/View/Generated/VoornaamwoordView.gen.php');
			break;
		case 'Voornaamwoord_Generated':
			require_once('WhosWho4/DBObject/Generated/Voornaamwoord.gen.php');
			break;
		case 'Wachtwoord':
			require_once('WhosWho4/DBObject/Wachtwoord.cls.php');
			break;
		case 'WachtwoordQuery':
			require_once('WhosWho4/Query/Wachtwoord.query.php');
			break;
		case 'WachtwoordQuery_Generated':
			require_once('WhosWho4/Query/Generated/WachtwoordQuery.gen.php');
			break;
		case 'WachtwoordVerzameling':
			require_once('WhosWho4/Verzameling/WachtwoordVerzameling.cls.php');
			break;
		case 'WachtwoordVerzamelingView':
			require_once('WhosWho4/View/WachtwoordVerzameling.view.php');
			break;
		case 'WachtwoordVerzamelingView_Generated':
			require_once('WhosWho4/View/Generated/WachtwoordVerzamelingView.gen.php');
			break;
		case 'WachtwoordVerzameling_Generated':
			require_once('WhosWho4/Verzameling/Generated/WachtwoordVerzameling.gen.php');
			break;
		case 'WachtwoordView':
			require_once('WhosWho4/View/Wachtwoord.view.php');
			break;
		case 'WachtwoordView_Generated':
			require_once('WhosWho4/View/Generated/WachtwoordView.gen.php');
			break;
		case 'Wachtwoord_Generated':
			require_once('WhosWho4/DBObject/Generated/Wachtwoord.gen.php');
			break;

	}
}
spl_autoload_register('wsw4_autoload');