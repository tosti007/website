<?php
/**
 * Deze file bevat allerhande constants t.b.v. Who's Who 4.
 **/


define('KEYTAB_FILE', '/var/www/website-ipa.keytab');

define('LDAP_SERVER', 'ldap.a-eskwadraat.nl');

/**
 * Constant die gebruikt kan worden in een query op de 'Lid'-tabel om Lid
 * objecten te construeren. Door deze constant op te nemen in de WHERE clause
 * van een query, worden enkel leden en oud-leden geretourneerd.
 *
 * Let op! Deze query kan ook oud-leden retourneren die overleden zijn! Leden
 * doe geschorst zijn, of enkel status 'boekenkoper' hebben worden niet
 * geretourneerd.
 **/
define('WSW4_WHERE_LID',
	'( 
		lidToestand != "OUDLID" AND
		lidToestand != "GESCHORST" AND 
		lidToestand != "BOEKENKOPER" AND 
		lidToestand != "BIJNALID"
	)'
);
//Geen ereleden en lidvv
define('WSW4_WHERE_NORMAALLID',
	'(' . WSW4_WHERE_LID . '
	AND ( lidToestand = "LID"
	   OR lidToestand = "BAL" )
	)'
);
define('WSW4_WHERE_OUDLID',
	'(lidToestand = "OUDLID")'
);
define('WSW4_WHERE_BIJNALID',
	'(lidToestand = "BIJNALID")'
);

define('WSW4_WHERE_LIDOFOUDLID',
	'(' . WSW4_WHERE_LID . ' OR ' . WSW4_WHERE_OUDLID . ')'
);
define('WSW4_WHERE_OPZOEKBAAR_LID',
	'( (opzoekbaar = "J" OR opzoekbaar = "A") AND ' . WSW4_WHERE_LID . ')'
);

define('WSW4_WHERE_STUDIE_ACTIEF',
	'( `datumBegin` < CURRENT_DATE()
		 AND (`datumEind` > CURRENT_DATE 
			OR (`datumEind` IS NULL AND `status` = "STUDEREND")
		 )
	 )'
);

define('WSW4_MEDEDELINGEN_PER_PAGINA', 20);

define('MAX_COMPUTERS', 12);

define('PASFOTOSDIR', '/srv/fotos/Private/');

define('FOTOBATCH', 500);

define('DIBS_TABLET_PERSOON_ID', 8516);

$studienamen =
	array(
		'Informatica,Informatiekunde'                  => _('INFO (Informatica en informatiekunde)'),
		'Natuur- en sterrenkunde,Wiskunde'             => _('TWIN (Natuur- en wiskunde)'),
		'Informatica,Wiskunde'                         => _('TWINFO (Wiskunde en informatica)'),
		'Informatica,Natuur- en sterrenkunde,Wiskunde' => _('INTWIN (Natuurkunde, wiskunde en informatica)')
	);

// Alle statussets: verzamelingen van bug-statussen met een naam
$bugStatusSets = array(
	'OPENS'     => array('OPEN','BEZIG','UITGESTELD','WANNAHAVE','FEEDBACK'),
	'NIETBEZIG' => array('OPEN','UITGESTELD'),
	'GESLOTENS' => array('GESLOTEN','BOGUS','DUBBEL'),
	'OPGELOSTS' => array('OPGELOST','GECOMMIT'),
	'ALLE'      => Bug::enumsStatus()
);

// Boekwebconstants
define('BOEKWEBBASE', '/Onderwijs/Boekweb/');
// De naam van de docentenmailingtemplate voor de Templater.
define("BW2_DOCENTMAIL_TEMPLATE", "Docentenmail");
// De naam van de xeroxmailingtemplate voor de Templater.
define("BW2_XEROXMAIL_TEMPLATE", "Xeroxmail");

// Docuweb-base
define('DOCUBASE', '/Vereniging/Docu/');

// Activiteitenposters
define('ACTPOSTERS_SRV', 'actposters/');
define('ACTPOSTERS_MAX_FILE_SIZE', 10000000); // 10 MB

define('DOCSPERPAGINA', 50);

// PapierMolen
define('PAPIERMOLENBASE', '/Onderwijs/PapierMolen/');

// Mailing-web
define('MAILINGWEB', '/Service/MailingWeb/');

// publisher
define('PUBLISHERBASE', '/Service/Intern/Publisher/');

// De default instellingen voor nieuwe Bugs
define('BUGCATEGORIE_DEFAULT_ID', 1);
define('BUG_DEFAULT_PRIORITEIT', 'LAAG');
define('BUG_DEFAULT_STATUS', 'OPEN');

// Aantallen op Bugweb-overzichtspagina
define('BUGWEB_AANTAL_GEMELD', 20);
define('BUGWEB_AANTAL_BECOMMENTARIEERD', 20);
define('BUGWEB_BUGS_BIJ_ZOEKEN', 500); // aantal bugs dat BugVerzameling::zoeken maximaal geeft, om overflows te voorkomen

// Te veel leden in een kart zorgt voor memory overflows, beperk dat aantal dus
define('KART_MAX_LEDEN', 2048);

define('API_BASE', '/apiv2/');

// Register
// De pagina waar het register te wijzigen is.
define('REGISTER_BASE', '/Service/Intern/Register/');

// DiLI
define('DILI_BASE', '/Leden/Intro/Dili/');
define('LIDMAATSCHAP_VOORRAAD', 118);

// Templates
define('TEMPLATE_BASE', '/Service/Intern/Template/');

// Tests
define('TEST_BASE', '/Service/Intern/Test/');

// WebCam
define('WEBCAM_BASE', '/Service/Intern/Webcam/');

// Git versiebeheer van de website

// Dit is het formaat voor een commit-hash naar onze huidige website:
define('GIT_COMMIT_FORMAT', 'https://git.a-eskwadraat.nl/webcie/website/commit/%s');


// Badges
$BADGES = 
	array(
		'onetime' => array(
			'iba' => array(
				'titel' => 'Beheer de Informatie',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb een IBA-commissie gedaan (TeXniCie, Sysop of WebCie)'
			),
			'bestuur' => array(
				'titel' => 'Beste Uur',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb in het bestuur gezeten'
			),
			'ec' => array(
				'titel' => 'Eej See',
				'herhaaldChecken' => true,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Heb een Eerstejaarscommissie gedaan'
			),
			'ar' => array(
				'titel' => 'Wie bestuurt de bestuurders?',
				'herhaaldChecken' => true,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Heb in de adviesraad of de KasCom gezeten'
			),
			'almanak' => array(
				'titel' => 'Boekjesmaker',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb een almanak of jaarboek gemaakt (of ben bezig er een te maken)'
			),
			'reis' => array(
				'titel' => 'Reis rond de wereld',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb een studiereis georganiseerd'
			),
			'intro' => array(
				'titel' => 'Genesis',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb een introductie georganiseerd'
			),
			'sympo' => array(
				'titel' => 'Symposium',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb een symposium georganiseerd'
			),
			'ingelogd' => array(
				'titel' => 'Hello World!',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Je hebt op de site ingelogd'
			),
			'badges' => array(
				'titel' => 'Egotripper',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Je hebt je eigen achievements bekeken'
			),
			'stamboom' => array(
				'titel' => 'Oma, wat heeft u veel kinderen',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Je hebt je eigen stamboom bekeken'
			),
			'supermentor' => array(
				'titel' => 'Superman',
				'herhaaldChecken' => true,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Je bent supermentor geweest'
			),
			'perry' => array(
				'titel' => 'Hé, waar is Perry?',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Vind Perry'
			),
			'nolife' => array(
				'titel' => 'No Life',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Zoek 100 willekeurige leden achter elkaar'
			),
			'tentamen' => array(
				'titel' => 'Werk aan je studie',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Open een tentamen'
			),
			'docuweb' => array(
				'titel' => 'Documenteer het',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Open een bestand van docuweb'
			),
			'feedback' => array(
				'titel' => 'Bedankt voor uw feedback!',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Verstuur een keer feedback'
			),
			'jaloers' => array(
				'titel' => 'Jaloers',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Bekijk de lidpagina van iemand die jarig is'
			),
			'creeper' => array(
				'titel' => 'Creeper',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Bekijk de webcam minstens een half uur achter elkaar'
			),
			'webcie' => array(
				'titel' => 'Connecties',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => 'god',
				'omschrijving' => 'Word vriendjes met de WebCie'
			),
			'nijntje' => array(
				'titel' => 'Nijntje',
				'herhaaldChecken' => true,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Koop iets bij de boekverkoop voor 2018'
			),
			'kart' => array(
				'titel' => 'Shopping',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Sla een kart op'
			),
			'historie' => array(
				'titel' => 'Vroeger was alles beter',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Lees de volledige A–Eskwadraathistorie'
			),
			'hacker' => array(
				'titel' => '1337 #4XX0R',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Hack deze achievement'
			),
			'ibablog' => array(
				'titel' => 'Blogger',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Lees de IBA-blog'
			),
			'planner' => array(
				'titel' => 'Datumprikker',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Maak een planner aan'
			),
			'activiteit' => array(
				'titel' => 'Be active!',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Maak een activiteit aan'
			),
			'virus' => array(
				'titel' => 'Virus',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Bekijk een lid dat deze achievement heeft'
			),
			'even' => array(
				'titel' => 'Even',
				'herhaaldChecken' => true,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Je hebt een even lidnummer'
			),
			'oneven' => array(
				'titel' => 'Oneven',
				'herhaaldChecken' => true,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Je hebt een oneven lidnummer'
			),
			'almanakstukje'  => array(
				'titel' => 'WebCie-almanakstukje',
				'herhaaldChecken' => false,
				'schaduw' => true,
				'beloonbaar' => false,
				'omschrijving' => 'Lees het WebCie-almanakstukje'
			),
			'iba-activiteit'  => array(
				'titel' => 'Gaan met IBAnaan!',
				'herhaaldChecken' => false,
				'schaduw' => true,
				'beloonbaar' => 'god',
				'omschrijving' => 'Je hebt een achievement gewonnen op de borrel'
			),
			'dilbert' => array(
				'titel' => 'Dilbert',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Doe wat Dilbert zegt'
			),
			'eureka' => array(
				'titel' => 'Eureka!',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Kom in de experimensen-groep'
			),
			'donateur' => array(
				'titel' => 'Crowdfunding',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Ben donateur (geweest)'
			),
			'vakidioot' => array(
				'titel' => 'Propellorhoedje',
				'herhaaldChecken' => false,
				'schaduw' => false,
				'beloonbaar' => 'vakidioot',
				'omschrijving' => 'Krijg iets gepubliceerd in de Vakidioot'
			),
			'medezeggenschap' => array(
				'titel' => 'Doe je zegje',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Heb medezeggenschap gedaan'
			),
			'bier' => array(
				'titel' => 'Ha, bier!',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Ben actief geweest in de omgeving van bier'
			),
			'eten' => array(
				'titel' => 'Aan tafel!',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Ben actief met eten omgegaan'
			),
			'kamerdienst' => [
				'titel' => 'Koffiekoning',
				'herhaaldChecken' => true,
				'schaduw' => false,
				'beloonbaar' => false,
				'omschrijving' => 'Word kamerdienstvrijwilliger'
			],
		),
		// Leuk weetje, bsgp staat voor BronzeSilverGoldPlatinum
		'bsgp' => array(
			'bug_melder' => array(
				'multi' => 'long',
				'omschrijving' => _('Bugs melden'),
				'value' => 10
			),
			'commissie_lid' => array(
				'multi' => 'long',
				'omschrijving' => _('Aantal commissies'),
				'value' => 1
			),
			'groep_lid' => array(
				'multi' => 'long',
				'omschrijving' => _('Aantal groepen'),
				'value' => 1
			),
			'dispuut_lid' => array(
				'multi' => 'short',
				'omschrijving' => _('Aantal disputen'),
				'value' => 1
			),
			'dibstransactie' => array(
				'multi' => 'long',
				'omschrijving' => _('Aantal DiBS-transacties'),
				'value' => 25
			),
			'tag_tagger' => array(
				'multi' => 'long',
				'omschrijving' => _('Tags op foto\'s geplaatst'),
				'value' => 500
			),
			'tag_getagd' => array(
				'multi' => 'long',
				'omschrijving' => _('Op foto\'s getagd'),
				'value' => 100
			),
			'rating_persoon' => array(
				'multi' => 'long',
				'omschrijving' => _('Foto\'s raten'),
				'value' => 50
			),
			'mentor_persoon' => array(
				'multi' => 'short',
				'omschrijving' => _('Mentor geweest'),
				'value' => 1
			),
			'fotograaf_persoon' => array(
				'multi' => 'long',
				'omschrijving' => _('Foto\'s gemaakt'),
				'value' => 100
			),
			'IOUBon_persoon' => array(
				'multi' => 'long',
				'omschrijving' => _('IOU-bonnen en IOU-betalingen ingevoerd'),
				'value' => 20
			),
			'iDealTransactie' => array(
				'multi' => 'long',
				'omschrijving' => _('Aankopen met iDeal gedaan'),
				'value' => 1
			),
			'activiteiten' => array(
				'multi' => 'long',
				'omschrijving' => _('Activiteiten bezocht'),
				'value' => 25
			),
			'uitwerking' => array(
				'multi' => 'long',
				'omschrijving' => _('Uitwerkingen geüpload'),
				'value' => 2
			),
			'generatie' => array(
				'multi' => 'short',
				'omschrijving' => _('Generaties voortgebracht'),
				'value' => 2
			)
		)
	);
