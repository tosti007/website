<?
abstract class CollectieVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de CollectieVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze CollectieVerzameling een CollectieVerzameling.
	 *
	 * @return CollectieVerzameling
	 * Een CollectieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze CollectieVerzameling.
	 */
	public function toParentCollectieVerzameling()
	{
		if($this->aantal() == 0)
			return new CollectieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getParentCollectieCollectieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getParentCollectieCollectieID()
			                      );
		}
		$this->positie = $origPositie;
		return CollectieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een CollectieVerzameling van ParentCollectie.
	 *
	 * @return CollectieVerzameling
	 * Een CollectieVerzameling die elementen bevat die bij de ParentCollectie hoort.
	 */
	static public function fromParentCollectie($parentCollectie)
	{
		if(!isset($parentCollectie))
			return new CollectieVerzameling();

		return CollectieQuery::table()
			->whereProp('ParentCollectie', $parentCollectie)
			->verzamel();
	}
}
