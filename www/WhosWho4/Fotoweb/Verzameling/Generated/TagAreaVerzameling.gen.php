<?
abstract class TagAreaVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TagAreaVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TagAreaVerzameling een TagVerzameling.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze TagAreaVerzameling.
	 */
	public function toTagVerzameling()
	{
		if($this->aantal() == 0)
			return new TagVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTagTagID()
			                      );
		}
		$this->positie = $origPositie;
		return TagVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TagAreaVerzameling van Tag.
	 *
	 * @return TagAreaVerzameling
	 * Een TagAreaVerzameling die elementen bevat die bij de Tag hoort.
	 */
	static public function fromTag($tag)
	{
		if(!isset($tag))
			return new TagAreaVerzameling();

		return TagAreaQuery::table()
			->whereProp('Tag', $tag)
			->verzamel();
	}
}
