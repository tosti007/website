<?
abstract class TagVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de TagVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze TagVerzameling een MediaVerzameling.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze TagVerzameling.
	 */
	public function toMediaVerzameling()
	{
		if($this->aantal() == 0)
			return new MediaVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMediaMediaID()
			                      );
		}
		$this->positie = $origPositie;
		return MediaVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TagVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TagVerzameling.
	 */
	public function toPersoonVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getPersoonContactID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getPersoonContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TagVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TagVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCommissieCommissieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TagVerzameling een IntroGroepVerzameling.
	 *
	 * @return IntroGroepVerzameling
	 * Een IntroGroepVerzameling die elementen bevat die via foreign keys
	 * corresponderen aan de elementen in deze TagVerzameling.
	 */
	public function toIntrogroepVerzameling()
	{
		if($this->aantal() == 0)
			return new IntroGroepVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getIntrogroepGroepID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getIntrogroepGroepID()
			                      );
		}
		$this->positie = $origPositie;
		return IntroGroepVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TagVerzameling een CollectieVerzameling.
	 *
	 * @return CollectieVerzameling
	 * Een CollectieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TagVerzameling.
	 */
	public function toCollectieVerzameling()
	{
		if($this->aantal() == 0)
			return new CollectieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			if(is_null($obj->getCollectieCollectieID())) {
				continue;
			}

			$foreignkeys[] = array($obj->getCollectieCollectieID()
			                      );
		}
		$this->positie = $origPositie;
		return CollectieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze TagVerzameling een PersoonVerzameling.
	 *
	 * @return PersoonVerzameling
	 * Een PersoonVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze TagVerzameling.
	 */
	public function toTaggerVerzameling()
	{
		if($this->aantal() == 0)
			return new PersoonVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getTaggerContactID()
			                      );
		}
		$this->positie = $origPositie;
		return PersoonVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een TagVerzameling van Media.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die bij de Media hoort.
	 */
	static public function fromMedia($media)
	{
		if(!isset($media))
			return new TagVerzameling();

		return TagQuery::table()
			->whereProp('Media', $media)
			->verzamel();
	}
	/**
	 * @brief Maak een TagVerzameling van Persoon.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die bij de Persoon hoort.
	 */
	static public function fromPersoon($persoon)
	{
		if(!isset($persoon))
			return new TagVerzameling();

		return TagQuery::table()
			->whereProp('Persoon', $persoon)
			->verzamel();
	}
	/**
	 * @brief Maak een TagVerzameling van Commissie.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new TagVerzameling();

		return TagQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
	/**
	 * @brief Maak een TagVerzameling van Introgroep.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die bij de Introgroep hoort.
	 */
	static public function fromIntrogroep($introgroep)
	{
		if(!isset($introgroep))
			return new TagVerzameling();

		return TagQuery::table()
			->whereProp('Introgroep', $introgroep)
			->verzamel();
	}
	/**
	 * @brief Maak een TagVerzameling van Collectie.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die bij de Collectie hoort.
	 */
	static public function fromCollectie($collectie)
	{
		if(!isset($collectie))
			return new TagVerzameling();

		return TagQuery::table()
			->whereProp('Collectie', $collectie)
			->verzamel();
	}
	/**
	 * @brief Maak een TagVerzameling van Tagger.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling die elementen bevat die bij de Tagger hoort.
	 */
	static public function fromTagger($tagger)
	{
		if(!isset($tagger))
			return new TagVerzameling();

		return TagQuery::table()
			->whereProp('Tagger', $tagger)
			->verzamel();
	}
}
