<?
abstract class CommissieFotoVerzameling_Generated
	extends Verzameling
{
	/**
	 * @brief De constructor van de CommissieFotoVerzameling_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Verzameling
	}
	/**
	 * @brief Maak van deze CommissieFotoVerzameling een CommissieVerzameling.
	 *
	 * @return CommissieVerzameling
	 * Een CommissieVerzameling die elementen bevat die via foreign keys corresponderen
	 * aan de elementen in deze CommissieFotoVerzameling.
	 */
	public function toCommissieVerzameling()
	{
		if($this->aantal() == 0)
			return new CommissieVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getCommissieCommissieID()
			                      );
		}
		$this->positie = $origPositie;
		return CommissieVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak van deze CommissieFotoVerzameling een MediaVerzameling.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling die elementen bevat die via foreign keys corresponderen aan
	 * de elementen in deze CommissieFotoVerzameling.
	 */
	public function toMediaVerzameling()
	{
		if($this->aantal() == 0)
			return new MediaVerzameling();

		$origPositie = $this->positie;
		$foreignkeys = array();
		foreach($this as $obj)
		{
			$foreignkeys[] = array($obj->getMediaMediaID()
			                      );
		}
		$this->positie = $origPositie;
		return MediaVerzameling::verzamel($foreignkeys);
	}
	/**
	 * @brief Maak een CommissieFotoVerzameling van Commissie.
	 *
	 * @return CommissieFotoVerzameling
	 * Een CommissieFotoVerzameling die elementen bevat die bij de Commissie hoort.
	 */
	static public function fromCommissie($commissie)
	{
		if(!isset($commissie))
			return new CommissieFotoVerzameling();

		return CommissieFotoQuery::table()
			->whereProp('Commissie', $commissie)
			->verzamel();
	}
	/**
	 * @brief Maak een CommissieFotoVerzameling van Media.
	 *
	 * @return CommissieFotoVerzameling
	 * Een CommissieFotoVerzameling die elementen bevat die bij de Media hoort.
	 */
	static public function fromMedia($media)
	{
		if(!isset($media))
			return new CommissieFotoVerzameling();

		return CommissieFotoQuery::table()
			->whereProp('Media', $media)
			->verzamel();
	}
}
