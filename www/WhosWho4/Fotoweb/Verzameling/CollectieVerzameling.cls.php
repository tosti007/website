<?
/**
 * $Id$
 */
class CollectieVerzameling
	extends CollectieVerzameling_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // CollectieVerzameling_Generated
	}

	/**
	 *  Haal alle collecties uit de db
	 */
	public static function getAllCollecties() {
		global $WSW4DB;

		$ids = $WSW4DB->q("COLUMN SELECT `collectieID` "
						. "FROM `Collectie` "
						. "ORDER BY `naam`");

		return self::verzamel($ids);
	}

	/**
	 *  Geeft een array terug van ids=>naam, handig voor een selectbox
	 */
	static public function maakCollectieArray() {
		global $WSW4DB;

		$ids = $WSW4DB->q("TABLE SELECT `collectieID`,`naam` FROM `Collectie` WHERE `actief` = 1");

		$arr = array();
		foreach($ids as $i) {
			$arr[$i["collectieID"]] = htmlspecialchars($i["naam"]);
		}
		return $arr;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
