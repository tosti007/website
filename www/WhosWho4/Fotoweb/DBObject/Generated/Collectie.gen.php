<?
abstract class Collectie_Generated
	extends Entiteit
{
	protected $collectieID;				/**< \brief PRIMARY */
	protected $parentCollectie;			/**< \brief NULL */
	protected $parentCollectie_collectieID;/**< \brief PRIMARY */
	protected $naam;
	protected $omschrijving;			/**< \brief NULL */
	protected $actief;
	/** Verzamelingen **/
	protected $tagVerzameling;
	protected $collectieVerzameling;
	/**
	 * @brief De constructor van de Collectie_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->collectieID = NULL;
		$this->parentCollectie = NULL;
		$this->parentCollectie_collectieID = NULL;
		$this->naam = '';
		$this->omschrijving = NULL;
		$this->actief = True;
		$this->tagVerzameling = NULL;
		$this->collectieVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld collectieID.
	 *
	 * @return int
	 * De waarde van het veld collectieID.
	 */
	public function getCollectieID()
	{
		return $this->collectieID;
	}
	/**
	 * @brief Geef de waarde van het veld parentCollectie.
	 *
	 * @return Collectie
	 * De waarde van het veld parentCollectie.
	 */
	public function getParentCollectie()
	{
		if(!isset($this->parentCollectie)
		 && isset($this->parentCollectie_collectieID)
		 ) {
			$this->parentCollectie = Collectie::geef
					( $this->parentCollectie_collectieID
					);
		}
		return $this->parentCollectie;
	}
	/**
	 * @brief Stel de waarde van het veld parentCollectie in.
	 *
	 * @param mixed $new_collectieID De nieuwe waarde.
	 *
	 * @return Collectie
	 * Dit Collectie-object.
	 */
	public function setParentCollectie($new_collectieID)
	{
		unset($this->errors['ParentCollectie']);
		if($new_collectieID instanceof Collectie
		) {
			if($this->parentCollectie == $new_collectieID
			&& $this->parentCollectie_collectieID == $this->parentCollectie->getCollectieID())
				return $this;
			$this->parentCollectie = $new_collectieID;
			$this->parentCollectie_collectieID
					= $this->parentCollectie->getCollectieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_collectieID) || $new_collectieID == 0)) {
			if($this->parentCollectie == NULL && $this->parentCollectie_collectieID == NULL)
				return $this;
			$this->parentCollectie = NULL;
			$this->parentCollectie_collectieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_collectieID)
		) {
			if($this->parentCollectie == NULL 
				&& $this->parentCollectie_collectieID == (int)$new_collectieID)
				return $this;
			$this->parentCollectie = NULL;
			$this->parentCollectie_collectieID
					= (int)$new_collectieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld parentCollectie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld parentCollectie geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkParentCollectie()
	{
		if (array_key_exists('ParentCollectie', $this->errors))
			return $this->errors['ParentCollectie'];
		$waarde1 = $this->getParentCollectie();
		$waarde2 = $this->getParentCollectieCollectieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld parentCollectie_collectieID.
	 *
	 * @return int
	 * De waarde van het veld parentCollectie_collectieID.
	 */
	public function getParentCollectieCollectieID()
	{
		if (is_null($this->parentCollectie_collectieID) && isset($this->parentCollectie)) {
			$this->parentCollectie_collectieID = $this->parentCollectie->getCollectieID();
		}
		return $this->parentCollectie_collectieID;
	}
	/**
	 * @brief Geef de waarde van het veld naam.
	 *
	 * @return string
	 * De waarde van het veld naam.
	 */
	public function getNaam()
	{
		return $this->naam;
	}
	/**
	 * @brief Stel de waarde van het veld naam in.
	 *
	 * @param mixed $newNaam De nieuwe waarde.
	 *
	 * @return Collectie
	 * Dit Collectie-object.
	 */
	public function setNaam($newNaam)
	{
		unset($this->errors['Naam']);
		if(!is_null($newNaam))
			$newNaam = trim($newNaam);
		if($newNaam === "")
			$newNaam = NULL;
		if($this->naam === $newNaam)
			return $this;

		$this->naam = $newNaam;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld naam geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld naam geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkNaam()
	{
		if (array_key_exists('Naam', $this->errors))
			return $this->errors['Naam'];
		$waarde = $this->getNaam();
		if (empty($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Collectie
	 * Dit Collectie-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (empty($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld actief.
	 *
	 * @return bool
	 * De waarde van het veld actief.
	 */
	public function getActief()
	{
		return $this->actief;
	}
	/**
	 * @brief Stel de waarde van het veld actief in.
	 *
	 * @param mixed $newActief De nieuwe waarde.
	 *
	 * @return Collectie
	 * Dit Collectie-object.
	 */
	public function setActief($newActief)
	{
		unset($this->errors['Actief']);
		if(!is_null($newActief))
			$newActief = (bool)$newActief;
		if($this->actief === $newActief)
			return $this;

		$this->actief = $newActief;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld actief geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld actief geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkActief()
	{
		if (array_key_exists('Actief', $this->errors))
			return $this->errors['Actief'];
		$waarde = $this->getActief();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de TagVerzameling die hoort bij dit object.
	 */
	public function getTagVerzameling()
	{
		if(!$this->tagVerzameling instanceof TagVerzameling)
			$this->tagVerzameling = TagVerzameling::fromCollectie($this);
		return $this->tagVerzameling;
	}
	/**
	 * @brief Returneert de CollectieVerzameling die hoort bij dit object.
	 */
	public function getCollectieVerzameling()
	{
		if(!$this->collectieVerzameling instanceof CollectieVerzameling)
			$this->collectieVerzameling = CollectieVerzameling::fromParentCollectie($this);
		return $this->collectieVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Collectie::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Collectie.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Collectie::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getCollectieID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Collectie|false
	 * Een Collectie-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$collectieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$collectieID = (int)$a;
		}

		if(is_null($collectieID))
			throw new BadMethodCallException();

		static::cache(array( array($collectieID) ));
		return Entiteit::geefCache(array($collectieID), 'Collectie');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Collectie');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Collectie::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Collectie`.`collectieID`'
		                 .     ', `Collectie`.`parentCollectie_collectieID`'
		                 .     ', `Collectie`.`naam`'
		                 .     ', `Collectie`.`omschrijving`'
		                 .     ', `Collectie`.`actief`'
		                 .     ', `Collectie`.`gewijzigdWanneer`'
		                 .     ', `Collectie`.`gewijzigdWie`'
		                 .' FROM `Collectie`'
		                 .' WHERE (`collectieID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['collectieID']);

			$obj = new Collectie();

			$obj->inDB = True;

			$obj->collectieID  = (int) $row['collectieID'];
			$obj->parentCollectie_collectieID  = (is_null($row['parentCollectie_collectieID'])) ? null : (int) $row['parentCollectie_collectieID'];
			$obj->naam  = trim($row['naam']);
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->actief  = (bool) $row['actief'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Collectie')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getParentCollectieCollectieID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->collectieID =
			$WSW4DB->q('RETURNID INSERT INTO `Collectie`'
			          . ' (`parentCollectie_collectieID`, `naam`, `omschrijving`, `actief`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %s, %s, %i, %s, %i)'
			          , $this->parentCollectie_collectieID
			          , $this->naam
			          , $this->omschrijving
			          , $this->actief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Collectie')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Collectie`'
			          .' SET `parentCollectie_collectieID` = %i'
			          .   ', `naam` = %s'
			          .   ', `omschrijving` = %s'
			          .   ', `actief` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `collectieID` = %i'
			          , $this->parentCollectie_collectieID
			          , $this->naam
			          , $this->omschrijving
			          , $this->actief
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->collectieID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenCollectie
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenCollectie($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenCollectie($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'ParentCollectie';
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'Actief';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Naam';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'ParentCollectie';
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'Actief';
			break;
		case 'get':
			$velden[] = 'ParentCollectie';
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'Actief';
		case 'primary':
			$velden[] = 'parentCollectie_collectieID';
			break;
		case 'verzamelingen':
			$velden[] = 'TagVerzameling';
			$velden[] = 'CollectieVerzameling';
			break;
		default:
			$velden[] = 'ParentCollectie';
			$velden[] = 'Naam';
			$velden[] = 'Omschrijving';
			$velden[] = 'Actief';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Tag-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$TagFromCollectieVerz = TagVerzameling::fromCollectie($this);
		$returnValue = $TagFromCollectieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tag met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Collectie-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$CollectieFromParentCollectieVerz = CollectieVerzameling::fromParentCollectie($this);
		$returnValue = $CollectieFromParentCollectieVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Collectie met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($TagFromCollectieVerz as $v)
		{
			$v->setCollectie(NULL);
		}
		$TagFromCollectieVerz->opslaan();

		foreach($CollectieFromParentCollectieVerz as $v)
		{
			$v->setParentCollectie(NULL);
		}
		$CollectieFromParentCollectieVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Collectie`'
		          .' WHERE `collectieID` = %i'
		          .' LIMIT 1'
		          , $this->collectieID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->collectieID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Collectie`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `collectieID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->collectieID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Collectie terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'collectieid':
			return 'int';
		case 'parentcollectie':
			return 'foreign';
		case 'parentcollectie_collectieid':
			return 'int';
		case 'naam':
			return 'string';
		case 'omschrijving':
			return 'string';
		case 'actief':
			return 'bool';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'collectieID':
		case 'parentCollectie_collectieID':
		case 'actief':
			$type = '%i';
			break;
		case 'naam':
		case 'omschrijving':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Collectie`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `collectieID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->collectieID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Collectie`'
		          .' WHERE `collectieID` = %i'
		                 , $veld
		          , $this->collectieID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Collectie');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Tag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagFromCollectieVerz = TagVerzameling::fromCollectie($this);
		$dependencies['Tag'] = $TagFromCollectieVerz;

		// Verzamel alle Collectie-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CollectieFromParentCollectieVerz = CollectieVerzameling::fromParentCollectie($this);
		$dependencies['Collectie'] = $CollectieFromParentCollectieVerz;

		return $dependencies;
	}
}
