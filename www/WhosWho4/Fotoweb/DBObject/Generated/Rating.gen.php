<?
abstract class Rating_Generated
	extends Entiteit
{
	protected $media;					/**< \brief PRIMARY */
	protected $media_mediaID;			/**< \brief PRIMARY */
	protected $persoon;					/**< \brief PRIMARY */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $rating;					/**< \brief ENUM:1/2/3/4/5/6/7/8/9/10 */
	/**
	/**
	 * @brief De constructor van de Rating_Generated-klasse.
	 *
	 * @param mixed $a Media (Media OR Array(media_mediaID) OR media_mediaID)
	 * @param mixed $b Persoon (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Media)
		{
			$this->media = $a;
			$this->media_mediaID = $a->getMediaID();
		}
		else if(is_array($a))
		{
			$this->media = NULL;
			$this->media_mediaID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->media = NULL;
			$this->media_mediaID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->persoon = $b;
			$this->persoon_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->persoon = NULL;
			$this->persoon_contactID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->rating = '1';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Media';
		$volgorde[] = 'Persoon';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld media.
	 *
	 * @return Media
	 * De waarde van het veld media.
	 */
	public function getMedia()
	{
		if(!isset($this->media)
		 && isset($this->media_mediaID)
		 ) {
			$this->media = Media::geef
					( $this->media_mediaID
					);
		}
		return $this->media;
	}
	/**
	 * @brief Geef de waarde van het veld media_mediaID.
	 *
	 * @return int
	 * De waarde van het veld media_mediaID.
	 */
	public function getMediaMediaID()
	{
		if (is_null($this->media_mediaID) && isset($this->media)) {
			$this->media_mediaID = $this->media->getMediaID();
		}
		return $this->media_mediaID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld rating.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld rating.
	 */
	static public function enumsRating()
	{
		static $vals = array('1','2','3','4','5','6','7','8','9','10');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @return string
	 * De waarde van het veld rating.
	 */
	public function getRating()
	{
		return $this->rating;
	}
	/**
	 * @brief Stel de waarde van het veld rating in.
	 *
	 * @param mixed $newRating De nieuwe waarde.
	 *
	 * @return Rating
	 * Dit Rating-object.
	 */
	public function setRating($newRating)
	{
		unset($this->errors['Rating']);
		if(!is_null($newRating))
			$newRating = strtoupper(trim($newRating));
		if($newRating === "")
			$newRating = NULL;
		if($this->rating === $newRating)
			return $this;

		$this->rating = $newRating;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rating geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rating geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkRating()
	{
		if (array_key_exists('Rating', $this->errors))
			return $this->errors['Rating'];
		$waarde = $this->getRating();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsRating()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Rating::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Rating.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Rating::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getMediaMediaID()
		                      , $this->getPersoonContactID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return Rating|false
	 * Een Rating-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$media_mediaID = (int)$a[0];
			$persoon_contactID = (int)$a[1];
		}
		else if($a instanceof Media
		     && $b instanceof Persoon)
		{
			$media_mediaID = $a->getMediaID();
			$persoon_contactID = $b->getContactID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$media_mediaID = (int)$a;
			$persoon_contactID = (int)$b;
		}

		if(is_null($media_mediaID)
		|| is_null($persoon_contactID))
			throw new BadMethodCallException();

		static::cache(array( array($media_mediaID, $persoon_contactID) ));
		return Entiteit::geefCache(array($media_mediaID, $persoon_contactID), 'Rating');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Rating');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Rating::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Rating`.`media_mediaID`'
		                 .     ', `Rating`.`persoon_contactID`'
		                 .     ', `Rating`.`rating`'
		                 .     ', `Rating`.`gewijzigdWanneer`'
		                 .     ', `Rating`.`gewijzigdWie`'
		                 .' FROM `Rating`'
		                 .' WHERE (`media_mediaID`, `persoon_contactID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['media_mediaID']
			                  ,$row['persoon_contactID']);

			$obj = new Rating(array($row['media_mediaID']), array($row['persoon_contactID']));

			$obj->inDB = True;

			$obj->rating  = strtoupper(trim($row['rating']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Rating')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getMedia();
		if(!$rel->getInDB())
			throw new LogicException('foreign Media is not in DB');
		$this->media_mediaID = $rel->getMediaID();

		$rel = $this->getPersoon();
		if(!$rel->getInDB())
			throw new LogicException('foreign Persoon is not in DB');
		$this->persoon_contactID = $rel->getContactID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `Rating`'
			          . ' (`media_mediaID`, `persoon_contactID`, `rating`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i)'
			          , $this->media_mediaID
			          , $this->persoon_contactID
			          , $this->rating
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Rating')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Rating`'
			          .' SET `rating` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `media_mediaID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->rating
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->media_mediaID
			          , $this->persoon_contactID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenRating
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenRating($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenRating($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Rating';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Rating';
			break;
		case 'get':
			$velden[] = 'Rating';
		case 'primary':
			$velden[] = 'media_mediaID';
			$velden[] = 'persoon_contactID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Rating';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Rating`'
		          .' WHERE `media_mediaID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          .' LIMIT 1'
		          , $this->media_mediaID
		          , $this->persoon_contactID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->media = NULL;
		$this->media_mediaID = NULL;
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Rating`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `media_mediaID` = %i'
			          .  ' AND `persoon_contactID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->media_mediaID
			          , $this->persoon_contactID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Rating terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'media':
			return 'foreign';
		case 'media_mediaid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'rating':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'media_mediaID':
		case 'persoon_contactID':
			$type = '%i';
			break;
		case 'rating':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Rating`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `media_mediaID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->media_mediaID
		          , $this->persoon_contactID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Rating`'
		          .' WHERE `media_mediaID` = %i'
		          .  ' AND `persoon_contactID` = %i'
		                 , $veld
		          , $this->media_mediaID
		          , $this->persoon_contactID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Rating');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
