<?
abstract class Tag_Generated
	extends Entiteit
{
	protected $tagID;					/**< \brief PRIMARY */
	protected $media;
	protected $media_mediaID;			/**< \brief PRIMARY */
	protected $persoon;					/**< \brief NULL */
	protected $persoon_contactID;		/**< \brief PRIMARY */
	protected $commissie;				/**< \brief NULL */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $introgroep;				/**< \brief NULL */
	protected $introgroep_groepID;		/**< \brief PRIMARY */
	protected $collectie;				/**< \brief NULL */
	protected $collectie_collectieID;	/**< \brief PRIMARY */
	protected $tagger;
	protected $tagger_contactID;		/**< \brief PRIMARY */
	/** Verzamelingen **/
	protected $tagAreaVerzameling;
	/**
	 * @brief De constructor van de Tag_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Entiteit

		$this->tagID = NULL;
		$this->media = NULL;
		$this->media_mediaID = 0;
		$this->persoon = NULL;
		$this->persoon_contactID = NULL;
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->introgroep = NULL;
		$this->introgroep_groepID = NULL;
		$this->collectie = NULL;
		$this->collectie_collectieID = NULL;
		$this->tagger = NULL;
		$this->tagger_contactID = 0;
		$this->tagAreaVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		return false;
	}
	/**
	 * @brief Geef de waarde van het veld tagID.
	 *
	 * @return int
	 * De waarde van het veld tagID.
	 */
	public function getTagID()
	{
		return $this->tagID;
	}
	/**
	 * @brief Geef de waarde van het veld media.
	 *
	 * @return Media
	 * De waarde van het veld media.
	 */
	public function getMedia()
	{
		if(!isset($this->media)
		 && isset($this->media_mediaID)
		 ) {
			$this->media = Media::geef
					( $this->media_mediaID
					);
		}
		return $this->media;
	}
	/**
	 * @brief Stel de waarde van het veld media in.
	 *
	 * @param mixed $new_mediaID De nieuwe waarde.
	 *
	 * @return Tag
	 * Dit Tag-object.
	 */
	public function setMedia($new_mediaID)
	{
		unset($this->errors['Media']);
		if($new_mediaID instanceof Media
		) {
			if($this->media == $new_mediaID
			&& $this->media_mediaID == $this->media->getMediaID())
				return $this;
			$this->media = $new_mediaID;
			$this->media_mediaID
					= $this->media->getMediaID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_mediaID)
		) {
			if($this->media == NULL 
				&& $this->media_mediaID == (int)$new_mediaID)
				return $this;
			$this->media = NULL;
			$this->media_mediaID
					= (int)$new_mediaID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld media geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld media geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkMedia()
	{
		if (array_key_exists('Media', $this->errors))
			return $this->errors['Media'];
		$waarde1 = $this->getMedia();
		$waarde2 = $this->getMediaMediaID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld media_mediaID.
	 *
	 * @return int
	 * De waarde van het veld media_mediaID.
	 */
	public function getMediaMediaID()
	{
		if (is_null($this->media_mediaID) && isset($this->media)) {
			$this->media_mediaID = $this->media->getMediaID();
		}
		return $this->media_mediaID;
	}
	/**
	 * @brief Geef de waarde van het veld persoon.
	 *
	 * @return Persoon
	 * De waarde van het veld persoon.
	 */
	public function getPersoon()
	{
		if(!isset($this->persoon)
		 && isset($this->persoon_contactID)
		 ) {
			$this->persoon = Persoon::geef
					( $this->persoon_contactID
					);
		}
		return $this->persoon;
	}
	/**
	 * @brief Stel de waarde van het veld persoon in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Tag
	 * Dit Tag-object.
	 */
	public function setPersoon($new_contactID)
	{
		unset($this->errors['Persoon']);
		if($new_contactID instanceof Persoon
		) {
			if($this->persoon == $new_contactID
			&& $this->persoon_contactID == $this->persoon->getContactID())
				return $this;
			$this->persoon = $new_contactID;
			$this->persoon_contactID
					= $this->persoon->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->persoon == NULL && $this->persoon_contactID == NULL)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->persoon == NULL 
				&& $this->persoon_contactID == (int)$new_contactID)
				return $this;
			$this->persoon = NULL;
			$this->persoon_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld persoon geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld persoon geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPersoon()
	{
		if (array_key_exists('Persoon', $this->errors))
			return $this->errors['Persoon'];
		$waarde1 = $this->getPersoon();
		$waarde2 = $this->getPersoonContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld persoon_contactID.
	 *
	 * @return int
	 * De waarde van het veld persoon_contactID.
	 */
	public function getPersoonContactID()
	{
		if (is_null($this->persoon_contactID) && isset($this->persoon)) {
			$this->persoon_contactID = $this->persoon->getContactID();
		}
		return $this->persoon_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Stel de waarde van het veld commissie in.
	 *
	 * @param mixed $new_commissieID De nieuwe waarde.
	 *
	 * @return Tag
	 * Dit Tag-object.
	 */
	public function setCommissie($new_commissieID)
	{
		unset($this->errors['Commissie']);
		if($new_commissieID instanceof Commissie
		) {
			if($this->commissie == $new_commissieID
			&& $this->commissie_commissieID == $this->commissie->getCommissieID())
				return $this;
			$this->commissie = $new_commissieID;
			$this->commissie_commissieID
					= $this->commissie->getCommissieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_commissieID) || $new_commissieID == 0)) {
			if($this->commissie == NULL && $this->commissie_commissieID == NULL)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_commissieID)
		) {
			if($this->commissie == NULL 
				&& $this->commissie_commissieID == (int)$new_commissieID)
				return $this;
			$this->commissie = NULL;
			$this->commissie_commissieID
					= (int)$new_commissieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld commissie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld commissie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCommissie()
	{
		if (array_key_exists('Commissie', $this->errors))
			return $this->errors['Commissie'];
		$waarde1 = $this->getCommissie();
		$waarde2 = $this->getCommissieCommissieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld introgroep.
	 *
	 * @return IntroGroep
	 * De waarde van het veld introgroep.
	 */
	public function getIntrogroep()
	{
		if(!isset($this->introgroep)
		 && isset($this->introgroep_groepID)
		 ) {
			$this->introgroep = IntroGroep::geef
					( $this->introgroep_groepID
					);
		}
		return $this->introgroep;
	}
	/**
	 * @brief Stel de waarde van het veld introgroep in.
	 *
	 * @param mixed $new_groepID De nieuwe waarde.
	 *
	 * @return Tag
	 * Dit Tag-object.
	 */
	public function setIntrogroep($new_groepID)
	{
		unset($this->errors['Introgroep']);
		if($new_groepID instanceof IntroGroep
		) {
			if($this->introgroep == $new_groepID
			&& $this->introgroep_groepID == $this->introgroep->getGroepID())
				return $this;
			$this->introgroep = $new_groepID;
			$this->introgroep_groepID
					= $this->introgroep->getGroepID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_groepID) || $new_groepID == 0)) {
			if($this->introgroep == NULL && $this->introgroep_groepID == NULL)
				return $this;
			$this->introgroep = NULL;
			$this->introgroep_groepID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_groepID)
		) {
			if($this->introgroep == NULL 
				&& $this->introgroep_groepID == (int)$new_groepID)
				return $this;
			$this->introgroep = NULL;
			$this->introgroep_groepID
					= (int)$new_groepID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld introgroep geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld introgroep geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkIntrogroep()
	{
		if (array_key_exists('Introgroep', $this->errors))
			return $this->errors['Introgroep'];
		$waarde1 = $this->getIntrogroep();
		$waarde2 = $this->getIntrogroepGroepID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld introgroep_groepID.
	 *
	 * @return int
	 * De waarde van het veld introgroep_groepID.
	 */
	public function getIntrogroepGroepID()
	{
		if (is_null($this->introgroep_groepID) && isset($this->introgroep)) {
			$this->introgroep_groepID = $this->introgroep->getGroepID();
		}
		return $this->introgroep_groepID;
	}
	/**
	 * @brief Geef de waarde van het veld collectie.
	 *
	 * @return Collectie
	 * De waarde van het veld collectie.
	 */
	public function getCollectie()
	{
		if(!isset($this->collectie)
		 && isset($this->collectie_collectieID)
		 ) {
			$this->collectie = Collectie::geef
					( $this->collectie_collectieID
					);
		}
		return $this->collectie;
	}
	/**
	 * @brief Stel de waarde van het veld collectie in.
	 *
	 * @param mixed $new_collectieID De nieuwe waarde.
	 *
	 * @return Tag
	 * Dit Tag-object.
	 */
	public function setCollectie($new_collectieID)
	{
		unset($this->errors['Collectie']);
		if($new_collectieID instanceof Collectie
		) {
			if($this->collectie == $new_collectieID
			&& $this->collectie_collectieID == $this->collectie->getCollectieID())
				return $this;
			$this->collectie = $new_collectieID;
			$this->collectie_collectieID
					= $this->collectie->getCollectieID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_collectieID) || $new_collectieID == 0)) {
			if($this->collectie == NULL && $this->collectie_collectieID == NULL)
				return $this;
			$this->collectie = NULL;
			$this->collectie_collectieID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_collectieID)
		) {
			if($this->collectie == NULL 
				&& $this->collectie_collectieID == (int)$new_collectieID)
				return $this;
			$this->collectie = NULL;
			$this->collectie_collectieID
					= (int)$new_collectieID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld collectie geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld collectie geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkCollectie()
	{
		if (array_key_exists('Collectie', $this->errors))
			return $this->errors['Collectie'];
		$waarde1 = $this->getCollectie();
		$waarde2 = $this->getCollectieCollectieID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld collectie_collectieID.
	 *
	 * @return int
	 * De waarde van het veld collectie_collectieID.
	 */
	public function getCollectieCollectieID()
	{
		if (is_null($this->collectie_collectieID) && isset($this->collectie)) {
			$this->collectie_collectieID = $this->collectie->getCollectieID();
		}
		return $this->collectie_collectieID;
	}
	/**
	 * @brief Geef de waarde van het veld tagger.
	 *
	 * @return Persoon
	 * De waarde van het veld tagger.
	 */
	public function getTagger()
	{
		if(!isset($this->tagger)
		 && isset($this->tagger_contactID)
		 ) {
			$this->tagger = Persoon::geef
					( $this->tagger_contactID
					);
		}
		return $this->tagger;
	}
	/**
	 * @brief Stel de waarde van het veld tagger in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Tag
	 * Dit Tag-object.
	 */
	public function setTagger($new_contactID)
	{
		unset($this->errors['Tagger']);
		if($new_contactID instanceof Persoon
		) {
			if($this->tagger == $new_contactID
			&& $this->tagger_contactID == $this->tagger->getContactID())
				return $this;
			$this->tagger = $new_contactID;
			$this->tagger_contactID
					= $this->tagger->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->tagger == NULL 
				&& $this->tagger_contactID == (int)$new_contactID)
				return $this;
			$this->tagger = NULL;
			$this->tagger_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld tagger geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld tagger geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkTagger()
	{
		if (array_key_exists('Tagger', $this->errors))
			return $this->errors['Tagger'];
		$waarde1 = $this->getTagger();
		$waarde2 = $this->getTaggerContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return _('dit is een verplicht veld');

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld tagger_contactID.
	 *
	 * @return int
	 * De waarde van het veld tagger_contactID.
	 */
	public function getTaggerContactID()
	{
		if (is_null($this->tagger_contactID) && isset($this->tagger)) {
			$this->tagger_contactID = $this->tagger->getContactID();
		}
		return $this->tagger_contactID;
	}
	/**
	 * @brief Returneert de TagAreaVerzameling die hoort bij dit object.
	 */
	public function getTagAreaVerzameling()
	{
		if(!$this->tagAreaVerzameling instanceof TagAreaVerzameling)
			$this->tagAreaVerzameling = TagAreaVerzameling::fromTag($this);
		return $this->tagAreaVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Tag::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Tag.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Tag::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getTagID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Tag|false
	 * Een Tag-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$tagID = (int)$a[0];
		}
		else if(isset($a))
		{
			$tagID = (int)$a;
		}

		if(is_null($tagID))
			throw new BadMethodCallException();

		static::cache(array( array($tagID) ));
		return Entiteit::geefCache(array($tagID), 'Tag');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Tag');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Tag::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Tag`.`tagID`'
		                 .     ', `Tag`.`media_mediaID`'
		                 .     ', `Tag`.`persoon_contactID`'
		                 .     ', `Tag`.`commissie_commissieID`'
		                 .     ', `Tag`.`introgroep_groepID`'
		                 .     ', `Tag`.`collectie_collectieID`'
		                 .     ', `Tag`.`tagger_contactID`'
		                 .     ', `Tag`.`gewijzigdWanneer`'
		                 .     ', `Tag`.`gewijzigdWie`'
		                 .' FROM `Tag`'
		                 .' WHERE (`tagID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['tagID']);

			$obj = new Tag();

			$obj->inDB = True;

			$obj->tagID  = (int) $row['tagID'];
			$obj->media_mediaID  = (int) $row['media_mediaID'];
			$obj->persoon_contactID  = (is_null($row['persoon_contactID'])) ? null : (int) $row['persoon_contactID'];
			$obj->commissie_commissieID  = (is_null($row['commissie_commissieID'])) ? null : (int) $row['commissie_commissieID'];
			$obj->introgroep_groepID  = (is_null($row['introgroep_groepID'])) ? null : (int) $row['introgroep_groepID'];
			$obj->collectie_collectieID  = (is_null($row['collectie_collectieID'])) ? null : (int) $row['collectie_collectieID'];
			$obj->tagger_contactID  = (int) $row['tagger_contactID'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Tag')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getMediaMediaID();
		$this->getPersoonContactID();
		$this->getCommissieCommissieID();
		$this->getIntrogroepGroepID();
		$this->getCollectieCollectieID();
		$this->getTaggerContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->tagID =
			$WSW4DB->q('RETURNID INSERT INTO `Tag`'
			          . ' (`media_mediaID`, `persoon_contactID`, `commissie_commissieID`, `introgroep_groepID`, `collectie_collectieID`, `tagger_contactID`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %i, %i, %i, %s, %i)'
			          , $this->media_mediaID
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          , $this->introgroep_groepID
			          , $this->collectie_collectieID
			          , $this->tagger_contactID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Tag')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Tag`'
			          .' SET `media_mediaID` = %i'
			          .   ', `persoon_contactID` = %i'
			          .   ', `commissie_commissieID` = %i'
			          .   ', `introgroep_groepID` = %i'
			          .   ', `collectie_collectieID` = %i'
			          .   ', `tagger_contactID` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `tagID` = %i'
			          , $this->media_mediaID
			          , $this->persoon_contactID
			          , $this->commissie_commissieID
			          , $this->introgroep_groepID
			          , $this->collectie_collectieID
			          , $this->tagger_contactID
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->tagID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTag
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTag($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTag($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Media';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Introgroep';
			$velden[] = 'Collectie';
			$velden[] = 'Tagger';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			$velden[] = 'Media';
			$velden[] = 'Tagger';
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Media';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Introgroep';
			$velden[] = 'Collectie';
			$velden[] = 'Tagger';
			break;
		case 'get':
			$velden[] = 'Media';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Introgroep';
			$velden[] = 'Collectie';
			$velden[] = 'Tagger';
		case 'primary':
			$velden[] = 'media_mediaID';
			$velden[] = 'persoon_contactID';
			$velden[] = 'commissie_commissieID';
			$velden[] = 'introgroep_groepID';
			$velden[] = 'collectie_collectieID';
			$velden[] = 'tagger_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'TagAreaVerzameling';
			break;
		default:
			$velden[] = 'Media';
			$velden[] = 'Persoon';
			$velden[] = 'Commissie';
			$velden[] = 'Introgroep';
			$velden[] = 'Collectie';
			$velden[] = 'Tagger';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle TagArea-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TagAreaFromTagVerz = TagAreaVerzameling::fromTag($this);
		$returnValue = $TagAreaFromTagVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object TagArea met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $TagAreaFromTagVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Tag`'
		          .' WHERE `tagID` = %i'
		          .' LIMIT 1'
		          , $this->tagID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->tagID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Tag`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `tagID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->tagID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Tag terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'tagid':
			return 'int';
		case 'media':
			return 'foreign';
		case 'media_mediaid':
			return 'int';
		case 'persoon':
			return 'foreign';
		case 'persoon_contactid':
			return 'int';
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'introgroep':
			return 'foreign';
		case 'introgroep_groepid':
			return 'int';
		case 'collectie':
			return 'foreign';
		case 'collectie_collectieid':
			return 'int';
		case 'tagger':
			return 'foreign';
		case 'tagger_contactid':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Tag`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `tagID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->tagID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Tag`'
		          .' WHERE `tagID` = %i'
		                 , $veld
		          , $this->tagID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Tag');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle TagArea-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagAreaFromTagVerz = TagAreaVerzameling::fromTag($this);
		$dependencies['TagArea'] = $TagAreaFromTagVerz;

		return $dependencies;
	}
}
