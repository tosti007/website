<?
/**
 * @brief 'AT'AUTH_GET:vicie,AUTH_SET:vicie
 */
abstract class Media_Generated
	extends Entiteit
{
	protected $mediaID;					/**< \brief PRIMARY */
	protected $activiteit;				/**< \brief NULL */
	protected $activiteit_activiteitID;	/**< \brief PRIMARY */
	protected $uploader;				/**< \brief NULL */
	protected $uploader_contactID;		/**< \brief PRIMARY */
	protected $fotograaf;				/**< \brief NULL */
	protected $fotograaf_contactID;		/**< \brief PRIMARY */
	protected $soort;					/**< \brief ENUM:foto/film */
	protected $gemaakt;					/**< \brief NULL */
	protected $licence;					/**< \brief ENUM:cc3.0-by-nc-sa/cc3.0-by-nc-sa-aes2/cc3.0-by-nc/cc3.0-by-nd-aes2 */
	protected $premium;
	protected $views;
	protected $lock;
	protected $rating;
	protected $omschrijving;			/**< \brief NULL */
	protected $breedte;
	protected $hoogte;
	/** Verzamelingen **/
	protected $tagVerzameling;
	protected $ratingVerzameling;
	protected $kartVoorwerpVerzameling;
	protected $profielFotoVerzameling;
	protected $commissieFotoVerzameling;
	/**
	/**
	 * @brief De constructor van de Media_Generated-klasse.
	 *
	 * @param mixed $a Activiteit (Activiteit OR Array(activiteit_activiteitID) OR
	 * activiteit_activiteitID)
	 * @param mixed $b Uploader (Persoon OR Array(persoon_contactID) OR
	 * persoon_contactID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Activiteit)
		{
			$this->activiteit = $a;
			$this->activiteit_activiteitID = $a->getActiviteitID();
		}
		else if(is_array($a))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = (int)$a;
		}
		else
		{
			$this->activiteit = NULL;
			$this->activiteit_activiteitID = NULL;
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Persoon)
		{
			$this->uploader = $b;
			$this->uploader_contactID = $b->getContactID();
		}
		else if(is_array($b))
		{
			$this->uploader = NULL;
			$this->uploader_contactID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->uploader = NULL;
			$this->uploader_contactID = (int)$b;
		}
		else
		{
			$this->uploader = NULL;
			$this->uploader_contactID = NULL;
		}

		$this->mediaID = NULL;
		$this->fotograaf = NULL;
		$this->fotograaf_contactID = NULL;
		$this->soort = 'FOTO';
		$this->gemaakt = new DateTimeLocale(NULL);
		$this->licence = 'CC3.0-BY-NC-SA';
		$this->premium = False;
		$this->views = 0;
		$this->lock = False;
		$this->rating = 50;
		$this->omschrijving = NULL;
		$this->breedte = 0;
		$this->hoogte = 0;
		$this->tagVerzameling = NULL;
		$this->ratingVerzameling = NULL;
		$this->kartVoorwerpVerzameling = NULL;
		$this->profielFotoVerzameling = NULL;
		$this->commissieFotoVerzameling = NULL;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Activiteit';
		$volgorde[] = 'Uploader';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld mediaID.
	 *
	 * @return int
	 * De waarde van het veld mediaID.
	 */
	public function getMediaID()
	{
		return $this->mediaID;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit.
	 *
	 * @return Activiteit
	 * De waarde van het veld activiteit.
	 */
	public function getActiviteit()
	{
		if(!isset($this->activiteit)
		 && isset($this->activiteit_activiteitID)
		 ) {
			$this->activiteit = Activiteit::geef
					( $this->activiteit_activiteitID
					);
		}
		return $this->activiteit;
	}
	/**
	 * @brief Geef de waarde van het veld activiteit_activiteitID.
	 *
	 * @return int
	 * De waarde van het veld activiteit_activiteitID.
	 */
	public function getActiviteitActiviteitID()
	{
		if (is_null($this->activiteit_activiteitID) && isset($this->activiteit)) {
			$this->activiteit_activiteitID = $this->activiteit->getActiviteitID();
		}
		return $this->activiteit_activiteitID;
	}
	/**
	 * @brief Geef de waarde van het veld uploader.
	 *
	 * @return Persoon
	 * De waarde van het veld uploader.
	 */
	public function getUploader()
	{
		if(!isset($this->uploader)
		 && isset($this->uploader_contactID)
		 ) {
			$this->uploader = Persoon::geef
					( $this->uploader_contactID
					);
		}
		return $this->uploader;
	}
	/**
	 * @brief Geef de waarde van het veld uploader_contactID.
	 *
	 * @return int
	 * De waarde van het veld uploader_contactID.
	 */
	public function getUploaderContactID()
	{
		if (is_null($this->uploader_contactID) && isset($this->uploader)) {
			$this->uploader_contactID = $this->uploader->getContactID();
		}
		return $this->uploader_contactID;
	}
	/**
	 * @brief Geef de waarde van het veld fotograaf.
	 *
	 * @return Persoon
	 * De waarde van het veld fotograaf.
	 */
	public function getFotograaf()
	{
		if(!isset($this->fotograaf)
		 && isset($this->fotograaf_contactID)
		 ) {
			$this->fotograaf = Persoon::geef
					( $this->fotograaf_contactID
					);
		}
		return $this->fotograaf;
	}
	/**
	 * @brief Stel de waarde van het veld fotograaf in.
	 *
	 * @param mixed $new_contactID De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setFotograaf($new_contactID)
	{
		unset($this->errors['Fotograaf']);
		if($new_contactID instanceof Persoon
		) {
			if($this->fotograaf == $new_contactID
			&& $this->fotograaf_contactID == $this->fotograaf->getContactID())
				return $this;
			$this->fotograaf = $new_contactID;
			$this->fotograaf_contactID
					= $this->fotograaf->getContactID();
			$this->gewijzigd();
			return $this;
		}
		if ((is_null($new_contactID) || $new_contactID == 0)) {
			if($this->fotograaf == NULL && $this->fotograaf_contactID == NULL)
				return $this;
			$this->fotograaf = NULL;
			$this->fotograaf_contactID = NULL;
			$this->gewijzigd();
			return $this;
		}
		if(isset($new_contactID)
		) {
			if($this->fotograaf == NULL 
				&& $this->fotograaf_contactID == (int)$new_contactID)
				return $this;
			$this->fotograaf = NULL;
			$this->fotograaf_contactID
					= (int)$new_contactID;
			$this->gewijzigd();
			return $this;
		}
		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld fotograaf geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld fotograaf geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkFotograaf()
	{
		if (array_key_exists('Fotograaf', $this->errors))
			return $this->errors['Fotograaf'];
		$waarde1 = $this->getFotograaf();
		$waarde2 = $this->getFotograafContactID();
		if(empty($waarde1)
			&& (empty($waarde2)))
		{
			return False;

		}
		return False;
	}
	/**
	 * @brief Geef de waarde van het veld fotograaf_contactID.
	 *
	 * @return int
	 * De waarde van het veld fotograaf_contactID.
	 */
	public function getFotograafContactID()
	{
		if (is_null($this->fotograaf_contactID) && isset($this->fotograaf)) {
			$this->fotograaf_contactID = $this->fotograaf->getContactID();
		}
		return $this->fotograaf_contactID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld soort.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld soort.
	 */
	static public function enumsSoort()
	{
		static $vals = array('FOTO','FILM');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld soort.
	 *
	 * @return string
	 * De waarde van het veld soort.
	 */
	public function getSoort()
	{
		return $this->soort;
	}
	/**
	 * @brief Stel de waarde van het veld soort in.
	 *
	 * @param mixed $newSoort De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setSoort($newSoort)
	{
		unset($this->errors['Soort']);
		if(!is_null($newSoort))
			$newSoort = strtoupper(trim($newSoort));
		if($newSoort === "")
			$newSoort = NULL;
		if($this->soort === $newSoort)
			return $this;

		$this->soort = $newSoort;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld soort geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld soort geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkSoort()
	{
		if (array_key_exists('Soort', $this->errors))
			return $this->errors['Soort'];
		$waarde = $this->getSoort();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsSoort()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld gemaakt.
	 *
	 * @return DateTimeLocale
	 * De waarde van het veld gemaakt.
	 */
	public function getGemaakt()
	{
		return $this->gemaakt;
	}
	/**
	 * @brief Stel de waarde van het veld gemaakt in.
	 *
	 * @param mixed $newGemaakt De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setGemaakt($newGemaakt)
	{
		unset($this->errors['Gemaakt']);
		if(!$newGemaakt instanceof DateTimeLocale) {
			try {
				$newGemaakt = new DateTimeLocale($newGemaakt);
			} catch(Exception $e) {
				return $this;
			}
		}
		if($this->gemaakt->strftime('%F %T') == $newGemaakt->strftime('%F %T'))
			return $this;

		$this->gemaakt = $newGemaakt;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld gemaakt geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld gemaakt geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkGemaakt()
	{
		if (array_key_exists('Gemaakt', $this->errors))
			return $this->errors['Gemaakt'];
		$waarde = $this->getGemaakt();
		if (!$waarde->hasTime())
			return False;

		return False;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld licence.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld licence.
	 */
	static public function enumsLicence()
	{
		static $vals = array('CC3.0-BY-NC-SA','CC3.0-BY-NC-SA-AES2','CC3.0-BY-NC','CC3.0-BY-ND-AES2');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld licence.
	 *
	 * @return string
	 * De waarde van het veld licence.
	 */
	public function getLicence()
	{
		return $this->licence;
	}
	/**
	 * @brief Stel de waarde van het veld licence in.
	 *
	 * @param mixed $newLicence De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setLicence($newLicence)
	{
		unset($this->errors['Licence']);
		if(!is_null($newLicence))
			$newLicence = strtoupper(trim($newLicence));
		if($newLicence === "")
			$newLicence = NULL;
		if($this->licence === $newLicence)
			return $this;

		$this->licence = $newLicence;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld licence geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld licence geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkLicence()
	{
		if (array_key_exists('Licence', $this->errors))
			return $this->errors['Licence'];
		$waarde = $this->getLicence();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsLicence()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld premium.
	 *
	 * @return bool
	 * De waarde van het veld premium.
	 */
	public function getPremium()
	{
		return $this->premium;
	}
	/**
	 * @brief Stel de waarde van het veld premium in.
	 *
	 * @param mixed $newPremium De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setPremium($newPremium)
	{
		unset($this->errors['Premium']);
		if(!is_null($newPremium))
			$newPremium = (bool)$newPremium;
		if($this->premium === $newPremium)
			return $this;

		$this->premium = $newPremium;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld premium geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld premium geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkPremium()
	{
		if (array_key_exists('Premium', $this->errors))
			return $this->errors['Premium'];
		$waarde = $this->getPremium();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld views.
	 *
	 * @return int
	 * De waarde van het veld views.
	 */
	public function getViews()
	{
		return $this->views;
	}
	/**
	 * @brief Stel de waarde van het veld views in.
	 *
	 * @param mixed $newViews De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setViews($newViews)
	{
		unset($this->errors['Views']);
		if(!is_null($newViews))
			$newViews = (int)$newViews;
		if($this->views === $newViews)
			return $this;

		$this->views = $newViews;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld views geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld views geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkViews()
	{
		if (array_key_exists('Views', $this->errors))
			return $this->errors['Views'];
		$waarde = $this->getViews();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld lock.
	 *
	 * @return bool
	 * De waarde van het veld lock.
	 */
	public function getLock()
	{
		return $this->lock;
	}
	/**
	 * @brief Stel de waarde van het veld lock in.
	 *
	 * @param mixed $newLock De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setLock($newLock)
	{
		unset($this->errors['Lock']);
		if(!is_null($newLock))
			$newLock = (bool)$newLock;
		if($this->lock === $newLock)
			return $this;

		$this->lock = $newLock;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld lock geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld lock geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkLock()
	{
		if (array_key_exists('Lock', $this->errors))
			return $this->errors['Lock'];
		$waarde = $this->getLock();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld rating.
	 *
	 * @return int
	 * De waarde van het veld rating.
	 */
	public function getRating()
	{
		return $this->rating;
	}
	/**
	 * @brief Stel de waarde van het veld rating in.
	 *
	 * @param mixed $newRating De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setRating($newRating)
	{
		unset($this->errors['Rating']);
		if(!is_null($newRating))
			$newRating = (int)$newRating;
		if($this->rating === $newRating)
			return $this;

		$this->rating = $newRating;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld rating geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld rating geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkRating()
	{
		if (array_key_exists('Rating', $this->errors))
			return $this->errors['Rating'];
		$waarde = $this->getRating();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld omschrijving.
	 *
	 * @return string
	 * De waarde van het veld omschrijving.
	 */
	public function getOmschrijving()
	{
		return $this->omschrijving;
	}
	/**
	 * @brief Stel de waarde van het veld omschrijving in.
	 *
	 * @param mixed $newOmschrijving De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setOmschrijving($newOmschrijving)
	{
		unset($this->errors['Omschrijving']);
		if(!is_null($newOmschrijving))
			$newOmschrijving = trim($newOmschrijving);
		if($newOmschrijving === "")
			$newOmschrijving = NULL;
		if($this->omschrijving === $newOmschrijving)
			return $this;

		$this->omschrijving = $newOmschrijving;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld omschrijving geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld omschrijving geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkOmschrijving()
	{
		if (array_key_exists('Omschrijving', $this->errors))
			return $this->errors['Omschrijving'];
		$waarde = $this->getOmschrijving();
		if (is_null($waarde))
			return False;

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld breedte.
	 *
	 * @return int
	 * De waarde van het veld breedte.
	 */
	public function getBreedte()
	{
		return $this->breedte;
	}
	/**
	 * @brief Stel de waarde van het veld breedte in.
	 *
	 * @param mixed $newBreedte De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setBreedte($newBreedte)
	{
		unset($this->errors['Breedte']);
		if(!is_null($newBreedte))
			$newBreedte = (int)$newBreedte;
		if($this->breedte === $newBreedte)
			return $this;

		$this->breedte = $newBreedte;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld breedte geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld breedte geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkBreedte()
	{
		if (array_key_exists('Breedte', $this->errors))
			return $this->errors['Breedte'];
		$waarde = $this->getBreedte();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld hoogte.
	 *
	 * @return int
	 * De waarde van het veld hoogte.
	 */
	public function getHoogte()
	{
		return $this->hoogte;
	}
	/**
	 * @brief Stel de waarde van het veld hoogte in.
	 *
	 * @param mixed $newHoogte De nieuwe waarde.
	 *
	 * @return Media
	 * Dit Media-object.
	 */
	public function setHoogte($newHoogte)
	{
		unset($this->errors['Hoogte']);
		if(!is_null($newHoogte))
			$newHoogte = (int)$newHoogte;
		if($this->hoogte === $newHoogte)
			return $this;

		$this->hoogte = $newHoogte;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld hoogte geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld hoogte geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkHoogte()
	{
		if (array_key_exists('Hoogte', $this->errors))
			return $this->errors['Hoogte'];
		$waarde = $this->getHoogte();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Returneert de TagVerzameling die hoort bij dit object.
	 */
	public function getTagVerzameling()
	{
		if(!$this->tagVerzameling instanceof TagVerzameling)
			$this->tagVerzameling = TagVerzameling::fromMedia($this);
		return $this->tagVerzameling;
	}
	/**
	 * @brief Returneert de RatingVerzameling die hoort bij dit object.
	 */
	public function getRatingVerzameling()
	{
		if(!$this->ratingVerzameling instanceof RatingVerzameling)
			$this->ratingVerzameling = RatingVerzameling::fromMedia($this);
		return $this->ratingVerzameling;
	}
	/**
	 * @brief Returneert de KartVoorwerpVerzameling die hoort bij dit object.
	 */
	public function getKartVoorwerpVerzameling()
	{
		if(!$this->kartVoorwerpVerzameling instanceof KartVoorwerpVerzameling)
			$this->kartVoorwerpVerzameling = KartVoorwerpVerzameling::fromMedia($this);
		return $this->kartVoorwerpVerzameling;
	}
	/**
	 * @brief Returneert de ProfielFotoVerzameling die hoort bij dit object.
	 */
	public function getProfielFotoVerzameling()
	{
		if(!$this->profielFotoVerzameling instanceof ProfielFotoVerzameling)
			$this->profielFotoVerzameling = ProfielFotoVerzameling::fromMedia($this);
		return $this->profielFotoVerzameling;
	}
	/**
	 * @brief Returneert de CommissieFotoVerzameling die hoort bij dit object.
	 */
	public function getCommissieFotoVerzameling()
	{
		if(!$this->commissieFotoVerzameling instanceof CommissieFotoVerzameling)
			$this->commissieFotoVerzameling = CommissieFotoVerzameling::fromMedia($this);
		return $this->commissieFotoVerzameling;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('vicie');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return Media::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van Media.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('vicie');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return Media::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID($this->getMediaID());
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return Media|false
	 * Een Media-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$mediaID = (int)$a[0];
		}
		else if(isset($a))
		{
			$mediaID = (int)$a;
		}

		if(is_null($mediaID))
			throw new BadMethodCallException();

		static::cache(array( array($mediaID) ));
		return Entiteit::geefCache(array($mediaID), 'Media');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'Media');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('Media::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `Media`.`mediaID`'
		                 .     ', `Media`.`activiteit_activiteitID`'
		                 .     ', `Media`.`uploader_contactID`'
		                 .     ', `Media`.`fotograaf_contactID`'
		                 .     ', `Media`.`soort`'
		                 .     ', `Media`.`gemaakt`'
		                 .     ', `Media`.`licence`'
		                 .     ', `Media`.`premium`'
		                 .     ', `Media`.`views`'
		                 .     ', `Media`.`lock`'
		                 .     ', `Media`.`rating`'
		                 .     ', `Media`.`omschrijving`'
		                 .     ', `Media`.`breedte`'
		                 .     ', `Media`.`hoogte`'
		                 .     ', `Media`.`gewijzigdWanneer`'
		                 .     ', `Media`.`gewijzigdWie`'
		                 .' FROM `Media`'
		                 .' WHERE (`mediaID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['mediaID']);

			$obj = new Media(array($row['activiteit_activiteitID']), array($row['uploader_contactID']));

			$obj->inDB = True;

			$obj->mediaID  = (int) $row['mediaID'];
			$obj->fotograaf_contactID  = (is_null($row['fotograaf_contactID'])) ? null : (int) $row['fotograaf_contactID'];
			$obj->soort  = strtoupper(trim($row['soort']));
			$obj->gemaakt  = new DateTimeLocale($row['gemaakt']);
			$obj->licence  = strtoupper(trim($row['licence']));
			$obj->premium  = (bool) $row['premium'];
			$obj->views  = (int) $row['views'];
			$obj->lock  = (bool) $row['lock'];
			$obj->rating  = (int) $row['rating'];
			$obj->omschrijving  = (is_null($row['omschrijving'])) ? null : trim($row['omschrijving']);
			$obj->breedte  = (int) $row['breedte'];
			$obj->hoogte  = (int) $row['hoogte'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'Media')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		if (!$this->dirty)
			return;
		$this->getActiviteitActiviteitID();
		$this->getUploaderContactID();
		$this->getFotograafContactID();

		$this->gewijzigd();

		if(!$this->inDB) {
			$this->mediaID =
			$WSW4DB->q('RETURNID INSERT INTO `Media`'
			          . ' (`activiteit_activiteitID`, `uploader_contactID`, `fotograaf_contactID`, `soort`, `gemaakt`, `licence`, `premium`, `views`, `lock`, `rating`, `omschrijving`, `breedte`, `hoogte`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %s, %s, %s, %i, %i, %i, %i, %s, %i, %i, %s, %i)'
			          , $this->activiteit_activiteitID
			          , $this->uploader_contactID
			          , $this->fotograaf_contactID
			          , $this->soort
			          , (!is_null($this->gemaakt))?$this->gemaakt->strftime('%F %T'):null
			          , $this->licence
			          , $this->premium
			          , $this->views
			          , $this->lock
			          , $this->rating
			          , $this->omschrijving
			          , $this->breedte
			          , $this->hoogte
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'Media')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `Media`'
			          .' SET `activiteit_activiteitID` = %i'
			          .   ', `uploader_contactID` = %i'
			          .   ', `fotograaf_contactID` = %i'
			          .   ', `soort` = %s'
			          .   ', `gemaakt` = %s'
			          .   ', `licence` = %s'
			          .   ', `premium` = %i'
			          .   ', `views` = %i'
			          .   ', `lock` = %i'
			          .   ', `rating` = %i'
			          .   ', `omschrijving` = %s'
			          .   ', `breedte` = %i'
			          .   ', `hoogte` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mediaID` = %i'
			          , $this->activiteit_activiteitID
			          , $this->uploader_contactID
			          , $this->fotograaf_contactID
			          , $this->soort
			          , (!is_null($this->gemaakt))?$this->gemaakt->strftime('%F %T'):null
			          , $this->licence
			          , $this->premium
			          , $this->views
			          , $this->lock
			          , $this->rating
			          , $this->omschrijving
			          , $this->breedte
			          , $this->hoogte
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mediaID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenMedia
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenMedia($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenMedia($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Fotograaf';
			$velden[] = 'Soort';
			$velden[] = 'Gemaakt';
			$velden[] = 'Licence';
			$velden[] = 'Premium';
			$velden[] = 'Views';
			$velden[] = 'Lock';
			$velden[] = 'Rating';
			$velden[] = 'Omschrijving';
			$velden[] = 'Breedte';
			$velden[] = 'Hoogte';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Activiteit';
			$velden[] = 'Uploader';
			$velden[] = 'Fotograaf';
			$velden[] = 'Soort';
			$velden[] = 'Gemaakt';
			$velden[] = 'Licence';
			$velden[] = 'Premium';
			$velden[] = 'Views';
			$velden[] = 'Lock';
			$velden[] = 'Rating';
			$velden[] = 'Omschrijving';
			$velden[] = 'Breedte';
			$velden[] = 'Hoogte';
			break;
		case 'get':
			$velden[] = 'Activiteit';
			$velden[] = 'Uploader';
			$velden[] = 'Fotograaf';
			$velden[] = 'Soort';
			$velden[] = 'Gemaakt';
			$velden[] = 'Licence';
			$velden[] = 'Premium';
			$velden[] = 'Views';
			$velden[] = 'Lock';
			$velden[] = 'Rating';
			$velden[] = 'Omschrijving';
			$velden[] = 'Breedte';
			$velden[] = 'Hoogte';
		case 'primary':
			$velden[] = 'activiteit_activiteitID';
			$velden[] = 'uploader_contactID';
			$velden[] = 'fotograaf_contactID';
			break;
		case 'verzamelingen':
			$velden[] = 'TagVerzameling';
			$velden[] = 'RatingVerzameling';
			$velden[] = 'KartVoorwerpVerzameling';
			$velden[] = 'ProfielFotoVerzameling';
			$velden[] = 'CommissieFotoVerzameling';
			break;
		default:
			$velden[] = 'Activiteit';
			$velden[] = 'Uploader';
			$velden[] = 'Fotograaf';
			$velden[] = 'Soort';
			$velden[] = 'Gemaakt';
			$velden[] = 'Licence';
			$velden[] = 'Premium';
			$velden[] = 'Views';
			$velden[] = 'Lock';
			$velden[] = 'Rating';
			$velden[] = 'Omschrijving';
			$velden[] = 'Breedte';
			$velden[] = 'Hoogte';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		// Verzamel alle Tag-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$TagFromMediaVerz = TagVerzameling::fromMedia($this);
		$returnValue = $TagFromMediaVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Tag met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle Rating-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$RatingFromMediaVerz = RatingVerzameling::fromMedia($this);
		$returnValue = $RatingFromMediaVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object Rating met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle KartVoorwerp-objecten die op dit object dependen
		// om de foreign key op null te zetten,
		// en return een error als ze niet aangepakt mogen worden.
		$KartVoorwerpFromMediaVerz = KartVoorwerpVerzameling::fromMedia($this);
		$returnValue = $KartVoorwerpFromMediaVerz->magWijzigen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object KartVoorwerp met id ".$val." verwijst naar het te verwijderen object, maar mag niet gewijzigd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle ProfielFoto-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$ProfielFotoFromMediaVerz = ProfielFotoVerzameling::fromMedia($this);
		$returnValue = $ProfielFotoFromMediaVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object ProfielFoto met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		// Verzamel alle CommissieFoto-objecten die op dit object dependen
		// om te verwijderen,
		// en return een error als ze niet aangepakt mogen worden.
		$CommissieFotoFromMediaVerz = CommissieFotoVerzameling::fromMedia($this);
		$returnValue = $CommissieFotoFromMediaVerz->magVerwijderen();
		if($returnValue !== true)
		{
			foreach($returnValue as $i => $val)
			{
				$returnValue[$i] = "Een object CommissieFoto met id ".$val." verwijst naar het te verwijderen object, maar mag niet verwijderd worden.";
			}
			return $returnValue;
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		foreach($KartVoorwerpFromMediaVerz as $v)
		{
			$v->setMedia(NULL);
		}
		$KartVoorwerpFromMediaVerz->opslaan();

		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode
		$returnValue = $TagFromMediaVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $RatingFromMediaVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $ProfielFotoFromMediaVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}

		$returnValue = $CommissieFotoFromMediaVerz->verwijderen(true);
		if(!is_array($returnValue))
		{
			return $returnValue;
		} else {
			$queryarray[] = $returnValue;
		}


		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `Media`'
		          .' WHERE `mediaID` = %i'
		          .' LIMIT 1'
		          , $this->mediaID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->mediaID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `Media`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `mediaID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->mediaID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van Media terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'mediaid':
			return 'int';
		case 'activiteit':
			return 'foreign';
		case 'activiteit_activiteitid':
			return 'int';
		case 'uploader':
			return 'foreign';
		case 'uploader_contactid':
			return 'int';
		case 'fotograaf':
			return 'foreign';
		case 'fotograaf_contactid':
			return 'int';
		case 'soort':
			return 'enum';
		case 'gemaakt':
			return 'datetime';
		case 'licence':
			return 'enum';
		case 'premium':
			return 'bool';
		case 'views':
			return 'int';
		case 'lock':
			return 'bool';
		case 'rating':
			return 'int';
		case 'omschrijving':
			return 'text';
		case 'breedte':
			return 'int';
		case 'hoogte':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'mediaID':
		case 'activiteit_activiteitID':
		case 'uploader_contactID':
		case 'fotograaf_contactID':
		case 'premium':
		case 'views':
		case 'lock':
		case 'rating':
		case 'breedte':
		case 'hoogte':
			$type = '%i';
			break;
		case 'soort':
		case 'gemaakt':
		case 'licence':
		case 'omschrijving':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `Media`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `mediaID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->mediaID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `Media`'
		          .' WHERE `mediaID` = %i'
		                 , $veld
		          , $this->mediaID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'Media');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		// Verzamel alle Tag-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$TagFromMediaVerz = TagVerzameling::fromMedia($this);
		$dependencies['Tag'] = $TagFromMediaVerz;

		// Verzamel alle Rating-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$RatingFromMediaVerz = RatingVerzameling::fromMedia($this);
		$dependencies['Rating'] = $RatingFromMediaVerz;

		// Verzamel alle KartVoorwerp-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$KartVoorwerpFromMediaVerz = KartVoorwerpVerzameling::fromMedia($this);
		$dependencies['KartVoorwerp'] = $KartVoorwerpFromMediaVerz;

		// Verzamel alle ProfielFoto-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$ProfielFotoFromMediaVerz = ProfielFotoVerzameling::fromMedia($this);
		$dependencies['ProfielFoto'] = $ProfielFotoFromMediaVerz;

		// Verzamel alle CommissieFoto-objecten die op dit object dependen,
		// en stop die in onze returnwaarde.
		$CommissieFotoFromMediaVerz = CommissieFotoVerzameling::fromMedia($this);
		$dependencies['CommissieFoto'] = $CommissieFotoFromMediaVerz;

		return $dependencies;
	}
}
