<?
abstract class TagArea_Generated
	extends Entiteit
{
	protected $tag;						/**< \brief PRIMARY */
	protected $tag_tagID;				/**< \brief PRIMARY */
	protected $beginPixelX;
	protected $eindPixelX;
	protected $beginPixelY;
	protected $eindPixelY;
	/**
	/**
	 * @brief De constructor van de TagArea_Generated-klasse.
	 *
	 * @param mixed $a Tag (Tag OR Array(tag_tagID) OR tag_tagID)
	 */
	public function __construct($a = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Tag)
		{
			$this->tag = $a;
			$this->tag_tagID = $a->getTagID();
		}
		else if(is_array($a))
		{
			$this->tag = NULL;
			$this->tag_tagID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->tag = NULL;
			$this->tag_tagID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->beginPixelX = 0;
		$this->eindPixelX = 0;
		$this->beginPixelY = 0;
		$this->eindPixelY = 0;
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Tag';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld tag.
	 *
	 * @return Tag
	 * De waarde van het veld tag.
	 */
	public function getTag()
	{
		if(!isset($this->tag)
		 && isset($this->tag_tagID)
		 ) {
			$this->tag = Tag::geef
					( $this->tag_tagID
					);
		}
		return $this->tag;
	}
	/**
	 * @brief Geef de waarde van het veld tag_tagID.
	 *
	 * @return int
	 * De waarde van het veld tag_tagID.
	 */
	public function getTagTagID()
	{
		if (is_null($this->tag_tagID) && isset($this->tag)) {
			$this->tag_tagID = $this->tag->getTagID();
		}
		return $this->tag_tagID;
	}
	/**
	 * @brief Geef de waarde van het veld beginPixelX.
	 *
	 * @return int
	 * De waarde van het veld beginPixelX.
	 */
	public function getBeginPixelX()
	{
		return $this->beginPixelX;
	}
	/**
	 * @brief Stel de waarde van het veld beginPixelX in.
	 *
	 * @param mixed $newBeginPixelX De nieuwe waarde.
	 *
	 * @return TagArea
	 * Dit TagArea-object.
	 */
	public function setBeginPixelX($newBeginPixelX)
	{
		unset($this->errors['BeginPixelX']);
		if(!is_null($newBeginPixelX))
			$newBeginPixelX = (int)$newBeginPixelX;
		if($this->beginPixelX === $newBeginPixelX)
			return $this;

		$this->beginPixelX = $newBeginPixelX;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld beginPixelX geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld beginPixelX geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBeginPixelX()
	{
		if (array_key_exists('BeginPixelX', $this->errors))
			return $this->errors['BeginPixelX'];
		$waarde = $this->getBeginPixelX();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld eindPixelX.
	 *
	 * @return int
	 * De waarde van het veld eindPixelX.
	 */
	public function getEindPixelX()
	{
		return $this->eindPixelX;
	}
	/**
	 * @brief Stel de waarde van het veld eindPixelX in.
	 *
	 * @param mixed $newEindPixelX De nieuwe waarde.
	 *
	 * @return TagArea
	 * Dit TagArea-object.
	 */
	public function setEindPixelX($newEindPixelX)
	{
		unset($this->errors['EindPixelX']);
		if(!is_null($newEindPixelX))
			$newEindPixelX = (int)$newEindPixelX;
		if($this->eindPixelX === $newEindPixelX)
			return $this;

		$this->eindPixelX = $newEindPixelX;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld eindPixelX geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld eindPixelX geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkEindPixelX()
	{
		if (array_key_exists('EindPixelX', $this->errors))
			return $this->errors['EindPixelX'];
		$waarde = $this->getEindPixelX();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld beginPixelY.
	 *
	 * @return int
	 * De waarde van het veld beginPixelY.
	 */
	public function getBeginPixelY()
	{
		return $this->beginPixelY;
	}
	/**
	 * @brief Stel de waarde van het veld beginPixelY in.
	 *
	 * @param mixed $newBeginPixelY De nieuwe waarde.
	 *
	 * @return TagArea
	 * Dit TagArea-object.
	 */
	public function setBeginPixelY($newBeginPixelY)
	{
		unset($this->errors['BeginPixelY']);
		if(!is_null($newBeginPixelY))
			$newBeginPixelY = (int)$newBeginPixelY;
		if($this->beginPixelY === $newBeginPixelY)
			return $this;

		$this->beginPixelY = $newBeginPixelY;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld beginPixelY geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld beginPixelY geldig is; anders een
	 * string met een foutmelding.
	 */
	public function checkBeginPixelY()
	{
		if (array_key_exists('BeginPixelY', $this->errors))
			return $this->errors['BeginPixelY'];
		$waarde = $this->getBeginPixelY();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Geef de waarde van het veld eindPixelY.
	 *
	 * @return int
	 * De waarde van het veld eindPixelY.
	 */
	public function getEindPixelY()
	{
		return $this->eindPixelY;
	}
	/**
	 * @brief Stel de waarde van het veld eindPixelY in.
	 *
	 * @param mixed $newEindPixelY De nieuwe waarde.
	 *
	 * @return TagArea
	 * Dit TagArea-object.
	 */
	public function setEindPixelY($newEindPixelY)
	{
		unset($this->errors['EindPixelY']);
		if(!is_null($newEindPixelY))
			$newEindPixelY = (int)$newEindPixelY;
		if($this->eindPixelY === $newEindPixelY)
			return $this;

		$this->eindPixelY = $newEindPixelY;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld eindPixelY geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld eindPixelY geldig is; anders een string
	 * met een foutmelding.
	 */
	public function checkEindPixelY()
	{
		if (array_key_exists('EindPixelY', $this->errors))
			return $this->errors['EindPixelY'];
		$waarde = $this->getEindPixelY();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return TagArea::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van TagArea.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return TagArea::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getTagTagID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 *
	 * @return TagArea|false
	 * Een TagArea-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a)
	{
		if(is_null($a))
			return false;

		if(is_string($a))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& count($a) == 1)
		{
			$tag_tagID = (int)$a[0];
		}
		else if($a instanceof Tag)
		{
			$tag_tagID = $a->getTagID();
		}
		else if(isset($a))
		{
			$tag_tagID = (int)$a;
		}

		if(is_null($tag_tagID))
			throw new BadMethodCallException();

		static::cache(array( array($tag_tagID) ));
		return Entiteit::geefCache(array($tag_tagID), 'TagArea');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een int. $ids = array( aID, bID,
	 * ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'TagArea');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('TagArea::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `TagArea`.`tag_tagID`'
		                 .     ', `TagArea`.`beginPixelX`'
		                 .     ', `TagArea`.`eindPixelX`'
		                 .     ', `TagArea`.`beginPixelY`'
		                 .     ', `TagArea`.`eindPixelY`'
		                 .     ', `TagArea`.`gewijzigdWanneer`'
		                 .     ', `TagArea`.`gewijzigdWie`'
		                 .' FROM `TagArea`'
		                 .' WHERE (`tag_tagID`)'
		                 .      ' IN (%A{i})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['tag_tagID']);

			$obj = new TagArea(array($row['tag_tagID']));

			$obj->inDB = True;

			$obj->beginPixelX  = (int) $row['beginPixelX'];
			$obj->eindPixelX  = (int) $row['eindPixelX'];
			$obj->beginPixelY  = (int) $row['beginPixelY'];
			$obj->eindPixelY  = (int) $row['eindPixelY'];
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'TagArea')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getTag();
		if(!$rel->getInDB())
			throw new LogicException('foreign Tag is not in DB');
		$this->tag_tagID = $rel->getTagID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `TagArea`'
			          . ' (`tag_tagID`, `beginPixelX`, `eindPixelX`, `beginPixelY`, `eindPixelY`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %i, %i, %i, %s, %i)'
			          , $this->tag_tagID
			          , $this->beginPixelX
			          , $this->eindPixelX
			          , $this->beginPixelY
			          , $this->eindPixelY
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'TagArea')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `TagArea`'
			          .' SET `beginPixelX` = %i'
			          .   ', `eindPixelX` = %i'
			          .   ', `beginPixelY` = %i'
			          .   ', `eindPixelY` = %i'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `tag_tagID` = %i'
			          , $this->beginPixelX
			          , $this->eindPixelX
			          , $this->beginPixelY
			          , $this->eindPixelY
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->tag_tagID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenTagArea
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenTagArea($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenTagArea($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'BeginPixelX';
			$velden[] = 'EindPixelX';
			$velden[] = 'BeginPixelY';
			$velden[] = 'EindPixelY';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'BeginPixelX';
			$velden[] = 'EindPixelX';
			$velden[] = 'BeginPixelY';
			$velden[] = 'EindPixelY';
			break;
		case 'get':
			$velden[] = 'BeginPixelX';
			$velden[] = 'EindPixelX';
			$velden[] = 'BeginPixelY';
			$velden[] = 'EindPixelY';
		case 'primary':
			$velden[] = 'tag_tagID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'BeginPixelX';
			$velden[] = 'EindPixelX';
			$velden[] = 'BeginPixelY';
			$velden[] = 'EindPixelY';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `TagArea`'
		          .' WHERE `tag_tagID` = %i'
		          .' LIMIT 1'
		          , $this->tag_tagID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->tag = NULL;
		$this->tag_tagID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `TagArea`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `tag_tagID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->tag_tagID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van TagArea terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'tag':
			return 'foreign';
		case 'tag_tagid':
			return 'int';
		case 'beginpixelx':
			return 'int';
		case 'eindpixelx':
			return 'int';
		case 'beginpixely':
			return 'int';
		case 'eindpixely':
			return 'int';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `TagArea`'
		          ." SET `%l` = %i"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `tag_tagID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->tag_tagID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `TagArea`'
		          .' WHERE `tag_tagID` = %i'
		                 , $veld
		          , $this->tag_tagID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'TagArea');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
