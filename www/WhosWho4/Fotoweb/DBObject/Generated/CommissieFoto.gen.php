<?
abstract class CommissieFoto_Generated
	extends Entiteit
{
	protected $commissie;				/**< \brief PRIMARY */
	protected $commissie_commissieID;	/**< \brief PRIMARY */
	protected $media;					/**< \brief PRIMARY */
	protected $media_mediaID;			/**< \brief PRIMARY */
	protected $status;					/**< \brief ENUM:OUD/HUIDIG */
	/**
	/**
	 * @brief De constructor van de CommissieFoto_Generated-klasse.
	 *
	 * @param mixed $a Commissie (Commissie OR Array(commissie_commissieID) OR
	 * commissie_commissieID)
	 * @param mixed $b Media (Media OR Array(media_mediaID) OR media_mediaID)
	 */
	public function __construct($a = NULL,
			$b = NULL)
	{
		parent::__construct(); // Entiteit

		if(is_array($a) && is_null($a[0]))
			$a = NULL;

		if($a instanceof Commissie)
		{
			$this->commissie = $a;
			$this->commissie_commissieID = $a->getCommissieID();
		}
		else if(is_array($a))
		{
			$this->commissie = NULL;
			$this->commissie_commissieID = (int)$a[0];
		}
		else if(isset($a))
		{
			$this->commissie = NULL;
			$this->commissie_commissieID = (int)$a;
		}
		else
		{
			throw new BadMethodCallException();
		}

		if(is_array($b) && is_null($b[0]))
			$b = NULL;

		if($b instanceof Media)
		{
			$this->media = $b;
			$this->media_mediaID = $b->getMediaID();
		}
		else if(is_array($b))
		{
			$this->media = NULL;
			$this->media_mediaID = (int)$b[0];
		}
		else if(isset($b))
		{
			$this->media = NULL;
			$this->media_mediaID = (int)$b;
		}
		else
		{
			throw new BadMethodCallException();
		}
		$this->status = 'OUD';
	}
	/*** CONSTRUCTOR_VOLGORDE ***/
	static public function constructorVolgorde()
	{
		$volgorde = array();
		$volgorde[] = 'Commissie';
		$volgorde[] = 'Media';
		return $volgorde;
	}
	/**
	 * @brief Geef de waarde van het veld commissie.
	 *
	 * @return Commissie
	 * De waarde van het veld commissie.
	 */
	public function getCommissie()
	{
		if(!isset($this->commissie)
		 && isset($this->commissie_commissieID)
		 ) {
			$this->commissie = Commissie::geef
					( $this->commissie_commissieID
					);
		}
		return $this->commissie;
	}
	/**
	 * @brief Geef de waarde van het veld commissie_commissieID.
	 *
	 * @return int
	 * De waarde van het veld commissie_commissieID.
	 */
	public function getCommissieCommissieID()
	{
		if (is_null($this->commissie_commissieID) && isset($this->commissie)) {
			$this->commissie_commissieID = $this->commissie->getCommissieID();
		}
		return $this->commissie_commissieID;
	}
	/**
	 * @brief Geef de waarde van het veld media.
	 *
	 * @return Media
	 * De waarde van het veld media.
	 */
	public function getMedia()
	{
		if(!isset($this->media)
		 && isset($this->media_mediaID)
		 ) {
			$this->media = Media::geef
					( $this->media_mediaID
					);
		}
		return $this->media;
	}
	/**
	 * @brief Geef de waarde van het veld media_mediaID.
	 *
	 * @return int
	 * De waarde van het veld media_mediaID.
	 */
	public function getMediaMediaID()
	{
		if (is_null($this->media_mediaID) && isset($this->media)) {
			$this->media_mediaID = $this->media->getMediaID();
		}
		return $this->media_mediaID;
	}
	/**
	 * @brief Geef de mogelijke enums van het veld status.
	 *
	 * @return string[]
	 * Een array met alle enum-waarden van het veld status.
	 */
	static public function enumsStatus()
	{
		static $vals = array('OUD','HUIDIG');
		return $vals;
	}
	/**
	 * @brief Geef de waarde van het veld status.
	 *
	 * @return string
	 * De waarde van het veld status.
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @brief Stel de waarde van het veld status in.
	 *
	 * @param mixed $newStatus De nieuwe waarde.
	 *
	 * @return CommissieFoto
	 * Dit CommissieFoto-object.
	 */
	public function setStatus($newStatus)
	{
		unset($this->errors['Status']);
		if(!is_null($newStatus))
			$newStatus = strtoupper(trim($newStatus));
		if($newStatus === "")
			$newStatus = NULL;
		if($this->status === $newStatus)
			return $this;

		$this->status = $newStatus;

		$this->gewijzigd();

		return $this;
	}
	/**
	 * @brief Controleer of de waarde van het veld status geldig is.
	 *
	 * @return bool|string
	 * False als de huidige waarde van het veld status geldig is; anders een string met
	 * een foutmelding.
	 */
	public function checkStatus()
	{
		if (array_key_exists('Status', $this->errors))
			return $this->errors['Status'];
		$waarde = $this->getStatus();
		if (is_null($waarde))
			return _('dit is een verplicht veld');

		if(!in_array($waarde, static::enumsStatus()))
			return _('onbekende waarde');

		return False;
	}
	/**
	 * @brief Retourneert of de gebruiker deze klasse mag zien.
	 */
	static public function magKlasseBekijken()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit mag zien.
	 */
	public function magBekijken()
	{
		return CommissieFoto::magKlasseBekijken();
	}
	/**
	 * @brief Geeft alle klassen terug die een extensie zijn van CommissieFoto.
	 *
	 * @return array|false
	 * Een array met de klassen as string, of False als er geen klassen zijn.
	 */
	static public function geefExtensies()
	{
		return array();
	}
	/**
	 * @brief Retourneert of de gebruiker een object van deze klasse mag aanpassen.
	 */
	static public function magKlasseWijzigen()
	{
		return hasAuth('god');
	}
	/**
	 * @brief Retourneert of de gebruiker dit object mag aanpassen.
	 */
	public function magWijzigen()
	{
		return CommissieFoto::magKlasseWijzigen();
	}
	public function getBekijkbareVars($vars = NULL)
	{
		if(!$this->magBekijken())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		if(!isset($vars['vars']))
			$vars['vars'] = array();
		$vars['vars'] += static::velden('get');
		if(!isset($vars['verzamelingen']))
			$vars['verzamelingen'] = array();
		$vars['verzamelingen'] += static::velden('verzamelingen');

		return parent::getBekijkbareVars($vars);
	}
	public function getWijzigbareVars($vars = NULL)
	{
		if(!$this->magWijzigen())
			return array();

		if(is_null($vars) || !is_array($vars))
			$vars = array();

		$vars += static::velden('set');
		return parent::getWijzigbareVars($vars);
	}
	/**
	 * @brief Geef de unieke key van het object, zoals die gebruikt wordt in de cache.
	 */
	public function geefID()
	{
		return static::naarID( $this->getCommissieCommissieID()
		                      , $this->getMediaMediaID()
		                      );
	}
	/**
	 * @brief Haal een object op uit de database.
	 * 
	 * geef() kan op verschillende manieren aangeroepen worden:
	 * - alle parameters netjes invullen (dit heeft de voorkeur)
	 * - alleen de eerste parameter, met als vorm: "$aID_$bID"
	 * - alleen de eerste parameter, met als vorm: array($aID, $bID)
	 *
	 * @param mixed $a Object of ID waarop gezocht moet worden.
	 * @param mixed $b Object of ID waarop gezocht moet worden.
	 *
	 * @return CommissieFoto|false
	 * Een CommissieFoto-object als deze gevonden wordt; False anders.
	 */
	static public function geef($a, $b = NULL)
	{
		if(is_null($a)
		&& is_null($b))
			return false;

		if(is_string($a)
		&& is_null($b))
			$a = static::vanID($a); # string -> array

		if(is_array($a)
		&& is_null($b)
		&& count($a) == 2)
		{
			$commissie_commissieID = (int)$a[0];
			$media_mediaID = (int)$a[1];
		}
		else if($a instanceof Commissie
		     && $b instanceof Media)
		{
			$commissie_commissieID = $a->getCommissieID();
			$media_mediaID = $b->getMediaID();
		}
		else if(isset($a)
		     && isset($b))
		{
			$commissie_commissieID = (int)$a;
			$media_mediaID = (int)$b;
		}

		if(is_null($commissie_commissieID)
		|| is_null($media_mediaID))
			throw new BadMethodCallException();

		static::cache(array( array($commissie_commissieID, $media_mediaID) ));
		return Entiteit::geefCache(array($commissie_commissieID, $media_mediaID), 'CommissieFoto');
	}
	/**
	 * @brief Stop meerdere objecten tegelijk in de cache.
	 *
	 * @param array $ids is een ARRAY met per object een array met de ID's. $ids =
	 * array( array(a1ID,a2ID), array(b1ID,b2ID), ...)
	 * @param bool|array $recur recur is een parameter om aan te geven of we een
	 * recursieve invulstap moeten doen.
	 */
	static public function cache($ids, $recur = False)
	{
		global $WSW4DB;
		$cachetm = NULL;

		$ids = Entiteit::nietInCache($ids, 'CommissieFoto');

		// is er nog iets te doen?
		if(empty($ids)) return;

		$cachetm = new ProfilerTimerMark('CommissieFoto::cache( #'.count($ids).' )');
		Profiler::getSingleton()->addTimerMark($cachetm);

		// query alles in 1x
		$res = $WSW4DB->q('TABLE SELECT `CommissieFoto`.`commissie_commissieID`'
		                 .     ', `CommissieFoto`.`media_mediaID`'
		                 .     ', `CommissieFoto`.`status`'
		                 .     ', `CommissieFoto`.`gewijzigdWanneer`'
		                 .     ', `CommissieFoto`.`gewijzigdWie`'
		                 .' FROM `CommissieFoto`'
		                 .' WHERE (`commissie_commissieID`, `media_mediaID`)'
		                 .      ' IN (%A{ii})'
		                 , $ids
		                 );

		$found = array();
		foreach($res as $row)
		{
			$id = static::naarID($row['commissie_commissieID']
			                  ,$row['media_mediaID']);

			$obj = new CommissieFoto(array($row['commissie_commissieID']), array($row['media_mediaID']));

			$obj->inDB = True;

			$obj->status  = strtoupper(trim($row['status']));
			$obj->gewijzigdWanneer = new DateTimeLocale($row['gewijzigdWanneer']);
			$obj->gewijzigdWie = $row['gewijzigdWie'];
			$obj->dirty = false;
			self::stopInCache($id, $obj);

			$obj->errors = array();
			foreach(static::velden('get') as $veld)
				$obj->errors[$veld] = False;

			$found[$id] = True;
		}

		// markeer ook de ids die we niet konden vinden
		foreach($ids as $id)
		{
			if(!isset($found[static::naarID($id)]))
				self::stopInCache($id, False);
		}

		if($cachetm) $cachetm->markEnd();
	}
	/**
	 * @brief Sla het object op in de database.
	 */
	public function opslaan($classname = 'CommissieFoto')
	{
		global $WSW4DB;

		if(!$this->valid()) {
			$errorInfo = "";
			$first = true;
			foreach ($this->getErrors() as $id => $error) {
				if (is_string($error)) {
					if ($first) $first = false;
					else $errorInfo .= ",";
					$errorInfo .= "\n$id: '$error'";
				}
				else if (is_array($error))
					foreach ($error as $lang => $msg)
						if (is_string($msg)) {
							if ($first) $first = false;
							else $errorInfo .= ",";
							$errorInfo .= "\n$id ($lang): '$msg'";
						}
			}
			throw new LogicException('Not all constraints passed: ' . $errorInfo);
		}
		$rel = $this->getCommissie();
		if(!$rel->getInDB())
			throw new LogicException('foreign Commissie is not in DB');
		$this->commissie_commissieID = $rel->getCommissieID();

		$rel = $this->getMedia();
		if(!$rel->getInDB())
			throw new LogicException('foreign Media is not in DB');
		$this->media_mediaID = $rel->getMediaID();

		if (!$this->dirty)
			return;

		$this->gewijzigd();

		if(!$this->inDB) {
			$WSW4DB->q('INSERT INTO `CommissieFoto`'
			          . ' (`commissie_commissieID`, `media_mediaID`, `status`, `gewijzigdWanneer`, `gewijzigdWie`)'
			          . ' VALUES (%i, %i, %s, %s, %i)'
			          , $this->commissie_commissieID
			          , $this->media_mediaID
			          , $this->status
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          );

			if($classname == 'CommissieFoto')
				$this->inDB = True;

			self::stopInCache($this->geefID(), $this);
		} else {
			$WSW4DB->q('UPDATE `CommissieFoto`'
			          .' SET `status` = %s'
			.   ', `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `commissie_commissieID` = %i'
			          .  ' AND `media_mediaID` = %i'
			          , $this->status
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->commissie_commissieID
			          , $this->media_mediaID
			          );
		}
		$this->dirty = false;
	}
	/**
	 * @brief Aanvulling op parent::velden(welke).
	 *
	 * @see veldenCommissieFoto
	 */
	public static function velden($welke = NULL)
	{
		static $veldenCache = array();

		if(!isset($veldenCache[$welke])) {
			$veldenCache[$welke] =
					array_merge( parent::velden($welke)
					           , static::veldenCommissieFoto($welke)
					           );
		}

		return $veldenCache[$welke];
	}
	/**
	 * @brief Geef velden die bij deze klasse horen.
	 *
	 * @see velden
	 *
	 * @param string|null $welke String die een subset van velden beschrijft, of NULL
	 * voor alle velden.
	 *
	 * @return array
	 * Een array met velden.
	 */
	protected static function veldenCommissieFoto($welke = NULL)
	{
		$velden = array();
		switch($welke) {
		case 'set':
			$velden[] = 'Status';
			break;
		case 'lang':
			break;
		case 'lazy':
			break;
		case 'verplicht':
			break;
		case 'viewInfo':
		case 'viewWijzig':
			$velden[] = 'Status';
			break;
		case 'get':
			$velden[] = 'Status';
		case 'primary':
			$velden[] = 'commissie_commissieID';
			$velden[] = 'media_mediaID';
			break;
		case 'verzamelingen':
			break;
		default:
			$velden[] = 'Status';
			break;
		}
		return $velden;
	}
	/**
	 * @brief Verwijder het object.
	 */
	public function verwijderen($foreigncall = NULL)
	{
		global $WSW4DB;

		if(!$this->inDB)
			throw new LogicException('object is not inDB');

		$magVerwijderen = $this->magVerwijderen();
		if(!$magVerwijderen)
		{
			return("Je mag het object niet verwijderen.");
		}

		//Hier begint de mysql-transactie
		if(!$foreigncall)
			$WSW4DB->q('START TRANSACTION');
		$queryarray = array();
		// Als de return-value van het verwijderen van dependencies
		// niet een array is, is het een string met een foutcode

		// uit de DB
		$query = $WSW4DB->q('DELETE FROM `CommissieFoto`'
		          .' WHERE `commissie_commissieID` = %i'
		          .  ' AND `media_mediaID` = %i'
		          .' LIMIT 1'
		          , $this->commissie_commissieID
		          , $this->media_mediaID
		          );

		// Verzamel alle queries in 1 array
		$finalqueries = array();
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($queryarray));
		foreach($it as $singlequery)
		{
			$finalqueries[] = $singlequery;
		}
		$finalqueries[] = $query;

		// Kijk of alle queries gelukt zijn zodat we de transaction kunnen committen naar mysql
		if(!$foreigncall)
		{
			$queriesgelukt = true;
			$output = array();
			foreach($finalqueries as $singlequery)
			{
				$queriesgelukt &= $singlequery;
				if($queriesgelukt == false) {
					$output[] = $singlequery;
					break;
				}
			}
			if($queriesgelukt)
			{
				$WSW4DB->q('COMMIT');
			}
			else
			{
				$WSW4DB->q('ROLLBACK');
				return $output;
			}
		}

		$this->inDB = False;

		// unset IDs
		$this->commissie = NULL;
		$this->commissie_commissieID = NULL;
		$this->media = NULL;
		$this->media_mediaID = NULL;

		if($foreigncall)
			return($finalqueries);
		else
			return NULL;
	}
	/**
	 * @brief Markeer dit object als zijnde gewijzigd.
	 */
	public function gewijzigd($updatedb = False)
	{
		parent::gewijzigd(False);

		if($updatedb)
		{
			global $WSW4DB;

			$WSW4DB->q('UPDATE LOW_PRIORITY `CommissieFoto`'
			          .' SET `gewijzigdWanneer` = %s'
			          .   ', `gewijzigdWie` = %i'
			          .' WHERE `commissie_commissieID` = %i'
			          .  ' AND `media_mediaID` = %i'
			          , $this->gewijzigdWanneer->strftime('%F %T')
			          , $this->gewijzigdWie
			          , $this->commissie_commissieID
			          , $this->media_mediaID
			          );
		}
	}
	/**
	 * @brief Geeft van een veld van CommissieFoto terug wat het type is.
	 *
	 * @param string $field Het veld waarvan het type teruggegeven moet worden.
	 *
	 * @return string|null
	 * Het type als string.
	 */
	static public function getVeldType($field)
	{
		switch(strtolower($field))
		{
		case 'commissie':
			return 'foreign';
		case 'commissie_commissieid':
			return 'int';
		case 'media':
			return 'foreign';
		case 'media_mediaid':
			return 'int';
		case 'status':
			return 'enum';
		default:
			return NULL;
		}
	}
	/**
	 * @brief intern: voor SETTERS
	 * sla wijziging op in de database
	 */
	protected function naarDB($veld, $waarde, $lang)
	{
		global $WSW4DB;

		switch($veld) {
		case 'commissie_commissieID':
		case 'media_mediaID':
			$type = '%i';
			break;
		case 'status':
			$type = '%s';
			break;
		default:
			throw new BadMethodCallException("onbekend veld '$veld'");
		}

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		$WSW4DB->q('UPDATE `CommissieFoto`'
		          ." SET `%l` = $type"
		          .   ', `gewijzigdWanneer` = %s'
		          .   ', `gewijzigdWie` = %i'
		          .' WHERE `commissie_commissieID` = %i'
		          .  ' AND `media_mediaID` = %i'
		          , $veld
		          , $waarde
		          , $this->gewijzigdWanneer->strftime('%F %T')
		          , $this->gewijzigdWie
		          , $this->commissie_commissieID
		          , $this->media_mediaID
		          );
	}
	/**
	 * @brief intern: voor GETTERS
	 * haal een 'LAZY' variabele op uit de database
	 */
	protected function uitDB($veld, $lang)
	{
		global $WSW4DB;

		if(isset($lang))
			$veld .= '_' . strtoupper($lang);

		return $WSW4DB->q('VALUE SELECT `%l`'
		                 .' FROM `CommissieFoto`'
		          .' WHERE `commissie_commissieID` = %i'
		          .  ' AND `media_mediaID` = %i'
		                 , $veld
		          , $this->commissie_commissieID
		          , $this->media_mediaID
		                 );
	}
	static public function stopInCache($id, $obj, $class = NULL, $key = NULL)
	{
		parent::stopInCache($id, $obj, 'CommissieFoto');
	}

	/**
	 * @brief Geef een array klassenaam => verzameling van alle objecten in de DB die
	 * een foreign key naar deze toe hebben.
	 */
	public function getDependencies()
	{
		// Maak een array klassenaam => dependencyverzameling.
		$dependencies = parent::getDependencies();

		return $dependencies;
	}
}
