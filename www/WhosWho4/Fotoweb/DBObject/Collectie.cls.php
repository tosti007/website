<?
/**
 * $Id$
 */
class Collectie
	extends Collectie_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Collectie_Generated

		$this->collectieID = NULL;
	}

	public function magVerwijderen()
	{
		return hasAuth('vicie');
	}

	public function magWijzigen()
	{
		return hasAuth('vicie');
	}

	/**
	 *  Functie die url maakt van de collectie
	 */
	public function url() {
		return '/FotoWeb/Collecties/' . $this->getCollectieID();
	}

	/**
	 *  Functie die url naar de fotos van een collectie maakt
	 */
	public function fotoUrl() {
		return $this->url() . '/Fotos';
	}

	/**
	 *  Functie voor het tellen van het aantal fotos bij een collectie
	 */
	public function getAantalFotos() {
		global $WSW4DB;

		$ids = $WSW4DB->q("VALUE SELECT COUNT(*) "
						. "FROM `Tag` "
						. "WHERE `collectie_collectieID` = %i"
						, $this->getCollectieID());

		return $ids;
	}

	/**
	 *  Functie die de parent, siblings en children van een collectie geeft in een array van drie arrays
	 */
	public function getRelatives() {
		global $WSW4DB;

		$parentarray = array();
		$siblings = array();
		$children = array();

		if($parent = $this->getParentCollectie()) {
			$parentarray = array($parent);
			$siblings = $WSW4DB->q("COLUMN SELECT `collectieID` "
								. "FROM `Collectie` "
								. "WHERE `parentCollectie_collectieID` = %i "
								. "AND `collectieID` != %i"
								, $parent->getCollectieID()
								, $this->getCollectieID());
		}
		
		$children = $WSW4DB->q("COLUMN SELECT `collectieID` "
							. "FROM `Collectie` "
							. "WHERE `parentCollectie_collectieID` = %i"
							, $this->getCollectieID());

		if(sizeof($children) == 0 
				&& sizeof($parentarray) == 0 
				&& sizeof($siblings) == 0)
			return array();
		return array('parent'=>$parentarray
			, 'siblings'=>$siblings
			, 'children'=>$children);
	}

	/**
	 *  Functie die de children van een collectie geeft
	 */
	public function getChildren($actief = false) {
		global $WSW4DB;

		$children = $WSW4DB->q("COLUMN SELECT `collectieID` "
							. "FROM `Collectie` "
							. "WHERE `parentCollectie_collectieID` = %i "
							. ($actief ? "AND `actief` = 1 " : "")
							. "ORDER BY `naam`"
							, $this->getCollectieID());

		return CollectieVerzameling::verzamel($children);
	}

	/**
	 *  Functie die de parent van een collectie geeft (indien aanwezig)
	 */
	public function getParent() {
		global $WSW4DB;

		$parent = $WSW4DB->q("MAYBEVALUE SELECT `parentCollectie_collectieID` "
							."FROM `Collectie` "
							."WHERE `collectieID` = %i"
							, $this->getCollectieID());

		return self::geef($parent);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
