<?
/**
 * $Id$
 */
class Rating
	extends Rating_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // Rating_Generated

		$this->ratingID = NULL;
	}

	public function magVerwijderen()
	{
		return $this->getMedia()->magVerwijderen();
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
