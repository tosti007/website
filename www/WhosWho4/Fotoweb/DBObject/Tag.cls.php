<?
/**
 * $Id$
 */
class Tag
	extends Tag_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct()
	{
		parent::__construct(); // Tag_Generated

		$this->tagID = NULL;
	}

	/**
	 *  Geeft de tagarea terug als die bestaat
	 */
	public function getTagArea() {
		global $WSW4DB;

		$id = $WSW4DB->q("MAYBEVALUE SELECT `tag_tagID` "
						. "FROM `TagArea` "
						. "WHERE `tag_tagID`=%i"
						, $this->getTagId());

		if(!$id)
			return false;
		return TagArea::geef($this);
	}

	public function magVerwijderen()
	{
		return hasAuth('ingelogd');
	}

	public function magWijzigen()
	{
		return hasAuth('bestuur');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
