<?
class ProfielFoto
	extends ProfielFoto_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($a, $b)
	{
		parent::__construct($a, $b); // ProfielFoto_Generated
	}

	static public function zoekHuidig(Persoon $pers)
	{
		return ProfielFotoQuery::Table()
			->whereProp('Persoon', $pers)
			->whereProp('status', 'HUIDIG')
			->geef();
	}

	static public function zoekNieuw(Persoon $pers)
	{
		return ProfielFotoQuery::Table()
			->whereProp('Persoon', $pers)
			->whereProp('status', 'NIEUW')
			->geef();
	}

	static public function zoekBijMediaEnPersoon(Persoon $pers, Media $media)
	{
		return ProfielFotoQuery::Table()
			->whereProp('Persoon', $pers)
			->whereInt('media_mediaID', $media->geefID())
			->geef();
	}

	static public function zoekHuidigBijMedia(Media $media)
	{
		return ProfielFotoQuery::Table()
			->whereInt('media_mediaID', $media->geefID())
			->whereProp('status', 'HUIDIG')
			->geef();
	}

	static public function getFirstNieuw()
	{
		return ProfielFotoQuery::Table()
			->whereProp('status', 'NIEUW')
			->limit(1)
			->geef();
	}

	public function magVerwijderen()
	{
		if(hasAuth('bestuur') || $this->getPersoon() == Persoon::getIngelogd()) {
			return True;
		}

		return False;
	}

	/**
	 * @brief geeft de url naar de profielfoto en anders de penguin
	 * @param profielFoto de profielfoto van een persoon of NULL
	 * @param fotoGrootte de grootte van de foto ('Thumbnail', 'Groot', etc.)
	 */
	public static function fotoURL($profielFoto, $fotoGrootte)
	{
		if ($profielFoto)
			return $profielFoto->getMedia()->url() . '/' . $fotoGrootte;
		return Media::penguinFoto(false);
	}
}
