<?php

use Intervention\Image\ImageManagerStatic;

/**
 * $Id$
 */
class Media extends Media_Generated
{
	/*** CONSTRUCTOR ***/
	public function __construct($act = null, $uploader = null)
	{
		parent::__construct($act, $uploader); // Media_Generated
	}

	/**
	 *  getActiviteit uitgebreid omdat foto's zonder activiteit erbij te kunnen weergeven
	 */
	public function getActiviteit()
	{
		$act = parent::getActiviteit();
		if (!$act)
		{
			$act = new ActiviteitInformatie();
		}
		return $act;
	}

	/**
	 *  Functie die een 'view' van een foto update
	 */
	public function updateViews()
	{
		global $session;

		if (Persoon::getIngelogd())
		{
			// Bekeken fotos worden als sessievariabele opgeslagen als een array
			// van 20 elementen, het is dus een roterende cache
			if ($session->has('fotoviews/' . $this->geefID()))
			{
				return $this;
			}
			$this->setViews($this->getViews() + 1);
			$this->opslaan();
			$session->set('fotoviews/' . $this->geefID(), $this->geefID());
			while (sizeof($session->get('fotoviews')) > 100)
			{
				$fotoviews_array = $session->get('fotoviews');
				array_shift($fotoviews_array);
				$session->set('fotoviews', $fotoviews_array);
			}
		}

		return $this;
	}

	public function setBreedteEnHoogte()
	{
		$loc = $this->geefLoc('Origineel', true);

		if ($this->getSoort() == 'FOTO')
		{
			$this->setBreedte(ImageManagerStatic::make($loc)->width());
			$this->setHoogte(ImageManagerStatic::make($loc)->height());
		}
		else
		{
			$this->setBreedte(134);
			$this->setHoogte(100);
		}

		$this->opslaan();
	}

	/**
	 *  Functie die de filestamp van een foto geeft
	 */
	public function getFileStamp($size = 'Origineel')
	{
		global $filesystem;
		return $filesystem->getTimestamp($this->geefLoc($size));
	}

	/**
	 *  Functie die de url naar een foto geeft;
	 */
	public function url()
	{
		return '/FotoWeb/Media/' . $this->geefID();
	}

	/**
	 *  Functie die de link geeft naar een foto in zijn activiteitenoverzicht
	 */
	public function fotoLink()
	{
		return HTTPS_ROOT . '/Activiteiten/Fotos/'
			. $this->getActiviteit()->getActiviteitId()
			. '/'
			. $this->getActiviteit()->getTitel()
			. '/'
			. $this->geefID();
	}

	public function magBekijken()
	{
		if ($pf = ProfielFoto::zoekHuidigBijMedia($this))
		{
			$pers = $pf->getPersoon();
			if ($pers->magBekijken())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}

	/**
	 *  Functie die een bool teruggeeft of de user de foto mag wijzigen
	 */
	public function magWijzigen()
	{
		// Mensen mogen hun eigen profielfoto aanpassen/wijzigen
		if (ProfielFoto::zoekBijMediaEnPersoon(Persoon::getIngelogd(), $this))
		{
			return true;
		}

		return hasAuth('vicie') ||
			(Persoon::getIngelogd()
				&& (($this->getFotograaf() == Persoon::getIngelogd()) || ($this->getUploader() == Persoon::getIngelogd())));
	}

	public function magVerwijderen()
	{
		// Mensen mogen hun eigen profielfoto aanpassen/wijzigen
		if (ProfielFoto::zoekBijMediaEnPersoon(Persoon::getIngelogd(), $this))
		{
			return true;
		}

		return hasAuth('vicie') ||
			(Persoon::getIngelogd()
				&& (($this->getFotograaf() == Persoon::getIngelogd()) || ($this->getUploader() == Persoon::getIngelogd())));
	}

	/**
	 *  functie die een anchor terug kan geven van de verschillende grote
	 *          van fotos, ie thumb, medium of groot. We doen hier ook de getFileStamp
	 *          zodat de browser de image 1234?00 als een andere image zit dan 1234?01;
	 *          maw, als we een foto wijzigen laad dan niet de gecached versie in.
	 */
	public function maakLink($size)
	{
		$img = new HtmlImage($this->url() . "/$size", 'Foto', 'foto-groot');
		$img->setLazyLoad();
		$img->setAttribute('id', 'foto_' . $this->geefID());
		$link = $this->url() . '/Origineel?' . $this->getFileStamp();
		$anch = new HtmlAnchor($link, $img);
		$anch->setAttribute('id', '#' . $this->geefID());
		return $anch;
	}

	/**
	 *  Functie die een thumbnailanchor maakt voor in het matrixoverzicht
	 */
	public function maakThumbnail()
	{
		$img = new HtmlImage($this->url() . '/Thumbnail', 'Foto', 'groot thumbnail');
		$img->setLazyLoad();

		if (!is_null($this->getActiviteit()))
		{
			$link = $this->getActiviteit()->fotoUrl();
			$anch = new HtmlAnchor($link, $img);
		}
		else
		{
			$anch = $img;
		}
		return $anch;
	}

	/**
	 *  Functie die een thumbnailanchor maakt voor een profielfoto
	 */
	public function maakProfielThumbnail($pers, $anchor = true, $size = 'Medium', $printbaar = false)
	{
		$img = new HtmlImage($this->url() . "/" . $size, 'Profielfoto van ' . PersoonView::naam($pers));

		if (!$printbaar)
		{
			$img->setLazyLoad();
		}
		$link = $this->url() . '/Origineel/';

		if ($anchor)
		{
			$anch = new HtmlAnchor($link, $img);
			$anch->addClass('thumbnail');
		}
		else
		{
			$anch = $img;
		}
		return $anch;
	}

	//Functie die de locatie van een foto op de server teruggeeft
	public static function geefAbsLoc($format = 'Origineel', $mediaid, $force_debug = false, $force_extension = null)
	{
		global $FW_EXTENSIONS, $filesystem;

		$format = strtolower($format);
		$base = 'cache/fotocache/fotoweb/';

		$lastdigits = substr('00' . $mediaid, -2);

		switch ($format)
		{
			case "thumbnail":
				$basedir = $base . "thumb/$lastdigits";
				break;
			case "medium":
				$basedir = $base . "medium/$lastdigits";
				break;
			case "groot":
				$basedir = $base . "large/$lastdigits";
				break;
			default:
				$basedir = "fotos/Public/fotoweb/$lastdigits";
				break;
		}

		// We weten de directory waarin we moeten zoeken, en we weten een
		// mediaid. Maar wat is het filetype?
		if (!$force_extension)
		{
			$livefile = false;
			foreach ($FW_EXTENSIONS as $fw_extension)
			{
				$tryfile = "$basedir/$mediaid.$fw_extension";
				if ($filesystem->has($tryfile))
				{
					$livefile = $tryfile;
					break;
				}

				if (DEBUG)
				{
					$tryfile = "$basedir/$mediaid.debug.$fw_extension";
					if ($filesystem->has($tryfile))
					{
						$livefile = $tryfile;
						break;
					}
				}
			}
			if (!$livefile)
			{
				return false;
			} // niets gevonden

			// Extensie bepalen
			$extension_exp = explode('.', $livefile);
			$extension = array_pop($extension_exp);
			if (!$extension)
			{
				// Geen extensie?
				user_error("Bestandstype van mediabestand met ID $mediaid kan niet bepaald worden?", E_USER_ERROR);
			}

			$debugfile = "$basedir/$mediaid.debug.$extension";
		}
		else
		{
			//We weten de bestandsextensie door de optie $force_extension
			$livefile = "$basedir/$mediaid.$force_extension";
			$debugfile = "$basedir/$mediaid.debug.$force_extension";
		}

		if (DEBUG && ($force_debug || $filesystem->has($debugfile)))
		{
			// Pad naar speciale debug-editie retourneren
			return $debugfile;
		}
		else
		{
			return $livefile;
		}
	}

	public function geefLoc($formaat = 'Origineel', $full_loc = false)
	{
		return ($full_loc ? FILESYSTEM_PREFIX : '') . Media::geefAbsLoc($formaat, $this->geefID());
	}

	//Functie die de exif-data teruggeeft.
	public function geefExif()
	{
		return ImageManagerStatic::make($this->geefLoc('Origineel', true))->exif();
	}

	/** Geeft IPTC data terug van de originele foto. Zie ook:
	 * www.php.net/getimagesize (parameter "imageinfo") en
	 * www.php.net/iptcparse
	 */
	public function geefIPTC()
	{
		return ImageManagerStatic::make($this->geefLoc('Origineel', true))->iptc();
	}

	//Geeft alle collecties van een foto terug
	public function getCollecties()
	{
		return TagQuery::table()
			->join('Collectie')
			->whereProp('Media', $this)
			->wherePropNotNull('Collectie')
			->verzamel('Collectie');
	}

	//Geef alle tags bij een foto
	public function getTags()
	{
		return TagQuery::table()
			->whereProp('Media', $this)
			->verzamel();
	}

	//Geef alle tags met personen bij een foto
	public function getTagsPersonen()
	{
		return TagQuery::table()
			->join('Persoon', 'Tag.persoon_contactID', 'Persoon.contactID')
			->whereProp('Media', $this)
			->whereNotNull('Tag.persoon_contactID')
			->orderByAsc('voornaam')
			->verzamel();
	}

	//Geef alle tags met commissie bij een foto
	public function getTagsCommissies()
	{
		return TagQuery::table()
			->join('Commissie')
			->whereProp('Media', $this)
			->whereNotNull('Tag.commissie_commissieID')
			->orderByAsc('naam')
			->verzamel();
	}

	//Geef alle tags met mentorgroepen bij een foto
	public function getTagsMentorGroepen()
	{
		return TagQuery::table()
			->join('IntroGroep')
			->whereProp('Media', $this)
			->whereNotNull('Tag.introgroep_groepID')
			->orderByAsc('naam')
			->verzamel();
	}

	//Geef alle tags met collecties bij een foto
	public function getTagsCollecties()
	{
		return TagQuery::table()
			->join('Collectie')
			->whereProp('Media', $this)
			->whereNotNull('Tag.collectie_collectieID')
			->orderByAsc('naam')
			->verzamel();
	}

	//Geeft de tag van een persoon terug bij een foto, mits de tag bestaat
	public function getTagPersoon($persoon)
	{
		return TagQuery::table()
			->whereProp('Media', $this)
			->whereProp('Persoon', $persoon)
			->geef();
	}

	//Geeft de tag van een commissie terug bij een foto, mits de tag bestaat
	public function getTagCommissie($cie)
	{
		return TagQuery::table()
			->whereProp('Media', $this)
			->whereProp('Commissie', $cie)
			->geef();
	}

	//Geeft de tag van een mentorgroep terug bij een foto, mits de tag bestaat
	public function getTagMentorGroep($mg)
	{
		return TagQuery::table()
			->whereProp('Media', $this)
			->whereProp('IntroGroep', $mg)
			->geef();
	}

	//Geeft de tag van een collectie terug bij een foto, mits de tag bestaat
	public function getTagCollectie($col)
	{
		return TagQuery::table()
			->whereProp('Media', $this)
			->whereProp('Collectie', $col)
			->geef();
	}

	//Geeft alle Rating-objecten van een foto terug
	public function getAllRatings()
	{
		return RatingQuery::table()
			->whereProp('Media', $this)
			->verzamel();
	}

	//Functie die teruggeeft hoevaak een foto geratet is.
	public function getRatingCount()
	{
		return RatingQuery::table()
			->whereProp('Media', $this)
			->count();
	}

	//Update de rating die in de media-tabel staat
	public function updateRating()
	{
		global $WSW4DB;

		$value = $WSW4DB->q("MAYBEVALUE SELECT AVG(`rating`) "
			. "FROM `Rating` "
			. "WHERE `media_mediaID` = %i "
			. "GROUP BY `media_mediaID`", $this->geefID());

		if ($value)
		{
			$this->setRating($value * 10);
		}
		else
		{
			$this->setRating(5);
		}

		return $this;
	}

	//Verwijdert de foto van het filesystem, tenzij in debug
	public function verwijderVanFilesystem()
	{
		global $filesystem;

		if ($this->magWijzigen() && !DEBUG && !DEMO)
		{
			$id = $this->geefID();

			$orig = $this->geefLoc('Origineel');
			$groot = $this->geefLoc('Groot');
			$medium = $this->geefLoc('Medium');
			$thumbnail = $this->geefLoc('Thumbnail');

			//je kunt een file natuurlijk alleen verwijderen als hij bestaat.
			$absloc_orig = Media::geefAbsLoc("origineel", $id);
			if ($filesystem->has($absloc_orig))
			{
				if ($orig && $filesystem->has($orig))
				{
					$filesystem->delete($orig);
				}
				if ($groot && $filesystem->has($groot))
				{
					$filesystem->delete($groot);
				}
				if ($medium && $filesystem->has($medium))
				{
					$filesystem->delete($medium);
				}
				if ($thumbnail && $filesystem->has($thumbnail))
				{
					$filesystem->delete($thumbnail);
				}
			}
			else
			{
				Page::addMelding("De foto die je probeerde te verwijderen bestond helemaal niet.", 'waarschuwing');
			}

		}
	}

	public function videoConvert($to = 'mp4')
	{
		$absloc_org = FILESYSTEM_PREFIX . Media::geefAbsLoc("origineel", $this->geefID());
		$absloc_to = FILESYSTEM_PREFIX . Media::geefAbsLoc("groot", $this->geefID(), false, $to);

		$ffmpeg = FFMpeg\FFMpeg::create(array('timeout' => 0));

		$video = $ffmpeg->open($absloc_org);

		$video->filters()
			->resize(new FFMpeg\Coordinate\Dimension(900, 676))
			->synchronize();

		if ($to == 'mp4')
		{
			$video->save(new FFMpeg\Format\Video\X264(), $absloc_to);
		}
		else
		{
			$video->save(new FFMpeg\Format\Video\Ogg(), $absloc_to);
		}
	}

	/**
	 * Creert een cache-versie van het origineel van het opgegeven mediaid in
	 * het gegeven formaat ("groot", "medium", "thumbnail")
	 */
	public function createThumbnail($formaat)
	{
		global $filesystem;

		// kwaliteit van de foto compressie
		$kwaliteit = 85;

		// Hardcoded waardes groottes van bestanden

		if ($formaat == "thumbnail")
		{
			$maxwidth = 134;
			$maxheight = 100;
			$kwaliteit = 80;
		}
		elseif ($formaat == "medium")
		{
			$maxwidth = 450;
			$maxheight = 338;
		}
		elseif ($formaat == "groot")
		{
			$maxwidth = 900;
			$maxheight = 676;
		}
		else
		{
			user_error('Ongeldig formaat opgegeven voor Foto->createCacheFile(...)', E_USER_ERROR);
		}

		if ($this->getSoort() == 'FOTO')
		{
			$mediaid = $this->geefID();
			$originalFile = Media::geefAbsLoc("origineel", $mediaid);

			// getAbsLocation retourneert in dit geval geforceerd een
			// debug-filename (ook al bestaat er geen debugfile) als de site in
			// DEBUG-mode is. Merk op dat er geforceerd wordt gevraagd om
			// bestandsextensie "jpg". Dat zorgt ervoor dat getAbsLocation niet op
			// zoek gaat naar de correcte extensie door de directory listing te
			// bekijken, immers: het zou kunnen zijn dat de thumbnail nog niet
			// bestaat

			// Bug #8019: gebruik PNG in plaats van JPG
			$cacheFile = Media::geefAbsLoc($formaat, $mediaid, DEBUG || DEMO, "png");

			if (!$filesystem->has($originalFile))
			{
				user_error("Kan geen cache ($formaat) maken van mediaid $mediaid, de originele foto bestaat niet", E_USER_ERROR);
			}

			$originalFile = FILESYSTEM_PREFIX . $originalFile;

			$image = ImageManagerStatic::make($originalFile);

			if ($image->height() > $image->width())
			{
				$tmp = $maxwidth;
				$maxwidth = $maxheight;
				$maxheight = $tmp;
				$maxwidth = $image->width() * ($maxheight / $image->height());
			}
			else
			{
				$maxheight = $image->height() * ($maxwidth / $image->width());
			}

			$image->resize($maxwidth, $maxheight)->save(FILESYSTEM_PREFIX . $cacheFile, $kwaliteit);
		}
		else
		{
			if ($formaat == 'groot')
			{
				$this->videoConvert('mp4');
				//$this->videoConvert('ogv');
				return;
			}

			$mediaid = $this->geefID();
			// TODO: dit kan misschien ook met het '.png'-formaat
			$absloc_thumb = Media::geefAbsLoc($formaat, $this->geefID(), DEBUG || DEMO, "jpg");
			$absloc_orig = Media::geefAbsLoc("origineel", $this->geefID());

			if (!$filesystem->has($absloc_orig))
			{
				user_error("Kan geen thumbnail genereren voor mediaid $mediaid, het originele bestand bestaat niet?!", E_USER_ERROR);
			}

			$ffmpeg = FFMpeg\FFMpeg::create();

			$video = $ffmpeg->open(FILESYSTEM_PREFIX . $absloc_orig);

			$frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(0));
			$frame->save('/tmp/image.jpg');

			$img = ImageManagerStatic::make('/tmp/image.jpg');

			$img->resize($maxwidth, $maxheight)
				->save(FILESYSTEM_PREFIX . $absloc_thumb, $kwaliteit);

			unlink('/tmp/image.jpg');
		}
	}

	/**
	 * Stuurt een raw mediafile door, zoals whoswho/fotoweb/MediaFile.cls.php:passthruMediaFile doet/deed.
	 * $formaat kan zijn:
	 * "thumbnail" -- toont alleen thumbnail (content-type: image/jpeg)
	 * "medium" -- toont medium formaat
	 * "groot" - groot formaat, maar niet geresized naar standaardafmetingen (zoals 'medium') en dus zonder witte randen
	 * "medium.flv" -- idem
	 * "origineel" -- toont origineel
	 **/
	public static function passthruMediaFile($formaat, $mediaid, $base64 = false)
	{
		global $filesystem;

		$formaat = strtolower($formaat);
		$exts = explode('.', $formaat);
		if (isset($exts[1]))
		{
			$extension = $exts[1];
		}
		else
		{
			$extension = null;
		}
		$formaat = $exts[0];

		$absloc = self::geefAbsLoc($formaat, $mediaid, false, $extension);
		$absloc_original = self::geefAbsLoc("origineel", $mediaid);

		if (!$absloc_original)
		{
			user_error("Bestand bij mediaid $mediaid bestaat niet op de server!", E_USER_ERROR);
		}

		if (!$absloc || !$filesystem->has($absloc))
		{
			// Thumbnail (of FLV-versie) bestaat niet?
			$mediafile = Media::geef($mediaid);
			if (!$mediafile)
			{
				user_error("Mediaid $mediaid bestaat niet in de database!", E_USER_ERROR);
			}

			if ($formaat == "thumbnail")
			{
				// Thumbnail van foto of film bestaat niet, maar is wel te genereren
				$mediafile->createThumbnail("thumbnail");
				$absloc = Media::geefAbsLoc($formaat, $mediaid, DEBUG || DEMO);
			}
			elseif ($formaat == "medium")
			{
				// Medium thumbnail kunnen we genereren voor mediatype = foto
				$mediafile->createThumbnail("medium");
				$absloc = Media::geefAbsLoc($formaat, $mediaid, DEBUG || DEMO);
			}
			elseif ($formaat == "groot")
			{
				// Grote cacheversie (zonder kader) kunnen we genereren voor mediatype = foto
				$mediafile->createThumbnail("groot");
				$absloc = Media::geefAbsLoc($formaat, $mediaid, DEBUG || DEMO);
			}
		}

		$savefilename = "aes2_" . basename($absloc);

		return sendfilefromfs($absloc, $savefilename);
	}

	/**
	 * Draait de foto en slaat deze op. Genereert ook nieuwe bijbehorende cachebestanden. 90 graden
	 * is een slag rechtsom, 270 graden is een slag linksom
	 */
	public function rotate($graden)
	{
		global $filesystem;

		if ($graden != 90 && $graden != 180 && $graden != 270)
		{
			user_error(_("Het aantal graden kan enkel en alleen 90, 180 of 270 zijn!"), E_USER_ERROR);
		}

		$src = Media::geefAbsLoc("origineel", $this->geefID());
		$target = Media::geefAbsLoc("origineel", $this->geefID(), true);

		if ($filesystem->has($src))
		{
			$img = ImageManagerStatic::make(FILESYSTEM_PREFIX . $src);
			$img->rotate($graden)->save(FILESYSTEM_PREFIX . $target);

			$this->generateThumbnails();

			if ($graden == 90 || $graden == 270)
			{
				$width = $this->getBreedte();
				$height = $this->getHoogte();
				$this->setBreedte($height);
				$this->setHoogte($width);
				$this->opslaan();
			}
		}
	}

	public function generateThumbnails($only_delete = false, $extension = null)
	{
		global $filesystem;

		// Forceer debug-files zodat je op de debug geen echte bestanden weggooit
		$absLocMedium = Media::geefAbsLoc('medium', $this->geefID(), DEBUG || DEMO, $extension);
		$absLocGroot = Media::geefAbsLoc('groot', $this->geefID(), DEBUG || DEMO, $extension);
		$absLocThumb = Media::geefAbsLoc('thumbnail', $this->geefID(), DEBUG || DEMO, $extension);

		// Eventueel bestaand cache wegsodemieteren, zou inconsistenties opleveren
		if ($filesystem->has($absLocThumb))
		{
			$filesystem->delete($absLocThumb);
		}

		if ($filesystem->has($absLocGroot))
		{
			$filesystem->delete($absLocGroot);
		}

		if ($filesystem->has($absLocMedium))
		{
			$filesystem->delete($absLocMedium);
		}

		if ($only_delete)
		{
			return;
		}

		$this->createThumbnail("thumbnail");
		$this->createThumbnail("medium");
		$this->createThumbnail("groot");
	}

	public static function uploaden($file, $alleen_foto = false, $act = null)
	{
		global $FW_EXTENSIONS, $FW_EXTENSIONS_FOTO, $filesystem;

		$errorcode = $file['error'];

		$fouten = array();

		if (count($_FILES) == 0 || $errorcode == UPLOAD_ERR_NO_FILE)
		{
			$fouten[] = _('Vergeet niet bestanden te selecteren!');
		}
		else
		{
			if ($errorcode)
			{
				switch ($errorcode)
				{
					case UPLOAD_ERR_INI_SIZE: // groter dan php.ini toestaat
					case UPLOAD_ERR_FORM_SIZE: // groter dan het formulier toestaat
						$fouten[] = _('Het bestand is te groot om geupload te worden.');
						break;
					case UPLOAD_ERR_PARTIAL: // tijdens uploaden onderbroken
					case UPLOAD_ERR_CANT_WRITE: // fout tijdens opslaan op schijf
					case UPLOAD_ERR_EXTENSION: // een extensie heeft zitten falen, maar PHP is niet capabel genoeg te vertellen welke
						$fouten[] = _('Tijdens het uploaden is een fout gebeurd. '
							. 'Probeer het nog eens of vraag de WebCie om hulp.');
						break;
					case UPLOAD_ERR_NO_TMP_DIR: // er is geen temp folder om bestanden in te stoppen
						// dit betekent dat de configuratie van het systeem echt heel erg fout is, dus dat moeten we wel even bekendmaken
						$fouten[] = _('Er is geen plek om bestanden te bewaren. '
							. 'De WebCie wordt ingelicht over deze fout.');
						sendmail('www@a-eskwadraat.nl', 'www@a-eskwadraat.nl'
							, 'Flagrante fout ontdekt: UPLOAD_ERR_NO_TMP_DIR'
							, "Lieve WebCie,\n"
							. 'Zojuist is de foutmelding UPLOAD_ERR_NO_TMP_DIR '
							. 'opgetreden bij het uploaden van een foto. '
							. 'Dit betekent dat er geen temporary file directory '
							. 'is om foto\'s neer te zetten, oftewel dat er dingen '
							. "grondig fout zijn gegaan.\n"
							. 'Met vriendelijke groet, Het Systeem');
						break;
				}
			}
		}

		if (count($fouten) != 0)
		{
			return $fouten;
		}

		$path = $file['tmp_name'];
		$uploadfile = $file['name'];

		$lastdotpos = strrpos($uploadfile, '.');

		if ($lastdotpos === false)
		{
			$extension = false;
		}
		else
		{
			$extension = strtolower(substr($uploadfile, $lastdotpos + 1));
		}

		if (!in_array($extension, $FW_EXTENSIONS) || !$extension)
		{
			return ['Geen geldig bestandstype'];
		}

		if ($alleen_foto && in_array($extension, $FW_EXTENSIONS_FOTO))
		{
			$a = getimagesize($path);
			$image_type = $a[2];

			if (!in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP)))
			{
				return ['Geen geldig bestand'];
			}
		}

		// act kan null zijn, maar daar geeft Media niet om
		$mediafile = new Media($act, Persoon::getIngelogd());

		if (in_array($extension, $FW_EXTENSIONS_FOTO))
		{
			$mediafile->setSoort('FOTO');
		}
		else
		{
			$mediafile->setSoort('FILM');
		}

		$uploadstamp = date('Y-m-d H:i:s', strtotime('now'));
		$mediafile->setGemaakt($uploadstamp); // EXIF-datum wordt later nog ingesteld

		$mediafile->opslaan();

		// GetAbsLocation retourneert in DEBUG-mode een speciale debug-bestandsnaam, bijvoorbeeld:
		//   /srv/fotos/Public/fotoweb/34/1234.debug.jpg
		$targetFile = Media::geefAbsLoc('Origineel', $mediafile->getMediaID(), DEBUG || DEMO, $extension);

		if ($filesystem->has($targetFile))
		{
			// Hmmmm...
			if (DEBUG)
			{
				// In debug-site, en het gekozen mediaid bestaat al in het filesystem.
				// De foto bestaat dus hoogstwaarschijnlijk wel in de live-database,
				// maar de test-database is out-of-date. Foto dus niet uploaden,
				// dat geeft hele vreemde effecten!

				$id = $mediafile->geefID();
				// Info verwijderen uit debug-db
				$mediafile->verwijderen(); // (true: suppress debug error)

				// waarschuwen
				return ["Jo debugger! Je probeert een foto te uploaden die ID $id " .
					"toegewezen heeft gekregen in de debug-database. Echter, een foto " .
					"met dit ID bestaat al in het filesystem, wat waarschijnlijk " .
					"betekent dat je debug-database out-of-date is. Hoe dan ook, deze " .
					"foto wordt niet opgeslagen! ($targetFile)"];
			}
			else
			{
				// In livesite bestaat het mediaid al in het filesystem. Waarschijnlijk
				// is het daar terechtgekomen door een upload-operatie op iemands
				// debug-site. In ieder geval is er geen informatie in de database
				// beschikbaar over deze foto, hij is "orphan", en kan dus sowieso weg.
				$filesystem->delete($targetFile);
			}
		}

		$stream = fopen($path, 'r+');
		$filesystem->writeStream($targetFile, $stream);
		fclose($stream);

		// Delete de eventuele thumbnails die al bestaan
		$mediafile->generateThumbnails(true, $extension);

		if ($mediafile->getSoort() == "FOTO")
		{
			// EXIF-datetime uitlezen
			$exif = $mediafile->geefExif();
			$exifdatetime = @$exif['DateTimeOriginal'];

			if ($exifdatetime && $exifdatetime != "0000-00-00 00:00:00")
			{
				$mediafile->setGemaakt($exifdatetime);
				$mediafile->opslaan();
			}
		}

		// Sla de grootte van de media-file op in de db
		$mediafile->setBreedteEnHoogte();

		return $mediafile;
	}

	/**
	 * @brief geeft de relative url naar de penguin foto (linux symbool)
	 * @param forbidden bool of de werkelijke foto eigenlijk verboden te zien is
	 * en een penguin ervoor in plaats van moet worden gegeven. Anders is het een
	 * normale penguin
	 *
	 * De forbidden penguin heeft een "403" tekst over de foto heen.
	 */
	public static function penguinFoto($forbidden = true)
	{
		$DEFFOTO = 'Penguin.png';
		$FORBIDFOTO = 'Penguin_403.png';

		$foto = $forbidden ? $FORBIDFOTO : $DEFFOTO;
		return '/FotoWeb/' . $foto;
	}
}
