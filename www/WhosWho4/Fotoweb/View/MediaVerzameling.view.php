<?
/**
 * $Id$
 */
abstract class MediaVerzamelingView
	extends MediaVerzamelingView_Generated
{
	/**
	 *  Maakt het geraamte voor een foto-overzichtspagina, de meeste content wordt met javascript en ajax ingeladen
	 *
	 * @param fotos De array van fotos die op de pagina moeten komen
	 * @param titel De titel van de pagina
	 */
	public static function maakPagina($fotos, $titel)
	{
		$page = Page::getInstance()
			->addHeadCss(Page::minifiedFile('foto', 'css'))
			->addFooterJS(Page::minifiedFile('foto', 'js'))
			->start(sprintf(_("Foto's van %s"), $titel), null, null, true);

		$page->add(HtmlInput::makeHidden('fotoarray', json_encode(array_values($fotos)), null, 'fotoarray'));

		// Als we meer dan FOTOBATCH aan foto's hebben willen we de boel paginaten
		// zodat we niet teveel foto's per keer hebben en op die manier js
		// laten crashen
		$fotoAantal = count($fotos);
		if($fotoAantal > FOTOBATCH) {
			$offset = tryPar('offset', 0);

			$offset = max(0, min($offset, $fotoAantal));

			$page->add(new HtmlHeader(4, sprintf(_("Je bekijkt nu foto's %d tot %d.")
				, $offset + 1, min($offset + FOTOBATCH, $fotoAantal))));

			$page->add($div = new HtmlParagraph(null, 'btn-group'));

			$counter = 0;
			while($counter < $fotoAantal) {
				$div->add(HtmlAnchor::button('?offset='.$counter, sprintf(_("Foto's %d - %d")
					, $counter + 1, min($counter + FOTOBATCH, $fotoAantal))));
				$counter += FOTOBATCH;
			}

			$fotos = array_slice($fotos, $offset, FOTOBATCH);
		} else {
			$page->add(new HtmlHeader(4, sprintf(_("Je bekijkt nu %s foto's."), $fotoAantal)));
		}

		$page->add($div = new HtmlDiv(null, null, 'fotoContainer'));

		$div->add($grid = new HtmlDiv(null, 'grid-sizer'));
		$div->add(new HtmlDiv(null, 'grid-item sokkel'));

		foreach($fotos as $foto)
		{
			if(is_null($foto['gemaakt']))
				$tmpDiv = new HtmlDiv(null, 'grid-item', '1970-01-01 -0:00:00');
			else
				$tmpDiv = new HtmlDiv(null, 'grid-item', $foto['gemaakt']);

			$tmpDiv->setCssStyle('height: 0;');

			if($foto['soort'] == 'FILM')
			{
				$tmpDiv->add($img = new HtmlImage("/FotoWeb/Media/$foto[mediaID]/Medium", _('Foto'), $foto['mediaID'], 'foto lazy'));
				$tmpDiv->setAttribute('data-width', '113');
				$tmpDiv->setAttribute('data-height', '84');
				$tmpDiv->add(new HtmlDiv(new HtmlDiv(HtmlSpan::fa('play', _('video'), 'fa-5x'), 'inner'), 'cn'));
			}
			else
			{
				$tmpDiv->add($img = new HtmlImage("/FotoWeb/Media/$foto[mediaID]/Medium", _('Foto'), $foto['mediaID'], 'foto lazy'));
				$tmpDiv->setAttribute('data-width', $foto['breedte']);
				$tmpDiv->setAttribute('data-height', $foto['hoogte']);
			}

			$img->setLazyLoad();
			$img->unsetLowres();
			$img->setId($foto['mediaID']);

			$tmpDiv->add($a = new HtmlAnchor('#', new HtmlDiv(new HtmlParagraph($foto['mediaID']), 'description'), null, 'overlay'));
			$a->setAttribute('data-id', $foto['mediaID']);

			$div->addImmutable($tmpDiv);
		}

		$page->add($pswp = new HtmlDiv(null, 'pswp'));
		$pswp->add(new HtmlDiv(null, 'pswp__bg'));
		$pswp->add($pswp__scroll = new HtmlDiv(null, 'pswp__scroll-wrap'));

		/* Container that holds slides. PhotoSwipe keeps only 3 of them
		 * in the DOM to save memory. Don\'t modify these 3 pswp__item
		 * elements, data is added later on.
		 */
		$pswp__scroll->add($pswp__container = new HtmlDiv(null, 'pswp__container'));
		$pswp__container->add(new HtmlDiv(null, 'pswp__item'));
		$pswp__container->add(new HtmlDiv(null, 'pswp__item'));
		$pswp__container->add(new HtmlDiv(null, 'pswp__item'));

		/* Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. */
		$pswp__scroll->add($pswp__ui = new HtmlDiv(null, 'pswp__ui pswp__ui--hidden'));
		$pswp__ui->add($pswp__topbar = new HtmlDiv(null, 'pswp__top-bar'));

		/*Controls are self-explanatory. Order can be changed. */
		$pswp__topbar->add(new HtmlDiv(null, 'pswp__counter'));
		$pswp__topbar->add('<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>');
		$pswp__topbar->add('<button class="pswp__button pswp__button--share" title="Share"></button>');
		$pswp__topbar->add('<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>');
		$pswp__topbar->add('<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>');
		$pswp__topbar->add('<button class="pswp__button pswp__button--aes" title="A-Eskwadraatfuncties"></button>');

		/* element will get class pswp__preloader--active when preloader is running */
		$pswp__topbar->add($pswp__preloader = new HtmlDiv(null, 'pswp__preloader'));
		$pswp__preloader->add($pswp__preloader__icn = new HtmlDiv(null, 'pswp__preloader__icn'));
		$pswp__preloader__icn->add($pswp__preloader__cut = new HtmlDiv(null, 'pswp__preloader__cut'));
		$pswp__preloader__cut->add(new HtmlDiv(null, 'pswp__preloader__donut'));

		$pswp__ui->add($pswp__sharemodal = new HtmlDiv(null, 'pswp__aes-modal pswp__single-tap pswp__aes-modal--hidden'));
		$pswp__sharemodal->add($pswp__aestooltip = new HtmlDiv(null, 'pswp__aes-tooltip'));

		$pswp__ui->add($pswp__sharemodal = new HtmlDiv(null, 'pswp__share-modal pswp__share-modal--hidden pswp__single-tap'));
		$pswp__sharemodal->add(new HtmlDiv(null, 'pswp__share-tooltip'));

		$pswp__ui->add('<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>');
		$pswp__ui->add('<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>');

		$pswp__ui->add($pswp__caption = new HtmlDiv(null, 'pswp__caption'));
		$pswp__caption->add(new HtmlDiv(null, 'pswp__caption__center'));

		$page->add($div = new HtmlDiv(null, null, 'photodata'));
		$div->setCssStyle('display: none;');

		$page->add($footer = new HtmlDiv(null, 'navbar navbar-inverse', 'foto-footer'));
		$footer->add($footerContainer = new HtmlDiv(null, 'container'));
		$footerContainer->add($navbar = new HtmlDiv(null, 'navbar-header'));
		$navbar->setCssStyle('width: 100%;');

		$navbar->add($scrollable = new HtmlDiv(null, 'dragscroll', 'scrollBar'));
		// We stoppen hier niet het htmltable-element in omdat we deze table
		// zonder standaard klassen willen
		$scrollable->add('<table>');

		$scrollable->add($tr = new HtmlTableRow());
		$cell = $tr->addData(new HtmlDiv(_("Ga direct naar foto "), 'navbar-brand'), 'footer-cell');

		$factor = 40;

		for($i = 0; $i < sizeof($fotos)/$factor; $i++) {
			list($foto) = array_slice($fotos, $i*$factor, 1);
			$linkNr = max(1, $factor * $i);

			$cell = $tr->addData($a = new HtmlAnchor('#' . $foto['mediaID'], $linkNr, 'footer-cell'));
			$a->addClass('navbar-brand');
		}

		$scrollable->add('</table>');

		$page->end();
	}

	/**
	 *  Geeft een fotolijstje voor een activiteit
	 *
	 * @param fotos De fotos die in het lijstje komen
	 */
	static public function fotoLijst($fotos)
	{
		if(sizeof($fotos) == 0)
			return new HtmlDiv();

		$div = new HtmlDiv(null, 'row row-centered');
		foreach($fotos as $foto)
		{
			$foto = Media::geef($foto);
			$div->add($d = new HtmlDiv($foto->maakThumbnail(), 'col-xs-2 col-centered'));
			$d->setCssStyle('text-align: center;');
		}

		return $div;
	}

	/**
	 *  Geeft een fotolijstje voor een cie
	 *
	 * @param fotos De fotos die in het lijstje komen
	 * @param cie De cie waarbij we de fotos willen
	 */
	static public function fotoLijstCie($fotos,$cie)
	{
		if(sizeof($fotos) == 0)
			return new HtmlDiv();

		$div = new HtmlDiv();

		$div->add($rowDiv = new HtmlDiv(null, 'row'));

		$aantal = sizeof($fotos);
		foreach($fotos as $foto) {
			$foto = Media::geef($foto);
			$rowDiv->add(new HtmlDiv($anch = $foto->maakThumbnail(), 'col-xs-' . 12/$aantal));
			$anch->setAttribute('href',$cie->systeemUrl() . '/Fotos');
		}

		return $div;
	}

	/**
	 *  Geeft een fotolijstje voor een mentorgroep
	 *
	 * @param fotos De fotos die in het lijstje komen
	 * @param grp De mentorgroep waarbij we de fotos willen
	 */
	static public function fotoLijstMentorgroep($fotos,$grp)
	{
		if(sizeof($fotos) == 0)
			return new HtmlDiv();

		$div = new HtmlDiv();
		$div->add(new HtmlHeader(4, _("Foto's")));
		$div->add($table = new HtmlTable(null,'fotostrip'));
		$table->add($data = new HtmlTableRow());
		foreach($fotos as $foto) {
			$foto = Media::geef($foto);
			$data->add($cell = new HtmlTableDataCell($anch = $foto->maakThumbnail()));
			$anch->setAttribute('href',$grp->url() . '/Fotos');
		}

		return $div;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
