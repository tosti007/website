<?
/**
 * $Id$
 */
abstract class CollectieView
	extends CollectieView_Generated
{
	/**
	 *  Laat een pagina met alle info van een collectie zien.
	 * 
	 * @param collectie De collectie waarvan je de info wilt zien
	 */
	public static function collectieInfo($collectie)
	{
		global $logger;

		$page = self::pageLinks($collectie);
		$page->start(sprintf(_("Informatie over collection %s")
				, CollectieView::waardeNaam($collectie)));

		$page->add(new HtmlDiv(new HtmlDiv($li = new HtmlList(), 'col-md-6'), 'row'));

		$li->addChild(sprintf('%s: %s'
			, new HtmlStrong(_('Naam'))
			, CollectieView::waardeNaam($collectie)));
		$li->addChild(sprintf('%s: %s'
			, new HtmlStrong(_('Omschrijving'))
			, CollectieView::waardeOmschrijving($collectie)));
		$li->addChild(sprintf('%s: %s'
			, new HtmlStrong(_('Actief'))
			, CollectieView::waardeActief($collectie)));
		$li->addChild(sprintf('%s: %s'
			, new HtmlStrong(_('Aantal foto\'s'))
			, $collectie->getAantalFotos()));

		$page->add(new HtmlHeader(3
			, _("Overzicht van boven- en onderliggende collections")));

		$par = $collectie->getParentCollectie();
		if($par)
			$c = $par;
		else
			$c = $collectie;

		$page->add($table = new HtmlTable());

		$row = $table->addRow();

		if($c == $collectie)
			$row->setAttribute('style','font-weight:bold;');

		$row->addData(new HtmlAnchor($c->url(), CollectieView::waardeNaam($c)));
		$row->addData(CollectieView::waardeOmschrijving($c));
		$row->addData(CollectieView::waardeActief($c));

		$gehad = new CollectieVerzameling();
		$gehad->voegtoe($c);

		$children = $c->getChildren();
		if($children->bevat($c)) {
			$logger->warn($c->getNaam() . " heeft zichzelf als parent!");
			$children->verwijder($c);
		}

		CollectieVerzamelingView::collectionChildren($table,$gehad,$children,1);

		$page->add(HtmlAnchor::button($collectie->url() . '/Fotos'
			, _("Toon foto's in deze collectie")));
		$page->add(new HtmlBreak());
		$page->add(HtmlAnchor::button("/FotoWeb/Collecties"
			, _("Terug naar de lijst met collecties")));

		$page->end();
	}

	/**
	 *  Pagina voor het wijzigen/toevoegen van een collectie
	 *
	 * @param collectie De collectie die gewijzigd moet worden
	 * @param nieuw Bool of we een nieuwe collectie aanmaken of niet
	 * @param show_error Of de errors moeten worden laten zien
	 */
	public static function collectieWijzigen($collectie, $nieuw = false, $show_error = true)
	{
		if($nieuw)
			$page = Page::getInstance()->start('Toevoegen');
		else
			$page = Page::getInstance()->start('Wijzigen');

		if($nieuw)
			$page->add($form = HtmlForm::named('toevoegen'));
		else
			$page->add($form = HtmlForm::named('wijzigen'));

		$form->add($div = new HtmlDiv());

		$div->add(self::wijzigTR($collectie, 'naam', $show_error));
		$div->add(self::wijzigTR($collectie, 'omschrijving', $show_error));
		$div->add(self::wijzigTR($collectie, 'parentCollectie', $show_error));
		$div->add(self::wijzigTR($collectie, 'actief', $show_error));

		$form->add(HtmlInput::makeFormSubmitButton($nieuw ? _('Toevoegen') : _('Wijzig collectie')));

		$page->end();
	}

	public static function processNieuwForm(Collectie $obj)
	{
		parent::processForm($obj);

		$parentId = tryPar('Collectie[parentCollectie]', null);

		if(!is_null($parentId) && Collectie::geef($parentId))
			$obj->setParentCollectie($parentId);
	}

	public static function formParentCollectie(Collectie $obj, $include_id = false)
	{
		$selectarray = array(null => null) + CollectieVerzameling::maakCollectieArray();
		$selected = tryPar('Collectie[parentCollectie]', $obj->getCollectieId());
		return HtmlSelectbox::fromArray('Collectie[parentCollectie]', $selectarray, $selected, 1);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
