<?
abstract class ProfielFotoVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in ProfielFotoVerzamelingView.
	 *
	 * @param ProfielFotoVerzameling $obj Het ProfielFotoVerzameling-object waarvan de
	 * waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeProfielFotoVerzameling(ProfielFotoVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
