<?
abstract class TagAreaView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in TagAreaView.
	 *
	 * @param TagArea $obj Het TagArea-object waarvan de waarde kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeTagArea(TagArea $obj)
	{
		return self::defaultWaarde($obj);
	}

	/**
	 * @brief Geef het label van het veld tag.
	 *
	 * @param TagArea $obj Het TagArea-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld tag labelt.
	 */
	public static function labelTag(TagArea $obj)
	{
		return 'Tag';
	}
	/**
	 * @brief Geef de waarde van het veld tag.
	 *
	 * @param TagArea $obj Het TagArea-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld tag van het object obj
	 * representeert.
	 */
	public static function waardeTag(TagArea $obj)
	{
		//Als het object niet bestaat geven we niets terug
		if(!$obj->getTag())
			return NULL;
		return TagView::defaultWaardeTag($obj->getTag());
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld tag
	 * bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld tag representeert.
	 */
	public static function opmerkingTag()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beginPixelX.
	 *
	 * @param TagArea $obj Het TagArea-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld beginPixelX labelt.
	 */
	public static function labelBeginPixelX(TagArea $obj)
	{
		return 'BeginPixelX';
	}
	/**
	 * @brief Geef de waarde van het veld beginPixelX.
	 *
	 * @param TagArea $obj Het TagArea-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beginPixelX van het object
	 * obj representeert.
	 */
	public static function waardeBeginPixelX(TagArea $obj)
	{
		return static::defaultWaardeInt($obj, 'BeginPixelX');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beginPixelX.
	 *
	 * @see genericFormbeginPixelX
	 *
	 * @param TagArea $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginPixelX staat en kan
	 * worden bewerkt. Indien beginPixelX read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeginPixelX(TagArea $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'BeginPixelX', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beginPixelX. In
	 * tegenstelling tot formbeginPixelX moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeginPixelX
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginPixelX staat en kan
	 * worden bewerkt. Indien beginPixelX read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeginPixelX($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'BeginPixelX');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beginPixelX bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beginPixelX representeert.
	 */
	public static function opmerkingBeginPixelX()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld eindPixelX.
	 *
	 * @param TagArea $obj Het TagArea-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld eindPixelX labelt.
	 */
	public static function labelEindPixelX(TagArea $obj)
	{
		return 'EindPixelX';
	}
	/**
	 * @brief Geef de waarde van het veld eindPixelX.
	 *
	 * @param TagArea $obj Het TagArea-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld eindPixelX van het object obj
	 * representeert.
	 */
	public static function waardeEindPixelX(TagArea $obj)
	{
		return static::defaultWaardeInt($obj, 'EindPixelX');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld eindPixelX.
	 *
	 * @see genericFormeindPixelX
	 *
	 * @param TagArea $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindPixelX staat en kan
	 * worden bewerkt. Indien eindPixelX read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEindPixelX(TagArea $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'EindPixelX', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld eindPixelX. In
	 * tegenstelling tot formeindPixelX moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formeindPixelX
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindPixelX staat en kan
	 * worden bewerkt. Indien eindPixelX read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEindPixelX($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'EindPixelX');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * eindPixelX bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld eindPixelX representeert.
	 */
	public static function opmerkingEindPixelX()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld beginPixelY.
	 *
	 * @param TagArea $obj Het TagArea-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld beginPixelY labelt.
	 */
	public static function labelBeginPixelY(TagArea $obj)
	{
		return 'BeginPixelY';
	}
	/**
	 * @brief Geef de waarde van het veld beginPixelY.
	 *
	 * @param TagArea $obj Het TagArea-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld beginPixelY van het object
	 * obj representeert.
	 */
	public static function waardeBeginPixelY(TagArea $obj)
	{
		return static::defaultWaardeInt($obj, 'BeginPixelY');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld beginPixelY.
	 *
	 * @see genericFormbeginPixelY
	 *
	 * @param TagArea $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginPixelY staat en kan
	 * worden bewerkt. Indien beginPixelY read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formBeginPixelY(TagArea $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'BeginPixelY', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld beginPixelY. In
	 * tegenstelling tot formbeginPixelY moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formbeginPixelY
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld beginPixelY staat en kan
	 * worden bewerkt. Indien beginPixelY read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormBeginPixelY($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'BeginPixelY');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * beginPixelY bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld beginPixelY representeert.
	 */
	public static function opmerkingBeginPixelY()
	{
		return NULL;
	}
	/**
	 * @brief Geef het label van het veld eindPixelY.
	 *
	 * @param TagArea $obj Het TagArea-object waarvoor het veldlabel nodig is.
	 *
	 * @return string
	 * Een string die het veld eindPixelY labelt.
	 */
	public static function labelEindPixelY(TagArea $obj)
	{
		return 'EindPixelY';
	}
	/**
	 * @brief Geef de waarde van het veld eindPixelY.
	 *
	 * @param TagArea $obj Het TagArea-object waarvan de waarde wordt verkregen.
	 *
	 * @return string
	 * Een html-veilige string die de waarde van het veld eindPixelY van het object obj
	 * representeert.
	 */
	public static function waardeEindPixelY(TagArea $obj)
	{
		return static::defaultWaardeInt($obj, 'EindPixelY');
	}
	/**
	 * @brief Maak een specifiek formulieronderdeel voor het veld eindPixelY.
	 *
	 * @see genericFormeindPixelY
	 *
	 * @param TagArea $obj Het object waarvoor een formulieronderdeel nodig is.
	 * @param bool $include_id Indien True wordt de ID van obj meegenomen in de naam
	 * van het formulieronderdeel.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindPixelY staat en kan
	 * worden bewerkt. Indien eindPixelY read-only is betreft het een statisch
	 * html-element.
	 */
	public static function formEindPixelY(TagArea $obj, $include_id = false)
	{
		return static::defaultFormInt($obj, 'EindPixelY', null, null, $include_id);
	}
	/**
	 * @brief Maak een generiek formulieronderdeel voor het veld eindPixelY. In
	 * tegenstelling tot formeindPixelY moeten naam en waarde meegegeven worden, en
	 * worden niet uit het object geladen.
	 *
	 * @see formeindPixelY
	 *
	 * @param string $name De naam van het formulieronderdeel.
	 * @param mixed $waarde De waarde waar het formulieronderdeel mee begint, of NULL
	 * voor default.
	 *
	 * @return HtmlElement|null
	 * Een HtmlElement waarin de huidige waarde van het veld eindPixelY staat en kan
	 * worden bewerkt. Indien eindPixelY read-only is, betreft het een statisch
	 * html-element.
	 */
	public static function genericFormEindPixelY($name, $waarde=NULL)
	{
		return static::genericDefaultFormInt($name, $waarde, 'EindPixelY');
	}
	/**
	 * @brief Geef een opmerking die gebruikers te zien krijgen als ze het veld
	 * eindPixelY bewerken.
	 *
	 * @return string|null
	 * Een string die een opmerking bij het veld eindPixelY representeert.
	 */
	public static function opmerkingEindPixelY()
	{
		return NULL;
	}
}
