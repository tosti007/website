<?
abstract class MediaVerzamelingView_Generated
	extends View
{
	/**
	 * @brief Geeft de defaultWaarde van het object terug. Er wordt aangeraden deze
	 * functie te overschrijven in MediaVerzamelingView.
	 *
	 * @param MediaVerzameling $obj Het MediaVerzameling-object waarvan de waarde
	 * kregen moet worden.
	 *
	 * @return string
	 * Een Html-veilige string die de waarde van het object representeert.
	 */
	public static function defaultWaardeMediaVerzameling(MediaVerzameling $obj)
	{
		return self::defaultWaarde($obj);
	}

}
