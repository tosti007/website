<?
/**
 * $Id$
 */
abstract class CollectieVerzamelingView
	extends CollectieVerzamelingView_Generated
{
	/**
	 *  Maak een collectieselectbox voor de selectiemogelijkheden bij een foto-overzicht
	 *
	 * @param fotos de verzameling van fotos waarvan je een select wilt
	 */
	static public function maakCollectieSelect($fotos) {
		$collecties = array();
		foreach($fotos as $f) {
			if($f instanceof Media) {
				$colls = $f->getCollecties();
				foreach($colls as $c) {
					if(array_key_exists($c->getCollectieId(),$collecties))
						continue;
					$collecties[$c->getCollectieId()] = CollectieView::waardeNaam($c);
				}
			} else {
				if(array_key_exists($f,$collecties) || is_null($f))
					continue;
				$col = Collectie::geef($f);
				$collecties[$f] = CollectieView::waardeNaam($col);
			}
		}

		$selected = tryPar('collecties',array());

		$box = HtmlSelectbox::fromArray('collecties',$collecties,$selected,5);
		$box->setMultiple();
		return $box;
	}

	static public function maakSelectbox()
	{
		$box = new HtmlSelectbox('test');

		$collarray = CollectieVerzameling::maakCollectieArray();
		foreach($collarray as $id => $colla) {
			self::maakSelectRecurse($box, $id, $colla, 0);
		}
		return $box;
	}

	static public function maakSelectRecurse($selectbox, $collid, $collarray, $depth)
	{
		$selectbox->add(new HtmlSelectboxOption($collid, str_repeat("&ndash;", $depth) . $collarray['naam']));
		foreach($collarray['children'] as $id => $colla) {
			$selectbox->add(new HtmlSelectboxOption($id, str_repeat("&ndash;", $depth+1) . $colla['naam']));
			self::maakSelectRecurse($selectbox, $id, $colla, $depth + 1);
		}
	}

	/**
	 *  Maakt de view die de complete lijst met collecties geeft
	 */
	static function collectieLijst(){
		global $logger;

		// Haal alle collecties op
		$collections = CollectieVerzameling::getAllCollecties();

		$page = Page::getInstance()->start('Collecties');
		$page->add(new HtmlHeader(2,_("Alle Collecties in de database")));

		//Maak de tabel aan
		$page->add($table = new HtmlTable());
		$table->setAttribute('style','width: 100%');
		$table->add($row = new HtmlTableRow());
		$row->add(new HtmlTableHeaderCell(_("Naam")));
		$row->add(new HtmlTableHeaderCell(_("Omschrijving")));
		$row->add(new HtmlTableHeaderCell(_("Actief")));

		$gehad = new CollectieVerzameling();
		foreach ($collections as $c){
			if($c->getParentCollectie())
				continue;
			if($gehad->bevat($c))
				continue;
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableDataCell(
				new HtmlAnchor($c->url()
				, CollectieView::waardeNaam($c))));
			$row->add(new HtmlTableDataCell(
				CollectieView::waardeOmschrijving($c)));

			if($c->getActief())
				$row->add(new HtmlTableDataCell('Y'));
			else
				$row->add(new HtmlTableDataCell('N'));

			$gehad->voegtoe($c);

			$children = $c->getChildren();
			if($children->bevat($c)) {
				$logger->warn($c->getNaam() . " heeft zichzelf als parent!");
				$children->verwijder($c);
			}

			// Als de collectie children heeft stop die dan ook in de tabel
			self::collectionChildren($table,$gehad,$children,1);
		}

		if (sizeof($collections) == 0){
			$table->add($row = new HtmlTableRow());
			$row->add(new HtmlTableDataCell(_("Geen collecties gevonden")));
			$row->setAttributes(array('colspan'=>3,'style'=>'align: center'));
		}

		//Alleen actieve leden mogen nieuwe collecties aanmaken
		if(hasAuth('actief')) {
			$page->add(new HtmlBreak());
			$page->add(new HtmlAnchor('Toevoegen',_('Nieuwe collectie aanmaken')));
		}

		$page->end();
	}

	/**
	 *  Functie die alle children van een verzameling ook in de table stopt
	 *
	 * @param table De tabel waar je de children aan wilt toevoegen
	 * @param gehad De verzameling van alle collecties die al in de tabel staan
	 * @param children De children die je toe wilt voegen
	 * @param aantal De hoeveelste childrenverzameling er toegevoegd wordt
	 */
	public static function collectionChildren($table,$gehad,$children,$aantal = 1) {
		global $logger;

		if($children->aantal() == 0)
			return;
		foreach($children as $child) {
			if($gehad->bevat($child))
				continue;
			$table->add($row = new HtmlTableRow());
			$entry = vfsVarEntryNames();
			if(sizeof($entry) != 0 && $entry[0] == $child->getCollectieID())
				$row->setAttribute('style','font-weight:bold;');
			$row->add($cell = new HtmlTableDataCell(
				new HtmlAnchor($child->url(),CollectieView::waardeNaam($child))));
			if($children->last() == $child)
				$cell->setAttribute("style"
					, "background: url(/Layout/Images/Icons/tabletree-dots2.gif) "
					. (18*$aantal) . "px 54% no-repeat; "
					. "padding-left: " . (26+18*($aantal-1)) . "px; "
					. "font-size:11px;");
			else
				$cell->setAttribute("style"
				, "background: url(/Layout/Images/Icons/tabletree-dots.gif) "
				. (18*$aantal) . "px 54% no-repeat; "
				. "padding-left: " . (26+18*($aantal-1)) . "px; "
				. "font-size:11px;");
			$row->add(new HtmlTableDataCell(
				CollectieView::waardeOmschrijving($child)));

			if($child->getActief())
				$row->add(new HtmlTableDataCell('Y'));
			else
				$row->add(new HtmlTableDataCell('N'));

			$gehad->voegtoe($child);

			$children2 = $child->getChildren();
			if($children2->bevat($child)) {
				$logger->warn($child->getNaam() . " heeft zichzelf als parent!");
				$children2->verwijder($child);
			}

			self::collectionChildren($table,$gehad,$children2,$aantal+1);
		}
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
