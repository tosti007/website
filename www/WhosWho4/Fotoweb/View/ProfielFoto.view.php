<?
abstract class ProfielFotoView
	extends ProfielFotoView_Generated
{
	public static function pasfotoAdministratie($foto)
	{
		$page = Page::getInstance();
		$page->start('Pasfotoadministratie');

		if(is_null($foto))
		{
			$page->add(new HtmlDiv(_("Er zijn geen foto's meer die gecontroleerd moeten worden."), 'alert alert-success'));
		}
		else
		{
			$persoon = $foto->getPersoon();
			$fotoURL = ProfielFoto::fotoURL(ProfielFoto::zoekHuidig($persoon), 'Groot');

			$curimg = new HtmlImage($fotoURL, _("oud"));
			$curimg->setLazyLoad();

			$newimg = new HtmlImage($foto->getMedia()->url() . '/Groot', _("nieuw"));
			$newimg->setLazyLoad();

			$page->add(new HtmlHeader(3, sprintf(_('Nieuwe pasfoto van %s[VOC: lid-naam]'), PersoonView::naam($persoon))));

			$page->add($row = new HtmlDiv(null, 'row'));
			$row->add($coloud = new HtmlDiv(null, 'col-xs-6'))
				->add($colnew = new HtmlDiv(null, 'col-xs-6'));

			$coloud->add(new HtmlHeader(4, _('Oude foto')));
			$coloud->add(new HtmlDiv($curimg, 'thumbnail'));

			$colnew->add(new HtmlHeader(4, _('Nieuwe foto')));
			$colnew->add(new HtmlDiv($newimg, 'thumbnail'));

			$page->add($form = HtmlForm::named('accepteer'));

			$form->add($radio = new HtmlDiv(null, 'radio'));
			$radio->add(
				new HtmlLabel('', 
					array(
						HtmlInput::makeRadio('upload', 'Accepteer', true)
						, _('Accepteer de foto')
					)
				)
			);

			$mail = sprintf(_("Beste %s,\n\n"
				. "De pasfoto die je recentelijk hebt ingestuurd voldeed niet aan onze eisen.\n")
				, PersoonView::naam($persoon));

			$form->add(new HtmlDiv(new HtmlDiv(nl2br($mail), 'panel-body'), 'panel panel-default'));

			$form->add($radio = new HtmlDiv(null, 'radio'));
			$radio->add(
				new HtmlLabel('', 
					array(
						HtmlInput::makeRadio('upload', 'Identiteit', false)
						, _("Je staat niet op de foto.")
					)
				)
			);
			$form->add($radio = new HtmlDiv(null, 'radio'));
			$radio->add(
				new HtmlLabel('', 
					array(
						HtmlInput::makeRadio('upload', 'Zichtbaar', false)
						, _("Je bent niet zichtbaar genoeg.")
					)
				)
			);
			$form->add($radio = new HtmlDiv(null, 'radio'));
			$radio->add(
				new HtmlLabel('', 
					array(
						HtmlInput::makeRadio('upload', 'Overig', false)
						, HtmlInput::makeText('reden', _("Geef hier je reden van afwijzen op"), 40)
					)
				)
			);

			$mail = _("Je kan alsnog een goede foto insturen. Maar vooralsnog "
				. "blijven we je oude foto gebruiken.\n\nMet vriendelijke groet,\n\n")
				. SECRNAME . _("\nSecretaris A-Eskwadraat");

			$form->add(new HtmlDiv(new HtmlDiv(nl2br($mail), 'panel-body'), 'panel panel-default'));
			$form->add(HtmlInput::makeHidden('persoonID', $persoon->geefID()));
			$form->add(HtmlInput::makeSubmitButton(_('OK')));
		}

		$page->end();
	}
}
