<?
abstract class CommissieFotoQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de CommissieFotoQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een CommissieFotoQuery wat meteen gebruikt kan worden om methoden
	 * op toe te passen. We maken hier een nieuw CommissieFotoQuery-object aan om er
	 * zeker van te zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return CommissieFotoQuery
	 * Het CommissieFotoQuery-object waarvan meteen de db-table van het
	 * CommissieFoto-object als table geset is.
	 */
	static public function table()
	{
		$query = new CommissieFotoQuery();

		$query->tables('CommissieFoto');

		return $query;
	}

	/**
	 * @brief Maakt een CommissieFotoVerzameling aan van de objecten die geselecteerd
	 * worden door de CommissieFotoQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return CommissieFotoVerzameling
	 * Een CommissieFotoVerzameling verkregen door de query
	 */
	public function verzamel($object = 'CommissieFoto', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een CommissieFoto-iterator aan van de objecten die geselecteerd
	 * worden door de CommissieFotoQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een CommissieFotoVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'CommissieFoto', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een CommissieFoto die geslecteeerd wordt door de CommissieFotoQuery
	 * uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return CommissieFotoVerzameling
	 * Een CommissieFotoVerzameling verkregen door de query.
	 */
	public function geef($object = 'CommissieFoto')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van CommissieFoto.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'commissie_commissieID',
			'media_mediaID',
			'status',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'CommissieFoto';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als CommissieFoto een
	 * extended klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan CommissieFoto een extensie
	 * is. Dit wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in
	 * meerder tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('CommissieFoto'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			),
			'Media' => array(
				'media_mediaID' => 'mediaID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van CommissieFoto.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'CommissieFoto.commissie_commissieID',
			'CommissieFoto.media_mediaID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling))
				user_error('Value moet van type Commissie zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else $value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'media':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Media; })))
					user_error('Alle waarden in de array moeten van type Media zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Media) && !($value instanceof MediaVerzameling))
				user_error('Value moet van type Media zijn', E_USER_ERROR);
			$field = 'media_mediaID';
			if(is_array($value) || $value instanceof MediaVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMediaID();
				$value = $new_value;
			}
			else $value = $value->getMediaID();
			$type = 'int';
			break;
		case 'status':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, CommissieFoto::enumsStatus()))
						user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
				}
			}
			else if(!in_array($value, CommissieFoto::enumsStatus()))
				user_error($value . ' is niet een geldige waarde voor status', E_USER_ERROR);
			$type = 'string';
			$field = 'status';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'CommissieFoto.'.$v;
			}
		} else {
			$field = 'CommissieFoto.'.$field;
		}

		return array($field, $value, $type);
	}

}
