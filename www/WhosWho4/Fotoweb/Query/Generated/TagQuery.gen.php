<?
abstract class TagQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de TagQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een TagQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw TagQuery-object aan om er zeker van te zijn dat
	 * we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return TagQuery
	 * Het TagQuery-object waarvan meteen de db-table van het Tag-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new TagQuery();

		$query->tables('Tag');

		return $query;
	}

	/**
	 * @brief Maakt een TagVerzameling aan van de objecten die geselecteerd worden door
	 * de TagQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Tag', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Tag-iterator aan van de objecten die geselecteerd worden door
	 * de TagQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een TagVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Tag', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Tag die geslecteeerd wordt door de TagQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return TagVerzameling
	 * Een TagVerzameling verkregen door de query.
	 */
	public function geef($object = 'Tag')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Tag.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'tagID',
			'media_mediaID',
			'persoon_contactID',
			'commissie_commissieID',
			'introgroep_groepID',
			'collectie_collectieID',
			'tagger_contactID',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Tag';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Tag een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Tag een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Tag'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Media' => array(
				'media_mediaID' => 'mediaID'
			),
			'Persoon' => array(
				'persoon' => array(
					'persoon_contactID' => 'contactID'
				),
				'tagger' => array(
					'tagger_contactID' => 'contactID'
				)
			),
			'Commissie' => array(
				'commissie_commissieID' => 'commissieID'
			),
			'IntroGroep' => array(
				'introgroep_groepID' => 'groepID'
			),
			'Collectie' => array(
				'collectie_collectieID' => 'collectieID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Tag.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Tag.tagID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'tagid':
			$type = 'int';
			$field = 'tagID';
			break;
		case 'media':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Media; })))
					user_error('Alle waarden in de array moeten van type Media zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Media) && !($value instanceof MediaVerzameling))
				user_error('Value moet van type Media zijn', E_USER_ERROR);
			$field = 'media_mediaID';
			if(is_array($value) || $value instanceof MediaVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMediaID();
				$value = $new_value;
			}
			else $value = $value->getMediaID();
			$type = 'int';
			break;
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling) && !is_null($value))
				user_error('Value moet van type Persoon of NULL zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'commissie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Commissie; })))
					user_error('Alle waarden in de array moeten van type Commissie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Commissie) && !($value instanceof CommissieVerzameling) && !is_null($value))
				user_error('Value moet van type Commissie of NULL zijn', E_USER_ERROR);
			$field = 'commissie_commissieID';
			if(is_array($value) || $value instanceof CommissieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCommissieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCommissieID();
			$type = 'int';
			break;
		case 'introgroep':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof IntroGroep; })))
					user_error('Alle waarden in de array moeten van type IntroGroep zijn', E_USER_ERROR);
			}
			else if(!($value instanceof IntroGroep) && !($value instanceof IntroGroepVerzameling) && !is_null($value))
				user_error('Value moet van type IntroGroep of NULL zijn', E_USER_ERROR);
			$field = 'introgroep_groepID';
			if(is_array($value) || $value instanceof IntroGroepVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getGroepID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getGroepID();
			$type = 'int';
			break;
		case 'collectie':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Collectie; })))
					user_error('Alle waarden in de array moeten van type Collectie zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Collectie) && !($value instanceof CollectieVerzameling) && !is_null($value))
				user_error('Value moet van type Collectie of NULL zijn', E_USER_ERROR);
			$field = 'collectie_collectieID';
			if(is_array($value) || $value instanceof CollectieVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getCollectieID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getCollectieID();
			$type = 'int';
			break;
		case 'tagger':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'tagger_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Tag.'.$v;
			}
		} else {
			$field = 'Tag.'.$field;
		}

		return array($field, $value, $type);
	}

}
