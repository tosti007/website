<?
abstract class MediaQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de MediaQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een MediaQuery wat meteen gebruikt kan worden om methoden op toe te
	 * passen. We maken hier een nieuw MediaQuery-object aan om er zeker van te zijn
	 * dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return MediaQuery
	 * Het MediaQuery-object waarvan meteen de db-table van het Media-object als table
	 * geset is.
	 */
	static public function table()
	{
		$query = new MediaQuery();

		$query->tables('Media');

		return $query;
	}

	/**
	 * @brief Maakt een MediaVerzameling aan van de objecten die geselecteerd worden
	 * door de MediaQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Media', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Media-iterator aan van de objecten die geselecteerd worden door
	 * de MediaQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een MediaVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Media', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Media die geslecteeerd wordt door de MediaQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return MediaVerzameling
	 * Een MediaVerzameling verkregen door de query.
	 */
	public function geef($object = 'Media')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Media.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'mediaID',
			'activiteit_activiteitID',
			'uploader_contactID',
			'fotograaf_contactID',
			'soort',
			'gemaakt',
			'licence',
			'premium',
			'views',
			'lock',
			'rating',
			'omschrijving',
			'breedte',
			'hoogte',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Media';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Media een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Media een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Media'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Activiteit' => array(
				'activiteit_activiteitID' => 'activiteitID'
			),
			'Persoon' => array(
				'uploader' => array(
					'uploader_contactID' => 'contactID'
				),
				'fotograaf' => array(
					'fotograaf_contactID' => 'contactID'
				)
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Media.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Media.mediaID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'mediaid':
			$type = 'int';
			$field = 'mediaID';
			break;
		case 'activiteit':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Activiteit; })))
					user_error('Alle waarden in de array moeten van type Activiteit zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Activiteit) && !($value instanceof ActiviteitVerzameling) && !is_null($value))
				user_error('Value moet van type Activiteit of NULL zijn', E_USER_ERROR);
			$field = 'activiteit_activiteitID';
			if(is_array($value) || $value instanceof ActiviteitVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getActiviteitID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getActiviteitID();
			$type = 'int';
			break;
		case 'uploader':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling) && !is_null($value))
				user_error('Value moet van type Persoon of NULL zijn', E_USER_ERROR);
			$field = 'uploader_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'fotograaf':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling) && !is_null($value))
				user_error('Value moet van type Persoon of NULL zijn', E_USER_ERROR);
			$field = 'fotograaf_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else if(!is_null($value))
				$value = $value->getContactID();
			$type = 'int';
			break;
		case 'soort':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Media::enumsSoort()))
						user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Media::enumsSoort()))
				user_error($value . ' is niet een geldige waarde voor soort', E_USER_ERROR);
			$type = 'string';
			$field = 'soort';
			break;
		case 'gemaakt':
			if(is_array($value))
			{
				foreach($value as $k => $v)
				{
					if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value))
						user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie', E_USER_ERROR);
				}
			}
			else if(!($value instanceof DateTimeLocale) && !QueryBuilder::sqlDateFunctionCheck($value) && !is_null($value))
				user_error($value . ' moet een DateTimeLocale zijn of een correcte sql-date functie of NULL', E_USER_ERROR);
			if($value instanceof DateTimeLocale)
			{
				$type = 'string';
				$value = $value->strftime('%F %T');
			}
			else if(is_array($value))
			{
				$type = 'string';
				foreach($value as $k => $v)
					$value[$k] = $v->strftime('%F %T');
			}
			else
			{
				$type = 'function';
				$value = QueryBuilder::sqlDateFunctionCheck($value);
			}
			$field = 'gemaakt';
			break;
		case 'licence':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Media::enumsLicence()))
						user_error($value . ' is niet een geldige waarde voor licence', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Media::enumsLicence()))
				user_error($value . ' is niet een geldige waarde voor licence', E_USER_ERROR);
			$type = 'string';
			$field = 'licence';
			break;
		case 'premium':
			$type = 'int';
			$field = 'premium';
			break;
		case 'views':
			$type = 'int';
			$field = 'views';
			break;
		case 'lock':
			$type = 'int';
			$field = 'lock';
			break;
		case 'rating':
			$type = 'int';
			$field = 'rating';
			break;
		case 'omschrijving':
			$type = 'string';
			$field = 'omschrijving';
			break;
		case 'breedte':
			$type = 'int';
			$field = 'breedte';
			break;
		case 'hoogte':
			$type = 'int';
			$field = 'hoogte';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Media.'.$v;
			}
		} else {
			$field = 'Media.'.$field;
		}

		return array($field, $value, $type);
	}

}
