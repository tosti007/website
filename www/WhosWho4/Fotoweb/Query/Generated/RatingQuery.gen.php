<?
abstract class RatingQuery_Generated
	extends Query
{
	/**
	 * @brief De constructor van de RatingQuery_Generated-klasse.
	 */
	public function __construct()
	{
		parent::__construct(); // Query
	}
	/**
	 * @brief Maakt een RatingQuery wat meteen gebruikt kan worden om methoden op toe
	 * te passen. We maken hier een nieuw RatingQuery-object aan om er zeker van te
	 * zijn dat we alle eigenschappen daarvan ook meenemen.
	 *
	 * @return RatingQuery
	 * Het RatingQuery-object waarvan meteen de db-table van het Rating-object als
	 * table geset is.
	 */
	static public function table()
	{
		$query = new RatingQuery();

		$query->tables('Rating');

		return $query;
	}

	/**
	 * @brief Maakt een RatingVerzameling aan van de objecten die geselecteerd worden
	 * door de RatingQuery uit te voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 * @param bool $distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return RatingVerzameling
	 * Een RatingVerzameling verkregen door de query
	 */
	public function verzamel($object = 'Rating', $distinct = false)
	{
		return parent::verzamel($object, $distinct);
	}

	/**
	 * @brief Maakt een Rating-iterator aan van de objecten die geselecteerd worden
	 * door de RatingQuery uit te voeren met een SQL cursor.
	 *
	 * @param object De naam van het object om te verzamelen.
	 * @param distinct Een bool of alle elementen distinct moeten zijn.
	 *
	 * @return Een RatingVerzameling verkregen door de query
	 */
	public function verzamelIter($object = 'Rating', $distinct = false)
	{
		return parent::verzamelIter($object, $distinct);
	}

	/**
	 * @brief Geeft een Rating die geslecteeerd wordt door de RatingQuery uit te
	 * voeren.
	 *
	 * @param string $object De naam van het object om te verzamelen.
	 *
	 * @return RatingVerzameling
	 * Een RatingVerzameling verkregen door de query.
	 */
	public function geef($object = 'Rating')
	{
		return parent::geef($object);
	}

	/**
	 * @brief Checkt of een kolom wel voorkomt in de db-table van Rating.
	 *
	 * @param string $field De kolom om te checken.
	 *
	 * @return bool
	 * Een bool of de kolom wel voorkomt.
	 */
	static public function checkField($field)
	{
		$varArray = array(
			'media_mediaID',
			'persoon_contactID',
			'rating',
			'gewijzigdWanneer',
			'gewijzigdWie'
		);

		if(in_array($field, $varArray))
			return 'Rating';
		if($ret = parent::checkField($field))
			return $ret;
		return False;
	}

	/**
	 * @brief Haalt de sub-klasse op met de keys hoe te joinen als Rating een extended
	 * klasse is.
	 *
	 * @return array|false
	 * Een array met de tabel om automatisch te joinen.
	 */
	static public function automaticJoin()
	{
		$autoJoins = False;
		return $autoJoins;
	}

	/**
	 * @brief Geeft een array terug met de klassen waarvan Rating een extensie is. Dit
	 * wordt gebruikt om te kijken of een kolom gebruikt mag worden als het in meerder
	 * tabellen voor komt.
	 *
	 * @return array
	 * Een array met tabelnamen.
	 */
	static public function getEquivalentTables()
	{
		return array_merge(parent::getEquivalentTables(), array('Rating'));
	}

	/**
	 * @brief Geeft de velden die nodig zijn om met een ander object te joinen.
	 *
	 * @param string $table De table-naam om op te joinen.
	 *
	 * @return array|false
	 * False als er niet gejoined mag worden met de opgegeven table, anders wordt er
	 * een array teruggegeven met als key de tablenaam om op te joinen en als value een
	 * array met als key-value pairs de velden om mee te joinen.
	 */
	static public function getJoinKeys($table)
	{
		$tableArray = array(
			'Media' => array(
				'media_mediaID' => 'mediaID'
			),
			'Persoon' => array(
				'persoon_contactID' => 'contactID'
			)
		);

		if(array_key_exists($table, $tableArray))
			return $tableArray[$table];

		return False;
	}

	/**
	 * @brief Geeft alle primary keys van Rating.
	 *
	 * @return array
	 * Een array met alle primary keys.
	 */
	static public function getPrimaries()
	{
		return array(
			'Rating.media_mediaID',
			'Rating.persoon_contactID'
		);
	}

	/**
	 * @brief Checkt of een bepaalde property in een whereProp gebruikt kan worden en
	 * geeft meteen de fields, types en values voor de where terug.
	 *
	 * @param string $prop De property om te checken.
	 * @param mixed $value De waarde waarmee de prop vergeleken moet worden.
	 *
	 * @return array
	 * Een array met de fields, values en types in die volgorde.
	 */
	static public function checkProp($prop, $value = NULL)
	{
		switch(strtolower($prop))
		{
		case 'media':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Media; })))
					user_error('Alle waarden in de array moeten van type Media zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Media) && !($value instanceof MediaVerzameling))
				user_error('Value moet van type Media zijn', E_USER_ERROR);
			$field = 'media_mediaID';
			if(is_array($value) || $value instanceof MediaVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getMediaID();
				$value = $new_value;
			}
			else $value = $value->getMediaID();
			$type = 'int';
			break;
		case 'persoon':
			if(is_array($value))
			{
				if(count($value) != count(array_filter($value, function($x) { return $x instanceof Persoon; })))
					user_error('Alle waarden in de array moeten van type Persoon zijn', E_USER_ERROR);
			}
			else if(!($value instanceof Persoon) && !($value instanceof PersoonVerzameling))
				user_error('Value moet van type Persoon zijn', E_USER_ERROR);
			$field = 'persoon_contactID';
			if(is_array($value) || $value instanceof PersoonVerzameling)
			{
				$new_value = array();
				foreach($value as $v)
					$new_value[] = $v->getContactID();
				$value = $new_value;
			}
			else $value = $value->getContactID();
			$type = 'int';
			break;
		case 'rating':
			if(is_array($value))
			{
				foreach($value as $v)
				{
					if(!in_array($v, Rating::enumsRating()))
						user_error($value . ' is niet een geldige waarde voor rating', E_USER_ERROR);
				}
			}
			else if(!in_array($value, Rating::enumsRating()))
				user_error($value . ' is niet een geldige waarde voor rating', E_USER_ERROR);
			$type = 'string';
			$field = 'rating';
			break;
		default:
			return parent::checkProp($prop, $value);
			break;
		}

		if(is_array($field)) {
			foreach($field as $k => $v) {
				$field[$k] = 'Rating.'.$v;
			}
		} else {
			$field = 'Rating.'.$field;
		}

		return array($field, $value, $type);
	}

}
