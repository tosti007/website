<?php

$producten = ColaProductVerzameling::getActieveColaProducten();
$productenSorted = ColaProductVerzameling::getActieveColaProducten()->sorteer('volgorde');

?>

// Utility variables
var el = [];
var opendiv = "";
var n = 1;
var time = 3000;
var fout_timeout;
var stat_timeout = 0;
var user_timeout = 0;
var refr_timeout = 0;
var barcode;

// Constants
var USER_MAX_TIME_MS =  30000;
var FOUT_MAX_TIME_MS =   5000;
var STAT_MAX_TIME_MS =  10000;
var REFR_MAX_TIME_MS = 300000;
var WINKEL_FOUT_MAX_TIME_MS = 1000;
var EL_NAMES = [ "fout", "startscherm", "winkel", "confirmatie", "status", 
	"startscherm_rfid", "fout_melding", "dummy_button", "winkel_lidkudos",
	"winkel_selkudos", "winkel_overkudos", "conf_tbody", "conf_lidkudos",
	"conf_eindkudos", "conf_total", "status_msg", "winkel_msg", "wait",
    "winkel_titel",	"barcode_winkel", "popup" ];

// User variables
var lidnr;
var naam;
var pasID;
var productID;
var lid_kudos;
var sel_kudos;
<?php foreach ($producten as $product) { ?>
var num_<?php echo $product->getArtikelID(); ?>;
var kudos_<?php echo $product->getArtikelID(); ?> = <?php echo $product->getKudos(); ?>;
<?php } ?>

// Event voor het browser side reloaden
var cancelReloadEvent = new CustomEvent('cancelReload');

var winkelMsgTimeouts = [];

function setWinkelMsg(msg)
{
    var g_el = el;

    while (winkelMsgTimeouts.length > 0) {
        clearTimeout(winkelMsgTimeouts.pop());
    }

    if (msg) {
        g_el["winkel_msg"].innerHTML = msg;
        g_el["winkel_msg"].setAttribute('class', 'has_error');
        winkelMsgTimeouts.push(setTimeout(function() {
            setWinkelMsg();
        }, WINKEL_FOUT_MAX_TIME_MS));
    } else {
        g_el["winkel_msg"].innerHTML = '';
        g_el["winkel_msg"].setAttribute('class', '');
    }
}

function init ()
{
	var g_document = document;
	
	// Generate array of elements
	var cnt_el = EL_NAMES.length;
	while (--cnt_el	>= 0)
	{
		var name = EL_NAMES[cnt_el];
		el[name] = g_document.getElementById(name);
	}
	<?php foreach ($producten as $product) { ?>
	el["winkel_<?php echo $product->getArtikelID() ?>"] =
		g_document.getElementById("winkel_<?php echo $product->getArtikelID() ?>");
	<?php } ?>

	// Prevent text selection
    // odles 
	g_document.onselectstart = function() {return false;} // ie
	g_document.onmousedown = function() {return false;} // mozilla

	// Open the starting screen
	openDiv("startscherm");
	// Focus in 1500 ms
	setTimeout(function() {
        el["startscherm_rfid"].focus();
    }, 1500);

	setTimeout(function() {
		window.dispatchEvent(cancelReloadEvent);
	}, 2000);
	return false;
}

function openDiv (welke)
{
	var g_el = el, g_opendiv = opendiv;

    setWinkelMsg();

	revertRefrTimeout();
	if (welke == "startscherm")
	{
		loguit();
		removeKitten();
		var box = g_el["startscherm_rfid"];
		resetRefrTimeout();
		box.value = "";
		box.disabled = false;
		box.focus();
	}

	setWaitingIcon(false);

	if (g_opendiv == welke)
		return;

	if (g_opendiv != "")
		g_el[g_opendiv].style.visibility = "hidden"; 
	g_el[welke].style.visibility = "visible"; 

	opendiv = welke;

    if (welke == "startscherm") g_el["startscherm_rfid"].focus();
}

function handleFout (str)
{
	el["fout_melding"].innerHTML = str;
	openDiv("fout");
	fout_timeout = setTimeout("openDiv('startscherm');", FOUT_MAX_TIME_MS);
	return false;
}

function revertFout ()
{
	clearTimeout(fout_timeout);
	openDiv("startscherm");
	return false;
}

function resetFocus ()
{
	if (opendiv == "startscherm")
	{
		var box = el["startscherm_rfid"];
		box.value = "";
		box.focus();
	}
}

function handleStartscherm (form)
{
	var g_el = el;

	g_el["dummy_button"].focus();
	var rfid = g_el["startscherm_rfid"].value;
	if (opendiv == "startscherm")
	{
		checkRfid(rfid);
	}
	return false;
}

function setNederlands ()
{
	window.open('/Leden/Dibs/DibsTablet/ToonTablet?setlanguage=nl',"_self");
	return false;
}

function setEngels ()
{
	window.open('/Leden/Dibs/DibsTablet/ToonTablet?setlanguage=en',"_self");
	return false;
}

function initWinkel ()
{
	var g_document = document, g_el = el;

<?php foreach ($producten as $product) { ?>
	num_<?php echo $product->getArtikelID(); ?> = 0;
	g_el["winkel_<?php echo $product->getArtikelID(); ?>"].innerHTML = 0;
	$(g_el["winkel_<?php echo $product->getArtikelID(); ?>"]).next().css('display', 'none');
<?php } ?>

	g_el["winkel_titel"].innerHTML = "<?php echo _('Welkom')?>" + " " + naam + "!";
	g_el["winkel_lidkudos"].innerHTML = lid_kudos;
	g_el["winkel_selkudos"].innerHTML = sel_kudos;
	g_el["winkel_overkudos"].innerHTML = lid_kudos;

	$('.verderWinkel').hide();

	openDiv("winkel");
	resetUserTimeout();
}

function addWinkel (x)
{
	// De kittens werken door middel van locatie van het item, dus doe daarom
	// dit ingewikkelde proces om erachter te komen of het juiste item getikt is
	var counter = 0;
	var chosen = 0;
<?php foreach ($productenSorted as $product) { ?>
	if(x == <?php echo $product->geefID(); ?>) {
		chosen = counter;
	}
	counter++;
<?php } ?>

	kittenCounter(chosen);

	resetUserTimeout();
	var g_el = el;

	var new_total_kudos = sel_kudos + eval("kudos_"+x);
	if (new_total_kudos > lid_kudos)
	{
		setWinkelMsg("<?php echo _('Je hebt niet genoeg kudos')?>");
		return false;
	}
	<?php if (DIBS_MAX_TRANSACTIE_KUDOS > 0) { ?>
	if (new_total_kudos > <?php echo DIBS_MAX_TRANSACTIE_KUDOS; ?>)
	{
        setWinkelMsg("<?php echo _('De transactie wordt te groot')?>");
		return false;
	}
	<?php } ?>
    setWinkelMsg();

	var new_num = eval("++num_"+x);
	sel_kudos = new_total_kudos;

	g_el["winkel_"+x].innerHTML = new_num;
	$(g_el["winkel_"+x]).next().css('display', new_num > 0 ? 'block' : 'none');
	
	if (sel_kudos > 0)
		$('.verderWinkel').show();
	else
		$('.verderWinkel').hide();

	g_el["winkel_selkudos"].innerHTML = new_total_kudos;
	g_el["winkel_overkudos"].innerHTML = lid_kudos - new_total_kudos;

	return false;
}

function rmWinkel (x)
{
	resetUserTimeout();
	var g_el = el;

	var old_num = eval("num_"+x);
	if (old_num == 0)
		return false;

	var new_total_kudos = sel_kudos - eval("kudos_"+x);
	var new_num = old_num - 1;
	eval("num_"+x+" = new_num");
	sel_kudos = new_total_kudos;

	g_el["winkel_"+x].innerHTML = new_num;
	$(g_el["winkel_"+x]).next().css('display', new_num > 0 ? 'block' : 'none');

	if (sel_kudos > 0)
		$('.verderWinkel').show();
	else
		$('.verderWinkel').hide();

	g_el["winkel_selkudos"].innerHTML = sel_kudos;
	g_el["winkel_overkudos"].innerHTML = lid_kudos - new_total_kudos;
	setWinkelMsg();
	
	return false;
}

function revertWinkel ()
{
	revertUserTimeout();
	openDiv("startscherm");
	return false;
}

function handleWinkel ()
{
	resetUserTimeout();
	var g_el = el, g_sel_kudos = sel_kudos;
	if (g_sel_kudos == 0)
	{
		setWinkelMsg("<?php echo _('Je hebt niets geselecteerd')?>");
		return false;
	}

	var tbl = new StringBuffer(""), alt = false;
<?php foreach ($producten as $product) {
		$nr = $product->getArtikelID(); ?>
	if (num_<?php echo $nr; ?> > 0)
	{
		if (alt) tbl.append("<tr class=\"striped\">");
		else tbl.append("<tr>");
		alt = !alt;
		
		tbl.append("<td>");
        tbl.append(num_<?php echo $nr; ?>);
        tbl.append("&times;<?php echo $product->getNaam(); ?></td> <td class=\"num\"><?php echo $product->getKudos(); ?></td> <td class=\"num\">");
		tbl.append(num_<?php echo $nr; ?> * <?php echo $product->getKudos(); ?>);
		tbl.append("</td></tr>");
	}
<?php } ?>

    /*	tbl.append("<tr class=\"totaal\"><td colspan=\"3\"><strong><?php echo _('Totaal')?></strong></td><td id=\"conf_selkudos\" class=\"num\">");
	tbl.append(g_sel_kudos);
	tbl.append("</td></tr>");*/

	g_el["conf_tbody"].innerHTML = tbl.toString();
	g_el["conf_lidkudos"].innerHTML = lid_kudos;
	g_el["conf_eindkudos"].innerHTML = lid_kudos - sel_kudos;
	g_el["conf_total"].innerHTML = g_sel_kudos;

	openDiv("confirmatie");

	return false;
}

function revertConfirmatie ()
{
	resetUserTimeout();
	openDiv("winkel");
	return false;
}

function handleConfirmatie ()
{
	revertUserTimeout();
	var g_el = el;

	g_el["status_msg"].innerHTML = "";
	openDiv("status");

	var productenstring = new StringBuffer("");
<?php foreach ($producten as $product) { ?>
	if (num_<?php echo $product->getArtikelID(); ?> > 0)
	{
		productenstring.append("<?php echo $product->getArtikelID(); ?>,");
		productenstring.append(num_<?php echo $product->getArtikelID(); ?>);
		productenstring.append(";");
	}
<?php } ?>

	setWaitingIcon(true);
	$.ajax({ type: "POST", url: 'api/maakTransactie.php?', data: { lidnr: lidnr, pasID: pasID, kudos: sel_kudos, producten: productenstring.toString()} }).done(function (response)
		{
			var g_el = el;

			g_el["status_msg"].innerHTML = response.exit == 0
				? "<?php echo _('De transactie was succesvol, geniet van je versnapering(en)!')?>"
				: "<?php echo _('Er was een fout; er is niks afgeschreven')?>.<br /><?php echo _('De fout was')?>: "+response.msg+".";
			openDiv("status");
			stat_timeout = setTimeout("openDiv('startscherm');", STAT_MAX_TIME_MS);
			return;
		}
		);

	return false;
}

function resetUserTimeout ()
{
	if (user_timeout != 0)
		clearTimeout(user_timeout);
	user_timeout = setTimeout("openDiv('startscherm');", USER_MAX_TIME_MS);
}

function revertUserTimeout ()
{
	if (user_timeout != 0)
		clearTimeout(user_timeout);
	user_timeout = 0;
}

function resetRefrTimeout ()
{
	if (refr_timeout != 0)
		clearTimeout(refr_timeout);
	refr_timeout = setTimeout('this.window.location.reload();', REFR_MAX_TIME_MS);
}

function revertRefrTimeout ()
{
	if (refr_timeout != 0)
		clearTimeout(refr_timeout);
	refr_timeout = 0;
}

function revertStatus ()
{
	if (stat_timeout != 0)
		clearTimeout(stat_timeout);
	stat_timeout = 0;
	openDiv("startscherm");
	return false;
}

function setWaitingIcon (visibility)
{
	var g_el_wait = el["wait"];
	g_el_wait.style.visibility = visibility ? "visible" : "hidden";
}

function checkRfid (rfid)
{
	var g_el = el;
	setWaitingIcon(true);
	el["startscherm_rfid"].disabled = true;
	$.ajax({ type: "POST", url: 'api/checkPas.php', data: { rfid: rfid } }).done(function (response)
		{
			if (response.exit != 0)
			{
				pasID = 0;
				handleFout("<?php echo _('Ongeldige sleutelhanger')?>");
			}
			else
			{
				lidnr = response.lidnr;
				pasID = response.pasID;
				naam = response.naam;
				lid_kudos = response.kudos;
				sel_kudos = 0;
				initWinkel();
			}
		}
		);
	return false;
}

function loguit ()
{
	$.ajax({ type: "POST", url: 'api/loguit.php', data: { lidnr: lidnr } , async: false }).done(function (response)
		{
			if (response.exit == 0) {
				lidnr = 0;
				lid_kudos = 0;
			}
		}
	);
	return false;
}

var kittenVolgorde = [ 0, 0, 0, 0 ];
var kittenMax = 4;
var currKittenIndex = 0;
var useKitten = false;

function kittenCounter(chosen)
{
	if(useKitten) {
		return;
	}

	// Als de juiste index gekozen is, update naar de volgende index die gekozen moet worden
	if(chosen == kittenVolgorde[currKittenIndex]) {
		currKittenIndex++;
	} else {
		currKittenIndex = 0;
	}

	if(currKittenIndex == kittenMax) {
		useKitten = true;
		initKitten();
	}
}

function initKitten()
{
	var width = Math.floor(Math.random()*75)+50;
	var height = Math.floor(Math.random()*100)+75;

	var i = 0;
	var max = 20;
	$('.item_img').each(function() {
		var $clone = $(this).clone();
		$(this).css({'display': 'none'});
		$clone.attr('src', 'http://placekitten.com/'+(width+i+max)+'/'+height+'');
		$clone.attr('class', 'item_img_kitten');
		$(this).after($clone);
		++i;
	});
}

function removeKitten()
{
	$('.item_img_kitten').remove();
	$('.item_img').css({'display': 'block'});
	currKittenIndex = 0;
	useKitten = false;
}

$(document).ready(function() {
	init();

	$('body').click(function() {
		resetFocus();
	});

	$('.startschermTerug').click(function() {
		return revertFout();
	});

	$('.rfid_form').submit(function() {
		handleStartscherm();
		return false;
	});

	$('.vlaggetjeNederlands').click(function() {
		return setNederlands();
	});

	$('.vlaggetjeEngels').click(function() {
		return setEngels();
	});

	$('.product').parent().parent().click(function() {
		var productId = $(this).find('.product').attr('id');
		return addWinkel(productId);
	});

	$('.removeProduct').click(function() {
		var productId = $(this).attr('id');
		return rmWinkel(productId);
	});

	$('.annulerenWinkel').click(function() {
		return revertWinkel();
	});

	$('.verderWinkel').click(function() {
		return handleWinkel();
	});

	$('.annulerenConf').click(function() {
		return revertConfirmatie();
	});

	$('.verderConf').click(function() {
		return handleConfirmatie();
	});

	$('.annulerenEind').click(function() {
		return revertStatus();
	});
});
