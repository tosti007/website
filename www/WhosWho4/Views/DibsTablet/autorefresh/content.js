var perform_reload = true;

window.addEventListener("load", setTimeout(do_reload,60000),false);
window.addEventListener("cancelReload",disable_reload);

function disable_reload()
{
	perform_reload = false;
}
function do_reload()
{
	if (perform_reload)
	{
		window.location.reload();		
	}
}
