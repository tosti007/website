<?
/***
 * $Id$
 * Staat niet in de database, opslag gebeurt nauwelijks en supporten we dus
 * niet meer. (tot Alex het weer wil :P)
 */

class Kart
{
	protected $cies;
	protected $personen;
	protected $media;

	/** CONSTRUCTOR **/
	protected function __construct()
	{
		$this->cies = new CommissieVerzameling();
		$this->personen = new PersoonVerzameling();
		$this->media = new MediaVerzameling();
	}

	static public function geef()
	{
		global $session;

		static $instance = NULL;
		if (isset($instance))
		{
			return $instance;
		}

		$instance = new Kart();
		if (!$session->has('Kart'))
		{
			return $instance;
		}

		$arr = unserialize($session->get('Kart'));
		if (!is_array($arr))
		{
			throw new UnexpectedValueException(
				_("Opgeslagen kart is onjuist geserializeerd"));
		}

		if (isset($arr['cies']))
		{
			$instance->cies = CommissieVerzameling::verzamel($arr['cies']);
		}
		if (isset($arr['personen']))
		{
			$instance->personen = PersoonVerzameling::verzamel($arr['personen']);
		}
		if (isset($arr['media']))
		{
			$instance->media = MediaVerzameling::verzamel($arr['media']);
		}

		return $instance;
	}

	protected function gewijzigd($unused = False)
	{
		global $session;

		if ($this->personen->aantal() > KART_MAX_LEDEN)
		{
			Page::addMelding('Je kunt niet meer dan ' . KART_MAX_LEDEN . ' mensen in je kart shoppen.', 'fout');
			return;
		}

		$kart = serialize(array(
			"personen" => $this->personen->keys()
		, "cies" => $this->cies->keys()
		, "media" => $this->media->keys()
		));
		$session->set('Kart', $kart);
		/*
		$lid = Lid::getIngelogdLid();
		$pv = PersoonVoorkeur::geef($lid, 'KART');
		if($pv == NULL) {
			$pv = new PersoonVoorkeur($lid, 'KART');
		}		
		$pv->setContent($kart);
		$pv->opslaan();
		 */
	}

	public function voegCieToe($cieId)
	{
		$cie = Commissie::geef($cieId);
		$this->cies->voegtoe($cie);

		$this->gewijzigd();
	}

	public function voegCiesToe($cieArr)
	{
		if ($cieArr instanceof CommissieVerzameling)
		{
			$newCies = $cieArr;
		}
		else
		{
			$newCies = CommissieVerzameling::verzamel($cieArr);
		}
		$this->cies->union($newCies);

		$this->gewijzigd();
	}

	public function voegPersoonToe($persId)
	{
		$pers = Persoon::geef($persId);
		$this->personen->voegtoe($pers);

		$this->gewijzigd();
	}

	public function voegPersonenToe($personen)
	{
		if (!$personen instanceof PersoonVerzameling)
		{
			$newPersonen = PersoonVerzameling::verzamel($personen);
		}
		else
		{
			$newPersonen = $personen;
		}
		$this->personen->union($newPersonen);

		$this->gewijzigd();
	}

	public function voegMediumToe($mediaId)
	{
		$media = Media::geef($mediaId);
		$this->media->voegtoe($media);

		$this->gewijzigd();
	}

	public function voegMediaToe($mediaArr)
	{
		if ($mediaArr instanceof MediaVerzameling)
		{
			$newMedia = $mediaArr;
		}
		else
		{
			$newMedia = MediaVerzameling::verzamel($mediaArr);
		}
		$this->media->union($newMedia);

		$this->gewijzigd();
	}

	public function verwijderCie($cieId)
	{
		$this->cies->verwijder($cieId);

		$this->gewijzigd();
	}

	public function verwijderCies($cieIDs)
	{
		foreach ($cieIDs as $cieID)
		{
			if ($this->cies->bevat($cieID))
			{
				$this->cies->verwijder($cieID);
			}
		}

		$this->gewijzigd();
	}

	public function verwijderPersoon($persId)
	{
		$this->personen->verwijder($persId);

		$this->gewijzigd();
	}

	public function verwijderPersonen($personen)
	{
		foreach ($personen as $persoon)
		{
			if ($this->personen->bevat($persoon))
			{
				$this->personen->verwijder($persoon);
			}
		}

		$this->gewijzigd();
	}

	public function verwijderMedium($mediaId)
	{
		$this->media->verwijder($mediaId);

		$this->gewijzigd();
	}

	public function verwijderMedia()
	{
		$this->media = new MediaVerzameling();

		$this->gewijzigd();
	}

	public function getCies()
	{
		return clone $this->cies;
	}

	/**
	 * @brief Geeft een aanpasbare kopie van de personen die direct in de kart zitten.
	 *
	 * Als ook de personen nodig zijn, die bij een commissie die in de kart zit
	 * horen, dan kun je getAllePersonen gebruiken.
	 */
	public function getPersonen()
	{
		return clone $this->personen;
	}

	/**
	 * @brief Geeft op een snelle manier, zonder te clonen, het aantal personen wat direct in de kart zit.
	 */
	public function getAantalPersonen()
	{
		return $this->personen->aantal();
	}

	public function getLeden()
	{
		return LidVerzameling::verzamel($this->personen->keys());
	}

	public function getMedia()
	{
		return clone $this->media;
	}

	/**
	 *  Geef alle personen in de kart, inclusief de cieleden van cies in de kart.
	 * @returns Een PersoonVerzameling van echt iedereen in je kart.
	 */
	public function getAllePersonen()
	{
		$personen = $this->getPersonen();
		foreach ($this->getCies() as $cie)
		{
			$personen->union($cie->leden());
		}
		return $personen;
	}

	public function setCies($cies)
	{
		$this->cies = $cies;

		$this->gewijzigd();
	}

	public function setPersonen($pers)
	{
		$this->personen = $pers;

		$this->gewijzigd();
	}

	public function setMedia($media)
	{
		$this->media = $media;

		$this->gewijzigd();
	}

}
// vim:sw=4:ts=4:tw=0:foldlevel=1
