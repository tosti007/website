<?php

die();

// Op dit moment wordt de api niet ondersteund. Mocht die in de toekomst
// wel weer gebruikt worden, dan kan het onderstaande gebruikt worden

/*
// Laad de basis van WSW4 zoals $WSW4DB enzo
require_once('space/init.php');

try
{
	// Probeer het, als het niet werkt dan moeten we een fout-pagina tonen
	$request = Request::getInstance();
	$request->showGenericHeaders()
			->deroute()
			->checkCredentials()
			->parseInput()
			->dispatch()
			->show();
}
catch (Exception $e)
{
	// Ohw fuck, het ging fout
	// Dit object hebben we nodig om fout-informatie eruit te halen
	$request = Request::getInstance();

	// Kunnen we nog een bijbehorende fout in de header stoppen?
	$request->setErrorCode($e->getCode());

	// Afhankelijk van de fout gaan we iets extra's doen
	switch ($e->getCode())
	{
		// Er ging iets fout met de user input, wel zo aardig om te laten zien wat er mis ging
		case 400:
			if (!DEBUG)
				echo $e->getMessage();
			break;

		// Geen authorizatie, vraag aan request om het opnieuw te proberen met andere autherizatie gegevens
		case 401:
			$request->askAuthorization();
			break;
	}

	// Als we aan het debuggen zijn, publiseer dan even iets meer details
	if (DEBUG)
		echo 'Bericht: ' . $e->getMessage() . "\r\n" .
			 'Bestand: ' . $e->getFile() . "\r\n" .
			 'Regel: ' . $e->getLine() . "\r\n" .
			 "\r\n" .
			 'URI: ' . $_SERVER['REQUEST_URI'] . "\r\n" .
			 'Target: ' . $request->getTarget() . "\r\n" .
			 "\r\n" .
			 'Params: ' . print_r($request->getParams(), true);
}
 */
