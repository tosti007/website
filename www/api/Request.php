<?php
/**
 * Dit object handelt het hele verzoek van de gebruiker af
 * Omdat dit zo belangrijk is mag deze code niet uitgebreid worden door andere
 * klassen want die kunnen nieuwe bugs introduceren, dus is dit object ook
 * final
 */
final class Request
{
	// Dit object is een singleton, dit is de enige instantie
	static private $_instance;

	// De target-variabelen zeggen van welk object wat uitgevoerd moet worden
	private $targetObject; // voorbeeld: Lid
	private $targetOperation; // voorbeeld: get
	private $targetId; // voorbeeld 6206

	// Wie is er ingelogd, van het type Persoon
	private $login;

	// De params worden doorgegeven aan het object en de methode
	private $params;

	// De return waarde van de callback in array-formaat
	private $output;

	/**
	 * Dit object is een singleton, zorg ervoor dat er altijd een kopie van op
	 * handen is
	 */
	static public function getInstance ()
	{
		if (!self::$_instance instanceof Request)
			self::$_instance = new Request();
		return self::$_instance;
	}

	/**
	 * Deze constructor doet niks, wel zo veilig want hier willen we niks
	 * gooien
	 */
	public function __construct ()
	{
	}

	/**
	 * Returneert welke aanvraag er aangevraagd is
	 */
	public function getTarget ()
	{
		return $this->targetObject . '::' . $this->targetOperation;
	}

	/**
	 * Returneert welke parameters we aan de callback zouden sturen
	 */
	public function getParams ()
	{
		return $this->params;;
	}

	/**
	 * Returneer het contactnr van de ingelogd persoon
	 */
	public function getLoginID ()
	{
		if ($this->login instanceof Persoon)
			return $this->login->geefID();
		return false;
	}

	/**
	 * Zet de errorcode in de headers
	 * Zodra we over zijn op php 5.4+ kan dit vervangen worden door http_respond_code
	 */
	public function setErrorCode ($code)
	{
		// Welke fout geven we mee
		switch ($code)
		{
			case 100: $text = 'Continue'; break;
			case 101: $text = 'Switching Protocols'; break;
			case 200: $text = 'OK'; break;
			case 201: $text = 'Created'; break;
			case 202: $text = 'Accepted'; break;
			case 203: $text = 'Non-Authoritative Information'; break;
			case 204: $text = 'No Content'; break;
			case 205: $text = 'Reset Content'; break;
			case 206: $text = 'Partial Content'; break;
			case 300: $text = 'Multiple Choices'; break;
			case 301: $text = 'Moved Permanently'; break;
			case 302: $text = 'Moved Temporarily'; break;
			case 303: $text = 'See Other'; break;
			case 304: $text = 'Not Modified'; break;
			case 305: $text = 'Use Proxy'; break;
			case 400: $text = 'Bad Request'; break;
			case 401: $text = 'Unauthorized'; break;
			case 402: $text = 'Payment Required'; break;
			case 403: $text = 'Forbidden'; break;
			case 404: $text = 'Not Found'; break;
			case 405: $text = 'Method Not Allowed'; break;
			case 406: $text = 'Not Acceptable'; break;
			case 407: $text = 'Proxy Authentication Required'; break;
			case 408: $text = 'Request Time-out'; break;
			case 409: $text = 'Conflict'; break;
			case 410: $text = 'Gone'; break;
			case 411: $text = 'Length Required'; break;
			case 412: $text = 'Precondition Failed'; break;
			case 413: $text = 'Request Entity Too Large'; break;
			case 414: $text = 'Request-URI Too Large'; break;
			case 415: $text = 'Unsupported Media Type'; break;
			case 500: $text = 'Internal Server Error'; break;
			case 501: $text = 'Not Implemented'; break;
			case 502: $text = 'Bad Gateway'; break;
			case 503: $text = 'Service Unavailable'; break;
			case 504: $text = 'Gateway Time-out'; break;
			case 505: $text = 'HTTP Version not supported'; break;
			default: user_error('Onbekende foutcode mee gestuurd', E_USER_ERROR);
		}

		// Bepaal met welk protocol we werken
		$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

		// Zet de header
		header($protocol . ' ' . $code . ' ' . $text);
	}

	/**
	 * Stelt een eis aan de ingelogde gebruiker, mogelijke levels: gast,
	 * ingelogd, bestuur en god
	 */
	public function requireAuth ($level = 'ingelogd')
	{
		global $auth;

		// Als er om gast gevraagd wordt is het altijd goed
		if ($level == 'gast')
			return $this;

		// Controleer of er uberhaupt ingelogd is
		if (!$this->login instanceof Persoon)
			throw new Exception('Niet ingelogd', 401);

		// Controleer of de ingelogde gebruiker aan het level voldoet
		switch ($level)
		{
			// Ingelogd hebben we al in het begin van deze methode gechekt
			case 'ingelogd':
				break;

			// Bestuur is  kijken of we in de bestuursarray staan
			case 'bestuur':
				if (!isBestuur($this->login->geefID()))
					throw new Exception('Niet bestuur', 401);
				break;

			// God is ook een array
			case 'god':
				// Met de API kan godrechten aan/uit niet veranderd worden, dus
				// gebruik godachtigheid
				if (!$auth->isGodAchtig($this->login->geefID()))
					throw new Exception('Niet god', 401);
				break;
		}

		return $this;
	}

	/**
	 * Deze methode vraagt aan de gebruiker om zich te identificeren
	 */
	public function askAuthorization ()
	{
		// Stuur de header waaruit alle browsers opmaken dat ze wel een auth moeten sturen
		header('WWW-Authenticate: Basic realm="API A-Eskwadraat');
	}

	/**
	 * Deze methode stuurt generieke headers die altijd van toepassing zijn
	 */
	public function showGenericHeaders ()
	{
		// Dit is een publieke resource (openbare api)
		if (isset($_SERVER['HTTP_ORIGIN']))
			header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
		else
			header('Access-Control-Allow-Origin: *');

		// Voor nu ondersteunen we alleen get requests
		header('Access-Control-Request-Method: GET');

		// We gaan sowieso json(p) outputten
		header('Content-Type: application/json; charset=utf-8');

		return $this;
	}

	/**
	 * Deze methode deroute het verzoek en vult $targetObject, $targetOperation
	 * en $targetId in
	 */
	public function deroute ()
	{
		// Dit is de url die aangevraagd is
		$uri = $_SERVER['REQUEST_URI'];

		// Haal alles wat er na het ? zit eraf
		$uriparts = explode('?', $uri);
		$uri = $uriparts[0];

		// Als het begint met api, dan zitten we op www.a-eskwadraat.nl/api/*
		// anders op api.a-eskwadraat.nl/*
		// let op: in $uri zit ook de host, dus vandaar dat we er sowieso iets
		// vanaf moeten snoepen
		$expuri = explode('/', $uri);
		if ($expuri[1] == 'api')
			$expuri = array_slice($expuri, 2);
		else
			$expuri = array_slice($expuri, 1);

		// Vul $targetObject in
		// als we die niet kunnen vinden, ga dan door naar 404
		if (isset($expuri[0]) && class_exists($expuri[0]))
			$this->targetObject = $expuri[0];
		else
			throw new Exception('Geen juiste class meegegeven', 404);

		// Vul de $targetOperation in
		// als die niet GET is, ga dan door naar 404
		if (!in_array($_SERVER['REQUEST_METHOD'], array('GET')))
			throw new Exception('Geen geldige aanvraag', 404);
		$this->targetOperation = strtolower($_SERVER['REQUEST_METHOD']);

		// Als het bestaat, vul ook $targetId in
		$this->targetId = (isset($expuri[1])) ? $expuri[1] : null;

		return $this;
	}

	/**
	 * Deze methode checkt of dat er credentials zijn mee gegeven en zo ja,
	 * handel alle login gebeuren af
	 */
	public function checkCredentials ()
	{
		// Controleer of er login gegevens zijn opgestuurd
		if (isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_PW'])
		{
			// Controleer de opgestuurde gegevens
			if (!$wachtwoord = Wachtwoord::geefLogin($_SERVER['PHP_AUTH_USER']))
				throw new Exception('Ongeldige loginnaam', 401);
			if (!$wachtwoord->authenticate($_SERVER['PHP_AUTH_PW']))
				throw new Exception('Ongeldig wachtwoord', 401);

			// Alles klopt
			$this->login = $wachtwoord->getPersoon();
		}

		return $this;
	}

	/**
	 * Deze methode verwerkt de opgestuurde input, in het geval van een
	 * get-request houdt dat in dat de $params['constraints'],
	 * $params['velden'], $params['order'] en $params['offset'] gevuld moeten
	 * worden
	 */
	public function parseInput ()
	{
		// Afhankelijk van het verzoek maken we onderscheid in hoe de input
		// verwerkt dient te worden
		switch ($this->targetOperation)
		{
			case 'get':
				// De constraints geven aan waar de verzameling aan moet voldoen
				// voorbeeld: [{"veld": `Voornaam`, "waarde": `Emile`,"operatie",`LIKE`}]
				if (isset($_GET['constraints']))
					$this->params['constraints'] = new ConstraintVerzameling(json_decode($_GET['constraints'], true));
				else
					$this->params['constraints'] = new ConstraintVerzameling();

				// De velden geven aan welke velden teruggegeven moeten worden,
				// als leeg dan gewoon alles wat mogelijk is
				// voorbeeld: ["Voornaam", "Achternaam"]
				if (isset($_GET['velden']))
					$this->params['velden'] = json_decode($_GET['velden'], true);
				else
					$this->params['velden'] = array();

				// De order geeft aan op welk veld gesorteerd moet worden en
				// hoe
				// voorbeeld: {"veld": "Voornaam","hoe": "ASC"}
				if (isset($_GET['order']))
					$this->params['order'] = json_decode($_GET['order'], true);
				else
					$this->params['order'] = array('veld' => null, 'hoe' => null);

				// Controleer of er wel een geldige sorteerkeuze is gemaakt
				if (!in_array($this->params['order']['hoe'], array(null, 'ASC', 'DESC')))
					throw new Exception('Ongeldige sorteerkeuze', 400);

				// De offset geeft de offset aan in de gesorteerde lijst vanaf
				// waar we dingen terug willen (net als de limit bij sql)
				if (isset($_GET['offset']))
					$this->params['offset'] = (int) $_GET['offset'];
				else
					$this->params['offset'] = 0;

				break;
		}

		return $this;
	}

	/**
	 * Dit roept de uiteindelijke methode aan in het api-object.
	 * (!)Op dit moment is dit alleen nog maar de get functie in de generated class.
	 */
	public function dispatch ()
	{
		// Het object wat de request afhandelt
		$target = $this->targetObject . 'API';
		if (!class_exists($target))
			throw new Exception('API-object niet gevonden', 501);

		// Wat we willen uitvoeren
		$call = array($target, $this->targetOperation);
		if (!is_callable($call))
			throw new Exception('API ondersteunt methode niet', 501);

		// Roep alles aan
		$this->output = call_user_func($call, $this->targetId, $this->params);

		return $this;
	}

	/**
	 * Laat alles zien wat we hebben verkregen
	 */
	public function show ()
	{
		// Voor nu ondersteunen we alleen json(p)
		if (isset($_GET['callback']))
			echo $_GET['callback'] . '(' . json_encode($this->output) . ')';
		else
			echo json_encode($this->output);
	}
}
