<?php
// $Id$

/* Maak een random string van lengte $size. Deze string kan cijfers, hoofd- en
 * kleine letters bevatten. */
function randstr($size) {
	$str = '';
	for ($i = 0; $i < $size; $i++) {
		$v = rand(0, 10 + 26 + 26 - 1);
		if ($v < 10) {
			$str .= $v;
		}
		else if ($v < 36) {
			$str .= chr($v - 10 + 65);
		}
		else {
			$str .= chr($v - 36 + 65 + 32);
		}
	}
	return $str;
}

/* Maak een string $str geschikt als latex-text. */
function latexescape($str) {
	global $LATEX_ESCAPE_CHARS;
	$str = addcslashes($str, '#%$\&_^{}~');
	return strtr($str, $LATEX_ESCAPE_CHARS);
}

/**
 * @brief Lever een niet-HTML bestand op.
 *
 * Alle tekst die (direct) hierna wordt
 * weergegeven, komt in dit bestand te staan. De gebruiker krijgt dan de
 * mogelijkheid het bestand lokaal op te slaan.
 *
 * @deprecated Je wilt liever sendfilefromfs aanroepen, of de juiste Page-klasse gebruiken.
 *
 * @sa sendfilefromfs
 * */
function sendfile($name, $type = 'application/octet-stream') {
	user_error("Gebruik sendfilefromfs ipv sendfile, of een Page-klasse.", E_USER_DEPRECATED);

	header("Content-Disposition: attachment; filename=$name");
	header("Content-Type: $type");
	header('Content-Description: A-Eskwadraat Webservice');
}

use Intervention\Image\ImageManagerStatic;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * @brief Maak een Response die een bestand naar de browser stuurt.
 *
 * Gegeven een $path en een MIME-type, construeer downloadheaders
 * en stuur een file naar browser.
 *
 * @param string path
 * Het relatieve pad naar het bestand om te versturen.
 * @param string displayname
 * Een "mooie naam" die door de browser gebruikt wordt voor b.v. save as.
 * @param string prefix Default is FILESYSTEM_PREFIX. Wordt voor $path geplakt.
 * Dit wil je ivm beveiliging: is je prefix "/srv/dictaten", kan een inbreker
 * niet zomaar /srv/http/www/secret.php downloaden.
 *
 * @return \Symfony\Component\HttpFoundation\Response
 * Een Response-object dat je kan returnen om een bestand te versturen.
 */
function sendfilefromfs($path, $displayname = null, $prefix = NULL)
{
	global $logger;

	// FIXME: waarom wordt $displayname niet gebruikt?

	// Zet de juiste prefix voor het pad.
	// (FIXME: ivm beveiliging mogen er geen '..' enzo in $path staan, check dit!)
	if (!$prefix) {
		$abspath = FILESYSTEM_PREFIX . $path;
	} else {
		$abspath = $prefix . $path;
	}
	$logger->info('sendfilefromfs: abspath=' . $abspath);

	$file = new File($abspath);
	$type = $file->getMimeType();

	if(strpos($type, "image/") === 0 && tryPar('lowres', false))
	{
		try {
			echo ImageManagerStatic::make($abspath)->response('jpg', 1);
			exit;
		} catch (Intervention\Image\Exception\NotReadableException $e) {
			// Het is een plaatje die niet geschaald kan worden,
			// dus we roepen geen exit aan.
			// Dat zorgt ervoor dat we gewoon het origineel opsturen.
		}
	}

	$response = new BinaryFileResponse($abspath, 200, array(), true, null, true, true);

	$request = Request::createFromGlobals();
	$response->prepare($request);

	$logger->info(print_r($response, true));

	return $response;
}

/**
 *	@brief Probeer of een variabele $var in de $_REQUEST data zit.
 *	Het is mogelijk een standaardwaarde $default op te geven als $var niet
 *	bestaat, of geen waarde heeft. Returnt $default als de variabele niet
 *	gevonden is.
 *
 *	@param var      naam van de variabele
 *	@param default  return waarde als $var niet gevonden wordt
 *	@param method   GET, POST of REQUEST
 */
function tryPar($var, $default = NULL, $method = 'REQUEST')
{
	global $request;

	if(strpos($var, '[') !== False && strpos($var, ']') !== False)
	{
		$split = preg_split('/[[\]]+/', $var, -1, PREG_SPLIT_NO_EMPTY);
		$val = tryPar($split[0], array(), $method);
		array_shift($split);

		foreach($split as $key)
		{
			if(!isset($val[$key]))
				return $default;

			$val = $val[$key];
		}

		return $val;
	}

	if ($method === 'GET') return $request->query->get($var, $default);
	if ($method === 'POST') return $request->request->get($var, $default);
	assert($method === 'REQUEST');

	$get = $request->query->get($var);
	$post = $request->request->get($var);
	if (is_array($post) && is_array($get)) {
		// Als het allebei verzamelingen van parameters zijn, willen we de vereniging hiervan.
		// Als een key in allebei voorkomt, pak degene uit $get
		// (dit gaat in tegen default request_order volgorde van $_REQUEST)
		$ret = $get + $post;

		if(sizeof($ret) == 1) {
			return end($ret);
		} else {
			return $ret;
		}
	} else {
		return $request->get($var, $default);
	}
}

/**
 *	@brief Eis dat een variabele $var in de $_REQUEST data zit.
 *	Wanneer de variabele niet bestaat, of lege string is, verschijnt er een
 *	foutmelding.
 *
 * @deprecated Deze functie doet side-effectvolle output.
 * Beter is tryPar aan te roepen en de output zelf valideren,
 * of nog beter: doe het via processForm (indien mogelijk).
 *
 *	@see tryPar
 */
function requirePar($var, $method = 'REQUEST')
{
	if (getZeurmodus('requirePar'))
	{
		user_error(
			"requirePar is deprecated, gebruik liever tryPar + zelf valideren",
			E_USER_DEPRECATED
		);
	}

	$val = tryPar($var, NULL, $method);
	if(!isset($val))
		print_error(sprintf(_("Geef een waarde op voor %s.\n"), $var));

	return $val;
}

// Geeft terug of de persoon behorend bij dit ID,
// beperkte rechten heeft tot alle databases van WhosWho4
function isPersoonBeperkt($persoonID)
{
	global $auth;

	return $auth->isDiBSTablet($persoonID);
}

/**
 * @brief Doet eigenlijk hetzelfde als print_error, maar dan non-fatal: dus er
 * de uitvoer van code gaat gewoon door
 *
 * @deprecated Gebruik liever Page::addMelding en dan een return naar space.
*/
function print_warning ($tekst)
{
	if (getZeurmodus())
	{
		user_error("Gebruik liever Page::addMelding()", E_USER_DEPRECATED);
	}

	echo new HtmlParagraph($tekst, 'alert alert-warning');
}

/** kink 23/02/2002
 * Geeft een gebruikersfout weer op het scherm (deze fouten worden
 * niet gelogd omdat het geen systeemfouten zijn). Te gebruiken voor
 * bijvoorbeeld een onterecht leeg invoerveld. Gebruik user_error
 * voor foutmeldingen die wel gelogd moeten worden (fouten die eigenlijk
 * niet mogen gebeuren, zoals query errors).
 *
 * De functie geeft de melding weer en doet een Page::getInstance()->end() en een exit
 *
 * @deprecated Gebruik liever Page::addMelding en dan een return naar space.
 */
function print_error ($tekst)
{
	if (getZeurmodus())
	{
		user_error("Gebruik liever Page::addMelding()", E_USER_DEPRECATED);
	}

	echo new HtmlParagraph($tekst, 'alert alert-danger');

	if (function_exists('Page::getInstance()->end'))
		Page::getInstance()->end();

	exit;
}

/**
 * @brief Maak een 'TIP van de Webcie!'-html-item.
 *
 * @param tipstring HTML-code die in de tipbox komt.
 * @param echo Zo ja, wordt de html-code van de topbox meteen geechod. (DIT WIL JE NIET!)
 *
 * @deprecated echo=true zorgt voor slecht testbare code, zet dit op false en stop de returnwaarde in een HTMLObject.
 *
 * @return Een HTML-string, klaar om in je pagina te stoppen. Of doet een echo.
 */
function tipbox($tipstring, $echo = true) {
	if ($echo && getZeurmodus())
	{
		user_error("Doe AUB geen echo, zet dit netjes in een HTMLPage", E_USER_DEPRECATED);
	}

	global $KLEUR;

	$div = new HtmlDiv();
	$div->add($l = new HtmlLegend());
	$l->add($s = new HtmlSpan());
	$s->addClass('fa fa-info-circle');
	$l->add(_(" TIP van de Webcie!"));
	$l->addClass('legend-' . $KLEUR);
	$div->add(new HtmlParagraph($tipstring));
	$t = $div->addClass('bs-callout bs-callout-info')->makeHtml();
	if($echo)
		echo $t;
	else
		return $t;
}


/**
 * Geeft het huidige collegejaar. 01/02 levert op: 2001
 *
 * @return int
 * Het viercijferige collegejaar.
 */
function colJaar()
{
	return fromColJaar(date('n'), date('Y'));
}


/**
 * Geeft het ''collegejaar'' waarin een bepaalde maand valt.
 * maart 2002 valt in '01/'02 levert 2001 op
 *
 * @return int
 * Het viercijferige collegejaar.
 */
function fromColJaar($maand, $jaar)
{
	return coljaarFromDateTime(DateTimeLocale::createFromDate($jaar, $maand));
}

/**
 * @brief Geef het collegejaar waar het DateTimeLocale-object onder valt.
 * @see colJaar
 * @see fromColJaar
 * @param datetime DateTimeLocale
 * Wordt tot collegejaar omgezet.
 * @return int
 * Het collegejaar met 4 cijfers.
 */
function coljaarFromDateTime($datetime) {
	return Periode::vanDatum($datetime)->getCollegejaar();
}


function checkEmailFormat($str, $pedantic = false)
{
	/* We hebben twee checks, een strenge voor in de wsw4_integrity check
	   en een minder strenge voor als iemand zich bijvoorbeeld aanmeldt
	   voor de intro. Die frustreren we dan niet in zijn aanmeldproces, maar
	   er komen er wel achteraf achter als die (hoogwaarschijnlijk) een verkeerd adres
	   heeft ingevuld
	*/

	if($pedantic)
	{
		$localpartatom = '[-a-z0-9!#$\'%&\\*\/_`{|}~\+]';
		$localpart = "$localpartatom+(\\.$localpartatom+)*";
		$domainatom = '[a-z0-9]+[\-a-z0-9]*[a-z0-9]+';
		$domain = "$domainatom(\\.$domainatom)+";
		$emailreg="$localpart@$domain";
	}
	else
	{
		$emailreg = ".+@.{4,}";
	}

	return preg_match("/^$emailreg$/i", $str);
}


/**
	Deze functie accepteert een $dateString in twee formaten:
	2005-09-25 (YYYY-MM-DD)
	25-09-2005 (DD-MM-YYYY)

	(allebei betekenen ze 25 september 2005)

	en retourneert in beide gevallen een datestring in formaat YYYY-MM-DD
	of (als het een ongeldige string is) false.

	achter de datum mag een tijds aanduiding staan (formaat hh:mm:ss)
	deze wordt gechecked op nummertjes en op binnen een geldig interval:
	h [0,23], m,s [0,59] en (indien aanwezig) mee geretourneerd
	(inclusief voorloop 0 indien <10)

	Deze functie controleert ook of er 31 of 30 dagen in een
	maand zitten, en houdt rekening met schrikkeljaren.

	Als de datum ongeldig is, dan retourneert de functie 'false'.
*/
function parseDate($datestring){
	$datestring = trim($datestring);
	$dateparts = preg_split('/\s+/', $datestring);

	// geen datum gevonden
	if ( empty($dateparts[0])) return false;

	$datepartsparts = explode('-', $dateparts[0]);
	if(sizeof($datepartsparts) != 3) {
		return false;
	}

	// als er op positie 4 (het 5e karakter dus) een streepje staat, dan is er
	// wsl. sprake van een YYYY-MM-DD formaat. Errors worden onderdrukt met
	// '@', omdat het natuurlijk niet helemaal safe is om aan te nemen dat er
	// twee streepjes zijn.
	if (substr($datestring, 4, 1) == "-"){
		// YYYY-MM-DD?
		$y = @$datepartsparts[0];
		$m = @$datepartsparts[1];
		$d = @$datepartsparts[2];
	} elseif (substr($datestring, 2, 1) == "-") {
		// DD-MM-YYY
		$d = @$datepartsparts[0];
		$m = @$datepartsparts[1];
		$y = @$datepartsparts[2];
	} else {
		// Sowieso onbekend formaat
		return false;
	}

	// Checken of $d, $m en $y geldig zijn
	if (is_numeric($y) && is_numeric($m) && is_numeric($d)){
		// Ze zijn in ieder geval numeriek

		// check of deze combinatie zinnig is.
		// argh php debiele parametervolgorde!
		$valid = checkdate($m,$d,$y);
		if (!$valid) {
			return false;
		}
		$date = "$y-$m-$d";
		if(empty($dateparts[1])) {
			// er staat geen tijd achter... done
			return $date;
		}

		$timeparts = explode(':', $dateparts[1]);
		$h = $timeparts[0];
		$m = $timeparts[1];
		$s = $timeparts[2];

		if($h<0 || $h>23) return false;
		if($m<0 || $m>59) return false;
		if($s<0 || $s>59) return false;

		if (is_numeric($h) && is_numeric($m) && is_numeric($s)) {
			return sprintf("%s %02d:%02d:%02d", $date, $h, $m, $s);
		}
	} // else: dag, maand of jaar niet numeriek

	return false;
}

/**
 * Geeft nu in het formaat geschikt voor mysql fields van
 * type DATETIME. Functienaam gesuggereerd door Jeroen,
 * niet mijn verantwoordelijkheid.
 */
function getDateTime()
{
	return strftime('%Y-%m-%d %H:%M:%S');
}

//Voeg tabs toe tot we de gewenste ruimte achter de string hebben gemaakt.
function addTabs($string, $desiredlength, $tabsize = 8)
{
	$tabs = ceil(($desiredlength - strlen($string))/$tabsize);
	for($i = 0; $i < $tabs; $i++)
		$string .= "\t";
	return $string;
}

//Output een aantal bytes in een human-readable format
function show_readable_size($bytes) {
	$type = 'b';

	if ($bytes / 1024 > 1) {
		$bytes /= 1024;
		$type = 'k';

		if ($bytes / 1024 > 1) {
			$bytes /= 1024;
			$type = 'M';
		}
	}

	if ($bytes < 10) {
		$bytes *= 10;
		settype($bytes, 'integer');
		$bytes /= 10;
	} else {
		settype($bytes, 'integer');
	}

	return $bytes . '<small>&nbsp;' . $type . '</small>';
}

// Geeft een icon bij een bestandstype
function find_icon($filename) {
	// hier kunnen nog wel wat meer bij
	$available = array('pdf','ps','html');
	$ext = strtolower(substr($filename, strrpos($filename, '.')+1));

	if(in_array($ext, $available)) {
		$icon = $ext;
	} else {
		$icon = 'unknown';
	}

	return '<img src="/Layout/Images/Files/'.$icon.'.png" width="16" height="16"
		alt="'.$icon.'" title="'.$icon.'" border="0" />'."\n";
}

function printdate($value, $extended = FALSE)
{
	if ($value == '') {
		return '';
	} else if($value == '0000-00-00') {
		return _('onbekend');
	}

	$dagmaandformat = '%e %b';
	if (getLang() == 'en') {
		$dagmaandformat = '%b %e';
	}

	$stamp = strtotime($value);
	// Als stamp < 0 is de datum voor 1970, daar kan onze impl van strftime
	// niet mee omgaan. Daarom doen we strftime op dezelfde datum maar dan
	// in dit jaar, en plakken daar het jaar aan vast. We weten dan alleen
	// de dag van de week niet. Hacky, maar komt door een bug in de lib
	// implementatie dus niet echt anders op te lossen.
	if($stamp < 0) {
		$parts = explode('-',$value);
		$stamp = mktime(0,0,0, $parts[1], $parts[2]);
		return strftime($dagmaandformat, $stamp).' '.$parts[0];
	}

	// 2003-01-25 => ma 25 jan '03
	return strftime ( ($extended ? '%a ' : '') . $dagmaandformat .' \'%y',
		$stamp );
}

function externeSiteLink($url)
{
	if (!preg_match("/^(https?:|\/)/", $url) && !empty($url)) {
		$url = "http://" . $url;
	}
	return $url;
}

function gooiCacheEntrysWeg()
{
	global $cacheWeggemieterd, $cache;

	if(!$cacheWeggemieterd)
	{
		$cacheWeggemieterd = true;

		$cache->invalidateTags(['knopmenu']);

		if(hasAuth('god'))
			Page::addMelding("Cache-entry voor alle knopmenu's weggemieterd");
	}
}

function aesnaam()
{
	return _('A–Eskwadraat');
}

/**
 * zorgt dat string maximaal $len chars lang is. De laatste drie zicht-
 * bare chars worden puntjes.
 */
function str_cut($string,$len,$suffix='...')
{
	// is de string al okay?
	if (strlen($string) <= $len)
		return $string;

	// de string is te lang
	if (strlen($suffix) >= $len)
		return $suffix;

	return substr($string,0,$len-strlen($suffix)).$suffix;
}

/**
 * Deelfunctie die de default geeft als er door 0 gedeeld wordt.
 * Handig bij gemiddeldes.
 * @param number $x De noemer.
 * @param number $y De teller.
 * @param number $default Wat we geven als de teller 0 is.
 * @return number De deling of anders de default.
 */
function deelDefault($x, $y, $default = 0)
{
	if ($y == 0)
	{
		return $default;
	}
	else
	{
		return $x / $y;
	}
}
