<?php
/**
 *  Deze file bevat de functies die te maken hebben met webforms:
 *  begin een form, voeg items toe, etc.
 *
 */

$formstm = new ProfilerTimerMark('forms.php');
Profiler::getSingleton()->addTimerMark($formstm);

/* Voeg een textbox in op een rij in een tabel. Er moet een beschrijving bij
 * die in de 1e kolom wordt weergegeven. Eventueel kan er een defaultwaarde
 * $value, extra uitleg $extra, een grootte $size en een maximale invoerlengte
 * $maxlength worden opgegeven. */
function inputText($desc, $name, $value = '', $extra = '', $size = 0,
	$maxlength = 0) {

	echo "<tr><td>$desc:</td>".
		'<td>'.addInput1($name, $value, '', $size, $maxlength).'</td>';
	if ($extra) {
		echo "<td>$extra</td>";
	}
	echo "</tr>\n";
}

/* Lever een string op met daarin een textarea form-element. */
function addTextArea($name, $text='', $cols=40, $rows=10, $wrap='physical',$enabled=true) {
	return "<textarea name=\"$name\" rows=\"$rows\" cols=\"$cols\" "
		.'wrap="'.htmlspecialchars($wrap)
		.($enabled?'': ' disabled="disabled"') 
		.'">'.htmlspecialchars($text) ."</textarea>\n";
}

/* Lever een string op met daarin een checkbox form-element. */
function addCheckBox($name, $checked = false, $value='', $enabled = true) {
	$checkstr = ($checked?' checked="checked"':'') . ($enabled?'':' disabled="disabled"');
	$valuestr = $value?' value="'.htmlspecialchars($value).'"':'';
	return '<input type="checkbox" name="'.$name .'" id="'.
	// replace [ en ] met _ omdat die ongeldig zijn in een id
		strtr($name, array('['=>'_',']'=>'_')).'"'.$valuestr.$checkstr .'>';
}

/* Lever een string op met daarin een radio form-element. */
function addRadioBox($name, $checked = false, $value='') {
	$checkstr = $checked?' checked="checked"':'';
	$valuestr = $value?' value = "'.htmlspecialchars($value).'"':'';
	return '<input type="radio" id="'.$name.htmlspecialchars($value).'" name="'.$name .'"'.$valuestr.$checkstr .'>';
}

/* Lever een string op met daarin een hidden input form-element. */
function addHidden($name, $value) {
	if (is_array($value)){
		$inputs = "";
		foreach ($value as $arrkey=>$arrvalue){
			$inputs .= addInput1($name . '[' . $arrkey . ']', $arrvalue,
			true);
		}
		return $inputs;
	} else {
		return addInput1($name, $value, true);
	}
}

/* Lever een string op met daarin een hidden form-element of textbox. */
function addInput1($name, $value = '', $hidden = false, $size = 0,
	$maxlength = 0, $enabled = true) {

	$t = "<input name=\"$name\" type=\"".($hidden?'hidden':'text')
		."\" value=\"".htmlspecialchars($value)."\"";
	if ($size) {
		$t.= " size=\"$size\"";
	}
	if ($maxlength) {
		$t.= " maxlength=\"$maxlength\"";
	}
	if (!$enabled) {
		$t.= " disabled=\"disabled\"";
	}
	$t.= ">\n";
	return $t;
}

/* Lever een string op met daarin een file form-element. */
function addFileInput($name, $size='')
{
	if ($size){
		$size = " size=$size"; 
	} else {
		$size='';
	}
	return "<input name=\"$name\" type=\"file\"$size>\n";
}

/**
 * complete functie om een selectlist in te voegen.
 * gebruik:
 * name is de inhoud van het HTML name attribuut
 * values: array van option-value => zichtbare label
 * default: de key die selected moet zijn
 * usekeys: gebruik de keys van de array in option value of niet
 * disabled: als een key hierin voorkomt wordt ie disabled
 * multiple: moeten er meerdere opties tegelijk geselecteerd worden?
 * autosubmit: als deze true is dan wordt er automatische gesubmit, als dit wat anders is wordt de functie uitgevoerd. Bij gebruik moeten single 
 *		quotes worden gebruikt anders gaat 't mis!
 *
 * kink
 */
function addSelect($name, $values, $default = null, $usekeys = false, $autosubmit = false, $multiple = false, $disabled = array(), $style = "", $id = "")
{
	// slechts een element
	if(count($values) == 1) {
		$k = key($values); $v = array_pop($values);
		$s = ($usekeys?$k:$v);
		$o = (in_array($s, $disabled)?' disabled':'');
		return '<input type="hidden" name="'.
			htmlspecialchars($name) .'" value="'.
			htmlspecialchars($s) . '"' . $o . '>'.
			htmlspecialchars($v) . "\n";
	}

	$ret = '<select name="'.htmlspecialchars($name) . '"';
	if($autosubmit) {
		if($autosubmit === true){
		$ret .= ' onChange="submit();"';
		} else {
		$ret .= ' onChange="' . $autosubmit . '"'; 
		}
	}
	if($multiple){
		$ret .= ' multiple="multiple" size="5"';
	}
	$ret .= " style=\"" . htmlspecialchars($style) . "\"" .
		(empty($id) ? "" : " id=\"$id\"" ) . ">\n";
	foreach ($values as $k => $v) {
		if(!$usekeys) $k = $v;
		
		// Multiple selections checken
		$isSelected = false;
		if (is_array($default)){
			// Meerdere items selected
			foreach ($default as $defKey=>$defValue){
				if ($defValue == $k) $isSelected = true;
			}
		} elseif ($default != null){
			$isSelected = ($default == $k);
		}
		
		$isDisabled = in_array($k, $disabled);
		$ret .= '<option value="' .
			htmlspecialchars( $k ) . '"' .
			(($isSelected && !$isDisabled) ? ' selected':'') .
			(($isDisabled) ? ' disabled':'') .
			'>' . htmlspecialchars($v) ."</option>\n";
	}
	$ret .= "</select>\n";

	return $ret;
}

function showSelect($name,$selected,$choices,$edit=true,$onchange=null)
{
	$one_sel = FALSE;
	if($edit) {
		echo "<select name=\"$name\""
			.($onchange?" onchange=\"$onchange\"":'')
			.">\n";
	}
	foreach($choices as $group => $groupdata) {
		if (!is_array($groupdata)) {
			$groups = FALSE;
			$groupdata = array($group => $groupdata);
		} else {
			$groups = TRUE;
			echo $edit?'<optgroup label=\''.htmlspecialchars($group)."'>\n":'';
		}

		foreach ($groupdata as $value => $choice) {
			if(!strcasecmp($selected, $value)) {
				$one_sel = TRUE;
				if ($edit) {
					echo '<option selected value="'.htmlspecialchars($value).'">'
						.htmlspecialchars($choice)."</option>\n";
				} else {
					echo htmlspecialchars($choice);
				}
			} else if ($edit) {
				echo '<option value="'.htmlspecialchars($value).'">'
					.htmlspecialchars($choice)."</option>\n";
			}
		}

		echo $groups && $edit?"</optgroup>\n":'';
	}
	if (!$one_sel) {
		echo $edit?"<option selected=\"selected\" value=\"".htmlspecialchars($selected)."\">'". htmlspecialchars($selected)."'\n":$selected;
	}
	echo $edit?"</select>\n":
		"<input type=\"hidden\" NAME=\"$name\" VALUE=\"$selected\">";
}

// produceert een form-start-tag!
function addForm($action = '', $method = null, $enctype = '', $name = '', $moronproofid = null)
{
	if(!$method) {
		$method = 'POST';
	}
	if($enctype) {
		$enctype = ' enctype="'.$enctype.'"';
	}
	if($name) {
		$name = ' name="'.$name.'"';
	}

	return '<form action="'. $action .'" method="'. $method .'"'. $enctype . $name .
		' accept-charset="'. ACCEPT_CHARSET. '"' .
		( $moronproofid ? aes2funcs_veryfancyautodisablingsubmitbuttonfeaturespace($moronproofid) : '' ) .
		">\n";
}

// eindigt een form met submit (reset)?  "</form>"   (no echo)
function addFormEnd ($submit, $reset = FALSE, $cssClass = null)
{
	if ($cssClass){ 
		$cssClass = "class=\"" . htmlspecialchars($cssClass) . "\""; 
	} else {
		$cssClass = "";
	}
	
	
	$ret = "<input type=\"submit\" value=\"".htmlspecialchars($submit)."\" $cssClass>";
	if ( $reset !== FALSE ) {
		if ( !$reset ) $reset = _('Wis');
		$ret.= "<INPUT type=\"reset\" value='".htmlspecialchars($reset)."'>\n";
	}
		
	return $ret."</form>\n";
}


function simpleForm($action='',$extra='',$focusfield='',$method='')
{
	if (!$action) {
		$action = 'go';
	}
	startForm('','','',$method,$action,$extra);
	if ($focusfield) {
		focus($focusfield);
	}
}

function focus($focusfield)
{
	echo "\n<script language='JavaScript'>\n<!--\n".
		"\tfield=".
		 "document.".requirePar('page').".$focusfield;\n".
		"\tfield.focus();\n".
		"//-->\n</script>\n";
}

function aes2funcs_veryfancyautodisablingsubmitbuttonfeaturespace($submitknopname)
{
	return ' onsubmit="document.getElementById(\'' . $submitknopname . '\').disabled=true; return true;" ';
}

function startForm($next='',$mypage='',$enctype='',$method='',$action='',$extra='')
{
	if (!$next) {
		$next = requirePar('page');
	}
	if (!$mypage) {
		$mypage = '';
	}
	echo addForm($mypage, $method, $enctype, $next);
	echo addHidden('page', $next);
	if ($action) {
		echo addHidden('action', $action);
	}
	echo '<table'.($extra?" $extra":'').">\n";
}

function normalEndForm($ok_naam, $reset_naam = null)
{
	echo addFormEnd ( $ok_naam, ($reset_naam ? $reset_naam : FALSE) );
}

function endForm($submit,$reset=FALSE,$nobr=false)
{
	if ($nobr) {
		echo "<td><input type=\"submit\" value=\"$submit\"></td></tr></table>\n";
	} else {
		echo "</table><br><input type=\"submit\" value=\"$submit\">\n";
		if ( $reset !== FALSE ) {
			if ( !$reset ) $reset = 'Wis';
			echo "<INPUT type=\"reset\" value='$reset'>\n";
		}
	}
	echo "</form>\n";
}

$formstm->markEnd();
