<?php

namespace Space\Auth;

final class Constants
{
	/**
	 * @var \BestuursLid[]
	 */
	private $bestuur;
	/**
	 * @var int
	 */
	private $dibsTabletId;
	/**
	 * @var int
	 */
	private $gitWebhookId;
	/**
	 * @var int
	 */
	private $mollieWebhookId;

	/**
	 * Constants constructor.
	 * @param \BestuursLid[] $bestuur
	 * @param int $dibsTabletId
	 * @param int $gitWebhookId
	 * @param int $mollieWebhookId
	 */
	public function __construct($bestuur, $dibsTabletId, $gitWebhookId, $mollieWebhookId)
	{
		$this->bestuur = $bestuur;
		$this->dibsTabletId = $dibsTabletId;
		$this->gitWebhookId = $gitWebhookId;
		$this->mollieWebhookId = $mollieWebhookId;
	}

	public function getBestuurLidnrs()
	{
		return array_map(function($bestuurslid) {
			return $bestuurslid->getLidNummer();
		}, $this->bestuur);
	}

	public function getBoekcomLidnrs()
	{
		// WTF PHP, why array_filter(array, callback) but array_map(callback, array)...
		return array_map(function($boekcomlid) {
			return $boekcomlid->getLidNummer();
		}, array_filter($this->bestuur, function($bestuurslid) {
			return $bestuurslid->isBoekCom();
		}));
	}

	/**
	 * @return int
	 */
	public function getDibsTabletId()
	{
		return $this->dibsTabletId;
	}

	/**
	 * @return int
	 */
	public function getGitWebhookId()
	{
		return $this->gitWebhookId;
	}

	/**
	 * @return int
	 */
	public function getMollieWebhookId()
	{
		return $this->mollieWebhookId;
	}
}
