<?php
// $Id$

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;

$authinittm = new ProfilerTimerMark('auth/init.php');
Profiler::getSingleton()->addTimerMark($authinittm);

global $logger, $session, $request;

// Token-class moet geladen worden voordat de sessie start. Er kunnen namelijk
// Token-objecten in de sessie van de gebruiker zitten, die kunnen enkel
// beschikbaar gemaakt worden als de Token-class beschikbaar is als de sessie
// start.
require_once("space/classes/Token.cls.php");

require_once("space/aes2funcs.php");        // myerror()

// Alleen session starten als dit een http-request is, anders heeft het toch
// geen zin
if (!is_script())
{
	$session = new Session(null, new NamespacedAttributeBag());
	$session->start();

	// voorkom flaters
	if (!DEBUG && $session->get('realuserid'))
	{
		die("Verboden met overridden lidnummers op de echte site te komen!");
	}

	// Vertaal het huidige lidnummer naar een mooie menselijk leesbare string,
	// zodat we het kunnen loggen.
	$lidnr = $session->has('mylidnr')
		? ("'{$session->get('mylidnr')}'")
		: 'NULL';
	$logger->debug('$mylidnr = ' . $lidnr);
}
// TODO: misschien is er een custom sessie nodig die een lidnr bevat

global $LEVEL, $DEBUG_SITES;
require_once 'space/debug_sites.php';

// Nummers van speciale 'beestje'-accounts
$DIBSTABLETNR = 8516;
$GITWEBHOOKNR = 13480;
$MOLLIEWEBHOOKNR = 12781;

global $auth;
$authConstants = new \Space\Auth\Constants($BESTUUR, $DIBSTABLETNR, $GITWEBHOOKNR, $MOLLIEWEBHOOKNR);
$auth = new \Space\Auth\Auth($request, $session, $authConstants, $LEVEL, $DEBUG_SITES);

// Variabele om te controleren of iemand in de spocie, intro of archief zit.
// (Oftewel, cienummer.) Wordt gebruikt in hasAuth()
define('INTROCIE', 803); // Intro2018
define('SPOCIE', 36);
define('TBCIE', 41);
define('VOC', 454);
define('VICIE', 310);
define('MEDEZEGGENSCHAP', 398);
define('PROMOCIE', 8);
define('ALUMNICIE', 576);
define('KASCOM', 782);
define('BOEKCOM_CIE', 265);
define('VAKIDIOOT', 47);

require("space/auth/autorisatie.php");

$authinittm->markEnd();
