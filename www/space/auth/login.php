<?php

/**
 * De controller-functie voor login via het a-eskwadraat account
 */
function login()
{
	global $logger;

	$lidnummer = null;
	try
	{
		$ww = Wachtwoord::geefLogin(tryPar('loginnaam'));
		if ($ww)
		{
			$lidnummer = $ww->authenticate(tryPar('password'));
		}
	}
	catch (Exception $e)
	{
		$logger->notice("Exception " . $e . " in space/login.php:login");
		// beetje lelijk, maar werkt wel
	}
	return voltooiLogin($lidnummer, false);
}

/**
 * @brief Pagina om in te loggen via het systeemaccount.
 *
 * Deze pagina mag niet verplaatst worden omdat de Sysop moet weten
 * op welke plek de IPA-info erbij moet komen.
 *
 * @site {/space/auth/systeem.php}
 */
function systeemLogin()
{
	global $request;

	// Haal de via systeemaccount ingelogde persoon binnen, met het lidnummer
	// uit het REMOTE_USER veld.
	// Let op, deze is niet ingevuld op de livesite.
	$lidnummer = $request->server->get('REMOTE_USER_MEMBERID');
	if (!$lidnummer)
	{
		// Op de livesite wordt gebruikt gemaakt van
		// REDIRECT_REMOTE_USER_MEMBERID, dus kijk of deze wel een waarde heeft.
		// Zie voor dezelfde info bug #7998
		$lidnummer = $request->server->get('REDIRECT_REMOTE_USER_MEMBERID');
	}

	// Als er geen lidnummer gevonden is, is de login niet gelukt, maar dit
	// wordt in deze functie afgehandeld:
	return voltooiLogin($lidnummer, true);
}

/**
 * Controleert of het lidnummer mag inloggen, bestaat, etc. en genereert de error-pagina als het niet mag.
 * Als het wel mag, wordt doorverwezen naar wat de REQUEST param 'next' bevat.
 * @param lidnummer het ID van het lid wat gepoogd wordt om in te loggen
 * @param viaSysteemLogin bool of er via het systeem wordt ingelogd
 */
function voltooiLogin($lidnummer, $viaSysteemLogin)
{
	global $auth;
	global $BESTUUR;
	global $request;
	global $session;
	global $uriRef;

	// construeer nextpage, waarheen we redirecten nadat er succesvol ingelogd is
	$nextpage = tryPar('next');

	$redirectRef = array('request' => $nextpage, 'params' => $uriRef['params']);

	if (!$lidnummer)
	{
		// helaas...
		$secretarismail = new HtmlAnchor("mailto:" . $BESTUUR['secretaris']->getEmailAdres(), _("Secretaris"));
		$sysopmail = new HtmlAnchor("mailto:sysop@a-eskwadraat.nl", _("SysOp"));

		$nieuwloginlink = new HtmlAnchor("/Service/Intern/Logindata/", _("een nieuwe login/wachtwoord aanvragen"));

		$page = new HTMLPage();
		$page->start(_('Inloggen mislukt'));

		// normale login:
		$page
			->add(new HtmlParagraph(_("Sorry, je login en/of wachtwoord zijn fout. Probeer het opnieuw.")))
			->add(new HtmlParagraph(_("Als je je wachtwoord en/of login vergeten bent, of er nog geen hebt, ga dan naar $nieuwloginlink.")));

		if ($viaSysteemLogin)
		{
			// systeem login:
			$page
				->add(new HtmlParagraph(_("Als je je <em>systeem</em>wachtwoord en/of login vergeten bent, of er nog geen hebt, vraag dan aan de $sysopmail een nieuwe wachtwoord.")));
		}

		$page
			->add(new HtmlParagraph(_("Als je geen lid meer bent, kun je nog wel inloggen, maar als je je wachtwoord een jaar niet gebruikt hebt is hij verwijderd. Je kunt ook dan $nieuwloginlink.")))
			->add(new HtmlParagraph(_("Biedt die optie geen soelaas, mail dan de $secretarismail of kom even langs  in de kamer.")));

		$page->add(new HtmlParagraph(makelogin($redirectRef)));

		return new PageResponse($page);
	}

	$persoon = Persoon::geef($session->get('mylidnr'));
	if ($persoon && $persoon->getOverleden())
	{
		// Overleden personen mogen niet meer inloggen omdat dat best vreemd zou zijn als het wel zou gebeuren.
		$secretarismail = new HtmlAnchor('mailto:' . $BESTUUR['secretaris']->getEmailAdres(), _('secretaris'));

		$page = new HTMLPage();
		$page->start(_('Overleden'));

		$page->add(new HtmlParagraph(_("In onze administratie staat de persoon als wie u probeert in te loggen op overleden.")))
			->add(new HtmlParagraph(_("Als dit niet klopt, neem dan contact op met de $secretarismail.")))
			->add(new HtmlParagraph(makelogin($redirectRef)));

		return new PageResponse($page);
	}

	// Voorkom session fixation aanval
	$session->migrate(true);

	// inloggen gelukt, zet session variabelen op basis daarvan.
	// vervang het huidige lidnummer door degene waarmee ingelogd is (of null als er niet goed ingelogd is).
	$session->set('mylidnr', $lidnummer);
	$session->set('veilig', true);

	if (DEBUG && $auth->isGodAchtig())
	{
		// Op de debug mag je vanaf het inloggen meteen je godrechten gebruiken.
		$session->set('use_god', true);
	}

	$session->save();
	return responseRedirect($nextpage);
}

/**
 * @brief Voer de 'kruip in de huid van'-actie uit.
 * Of je kan weer terug naar je eigen ziel gaan.
 *
 * Pas op: 'mylidnr' is de persoon waarmee de site moet omgaan (varieert door
 * het huidkruipen), terwijl $session->get('realuserid') dezelfde persoon is die
 * inlogde en blijft hetzelfde gedurende de hele sessie.
 *
 * @site{/Service/Intern/Lidswitch}
 */
function overrideuser()
{
	global $auth;
	global $session;

	$newlidnr = tryPar('overridelidnr');

	$auth->terugNaarJeEigenHuid();

	// Alleen goden mogen in iemands huid kruipen
	if ($auth->isGodAchtig($session->get('mylidnr')) && !empty($newlidnr) && $newlidnr != $session->get('mylidnr'))
	{
		if (Lid::geef($newlidnr))
		{
			$session->set('realuserid', $auth->getLidnr());
			$session->set('mylidnr', $newlidnr);
		}
		elseif (Persoon::geef($newlidnr))
		{
			return Page::responseRedirectMelding('/Service/',
				"Helaas, dit lidnummer is aan een Persoon gekoppeld
				ipv aan een Lid. Deze mogen (nog) niet inloggen op de website en je kan dus 
				niet in hun huid kruipen.");
		}
	}

	return responseRedirect('/Service');
}
