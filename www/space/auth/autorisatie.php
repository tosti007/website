<?php
/**
 * $Id$
 *
 * Hier wordt bepaald welk level iemand heeft, danwel of iemand een gegeven
 * level heeft.  Deze functies worden overal in de code gebruikt waar
 * onderscheid gemaakt wordt naar level.
 *
 * LEVELSYSTEEM
 * ------------
 * Ons levelsysteem bestaat uit twee lagen.
 *
 * LAAG 1: dit bestaat uit vier boxen; iemand op de site bevindt zich altijd in
 * precies 1 van deze boxen. Het systeem is incrementeel, als je in een hogere
 * box zit heb je ook de rechten van een lagere box. Dit zijn ze:
 * - gast (niet ingelogde users)
 * - ingelogd (wel ingelogde users)
 * - bestuur (ingelogde users die in het bestuur zitten)
 * - god (ingelogde users die in de webcie zitten)
 * Deze corresponderen ook met database users; dat zorgt ervoor dat je in elk
 * geval nooit meer permissies hebt dan andere leden van jouw box.
 *
 * Daarnaast (naast) is er een systeem van rechten. Voorbeelden zijn:
 * lid, oud-lid, actief lid, introcommissielid, tbclid, boekcom. Hier zit geen
 * hierarchie in, een spocielid heeft vooral andere rechten dan een
 * introkernlid, maar niet meer of minder.
 *
 * Bij de implementatie is het wel zo dat bestuur aan bijna alle rechten
 * voldoet, en god aan alle.
 *
 * In de code gebruik je alleen hasAuth("EENLEVEL"). Dat level is een van die
 * opties uit hasAuth;
 * bv: hasAuth('actief'), hasAuth('intro'), hasAuth('god'), returnt een boolean
 * of je minimaal dat level hebt. Of requireAuth, dat is een hasAuth maar geeft
 * direct een error als je dat level niet hebt.
 */

use Space\Auth\Auth;

$authorisatietm = new ProfilerTimerMark('authorisatie');
Profiler::getSingleton()->addTimerMark($authorisatietm);

/**
 * Gegeven een level (een van de waarden in de switch in deze functie),
 * retourneer een boolean of je *minstens* dit level hebt. Een test
 * hasAuth('actief') is ook true voor o.a. mensen met level bestuur en god.
 *
 * Geeft een error als het level niet bestaat.
 */
function hasAuth($level)
{
	global $auth;

	if (is_null($level))
	{
		// Geen idee waarom dit bestaat en de git blame helpt ook niet.
		// In ieder geval gaan scripts erop stuk dus ik zet er een waarschuwing op.
		// Hopelijk kunnen we het dan deprecaten.
		$logger->info("Autorisatieniveau NULL is deprecated.");
		if (DEBUG)
		{
			user_error("Autorisatieniveau NULL is deprecated.", E_USER_NOTICE);
		}
		return false;
	}

	$lidnr = $auth->getLidnr();

	// god mag per definitie alles, op live alleen wanneer getoggled
	if ($auth->isGod($lidnr))
	{
		return true;
	}

	switch (strtolower($level))
	{
		case 'gast':
			return true;
		case 'actief':
			$pers = Persoon::getIngelogd();
			return $pers && $pers->getCommissies()->aantal() > 0;
		case 'bijnalid':
			// bijna lid of huidig lid
			return Lid::getIngelogdLid();
		case 'lid':
			// huidig lid
			$lid = Lid::getIngelogdLid();
			return $lid && $lid->isHuidigLid();
		case 'oud-lid':
			// huidig lid of oud lid
			$lid = Lid::getIngelogdLid();
			return $lid && ($lid->isHuidigLid() || $lid->isOudLid());
		case 'boekcom':
			if ((DEMO || DEBUG) && hasAuth('kascom'))
			{
				return true;
			}
			return $auth->isBoekenCommissaris($lidnr);
		case 'ingelogd':
			return $lidnr;
		case 'verkoper':
			if ((DEMO || DEBUG) && hasAuth('kascom'))
			{
				return true;
			}
		case 'bestuur':
			return $auth->isBestuur($lidnr);

		case 'tablet':
			return $auth->isDiBSTablet($lidnr);

		// Om mailings aan te passen, moet je in een van de volgende cies zitten,
		// want zo'n beetje elke mailing heeft een andere commissie
		// bijvoorbeeld spocie doet carrieremails en promocie activiteitenmails
		// (lang geleden hebben de wijzen van IBA besloten dat dit een goed idee is)
		case 'mailings':
			return hasAuth('spocie') || hasAuth('promocie') || hasAuth('vakidioot');

		/* TODO FIXME dit kan netter ... */
		case 'spocie':
			return $auth->hasCieAuth(SPOCIE);
		//Ook sponsorcommissarissen hebben toegang tot spookweb.
		case 'spookweb':
			if ($auth->hasCieAuth(SPOCIE))
			{
				return true;
			}

			$pers = Persoon::getIngelogd();
			return $pers && $pers->isSponsorCommisaris();
		case 'intro':
			return $auth->hasCieAuth(INTROCIE);
		case 'medezeggenschap':
			return $auth->hasCieAuth(MEDEZEGGENSCHAP);
		case 'tbc':
			return $auth->hasCieAuth(TBCIE);
		case 'vicie':
			return $auth->hasCieAuth(VICIE);
		case 'promocie':
			return $auth->hasCieAuth(PROMOCIE);
		case 'alumnicie':
			return $auth->hasCieAuth(ALUMNICIE);
		case 'kascom':
			return $auth->hasCieAuth(KASCOM);
		case 'vakidioot':
			return $auth->hasCieAuth(VAKIDIOOT);
		case 'god':
			// alle niet goden mogen dit niet
			// en goden worden bovenaan deze functie afgevangen
			return false;
		default:
			user_error("Autorisatie-niveau '$level' is onbekend!", E_USER_ERROR);
	}
}

/**
 * @brief Gooit een error als het rechtenniveau te laag is.
 *
 * Doet niets als de huidige gebruiker genoeg rechten heeft,
 * en gooit een AutorisatieError als de gebruiker niet genoeg rechten heeft.
 * Deze error wordt in principe in space.php afgevangen en omgezet in een 403-pagina.
 *
 * Gebruikt hasAuth voor de rechtencheck.
 *
 * @sa hasAuth
 * @param level Het autorisatieniveau dat benodigd is.
 * @returns Niets, of throwt een AutorisatieError.
 */
function requireAuth($level)
{
	global $logger;
	if (!hasAuth($level))
	{
		$logger->info('Autorisatielevel ' . $level . ' nodig maar niet gehaald');
		// TODO: misschien hier iets explicieter zijn en een AutorisatieError geven,
		// die verderop afvangen tot 403?
		spaceHTTP(403);
	}
}
/**
 * Geef de gebruikersnaam voor de database om in te loggen met het juiste auth-niveau.
 *
 * @see Auth:getLevel
 * @see sqlpass
 *
 * @return string
 * Een string met de databaasgebruikersnaam.
 */
function getDatabaseUsername()
{
	global $auth;

	$level = $auth->getLevel();
	// We hebben geen apart account voor bestuur.
	if ($level == 'bestuur')
	{
		$level = 'god';
	}

	return DB_USER_PREFIX . $level;
}

/**
 * Hulpfunctie: haal het database-password voor huidige level uit
 * secret.php, om in te loggen in de db.
 *
 * @see getDatabaseUsername
 *
 * @return string
 * Een string met het databaaswachtwoord.
 */
function sqlpass()
{
	$pass = secret::dbpasswords();
	$user = getDatabaseUsername();
	return $pass[$user];
}

/**
 *    Invoer is de $entry->url() van benamite waarvan we willen weten of die
 *    geedit mag worden door de huidig ingelogde persoon.
 *
 *    Bestuur/god mogen alles editen, verder alleen als je in de cie zit.
 */
function hasPublisherAuth($benamite_path)
{
	global $auth;

	$pad = explode('/', $benamite_path);

	if (hasAuth('promocie'))
	{
		return true;
	}

	if ($pad[1] == 'Vereniging' && $pad[2] == 'Commissies' && !empty($pad[3]))
	{
		$cie = Commissie::cieByLogin($pad[3]);
		if ($cie instanceof Commissie)
		{
			return $cie->hasLid(Persoon::getIngelogd());
		}
		return false;
	}
	elseif ($pad[1] == 'Activiteiten' && !empty($pad[2]) && !empty($pad[3]) && !empty($pad[4]))
	{
		if ($pad[2] == '*' || $pad[4] == '*')
		{
			return false;
		}
		$act = Activiteit::geef($pad[3]);
		if ($act instanceof Activiteit)
		{
			return $act->getCommissies()->hasLid(Persoon::getIngelogd());
		}
		return false;
	}
	elseif ($pad[1] == 'Onderwijs' && $pad[2] == 'Medezeggenschap')
	{
		return hasAuth('medezeggenschap');
	}
	elseif ($pad[1] == 'Leden' && $pad[2] == 'Alumni')
	{
		return hasAuth('alumnicie');
	}
	else
	{
		// elders mag alleen >= bestuur
		return false;
	}

	return $cienr && $auth->hasCieAuth($cienr);
}

$authorisatietm->markEnd();
