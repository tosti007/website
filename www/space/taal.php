<?php
// $Id: taal.php 7647 2007-05-21 15:07:17Z kink $

function getLang()
{
	global $session, $request;

	/* Language keuze:
	* 1) vlaggetje geklikt ( $_SESSION['language_override'] )
	* 2) expliciete lidsetting ( ->getVoorkeur()->getTaal() )
	* 3) browser voorkeur (algoritme)
	* 4) impliciete lidsetting ( ->isLidMaster() )
	* 5) default (nl)
	*/

	// 1) Vlaggetje
	if ($session && $session->get('language_override')) {
		return $session->get('language_override');
	}

	// command-line -> nederlands
	if (is_script())
	{
		return 'nl';
	}

	// 2) Kolom in 'leden' tabel
	$ingelogd = Persoon::getIngelogd();
	if ($ingelogd)
	{
		$taalvoorkeur = Persoon::getIngelogd()->getVoorkeur()->getTaal();
		if($taalvoorkeur != 'GEEN')
			return strtolower($taalvoorkeur);
	}

	// 3) FIXME: browser voorkeur nog niet geimplementeerd - is dit een goed idee? heel veel staan per ongeluk op Engels

	// 4) Ingelogd masterlidmaatschap zonder taalvoorkeur -> engels
	if ($ingelogd && $ingelogd->getLidMaster())
	{
		return "en";
	}

	// 5)
	return "nl";
}

function nl_en($nl, $en)
{
	return getLang() == "en" ? $en : $nl;
}

function gettext_init($overrulelanguage = FALSE)
{
	global $logger;

	if (!$overrulelanguage) {
		$overrulelanguage = getLang();
	}

	switch ($overrulelanguage) {
		case 'en': $locale = setlocale(LC_ALL, 'en_GB'); break;
		case 'nl': $locale = setlocale(LC_ALL, 'nl_NL'); break;
		default: user_error('Onbekende taal ' . $overrulelanguage . ' voor gettext', E_USER_ERROR);
	}

	$logger->debug('$overrulelanguage = ' . $overrulelanguage);
	$logger->debug('$locale = ' . $locale);

	// Returnwaarde van setlocale is de (nieuwe) locale, of false bij een error.
	if ($locale === false)
	{
		// Heb je deze error maar heb je wel een locale 'nl_NL.UTF-8'?
		// Je moet ook een locale 'nl_NL' aanmaken!
		user_error('setlocale voor taal ' . $overrulelanguage . ' gefaald: is die wel geinstalleerd?', E_USER_ERROR);
	}

	// FIXME: .. is evil
	$gettext_root = FS_ROOT."/../gettext";
	$LC_PATH = "$gettext_root/$locale/LC_MESSAGES";
	$domain = "aes-www";

	// pad naar mo-bestand
	$filename = "$LC_PATH/$domain.mo";
	$mtime = filemtime($filename); //check de tijd voor laatst gewijzigd
	// Nieuw mo-bestand
	$filename_nieuw = "$LC_PATH/{$domain}_{$mtime}.mo";

	// Kijk of het bestand al hebben, zo niet, gooi alle eerder gemaakte kopieen weg en kopieer de oude
	if(!file_exists($filename_nieuw)) {
		$files = preg_grep('/^([^.])/', scandir("$LC_PATH/"));
		foreach($files as $file) {
			if(is_file("$LC_PATH/$file") && $file !== "$domain.mo") {
				unlink("$LC_PATH/$file");
			}
		}
		copy($filename, $filename_nieuw);
	}

	$domain_nieuw = "{$domain}_{$mtime}";

	bindtextdomain($domain_nieuw, $gettext_root);
	bind_textdomain_codeset($domain_nieuw, "UTF-8");
	textdomain($domain_nieuw);

	$gettext_result = explode(': ',
	// voc: Vertaal dit met "bla bla...: TAAL (bijv. 'EN')
		_("[INTERNAL] Current language is: DEFAULT"));
	$gettext_result = strtolower($gettext_result[1]);

	// En ik (sjeik) citeer Wolffelaar:
	// Hij probeert iets te vertalen. Als de taal niet Nederlands is, dan wordt gecontroleerd
	// of het resultaat van gettext wel de originele string is (default, als er geen vertaling is)
	if ($gettext_result != 'default') {
		if ($gettext_result != $overrulelanguage) {
			user_error("Taal werkt niet met gettext: $overrulelanguage (is
				$gettext_result)", E_USER_WARNING);
		}
	}
}
