<?php

global $logger, $logHistory;

/**
 * @brief Loghandler die records bewaart.
 *
 * Monolog bewaart geen logrecords maar die willen we wel hebben voor (error)pages.
 */
class LogBewaarHandler extends \Monolog\Handler\AbstractProcessingHandler
{
	/**
	 * @brief Hierin staat een array van alle logrecords.
	 *
	 * Het formaat hiervan is dat van Monolog,
	 * dus een array met velden `message`, `context`, `level`, `level_name`,
	 * `channel`, `datetime`, `extra` en `formatted`.
	 *
	 * Dit wordt in de Whoops-pagina en HTMLPage uitgelezen.
	 */
	public $records;

	public function __construct($logger = \Monolog\Logger::DEBUG, $bubble = true)
	{
		parent::__construct($logger, $bubble);
		$this->records = [];
	}

	protected function initialize()
	{
		return true;
	}

	protected function write(array $record)
	{
		$this->records[] = $record;
	}
}

/**
 * @brief De generieke logger, waar je in principe elke melding aan kan toevoegen.
 *
 * Op debug:
 *  * meldingen komen in `wwwdebug/www/log/main.log` (wordt automatisch dagelijks geroteerd)
 *  * meldingen komen onder het debug-knopje op HTMLPages (indien gegenereerd)
 *  * meldingen komen op de 'Ĝi estas rompita'-pagina van Whoops (indien gegenereerd)
 * Op live en demo:
 *  * meldingen met level >= NOTICE komen in `/var/log/www/php-warnings.log`
 *  * meldingen met level >= NOTICE komen hooguit 1x per minuut in errormails
 *
 * (Zie ook de code hieronder.)
 *
 * Om de loggeschiedenis uit te kunnen lezen,
 * wordt op DEBUG een kopietje van elk logbericht in `$logHistory` gestopt.
 *
 * @see logHistory
 */
$logger = new \Monolog\Logger('main');

// Formattering voor entries in logbestanden.
$dateFormat = "Y-m-d H:i:s";
$output = "%datetime% > %level_name%\n%message%\n";
$formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true);

// Stel in waar de logs heengaan.
// Als je dit aanpast, vergeet dan niet de documentatie voor $logger aan te passen!
if (DEBUG)
{
	// Stop alles in een file in de debug.
	// Workaround: omdat Monolog alle files chmod't op onleesbaar voor anderen,
	// moeten we de filenaam aanpassen aan de hand van de huidige user.
	$processUser = posix_getpwuid(posix_geteuid());
	$stream = new \Monolog\Handler\RotatingFileHandler(FS_ROOT . '/log/' . $processUser['name'] . '.log', 14);
	$stream->setFormatter($formatter);
	$logger->pushHandler($stream);

	// Schrijf het ook naar stderr, zodat we het in `php -t` kunnen zien.
	$stream = new \Monolog\Handler\StreamHandler('php://stderr', \Monolog\Logger::INFO);
	$stream->setFormatter($formatter);
	$logger->pushHandler($stream);

	// Schrijf het ook naar stderr, zodat we het in `php -t` kunnen zien.
	$stream = new \Monolog\Handler\StreamHandler('php://stderr', \Monolog\Logger::INFO);
	$stream->setFormatter($formatter);
	$logger->pushHandler($stream);

	// TODO: als we PHP 7 draaien en Monolog updaten,
	// kunnen we dan misschien ook de BrowserConsoleHandler gebruiken?

	// Bewaar log records zodat we die in de page kunnen uitlezen.
	$dateFormat = "Y-m-d H:i:s";
	$output = "%level_name%: %message%";
	$historyFormatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true);
	$logHistory = new LogBewaarHandler();
	$logHistory->setFormatter($historyFormatter);
	$logger->pushHandler($logHistory);
}
else
{
	// Stop notices in de /var/log-map.
	$stream = new \Monolog\Handler\FingersCrossedHandler(new \Monolog\Handler\StreamHandler('/var/log/www/php-warnings.log', \Monolog\Logger::DEBUG), \MonoLog\Logger::INFO, 0, true, false);
	$stream->setFormatter($formatter);
	$logger->pushHandler($stream);

	// Maak een MailHandler die bij elke fout een html-mailtje stuurt
	$htmlFormatter = new \Monolog\Formatter\HtmlFormatter();
	$mailHandler = new \Monolog\Handler\NativeMailerHandler('webcie@a-eskwadraat.nl', 'ERROR - '.time(), 'webcie-errors@a-eskwadraat.nl', \Monolog\Logger::DEBUG, true, 120);
	$mailHandler->setContentType('text/html');
	$mailHandler->setFormatter($htmlFormatter);

	// De deduplicateHandler zorgt ervoor dat als dezelfde fout binnen 60 seconden
	// nog een keertje gebeurt hij niet gemaild wordt.
	$deduplicationHandler = new \Monolog\Handler\DeduplicationHandler($mailHandler, null, \Monolog\Logger::DEBUG, 60);
	$doorgeefHandler = new \Monolog\Handler\FingersCrossedHandler($deduplicationHandler, \MonoLog\Logger::NOTICE);
	$logger->pushHandler($doorgeefHandler);
}
