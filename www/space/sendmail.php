<?php
// $Id$

/**
* Stuur een mailtje door het aanroepen van een externe binary,
* en geef een melding als dat fout gaat. In DEBUG wordt de tekst
* op het scherm gezet.
*
* @param $from		from-adres
* @param $to		to-adres, OF lidnr
* @param $subject	subject
* @param $text		inhoud v/h mailtje
* @param $replyto (optional)	reply-to-adres
* @param $extra (optional)	eventuele extra mailheader inhoud
* @param $bounceto (optional) adres om bounce heen terug te knallen
* @param $html (optional) maak het html
*/
function sendmail($from, $to, $subject, $text, $replyto='', $extra='', $bounceto='', $html = false)
{
	global $request;

	if (!$from || !$to) {
		user_error("Geen from of to bij versturen van e-mail!", E_USER_ERROR);
	}
	require_once('mail_headers.php');

	if(is_numeric($to)) {
		$lidnr = (int)$to;

		$naam = PersoonView::naam(Persoon::geef($lidnr));
		$mail = Contact::geef($lidnr)->getEmail();

		if(!$mail) {
			user_error("Lid $naam ($lidnr) heeft geen e-mailadres, kan niet mailen.", E_USER_NOTICE);
			Page::addMelding(_("$naam ($lidnr) heeft geen e-mailadres en er is dus geen email gestuurd."));
			return;
		}

		$to = '"' . encodeHeader($naam) . '" <' . $mail . '>';
	}
	
	//Bepaal content-type, kan html/plain-text of zelf geset zijn
	if(preg_match('/content-type:/i', $extra))
		$contentType = '';
	else if($html)
		$contentType = "Content-Type: text/html\n";
	else
		$contentType = "Content-Type: text/plain; charset=utf-8\n";

	// In de headers van mailings op mailingweb staat ook een MIME-Version
	// Twee mimeVersions mag niet dus de volgende regels zijn om dat af 
	// te vangen
	$mime = "MIME-Version: 1.0\n";

	if($extra) {
		$mimes = explode(';', $extra);
		foreach($mimes as $i => $m) {
			if(strpos(strtoupper($m), 'MIME') !== FALSE) {
				$mime = '';
			}
		}
		$extra = implode(';', $mimes);
	}

	$mailcontent = $mime .
		$contentType .
		"Content-Transfer-Encoding: 8bit\n" .
		($replyto ? ("Reply-To: " . trim($replyto) . "\n"):'') .
		($extra ? trim($extra) . "\n": '') .
		"\n" . $text;

	$themail = "From: " . trim($from) . "\n" .
		"Sender: " . trim($from) . "\n" .
		"To: " . trim($to) . "\n" .
		"Subject: " . encodeHeader(trim($subject)) . "\n" .
		$mailcontent;

	if (DEBUG) {
		// Als de pagina al gestart is: even bufferen
		// Als de pagina nog niet gestart is, komt de melding meteen op de pagina
		$ingelogde = Persoon::getRealIngelogd();
		if ($ingelogde) {
			$standaardEmail = $ingelogde->getEmail();
		} else
			$standaardEmail = $_SERVER['SERVER_ADMIN'];

		$stuurForm = HtmlForm::named("debug-send-mail", "/Ajax/StuurDebugMail");
		$tekst = "Op de livesite zou er een mail gestuurd zijn" .
				(isset($lidnr) ? " (lidnr: $lidnr)" : '') .
				". Vul hieronder in waar je de mail wil ontvangen:";

		$stuurForm->setAttribute('onsubmit', "$.ajax({type:'POST',url:$(this).attr('action'),data:$(this).serialize(),success:function(r){alert('Toppie!');}}); return false;");
		$stuurForm->addChildren(array(
			HtmlInput::makeHidden('from', $from),
			HtmlInput::makeHidden('to', $to),
			HtmlInput::makeHidden('subject', $subject),
			HtmlInput::makeHidden('mailcontent', $mailcontent),
			new HtmlParagraph($tekst),
			new HtmlDiv(array(
				$textField = HtmlInput::makeText('address', $standaardEmail, 30, null, null, _("e-mailadres")),
				new HtmlSpan(array(
					HtmlInput::makeSubmitButton('Diebug!!', 'submit', 'btn btn-secondary'),
					$btn = new HtmlButton('button', HtmlSpan::fa('file-code-o', 'Bekijk inhoud'), null, null, 'btn btn-primary')
				), 'input-group-btn')), 'input-group'),
			$pre = new HtmlPre(htmlspecialchars(quoted_printable_decode($themail)))
		));
		$btn->setAttribute('onclick', '$(this.parentElement.parentElement.nextElementSibling).collapse(\'toggle\');');
		$pre->addClass('collapse');

		Page::addMelding($stuurForm->makeHtml());
		return;
	}

	//Stuur geen mail, maar laat ook geen extra's zien.
	if (DEMO) {
		return;
	}

	forceSendMail($from, $to, $themail, $bounceto);
}

/**
 * Stuurt een mailtje door het aanroepen van een externe library (sendmail).
 * Het geeft een foutmelding als het fout gaat.
 * In tegenstelling tot sendmail stuurt dit altijd de mail, ook op DEBUG dus pas op!
 *
 * @see sendmail
 *
 * @param $from		het mailadres van de afzender
 * @param $to		het mailadres waarnaartoe gestuurd wordt
 * @param $themail	de totale content van alle mail-headers en de mail zelf
 * @param $bounceto (optional) adres om bounce heen terug te knallen
 */
function forceSendMail($from, $to, $themail, $bounceto)
{
	global $logger;
	if (DEBUG)
	{
		$logger->info("Op DEBUG wordt een mail gestuurd, naar $to");
	}

	if($bounceto) {
		//Note: Mag maar een adres zijn!
		$bouncestr = "-f '$bounceto'";
	} elseif(!empty($lidnr)) {
		$bouncestr = '-f bounce-'.$lidnr;
	} else {
		$bouncestr = '';
	}

	$stream = popen( "/usr/sbin/sendmail -bm -oi -t $bouncestr", 'w');
	if (!$stream)
		user_error("Fout bij openen /usr/sbin/sendmail", E_USER_ERROR);
	if (!fwrite($stream, $themail))
		user_error("Fout bij schrijven naar /usr/sbin/sendmail", E_USER_ERROR);
	if ($ret = pclose($stream))
		user_error(sprintf("Fout bij versturen van e-mail van %s naar %s (%s)!", $from, $to, $ret), E_USER_ERROR);
}

