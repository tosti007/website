<?php

namespace Space;

use vfsEntry;

// We worden aangeroepen in www/space, maar space gaat ervan uit dat we in de webrootdir zitten.
chdir('..');

// auto_prepend moet voor alle andere php-files geincluded worden
require_once('space/auto_prepend.php');

// Laad ook alle andere dependencies in.
require_once('space/init.php');

global $logger;

// Initialiseer Space.
$space = new Space();

// Beslis of we een functie/methode gaan aanroepen of alles door Benamite halen.
if ($_SERVER['SPACE_SPECIFIED'] == 'true')
{
	// We ondersteunen geen legacy code in gespecifieerde mode.
	setZeurmodus();

	$bestand = $_SERVER['SPACE_FILE'];
	$functieNaam = $_SERVER['SPACE_FUNCTION'];

	// We moeten eerst wat requestglobals aanmaken.
	$space->initGlobals();

	// We moeten het bestand sowieso importeren.
	// TODO: Dit kan in deprecated gevallen leiden tot output,
	// vang dat af!
	require_once($bestand);
	// Bekijk wat voor functie we krijgen.
	// Dit gaat via vfsEntry want in feite zijn we het vfs aan het uitbesteden aan de aanroeper.
	$functie = vfsEntry::getMethodFromFuncString($functieNaam);
	$response = $functie();
}
else
{
	// Geen argument gekregen, dus haal door Benamite dmv de Space-klasse.
	global $request;
	$path = $request->server->get('PATH_INFO');
	if ($path == null)
	{
		// Geen request binnengekregen:
		// heeft de server misschien ons per ongeluk aangeroepen,
		// in plaats van space-cli?
		$logger->warning('space-cli aangeroepen zonder pad! :S');
		$response = responseUitStatusCode(400);
	}
	else
	{
		$response = $space->urlResponse($path);
	}
}

$response->send();
return;
