<?php
// $Id$

/***
 * Beheer en volg shortcuts (zoals www.a-eskwadraat.nl/av) die alle
 * commissies zelf in kunnen stellen als snelkoppeling naar een
 * diepergelegen onderdeel van de site
 *
 * Benamite structuur:
 *    /Service/Intern/Shortcuts
 *      /index.html     bestand (FS)    $file:shortcutlist
 *      /*              varEntry        $file:shortcut
 *        /Wijzig       bestand (FS)    $file:shortcutedit
 *        /Verwijder    bestand (FS)    $file:shortcutdel
 */

/***
 * Probeer $rest[0] te matchen met een shortcuts, dan wel een cielogin.
 * $rest[1+] wordt aan de redirect target geappend.
 *
 * Deze content hook hangt aan de site_live entry ('/').
 */
function followshortcut_content($rest)
{
	global $BMDB, $request;

	if(urlIsEmpty($rest))
	{
		return responseRedirect('/Vereniging/');
	}

	$short = array_shift($rest);

	$shortcut = $BMDB->q('MAYBETUPLE SELECT *'
					.	' FROM `shortcuts`'
					.	' WHERE `name` = %s'
					, $short);

	$target = $shortcut['target'];

	if($shortcut["english"]){
		// zorg ervoor dat je url lijkt op /map/pagina=met_vage?tekens/ voordat we er ?setlanguage=en achter plakken
		if(!preg_match("(\.[A-Za-z]+$|/$|\=[A-Za-z0-9\%\.-_~]+$)", $target)){
			$target .= "/";
		}
		//Ga goed om met alle getparams in je url.
		$params = $request->query->all();
		$params["setlanguage"] = "en";
		$target .= strstr($target, '?', true) . '?' . http_build_query($params);
	}

	// is $short anders soms een $cielogin ?
	if(!$target) {
		$cie = Commissie::cieByLogin($short);
		if($cie) {
			$target = $cie->getHomepage();
			if(substr($target, 0, 7) != 'http://')
				$target = $cie->getURLBase('');
		}
	}

	// geen matchende redirect target :/
	if(!$target)
	{
		return responseUitStatusCode(404);
	}

	if(count($rest)) {
		if(substr($target, -1) != '/')
			$target .= '/';
		$target .= $tail = implode('/', $rest);
	}
	return responseRedirect($target.vfsGenParams());
}

/**
 * @brief Toon een lijst met alle shortcuts.
 * Shortcuts van eigen commissies mogen gewijzigd worden.
 * Bestuur kan ook nieuwe shortcuts aanmaken.
 *
 * @return Een Response.
 */
function shortcutlist()
{
	global $BMDB, $uriRef;
	$ingelogd = Persoon::getIngelogd();
	if (!$ingelogd)
	{
		return responseUitStatusCode(403);
	}
	$mycies = $ingelogd->getCommissies();

	$allowed = hasAuth('bestuur');
	$ret = False;

	if($allowed) {
		$ret = shortcutlist_addform();

		$tbl = $BMDB->q('TABLE SELECT *'
					.   ' FROM `shortcuts`'
					.   ' ORDER BY `name`'
					);
	} else {
		$tbl = $BMDB->q('TABLE SELECT *'
					.   ' FROM `shortcuts`'
					.   ' WHERE `cienr` in (%Ai)'
					.   ' ORDER BY `name`'
					,	$mycies->keys()
					);
	}

	$page = new HTMLPage();
	$page->addHeadCSS(Page::minifiedFile('publisher.css', 'css'));
	$page->addFooterJS(Page::minifiedFile('publisher.js'));
	$page->start();

	if($allowed) {
		if($ret === True)
			$page->add(new HtmlParagraph(
						new HtmlEmphasis(_('shortcut toegevoegd'))));
		if(is_string($ret))
			$page->add(new HtmlParagraph(array
						( _('Fout opgetreden')
						, new HtmlBreak()
						, $ret
						), 'error'));
		$page->add(new HtmlParagraph($a = new HtmlAnchor(
			'#shortcut_toevoegen', _("Nieuwe shortcut toevoegen"))));
		$a->addClass('btn btn-primary');
	} else {
		$page->add(new HtmlParagraph(new HtmlEmphasis(
					_("Alleen bestuursleden kunnen nieuwe shortcuts aanmaken."))));
	}

	// pre-cache cie & wie
	$precache['cie'] = array();
	$precache['wie'] = array();
	foreach($tbl as $row) {
		$precache['cie'][] = $row['cienr'];
		$precache['wie'][] = $row['wie'];
	}
	Commissie::cache($precache['cie']);
	Persoon::cache($precache['wie']);

	$page->add(new HtmlHeader(2, _("Shortcuts van jouw commissies")));
	$page->add($htmltbl = new HtmlTable(NULL, 'publisher-entrylist'));
	$tr = $htmltbl->addRow();
	$tr->addHeader(_("Van"));
	$tr->addHeader(_("Naar"));
	$tr->addHeader(_("Engels"))->setCssStyle('width: 55px;');
	$tr->addHeader(_("Commissie"));
	$tr->addHeader(_("Laatst gewijzigd"))->setCssStyle('width: 210;');
	$tr->addHeader('&nbsp;')->colspan(2);

	foreach($tbl as $row)
	{
		$cie = Commissie::geef($row['cienr']);
		$wie = Persoon::geef($row['wie']);

		$tr = $htmltbl->addRow()->addClass('entry');
		$tr->addData($row['name']);
		$tr->addData(new HtmlAnchor($row['target'], $row['target']));

		if($row['english'] == 1){
			$tr->addData(new HtmlImage("/Layout/Images/Icons/english.png", _("Engels")))->setCssStyle('text-align: center;');
		} else {
			$tr->addData("&nbsp;");
		}

		$tr->addData(CommissieView::waardeNaam($cie));
		$tr->addData(sprintf(_('Aangepast op %s door %s')
					, $row['wanneer'] . new HtmlBreak()
					, PersoonView::naam($wie)
					))->addClass('lastmodified');

		if($allowed || isset($mycies[$row['cienr']])) {
			$tr->addData(new HtmlAnchor($uriRef['entry']->getParent()->url()
					. $row['name'] . '/Wijzig'
					, $img = new HtmlImage('/Layout/Images/Buttons/edit.png', _("Bewerken"))
					));
			$img->setAttribute('title', _('Bewerken'));
		}
		if($allowed) {
			$tr->addData(new HtmlAnchor($uriRef['entry']->getParent()->url()
					. $row['name'] . '/Verwijder'
					, $img = new HtmlImage('/Layout/Images/Buttons/delete.png', _("Verwijderen"))
					));
			$img->setAttribute('title', _('Verwijderen'));
		}
	}

	if($allowed) {
		$a = new HtmlAnchor();
		$page->add($a->setName('shortcut_toevoegen'));
		$page->add(new HtmlHeader(2, _("Voeg er eentje toe")));
		$page->add($form = HtmlForm::named('addshortcut'));
		$form->addClass('form-inline');

		$form->add($htmltbl = new HtmlTable());
		$tr = $htmltbl->addRow();
		$tr->addData(_("Van:"));
		$tr->addData(array
			( new HtmlSpan('http://www.A-Eskwadraat.nl/', 'monospace')
			, HtmlInput::makeText('name', $ret === True ? '' : tryPar('name'), 40)
			));
		$tr = $htmltbl->addRow();
		$tr->addData(_("Naar:"));
		$tr->addData(array
			( new HtmlSpan('http://www.A-Eskwadraat.nl', 'monospace')
			, HtmlInput::makeText('target', $ret === True ? '/Vereniging/Commissies/' : tryPar('name'), 40)
			));

		$tr = $htmltbl->addRow();
		$tr->addData(_('Link naar Engelse site:'));
		$tr->addData(HtmlInput::makeCheckbox('english', false));

		$tr = $htmltbl->addRow();
		$tr->addData(_("Namens commissie:"));

		$selectbox = new HtmlCommissieSelectbox();
		$selectbox->setCommissies(CommissieVerzameling::wijzigbare());
		$selectbox->setSelectedCommissie(tryPar('commissie'));

		$tr->addData($selectbox);

		$form->add(HtmlInput::makeSubmitButton(_('Voeg toe')));
	}

	return new PageResponse($page);
}

/**
 * Kijk of de gegeven bestandsnaam een beetje zinnig is.
 * (Dat wil zeggen, bevat het geen gekke tekens als '<' of '🚂' of '/')
 * @param url De bestandsnaam om te controleren
 * @returns True als de bestandsnaam geen gekke tekens bevat.
 */
function check_shortcut_naam($url)
{
	return !preg_match('/[^a-z0-9.\-]/i', $url);
}
/**
 * Kijk of de gegeven url een beetje zinnig is.
 * (Dat wil zeggen, bevat het geen rare tekens als '<' of '🚂')
 * @param url De url om te controleren
 * @returns True als de url geen gekke tekens bevat.
 */
function check_shortcut_url($url)
{
	return !preg_match("/[^a-z0-9.!@$^&*()_=,\-\?\/]/i", $url);
}

function shortcutlist_addform()
{
	global $BMDB;
	global $auth;

	if (Token::processNamedForm() != 'addshortcut')
	{
		return false;
	}

	$from = requirePar('name');
	$target = requirePar('target');
	$english = requirePar('english');
	$cienr = (int)requirePar('commissie');

	if ($english == 'on')
	{
		$english = true;
	}
	else
	{
		$english = false;
	}

	if (!check_shortcut_naam($from))
	{
		return _('De "van" van een shortcut mag geen gekke tekens bevatten, Baron Van van Gekkenstein!');
	}
	if (!check_shortcut_url($target) || $target{0} != '/')
	{
		return _('De "naar" van een shortcut mag geen gekke tekens bevatten, en moet met een slash beginnen, gekko');
	}

	if (!CommissieVerzameling::wijzigbare()->bevat($cienr))
	{
		return _('Ongeldige cie');
	}

	$exists = $BMDB->q('MAYBEVALUE SELECT `name`'
		. ' FROM `shortcuts`'
		. ' WHERE `name` = %s'
		, $from);
	if ($exists)
	{
		return sprintf(_("Er bestaat al een shortcut naar '%s'", $from));
	}

	$BMDB->q('INSERT INTO `shortcuts` SET %S', [
			'name' => $from,
			'target' => $target,
			'cienr' => $cienr,
			'english' => $english,
			'wie' => $auth->getLidNr(),
			'wanneer' => getDateTime(),
		]
	);
	return true;
}

function shortcut($args)
{
	global $BMDB;
	$data = $BMDB->q('MAYBETUPLE SELECT `name`, `cienr`'
					.' FROM `shortcuts`'
					.' WHERE `name` = %s'
					, $args[0]
					);

	if(!$data)
		return False;

	if(hasAuth('bestuur')) {
		return array( 'name'   => $data['name']
					, 'access' => true
					);
	}

	return array( 'name'   => $data['name']
				, 'access' => CommissieVerzameling::wijzigbare()->bevat($data['cienr'])
				);
}

/**
 * @brief Editpagina voor shortcuts.
 */
function shortcutedit()
{
	global $BMDB, $uriRef;

	$data = $BMDB->q('MAYBETUPLE SELECT *'
				.   ' FROM `shortcuts`'
				.   ' WHERE `name`  = %s'
				,	vfsVarEntryName()
				);

	// Verwerk de wijziging indien relevant.
	// Geeft een string met errors als er iets fout is,
	// of true als er succes was.
	$verwerking = shortcutedit_form($data);
	if ($verwerking === true) // let op: $verwerking kan truthy en tegelijk niet === true zijn!
	{
		return responseRedirect('..');
	}

	$page = new HTMLPage();
	$page->start();

	$page->setLastModified($data['wanneer']);
	$page->setLastModifier($data['wie']);

	if(is_string($verwerking)) {
		$page->add(new HtmlParagraph(sprintf(_('Fout opgetreden: %s'), $verwerking), 'error'));
	}

	$page->add(new HtmlHeader(2, _("Edit shortcut")));
	$page->add($form = HtmlForm::named('edit'));
	$form->addClass('form-inline');
	$form->add($tbl = new HtmlTable());

	$row = $tbl->addRow();
	$row->addData(_('Van:'));
	$row->addData(array("<tt>http://www.A-Eskwadraat.nl/</tt>"
					, "<b><u>".$data['name']."</b></u>"));

	$row = $tbl->addRow();
	$row->addData(_('Naar:'));
	$row->addData(array("<tt>http://www.A-Eskwadraat.nl</tt>"
		, HtmlInput::makeText('target' , tryPar('target', $data['target']), 40)
		));

	$row = $tbl->addRow();
	$row->addData(_('Link naar Engelse site:'));
	$row->addData(HtmlInput::makeCheckbox('english', $data['english']));

	$row = $tbl->addRow();
	$row->addData(_('Namens commissie:'));
	$row->addData(hasAuth('bestuur')
			? HtmlSelectbox::cies(CommissieVerzameling::wijzigbare()
				, 'cienr', tryPar('cienr', $data['cienr']))
			: Commissie::geef($data['cienr'])->getNaam()
			);

	$form->add(HtmlInput::makeSubmitButton(_("Wijzig")));

	return new PageResponse($page);
}

/**
 * @brief Verwerk het wijzigformulier.
 *
 * @return bool|string
 * False als er geen formulier was opgestuurd, true als het succesvol verwerkt is, of een string met de fout.
 */
function shortcutedit_form($data)
{
	global $BMDB;
	global $auth;

	if(Token::processNamedForm() != 'edit')
	{
		return false;
	}

	$target = requirePar('target');
	$cienr = $data['cienr'];

	if(hasAuth('bestuur'))
		$cienr = requirePar('cienr');

	if(!check_shortcut_url($target))
		return _('De "naar" van een shortcut mag geen gekke tekens bevatten, anders gebeuren nare dingen!');
	if($target{0} != '/')
		return _('De "naar" van een shortcut moet met een slash beginnen, gekko!');

	$BMDB->q("UPDATE `shortcuts`"
			.  " SET `target` = %s"
			.	  ", `cienr` = %i"
			.	  ", `wie` = %i"
			.	  ", `wanneer` = %s"
			." WHERE `name` = %s"
			, $target
			, $cienr
			, $auth->getLidNr()
			, getDateTime()
			, $data['name']
			);

	// terug naar Shortcuts lijst
	return true;
}

function shortcutdel()
{
	global $BMDB, $uriRef;

	$data = $BMDB->q('MAYBETUPLE SELECT *'
				.   ' FROM `shortcuts`'
				.   ' WHERE `name`  = %s'
				,	vfsVarEntryName()
				);

	shortcutdel_form($data);

	$page = new HTMLPage();
	$page->start();

	$page->setLastModified($data['wanneer']);
	$page->setLastModifier($data['wie']);

	$page->add(new HtmlHeader(2, _("Delete shortcut")));
	$page->add(sprintf(_("Weet je zeker dat je %s wil deleten? Dit kan mogelijk links breken!"),'<b>'.$data['name'].'</b>'));
	$page->add(new HtmlBreak());
	$page->add(new HtmlBreak());
	$page->add($form = HtmlForm::named('delete'));
	$form->addClass('form-inline');
	$form->add($tbl = new HtmlTable());
	$form->add(HtmlInput::makeSubmitButton(_("Ja, ik weet het zeker")));

	return new PageResponse($page);
}
function shortcutdel_form($data)
{
	global $BMDB;

	if(Token::processNamedForm() != 'delete')
		return False;

	$BMDB->q("DELETE FROM `shortcuts` WHERE `name` = %s", $data['name']);

	// terug naar list
	return responseRedirect('..');
}
