<?php
use function \Sentry\captureException;
// We gaan ervan uit dat space/logger.php al geinclude is in het begin.
global $error_occured, $logger;
$error_occured = false;

// Als we een errorhandler al hebben,
// dan willen we daar niet een eigen errorhandler overheen zetten.
// (Dit is bijvoorbeeld nodig om de unit tests netjes te laten verlopen.)
if (defined('ERROR_HANDLER_SET'))
{
	return;
}


// Als we op de debug zitten willen we een pretty-page en anders niets.
if (DEBUG)
{
	// Maak een nieuw Whoops-object voor de error-catching
	$whoops = new \Whoops\Run();

	// Op de cli willen we een andere error-handler dan in de html
	if (PHP_SAPI == 'cli')
	{
		$textPage = new \Whoops\Handler\PlainTextHandler();
		$whoops->pushHandler($textPage);
	}
	else
	{
		$errorPage = new \Whoops\Handler\PrettyPageHandler();
		$errorPage->setPageTitle('Ĝi estas rompita');
		$errorPage->setEditor('phpstorm');
		$whoops->pushHandler($errorPage);

		// Zet error_occured op true zodat de destructors dit kunnen gebruiken.
		$handler = new \Whoops\Handler\CallbackHandler(function ($exception, $inspector, $run) {
			global $error_occured;
			$error_occured = true;
		});

		// Stop loggegevens in de errorpage.
		// (Dit kan niet bij het aanmaken van het Whoops-object,
		// want dan krijg je alleen een lege log.)
		$errorPage->addDataTableCallback('main-log', function () {
			global $logger, $logHistory;
			$logTable = [];

			// Houd een tellertje bij zodat de keys niet overlappen.
			foreach ($logHistory->records as $i => $data)
			{
				$wanneer = $data['datetime']->format('c');
				$logTable[$wanneer . 'N' . $i] = $data['formatted'];
			}
			return $logTable;
		});
	}

	// Register de handlers
	$whoops->register();

	return;
}

use Psr\Log\LogLevel;

function codeToString($code)
{
	switch ($code)
	{
		case E_ERROR:
			return 'E_ERROR';
		case E_WARNING:
			return 'E_WARNING';
		case E_PARSE:
			return 'E_PARSE';
		case E_NOTICE:
			return 'E_NOTICE';
		case E_CORE_ERROR:
			return 'E_CORE_ERROR';
		case E_CORE_WARNING:
			return 'E_CORE_WARNING';
		case E_COMPILE_ERROR:
			return 'E_COMPILE_ERROR';
		case E_COMPILE_WARNING:
			return 'E_COMPILE_WARNING';
		case E_USER_ERROR:
			return 'E_USER_ERROR';
		case E_USER_WARNING:
			return 'E_USER_WARNING';
		case E_USER_NOTICE:
			return 'E_USER_NOTICE';
		case E_STRICT:
			return 'E_STRICT';
		case E_RECOVERABLE_ERROR:
			return 'E_RECOVERABLE_ERROR';
		case E_DEPRECATED:
			return 'E_DEPRECATED';
		case E_USER_DEPRECATED:
			return 'E_USER_DEPRECATED';
	}

	return 'Unknown PHP error';
}

function defaultErrorLevelMap()
{
	return [
		E_ERROR => LogLevel::CRITICAL,
		E_WARNING => LogLevel::WARNING,
		E_PARSE => LogLevel::ALERT,
		E_NOTICE => LogLevel::NOTICE,
		E_CORE_ERROR => LogLevel::CRITICAL,
		E_CORE_WARNING => LogLevel::WARNING,
		E_COMPILE_ERROR => LogLevel::ALERT,
		E_COMPILE_WARNING => LogLevel::WARNING,
		E_USER_ERROR => LogLevel::ERROR,
		E_USER_WARNING => LogLevel::WARNING,
		E_USER_NOTICE => LogLevel::NOTICE,
		E_STRICT => LogLevel::NOTICE,
		E_RECOVERABLE_ERROR => LogLevel::ERROR,
		E_DEPRECATED => LogLevel::NOTICE,
		E_USER_DEPRECATED => LogLevel::NOTICE,
	];
}

function isFatal($level)
{
	$errors = E_ERROR
		| E_PARSE
		| E_USER_ERROR
		| E_CORE_ERROR
		| E_CORE_WARNING
		| E_COMPILE_ERROR
		| E_COMPILE_WARNING;
	return ($level & $errors) > 0;
}

function shutdownHandler()
{
	$error = error_get_last();
	if ($error === null)
	{
		return;
	}    // no error

	errorHandler($error['type']
		, $error['message']
		, $error['file']
		, $error['line']
	);
}

function errorHandler($code, $str, $file, $line, $vars = null)
{
	global $session, $logger, $error_occured;
	global $private_debug_info;

	// als @ gebruikt is, niet reporten
	if (!error_reporting())
	{
		return;
	}
	global $private_debug_info;

	//Log de exception/error naar Sentry als deze gedefineerd is
	if(defined('SENTRY_URL'))
	{
		if($private_debug_info instanceof Exception)
		{
			captureException($private_debug_info);
		}
	}

	// Haal alle info op nodig voor de log
	$type = codeToString($code);

	if (isset($_SERVER['SERVER_NAME']))
	{
		$uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
	}
	else
	{
		$uri = $_SERVER['SCRIPT_FILENAME'];
	}

	$lidnr = isset($session) && $session->has('mylidnr')
		? ("'{$session->get('mylidnr')}'")
		: 'NULL';

	if (!$private_debug_info)
	{
		$private_debug_info = 'niet beschikbaar';
		$private_debug_info_str = 'niet beschikbaar';
	}
	elseif ($private_debug_info instanceof Exception)
	{
		$private_debug_info_str = $private_debug_info->getTraceAsString();
	}
	else
	{
		$private_debug_info_str = $private_debug_info;
	}

	global $auth;

	// Als we diep in de krochten van space doodgaan,
	// is het hele auth-systeem nog niet klaar.
	if (!is_null($auth) && method_exists($auth, 'getLevel'))
	{
		$authLevel = $auth->getLevel();
	}
	else
	{
		$authLevel = '(nog geen Auth.php geladen)';
	}
	$refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;

	// Maak de logmessage
	$log_msg = "Type: $type ($code)\n"
		. "Foutmelding: $str\n"
		. "Bestand/regel: $file:$line\n"
		. "URL: $uri\n"
		. "Referer: $refer\n"
		. "Info: $private_debug_info_str\n"
		. "Gebruiker: lid $lidnr (auth: $authLevel)\n"
		. "Info: $private_debug_info_str\n";

	$level = isset(defaultErrorLevelMap()[$code]) ? defaultErrorLevelMap()[$code] : LogLevel::CRITICAL;

	// Log de message
	$logger->log($level, $log_msg);

	// Resetting private_debug_info
	$private_debug_info = "";

	if (isFatal($code))
	{
		// Roep hier expliciet de close voor alle handlers aan omdat met een fatal
		// error deze niet worden aangeroepen
		foreach ($logger->getHandlers() as $handler)
		{
			if ($handler instanceof \Monolog\Handler\AbstractHandler)
			{
				$handler->close();
			}
		}

		$error_occured = true;

		// Als het op de cli is poepen we de log_msg op de cli uit
		if (is_script())
		{
			echo $log_msg;
		}
		else
		{
			// Zorg ervoor dat we een Page kunnen starten.
			// (Wat als de fout hier al is gebeurd...?)
			require_once('space/classes/Page/init.php');
			require_once('space/response.php');

			// We gaan ervan uit dat dit niet in bookweb gebeurt,
			// dus dat we over de vorige Page heen een nieuwe kunnen starten.
			$page = new CleanHTMLPage();
			$page->addHeadCss(Page::minifiedFile('a-eskwadraat.css', 'css'))
				->start(_("Interne fout opgetreden"));

			// We maken een jumbotron met een generieke pagina aan...
			?>
			<div class="container">
				<div class="jumbotron">
					<h1><? echo _('Er is iets misgegaan!'); ?></h1>
					<p><? echo _('Onze excuses voor het ongemak! De WebCie is op de hoogte gesteld. '
							. 'Wil je deze fout zelf oplossen? Kom bij de WebCie; stuur een e-mail '
							. 'naar <a href="mailto:webcie@a-eskwadraat.nl">webcie@a-eskwadraat.nl</a>.'); ?></p>
				</div>
			</div>
			</div>
			<?

			// Verstuur deze pagina!
			$response = new PageResponse($page);
			$response->send();
		}

		// Fatale fout, dus geef het op.
		die();
	}
}

function exceptionHandler($exception)
{
	// Errors lopen gewoon via user_error en geconfigureerde errorhandlers,
	// daardoor worden er automatisch mails verstuurd en wordt gevoelige
	// informatie (zoals stacktraces) alleen aan Gods getoond.
	global $private_debug_info;
	$private_debug_info = $exception;

	user_error(get_class($exception) . "(" . $exception->getMessage() . ")"
		, E_USER_ERROR);
}

// Registreer de handlers
set_error_handler('errorHandler');
set_exception_handler('exceptionHandler');
register_shutdown_function('shutdownHandler');

