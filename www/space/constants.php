<?php
// $Id$

/*+ Alle Space constanten
*/

$consttm = new ProfilerTimerMark('space/constants.php');
Profiler::getSingleton()->addTimerMark($consttm);

define('MSG', FALSE); // zet op DEBUG om aan te zetten voor ~www
define('SQLMSG', FALSE); // idem

// Beperk alle filesysteemoperaties tot paden met gegeven prefix.
// (Dit wil je in principe op CONTENT_ROOT houden plus een slash.)
define('FILESYSTEM_PREFIX', CONTENT_ROOT . '/');

// De verschillende kleuren voor de tabs
global $KLEURCODES;
$KLEURCODES = array( 'geel', 'roze', 'groen', 'blauw', 'oranje', 'paars' );

// voorlopig alleen date/time localizatie.
setlocale (LC_TIME, 'nl_NL');

// Defaultargument voor set_time_limit() bij webpagina, respectievelijk script.
// Geeft maximale executietijd aan voor een pagina / script in seconde.
// Indien 0, ongelimiteerd.
define('MAX_PAGE_EXECUTION_TIME', 30);
define('MAX_SCRIPT_EXECUTION_TIME', 0);

// waarom is dit geen define?
$INDEX='index.html';

define('WSWHOME', '/Leden/');
define('SPOOKBASE', '/Service/Spookweb2/');
define('BOEKEN_URL', '/Onderwijs/Boeken2');
define('DIBSBASE', '/Leden/Dibs');
define('COLAKASWEB', '/Service/ColakasWeb/');
define('BUGWEBBASE', '/Service/Bugweb');
define('IOUBASE', '/Leden/I-Owe-U/');
define('BENAMITEBASE', '/Service/Intern/Benamite');

define('DOCUMENTENROOT', 'docu/');

define('TENT_HOME', '/Onderwijs/Tentamens/');
define('TBC_HOME','/home/cies/tbc/texsources/');
define('TENT_SRV','tentamens/');

define('FOTOWEB_DIR', '/srv/fotos/Public/fotoweb/');
define('FOTOWEB_CACHEDIR', '/var/cache/www/fotocache/fotoweb/');

// Voor Git webhooks
// De branch waar de live versie van de site komt
define('GIT_LIVE_BRANCH', 'master');

define('WWWEMAIL', 'www@A-Eskwadraat.nl');

// in ads.php
define('SPOCIE_HOME', '/home/cies/spocie/www/Sponsorpagina/');

define('CSVSEP',';');

define('ACCEPT_CHARSET', 'utf-8');

define('MENU_CUTLENGTH' , 20);

// voor boeken2
define('AESKWLEVERANCIER', '9111');
define('DICTATEN_DIR', 'dictaten/');

// voor dibs
define('DIBS_STARTJAAR', 2012);
define('DIBS_TOON_AANTAL_TRANSACTIES', 10);
define('EURO_TO_KUDOS', 100);
// dibs-limieten. Op 0 zetten disablet ze.
define('DIBS_MAX_PERSOON_KUDOS', 1000);
define('DIBS_MAX_TRANSACTIE_KUDOS', 500);
define('DIBS_MAX_TOTAAL_KUDOS', 0);

// voor spookweb
define('SPOOKWEB_STARTJAAR', 2013);

// studienamen. Buiten whoswho omdat ook bw etc dit moeten kunnen gebruiken.
$STUDIENAMEN = array
	( 'ic' => _('Informatica')
	, 'ik' => _('Informatiekunde')
	, 'na' => _('Natuur- & Sterrenkunde')
	, 'wi' => _('Wiskunde')
	, 'gt' => _('Gametechnologie')
	, 'overig' => _('Overig')
	);

// departementsnamen
$DEPARTEMENTNAMEN = array
	( 'ic' => _('Departement Informatica')
	, 'na' => _('Departement Natuurkunde')
	, 'wi' => _('Departement Wiskunde')
	, 'ov' => _('Overig')
	, '' => ''
	);

// constants voor PersoonView::naam()
define('WSW_NAME_DEFAULT',		0); // Bas van Schaik
define('WSW_NAME_VOORNAAM',		1); // Bas
define('WSW_NAME_FORMEEL',		2); // Prof. dr. S.J. van Schaik MSc.
define('WSW_NAME_INITIALEN',		3); // BvS
define('WSW_NAME_LANG',   		4); // Prof. dr. Sebastian Johannes "Sjeik" (Bas) van Schaik MSc.
define('WSW_NAME_FORMEEL_LANG',		5); // Prof. dr. Sebastiaan Johannes van Schaik MSc.
define('WSW_NAME_CRIMI',		6); // Bas van S.
// de whereclause die bepaalt of iemand lid is. dit zou eigenlijk een soort
// 'virtual column' moeten zijn maar dat kan (nog) niet in MySQL

// eigenlijk is een geschorst lid wel lid, maar alleen geschorst van deelname
// aan reguliere ledenfuncties. Voor de website zullen we dus geschorste leden
// niet laten matchen met de WSW_LIDWHERE.
define('WSW_LIDOFOUDLIDWHERE', '((lidvan IS NOT NULL AND lidvan <= CURDATE() ) 
	AND (toestand IS NULL OR (toestand !="geschorst" and toestand != "boekenkoper" ) ) )');

define('WSW_LIDOUDLIDOFBIJNALIDWHERE', '((' . WSW_LIDOFOUDLIDWHERE . ') OR (toestand = "bijna lid"))' );

define('WSW_LIDWHERE',
	'( (lidtot IS NULL OR lidtot > CURDATE()) AND '.WSW_LIDOFOUDLIDWHERE.' )');

define('WSW_LIDOFBIJNALIDWHERE', '((' . WSW_LIDWHERE . ') OR (toestand = "bijna lid"))' );

// FIXME: mimetypes uit /etc/mime.type?
$ALLOWED_FILE_TYPES = array
	( 'pdf' => 'application/pdf'
	, 'png' => 'image/png'
	, 'jpg' => 'image/jpeg'
	, 'jpeg'=> 'image/jpeg'
	, 'gif' => 'image/gif'
	, 'svg' => 'image/svg+xml'
	, 'ico' => 'image/x-icon'
	, 'txt' => 'text/plain'
	, 'tex' => 'text/x-tex'
	, 'ps' => 'application/postscript'
	, 'eps' => 'application/postscript'
	, 'css' => 'text/css'
	, 'js'  => 'application/x-javascript'
	, 'psd' => 'application/photoshop'
	, 'ai'  => 'application/postscript'
	// voor activiteitenfeeds:
	, 'rss' => 'application/rss+xml'
	, 'ics' => 'text/x-vCalendar'
	// voor mozilla searchplugins:
	, 'src' => 'text/plain'
	// bron code:
	, 'c'   => 'text/x-csrc'
	, 'java'=> 'text/x-java'
	, 'pas' => 'text/x-pascal'
	, 'hs'  => 'text/plain'
	, 'lhs' => 'text/plain'
	// programmeer wedstijd input/output
	, 'in'  => 'text/plain'
	, 'out' => 'text/plain'
	// office
	, 'xls' => 'application/vnd.ms-excel'
	, 'doc'	=> 'application/msword'
	, 'ppt'	=> 'application/vns.ms-powerpoint'
	// office xml
	, 'xlsx'=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	, 'docx'=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	, 'pptx'=> 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
	// openoffice
	, 'sxw' => 'application/vnd.sun.xml.writer'
	, 'sxc' => 'application/vnd.sun.xml.calc'
	, 'sxi' => 'application/vnd.sun.xml.impress'
	// archives
	, 'rar' => 'application/rar'
	, 'zip' => 'application/zip'
	, 'gz'  => 'application/gzip'
	, 'tgz' => 'application/x-tar-gz'
	// multimedia
	, 'avi' => 'video/x-msvideo'
	, 'mpeg'=> 'video/mpeg'
	, 'mpg' => 'video/mpeg'
	// xml files
	, 'xml' => 'text/xml'
	// GIMP-bestanden
	, 'xcf' => 'image/xcf'
	);


$MAANDEN = array(
		1 => _('januari'), 2 => _('februari'), 3 => _('maart'), 4 => _('april'), 5 => _('mei'), 6 => _('juni'),
		7 => _('juli'), 8 => _('augustus'), 9 => _('september'), 10 => _('oktober'), 11 => _('november'), 12 => _('december') );

$LATEX_ESCAPE_CHARS = array(
	'à' => '\`{a}' ,
	'á' => '\\\'{a}',
	'â' => '\^{a}' ,
	'ä' => '\"{a}' ,
	'ã' => '\~{a}' ,
	'å' => '\{aa}' ,
	'æ' => '\{ae}' ,
	'ç' => '\c{c}' ,
	'è' => '\`{e}' ,
	'é' => '\\\'{e}',
	'ê' => '\^{e}' ,
	'ë' => '\"{e}' ,
	'ẽ' => '\~{e}' ,
	'ì' => '\`{i}' ,
	'í' => '\\\'{i}',
	'î' => '\^{i}' ,
	'ï' => '\"{i}' ,
	'ĩ' => '\~{i}' ,
	'ñ' => '\~{n}' ,
	'ò' => '\`{o}' ,
	'ó' => '\\\'{o}',
	'ô' => '\^{o}' ,
	'ö' => '\"{o}' ,
	'õ' => '\~{o}' ,
	'ø' => '\{o}'  ,
	'œ' => '\{oe}' ,
	'ù' => '\`{u}' ,
	'ú' => '\\\'{u}',
	'û' => '\^{u}' ,
	'ü' => '\"{u}' ,
	'ű' => '\~{u}' ,
	'ỳ' => '\`{y}' ,
	'ý' => '\\\'{y}',
	'ŷ' => '\^{y}' ,
	'ÿ' => '\"{y}' ,
	'ỹ' => '\~{y}' ,

	'À' => '\`{A}' ,
	'Á' => '\\\'{A}',
	'Â' => '\^{A}' ,
	'Ä' => '\"{A}' ,
	'Ã' => '\~{A}' ,
	'Å' => '\{AA}' ,
	'Æ' => '\{AE}' ,
	'Ç' => '\c{C}' ,
	'È' => '\`{E}' ,
	'É' => '\\\'{E}',
	'Ê' => '\^{E}' ,
	'Ë' => '\"{E}' ,
	'Ẽ' => '\~{E}' ,
	'Ì' => '\`{I}' ,
	'Í' => '\\\'{I}',
	'Î' => '\^{I}' ,
	'Ï' => '\"{I}' ,
	'Ĩ' => '\~{I}' ,
	'Ñ' => '\~{N}' ,
	'Ò' => '\`{O}' ,
	'Ó' => '\\\'{O}',
	'Ô' => '\^{O}' ,
	'Ö' => '\"{o}' ,
	'Õ' => '\~{O}' ,
	'Ø' => '\{O}'  ,
	'Œ' => '\{OE}' ,
	'Ù' => '\`{U}' ,
	'Ú' => '\\\'{U}',
	'Û' => '\^{U}' ,
	'Ü' => '\"{U}' ,
	'Ű' => '\~{U}' ,
	'Ỳ' => '\`{Y}' ,
	'Ý' => '\\\'{Y}',
	'Ŷ' => '\^{Y}' ,
	'Ÿ' => '\"{Y}' ,
	'Ỹ' => '\~{Y}'
);

//FotoWeb Licences
$FW_LICENCES = array(
	"cc3.0-by-nc-sa-aes2" => array( // Creative Commons 3.0, Attribution non-commercial share-alike, met A-Eskwadraat amendement
		"name" => "C.C. 3.0 BY-NC-SA geamendeerd",
		"url" => "http://www.a-eskwadraat.nl/www/cc3.0-by-nc-sa-aes2.html"
	),
	"cc3.0-by-nd-aes2" => array( // Creative Commons 3.0, Attribution no derivative works, met A-Eskwadraat amendement
		"name" => "C.C. 3.0 BY-ND geamendeerd",
		"url" => "http://www.a-eskwadraat.nl/www/cc3.0-by-nd-aes2.html"
	),
	"cc3.0-by-nc-sa" => array( // Creative Commons 3.0, Attribution non-commercial share-alike
		"name" => "C.C. 3.0 BY-NC-SA",
		"url" => "http://creativecommons.org/licenses/by-nc-sa/3.0"
	),
	"cc3.0-by-nc" => array( // Creative Commons 3.0, Attribution non-commercial
		"name" => "C.C. 3.0 BY-NC",
		"url" => "http://creativecommons.org/licenses/by-nc/3.0"
	),
	NULL => array ( // Onbekend
		"name" => "Onbekend",
		"url" => NULL
	)
);

$FW_EXTENSIONS_FOTO = array("jpg", "jpeg", "gif", "png");
$FW_EXTENSIONS_FILM = array("flv", "avi", "mpg", "mpeg", "mov", "wmv", "mp4", "ogg");
$FW_EXTENSIONS = array_merge($FW_EXTENSIONS_FOTO, $FW_EXTENSIONS_FILM);

$consttm->markEnd();

