<?php

class DateTimeLocale
	extends \Carbon\Carbon
{
	private $notime;

	public function __construct($time = 'now', $timezone = null)
	{
		if(is_int($time) || is_numeric($time))
		{
			$time = date("c", $time);
		}

		// Mocht er een datetime-format aangeleverd worden wat in Nederlandse format
		// staat (m-d-Y), converteer het naar Engels format zodat datetime ermee
		// om kan gaan. Dit komt alleen voor als er een externe site waarop er geen
		// harde restricties zijn op het date-veld in een html-form (zoals bv
		// de intro-aanmeld-site), data parset naar de a-essite
		if(preg_match('/\d{2}-\d{2}-\d{4} \d{2}:\d{2}:\d{2}/', $time))
		{
			//TODO: Er op deze manier een datetime van maken is lelijk, kan misschien beter
			$date = self::createFromFormat('m-d-Y H:i:s', $time);
			$time = $date->format('Y-m-d H:i:s');
		}
		else if(preg_match('/\d{2}-\d{2}-\d{4}/', $time))
		{
			$date = self::createFromFormat('m-d-Y', $time);
			$time = $date->format('Y-m-d');
		}

		parent::__construct($time);
		$this->notime = is_null($time);
	}

	public function strftime($format)
	{
		if($this->notime)
			return null;

		return $this->formatLocalized($format);
	}

	public function format($format)
	{
		if ($this->notime)
		{
			// FIXME: PHP geeft een fatale error als je een notice doet in __toString,
			// en Carbon zijn toString roept format aan...
			// user_error("DateTimeLocale zonder tijd wordt geformat", E_USER_NOTICE);
		}

		return parent::format($format);
	}

	/*
	 * Print de datumtijd in een formaat zoals "20170615T143000Z" (YYYYMMDDTHHIISSZ) in UTC.
	 */
	public function kalenderFormaat($type)
	{
		$copy = clone $this;
		$copy->setTimezone('UTC');
		$space = $type == 'outlook' ? '%3\\A' : '';
		return $copy->format("Ymd\\TH" . $space . "i" . $space . "s\\Z");
	}

	/* DateTime methods */

	public function add($interval)
	{
		if($this->notime)
			user_error("aanroep op DateTimeLocale met NULL", E_USER_NOTICE);

		return parent::add($interval);
	}

	public function diff($datetime2, $absolute = false)
	{
		if($this->notime)
			user_error("aanroep op DateTimeLocale met NULL", E_USER_NOTICE);

		return parent::diff($datetime2, (boolean)$absolute);
	}

	public function getTimestamp()
	{
		if($this->notime) {
			user_error("aanroep op DateTimeLocale met NULL", E_USER_NOTICE);
			return 0;
		}

		return parent::getTimestamp();
	}

	public function hasTime()
	{
		return !$this->notime;
	}

	public function modify($modify)
	{
		if($this->notime)
			user_error("aanroep op DateTimeLocale met NULL", E_USER_NOTICE);

		return parent::modify($modify);
	}

	public function setDate($year, $month, $day)
	{
		$this->notime = False;
		return parent::setDate($year, $month, $day);
	}

	public function setISODate($year, $week, $day = 1) {
		$this->notime = False;
		return parent::setISODate($year, $week, $day);
	}

	public static function __setstate(array $array)
	{
		$object = parent::__setstate($array);
		$object->notime = (boolean)@$array['notime'];
		return $obj;
	}

	public function setTime($hour, $minute, $second = 0, $microseconds = NULL)
	{
		if (!is_null($microseconds))
		{
			user_error('Deze parameter werkt alleen in PHP 7, dus gebruik die AUB pas als PHP 5 is uitgeroeid!', E_USER_WARNING);
		}

		$this->notime = false;

		// PHP 7.1 introduceert een nieuwe parameter, dus geef die door...
		if (version_compare(PHP_VERSION, '7.1.0') >= 0)
		{
			return parent::setTime($hour, $minute, $second, $microseconds);
		}
		else
		{
			return parent::setTime($hour, $minute, $second);
		}
	}

	public function setTimestamp($unixtimestamp)
	{
		$this->notime = False;
		return parent::setTimestamp($unixtimestamp);
	}

	public function sub($interval)
	{
		if($this->notime)
			user_error("aanroep op DateTimeLocale met NULL", E_USER_NOTICE);

		return parent::sub($interval);
	}
}
