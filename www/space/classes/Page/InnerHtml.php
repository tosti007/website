<?php
/**
 * Wordt gebruikt om html dat in een container geplaatst kan worden terug te sturen
 */
class InnerHTMLPage extends Page
{
	private $started = false;

	private $content = array();

	/**
	 * @brief Geef de content-type header die hoort bij dit Page-object.
	 *
	 * @return Een string met de waarde van de content-type.
	 */
	public function contentType() {
		return 'text/html; charset=utf-8';
	}

	/**
	 * @brief Doet niets maar lijkt op andere Page::start-methoden.
	 *
	 * @param title Wordt genegeerd.
	 */
	public function start ($title = '')
	{
		return $this;
	}

	public function add (...$children)
	{
		foreach ($children as $child)
			$this->content[] = $child;
		return $this;
	}

	public function end ()
	{
		echo $this->respond();
		exit;
	}

	/**
	 * @brief Geef de inhoud van deze pagina.
	 *
	 * @return Een string met HTML.
	 */
	public function respond()
	{
		return implode("\n", $this->content);
	}
}
