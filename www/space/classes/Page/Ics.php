<?php
require_once 'space/classes/VCalendar/Item.php';

/**
 * Wordt gebruikt om een ics-bestand naar het scherm te sturen
 */
class ICSPage extends Page
{
	private $started = false;

	private $content = array();

	/**
	 * @brief Maak de ICS-headers van deze Page aan.
	 *
	 * Over het algemeen doet twee keer `$page->start()` aanroepen niet veel goeds,
	 * dus zorg ervoor dat dat niet gebeurt!
	 *
	 * @sa respond
	 *
	 * @param string $title De titel van deze kalender.
	 * Wordt genegeerd: is alleen ter compat van RSSPage (?)
	 * @param string $description De omschrijving van deze kalender.
	 * Wordt genegeerd: is alleen ter compat van RSSPage (?)
	 * @return ICSPage
	 */
	public function start ($title = '', $description = '')
	{
		if (!$this->started)
		{
			$this->content[] = "BEGIN:VCALENDAR\r\n"
				."VERSION:2.0\r\n"
				."PRODID:-//A–Eskwadraat//WSW4\r\n";
			$this->started = true;
		}
		else
		{
			// Zeur als twee keer wordt gestart want dat doet niet wat je verwacht.
			if (getZeurModus())
			{
				user_error("ICSPage::start() wordt twee keer aangeroepen, daar kan de code (nog) niet tegen", E_USER_WARNING);
			}
		}
		return $this;
	}

	/**
	 * @brief Voeg wat inhoud toe aan de pagina.
	 *
	 * Deze functie accepteert arbitrair veel argumenten,
	 * die een voor een toegevoegd worden.
	 * Deze argumenten kunnen zelf ook weer een array zijn,
	 * waarvan de elementen ook weer een voor een worden toegevoegd.
	 * (Een niet-array telt hier als een argument.)
	 *
	 * Vervolgens wordt het element in een `VCalendar_Item` gezet indien het nog geen `VCalendar_Item` is,
	 * en tenslotte wordt dit `VCalendar_Item` omgezet in een string en dat achteraan de content gezet.
	 * (TODO: dit proces is superingewikkeld en kan beter...)
	 *
	 * @return ICSPage Het Page-object, zodat je `->add(...)->add(...)` kan doen.
	 */
	public function add (...$c)
	{
		foreach ($c as $children)
		{
			if (!is_array($children))
				$children = array($children);

			foreach ($children as $child)
			{
				if (!$child instanceof VCalendar_Item)
					$child = new VCalendar_Event($child);
				$this->content[] = (string) $child;
			}
		}
		return $this;
	}

	/**
	 * @brief Rond de pagina af en geef een mooie string.
	 *
	 * Als je een pagina maakt, wil je dit waarschijnlijk niet zelf gebruiken,
	 * maar iets als `new PageResponse($page)`.
	 *
	 * @return Een string die gebruikt kan worden als de content van een HTTP-response.
	 */
	public function respond()
	{
		$headerEnBody = implode("\n", $this->content) . "\r\n";
		$footer = "END:VCALENDAR\r\n";
		return $headerEnBody . $footer;
	}

	/**
	 * @brief Rond de pagina af en schrijf het allemaal naar stdout.
	 *
	 * Gaat hilarisch stuk als je headers gebruikt enzo.
	 * Dit is deprecated, gebruik respond!
	 * @sa respond
	 */
	public function end ()
	{
		if (getZeurModus())
		{
			user_error("Page::end is deprecated, gebruik een PageResponse!", E_USER_DEPRECATED);
		}
		echo $this->respond();
		exit;
	}

	/**
	 * @brief Geef de content-type header die hoort bij dit Page-object.
	 *
	 * Zou statisch moeten zijn, maar kan niet in PHP,
	 * want je wilt dit niet afhankelijk maken van de inhoud.
	 * Regel dat maar liever in je controller!
	 */
	public function contentType() {
		return 'text/calendar';
	}
}

/**
 * @brief Hetzelfde als ICSPage maar dan met een lelijke underscore.
 *
 * Nodig voor backwards compatibility.
 *
 * @deprecated Gebruik ICSPage want die past in de conventies.
 */
class Ics_Page extends ICSPage
{
	public function __construct()
	{
		if (getZeurmodus())
		{
			user_error("Denk aan de conventies, gebruik ICSPage ipv Ics_Page!", E_USER_DEPRECATED);
		}
	}
}
