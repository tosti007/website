<?php
require_once('space/classes/html/init.php');

/**
 * De standaard html-output met design
 */
class HTMLPage extends Page {
	private $wocpagetm;	// whole page profiletimer
	private $contenttm;	// content profiletimer

	private $started;	// ->start() should only be called once

	private $head;
	private $footer;
	private $kleuren;
	private	$deleteurl;
	private	$editurl;
	private	$popup;

	private $brand;
	private $content;
	private $modwhen;
	private $modwho;

	public $kleur;

	public $useCSP = false;

	public function __construct()
	{
		global $pageEnded;

		$pageEnded = false;

		$this->contenttm = NULL;

		$this->started = false;
		$this->head = array();
		$this->footer = array();
		$this->editurl = NULL;
		$this->popup = false;

		$this->modwhen = NULL;
		$this->modwho = NULL;

		$this->initKleuren();

		$this->content = new HtmlElementCollection();
	}

	public function __destruct()
	{
		global $error_occured;
		global $logger;
		global $pageEnded;

		// Be-eindig de pagina als de programmeur het is vergeten.
		// Dit is echt een Heel Slecht Idee :(
		// Fun fact: als je Page->end() aanroept als er al een errorpagina getoond wordt,
		// gaan dingen extreem stuk. Dit gaan we proberen te voorkomen
		// maar destructors runnen op rare momenten dus het zou nog steeds mis kunnen gaan...

		if (!$pageEnded && !$error_occured)
		{
			if (!getZeurmodus())
			{
				$logger->warn("Pagina begonnen maar nog niet beëindigd. Ik ga ervan uit dat je dit wel wil doen, dus bij deze doe ik Page->end(). RIP je executie!");

				$this->end();
			}
		}
	}

	/**
	 * @brief Redirect na een gegeven tijd te wachten.
	 *
	 * @deprecated Gebruik responseRedirect.
	 */
	public function inner_redirect ($url, $sec = '2')
	{
		global $logger;

		if (getZeurModus()) {
			user_error("inner_redirect wil je eigenlijk niet gebruiken", E_USER_DEPRECATED);
			// overigens mag je mij, Tim "Joe'p" Baanen, persoonlijk komen uitschelden als je een geldige reden hebt om te redirecten na een aantal seconden via een meta-tag
		}
		else
		{
			$logger->warn("Deprecated functie HTMLPage::inner_redirect aangeroepen");
		}

		$this->addHead('<meta HTTP-EQUIV="REFRESH" content="' . $sec . '; url=' . $url . '">');
		return $this;
	}

	public function getContentDiv()
	{
		return $this->content;
	}

	public function addHead($line)
	{
		if ($this->started) {
			trigger_error("Er is al begonnen met output.", E_USER_ERROR);
		}
		$this->head[] = $line;
		return $this;
	}

	public function addFooter($line)
	{
		$this->footer[] = $line;
		return $this;
	}

	public function addHeadCSS($script, $media = 'screen')
	{
		$this->addHead('<link rel="stylesheet" href="'.$script.'" '
						.'type="text/css" media="'.$media.'">');
		return $this;
	}

	public function addHeadJS($script)
	{
		$this->addHead('<script src="'
						. $script . '" type="text/javascript"></script>');
		return $this;
	}

	public function addFooterJS($script)
	{
		$this->addFooter('<script src="'
						. $script . '" type="text/javascript"></script>');
		return $this;
	}

	public function getDeleteUrl()
	{
		return $this->deleteurl;
	}
	public function setDeleteUrl($url)
	{
		$this->deleteurl = $url;
		return $this;
	}

	public function getEditUrl()
	{
		return $this->editurl;
	}
	public function setEditUrl($url)
	{
		$this->editurl = $url;
		return $this;
	}

	public function getPopup ()
	{
		return $this->popup;
	}
	public function setPopup ($html)
	{
		$this->popup = $html;
		return $this;
	}

	public function setBrand($brand)
	{
		switch($brand) {
		case 'intro':
			$this->brand = 'smurf.png';
			break;
		case 'bugweb':
			$this->brand = 'bug.png';
			break;
		default:
			user_error("onbekend brand '$brand'", E_USER_WARNING);
		}
		return $this;
	}

	public function setLastMod(Entiteit $obj)
	{
		$this->setLastModified($obj->getGewijzigdWanneer());
		$this->setLastModifier($obj->getGewijzigdWie());
	}

	public function setLastModified($when)
	{
		if($when instanceof DateTime)
			$this->modwhen = $when->format('Y-m-d H:i:s');
		else
			$this->modwhen = $when;
		return $this;
	}
	public function setLastModifier($who)
	{
		if(is_numeric($who))
		{
			if(Lid::geef($who))
				$this->modwho = hasAuth('lid')?PersoonView::naam(Lid::geef($who)):$who;
			else
				$this->modwho = 'onbekend';
		}
		else
			$this->modwho = hasAuth('lid')?$who:null;
		return $this;
	}

	public function getBenamiteUrl()
	{
		global $uriRef;

		// Zonder entry is er ook niets in Benamite te vinden.
		if (!$uriRef['entry'])
		{
			return null;
		}

		if (strpos($uriRef['request'], '/Benamite/') !== false)
			return NULL;

		return $uriRef['entry']->benamiteUrl();
	}

	public function hasStarted()
	{
		return $this->started;
	}

	public function enableCSP()
	{
		if($this->hasStarted()) {
			user_error('enableCSP mag niet aangeroepen nadat Page::start'
				. ' is aangeroepen. Dit moet gefixed worden!'
				, E_USER_NOTICE);
			return $this;
		}

		$cspLines = array(
			"default-src"	=> "*.a-eskwadraat.nl",
			"script-src"	=> "'unsafe-inline' 'unsafe-eval' *.a-eskwadraat.nl",
			"frame-src"		=> "'self'",
			"img-src"		=> "* data:",
			"style-src"		=> "'self' 'unsafe-inline'",
			"report-uri"	=> "/CSP-report"
		);

		$headerLine = "Content-Security-Policy-Report-Only: ";
		foreach($cspLines as $directive=>$policy)
		{
			$headerLine .= $directive . " " . $policy . "; ";
		}
		header("X-" . $headerLine); //Experimental header
		header($headerLine);

		return $this;
	}

	/**
	 *      @brief Functie die je pagina in de A-Es2 omgeving gaat maken.
	 *
	 *      @param maintitle De (escaped) title die bovenaan je pagina komt te staan.
	 *       Wordt ook gebruikt voor het maken van de full title, de echte
	 *       title van de pagina die wordt gemaakt aan de hand van alle
	 *       kindjes van deze pagina.
	 *
	 *      @param mainid De (CSS) id die de content van deze pagina krijgt
	 *       en automatische wordt geinclude als er een apart css bestand
	 *       voor is.
	 *
	 *      @param subtitle Een string die als subtitle naast de maintitle
	 *       komt te staan
	 *
	 *      @param foto Een bool of de pagina gebruikt wordt voor een foto-overzicht.
	 **/
	public function start ($maintitle = null, $mainid = null, $subtitle = null, $foto = false, $BOEKWEB2_IS_NOG_STEEDS_NIET_AF = false)
	{
		global $session, $request, $uriRef, $INDEX, $KLEUR, $badges;

		if($this->useCSP)
			$this->enableCSP();

		$this->wocpagetm = new ProfilerTimerMark('page::start');
		Profiler::getSingleton()->addTimerMark($this->wocpagetm);

		$entry = $uriRef['entry'];

		if ($entry)
		{
			if (!$maintitle)
				$maintitle = $entry->getDisplayName();

			/* make nice <head><title> */
			$prevMenuEntryId = 0;
			$menuEntryId = 0;
			while(true)
			{
				$kleurEntryId = $entry->getId();
				$prevMenuEntryId = $menuEntryId;
				$menuEntryId = $entry->getId();
				$entry = $entry->getParent();
				if($entry->isRoot()) {
					$menuEntryId = $prevMenuEntryId;
					break;
				}
			}
		}
		else
		{
			$kleurEntryId = false;
		}

		$fulltitle = $maintitle;

		if(isset($this->kleuren[$kleurEntryId])) {
			$kleur = $this->kleuren[$kleurEntryId];
			$this->kleur = $kleur;
		} else {
			$kleur = reset($this->kleuren);
			$this->kleur = $kleur;
			$kleurEntryId = key($this->kleuren);
		}

		if($uriRef['request'] == '/Home')
			$kleurEntryId = false;

		$KLEUR = $kleur;

		$title_feed = _("A–Eskwadraat activiteiten newsfeed");
		$title_os_l = _("A–Eskwadraat (leden)");
		$title_os_b = _("A–Eskwadraat (boeken)");

		// Laad alleen auth.js in als je nog niet ingelogd bent,
		// als je niet een keer handmatig ingelogd hebt en als je intern bij
		// A–Es zit (zoals de TV en de dibs-tablet)
		if (!$session->has('mylidnr') && !$session->has('isUitgelogd') && strpos($request->server->get('HTTP_X_REAL_IP'), '10.14') === 0)
		{
			$this->addFooter(HtmlScript::makeJavascript('
$.ajax({type: "POST", url: "https://www-auth.a-eskwadraat.nl/api/kerb_jsonp_session.php", data: {field: "value"}, dataType: "jsonp", success: authSuccess });

function authSuccess(xhr) {
	$.ajax({type: "POST", url: "/space/auth/kerb.php", data: { enc_kerb_data: xhr.data}, success: authSiteSuccess});
}

function authSiteSuccess(data) {
	if(data == 0)
		location.reload();
}
'));
		}

		// Laad speciaal voor de historiepagina een apart jsbestand in voor een achievement
		if($uriRef['request'] == '/Vereniging/Achtergrond/Historie')
		{
			$this->addFooter(HtmlScript::makeJavascript('
$(function() {
	$(window).scroll(function() {
		if($(window).scrollTop() + $(window).height() == $(document).height()) {
			$.ajax("/Ajax/Achievements/Historie");
		}
	});
});
'));
		}

		if($this->started) {
			user_error('LET OP: Page->start() werd voor een tweede keer'
					. ' aangeroepen. Dit moet gefixed worden!'
					, E_USER_NOTICE);
			return;
		}
		$this->started = true;

		if(DEBUG)
			$directory = '/srv/fotos/Background-Debug/';
		else
			$directory = '/srv/fotos/Background/';

		$scanned_directory = scandir($directory);
		$scanned_directory = array_diff($scanned_directory, array('.', '..'));
		if(count($scanned_directory) > 2){
			$scanned_directory = array_splice($scanned_directory, 2, count($scanned_directory)-2);

			$upperbound = count($scanned_directory);
			$randfotoid = rand(0,$upperbound-1);

			$randfotoid = str_replace(".jpg", "", $scanned_directory[$randfotoid]);
		}
		else
			$randfotoid = 0;

		// Om te kijken of we op de homepagina zitten
		$isHome = $uriRef['request'] == '/Home';
		$isHomeIngelogd = $isHome && Persoon::getIngelogd();
		if($session->has('override_datetime'))
		{
			Page::addMelding(sprintf(_('LET OP: De datetime is ge-override naar %s'), $session->get('override_datetime')), 'waarschuwing');
		}

		$themecolors = array(
			'paars' => 'magenta'
			, 'oranje' => 'orange'
			, 'groen' => 'limegreen'
			, 'blauw' => 'blue'
			, 'roze' => 'red'
			, 'geel' => 'rgb(200,200,0)'
		);

		if(Persoon::getIngelogd() && Persoon::getIngelogd()->getOverleden())
		{
			$logger->info("Ingelogde persoon is overleden!");
			Page::addMelding(_('Je bent overleden!'), 'fout');
		}

		// Eet alle echo()s op
		ob_start();

?><!DOCTYPE HTML>
<html lang="<?=getLang()?>">
<?php if(!DEBUG) { ?>
<!--
      ,-._.-._.-._.-._.-.       ,- - - - - - - - - - - - - - - - - - - - .
      `-.             ,-'      |   Wil je in de WebCie?!                 |
        |             |        |    We zoeken nog mensen voor o.a.:      |
        |             |        |     * digitale kaartverkoop bij feesten |
        |             |        |     * mobiele website of app            |
        |    .. ,.    |        |     * bugs oplossen en security testen  |
      ,-|___|  |  |___|-.      |     * coole nieuwe features             |
      | |   L__;__J   | |      |    Stuur een mailtje naar www@a-es2.nl  |
      `|      / \      |'     ,'    of kom even langs in de werkkamer!   |
       |     (   )     |     / _.. Wil je de Dilbert-achievement?        |
       |      `''      |    ;-'  | Ga dan naar je eigen achievement-     |
       |               |         | pagina en type de magische code       |
       |               |         | "U8DuvlB9HB" in en druk op enter!     |
       ;-...._____....-;         . - - - - - - - - - - - - - - - - - - - .
      ,'-..._/   \_...-`.
     /       `. ,'       \
    /    /`.  | |    _l_  \
   /_/   \  \_J |  |"   |\_\
   //     `-.__.'  |    | \\
  ||           |   `- -'   ||
  ||           |           ||
 ||            |            ||
 ||            |            ||
 ||            |            ||
;' |           |           | `;
 `' \          |          / `'
     `-...____/ \____...-'
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |       |       |
       |_______|_______|
 _.. -'      |   |      `- .._
;________.___|   |___.________;

-->
<?php } ?>
<head>
	<title><?=aesnaam() . ' - ' . strip_tags($fulltitle)?></title>
	<link rel="shortcut icon" href="/Layout/Images/favicon.ico">
	<link rel="alternate" type="application/rss+xml" title="<?=$title_feed?>" href="/agenda.rss">
	<meta http-equiv="Content-Type" content="<?=$this->contentType()?>">
	<meta name="ICBM" content="52.0873, 5.1662">
	<meta name="DC.title" content="A–Eskwadraat">
	<meta name="geo.position" content="52.0873;5.1662">
	<meta name="description" content="A–Eskwadraat is de studievereniging voor studenten natuurkunde, wiskunde, informatica en informatiekunde aan de Universiteit Utrecht.">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="<?echo $themecolors['roze'];?>" />
	<link rel="stylesheet" href="<? echo Page::minifiedFile('a-eskwadraat.css', 'css'); ?>" type="text/css" media="screen">
<?php
		// Voor debugbenodigdheden stoppen we een extra stylesheet ertussen.
		if (DEBUG)
		{
?>
	<link rel="stylesheet" href="<? echo Page::minifiedFile('aes-debug.css', 'css'); ?>" type="text/css" media="screen">
<?php
		}
?>
	<link rel="stylesheet" href="<? echo Page::minifiedFile('bootstrap-select.css', 'css'); ?>" type="text/css" media="screen">
<?
		/* Dit stukje script zorgt ervoor dat $(document).ready(function(){}), $.ready(function(){})
		 * en $(function(){}) nog goed blijven in inline script tags omdat jQuery pas aan
		 * het einde van de pagina wordt ingeladen.
		 * https://davidmurdoch.com/2011/07/01/how-to-setup-jqueryready-callbacks-before-jquery-is-loaded/
		 */
?>
	<script type="text/javascript">(function(a){_q=function(){return a;};$=function(f){typeof f==="function"&&a.push(arguments);return $;};jQuery=$.ready=$;}([]));</script>

<!--[if lt IE 9]>
	<script src="<?=Page::minifiedFile('html5shiv.js', 'js')?>"></script>
<![endif]-->
<?
		foreach($this->head as $line)
		{
			echo "\t$line\n";
		}
?>
	<link rel="stylesheet" href="<?=Page::minifiedFile('aes-'.$KLEUR, 'css')?>" type="text/css" media="screen">
<?php
		// Custom stylesheets! Stop de url van de sheet in een cookie en je ziet hem.
		if(isset($_COOKIE["custom_style"])) {
			// dit is wel afdoende om mensen te beschermen tegen html-injectie, toch?
			if (strpos($_COOKIE["custom_style"], '"') !== FALSE) {
				Page::addMelding(_("Je stylesheetnaam mag geen &quot; bevatten, joh!"));
			} else {
				?><link rel="stylesheet" href="<?=$_COOKIE["custom_style"]?>" type="text/css" media="screen"><?
			}
		}
?>
</head>
<body class="body-<?=$this->kleur?>" >
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="navbar-header">
			<!-- hamburger -->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-to-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- vlaggetjes -->
			<?=makeVlaggetjes(true)?>
			<!-- A-Es logo -->
			<a class="navbar-brand aeslogo" id="aeslogo" href='/'>
				<img src="/Layout/Images/logo.png" alt="logo" />
			</a>
		</div>
		<div class="navbar-collapse-small">
			<div class="navbar-to-collapse navbar-collapse sidebar-navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<?=bouwPersoonlijkMenu(true)?>
<?
		$this->bouwAlgemeenMenu($kleurEntryId, true)
?>
				</ul>
			</div>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
<?
		$this->bouwAlgemeenMenu($kleurEntryId, false)
?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<?=makeVlaggetjes()?>
				<?=bouwPersoonlijkMenu(false)?>
			</ul>
		</div>
	</div><!-- navbar -->
<?

		$header_text = '<div class="page-header-tekst">';

		if(DEBUG)
		{
			/* Omdat jquery nog niet ingeladen is als we dit gedeelte aanmaken
			 * en jquery niet ingeladen wordt als er halverwege de pagina een fout
			 * komt, stoppen we hier in de onclick van de anchor de js die ervoor
			 * zorgt dat we kunnen profilen en debuggen op een pagina
			 */

			$header_text .= '<span class="debug">Ontkever-modus [<a style="color:white; text-decoration: underline;" id="toggleDebug" onclick="
var elem = document.getElementsByClassName(\'debugmsgtocontent\')[0];
if(elem.innerHTML.length == 0) {
	elem.innerHTML = document.getElementsByClassName(\'debugmsgcontent\')[0].innerHTML;
}
if(elem.style.display !== \'inline\')
	elem.style.display = \'inline\';
else
	elem.style.display = \'none\';
var elems = document.getElementsByClassName(\'debugmsg\');
for(var i = 0, max = elems.length; i < max; ++i) {
	if(elems[i].style.display !== \'inline\')
		elems[i].style.display = \'inline\';
	else
		elems[i].style.display = \'none\';
}
" >debug</a><span class="hidden debugmsg"> | <a id="toggleProfiler">profiler</a></span>]: </span>';
		}
		else if(DEMO)
		{
			$header_text .= '<span class="debug">DEMO: </span>';
		}

		if($this->brand)
		{
			$img = new HtmlImage("/Layout/Images/Brand/" . $this->brand, $this->brand, null, "brand");
			$img->setSize(24, 24);
			$header_text .= $img->makeHtml();
		}

		$header_text .= $maintitle;

		if($subtitle)
		{
			$header_text .= ' <small><i>'.htmlspecialchars($subtitle).'</i></small>';
		}

		$header_text .= '<div class="page-header-knopjes" style="padding-right:15px">'
			. self::makePageLinks()
			. '</div>'
			. '</div>';

		if($foto) { // __1
?>

	<div class="balk-boven"></div>
		<div class="container-fluid title-photo-small" style="display: block;">
			<div class="col-xs-12">
			<?=$header_text?>
			</div>
		</div>
	<div class="container theme-showcase aes-container" style="width: 100%;">

<? } else if(!$isHome || Persoon::getIngelogd()) { // __1?>

	<? if($isHome) { // __2?>
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 40vh !important;">

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
<?
	$first = true;
	foreach($scanned_directory as $image) {
		$fotoid = str_replace(".jpg", "", $image);
?>
	<img class="resload" src="/Background/<?=$fotoid?>/" id="large-1">
	<div class="item <?=($first ? 'active' : '')?>" id="small-1" style="background-image: url('/Background/<?=$fotoid?>/');">
	</div>
<?
		if($first) {
			$first = false;
		}
	}
?>
  </div>

<div style="height: 100%; width: 100%; position: absolute; top: 0;" class="text-center">
  <div style=" width: 50%; height: 50%; position:absolute; left: 25%; top: 25%;">
	<h1 style="color: white; font-size: 5vw; padding-bottom: 20px; font-weight: bold; font-variant: small-caps; text-shadow: 0px 1px 20px rgba(0, 0, 0, 1);"><?=aesnaam()?></h1>
  </div>
</div>

</div>
	<div style="position: fixed; min-height: 100%; width: 100%;"></div>
	<div class="balk-boven"></div>
	<div class="container theme-showcase aes-container">

	<? } else {// __2?>

	<div style="position: fixed; min-height: 100%; width: 100%;"></div>
	<div class="balk-boven"></div>

<div class="container-fluid title-photo" style="background: url('/Background/<?=$randfotoid?>/'); background-size: cover; background-position: center; background-repeat: no-repeat;">
	<div class="container">
		<div class="col-md-12">
			<div>
				<div>
<?=$header_text?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid title-photo-small">
	<div class="col-xs-12">
	<?=$header_text?>
	</div>
</div>
	<div class="container theme-showcase aes-container">

	<? }// __2?>
<? } // __1?>

<? if(!$isHome || Persoon::getIngelogd()) { // __3?>

		<div class="col-md-12" style="">

  <!-- pagina titel -->
<?
	if(date('d-m') == '01-04') { // 1 april
		echo '<div class="alert alert-success" style="margin-top: 30px;">Gefeliciteerd! Je bent vandaag geselecteerd voor het WebCie-experiment met nanoservices: microservices die extra kleine pagina\'s genereren met blockchain, cloud en internesings.<br/><small>Is deze pagina te webscale voor jou? Je kan de cyber updaten door op F5 te drukken. Pas wel op voor rekrutering door DDOS hackers als je het teveel doet!)</small></div>';
	}
?>

	<? if(!$isHome)
	{ // __4
		if(DEBUG)
		{ // __5 ?>
			<div class='debugmsgtocontent' style='display: none;'></div>
		<?} // __5
	} // __4 ?>
<!-- content -->
<div class="pagecontent"<?php echo (!is_null($mainid)) ? ' id="' . $mainid . '"' : '';?>>
<? } // __3

		foreach($session->getFlashBag()->all() as $soort => $meldingen) {
			echo HtmlDiv::makeMeldingen($meldingen, $soort);
		}

		$session->getFlashBag()->clear();

		if($pers = Persoon::getIngelogd()) {
			echo '<div class="alert alert-info" role="alert" id="achievementsInfoBox" style="display: none;';
			if($isHome) {
				echo ' margin-top: 20px;';
			}
			echo '"></div>';
		}
		flush();

		if ($BOEKWEB2_IS_NOG_STEEDS_NIET_AF)
			echo ob_get_clean();
		else
			$this->add(ob_get_clean());

		$this->contenttm = new ProfilerTimerMark('page content');
		Profiler::getSingleton()->addTimerMark($this->contenttm);

		return $this;
	}

	public function add(...$children)
	{
		if(!$this->started) {
			user_error('LET OP: Page->start() nog niet aangeroepen.'
					. ' Dit moet eerst gefixed!'
					, E_USER_ERROR);
		}

		$this->content->addChildren(...$children);
		return $this;
	}

	/**
	 * @brief Rond de pagina af en geef een mooie HTML-string.
	 */
	public function respond()
	{
		global $uriRef, $badges, $pageEnded, $error_occured;

		if($error_occured)
			return;

		$pageEnded = true;

		ob_start();
		echo $this->content->makeHtml(0);

		if ($popup = $this->getPopup()) { ?>
<!-- popup -->
  <div id="popup" class="hidden">
	<div class="bg"></div>
	<div class="outer" onclick="Html_HidePopup()">
	  <div class="middle">
		<div class="inner" onclick="clickOpPopup(event)">
		  <div class='close'>
		  <a href="#" onclick="Html_HidePopup()">
			  <img src="/Layout/Images/Buttons/delete.png" alt="sluiten" />
			</a>
		  </div>
		  <div id="popup_content" class="hidden">
		    <?php echo $popup; ?>
	      </div>
		</div>
  	  </div>
    </div>
  </div>

<?php }

		if($this->contenttm) {
			$this->contenttm->markEnd();
			$t = new ProfilerTimerMark('page::end');
			Profiler::getSingleton()->addTimerMark($t);
			$t->markEnd();
		}

		echo "</div>\n"; // pagecontent

		if(DEBUG) {
			echo '<span class="debugmsgcontent" style="display: none;">';
			echo '<div class="log_messages">';
			echo static::makeLogHistory();
			echo "</div>\n";
			echo '<div class="special_marks">';
			Profiler::getSingleton()->printStats();
			echo '</div></span>';
		}

		echo "\n"
            ."</div>\n"
            ."</div>\n"
            ."</div>\n"
			."</div>\n";
		echo self::makeFooter();

		flush();
?>
	<div class="modal fade" id="logoModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
					<h4 class="modal-title" id="logoModalLabel"><?=_('Op zoek naar ons logo?')?></h4>
				</div>
				<div class="modal-body">
<?=sprintf(_('Je kunt al onze varianten van het logo in hoge kwaliteit vinden op de %s! '
		.'Zorg dat we met tenminste 300 DPI in je drukwerk terecht komen.')
		, new HtmlAnchor('/logos', _('logopagina')))?>
				</div>
			</div>
		</div>
	</div>
<?
		$pers = Persoon::getIngelogd();
		if($pers && !$pers->isBijnaLid() && !isPersoonBeperkt($pers->getContactID())) {
			PersoonBadge::checkAllBadges($pers);
			if($persBadges = $pers->getBadges()) {
				$last = $persBadges->getLast();
				if(!empty($last) && sizeof(json_decode($last)) != 0) {
					echo "<div class='lastBadges' style='display: none;'>";
					foreach(json_decode($last) as $l) {
						echo "<p>Nieuwe achievement gehaald: <a href='/Leden/" . $pers->geefID() . "/Achievements'>" . PersoonBadgeView::toString($l) . "</a>!</p>";
					}
					echo "</div>";
				}
				$persBadges->setLast(null);
				$persBadges->opslaan();
			}
		}
?>
	<script type="text/javascript">
window.lang = '<?=getLang()?>';
	</script>
	<script src="<?=Page::minifiedFile('a-eskwadraat', 'js')?>" type="text/javascript"></script>
	<script src="<?=Page::minifiedFile('home-carousel.js', 'js')?>" type="text/javascript"></script>
	<script src="<?=Page::minifiedFile('home2.js', 'js')?>" type="text/javascript"></script>
	<script src="<?=Page::minifiedFile('bootstrap-select.js', 'js')?>" type="text/javascript"></script>
<?
		foreach($this->footer as $foot) {
			echo "\t$foot\n";
		}
		/* Hier voeren we de $(function(){}), $(document).ready(function(){}) en $.ready(function{})
		 * uit die we op de pagina hebben afgevangen
		 */
?>
	<script type="text/javascript">
(function(i,s,q,l){for(q=window._q(),l=q.length;i<l;){$.apply(this,s.call(q[i++]));}window._q=undefined;}(0,Array.prototype.slice));
	</script>
<?
		// Zet dit aan om de tour mogelijk te maken
		/*$persoon = Persoon::getIngelogd();
		if($persoon && !$persoon->getVoorkeur()->getTourAfgemaakt() && hasAuth('actief'))
		{
?>
<script src="<?=Page::minifiedFile('bootstrap-tour', 'js')?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?=Page::minifiedFile('bootstrap-tour.css', 'css')?>" type="text/css" media="screen">
<script>
<?=LidView::makeTourJS($persoon)?>
</script>
<?
		}*/ ?>
	</body>
</html>
<?php
		$contents = ob_get_clean();

		if($this->wocpagetm)
			$this->wocpagetm->markEnd();

		timer_mark('PAGE END');

		return $contents;
	}

	public function end() {
		if (getZeurModus()) {
			user_error("Page::end is deprecated, gebruik een PageResponse!", E_USER_DEPRECATED);
		}
		echo $this->respond();
		exit;
	}

	public function contentType() {
		return 'text/html; charset=utf-8';
	}

	private function initKleuren()
	{
		global $KLEURCODES;

		$i = 0;

		$this->kleuren = array();

		$rootEntry = vfs::get(getTreeId());
		$children = $rootEntry->getChildrenIds(true);

		foreach($children as $childId)
		{
			if($i >= count($KLEURCODES)) {
				$this->kleuren[$childId] = $KLEURCODES[0];
			} else {
				$this->kleuren[$childId] = $KLEURCODES[$i];
			}
			++$i;
		}
	}

	private function bouwAlgemeenMenu($selectedId, $small = false)
	{
		global $cache;

		$size = sizeof($this->kleuren);
		$i = 0;
		foreach($this->kleuren as $entryId => $kleur)
		{
			$sel = $entryId == $selectedId;
			$entry = vfs::get($entryId);

			if(!empty($entry->getAuth()) && !hasAuth($entry->getAuth())) {
				continue;
			}

			$name = $entry->getDisplayName();
			if($small) {
?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle dropdown-small-anchor" data-toggle="dropdown"><?=$name?>&nbsp;<b class="caret"></b>
					<div class="navknop-links nav<?=$kleur?>-links"></div>
					</a>
					<ul class="dropdown-menu">
			<? } else { ?>
				<li class="dropdown menu-large<? if($sel) echo(" active"); if($i == $size - 1) echo(" menu-last"); ?>">
					<a href="#" class="dropdown-toggle"><?=$name?>
					</a>
					<ul class="dropdown-menu megamenu row">
			<? } ?>
<?
			//We gaan het linkermenu uit de cache halen
			//De cache wordt geshared tussen live en debug, dus daar moeten we onderscheid in maken
			if($small)
				$knopmenuNaam = 'knopmenu-small-' . (DEBUG ? 'debug' : 'live') . '-' . getLang() . '-' . $entryId . '-0-2';
			else
				$knopmenuNaam = 'knopmenu-' . (DEBUG ? 'debug' : 'live') . '-' . getLang() . '-' . $entryId . '-0-2';
			$succes = false;

			// Als het lid ingelogd is krijgen we een ander menu
			if($pers = Persoon::getIngelogd())
			{
				$knopmenuNaam .= '-' . $pers->geefID();
			}

			$item = $cache->getItem($knopmenuNaam);

			if(!$item->isHit()) //Hij staat niet in de cache
			{
				$knopmenutm = new ProfilerTimerMark('knopmenu-nocache');
				Profiler::getSingleton()->addTimerMark($knopmenutm);

				ob_start();
				$this->knopmenu($entryId, $kleur, $small);
				$menuData = ob_get_flush();

				$item->set($menuData);
				$item->tag('knopmenu');
				$cache->save($item);

				$knopmenutm->markEnd();
			}
			else //Hij staat wel in de cache
			{
				echo $item->get();
			}

?>
			</ul>
		</li>
<?
			$i++;
		}
	}

	/**
	 * Echo de inhoud van een knopmenu voor de huidige gebruiker.
	 *
	 * Een knopmenu is een grote `<ul>`-tag met allemaal `<li>`'tjes erin,
	 * en de `<li>`'tjes echoën we hier.
	 *
	 * @param int $rootEntryId Het id van de rootentry,
	 * dit is dus de knop met vaste kleur (bijvoorbeeld `/Service`).
	 * @param string $kleur De kleur van het menuutje.
	 * @param bool $false Moet dit een mobiel/hamburgerknopmenu zijn?
	 *
	 * @return void
	 * Alle waarden worden via echo geoutput.
	 */
	private function knopmenu($rootEntryId, $kleur, $small = false)
	{
		global $INDEX;

		$rootEntry = vfs::get($rootEntryId);

		// quick fix voor als we vfsEntryFiles binnenkrijgen
		if ($rootEntry instanceof vfsEntryDir) {
			$children = $rootEntry->getChildrenIds(true);
		} else {
			$children = array();
		}

		$withoutchildren = array();
		$withchildren = array();

		foreach($children as $child) {
			$child = vfs::get($child);
			// Boeken is nog de enige hook die we willen behouden
			if($child->isHook() && $child->getName() != "Boeken")
				continue;
			if(!$child->mayAccess())
				continue;
			if(!$child->isDir()) {
				$withoutchildren[] = $child;
				continue;
			}
			$childchildren = $child->getChildrenIds(true);
			if(sizeof($childchildren) == 0) {
				$withoutchildren[] = $child;
				continue;
			}

			// Als de map alleen een index.html heeft, stop de child dan in withoutchildren
			if(sizeof($childchildren) == 1) {
				$childchild = vfs::get($childchildren[0]);
				if($childchild->getName() == 'index.html' && $childchild->mayAccess() == true) {
					$withoutchildren[] = $child;
				}
			}

			foreach($childchildren as $childchild) {
				$childchild = vfs::get($childchild);
				if(($childchild->getName() != 'index.html') && ($childchild->mayAccess() == true)) {
					$withchildren[] = $child;
					break;
				}
			}
		}

		$cols = sizeof($withchildren) + 1;
		$colsize = floor(12/$cols);

		// Het knopmenu bestaat uit een <ul> met <li>'s erin per kleur,
		// en een grote editie ook een <ul> in die li voor de losse pagina's en een <ul> per submap van de kleur.
		// De eerste binnenste <ul> wordt hier geopend en snel daarna weer gesloten.
		// TODO SOEPMES: Dit is allemaal niet ideaal, voel je vooral vrij om het te herimplementeren met HTML-objecten, maar het werkt wel :P
		if (!$small)
		{
			echo '<li class="col-md-' . $colsize . ' col-md-aes"><ul>';
		}
		echo self::maakKnopMenuItemVanEntry($rootEntry, 'dropdown-header-large dropdown-header-large-' . $kleur);

		foreach ($withoutchildren as $wc)
		{
			echo self::maakKnopMenuItemVanEntry($wc);
		}
		if (!$small)
		{
			echo '</ul>';
		}

		foreach ($withchildren as $wc)
		{
			$children = $wc->getChildrenIds(true);
			if ($small)
			{
				echo '<li class="divider divider-' . $kleur . '"></li>';
			}
			else
			{
			// Het knopmenu bestaat uit een <ul> met <li>'s erin per kleur,
			// en een grote editie ook een <ul> in die li voor de losse pagina's en een <ul> per submap van de kleur.
			// De niet-eerste binnenste <ul>'s worden hier geopend en tegen het einde van de loopbody weer gesloten.
			// TODO SOEPMES: Dit is allemaal niet ideaal, voel je vooral vrij om het te herimplementeren met HTML-objecten, maar het werkt wel :P
				echo '<li class="col-md-' . $colsize . ' col-md-aes"><ul>';
			}
			echo '<li class="dropdown-header dropdown-header-' . $kleur . '">' . htmlspecialchars($wc->getDisplayName()) . '</li>';

			// De css-class van de items in het knopmenu.
			$itemClass = $small ? '' : 'non-main-ul';

			// We tonen voor een vfsEntryDir de index.html in plaats van de dir zelf.
			if ($wc->hasChild('index.html'))
			{
				$c = $wc->getChild('index.html');
				if ($c->mayAccess())
				{
					echo self::maakKnopMenuItemVanEntry($c, $itemClass);
				}
			}
			// TODO SOEPMES: Haal dit uitzonderlijk geval weg...
			if ($wc->getName() == "Boeken")
			{
				echo self::maakKnopMenuItemMetInhoud('/Onderwijs/Boeken', _("Boeken"), $itemClass);
			}
			foreach ($children as $c)
			{
				$c = vfs::get($c);
				if (!$c->mayAccess() || $c->getName() == 'index.html')
				{
					continue;
				}

				// Alleen Bookweb heeft nog menu-haken.
				$item = $c->menuItem($wc->getName() == 'Boeken');
				echo self::maakKnopMenuItemMetInhoud($item['url'], $item['displayName'], $itemClass);
			}

			// Einde van de sub-<ul> van de <ul> die het knopmenu bevat.
			if (!$small)
			{
				echo '</ul>';
			}
		}
	}

	/**
	 * Geef het item van het knopmenu dat hoort bij de entry.
	 *
	 * @param vfsEntry $entry De entry om in het menu te stoppen.
	 * @param string $class De class voor de <li>-tag. Default: geen class.
	 *
	 * @return string Een HTML-veilige string om in een knopmenu te stoppen.
	 */
	private static function maakKnopMenuItemVanEntry(vfsEntry $entry, $class = '')
	{
		return self::maakKnopMenuItemMetInhoud($entry->url(), $entry->getDisplayName(), $class);
	}

	/**
	 * Geef het item van het knopmenu met de gegeven inhoud.
	 *
	 * @param string $url De (HTML-onveilige) URL van het item.
     * @param string $caption De (HTML-onveilige) klikbare linktekst.
	 * @param string $class De class voor de <li>-tag. Default: geen class.
	 *
	 * @return string Een HTML-veilige string om in een knopmenu te stoppen.
	 */
	private static function maakKnopMenuItemMetInhoud($url, $caption, $class='')
	{
		return '<li class="' . $class . '"><a href="' . htmlspecialchars($url) . '">' . htmlspecialchars($caption) . '</a></li>';
	}

	private function makePageLinks()
	{
		global $request, $KLEUR, $DEBUG_SITES;

		$div = new HtmlDiv(null, 'wijzigen');

		if(rand(0,20) == 10) {
			$pers = Persoon::getIngelogd();

			if($pers && !$pers->hasBadge('perry')) {
				$img = new HtmlImage('/Layout/Images/Icons/perry.png', _("Perry het vogelbekdier!"));

				$a = new HtmlAnchor('/Leden/'.$pers->geefID().'/Achievements?perry=\'daar!\'', $img);

				$div->add($a);
			}
		}

		$string = '';
		if ($this->modwhen) {
			if (hasAuth('ingelogd')) {
				$span = new HtmlSpan(null, 'fa fa-info-circle fa-aes');

				$span->setAttribute('data-toggle', 'tooltip');
				$span->setAttribute('title', $modtext = sprintf(_('Aangepast op %s door %s.'), htmlspecialchars($this->modwhen), htmlspecialchars($this->modwho)));

				$div->add($span);
			}
		}

		if ($this->editurl) {
			$span = HtmlSpan::fa('pencil', _('Aanpassen'));

			$div->add(new HtmlAnchor($this->editurl, $span));
		}

		if ($this->deleteurl) {
			$span = HtmlSpan::fa('times', _('Verwijderen'));

			$div->add(new HtmlAnchor($this->deleteurl, $span));
		}

		if (DEBUG) {
			$span = HtmlSpan::fa('arrow-up', _('Ga naar live'));

			$div->add(new HtmlAnchor("https://www.a-eskwadraat.nl".$request->server->get('PATH_INFO'), $span));
		}

		if (!DEBUG) {
			// Live-site OF demo-site:
			$lid = Persoon::getIngelogd();
			if ($lid && isset($DEBUG_SITES)
					&& array_key_exists($lid->getContactID(), $DEBUG_SITES)) {
				$debugSite = $DEBUG_SITES[$lid->getContactID()];
				$span = HtmlSpan::fa('arrow-down', _('Ga naar je debug'));
				$div->add(new HtmlAnchor("https://" . $debugSite . "-debug.a-eskwadraat.nl".$request->server->get('PATH_INFO'), $span));
			}
		}

		if (DEBUG && hasAuth('god') && $benamiteurl = $this->getBenamiteUrl()) {
			$span = HtmlSpan::fa('folder-open', _('Open in benamite'));

			$div->add(new HtmlAnchor($benamiteurl, $span));
		}

		if (hasAuth('ingelogd')) {
			$span = HtmlSpan::fa('thumbs-up', _('Feedback ☺'));

			$div->add(new HtmlAnchor("/Service/Bugweb/Melden/?pagina=".$request->server->get("REQUEST_URI"), $span));

			$span = HtmlSpan::fa('thumbs-down', _('Feedback ☹'));

			$div->add(new HtmlAnchor("/Service/Bugweb/Melden/?pagina=".$request->server->get("REQUEST_URI"), $span));
		}

		$div->add($string);

		return $div;
	}

	static private function makeFooter()
	{
		$footer = new HtmlDiv(
			new HtmlDiv(
				new HtmlDiv(
					new HtmlDiv(null, null, 'thick-bar'),
					'col-md-12'),
				'container'),
			'footer-scheiding');

		$footerDiv = new HtmlDiv(null, 'footer-distributed container-fluid');
		$footerDiv->add($left = new HtmlDiv(null, 'footer-left col-sm-4'));
		$left->add($h = new HtmlHeader(3, _('A&ndash;Eskwadraat')));
		$h->setNoDefaultClasses();
		$left->add($p = new HtmlParagraph(null, 'footer-links'));
		$p->add(array(new HtmlAnchor('/', _('Home')), '·',
			new HtmlAnchor('/Vereniging/Bestuur', _('Bestuur')), '·',
			new HtmlAnchor('/Vereniging/Contact', _('Contact'))));
		$left->add(new HtmlParagraph(sprintf('%s © %s', aesnaam(), date("Y")), 'footer-company-name'));

		$footerDiv->add($center = new HtmlDiv(null, 'footer-center col-sm-4'));
		$center->add(new HtmlDiv(array('<i class="fa fa-map-marker"></i>',
			new HtmlParagraph(new HtmlSpan(_('Princetonplein 5')) . _('3584CC Utrecht')))));
		$center->add(new HtmlDiv(array('<i class="fa fa-phone"></i>',
			new HtmlParagraph('+3130 253 4499'))));
		$center->add(new HtmlDiv(array('<i class="fa fa-envelope"></i>',
			new HtmlParagraph(new HtmlAnchor('mailto:bestuur@a-eskwadraat.nl', 'bestuur@a-eskwadraat.nl')))));

		$footerDiv->add($right = new HtmlDiv(null, 'footer-right col-sm-4'));
		$right->add($p = new HtmlParagraph(null, 'footer-company-about'));
		$p->add(new HtmlSpan(sprintf(_('Over %s[VOC: aesnaam]'), aesnaam())));
		$p->add(sprintf(_('%s[VOC: aesnaam] is de studievereniging voor de studies Gametechnologie, Informatica, Informatiekunde, Natuurkunde en Wiskunde'), aesnaam()));

		$right->add($icons = new HtmlDiv(null, 'footer-icons'));
		$icons->add(new HtmlAnchor('https://www.facebook.com/groups/103329707536/', '<i class="fa fa-facebook" style="line-height: 35px;"></i>'));
		$icons->add(new HtmlAnchor('https://twitter.com/aeskwadraat', '<i class="fa fa-twitter" style="line-height: 35px;"></i>'));
		$icons->add(new HtmlAnchor('https://www.linkedin.com/groups/55143/profile', '<i class="fa fa-linkedin" style="line-height: 35px;"></i>'));

		return $footer . $footerDiv;
	}

	/**
	 * @brief Geef de inhoud van de logs in een tabel weer.
	 *
	 * @return Een HTMLTable met de inhoud van de logger.
	 */
	private static function makeLogHistory()
	{
		global $logHistory;

		// Voorkom dat we crashen bij het bouwen van de log op live,
		// of bij de RIP je executie-fallback als $logHistory al gedestruct is.
		if (is_null($logHistory))
		{
			return '';
		}

		$tabelInhoud = [];
		foreach ($logHistory->records as $data)
		{
			$rij = [];
			$rij[] = $data['datetime']->format('c');
			$rij[] = $data['level_name'];
			$rij[] = new HtmlTableDataCell($data['message'], 'logstring');
			$tabelInhoud[] = $rij;
		}

		return HtmlTable::fromArray($tabelInhoud);
	}
}

function getParentTillRoot($request)
{
	if (!$request)
	{
		return ROOT_ID;
	}

	$pos = strpos($request, '/', 1);
	if($pos !== false) {
		$request = substr($request, 0, $pos);
	}
	$entry = hrefToEntry($request);
	if(!$entry) {
		return ROOT_ID;
	}
	return $entry->getId();
}

function makeVlaggetjes($small = false)
{
	global $request;

	$gets = $request->query->all();
	if(isset($gets['setlanguage'])) {
		unset($gets['setlanguage']);
	}

	if(getLang() == 'en') {
		$lang = 'nl';
		$icon = 'nederlands';
	} else {
		$lang = 'en';
		$icon = 'english';
	}

	$gets['setlanguage'] = $lang;

	if($small) {
		$a = new HtmlAnchor('?' . http_build_query($gets), $strong = new HtmlStrong(strtoupper($lang)));
		$a->addClass('navbar-toggle collapsed vlag-small');
		$strong->setCssStyle('color: white;');

		return $a;
	} else {
		$li = new HtmlListItem(null, 'vlaggetje');
		$li->setNoDefaultClasses();

		$img = new HtmlImage('/Layout/Images/Icons/'.$icon.'.png', strtoupper($lang), ucfirst($icon));
		$img->setCssStyle('height: 16px; width: auto;');

		$a = new HtmlAnchor('?' . http_build_query($gets), $img);

		$li->add($a);

		return $li;
	}
}

/**
 * Maakt een persooncontentmenu. Let op, deze funcie wordt aangeroepen in een
 * ul, dus je wilt altijd een li teruggeven
 */
function makePersoonContent($uriRef, $small = false)
{
	if(!$persoon = Persoon::getIngelogd())
		return makelogin();

	$cies = $persoon->mijnCiesGroepenDisputen('CIE');
	$groepen = $persoon->mijnCiesGroepenDisputen('GROEP');
	$disps = $persoon->mijnCiesGroepenDisputen('DISPUUT');

	$arr = array_filter(array($cies, $groepen, $disps));

	$cols = 1 + count($arr);

	$colam = floor(12/$cols);
	$return = [];

	// We stoppen alle lis in een array zodat we er makkelijk over kunnen loopen
	// om setnodefaultclasses op iedere li aan te roepen
	$lis = array();

	if($small)
		$lis[] = new HtmlListItem(new HtmlAnchor($persoon->url(), PersoonView::naam($persoon)));
	else
		$lis[] = new HtmlListItem(new HtmlAnchor($persoon->url(),
			PersoonView::naam($persoon)), 'dropdown-header-large');

	$lis[] = new HtmlListItem(new HtmlAnchor($persoon->url().'/Wijzig',
		_('Wijzig mijn gegevens')));
	$lis[] = new HtmlListItem(new HtmlAnchor('/Service/Intern/Logindata',
		_('Wachtwoord wijzigen')));
	$lis[] = new HtmlListItem(new HtmlAnchor($persoon->urlAchievements(),
		_('Mijn achievements')));

	if($persoon->heeftFotos())
		$lis[] = new HtmlListItem(new HtmlAnchor($persoon->fotoUrl(),
			_('Mijn foto\'s')));

	if($persoon->heeftPlanners())
		$lis[] = new HtmlListItem(new HtmlAnchor('/Activiteiten/Planner/',
			_('Mijn planners')));

	if($persoon->heeftDibs())
		$lis[] = new HtmlListitem(new HtmlAnchor('/Leden/Dibs/Home', _('Mijn DiBS')));

	$lis[] = new HtmlListItem(new HtmlAnchor(IOUBASE, _('Mijn I-Owe-U')));
	$lis[] = new HtmlListItem(new HtmlAnchor('/space/auth/logout.php?parent='
		.getParentTillRoot($uriRef['request']), _('Uitloggen'), null, 'nav'));

	if($small)
	{
		foreach($arr as $c)
		{
			$lis[] = new HtmlListItem(null, 'divider');
			$lis[] = new HtmlListItem($c['displayName'], 'dropdown-header');

			foreach($c['children'] as $a)
			{
				$lis[] = new HtmlListItem(new HtmlAnchor($a['url'], $a['displayName']));
			}
		}

		foreach($lis as $li)
		{
			$li->setNoDefaultClasses();
			$return[] = $li;
		}
	}
	else
	{
		foreach($lis as $li)
			$li->setNoDefaultClasses();

		// Voeg alle lis toe aan een ul
		$ul = new HtmlList(false, $lis, 'nav');
		$ul->setNoDefaultClasses();

		// Stop de ul in een li
		$li = new HtmlListItem($ul, 'col-md-'.$colam.' col-md-aes');
		$li->setNoDefaultClasses();

		$return[] = $li;

		foreach($arr as $c)
		{
			$lis = array();

			$li = new HtmlListItem(null, 'dropdown-header');
			$li->add(array($c['displayName'], new HtmlSpan(new HtmlSpan(null, 'caret'), 'caretClass')));
			$lis[] = $li;
			$lis[] = new HtmlListItem(null, 'divider hideableCies');

			foreach($c['children'] as $a) {
				$lis[] = new HtmlListItem(new HtmlAnchor($a['url'], $a['displayName']), 'hideableCies');
			}

			foreach($lis as $li)
				$li->setNoDefaultClasses();

			// Voeg alle lis van een groep samen in een ul
			$ul = new HtmlList(false, $lis, 'nav hideableCiesHeader');
			$ul->setNoDefaultClasses();

			// stop de ul in een li
			$li = new HtmlListItem($ul, 'col-md-'.$colam.' col-md-aes');
			$li->setNoDefaultClasses();
			$return[] = $li;
		}

	}

	return $return;
}

function bouwPersoonlijkMenu($isklein)
{
	global $uriRef;

	if($persoon = Persoon::getIngelogd()) {
		$anchorContent = [ PersoonView::naam($persoon) ];
	} else {
		$anchorContent = [ _("Login") ];
	}

	if($isklein) {
		$anchorContent[] = new HtmlSpan(null, 'caret');
	}

	$menu = new HtmlListItem(null, 'persoonlijk-menu dropdown' . ($isklein ? '' : ' menu-large'));
	$menu->add($a = new HtmlAnchor('#', $anchorContent, null, 'dropdown-toggle'));
	$a->setAttribute('data-toggle', 'dropdown');

	$menu->setNoDefaultClasses();

	$menu_ul = new HtmlList();
	$menu_ul->setNoDefaultClasses();
	$menu_ul->addClass('dropdown-menu' . ($isklein ? '' : ' megamenu'));

	if(!Persoon::getIngelogd()) {
		$login = makelogin($uriRef);

		$menu_ul->addClass('dropdown-menu megamenu no-padding');
		$menu_ul->add(new HtmlListItem($login, null, null, false));
	} else {
		$menu_ul->add(makePersoonContent($uriRef, $isklein));
	}

	$menu->add($menu_ul);
	return $menu;
}

/**
 * maak een login-windowtje.
 * @param redirRef (optional) de uriRef met info waar naar moet worden geredirect
 */
function makelogin($redirRef='')
{
	global $session, $uriRef;
	static $tabstop3;

	// Deze functie mag alleen geroepen worden als er nog niemand ingelogd is.
	assert(!$session->has('lidnr'));

	if (empty($redirRef['request'])
		|| strstr($redirRef['request'],'login.php')
		|| strstr($redirRef['request'],'logout.php') ) {
		$next = '/';
		$options = array();
	} else {
		$next = $redirRef['request'];
		$options = $redirRef['params'];
	}

	// In de javascript, wordt de action van dit form veranderd naar /space/auth/systeem.php als er met het systeemaccount wordt ingelogd.
	$ret = new HtmlForm('post', '/space/auth/login.php', true);
	$ret->addClass('form-signin inlog-form');
	$ret->setName('loginform');

	$next = tryPar('next', htmlspecialchars($next));

	// ondersteun meerdere loginboxes op dezelfde pagina
	// door tabstop3 static te houden.
	if ( empty($tabstop3) ) $tabstop3 = 0;
	$tabstop1 = $tabstop3+1;
	$tabstop2 = $tabstop1+1;
	$tabstop3 = $tabstop2+1;

	// voc: deze vier strings zijn voor het login form rechts-boven
	$loginlabel  = _("Login:");
	$passlabel   = _("Wachtwd:");
	$helplabel   = _("Hulp nodig?");
	$submitlabel = _("Login");

	// Plak queryparameters in de redirectie-URL.
	if (count($options))
	{
		$next .= '?' . http_build_query($options);
	}

	$ret->add($retDiv = new HtmlDiv());

	$retDiv->add($loginDiv = new HtmlDiv(null, 'login-div'));
	$loginDiv->add($inp = HtmlInput::makeText('loginnaam', '', 14, 'login-field', 'loginnaam', _('Login')));
	$inp->setAttribute('required', true);

	$retDiv->add($loginDiv = new HtmlDiv(null, 'login-div'));
	$loginDiv->add($inp = HtmlInput::makePassword('password', 14, 'login-field', 'password', _('Wachtwoord')));
	$inp->setAttribute('required', true);

	$retDiv->add($loginDiv = new HtmlDiv(null, 'checkbox login-div'));
	$loginDiv->add(new HtmlLabel('', array(
		HtmlInput::makeCheckbox('systeem', false, 'systeemCheckbox'),
		_('Systeemaccount')
	)));

	$retDiv->add(HtmlInput::makeHidden('next', $next));

	$retDiv->add(new HtmlButton('submit', $submitlabel, null, $submitlabel, 'btn btn-lg btn-primary btn-block'));
	$retDiv->add(new HtmlDiv(new HtmlAnchor('/Service/Intern/Logindata', $helplabel), 'login-div help-label'));
	return $ret;
}

/**
 * @brief Hetzelfde als HtmlPage maar dan met een lelijke underscore.
 *
 * Nodig voor backwards compatibility.
 *
 * @deprecated Gebruik HTMLPage want die past in de conventies.
 */
class Html_Page extends HTMLPage {
	public function __construct() {
		if (getZeurmodus()) {
			user_error("Denk aan de conventies, gebruik HTMLPage ipv Html_Page!", E_USER_DEPRECATED);
		}
		parent::__construct();
	}
}
