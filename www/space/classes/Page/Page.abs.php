<?php

use Symfony\Component\HttpFoundation\Response;

abstract class Page
{
	private static $instance = null;
	private static $voorkeurType = null;

	/**
	 * @brief Geef de singleton-instance van de Page-klasse.
	 *
	 * @deprecated Gebruik liever iets als new HTMLPage.
	 * Indien er nog geen instance gemaakt is van Page, wordt deze hier aangemaakt.
	 *
	 * @param type Het type van de page, het sterretje van de klasse *_Page.
	 * Default is het argument van setVoorkeur, of HTML als dat nog niet is gebeurd.
	 * @param forceCache Zet dit op true om te voorkomen dat een nieuw object gemaakt wordt.
	 * Dit is vooral handig als je bijvoorbeeld een errorhandler maakt die gestarte pages stopt.
	 * Als dit geset is en er is geen Page aangemaakt, is het resultaat null.
	 *
	 * @sa setVoorkeur
	 *
	 * @return Page Een Page-instance of null als forceCache aanstaat en er geen Page-instance gemaakt is.
	 */
	static public function getInstance($type = null, $forceCache = false)
	{
		// Als we diep in de krochten van space worden aangeroepen,
		// (iets met errorpagina's enzo: zie space/error-handler.php)
		// kan getZeurmodus nog niet gedefinieerd zijn.
		// TODO: zet zeurmodus-functies in los bestand zodat we gewoon include kunnen doen.
		if (function_exists('getZeurModus') && getZeurModus()) {
			user_error('Gebruik liever "new $typePage" ipv Page::getInstance', E_USER_DEPRECATED);
		}

		if(!$type)
		{
			if(self::$voorkeurType)
				$type = self::$voorkeurType;
			else
				$type = 'html';
		}

		if(!isset(self::$instance[$type]))
		{
			if ($forceCache)
			{
				return null;
			}

			switch ($type)
			{
				case 'innerhtml':	self::$instance[$type] = new InnerHTMLPage();	break;
				case 'cleanhtml': 	self::$instance[$type] = new CleanHTMLPage();	break;
				case 'html': 		self::$instance[$type] = new HTMLPage(); 		break;
				case 'ics': 		self::$instance[$type] = new Ics_Page(); 		break;
				case 'rss': 		self::$instance[$type] = new Rss_Page(); 		break;
				case 'text': 		self::$instance[$type] = new Text_Page(); 		break;
				default: user_error("Paginatype $type bestaat niet", E_USER_ERROR);	break;
			}
		}

		return self::$instance[$type];
	}

	static public function setVoorkeur($type)
	{
		self::$voorkeurType = $type;
	}

	/**
	 * @brief Stuur de gebruiker door naar een andere pagina.
	 *
	 * @deprecated gebruik responseRedirect, wat niet magisch is en codeduplicatie voorkomt.
	 *
	 * @param url De url om naar door te sturen.
	 * @param sec De tijd om op de huidige pagina te blijven.
	 */
	static public function redirect ($url, $sec = '0')
	{
		if (getZeurModus()) {
			user_error("codeduplicatie: gebruik responseRedirect", E_USER_DEPRECATED);
		}
		$instance = self::getInstance();
		if ($sec != 0) {
			$instance = self::getInstance();
			return $instance->inner_redirect($url, $sec);
		} else {
			spaceRedirect($url);
		}
	}

	/**
	 * @brief Stuur de gebruiker door naar een andere pagina, die een melding bevat.
	 *
	 * Overigens doet dit niets anders dan addMelding en responseRedirect.
	 *
	 * @param url De url om naar door te sturen.
	 * @param melding De tekst van de melding.
	 * @param soort Een van de geldige meldingssoorten.
	 *
	 * @sa addMelding
	 * @sa responseRedirect
	 *
	 * @return Response Een Response die de gebruiker doorstuurt.
	 */
	static public function responseRedirectMelding ($url, $melding, $soort = 'succes')
	{
		self::addMelding($melding, $soort);
		return responseRedirect($url);
	}

	/**
	 * @brief Stuur de gebruiker door naar een andere pagina, die een melding bevat.
	 *
	 * @deprecated gebruik Page::responseRedirectMelding, wat niet stateful is.
	 *
	 * @param url De url om naar door te sturen.
	 * @param melding De tekst van de melding.
	 * @param soort Een van de geldige meldingssoorten.
	 *
	 * @sa addMelding
	 * @sa responseRedirectMelding
	 */
	static public function redirectMelding ($url, $melding, $soort = 'succes')
	{
		self::addMelding($melding, $soort);

		if (getZeurModus()) {
			user_error("gebruik responseRedirectMelding voor minder statefulness", E_USER_DEPRECATED);
		}
		$instance = self::getInstance();
		if (headers_sent())
		{
			if(DEBUG)
				echo "<p>Redirect: <a href='" . $url . "'>$url</a></p>";
			else
				$instance->inner_redirect($url, 0);
			user_error('Cannot redirect, headers already sent.', E_USER_WARNING);
		}
		header('Location: ' . $url);
		exit();
	}

	/**
	 * Voegt een melding toe aan de volgende pagina die geladen wordt.
	 * $soort is 'succes', 'fout' of 'waarschuwing'
	 */
	static public function addMelding ($melding, $soort = 'succes')
	{
		global $session;

		$soort = strtolower($soort);
		$session->getFlashBag()->add($soort, $melding);
	}

	static public function addMeldingArray($msgArray, $soort = 'succes')
	{
		foreach($msgArray as $msg) {
			self::addMelding($msg, $soort);
		}
	}

	// Functie die van een js-bestand of de minified neerzet of als je
	// op de debug zit gewoon het bestand zelf terug geeft
	static public function minifiedFile($file, $ext = 'js', $forceMinified = false)
	{
		$ext = strtolower($ext);
		$extl = strlen($ext);
		if(substr($file, -5 - 1 * $extl) === ".min." . $ext) {
			$file = substr($file, 0, -5 - 1 * $extl);
		} elseif(substr($file, -1 - 1 * $extl) === "." . $ext) {
			$file = substr($file, 0, -1 - 1 * $extl);
		}

		$miniPrefix = '/Minified/' . $ext . '/';

		if(DEBUG && !$forceMinified) {
			$filePath = $miniPrefix . $file . "." . $ext;
			$mtime = filemtime(FS_ROOT . $filePath);
			return $miniPrefix . $file . "." . $mtime . "." . $ext;
		} else {
			$filePath = $miniPrefix . $file . ".min." . $ext;
			$mtime = filemtime(FS_ROOT . $filePath);
			return $miniPrefix . $file . "." . $mtime . ".min." . $ext;
		}
	}

	/**
	 * @brief Maak de paginaspecifieke headers van deze Page aan.
	 *
	 * Over het algemeen doet twee keer `$page->start()` aanroepen niet veel goeds,
	 * dus zorg ervoor dat dat niet gebeurt!
	 *
	 * @sa respond
	 *
	 * @param title De titel van deze pagina.
	 * (Het ligt aan de instance wat dit precies te betekenen heeft.)
	 *
	 * @return Page De page die net aangemaakt is.
	 */
	abstract public function start ($title);

	/**
	 * @brief Voeg wat inhoud toe aan de pagina.
	 *
	 * Over het algemeen zal een implementerende klasse parameters toevoegen
	 * die bijvoorbeeld een HTML-object accepteren,
	 * of arbitrair veel argumenten accepteren en die allemaal toevoegen.
	 *
	 * @return Page Het Page-object, zodat je `->add(...)->add(...)` kan doen.
	 */
	abstract public function add (...$children);

	/**
	 * @brief Rond de pagina af en schrijf het allemaal naar stdout.
	 *
	 * Gaat hilarisch stuk als je headers gebruikt enzo.
	 * Dit is deprecated, gebruik respond!
	 * @sa respond
	 */
	abstract public function end ();

	/**
	 * @brief Rond de pagina af en geef een mooie (HTML?)string.
	 *
	 * Als je een pagina maakt, wil je dit waarschijnlijk niet zelf gebruiken,
	 * maar iets als `new PageResponse($page)`.
	 *
	 * @return Een string die gebruikt kan worden als de content van een HTTP-response.
	 */
	abstract public function respond();

	/**
	 * @brief Geef de content-type header die hoort bij dit Page-object.
	 *
	 * Zou statisch moeten zijn, maar kan niet in PHP,
	 * want je wilt dit niet afhankelijk maken van de inhoud.
	 * Regel dat maar liever in je controller!
	 */
	abstract public function contentType();
}
