<?php
require_once 'space/classes/Rss/Item.php';

/**
 * Wordt gebruikt om een rss-feed naar het scherm te sturen
 */
class RSSPage extends Page
{
	private $started = false;

	private $content = array();

	/**
	 * @brief Maak de RSS-headers van deze Page aan.
	 *
	 * Over het algemeen doet twee keer `$page->start()` aanroepen niet veel goeds,
	 * dus zorg ervoor dat dat niet gebeurt!
	 *
	 * @sa respond
	 *
	 * @param string $title De titel van deze feed. Mag geen XML-karakters bevatten!
	 * @param string $description De omschrijving van deze feed. Mag geen XML-karakters bevatten!
	 * @return RSSPage
	 */
	public function start ($title = '', $description = '')
	{
		if (!$this->started)
		{
			// TODO: het zou cool zijn als we een of andere XML-klasse hadden
			// waarin we dit soort dingen konden doen zonder stringmanipulatie.
			$this->content[] = '<?xml version="1.0">';
			$this->content[] = '<rss version="2.0">';
			$this->content[] = '<channel>';
			$this->content[] = '<title>' . $title . '</title>';
			$this->content[] = '<link>https://www.a-eskwadraat.nl</link>';
			$this->content[] = '<description>' . $description . '</description>';
			$this->content[] = '<language>' . (getLang() == 'nl' ? 'nl-NL' : 'en-US') . '</language>';
			$this->content[] = '<lastBuildDate>' . date('c') . '</lastBuildDate>';
			$this->content[] = '<generator>' . aesnaam() . '</generator>';
			$this->content[] = '<managingEditor>bestuur@A-Eskwadraat.nl</managingEditor>';
			$this->content[] = '<webMaster>www@A-Eskwadraat.nl</webMaster>';
			$this->content[] = '<ttl>60</ttl>';
			$this->started = true;
		} else {
			// Zeur als twee keer wordt gestart want dat doet niet wat je verwacht.
			if (getZeurModus())
			{
				user_error("RSSPage::start() wordt twee keer aangeroepen, daar kan de code (nog) niet tegen", E_USER_WARNING);
			}
		}
		return $this;
	}

	/**
	 * @brief Voeg wat inhoud toe aan de pagina.
	 *
	 * Deze functie accepteert arbitrair veel argumenten,
	 * die een voor een toegevoegd worden.
	 * Deze argumenten kunnen zelf ook weer een array zijn,
	 * waarvan de elementen ook weer een voor een worden toegevoegd.
	 * (Een niet-array telt hier als een argument.)
	 *
	 * Vervolgens wordt het element in een `RSS_Item` gezet indien het nog geen `RSS_Item` is,
	 * en tenslotte wordt dit `RSS_Item` omgezet in een string en dat achteraan de content gezet.
	 * (TODO: dit proces is superingewikkeld en kan beter...)
	 *
	 * @return RSSPage Het Page-object, zodat je `->add(...)->add(...)` kan doen.
	 */
	public function add (...$c)
	{
		foreach ($c as $children)
		{
			if (!is_array($children))
			{
				$children = array($children);
			}

			foreach ($children as $child)
			{
				if (!$child instanceof Rss_Item)
				{
					$child = new Rss_Item($child);
				}
				$this->content[] = (string) $child;
			}
		}
		return $this;
	}

	/**
	 * @brief Rond de pagina af en geef een mooie (HTML?)string.
	 *
	 * Als je een pagina maakt, wil je dit waarschijnlijk niet zelf gebruiken,
	 * maar iets als `new PageResponse($page)`.
	 *
	 * @return Een string die gebruikt kan worden als de content van een HTTP-response.
	 */
	public function respond()
	{
		$headerEnBody = implode("\n", $this->content);
		$footer = "</channel>\n"
				. "</rss>\n";
		return $headerEnBody . $footer;
	}

	/**
	 * @brief Rond de pagina af en schrijf het allemaal naar stdout.
	 *
	 * Gaat hilarisch stuk als je headers gebruikt enzo.
	 * Dit is deprecated, gebruik respond!
	 * @sa respond
	 */
	public function end ()
	{
		if (getZeurModus())
		{
			user_error("Page::end is deprecated, gebruik een PageResponse!", E_USER_DEPRECATED);
		}
		echo $this->respond();
		exit;
	}

	/**
	 * @brief Geef de content-type header die hoort bij dit Page-object.
	 *
	 * Zou statisch moeten zijn, maar kan niet in PHP,
	 * want je wilt dit niet afhankelijk maken van de inhoud.
	 * Regel dat maar liever in je controller!
	 */
	public function contentType() {
		return 'application/rss+xml';
	}
}

/**
 * @brief Hetzelfde als RSSPage maar dan met een lelijke underscore.
 *
 * Nodig voor backwards compatibility.
 *
 * @deprecated Gebruik RSSPage want die past in de conventies.
 */
class Rss_Page extends RSSPage
{
	public function __construct()
	{
		if (getZeurmodus())
		{
			user_error("Denk aan de conventies, gebruik RSSPage ipv Rss_Page!", E_USER_DEPRECATED);
		}
	}
}
