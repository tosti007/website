<?php

function page_autoload($class)
{
	$pages = array(
		'Page' => 'Page.abs',
		'CleanHTMLPage' => 'CleanHtml',
		'CleanHtml_Page' => 'CleanHtml',
		'InnerHTMLPage' => 'InnerHtml',
		'HTMLPage' => 'Html',
		'Html_Page' => 'Html',
		'ICSPage' => 'Ics',
		'Ics_Page' => 'Ics',
		'RSSPage' => 'Rss',
		'Rss_Page' => 'Rss',
		'TextPage' => 'Text',
		'Text_Page' => 'Text',
	);

	if (array_key_exists($class, $pages)) {
		require_once('space/classes/Page/' . $pages[$class] . '.php');
	}
}
spl_autoload_register('page_autoload');

//TODO: nette oplosing vinden voor funncties buiten classes maar die wel in het
// bestand staand.
require_once('Html.php');
