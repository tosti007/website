<?php
declare(strict_types=1);

namespace Space;

use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\HttpFoundation\Response;

use Dili_Controller;
use Lid;
use Media;
use Persoon;
use Profiler;
use ProfilerTimerMark;
use ResponseException;
use function responseFromFile;
use Space\Auth\Auth;
use vfsEntry;

/**
 * Deze klasse regelt het parsen van de URI naar een entry,
 * de entry aanroepen om een response te krijgen,
 * en het tonen van de response.
 */
class Space
{
	/**
	 * @var ProfilerTimerMark $spacetm
	 * Deze TimerMark wordt opgestart zodra de request wordt geparst,
	 * en beëindigd voordat de request wordt gedisplayd.
	 */
	private $spacetm;

	public function __construct()
	{
		$this->spacetm = new ProfilerTimerMark('class Space');
		Profiler::getSingleton()->addTimerMark($this->spacetm);
	}

	/**
	 * Geef een uriRef die hoort bij de request.
	 *
	 * In de UriRef staat onder andere:
	 *  * welke entry hoort bij deze request
	 *  * of er nog een ongeparst stukje url overblijft
	 *  * of deze request bekeken mag worden
	 *
	 * @param string requestURI
	 * De URI, zonder GET-parameters, om te parsen.
	 * Komt overeen met de CGI-parameter PATH_INFO.
	 *
	 * @return array
	 * De uriRef-array.
	 */
	private function getUriRef($requestURI)
	{
		// $request wordt in space/init.php ingesteld.
		global $request;

		$uriRef = hrefToReference($requestURI, $request->query->all());

		// Randgevalletje: cross-origin XHR's van de introsite mogen wel,
		// hoewel dat van Benamite niet zomaar mag.
		// Daarom updaten we de uriRef nog even.

		// Regex die hostnamen van introsites matcht.
		$introHostRegex = '@https?://((bachelor|www)\.)?intro-utrecht\.nl@';

		// We laten cross-origin XHR's alleen toe vanaf de introsite.
		switch ($uriRef['request'])
		{
			case '/Leden/Intro/Aanmelden':
				$httpOrigin = $request->server->get('HTTP_ORIGIN');
				if ($httpOrigin && preg_match($introHostRegex, $httpOrigin))
				{
					$logger->info('Request is XHR van de introsite, dat staan we dus toe.');

					header('Access-Control-Allow-Origin: ' . $httpOrigin);
					header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
					header('Access-Control-Max-Age: 1000');
					header('Access-Control-Allow-Headers: Content-Type');

					// Geef aan dat onze toestemming afhangt van de origin.
					// Dat moet namelijk van Mozilla.
					header('Vary: Origin');
					$uriRef['access'] = true;
				}
				elseif (DEBUG)
				{
					$uriRef['access'] = true;
				}
				break;
		}

		return $uriRef;
	}

	/**
	 * Parse de URL naar een entry, bekom een response en toon die.
	 *
	 * Deze methode roept zo'n beetje alle andere methoden in de klasse aan,
	 * in de juiste volgorde.
	 * Wil je dus alleen maar een response geven op een request,
	 * dan kun je deze methode gebruiken.
	 * Voor geavanceerdere use cases moet je vooral kijken naar de andere publieke methoden.
	 *
	 * @see urlResponse
	 * @see getResponse
	 *
	 * @return void
	 */
	public function display()
	{
		global $request;

		// De $requestURI is de URI zonder servernaam en zonder GET-parameters.
		// In PHP heet die PATH_INFO.
		// (REQUEST_URI is inclusief GET-parameters).
		$requestURI = $request->server->get('PATH_INFO');
		$response = $this->urlResponse($requestURI);
		$response->send();
	}

	/**
	 * Parse de URL naar een entry, en geef de bijbehorende response.
	 *
	 * @see display
	 * @see getResponse
	 *
	 * @param string $requestURI
	 * De URL van de pagina om te tonen.
	 *
	 * @return Response
	 */
	public function urlResponse($requestURI)
	{
		global $request;

		$requestRef = $this->getUriRef($requestURI);
		return $this->getResponse($requestURI, $requestRef);
	}

	/**
	 * Stel globale variabelen in die bij elke request gebruikt worden.
	 *
	 * @return void
	 */
	function initGlobals()
	{
		global $cache;
		global $logger;
		global $session;

		// We kunnen de huidige tijd vervangen,
		// maar alleen indien de sessie geinitialiseerd is.
		if ($session && $session->get('override_datetime'))
		{
			$knownDate = new Carbon($session->get('override_datetime'));
			$logger->info("Datetime wordt nu " . $knownDate);
			Carbon::setTestNow($knownDate);
		}

		$cache = new TagAwareAdapter(
			new ApcuAdapter(),
			new ApcuAdapter()
		);
	}

	/**
	 * Geef de response voor de pagina met de gegeven url en uriRef.
	 *
	 * We stellen hier de waarde van $uriRef in,
	 * omdat zo'n beetje alle vfsEntry::display-implementaties die nodig hebben.
	 *
	 * @see display
	 * @see urlResponse
	 *
	 * @param string requestURI
	 * De URI, zonder GET-parameters.
	 * Komt overeen met de CGI-parameter PATH_INFO.
	 * Deze is nodig omdat we stiekem toch niet precies de $requestURI-inhoud
	 * precies gaan aanhouden: o.a. minified files zijn een speciaal geval.
	 *
	 * @param array $requestRef
	 * In de UriRef staat onder andere:
	 *  * welke entry hoort bij deze request
	 *  * of er nog een ongeparst stukje url overblijft
	 *  * of deze request bekeken mag worden
	 *
	 * @return Response
	 */
	public function getResponse($requestURI, $requestRef)
	{
		global $logger, $request, $session, $uriRef;

		$uriRef = $requestRef;
		$this->initGlobals();

		// Minified files willen we in principe ook via space kunnen versturen
		// in plaats van via Apache:
		// dit helpt runnen met bijvoorbeeld `php -t`.
		$minifiedPrefix = "/Minified/";
		if (substr($requestURI, 0, strlen($minifiedPrefix)) == $minifiedPrefix)
		{
			$logger->debug('Minified file via space');
			$pad = explode("/", $requestURI);
			if (count($pad) < 4)
			{
				$logger->info('Minified file ' . $requestURI . ' heeft foutief pad');
				return responseUitStatusCode(404);
			}
			// Pad ziet eruit als /Minified/$ext/$naamDelen.$datum.[min.]$ext
			$ext = $pad[2];
			$naamDelen = explode('.', $pad[3]);
			$logger->info(print_r($naamDelen, true));
			if (count($naamDelen) == 3)
			{
				$filenaam = $naamDelen[0] . '.' . $ext;
			}
			elseif (count($naamDelen) == 4)
			{
				$filenaam = $naamDelen[0] . '.min.' . $ext;
			}
			else
			{
				$logger->info('Minified file ' . $requestURI . ' heeft foutieve naamdelen');
				return responseUitStatusCode(404);
			}

			$lokaalPad = '/Minified/' . $ext . '/' . $filenaam;
			$logger->info('Minified file ' . $lokaalPad . ' sturen...');
			return responseFromFile($lokaalPad, null, FS_ROOT);
		}

		# Checken of er een foto opgevraagd wordt: dat kan prima buiten Benamite om
		if (substr($requestURI, 0, 15) == "/FotoWeb/Media/")
		{
			$logger->debug('Request is foto: Benamite wordt vermeden!');
			// Je moet ingelogd zijn om foto's te bekijken,
			// maar space/auth is nog niet ge-init dus vandaar in de session spieken.
			// TODO: space/auth is echt wel ge-init...
			if (!$uriRef['access'] || !$session->has('mylidnr'))
			{
				return responseUitStatusCode(403);
			}

			$entries = vfsVarEntryNames();

			// Dispatchen naar FotoWeb
			if (!is_null($entries))
			{
				if (sizeof($entries) == 1 && is_numeric($entries[0]))
				{
					return Media::passthruMediaFile('origineel', $entries[0]);
				}
				elseif (sizeof($entries) == 2 && is_numeric($entries[1]))
				{
					return Media::passthruMediaFile(strtolower($entries[0]), $entries[1]);
				}
			}

			// We snappen deze URL niet, dus error 404.
			return responseUitStatusCode(404);
		}

		// Een bijna-lid mag alleen een paar selecte pagina's bekijken
		$pers = Persoon::getIngelogd();
		if ($pers && $pers instanceof Lid && $pers->getLidToestand() == 'BIJNALID')
		{
			$logger->debug('Gebruiker is bijnalid, dus beperken tot bijnalidpagina\'s.');

			$logoutUrl = '/space/auth/logout.php';

			$toegestaneUrls = [
				$pers->url() . '/Wijzig',
				'/space/auth/login.php',
				$logoutUrl,
				'/Leden/Intro/Dili/Betalen',
				'/Leden/Intro/Dili/Betaald',
				'/Service/IDEAL/bankGekozen',
				'/Service/IDEAL/Klaar',
				'/Service/IDEAL/nieuwePoging',
			];

			if (DEBUG)
			{
				$toegestaneUrls[] = '/Service/';
				$toegestaneUrls[] = '/Service/Intern/Lidswitch/';
			}

			// Als de inschrijving niet meer open is, log het lid uit
			if (!Dili_Controller::inschrijvingOpen() && $uriRef['request'] != $logoutUrl)
			{
				$logger->info('Gebruiker is bijnalid maar inschrijving dicht: uitloggen!');
				return responseRedirect($logoutUrl);
			}

			if (!in_array($uriRef['request'], $toegestaneUrls))
			{
				$logger->info('Gebruiker is bijnalid op foute url: dwing tot inschrijven.');
				return responseRedirect($pers->url() . '/Wijzig');
			}
		}

		if (!$uriRef['access'])
		{
			return responseUitStatusCode(403);
		}

		$entry = $uriRef['entry'];
		if ($uriRef['rewrite'])
		{
			if (preg_match('#^https?://#i', $uriRef['remaining']) === 1)
			{
				return responseRedirect($uriRef['remaining'] . vfsGenParams());
			}
			else
			{
				return responseRedirect($entry->url()
					. (!empty($uriRef['remaining']) && !$entry->isDir() ? '/' : '')
					. $uriRef['remaining']
					. vfsGenParams());
			}
		}
		if (!empty($uriRef['remaining']) && !$entry->isHook())
		{
			return responseUitStatusCode(404);
		}

		// Klaar met allemaal special casing afhandelen,
		// nu kunnen we de echte entry displayen!
		$this->spacetm->markEnd();
		timer_mark('$entry->display()');
		return $this->displayEntry($entry);
	}

	/**
	 * Geef de Response die komt uit de display-methode van de gegeven vfsEntry.
	 *
	 * We krijgen een response op 2 manieren:
	 * * als alles goedgaat, returnt de entry een response
	 * * we vangen een ResponseException, bijvoorbeeld uit requireAuth of token-fouten
	 * In feite is dit dus een speciale wrapper om vfsEntry::display.
	 *
	 * We gaan ervan uit dat de volgende globals zijn ingesteld:
	 *  - $cache (door de constructor)
	 *  - $request (door space/init.php)
	 *  - $session (door space/init.php)
	 *  - $uriRef (door Space::getResponse)
	 *
	 * @param vfsEntry $entry
	 * De entry om te tonen.
	 *
	 * @return Response
	 * De Response die je krijgt uit het tonen van de entry.
	 */
	public function displayEntry(vfsEntry $entry)
	{
		global $logger;

		try
		{
			$response = $entry->display();
		}
		catch (ResponseException $e)
		{
			$logger->debug('ResponseException aangekomen in space.php, dus die wordt getoond.');
			return $e->maakResponse();
		}

		// Fallbackresponse is statuscode 501.
		if (!($response instanceof Response))
		{
			user_error("Pagina is nog niet beëindigd maar executie is afgelopen.", E_USER_WARNING);
			return responseUitStatusCode(501);
		}
		return $response;
	}
}
