<?php
require_once 'space/classes/VCalendar/Event.php';
 
/**
 * De basis klasse voor een vcalender-iets
 */
abstract class VCalendar_Item
{
	protected $description, $dtstart, $dtend, $dtstamp, $location, $organizer, $summary, $uid, $url;

	public function __construct ()
	{
	}

	public function __toString ()
	{
		$output = array();
		if (strlen($this->description) > 0)
			$output[] = 'DESCRIPTION:' . $this->description;
		if (strlen($this->dtstart) > 0)
			$output[] = 'DTSTART:' . $this->dtstart;
		if (strlen($this->dtend) > 0)
			$output[] = 'DTEND:' . $this->dtend;
		if (strlen($this->dtstamp) > 0)
			$output[] = 'DTSTAMP:' . $this->dtstamp;
		if (strlen($this->location) > 0)
			$output[] = 'LOCATION:' . $this->location;
		if (strlen($this->organizer) > 0)
			$output[] = 'X-ORGANIZER:' . $this->organizer;
		if (strlen($this->summary) > 0)
			$output[] = 'SUMMARY:' . $this->summary;
		if (strlen($this->uid) > 0)
			$output[] = 'UID:' . $this->uid;
		if (strlen($this->url) > 0)
			$output[] = 'URL:' . $this->url;

		$string = "";
		foreach($output as $line) {
			$line = self::escapeIcalText($line);
			while(strlen($line) > 74) {
				$string .= substr($line, 0, 74) . "\r\n ";
				$line = substr($line, 74);
			}
			$string .= $line."\r\n";
		}
		return $string;
	}

	/**
	 * Vervangt alle speciale karakters in $string zodat het een TEXT object is
	 * volgens de iCal specificaties.
	 * @see https://stackoverflow.com/questions/6191503/icalendar-text-data-type-preparing-values-with-php
	 */
	private function escapeIcalText($string)
	{
		// Vervang alle newline-achtigen met '\n'
		// Zie bug #7993
		$string = preg_replace("/(\r\n|\r|\n)/", "\\n", $string);
		// Dit moet ook volgens de specificaties (omdat dit speciale tekens
		// zijn)
		return preg_replace('/([\,;])/','\\\$1', $string);
	}

	public function getDescription ()
	{
		return $this->description;
	}

	public function getDTStart ()
	{
		return $this->dtstart;
	}

	public function getDTEnd ()
	{
		return $this->dtend;
	}

	public function getDTStamp()
	{
		return $this->dtstamp;
	}

	public function getLocation()
	{
		return $this->location;
	}

	public function getOrganizer ()
	{
		return $this->organizer;
	}

	public function getSummary ()
	{
		return $this->summary;
	}

	public function getUID ()
	{
		return $this->uid;
	}

	public function getUrl ()
	{
		return $this->url;
	}

	public function setDescription ($description)
	{
		$this->description = preg_replace('/\s\s+/', ' ', (string)$description);
		return $this;
	}

	public function setDTStart ($dtstart)
	{
		$this->dtstart = (string) $dtstart;
		return $this;
	}

	public function setDTEnd ($dtend)
	{
		$this->dtend = (string) $dtend;
		return $this;
	}

	public function setDTStamp($dtstamp)
	{
		$this->dtstamp = (string) $dtstamp;
		return $this;
	}

	public function setLocation($location)
	{
		$this->location = (string) $location;
		return $this;
	}

	public function setOrganizer ($organizer)
	{
		$this->organizer = (string) $organizer;
		return $this;
	}

	public function setSummary ($summary)
	{
		$this->summary = preg_replace('/\s\s+/', ' ', (string)$summary);
		return $this;
	}

	public function setUID ($uid)
	{
		$this->uid = (string) $uid;
		return $this;
	}

	public function setUrl ($url)
	{
		$this->url = (string) $url;
		return $this;
	}
}
