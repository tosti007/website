<?php
// $Id$

/*
 *   Klasse met geldfuncties
 */

class Money {
	// is het een significant bedrag? 
	// significant = strict groter dan 0.005 euro (strict groter dan een halve eurocent)
	static function isSignificant($bedrag){
		return (round($bedrag, 3) > 0.005);
	}

	// geef een prijs op nette wijze weer
	static function addPrice ($prijs, $htmloutput = true, $round = false, $allowempty = false) {
		if (is_null($prijs))
			return '-';

		$prijs = Money::parseMoney($prijs, $allowempty);

		if ($round){ // bedrag afronden op 2 decimalen
			$prijs = round($prijs, 2);
		}

		if(abs(round($prijs,2) - $prijs) > 1e-6){
			user_error('Oei, prijs te nauwkeurig: ' . $prijs, E_USER_WARNING);
		}

		if(round($prijs,2) == 0) $prijs = 0;		// (c) Hedzer  ;-) [dit is om -0.00 weergave te voorkomen (kink)]

		$res = "";
		if ($htmloutput){
			$res .= "&euro;&nbsp;";
		}

		$res .= number_format($prijs, 2, ',', '.');
		return $res;
	}

	// geef een inputveld voor een bedrag
	static function moneyInput($text, $name, $value = '') {
		$string = "<tr><td>$text</td>\n<td colspan=2>";
		$string .= HtmlInput::makeText($name,$value);
		$string .= "&nbsp;euro</td></tr>\n";
		return $string;
	}

	// parse userinput, net als parseMoney. Geeft echter geen errors, maar het
	// bedrag terug als alles in orde is, of false als er iets mis is.
	static function checkMoney($input, $allowempty = false){
		$input = trim($input);
		$input = strtr($input, ',', '.');

		$dots = substr_count($input, '.');
		if($dots > 1) {
			$input = implode(explode('.', $input, $dots));
		}

		$input = number_format($input, 2, '.', '');

		if ($allowempty && $input === ""){
			return "0";
		} elseif (is_numeric($input)) {
			return $input;
		}

		return false;
	}

	// maak een fatsoenlijk bedrag van de userinput
	static function parseMoney ($input, $allowempty = false, $fieldname = null) {
		// Een lege string betekent gewoon 0 euro
		if(empty($input))
			return 0;

		$output = Money::checkMoney($input, $allowempty);

		if(!is_numeric($output)) {
			print_error('Van dit &quot;bedrag&quot; in veld '.$fieldname.' kan ik geen chocolade maken: "'.htmlspecialchars($input).'"');
		}

		return $output;
	}
}
