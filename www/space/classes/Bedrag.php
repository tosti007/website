<?php
declare(strict_types=1);

namespace Space;

use function is_int;
use Iterator;
use function number_format;
use function preg_match;
use function round;
use function str_pad;
use function str_replace;
use function strlen;
use function substr;

/**
 * Deze klasse representeert bedragen in gehele eurocenten,
 * en bevat functies om ze netjes te formatteren en te delen.
 *
 * De operaties op deze klasse passen de objecten niet aan,
 * maar ze geven een nieuw object terug indien ze iets wijzigen.
 * Dit zodat je veilig kan zeggen $a = $b = new Bedrag();
 */
class Bedrag
{
	/**
	 * @var int $centen Het bedrag in centen.
	 */
	private $centen;

	/**
	 * Construeer een bedrag gegeven de hoeveelheid (hele) centen.
	 *
	 * Voor parsen uit floats e.d. zijn er statische methoden.
	 *
	 * @param int $centen Het bedrag in centen.
	 */
	public function __construct($centen)
	{
		assert(is_int($centen), 'Bedragen moeten bestaan uit gehele centen!');
		$this->centen = $centen;
	}

	/**
	 * Geef een stringvormige representatie.
	 *
	 * De inverse van deze functie is Bedrag::parse().
	 *
	 * @return string
	 */
	public function __toString()
	{
		$teken = '';
		if ($this->centen < 0)
		{
			$teken = '-';
		}

		// De centen moeten precies 2 getallen zijn.
		$centenStr = str_pad((string) abs($this->getRestcenten()), 2, '0', STR_PAD_LEFT);
		assert(strlen($centenStr) == 2);

		return $teken . abs($this->getGeheel()) . '.' . $centenStr;
	}

	/**
	 * Geef het gehele deel van dit bedrag.
	 *
	 * Afronden gebeurt richting 0,
	 * dus een schuld van anderhalve euro heeft getGeheel van -1, en getRestcenten van -50.
	 *
	 * Merk op dat sgn(getGeheel) == sgn(getRestcenten).
	 *
	 * @return int
	 */
	public function getGeheel()
	{
		return (int)($this->centen / 100); // PHP doet ook afronden richting 0.
	}

	/**
	 * Geef de centen die resteren bij de gehelen.
	 *
	 * Afronden gebeurt richting 0,
	 * dus een schuld van anderhalve euro heeft getGeheel van -1, en getRestcenten van -50.
	 *
	 * @return int
	 */
	public function getRestcenten()
	{
		return $this->centen % 100; // PHP doet ook afronden richting 0.
	}

	/**
	 * Geef een float met dit bedrag erin.
	 *
	 * Let op: deze operatie is lossy en kan dus leiden tot subtiele afrondfoutjes!
	 * (Zo is bijvoorbeeld 0.10 + 0.20 niet gelijk aan 0.30.)
	 *
	 * @return float
	 */
	public function alsFloat()
	{
		return (float)($this->getGeheel()) + ((float)$this->getRestcenten()) / 100.0;
	}

	/**
	 * Geef een menselijk leesbare weergave van dit bedrag.
	 *
	 * Let op: we garanderen niet dat dit teruggeparst kan worden tot een Bedrag!
	 */
	public function alsMenselijkeString()
	{
		// $this->alsFloat() is lossy maar dat geeft niet, men zoekt het wel uit.
		$getal = $this->alsFloat();
		// We doen geen money_format want die gaat er in en_GB-locales opeens ponden van maken*
		// in plaats daarvan een net euroteken en de string numeriek geformatteerd.
		// * dit is een faal in gettext, niet eens de schuld van PHP dus...
		return '€' . number_format($getal, 2);
	}

	/**
	 * Lees een bedrag uit van een string.
	 *
	 * De bedragenparser is een beetje ingewikkeld,
	 * maar we ondersteunen '.', ',' of ' ' als duizendtalafscheider,
	 * en '.' of ',' als decimaalafscheider.
	 * Als nuttig gevolg werken lakhs en crores ook,
	 * mocht iemand zich geroepen voelen het op zijn Indisch te groeperen.
	 *
	 * @param string $input
	 *
	 * @throws GeenChocolaException Als van dit bedrag geen chocola te maken valt.
	 *
	 * @return Bedrag
	 */
	public static function parse($input)
	{
		// Sowieso overbodige witruimte weghalen.
		$input = trim($input);

		if ($input === '')
		{
			throw new GeenChocolaException('Je moet wel een bedrag invullen');
		}

		// Bepaal of we decimalen hebben, en zo ja, welke afscheider.
		// Accepteer als afscheider ',' en '.' met 0, 1, of 2 decimalen erna.
		$matches = [];
		if (preg_match('/^(.*)[\,\.](\d{0,2})$/', $input, $matches))
		{
			$gehelenString = $matches[1];
			$centenString = $matches[2];
		}
		else
		{
			$gehelenString = $input;
			$centenString = '';
		}

		// De centen zijn nu een string van 0 à 2 decimale getallen,
		// gooi dat door de PHP-parser.
		if ($centenString == '')
		{
			$centen = 0;
		}
		else
		{
			$centen = (int)$centenString;
		}

		// Haal afscheiders uit de gehelen.
		$gehelenString = str_replace([' ', ',', '.'], '', $gehelenString);

		// Welk teken moet dit getal krijgen?
		// We moeten dit eraf parsen om de centen ook negatief te krijgen:
		// anders wordt -1.5 gelijk aan -1.0 + 0.5, niet -(1.0 + 0.5).
		if ($gehelenString[0] == '-')
		{
			$teken = -1;
			$gehelenString = substr($gehelenString, 1);
		}
		elseif ($gehelenString[0] == '+')
		{
			$teken = 1;
			$gehelenString = substr($gehelenString, 1);
		}
		elseif (is_numeric($gehelenString[0]))
		{
			// Geen expliciet teken? De default is positief.
			$teken = 1;
		}
		else
		{
			throw new GeenChocolaException('Kan geen chocola maken van het bedrag ' . $input . ': dat beginkarakter daar klopt niet van!');
		}

		// Laatste check op zinnige getalvormigheid:
		if (!preg_match('/^\d+$/', $gehelenString))
		{
			throw new GeenChocolaException('Kan geen chocola maken van het natuurlijke getal ' . $input);
		}
		$gehelen = (int)$gehelenString;

		// Zet het bedrag in elkaar.
		return new Bedrag($teken * ($gehelen * 100 + $centen));
	}

	/**
	 * Parse een bedrag of geef de default als de input null is.
	 * Merk op dat het parsen van een bedrag nog steeds kan falen.
	 *
	 * Een nuttige toepassing is deze methode aanroepen op de output van tryPar:
	 *     $bedrag = Bedrag::parseFromMaybe(tryPar('bedrag'));
	 *
	 * @param string|null $input
	 * @param int|Bedrag $default De defaultwaarde als er geen input is meegegeven.
	 * Indien int, wordt het meegegeven aan de Bedrag-constructor,
	 * anders moet het een Bedrag zijn.
	 * @return Bedrag
	 * @throws GeenChocolaException
	 */
	public static function parseFromMaybe($input, $default = 0)
	{
		if (is_int($default))
		{
			$default = new Bedrag($default);
		}
		assert($default instanceof Bedrag, 'defaultwaarde moet een Bedrag zijn');

		// Geef een default of parse het.
		if (is_null($input))
		{
			return $default;
		}
		return static::parse($input);
	}

	/**
	 * Probeer een zinnig Bedrag te maken van een float.
	 *
	 * Merk op dat floats lossy zijn, dus dit kan subtiel misgaan.
	 *
	 * @param float $input Een float met het bedrag in euro's.
	 *
	 * @return Bedrag
	 */
	public static function vanFloat($input)
	{
		$centenOngeveer = $input * 100;
		$centenAfgerond = (int)round($centenOngeveer, 0);
		return new Bedrag($centenAfgerond);
	}

	/**
	 * Geef het hele bedrag in centen.
	 *
	 * Een bedrag van een euro geeft dus 100 centen.
	 *
	 * Hoewel je fijn kan rekenen met deze waarden, is het netter om berekeningen in de Bedrag-klasse te stoppen.
	 *
	 * @return int
	 */
	public function getCenten()
	{
		return $this->centen;
	}

	/**
	 * Geef een nieuw Bedrag dat de som is van deze twee bedragen.
	 *
	 * @param Bedrag $ander
	 * @return Bedrag
	 */
	public function plus(Bedrag $ander)
	{
		return new Bedrag($this->getCenten() + $ander->getCenten());
	}
	/**
	 * Geef een nieuw Bedrag dat het verschil is van deze twee bedragen.
	 *
	 * @param Bedrag $ander
	 * @return Bedrag
	 */
	public function min(Bedrag $ander)
	{
		return new Bedrag($this->getCenten() - $ander->getCenten());
	}

	/**
	 * Vermenigvuldig dit Bedrag scalair met een geheel getal.
	 *
	 * @param int $factor
	 * @return Bedrag
	 */
	public function keer($factor)
	{
		assert(is_int($factor), "Een bedrag mag alleen keer een int!");
		return new Bedrag($this->centen * $factor);
	}

	/**
	 * Deel dit bedrag in gelijke stukken en geef de rest.
	 *
	 * @param int $teller
	 * @return Bedrag[] Een array: `[0 => $deling, 1 => $rest]`.
	 */
	public function deelmod($teller)
	{
		assert(is_int($teller), "Een bedrag mag gedeeld worden door een int!");

		$delingCenten = $this->centen / $teller; // Int gedeeld door int is delen met truncatie.
		$restCenten = $this->centen % $teller;
		return [new Bedrag($delingCenten), new Bedrag($restCenten)];
	}

	/**
	 * Geef een nieuw Bedrag dat gelijk is aan -1 keer dit bedrag..
	 *
	 * @return Bedrag
	 */
	public function negatief()
	{
		return new Bedrag(-1 * $this->getCenten());
	}

	/**
	 * Neem de som van een iterator vol Bedragen.
	 *
	 * Een lege iterator geeft 0.
	 *
	 * @param Iterator $bedragem
	 *
	 * @return Bedrag
	 */
	public static function sommeer($bedragen)
	{
		$resultaat = new Bedrag(0);
		foreach ($bedragen as $bedrag)
		{
			$resultaat = $resultaat->plus($bedrag);
		}
		return $resultaat;
	}

	/**
	 * Gaat het om een niet-verwaarloosbaar bedrag?
	 *
	 * @return bool
	 */
	public function isSignificant()
	{
		return $this->centen != 0;
	}

	/**
	 * Gaat het om een (strikt) positief bedrag?
	 *
	 * @return bool
	 */
	public function isPositief()
	{
		return $this->centen > 0;
	}

	/**
	 * Gaat het om een (strikt) negatief bedrag?
	 *
	 * @return bool
	 */
	public function isNegatief()
	{
		return $this->centen < 0;
	}
}
