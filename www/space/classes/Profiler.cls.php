<?php

// backwards compatibility:
function timer_mark($what){
	if (! $what instanceof ProfilerTimerMark){
		// $what moet nog even een object worden
		$what = new ProfilerTimerMark($what);
	}

	Profiler::getSingleton()->addTimerMark($what);
	return $what;
}

function timer_dump(){
	Profiler::getSingleton()->printStats();
}

function counter_mark($what){
	Profiler::getSingleton()->incrementCounter($what);
}

function profiler_dump(){
	Profiler::getSingleton()->printStats();
}

/**
	Classe (kan singleton gebruikt worden) om performance te meten. 
	Met dank aan Jeroen van	Wolffelaar, Peter van de Werken e.a.
*/
class Profiler
{
	var $_timerStart = 0;

	// Array met ProfilerTimerMark-objecten
	var $_timers = null;

	// Simpeler array:
	// $_counters["functienaam"] = counter
	var $_counters = null;

	// Constructor
	public function __construct()
	{
		$this->_timers = array();
		$this->_counters = array();
		$this->_timerStart = $this->microtime_float();
	}

	// Stopt de timer en print een leuk overzicht van bevindingen van timers en counters
	function printStats($minFunctionTime = null)
	{
		if (!DEBUG)
			return; // geen debug-site: niks doen!

		if (!isset($this) || ! ($this instanceof Profiler)){
			// Static call!
			Profiler::getSingleton()->printStats($minFunctionTime);
			return;
		}

		$timers = $this->_timers;
		$counters = $this->_counters;

		echo "<div class=\"profiler\">";
		if (sizeof($timers) != 0){
			echo "<pre>Timers (in seconden):\n";
			echo "start  duration\n";

			$parentStack = array();
			$lastTimer = null;
			foreach($timers as $timer) {
				if ($timer->childOf($lastTimer)){
					array_push($parentStack, $lastTimer);
				} else {
					// parentStack van achter naar voren doorlopen,
					// als de huidige timermark geen kind is van een
					// item op de omgekeerde stack, dan kan er een item
					// van de originele stack gepopped worden
					$popcount = 0;
					for ($i = sizeof($parentStack) - 1; $i >= 0; $i--){
						$parent = $parentStack[$i];

						if (!$timer->childOf($parent)){
							array_pop($parentStack);
							$popcount++;
						}
					}

					// meer dan 2 sprongen "terug"? Newline voor duidelijkheid
					if ($popcount > 1) echo "\n";
				}

				$relative = round(abs($timer->getMarkTime() - $this->_timerStart), 4);
				$functiontime = "";
				if ($timer->getEndTime() != 0) {
					$functiontime = number_format(abs($timer->getEndTime() - $timer->getMarkTime()), 4);
				}

				$indent = str_repeat(" ", sizeof($parentStack));

				if ($minFunctionTime === null || $functiontime > $minFunctionTime){
					if ($lastTimer != null && $lastTimer->getEndTime() != 0 && $timer->getMarkTime() - $lastTimer->getEndTime() > 0.01){
						// time jump!
						$jumpsize = number_format($timer->getMarkTime() - $lastTimer->getEndTime(), 4);

						echo "                  $indent (jump $jumpsize secs)\n";
					}

					printf("%6s %6s   - %s %s \n"
						, number_format($relative, 4)
						, $functiontime
						, $indent, $timer->getName()
					);
				}

				$lastTimer = $timer;
			}
			echo "</pre>";
		}

		if (sizeof($counters) != 0){
			echo "<pre>Counters:\n";
			ksort($counters);
			foreach($counters as $functionname => $count) {
				printf("%4d - %s\n", $count, $functionname);
			}
			echo "</pre>";
		}

		echo "</div>";
	}

	/**
		Voegt een timermark toe. Kan zowel
		als dynamisch ($prf->addTimerMark(...)) aangeroepen worden
	*/
	function addTimerMark($timer)
	{
		if (!$timer instanceof ProfilerTimerMark ){
			print_error("Profiler::addTimerMark verwacht een parameter met type ProfilerTimerMark");
		} elseif (!isset($this) || ! $this instanceof Profiler){
			Profiler::getSingleton()->addTimerMark($timer);
			return;
		}

		$this->_timers[] = $timer;
	}

	/**
		Hoogt een functiecounter op met 1
	*/
	function incrementCounter($functionname)
	{
		if (!isset($this) || ! ($this instanceof Profiler)){
			Profiler::getSingleton()->incrementCounter($functionname);
			return;
		}

		if (!isset($this->_counters["$functionname"])) {
			$this->_counters["$functionname"] = 0;
		}
		$this->_counters["$functionname"]++;
	}

	/**
		Simple function to replicate PHP 5 behaviour:
		returns a float (in seconds) of current time
	 */
	function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	static function getSingleton(){
		static $instance = null;

		if ($instance === null){
			$instance = new Profiler();
		}
		return $instance;
	}
}

/**
	Simpele datastructuur
*/
class ProfilerTimerMark
{
	var $_markTime = 0;
	var $_endTime = 0;
	var $_name = "";

	public function __construct($name) {
		$this->_name = $name;
		$this->_markTime = Profiler::getSingleton()->microtime_float();
	}

	function markEnd() {
		$this->_endTime = Profiler::getSingleton()->microtime_float();
	}

	function getName() {		return $this->_name; }
	function getMarkTime() {	return $this->_markTime; }
	function getEndTime() {		return $this->_endTime; }
	function toString() {		return $this->_name; }

	/**
		timer1.chilOf($timer2) is true, dan en slechts dan als:
			timer1._markTime > timer2._markTime
			timer1._endTime < timer2._endtime
	*/
	function childOf($otherTimer) {
		if ($this->_endTime == 0 || $otherTimer->_endTime == 0)
			return false;
		if ($otherTimer == null)
			return false;
		if (!$otherTimer instanceof ProfilerTimerMark)
			return false;

		$res = ($this->_markTime < $otherTimer->_endTime);

		return $res;
	}
}

timer_mark('PAGE START');
