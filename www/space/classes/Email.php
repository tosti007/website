<?php

class Email
{
	protected $to;
	protected $from;
	protected $replyto;
	protected $subject;
	protected $body;
	protected $headers = array();
	protected $isHtml = true;
	protected $attachments = array();
	protected $separator;

	// De functie quoted_printable_encode zet bij een linebreak een '=' neer 
	// wat in bv gmail naar een spatie vertaald wordt
	static public function quoted_printable_encode2($input, $line_max = 75, $html = false) {
		$hex = array('0','1','2','3','4','5','6','7',
			'8','9','A','B','C','D','E','F');
		$lines = preg_split("/(?:\r\n|\r|\n)/", $input);
		if($html) {
			$linebreak = "=\r\n";
		} else {
			$linebreak = "=0D=0A\r\n";
		}
		/* the linebreak also counts as characters in the mime_qp_long_line
		 * rule of spam-assassin */
		$line_max = $line_max - strlen($linebreak);
		$escape = "=";
		$output = "";
		$cur_conv_line = "";
		$length = 0;
		$whitespace_pos = 0;
		$addtl_chars = 0;

		// iterate lines
		for ($j=0; $j<count($lines); $j++) {
			$line = $lines[$j];
			$linlen = strlen($line);

			// iterate chars
			for ($i = 0; $i < $linlen; $i++) {
				$c = substr($line, $i, 1);
				$dec = ord($c);

				$length++;

				if ($dec == 32) {
					// space occurring at end of line, need to encode
					if (($i == ($linlen - 1))) {
						$c = "=20";
						$length += 2;
					}

					$addtl_chars = 0;
					$whitespace_pos = $i;
				} elseif ( ($dec == 61) || ($dec < 32 ) || ($dec > 126) ) {
					$h2 = floor($dec/16); $h1 = floor($dec%16);
					$c = $escape . $hex["$h2"] . $hex["$h1"];
					$length += 2;
					$addtl_chars += 2;
				}

				// length for wordwrap exceeded, get a newline into the text
				if ($length >= $line_max) {
					$cur_conv_line .= $c;

					// read only up to the whitespace for the current line
					$whitesp_diff = $i - $whitespace_pos + $addtl_chars;

					/* the text after the whitespace will have to be read
					 * again ( + any additional characters that came into
					 * existence as a result of the encoding process after the whitespace)
					 *
					 * Also, do not start at 0, if there was *no* whitespace in 
					 * the whole line */
					if (($i + $addtl_chars) > $whitesp_diff) {
						$output .= substr($cur_conv_line, 0, (strlen($cur_conv_line) -
							$whitesp_diff)) . $linebreak;
						$i =  $i - $whitesp_diff + $addtl_chars;
					} else {
						$output .= $cur_conv_line . $linebreak;
					}

					$cur_conv_line = "";
					$length = 0;
					$whitespace_pos = 0;
				} else {
					// length for wordwrap not reached, continue reading
					$cur_conv_line .= $c;
				}
			} // end of for

			$length = 0;
			$whitespace_pos = 0;
			$output .= $cur_conv_line;
			$cur_conv_line = "";

			if ($j<=count($lines)-1) {
				$output .= $linebreak;
			}
		} // end for

		if($html) {
			return preg_replace('/\R+/', "\n", trim($output));
		} else {
			return trim($output);
		}
	} // end quoted_printable_encode

	public static function substituties ($obj)
	{
		$naam = geefNaam($obj);
		// Als er geen naam bijhoort zijn andere subs ook niet zinnig.
		if ($naam === NULL)
		{
			return array();
		}

		$ret = array('%naam%' => $naam);
		if ($obj instanceof Persoon)
		{
			$studies = '';
			if ($obj instanceof Lid)
			{
				$studies = LidStudieVerzamelingView::simpel($obj->getStudies());
			}
			if (empty($studies))
			{
				$studies = _('studieloos');
			}
			$ret = array_merge($ret, array(
				'%voorletters%' => PersoonView::waardeVoorletters($obj),
				'%voornaam%' 	=> PersoonView::waardeVoornaam($obj),
				'%studies%' 	=> $studies,
				'%telefoonnr%' 	=> ContactTelnrView::waardeTelefoonnummer($obj->getPrefTelnr())
			));
		}
		return $ret;
	}

	public function __construct ()
	{
	}

	public function getTo ()
	{
		return $this->to;
	}

	public function getFrom ()
	{
		return $this->from;
	}

	public function getReplyTo ()
	{
		return $this->replyto;
	}

	public function getSubject ()
	{
		return $this->subject;
	}

	public function getBody ()
	{
		return $this->body;
	}

	public function setTo ($to)
	{
		$this->to = $to;
		return $this;
	}

	public function setFrom ($from)
	{
		if (!is_null($from) && !self::validFrom($from))
			user_error("Email: gebruik setFrom met Contact, Commissie of Bug)", E_USER_ERROR);

		$this->from = $from;
		return $this;
	}

	public function setReplyTo ($replyto)
	{
		$this->replyto = $replyto;
		return $this;
	}

	public function setSubject ($subject)
	{
		$this->subject = $subject;
		return $this;
	}

	public function setBody ($body, $html = true)
	{
		$this->body = $body;
		$this->isHtml = (bool) $html;
		return $this;
	}

	public function setAttachment($type, $naam, $data)
	{
		$this->attachments[] = array($type, $naam, chunk_split(base64_encode($data)));
	}

	/**
	 * @brief Voeg een attachment toe aan deze mail op basis van een bestand.
	 *
	 * @see setAttachment
	 *
	 * @param type Het MIME-type van deze attachment.
	 * @param filename Het (absolute) pad naar het bestand dat we attachen.
	 * @param naam Indien niet NULL, de bestandsnaam die de ontvanger ziet. Default: basename($filename).
	 */
	public function setAttachmentFromFile($type, $filename, $naam = NULL)
	{
		if (is_null($naam)) {
			$naam = basename($filename);
		}
		$data = file_get_contents($filename);
		if ($data === FALSE) {
			trigger_error("Bestand voor attachment kon niet gelezen worden.", E_USER_ERROR);
		}
		$this->setAttachment($type, $naam, $data);
	}

	/**
	 * @brief Kijkt of $from een valide afzender-object is
	 * @param from het afzend-'object'
	 * $from mag een Contact, Commissie, Bug of BestuursLid zijn.
	 * Hier bestaat een $from->getEmail() functie op, waardoor het werkelijke
	 * emailadres wordt aangeroepen in sendmail.
	 * @return of $from geldig is voor afzender
	 */
	public static function validFrom($from)
	{
		return
			$from instanceof Contact ||
			$from instanceof Commissie ||
			$from instanceof Bug ||
			$from instanceof BestuursLid;
	}

	public function valid ()
	{
		if (!$this->to)
			return false;
		if (!self::validFrom($this->from))
			return false;
		if (is_null($this->from->getEmail()))
			return false;
		if (strlen($this->subject) < 2)
			return false;
		if (strlen($this->body) < 2)
			return false;
		return true;
	}

	public function send ()
	{
		// Vieze hack zodat een emailadres-string of object ook als zodanig gebruikt wordt
		if (!($this->to instanceof Iterator))
		{
			$this->to = array($this->to);
		}

		foreach ($this->to as $receiver)
		{
			$content = $this->body;

			if (is_string($receiver))
				$email = $receiver;
			else
			{
				$email = geefEmail($receiver);
				if(is_null($email) || empty($email))
				{
					//TODO: Hier nog een goede afhandeling doen
					return;
				}
				foreach(self::substituties($receiver) as $key=>$value)
					$content = str_replace($key, $value, $content);
			}

			//Als het geen simpele plain text mailing is, maak er een multipart van:
			if($this->isHtml || $this->attachments)
			{
				$this->isHtml = true;

				$this->separator = md5(date('r', time()));
				$this->headers["Content-Type"] = 'Content-Type: multipart/mixed; boundary="PHP-mixed-' . $this->separator . '"';

				$contentHtml = $content;
				//Replace basic html met newlines om het leesbaarder te maken.
				$contentPlain = str_replace("<br />","\r\n",$contentHtml);
				$contentPlain = str_replace("<hr />","\r\n-----\r\n",$contentHtml);
				$contentPlain = str_replace("<p>","\r\n\r\n",$contentPlain);
				$contentPlain = strip_tags($contentPlain);

				$content = "--PHP-mixed-" . $this->separator ."\n";
				$content .= 'Content-Type: multipart/alternative; boundary="PHP-alt-' . $this->separator .'"' ."\n\n";

				$content .= "--PHP-alt-" . $this->separator ."\n";
				$content .= 'Content-Type: text/plain; charset="UTF-8"' ."\n";
				$content .= 'Content-Transfer-Encoding: quoted-printable' ."\n\n";
				$content .= Email::quoted_printable_encode2($contentPlain) ."\n\n";

				$content .= "--PHP-alt-" . $this->separator ."\n";
				$content .= 'Content-Type: text/html; charset="UTF-8"' ."\n";
				$content .= 'Content-Transfer-Encoding: quoted-printable' ."\n\n";
				$content .= Email::quoted_printable_encode2($contentHtml, 75, true) ."\n\n";

				$content .= "--PHP-alt-" . $this->separator ."--\n\n";

				foreach($this->attachments as $att)
				{
					$content .= "\n";

					$type = $att[0];
					$naam = $att[1];
					$data = $att[2];
					$content .= "--PHP-mixed-" . $this->separator ."\n";
					$content .= 'Content-Type: ' . $type . '; name="' . $naam .'"' ."\n";
					$content .= "Content-Transfer-Encoding: base64\n";
					$content .= "Content-Disposition: inline\n\n";

					$content .= $data;
				}

				$content .= "--PHP-mixed-" . $this->separator . '--';
			}

			$fromMail = $this->from->getEmail();
			sendmail($fromMail,						// from
					$email, 						// to
					$this->subject, 				// subject
					$content, 						// content
					(isset($this->replyto)) ? $this->replyto : $fromMail, //reply to
					implode("\n", $this->headers),	// extra
					'', 							// bounceto
					$this->isHtml					// html?
			);
		}
	}
}

