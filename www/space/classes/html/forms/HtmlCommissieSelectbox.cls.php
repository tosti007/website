<?php

/**
	Maakt een selectbox met daarin de gewenste commissies
*/
class HtmlCommissieSelectbox
	extends HtmlSelectbox
{
	private $_commissies;
	private $_selectedCieNr;
	private $_canSelectNoCie;

	public function __construct($selectboxname = "commissie", CommissieVerzameling $commissies = null)
	{
		parent::__construct($selectboxname);

		if(!$commissies) {
				$commissies = new CommissieVerzameling();
		}
		$this->setCommissies($commissies);
	}

	/**
	 * Sla een CLONE op van de meegegeven verzameling. Het zou namelijk kunnen
	 * dat de verzameling in deze selectbox later wordt aangepast (wellicht
	 * worden er nog commissies toegevoegd). Het is in dat geval onwenselijk
	 * dat de oorspronkelijke verzameling ook wijzigt.
	 */
	public function setCommissies(CommissieVerzameling $commissies)
	{
		$this->_commissies = clone $commissies;
	}

	public function getCommissies()
	{
		return $this->_commissies;
	}

	public function canSelectNoCie($state = true)
	{
		$this->_canSelectNoCie = $state;
	}

	public function setSelectedCommissie($cie)
	{
		if($cie instanceof Commissie) {
			$_selectedCieNr = $cie->geefID();
		} else {
			$_selectedCieNr = $cie;
		}
	}

	public function prepareHtml()
	{
		if($this->_canSelectNoCie) {
			$this->addChild(new SelectboxOption());
		}

		$this->_commissies->sorteer('naam');
		foreach($this->_commissies as $cienr => $cie)
		{
			$option = new HtmlSelectboxOption($cienr, $cie->getNaam());
			$this->addChild($option);

			if($cienr == $this->_selectedCieNr){
				$option->setSelected();
			}
		}
	}
}
