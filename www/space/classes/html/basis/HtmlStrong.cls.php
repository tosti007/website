<?php
// $Id$

/**
 *	@brief Deze class implementeert een HTML Strong container: &lt;strong&gt;
 */
class HtmlStrong
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw strong-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlStrong 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('strong', true);

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
