<?
// $Id$

/**
 *	@brief Class om een HTML div-element te produceren
 */
class HtmlDiv
	extends HtmlContainer
{
	/**
	 * @brief Construeer een div-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlDiv automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('div', true);
		$this->_allowedAttributes[] = "class";
		$this->_allowedAttributes[] = "dir";
		$this->_allowedAttributes[] = "id";
		$this->_allowedAttributes[] = "onclick";
		$this->_allowedAttributes[] = "ondblclick";
		$this->_allowedAttributes[] = "onkeydown";
		$this->_allowedAttributes[] = "onkeypress";
		$this->_allowedAttributes[] = "onkeyup";
		$this->_allowedAttributes[] = "onmousedown";
		$this->_allowedAttributes[] = "onmousemove";
		$this->_allowedAttributes[] = "onmouseout";
		$this->_allowedAttributes[] = "onmouseover";
		$this->_allowedAttributes[] = "onmouseup";
		$this->_allowedAttributes[] = "style";
		$this->_allowedAttributes[] = "title";

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}
	
	public static function makeNumberMenu($min, $max, $offset, $linkprefix)
	{
		$result = "";
		$begin = max($min, $offset - 3);
		$eind = min($max, $offset + 3);
		
		//Pijltje ervoor maken
		if ($offset != $min)
		{
			$prev = $offset - 1;
			$result .= new HtmlAnchor($linkprefix.$prev, "<") . " ";
		}
		
		//Ook eerste tonen
		if ($begin != $min)
		{
			$result .= new HtmlAnchor($linkprefix.$min, $min) . " ";
			if ($begin - $min > 1)
				$result .= "... ";
		}
			
		//Links ertussen
		for ($i = $begin; $i <= $eind; $i++)
		{
			//Niet de huidige link
			if ($i != $offset)
			{
				$a = new HtmlAnchor($linkprefix.$i, $i);
				$result .= $a . " ";
			}
			else
				$result .= $i . " ";
		}
		
		//Ook laatste tonen
		if ($eind != $max)
		{
			if ($max - $eind > 1)
				$result .= "... ";
			$result .= new HtmlAnchor($linkprefix.$max, $max);
		}
		
		//Pijltje erachter maken
		if ($offset != $max)
		{
			$next = $offset + 1;
			$result .= " " . new HtmlAnchor($linkprefix.$next, ">");
		}
		
		return new HtmlDiv(new HtmlParagraph($result));
	}

	/**
	 * @brief Maak een div voor generieke meldingen, bijv. na het POST-en van een form.
	 *
	 * @param meldingen Array van meldingen die in de div moeten.
	 * @param soort Geeft de soort melding aan: 'succes', 'waarschuwing' of 'fout'.
	 *
	 * @returns Een HtmlDiv met de meldingen in de juiste stijl.
	 */
	public static function makeMeldingen($meldingen, $soort = 'succes') {
		switch($soort) {
			case 'succes':
				$alertclass = 'success';
				break;
			case 'fout':
				$alertclass = 'danger';
				break;
			case 'waarschuwing':
			default:
				$alertclass = 'warning';
				break;
		}
		$alertclass = 'alert-' . $alertclass;

		$div = new HtmlDiv(array(), $alertclass . ' alert alert-dismissible');
		$div->setAttribute('role', 'alert');

		$closeIcon = new HtmlSpan('&times;');
		$button = new HtmlButton('button', $closeIcon, null, null, 'close');
		$button->setAttribute('data-dismiss', 'alert');
		$div->addImmutable($button);

		foreach ($meldingen as $melding) {
			// addImmutable() is wat efficienter dan gewoon addChild().
			$div->addImmutable(new HtmlParagraph($melding));
		}

		return $div;
	}

	/**
	 * @brief Zelfde als makeMeldingen(), maar voor een enkele melding.
	 */
	public function makeMelding($melding, $soort = 'succes') {
		return static::makeMeldingen(array($melding), $soort);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
