<?
// $Id$

/**
 *	@brief Class om een HTML code-element te produceren
 */
class HtmlCode
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw code-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlCode
	 * automatisch als kind(eren) krijgt.
	 */
	public function __construct($children = array())
	{
		parent::__construct('code', true);
		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
