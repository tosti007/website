<?
// $Id$

/**
 *	@brief Class om een HTML pre-element te produceren
 */
class HtmlPre
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw pre-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlPre
	 * automatisch als kind(eren) krijgt.
	 */
	public function __construct($children = array())
	{
		parent::__construct('pre', true);

		$this->addChildren($children);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
