<?php
// $Id$

/**
 *	@brief Implementeert een HTML script-container
 */
class HtmlScript
	extends HtmlContainer
{
	/**
	 *	@brief Simpele constructor voor HtmlScript
	 *
	 *	@param type het soort script, default='text/javascript'
	 */
	public function __construct($type = "text/javascript")
	{
		parent::__construct('script', true);
		$this->_allowedAttributes[] = 'type';
		$this->_allowedAttributes[] = 'src';
		$this->setAttribute("type", $type);
	}

	/**
	 * @brief Maakt een HtmlScript van type text/javascript met de gegeven
	 * code erin.
	 *
	 * @param scriptcode Het script om toe te voegen.
	 * @return een HtmlScript object
	 */
	public static function makeJavascript($scriptcode)
	{
		$script = new HtmlScript();
		$script->addChild($scriptcode);
		return $script;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
