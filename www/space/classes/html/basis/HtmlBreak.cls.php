<?php
// $Id$

/**
	@brief Klasse die een HTML break representeert: 'br'
**/
class HtmlBreak
	extends HtmlElement
{
	function __construct()
	{
		parent::__construct('br');
	}

	/**
	 *	@brief Construeert een aantal br-elementen
	 *	@param count het aantal elementen om te genereren
	 *	@return een array van HtmlBreak objecten
	 */
	public static function multiple($count)
	{
		$res = new HtmlElementCollection();
		for($i = 0; $i < $count; $i++)
			$res->add(new HtmlBreak());
		return $res;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
