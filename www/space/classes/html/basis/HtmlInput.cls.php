<?php
// $Id$

/**
 *	@brief Implementeert een HTML input-element voor in een form.
 *
 *	Deze klasse kan meerdere typen elementen genereren, zie o.a. makeHidden()
 *
 *	@see HtmlForm
 */
class HtmlInput
	extends HtmlElement
{
	/**
	 *	@brief Simpele constructor voor een leeg type
	 *
	 *	@param name de naam van dit element (dus niet het type!)
	 *	@param class de klasse van het object
	 *	@param id het id van het object
	 *	@see Token makeHidden()
	 */
	public function __construct($name, $class = null, $id = null)
	{
		parent::__construct('input');
		$this->_allowedAttributes[] = 'type';
		$this->_allowedAttributes[] = 'name';
		$this->_allowedAttributes[] = 'value';
		$this->_allowedAttributes[] = 'size';
		$this->_allowedAttributes[] = 'checked';
		$this->_allowedAttributes[] = 'disabled';
		$this->_allowedAttributes[] = 'min';
		$this->_allowedAttributes[] = 'max';
		$this->_allowedAttributes[] = 'maxlength';
		$this->_allowedAttributes[] = 'multiple';
		$this->_allowedAttributes[] = 'onkeypress';
		$this->_allowedAttributes[] = 'onkeyup';
		$this->_allowedAttributes[] = 'onclick';
		$this->_allowedAttributes[] = 'onchange';
		$this->_allowedAttributes[] = 'oninput';
		$this->_allowedAttributes[] = 'placeholder';
		$this->_allowedAttributes[] = 'required';
		$this->_allowedAttributes[] = 'src';
		$this->_allowedAttributes[] = 'step';
		$this->_allowedAttributes[] = 'alt';
		$this->_allowedAttributes[] = 'height';
		$this->_allowedAttributes[] = 'width';
		$this->_allowedAttributes[] = 'placeholder';

		if($name) $this->setAttribute("name", $name);

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

	}

	public function setAutoSubmit($autosubmit = true){
		if ($autosubmit) $this->setAttribute("data-autosubmit", "true");
		else $this->unsetAttribute("data-autosubmit");
		return $this;
	}

	/**
	 *	@brief Stelt het type van dit HtmlInput element in.
	 *
	 *	@param inputtype het gewenste type
	 *	@see makeHidden()
	 */
	public function setType($inputtype)
	{
		$this->setAttribute("type", $inputtype);
		return $this;
	}

	/**
	 * Stelt het type 'value' in
	 */
	public function setValue ($value)
	{
		$this->setAttribute('value', (string) $value);
		return $this;
	}

	/**
	 *  Stel het type 'value' in. Probeer eerst of je een geposte value kan vinden
	 *  met trypar en val anders terug op de meegegeven value. Als deze null is dan
	 *  gebeurt er effectief niets.
	 **/
	public function setValueFromForm ($value = null)
	{
		//Is er een trypar value? Let op dat voor b.v. checkboen 0 wel een geldige value is.
		if(trypar($this->getAttribute('name')) || trypar($this->getAttribute('name')) == "0") {
			$value = trypar($this->getAttribute('name'));
		}

		//Checkboxen zijn erg speciaal.
		if($this->getAttribute("type") == 'checkbox') {
			$this->setChecked($value == "on"?"true":null);
		} else {
			$this->setAttribute('value', (string) $value);
		}

		return $this;
	}

	/**
	 * Bepaalt of een checkbox/radio-button checked moet zijn.
	 *
	 * @param bool Een bool of je de box wel of niet checked
	 */
	public function setChecked ($bool = true)
	{
		if ($bool)
			$this->setAttribute('checked', null);
		else
			$this->unsetAttribute('checked');
	}

	/**
	 * @brief Stel de placeholder van de input in.
	 *
	 * @param placeholder de nieuwe placeholder van de input.
	 * @return deze HtmlInput.
	 */
	public function setPlaceholder ($placeholder = null)
	{
		$this->setAttribute('placeholder', $placeholder);
		return $this;
	}

	/**
	 *	@brief Factory function om een checkbox HtmlInput form element te maken
	 *
	 *	@param name de naam van de variabele
	 *	@param value als True dan gechecked
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 *	@param caption een caption die als label geset kan worden
	 *	@param autosubmit een bool of de autosubmit geset moet worden
	 *
	 *	@return Een HtmlInput element van type 'checkbox'
	 */
	public static function makeCheckbox($name, $value = False, $class = null, $id = null, $caption = null, $autosubmit = false)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('checkbox');
		if((boolean)$value) $elem->setChecked();
		if((boolean)$autosubmit) $elem->setAutoSubmit(True);

		//Set caption met label
		if(!empty($caption))
		{
			$elem->ensureId();
			$collectie = new HtmlElementCollection();
			$collectie->add($elem)
				->add(new HtmlLabel($elem->getId(), new HtmlSpan($caption)));
			return $collectie;
		}

		return $elem;
	}

	public static function makeCheckAllCheckbox($name, $value = False, $class = null, $caption = null, $autosubmit = false)
	{
		$elem = self::makeCheckbox($name, $value, $class, $caption, $autosubmit);
		$elem->setId("checkbox-all");
		return $elem;
	}

	/**
	 *	@brief Factory function om een hidden HtmlInput form element te maken
	 *
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 *
	 *	@return Een HtmlInput element van type 'hidden'
	 */
	public static function makeHidden($name, $value, $class = null, $id = null)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('hidden');
		if(isset($value)) $elem->setAttribute("value", $value);
		return $elem;
	}

	/**
	 *	@brief Factory function om een HtmlInput form element van type 'text'
	 *	te maken
	 *
	 *	@param name de naam van de variabele
	 *	@param value de default value (null betekent niet ingesteld)
	 *	@param size de grootte van de box
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 *  @param placeholder de tekst die te zien is in het veld als er geen tekst in staat ter info voor de gebruiker
	 *	@return een HtmlInput element van type 'text'
	 */
	public static function makeText($name, $value = null, $size = null, $class = null, $id = null, $placeholder = null)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('text');
		if(isset($value)) $elem->setAttribute('value', $value);
		if($size)  $elem->setAttribute('size', $size);
		if($placeholder) $elem->setAttribute('placeholder', $placeholder);
		return $elem;
	}

	/**
	 *  Functie om een HtmlInput-element van type 'search' te maken
	 * 
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	public static function makeSearch ($name, $value = null, $class = null, $id = null)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('search')
			 ->setValue($value);
		return $elem;
	}

	/**
	 *  Functie om een HtmlInput-element van type 'url' te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	public static function makeUrl ($name, $value = null, $class = null, $id = null)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('text') //Geen url want die moet van de vorm 'http://' zijn
			 ->setValue($value);
		return $elem;
	}

	/**
	 *  Functie om een HtmlInput-element van type 'number' te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param min de minimale waarde voor de box
	 *	@param max de maximale waarde voor de box
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	static public function makeNumber ($name, $value = null, $min = null, $max = null, $class = null, $id = null)
	{
		$input = new HtmlInput($name, $class, $id);
		$input->setType('number')
			  ->setValue(is_null($value)?NULL:(int)$value);
		if (!is_null($min))
			$input->setAttribute('min', (int) $min);
		if (!is_null($max))
			$input->setAttribute('max', (int) $max);
		return $input;
	}

	/**
	 * Functie om een HtmlInput-element van type 'date' te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	static public function makeDate ($name, $value = null, $class = null, $id = null)
	{
		$input = new HtmlInput($name, 'datepicker-input ' . $class, $id);
		$input->setType('text');
		if($value && (!($value instanceof DateTimeLocale) || $value->hasTime())	)
		{
			  $input->setValue((is_int($value)) ? strftime('%Y-%m-%d', $value) :
			  			 (($value instanceof DateTimeLocale) ? $value->strftime('%Y-%m-%d') :
						 $value));
		}
		return $input;
	}

	/**
	 * Functie om een HtmlInput-element van type 'date' te maken, die uitgeklapt is!
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	static public function makeDateUitgeklapt ($class = null, $id = null)
	{
		$input = new HtmlDiv(null, $class . ' dateUitgeklapt', $id);
		return $input;
	}

	/**
	 * Functie om een HtmlInput-element van type 'datetime' te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 *	@param required een bool om het required-veld te setten
	 */
	static public function makeDatetime ($name, $value = null, $class = null, $id = null, $required = false)
	{
		$input = new HtmlInput($name, 'datetimepicker-input ' . $class, $id);
		$input->setType('text');
		if($value && (!($value instanceof DateTimeLocale) || $value->hasTime())	)
		{
			  $input->setValue((is_int($value)) ? strftime('%F %T', $value) :
			  			 (($value instanceof DateTimeLocale) ? $value->strftime('%F %T') :
						 $value));
		}
		return $input;
	}

	/**
	 * Functie om een HtmlInput-element van type 'datetime-local' te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	static public function makeDatetimeLocal ($name, $value = null, $class = null, $id = null)
	{
		return self::makeDateTime($name, $value, $class, $id);
	}

	/**
	 * Functie om een HtmlInput-element van type 'time' te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	static public function makeTime ($name, $value = null, $class = null, $id = null)
	{
		$input = new HtmlInput($name, 'timepicker-input ' . $class, $id);
		$input->setType('text')
			  ->setValue((is_int($value)) ? strftime('%T', $value) : $value);
		return $input;
	}

	/**
	 * Functie om een radio-button te maken
	 *	@param name de naam van de variabele
	 *	@param value de waarde van de variabele
	 *	@param checked een bool om aan te geven of de radio checked moet zijn
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 */
	static public function makeRadio ($name, $value = null, $checked = false, $class = null, $id = null)
	{
		$input = new HtmlInput($name, $class, $id);
		$input->setType('radio');

		if (is_array($value))
			  $input->setValue($value[0])
			  		->setCaption($value[1]);
		else
			  $input->setValue($value);

		if ($checked)
			$input->setChecked();

		return $input;

	}

	/**
	 * Hiermee maak je een array radio-buttons aan
	 *	@param name de naam van de variabele
	 *	@param values array(value=>caption) van de verschillende opties.
	 *	@param selected een waarde om aan te geven welke radio selected is
	 *	@param auto een bool om aan te geven of autosubmit geset moet worden
	 */
	static public function makeRadioFromArray ($name, $values, $selected = null, $auto = false)
	{
		$ret = array();
		foreach ($values as $value => $caption)
		{
			if ($selected == $value)
				$inp = self::makeRadio($name, array($value, $caption), true);
			else
				$inp = self::makeRadio($name, array($value, $caption));
			if ($auto)
				$inp->setAutoSubmit();
			$ret[] = $inp;
		}
		return new HtmlList(false, $ret);
	}

	/**
	 *	@brief Maakt een submitbutton
	 *
	 *	@param value de value van de button
	 *	@param name de naam van de button
	 *  @param class de class, voor bv images als buttons
	 *	@return een HtmlInput element van type 'submit'
	 *
	 *	\note De volgorde van de parameters is anders dan bij alle andere
	 *	factory functions.
	 */
	public static function makeSubmitButton($value, $name = "submit", $class = 'button')
	{
		$elem = new HtmlInput($name);
		$elem->setType('submit');
		$elem->addClass($class);

		if(isset($value)) $elem->setAttribute('value', $value);
		return $elem;
	}

	public static function makeFormSubmitButton($value, $name = "submit", $class = 'button')
	{
		return new HtmlDiv(new HtmlDiv(
			self::makeSubmitButton($value, $name, $class)
			, 'col-sm-offset-2 col-sm-10'), 'form-group');
	}

	/**
	 *	@brief Maakt een normale button (dat is dus geen submitbutton!)
	 *
	 *	@param name de naam van de button
	 *	@param value de value van de button
	 *	@param onclick een javascript-string die uitgevoerd wordt wanneer
	 *	 er op de button geklikt wordt
	 *	@return een HtmlInput element van type 'button'
	 *	@see makeSubmitButton
	 */
	public static function makeButton($name, $value = null, $onclick = null)
	{
		$elem = new HtmlInput($name);
		$elem->setType('button');
		if (!is_null($value))
			$elem->setAttribute('value', $value);
		if (!is_null($onclick))
			$elem->setAttribute('onclick', $onclick);
		return $elem;
	}

	public static function makeFormButton($name, $value = null, $onclick = null)
	{
		$div = new HtmlDiv(null, 'form-group');

		$div->add($idiv = new HtmlDiv(null, 'col-sm-offset-2 col-sm-10'));

		$idiv->add(self::makeButton($name, $value, $onclick));

		return $div;
	}

	/**
	 *  TODO: verwijderen omdat het niet meer gebruikt wordt.
	 *	@brief Maakt een image waar je mee kan submitten
	 *
	 *	@param value de value van het image
	 *  @param src de src van het image
	 *	@param name de naam van het image
	 *	@param alt de alttext van het image
	 *	@param title de titel van het image
	 *
	 *	@return een HtmlInput element van type 'image'
	 *
	 */
	public static function makeImageSubmit($value, $src, $name = "submit", $alt = null, $title = null)
	{
		$elem = new HtmlInput($name);
		$elem->setType('image');
		$elem->setAttribute('src', $src);
		if(substr($src, 0, 1) == "/")
			$src = FS_ROOT . $src;
		$size = getimagesize($src);
		$elem->setAttribute('width', $size[0]);
		$elem->setAttribute('height', $size[1]);
		if($alt)
			$elem->setAttribute('alt', $alt);

		if(isset($value)) $elem->setAttribute('value', $value);
		return $elem;
	}

	/**
	 *	@brief Maakt een cancel-button die history.back() doet
	 *
	 *	@param value de value van de button
	 *	@return een HtmlInput element van type 'button'
	 *	@see makeSubmitButton
	**/
	public static function makeCancelButton($value)
	{
		$elem = new HtmlInput("cancel");
		$elem->addClass('button');
		$elem->setType('button');
		$elem->setAttribute("onclick", "history.back(); return false;");
		if(isset($value)) $elem->setAttribute('value', $value);
		return $elem;
	}

	/**
	 *	@brief Maak een wachtwoord veld.
	 *
	 *	@param name De naam van het veld
	 *	@param size de grootte van de box
	 *	@param class de klasse van de variabele
	 *	@param id het id van de variabele
	 *  @param placeholder de tekst die te zien is in het veld als er geen tekst in staat ter info voor de gebruiker
	 *	@return een HtmlInput element van type 'passwordtext'
	 */
	public static function makePassword($name, $size = null, $class = null, $id = null, $placeholder = null)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('password');
		if($size)  $elem->setAttribute('size', $size);
		if($placeholder) $elem->setAttribute('placeholder', $placeholder);
		return $elem;
	}

	/**
	 *  makeFile krijgt extra ondersteuning van een HtmlForm; deze zal de juiste encoding
	 *  toevoegen aan het form als er een file input wordt gevonden. Dit hoeft dus niet
	 *  hier te gebeuren.
	 */

	public static function makeFile($name, $size = null, $multiple = false, $class = null, $id = null)
	{
		$elem = new HtmlInput($name, $class, $id);
		$elem->setType('file'); 
		if($size) $elem->setAttribute('size', $size);
		if($multiple) $elem->setAttribute('multiple', null);
		return $elem;
	}

	public static function makeFormFile($name, $size = null, $multiple = false, $class = null, $id = null)
	{
		$div = new HtmlDiv(null, 'form-group');

		$div->add($idiv = new HtmlDiv(null, 'col-sm-offset-2 col-sm-10'));

		$idiv->add(self::makeFile($name, $size, $multiple, $class, $id));

		return $div;
	}

	/**
	 *	@brief In het geval van een checkbox, zet er een hidden input voor met
	 *	hetzelfde name attribuut.  Dit maakt het makkelijker om te checken
	 *	ofdat een checkbox niet aangechecked is.
	 *
	 *	@see HtmlElement::makeHtml
	 */
	public function makeHtml()
	{
		$prepend = '';

		// TODO: Zoek uit op welke plekken, de achterliggende data-verwerking afhangt van een key-value pair in de post/get-data van een form.
		// Wellicht wordt dit opgevangen door tryPar die een default-waarde verwacht, maar misschien ook niet

		if($this->getAttribute("type") == 'checkbox')
			$prepend .= HtmlInput::makeHidden($this->getAttribute("name"), "0");

		$postpend = '';
		$caption = $this->getCaption();
		if ($caption)
		{
			$this->setCaption(null);
			if (!$this->getId())
				$this->setId('form_' . $this->getAttribute("name")
							 . (($val = $this->getAttribute("value")) ? '_' . $val : ''));
			$postpend = new HtmlLabel($this->getId(), '&nbsp;' . $caption);
		}

		return $prepend . parent::makeHtml() . $postpend;
	}

	protected function prepareHtml()
	{
		if($this->noDefaultClasses)
			return $this;

		if($this->getAttribute("type") == 'button'
			|| $this->getAttribute("type") == 'submit')
			$this->addClass('btn btn-primary');
		elseif($this->getAttribute("type") != 'checkbox' && $this->getAttribute("type") != 'radio')
			$this->addClass('form-control');

		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
