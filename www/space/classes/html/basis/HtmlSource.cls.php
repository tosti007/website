<?php
// $Id$

/**
 *	@brief Klasse die een HTML source representeert.
 */
class HtmlSource
	extends HtmlElement
{
	/**
	 *	@brief Construeer een nieuwe anchor tag met opgegeven URL en caption.
	 *
	 *  @param src een string met de srclocatie
	 *  @param type een string om aan te geven welk type source het is
	 */
	function __construct ($src = null, $type = null)
	{
		parent::__construct('source', true);
		$this->_allowedAttributes[] = "media";
		$this->_allowedAttributes[] = "src";
		$this->_allowedAttributes[] = "type";

		if ($src)
			$this->setAttribute('src', $src);
		if ($type)
			$this->setAttribute('type', $type);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
