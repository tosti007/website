<?php
// $Id$

/**
 *	@brief Klasse die een HTML video representeert.
 */
class HtmlVideo
	extends HtmlContainer
{
	/**
	 *	@brief Construeer een nieuwe anchor tag met opgegeven URL en caption.
	 *
	 *  @param children De children van de video
	 *	@param width De width van de video
	 *	@param height De height van de video
	 *	@param controls Of er controls moeten worden laten zien
	 *	@param autoplay Bool of het video-element autoplay geset heeft
	 *	wordt geklikt.
	 */
	function __construct ($children = array(), $width = 320, $height = 240, $controls = true, $autoplay = true)
	{
		parent::__construct('video', true);
		$this->_allowedAttributes[] = "height";
		$this->_allowedAttributes[] = "width";
		$this->_allowedAttributes[] = "controls";
		$this->_allowedAttributes[] = "autoplay";

		$this->addChildren($children);
		if ($width)
			$this->setAttribute('width', $width);
		if ($height)
			$this->setAttribute('height', $height);
		if($controls)
			$this->setAttribute('controls', null);
		if($autoplay)
			$this->setAttribute('autoplay', null);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
