<?php
// $Id$

/**
 *  @brief Implementeert een HTML table
 *
 *	@todo Er moet (meer) nagedacht worden over helper functies/classen voor het
 *	aanmaken & gebruiken van HtmlTable.
 */
class HtmlTable
	extends HtmlContainer
{
	/**
	 * @var bool Of er bij het genereren van de HTML string een extra div
	 *    omheen moet (want dat wil Bootstrap).
	 */
	private $wrappingDiv = true;

	/**
	 * @brief Construeer een nieuw table-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlTable
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('table', false);
		$this->_allowedAttributes[] = 'align';
		$this->_allowedAttributes[] = 'border';
		$this->_allowedAttributes[] = 'cellpadding';

		$this->_allowedChildren[] = 'HtmlTableCol';
		$this->_allowedChildren[] = 'HtmlTableHead';
		$this->_allowedChildren[] = 'HtmlTableFoot';
		$this->_allowedChildren[] = 'HtmlTableBody';
		$this->_allowedChildren[] = 'HtmlTableRow';

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setAttribute('id', $id);

		$this->addChildren($children);
	}

	/**
	 * @brief Stel de waarde van `wrappingDiv` in.
	 *
	 * @param bool waarde
	 */
	public function setWrappingDiv($waarde) {
		$this->wrappingDiv = $waarde;
	}

	/**
	 * @brief Voeg een HtmlTableRow toe.
	 *
	 * @return de toegevoegde HtmlTableRow.
	 */
	public function addRow()
	{
		$row = new HtmlTableRow();
		$this->addChild($row);
		return $row;
	}

	/**
	 * @brief Voeg een HtmlTableHead toe.
	 *
	 * @return de toegevoegde HtmlTableHead.
	 */
	public function addHead()
	{
		$head = new HtmlTableHead();
		$this->addChild($head);
		return $head;
	}

	/**
	 * @brief Voeg een HtmlTableBody toe.
	 *
	 * @return de toegevoegde HtmlTableBody.
	 */
	public function addBody()
	{
		$body = new HtmlTableBody();
		$this->addChild($body);
		return $body;
	}

	/**
	 * @brief Voeg een HtmlTableFoot toe.
	 *
	 * @return de toegevoegde HtmlTableFoot.
	 */
	public function addFoot()
	{
		$body = new HtmlTableFoot();
		$this->addChild($body);
		return $body;
	}

	/**
	 *	@brief Zet een 2D array om in HtmlTable.
	 *
	 *	@param mixed[][]|HtmlTableRow[] arr De 2D array met wat er in de cellen moet komen.
	 *	@param bool kaal Of de tabel 'kaal' moet zijn, i.e. zonder extra divs en classes.
	 *
	 *	@return HtmlTable De gevulde HtmlTable.
	 */
	static function fromArray($arr, $kaal = false)
	{
		$table = new HtmlTable();
		if ($kaal) {
			// We willen in dit geval niet al die bootstrap-tsjak.
			$table->setNoDefaultClasses();
			$table->setWrappingDiv(false);
		}

		if(!is_array($arr))
			// FIXME: Ja hallo, hier willen we gewoon een error geven.
			return $table;

		foreach ($arr as $r)
		{
			if ($r instanceof HtmlTableRow) {
				$table->addChild($r);
				continue;
			}
			if (!is_array($r))
				// FIXME: Hier ook...
				continue;

			$row = $table->addRow();
			foreach ($r as $c)
			{
				if ($c instanceof HtmlTableDataCell
					|| $c instanceof HtmlTableHeaderCell) {
					$row->addChild($c);
					continue;
				}
				$row->addData($c);
			}
		}
		return $table;
	}

	/**
	 * @brief Zet een associatieve array om in een HtmlTable.
	 *
	 * De table heeft een kolom voor keys en een voor values.
	 * Als een value een Cell is, dan wordt die rechtstreeks toegevoegd,
	 * anders met behulp van addData.
	 *
	 * @param arr Een key => value array.
	 * @return De gevulde HtmlTable.
	 */
	static function fromAssociativeArray($arr)
	{
		$table = new HtmlTable();

		foreach($arr as $k => $v)
		{
			$row = $table->addRow();
			$row->addData($k);
			if ($v instanceof HtmlTableDataCell || $v instanceof HtmlTableHeaderCell)
			{
				$row->addChild($v);
			}
			else
			{
				$row->addData($v);
			}
		}
		return $table;
	}

	protected function prepareHtml()
	{
		if(!$this->noDefaultClasses)
			$this->addClass('table table-white table-hover');

		return $this;
	}

	public function makeHtml($indent = 0)
	{
		$out = parent::makeHtml($indent);

		if ($this->wrappingDiv) {
			return '<div class="table-responsive">'.$out.'</div>';
		} else {
			return $out;
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
