<?php
// $Id$

/**
 *	@brief Deze class implementeert een HTML Header container: bijvoorbeeld
 *	\<h3\>.
 */
class HtmlHeader
	extends HtmlContainer
{
	/**
	 *	@brief maak eem HTML Header element aan.
	 *	@param level level van de header, default 3
	 *	@param children de children van de header
	 */
	function __construct($level = 3, $children = null)
	{
		if(!is_int($level) || $level < 1 || $level > 6)
			throw new BadMethodCallException("Je moet een level tussen 1 en 6 opgeven voor je header");
		parent::__construct('h'.$level, true);
		if($children) $this->addChildren($children);
	}

	/*protected function prepareHtml()
	{
		$div = $this;

		if((int)substr($this->_elementName, 1, 1) < 4 && !$this->noDefaultClasses)
			$div = new HtmlDiv($this, 'page-header');
			//$this->addClass('page-header');

		return $div;
	}*/

	public function makeHtml($indent = 0)
	{
		$out = parent::makeHtml($indent);
		if((int)substr($this->_elementName, 1, 1) < 4 && !$this->noDefaultClasses)
			return '<div class="page-header">'.$out.'</div>';
		return $out;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
