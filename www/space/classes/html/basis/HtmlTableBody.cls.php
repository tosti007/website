<?php
/**
 * Implementeert een html-table-body
 */
class HtmlTableBody extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw table body-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlTableBody 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('tbody', false);

		$this->_allowedChildren[] = 'HtmlTableRow';

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		$this->addChildren($children);
	}

	/**
	 * @brief Voeg een HtmlTableRow toe.
	 *
	 * @return de toegevoegde HtmlTableRow.
	 */
	public function addRow()
	{
		$row = new HtmlTableRow();
		$this->addChild($row);
		return $row;
	}
}
