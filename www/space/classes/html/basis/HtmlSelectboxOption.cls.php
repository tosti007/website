<?php
// $Id$

/**
 *	@brief Representeert een &lt;option&gt; binnen een &lt;selectbox&gt;
 */
class HtmlSelectboxOption
	extends HtmlContainer
{
	/**
	 *	@brief Constructor voor een HtmlSelectboxOption waarbij de GET/POST
	 *	value kan worden meegegeven en het label van de option.
	 */
	public function __construct($formvalue = NULL, $label = NULL)
	{
		parent::__construct('option', true);
		$this->_allowedAttributes[] = "class";
		$this->_allowedAttributes[] = "disabled";
		$this->_allowedAttributes[] = "id";
		$this->_allowedAttributes[] = "name";
		$this->_allowedAttributes[] = "onclick";
		$this->_allowedAttributes[] = "selected";
		$this->_allowedAttributes[] = "style";
		$this->_allowedAttributes[] = "value";
		$this->_issueNewlines = false;

		if($formvalue !== null && $formvalue !== false)
			$this->setAttribute("value", $formvalue);
		if($label !== null && $label !== false)
			$this->setChildren($label);
	}

	public function setSelected($selected = true)
	{
		if($selected)
			$this->setAttribute("selected", null);
		else
			$this->unsetAttribute("selected");
	}

	public function setDisabled($disabled = true)
	{
		if($disabled)
			$this->setAttribute("disabled", null);
		else
			$this->unsetAttribute("disabled");
	}

	public function getValue()
	{
		return $this->getAttribute("value");
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
