<?php
// $Id$

/**
 *	@brief Klasse die een HTML horizontal line representeert: 'hr'
 */
class HtmlHR
	extends HtmlElement
{
	function __construct()
	{
		parent::__construct('hr');
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
