<?php
// $Id$

/**
 *	@brief Implementeert een HTML image-element;
 */
class HtmlImage
	extends HtmlElement
{
	protected $lazyLoad;
	protected $lowres;
	/**
	 *	@brief Simpele constructor voor HtmlImage
	 *
	 *	@param src de lokatie van het image
	 *	@param alt de alternate text voor het image
	 *	@param class de class text van het image
	 *	@param title de title van het image (meeste browsers laten dit zien als
	 *   over het image hovert)
	 */
	public function __construct($src, $alt='', $title='', $class='')
	{
		if(empty($alt))
			user_error("Hola, bij een image hoort een alt text!");

		parent::__construct('img');
		$this->_allowedAttributes[] = 'src';
		$this->_allowedAttributes[] = 'data-src';
		$this->_allowedAttributes[] = 'alt';
		$this->_allowedAttributes[] = 'height';
		$this->_allowedAttributes[] = 'width';
		$this->_allowedAttributes[] = 'title';
		$this->_allowedAttributes[] = 'onclick';
		$this->_allowedAttributes[] = 'onload';

		if($src) $this->setAttribute('src', $src);
		$this->setAttribute('alt', $alt ?: '');
		$this->addClass($class ?: '');
		$this->setAttribute('title', $title ?: '');

		$this->lazyLoad = false;
		$this->lowres = true;
	}

	/**
	 * @brief Stel de hoogte van deze afbeelding in.
	 * 
	 * @param height De nieuwe hoogte van deze HtmlImage.
	 */
	public function setHeight($height)
	{
		$this->setAttribute('height', $height);
	}

	/**
	 * @brief Stel de breedte van deze afbeelding in.
	 * 
	 * @param width De nieuwe breedte van deze HtmlImage.
	 */
	public function setWidth($width)
	{
		$this->setAttribute('width', $width);
	}

	/**
	 * @brief Stel de hoogte en breedte van deze afbeelding in.
	 * 
	 * @param width  De nieuwe breedte van deze HtmlImage.
	 * @param height De nieuwe hoogte van deze HtmlImage.
	 * @see ::setHeight
	 * @see ::setWidth
	 */
	public function setSize($width, $height)
	{
		$this->setWidth($width);
		$this->setHeight($height);
	}

	public function useDataSrc()
	{
		$this->setAttribute('data-src', $this->getAttribute('src'));
		$this->setAttribute('src', "");
	}

	public function setLazyLoad()
	{
		$this->lazyLoad = true;
	}

	public function unsetLazyLoad()
	{
		$this->lazyload = false;
	}

	public function setLowres()
	{
		$this->lowres = true;
	}

	public function unsetLowres()
	{
		$this->lowres = false;
	}

	protected function prepareHtml()
	{
		if($this->lazyLoad)
		{
			$this->setAttribute('data-original', $this->getAttribute('src'));
			if($this->lowres
				&& strpos($this->getAttribute('src'), '://') === false
				&& substr($this->getAttribute('src'), 0, 2) !== '//'
			)
				$this->setAttribute('src', $this->getAttribute('src') . '?lowres=1');
			else
				$this->unsetAttribute('src');

			$this->addClass('lazy');
		}

		return $this;
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
