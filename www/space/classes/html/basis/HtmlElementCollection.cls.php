<?
// $Id$

/**
 *	@brief Deze klasse implementeert een verzameling van Html Elementen.
 *	Eigenlijk is het dus een HtmlContainer zonder eigen tags.
 */
class HtmlElementCollection
	extends HtmlContainer
{
	/**
	 *	@brief Construeert een tagless html container
	 */
	public function __construct()
	{
		parent::__construct('', true);
		$this->_allowedAttributes = array();
	}

	/**
	 * @brief Zet de elementen in deze verzameling om in tekst.
	 *
	 * @param indent Het aantal spaties dat voor elke regel wordt geplaatst.
	 * @return Een tekstrepresentatie van deze HtmlElementCollection.
	 * @see HtmlElement::makeInnerHtml
	 */
	public function makeHtml($indent = 0)
	{
		return $this->makeInnerHtml($indent) . "\n";
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
