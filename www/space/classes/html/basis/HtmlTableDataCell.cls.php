<?php
// $Id$

/**
 *  @brief Implementeert een HTML table data cell
 */
class HtmlTableDataCell
	extends HtmlContainer
{
	/**
	 * @brief Construeer een nieuw table data cell-element.
	 *
	 * @param children Een HtmlElement of array daarvan, die deze HtmlTableDataCell 
	 * automatisch als kind(eren) krijgt.
	 * @param class De html-class die dit element krijgt.
	 * @param id De html-id die dit element krijgt.
	 */
	public function __construct ($children = array(), $class = null, $id = null)
	{
		parent::__construct('td', true);
		$this->_allowedAttributes[] = 'colspan';
		$this->_allowedAttributes[] = 'rowspan';
		$this->_allowedAttributes[] = 'onclick';
		$this->_allowedAttributes[] = 'onmouseout';
		$this->_allowedAttributes[] = 'onmouseover';
		$this->_allowedAttributes[] = 'align';
		$this->_allowedAttributes[] = 'valign';
		$this->_allowedAttributes[] = 'sorttable_customkey'; //Custom sorteerkey voor JS

		if ($class != null)
			$this->addClass($class);
		if ($id != null)
			$this->setId($id);

		$this->addChildren($children);
	}

	/**
	 * @brief Stel de breedte van deze cel in.
	 *
	 * @param cols De nieuwe breedte van deze HtmlTableDataCell.
	 */
	public function colspan($cols)
	{
		$this->setAttribute('colspan', $cols);
	}

	/**
	 * @brief Maak een HtmlTableDataCell met een handmatige sorterting.
	 *
	 * @param content De inhoud van de nieuwe cel.
	 * @param sortkey De waarde waarop deze cel gesorteerd wordt.
	 * @return De nieuwe HtmlTableDataCell.
	 */
	public static function maakGesorteerde($content, $sortkey)
	{
		$cell = new HtmlTableDataCell($content);
		$cell->setAttribute('sorttable_customkey', $sortkey);
		return $cell;
	}

	//Maakt een sorteerbare cel met een vinkje of een kruisje
	/**
	 * @brief Maak een HtmlTableDataCell met een vinkje of een kruisje.
	 *
	 * @param bool True om een vinkje te maken, False om een kruisje te maken.
	 * @return De nieuwe HtmlTableDataCell.
	 */
	public static function maakBoolPicto ($bool)
	{
		return self::maakGesorteerde(
			HtmlSpan::maakBoolPicto($bool),
			$bool ? 1 : 0);
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
