<?
// $Id$

/**
	@brief Deze klasse implementeert de basis van alle HTML container elementen (elementen met elementen daarbinnen)
**/
abstract class HtmlContainer
	extends HtmlElement
{
	private $_allowText = false;
	protected $_children = array();
	protected $_allowedChildren = null;
	protected $_issueNewlines = true;

	/**
		@brief Construeert een container HTML-element

		@param elemName De naam van dit element, bijvoorbeeld 'div'
		@param allowText Bepaalt of dit element reguliere tekst als child kan hebben
	**/
	protected function __construct($elemName, $allowText)
	{
		parent::__construct($elemName);
		$this->_allowText = $allowText;
	}

	/**
		@brief Maakt een HTML-representatie van alles binnen deze container.

		Deze functie retourneert dus niet de container-tags zelf, dus ook geen
		attributes voor de container. Wel genereert het dus de volledige
		HTML-representatie van alle children.

		@param indent Het aantal spaties dat voor elke regel komt te staan.
		@return Een string met daarin de HTML-representatie van alles binnen deze container
	**/
	public function makeInnerHtml($indent = 0)
	{
		$childrenHtml = array();
		foreach($this->_children as $child){
			if ($child instanceof HtmlElement){
				$childrenHtml[] = str_repeat(" ", $indent) . rtrim($child->makeHtml($indent));
			} else {
				$childrenHtml[] = str_repeat(" ", $indent) . trim($child);
			}
		}
		return implode("\n", $childrenHtml);
	}

	/**
		@brief Alias voor addChildren() (maar accepteert ook enkele parameters)
		@see addChild
		@see addChildren
		@see addImmutable
	**/
	public function add(){
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->addChildren(func_get_args());
		return $this;
	}

	public function addComment($comment)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->addChildText('<!-- ' . $comment . ' -->');
		return $this;
	}

	/**
		@brief Voegt een child toe voor aan de lijst.
	**/
	public function prependChild($child){
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		array_unshift($this->_children, $child);
	}

	/**
		@brief Voegt een child toe aan deze container

		Een child kan simpele tekst zijn (een string) of een HtmlElement

		@see addChildren
		@see addImmutable
		@param child De child om toe te voegen
		@exception HtmlException als het toe te voegen child volgens de
		HTML-specificatie niet kan bestaan binnen deze container
	**/
	public function addChild($child)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		if(is_null($child))
			return;

		if($child instanceof HtmlElement)
		{
			$this->checkAddChild($child);
			$this->_children[]=$child;
			return;
		}

		// Beschouw de child als text en voeg het toe.
		if(!$this->_allowText)
			throw new HtmlException("Het toevoegen van text ('".$child."') "
					. "aan een HTML container van type "
					. "'".get_class($this)."'is niet toegestaan");

		$this->addChildText($child);
	}

	/**
	 * @brief Controleer of dit soort child wel toegevoegd mag worden
	 *
	 * Als het element van de HTML spec niet een kind mag zijn van dit
	 * element, dan gooit het een error. Als het mag, doet de functie niets.
	 * @param child De child om toe te voegen
	 * @exception HtmlException als het toe te voegen child volgens de
	 * HTML-specificatie niet kan bestaan binnen deze container
	 */
	private function checkAddChild(HtmlElement $child) {
		if(is_array($this->_allowedChildren)
		&& !in_array(get_class($child), $this->_allowedChildren))
				throw new HtmlException("Je kunt geen child van type "
						. "'".get_class($child)."' toevoegen aan een "
						. "'".get_class($this)."'");
	}

	/**
	 * @brief Voegt een string als child toe aan deze container.
	 *
	 * Een child kan alleen simpele tekst zijn (een string), maar mag
	 * ook html-elementen bevatten in stringvorm, voor de efficiëntie.
	 *
	 * We doen geen checks omdat we aannemen dat die al gedaan zijn
	 * in de publieke methoden.
	 *
	 * @see addChild
	 * @param child De child om toe te voegen
	 */
	private function addChildText($child) {
		// Controleren of het laatste child van deze container ook een
		// regulier textding is. In dat geval: beide stukken text
		// samenvoegen, dat levert nettere HTML op
		$numChildren = sizeof($this->_children);
		//if($numChildren == 0
		//|| $this->_children[$numChildren - 1] instanceof HtmlElement)
		{
			// Laatste child was geen string
			$this->_children[] = $child;
			return;
		}
		// Laatste child is string
		$this->_children[$numChildren - 1] .= $child;
	}

	/**
		@brief Voegt in 1 operatie meerdere children toe aan deze container

		Deze functie accepteert een variabel aantal parameters, die ieder een
		child of een array van children kunnen zijn.

		@see addChild
		@exception HtmlException als een bepaald child niet kan bestaan binnen
		deze container
	**/
	public function addChildren()
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		foreach(func_get_args() as $arg)
		{
			if(!is_array($arg)) {
				$this->addChild($arg);
				continue;
			}

			foreach($arg as $child)
				$this->addChildren($child);
		}
	}

	/**
	 * @brief Voeg een child toe aan deze container dat daarna nooit verandert.
	 *
	 * Zou geheugenefficiënter moeten zijn dan ->add en kompanen.
	 *
	 * We hebben redelijk vaak een memory overflow omdat er heel veel html-objecten
	 * gemaakt worden. Als we weten dat er niets meer verandert, kunnen we de
	 * afmetingen van de objecten aanzienlijk beperken door het alvast in een
	 * stringvorm te gieten (en die zelfs alvast op te sturen).
	 *
	 * @see addChild
	 * @see add
	 * @param child Het html-element dat kind moet worden.
	 * @exception HtmlException als het toe te voegen child volgens de
	 * HTML-specificatie niet kan bestaan binnen deze container
	 */
	public function addImmutable($child)
	{
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		// Lege waarden toevoegen schiet snel op
		if (is_null($child)) return;

		if ($child instanceof HtmlElement) {
			// Voeg het direct in stringvorm toe
			$this->checkAddChild($child);
			$this->addChildText($child->makeHtml());
			// Maak het na afloop pas immutable, want sommige klassen willen
			// eerst nog wat kinderen aan het einde neerzetten
			$child->makeImmutable();
			return;
		}

		// Geen html-element dus we kunnen er geen fancy trucjes mee doen.
		$this->addChild($child);
	}

	/**
		@brief Stelt in 1x alle children van deze HtmlContainer in. 
		
		Eventueel eerder ingestelde children worden gewist.

		@param children een array van elementen zijn, of een enkel element.
		@exception HtmlException als een bepaald child niet kan bestaan binnen
		deze container
	**/
	public function setChildren($children){
		if ($this->immutable) throw new HtmlElementImmutableException($this);
		$this->_children = array();

		if (is_array($children)) foreach($children as $child) $this->addChild($child);
		else $this->addchild($children);
	}

	/**
		@brief Retourneert een array van de children van deze container.

		@return Een array met children (HtmlElement objecten en/of strings) van
		deze container
	**/
	public function getChildren(){
		return $this->_children;
	}

	public function getChild($index)
	{
		if($index >= count($this->_children))
			throw new HtmlException('Index groter dan het laatste element');

		return $this->_children[$index];
	}

	/**
		@brief Maakt een HTML-representatie van de opening tag van dit element.

		In uitzonderlijke gevallen kan een element ook een uitgebreidere
		opening hebben, bijvoorbeeld met voorbereidend JavaScript.

		@return Een string met een HTML-representatie van de opening van dit
		element, inclusief eventueel ingestelde attributes.
		@see makeElemEnd
	**/
	protected function makeElemBegin()
	{
		if(!isset($this->_elementName))
			return '';

		return '<'.$this->_elementName.$this->makeAttributesString().'>';
	}

	/**
		@brief Maakt een HTML-representatie van de closing tag van dit element.

		In uitzonderlijke gevallen kan een element ook een uitgebreidere
		closing hebben, bijvoorbeeld met afsluitend stuk JavaScript.

		@return Een string met een HTML-representatie van de sluiting van dit
		element.
		@see makeElemBegin
	**/
	protected function makeElemEnd()
	{
		return (isset($this->_elementName) ? '</' . $this->_elementName . '>' : '');
	}

	/**
		@brief Retourneert het aantal children dat een instantie is van een
		subclass van HtmlElement
	**/
	public function numHtmlChildren(){
		$count = 0;
		foreach($this->_children as $child){
			if ($child instanceof HtmlElement) $count++;
		}
		return $count;
	}

	public function hasHtmlChildren() {
		foreach($this->_children as $child) {
			if ($child instanceof HtmlElement) return true;
		}
		return false;
	}

	/**
		@brief Maakt een volledige HTML-representatie van dit element.

		De volledige HTML-representatie bevat openings- en sluitingstags en de
		HTML-representatie van alle children.

		@param indent Het aantal spaties dat voor elke regel komt te staan.
		@return Een string met de volledige HTML-representatie van deze HTML
		container
	**/
	public function makeHtml ($indent = 0)
	{
		if (!$this->immutable)
			$this->prepareHtml();

		$res = array();
		$inner = $this->makeInnerHtml($indent + 1);
		if ($this->numHtmlChildren() == 0 || !$this->_issueNewlines || (strpos($inner, "\n") === false && strlen($inner) < 50)) {
			// Make inline HTML without any newlines:
			// <elem>sometext</elem>
			$res[] = $this->makeElemBegin();
			$res[] = trim($inner);
			$res[] = $this->makeElemEnd();
		} else {
			/* Make HTML with newlines and indentation:
				<thisElem>
			     <childElem>sometext</childElem>
				 (...)
				</elem>
			*/
			$res[] = $this->makeElemBegin();
			$res[] = "\n";
			$res[] = rtrim($inner);
			$res[] = "\n";
			$res[] = str_repeat(" ", $indent);
			$res[] = $this->makeElemEnd();
			$res[] = "\n";
		}

		if(!is_null($this->caption))
			$res[] = new HtmlParagraph($this->caption,
				(isset($this->_attributes['class'])) ? $this->_attributes['class'] : null,
				(isset($this->_attributes['id'])) ? $this->_attributes['id'] . '_caption' : null);

		return implode("", $res);
	}

	/**
	 * @brief Override voor makeImmutable die ook children immutable maakt.
	 */
	public function makeImmutable() {
		parent::makeImmutable();
		foreach ($this->_children as $child) {
			if ($child instanceof HtmlElement) {
				$child->makeImmutable();
			}
		}
	}
}
// vim:sw=4:ts=4:tw=0:foldlevel=1
