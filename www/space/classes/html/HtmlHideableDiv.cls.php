<?php
/**
	Klasse die een DIV-element kan maken dat verborgen en getoond kan worden d.m.v.
	javascript
*/
class HtmlHideableDiv extends HtmlDiv {
	private $_initiallyVisible = null;
	private $_autoHide = false;

	public function __construct($initiallyVisible = false, $autohide = false){
		parent::__construct();
		$this->ensureId();
		$this->_initiallyVisible = $initiallyVisible;
		$this->_autoHide = $autohide;
		//Forceer het verborgen zijn als we willen autohiden
		if($autohide)
			$this->setAttribute('style', 'display: none');
		
		$this->setAttribute('data-hideable', $initiallyVisible ? 'visible' : 'hidden');
	}

	/**
		Retourneert een stukje javascript wat de div laat zien
	*/
	function getJscriptShow(){
		$id = $this->getId();
		return "document.getElementById('$id').style.display = 'block';";
	}

	/**
		Retourneert een stukje javascript wat de div verbergt
	*/
	function getJscriptHide(){
		$id = $this->getid();
		return "document.getElementById('$id').style.display = 'none';";
	}
	
	/**
		Maakt een "button" (icon) dat de state van de div representeert.
		Het icon is een link naar de tegenovergestelde state.
	*/
	function makeButton()
	{
		$id = $this->getid();
		$iconid = $id . "_icon";
		
		$icon = '/Layout/Images/Icons/collapse.png';
		$alt = "Minder informatie tonen";

		$icon = new HtmlImage($icon, $alt);
		$icon->setId($iconid);

		$anchor = new HtmlAnchor('#', $icon);
		return $anchor;
	}


	protected function makeElemBegin(){
		if ($this->_autoHide) $this->setAttribute("onmouseout", $this->getJScriptHide()); 
		return parent::makeElemBegin();
	}
	
}

