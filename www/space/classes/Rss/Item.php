<?php
/**
 * Stelt een RSS-feed-item voor
 */
class Rss_Item
{
	private $title;
	private $link;
	private $description;
	private $pubdate;

	public function __construct ($title = '', $link = '', $description = '', $pubdate = '')
	{
		$this->title = (string) $title;
		$this->link = (string) $link;
		$this->description = (string) $description;
		$this->pubdate = (string) $pubdate;
	}

	public function __toString ()
	{
		$output = '<item>' . "\n";
		if (strlen($this->title) > 0)
			$output .= "\t" . '<title>' . htmlspecialchars($this->title) . '</title>' . "\n";
		if (strlen($this->description) > 0)
			$output .= "\t" . '<description>' . htmlspecialchars($this->description) . '</description>' . "\n";
		if (strlen($this->link) > 0)
			$output .= "\t" . '<link>' . htmlspecialchars($this->link) . '</link>' . "\n";
		if (strlen($this->pubdate) > 0)
			$output .= "\t" . '<pubDate>' . htmlspecialchars($this->pubdate) . '</pubDate>' . "\n";
		$output .= '</item>';

		return $output;
	}

	public function getTitle ()
	{
		return $this->title;
	}

	public function getDescription ()
	{
		return $this->description;
	}

	public function getLink ()
	{
		return $this->link;
	}

	public function getPubdate ()
	{
		return $this->pubdate;
	}

	public function setTitle ($title)
	{
		$this->title = (string) $title;
		return $this;
	}

	public function setDescription ($description)
	{
		$this->description = (string) $description;
		return $this;
	}

	public function setLink ($link)
	{
		$this->link = (string) $link;
		return $this;
	}

	public function setPubdate ($pubdate)
	{
		$this->pubdate = (string) $pubdate;
		return $this;
	}
}
