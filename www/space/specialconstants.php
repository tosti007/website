<?php
// $Id$

/*+ Alle elk jaar te wijzigen of controleren constanten
*/

//Maand dat het nieuwe boekjaar en collegejaar begint in een jaar
define('COLJAARSWITCHMAAND', 8);

define('BOEKLEVERANCIER', 'NewBricks');
define('BOEKLEVNR', 3);

// BEGIN BESTUUR DATA ----------------------------------------------------------
define('BESTUURLOGINNAAM', 'bestuur1819');
define('BOEKCOMNAME', 'Mattijs Léon');
define('SECRNAME', 'Iris Tuhumena');

// Een kandidaatbestuur van jaar (X, X + 1) is actief in het collegejaar (X - 1, X)
// en wordt bestuur in jaar (X, X + 1)
define('KANDIDAATBESTUURLOGINNAAM', 'kb1819');

/** @var BestuursLid[string] $BESTUUR
 * Wordt geinitialiseerd uit configuratiedata door de functie _laad_bestuurdata.
 */
global $BESTUUR;

// EINDE BESTUUR DATA ----------------------------------------------------------

/**
 * Stel bestuursfuncties in op basis van de keys in de array.
 * Moet in een functie om te voorkomen dat deze variabelen in de globals terechtkomen.
 * (FOEI, PHP!)
 */
function _laad_bestuurdata()
{
	global $BESTUUR;
	$BESTUUR = [];

	$configdata = yaml_parse_file(FS_ROOT . '/../config.yaml');

	foreach ($configdata['bestuur'] as $bestuurdata)
	{
		$contactid = $bestuurdata['contactid'];
		$email = $bestuurdata['email'];
		$boekcom = false;
		if (array_key_exists('boeken', $bestuurdata))
		{
			$boekcom = $bestuurdata['boeken'];
		}
		$functie = $bestuurdata['functie'];
		$bestuurslid = new BestuursLid($contactid, $email, $boekcom);
		$bestuurslid->setFunctie($functie);
		$BESTUUR[$functie] = $bestuurslid;
		$BESTUUR[strtolower($functie)] = $bestuurslid;
	}
}
_laad_bestuurdata();

// voor spookweb (en bookweb?)
define('BTWPERCENTAGE', '21');

// evenementenrekening
define('EVENEMENTENREKENING', 'NL67 INGB 0003 7433 05');

// Geeft aan wanneer een contract "late" is.
$CONTRACTLATE = array (

	"OPGESTUURD" 	=> new DateInterval("P6M")
	,"RETOUR"	=> new DateInterval("P1Y")

	);

// Het aantal kindjes dat mee kunnen op kamp voordat ze op de reservelijst komen.
define('MAXAANTALKAMP', 308);

/* 	Alle periodes per jaar met hun bijbehorende begindatum.

	Gelieve oude jaren te laten staan, dit i.v.m. statistische
	informatie voor de boekcom en spookweb
*/
$PERIODES = array (

			 2003 => array
				(1 => '2003-09-01'
				,2 => '2003-11-17'
				,3 => '2004-02-02'
				,4 => '2004-04-19'
				)
			,2004 => array
				(1 => '2004-09-02'
				,2 => '2004-11-15'
				,3 => '2005-02-07'
				,4 => '2005-04-25'
				)
			,2005 => array
				(1 => '2005-09-08'
				,2 => '2005-11-14'
				,3 => '2006-02-06'
				,4 => '2006-04-24'
				)
			,2006 => array
				(1 => '2006-09-04'
				,2 => '2006-11-13'
				,3 => '2007-02-05'
				,4 => '2007-04-23'
				)
			, 2007 => array
				(1 => '2007-09-03'
				,2 => '2007-11-12'
				,3 => '2008-02-04'
				,4 => '2008-04-21'
				)
			, 2008 => array
				(1 => '2008-09-01'
				,2 => '2008-11-10'
				,3 => '2009-02-02'
				,4 => '2009-04-20'
				)
			, 2009 => array
				(1 => '2009-08-31'
				,2 => '2009-11-08'
				,3 => '2010-02-08'
				,4 => '2010-04-26'
				)
			, 2010 => array
				(1 => '2010-09-06'
				,2 => '2010-11-14'
				,3 => '2011-02-07'
				,4 => '2011-04-25'
				)
			, 2011 => array
				(1 => '2011-09-08'
				,2 => '2011-11-14'
				,3 => '2012-02-06'
				,4 => '2012-04-23'
				)
			, 2012 => array
				(1 => '2012-09-03'
				,2 => '2012-11-12'
				,3 => '2013-02-04'
				,4 => '2013-04-22'
				)
			, 2013 => array
				(1 => '2013-09-02'
				,2 => '2013-11-11'
				,3 => '2014-02-03'
				,4 => '2014-04-21'
				)
			, 2014 => array
				(1 => '2014-09-01'
				,2 => '2014-11-10'
				,3 => '2015-02-02'
				,4 => '2015-04-20'
				)
			, 2015 => array
				(1 => '2015-08-31'
				,2 => '2015-11-09'
				,3 => '2016-02-08'
				,4 => '2016-04-25'
				)
			, 2016 => array
				(1 => '2016-09-05'
				,2 => '2016-11-14'
				,3 => '2017-02-06'
				,4 => '2017-04-24'
				)
			, 2017 => array
				(1 => '2017-09-04'
				,2 => '2017-11-13'
				,3 => '2018-02-05'
				,4 => '2018-04-23'
				)
			, 2018 => array
				(1 => '2018-09-03'
				,2 => '2018-11-12'
				,3 => '2019-02-04'
				,4 => '2019-04-22'
				)
			);

/* De jaren waarvoor studenten boeken kunnen bestellen hangt af voor welke
 * jaren er periodes bekend zijn */
$COLJAREN = array();
foreach ($PERIODES as $jaar => $dummy) {
	$COLJAREN[$jaar] = $jaar.'-'.($jaar+1);
}

/* De datum waarop de eerstejaars boekverkoop is (JJJJ-MM-DD).
 * Wordt gebruikt om de 'pinII'-knop al dan niet weer te geven
 * en voor de nieuwe leden die zich aanmelden voor de intro */
define('EJBV', '2018-09-03');

/* Vakanties en vrije dagen: array(eerste_vakantiedag => laatste_vakantiedag).
 * - Alles in YYYY-MM-DD.
 * - Volgorde van vrije dagen moet chronologisch.
 */
$VAKANTIES = array (
// '06/'07
	 '2006-11-10' => '2006-11-10' // Voorlichtingsdag
	,'2006-12-25' => '2007-01-05' // Hertentamens blok 1 en Kerstvakantie
	,'2007-03-12' => '2007-03-16' // Hertentamens blok 2
	,'2007-04-06' => '2007-04-09' // Goede Vrijdag / Pasen
	,'2007-04-30' => '2007-04-30' // Koninginnedag
	,'2007-05-17' => '2007-05-18' // Hemelvaart
	,'2007-05-28' => '2007-06-01' // Hertentamens blok 3 en Pinksteren
	,'2007-07-09' => '2007-08-31' // Zomervakantie en hertentamens blok 4
// '07/'08
	,'2007-12-24' => '2008-01-04' //Kerst en nieuwjaar
	,'2008-03-17' => '2008-03-24' //Herkansingen en Goede vrijdag en Pasen
	,'2008-04-30' => '2008-05-05' //Koninginnedag en hemelvaart
	,'2008-05-12' => '2008-05-12' //Pinksteren
	,'2008-05-26' => '2008-05-30' //Herkjansingen 3
	,'2008-07-07' => '2008-08-29' //Zomervkantie + Herkansingen 4
// '08/'09
	,'2009-07-01' => '2009-08-30' // Zomervakantie + Herkansingen 4
// '09/'10
	,'2009-12-25' => '2010-01-08' // Kerstvakantie
	,'2010-04-02' => '2010-04-05' // Goede vrijdag + pasen
	,'2010-04-30' => '2010-04-30' // Koninginnedag
	,'2010-05-05' => '2010-05-05' // Bevrijdingsdag
	,'2010-05-13' => '2010-05-04' // Hemelvaart
	,'2010-05-24' => '2010-05-24' // Pinksteren
	,'2010-07-12' => '2010-09-03' // Zomervakantie + Herkansingen 4
// '10/'11
	,'2010-12-27' => '2011-01-07' // Kersvakantie + Herkansingen 1
	,'2011-04-22' => '2011-04-25' // Goede vrijdag + pasen
	,'2011-05-05' => '2011-05-05' // Bevrijdingsdag
	,'2011-05-30' => '2011-06-03' // Herkansingen 3 + Hemelvaart
	,'2011-06-13' => '2011-06-13' // Pinksteren
	,'2011-07-11' => '2011-09-02' // Zomervakantie + Herkansingen 4
// '11/'12
	,'2011-12-26' => '2011-12-30' // Kerstvakantie
	,'2012-04-06' => '2012-04-06' // Goedevrijdag
	,'2012-04-09' => '2012-04-09' // 2e Paasdag
	,'2012-04-30' => '2012-04-30' // Koninginnedag
	,'2012-05-17' => '2012-05-17' // Hemelvaart
	,'2012-05-28' => '2012-05-28' // Pinksteren
	,'2012-07-09' => '2012-08-31' // Zomervakantie + Herkansingen 4
// '12/'13
	,'2012-09-03' => '2012-09-06' // Intro en EJBV
	,'2012-12-23' => '2013-01-04' // Kerstvakantie en hertentamenweek
	,'2013-03-29' => '2013-03-29' // Goedevrijdag
	,'2013-04-01' => '2013-04-01' // 2e Paasdag
	,'2013-04-30' => '2013-04-30' // Koninginnedag
	,'2013-05-09' => '2013-05-10' // Hemelvaart (en dag erna)
	,'2013-05-20' => '2013-05-20' // Pinksteren
	,'2013-07-08' => '2013-08-30' // Zomervakantie + Herkansingen 4
// '13/'14
	,'2013-09-02' => '2013-09-05' // Intro en EJBV
	,'2013-12-21' => '2014-01-05' // Kerstvakantie en hertentamenweek
	,'2014-04-18' => '2014-04-18' // Goedevrijdag
	,'2014-04-21' => '2014-04-21' // 2e Paasdag
	,'2014-04-26' => '2014-04-26' // Koning(!)sdag
	,'2014-05-05' => '2014-05-05' // Bevrijdingsdag
	,'2014-05-29' => '2014-05-30' // Hemelvaart (en dag erna)
	,'2014-06-09' => '2014-06-09' // 2e Pinksterdag
	,'2014-07-06' => '2014-08-31' // Zomervakantie + Herkansingen 4
// '14/'15
	,'2014-09-01' => '2014-09-04' // Intro en EJBV
	,'2014-12-22' => '2015-01-02' // Kerstvakantie

// '15'16
	,'2015-08-31' => '2015-09-04' // EJBV en Intro
	,'2015-12-21' => '2016-01-01' // Kerstvakantie

// '16'17
	,'2016-09-05' => '2016-09-09' // EJBV en Intro
	,'2016-12-26' => '2017-01-08' // Kerstvakantie
);

// LET OP LET OP: text duplicatie van bookweb/boekcom.php (r2758), graag
// gelijktijdig updaten!!

/* Besteldata van de BoekCom: array(besteldatum => leverdatum)
 * Als er een leveranciersbestelling gedaan wordt, wordt de meest recente
 * besteldatum gekozen die hooguit een week (7 dagen) in de toekomst ligt.
 * Dus, van een bestelling enkele dagen na de besteldatum in deze array wordt
 * aangenomen dat 'ie toch geleverd kan worden alsof 'ie wel op tijd gedaan
 * is.
 * Op dit moment, wordt ook als er nog geen bestelling is gedaan, voor de
 * student een projectie gemaakt alsof die een week de besteldeadline mag
 * missen. Dus hooguit een week te laat bestellen, en toch is de verwachtte
 * leverdatum alsof de student op tijd besteld heeft.
 *  */
// LET OP LET OP: text duplicatie van bookweb/boekcom.php, graag
// gelijktijdig updaten!!

$BESTELDATA = array
	('2005-07-06' => '2005-08-31' # 05-06
	,'2005-09-13' => '2005-10-10'
	,'2005-10-11' => '2005-11-10'
	,'2005-11-11' => '2005-12-05'
	,'2006-01-03' => '2006-01-31'
	,'2006-02-03' => '2006-02-27'
	,'2006-03-21' => '2006-04-18'
	,'2006-04-26' => '2006-05-22'
	,'2006-07-20' => '2006-09-01' # 1.1 06-07
	,'2006-09-13' => '2006-10-09' # 1.2
	,'2006-10-11' => '2006-11-06' # 2.1
	,'2006-11-21' => '2006-12-11' # 2.2
	,'2006-12-18' => '2007-02-01' # 3.1
	,'2007-02-15' => '2007-03-05' # 3.2
	,'2007-03-21' => '2007-04-16' # 4.1
	,'2007-04-27' => '2007-05-21' # 4.2
	,'2007-07-13' => '2007-09-01' # 1.1 07-08
	,'2007-09-14' => '2007-10-10' # 1.2
	,'2007-10-08' => '2007-11-05' # 2.1
	,'2007-11-16' => '2007-12-14' # 2.2
	,'2007-12-21' => '2008-01-28' # 3.1
	,'2008-02-08' => '2008-03-07' # 3.2
	,'2008-03-17' => '2008-04-14' # 4.1
	,'2008-04-25' => '2008-05-23' # 4.2
	,'2008-07-21' => '2008-09-08' # 1	08-09
	,'2008-09-23' => '2008-11-03' # 2
	,'2008-12-08' => '2009-01-18' # 3
	,'2009-02-07' => '2009-04-12' # 4
	,'2009-08-07' => '2009-09-07' # 1	09-10
	,'2009-10-01' => '2009-10-27' # 2.1
	,'2009-10-22' => '2009-11-19' # 2.2
	,'2009-12-13' => '2010-01-26' # 3.1
	,'2010-01-15' => '2010-02-12' # 3.2
	,'2010-03-25' => '2010-04-13' # 4.1
	,'2010-07-09' => '2010-08-25' # 1	10-11
	,'2010-10-08' => '2010-11-09' # 2.1
	,'2010-10-29' => '2010-11-30' # 2.2
	,'2010-12-13' => '2011-01-26' # 3.1
	,'2011-01-23' => '2011-02-16' # 3.2
	,'2011-03-15' => '2011-04-13' # 4.1
	,'2011-04-12' => '2011-05-10' # 4.2
	,'2011-07-27' => '2011-09-08' # 1.1		11-12
	,'2011-08-17' => '2011-09-16' # 1.2
	,'2011-10-17' => '2011-11-11' #	2.1
	,'2011-11-06' => '2011-12-05' #	2.2
	,'2011-12-16' => '2012-02-01' #	3.1
	,'2012-01-15' => '2012-02-12' # 3.2
	,'2012-03-25' => '2012-04-17' #	4.1
	,'2012-04-15' => '2012-05-08' #	4.2
	,'2012-04-28' => '2012-05-22' #	4.3
	,'2012-06-24' => '2012-08-26' #	1.1		12-13
	,'2012-07-22' => '2012-09-05' #	1.2
	,'2012-10-14' => '2012-11-06' #	2.1
	,'2012-11-06' => '2012-11-21' #	2.2
	,'2012-12-14' => '2013-01-30' #	3.1
	,'2013-01-15' => '2013-02-10' # 3.2
	,'2013-03-15' => '2013-04-17' #	4.1
	,'2013-04-18' => '2013-05-06' #	4.2
	,'2013-06-23' => '2013-08-26' #	1.1		13-14
	,'2013-07-29' => '2013-09-09' #	1.2
	,'2013-10-03' => '2013-11-01' #	2.1
	,'2013-12-04' => '2014-01-22' # 3.1
	,'2014-06-10' => '2014-07-01' # 1.1
    ,'2014-07-03' => '2014-08-26' # 1.1     14-15
	,'2014-10-03' => '2014-11-04' #	2.1
	,'2014-12-17' => '2015-01-28' # 3.1
	,'2015-02-20' => '2015-03-01' # 3.2
	,'2015-03-13' => '2015-05-16' # 4.1
	,'2015-06-26' => '2015-08-26' # 1.1		15-16 // Deze data is tijdelijk, nog niet helemaal bekend
	);

// Rekeningnummers voor rekeningafschriften-interface
$BOEKENREKENINGEN = array(
	507122690 => "Betaalrekening (50.71.22.690)",
	586063161 => "Spaarrekening (58.60.63.161)",
	576793418 => "Oude Spaarrekening (57.67.93.418)"

);

// De niet nul kosten die donateurs maken voor het Jaarverslag,Studiereis danwel Almanak opgestuurt krijgen
$DONAKOSTEN = array(
	// In verband met de regels rondom donateurs kunnen we niet meer extra geld vragen voor features.
	'almanak' => "0.00",
	'studiereis' => "0.00",
	'jaarverslag' => "0.00"
);
