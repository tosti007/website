<?php

# $Id$

/**
* Encodes header as quoted-printable
*
* Encode a string according to RFC 1522 for use in headers if it
* contains 8-bit characters or anything that looks like it should
* be encoded.
*
* @param string $string header string, that has to be encoded
* @return string quoted-printable encoded string
*/
function encodeHeader ($string) {
    $default_charset = 'utf-8';

    // Encode only if the string contains 8-bit characters or =?
    $j = strlen($string);
    $max_l = 75 - strlen($default_charset) - 7;
    $aRet = array();
    $ret = '';
    $iEncStart = $enc_init = false;
    $cur_l = $iOffset = 0;
    for($i = 0; $i < $j; ++$i) {
        switch($string{$i})
        {
        case '=':
        case '<':
        case '>':
        case ',':
        case '?':
        case '_':
            if ($iEncStart === false) {
                $iEncStart = $i;
            }
            $cur_l+=3;
            if ($cur_l > ($max_l-2)) {
                /* if there is an stringpart that doesn't need encoding, add it */
                $aRet[] = substr($string,$iOffset,$iEncStart-$iOffset);
                $aRet[] = "=?$default_charset?Q?$ret?=";
                $iOffset = $i;
                $cur_l = 0;
                $ret = '';
                $iEncStart = false;
            } else {
                $ret .= sprintf("=%02X",ord($string{$i}));
            }
            break;
        case '(':
        case ')':
            if ($iEncStart !== false) {
                $aRet[] = substr($string,$iOffset,$iEncStart-$iOffset);
                $aRet[] = "=?$default_charset?Q?$ret?=";
                $iOffset = $i;
                $cur_l = 0;
                $ret = '';
                $iEncStart = false;
            }
            break;
        case ' ':
            if ($iEncStart !== false) {
                $cur_l++;
                if ($cur_l > $max_l) {
                    $aRet[] = substr($string,$iOffset,$iEncStart-$iOffset);
                    $aRet[] = "=?$default_charset?Q?$ret?=";
                    $iOffset = $i;
                    $cur_l = 0;
                    $ret = '';
                    $iEncStart = false;
                } else {
                    $ret .= '_';
                }
            }
            break;
        default:
            $k = ord($string{$i});
            if ($k > 126) {
                if ($iEncStart === false) {
                    // do not start encoding in the middle of a string, also take the rest of the word.
                    $sLeadString = substr($string,0,$i);
                    $aLeadString = explode(' ',$sLeadString);
                    $sToBeEncoded = array_pop($aLeadString);
                    $iEncStart = $i - strlen($sToBeEncoded);
                    $ret .= $sToBeEncoded;
                    $cur_l += strlen($sToBeEncoded);
                }
                $cur_l += 3;
                /* first we add the encoded string that reached it's max size */
                if ($cur_l > ($max_l-2)) {
                    $aRet[] = substr($string,$iOffset,$iEncStart-$iOffset);
                    $aRet[] = "=?$default_charset?Q?$ret?= "; /* the next part is also encoded => separate by space */
                    $cur_l = 3;
                    $ret = '';
                    $iOffset = $i;
                    $iEncStart = $i;
                }
                $enc_init = true;
                $ret .= sprintf("=%02X", $k);
            } else {
                if ($iEncStart !== false) {
                    $cur_l++;
                    if ($cur_l > $max_l) {
                        $aRet[] = substr($string,$iOffset,$iEncStart-$iOffset);
                        $aRet[] = "=?$default_charset?Q?$ret?=";
                        $iEncStart = false;
                        $iOffset = $i;
                        $cur_l = 0;
                        $ret = '';
                    } else {
                        $ret .= $string{$i};
                    }
                }
            }
            break;
        }
    }

    if ($enc_init) {
        if ($iEncStart !== false) {
            $aRet[] = substr($string,$iOffset,$iEncStart-$iOffset);
            $aRet[] = "=?$default_charset?Q?$ret?=";
        } else {
            $aRet[] = substr($string,$iOffset);
        }
        $string = implode('',$aRet);
    }
    return $string;
}
