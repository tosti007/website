<?php

use \Symfony\Component\HttpFoundation\Response;

function webcam()
{
	global $request;

	// Dit ip-adres is vermoedelijk de TV
	if (!hasAuth('ingelogd') && $request->server->get('REMOTE_ADDR') !== '10.14.0.43')
	{
		return responseUitStatusCode(403);
	}

	$page = new HTMLPage();

	$page->addFooterJS(Page::minifiedFile('webcam.js'));

	$page->start();

	$page->add($div = new HtmlDiv(null, 'row'));
	for ($i = 1; $i <= 4; $i++)
	{
		$img = new HtmlImage(WEBCAM_BASE . $i . "?" . time(),
			_('A–Eskwadraat webcam'),
			_('A–Eskwadraat webcam'),
			'thumbnail webcam'
		);
		$div->add(new HtmlDiv($img, 'col-sm-6'));
	}

	return new PageResponse($page);
}

function getWebcamPath($nr)
{
	return "webcam/webcam-".((int)$nr).".jpeg";
}

function webcamNr($args)
{
	global $filesystem;

	if (!$filesystem->has(getWebcamPath($args[0])))
	{
		return false;
	}

	return array('name' => (int)$args[0]
	, 'displayName' => (int)$args[0]
	, 'access' => true
	);
}

function webcamImage()
{
	global $request;

	if(!hasAuth('ingelogd') && $request->server->get('REMOTE_ADDR') !== '10.14.0.43')
	{
		return responseUitStatusCode(403);
	}

	return sendFileFromFS(getWebcamPath(vfsVarEntryName()));
}
