<?php
// Is helaas nog nodig vanwege backwards compat, moet ook eens weg op den duur

function commissiewsw_content($rest)
{
	global $uriRef, $INDEX;
	global $space;

	$requestURL = $uriRef['request'];

	$entry = $uriRef['entry'];

	if(!$rest)
	{
		//Zoek de index.html en gebruik die
		if($child = $entry->getChild($INDEX)) {
			$child->display();
		}
		//Redirect anders naar Commissies/*/Info
		$uriRef['remaining'] = 'Info';
	}

	//Ga anders naar *.
	$ster = $entry->getParent()->getChild('*');
	//Verander de NAAM van het sterobject zodat de views de goede naam van de commissie hebben.
	if(!$ster->checkUrl($entry->getName())) //Als dit fout gaat is er echt stront aan de knikker!
	{
		return responseUitStatusCode(500);
	}

	// We gaan nu vanaf de variabele entry, niet vanaf de haak, door benamite lopen.
	$target = hrefToReference($uriRef['remaining'], $uriRef['params'], $ster, null, $uriRef['rewrite']);
	return $space->getResponse($requestURL, $target);
}
