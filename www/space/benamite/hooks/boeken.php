<?php
/**
 *	$Id$
 */

function boekenbestelbaar_content($rest)
{
	global $request;
	$request->request->set('page', 'alleboeken');
	require_once("bookweb/home.html");
}

function boekenbestelling_content($rest)
{
	global $request;
	$request->request->set('page', 'bestellen');
	require_once("bookweb/home.html");
}

function boekenvrijeverkoop_content($rest)
{
	global $request;
	$request->request->set('page', 'allelossevk');
	require_once("bookweb/home.html");
}

function bookwebverkoop_menu()
{
	if ( hasAuth('verkoper') ) {
		# whoeij
		require_once('bookweb/interface.php');
		require_once('bookweb/verkoop.php');
		if(tryVerkoopOpen()) {
			return array('url' => '/Onderwijs/Boeken/Verkoop', 'displayName' => _('Verkoop-menu'));
		} else {
			return array('url' => '/Onderwijs/Boeken/Openverkoop', 'displayName' => _('Open Boekverkoop'));
		}
	}
}

function bookwebfinancieel_menu()
{
	if ( hasAuth('boekcom') ) {
		return array('url' => '/Onderwijs/Boeken/Financieel', 'displayName' => 'Financieel');
	}
}
