<?php
// $Id: $

use Symfony\Component\HttpFoundation\Response;

/**
 * @brief Deze klasse representeert een HTTP-statuscode.
 *
 * Dit zou een backwards-compatible overgang moeten zijn tussen exit() aanroepen in spaceHTTP,
 * en werken met Symfony-responses.
 *
 * @sa spaceHTTP
 */
class HTTPStatusException extends ResponseException {
	/**
	 * @brief Deze int representeert de HTTP-statuscode.
	 *
	 * Het is niet de bedoeling dat de waarde hiervan 200 is:
	 * gebruik daar een normale Response voor svp.
	 */
	protected $statusCode;
	/**
	 * @brief Indien geset, wordt er geen error-response gemaakt maar in plaats
	 * daarvan een 'Location: $bestemming'-header.
	 */
	protected $bestemming;

	public function __construct($code, $bestemming=NULL) {
		$this->statusCode = $code;
		$this->bestemming = $bestemming;
	}

	/**
	 * @brief Maak een Response-object van deze exception.
	 *
	 * @return Een Symfony\Component\HttpFoundation\Response met de juiste statuscode.
	 */
	public function maakResponse() {
		if (getZeurmodus()) {
			user_error("Gebruik responseUitStatusCode ipv spaceHTTP want dat is niet zo stateful.", E_USER_DEPRECATED);
		}
		return responseUitStatusCode($this->statusCode, $this->bestemming);
	}
}

/**
 * @brief Verstuur een HTTP status code.
 *
 *	Zie ook RFC 2616 voor toelichting.
 *	http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10
 *
 * @param code Een int met de HTTP-statuscode.
 * @param dest Indien geset, wordt er geen response gemaakt met errorpagina,
 * maar in plaats daarvan een 'Location: $destination'-header.
 *
 * @deprecated Gebruik liever iets als responseUitStatusCode
 *
 * @sa HTTPStatusException
 * @sa responseUitStatusCode
 *
 * @return Gooit een HTTPStatusException die space/space.php afvangt om de response te geven.
 */
function spaceHTTP($code, $dest = NULL)
{
	if (getZeurmodus()) {
		user_error("gebruik liever iets als responseUitStatusCode", E_USER_DEPRECATED);
	}
	throw new HTTPStatusException($code, $dest);
}

/**
 * @brief Verstuur een HTTP status code.
 *
 *	Zie ook RFC 2616 voor toelichting.
 *	http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10
 *
 * @param code Een int met de HTTP-statuscode.
 * @param dest Indien geset, wordt er geen response gemaakt met errorpagina,
 * maar in plaats daarvan een 'Location: $destination'-header.
 *
 * @return Response Response-object die je kan senden voor de error.
 */
function responseUitStatusCode($code, $dest=NULL) {
	global $logger;
	global $request, $uriRef;

	switch($code) {
		case 301:	$msg = "Moved Permanently";				break;
		case 302:	$msg = "Temporary";						break;
		case 303:	$msg = "See Other";						break;
		case 400:	$msg = "Bad request";					break;
		case 401:	$msg = "Unauthorized";					break;
		case 402:	$msg = "Payment required";				break;
		case 403:	$msg = "Forbidden";						break;
		case 404:	$msg = "Not Found";						break;
		case 405:	$msg = "Method not allowed";			break;
		case 406:	$msg = "Not acceptable";				break;
		case 407:	$msg = "Proxy Authentication Required";	break;
		case 408:	$msg = "Request timed out";				break;
		case 409:	$msg = "Conflict";						break;
		case 410:	$msg = "Gone";							break;
		case 411:	$msg = "Length required";				break;
		case 412:	$msg = "Precondition failed";			break;
		case 413:	$msg = "Request Entity Too Large";		break;
		case 414:	$msg = "Request-URI Too Long";			break;
		case 415:	$msg = "Unsupported Media Type";		break;
		case 418:	$msg = "I'm a Teapot";					break;
		case 500:	$msg = "Internal Server Error";			break;
		case 501:	$msg = "Not Implemented";				break;
		case 502:	$msg = "Bad Gateway";					break;
		case 503:	$msg = "Service Unavailable";			break;
		case 504:	$msg = "Gateway Timeout";				break;
		default:	$msg = "Status Code $code";				break;
	}
	if($code == 403 && !hasAuth('ingelogd')) {
		// TODO SOEPMES: wat heeft dit te maken met uriRef (en wat is een uriRef eigenlijk?)
		$uriRef['error'] = _("Misschien moet je eerst inloggen?")
						 . "<p>".makelogin($uriRef).'</p>';
	}

	$response = new Response('', $code);

	$protocol = $request->server->get('SERVER_PROTOCOL');
	if ($protocol) {
		$response->headers->set($protocol, "$code $msg");
	}

	if(isset($dest)) {
		$response->headers->set("Location", $dest);
		return $response;
	}

	$file = FS_ROOT."/Errors/$code.html";
	if(!is_file($file) || !is_readable($file)) {
		user_error("Internal error: Would display $code, but $code page not found", E_USER_ERROR);
	}

	$page = new HTMLPage();
	$page->start();
	ob_start();
	require($file);
	$logger->debug('responseUitStatusCode voor entry ' . print_r(@$uriRef['entry'], true));
	$page->add(ob_get_clean());
	$response->setContent($page->respond());
	return $response;
}

function spaceRedirect($dest)
{
	spaceHTTP(302, $dest);
}

/**
 * @brief Geef een Response die de browser redirect naar de gegeven URL.
 *
 * @return Response De redirect-Response.
 */
function responseRedirect($dest) {
	return responseUitStatusCode(302, $dest);
}

/***
 *	Helper functie voor het verwerken van een URL array.
 *	Retourneerd True als er geen stukjes $url in $rest zitten.
 */
function urlIsEmpty($rest)
{
	return (count($rest) == 0);
}

/***
 * @brief Geef de laatste waarde voor een variabele entry.
 *
 * Gaat ervan uit dat `$uriRef` geset is, bijvoorbeeld door hrefToReference.
 */
function vfsVarEntryName()
{
	global $uriRef;
	$entry = $uriRef['entry'];

	while(! $entry instanceof vfsEntryVariable)
	{
		$entry = $entry->getParent();
	}
	return $entry->getName();
}

function vfsVarEntryNames($entry = NULL)
{
	global $uriRef;
	$names = array();
	if(!isset($entry))
		$entry = $uriRef['entry'];
	if(!isset($entry))
		return $names;

	while(! $entry->isRoot())
	{
		if($entry instanceof vfsEntryVariable)
			$names[] = $entry->getName();

		$entry = $entry->getParent();
	}
	return $names;
}

/**
 * @brief Nuttig om aan te roepen en te returnen als je pagina klaar is.
 *
 * Zorgt ervoor dat verscheidene dingen netjes opgeruimd worden.
 *
 * Is netter dan exit() aanroepen, want
 * a) expliciet dat je klaar bent ipv heel erg dood
 * b) sloopt tests niet.
 *
 * @returns Een leeg Response-object.
 */
function spaceFinished() {
	// Er is niet veel om stuk te laten gaan.
	return new Response();
}
