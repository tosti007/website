<?php
// $Id$

define( 'ENTRY',                 0 );
define( 'ENTRYDIR',              1 );
define( 'ENTRYFILE',             2 );
define( 'ENTRYUPLOADED',         3 );
define( 'ENTRYHOOK',             4 );
define( 'ENTRYLINK',             5 );
define( 'ENTRYINCLUDE',          6 );
define( 'ENTRYVARIABLE',         7 );

define( 'NO_PARENT',             0 );
define( 'NO_CHILD',             -2 );
define( 'NO_PREV',              -3 );
define( 'NO_NEXT',              -4 );

define( 'ROOT_ID',              11444);

// LET OP: op verschillende plaatsen in de code is de aanname dat dit evalueert
// naar false in boolean context
define( 'INPUT_ERROR',  null);

$HOOK_TYPES = array('menu', 'rewrite', 'content');
