<?php
// $Id$

/***
 *	_VFSENTRIES [ $id ] = entry
 *	globale array waar alle (nodige) vfs entries in komen
 */
global $_VFSENTRIES;
$_VFSENTRIES = array();

class vfs
{
	/**
	 * De namen van de velden voor een entry in SQL.
	 *
	 * Doe dus iets als 'SELECT ' . vfs::SQL_ENTRY_FIELDNAMES . ' FRON benamite WHERE ...'.
	 * Je wil dit doen in plaats van 'SELECT * FROM benamite WHERE...',
	 *  om iets minder koppeling te hebben tussen de precieze kolomlijst en de code.
	 */
	const SQL_ENTRY_FIELDNAMES = 'id, type, name, displayName, displayName_eng, parent, auth, visible, special, rank, wanneer, wie';

	/**
	 * @brief Geef de vfsEntry met de gegeven ID.
	 *
	 * Zoekt eerst in een globaal array, en probeert het anders uit de DB op te halen.
	 *
	 * @return vfsEntry|null
	 */
	static function get($id)
	{
		global $_VFSENTRIES;

		$id = (int)$id;
		if ($id <= 0)
		{
			return null;
		}

		if (!isset($_VFSENTRIES[$id]))
		{
			self::getMulti([$id]);
		}

		return $_VFSENTRIES[$id];
	}

	/**
	 * @brief Geef alle entry-id's.
	 *
	 * @return Een array met de id's van elke entry, die in ::getMulti kan.
	 */
	static function geefAlleIDs()
	{
		global $BMDB;
		return $BMDB->q('COLUMN SELECT id FROM benamite;');
	}

	/**
	 * @brief Zoek meerdere entries tegelijkertijd op, op ID.
	 */
	static function getMulti($ids)
	{
		global $_VFSENTRIES, $BMDB;

		$todo = [];

		foreach ($ids as $id)
		{
			if (isset($_VFSENTRIES[$id]))
			{
				continue;
			}

			$todo[] = $id;
		}

		if (count($todo) == 0)
		{
			return;
		}

		$data = $BMDB->q('TABLE SELECT ' . vfs::SQL_ENTRY_FIELDNAMES . ' FROM benamite WHERE id IN (%Ai)', $todo);
		foreach ($data as $fields)
		{
			$id = $fields['id'];

			$_VFSENTRIES[$id] = self::fieldsToEntry($fields);
		}
		foreach ($todo as $id)
		{
			if (isset($_VFSENTRIES[$id]))
			{
				continue;
			}

			$_VFSENTRIES[$id] = false;
		}
	}

	/***
	 * @brief Voeg een nieuwe vfsEntry toe aan het systeem.
	 *
	 * Checkt eerst alle velden.
	 *
	 * @return vfsEntry|false
	 * Een verwijzing naar de nieuwe entry, of `false` wanneer onsuccesvol.
	 */
	static function add($fields)
	{
		global $auth;
		global $BMDB;
		counter_mark(__CLASS__.'::'.__FUNCTION__);

		unset($fields['id']);

		// do clean up and checking of $fields
		$fields = vfs::cleanFields($fields);

		if (empty($fields['displayName']))
		{
			$fields['displayName'] = $fields['name'];
		}

		if ($fields['type'] == ENTRYVARIABLE)
		{
			$fields['name'] = '*';
		}

		$fields['parent'] = (int)$fields['parent'];
		// FIXME: gebruik geen @ pls
		$fields['visible'] = (boolean)@$fields['visible'];

		$fields['name'] = str_replace('/', '', $fields['name']);
		if (empty($fields['name']) || !isset($fields['parent']) || vfs::exists($fields['name'], $fields['parent']))
		{
			return false;
		}

		if (empty($fields['displayName_eng']))
		{
			$fields['displayName_eng'] = $fields['name'];
		}

		if (empty($fields['content']))
		{
			$fields['content'] = null;
		}
		if (empty($fields['special']))
		{
			$fields['special'] = null;
		}

		switch ($fields['type'])
		{
			case ENTRYFILE:
				$fields['special'] = null;
				if (empty($fields['content']))
				{
					$fields['content'] = 'Dit is een nieuwe pagina, vermaak je!';
				}
				break;
			default:
				$fields['content'] = null;
				break;
		}

		if (!isset($fields['wanneer']))
		{
			$fields['wanneer'] = getDateTime();
		}
		if (!isset($fields['wie']))
		{
			$fields['wie'] = $auth->getLidnr();
		}

		if (!isset($fields['rank']))
		{
			$fields['rank'] = $BMDB->q(
					'MAYBEVALUE SELECT MAX(rank)'
					. ' FROM benamite'
					. ' WHERE parent = %i',
					$fields['parent']
				) + 1;
		}
		$id = $BMDB->q('RETURNID INSERT INTO benamite SET %S', $fields);

		$parent = vfs::get($fields['parent']);
		$parent->unsetChildCache($fields['name']);

		gooiCacheEntrysWeg();
		return vfs::get($id);
	}

	/***
	 * @brief Verwijder een vfsEntry en al z'n kinderen.
	 */
	static function del($entry)
	{
		global $_VFSENTRIES, $BMDB;
		$id = $entry->getId();
		$rank = $entry->getRank();

		// parent shouldn't reference this entry any more
		$parent = $entry->getParent();
		$parent->unsetChildCache($entry->getName());

		// remove any possible children
		if ($entry->isDir())
		{
			$children = $entry->getChildrenIds();
			foreach ($children as $childId)
			{
				$child = vfs::get($childId);
				$entry->removeChild($child);
				vfs::del($child);
			}
		}
		// now also remove from DB
		unset($_VFSENTRIES[$id]);
		$BMDB->q('DELETE FROM benamite WHERE id = %i', $id);
		$BMDB->q('UPDATE benamite SET rank = rank - 1 ' . 'WHERE rank > %i AND parent = %i', $rank, $parent->getId());
		gooiCacheEntrysWeg();
	}

	private static function cleanFields($fields)
	{
		foreach ($fields as $key => $val)
		{
			if (is_numeric($key))
			{
				unset($fields[$key]);
				continue;
			}
			$fields[$key] = trim($val);
		}
		return $fields;
	}

	/**
	 * @brief Kijk of een entry met een bepaalde naam al bestaat in een map
	 */
	static function exists($name, $parent)
	{
		global $BMDB;

		$id = $BMDB->q("MAYBEVALUE SELECT `id` FROM `benamite` WHERE `parent`=%i AND `name`=%s", $parent, $name);

		return $id;
	}

	/**
	 * Geef een vfsEntry op basis van de fields in de databaas.
	 * Die kun je bijvoorbeeld krijgen als array door middel van een aanroep op `$BMDB->q`.
	 *
	 * @param array $fields
	 *
	 * @return vfsEntry
	 * De entry die correspondeert met de gegeven fields.
	 */
	private static function fieldsToEntry($fields)
	{
		// De DisplayName varieert op basis van de huidige taal.
		if (getLang() == "en")
		{
			$fields['displayName'] = !empty($fields['displayName_eng']) ?
				$fields['displayName_eng'] : $fields['displayName'];
		}

		// De klassen handelen het verdere constructen af, dus we hoeven alleen de juiste klasse aan te roepen.
		switch ($fields['type'])
		{
			case ENTRYDIR:
				return new vfsEntryDir($fields);
				break;
			case ENTRYFILE:
				return new vfsEntryFile($fields);
				break;
			case ENTRYUPLOADED:
				return new vfsEntryUploaded($fields);
				break;
			case ENTRYHOOK:
				return new vfsEntryHook($fields);
				break;
			case ENTRYLINK:
				return new vfsEntryLink($fields);
				break;
			case ENTRYINCLUDE:
				return new vfsEntryInclude($fields);
				break;
			case ENTRYVARIABLE:
				return new vfsEntryVariable($fields);
				break;
			default:
				user_error("Onbekend type: " . $fields['type'], E_USER_ERROR);
		}
	}

	/**
	 * Geef de vfsEntry die correspondeert met de fields uit de databaas.
	 * Als de bijbehorendeentry al in de cache staat, geven we die.
	 * Anders wordt een nieuw vfsEntry-object aangemaakt en in de cache gestopt.
	 *
	 * De $fields mogen ook null zijn, indien $id geset is.
	 * Zo kun je de volgende constructie gebruiken:
	 * $entry = vfs::geefVanFields($BMDB->q(...)),
	 * en wordt alles netjes gecached.
	 *
	 * @see vfs::get
	 * @see vfs::getMulti
	 *
	 * @param array|null $fields
	 * De fields uit de database, of null.
	 * @param int|null $id
	 * Het id van de vfsEntry (optioneel). Wordt gebruikt als $fields === null toch te kunnen cachen.
	 * @return vfsEntry|null
	 */
	public static function geefVanFields($fields, $id = null)
	{
		global $_VFSENTRIES;

		// Geen fields: cache dat en geef het op.
		if (!$fields)
		{
			if (!$id)
			{
				$_VFSENTRIES[$id] = false;
			}
			return null;
		}

		// Zoek het op in de cache.
		$id = $fields['id'];
		if (array_key_exists($id, $_VFSENTRIES))
		{
			return $_VFSENTRIES[$id];
		}

		// Bouw een nieuw object en cache dat.
		$entry = self::fieldsToEntry($fields);
		$_VFSENTRIES[$id] = $entry;
		return $entry;
	}
}
