<?php
// $Id: vfsEntryDir.php 9690 2010-01-22 19:06:53Z pwerken $

class vfsEntryVariable extends vfsEntryDir
{
	private $access;

	/**
	 * @brief (Maybe) de string waarheen de URL herschreven moet worden.
	 *
	 * In de checkURL-methode kan dit op niet-null geset worden,
	 * in welk geval deze entry gaat werken als link i.p.v. directory.
	 * (Dit is bedoeld om Publisher een hele URL te laten accepteren:
	 * rewrite Publisher/Lange/Url/Enzo naar Publisher/<entry-id>.)
	 */
	private $rewrite = null;

	/***
	 *	Constructor, should not be called!
	 *	Instead use vfs::get($id) or vfs::add($fields)
	 */
	function __construct($fields)
	{
		parent::__construct($fields);

		$this->access = True;
	}

 /****************************************************************************/

	function setName($n)
	{
		// FIXME: wat.
		return;
	}
	function mayAccess()
	{
		if(!$this->access)
			return False;

		return parent::mayAccess();
	}
	function isVisible()
	{
		return false;
	}
	function setVisible($v)
	{
		// FIXME: wat.
		return;
	}

	/**
	 * @brief Geef (Maybe) de URL waar deze entry en diens children te vinden zijn.
	 *
	 * Als dit voor een entry met URL '/Aap/Noot' niet null is,
	 * maar bijvoorbeeld 'Mies', dan wordt '/Aap/Noot/Random/Tsjak'
	 * herschreven naar '/Aap/Mies'.
	 * Bij een vfsEntryLink, die getLink gebruikt, zou dat '/Aap/Mies/Random/Tsjak' worden.
	 *
	 * Deze implementatie geeft de waarde van $this->rewrite,
	 * die als nuttige bijwerking van checkUrl geset kan worden.
	 *
	 * @return null, of een string die een URL aangeeft.
	 */
	function getRewriteNaar()
	{
		return $this->rewrite;
	}

	/**
	 * @brief Check of het variabele stukje geldig is.
	 *
	 * Als nuttige bijwerking worden ook de access en displayname bijgewerkt.
	 *
	 * @param $stukje is wat er op de plek van * in de url is ingevuld.
	 * vb: url = /Leden/2998
	 * 2998 matched nergens mee, wat voor een fallback naar *-entry zorgt
	 * waardoor checkUrl aangeroepen wordt met $stukje == '2998'
	 *
	 * @return Bool of de entry geldig is. (Denk bij false aan Error 404.)
	 */
	function checkUrl($stukje)
	{
		if($stukje == '*')
			return False;

		$func = $this->getSpecialFunc();
		$args = $this->getEntryNames();
		// Vervang '*' met de childnaam.
		// ($args is het pad met de root als laatst)
		$args[0] = $stukje;

		$calledFunc = $this->getMethodFromFuncString($func);
		$ret = $calledFunc($args);

		if(!$ret)
			return False;

		// Je moet kiezen uit rewriten of een naam accepteren.
		if (isset($ret['rewrite']))
		{
			$this->rewrite = $ret['rewrite'];
			$this->name = $ret['name'];
		}
		else
		{
			// Zet rewriten expliciet uit,
			// anders krijg je misschien oneindige rewriteloops ivm caching.
			$this->rewrite = null;
			$this->name = $ret['name'];
		}

		if(isset($ret['displayName']))
			$this->displayName = $ret['displayName'];
		if(isset($ret['access']))
			$this->access = $ret['access'];
		if(!$ret['access'])
			$this->displayName = _('Geen toegang');

		return True;
	}
}
