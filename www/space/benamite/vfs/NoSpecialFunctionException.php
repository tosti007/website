<?php

namespace Space\Benamite;

use Exception;

/**
 * Gegooid als de speciale functie van een vfsEntry ontbreekt.
 */
class NoSpecialFunctionException extends Exception
{

}