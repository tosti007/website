<?php
// $Id$

$vfsinittm = new ProfilerTimerMark('vfs/vfsInit.php');
Profiler::getSingleton()->addTimerMark($vfsinittm);

require_once('vfsConstants.php');
require_once('vfsGlobals.php');
require_once('vfsChecks.php');

function benamite_autoload($class)
{
	switch($class)
	{
	case 'vfs':
		require_once('space/benamite/vfs/vfs.php');
		break;
	case 'vfsEntry':
		require_once('space/benamite/vfs/vfsEntry.php');
		break;
	case 'vfsEntryHook':
		require_once('space/benamite/vfs/vfsEntryHook.php');
		break;
	case 'vfsEntryLink':
		require_once('space/benamite/vfs/vfsEntryLink.php');
		break;
	case 'vfsEntryInclude':
		require_once('space/benamite/vfs/vfsEntryInclude.php');
		break;
	case 'vfsEntryDir':
		require_once('space/benamite/vfs/vfsEntryDir.php');
		break;
	case 'vfsEntryFile':
		require_once('space/benamite/vfs/vfsEntryFile.php');
		break;
	case 'vfsEntryUploaded':
		require_once('space/benamite/vfs/vfsEntryUploaded.php');
		break;
	case 'vfsEntryVariable':
		require_once('space/benamite/vfs/vfsEntryVariable.php');
		break;
	}
}
spl_autoload_register('benamite_autoload');

$vfsinittm->markEnd();

$vfsinittm = new ProfilerTimerMark('getTreeId()');
Profiler::getSingleton()->addTimerMark($vfsinittm);

getTreeId();

$vfsinittm->markEnd();

