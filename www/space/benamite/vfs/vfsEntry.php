<?php
// $Id$
use Space\Benamite\NoSpecialFunctionException;

require_once 'space/benamite/vfs/NoSpecialFunctionException.php';

/**
 * @brief De algemene basisclass voor VFS entries.
 */
class vfsEntry
{
	private $type;
	private $id;

	protected $name;
	protected $displayName;
	private $parentId;
	private $auth;
	private $visible;
	private $special;
	private $viewcount;

	private $rank;
	private $modwhen;
	private $modwho;


	/**
	 * @brief De constructor
	 *
	 * Roep deze niet direct aan!
	 * Gebruik in plaats daarvan `vfs::get()` of `vfs::add()`.
	 */
	function __construct($fields)
	{
		$this->type = (int)$fields['type'];
		$this->id = (int)$fields['id'];
		$this->name = $fields['name'];
		$this->displayName = $fields['displayName'];
		$this->parentId = (int)$fields['parent'];
		$this->auth = $fields['auth'];
		$this->visible = (boolean)$fields['visible'];
		$this->special = $fields['special'];

		$this->rank = (int)$fields['rank'];
		$this->modwhen = $fields['wanneer'];
		$this->modwho = (int)$fields['wie'];
	}

	/**
	 * @brief Converteer de vfsEntry naar een stringweergave.
	 *
	 * @return Een menselijk leesbare string die deze vfsEntry representeert.
	 */
	public function __toString()
	{
		return $this->url() . " (" . $this->getID() . ")";
	}

	function getId()
	{
		return $this->id;
	}

	function getType()
	{
		return $this->type;
	}

	function getName()
	{
		return $this->name;
	}

	function setName($n)
	{
		$n = trim(vfsSlashCheck($n));

		$parent = $this->getParent();
		if (empty($n) || $parent->hasChild($n) || $n == $this->getName())
		{
			return;
		}
		if ($n == '*' && !$this->isA(ENTRYVARIABLE))
		{
			return;
		}
		$this->name = $this->setField('name', $n);
		gooiCacheEntrysWeg();
	}

	function getDisplayName()
	{
		return $this->displayName;
	}

	function setDisplayName($n)
	{
		$n = trim($n);
		if (empty($n))
		{
			$n = $this->name;
		}
		if ($n == $this->getDisplayName())
		{
			return;
		}

		switch (getLang())
		{
			case 'en':
				$field = 'displayName_eng';
				break;
			default:
				$field = 'displayName';
				break;
		}
		$this->displayName = $this->setField($field, $n);
		gooiCacheEntrysWeg();
	}

	function getTooltip()
	{
		return $this->tooltip;
	}

	function setTooltip($t)
	{
		$t = trim($t);

		if (empty($t) || $t == $this->getTooltip())
		{
			return;
		}

		// Zet het juiste veld voor de juiste taal om de
		// displayName naar binnen te trekken

		if (getLang() == "en")
		{
			$tooltipTaal = "tooltip_eng";
		}
		else
		{
			$tooltipTaal = "tooltip";
		}

		$this->tooltip = $this->setField($tooltipTaal, $t);
	}

	function getParentId()
	{
		return $this->parentId;
	}

	function getParent()
	{
		return vfs::get($this->getParentId());
	}

	function getAuth()
	{
		return $this->auth;
	}

	function setAuth($a)
	{
		if ($a != $this->getAuth())
		{
			$this->auth = $this->setField('auth', $a);
		}
	}

	function mayAccess()
	{
		$lvl = $this->getAuth();
		return hasAuth(($lvl ? $lvl : 'gast'));
	}

	function isVisible()
	{
		return $this->visible;
	}

	function setVisible($v)
	{
		$v = (boolean)$v;
		if ($this->isVisible() == $v)
		{
			return;
		}
		$this->visible = $this->setField('visible', $v);
		gooiCacheEntrysWeg();
	}

	public function getSpecial()
	{
		return $this->special;
	}

	public function setSpecial($d)
	{
		$d = vfsSlashCheck(trim($d));
		if ($d == $this->getSpecial())
		{
			return;
		}
		$this->special = $this->setField('special', $d);
	}

	function setRank($r)
	{
		$this->rank = $r;
	}

	function getRank()
	{
		return $this->rank;
	}

	function getLastModified()
	{
		return $this->modwhen;
	}

	function getLastModifier()
	{
		return $this->modwho;
	}

	/****************************************************************************/

	private function isA($class)
	{
		return ($this->getType() == $class);
	}

	function isDir()
	{
		return $this->isA(ENTRYDIR) || $this->isA(ENTRYVARIABLE);
	}

	function isFile()
	{
		return $this->isA(ENTRYFILE);
	}

	function isHook()
	{
		return $this->isA(ENTRYHOOK) ||
			(($this->isA(ENTRYDIR) || $this->isA(ENTRYVARIABLE))
				&& $this->getHook());
	}

	function isInclude()
	{
		return $this->isA(ENTRYINCLUDE);
	}

	/**
	 * @brief Is deze entry een link?
	 *
	 * @return Bool. Zo ja, heeft de entry een methode getLink().
	 */
	function isLink()
	{
		return $this->isA(ENTRYLINK);
	}

	/**
	 * @brief Geef (Maybe) de URL waar deze entry en diens children te vinden zijn.
	 *
	 * Als dit voor een entry met URL '/Aap/Noot' niet null is,
	 * maar bijvoorbeeld 'Mies', dan wordt '/Aap/Noot/Random/Tsjak'
	 * herschreven naar '/Aap/Mies'.
	 * Bij een vfsEntryLink, die getLink gebruikt, zou dat '/Aap/Mies/Random/Tsjak' worden.
	 *
	 * Defaultimplementatie geeft altijd null, override dit in je childklasse.
	 *
	 * @return null, of een string die een URL aangeeft.
	 */
	function getRewriteNaar()
	{
		return null;
	}

	/****************************************************************************/

	/**
	 * @brief Geef deze entry weer als Response-object.
	 *
	 * Voert alles uit om de inhoud van deze entry aan de gebruiker te tonen.
	 *
	 * LET OP: Deze methode is alleen geimplementeerd in subclasses van `vfsEntry`.
	 *
	 * @returns Een Response-object, waar ->send() op aangeroepen kan worden.
	 *
	 * @throws ResponseException
	 * Om flow te short-circuiten, kan ook een ResponseException gegooid worden.
	 * Je kan dan de response zelf krijgen door daarop ->maakResponse() aan te roepen.
	 */
	function display()
	{
		global $logger;
		$logger->error('vfsEntry::display aangeroepen, dit moet op een subklasse gebeuren!');
		return responseUitStatusCode(500);
	}

	/**
	 * @brief Check of deze entry de 'root' entry is.
	 */
	function isRoot($root = null)
	{
		return $this->getId() == getTreeId($root)
			|| $this->getId() == ROOT_ID;
	}

	/**
	 * @brief Probeer de inhoud van het bestand waar het speciale veld naar
	 *     verwijst te lezen.
	 *
	 * @returns De inhoud van het bestand achter het speciale veld als dat een
	 *     waarde heeft, anders `NULL`.
	 */
	function getSpecialFile()
	{
		global $logger;

		$special = $this->getSpecial();

		if (empty($special))
		{
			return null;
		}

		$arr = explode(':', FS_ROOT . $special);
		$file = $arr[0];

		if (!is_file($file) || !is_readable($file))
		{
			$logger->info('Ontbrekend bestand: ' . $file);
			user_error("File not found on disk, internal error.", E_USER_ERROR);
		}
		return $file;
	}

	/**
	 * @brief Probeer het speciale veld van de entry als een functie te lezen,
	 *    en probeer in te laden.
	 *
	 * LET OP: doet als side-effect een require_once op het bestand van
	 * getSpecialFile().
	 *
	 * @returns callable
	 * De functie wanneer die aangeroepen kan worden, anders wordt een NoSpecialFunctionException gegooid.
	 *
	 * @throws NoSpecialFunctionException
	 * Indien de speciale functie niet bestaat.
	 */
	function getSpecialFunc($postfix = NULL)
	{
		$file = $this->getSpecialFile();
		if (!$file)
		{
			throw new NoSpecialFunctionException("Naam van de benodigde file ontbreekt.");
		}

		require_once($file);

		$arr = explode(':', $this->getSpecial());
		if (!isset($arr[1]))
		{
			throw new NoSpecialFunctionException("Naam van de speciale functie ontbreekt.");
		}

		if (count($arr) == 2)
		{ //only func
			$func = $arr[1] . $postfix;

			if (!function_exists($func))
			{
				throw new NoSpecialFunctionException("Speciale functie niet gevonden bij vfsEntry: " . $func);
			}
		}
		elseif (count($arr) == 3)
		{ //class && func
			$func = $arr[1] . '::' . $arr[2] . $postfix;

			if (!is_callable($func))
			{
				throw new NoSpecialFunctionException("Speciale functie niet gevonden bij vfsEntry: " . $func);
			}
		}
		else
		{
			throw new NoSpecialFunctionException("Je mag je speciale veld maar in 3 delen opdelen met ':'. Joh!");
		}
		return $func;
	}

	/**
	 * @brief Geef de (relatieve) URL van deze entry.
	 *
	 * @param root De entry om bij te stoppen. Wanneer afwezig wordt er
	 *     gecheckt op `$entry->isRoot()`.
	 *
	 * @returns De URL als string.
	 */
	function url($root = null)
	{
		$ret = '/' . implode('/', array_reverse($this->getEntryNames($root)));
		if ($this->isDir() && !$this->isRoot($root))
		{
			$ret .= '/';
		}
		return $ret;
	}

	/**
	 * @brief Geef de URL om deze entry in het adminpaneel van Benamite te openen.
	 *
	 * @return string
	 * De URL als string.
	 */
	function benamiteURL()
	{
		return BENAMITEBASE . '/site_live' . $this->url();
	}

	/**
	 * @brief Check of de URL van deze entry daadwerkelijk bezocht kan worden.
	 *
	 * Entries die ergens een variabele-entry in hun parents hebben zitten,
	 * of dat zelf zijn, hebben niet echt een enkele URL die naar een pagina
	 * leidt. Deze methode checkt dus of dat niet zo is.
	 *
	 * @param vfsEntry|null $root De entry om bij te stoppen. Wanneer afwezig wordt er
	 *     gecheckt op `$entry->isRoot()`.
	 *
	 * @return bool
	 * `true` wanneer de URL van deze entry echt naar een pagina leidt,
	 *     anders `false`.
	 */
	function hasConcreteURL($root = null)
	{
		$isVariable =
			function ($entry) {
				return $entry->getType() == ENTRYVARIABLE;
			};

		return empty(array_filter($this->getParents($root), $isVariable));
	}

	/**
	 * @brief Returnt de URL om deze entry in de Publisher te openen.
	 *
	 * @param vfsEntry|null $root Deze parameter wordt genegeerd.
	 *     TODO: Check of die verwijderd kan worden.
	 *
	 * @return string
	 * De URL als string.
	 */
	function publisherURL($root = null)
	{
		return PUBLISHERBASE . $this->id . '/';
	}

	/**
	 * @brief Returnt de 'echte' publisher-url van een entry.
	 *
	 * Bijna hetzelfde als de functie `vfsEntry::url()`, behalve dat voor
	 * act-urls de variabele entries in de url voor cies en actnaam worden
	 * vervangen door de cies en de actnaam, zodat het makkelijker checken is
	 * of een user die entry mag wijzigen en zodat het voor normale actieve
	 * leden duidelijker is waar ze in zitten te werken.
	 */
	function publisherRealURL($root = null)
	{
		$url = $this->url($root);

		if (strpos($url, '/Activiteiten') === false)
		{
			return $url;
		}

		$strs = explode('/', $url);

		if (sizeof(array_filter($strs)) <= 2 || $strs[3] == '*' || !is_numeric($strs[3]))
		{
			return $url;
		}

		$act = Activiteit::geef($strs[3]);

		if (!$act)
		{
			user_error('Activiteit ' . $strs[3] . ' bestaat niet');
		}

		$url = $act->url();

		if (sizeof($strs) == 5)
		{
			$urls = explode('/', $url);
			return implode('/', array_slice($urls, 0, sizeof($urls) - 2));
		}
		else
		{
			if (sizeof($strs) > 5)
			{
				return $url . implode('/', array_slice($strs, 5, sizeof($strs)));
			}
		}

		return $url;
	}

	/**
	 * @brief Geef de de entries die het pad naar deze entry vormen.
	 *
	 * @param root De entry om bij te stoppen. Wanneer afwezig wordt er
	 *     gecheckt op `$entry->isRoot()`.
	 *
	 * @returns Een array met de entries.
	 */
	function getParents($root = null)
	{
		if ($this->isRoot($root))
		{
			return array();
		}

		$parents = $this->getParent()->getParents($root);
		array_unshift($parents, $this);
		return $parents;
	}

	/**
	 * @brief Geef de namen van de entries die het pad naar deze entry vormen.
	 *
	 * @param root De entry om bij te stoppen. Wanneer afwezig wordt er
	 *     gecheckt op `$entry->isRoot()`.
	 *
	 * @returns Een array met de namen van de entries, in aflopende volgorde:
	 *  /Service/Intern/Publisher geeft bijvoorbeeld `['Publisher', 'Intern', 'Service']`.
	 */
	function getEntryNames($root = null)
	{
		return array_map(
			function ($entry) {
				return $entry->getName();
			},
			$this->getParents($root)
		);
	}

	/***
	 * @brief Verplaats deze entry naar een nieuwe URL.
	 */
	function move($newUrl)
	{
		global $BMDB;

		if ($this->isRoot())
		{
			user_error('niet alles breken!', E_USER_ERROR);
		}

		$newRef = hrefToReference($newUrl, null, null, ROOT_ID);
		$destParent = $newRef['entry'];

		if (!$destParent->isDir())
		{
			user_error('target bestaat al', E_USER_ERROR);
		}

		$newName = vfsSlashCheck(trim($newRef['remaining']));
		if (strpos($newName, '/') !== False)
		{
			user_error('er missen tussen liggende dirEntries', E_USER_ERROR);
		}

		if (empty($newName))
		{
			$newName = $this->getName();
		}

		$newParent = $destParent->getId();
		if ($this->getParentId() == $newParent)
		{    /* das makkelijk */
			$this->setName($newName);
			return;
		}

		if ($this->isDir())
		{    /* niet onder een subdir van zichzelf plaatsen */
			$fromUrl = $this->url(ROOT_ID);
			if (!strncmp($fromUrl, $destParent->url(ROOT_ID), strlen($fromUrl)))
			{
				user_error('er missen tussen liggende dirEntries', E_USER_ERROR);
			}
		}

		if ($destParent->hasChild($newName))
		{
			user_error('target bestaat al', E_USER_ERROR);
		}

		$newRank = $BMDB->q('MAYBEVALUE SELECT MAX(rank)'
				. ' FROM benamite'
				. ' WHERE parent = %i'
				, $newParent) + 1;

		$parent = $this->getParent();
		$parent->unsetChildCache($this->getName());

		$BMDB->q('UPDATE benamite SET name = %s, rank = %i, parent = %i'
			. ' WHERE id = %i'
			, $newName, $newRank, $newParent, $this->getId());
		$BMDB->q('UPDATE benamite SET rank = rank - 1'
			. ' WHERE rank > %i AND parent = %i'
			, $this->getRank(), $this->getParentId());

		$this->setName($newName);
		$this->setRank($newRank);
		$this->parentId = $newParent;
	}

	/****************************************************************************/

	/**
	 * @brief Leg vast dat deze entry opgevraagd is.
	 *
	 * Alleen `vfsEntryFile`, `vfsEntryHook` en `vfsEntryInclude` worden gelogd.
	 *
	 * Er wordt hier gewoon DateTimeLocale gebruikt, dus de precisie is op
	 * de seconde.
	 */
	function logView()
	{
		global $BMDB;

		$now = new DateTimeLocale();

		//$fields = array('benamite_id' => $this->getId(), 'moment' => $now);
		//var_dump($fields); die();

		$result = $BMDB->q(
			'INSERT INTO views (benamite_id, moment) '
			. 'VALUES (%i, %l)'
			, $this->getId()
			, $now
		);
	}

	/**
	 * @brief Geef een veld een nieuwe waarde, en sla het meteen in de DB op.
	 *
	 * @returns De nieuwe waarde.
	 */
	protected function setField($field, $val)
	{
		global $BMDB;
		global $auth;

		$this->modwhen = getDateTime();
		$this->modwho = $auth->getLidNr();

		if ($val === '')
		{
			$val = NULL;
		}

		$BMDB->q('UPDATE benamite SET %S WHERE id = %i'
			, array($field => $val
			, 'wanneer' => $this->modwhen
			, 'wie' => $this->modwho
			)
			, $this->getId());
		return $val;
	}

	/**
	 * @brief Maakt een functie van een string die die functie voorstelt.
	 * Bijvoorbeeld de string 'Klasse::met' wordt geconverteerd naar een methode
	 * 'met' van de klasse 'Klasse' (met kan zowel static als niet-static zijn).
	 *
	 * @param string $func
	 * Een string die de functie representeert, zoals bv 'Klasse::methode'
	 *
	 * @return closure
	 * Een closure die als die uitgevoerd wordt de methode van de klasse uitvoert.
	 *
	 * @throws ReflectionException
	 * Als de methode niet gevonden is, kan een ReflectionException gegooid worden.
	 */
	public static function getMethodFromFuncString($func)
	{
		global $auth;

		//TODO: in PHP 7.1 kunnen de anonieme functies vervangen worden door arrow functies
		if (!$func)
		{
			return function (...$args) {
				return null;
			};
		}

		if (strpos($func, '::') === false)
		{
			return function (...$args) use ($func) {
				return call_user_func_array($func, $args);
			};
		}

		list($class, $method) = explode('::', $func);
		$reflection = new ReflectionMethod($class, $method);
		if ($reflection->isStatic())
		{
			return function (...$args) use ($func) {
				return call_user_func_array($func, $args);
			};
		}

		// TODO: De controllers op deze manier constructen is lelijk,
		// dit moet op een manier mooier.
		$entryNames = vfsVarEntryNames();
		$object = new $class($auth, $entryNames);
		return function (...$args) use ($object, $method) {
			return $object->$method(...$args);
		};
	}

	/**
	 * Geef de data om een menu-item te maken voor deze entry.
	 *
	 * @param bool $hookEnabled Mag een menuhook gebruikt worden voor het item?
	 * Dit is deprecated en alleen nodig voor Bookweb.
	 *
	 * @return array|null
	 * Een associatieve array `['url' => ..., 'displayName' => ...]`,
	 * of null indien er niets te tonen is.
	 */
	public function menuItem($hookEnabled)
	{
		return ['url' => $this->url(), 'displayName' => $this->getDisplayName()];
	}
}
