<?php
// $Id$

use Space\Benamite\NoSpecialFunctionException;

class vfsEntryHook extends vfsEntry
{
	/***
	 *    Constructor, should not be called!
	 *    Instead use vfs::get($id) or vfs::add($fields)
	 */
	function __construct($fields)
	{
		parent::__construct($fields);
	}

	function display()
	{
		global $uriRef;

		$this->logView();

		// Roep alleen de contenthook aan als die bestaat.
		try
		{
			$hookFunc = $this->getHookFunc('content');
		}
		catch (NoSpecialFunctionException $e)
		{
			return responseUitStatusCode(403);
		}
		$calledFunc = $this->getMethodFromFuncString($hookFunc);

		if ($uriRef['remaining'])
		{
			return $calledFunc(explode('/', $uriRef['remaining']));
		}
		else
		{
			return $calledFunc([]);
		}
	}

	/****************************************************************************/

	function getHook()
	{
		return $this->getSpecial();
	}

	function setHook($hook)
	{
		$this->setSpecial($hook);
	}

	/**
	 * Geef de callable voor de hook van het gegeven type.
	 *
	 * @param string $type Het type van de hook.
	 * Is tegenwoordig bijna altijd 'content', maar in Bookweb wordt ook 'menu' gebruikt.
	 *
	 * @return callable
	 *
	 * @throws NoSpecialFunctionException Indien de hook niet bestaat.
	 */
	function getHookFunc($type)
	{
		return $this->getSpecialFunc("_$type");
	}

	/**
	 * Heeft deze entry een hook van het gegeven type?
	 *
	 * Zo ja, dan geeft getHookFunc een callable i.p.v. exceptie.
	 *
	 * @param string $type Het type van de hook.
	 * Is tegenwoordig bijna altijd 'content', maar in Bookweb wordt ook 'menu' gebruikt.
	 *
	 * @return bool
	 */
	function hasHook($type)
	{
		try
		{
			$this->getHookFunc($type);
			return true;
		}
		catch (NoSpecialFunctionException $e)
		{
			return false;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function menuItem($hookEnabled)
	{
		// Check of we eigenlijk wel als haak mogen opereren.
		if (!$hookEnabled)
		{
			return parent::menuItem($hookEnabled);
		}

		try
		{
			$hookFunc = $this->getHookFunc('menu');
			return call_user_func($hookFunc);
		}
		catch (NoSpecialFunctionException $e)
		{
			try
			{
				$hookFunc = $this->getHookFunc('content');
				return parent::menuItem($hookEnabled);
			}
			catch (NoSpecialFunctionException $e)
			{
				return null;
			}
		}
	}
}
