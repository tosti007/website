<?php
// $Id$

/**
 * @brief Entry met statische HTML-tekst in getContent.
 *
 * Deze entries zijn bewerkbaar met Publisher.
 * (In tegenstelling tot vfsEntryUploaded, die niet bewerkbaar zijn.)
 */
class vfsEntryFile extends vfsEntry
{
	var $content;

	/***
	 *	Constructor, should not be called!
	 *	Instead use vfs::get($id) or vfs::add($fields)
	 */
	function __construct($fields)
	{
		parent::__construct($fields);
		$this->content = null;
	}

	function display()
	{
		$this->logView();

		$page = new HTMLPage();

		$page->setLastModified($this->getLastModified());
		$page->setLastModifier($this->getLastModifier());

		if (hasPublisherAuth($this->publisherRealUrl()) && !$page->getEditUrl())
		{
			$page->setEditUrl($this->publisherUrl());
		}

		$page->start($this->getDisplayName());

		$page->add(replace_publisher_tags($this->getContent()));

		return new PageResponse($page);
	}

 /****************************************************************************/

	/***
	 *	$getlang == null		gebruik getLang() of anders nl
	 *  $getlang != null		gebruik $getlang ipv getLang()
	 */
	function getContent($getlang = null)
	{
		global $BMDB;

		$lang = ($getlang ? $getlang : getLang());

		if(!isset($this->content[$getlang])) {
			switch($lang) {
			case 'en':	$field = 'content_eng';	break;
			default:	$field = 'content';		break;
			}
			$this->content[$lang] = $BMDB->q("VALUE SELECT $field FROM benamite "
									. 'WHERE id = %i'
									, $this->getId()
									);
		}
		if(!isset($getlang) && $lang != 'nl' && empty($this->content[$lang])) {
			return "<em>No translation available, dutch content follows.</em><br/>"
				. $this->getContent('nl');
		}
		return $this->content[$lang];
	}
	function setContent($text)
	{
		$text = trim($text);
		if($text == $this->getContent()) {
			return;
		}

		switch(getLang()) {
		case 'en':	$field = 'content_eng';	break;
		case 'nl':	$field = 'content';		break;
		}

		$this->content[getLang()] = $this->setField($field, $text);
	}
}
