<?php
// $Id$

function test_vfs_menu()
{
	if(hasAuth('god')) {
		return array('url' => '/Service/Testaanroep',
			'displayName' => 'VFS Testaanroep');
	}
}

/***
 *	Deze pagina test de algehele functionaliteit van het VFS systeem.
 *
 *	TODO: meer tests...
 */
function test_vfs_content($rest)
{
	global $BMDB;

	if(!empty($rest)) {
		return responseUitStatusCode(404);
	}

	$page = new HTMLPage();
	$page->start('Benamite VFS unit tests');

	assert_options(ASSERT_ACTIVE, true);
	assert_options(ASSERT_WARNING, false);

	ob_start();
	echo "<pre>\n";
	echo "vfs unit tests\n";

	test_vfs_assert('website ontkever-modus', DEBUG, true);
	test_vfs_assert('benamite database available'
		, 'isset($GLOBALS["BMDB"])'
		. ' && is_object($GLOBALS["BMDB"])'
		. ' && method_exists($GLOBALS["BMDB"], "q")'
		, true
		);

	test_vfs_constants();
	test_vfs_checks();
	test_vfs_root();
	test_vfs_create();

	// cleanup
	unset($GLOBALS['vfstest']);

	echo "\ndone";
	echo "</pre>";
	$page->add(ob_get_clean());
	return new PageResponse($page);
}

function test_vfs_assert($descr, $assertion, $bail=false)
{
	if(assert($assertion)) {
		echo '<span class="text-success strongtext">  ok  </span>: ' . $descr . "\n";
		return true;
	}

	echo '<span class="text-danger strongtext">failed</span>: ' . $descr . "\n";
	if(!$bail) {
		return false;
	}

	echo '</pre>';
	return $page;
}

/***
 *	Check that vfs constants are set and sane.
 */
function test_vfs_constants()
{
	echo "\nvfsConstats.php\n";

	test_vfs_assert('ROOT_ID is defined'
		, 'defined("ROOT_ID")'
		);
	test_vfs_assert('ROOT_ID is an positive integer'
		, 'is_integer(ROOT_ID) && ROOT_ID > 0'
		);

	test_vfs_assert('ENTRY* types are defined'
		, 'defined("ENTRY") && defined("ENTRYDIR")'
		. ' && defined("ENTRYFILE") && defined("ENTRYUPLOADEDFILE")'
		);
	test_vfs_assert("ENTRY* types are destinct"
		, 'ENTRY != ENTRYDIR && ENTRY != ENTRYDIR && ENTRY != ENTRYFILE'
		, ' && ENTRY != ENTRYUPLOADEDFILE && ENTRYDIR != ENTRYFILE'
		, ' && ENTRYDIR != ENTRYUPLOADEDFILE'
		, ' && ENTRYFILE != ENTRYUPLOADEDFILE'
		);

	test_vfs_assert('INPUT_ERROR is defined'
		, 'defined("INPUT_ERROR")'
		);
	test_vfs_assert('INPUT_ERROR equals NULL and evaluates to false'
		, 'INPUT_ERROR === NULL && (bool)INPUT_ERROR === false'
		);

	test_vfs_assert('HOOK_TYPES is (non-empty) array'
		, 'isset($GLOBALS["HOOK_TYPES"])'
		. ' && is_array($GLOBALS["HOOK_TYPES"])'
		. ' && count($GLOBALS["HOOK_TYPES"]) > 0'
		);
}

/***
 *	Check that vfs checks work.
 *
 *	TODO: also check the other $fields check functions...
 */
function test_vfs_checks()
{
	echo "\nvfsChecks.php\n";

	test_vfs_assert('trailing slash removal - empty string'
		, 'vfsSlashCheck("") == ""'
		. ' && vfsSlashCheck(NULL) == ""'
		);
	test_vfs_assert('trailing slash removal - just slashes'
		, 'vfsSlashCheck("/") == ""'
		. ' && vfsSlashCheck("/////") == ""'
		);
	test_vfs_assert('trailing slash removal - assorted strings'
		, 'vfsSlashCheck("boot") == "boot"'
		. ' && vfsSlashCheck("aap/") == "aap"'
		. ' && vfsSlashCheck("noot/////") == "noot"'
		. ' && vfsSlashCheck("aap/noot/") == "aap/noot"'
		. ' && vfsSlashCheck("/mies//peer///") == "/mies//peer"'
		);
}

/***
 *	Perform units test to check root node properties.
 */
function test_vfs_root()
{
	echo "\nroot node (".ROOT_ID.") property checks\n";

	test_vfs_assert('exists'
		, '$GLOBALS["vfstest"]["root"] = vfs::get(ROOT_ID)'
		, true
		);
	test_vfs_assert('has ROOT_ID'
		, '$GLOBALS["vfstest"]["root"]->getId() === ROOT_ID'
		);

	test_vfs_assert('instanceof vfsEntry'
		, '$GLOBALS["vfstest"]["root"] instanceof vfsEntry'
		);
	test_vfs_assert('instanceof vfsEntryDir'
		, '$GLOBALS["vfstest"]["root"] instanceof vfsEntryDir'
		. ' && $GLOBALS["vfstest"]["root"]->getType() === ENTRYDIR'
		, true
		);

	test_vfs_assert('no parent'
		, '$GLOBALS["vfstest"]["root"]->getParentId() === NO_PARENT'
		);
	test_vfs_assert('no hook'
		, '$GLOBALS["vfstest"]["root"]->getHook() == NULL'
		);
	test_vfs_assert('no content'
		, '$GLOBALS["BMDB"]->q("MAYBEVALUE SELECT content FROM benamite WHERE id = %i", ROOT_ID) == NULL'
		);

	test_vfs_assert('has child - site_live'
		, '$GLOBALS["vfstest"]["root"]->hasChild("site_live")'
		);
	test_vfs_assert('has child - site_publisher'
		, '$GLOBALS["vfstest"]["root"]->hasChild("site_publisher")'
		);
	test_vfs_assert('has child - act_live'
		, '$GLOBALS["vfstest"]["root"]->hasChild("act_live")'
		);
	test_vfs_assert('has child - act_publisher'
		, '$GLOBALS["vfstest"]["root"]->hasChild("act_publisher")'
		);
}

/***
 *	Test vfs entry creation.
 *
 *	assumes test_vfs_root is already called
 */
function test_vfs_create()
{
	echo "\nentry creation\n";

	test_vfs_assert('"testdir" didn\'t exist (failed => removed)'
		, '$GLOBALS["BMDB"]->q("RETURNAFFECTED DELETE FROM benamite WHERE parent = %i AND name = %s", ROOT_ID, "testdir") == 0'
		);

	$GLOBALS["vfstest"]["newfields"] =
		array('name' => 'testdir'
			, 'parent' => ROOT_ID
			, 'type' => ENTRYDIR
			);

	test_vfs_assert('create vfsEntryDir'
		, '$GLOBALS["vfstest"]["testdir"] = vfs::add($GLOBALS["vfstest"]["newfields"])'
		);
	$GLOBALS["vfstest"]["testdirid"] = $GLOBALS["vfstest"]["testdir"]->getId();
	test_vfs_assert('exists in db'
		, '$GLOBALS["BMDB"]->q("VALUE SELECT COUNT(*) FROM benamite WHERE id = %i", $GLOBALS["vfstest"]["testdirid"])'
		);
	test_vfs_assert('parent relation'
		, '$GLOBALS["vfstest"]["testdir"]->getParentId() == ROOT_ID'
		. ' && $GLOBALS["vfstest"]["root"]->hasChild("testdir")'
		. ' && $GLOBALS["BMDB"]->q("VALUE SELECT parent FROM benamite WHERE id = %i", $GLOBALS["vfstest"]["testdirid"])'
		);
	test_vfs_assert('unique rank'
		, '$GLOBALS["BMDB"]->q("VALUE SELECT COUNT(*) FROM benamite
				WHERE parent = %i AND rank = %i"
				, ROOT_ID, $GLOBALS["vfstest"]["testdir"]->getRank()) == 1'
		);
}
