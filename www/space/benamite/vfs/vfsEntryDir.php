<?php
// $Id$

class vfsEntryDir extends vfsEntryHook
{
	/**
	 * Cache voor het bestaan van child-entries.
	 *
	 * Mapt naam naar false of id van child.
	 *
	 * @var array
	 */
	private $childNames;

	/***
	 *	Constructor, should not be called!
	 *	Instead use vfs::get($id) or vfs::add($fields)
	 */
	public function __construct($fields)
	{
		parent::__construct($fields);
		$this->childNames = [];
	}

	public function display()
	{
		global $INDEX, $uriRef;

		// De display van een /dir staat in het /dir/index.html-bestand.
		// Merk op dat dit de contenthook overridet,
		// maar alleen als de index.html bestaat.
		// (Dit is ook hoe het werkt als we wel url /dir/index.html hebben.)
		if (empty($uriRef['remaining']))
		{
			// Probeer of de index.html bestaat.
			$child = $this->getChild($INDEX);
			if ($child)
			{
				// TODO: dit is wel wat dubbelop met space.php.
				if (!$child->mayAccess())
				{
					return responseUitStatusCode(403);
				}
				// TODO: moeten we nog andere globale variabelen updaten?
				$uriRef['entry'] = $child;

				return $child->display();
			}
		}

		// Roep de contenthook aan als het kind niet gevonden is,
		// inclusief als index.html niet bestaat.
		if ($this->hasHook('content'))
		{
			// Roep de hook aan via inheritance.
			return parent::display();
		}

		// Kind nodig maar niet gevonden, dus geef 404.
		return responseUitStatusCode(404);
	}

	/****************************************************************************/

	/***
	 *	Retourneer een (ordende) lijst van kinderen. Parameters:
	 *   - getVisible: alleen non-hidden entries tonen
	 *   - orderby: sortering van restultaat (enkel "name" en "rank" zijn mogelijk)
	 */
	public function getChildrenIds($getVisible = false, $orderby = "rank")
	{
		global $BMDB;
		if ($orderby != "rank" && $orderby != "name")
		{
			$orderby = "rank";
		}
		// default naar rank

		$ids = $BMDB->q('COLUMN SELECT id FROM benamite'
			. ' WHERE parent = %i'
			. ($getVisible ? ' AND visible = 1' : '')
			. " ORDER BY $orderby"
			, $this->getId()
		);
		vfs::getMulti($ids);

		if ($getVisible)
		{
			return $ids;
		}

		// zorg dat de vfsEntryVariable altijd vooraan staat
		$eVar = $this->getChild('*');
		if ($eVar)
		{
			$k = array_search($eVar->getId(), $ids);
			unset($ids[$k]);
			array_unshift($ids, $eVar->getId());
		}

		return $ids;
	}

	/**
	 * Geef het child-entry met gegeven naam, of null.
	 *
	 * Er wordt veel gecached, dus deze functie is redelijk goedkoop.
	 *
	 * @see vfsEntryDir::hasChild
	 *
	 * @param string $name
	 * De (hoofdletterongevoelige) naam van de child-entry.
	 * @return null|vfsEntry
	 */
	public function getChild($name)
	{
		global $BMDB;
		counter_mark(__FUNCTION__);
		if ($this->hasChild($name))
		{
			// mb_strtolower behoudt μ (griekse mu) terwijl strtolower dat niet
			// doet!
			return vfs::get($this->childNames[mb_strtolower($name)]);
		}
		return false;
	}

	/**
	 * Geef of er een childentry bestaat met deze naam.
	 *
	 * Vanwege roundtrips en n+1-querypatronen is de functie getChild vaak efficienter.
	 *
	 * @see vfsEntryDir::getChild
	 *
	 * @param string $name
	 * De (hoofdletterongevoelige) naam van de child-entry.
	 * @return bool
	 */
	public function hasChild($name)
	{
		global $BMDB;
		$name = mb_strtolower($name);
		if (!isset($this->childNames[$name]))
		{
			$id = $BMDB->q('MAYBEVALUE SELECT id FROM benamite'
				. ' WHERE parent = %i AND name = %s'
				, $this->getId()
				, $name
			);
			if (!$id)
			{
				$this->childNames[$name] = false;
			}
			else
			{
				$this->childNames[$name] = $id;
			}
		}
		return (boolean) $this->childNames[$name];
	}
	public function removeChild($child)
	{
		$this->unsetChildCache($child->getName());
		gooiCacheEntrysWeg();
	}

	/***
	 *	zo kunnen we met de kindjes schuiven
	 *
	 *	Up is wisselen met de prev
	 *	Down is wisselen met de next
	 */
	public function moveChildUp($child)
	{
		$this->moveChild($child, -1);
	}
	public function moveChildDown($child)
	{
		$this->moveChild($child, 1);
	}
	private function moveChild($child, $dir)
	{
		global $BMDB;

		$childRank = $child->getRank();

//		$BMDB->q('BEGIN');
		$other = $BMDB->q('MAYBETUPLE SELECT id, rank'
			. ' FROM benamite'
			. ' WHERE parent = %i'
			. ' AND rank ' . ($dir < 0 ? '<' : '>') . ' %i'
			. ' ORDER BY rank ' . ($dir < 0 ? 'DESC' : 'ASC')
			. ' LIMIT 0, 1'
			, $this->getId()
			, $childRank
		);
		if (!$other)
		{
//			$BMDB->q('COMMIT');
			return;
		}

		$BMDB->q('UPDATE benamite SET rank = %i WHERE id = %i'
			, $other['rank']
			, $child->getId()
		);
		$child->setRank($other['rank']);

		$BMDB->q('UPDATE benamite SET rank = %i WHERE id = %i'
			, $childRank
			, $other['id']
		);
		$otherChild = vfs::get($other['id']);
		$otherChild->setRank($childRank);
		gooiCacheEntrysWeg();

//		$BMDB->q('COMMIT');
	}

	/***
	 *	Verwijder het kindje uit de cache.
	 */
	public function unsetChildCache($name)
	{
		unset($this->childNames[mb_strtolower($name)]);
	}
}
