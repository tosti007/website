<?php
// $Id$

// De locatie waar alle geüploade bestanden terechtkomen.
define('BENAMITE_FILES', '/srv/http/www/benamite_files/');

class vfsEntryUploaded extends vfsEntry
{
	/***
	 *	Constructor, should not be called!
	 *	Instead use vfs::get($id) or vfs::add($fields)
	 */
	function __construct($fields)
	{
		parent::__construct($fields);
	}

	function display()
	{
		return sendfilefromfs($this->getFile(), NULL, BENAMITE_FILES);
	}

 /****************************************************************************/

	function getFile()
	{
		$filename = substr($this->getSpecial(), 0, 32);
		$path = BENAMITE_FILES . $filename;
		if (DEBUG && file_exists($path . '.debug')) {
			return $filename . '.debug';
		}

		return $filename;
	}

	function getFileAbsolute()
	{
		// FIXME: maak dit meer DRY
		$filename = substr($this->getSpecial(), 0, 32);
		$path = BENAMITE_FILES . $filename;
		if (DEBUG && file_exists($path . '.debug')) {
			return $path . '.debug';
		}

		return $path;
	}

	function getMime()
	{
		return substr($this->getSpecial(), 33);
	}
}
