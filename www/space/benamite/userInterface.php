<?php

function benamiteUI_content($rest)
{
	global $request, $uriRef;

	$ref = benamiteHrefToReference($uriRef['remaining'] , vfs::get(ROOT_ID), true);

	/** @var vfsEntry $entry */
	$entry = $ref['entry'];
	if(!$entry || !empty($ref['remaining'])) {
		return responseUitStatusCode(404);
	}

	$benamite = $uriRef['entry'];
	$bUrl = $benamite->url();
	$eUrl = $entry->url(ROOT_ID, true);

	if($ref['rewrite']) {
		return responseRedirect($bUrl.$eUrl);
	}

	/* handel post acties af */
	BenamiteUserInterface::uiParams($entry);

	if($request->request->get("zoekveld")) {
		$page = BenamiteUserInterface::uiZoekResultaten($benamite, $entry, $request->request->get("zoekveld"), $bUrl);
		return new PageResponse($page);
	}

	if (!$entry->isDir()) {
		/* toon de pagina */
		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('benamite.css', 'css'));
		$page->start($benamite->getDisplayName()." > ".$entry->getDisplayName());

		/* Zoeken in benamite */
		$page->add(BenamiteUserInterface::zoekFormulier());
		$page->add(BenamiteUserInterface::breadcrumbs($eUrl, $bUrl));

		/* Het is geen directory : toon wat info over de entry */
		$page->add(BenamiteUserInterface::showInfo($entry));
		return new PageResponse($page);
	}

	// Op deze plek is de entry dus een vfsEntryDir
	/** @var vfsEntryDir $entry */

	// Kijk of er commissiepagina's zijn die onzichtbaar gemaakt worden
	$hasciepaginas = false;
	$children = $entry->getChildrenIds();
	foreach ($children as $childId) {
		$child = vfs::get($childId);
		$name = $child->getName();

		if(Commissie::cieByLogin($name) && $child->isHook()
				&& $child->getHook() == '/space/benamite/hooks/commissies.php:commissiewsw') {
			$hasciepaginas = true;
			break;
		}
	}

	if($hasciepaginas) {
		$waarschuwing = tryPar('commissiepaginaszichtbaar', 0) == 0
			? 'Deze map heeft commissiepagina\'s die hier niet in het overzicht staan! Om ze wel zichtbaar te maken, klik <a href="?commissiepaginaszichtbaar=1">hier</a>'
			: 'Deze map heeft commissiepagina\'s die weer onzichtbaar gemaakt kunnen worden. Klik daarvoor <a href="?commissiepaginaszichtbaar=0">hier</a>';
		Page::addMelding($waarschuwing, 'waarschuwing');
	}

	/* toon de pagina */
	$page = new HTMLPage();
	$page->addHeadCSS(Page::minifiedFile('benamite.css', 'css'));
	$page->start($benamite->getDisplayName()." > ".$entry->getDisplayName());

	/* Zoeken in benamite */
	$page->add(BenamiteUserInterface::zoekFormulier());
	$page->add(BenamiteUserInterface::breadcrumbs($eUrl, $bUrl));

	$page->add($form = new HtmlForm('post', $uriRef['request']));
	$form->setAttribute('enctype', 'multipart/form-data');

	$form->add(new HtmlHeader(2, _('Bestanden op de website')));
	$form->add(new HtmlTable(array(
			new HtmlTableHead($benamiteHead = new HtmlTableRow()),
			$benamiteBody = new HtmlTableBody()
	), 'benamite-table'));

	$addHeader = function($content, $width) use (&$benamiteHead) {
		$th = new HtmlTableHeaderCell($content);
		$th->setCssStyle("width: " . $width . "px");
		$benamiteHead->add($th);
	};

	$addHeader("&nbsp;", 25);
	$addHeader("&nbsp;", 25);
	$addHeader(_('Bestandsnaam'), 140);
	$addHeader(_('Pagina Titel'), 180);
	$benamiteHead->add($zichtbaar = new HtmlTableHeaderCell(_('Zichtbaar')));
	$zichtbaar->setAttributes(array(
		"colspan" => 2,
		"style" => 'text-align: right;'
	));
	$addHeader(_('Min. Auth'), 80);
	// gap;
	$addHeader("&nbsp;", 20);
	$addHeader("&nbsp;", 20);
	$addHeader("&nbsp;", 20);
	$authLevelArray = array('', 'god', 'bestuur', 'boekcom'
						, 'actief', 'lid', 'oud-lid', 'ingelogd', 'bijnalid'
						, 'intro', 'spocie', 'tbc', 'vicie', 'spookweb'
						, 'kasCom', 'mailings');

	// rijtje kinderen dat aangepast kan worden
	$nr = 0;
	foreach ($children as $childId) {
		$child = vfs::get($childId);
		$name = $child->getName();

		if(tryPar('commissiepaginaszichtbaar', 0) == 0 && Commissie::cieByLogin($name) && $child->isHook()
				&& $child->getHook() == '/space/benamite/hooks/commissies.php:commissiewsw') {
			continue;
		}

		$icon = BenamiteUserInterface::getIcon($child->getType());

		$benamiteBody->add($row = new HtmlTableRow(array(
			$fieldGoTo = new HtmlTableDataCell(null, "borderless"),
			$fieldGoIn = new HtmlTableDataCell(null, "borderless"),
			$fieldName = new HtmlTableDataCell(),
			$fieldTitle = new HtmlTableDataCell(),
			$fieldExtra = new HtmlTableDataCell(),
			$fieldVisible = new HtmlTableDataCell(),
			$fieldMinAuth = new HtmlTableDataCell(),
			$fieldMoveUp = new HtmlTableDataCell(null, "borderless"),
			$fieldMoveDown = new HtmlTableDataCell(null, "borderless"),
			$fieldDelete = new HtmlTableDataCell(null, "borderless"),
		), "entry"));

		$fieldGoTo->setAttribute('align', 'center');
		$fieldGoIn->setAttribute('align', 'center');
		$fieldVisible->setAttribute('align', 'center');
		$fieldVisible->setCssStyle('width: 25px;');
		$fieldMoveUp->setAttribute('align', 'right');
		$fieldMoveDown->setAttribute('align', 'left');

		if($child->hasConcreteUrl()) {
			$fieldGoTo->add($link = new HtmlAnchor(
				$child->url(), HtmlSpan::fa('external-link', 'Direct naar ' . $child->url())
			));
			$link->setAttribute('title', 'Direct naar ' . $child->url());
		}

		$fieldGoIn->add(array(
				addHidden("id[$nr]", $childId),
				addHidden("type[$nr]", $child->getType()),
				new HtmlAnchor($bUrl . $child->url(ROOT_ID, true), $icon)
		));
		$fieldName->add("<input style=\"width: 100%\" type=\"text\" name=\"name[$nr]\" value=\"$name\">");
		$fieldTitle->add("<input style=\"width: 100%\" type=\"text\""
				. " name=\"displayName[$nr]\""
				. " value=\"".$child->getDisplayName()."\">"
		);
		switch($child->getType()) {
		case ENTRYDIR:
		case ENTRYHOOK:
		case ENTRYINCLUDE:
		case ENTRYLINK:
		case ENTRYVARIABLE:
			$fieldExtra->add("<input style=\"width: 100%\" type=\"text\" name=\"special[$nr]\""
					. " value=\"".$child->getSpecial()."\">");
			break;
		case ENTRYUPLOADED:
			$fieldExtra->add("&nbsp;" . $child->getMime());
			break;
		default:
			$fieldExtra->add("&nbsp;");
		}

		$input = HtmlInput::makeCheckbox("visible[$nr]", $child->isVisible());
		// Variabele entries horen niet zichtbaar in het menu te zijn,
		// omdat ze niet "echt" bestaan onder de gegeven name.
		// We voorkomen dat ze toch zichtbaar zijn door de checkbox te disablen.
		if ($child->getType() == ENTRYVARIABLE)
		{
			// Disablen gaat door een attribuut disabled toe te voegen
			// en geeft niet om de waarde van dit attribuut.
			// TODO: kan dit een methode voor HtmlInput worden om bugs te voorkomen?
			$input->setAttribute('disabled', null);
		}
		$fieldVisible->add($input);
		$fieldMinAuth->add(HtmlSelectbox::fromSingleDimensionArray("auth[$nr]", $authLevelArray, $child->getAuth()));

		if ($nr != 0) {
			$fieldMoveUp->add(
				'<input type="image" src="/Layout/Images/Icons/sort-ASC.png"'
				. " name=\"moveUP[$nr]\" title=\"" . _('Omhoog') . '"'
				. ' value="$nr" />'
			);
		}
		if ($nr != count($children) - 1) {
			$fieldMoveDown->add('<input type="image" src="/Layout/Images/Icons/sort-DESC.png"'
				. " name=\"moveDOWN[$nr]\" title=\"" . _('Omlaag') . '"'
				. " value=\"$nr\" />");
		}
		$fieldDelete->add("<input type=\"image\" src=\"/Layout/Images/Buttons/delete.png\""
				. " name=\"delete[$nr]\" title=\""._('Verwijder entry')."\""
				. " value=\"$nr\" data-confirmation = '"
				. sprintf(_("Weet je zeker dat je de entry \"%s\" wilt verwijderen?"), $name) . "' />");
		$nr++;
	}

	$form->add($submitBar = new HtmlDiv(new HtmlDiv(
		$nr > 0 ? HtmlInput::makeSubmitButton('Sla wijzigingen op', 'edit') : _('Er zijn geen entries'),
		'col-sm-12'), 'form-group'));
	$submitBar->setCssStyle('text-align: center; margin-top: 1em;');

	$form->add(new HtmlHeader(2, _('Nieuwe entry aanmaken')));

	$options = array();
	foreach (BenamiteUserInterface::$ENTRY_ADDABLE as $entry) {
		$options[$entry] = BenamiteUserInterface::$ENTRY_NAMES[$entry];
	}

	$form->add(new HtmlDiv(array(
		new HtmlDiv(array(
			new HtmlLabel("type[$nr]", _('Soort'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlSelectbox::fromArray("type[$nr]", $options, ENTRYINCLUDE), 'col-sm-3'),
			$zichtbaarLabel = new HtmlLabel(null, array(HtmlInput::makeCheckbox("visible[$nr]"), _('Zichtbaar')), 'col-sm-2 control-label'),
			new HtmlLabel("auth[$nr]", _('Minimale authorisatie'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlSelectbox::fromArray("auth[$nr]", array_combine($authLevelArray, $authLevelArray)), 'col-sm-2')
		), 'form-group'),
		new HtmlDiv(array(
			new HtmlLabel("name[$nr]", _('Naam'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText("name[$nr]", null, null, "form-control", null, _('Naam')), 'col-sm-10')
		), 'form-group'),
		new HtmlDiv(array(
			new HtmlLabel("displayName[$nr]", _('Pagina titel'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText("displayName[$nr]", null, null, "form-control", null, _('Pagina titel')), 'col-sm-10')
		), 'form-group'),
		new HtmlDiv(array(
			new HtmlLabel("special[$nr]", _('Speciaal veld'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText("special[$nr]", null, null, "form-control", null, _('Speciaal veld')), 'col-sm-10')
		), 'form-group'),
		$submitBar = new HtmlDiv(
			new HtmlDiv(HtmlInput::makeSubmitButton(_('Aanmaken'), 'add'), 'col-sm-12')
		, 'form-group'),
	), 'form-horizontal'));
	$zichtbaarLabel->setCssStyle('text-align: center;');
	$submitBar->setCssStyle('text-align: center');
	$submitBar->setCssStyle('text-align: center');

	$form->add(HtmlInput::makeHidden('rowCount', $nr));

	$form->add(new HtmlHeader(2, _('Verplaats entry')));
	$form->add($verplaatsForm = new HtmlDiv(array(
		new HtmlDiv(array(
			new HtmlLabel('fromUrl', _('Van'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText('fromUrl', $eUrl, null, 'form-control'), 'col-sm-10', 'Huidige pad')
		), 'form-group'),
		new HtmlDiv(array(
			new HtmlLabel('destUrl', _('Naar'), 'col-sm-2 control-label'),
			new HtmlDiv(HtmlInput::makeText('destUrl', $eUrl, null, 'form-control'), 'col-sm-10', null, 'Nieuwe pad')
		), 'form-group'),
		$submitBar = new HtmlDiv(
			new HtmlDiv(HtmlInput::makeSubmitButton(_('Verplaats'), 'move', 'btn btn-default'), 'col-sm-12')
		, 'form-group')
	), 'form-horizontal'));
	$submitBar->setCssStyle('text-align: center');

	$page->add(new HtmlHeader(2, _('Legenda')));
	$page->add(new HtmlTable(array(
			new HtmlTableHead($legendaHead = new HtmlTableRow()),
			$legendaBody = new HtmlTableBody()
	)));
	$legendaHead->add(array(
		new HtmlTableHeaderCell(_('Soort')),
		new HtmlTableHeaderCell(_('Wat is het')),
		new HtmlTableHeaderCell(_('Werking speciaal veld')),
	));

	$watishet = array(
		ENTRYDIR	=> 'Bevat andere bestanden',
		ENTRYFILE	=> 'Kun je bewerken met publisher',
		ENTRYUPLOADED	=> 'Dit is een geüpload bestand',
		ENTRYINCLUDE	=> 'Levert een bestand direct van het bestandsysteem (FS).',
		ENTRYLINK	=> 'Verwijst naar elders',
		ENTRYHOOK	=> 'Voert een specifieke functie uit',
		ENTRYVARIABLE	=> 'Verwijst naar een DB-object om dingen mee te doen'
	);
	$werkingSpeciaal = array(
		ENTRYDIR	=> 'Zie PHP Haak',
		ENTRYFILE	=> 'n.v.t.',
		ENTRYUPLOADED	=> 'n.v.t.',
		ENTRYINCLUDE	=> '/path/bestand[[:class]:functie]',
		ENTRYLINK	=> '/url/nieuwe/locatie',
		ENTRYHOOK	=> '/path/bestand[:class]:functie',
		ENTRYVARIABLE	=> '/path/bestand[:class]:functie'
	);

	foreach (BenamiteUserInterface::$ENTRY_NAMES as $entry => $name) {
		$legendaBody->add($tr = new HtmlTableRow(array(
			new HtmlTableDataCell(BenamiteUserInterface::getIcon($entry)),
			new HtmlTableDataCell($name),
			new HtmlTableDataCell($watishet[$entry]),
			new HtmlTableDataCell($werkingSpeciaal[$entry])
		)));
	}
	return new PageResponse($page);
}


class BenamiteUserInterface {

	public static $ENTRY_NAMES = array(
		ENTRYDIR	=> 'Map',
		ENTRYFILE	=> 'Bestand (publisher)',
		ENTRYUPLOADED	=> "Bestand (geüpload)",
		ENTRYINCLUDE	=> 'Bestand (op fs)',
		ENTRYLINK	=> 'Link',
		ENTRYHOOK	=> 'PHP Haak',
		ENTRYVARIABLE	=> 'Variable entry'
	),
	$ENTRY_ADDABLE = array(ENTRYDIR, ENTRYINCLUDE, ENTRYLINK, ENTRYHOOK, ENTRYVARIABLE),
	$ICONS = array(
		ENTRYDIR => 'folder-o',
		ENTRYFILE => 'file-o',
		ENTRYUPLOADED => 'cloud-upload',
		ENTRYINCLUDE => 'database',
		ENTRYLINK => 'link',
		ENTRYHOOK => 'file-code-o',
		ENTRYVARIABLE => 'sitemap'
	);

	public static function getIcon($entry) {
		if (array_key_exists($entry, self::$ENTRY_NAMES)) {
			return HtmlSpan::fa(self::$ICONS[$entry], _(self::$ENTRY_NAMES[$entry]));
		} else {
			return HtmlSpan::fa('question-circle-o', _('Bestand van onbekend type'));
		}
	}

	public static function breadcrumbs($entry_url, $base_url) {
		$subs = explode('/', $entry_url);
		$elem = array_pop($subs);
		$crumbs = '/';

		$items = array();
		$addItem = function($item, $active = false) use (&$items) {
			$item = new HtmlListItem($item, 'breadcrumb-item' . ($active ? ' active' : ''));
			$item->setNoDefaultClasses();
			$items[] = $item;
		};

		foreach($subs as $sub) {
			if ($sub) {
				$crumbs .= "$sub/";
			} else {
				$sub = 'root';
			}
			$addItem(new HtmlAnchor($base_url . $crumbs, $sub));
		}
		if($elem) {
			$addItem($elem);
		}
		$list = new HtmlList(true, $items, 'breadcrumb');
		$list->setNoDefaultClasses();
		return $list;
	}

	public static function showInfo($entry) {
		$ret = "<p>\n"
			. '<strong>' . _("Paginatitel").":</strong>&nbsp;".htmlspecialchars($entry->getDisplayName())
			. "<br/>\n"
			. '<strong>' . _('Soort') . ':</strong>&nbsp;';
		if (array_key_exists($entry->getType(), self::$ENTRY_NAMES)) $ret .= _(self::$ENTRY_NAMES[$entry->getType()]);
		else $ret .= "???";

		$ret .= "<br/>\n";
		$visible = $entry->isVisible() ? _('ja') : _('nee');
		$ret .= '<strong>' . _('Zichtbaar') . ':</strong>&nbsp;' . $visible;
		if ($special = $entry->getSpecial())
		{
			$ret .= "<br/>\n";
			$ret .= '<strong>' . _('Speciaal veld') . ':</strong>&nbsp;' . $special;
		}
		$ret .= "<br/>\n";
		$ret .= '<strong>' . _('Minimale authenticatie') . ':</strong>&nbsp;' . ($entry->getAuth() ?: '<em>geen</em>');
		$ret .= "<br/>\n";
		if(!in_array('*', $entry->getEntryNames()))
			$ret .= '<strong>' . sprintf(_("Link naar %slive%s")
						, '<a href="'.$entry->url().'">'
						, '</a>'
						) . '</strong>';
		if($entry->getType() == ENTRYFILE) {
			$ret .= ' <a href="/Service/Intern/Publisher'.$entry->url().'">'
					. _("publisher")
					. '</a>';
		}
		$ret .= "</p>\n";

		$wie = Persoon::geef($entry->getLastModifier());
		$ret .= sprintf(_("Aangepast op %s door %s")
				, htmlspecialchars($entry->getLastModified())
				, ($wie ? PersoonView::naam($wie) : _("Onbekend"))
				);
		return $ret;
	}

	public static function zoekFormulier() {
		$form = new HtmlForm("POST");
		$form->addClass('form-inline benamite-zoek');
		$form->addChildren(array(
			// Hidden om ervoor te zorgen dat we de cache niet wegmieteren als we zoeken
			HtmlInput::makeHidden('gooiCacheNietWeg', 'true'),
			new HtmlEmphasis(_("Zoeken in benamite op functie-aanroep") . ':'),
			new HtmlDiv(array(
				HtmlInput::makeText("zoekveld"),
				new HtmlDiv(new HtmlButton('submit', HtmlSpan::fa('search', _('Zoek'))), 'input-group-btn')
			), 'input-group')
		));
		return $form;
	}

	/**
	 * @brief Geef de pagina voor zoekresultaten.
	 *
	 * @return Een HTMLPage.
	 */
	public static function uiZoekResultaten($benamite, $entry, $str, $bUrl) {
		global $BMDB;

		$page = new HTMLPage();
		$page->addHeadCSS(Page::minifiedFile('benamite.css', 'css'));
		$page->start($benamite->getDisplayName()." > ".$entry->getDisplayName());

		/* Zoeken in benamite */
		$page->add(BenamiteUserInterface::zoekFormulier());

		$result = $BMDB->q("COLUMN SELECT `id` FROM `benamite` WHERE `special` LIKE %c", $str);
		if (count($result) == 0) {
			$page->add(new HtmlSpan(_("Er zijn geen resultaten gevonden!"), 'text-danger strongtext'));
		}

		foreach($result as $id) {
			$entry = vfs::get($id);
			if (!$entry) {
				continue;
			}
			$eUrl = $entry->url(ROOT_ID, true);

			$page->add(array(
				self::breadcrumbs($eUrl, $bUrl),
				self::showInfo($entry),
				new HtmlHR()
			));
		}
		return $page;
	}

	/***
	 *	Handel de post acties van de UI af.
	 *	TODO: vriendelijke error meldingen...
	 */
	public static function uiParams($parent)
	{
		global $request;

		// Als je een file upload boven de limiet, is $_POST leeg, maar
		// $_SERVER[REQUEST_METHOD] is wel POST
		if ($request->server->get('REQUEST_METHOD') == 'POST' && $request->request->count() == 0) {
			user_error(_("Je hebt een file geupload die te groot was.
				Neem contact op met de webcie als we de limiet moeten
				verhogen, of bekijk of het niet kleiner kan."), E_USER_ERROR);
		}

		//Als er een actie vereist is:
		//Gooi de left-menu cache entries weg, want we gaan mogelijkerwijs dit menu in de war gooien
		if($request->request->count() != 0 && !$request->request->get('gooiCacheNietWeg')) {
			gooiCacheEntrysWeg();
		}

		$parentId = $parent->getId();

		// image submitbutton values opvragen
		// image submits kunnen geen value hebben. Daarom is het een array met als
		// eerste key de value die we nodig hebben.
		$moveUP   = (count($ar = array_keys(tryPar('moveUP', array())))
					? array_pop($ar) : null);
		$moveDOWN = (count($ar = array_keys(tryPar('moveDOWN', array())))
					? array_pop($ar) : null);
		$delete = (count($ar = array_keys(tryPar('delete', array())))
					? array_pop($ar) : null);

		if (isset($moveUP) || isset($moveDOWN) || isset($delete)) {
			$arr_id = requirePar('id');

			if(isset($moveUP)) {
				$move = 'up';
				$i = $moveUP;
			} elseif(isset($moveDOWN)) {
				$move = 'down';
				$i = $moveDOWN;
			} else {
				$move = '';
				$i = $delete;
			}
			$entry = vfs::get($arr_id[$i]);
			if(!$entry) {
				user_error('niet hakken 1!', E_USER_ERROR);
			}
			if($entry->getParentId() != $parentId) {
				user_error('niet hakken 2! dat is stout!', E_USER_ERROR);
			}

			switch($move) {
			case 'up':      $parent->moveChildUp($entry);   break;
			case 'down':    $parent->moveChildDown($entry); break;
			case '':        vfs::del($entry);               break;
			}
			return;
		}

		if(tryPar('move')) {
			$fromUrl = requirePar('fromUrl');
			$destUrl = requirePar('destUrl');

			$from = hrefToEntry($fromUrl, null, ROOT_ID);
			if(!$from) return;

			$from->move($destUrl);
			return;
		}

		$rowCount = tryPar('rowCount');

		$arr_id             = tryPar('id');
		$arr_type           = tryPar('type');
		$arr_name           = tryPar('name');
		$arr_displayName    = tryPar('displayName');
		$arr_special        = tryPar('special');
		$arr_visible        = tryPar('visible');
		$arr_auth           = tryPar('auth');

		if(tryPar('add')) {
			$fields['type']         = (int)$arr_type[$rowCount];
			$fields['name']         = $arr_name[$rowCount];
			$fields['displayName']  = @$arr_displayName[$rowCount];
			$fields['parent']       = $parentId;
			$fields['special']      = @$arr_special[$rowCount];
			$fields['visible']      = (boolean)@$arr_visible[$rowCount];
			$fields['auth']         = @$arr_auth[$rowCount];

			vfs::add($fields);
			return;
		}

		if(!$rowCount) {
			return;
		}

		for ($i = 0; $i < $rowCount; $i++) {
			$entry = vfs::get($arr_id[$i]);

			// this vfsEntry MAY have changed
			if(!$entry) {
				user_error('niet hakken 3!', E_USER_ERROR);
			}
			if($parentId != $entry->getParentId()) {
				user_error('niet hakken 4! dat is stout!', E_USER_ERROR);
			}

			$entry->setVisible(@$arr_visible[$i]);
			$entry->setDisplayName(@$arr_displayName[$i]);

			if($entry->getName() !== @$arr_name[$i] && $entry->getParent()->hasChild(@$arr_name[$i])) {
				Page::addMelding(sprintf(_('Er is al een bestand met de naam %s. Je mag de naam van %s dus niet hierin wijzigen.'), $entry->getName(), @$arr_name[$i]), 'fout');
				Page::addMelding(sprintf(_('Als je de namen van twee bestanden wilt verwisselen moet je dus eerst de naam van het ene bestands in iets anders veranderen voordat je ze kan omwisselen (welkom op het zeer geavanceerde benamite!).')), 'fout');
			}

			$entry->setName(@$arr_name[$i]);
			$entry->setAuth(@$arr_auth[$i]);

			switch($entry->getType()) {
			case ENTRYDIR:
			case ENTRYHOOK:
			case ENTRYINCLUDE:
			case ENTRYLINK:
			case ENTRYVARIABLE:
				$entry->setSpecial(@$arr_special[$i]);
				break;
			}
		}
	}
}

