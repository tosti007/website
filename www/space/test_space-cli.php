<?php

namespace Space;

/**
 * Een nuttige functie om te testen dat space-cli werkt.
 */
function rooktest($stdin)
{
	return 'test';
}

/**
 * Klasse met methoden die we willen aanroepen om vreemde functies te testen.
 */
class CLITest
{
	public static function testStatisch($stdin)
	{
		return 'test statisch';
	}
}
