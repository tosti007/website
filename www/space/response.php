<?php

use Symfony\Component\HttpFoundation\Response;

/**
 * @brief Een Response-klasse die een Page als content heeft i.p.v. een string.
 *
 * Werkt verder hetzelfde als een Symfony-response.
 */
class PageResponse extends Response {
	protected $page = null;

	public function __construct(Page $page, $status = 200, $headers = array()) {
		parent::__construct('', $status, $headers);
		$this->page = $page;
	}

	/**
	 * @brief Verstuur de headers en inhoud van deze response.
	 *
	 * Zorgt in vergelijking met Response::send voornamelijk
	 * ervoor dat de Page goed gerenderd wordt.
	 */
	public function send() {
		parent::setContent($this->page->respond());
		$this->headers->set("Content-type", $this->page->contentType());
		return parent::send();
	}
}

/**
 * @brief Een Response-klasse die CSV-data als content heeft i.p.v. een string.
 *
 * Werkt verder hetzelfde als een Symfony-response.
 */
class CSVResponse extends Response {
	protected $data = null;
	protected $filename = "";

	/**
	 * @brief Maak een response die gevuld is met de gegeven data.
	 *
	 * @param data Een array van arrays, met de begindata.
	 * @param filename Indien geset, wordt de file gedownload onder deze naam.
	 * @param status De HTTP-statuscode van deze response (bijvoorbeeld 200).
	 * @param headers Een array van extra HTTP-headers.
	 */
	public function __construct($data, $filename = "", $status = 200, $headers = array()) {
		parent::__construct('', $status, $headers);
		$this->data = $data;
		$this->filename = $filename;
	}

	/**
	 * @brief Voeg een rij aan data toe aan de CSV-pagina.
	 *
	 * @param row De data om op te schrijven. Zie ook $velden.
	 * @param velden Indien niet NULL, een array van keys in $row,
	 * die aangeeft welke velden in welke volgorde moeten staan.
	 *
	 * @see fputcsv
	 */
	public function addRow($row, $velden = NULL) {
		if (is_null($velden))
		{
			$output = $row;
		}
		else
		{
			$output = array();
			foreach ($velden as $veld) {
				$output[] = $row[$veld];
			}
		}
		$this->data[] = $output;
	}

	/**
	 * @brief Verstuur de headers en inhoud van deze response.
	 *
	 * Zorgt in vergelijking met Response::send voornamelijk
	 * ervoor dat de data goed als CSV ge-encodeerd wordt.
	 */
	public function send() {
		// PHP heeft geen functie voor array->csv-string,
		// dus we moeten een tijdelijk bestand maken (soepmes, soepmes!)
		$contents = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
		foreach ($this->data as $row) {
			fputcsv($contents, $row);
		}
		rewind($contents);
		parent::setContent(stream_get_contents($contents));

		if ($this->filename)
		{
			$this->headers->set("Content-disposition",
				'attachment; filename=' . $this->filename
			);
		}
		$this->headers->set("Content-type", 'text/csv; encoding=utf-8');
		$this->headers->set("Content-disposition", 'attachment; filename="' . $this->filename . '"');
		return parent::send();
	}

	/**
	 * Geef de data van deze response als array.
	 *
	 * (Deze functie is vooral bedoeld voor tests, gelieve geen leipe poep hiermee te doen!)
	 *
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
}

/**
 * @brief Een Response-klasse die een JSON-object als content heeft i.p.v. een string.
 *
 * Werkt verder hetzelfde als een Symfony-response.
 */
class JSONResponse extends Response {
	protected $data = null;

	/**
	 * @brief Maak een response door de data te JSON-encoderen.
	 *
	 * @param data Iets wat door json_encode geaccepteerd wordt.
	 */
	public function __construct($data, $status = 200, $headers = array()) {
		parent::__construct('', $status, $headers);
		$this->data = $data;
	}

	/**
	 * @brief Verstuur de headers en inhoud van deze response.
	 *
	 * Zorgt in vergelijking met Response::send voornamelijk
	 * ervoor dat de data goed als JSON ge-encodeerd wordt.
	 */
	public function send() {
		parent::setContent(json_encode($this->data));
		$this->headers->set("Content-type", 'application/json');
		return parent::send();
	}
}

/**
 * @brief Een Response-klasse die een TeX-bestand downloadt.
 *
 * Werkt verder hetzelfde als een Symfony-response.
 */
class TeXResponse extends Response {
	protected $filenaam = 'aes2.tex';

	/**
	 * @brief Maak een response met gegeven inhoud.
	 *
	 * @param filenaam De naam van het TeX-bestand dat gedownload wordt.
	 * @param data De inhoud van het TeX-bestand.
	 * @param status De HTTP-status van de response.
	 * @param headers Een array headernaam => headerwaarde.
	 */
	public function __construct($filenaam, $data='', $status = 200, $headers = array()) {
		parent::__construct($data, $status, $headers);
		$this->filenaam = $filenaam;
	}

	/**
	 * @brief Verstuur de headers en inhoud van deze response.
	 *
	 * Zorgt in vergelijking met Response::send voornamelijk
	 * ervoor dat het gedownload wordt als een .tex-bestand.
	 */
	public function send() {
		$this->headers->set("Content-type", 'text/tex');
		$this->headers->set("Content-disposition", 'attachment; filename="' . $this->filenaam . '"');
		return parent::send();
	}
}
