<?php
// $Id$

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use Symfony\Component\HttpFoundation\Request;
use function \Sentry\init;

$spaceinittm = new ProfilerTimerMark('space/init.php');
Profiler::getSingleton()->addTimerMark($spaceinittm);

// In principe zou de logger al bij de auto_prepend geinitialiseerd moeten zijn,
// maar kan geen kwaad om de dependency expliciet te maken.
require_once('space/logger.php');
global $logger;

global $request;
$request = Request::createFromGlobals();

/*+ Alle initialisatiecode: session, PRNG, security- en library includes,
*   rewrite-path uitlezen
*/

// We hebben BestuursLid nodig voor de specialconstants :)
require_once('space/classes/includes.php');

require_once('space/specialconstants.php');
// databasewachtwoorden:
require_once('secret.php');

//Initialisatie van Sentry. Dit moet zo snel mogelijk gebeuren zodat we gegevens kunnen gaan verzamelen.
//SENTRY_URL staat in secret.php
if(defined('SENTRY_URL'))
{
	init(['dsn' => SENTRY_URL ]);
}

// PDO-databaseclass
require_once('space/pdo.php');

// Responseklasse die Pages ondersteunt.
require_once('space/response.php');
// Excepties die met Responses werken.
require_once('space/exception.php');

// Dingen voor de API
require_once('api/Request.php');
require_once('api/RequestAPI.php');

$logger->debug('Autorisatie initialiseren.');
require_once('space/auth/init.php');

// Hulpfuncties voor debuggen.
require_once('space/debug/zeurmodus.php');

$dbstm = new ProfilerTimerMark('create dbs');
Profiler::getSingleton()->addTimerMark($dbstm);


$adapter = new Local(FILESYSTEM_PREFIX);
global $filesystem;
$filesystem = new Filesystem($adapter);

// moet helaas hier, want auth heeft sql nodig, en vice versa, dus gaat niet
// echt lekker...
// Maak db's aan er wordt niet meteen geconnect, dat gebeurt pas zodra nodig.
$dbuser = getDatabaseUsername();
$logger->debug("Level is $dbuser");
$dbpass = sqlpass();

global $WSW4DB, $BMDB, $BWDB;
$WSW4DB = new dbpdo(WHOSWHO4_DB, '127.0.0.1', $dbuser, $dbpass);
$BWDB = new dbpdo(BOEKEN_DB, '127.0.0.1', $dbuser, $dbpass);
$BMDB = new dbpdo(BENAMITE_DB, '127.0.0.1', $dbuser, $dbpass);

$CACHING = false;

// Om te kijken of de cache al weggemieterd is zodat we dat niet
// nog een keer hoeven te doen
$cacheWeggemieterd = false;

unset($dbuser, $dbpass);

$dbstm->markEnd();

$taaltm = new ProfilerTimerMark('taal.php');
Profiler::getSingleton()->addTimerMark($taaltm);

require_once('space/taal.php');

global $session;
// Zo vroeg mogelijk eventuele language override (via vlaggetjes) processen,
// dit moet in ieder geval voor gettext_init gebeuren.
if ($setLanguage = tryPar("setlanguage"))
{
	$logger->debug("Override taal naar $setLanguage");
	if ($setLanguage === "en" || $setLanguage === "nl")
	{
		$session->set("language_override", $setLanguage);
	}
}

$taaltm->markEnd();

require_once('space/forms.php');
require_once('space/purify.php');
require_once('space/sendmail.php');

require_once('WhosWho4/init.php');

if (!defined('NO_GETTEXT'))
{
	$gettexttm = new ProfilerTimerMark('gettext_init()');
	Profiler::getSingleton()->addTimerMark($gettexttm);
	gettext_init();
	$gettexttm->markEnd();
}

require_once('space/benamite/init.php');

umask(077); // as opposed to 002

$spaceinittm->markEnd();
