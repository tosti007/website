<?php // $Id$

/* Deze file zou als eerste moeten worden geladen, en regelt de basale includes. */

// Zeur als de configuratie niet klopt.
use Symfony\Component\Cache\Traits\ApcuTrait;

if (!function_exists('mb_internal_encoding'))
{
	user_error('mb_internal_encoding bestaat niet. Is de mbstring-extensie wel geinstalleerd?', E_USER_ERROR);
}
if (!function_exists('_'))
{
	user_error('De functie _ bestaat niet. Is de gettext-extensie wel geinstalleerd?', E_USER_ERROR);
}
if (!ini_get('short_open_tag'))
{
	user_error('short_open_tag staat uit in php.ini. Je krijgt nu vage syntaxerrors!', E_USER_ERROR);
}

mb_internal_encoding('UTF-8');

// composer-autoload
require_once('vendor/autoload.php');

// Controleer dat verscheidene Composerdependencies werken.
if (!ApcuTrait::isSupported())
{
	user_error('APCu is niet supported. Is de apcu-extensie wel geinstalleerd?', E_USER_ERROR);
}

// deze file zit NIET in de repo!!! zet daar dus
// de locatie-specifieke variabelen! (debug, root)
require_once('space/init_defines.php');

require_once('space/prelude.php');

// Doe logging en error handling zo vroeg mogelijk,
// dan hebben we geen generieke errormeldingen.
require_once('space/logger.php');
require_once('space/error-handler.php');

// om profiling informatie te verkrijgen
require_once("space/classes/Profiler.cls.php");

// en space constants:
require_once(FS_ROOT.'/space/constants.php');

timer_mark("DONE: auto_prepend.php");
