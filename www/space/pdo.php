<?php

/**
 * Informatie over het uitvoeren van een query.
 *
 * Denk hierbij aan:
 * * wat voor een query is het ueberhaupt (SELECT, UPDATE, etc.)
 * * hoe willen we het resultaat (COLUMN, RETURNID, etc.)
 *
 * Dit wordt geparsed uit het eerste argument aan dbpdo::q.
 * (Parsen gaat door te zeggen: `(new QueryType())->parse($key);`)
 */
class QueryType
{
	/**
	 * @var string $type
	 *
	 * Denk hier aan SELECT, UPDATE, etc.
	 */
	public $type;
	/**
	 * @var string $resultFormat
	 *
	 * Denk hier aan COLUMN, TUPLE, etc.
	 */
	public $resultFormat;

	/**
	 * @var bool $maybe
	 * Indien true, geven we null ipv een error indien geen rows gevonden zijn.
	 */
	public $maybe = false;

	/**
	 * @var int? $cursorFormat
	 *
	 * Kan zijn null (geen cursor, return een array)
	 * of pdo::CURSOR_FWDONLY (return een PDOStatement)
	 * of pdo::CURSOR_SCROLL (return een PDOStatement).
	 *
	 * FIXME: denk na voor welke types / result formats dit sense maakt, en geef error als het dat niet is?
	 */
	public $cursorFormat = null;

	/**
	 * @var string? $transactionType
	 * Voor transactie-gerelateerde queries: welke operatie doen we precies?
	 * Denk hier bijvoorbeeld aan 'start' of 'rollback'.
	 */
	public $transactionType = null;

	/**
	 * Parse het querytype uit de eerste parameter van ->q().
	 *
	 * query can be prepended with a keyword to change the returned data
	 * format:
	 * - returnid: for use with INSERT, returns the auto_increment value used for that row.
	 * - returnaffected: return the number of modified rows by this query.
	 * - tuple, value: select exactly one row or one value
	 * - maybetuple, maybevalue: select zero or one rows or values
	 * - column: return a list of a single attribute
	 * - table: return complete result in one array (indexed by column name and column number)
	 * - keytable: same as table but arraykey is the field called ARRAYKEY
	 * - keyvaluetable: select two columns, and this returns a map from the first field (key) to the second (exactly one value)
	 * - cursor $keyword: return a PDO cursor instead of an array.
	 *
	 * @param string $format De query om te parsen.
	 *
	 * @return string De overgebleven delen van de query (dus valide (My-)SQL).
	 */
	public function parse($format)
	{
		// In principe is het keyword het eerste woord van de query.
		// We updaten $format hier niet, omdat het keyword al meteen insert/update/etc. kan zijn.
		$onderdelen = preg_split('/\s+/', $format, 2);
		// Het kan gebeuren dat we alleen een keyword hebben, maar meestal komt er een rest na.
		$keyword = $onderdelen[0];
		if (count($onderdelen) > 1)
		{
			assert(count($onderdelen) == 2);
			$rest = $onderdelen[1];
		}
		else
		{
			$rest = '';
		}

		$keyword = strtolower($keyword);

		switch ($keyword)
		{
			// Basale SQL-keywords.
			case 'insert':
			case 'update':
			case 'replace':
			case 'set':
			case 'alter':
				$this->type = 'update';
				return $format;
			case 'delete':
				$this->type = 'delete';
				return $format;
			case 'select':
			case 'describe':
			case 'show':
				$this->type = 'select';
				return $format;
			// transactions
			case 'start':    // start transaction. Do not support BEGIN, it's deprecated
			case 'commit':
			case 'rollback':
				$this->type = 'transaction';
				$this->transactionType = $keyword;
				return $format;

			case 'cursor':
				// Geef een PDO-cursor.
				$this->cursorFormat = PDO::CURSOR_FWDONLY;
				return $this->parse($rest);

			case 'returnid':
			case 'returnaffected':
				// Voor INSERT of UPDATE of vergelijkbaar: return de id's die onder handen zijn genomen.
				$this->resultFormat = $keyword;
				return $this->parse($rest);

			// select commandos; keywords, then regular
			case 'maybetuple':
				$this->maybe = true;
				$this->resultFormat = 'tuple';
				return $this->parse($rest);
			case 'maybevalue':
				$this->maybe = true;
				$this->resultFormat = 'value';
				return $this->parse($rest);

			case 'column':
			case 'table':
			case 'rows':
			case 'keytable':
			case 'keyvaluetable':
			case 'tuple':
			case 'value':
				$this->resultFormat = $keyword;
				return $this->parse($rest);
			default:
				user_error("SQL command/lib keyword '$keyword' unknown!", E_USER_ERROR);
		}

		return $format;
	}
};

/**
 * Zelfgeschreven class als extensie op de PDO-class.
 */
class dbpdo
{
	public $pdo;
	private $database;
	private $openTransaction = false;

	public function __construct($database, $host, $user, $password)
	{
		global $logger;
		$logger->debug("Verbinden met $host/$database als $user");

		$this->database = $database;
		$this->pdo = new PDO('mysql:dbname='.$database.';'
							.'host='.$host.';'
							.'charset=utf8mb4'
							, $user
							, $password
							, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));

		if (!$this->pdo) {
			user_error("PDO-object kon niet gemaakt worden: " . $this->pdo, E_USER_ERROR);
		}
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
	}

	/**
	 * Execute a query.
	 *
	 * syntax:
		%%: literal %
		%.: auto-detect
		%s: string with quotes and escaping
		%c: string with quotes and escaping, embraced by percent signs for
		    usage with LIKE
		%i: integer
		%f: floating point
		%l: literal (no quoting/escaping)
		%_: nothing, but do process one argument
		%F: function
		%A?: array of type ?, comma separated
		%S: array of key => ., becomes key=., comma separated

	* Zie documentatie van QueryType::parse voor keywords.

	* NOTE: komt direct uit de LIBDB documentation.
	*/

	public function q() // queryf
	{
		global $logger;

		$argv = func_get_args();
		$format = trim(array_shift($argv));

		$logger->debug($this->database . '->q(' . $format . ')');

		// Parse de query zodat we weten wat we moeten returnen.
		$queryInfo = new QueryType();
		$format = $queryInfo->parse($format);

		// Parse de query voor parameters om in te vullen.
		$parts = explode('%', $format);
		$literal = false;
		$values = array();
		$counter = 0;

		// Deel 1 heeft geen % ervoor, dus kunnen we meteen in de query gooien.
		$query = array_shift($parts);

		foreach ($parts as $partkey => $part) {
			if ($literal) {
				$literal = false;
				$query .= $part;
				continue;
			}
			if (!$part) {
				// literal %%
				$query .= '%';
				$literal=true;
				continue;
			}
			if (!$argv) {
				user_error("Geen argument voor queryparameter %".$part[0], E_USER_ERROR);
			}
			$val = array_shift($argv);
			switch ($part{0}) {
				case 'A':
					if (!is_array($val) || !$val) {
						throw new InvalidArgumentException(
							"%A in \$DATABASE->q() has to correspond to a "
							. "non-empty array, it's now a '$val' (Query:"
							. "'$format')!");
					}
					if ($part{1} == '{')
					{
						$subpart = explode('}', substr($part, 2), 2);
						if (count($subpart) == 1 ) {
							throw new InvalidArgumentException(
								"Missing } in: ".$part);
						}
						foreach($val as $subval)
						{
							$query .= (isset($GLOBALS['MODE']) ? ', ' : '');
							if (!is_array($subval) || !$subval) {
								throw new InvalidArgumentException(
									"%A{} in \$DATABASE->q() has to correspond "
									. "to a non-empty sub-array, it's now a "
									. "'$subval' (Query:'$format')");
							}
							if (count($subval) != strlen($subpart[0])) {
								throw new InvalidArgumentException(
									"%A{} in \$DATABASE->q() has sub-array "
									. "with mismatching length (Query:"
									. "'$format'");
							}
							for($i = 0; $i < strlen($subpart[0]); $i++)
							{
								$query .= ($i == 0 ? '(' : ', ');
								$GLOBALS['MODE'] = $subpart[0]{$i};
								$query .= ":value".++$counter;
								$values[":value".$counter] = dbpdo::val2sql(array_shift($subval));
							}
							$query .= ')';
						}
						unset($GLOBALS['MODE']);
						$query .= $subpart[1];
						break;
					}
					$GLOBALS['MODE'] = $part{1};
					$valsArray = array();
					foreach($val as $v) {
						$valsArray[] = ":value".++$counter;
						$values[":value".$counter] = $v;
					}
					if(substr($query, -1) !== '(') {
						$query .= '(';
					}
					$query .= implode( ', ', $valsArray );
					unset($GLOBALS['MODE']);
					if(substr($part, 2) !== ')') {
						$query .= ')';
						$query .= substr($part,3);
					} else {
						$query .= substr($part,2);
					}
					break;
				case 'S':
					$parts = array();
					foreach ( $val as $field => $value ) {
						$parts[] = '`'.$field.'` = :value'.++$counter;
						$values[":value".$counter] = dbpdo::val2sql($value);
					}
					$query .= implode(', ', $parts);
					unset($parts);
					$query .= substr($part,1);
					break;
				case 'F':
					$pos1 = strpos($val, '(');
					$pos2 = strpos($val, ')');
					if(!($pos1 && $pos2 && $pos1 < $pos2 && $pos1 != 0))
						user_error($val . ' is not a valid function', E_USER_ERROR);
					$query .= $val . substr($part, 1);
					break;
				case 's':
				case 'c':
				case 'i':
				case 'f':
				case 'l':
				case '.':
					// Als het origineel tussen backticks staat, willen we een
					// table- of column-naam hebben. Dit is nog steeds niet
					// helemaal wat we willen, maar pdo heeft niet een functie
					// (zoals mysql had) om data fatsoenlijk te sanitizen
					if(substr($query, -1) === '`' && substr($parts[$partkey], 1, 1) === '`') {
						$query .= dbpdo::val2sql($val, $part{0});
						$query .= substr($part,1);
					} else {
						$query .= ":value".++$counter;
						$values[":value".$counter] = dbpdo::val2sql($val, $part{0});
						$query .= substr($part,1);
					}
					break;
				case '_': // eat one argument
					$query .= substr($part,1);
					break;
				default:
					user_error("Unkown %-code: ".$part{0}, E_USER_ERROR);
			}

		}

		if ($literal) {
			user_error("Internal error in q()", E_USER_ERROR);
		}

		try
		{
			$pdo = $this->pdo;

			// nothing left to do if transaction statement...
			if ($queryInfo->type == 'transaction') {
				switch($queryInfo->transactionType) {
					case 'start':
						$pdo->beginTransaction();
						$this->openTransaction = true;
						break;
					case 'commit':
						$pdo->commit();
						$this->openTransaction = false;
						break;
					case 'rollback':
						$pdo->rollback();
						$this->openTransaction = false;
						break;
				}
				return null;
			}

			// Lees opties voor prepare uit.
			$pdoOptions = [];
			$cursorFormat = $queryInfo->cursorFormat;
			if (!is_null($cursorFormat))
			{
				$pdoOptions[PDO::ATTR_CURSOR] = $cursorFormat;
			}
			$statement = $pdo->prepare($query, $pdoOptions);

			foreach($values as $k => $v) {
				if(is_null($v)) {
					$statement->bindValue($k, NULL, PDO::PARAM_INT);
				} else {
					$statement->bindValue($k, $v);
				}
			}
			$statement->execute();

			// When we delete, return the number of rows affected
			if ($queryInfo->type == 'delete') {
				return $statement->rowCount();
			}

			if ($queryInfo->type == 'update') {
				if ($queryInfo->resultFormat == 'returnid') {
					return $pdo->lastInsertId();
				}
				if ($queryInfo->resultFormat == 'returnaffected') {
					return $statement->rowCount();
				}

				// Update-query returnt bij default niets.
				return;
			}

			assert($queryInfo->type == 'select');
			// Handel eerst statements af met een vaste layout (1 row/kolom/value).
			if ($queryInfo->resultFormat == 'tuple' || $queryInfo->resultFormat == 'value') {
				$res = $statement->fetchAll();
				if (count($res) < 1)
				{
					// Bij een maybe-query geven we NULL ipv error bij te weinig resultaten.
					if ($queryInfo->maybe)
					{
						return NULL;
					}

					user_error("$this->database query error".
						": Query did not return any rows", E_USER_ERROR);
				}

				if (count($res) > 1)
				{
					user_error("$this->database query error".
						": Query returned too many rows (".count($res).")",
						E_USER_ERROR);
				}

				$row = current($res);

				// Bij value geven we de eerste kolom.
				if ($queryInfo->resultFormat == 'value')
				{
					return current($row);
				}

				return $row;
			}

			if ($queryInfo->resultFormat == 'column') {
				return $statement->fetchAll(PDO::FETCH_COLUMN, 0);
			}

			// Handel cursors af: geef de statement zelf.
			if (!is_null($queryInfo->cursorFormat))
			{
				return $statement;
			}

			// Handel resultaten met meerdere rows en columns af.
			if ($queryInfo->resultFormat == 'table') {
				// Een leuk iets, een normale fetchAll() werkt hier niet, want als
				// je kwerrie select bevat waarbij aliases naar indices gezet wordt in
				// de pdo-fetch er stiekem nog een extra resultaat in de array gestopt

				$retTable = array();
				while ($row = $statement->fetch(PDO::FETCH_ASSOC))
				{
					$rowArray = array();
					$i = 0;
					foreach($row as $key => $val) {
						$rowArray[$i] = $val;
						$rowArray[$key] = $val;
						$i++;
					}
					$retTable[] = $rowArray;
				}
				return $retTable;
			}
			if ($queryInfo->resultFormat == 'keytable') {
				$ret = array();
				$res = $statement->fetchAll();
				foreach($res as $r) {
					$arraykey = $r['ARRAYKEY'];
					unset($r['ARRAYKEY']);
					$ret[$arraykey] = $r;
				}
				return $ret;
			}
			if ($queryInfo->resultFormat == 'keyvaluetable') {
				if($statement->columnCount() != 2) {
					user_error("Keyvaluetable only works on a table with exactly 2 columns!");
				}
				$ret = array();
				$res = $statement->fetchAll();
				foreach($res as $r) {
					$ret[$r[0]] = $r[1];
				}
				return $ret;
			}

			// Default: doe gewoon een standaard fetchall.
			return $statement->fetchAll();
		} catch(PDOException $e) {
			if($this->openTransaction) {
				$pdo->rollback();
				$this->openTransaction = false;
			}

			throw $e;
		}
	}

	// transform a php variable into one that can be put directly into a query
	public static function val2sql($val, $mode='.')
	{
		if(is_null($val)) {
			return NULL;
		}
		if (isset($GLOBALS['MODE'])) {
			$mode = $GLOBALS['MODE'];
		}
		if (!isset($val)) return 'null';
		switch ($mode)
		{
			case 'f': return (float)$val;
			case 'i': return (int)$val;
			case 's': return ''.$val.'';
			case 'c': return '%'.$val.'%';
			case 'l': return $val;
			case '.': break;
			default: user_error("Unknown mode: $mode", E_USER_ERROR);
		}

		switch (gettype($val))
		{
			case 'boolean':
				return (int) $val;
			case 'integer':
			case 'double':
				return $val;
			case 'string':
				return ''.$val.'';
			case 'array':
			case 'object':
				return ''.serialize($val).'';
			case 'resource':
				user_error('Cannot store a resource in database', E_USER_ERROR);
		}
		user_error('Case failed in lib.database', E_USER_ERROR);
	}
}
