<?php

/**
 * @brief Een exceptie die onafgevangen een custom Response toont aan de gebruiker.
 *
 * Beter is uiteraard om alles in je returnwaarden te stoppen, maar soms moet
 * flow hardhandig onderbroken worden omdat het niets gaat worden.
 * Denk hierbij bijvoorbeeld aan een gevaarlijke functie die nog een extra
 * `requireAuth`-check uitvoert en indien gefaald een 403-error wil geven,
 * of de foutmelding bij het hersubmitten van een Token.
 *
 * Als een ResponseException niet wordt opgevangen in de controllerfunctie,
 * komt deze terecht in `space.php`, die maakResponse erop aanroept.
 * Deze response wordt naar de gebruiker gestuurd.
 *
 * @see spaceHTTP
 * @see Token
 */
abstract class ResponseException extends Exception
{
	/**
	 * @brief Maak een Response-object van deze exception.
	 *
	 * @return Een instance van \Symfony\Component\HttpFoundation\Response.
	 */
	abstract public function maakResponse();
}

/**
 * @brief Een exceptie die een gegeven response toont aan de gebruiker.
 *
 * Dit is een handige uitbreiding voor ResponseException
 * in het geval dat je response al van tevoren bekend is.
 *
 * Zie de documentatie van ResponseException voor wanneer je deze constructie
 * wel en niet wil gebruiken.
 *
 * @see ResponseException
 */
class ConstantResponseException extends ResponseException
{
	private $response;

	public function __construct($response)
	{
		$this->response = $response;
	}
	/**
	 * @brief Maak een Response-object van deze exception.
	 *
	 * @return Een instance van \Symfony\Component\HttpFoundation\Response.
	 */
	public function maakResponse()
	{
		return $this->response;
	}
}
