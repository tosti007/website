<?php
//dit is nodig!!
global $RECHTEN_LIJST_NIET_INGELOGD;
global $RECHTEN_LIJST_INGELOGD_LID;

global $auth;

require_once('space/init.php');
require_once('apiconst.php');

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
	//verkrijg sessie details
	//wanneer niet ingelogd of sessie verlopen -> status = not logged in
	//wanneer ingelogd -> details over de ingelogde persoon; rechten, naam, lidnummer, fotopad, etc.
	$lid = Lid::getIngelogdLid();
	//s=1 zorgt ervoor dat alleen de sessiestatus wordt teruggegeven
	if(isset($_GET['s']) && $_GET['s'] == '1')
		$msgbody = array('status' => ($lid == null ? SESSIE_STATUS_NIET_INGELOGD : SESSIE_STATUS_INGELOGD));
	else
	{
		if($lid != null)
		{
			$foto = ProfielFoto::zoekHuidig($lid);
			$fotourl = null;
			if($foto != null) //misschien moeten we dit toch terugzetten naar medium, want thumbnail ziet er eigenlijk baggerslecht uit :/
				$fotourl = HTTPS_ROOT . ProfielFoto::zoekHuidig($lid)->getMedia()->url() . '/Thumbnail';
		}
		//TODO: iets met rechten doorgeven, zodat de app weet welke dingen hij wel en niet van de server kan verwachten
		// (de app regelt dus niet wat de gebruiker wel of niet ziet natuurlijk, maar moet geen lege veldjes of onmogelijke
		// opties weergeven)
		$msgbody = array(
			'status' => SESSIE_STATUS_INGELOGD,
			'rechten' => ($lid == null ? $RECHTEN_LIJST_NIET_INGELOGD : $RECHTEN_LIJST_INGELOGD_LID),
			'persoon' => array(
				'contactId' => ($lid == null ? 0 : $lid->getContactID()),
				//dit geeft de naam in de vorm: Voornaam tussenvoegsel Achternaam, details kunnen later worden opgevraagd
				//met behulp van het contactId
				'naam' => ($lid == null ? null : $lid->getNaam(WSW_NAME_DEFAULT)),
				'email' => ($lid == null ? null : $lid->getEmail()),
				//een url naar de medium versie van de huidige profielfoto (of null als er geen is)
				//dit is alleen voor gebruik in de navdrawer bijvoorbeeld, bij het opvragen van lid details,
				//zullen media id's gebruikt worden, zodat de app ook toegang krijgt tot de hogere kwaliteit
				//versie (mocht dat nodig zijn)
				'foto' => ($lid == null ? null : $fotourl),
				//auth
				'authLevel' => $auth->getLevel(),
				'isLid' => !(!($lid = @Lid::getIngelogdLid()) instanceof Lid),
				'isHuidigLid' => $auth->getLidnr() && @Lid::getIngelogdLid()->isHuidigLid(),
				'isOudLid' => $auth->getLidnr() && @Lid::getIngelogdLid()->isOudLid(),
				'isActief' => !(!($pers = Persoon::getIngelogd()) instanceof Persoon) && $pers->getCommissies()->aantal() > 0,
				'isGod' => $auth->isGod(),
				'isBestuur' => $auth->isBestuur(),
				'isTablet' => $auth->isDiBSTablet(),
				'isBoekenCommissaris' => $auth->isBoekenCommissaris()
			)
		);
	}
}
else if($_SERVER['REQUEST_METHOD'] === 'POST' && DEBUG)
{
	//login
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		//komt uit login.php
		global $mylidnr; 
		try {
			$ww = Wachtwoord::geefLogin($_POST['username']);
			if($ww) $mylidnr = $ww->authenticate(utf8_decode(base64_decode($_POST['password'])));
		}
		catch(Exception $e) {
			// beetje lelijk, maar werkt wel
		}
		if (!$mylidnr)
		{
			//niet gelukt
			$msgbody = array('status' => SESSIE_STATUS_LOGIN_MISLUKT);
		}
		else
		{
			session_regenerate_id(true);
			// inloggen gelukt, zet session variabelen op basis daarvan.
			$_SESSION['mylidnr'] = $mylidnr;
			$_SESSION['veilig'] = true;
			$msgbody = array('status' => SESSIE_STATUS_INGELOGD);
		}
	}
}

?>
