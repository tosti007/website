<?php
ob_start("ob_gzhandler");

//dit is nodig!!
global $RECHTEN_LIJST_NIET_INGELOGD;
global $RECHTEN_LIJST_INGELOGD_LID;

global $BADGES;

require_once('space/init.php');
require_once('WhosWho4/constants.php');
require_once('apiconst.php');

function array_to_xml( $data, &$xml_data )
{
	foreach( $data as $key => $value )
	{
		if( is_numeric($key) )
			$key = 'item'.$key; //dealing with <0/>..<n/> issues

		if( is_array($value) )
		{
			$subnode = $xml_data->addChild($key);
			array_to_xml($value, $subnode);
		}
		else
			$xml_data->addChild("$key",htmlspecialchars("$value"));
	}
}


class API
{
	protected $method = '';
	protected $class = '';
	protected $subClass = '';
	protected $id = NULL;
	protected $expand = NULL;
	protected $vars = NULL;
	protected $content_type = 'json';
	protected $errors = '';

	public function __construct($args)
	{
		header('Access-Control-Allow-Orgin: *');
		header('Access-Control-Allow-Methods: *');

		if(isset($_SERVER['CONTENT_TYPE']))
		{
			switch($content_type = strtolower($_SERVER['CONTENT_TYPE']))
			{
			case 'json':
			case 'application/json':
			default:
				header('Content-Type: application/json; charset=utf-8');
				$this->content_type = $content_type;
				break;
			case 'xml':
			case 'application/xml':
				header('Content-Type: application/xml; charset=utf-8');
				$this->content_type = $content_type;
				break;
			}
		}
		else
		{
			header('Content-Type: application/json; charset=utf-8');
			$this->content_type = 'json';
		}

		$args = array_filter($args);

		if(count($args) == 3)
			$this->subClass = array_pop($args);
		if(count($args) == 2)
			$this->id = array_pop($args);
		$this->class = array_pop($args);

		if(!in_array($this->class, array('Contact', 'Persoon', 'Lid')))
			requireAuth('god');

		$this->method = $_SERVER['REQUEST_METHOD'];

		if($this->method == 'POST' && strtoupper(tryPar('method', false)) === 'PUT')
			$this->method = 'PUT';
		else if($this->method == 'POST' && strtoupper(tryPar('method', false)) === 'DELETE')
			$this->method = 'DELETE';

		switch($this->method)
		{
		case 'DELETE':
		case 'POST':
			$this->request = $this->_cleanInputs($_POST);
			break;
		case 'GET':
			$this->request = $this->_cleanInputs($_GET);
			break;
		case 'PUT':
			$this->request = $this->_cleanInputs($_GET);
			break;
		default:
			$this->errors = $this->_response('Invalid Method', 405);
			return;
			break;
		}

		$this->expand = strtolower(tryPar('expand', NULL));

		if(preg_match('#\((.*?)\)#', $this->expand, $match))
		{
			if(substr($this->expand, -1) !== ')' || substr($this->expand, 0, 1) !== '('
					|| substr_count($this->expand, '(') != 1 || substr_count($this->expand, ')') != 1)
			{
				$this->errors = $this->_response('Invalid expand expression', 405);
				return;
			}

			$this->expand = explode(',', $match[1]);
		}
		else if($this->expand)
			$this->expand = array($this->expand);

		$this->vars = strtolower(tryPar('vars', NULL));

		if(preg_match('#\((.*?)\)#', $this->vars, $match))
		{
			if(substr($this->vars, -1) !== ')' || substr($this->vars, 0, 1) !== '('
					|| substr_count($this->vars, '(') != 1 || substr_count($this->vars, ')') != 1)
			{
				$this->errors = $this->_response('Invalid vars expression', 405);
				return;
			}

			$this->vars = explode(',', $match[1]);
		}
		else if($this->vars)
			$this->vars = array($this->vars);

		$this->offset = (int)tryPar('offset', 0);
		$this->limit = (int)tryPar('limit', 20);
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function processAPI()
	{
		if(!class_exists($this->class))
			return $this->_response('Not Found', 404);

		if($this->id)
		{
			switch($this->method)
			{
			case 'GET':
				return $this->processGetAPI();
				break;
			case 'PUT':
				return $this->processPutAPI();
				break;
			case 'POST':
				return $this->_response('Forbidden', 403);
				break;
			case 'DELETE':
				return $this->processDeleteAPI();
				break;
			default:
				return $this->_response('Unkown method', 405);
				break;
			}
		}
		else
		{
			switch($this->method)
			{
			case 'GET':
				return $this->processGetAPI();
				break;
			case 'PUT':
				return $this->_response('Not Found', 404);
				break;
			case 'POST':
				return $this->processPostAPI();
				break;
			case 'DELETE':
				return $this->_response('Not Found', 404);
				break;
			default:
				return $this->_response('Unkown method', 405);
				break;
			}
		}
	}

	private function processPutAPI()
	{
		if(!is_subclass_of($this->class, 'Entiteit'))
			return $this->_response('Forbidden', 403);

		$data =json_decode(file_get_contents('php://input'));
		if(is_null($data))
			$data = new stdClass();

		$class = $this->class;
		$obj = $class::geef($this->id);

		if(!$obj)
			return $this->_response('Forbidden', 403);

		if(!$obj->magWijzigen())
			return $this->_response('Forbidden', 403);

		$ret = $obj->__jsonPut($data);
		if(!$ret)
			return $this->_response($obj->__json($this->vars, $this->expand), 200);
		else
		{
			if(is_array($ret))
				return $this->_response($ret, 400);
			else
				return $this->_response('Forbidden', 403);
		}
	}

	private function processPostAPI()
	{
		if(!is_subclass_of($this->class, 'Entiteit'))
			return $this->_response('Forbidden', 403);

		$data = json_decode(file_get_contents('php://input'));
		if(is_null($data))
			$data = new stdClass();

		$class = $this->class;

		$ret = $class::__jsonPost($data);
		if($ret instanceof $class)
		{
			header('Location: ' . $ret->getApiUri());
			return $this->_response($ret->__json($this->vars, $this->expand), 201);
		}
		else
		{
			if(is_array($ret))
				return $this->_response($ret, 400);
			else
				return $this->_response('Forbidden', 403);
		}
	}

	private function processDeleteAPI()
	{
		if(!is_subclass_of($this->class, 'Entiteit'))
			return $this->_response('Forbidden', 403);

		$class = $this->class;
		$obj = $class::geef($this->id);

		if(!$obj)
			return $this->_response('Forbidden', 403);

		if(!$obj->magVerwijderen())
			return $this->_response('Forbidden', 403);

		$ret = $obj->__jsonDelete();
		if(!$ret)
			return $this->_response('bla', 204);
		else
		{
			if(is_array($ret))
				return $this->_response($ret, 400);
			else
				return $this->_response('Forbidden', 403);
		}
	}

	private function processGetAPI()
	{
		if(!is_subclass_of($this->class, 'Entiteit'))
			return $this->_response('Forbidden', 403);

		if($this->id && !$this->subClass)
			return $this->processGetSingleAPI();
		else
			return $this->processGetVerzamelingAPI();
	}

	private function processGetSingleAPI()
	{
		$class = $this->class;
		$obj = $class::geef($this->id);

		if(!$obj)
			return $this->_response('Forbidden', 403);

		if(!$obj->magBekijken())
			return $this->_response('Forbidden', 403);

		$json_object = $obj->__json($this->vars, $this->expand);

		if(count($json_object) == 0)
			return $this->_response('Forbidden', 403);

		return $this->_response($json_object, 200);
	}

	private function processGetVerzamelingAPI()
	{
		if($this->id && $this->subClass)
		{
			$class = $this->class;
			$obj = $class::geef($this->id);

			if(!$obj)
				return $this->_response('Forbidden', 403);

			if(!$obj->magBekijken())
				return $this->_response('Forbidden', 403);

			$class = $this->subClass . 'Verzameling';

			if(!is_array($this->expand))
				$this->expand = array($this->expand);

			$objs = $class::geefAlleJson($this->subClass, $this->offset, $this->limit, $this->vars, $this->expand, $obj);
		}
		else
		{
			$class = $this->class . 'Verzameling';

			if(!is_array($this->expand))
				$this->expand = array($this->expand);

			$objs = $class::geefAlleJson($this->class, $this->offset, $this->limit, $this->vars, $this->expand);
		}

		return $this->_response($objs, 200);
	}

	private function _response($data, $status = 200)
	{
		header('HTTP/1.1 ' . $status . ' ' . $this->_requestStatus($status));

		switch($this->content_type)
		{
		case 'xml':
		case 'application/xml':
			$xml = new SimpleXMLElement('<'.strtolower($this->class).'/>');
			array_to_xml($data, $xml);
			$data = $xml->asXML();
			break;
		default:
		case 'json':
		case 'application/json':
			$data = json_encode($data);
			break;
		}

		return $data;
	}

	private function _cleanInputs($data)
	{
		$clean_input = Array();

		if (is_array($data))
		{
			foreach ($data as $k => $v)
				$clean_input[$k] = $this->_cleanInputs($v);
		}
		else
			$clean_input = trim(strip_tags($data));

		return $clean_input;
	}

	private function _requestStatus($code)
	{
		$status = array(
			200 => 'OK',
			201 => 'Created',
			204 => 'No Content',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			500 => 'Internal Server Error',
		);

		return (isset($status[$code]))?$status[$code]:$status[500];
	}
}

function apiDingen_content($rest)
{
	if(empty($rest))
	{
		//Welkom bij de a-es api
		echo "<html><head><title>" . aesnaam() . " API</title></head><body><h1>" . aesnaam() . " API v2</h1></body></html>";
		die();
	}
	//voorkom caching, gewoon voor de zekerheid
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	$error = null;
	$msgbody = array();
	switch($rest[0])
	{
		case "versie":
			header('Content-Type: application/json; charset=utf-8');
			include('api_versie.php');
			break;
		case "sessie":
			header('Content-Type: application/json; charset=utf-8');
			include('api_sessie.php');
			break;
		case "activiteiten":
			header('Content-Type: application/json; charset=utf-8');
			include('api_activiteiten.php');
			break;
		case "leden":
			header('Content-Type: application/json; charset=utf-8');
			requireAuth('god');
			include('api_leden.php');
			break;
		case "media":
			header('Content-Type: application/json; charset=utf-8');
			requireAuth('god');
			include('api_media.php');
			break;
		default:
			requireAuth('god');
			$api = new API($rest);

			$errors = $api->getErrors();
			if(!empty($errors))
			{
				echo $errors;
				die();
			}

			echo $api->processAPI();
			die();
			break;
	}
	//inkapselen in een standaard bericht formaat
	//if($error == null)
	echo json_encode(array('body' => $msgbody));
	/*else
	{
		$status = SESSIE_STATUS_NIET_INGELOGD;
		if(Lid::getIngelogdLid() != null)
			$status = SESSIE_STATUS_INGELOGD
		echo json_encode(array('error' => array('msg' => $error, 'sessiestatus' => $status)));
	}*/
	//belangrijk, anders krijg je er een paginalayout omheen!
	die();
}
?>
