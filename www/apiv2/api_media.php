<?php
require_once('space/init.php');
require_once('apiconst.php');

function mediaToJsonDetail($media)
{
	$url = HTTPS_ROOT . $media->url();
	$activiteitId = $media->getActiviteitActiviteitID();
	if($activiteitId == null)
		$activiteitId = -1;
	return array(
		'id' => $media->getMediaID(),
		'omschrijving' => $media->getOmschrijving(),
		'activiteitId' => $activiteitId,
		'origineelUrl' => $url . '/Origineel',
		'mediumUrl' => $url . '/Medium',
		'thumbnailUrl' => $url . '/Thumbnail'
	);
}

if(Lid::getIngelogdLid())
{
	if(array_key_exists(1, $rest))
	{
		$mediaid = intval($rest[1]);
		if($mediaid > 0)
		{
			$media = MediaVerzameling::verzamel(array($mediaid))->filterBekijken();
			if($media->bevat($mediaid))
				$msgbody = array('media' => mediaToJsonDetail($media->first()));
		}
		else
		{
			//error
		}
	}
	else
	{
		//error
		//er is geen overzicht voor media	
	}
}
else
{
	//error
	//niet ingelogde mensen kunnen geen media zien!	
}

?>