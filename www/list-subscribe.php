<?php
/*
 * $Id$
 */

require_once('space/sendmail.php');

function processlistsubscribe_content($rest) {

	$email    = trim($_POST['email']);
	$listname = $rest[0];
	
	if (!preg_match('/^[\w\-]+$/', $listname)) {
		echo "Joe, ongeldige listname: " . htmlspecialchars($listname);
		exit;
	}
	Page::getInstance()->start("Abonneren op $listname");

	if (!preg_match('/.+@.+\..+/',$email)) {
		echo "Oops, ongeldig adres: " . htmlspecialchars($email);
		exit;
	}

	sendmail($email, $listname.'-request@lists.A-Eskwadraat.nl'
			, 'subscribe', 'subscribe');

?>
<h2><?=_('Abonneren op')?> <?=htmlspecialchars($listname)?></h2>

<?=_('<p>Beste vriend,</p>

<p>Je bent nu <b>bijna</b> aangemeld voor de <i><?=htmlspecialchars($listname)?></i> mailinglist met e-mailadres <tt><?=htmlspecialchars($email)?></tt>.</p>

<p>Je krijgt op dat adres een mailtje waar je even op moet replyen (antwoorden).
Dan is alles toppie!</p>

<p>Groetjes, Mailman (postbode)</p>')?>

<?php
	Page::getInstance()->end();
}

