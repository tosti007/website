<?php
// $Id$

// sjablonen voor verschillende mailtjes...

define('BWMAIL_BOEKBINNEN', 1);
define('BWMAIL_BOEKALPOOSJEBINNEN', 2);
define('BWMAIL_BOEKVERLOPEN', 3);
define('BWMAIL_LEVERANCIER_BESTELLING', 4);
define('BWMAIL_LEVERANCIER_WIJZIGING_BESTELLING', 5);
define('BWMAIL_STUDENT_SPECIALE_BESTELLING', 6);
define('BWMAIL_SB_GEANNULEERD_VAN_HOGERHAND', 7);
define('BWMAIL_SB_VERTRAAGD_DOOR_VOORRAADPROBLEEM', 8);
define('BWMAIL_SB_HERINNERING_BESTELLING',9);
define('BWMAIL_BOEKENNIEUWS', 10);
define('BWMAIL_VERANDERD_BOEK_VAK', 11);
define('BWMAIL_TELLINGMELDING', 12);
define('BWMAIL_BESTELLING_TE_OUD', 13);
define('BWMAIL_BOEKCOM_BOOKWEBINFO', 14);
define('BWMAIL_DOCENT_BOEKENNIEUWS', 15);
define('BWMAIL_DOCENT_VERANDERD_BOEKVAK', 16);
define('BWMAIL_DOCENT_BOEKVAK_INFO', 17);
define('BWMAIL_DOCENT_VRIJEVERKOOP', 18);
define('BWMAIL_PM_REMIND', 19);
define('BWMAIL_WEBCIE_REMIND', 20);
define('BWMAIL_AANKOOP_BEVESTIGING', 21);

/**
	Stuurt een mail naar het gegeven lidnr, gebruik makend van template $what
	met data $data. Eventueel is het toegestaan om $lidnr = null mee te geven,
	dan wordt de mail niet naar buiten gestuurd maar bepaald de template (zie
	hieronder) waar de mail naar toe gaat (bijv. WebCie of BoekCom)
**/
function bwmail($lidnr, $what, $data)
{
	global $BESTUUR;

	$carboncopy = false;
	$carboncopy_label = $BESTUUR['boekencommissaris']->getEmailAdres();
	$printfooter = true;
	$boekcomname = BOEKCOMNAME;
	$boekemail = $BESTUUR['boekencommissaris']->getEmailAdres();
	$EJBVtime = strtotime(EJBV);
	// op de EJBV dag om 17:00 is het niet meer nodig om te waarschuwen dat de
	// op die dag geen reguliere verkoop meer is
	$EJBVtekst = (time() < ($EJBVtime+17*3600))?
		"\nOp " . strftime('%A %e %B', $EJBVtime)
		. " is de "
		. "boekverkoop uitsluitend geopend voor de eerstejaarsboekverkoop. \n"
		. "Er is die dag geen reguliere verkoop.\n":'';

	$boekvktijden = <<<HEREDOC
De boekverkoop is dagelijks geopend van 11.30 tot 13.30, in de eerste week
van elk blok zelfs van 10.45 tot 14.15.
$EJBVtekst
Je kunt de boekverkoop vinden op de tweede verdieping van het BBL tegenover
BBL-268.
Let op: je kunt alleen pinnen, dus niet chippen of contant betalen!
HEREDOC;

	$voorbehoud = <<<HEREDOC
* Kijk voor meer informatie op
https://www.a-eskwadraat.nl/Onderwijs/Boeken/Info/studvoorwaarden.html
HEREDOC;

	if ( !empty($data['EAN']) ) {
		$printisbn = " (ISBN: " . print_EAN($data['EAN']) . ")";
	} else {
		$printisbn = "";
	}

	if ($lidnr) {
		$email = $lidnr;
		if(is_numeric($lidnr)) {
			$naam = PersoonView::naam(Persoon::geef($lidnr));
		}
	}
	switch ($what) {

		case BWMAIL_BOEKBINNEN:
			$titel = "Boek is binnen";
			$prijsregel = ($data['prijs'] ?
			"De verwachte prijs is ongeveer $data[prijs] euro.*\n" : '');
			$tekst = <<<HEREDOC
Beste $naam,

Het door jou bestelde boek "$data[titel]" van
"$data[auteur]"$printisbn is binnen.
Je kunt het boek vanaf nu onder voorbehoud* ophalen tijdens de boekverkoop.
$prijsregel
Kom wel vóór $data[verlooptop], want vanaf die datum is je bestelling verlopen
en kan het boek aan iemand anders worden verkocht.

$boekvktijden

$voorbehoud
HEREDOC;
			break;

		case BWMAIL_BOEKALPOOSJEBINNEN:
			$titel  = 'Boek ligt nog tot en met '.$data['laatstedag']
				.' voor je klaar';
			$tekst = <<<HEREDOC
Beste $naam,

Het door jou bestelde boek "$data[titel]" van
"$data[auteur]"$printisbn is alweer eventjes binnen.
Je kunt het boek nog tot en met $data[laatstedag] onder voorbehoud*
ophalen tijdens de boekverkoop.

Daarna kun je het boek niet meer kopen, omdat het doorgeschoven wordt
naar iemand wiens boek nog niet binnen is! Dus wil je het nog hebben,
kom het dan kopen vóór $data[laatstedag].

Als je niet in staat bent het boek voor de verloopdatum op te komen halen,
stuur dan TIJDIG een reply met opgaaf van redenen. De boekencommissaris kan
dan het boek voor je verlengen.

Mocht je het boek niet (meer) willen, dan kun je de bestelling annuleren op
Bookweb. Zo schuift het boek door naar andere studenten voor wie het boek
nog niet binnen is.

$boekvktijden

$voorbehoud

HEREDOC;
			break;
		case BWMAIL_BOEKVERLOPEN:
		    $carboncopy = true;
			$titel  = 'Bestelling is verlopen';
			$tekst = <<<HEREDOC
Beste $naam,

Het door jou bestelde boek "$data[titel]" van
"$data[auteur]"$printisbn
is verlopen, omdat je het niet op tijd hebt opgehaald.


HEREDOC;

if ( $data['boekinvrijeverkoop'] ) {
	$tekst .= "Echter, het boek ligt nu in de vrije verkoop. Als je dus snel even\n" .
		"langs komt, is er een kans dat het nog niet verkocht is.\n";
} else {
	$tekst .= "Mocht je het boek alsnog willen hebben, dan zul je het opnieuw moeten
bestellen.\n";
}
			break;
		case BWMAIL_BESTELLING_TE_OUD:
			$titel  = 'Bestelling is geannuleerd';
			$tekst = <<<HEREDOC
Beste $naam,

Je hebt op $data[bestelddatum] het boek
"$data[titel]"
van "$data[auteur]"$printisbn
besteld, maar dit is helaas nog steeds niet geleverd.

Dit kan gebeuren omdat er bijvoorbeeld leveringsproblemen zijn
bij de uitgever van het boek, of dat het niet meer gedrukt wordt.
Omdat het al zo lang openstaat, achten we de kans niet zo groot
dat het binnenkort nog binnenkomt. Je bestelling is daarom
geannuleerd.

Als je het boek niet meer wilt hebben, hoef je niets te doen.
Mocht je het boek toch nog willen hebben, dan moet je even
reageren op dit bericht, en dan zullen we kijken wat de
mogelijkheden zijn.

HEREDOC;
			break;
		case BWMAIL_LEVERANCIER_BESTELLING:
			// zorg dat de mail naar boekcom@ gaat
			$email = $BESTUUR['boekencommissaris']->getEmailAdres();
			$naam = 'Boekencommissaris';

			$titel = "Bestelopdracht";
			$tekst = <<<HEREDOC
Beste $naam,

De volgende boeken moeten bij $data[leverancier] besteld worden.
Je kunt dit bericht forwarden naar $data[levmail] met als subject
het type van de bestelling (bestelling, nabestelling of eindbestelling)
en het blok nummer (1, 2, 3 of 4).

- - - - - - - - K N I P - - - - - - - - - -

Beste $data[levcontactpersoon],

De volgende boeken behoren tot de bestelling:

(ISBN/EAN) ; (titel) ; (auteur) ; (uitgever) ; (aantal)

HEREDOC;

	foreach ($data['boeken'] as $info) {
		$tekst .= "$info[EAN] ; $info[titel] ; ";
		$tekst .= "$info[auteur] ; $info[uitgever] ; $info[aantal]\n";
	}

	$tekst .= <<<HEREDOC

Een bestelling hebben we graag maximaal vrijdag voor het begin van het blok
binnen. Een nabestelling hebben we graag maximaal de vrijdag voor twee weken
na het begin van het blok binnen. Een eindbestelling hebben we graag maximaal
de vrijdag voor zes weken na het begin van het blok binnen.

Er zijn vier blokken per jaar.

HEREDOC;


			break;
		case BWMAIL_LEVERANCIER_WIJZIGING_BESTELLING:
			$wijziger = PersoonView::naam(Persoon::geef($data['wie']));
			$titel = "Gewijzigde bestelopdracht";
			$televeren = $data['newbesteld'] - $data['geleverd'];
			$tekst = <<<HEREDOC
Beste Boekencommissaris,

$wijziger heeft een wijziging ingevoerd in BookWeb voor een bestelling.
Om de wijziging ook daadwerkelijk door te laten voeren, moet je
$data[leverancier] nog op de hoogte brengen.

Het betreft de volgende bestelling:
ISBN:        $data[EAN]
Titel:       $data[titel]
Auteur:      $data[auteur]
Druk:        $data[druk]
Uitgever:    $data[uitgever]
Besteldatum: $data[besteldatum]

Er waren $data[besteld] exemplaren besteld (inmiddels zijn er $data[geleverd]
geleverd) en $wijziger heeft dit veranderd in $data[newbesteld].
BookWeb verwacht nu dus nog $televeren exemplaren van deze bestelling.

HEREDOC;

			// zorg dat de mail naar boekcom@ gaat
			$email = $BESTUUR['boekencommissaris']->getEmailAdres();
			$naam = 'Boekencommissaris';
			break;
		case BWMAIL_STUDENT_SPECIALE_BESTELLING:
			$titel = "Speciale studentbestelling";
			$email = $lidnr;
			$lidmailadres = Persoon::geef($lidnr)->getEmail();
			$tekst = <<<HEREDOC

Beste Boekencommissaris,

Er is een speciaal boek besteld.

Naam:		$naam
Lidnummer:	$lidnr
Email:		$lidmailadres
Titel:		$data[titel]
Auteur:		$data[auteur]
Druk:		$data[druk]
Uitgever:	$data[uitgever]
ISBN:		$data[ISBN]
Opmerking:  $data[opmerking]

LET OP: over deze bestelling is niks in bookweb opgeslagen.
Je moet nu zelf achter dit boek aangaan en het in 'boeken' zetten,
EN voor deze student het boek bestellen. Laat de student even
weten als je het boek besteld hebt en als je een prijs weet.

HEREDOC;

			// zorg dat de mail naar boekcom@ gaat
			$email = $BESTUUR['boekencommissaris']->getEmailAdres();
			$naam  = "Boekencommissaris A-Eskwadraat";

			break;
		case BWMAIL_SB_GEANNULEERD_VAN_HOGERHAND:
			$titel = 'Bestelling is geannuleerd';
			$tekst = <<<HEREDOC
Beste $naam,

Jouw bestelling van het boek "$data[titel]" van "$data[auteur]"
$printisbn is zojuist afbesteld door
$data[afbesteller].

Dit had de volgende reden:
$data[reden]

HEREDOC;
			break;
		case BWMAIL_SB_VERTRAAGD_DOOR_VOORRAADPROBLEEM:
			$titel = 'Bestelling is vertraagd';
			$tekst = <<<HEREDOC
Beste $naam,

Jouw bestelling van het boek "$data[titel]" van
"$data[auteur]"$printisbn is op dit
moment helaas niet leverbaar ondanks het eerdere bericht dat je bestelling
binnen was. Dit komt doordat er geen boeken van deze titel meer op voorraad
zijn*. Zodra er weer boeken binnenkomen, krijg je hiervan bericht en kun je je
boek alsnog komen ophalen.

Excuses voor het ongemak.

$voorbehoud

HEREDOC;
			break;
		case BWMAIL_SB_HERINNERING_BESTELLING:
			$titel = 'Herinnering bestelling';
			$tekst = <<<HEREDOC
Beste $naam,

Je hebt het boek $data[titel]
van $data[auteur]$printisbn bij ons besteld.

Zeer binnenkort zal dit boek bij de leverancier besteld worden. Vanaf dat
moment is de bestelling aan jou verbonden en is het de bedoeling dat je het
boek koopt wanneer het geleverd wordt.

Wanneer je niets doet zal het boek gewoon voor jou besteld worden.

Mocht je het boek niet meer willen hebben, dan verzoek ik je vriendelijk om
vóór $data[besteldatum] de bestelling van het boek via Bookweb te annuleren. Op
deze manier hoeven wij geen boeken bij onze leverancier te bestellen die
uiteindelijk niet afgehaald worden. De bestelling annuleren kan via deze link:
http://www.A-Eskwadraat.nl/Onderwijs/Boeken/Bestellingen/$data[bestelnr]

HEREDOC;
			break;
		case BWMAIL_BOEKENNIEUWS:
			$titel = 'Nieuws over boekbestelling';
			$tekst = <<<HEREDOC
Beste $naam,

Je hebt het boek $data[titel] van
$data[auteur]$printisbn besteld.
Er is het volgende nieuws over je bestelling:

$data[nieuws]

HEREDOC;
			break;
		case BWMAIL_VERANDERD_BOEK_VAK:
			$carboncopy = true;
			$titel = 'Boek gewijzigd';
			$tekst = <<<HEREDOC
Beste $naam,

Ter informatie, het door jou bestelde boek voor het vak $data[vak]
$data[oldboek]
is vervangen door het boek
$data[newboek].

Dit heeft de volgende reden:
$data[reden]

HEREDOC;
			break;
		case BWMAIL_TELLINGMELDING:
			$naam  = BOEKCOMNAME;
			$email = $BESTUUR['boekencommissaris']->getEmailAdres();
			$titel = 'tellingmelding';
			$tekst = <<<HEREDOC
Beste $naam,

Bij de BookWeb-telling op $data[wanneer] door $data[wie] is het
volgende geschied:

$data[probleem]

HEREDOC;
			break;
		case BWMAIL_BOEKCOM_BOOKWEBINFO:
			// zorg dat de mail naar boekcom@ gaat
			$email = $BESTUUR['boekencommissaris']->getEmailAdres();
			$naam = 'Boekencommissaris';

			$titel = "Belangrijke informatie";
			$tekst = <<<HEREDOC
Beste $naam,

Bookweb heeft informatie voor je waar je iets mee moet doen:


HEREDOC;
			foreach ($data as $regel) {
				$tekst .= "* $regel\n";
			}
			break;
		case BWMAIL_DOCENT_BOEKENNIEUWS:
			$carboncopy = true;
			$titel = 'Nieuws over boek van een vak dat u geeft';
			$verplicht = ($data['verplicht'] == 'Y'?'':'niet ').'verplicht bij vak';
			$tekst = <<<HEREDOC
Geachte docent,

Volgens onze gegevens doceert u dit jaar het vak '$data[vak]'.
Voor dit vak is het boek '$data[titel]' van
$data[auteur] ($verplicht) bestelbaar via onze website.
Over dit boek is het volgende nieuws (dit is ook gemaild naar alle studenten
die dit boek voor uw vak bij ons in bestelling hebben):

$data[nieuws]

HEREDOC;
			break;
		case BWMAIL_DOCENT_VERANDERD_BOEKVAK:
			$titel    = 'Wijziging boek voor een vak dat u geeft';
			$druk_oud = $data['druk_oud'] ? " (druk: $data[druk_oud])" : '';
			$druk     = $data['druk'] ? " (druk: $data[druk])" : '';
			$isbn_oud = print_EAN($data['EAN_oud']);
			$isbn     = print_EAN($data['EAN']);
			$tekst    = <<<HEREDOC
Geachte docent,

Volgens onze gegevens doceert u dit jaar het vak $data[vak].
Voor dit vak was het boek $data[titel_oud] van
$data[auteur_oud]$druk_oud (ISBN: $isbn_oud)
bestelbaar via onze website.

Zojuist is door de Boekencommissaris dit boek vervangen door
$data[titel] van $data[auteur]$druk
(ISBN: $isbn). Als reden is hiervoor opgegeven:
$data[reden]

Dit is ook gemaild naar alle studenten die dit boek voor uw vak
bij ons in bestelling hebben.

HEREDOC;
			break;
		case BWMAIL_DOCENT_VRIJEVERKOOP:
			$titel = 'Boeken bij uw vak direct leverbaar';
			$tekst = <<<HEREDOC
Geachte docent,

Volgens onze gegevens doceert u momenteel het vak "$data[vaknaam]".
Zoals u waarschijnlijk weet, hebben studenten voor dit vak het boek
"$data[boektitel]" bij A-Eskwadraat besteld.

Echter, op dit moment zijn nog niet alle boeken door studenten gekocht, er
liggen nog wat exemplaren van deze titel in de vrije verkoop. Mochten er
nog studenten zijn die het boek willen aanschaffen, dan kunnen zij het boek
dus direct (zonder te bestellen) bij A-Eskwadraat komen kopen.
Zou u dit in het college willen melden aan de studenten? Het gebeurt
namelijk vaak dat studenten het boek nog wel willen hebben, maar niet weten
dat er nog exemplaren bij A-Eskwadraat op voorraad liggen.
Zo proberen we te voorkomen dat we boeken terugsturen waar
studenten nog belangstelling voor hebben.
Alvast hartelijk dank voor uw medewerking!

HEREDOC;
			break;
		case BWMAIL_DOCENT_BOEKVAK_INFO:
			$numvakken = count($data);
			$vakken = ($numvakken == 1 ? 'vak':'vakken');
			$titel = "Gegevens die A-Eskwadraat van uw $vakken heeft";
			$tekst = <<<HEREDOC
Geachte docent,

Volgend blok doceert u volgens ons $numvakken $vakken, namelijk:
HEREDOC;
			foreach ($data as $vak) {
				$tekst .= "\n$vak[vak] ($vak[afkorting]).\n";

				if (isset($vak['boeken']) && sizeOf($vak['boeken']) > 0) {
					$tekst .= "Voor dit vak zijn bij ons de volgende boeken bekend:\n";
				} else {
					$tekst .= "* Voor dit vak zijn bij ons geen boeken bekend.\n";
				}

				foreach ($vak['boeken'] as $boekvakinfo) {
					$druk = $boekvakinfo['druk'] ? "\n  druk: $boekvakinfo[druk]" : '';
					$tekst .= '* \''.$boekvakinfo['titel']."' van '"
						.$boekvakinfo['auteur'].'\''.$druk
						."\n".'  ISBN: '.print_EAN($boekvakinfo['EAN'])
						."\n".'  Dit boek is ' .($boekvakinfo['verplicht']=='Y'?'':'NIET ')
						.'VERPLICHT voor dit vak.'."\n";
				}

				if ($vak["dictaat"] == "Y"){
					if (isset($vak['boeken']) && sizeOf($vak['boeken']) > 0){
						// Naast boeken ook dictaat
						$tekst .= "* Naast bovenstaand(e) boek(en) wordt er " .
							"volgens onze gegevens\n  bij dit vak ook een " .
							"dictaat* gebruikt.\n";
					} else {
						// Enkel dictaat
						$tekst .= "* Volgens onze gegevens wordt er bij ".
							"dit vak een dictaat* gebruikt.\n";
					}
				} else {
					$tekst .= "* Volgens onze gegevens wordt er bij dit " .
						"vak geen dictaat* gebruikt.\n";
				}
			}

			$tekst .= <<<HEREDOC

* Met een dictaat wordt ondersteunend materiaal bedoeld dat door u is
samengesteld en door A-Eskwadraat kan worden gedrukt en dat aan de studenten wordt
aangeboden bij onze boekverkoop.

Ik zou u vriendelijk willen verzoeken mij uiterlijk 16 februari om 23:59 te laten weten
of deze informatie correct is door te reageren op deze mail.

Wanneer u gebruik maakt van een dictaat, zou ik u willen vragen uw dictaat uiterlijk 9 april te uploaden via https://www.a-eskwadraat.nl/dictaat of naar mij te mailen in pdf-formaat. Van een aantal dictaten hebben wij ruime hoeveelheden over. Om papierverspilling enigszins tegen te gaan hoor ik graag of oude dictaten nog gebruikt kunnen worden.

Verder zou ik u nog willen vragen of u op uw vaksite zou kunnen noemen dat de studenten de dictaten voor uw vak bij A-Eskwadraat kunnen kopen en de boeken op de webshop van A-Eskwadraat kunnen bestellen (https://aeskwadraat.itdepartment.nl/home).

Mochten er problemen of onduidelijkheden zijn, laat u het mij dan vooral weten. Hartelijk dank!

HEREDOC;
			break;
		case BWMAIL_PM_REMIND:
			$titel = "Herinnering advertenties PapierMolen";
			$tekst = <<<HEREDOC
Hoi,

Je hebt op de PapierMolen, de tweedehands boekenmarkt van A-Eskwadraat,
een of meerdere advertenties geplaatst die inmiddels langer dan drie
maanden openstaan:

HEREDOC;

			foreach ($data as $row) {
				$tekst .= "\n* ISBN $row[EAN]:\n  $row[auteur]\n  $row[titel]\n";
				$tekst .= "  geplaatst: " .
					substr($row['wanneer'], 0, 10) . ", vervalt: " .
					substr($row['verloopdatum'], 0, 10) . "\n";
			}

			$tekst .= <<<HEREDOC

Mocht een aanbieding inmiddels niet meer relevant zijn, dan kun je deze
via de website eenvoudig intrekken. Op onze website vind je een
overzicht van al je aanbiedingen:
http://www.A-Eskwadraat.nl/Onderwijs/Boeken/Papiermolen/Zoeken

Als je verder geen actie onderneemt, blijft de advertentie staan totdat
bovenstaande vervaldatum verstreken is.

HEREDOC;
			break;
		case BWMAIL_WEBCIE_REMIND:
			$carboncopy = true;
			$email = 'www@a-eskwadraat.nl';
			$naam = 'World Wide WebCie';
			$titel = "herinnering";
			$tekst = <<<HEREDOC
Geachte leden der World Wide WebCie,

Er zijn een of meerdere zaken in BookWeb die jullie aandacht vereisen, deze
automatisch gegenereerde e-mail attendeert jullie daarop. Het betreft het
volgende:


HEREDOC;
			foreach ($data as $regel) {
				$tekst .= wordwrap("* $regel\n", 75, "\n  ");
			}

			$tekst .= <<<HEREDOC

Heel veel succes met het afhandelen van het bovenstaande.
HEREDOC;
			break;
		case BWMAIL_AANKOOP_BEVESTIGING:
			$titel = "Aankoopbevestiging";
			$datumverkocht = date("d-m-Y") . " om " . date("H:i:s");
			$datesold = date("d-m-Y") . " on " . date("H:i:s");
			$producten = $data['aantal'] > 1 ? "producten" : "product";
			$products = $data['aantal'] > 1 ? "products" : "product";
			$carboncopy = true;
			$printfooter = false;
			$carboncopy_label = "boekcom-verkoopbewijzen@a-eskwadraat.nl";
			$tekst = <<<HEREDOC
Beste $naam,

Hierbij de bevestiging voor je gekochte $producten op $datumverkocht:

$data[producten]
Betaalwijze: $data[betaalwijze]
Aankoop door: $naam ($lidnr)

Dit e-mailbericht geldt als aankoopbewijs van je gekochte $producten en kan worden gebruikt bij het inruilen. Neem in dat geval een afdruk van dit e-mailbericht mee.

Inruilvoorwaarden
Er worden in principe geen boeken teruggekocht, hierbij kan in uitzondering van afgeweken worden.
Een misdruk kan omgeruild worden voor een goed exemplaar, maar alleen binnen acht dagen en op vertoon van dit bewijs. 

Met vriendelijke groeten,

	$boekcomname

Boekencommissaris A-Eskwadraat ($boekemail)

(dit bericht is automatisch aangemaakt door BookWeb)


***English***


Dear $naam,

This is the confirmation of your purchase(s) on $datesold:

$data[products]
Payment method: $data[betaalwijze]
Purchased by: $naam ($lidnr)

This email serves as proof of purchase of your purchased $products and can be used to redeem. In that case, you'll need a print of this email.

Redeem policy
In principle, no books will be redeemed, in some cases exceptions can be made.
A misprint can be exchanged for a good copy, within eight days by showing this proof of purchase.

Best regards,

	$boekcomname

Book Officer A-Eskwadraat ($boekemail)

(this e-mail was automatically generated by BookWeb)

HEREDOC;
			break;

		default:
			user_error("Oeps, onbekende what van bwmail", E_USER_ERROR);
	}

	if(!DEBUG) {
		$fp = fopen('/var/log/www/bookwebmails.log', 'a');
		fwrite($fp, "==========\n".strftime('%Y-%m-%d %H:%M:%S').
			"\nTo-Lidnr: $lidnr" .
			"\nSubject: $titel\n$tekst");
		fclose($fp);
	}

	if($printfooter)
		$tekst = <<<HEREDOC
$tekst

Met vriendelijke groeten,

	$boekcomname

Boekencommissaris A-Eskwadraat ($boekemail)

(dit bericht is automatisch aangemaakt door BookWeb)

HEREDOC;

	$from = $BESTUUR['boekencommissaris']->getEmail();

	$voorzetsel = "";
	if(BWMAIL_DOCENT_BOEKVAK_INFO !== $what)
	{
		$voorzetsel = "Bookweb: ";
	}
	else
	{
		//$voorzetsel = "Reminder:";
	}
	sendmail($from, $email, $voorzetsel.$titel, $tekst, null, $carboncopy ? "Bcc: ".$carboncopy_label : null, $BESTUUR['boekencommissaris']->getEmailAdres());


}
