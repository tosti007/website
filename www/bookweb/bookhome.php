<?php // vim: ts=4 sw=4 syntax=php tw=78
// $Id$

/**

Deze file zorgt voor het hoofdmenu van BookWeb. Afhankelijk van het level
worden er meer of minder functies in het menu gezet. De afhandeling en
authenticatie van deze functies wordt centraal geregeld in dispatch.php

**/

function hoofdmenu()
{
	global $BESTUUR;

	echo '<p>'
		._('Welkom op het boekbestelsysteem van A-Eskwadraat!') . '</p>';
	if (!hasAuth('verkoper')) {
		echo _("<p>Als je vragen hebt over de boekverkoop,-bestellingen of over "
			."boeken in het algemeen lees eerst de "
			.makeRef("/boekcom","informatie en voorwaarden boekverkoop")
			.". Als je vragen hebt over het inloggen, logins, passwords e.d., "
			."mail dan de ".makeRef("mailto:" . $BESTUUR['secretaris']->getEmailAdres(), 'Secretaris')
			.". Heb je dan nog vragen over Bookweb mail dan de "
			.makeRef("mailto:".$BESTUUR['boekencommissaris']->getEmailAdres(),"boekencommissaris").".</p>"
			);
	}
	echo "<p>"._("Je kunt nu de volgende dingen doen:")."</p><div
class='row'> ";

//verkoper-opties
	if (hasAuth('verkoper')) {
		echo "<div class='col-md-6'><h3 class='page-header'>" . _("Voor de
verkopers:") . "</h3><ul>";

		$verkoopopen = tryVerkoopOpen();  // query once, use many
		// dit is alleen relevant als de verkoop niet open is
		// check of er telling geforceerd moet worden.
		// in debugsite is dat niet wenselijk
		if(!$verkoopopen && !DEBUG) {
			$forcetelling = sqlForceTelling();
		} else {
			$forcetelling = FALSE;
		}

		echo "\n<li class='list-group-item'>"
			.makeCondBookRef('openverkoop',_('Open boekverkoop'),$verkoopopen,true)
			."\n<li class='list-group-item'>"
			.makeCondBookRef('verkoop',_('Verkoop-menu'),!$verkoopopen, true);

		echo "\n<P>\n<li class='list-group-item'>"
			. makeCondBookRef('toonverkopen', _('Toon alle transacties van ')
			. (isActionDateTimeChanged() ? _('de geselecteerde dag')
				: _('vandaag'))
			. ($verkoopopen?'':_(' (en corrigeer eventueel de eindedagwaardes)')), false, true);

		$verkopers = sqlGetDagVerkopers(getActionDate());

		if (sizeOf($verkopers) > 1) {
//			getLedenNames($verkopers);
			echo '<br />(<span class="smallerfont">';
			$links = array();
			foreach ($verkopers as $verkoper) {
				$links[] = makeCondBookRef('toonverkopen',
					_('alleen van ') . PersoonView::naam(Persoon::geef($verkoper)), false,
					true, 'verkopers[]='.$verkoper
					);
			}
			echo implode (', ', $links);
			echo ')</span>';
		}


		echo "\n<li class='list-group-item'>"
			.makeCondBookRef('toonverkopenperproduct',_('Toon verkopen per
			product'), $forcetelling);
		echo "\n<li class='list-group-item'>"
			.makeCondBookRef('verkoopoverzicht',_('Toon verkoopoverzicht'));

		echo "\n<p>";

		echo "\n<li class='list-group-item'>"
			.makeCondBookRef('teverkopenbestellingen',_('Alle '
			.'studentenbestellingen die verkocht kunnen worden '));

		echo "\n<li class='list-group-item'>"
			.makeCondBookRef('kasverschillen',_('Toon kasverschillen van vandaag'),
				!$verkoopopen, TRUE);
		echo "\n<li class='list-group-item'>"
			.makeCondBookRef('sluitverkoop',_('Sluit boekverkoop'),!$verkoopopen,true);

		echo "\n<P>\n<li class='list-group-item'>"
			.makeCondBookRef('telling', 'Doe een boekentelling',false,true)
."\n<li class='list-group-item'>"
			.makeCondBookRef('boekinfo','Boek-geori&euml;nteerde informatie (BoekInfo)', $forcetelling);

		echo "</UL></div>";
	} else {
		$forcetelling = false;
	}

	if (hasAuth('verkoper'))
		printHTML("<h3 class='page-header'>" . _('Voor leden en verkopers:')
. '</h3>');

//lid-opties
	if (hasAuth('bijnalid')) {
		echo "<div class='col-md-6'>\n<ul class='list-group'>\n<li class='list-group-item'>".makeRef("Bestelling",_('Boeken bestellen'));
		echo "\n<li class='list-group-item'>".makeRef("Bestellingen"
			,_('Bestellingsoverzicht / bestelling annuleren / nieuws'));

		echo "\n<P>\n<li class='list-group-item'>".makeRef('alleBoekenOpenbaar'
			,_('Alle artikelen die direct verkrijgbaar zijn zonder te bestellen'));

	}
	echo "</UL> </div></div>";

//boekcom-opties
    if(hasAuth('boekcom')) {
		echo "<h2 class='page-header'>Voor de boekencommissaris:</h2>";

		if($forcetelling) {
			echo "<em>Helaas, er is deze maand nog geen telling gedaan, dus
			zijn sommige functies tijdelijk uitgeschakeld. Tel nauwkeurig, want dat is
			essentieel voor de afrekening!</em><br/>";
		}

		echo '<div class="row">';
		
		// Algemene functies
		echo '	<div class="col-md-3"><ul class="list-group"><h3
class="page-header">Algemeen </h3><li
class="list-group-item">'
			.makeCondBookRef('boekenlijst','Boekenlijsten')
			. '<li class="list-group-item">'
			.makeCondBookRef('geschiedenis','Geschiedenis boekverkopen')
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('offerteproblemen', 'Ontbrekende offertes')
			. '<li class="list-group-item">'
			.makeCondBookRef('docentenboekenmail', 'Docenten mailen')
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('financieel','Financi&euml;le administratie')
			. '<li class="list-group-item">'
			.makeCondBookRef('export','Bestanden uitvoeren')
			. '<li class="list-group-item">'
			.makeCondBookRef('specialconstants','Special Constants')
			. '<li class="list-group-item">'
			.makeCondBookRef('stats', 'Statistieken bekijken');
		echo '		</ul></div>';

		// Voorraad
		echo '	<div class="col-md-3">
<h3 class="page-header"> Voorraad </h3>
<ul class="list-group">
<li class="list-group-item">'
			.makeCondBookRef('totalevoorraad','Totale voorraad', $forcetelling)
			. '<li class="list-group-item">'
			.makeCondBookRef('offerte','Offerte invoeren', false,true)
			. '<li class="list-group-item">'
			.makeCondBookRef('verplaatsing','Artikelen verplaatsen',
				$forcetelling,true)
			. '<li class="list-group-item">'
			.makeCondBookRef('annuleren','Annuleringen bekijken', false,true);
		echo '</ul></div>';

		// Bestellingen en leveringen
		echo '	<div class="col-md-3">
<h3 class="page-header"> Bestellingen en Leveringen </h3>
<ul class="list-group">
<li class="list-group-item">'
			.makeCondBookRef('levering','Levering invoeren', $forcetelling,true)
			. '<li class="list-group-item">'
			.makeBookRef('Showleveringen','Leveringen tonen')
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('retour', 'Retour invoeren',
			$forcetelling,true)
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('tebestellen','Te bestellen boeken')
			. '<li class="list-group-item">'
			.makeCondBookRef('televeren','Te leveren boeken')
			. '<li class="list-group-item">'
			.makeCondBookRef('updatebestellingen', 'ISBNs van bestellingen wijzigen')
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('leveranciers', 'Leveranciers bekijken');
		echo '		</ul></div>';

		// Artikelen
		echo '	
<div class="col-md-3">
<h3 class="page-header"> Artikelen </h3>
<ul class="list-group">
<li class="list-group-item">'
			.makeCondBookRef('insboek','Artikel invoeren')
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('pakket','Pakket invoeren')
			. '<li class="list-group-item">'
			.makeCondBookRef('pakketten', 'Pakketten bekijken')
			. '<li class="list-group-item"><br/>'
			.makeCondBookRef('insvak', 'Vak invoeren')
			.'</ul></div>';

	    echo '</div>'; //closing class=row

		//algemeen
		echo ' <div class="row">';
		echo '<h2 class="page-header">Afrekeningen en andere financi&euml;le
zaken</h2>';
		echo '	<div class="col-md-3">
<h3 class="page-header"> Algemeen </h3>
<ul class="list-group"><li class="list-group-item">'
			. makeBookRef('afrekening', 'Grootboeken')
			. '</ul></div>';

		//rekeningafschriften
		echo '	<div class="col-md-3">'
. '<h3 class="page-header"> Afschriften </h3>'
. '<ul class="list-group"> <li class="list-group-item">'
			. makeBookRef('financieel', 'Opvragen', 'fin_action=afschriften&fin_subaction=list')
			. '<li class="list-group-item">'
			. makeBookRef('financieel', 'Invoeren', 'fin_action=afschriften&fin_subaction=edit')
			. '</ul></div>';

		// facturen
		echo '	<div class="col-md-3">
<h3 class="page-header"> Facturen </h3>
<ul class="list-group">
<li class="list-group-item">'
			. makeBookRef('financieel', 'Opvragen', 'fin_action=facturen&fin_subaction=list')
			. '<li class="list-group-item">'
			. makeBookRef('financieel', 'Invoeren', 'fin_action=facturen&fin_subaction=edit')
			. '<li class="list-group-item">'
			. makeBookRef('financieel', 'Uitgebreide controle', 'fin_action=facturen&fin_subaction=factuuroverzicht')
			.'</ul></div></div>';

    }

//god-opties
	if (hasAuth('god')) {
		echo "<br/><strong>Voor gods:</strong>\n<ul class='list-group'>";
		echo "\n<li class='list-group-item'>".makeCondBookRef('processTo4', 'Process To 4')
		    ."\n<li class='list-group-item'>".makeCondBookRef("test","Testcall")
			."\n<li class='list-group-item'>".makeCondBookRef("vars","Toon gebruikte variabelen");
 		echo "\n</UL>";
	}

	bookweb_levelswitch();
}
