<?php
/**
	File met speciale Bookweb-forms (bijv. lijst met leveranciers in mooi
	tabelletje.
	Dit als toevoeging voor de standaard form-functies in space/forms.php.

	Alle functies beginnen met bwf ("BookWeb Form")
**/

function bwfAddSelectLeveranciers($fieldname = "leveranciernrs", $multiselect = true){
	$selected = tryPar($fieldname);

	if ($selected == null) $selected = array(BOEKLEVNR);

	$leveranciers = sqlLeveranciers();
	foreach ($leveranciers as $leverancier){
		$leverancierNamen[$leverancier["leveranciernr"]] = $leverancier["naam"];
	}

	return addSelect("$fieldname" . "[]", $leverancierNamen, $selected,
	true, false, $multiselect);
}

/**
	Print een tabelletje met header en daaronder een lijst met alle
	leveranciers uit de database. Uiteraard is de standaardleverancier
	default geselecteerd.
**/
function bwfLeveranciers(
		$fieldname = "leveranciernrs", $multiselect = true){
	$res = "<table width=\"100%\" class=\"bw_datatable_medium\">\n" .
		"	<tr><th>\n" .
		"		Leveranciers:" .
		"	</th></tr>\n" .
		"	<tr>\n" .
		"		<td>\n" . bwfAddSelectLeveranciers($fieldname, $multiselect) .
		"		</td>\n" .
		"	</tr>\n" .
		"</table>\n";

	return $res;
}


