<?php
// $Id$

/*

Functies voor de financiŽle afwikkeling voor de boekencommissaris, zoals de
'print afrekening'-knop, het maandoverzicht (met welke dagen wel en niet
correct zijn volgens tellingen en geregistreerde transacties, alsmede
totalen), het dagoverzicht (de verkopen van die dag, de geld-transacties van
die dag, en de voorraadtellingendiff, en het totaal van de pintransacties, en
de evt. discrepanties tussen deze zaken), en meer.

Voor de overzichtelijkheid beginnen alle functienamen met 'fin'

*/

// deze functie wordt rechtstreeks vanuit dispatch aangeroepen
function finDispatch()
{
	switch (tryPar('action')) {
		case 'week':
			$dag = requirePar('dag');
			$maand = requirePar('maand');
			$jaar = requirePar('jaar');
			if ( $maand < COLJAARSWITCHMAAND ) {
				$jaar++;
			}

			// $begin is de maandag van deze week
			$time = cal_getLastMonday(mktime(0,0,0,$maand,$dag,$jaar));

			$begin = strftime('%Y-%m-%d', $time );

			$eind = strftime('%Y-%m-%d', cal_AddDays($time, 7) );
			$vorigeweek   = getdate(cal_AddDays($time, -7));
			$volgendeweek = getdate(cal_AddDays($time, 7));
			echo '<table width="100%"><tr><td>'.
				makeBookRef("afrekening&action=week&dag=$vorigeweek[mday]&"
					."maand=$vorigeweek[mon]&jaar=$vorigeweek[year]",
					'&lt;&lt;&lt; Vorige week').
				'</td><td align=right>'.
				makeBookRef("afrekening&action=week&dag=$volgendeweek[mday]&"
					."maand=$volgendeweek[mon]&jaar=$volgendeweek[year]",
					'Volgende week &gt;&gt;&gt;').
				"</td></table>\n";
			echo strftime("<h2>Weekoverzicht van week %V", $time);
			echo strftime(" (%A %e %B", $time);
			// t/m zondag. Zou misschien cal_getSunday moeten gebruiken?
			echo strftime(" - %A %e %B %Y)</h2>", cal_AddDays($time, 6) );
 			finShowOverzicht($begin, $eind, 'dag', 'alle', TRUE, TRUE);
			break;
		case 'maand':
			$maand = requirePar('maand');
			$jaar = requirePar('jaar');
			if ( $maand < COLJAARSWITCHMAAND ) {
				$jaar++;
			}

			$begin = strftime('%Y-%m', $time = mktime(0,0,0,$maand,1,$jaar));
			echo strftime("<h2>Maandafrekening van %B %Y</h2>\n", $time);
			if ((int)strftime('%m', $time) != $maand) {
				print_error('Ongeldige maand: '.$maand);
			}
			finShowOverzicht("$begin-01", "$begin-15", 'maand', 'een', TRUE,
				TRUE);
			break;
		case 'jaar':
			echo "<h2>Jaaroverzicht boekjaar ".
				$GLOBALS['COLJAREN'][requirePar('jaar')].
				"</h2>\n";
			$jaar = requirePar('jaar');
			$begin = strftime('%Y-%m-%d', mktime(0,0,0,8,1,$jaar));
			$eind = strftime('%Y-%m-%d', mktime(0,0,0,7,31,$jaar+1));
			finShowOverzicht($begin, $eind, 'maand', 'een', TRUE, TRUE);
			break;
		case 'custom':
			$begin = requirePar('begin');
			$eind = requirePar('eind');
			echo "<h2>Overzicht van " .
				print_date($begin) . ( $begin != $eind ? " tot aan " . print_date($eind) : "" ) .
				"</h2>\n";
			$showtrans = tryPar('showtrans')?
				(array_sum(tryPar('showtrans')) > 0):
				FALSE;
			finShowOverzicht($begin, $eind,
				requirePar('resolutie'), tryPar('toontellingen'), $showtrans,
				tryPar('toongelden', FALSE));
			break;
		case 'putcsv':
			finPutCSV(
				requirePar('van'),
				requirePar('tot'),
				requirePar('aggregatie'));
			break;
		default:
			finAfrekeningHome();
		return;
	}
}			// finDispatch

function finAfrekeningHome()
{
	echo "<h3>Overzicht</h3>\n";
	startForm(null, null, null, 'get');
	echo '<tr><td>Per:</td><td>'
		.'<input type="radio" name="action" value="week" id="week"><label for="week">Week</label> '
		.'<input type="radio" name="action" value="maand" id="maand" checked="checked"><label for="maand">Maand</label> '
		.'<input type="radio" name="action" value="jaar" id="jaar"><label for="jaar">Jaar</label>'
		."</td></tr>\n";
	inputText('Dag', 'dag', NULL, NULL, 2);
	echo "<tr><td>Maand:</td><td>".
		addSelect('maand', array('','jan','feb','mrt','apr','mei','jun',
			'jul','aug','sep','okt','nov','dec'), NULL, TRUE)."</td></tr>\n";
	echo '<tr><td>Collegejaar:</td><td>'
		.addSelect('jaar', $GLOBALS['COLJAREN'],colJaar(),true)
		."</td></tr>\n";
	endForm('OK');

	echo "<h3>Anders</h3>\n";
	simpleForm('custom', null, null, 'GET');
	echo "<tr><td>Resolutie:</td><td>\n";
	echo addSelect('resolutie', array('dag','week','maand'),
		tryPar('resolutie'));
	echo "</td><td>(mate van detail)</td></tr>\n";
	inputText('Begin', 'begin', tryPar('begin'), '(JJJJ-MM-DD, vanaf)');
	inputText('Eind', 'eind', tryPar('eind'), '(JJJJ-MM-DD, tot aan)');
	echo "<tr><td>Toon tellingen:</td><td colspan=2>\n";
	echo addRadioBox('toontellingen', FALSE, 'geen').'<label for="toontellingengeen">Geen</label> ';
	echo addRadioBox('toontellingen', TRUE, 'een').'<label for="toontellingeneen">Eerste van elke resolutie</label> ';
	echo addRadioBox('toontellingen', FALSE, 'alle').'<label for="toontellingenalle">Allemaal</label>';

	// mogelijkheid alleen bepaalde transacties te zien
	// TODO: zorgen dat het ook niet gequeried wordt.
	echo "</td></tr>\n<tr><td>Toon transacties:</td><td colspan=2>\n";
	echo addCheckBox('showtrans[verkoop]', '1', TRUE) . '&nbsp;<label for="showtrans_verkoop_">verkopen</label> ';
	echo addCheckBox('showtrans[terugkoop]', '1', TRUE) . '&nbsp;<label for="showtrans_terugkoop_">terugkopen</label> ';
	echo addCheckBox('showtrans[levering]', '1', TRUE) . '&nbsp;<label for="showtrans_levering_">leveringen</label> ';
	echo addCheckBox('showtrans[retour]', '1', TRUE) . '&nbsp;<label for="showtrans_retour_">retouren</label>' ;
	echo "</td></tr>\n<tr><td><label for=\"toongelden\">Toon geldzaken</label>:</td><td colspan=2>\n";
	echo addCheckBox('toongelden', '1', TRUE);
	echo "</td></tr>\n";
	endForm('OK', FALSE);

}

// Laat een megatabel zien waarop je de voorraadwijzigingen van de
// verschillende boeken door de maand heen kunt zien, en ook precies waar een
// discrepantie is.

// Links staan alle boeken (sommige boeken meer dan 1x, indien er iets met
// voorradenen van verschillende prijzen is gebeurt), waarvan enige
// registratie in bw gevonden is voor die maand. Naast het boek staat de prijs

// Boven staan alle dagen van de maand

// in elke cel staan N tellingen (0 of meer) voor dat boek die dag, tussen {
// en }. Deze telling is INCL buitenvk, en is dus netjes ge-extrapoleerd uit de
// gewone telling, en een oude telling wat er in buitenvk lag. (als er boeken bij
// buitenvk liggen, moet dat duidelijk worden bijv door voorraad+buitenvk tussen {} te
// zetten, of 'totaal (buitenvk)', zoiets)

// FIXME: dit is op dit moment niet zo geimplementeerd, er wordt niks
// geextrapoleerd of wat dan ook

// de tellingen staan steeds in een rowspan voor alle rows van 1 boek, omdat
// je bij het tellen geen onderscheid kan maken tussen de verschillende
// voorraden

// verder staat er N+1 keer (gescheiden door de tellingen, dit keer WEL in
// elke rij) het volgende:
// - de leveringen en retouren als '+30-5' (30 geleverd, 5 retour gezonden)
// - de verkopen en terugkopen als '-10+1' (10 verkopen, 1 terugkoop)

// Per periode-eenheid staat er dan het financiele (richting student bezien)
// gedeelte van die dag. Een optie voor de leverancierskant van de zaak volgt
// nog (#347)

// en dan weer slechts 1x per boek de discrepantie tussen wat er voor mutaties
// geregistreerd zijn, en wat voor mutaties uit de telling blijkt, in een
// felle kleur oid

// je moet per boekdag op een cel kunnen klikken, en dan een gedetailleerd
// overzicht krijgen van de transacties en tellingen omstreeks die dag met dat
// boek

// Onderaan komt dan per dag een totaal van de verkopen-terugkopen, en dan nog
// een rijen met kasinkomsten, pininkomsten, e.d., en eventuele discrepantie

// de data zijn in YYYY-MM-DD, resolutie is dag, week, maand [blok of kwartaal]
function finShowOverzicht($pbegin, $peind, $resolutie, $toontellingen = 'een',
	$toonmutaties = TRUE, $toongelden = TRUE)
{
	/* Binnen deze functie worden de volgende variabelen voor lange tijd
	 * bijgehouden, waarna alles geoutput wordt:
	 * - $tabel_voorraden = { EAN => { voorraadnr => { dag => { kolommen } } } }:
	 *   alle cellen van de tabel, voor zover er voor elk voorraadnr een waarde
	 *   kan zijn
	 * - $tabel_EAN = { EAN => { dag => { kolommen } } }:
	 *   alle cellen van de tabel, voor zover er voor elk EAN een waarde
	 *   kan zijn (niet uitgesplitst naar voorraadnr)
	 * - $kolommen = { dag => { array( type => (virt)telling/events, ...) } }
	 * - $totaal_inkomsten: inkomsten van alle boeken
	 * - $boekgegevens = { EAN => array( voorraden =>
	 *   aantalverschillendevoorraden, auteur => ..., ...) }
	 * - $nachtvoorraad = { EAN => boekenvoorraad om 00:00 }
	 *
	 * De strategie voor het controleren van de tellingen is als volgt:
	 * - begin met de laatste telling op te zoeken VOOR deze maand
	 * - bereken adh daarvan de voorraad op de 1e, om 00:00.
	 * - blijf dit getal updaten door bookevents, en steeds als je een echte
	 *   telling tegenkomt, RESET je teller naar wat de telling aangeeft, en
	 *   laat tegelijkertijd heel erg opvallend zien als er een discrepantie
	 *   was (de teller is 'nachtvoorraad')
	 * - Als er een bepaalde dag wel bookevents waren, maar geen telling, laat
	 *   dan de berekende telling zien voor 00:00
	 */
	global $BWDB;

	// pbegin & peind zijn nu nog JJJJ-MM-DD, en moeten ook nog genormaliseerd
	// worden adhv de resolutie
	$superkolommen = finGetSuperkolommen($resolutie, $pbegin, $peind);
	// het eerste element is pbegin...
	list(,$pbegin) = each($superkolommen);
	// en de sentinel is peind...
	$peind = $superkolommen[0];
	// pbegin & peind zijn nu volledige EN correcte data

	$boekgegevens = finGetBoekgegevens($pbegin, $peind, $toontellingen!='geen');

	$kolommen = array();
	// vul de kolommen in
	if ($toontellingen != 'geen') {
		$kolommen['eerder'] = finGetColsEerder($pbegin, $toonmutaties);
	}
	$kolommen2 = finGetCols($superkolommen, $toontellingen, $toonmutaties,
		$toongelden);
	foreach ($kolommen2 as $k=>$v) {
		$kolommen[$k] = $v;
	}
	unset($kolommen2);
	if ($toontellingen != 'geen') {
		$kolommen['later'] = finGetColsLater($peind, $toonmutaties);
	}
	if ($toongelden || $toonmutaties) {
		$kolommen['totalen'] = finGetColsTotaal($pbegin,
			$toontellingen != 'geen', $toonmutaties, $toongelden);
	}

	// let op, kolommen gaat per referentie!
	list($tabel_EAN, $tabel_voorraden) = finGetTableCells($kolommen, $boekgegevens);

	if (!$toongelden && !$toonmutaties && $toontellingen == 'geen') {
		printHTML('De selectie is te beperkt, er zijn geen gegevens die '
			.'getoond kunnen worden.', TRUE);
		finAfrekeningHome();
		return;
	}

	finShowTable($kolommen, $tabel_EAN, $tabel_voorraden, $boekgegevens,
		$toongelden);

	echo '<p>Download grootboek op:
	<a href="/Onderwijs/Boeken/index.php?page=afrekening&amp;action=putcsv&amp;aggregatie=transactie&amp;van='.
		urlencode($pbegin).'&amp;tot='.urlencode($peind).'">transactieniveau</a>,
	<a href="/Onderwijs/Boeken/index.php?page=afrekening&amp;action=putcsv&amp;aggregatie=dag&amp;van='.
		urlencode($pbegin).'&amp;tot='.urlencode($peind).'">dagniveau</a>,
	<a href="/Onderwijs/Boeken/index.php?page=afrekening&amp;action=putcsv&amp;aggregatie=maand&amp;van='.
		urlencode($pbegin).'&amp;tot='.urlencode($peind).'">maandniveau</a>, of
	<a href="/Onderwijs/Boeken/index.php?page=afrekening&amp;action=putcsv&amp;aggregatie=geheel&amp;van='.
		urlencode($pbegin).'&amp;tot='.urlencode($peind).'">geaggregeerd</a></p>';

	echo "\n\n<p><a href=\"toelichting_fin.html\">Toelichting bij deze tabel</a></p>\n\n";
}

// geef een array met daarin iig alle EANs die voor een bepaalde periode
// relevant zijn
function finGetBoekgegevens($pbegin, $peind, $tellingen = TRUE)
{
	global $BWDB;

	$alle_events = finBookEvents($pbegin, $peind);

	// TODO: gebruik ter controle:
	//$totaal_inkomsten = finCalcBoekInkomsten($alle_events);

	// alle EAN's waar een event voor is...
	$EANs = array();
	foreach ($alle_events as $data) {
		$EANs[] = $data['EAN'];
	}

	if ($tellingen) {
		// en ook alle EANs die geteld zijn deze periode erbij, zodat je altijd
		// alle tellingen ziet
		$res = $BWDB->q("COLUMN SELECT DISTINCT `tellingitem`.`EAN`"
			." FROM `telling` INNER JOIN `tellingitem` USING (`tellingnr`)"
			." WHERE `telling`.`wanneer` >= %s"
			." AND `telling`.`wanneer` < %s"
			." AND `tellingitem`.`aantal_voorraad`+`tellingitem`.`aantal_buitenvk` > 0"
			, $pbegin
			, $peind);
		$EANs = array_merge($EANs, $res);
	}

	return sqlBoekenGegevens($EANs);

}

// geeft de kolommen voor 'eerder'
function finGetColsEerder($pbegin, $toonmutaties = TRUE)
{
	global $BWDB;

	// **********************************************************
	// Zoek de meest recente telling VOOR deze periode op
	// **********************************************************
	$row = $BWDB->q('MAYBETUPLE SELECT * FROM telling
		WHERE wanneer < %s
		ORDER BY wanneer DESC LIMIT 1', $pbegin);
	$return = array();
	$return[] = array
		( 'type' => 'telling'
		, 'header' => "{".$row['wanneer']."}"
		, 'tellingnr' => $row['tellingnr']
		, 'wanneer' => $row['wanneer']
		, 'totaal_voorraad' => 0
		, 'totaal_buitenvk' => 0
		, 'verschil_pos' => 0
		, 'verschil_neg' => 0
		);
	if ($toonmutaties) {
		$return[] = array
			( 'type' => 'events'
			, 'header' => "mut."
			, 'begin' => $row['wanneer']
			, 'eind'  => $pbegin
			, 'totaal' => array
				( 'levering'  => 0
				, 'retour'    => 0
				, 'verkoop'   => 0
				, 'terugkoop' => 0
				)
			, 'bookevents' => finBookEvents($row['wanneer'], $pbegin)
			);
	}

	return $return;
}

// geeft de superkolommen dwz eigenlijk de periodes. Formaat: { omschrijving
// => datum } (eindigend met een omschrijving '0' en de einddatum)
function finGetSuperkolommen($resolutie, $pbegin, $peind)
{
	// **********************************************************
	// Wat zijn de elementen bij deze resolutie?
	// **********************************************************
	list($jaar,$maand,$dag) = explode('-', $pbegin);
	$psec_begin = mktime(0,0,0,$maand,$dag,$jaar);
	list($jaar,$maand,$dag) = explode('-', $peind);
	$psec_eind  = mktime(0,0,0,$maand,$dag,$jaar);

	$superkolommen = array();
	switch ($resolutie) {
		case 'dag':
			for ($time = $psec_begin; $time < $psec_eind; $time = cal_AddDays($time, 1) ) {
				$superkolommen[strftime('%a %d', $time)] =
					strftime('%Y-%m-%d %H:%M:%S', $time);
			}
			break;
		case 'week':
			// rond psec_begin naar het verleden af tot een maandag
			$psec_begin = cal_getLastMonday($psec_begin);
			for ($time = $psec_begin; $time < $psec_eind; $time = cal_AddDays($time, 7) ) {
				$superkolommen[strftime('week %V (%a %d %b)', $time)] =
					strftime('%Y-%m-%d %H:%M:%S', $time);
			}
			break;
		case 'maand':
			// rond psec_begin naar het verleden af tot de eerste vd maand
			$jaar = strftime('%Y', $psec_begin);
			$maand = strftime('%m', $psec_begin);
			while ($psec_eind > ($time = mktime(0,0,0,$maand++,1,$jaar))) {
				$superkolommen[strftime('%b %Y', $time)] =
					strftime('%Y-%m-%d %H:%M:%S', $time);
			}
			break;
		default:
			user_error("Ik snap deze resolutie niet: $resolutie",
				E_USER_ERROR);
	}

	// een sentinel
	$superkolommen[0] =
		strftime('%Y-%m-%d %H:%M:%S', $time);

	return $superkolommen;
}

function finGetCols($superkolommen, $toontellingen, $toonmutaties, $toongelden)
{
	global $request, $BWDB;

	$kolommen = array();
	// **********************************************************
	// Loop nu alle dagen af
	// **********************************************************
	$eind = $eind_periodenaam = null;
	foreach ($superkolommen as $volgperiodenaam => $volgperiode) {
		if (!$eind) {
			$eind = $volgperiode;
			$periodenaam = $volgperiodenaam;
			continue;
		}
		$begin = $eind;
		$eind = $volgperiode;

		// nu beginnen we echt, en hebben deze vars: begin, eind en
		// periodenaam

		if ($toontellingen != 'geen') {
			// **********************************************************
			// Wat zijn de tellingen van vandaag?
			// **********************************************************

			$res = $BWDB->q("TABLE SELECT *"
				." FROM `telling`"
				." WHERE `wanneer` >= %s"
				." AND `wanneer` < %s"
				." ORDER BY `wanneer`"
				." LIMIT ".($toontellingen=='alle'?"NULL":"1")
				, $begin
				, $eind);

			$tellingen = array();
			foreach($res as $row) {
				$tellingen[$row['tellingnr']] = $row['wanneer'];
			}
		}

		// Een sentinel, om aan te geven dat we alle tellingen van vandaag
		// gehad hebben. Als $toontellingen op 'geen' staat, betekent dit, dat
		// alleen deze sentinel in $tellingen zit.
		$tellingen[0] = $eind;

		if ($toontellingen != 'geen' && count($tellingen) <= 1) {
			// alleen de sentinel, dus geen telling deze dag, dus laat een
			// virtuele telling zien
			$kolommen[$periodenaam][] = array
				( 'type' => 'virttelling'
				, 'header' => "n.g."
				, 'wanneer' => $begin
				, 'totaal_voorraad' => 0
				, 'totaal_buitenvk' => 0
				);
		}

		// **********************************************************
		// Welke kolommen gaan we laten zien voor deze dag?
		// **********************************************************

		$subbegin = $begin;
		foreach ($tellingen as $tellingnr => $wanneer) {
			$subeind = $wanneer;

			// als er events zijn voor deze tijdsspanne, een kolom ervoor
			// maken
			if ($toonmutaties && $ev = finBookEvents($subbegin, $subeind)) {
				$kolommen[$periodenaam][] = array
					( 'type' => 'events'
					, 'header' => "mut."
					, 'begin' => $subbegin
					, 'eind'  => $subeind
					, 'totaal' => array
						( 'levering'  => 0
						, 'retour'    => 0
						, 'verkoop'   => 0
						, 'terugkoop' => 0
						)
					, 'bookevents' => $ev
					);
			}
			// tenzij dit de sentinel is, telling laten zien
			if ($tellingnr) {
				$h = $wanneer;
				if ($request->get('resolutie') == 'dag') $h = substr($h, 11, 5);
				$kolommen[$periodenaam][] = array
					( 'type' => 'telling'
					, 'header' => "{".$h."}"
					, 'tellingnr' => $tellingnr
					, 'wanneer' => $wanneer
					, 'totaal_voorraad' => 0
					, 'totaal_buitenvk' => 0
					, 'verschil_pos' => 0
					, 'verschil_neg' => 0
					);
			}
			$subbegin = $subeind;
		}
		// En per periode, ook (evt, zie #347) een geldkolom
		if ($toongelden) {
			$kolommen[$periodenaam][] = array
				( 'type' => 'verkoopgeld'
				, 'header' => "geld"
				, 'begin' => $begin
				, 'eind'  => $eind
				, 'totaal_inkomsten' => 0
				, 'bookevents' => finBookEvents($begin, $eind)
				);
		}
		$periodenaam = $volgperiodenaam;
	}

	return $kolommen;
}

// geeft de kolommen voor 'later''
function finGetColsLater($peind, $toonmutaties = TRUE)
{
	global $BWDB;

	// **********************************************************
	// Zoek de meest recente telling NA deze periode op
	// **********************************************************
	$laatste = $BWDB->q('MAYBETUPLE SELECT * FROM telling
		WHERE wanneer <= %s
		ORDER BY wanneer DESC LIMIT 1', $peind);
	$later = $BWDB->q('MAYBETUPLE SELECT * FROM telling
		WHERE wanneer >= %s
		ORDER BY wanneer ASC LIMIT 1', $peind);
	$return = array();
	if ($toonmutaties && $laterev = finBookEvents($peind, $later['wanneer'])) {
		$return[] = array
			( 'type' => 'events'
			, 'header' => "mut."
			, 'begin'  => $laatste['wanneer']
			, 'eind' => $later['wanneer']
			, 'totaal' => array
				( 'levering'  => 0
				, 'retour'    => 0
				, 'verkoop'   => 0
				, 'terugkoop' => 0
				)
			, 'bookevents' => $laterev
			);
	}
	$return[] = array
		( 'type' => ($later?'telling':'virttelling')
		, 'header' => ($later?"\{$later[wanneer]}":'n.g.')
		, 'tellingnr' => $later['tellingnr']
		, 'wanneer' => $later['wanneer']
		, 'totaal_voorraad' => 0
		, 'totaal_buitenvk' => 0
		, 'verschil_pos' => 0
		, 'verschil_neg' => 0
		);

	return $return;
}

// geeft de kolommen voor 'totaal'
function finGetColsTotaal($pbegin, $toontellingen, $toonmutaties, $toongelden)
{
	$return = array();
	if ($toontellingen && $toonmutaties) {
		$return[] = array
			( 'type' => 'verschiltotaal'
			, 'header' => "Discrepanties"
			, 'verschil_pos' => 0
			, 'verschil_neg' => 0
			, 'nettoverschil_pos' => 0
			, 'nettoverschil_neg' => 0
			);
	}
	if ($toonmutaties) {
		$return[] = array
			( 'type' => 'eventstotaal'
			, 'header' => "Mutaties"
			, 'totaal' => array
				( 'levering'  => 0
				, 'retour'    => 0
				, 'verkoop'   => 0
				, 'terugkoop' => 0
				)
			, 'bookevents' => array()
			);
	}
	if ($toongelden) {
		$return[] = array
			( 'type' => 'verkoopgeldtotaal'
			, 'header' => "Gelden"
			, 'bookevents' => array()
			, 'totaal_inkomsten' => 0
			, 'verkoopgeld' => array
				( 'pin' => 0
				, 'pinII' => 0
				, 'pin1' => 0
				, 'pin2' => 0
				, 'chip' => 0
				, 'cash' => 0
				, 'facturen' => 0
				, 'debiteuren' => 0
				, 'crediteuren' => 0
				, 'donaties' => 0
				, 'mutaties' => 0
				, 'kasverschil_verklaard' => 0
				, 'totaalverkocht' => 0
				, 'totaalin' => 0
				, 'verschil' => 0
				)
			);
	}

	return $return;
}

function finGetTableCells(&$kolommen, $boekgegevens)
{
	global $BWDB;

	// Definitie, omdat deze mogelijk niet aangemaakt wordt hieronder. Dit
	// gebeurt wanneer er geen tellingen weergegeven hoeven te worden.
	$tabel_EAN = array();
	$tabel_voorraden = array();
	// **********************************************************
	// okay... dan gaan we nu de collectie tabelcellen per EAN&dag genereren
	// **********************************************************
	foreach ($kolommen as $dag => $dagkolommen) {
		foreach ($dagkolommen as $kolomnr => $kolom) {
			if ($kolom['type'] == 'telling') {
				$tellingen = array();
				$res = $BWDB->q("TABLE SELECT *"
					." FROM `tellingitem`"
					." WHERE `tellingnr` = %s"
					, $kolom['tellingnr']);
				foreach($res as $row) {
					$tellingen[$row['EAN']]['voorraad'] =
						$row['aantal_voorraad'];
					$tellingen[$row['EAN']]['buitenvk'] =
						$row['aantal_buitenvk'];
				}
			}
			foreach ($boekgegevens as $EAN => $boekdata) {
				// zorg dat de sortering van boekgegevens aangehouden wordt,
				// door nu vast de entry in tabel_voorraden aan te maken
				if (!isset($tabel_voorraden[$EAN])) {
					$tabel_voorraden[$EAN] = array();
				}

				switch ($kolom['type']) {
					case 'events':
						$events = array();

						foreach ($kolom['bookevents'] as $voorraadnr => $event) {
							if ($event['EAN'] != $EAN) continue;
							$events[$voorraadnr] = $event;

							$tabel_voorraden[$EAN][$voorraadnr][$dag][$kolomnr] =
								finShowMutaties($event);

							$kolommen[$dag][$kolomnr]['totaal']['levering'] +=
								$event['levering'];
							$kolommen[$dag][$kolomnr]['totaal']['retour'] +=
								$event['retour'];
							$kolommen[$dag][$kolomnr]['totaal']['verkoop'] +=
								array_sum($event['verkoop']);
							$kolommen[$dag][$kolomnr]['totaal']['terugkoop'] +=
								array_sum($event['terugkoop']);

							if (!isset($eventstotaal[$voorraadnr]['levering']))
								$eventstotaal[$voorraadnr]['levering'] = 0;
							if (!isset($eventstotaal[$voorraadnr]['retour']))
								$eventstotaal[$voorraadnr]['retour'] = 0;
							if (!isset($eventstotaal[$voorraadnr]['verkoop']))
								$eventstotaal[$voorraadnr]['verkoop'] = 0;
							if (!isset($eventstotaal[$voorraadnr]['terugkoop']))
								$eventstotaal[$voorraadnr]['terugkoop'] = 0;

							/* Mutaties alleen optellen als het in het huidige
							 * tijdvak zit. */
							if ($dag != 'eerder' && $dag != 'later') {
								$eventstotaal[$voorraadnr]['levering'] +=
									$event['levering'];
								$eventstotaal[$voorraadnr]['retour'] +=
									$event['retour'];
								$eventstotaal[$voorraadnr]['verkoop'] +=
									array_sum($event['verkoop']);
								$eventstotaal[$voorraadnr]['terugkoop'] +=
									array_sum($event['terugkoop']);
							}
						}

						// update nachtvoorraad
						@$nachtvoorraad[$EAN]['voorraad'] +=
							finCalcVoorraadMutatie($events);
						break;
					case 'verkoopgeld':
						foreach ($kolom['bookevents'] as $voorraadnr => $event) {
							if ($event['EAN'] != $EAN) continue;

							$inkomsten = finCalcBoekInkomsten(array($event));
							$tabel_voorraden[$EAN][$voorraadnr][$dag][$kolomnr] =
								Money::addPrice($inkomsten);
							@$kolommen[$dag][$kolomnr]['totaal_inkomsten'] +=
								$inkomsten;

							if (!isset($geldtotaal[$voorraadnr])) {
								$geldtotaal[$voorraadnr] = 0;
							}
							$geldtotaal[$voorraadnr] += $inkomsten;
						}

						break;
					case 'virttelling':
					case 'telling':
						if ($kolom['type'] == 'virttelling' ||
							$boekgegevens[$EAN]['telbaar'] == 'N')
						{
							$tabel_EAN[$EAN][$dag][$kolomnr] =
								"<span class=\"bw_virt\">{"
								.(@$nachtvoorraad[$EAN]['buitenvk']?
								 ($nachtvoorraad[$EAN]['voorraad'].'&nbsp;+&nbsp;'
								.$nachtvoorraad[$EAN]['buitenvk']):
								@$nachtvoorraad[$EAN]['voorraad'])."}</span>";
						   $kolommen[$dag][$kolomnr]['totaal_voorraad'] +=
							   @$nachtvoorraad[$EAN]['voorraad'];
						   $kolommen[$dag][$kolomnr]['totaal_buitenvk'] +=
							   @$nachtvoorraad[$EAN]['buitenvk'];
							break;
						}
						$telling_voorraad = (int)@$tellingen[$EAN]['voorraad'];
						$telling_buitenvk = (int)@$tellingen[$EAN]['buitenvk'];
						$telling = $telling_voorraad + $telling_buitenvk;
						$tekst = '';

						/* Pas bij de tweede telling de voorraad checken, bij
						 * de 'eerder' is er namelijk nog niks bekend */
						if ($dag != 'eerder') {
							/* Als er al een nachtvoorraad was, trek deze dan
							 * van de huidige telling af. */
							if (isset($nachtvoorraad[$EAN])) {
								$verschil = $telling -
									array_sum($nachtvoorraad[$EAN]);
							} else {
								$verschil = $telling;
							}

							if ($verschil > 0) {
								$verschil = "+$verschil";
								$kolommen[$dag][$kolomnr]['verschil_pos'] +=
									$verschil;
								/* Discrepanties alleen optellen als het in
								 * het huidige tijdvak zit. */
								if ($dag != 'eerder' && $dag != 'later') {
									$verschil_pos[$EAN] += $verschil;
								}
							}
							if ($verschil < 0) {
								$kolommen[$dag][$kolomnr]['verschil_neg'] -=
									$verschil;
								/* Discrepanties alleen aftrekken als het in
								 * het huidige tijdvak zit. */
								if ($dag != 'eerder' && $dag != 'later') {
									$verschil_neg[$EAN] -= $verschil;
								}
							}
							if ($verschil) {
								$tekst .=
									"<span class=\"text-danger strongtext\">$verschil</span>";
							}
						} else {
							if (!isset($verschil_neg[$EAN])) {
								$verschil_neg[$EAN] = 0;
							}
							if (!isset($verschil_pos[$EAN])) {
								$verschil_pos[$EAN] = 0;
							}
						}

						$nachtvoorraad[$EAN] = @$tellingen[$EAN];
						$tekst .= '{'.($telling_buitenvk?
							"$telling_voorraad&nbsp;+&nbsp;$telling_buitenvk":$telling)
							.'}';
						$tabel_EAN[$EAN][$dag][$kolomnr] = $tekst;
						$kolommen[$dag][$kolomnr]['totaal_voorraad'] +=
							$telling_voorraad;
						$kolommen[$dag][$kolomnr]['totaal_buitenvk'] +=
							$telling_buitenvk;
						break;
					case 'verschiltotaal':
						if (@$verschil_pos[$EAN] || @$verschil_neg[$EAN]) {
							$netto = $verschil_pos[$EAN] -
								$verschil_neg[$EAN];
							if ($netto>0) {
								$kolommen[$dag][$kolomnr]['nettoverschil_pos']
									+= $netto;
								$netto = "+$netto";
							} else {
								$kolommen[$dag][$kolomnr]['nettoverschil_neg']
									-= $netto;
							}
							$tekst = "<span class=\"text-danger strongtext\">+".
								$verschil_pos[$EAN]."-".$verschil_neg[$EAN].
								"= $netto</span>";

							$kolommen[$dag][$kolomnr]['verschil_pos'] +=
								$verschil_pos[$EAN];
							$kolommen[$dag][$kolomnr]['verschil_neg'] +=
								$verschil_neg[$EAN];
						} else if ($boekgegevens[$EAN]['telbaar'] == 'N') {
							$tekst = '<span class="bw_onbelangrijk">n.v.t.'
								.'</span>';
						} else {
							$tekst = '<span class="text-success strongtext">OK</span>';
						}

						$tabel_EAN[$EAN][$dag][$kolomnr] = $tekst;
						break;
					case 'eventstotaal':
						$kolommen[$dag][$kolomnr]['bookevents'] = $eventstotaal;

						foreach ($tabel_voorraden[$EAN] as $voorraadnr => $dummy) {
							$kolommen[$dag][$kolomnr]['totaal']['levering'] +=
								$eventstotaal[$voorraadnr]['levering'];
							$kolommen[$dag][$kolomnr]['totaal']['retour'] +=
								$eventstotaal[$voorraadnr]['retour'];
							$kolommen[$dag][$kolomnr]['totaal']['verkoop'] +=
								$eventstotaal[$voorraadnr]['verkoop'];
							$kolommen[$dag][$kolomnr]['totaal']['terugkoop'] +=
								$eventstotaal[$voorraadnr]['terugkoop'];

							$tabel_voorraden[$EAN][$voorraadnr][$dag][$kolomnr] =
								finShowMutaties($eventstotaal[$voorraadnr]);
						}
						break;
					case 'verkoopgeldtotaal':
						$kolommen[$dag][$kolomnr]['bookevents'] = $geldtotaal;

						foreach ($tabel_voorraden[$EAN] as $voorraadnr => $dummy) {
							if (!isset($geldtotaal[$voorraadnr])) continue;
							$kolommen[$dag][$kolomnr]['totaal_inkomsten'] +=
								$geldtotaal[$voorraadnr];
							$tabel_voorraden[$EAN][$voorraadnr][$dag][$kolomnr]
								= Money::addPrice($geldtotaal[$voorraadnr]);
						}
						break;
					default:
						user_error("Interne fout", E_USER_ERROR);
				}
			}

			if ($kolom['type'] == 'verkoopgeld') {
				// en lees ook verkoopgeld in

				$kolommen[$dag][$kolomnr]['verkoopgeld'] =
					$BWDB->q("TUPLE SELECT SUM(`pin`) as pin"
						.",SUM(`pinII`) as pinII"
						.",SUM(`pin1`) as pin1"
						.",SUM(`pin2`) as pin2"
						.",SUM(`chip`) as chip"
						.",SUM(`facturen`) as facturen"
						.",SUM(`debiteuren`) as debiteuren"
						.",SUM(`crediteuren`) as crediteuren"
						.",SUM(`cash`) as cash"
						.",SUM(`donaties`) as donaties"
						.",SUM(`mutaties`) as mutaties"
						." FROM `verkoopgeld`"
						." WHERE `geldigtot` IS NULL"
						." AND `verkoopdag` >= %s"
						." AND `verkoopdag` < %s"
						, $kolom['begin']
						, $kolom['eind']);
				$kolommen[$dag][$kolomnr]['verkoopgeld']['kasverschil_verklaard'] =
					$BWDB->q('MAYBEVALUE SELECT SUM(kasverschil) as
						kasverschil_verklaard
						FROM kasverschil
						WHERE geldigtot IS null AND
							verkoopdag >= SUBSTRING(%s, 1, 10) AND
							verkoopdag < SUBSTRING(%s, 1, 10)',
						$kolom['begin'], $kolom['eind']);
				/* Wegens bug in mysql (som van 0 rows is NULL ipv 0), staan
				 * hieronder aapjes. */
				$kolommen[$dag][$kolomnr]['verkoopgeld']['totaalverkocht'] =
					$kolommen[$dag][$kolomnr]['totaal_inkomsten'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['mutaties'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['donaties'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['crediteuren'] +
					$kolommen[$dag][$kolomnr]['verkoopgeld']['kasverschil_verklaard'];
				$kolommen[$dag][$kolomnr]['verkoopgeld']['totaalin'] =
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['pin'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['pinII'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['pin1'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['pin2'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['chip'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['cash'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['facturen'] +
					@$kolommen[$dag][$kolomnr]['verkoopgeld']['debiteuren'];
				$kolommen[$dag][$kolomnr]['verkoopgeld']['verschil'] =
					$kolommen[$dag][$kolomnr]['verkoopgeld']['totaalin'] -
					$kolommen[$dag][$kolomnr]['verkoopgeld']['totaalverkocht'];

				foreach ($kolommen[$dag][$kolomnr]['verkoopgeld'] as
					$key => $value)
				{
					if (!isset($verkoopgeldtotaal[$key])) {
						$verkoopgeldtotaal[$key] = 0;
					}
					$verkoopgeldtotaal[$key] += $value;
				}
			}
			if ($kolom['type'] == 'verkoopgeldtotaal') {
				foreach ($verkoopgeldtotaal as $key => $value) {
					$kolommen[$dag][$kolomnr]['verkoopgeld'][$key] = $value;
				}
			}
		}
	}
	return array($tabel_EAN, $tabel_voorraden);
}

function finShowTable($kolommen, $tabel_EAN, $tabel_voorraden, $boekgegevens,
	$toongelden)
{
	// hehe, dan kunnen we nu eindelijk de tabel (die met gemak >5000 cellen
	// kan hebben) naar het scherm duwen...
	echo "<table border=1>\n";
	finPrintOverzichtTabelHeaders($kolommen);
	finPrintOverzichtTabelTotalen($kolommen);
	finPrintOverzichtTabelBody($kolommen, $tabel_EAN, $tabel_voorraden,
		$boekgegevens);
	finPrintOverzichtTabelTotalen($kolommen);
	if ($toongelden) {
		finPrintOverzichtTabelFinancieel($kolommen,'Donaties', 'donaties');
		finPrintOverzichtTabelFinancieel($kolommen,'Mutaties', 'mutaties');
		finPrintOverzichtTabelFinancieel($kolommen,'Crediteuren',
			'crediteuren');
		finPrintOverzichtTabelFinancieel($kolommen,'Verklaarde kasverschillen',
			'kasverschil_verklaard');
		finPrintOverzichtTabelFinancieel($kolommen,'Totaal verkocht',
			'totaalverkocht', true);
		finPrintOverzichtTabelFinancieel($kolommen,'PIN in', 'pin');
		finPrintOverzichtTabelFinancieel($kolommen,'PIN bij I&I in', 'pinII');
		finPrintOverzichtTabelFinancieel($kolommen,'PIN 1', 'pin1');
		finPrintOverzichtTabelFinancieel($kolommen,'PIN 2', 'pin2');
		finPrintOverzichtTabelFinancieel($kolommen,'Chip in', 'chip');
		finPrintOverzichtTabelFinancieel($kolommen,'Cash in', 'cash');
		finPrintOverzichtTabelFinancieel($kolommen,'Facturen', 'facturen');
		finPrintOverzichtTabelFinancieel($kolommen,'Debiteuren', 'debiteuren');
		finPrintOverzichtTabelFinancieel($kolommen,'Totaal inkomsten',
			'totaalin', true);
		finPrintOverzichtTabelFinancieel($kolommen,'Onverklaard kasverschil',
			'verschil', true);
	}
	finPrintOverzichtTabelHeaders($kolommen);
	echo "</table>";
}

/* Parameter $mutaties bevat mogelijk de keys 'levering', 'retour', 'verkoop'
 * en 'terugkoop', welke in dat geval gebruikt worden om de mutaties weer te
 * geven. */
function finShowMutaties($mutaties)
{
	if (!$showtrans = tryPar('showtrans')) {
		/* Als er niets is meegegeven, laat dan alles zien */
		$showtrans = array('levering' => 1, 'retour' => 1, 'verkoop' => 1,
			'terugkoop' => 1);
	}

	$levering = (int)@$mutaties['levering'];
	$retour = (int)@$mutaties['retour'];
	$verkoop = @$mutaties['verkoop'];
	$terugkoop = @$mutaties['terugkoop'];
	$verkoop = is_array($verkoop) ? array_sum($verkoop) : (int)$verkoop;
	$terugkoop = is_array($terugkoop) ? array_sum($terugkoop) : (int)$terugkoop;

	$tekst = '<span class="bw_voorraad">';
	if ( (@$showtrans['levering'] && isset($mutaties['levering'])) ||
		(@$showtrans['retour']   && isset($mutaties['retour'])) ) {
		$tekst .= '<span class="bw_lev">';
		if (@$showtrans['levering'] && isset($mutaties['levering'])) {
			$tekst .= "+$levering";
		}
		if (@$showtrans['retour'] && isset($mutaties['retour'])) {
			$tekst .= "-$retour";
		}
		$tekst .= '</span>&nbsp;';
	}
	if(@$showtrans['verkoop'] && isset($mutaties['verkoop'])) {
		$tekst .= "-".$verkoop;
	}
	if (@$showtrans['terugkoop'] && isset($mutaties['terugkoop'])) {
		$tekst .= "+$terugkoop";
	}

	$tekst .= '</span>';

	return $tekst;
}

/* Parameter $mutaties bevat mogelijk de keys 'levering', 'retour', 'verkoop'
 * en 'terugkoop', welke in dat geval gebruikt worden om de netto mutaties uit
 * te rekenen. */
function finCalcMutaties($mutaties)
{
	return @$mutaties['levering'] - @$mutaties['verkoop'] +
		@$mutaties['terugkoop']	- @$mutaties['retour'];
}

function finPrintOverzichtTabelHeaders($kolommen)
{
	echo "<tr><th rowspan=2>ISBN</th>\n";
	echo "<th rowspan=2>Auteur</th>\n";
	echo "<th rowspan=2>Titel</th>\n";
	echo "<th rowspan=2>Druk</th>\n";
	foreach ($kolommen as $dag => $dagkolommen) {

		echo "<th colspan=".count($dagkolommen).">$dag</th>\n";
	}
	echo "</tr>\n<tr>";
	foreach ($kolommen as $dag => $dagkolommen) {
		foreach ($dagkolommen as $kolom) {
			echo "<th>$kolom[header]</th>";
		}
	}
}

// voorraad: OK
function finPrintOverzichtTabelBody($kolommen, $tabel_EAN, $tabel_voorraad,
	$boekgegevens)
{
	// dan nu eindelijk ECHT de tabel... hold on
	foreach ($tabel_voorraad as $EAN => $voorraden) {
		$first_voorraad_of_EAN = true;
		// als er geen voorraden zijn, wil je precies 1 rij, voor de
		// tellingen
		if (!$voorraden) {
			$voorraden = array(null);
		}
		$numvoorraden = count($voorraden);
		foreach ($voorraden as $voorraadnr => $dagen) {
			echo "<tr>";
			if ($first_voorraad_of_EAN) {
				if (tryPar('action') == 'custom') {
					$begin = tryPar('begin')?'&begin='.tryPar('begin'):'';
					$eind  = tryPar('eind') ?'&eind=' .tryPar('eind' ):'';
				} else {
					$begin = '&begin='.tryPar('jaar').'-'.tryPar('maand', '01')
						.'-'.tryPar('dag', '01');
					$eind = '&begin='.tryPar('jaar').'-'.tryPar('maand', '01')
						.'-'.tryPar('dag', '01');
				}
				echo "<td align=center rowspan=$numvoorraden>".
					makeBookRef('voorraadmutaties', print_EAN($EAN),
						'EAN='.$EAN.$begin.$eind)
					."</td>\n";
				echo "<td align=left rowspan=$numvoorraden>".
					cut_auteur($boekgegevens[$EAN]['auteur'], TRUE)."</td>\n";
				echo "<td align=left rowspan=$numvoorraden>".
					cut_titel($boekgegevens[$EAN]['titel'], TRUE)."</td>\n";
				echo "<td align=left rowspan=$numvoorraden>".
					(isset($boekgegevens[$EAN]['druk'])?$boekgegevens[$EAN]['druk']:'&nbsp;')."</td>\n";
			}
			foreach ($kolommen as $dag => $dagkolommen) {
				foreach ($dagkolommen as $kolomnr => $kolom) {
					if ($kolom['type'] == 'events' ||
						$kolom['type'] == 'verkoopgeld' ||
						$kolom['type'] == 'eventstotaal' ||
						$kolom['type'] == 'verkoopgeldtotaal') {
						echo "<td align=center>".@$dagen[$dag][$kolomnr]."</td>\n";
					} else if ($first_voorraad_of_EAN) {
						$title = empty($kolom['wanneer']) ? '' : "title=\"$kolom[wanneer]\"";
						echo "<td align=center rowspan=$numvoorraden " .
							$title . ">" .
							$tabel_EAN[$EAN][$dag][$kolomnr]."</td>\n";
					}
				}
			}

			echo "</tr>\n";
			$first_voorraad_of_EAN = false;
		}
	}
}

function finPrintOverzichtTabelTotalen($kolommen)
{
	echo "<tr><th colspan=4>TOTAAL</th>\n";
	foreach ($kolommen as $dag => $dagkolommen) {
		foreach ($dagkolommen as $kolom) {
			echo "<th>";
			switch ($kolom['type']) {
				case 'events':
					echo finShowMutaties($kolom['totaal']);
					break;
				case 'telling':
					if ($kolom['verschil_pos'] || $kolom['verschil_neg']) {
						echo "<span class=\"text-danger strongtext\">+$kolom[verschil_pos]";
						echo "-$kolom[verschil_neg]</span>";
						// GEEN netto discrepanties laten zien hier, die
						// betekenen namelijk niks over verschillende boeken!
					}
					echo '{'.(@$kolom['totaal_buitenvk']?
						$kolom['totaal_voorraad'].'&nbsp;+&nbsp;'
							.$kolom['totaal_buitenvk']:
						$kolom['totaal_voorraad'])
						.'}';
					break;
				case 'verschiltotaal':
					if ($kolom['verschil_pos'] || $kolom['verschil_neg']) {
						echo "<span class=\"text-danger strongtext\">+$kolom[verschil_pos]";
						echo "-$kolom[verschil_neg] = ";
						echo "+$kolom[nettoverschil_pos]";
						echo "-$kolom[nettoverschil_neg]</span>";
						// NIET netto in 1 getal laten zijn hier, dat betekent
						// namelijk niets over verschillende boeken heen
					} else {
						echo "<span class=\"text-success strongtext\">OK</span>";
					}
					break;
				case 'virttelling':
					echo '<span class="bw_virt">{'
						.(@$kolom['totaal_buitenvk']?
						$kolom['totaal_voorraad'].'&nbsp;+&nbsp;'
							.$kolom['totaal_buitenvk']:
						$kolom['totaal_voorraad'])
						.'}</span>';
					break;
				case 'verkoopgeld':
					echo Money::addPrice($kolom['totaal_inkomsten']);
					break;
				case 'eventstotaal':
					echo finShowMutaties($kolom['totaal']);
					break;
				case 'verkoopgeldtotaal':
					echo Money::addPrice($kolom['totaal_inkomsten']);
					break;
				default:
					user_error("Interne fout", E_USER_ERROR);
			}
			echo "</th>\n";
		}
	}
}

function finPrintOverzichtTabelFinancieel($kolommen, $descr, $wat,
	$header=false)
{
	$cel = $header ? 'th' : 'td';

	echo "<tr><$cel colspan=4>$descr</$cel>\n";
	foreach ($kolommen as $dag => $dagkolommen) {
		foreach ($dagkolommen as $kolom) {
			if ($kolom['type'] != 'verkoopgeld' &&
				$kolom['type'] != 'verkoopgeldtotaal')
			{
				echo "<$cel></$cel>\n";
				continue;
			}
			if (isset($kolom['verkoopgeld'][$wat])) {
				echo "<$cel>".Money::addPrice($kolom['verkoopgeld'][$wat])."</$cel>\n";
			} else {
				echo "<$cel>&nbsp;</$cel>\n";
			}
		}
	}
	echo "</tr>\n";
}

/* Levert alle bookevents van dit boek (of alle boeken) op uit een bepaalde
 * periode, al dan niet gegroepeerd per voorraad en studentenprijs.
 *
 * Resultaattype 1 (als !$detail):
 * 	array { voorraadnr => array
 *		( EAN => ...
 *		, levprijs=> ...
 *		, verkoop=> { studprijs => aantal }
 *		, terugkoop=> { studprijs => aantal }
 *		, levering=> aantal
 *		, retour=> aantal
 *		) }
 * Resultaattype 2 (als $detail):
 *	array { DATETIME => array
 *				( voorraadnr => array
 *					('levering'  => {prijs => aantal}
 *					,'retour'    => {prijs => aantal}
 *					,'verkoop'   => {prijs => aantal}
 *					,'terugkoop' => {prijs => aantal}
 *				) )
 *		}
 *
 * TODO: verplaatsing
 */
function finBookEvents($start = NULL, $eind = NULL, $EAN = NULL, $detail = NULL, $verkopers = NULL)
{
	global $BWDB;

	$result = $finalresult = array();

	if (!$transactiesoorten = tryPar('showtrans')) {
		/* Als er niets is meegegeven, laat dan alles zien */
		$transactiesoorten = array('levering' => 1, 'retour' => 1, 'verkoop' => 1,
			'terugkoop' => 1);
	}

	foreach (array_keys($transactiesoorten) as $table) {
		$from = "$table LEFT JOIN voorraad USING (voorraadnr)";

		/* of dit met verschillende studprijs per voorraadnr werkt, of niet */
		$hasstudprijs = ($table == 'verkoop' || $table == 'terugkoop');

		/* deze functie wordt 1 + #tellingen + 2 * #periodes keer aangeroepen.
		 * Performance is daardoor acceptabel */
		$andlist = array();
		if (isset($start)) $andlist[] = "$table.wanneer >= \"$start\"";
		if (isset($eind)) $andlist[]  =	"$table.wanneer < \"$eind\"";
		if ($EAN) $andlist[] = "EAN = \"$EAN\"";
		if (!empty($verkopers)){
			$in = join(',', array_fill(0, count($verkopers), '?'));
			$andlist[] = "wie IN ($in)";
		} elseif (is_array($verkopers) && sizeof($verkopers < 1)){
			// Geen verkopers? Naja, dan ook geen rows
			return array();
		}

		$res = $BWDB->q('TABLE SELECT wanneer, EAN, voorraad.voorraadnr, levprijs, voorraad.btwtarief, voorraad.btw, '
				. ($detail?'aantal':"SUM($table.aantal)") . ' as t_aantal'
				. ($hasstudprijs?', studprijs':'')
			. ' FROM ' . $from
			. ' WHERE ' . implode(" AND ", $andlist)
			. ($detail?'':' GROUP BY voorraad.voorraadnr'
				.($hasstudprijs?', studprijs':'') )
			. ' ORDER BY leveranciernr, EAN'
			);

		foreach ($res as $row) {
		/* Loop alle rijen af van de huidige $table */
			$voorraadnr = $row['voorraadnr'];

			if (!$detail && !isset($result[$voorraadnr])) {
				$result[$voorraadnr] = array('EAN' => $row['EAN'],
					'levprijs' => $row['levprijs'],
					'btwtarief' => $row['btwtarief'],
					'btw' => $row['btw'],
					'levering' => 0, 'retour' => 0,
					'verkoop' => array(), 'terugkoop' => array());
			}

			if ($detail) {
				$wanneer = $row['wanneer'];
				$intprijs = $hasstudprijs?$row['studprijs']:$row['levprijs'];
				if (isset($result[$wanneer][$voorraadnr][$table][$intprijs])) {
					// Er was al een waarde, dus tel ze op
					$result[$wanneer][$voorraadnr][$table][$intprijs] +=
						$row['t_aantal'];
				} else {
					// Voeg toe
					$result[$wanneer][$voorraadnr][$table][$intprijs] =
						$row['t_aantal'];
				}
			} else {
				if ($hasstudprijs) {
					if (!isset($result[$voorraadnr][$table][$row['studprijs']]))
					{
						$result[$voorraadnr][$table][$row['studprijs']] = 0;
					}
					$result[$voorraadnr][$table][$row['studprijs']] +=
						$row['t_aantal'];
				} else {
					$result[$voorraadnr][$table] = $row['t_aantal'];
				}
			}
		}
	}

	if ($detail) {
		ksort($result);
	}

	return $result;
}

// geeft mutatie in de voorraad adhv een array met bookevents
// returnt: voorraadmutatie
function finCalcVoorraadMutatie($bookevents)
{
	$voorraad = 0;
	foreach ($bookevents as $voorraadnr => $events) {
		$voorraad -= array_sum($events['verkoop']);
		$voorraad += array_sum($events['terugkoop']);
		$voorraad += $events['levering'];
		$voorraad -= $events['retour'];
		// verplaatsing hoeft niet, dat verandert de voorraad namelijk niet
	}

	return $voorraad;
}

// geeft de genoten financiele inkomsten uit de verkoop van een collectie
// bookevents
// returnt: bedrag in euro's
function finCalcBoekInkomsten($bookevents)
{
	$bedrag = 0;
	foreach ($bookevents as $voorraadnr => $events) {
		foreach ($events['verkoop'] as $studprijs => $aantal) {
			$bedrag += $studprijs * $aantal;
		}
		foreach ($events['terugkoop'] as $studprijs => $aantal) {
			$bedrag -= $studprijs * $aantal;
		}
	}

	return $bedrag;
}

// geeft het verschuldigde bedrag over de verkoop van een aantal boeken dat
// betaald moet worden aan de leverancier, adhv een collectie bookevents
// returnt: bedrag in euro's
function finCalcBoekDebet($bookevents)
{
	$bedrag = 0;
	foreach ($bookevents as $voorraadnr => $events) {
		$bedrag += $events['levprijs'] *
			array_sum($events['verkoop'])-array_sum($events['terugkoop']);
	}

	return $bedrag;
}

/* output een CSV van de gegevens voor de periode van-tot.
 * Dit is het grootboek, want alle transacties voor zover die bij bookweb
 * bekend zijn staan erin. Er is keuze uit zo veel mogelijk uitgesplitst per
 * transactie, maar het kan ook geaggregeerd worden zodat je het rechtstreeks
 * over kan pennen naar de WVR.
 *
 * Aggregatie is: transactie (zoveel mogelijk per transactie), dag (per dag
 * geaggregeerd), maand (per maand geaggregeerd) of geheel (per type slechts 1
 * maal genoemd)
 */
function finPutCSV($van, $tot, $aggregatie, $is_cons_check = false)
{
	global $BWDB;
	$AES2_data = sqlLeverancierData(LEVNR_AES2);

	// hierin komen als gewone tekst (zonder newlines) alle regels, met als
	// key de datum
	$regels = array();

	switch ($aggregatie) {
		case 'transactie':
			$groupv = 'verkoopnr';
			$groupt = 'terugkoop.verkoopnr';
			$groupg = 'verkoopdag';
			$datumv = 'verkoop.wanneer';
			$datumt = 'terugkoop.wanneer';
			$datumg = 'verkoopdag';
			break;
		case 'dag':
			$groupv = 'leveranciernr,datum,methode,substring(voorraad.EAN,0,3)';
			$groupt = 'leveranciernr,datum,methode,substring(voorraad.EAN,0,3)';
			$groupg = 'datum';
			$datumv = 'substring(verkoop.wanneer,1,10)';
			$datumt = 'substring(terugkoop.wanneer,1,10)';
			$datumg = 'verkoopdag';
			break;
		case 'maand':
			$groupv = 'leveranciernr,datum,methode,substring(voorraad.EAN,0,3)';
			$groupt = 'leveranciernr,datum,methode,substring(voorraad.EAN,0,3)';
			$groupg = 'datum';
			$datumv = 'substring(verkoop.wanneer,1,7)';
			$datumt = 'substring(terugkoop.wanneer,1,7)';
			$datumg = 'substring(verkoopdag,1,7)';
			break;
		case 'geheel':
			$groupv = 'leveranciernr, methode, substring(voorraad.EAN,0,3)';
			$groupt = 'leveranciernr, methode, substring(voorraad.EAN,0,3)';
			$groupg = '1';
			$datumv = '"n.v.t."';
			$datumt = '"n.v.t."';
			$datumg = '"n.v.t."';
			break;
	}

	// $i om te zorgen dat alle keys uniek zijn, zodat er niks overschreven
	// wordt
	$i = 'aaaaaaaa';

	$res = $BWDB->q("TABLE SELECT " . $datumv . " as datum"
		.", `leverancier`.`leveranciernr`"
		.", `leverancier`.`rubriek`"
		.", `voorraad`.`EAN`"
		.($aggregatie == 'transactie' ?
			', concat(SUM(`verkoop`.`aantal`)," maal \'", titel,", ",auteur,"\' a ",
			`voorraad`.`levprijs`) as omschrijving, `verkoop`.`lidnr` as lidnr,
			`verkoop`.`wie` as vklidnr' :
			', concat(SUM(`verkoop`.`aantal`)," diverse verkopen") as omschrijving, `verkoop`.`lidnr` as lidnr')
		.", `titel` as titel"
		.", SUM(`verkoop`.`aantal`) as aantal"
		.", SUM(`verkoop`.`aantal`*-`voorraad`.`levprijs`) as bedrag"
		.", SUM(`verkoop`.`aantal`*`verkoop`.`studprijs`) as vkbedrag"
		.", SUM(`verkoop`.`aantal`*(`voorraad`.`levprijs`-`verkoop`.`studprijs`)) as fonds"
		.", `verkoop`.`betaalmethode` as methode, `voorraad`.`btwtarief` as btwtarief, `voorraad`.`btw` as btw"
		." FROM `verkoop`"
		." LEFT JOIN `voorraad` USING(`voorraadnr`)"
		." LEFT JOIN `boeken` USING(`EAN`)"
		." LEFT JOIN `leverancier` ON(`leverancier`.`leveranciernr` = `voorraad`.`leveranciernr`)"
		." WHERE `verkoop`.`wanneer` < %s"
		." AND `verkoop`.`wanneer` >= %s"
		." GROUP BY " . $groupv
		." ORDER BY `verkoop`.`wanneer`"
		, $tot
		, $van);

	$var = 0; //Dit gaan we modulo twee bekijken omdat boekhouden dan leuk wordt
	foreach($res as $row) {
		$var = $var +1;
		if (!@$row["rubriek"]) $row["rubriek"] = "onbekend";

		if (!isset($fonds[$row['datum']])) {
			$fonds[$row['datum']] = 0;
		}
		if (!isset($row['methode'])) {
			$row['methode'] = 'Onbekend';
		}
		if ($row['methode'] == '') {
			$row['methode'] = 'Intern';
		}
		if (!isset($row['vklidnr'])) {
			$row['vklidnr'] = 0;
		}
		// lastig in de query te verwerken, dan maar zo
		// query de data voor elke leverancier maar 1x
		if(!isset($voorraaddata[$row['leveranciernr']])) {
			$voorraaddata[$row['leveranciernr']] =
				sqlLeverancierData($row['leveranciernr']);
		}

		if($row["btw"] != 0)//Je transactie heeft BTW, dus het is een dictaat, dus rubriek is dictaatverkoop. Deze stap is binnenkort niet meer nodig, maar omdat we twee maanden zonder hebben gedaan moet het er nu hardcoded bij.
		{
			$row["rubriek"] = "Dictaatverkoop";
		}

		$j = $i;//Zorg ervoor dat we de volgende regel terug kunnen vinden voor het geval BTW >  0
		$regels[$row['datum'] . $i++] = array (
			"Type" =>"Crediteur","Datumtijd"=>$row['datum'],"Tegenpartij" => $voorraaddata[$row['leveranciernr']]['naam'],
			 ($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
			 "Omschrijving" => $row['omschrijving'], "Bedrag" => $row['bedrag'],"Rubriek" => $row["rubriek"],"BTW" => $row["btw"], "BTW-tarief" => $row["btwtarief"],
			 "Leverancier" =>$row['leveranciernr'] , "Alternator" => $var,  "EAN" => $row["EAN"], "titel" => $row["titel"], "aantal" =>$row["aantal"], "Lidnr" => (isset($row['lidnr'])?$row['lidnr']:NULL));


		if(!is_null($row["btwtarief"]))//Als er BTW is moet het kaalbedrag en btwbedrag op aparte regels komen.
		{
			if($row["btwtarief"] > 0 && $row['lidnr'] !== "10298") //OSZ heeft geen btw tjsak
			{
				//Bel me maar zodra je je euro's in meer dan twee decimalen nodig hebt
				$btwbedrag = -1 * $row["btw"] * $regels[$row['datum'] . $j]['aantal']; 
				$kaalbedrag = $row["bedrag"] - $btwbedrag;

				$regels[$row['datum']. $i++] = $regels[$row['datum']. $j];
				$regels[$row['datum']. $j]["Bedrag"] = $kaalbedrag; //Reset vorige regel naar kaalbedrag
				$j = ++$j;//Note to self; plussen ervoor wil zeggen eerst verhogen dan door in tegenstelling tot plussen erachter.

				$regels[$row['datum']. $j]["Bedrag"] = $btwbedrag;//Reset deze regel naar btwbedrag
				$regels[$row['datum']. $j]["Type"] = "BTW"; 
				$regels[$row['datum']. $j]['Tegenpartij'] = "BTW"; //Tegenpartij
				$regels[$row['datum']. $j]["Omschrijving"] = "BTW ".$regels[$row['datum']. $j]["aantal"]." maal '".$regels[$row['datum']. $j]["titel"].", Bestuur' a ".$btwbedrag; 
			}	
		}

		if ($row['methode'] == 'pinII') {
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Debiteur", "Datumtijd"=>$row['datum'], "Tegenpartij" =>"Balie Informatica en Informatiekunde",
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Debiteur dankzij $row[omschrijving]"
					. ($aggregatie == 'transactie'?' door '
					. PersoonView::naam(Persoon::geef($row['lidnr'])):''),
					"Bedrag" =>$row['vkbedrag'], "Rubriek" =>$row["rubriek"],"BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Leverancier" =>$row['leveranciernr'],"Alternator" => $var, "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
		} else {
			$regels[$row['datum'] . $i++] = array (
				"Type" => ucfirst($row['methode']), "Datumtijd"=>$row['datum'],
				"Tegenpartij" =>($aggregatie == 'transactie'?PersoonView::naam(Persoon::geef($row['lidnr'])):
					'Diverse personen'),
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Inkomsten dankzij $row[omschrijving]"
					.($aggregatie == 'transactie'?
					' bij '.PersoonView::naam(Persoon::geef($row['vklidnr'])):
					''),
				"Bedrag" =>$row['vkbedrag'],
				"Rubriek" =>$row["rubriek"], "BTW" =>$row["btw"],"BTW-tarief" => $row["btwtarief"], "Leverancier" => $row['leveranciernr'],"Alternator" => $var,"EAN" => $row["EAN"]);
		}

		if ($row['fonds'] != 0) {
			$fonds[$row['datum']] -= $row['fonds'];
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern","Datumtijd"=> $row['datum'], "Tegenpartij" =>"Boekenfonds",
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Bijdrage ".($row['fonds'] > 0?'van':'aan')
					." boekenfonds voor $row[omschrijving]",
				"Bedrag" =>$row['fonds'],
				"Rubriek" =>$row["rubriek"], "BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern","Datumtijd"=> $row['datum'], "Tegenpartij" =>"Boekverkoop",
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Bijdrage ".($row['fonds'] > 0?'aan':'van')
					." boekverkoop voor $row[omschrijving]",
				"Bedrag" => -$row['fonds'],
				"Rubriek" =>"Boekenfonds", "BTW" => $row["btw"], "BTW-tarief" => $row["btwtarief"], "Leverancier" => $row['leveranciernr'],"Alternator" => $var, "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
		}
	}

	$res = $BWDB->q("TABLE SELECT " . $datumt . " as datum"
		.", `leverancier`.`leveranciernr`"
		.", `leverancier`.`rubriek`"
		.", `voorraad`.`EAN`"
		.($aggregatie == 'transactie' ?
			', concat(SUM(`terugkoop`.`aantal`)," maal \'", titel,", ",auteur,
			"\' a ",`voorraad`.`levprijs`) as omschrijving, '.
			'`terugkoop`.`lidnr` as `lidnr`, `terugkoop`.`wie` as vklidnr'
				:', concat(SUM(`terugkoop`.`aantal`)," diverse terugkopen") '.
				 'as omschrijving')
		.", SUM(`terugkoop`.`aantal`*`voorraad`.`levprijs`) as bedrag"
		.", SUM(`terugkoop`.`aantal`*-`terugkoop`.`studprijs`) as vkbedrag"
		.", SUM(`terugkoop`.`aantal`*(`terugkoop`.`studprijs`-`voorraad`.`levprijs`)) as fonds"
		.", `terugkoop`.`lidnr` as lidnr"
		.", `terugkoop`.`betaalmethode` as methode, `voorraad`.`btwtarief`, `voorraad`.`btw`"
		." FROM `terugkoop`"
		." LEFT JOIN `verkoop` USING(`verkoopnr`)"
		." LEFT JOIN `voorraad` ON(`terugkoop`.`voorraadnr` = `voorraad`.`voorraadnr`)"
		." LEFT JOIN `boeken` USING(`EAN`)"
		." LEFT JOIN `leverancier` ON(`voorraad`.`leveranciernr` = `leverancier`.`leveranciernr`)"
		." WHERE `terugkoop`.`wanneer` < %s"
		." AND `terugkoop`.`wanneer` >= %s"
		." GROUP BY " . $groupt
		." ORDER BY `terugkoop`.`wanneer`"
		, $tot
		, $van);
	foreach($res as $row) {
		$var = $var +1;//Deze leeft nog steeds, zelfde truuk als hierboven.

		if (!@$row["rubriek"]) $row["rubriek"] = "onbekend";

		if (!isset($fonds[$row['datum']])) {
			$fonds[$row['datum']] = 0;
		}
		if (!isset($row['methode'])) {
			$row['methode'] = 'Onbekend';
		}
		if ($row['methode'] == '') {
			$row['methode'] = 'Intern';
		}
		if (!isset($row['vklidnr'])) {
			$row['vklidnr'] = 0;
		}

		if($row["btwtarief"] != 0)//Je transactie heeft BTW, dus het is een dictaat, dus rubriek is dictaatverkoop. Deze stap is binnenkort niet meer nodig, maar omdat we twee maanden zonder hebben gedaan moet het er nu hardcoded bij.
		{
			$row["rubriek"] = "Dictaatverkoop";
		}

		// lastig in de query te verwerken, dan maar zo
		// query de data voor elke leverancier maar 1x
		if(!isset($levdata[$row['leveranciernr']])) {
			$levdata[$row['leveranciernr']] =
				sqlLeverancierData($row['leveranciernr']);
		}
		$regels[$row['datum'] . $i++] = array (
			"Type" =>"Debiteur", "Datumtijd"=>$row['datum'],"Tegenpartij" => $levdata[$row['leveranciernr']]['naam'],
			($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
			"Omschrijving" =>$row['omschrijving'], "Bedrag" =>$row['bedrag'], "Rubriek" =>$row["rubriek"],"BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Alternator" => $var , "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
		if ($row['methode'] == 'pinII') {
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Crediteur", "Datumtijd"=>$row['datum'],"Tegenpartij" => "Balie Informatica en Informatiekunde",
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Crediteur dankzij $row[omschrijving]"
					.($aggregatie == 'transactie'?
					' door '.PersoonView::naam(Persoon::geef($row['lidnr'])):
					''),
				"Bedrag" =>$row['vkbedrag'], "Rubriek" =>$row["rubriek"],"BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Leverancier" =>$row['leveranciernr'],"Alternator" => $var, "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"] );
		} else {
			$regels[$row['datum'] . $i++] = array (
				"Type" =>ucfirst($row['methode']),"Datumtijd"=> $row['datum'],
				"Tegenpartij" =>($aggregatie == 'transactie'?PersoonView::naam(Persoon::geef($row['lidnr'])):
					'Diverse personen'),
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Uitgaven dankzij $row[omschrijving]"
					.($aggregatie == 'transactie'?
					' bij '.PersoonView::naam(Persoon::geef($row['vklidnr'])):
					''),
				"Bedrag" =>$row['vkbedrag'], "Rubriek" =>$row["rubriek"],"BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Leverancier" =>$row['leveranciernr'] ,"Alternator" => $var, "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
		}

		if ($row['fonds'] != 0) {
			$fonds[$row['datum']] -= $row['fonds'];
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern", "Datumtijd"=>$row['datum'],"Tegenpartij" => "Boekenfonds",
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Bijdrage ".($row['fonds'] > 0?'van':'aan')
					." boekenfonds voor $row[omschrijving]",
				"Bedrag" =>$row['fonds'], "Rubriek" =>$row["rubriek"],"BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Alternator" => $var, "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern", "Datumtijd"=>$row['datum'], "Tegenpartij" =>"Boekverkoop",
				($aggregatie == 'transactie'?print_EAN($row['EAN'], 'text'):'n.v.t.'),
				"Omschrijving" =>"Bijdrage ".($row['fonds'] > 0?'aan':'van')
					." boekverkoop voor $row[omschrijving]",
				"Bedrag" =>-$row['fonds'], "Rubriek" =>"Boekenfonds" ,"BTW" =>$row["btw"], "BTW-tarief" => $row["btwtarief"], "Leverancier" =>$row['leveranciernr'],"Alternator" => $var, "Lidnr" => $row['lidnr'],"EAN" => $row["EAN"]);
		}
	}

	$res = $BWDB->q('TABLE SELECT *
		FROM kasverschil
		WHERE geldigtot IS NULL AND verkoopdag < %s AND verkoopdag >= %s
		ORDER BY verkoopdag, volgnr', $tot, $van);
	foreach($res as $row) {
		if (!isset($fonds[$row['verkoopdag']])) {
			$fonds[$row['verkoopdag']] = 0;
		}
		$fonds[$row['verkoopdag']] += $row['kasverschil'];

		$door = " door ". PersoonView::naam(Lid::geef($row['wie']));
		$regels[$row['verkoopdag'] . $i++] = array (
			"Type" =>"Intern","Datumtijd"=> $row['verkoopdag'], "Tegenpartij" =>"Onbekend", "n.v.t.",
			"Omschrijving" =>"Verklaard kasverschil ($row[reden])$door",
			"Bedrag" =>$row['kasverschil'], "Boekenfonds", "Alternator" => $var, "Lidnr" => $row['wie']);
	}

	$res = $BWDB->q("TABLE SELECT " . $datumg . " as datum, `verkoper`"
		.", SUM(-`mutaties`) as mutaties"
		.", SUM(`kasverschil`) as kasverschil"
		.", SUM(`donaties`) as donaties"
		.", `kv_reden`"
		." FROM `verkoopgeld`"
		." WHERE `verkoopdag` < %s"
		." AND `verkoopdag` >= %s"
		." AND `geldigtot` IS NULL"
		." GROUP BY " . $groupg
		." ORDER BY `datum`"
		, $tot
		, $van);
	foreach($res as $row) {
		if (!isset($fonds[$row['datum']])) {
			$fonds[$row['datum']] = 0;
		}
		$var = $var + 1;//Ook hier meeverhogen
		if ($aggregatie == 'transactie' || $aggregatie == 'dag') {
			$bij = " bij ".PersoonView::naam(Lid::geef($row['verkoper']));
		} else {
			$bij = '';
		}

		if (0 != $row['mutaties']) {
			if(!isset($row['btw']))
				$row['btw'] = null;
			if(!isset($row['btwtarief']))
				$row['btwtarief'] = null;

			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Crediteur","Datumtijd"=>$row['datum'],
				$AES2_data['naam'], "n.v.t.",
				"Omschrijving" =>"Mutaties$bij","Bedrag" => $row['mutaties'], "BTW" => $row["btw"], "BTW-tarief" => $row["btwtarief"], "Tegenpartij" =>"Boekverkoop","Alternator" => $var );
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern","Datumtijd"=> $row['datum'],"Tegenpartij" => "Onbekend", "n.v.t.",
				(-$row['mutaties'] > 0?'Inkomsten':'Uitgaven')
					." dankzij mutaties" . $bij,
				"Bedrag" =>-$row['mutaties'], "Boekverkoop" ,"Alternator" => $var);
		}
		if (0 != $row['kasverschil']) {
			$fonds[$row['datum']] += $row['kasverschil'];
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern", "Datumtijd"=>$row['datum'], "Tegenpartij" =>"Onbekend", "n.v.t.",
				"Omschrijving" =>($bij?
					@$row['kv_reden'].$bij:
					"Kasverschillen"),
				"Bedrag" =>$row['kasverschil'], "Boekenfonds","Alternator" => $var);
		}
		if (0 != $row['donaties']) {
			$fonds[$row['datum']] += $row['donaties'];
			$regels[$row['datum'] . $i++] = array (
				"Type" =>"Intern", "Datumtijd"=> $row['datum'],"Tegenpartij" => "Onbekend", "n.v.t.",
				"Omschrijving" =>"Donaties". $bij,"Bedrag"=> $row['donaties'], "Boekenfonds" ,"Alternator" => $var);
		}
	}

	// Zorg dat het saldo van het Boekenfonds uiteindelijk met A-Eskwadraat
	// verrekend wordt, zodat het grootboek opgeteld 0 is. Bovendien beheert
	// A-Eskwadraat het fonds voor de Boekcom
	foreach ($fonds as $datum => $saldo) {
		$var = $var +1;//We altereneren vrolijk door

		if ($saldo == 0) continue;
		$regels[$datum.$i++] = array (
			"Type" =>($saldo > 0?'Crediteur':'Debiteur'), "Datumtijd"=>$datum,
			"Tegenpartij" =>$AES2_data['naam'], "n.v.t.",
			"Omschrijving" =>"Saldo boekenfonds verrekenen",
			"Bedrag"=>-$saldo, "Boekenfonds","Alternator" => $var );
	}

	ksort($regels);

	if ($aggregatie == 'maand') {
		$len = 7;
	} else if ($aggregatie == 'geheel') {
		$len = false;
	} else {
		// dag of nauwkeuriger
		$len = 10;
	}

	// Het totaal van het grootboek hoort per definitie 0 te zijn, onze
	// leveranciers worden debiteur/crediteur, een bepaald bedrag naar het
	// boekenfonds, en dit alles wordt uit de dagafsluiting betaald (en kan
	// o.a. ook een kasverschil bevatten). Elke andere inconsistentie zou
	// niet moeten kunnen, want betekent dat er dingen veranderd zijn zonder
	// de dag opnieuw netjes af te sluiten (of is een bug)
	$prev_datum = null;
	$saldo = 0;
	$hedzerregels = array();
	$inconsistenties = array();
	foreach ($regels as $regel) {
		if ($len) {
			$datum = $regel["Datumtijd"];
			if ($prev_datum && $datum != $prev_datum) {

				if (abs($saldo) > 1e-8) {
					$hedzerregels[$prev_datum.$i++] = array (
						"Type" =>"Intern","Datumtijd"=> $prev_datum,
						"Tegenpartij" =>"Onbekend", "Inconsistenties in bookweb: "
						."Dagafsluiting komt niet overeen met transacties",
						"Bedrag" => -$saldo, "Boekverkoop" ,"Alternator" => $var);
					$inconsistenties[$prev_datum] = -$saldo;
				}
				$saldo = 0;

			}
			$prev_datum = $datum;
		}
		$saldo += Money::parseMoney($regel["Bedrag"]);
	}

	if (!$prev_datum) $prev_datum = 'n.v.t.';
	if (abs($saldo) > 1e-8) {
		$hedzerregels[$prev_datum.$i++] = array (
			"Type" =>"Intern", "Datumtijd"=>$prev_datum,
			"Tegenpartij" =>"Onbekend", "n.v.t.", "Inconsistenties in bookweb: Dagafsluiting komt niet "
			." overeen met transacties",
			-$saldo, "Boekverkoop","Alternator" => $var );
		$inconsistenties[$prev_datum] = -$saldo;
	}

	if ($is_cons_check) return $inconsistenties;

	foreach ($hedzerregels as $k => $v) {
		$regels[$k] = $v;
	}
	unset($hedzerregels);
	ksort($regels);
	
	$gbfilename = str_replace(' 00:00:00','',
		"grootboek_{$aggregatie}_{$van}_{$tot}.csv");

	$verzlid = array();//Dit wordt een verzameling van lidnummers voor alle lidnummers die in deze CSV gaan verschijnen,
				//die kunnen dat in 1 beweging in Exact Online gegooit worden als contacten of iets van die vorm
	putCsvLine(array("Type","Tegenpartij","ISBN/EAN","Omschrijving","Bedrag","Rubriek","BTW-bedrag","BTW-code","Datum","Tijd", "Grootboek", "Alternator", "Lidnummer/Relatienummer"));
	foreach ( $regels as $regel ) { 

		$error = "";
		
		// Kasverschillen hebben soms geen BTW-bedrag, maar ze mogen niet de csv opblazen
		// daarom zetten we een leeg veld neer en hopen dat Exact het lust.
		if(!isset($regel["BTW"]))
		{
			$regel["BTW"] = "";
		}
		if(!isset($regel["BTW-tarief"]))
		{
			$regel["BTW-tarief"] = 0;
		}
		
		if(!isset($regel["Datumtijd"]))
		{
			$error = "Datumtijd";
		}
		else
		{
			$datumtijd = $regel["Datumtijd"];
			$regel["Datum"] = substr($datumtijd,0,10);
			if(substr($datumtijd,10) !== false) //substr geeft false in geval van lege array
			{
				$regel["Tijd"] = substr($datumtijd,10);
			}
			else
			{
				$regel["Tijd"] = "Onbekend";
			}
		}
		
		if(!isset($regel["Type"]))
		{
			$error = "Type";
			$regel["Type"] = "WAAROM HEB IK GEEN TYPE?";
		}

		if($regel["Type"] == "Debiteur")
		{
			$regel["Grootboek"] = "1306";
		}
		
		if($regel["Type"] == "Crediteur" )
		{
			if(isset($regel["Leverancier"]))
			{
				if($regel["Leverancier"] == 3) //3->Studystore en die leveren alleen boeken
				{
					$regel["Grootboek"] = "8204"; //Boeken-grootboek
				}
				elseif($regel["Leverancier"] == 2) //2->Studievereniging A-Eskwadraat
				{
					$regel["Grootboek"] = "8205";
				}
				elseif($regel["Leverancier"] == 8) //8->De Boekencommissie
				{
					$regel["Grootboek"] = "8202";// Is eigenlijk alleen voor kantoorartikelen, moet nog over vouchers nagedacht worden. 
				}
				elseif($regel["Leverancier"] == 11) //11->Grijpink
				{
					$regel["Grootboek"] = "8206";
				}
			}
		}
		
		//Eigenlijk komen alle dictaten van 17-> Xerox, maar vroeger bestond 17 nog niet en we moeten dus chekken op op BTW
		//Dit kan omdat artikel->BTW() > 0 iff artikel->BTW() = 6 iff (artikel instanceof Dictaat) = true 	
		if($regel["BTW-tarief"] == "6")
		{

			$btwbedrag = round( $regel["Bedrag"]* 6 /(100 + 6),2); 
			$kaalbedrag = $regel["Bedrag"] - $btwbedrag;

			if(!empty($regel["Lidnr"]))
			{
				if($regel["Lidnr"] == "10298")
				{
					if($regel["Type"] == "Crediteur")//Dit is lid OSZ, hier dumpen we de dictaten voor werkcollege begeleiders.
					{
						$regel["Grootboek"] = "8201";
						$regel["Bedrag"] = $kaalbedrag;
					}
				}
				else
				{
					$regel["Grootboek"] = "8201"; //Grootboek voor dicaten
				}
			}
			elseif($regel["Type"] == "Debiteur")
			{
				$regel["Grootboek"] = 2206;//Af te dragen unie, de osz-dictaten.
				$regel["Bedrag"] = $kaalbedrag;
				$regel["Lidnr"] = "14";
			}
			else
			{	
				$regel["Grootboek"] = "8201"; //Grootboek voor dicaten
			}

			//$regel["Tegenpartij"] = "Dictaatverkoop"; //Deze is ook niet per se noodzakelijk, maar er wordt gekeken in hoeverre het handig is.
		}

		if(isset($regel["EAN"]))//Wederom luiheid, liever een extra check dan consequent programmeren.
			{
				if($regel["EAN"] == "2200000000712" ) //Een voucher! Deze dingen zijn voorlopig hardcoed en uniek.
				{
					if($regel["Type"] == "Crediteur" && !isset($regel["Grootboek"]))
					{
						$regel["Grootboek"] = 1300;//Zorgt ervoor dat het netto 0 wordt in dit grootboek
						$lidnummer = $regel["Lidnr"];
					}
					if($regel["Type"] == "Intern")
					{
						$regel["Type"] = "Crediteur";
						$regel["Grootboek"] = 1300;//Het crediteuren grootboek
						$lidnummer = $regel["Lidnr"]; //Dit type crediteuren zijn in het boekhoudprogramma aan leden gekoppelt, lidnummer wordt als ware de primary.
					}
					if($regel["Type"] == "Debiteur")
					{
						$regel["Grootboek"] = 8401; //Speciaal grootboek voor vouchers
						$lidnummer = $regel["Lidnr"];
					}
				}
				elseif($regel["EAN"] == "2100000005833") //Almanak doet aan BTW maar is geen dictaat
				{
					if($regel["Type"] == "Crediteur")
					{
						$regel["Grootboek"] = "8205";
					}
				}
			}
			
		if($regel["Type"] == "Intern")
		{	
			if($regel[0] == '978-0-321-78251-9') //Het boek dat met de voucher te kopen is, dus ook hardcoded en uniek (mathematical proofs nogwat)
			{
				$regel["Type"] = "Debiteur";//Voor ons is de persoon nu een debiteur geworden
				$regel["Grootboek"] = "1300";//Grootboek voor debiteuren.
				$lidnummer = $regel["Lidnr"];//Zie soortgelijke consturctie hierboven.			
			}
		}
		
		if($regel["BTW-tarief"] == 6)
		{
			$regel["BTW-tarief"] = '3';//De BTW-code die bij 6% btw hoort.
		}

		if($regel["Type"] == "Pin1" || $regel["Type"] == "Pin2") //De pinautomaten die we nu bezigen
		{
			$regel["Grootboek"] = "8301";//Pininkomsten daar pinnen nooit een uitgaaf kan zijn.
		}

		if($regel["Type"] == "BTW")// Alle dingen die we hebben met btw hebben btw laag
		{
			$regel["Grootboek"] = 1510;
			
		}

		if(!isset($regel["Grootboek"]))
		{
			$regel["Grootboek"] = "8203";	//Grootboek voor overige inkomsten; 8203
		}

		$alt = $regel["Alternator"];
		$regel["Alt"] = $alt;
				
		if(! ($error == ""))
		{
			echo 'Whaa, het ging mis met '.$error.', schop meteen een technisch persoon in zijn ballen!!!!';
			echo $regel;
			die();
		}
		
		if(isset($lidnummer)) //Ik verkracht hier de logische volgorde zodat Lidnummer/Relatienummer als laatste komt.
		{
			$regel["Lidnummer"] = $lidnummer;
			$verzlid[] = $lidnummer;
			unset($lidnummer);
		}
		
		//Niet alles hoeft in de csv;
		unset($regel["Datumtijd"],$regel["Leverancier"],$regel["Alternator"],$regel["Lidnr"],$regel["EAN"],$regel["aantal"], $regel["titel"]);
		
		if(substr($regel["Omschrijving"],0,15) == "Onverklaard kas" || substr($regel["Omschrijving"],0,15) == "Verklaard kasve") //We gaan kasverschillen lekker schreewerig maken.
		{
			foreach( $regel as $key => $value)
			{
				$regel[$key] = strtoupper($value);
			}
		}

		putCsvLine($regel);
	}
	//Dit is een onstellend lelijke constructie om ervoor te zorgen dat lidnummers onder elkaar neergezet worden in de csv
	$verzlid = array_unique($verzlid);
	$csvlid = array();
	foreach($verzlid as $lid)
	{
		$csvlid[] = array($lid);
	}
	foreach($csvlid as $lid)
	{	
		putCsvLine($lid);
	}

	sendfile($gbfilename);
	exit;
}

// laat een afrekening zien
function finShowMaandAfrekening()
{
}

function finGetVerkoopdagGelden($verkoopdag, $verkoopgeld = NULL)
{
	/* Alle waarden uit de verkoopgeldtabel */
	$res = isset($verkoopgeld)?$verkoopgeld:sqlGetVerkoopGeld($verkoopdag);

	/* Totaal verklaarde kasverschillen */
	$res['kasverschil_verklaard'] =
		sqlGetVerklaardeKasverschillen($verkoopdag, TRUE);

	/* Saldo van ver- en terugkopen */
	$verterug  = sqlVerTeruggekocht($verkoopdag);
	$res['verkochtamount'] = array_sum(unzip($verterug['verkoop'],'totaal')) -
		array_sum(unzip($verterug['terugkoop'],'totaal'));

	/* Totaal financien, transacties en onverklaard kasverschil */
	$res['fin_totaal']   = $res['cash'] + $res['pin'] + $res['pinII'] +
		$res['pin1'] + $res['pin2'] + $res['chip'] + $res['facturen'];
	$res['trans_totaal'] = $res['verkochtamount'] + $res['mutaties'] +
		$res['donaties'] + $res['kasverschil_verklaard'];
	$res['kasverschil_onverklaard'] = $res['fin_totaal'] - $res['trans_totaal'];

	return $res;
}

