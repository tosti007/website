<?php
/* $Id$
 *
 * Hier bevinden zich de functies de bestanden kunnen exporteren.
 *
 */

function bonnenexport($rows, $aantallen)
{
	global $PERIODES, $BESTELDATA;

	echo "\\documentclass{article}\n".
		"\\usepackage{bonnen}\n\\begin{document}\n\n";

	echo "\\besteldata{\n";
	$blokken = $PERIODES[colJaar()];
	$bloknr  = 1;
	foreach($BESTELDATA as $datumBestel => $datumLever) {
		if(strcmp($datumBestel, $blokken[$bloknr]) > 0) {
			echo "\\data{{$bloknr}}{}{"
				. strftime("%e %B", strtotime($prevBestel)) . "}{"
				. strftime("%e %B", strtotime($prevLever))  . "}\n";
			$bloknr++;
			if($bloknr > count($blokken)) {
				break;
			}
		}
		$prevBestel = $datumBestel;
		$prevLever  = $datumLever;
	}
	if($bloknr == count($blokken)) {
		echo "\\data{{$bloknr}}{}{"
			. strftime("%e %B", strtotime($prevBestel)) . "}{"
			. strftime("%e %B", strtotime($prevLever))  . "}\n";
	}
	echo "}\n\n";

	foreach($rows as $row)
	{
		$row = array_map('latexescape',$row);
		$titel = cut_titel($row["titel"], false, 60);
		$auteur = cut_auteur($row["auteur"], false, true, 30);

		echo "\\aantalbonnen{".(int)$aantallen[$row['voorraadnr']]."}\n".
			"\\titel{". $titel ."}\n".
			"\\auteur{". $auteur ."}\n".
			"\\vak{}\n".
			"\\prijs{" . Money::addPrice(berekenVerkoopPrijs($row['levprijs'],
			$row['boekenmarge'], @$row['adviesprijs']), FALSE) . "}\n".
			"\\bon\n\n";
	}
	echo "\\end{document}\n";
}

//Krijgt rijen binnen en maakt er een juiste array van rows van.

function inputLijstExport($rows)
{
	foreach ($rows as $row) {
		$new_row = array();

		$new_row['EAN'] = print_EAN($row['EAN'], 'latex');
		// Laat alle voornamen en voorletters weg
		while (preg_match('/,/', $row['auteur'])) {
			$row['auteur'] = preg_replace('/,[^,]*/','',$row['auteur']);
		}
		$new_row['auteurtitel'] = latexescape($row['auteur']).' \hfill '
			.latexescape($row['titel']);

		//Rond de prijs af op twee decimalen
		$prijs = Money::addPrice(berekenVerkoopPrijs($row['levprijs'],
			$row['boekenmarge'], @$row['adviesprijs']), FALSE);
		$new_row['adviesprijs'] = '\euro \,'.str_replace('.', '{,}', $prijs);

		if ($row['telbaar'] == "Y"){
			// Telgegevens in lijst opnemen
			$new_row['voorraad'] = $row['voorraad']?$row['voorraad']:'';
			$new_row['buitenvk'] = $row['buitenvk']?$row['buitenvk']:'';
		} else {
			// Telgegevens niet tonen
			$new_row['voorraad'] = 'n.v.t.';
			$new_row['buitenvk'] = 'n.v.t.';
		}

		$new_row['verkocht'] = '';
		$new_row['totaal'] = '';

		$result[] = $new_row;
	}

	return $result;
}

/* de exporteer-homepage */
function exportHome()
{
	printHTML(_('In dit menu kun je bestanden exporteren.'));

	// verkooplijst
	startForm('','/Onderwijs/Boeken/index.php','','GET','input');
	echo '<h3>'._('Input-lijst voor verkooplijsten').'</h3>';
	endForm('Exporteer',FALSE);

	// verkoopbonnen
	startForm('','/Onderwijs/Boeken/index.php','','GET','bonnen');
	echo '<h3>'._('Verkoopbonnen van de voorraad').'</h3>';

	global $BWDB;
	$res = $BWDB->q('TABLE SELECT *
		FROM voorraad v NATURAL JOIN boeken b, leverancier l
		WHERE voorraad > 0 AND v.leveranciernr = l.leveranciernr
		ORDER BY l.leveranciernr, auteur');

	echo '<p><table class="bw_datatable_medium_100">'.
		'<tr><th>&nbsp;</th><th>'._('Prijs').'</th><th>'._('Aantal')
		.'</th><th>'._('Artikel').'</th></tr>';
	foreach($res as $r) {
		echo '<tr><td>';
			echo addCheckBox('voorraadnrs[]', FALSE, $r['voorraadnr']);
			echo '</td><td align="right">'.Money::addPrice(berekenVerkoopPrijs(
				$r['levprijs'], $r['boekenmarge'], @$r['adviesprijs'])).'</td>';
			echo '<td align="right">' . addInput1("aantal[$r[voorraadnr]]", $r['voorraad'], FALSE, 2) . '</td>'
				.'<td title="'.htmlspecialchars($r['titel']).'">'
				.print_EAN($r['EAN']). ': ' .
				htmlspecialchars($r['auteur'] . ', ' . $r['titel']);
			'</td></tr>';
	}

	endForm('Exporteer',_('Zet voorraad getallen terug'));

	return;
}

function export($action = null)
{
	if (!$action) $action = tryPar('action');

	switch($action) {
		case 'input':
			sendfile('input');
			$lijst = inputLijstExport(sqlVoorraadLijst(FALSE));
			echo strftime("%% Gegenereerd op %Y-%m-%d %H:%M:%S\n\n");
			echo "% ".implode(' & ', array_keys($lijst[0]))."\n";
			echo "\n";
			foreach ($lijst as $rij) {
				echo implode(' & ', $rij)." \\\\ \\hline\n";
			}
			break;
		case 'bonnen':
			$nrs = tryPar('voorraadnrs');
			$aantallen = tryPar('aantal');
			if ( empty($nrs) ) {
				print_error(_('Je moet wel wat bonnen aanchecken.'));
			}
			sendfile('bonnen.tex');
			bonnenExport( sqlBonnen( $nrs ), $aantallen );
			break;
		case 'voorraadtotaal';
			sendfile('voorraad-totaal.csv');
			$voorraad = sqlVoorraadLijst(FALSE, FALSE);
			echo "ISBN;"
				._("Titel").";"
				._("Auteur").";"
				._("Totale Voorraad")."\n";
			foreach ($voorraad as $artikel) {
				putCsvLine( print_EAN($artikel['EAN']),
					$artikel['titel'],
					$artikel['auteur'],
					$artikel['voorraad'] + $artikel['buitenvk']
					);
			}
			break;
		case 'voorraadgescheiden';
			sendfile('voorraad-perlocatie.csv');
			$voorraad = sqlVoorraadLijst(FALSE, FALSE);
			echo "ISBN;"
				._("Titel").";"
				._("Auteur").";"
				._("Voorraad").";"
				._("Buiten verkoop")."\n";
			foreach ($voorraad as $artikel) {
				putCsvLine( print_EAN($artikel['EAN']),
					$artikel['titel'],
					$artikel['auteur'],
					$artikel['voorraad'],
					$artikel['buitenvk']);
			}
			break;
		case '':
			exportHome();
			break;
		default:
			bwnietherkend("export $action");
	}

	exit;
}

