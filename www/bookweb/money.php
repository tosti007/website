<?php
// $Id$

/*
 *   Hier staan alle geldfuncties, specifiek voor BookWeb
 */

function berekenVerkoopPrijs($levprijs, $boekenmarge, $adviesprijs = NULL)
{
	if (!$levprijs) {
		user_error("Interne fout: levprijs niet meegegeven
			($levprijs/$adviesprijs)", E_USER_ERROR);
	}

	if (!isset($boekenmarge)) {
		user_error("Interne fout: boekenmarge niet ingesteld, waarschijnlijk
			programmeerfout (query geeft geen boekenmarge mee)", E_USER_ERROR);
	}
	if ($boekenmarge > 0.02) {
		user_error("Interne fout: boekenmarge meer dan 2%, waarschijnlijk
			programmeerfout", E_USER_ERROR);
	}
	if ($boekenmarge < 0.00) {
		user_error("Interne fout: boekenmarge minder dan 0%, waarschijnlijk
			programmeerfout", E_USER_ERROR);
	}
	return round((1+$boekenmarge)*$levprijs, 2);
}
