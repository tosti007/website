<?php
// $Id$

/**
 * lidfuncties:
 * - overzicht alle bestellingen, lopend en historie
 * - detailinfo bestelling, met nieuws en mogelijkheid afbestellen
 * - afbestellen
 */

/**
 * dit bestelt een boek af.
 * als $bestelnr gegeven is wordt die specifieke SB afbesteld, anders alle
 * openstaande (= nog niet geleverde/geannuleerde) van dat EAN.
 */
function annuleerBoek($bestelnr = null, $EAN , $reden) {
	global $auth;

	if($bestelnr) {
		$bestelnrs = array($bestelnr);
		$statusinfo = sqlStatus($bestelnr);
	} else {
		$openSBs = sqlFindOpenSBs($EAN);
		if(count($openSBs) == 0) {
			print_error(_('Er staan helemaal geen studentbestellingen open van dit boek.'));
		}
		$bestelnrs = array_keys($openSBs);
	}

	if (!$reden) {
		// Reden NIET vertalen: komt in database
		$reden = 'Reden onbekend';
	}

	// een student kan niet meerdere boeken tegelijkertijd annuleren
	if ($bestelnr && $auth->getLidnr() == $statusinfo['lidnr']) {
		$afbesteller = 'student';
	} else {
		// Afbesteld door iemand anders dan de student zelf
		$afbesteller = $auth->getLevel();
		if ($afbesteller == 'god') {
			// anders wordt Rob boos
			$afbesteller = "Webmaster";
		}
		$afbesteller .= " (".PersoonView::naam(Persoon::geef($auth->getLidnr())).")";
	}

	sqlBestelAf($bestelnrs, $afbesteller, $reden);

	// even iedereen mailen hierover
	if(hasAuth('verkoper')) {
		$boekinfo = sqlBoekGegevens($EAN);
		$data = array();
		$data['titel'] = $boekinfo['titel'];
		$data['auteur'] = $boekinfo['auteur'];
		$data['afbesteller'] = $afbesteller;
		$data['reden'] = $reden;

		foreach($bestelnrs as $thisbestelnr) {
			$lidnr = ($bestelnr ? $statusinfo['lidnr'] : $openSBs[$thisbestelnr]['lidnr']);
			bwmail($lidnr, BWMAIL_SB_GEANNULEERD_VAN_HOGERHAND
				,$data);
		}
	}

	processTo4(array($EAN));

	return count($bestelnrs);
}


// afbestellen == bestelling annuleren
function bestelaf() {
	$bestelnr = requirePar('bestelnr');
	$info     = sqlStatus($bestelnr);

	if( $info['lidnr'] != getSelLid() ) {
		print_error(_('Deze bestelling hoort niet bij dit lid.'));
	}

	if(go()) {
		checkRedo();

		annuleerBoek($bestelnr, $info['EAN'], tryPar('reden'));

		printHTML(_("De bestelling van het boek is geannuleerd."));
	}
	else
	{
		printHTML(_("Wil je de bestelling van het volgende boek annuleren?"));
		printHTML("<table>\n".
		"<tr><td>" . _('Titel') . ":</td><td>".htmlspecialchars($info['titel'])."</td></tr>\n".
		"<tr><td>" . _('Auteur') . ":</td><td>".htmlspecialchars($info['auteur'])."</td></tr>\n".
		"<tr><td>" . _('ISBN') . ":</td><td>".print_EAN($info['EAN'])."</td></tr>\n".
		"<tr><td>" . _('Leverdatum') . ":</td><td>".(isset($info['gemailddatum'])?print_date($info['gemailddatum']):'').
			"</td></tr>\n".
		"</table>");

		simpleForm('go');
		addRedo();
		echo '<TR><TD VALIGN=top>' . _('Reden van het annuleren') . ':</TD><TD>'
			.addTextArea('reden','',40,3).'</TD></TR>';
		if (hasAuth('verkoper')) {
			echo '<TR><TD COLSPAN=2><B>' . _('Let op') . ':</B> '
				. _('Deze reden zal ook naar de betreffende student worden gemaild')
				. '.</TD></TR>';
		}

		echo addHidden('bestelnr',$bestelnr);
		endForm(_('Annuleer bestelling'),FALSE);
	}
}

/**
 * kink: geeft alle bestellingen van een lid, gegroepeerd per lopende
 * nog niet verkochte/vervallen bestellingen, en historische bestellingen.
 * geeft ook boekennieuws en link naar meer info per boek.
 */
function bestelling($lidnr)
{
	echo "<h2>"._("Bestelstatus")."</h2>\n\n";

	$bestellingen=sqlGetBestellingen($lidnr);
	if (count($bestellingen) == 0) {
		printHTML(_("Je hebt geen boeken besteld."));
		return;
	}

	$nieuwsEANs = $lopendebestellingen = $geleverdebestellingen =
		$oudebestellingen = array();
	$offertes = sqlOffertePrijzen(unzip($bestellingen, 'EAN'));
	foreach ($bestellingen as $bestelnr => $bestelling) {
		$bestelling['titel'] = makeRef('/Onderwijs/Boeken/Bestellingen/'.$bestelnr,
			$bestelling['titel'] );
		if($bestelling['gekocht']) {
			$bestelling['status'] = _('Gekocht');
			$oudebestellingen[] = $bestelling;
		} elseif ($bestelling['vervallen']) {
			$bestelling['status'] = _('Vervallen');
			$oudebestellingen[] = $bestelling;
		} else {
			$offprijs = @$offertes[$bestelling['EAN']];
			$bestelling['prijs'] = ($offprijs ?
				Money::addPrice($offprijs,true,true):_('onbekend'));
			$nieuwsEANs[] = $bestelling['EAN'];
			if ($bestelling['geleverd']) {
				$geleverdebestellingen[] = $bestelling;
			} else {
				$bestelling['bestelnr'] = $bestelnr;
				$bestelling['leverdatum'] = calcSBLeverdatum($bestelling);
				$lopendebestellingen[] = $bestelling;
			}
		}
	}

	if(count($lopendebestellingen) == 0 && count($geleverdebestellingen) == 0) {
		printHTML(_('Je hebt momenteel geen boeken in bestelling.'));
	} else {
		if (count($geleverdebestellingen) > 0)  {
			printHTML(_('De volgende boeken zijn voor jou <strong>geleverd</strong>:<BR>(klik op een titel voor meer informatie)'));
			echo arrayToHTMLTable($geleverdebestellingen, array
				( 'EAN'    => 'ISBN'
				, 'titel'  => _('Titel')
				, 'auteur' => _('Auteur')
				, 'druk'   => _('Druk')
				, 'vak'    => _('Vak')
				, 'vervaldatum' => _('Vervaldatum')
				, 'prijs'  => _('Richtprijs')
				),
				array('EAN'=>'print_EAN','vervaldatum'=>'print_date'),
				array('vervaldatum' => 'right', 'prijs' => 'right'),
				null, "bw_datatable_medium_100");
		}
		if (count($lopendebestellingen) > 0) {
			printHTML(_('Je hebt de volgende boeken <strong>in bestelling</strong>:<BR> (klik op een titel voor meer informatie of om die bestelling te annuleren)'
					));

			echo arrayToHTMLTable($lopendebestellingen, array
				( 'EAN'    => 'ISBN'
				, 'titel'  => _('Titel')
				, 'auteur' => _('Auteur')
				, 'druk'   => _('Druk')
				, 'vak'    => _('Vak')
				, 'leverdatum' => _('Verwachte leverdatum')
				, 'prijs'  => _('Richtprijs')
				),
				array('EAN'=>'print_EAN','leverdatum'=>'print_date'),
				array('leverdatum' => 'right', 'prijs' => 'right'),
				null, "bw_datatable_medium_100");

			if (sizeOf($nieuwsEANs) > 0) {
				$nieuws = sqlGetBoekenNieuws($nieuwsEANs);
				if (sizeOf($nieuws) > 0) {
					printHTML(_('Er is <B>extra informatie</B> beschikbaar over boeken die door jou besteld zijn:'));
					echo arrayToHTMLTable($nieuws, array
						('EAN'   => 'ISBN'
						,'nieuws' => _('Nieuws')
						,'datum'  => _('Sinds')),
						array('EAN'=>'print_EAN',
							'datum'=>'print_date',
							'nieuws'=>'htmlspecialchars'),
						array('datum' => 'right'), null,
						"bw_datatable_medium_100");
				}
			}
		}
	}

	if (count($oudebestellingen) > 0) {
		echo "<h3>"._("Bestelhistorie")."</h3>\n\n"
			."<p>"._("Dit zijn je bestellingen van de afgelopen 180
			dagen:") ."</p>\n\n";

		echo arrayToHTMLTable($oudebestellingen, array
			( 'status' => _('Status')
			, 'titel'  => _('Titel')
			, 'auteur' => _('Auteur')
			, 'druk'   => _('Druk')
			, 'EAN'   => 'ISBN'
			, 'vak'    => _('Vak')
			),
			array('EAN' => 'print_EAN'),
			array('druk' => 'center'),
			null, "bw_datatable_medium_100");
	}

	return;

}

/**
 * Geef een overzicht van de status van 1 studentbestelling,
 * met info over of het boek al geleverd is, boekennieuws en
 * link naar afbestellen. Voor boekcom ook de mogelijkheid
 * de vervaldatum van een boek te wijzigen.
 */
function bestelstatus()
{
	global $BWDB;

	$bestelnr = requirePar('bestelnr');

	$boekinfo = sqlStatus($bestelnr);
	if (!$boekinfo) {
		print_error(_('Van deze bestelling is geen status op te vragen.'));
	}

	$offertes = sqlOffertePrijzen(array($boekinfo['EAN']));

	if( $boekinfo['lidnr'] != getSelLid() ) {
		print_error(_('Deze bestelling hoort niet bij dit lid.'));
	}

	printHTML(_('Hieronder vind je informatie over de toestand van deze bestelling.'
			));

	printHTML(sprintf(_("Bestelstatus voor bestelling %d:"), $bestelnr));

	printHTML("<i>" . $boekinfo['auteur'] . "</i><br >\n" .
		"<b>" . $boekinfo['titel'] . "</b><br />\n" .
		_('ISBN') . ': ' . print_EAN($boekinfo['EAN']));

	echo '<p><blockquote>'._('Je hebt het boek besteld op ').
		print_date($boekinfo['bestelddatum']).".<br>\n";

	// afbestelbaar bepaalt of je een boek nog kunt afbestellen
	// kan alleen als het nog niet verkocht of vervallen is.
	$afbestelbaar = FALSE;
	if($boekinfo['gekocht']) {
		printf(_('Je hebt het boek <b>gekocht</b> op %s.'),
			print_date($boekinfo['gekochtdatum']));
	} elseif($boekinfo['vervallen']) {
		printf(_("Je bestelling is <b>vervallen</b> op %s.<br />\n
		Vervalreden: <em>%s</em>"),
			print_date($boekinfo['vervaldatum']),
			htmlspecialchars($boekinfo['vervalreden']));
	} elseif ($boekinfo['geleverd']) {
		printf(_('Dit boek ligt voor je klaar bij de boekverkoop. Je kunt het
			komen kopen uiterlijk <em>voor</em> %s.'),
			print_date($boekinfo['vervaldatum']));
		$afbestelbaar = TRUE;
	} else {
		printf(_("Het boek is <b>nog niet geleverd</b><br>
			Het boek wordt verwacht voor jou op %s."),
			print_date(calcSBLeverdatum($boekinfo)));
		$afbestelbaar = TRUE;
	}
	if ( !empty($offertes[$boekinfo['EAN']]) && !$boekinfo['gekocht'] && !$boekinfo['vervallen']) {
		echo "<br />\n";
		printf(_("De richtprijs voor het boek is <em>ongeveer</em> %s."),
			Money::addPrice($offertes[$boekinfo['EAN']],true,true));
	}

	echo "</blockquote></p>\n\n";


	// geef boekennieuws, indien beschikbaar
	$nieuws = sqlGetBoekenNieuws(array($boekinfo['EAN']));

	if(count($nieuws)) {
		printHTML(_('Nieuws:'));

		echo arrayToHTMLTable($nieuws,
			array('datum' => _('Sinds'), 'nieuws' => _('Nieuws')),
			array('datum' => 'print_date', 'nieuws' => 'htmlspecialchars'),
			array('datum' => 'right')
			);
	}

	if ($afbestelbaar) {
		printHTML(makeRef('/Onderwijs/Boeken/Bestellingen/'.$bestelnr.'/Afbestellen',
			_('Annuleer de bestelling van dit boek')));
	}
	if (hasAuth('boekcom') && $boekinfo['geleverd'] && !$boekinfo['gekocht'] && !$boekinfo['vervallen']) {
		printHTML(makeRef('/Onderwijs/Boeken/Bestellingen/'.$bestelnr.'/Vervalwijzig',
			_('Wijzig vervaldatum van dit boek') ));
	}

	printHTML(makeRef('/Onderwijs/Boeken/Bestellingen/',
		_('Terug naar overzicht bestellingen')));
}

/**
 * wijzig de vervaldatum van een bestaande bestelling
 */
function vervalwijzig() {

	echo "<h2>" . _('Vervaldatum wijzigen') . "</h2>";
	$bestelnr = requirePar('bestelnr');
	$info = sqlStatus($bestelnr);

	// extra checks... kunnen in principe niet voorkomen
	if($info['vervallen'] || $info['gekocht'] || !$info['geleverd']) {
		user_error(_('Van deze studentbestelling kan de vervaldatum niet meer gewijzigd worden.'),
			E_USER_ERROR);
	}
	if(!isset($info['vervaldatum'])) {
		user_error(_('Deze bestelling heeft geen vervaldatum...!'), E_USER_ERROR);
	}

	if(go()) {
		$verval = requirePar('vervaldatum');
		if(strcmp($verval, date('Y-m-d')) <= 0) {
			/* voc: %s is de foutieve vervaldatum */
			print_error(sprintf(_('Ho, je wilt een vervaldatum in het verleden invoeren (%s),
				wat betekent dat het boek direct verloopt. Dat is niet de
				bedoeling, je kunt de vervaldatum alleen naar voren schuiven.'), $verval));
		}
		sqlUpdateVervaldatum($bestelnr, $verval);

		/* voc: %s is de vervaldatum */
		printHTML(sprintf(_('Okido, de vervaldatum van deze bestelling is nu %s.'),
			print_date($verval)));

		// geef de gewijzigde bestelstatus weer.
		bestelstatus();

		return;
	}

	simpleForm('go');
	echo addHidden('bestelnr', $bestelnr).
		"<tr><td>" . _('Boek') . ":</td><td colspan=\"2\">".print_EAN_bi($info['EAN']) . ": ".
			/* voc: (boektitel) van (auteur) */
			htmlspecialchars(sprintf(_("%s van %s"), $info['titel'],
				$info['auteur']))."</td></tr>\n".
		"<tr><td>" . _('Besteldatum') . ":</td><td>".print_date($info['bestelddatum'])."</td></tr>\n".
		"<tr><td>" . _('Leverdatum') . ":</td><td>".
			(isset($info['gemailddatum'])?print_date($info['gemailddatum']):'')."</td></tr>\n";
	inputText(_('Vervaldatum'),'vervaldatum', $info['vervaldatum'],
			_('(jjjj-mm-dd)'), 11, 10);
	endForm(_('wijzig'),FALSE);

	return;
}
