<?php
// $Id$

/* Hier staan alle functies die iets met periodes etc te maken hebben.
 *
 * Volgens europese richtlijnen geldt:
 * Week 1 is de week met tenminste 4 dagen in het nieuwe jaar (dwz do-zo).
 * Dus:neem de eerste donderdag van het jaar en je hebt meteen week 1 te
 * pakken.
 */

/**
 * \brief Geeft de periode waarin we ons momenteel bevinden.
 *
 * \sa geefPeriode
 */
function huidigePeriode() {
	global $PERIODES;

	$vandaag      = date('Y-m-d');
	$periodes     = $PERIODES[colJaar()];
	$aantperiodes = sizeOf($periodes);

	for ($i = 1; $i < $aantperiodes; $i++) {
		if ($vandaag < $periodes[$i+1] &&
			($vandaag >= $periodes[$i] || $periodes[$i] > $periodes[$i+1])) {
			return $i;
		}
	}
	return $aantperiodes;
}

/*
 * (kink, 9 maart 2003)
 */

// add a number of days to a given time. The nr can of course be negative.
function cal_AddDays($stamp, $nrofdays, $resultform = NULL)
{
	if (!is_numeric($nrofdays))
		user_error('AddDays: nrofdays not a number', E_USER_ERROR);

	if (!$nrofdays) {
		return isset($resultform)?strftime($resultform, $stamp):$stamp;
	}

	if ($nrofdays > 0)
		$nrofdays = "+$nrofdays";

	$finaltime = strtotime("$nrofdays days", $stamp);
	return isset($resultform)?strftime($resultform, $finaltime):$finaltime;
}

// returns the previous monday for a given date.
// here defined as: if today is monday: last monday returns today.
function cal_getLastMonday($stamp)
{
	if( date('w', $stamp) == 1)
		return $stamp;
	return strtotime ('last Monday', $stamp);
}

// returns the next monday for a given date.
// here defined as: if today is monday: next monday returns today.
function cal_getNextMonday($stamp)
{
	if( date('w', $stamp) == 1)
		return $stamp;
	return strtotime ('next Monday', $stamp);
}


// output een datum
function print_date($datum, $html = TRUE)
{
	if(!isset($datum) || !$datum) {
		return _('onbekend');
	}
	$space = $html?'&nbsp;':' ';
	return strftime('%e' . $space . '%b' . $space . '%Y',strtotime($datum));
}

// output een datum en tijd
function print_datetime($datum)
{
	return strftime('%e&nbsp;%b&nbsp;%Y %R',strtotime($datum));
}

/* Geeft de juiste vervaldatum terug voor het vervallen van een
 * studentenbestelling. Deze functie houdt rekening met vakanties en andere
 * vrije dagen die in $VAKANTIES staan.
 * $begindatum is de eerste dag dat de bestelling afgehaald kan worden (meestal
 * vandaag). Formaat: YYYY-MM-DD
 * $uitvoerformaat is a la strftime (wordt ook gebruikt hierin).
 *
 * Neus, 2 jan '04
 */
function sb_calcVervaldatum($begindatum, $uitvoerformaat = '%Y-%m-%d')
{
	global $VAKANTIES;
	$DAG_IN_SECONDEN = 24 * 60 * 60;

	$verloopdagen    = SB_VERLOOPDAGEN;
	// Voor het rekenen is seconden makkelijker
	$begindatum = strtotime($begindatum);

	// Loop alle vakantiedatums af en probeer een overlappende vakantie te
	// vinden met de verloopperiode
	foreach ($VAKANTIES as $vak_begin => $vak_eind) {
		// Zet deze datums om in seconden
		$vak_begin = strtotime($vak_begin);
		$vak_eind = strtotime($vak_eind);

		// De vakantie is al begonnen voor/begint op de begindatum
		// en is nog bezig
		if ($begindatum >= $vak_begin && $begindatum <= $vak_eind) {
			$begindatum = cal_AddDays($vak_eind, 1);
			continue;
		}

		// De vakantie gaat beginnen ergens tussen begindatum en het aantal
		// verloopdagen na de begindatum
		if ($begindatum < $vak_begin
			&& cal_AddDays($begindatum, $verloopdagen + cal_telToevoegDagen($begindatum, $verloopdagen)) > $vak_begin)
		{
			$dagenvoorvakantie = ($vak_begin - $begindatum) / $DAG_IN_SECONDEN;
			$dagenvoorvakantie = round($dagenvoorvakantie);
			/* aantal dagen voor de vakantie - aantal weekenddagen in die
			 * periode */
			$verloopdagen -= ($dagenvoorvakantie -
				cal_telWeekendDagen($begindatum, $dagenvoorvakantie));
			$begindatum = cal_AddDays($vak_eind, 1);
			continue;
		}
	}

	/* Voeg eventuele extra dagen (dankzij weekenddagen) toe aan $verloopdagen */
	$verloopdagen += cal_telToevoegDagen($begindatum, $verloopdagen);

	return strftime($uitvoerformaat, cal_AddDays($begindatum, $verloopdagen));
}

/* Tel het aantal extra toe te voegen dagen als er nog weekenden zitten tussen
 * $begindatum en $dagen dagen hierna */
function cal_telToevoegDagen($begindatum, $dagen)
{
	$toevoegdagen = 0;
	$weekenddagen = cal_telWeekendDagen($begindatum, $dagen);

	while($weekenddagen > $toevoegdagen) {
		$toevoegdagen = $weekenddagen;
		$weekenddagen = cal_telWeekendDagen($begindatum,
			$dagen + $toevoegdagen);
	}

	return $toevoegdagen;
}

/* Geeft het aantal weekenddagen (dus zater- en zondagen) vanaf $begindatum tot
 * $dagen dagen erna.
 */
function cal_telWeekendDagen($begindatum, $dagen)
{
	$result = 0;

	/* Haal alles >= 7 dagen er alvast af */
	$weekends = (int) floor($dagen / 7);
	$dagen   %= 7;

	/* Vanaf hier geldt: 0 <= $dagen < 7 */

	/* 1 = maandag, ..., 7 = zondag */
	$begindatumweekdag = strftime('%u', $begindatum);

	/* Er zit een zaterdag binnen beriek */
	if ($dagen >= (6 - $begindatumweekdag + 7)%7) {
		$result++;
	}
	/* Er zit een zondag binnen beriek */
	if ($dagen >= 7 - $begindatumweekdag) {
		$result++;
	}

	$result  += 2 * $weekends;
	return $result;
}

function calcVerwachteLeverdatum($EAN)
{
	if (!is_EAN($EAN)) {
		user_error('calcVerwachteLeverdatum: Argument moet een EAN zijn',
			E_USER_ERROR);
	}
	$leverdata = calcVerwachteLeverdatums(array($EAN));
	return $leverdata[$EAN];
}

/* Levert de leverdatum voor een lijst met EANs op aan de hand van de
 * hoeveelheid in vrije verkoop, de huidige bestellingen en het eerstvolgende
 * bestelmoment. */
function calcVerwachteLeverdatums($EANs)
{
	global $BESTELDATA;
	global $BWDB;

	$result = array();
	$bestelling = array();
	$res = $BWDB->q('TABLE SELECT EAN, (aantal-aantalgeleverd) AS verwacht,
			datum
		FROM bestelling
		WHERE aantalgeleverd < aantal
		ORDER BY EAN, datum ASC');
	foreach ($res as $row) {
		$bestellingen[$row['EAN']][] = $row;
	}
	sort($EANs);
	$allefaseninfo = sqlAlleFasenInfo($EANs);

	foreach ($allefaseninfo as $EAN => $faseinfo) {
		/* Check of er nog iets in fase 3 ligt:
		 * Er is nog voorraad in de vrije verkoop => per direct */
		if ($faseinfo['fase_3'] > 0) {
			$result[$EAN] = date('Y-m-d');
			continue;
		/* Check of fase 1 < 0:
		 * Er zijn teveel boeken in bestelling, bij de eerste 'vrije' levering
		 * komt het boek beschikbaar => leverdatum(laatste bestelling) */
		} else if ($faseinfo['fase_1'] < 0) {

			/* Er zijn nog $wachtendenvooru mensen die het boek besteld hebben
			 * en dus voor gaan. */
			$wachtendenvooru = $faseinfo['sb_123'];
			$bestelling = array_shift($bestellingen[$EAN]);

			/* Loop alle bestellingen voor deze EAN af mits er nog bestellingen
			 * EN er nog 0 of meer wachtenden zijn. */
			while($bestelling && $wachtendenvooru >= 0) {
				$wachtendenvooru -= $bestelling['verwacht'];
				if ($wachtendenvooru < 0) {
					$result[$EAN] = calcLeverdatum($bestelling['datum']);
					continue 2;
				}
				$bestelling = array_shift($bestellingen[$EAN]);
			}

			// Hier zou je niet moeten mogen komen.....
			user_error('calcVerwachteLeverdatums: Phase 2 fulfilness problem '
				.'with '.print_EAN($EAN).'!', E_USER_ERROR);
		}

		//return leverdatum van eerstvolgende bestelling > nu
		$result[$EAN] = calcLeverdatum(cal_AddDays(time(), 1));
	}

	return $result;
}

/* Levert de leverdatum op, aan de hand van een besteldatum.
 * Als er een bestelling gedaan wordt hooguit een week na een officieel
 * bestelmoment, wordt de bijbehorende leverdatum van dat bestelmoment nog
 * genomen. Als een bestelling later gebeurt, maar nog voor het volgende
 * bestelmoment, wordt de leverdatum van het eerstkomende bestelmoment als
 * leverdatum opgeleverd. */
function calcLeverdatum($besteldatum = NULL)
{
	global $BESTELDATA;

	if (!isset($besteldatum)) {
		$besteldatum = date('Y-m-d');
	} elseif (!preg_match('/\d{4}-\d{2}-\d{2}/', $besteldatum)) {
		$besteldatum = date('Y-m-d', $besteldatum);
	}

	foreach ($BESTELDATA as $bestelmoment => $leverdatum) {
		if (strtotime($besteldatum) > strtotime('+1 week', strtotime($bestelmoment))) continue;
		return $leverdatum;
	}

	return FALSE;
}

/* Levert de (verwachte) leverdatum op van een reeds geplaatste
 * studentbestelling. In $sbdata zit info over de studentbestelling:
 * 'EAN', 'bestelnr' en 'bestelddatum'. */
function calcSBLeverdatum($sbdata)
{
	$sbsdata = sqlNogNietGekochteSBs($sbdata['EAN']);
	$wachtendenvooru = 0;

	foreach($sbsdata as $row) {
		if ($row['bestelnr'] == $sbdata['bestelnr']) {
			break;
		} else {
			$wachtendenvooru++;
		}
	}

	/* Als de sb niet gevonden is, is er iets mis. */
	if ($row['bestelnr'] != $sbdata['bestelnr']) {
		user_error('calcSBLeverdatum: Unknown studentorderrequest',
			E_USER_ERROR);
	}

	/* Bereken de leverdatum met $wachtendenvooru wachtenden voor de student. */
	$verwachtebestellingen = sqlGetBestellingenByEAN($sbdata['EAN'], TRUE);
	foreach ($verwachtebestellingen as $bestellingdata) {
		$wachtendenvooru -= $bestellingdata['aantal'];

		/* Er is een bestelling gevonden waarbij de bestelling van de student
		 * momenteel hoort */
		if ($wachtendenvooru < 0) {
			/* Lever dus de leverdatum op die bij deze bestelling hoort */
			return calcLeverdatum($bestellingdata['datum']);
		}
	}

	// Nog niet alle studentbestellingen zijn in bestelling bij de leverancier
	return calcLeverdatum($sbdata['bestelddatum']);
}

/**
 * geef de begindatum van de volgende periode.
 */
function periodeEinddatum ($curjaar, $curper) {
	global $PERIODES;

	if(isset($PERIODES[$curjaar][$curper+1])) {
		return $PERIODES[$curjaar][$curper+1];
	}
	if(isset($PERIODES[$curjaar+1])) {
		$nextyear = $PERIODES[$curjaar+1];
		return array_shift($nextyear);
	}

	// hee, dat is raar, er is geen volgende periode gedefinieerd
	return false;
}
