<?php
// als getlidname, maar dan input array lidnrs, output array lidnr->naam
function getLedenNames($lidnrs)
{
	user_error("WSW3 methode aangeroepen", E_USER_DEPRECATED);

	if (!is_array($lidnrs)) {
		user_error("getLedenNames aangeroepen met niet-array",
			E_USER_ERROR);
	}

	counter_mark(__FUNCTION__);
	$ret = getLedenNamenCache($lidnrs);
	natcasesort($ret);
	return $ret;
}

function getLedenNamenCache($lidnrs, $type = WSW_NAME_DEFAULT)
{
	user_error("WSW3 methode aangeroepen", E_USER_DEPRECATED);
	global $WSW4DB;
	static $found = array();

	$todo = array();
	foreach($lidnrs as $lidnr)
	{
		if(!isset($found[$lidnr][$type])) {
			$todo[] = $lidnr;
		}
	}
	if(count($todo))
	{
		$tbl = $WSW4DB->q('KEYTABLE SELECT contactID AS ARRAYKEY,'
						.' voornaam,voorletters,tussenvoegsels,achternaam,'
						.' titelsPostfix,titelsPrefix'
						.' FROM Persoon'
						.' WHERE contactID IN (%Ai)'
						, $todo
						);
		foreach($todo as $lidnr)
		{
			if(!isset($tbl[$lidnr])) {
				$found[$lidnr][$type] = _('onbekend');
				continue;
			}
			$found[$lidnr][$type] = getLidRow2Name($type, $tbl[$lidnr]);
		}
	}

	$res = array();
	foreach($lidnrs as $lidnr)
	{
		$res[$lidnr] = $found[$lidnr][$type];
	}

	return $res;
}

function getLidRow2Name($type, $row)
{
	switch($type) {
		case WSW_NAME_VOORNAAM:
			// wat als voornaam leeg is?
			$naam = $row['voornaam'];
			break;
		case WSW_NAME_FORMEEL:
			$naam = @$row['titels_prefix'].' '.@$row['voorletters'].' '.@$row['tussenvoegsels'].
					' '.$row['achternaam'].' '.@$row['titels_postfix'];
			break;
		case WSW_NAME_INITIALEN:
			$spaces = preg_split('/\s+/',($row['voornaam'] .' '. @$row['tussenvoegsels'] .' '. $row['achternaam']));
			$naam = '';
			foreach ( $spaces as $joe ) $naam .= $joe{0};
			break;
		default:
			if($type != WSW_NAME_DEFAULT)
				user_error("Onbekend type '$type' in getLidName!", E_USER_WARNING);
			// geen voornaam, dan voorletters.
			if(!@$row['voornaam']) {
				$naam = $row['voorletters'];
			} else {
				$naam = $row['voornaam'];
			}
			$naam .= ' '.@$row['tussenvoegsels'].' '.@$row['achternaam'];
	}
	// dubbele spaties wegbokken
	return preg_replace('/ +/', ' ', $naam);
}

//function fotourl($foto,$size='thumb',$type='',$alt='')
// type: lid/act/cie
// size: orig/large/thumb
function addFoto($type, $size, $id, $alt='')
{
	global $THUMB;

	// urlencode() voor search string, rawurlencode voor src=""
	// FIXME: dit kan beter!
	switch ($size) {
		case 'thumb':
			$xsize = 113;
			$ysize = 84;
			
			if ($type=='lid') {
				$xsize = 75;
				$ysize = 100;
				$url = '/Leden/'.$id.'/Foto/Duimnagel';
			} else {
				user_error("WSW3 methode aangeroepen", E_USER_ERROR);
			}
			break;
		// alleen voor cie/act
		case 'large':
			if ( $type == 'lid' ) {
				$url = '/Leden/'.$id.'/Foto';
			} else {
				user_error("WSW3 methode aangeroepen", E_USER_ERROR);
			}
			$xsize=450; $ysize=338;
			break;
	}

	return "<img src=\"".htmlspecialchars($url)."\" width=\"$xsize\" height=\"$ysize\" ".
		"alt=\"".htmlspecialchars($alt)."\" border=\"1\" />";
}
