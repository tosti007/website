<?php

// $Id$

/*

Hier komen alle BoekCom functies

*/

/* bestel alle nog niet geleverde of geannuleerde SB's af */
function annuleerOpenSBs() {

	$EAN = requireEAN();

	if(go()) {
		checkRedo();

		$totaal = annuleerBoek(null, $EAN, tryPar('reden'));

		printHTML(sprintf(_("Ik heb %s studentbestelling(en) van %s afbesteld."),
			$totaal, print_EAN_bi($EAN)));

		return;
	}
	$info = sqlBoekGegevens($EAN);

	printHTML(_("Wil je van het volgende boek alle studentbestellingen die nog niet
		geleverd zijn annuleren?"));
	printHTML("<table>\n".
	"<tr><td>" . _('Titel') . ":</td><td>".htmlspecialchars($info['titel'])."</td></tr>\n".
	"<tr><td>" . _('Auteur') . ":</td><td>".htmlspecialchars($info['auteur'])."</td></tr>\n".
	"<tr><td>" . _('ISBN') . ":</td><td>".print_EAN($info['EAN'])."</td></tr>\n".
	"</table>");

	simpleForm('go');
	addRedo();
	echo '<TR><TD VALIGN=top>' . _('Reden van het annuleren') . ':</TD><TD>'
		.addTextArea('reden','',40,3).'</TD></TR>';
	echo '<TR><TD COLSPAN=2><B>' . _('Let op') . ':</B> '
		. _('Deze reden zal ook naar de betreffende student(en) worden gemaild')
		. '.</TD></TR>';

	endForm(_('Annuleer bestellingen'), FALSE);

}

function alterbestelling() {
	global $BESTUUR;
	global $auth;

	switch(tryPar('action')) {
		case 'altergo':
			$bestelnr  = requirePar('bestelnr');
			$newaantal = requirePar('aantal');
			$besteld   = requirePar('besteld');
			$geleverd  = requirePar('geleverd');

			if ($newaantal < $geleverd) {
				printHTML('<b>' . _('Te weinig besteld') . ':</b> '
					. _('Je kunt niet minder boeken bestellen dan dat er al geleverd zijn')
					. '.', TRUE);
			}
			elseif ($newaantal > $besteld){
				printHTML('<b>' . _('Te veel besteld') . ':</b> '
					. _('Je kunt niet meer boeken in bestelling zetten dan dat
					er oorspronkelijk besteld zijn. Hiervoor moet je een nieuwe
					bestelling plaatsen.'),
					TRUE);
			}
			elseif ($newaantal < $besteld && $newaantal >= $geleverd) {
				sqlChangeLevBestelling($bestelnr, $newaantal);
				$besteldata  = sqlGetBestellingInfo($bestelnr);
				$boekdata    = sqlBoekGegevens($besteldata['EAN']);
				$leverancier = sqlLeverancierData($besteldata['leveranciernr']);

				$maildata = array
					('titel' => $boekdata['titel']
					,'auteur' => $boekdata['auteur']
					,'druk' => $boekdata['druk']
					,'EAN' => print_EAN($besteldata['EAN'])
					,'uitgever' => $besteldata['uitgever']
					,'besteldatum' => $besteldata['datum']
					,'besteld' => $besteld
					,'geleverd' => $geleverd
					,'newbesteld' => $newaantal
					,'leverancier' => $leverancier['naam']
					,'wie' => $auth->getLidnr()
					);
				bwmail($BESTUUR['boekencommissaris']->getEmail(), BWMAIL_LEVERANCIER_WIJZIGING_BESTELLING,
					$maildata);
				printHTML(_('De wijziging is doorgevoerd in Bookweb.'));
				printHTML('<b>'
					. sprintf(_('Nogmaals: Deze bestellingswijziging moet je
						zelf aan de leverancier doorgeven.  Dit gebeurt
						%sniet%s door Bookweb!'),
						'<i>', '</i>') .
				'</b><br>'
					._('Hierover heb je een mailtje ontvangen.'));
				return;
			}
		case 'alter':
			$bestelnr = requirePar('bestelnr');
			$bestelling = sqlLevBestelRes(array($bestelnr));
			echo arrayToHTMLTable($bestelling
				, array('besteldatum' => _('Besteldatum')
					,'titel'    => _('Titel')
					,'auteur'   => _('Auteur')
					,'EAN'     => _('ISBN')
					,'besteld'  => _('Besteld')
					,'geleverd' => _('Geleverd'))
				,array('besteldatum'=>'print_date','EAN'=>'print_EAN_bi')
			);

			echo "\n<P><b>" . _('Let op') . ':</b> '
				. sprintf(_('Deze bestellingswijziging moet je zelf aan de
				leverancier doorgeven. Dit gebeurt %sniet%s door Bookweb!'),
				'<b>', '</b>') . '</P>';
			simpleForm('altergo');
			inputText(_('Nieuw aantal besteld'), 'aantal'
				,$bestelling[$bestelnr]['besteld'], '', 2);
			echo addHidden('bestelnr', $bestelnr);
			echo addHidden('besteld', $bestelling[$bestelnr]['besteld']);
			echo addHidden('geleverd', $bestelling[$bestelnr]['geleverd']);
			endForm('Ok!');
			return;
		case 'go':
			$datum = tryPar('datum');
			if (!$datum) {
				$EAN = requireEAN();
			}
			$bestellingen = sqlZoekBestelling($EAN, $datum);
			if (sizeOf($bestellingen) > 0) {
				echo '<p>' . _('Klik op een van de onderstaande links om de bestelling te veranderen.')
					. "<br>\n "
					. _("Let op: er worden alleen bestellingen weergegeven die nog niet (volledig) geleverd zijn")
					. ".</p>\n";

				foreach ($bestellingen as $bestelling) {
					$bestelling['bestelnr'] = makeBookRef('alterbestelling'
						,$bestelling['bestelnr']
						,'action=alter&bestelnr='.$bestelling['bestelnr']);
					$newbestellingen[] = $bestelling;
				}

				echo arrayToHTMLTable($newbestellingen, array(
					'bestelnr' => _('Bestelnummer'),
					'titel' => _('Titel'),
					'auteur' => _('Auteur'),
					'EAN' => _('ISBN'),
					'aantal' => _('Aantal'),
					'aantalgeleverd' => _('Aantal geleverd')));
			}
			else {
				printHTML(_('Geen bestelling gevonden die aan de eisen voldoet.'));
			}
			return;
		default:
			echo _("Vul zoveel mogelijk in zodat BookWeb op zoek kan gaan naar
				de bestelling.");
			simpleForm();
			inputText(_('EAN, ISBN, titel of auteur'), 'EAN');
			inputText(_('besteldatum'), 'datum');
			endForm(_('Zoek bestelling'));
	}
}


function insertartikel() {

	echo "<h2>" . _('Artikel invoeren') . "</h2>";

	switch(tryPar('action')) {
		case 'go':
			$updEAN = tryPar('updEAN');
			$soort = requirePar('soort');
			$artikel['titel'] = requirePar('titel');
			$artikel['bestelbaar'] = requirePar('bestelbaar');
			$artikel['telbaar'] = requirePar('telbaar');

			if ($soort == '210') {
				$cie = Commissie::geef(requirePar('cienr'));
				$artikel['cienr'] = $cie->getCommissieID();
				$artikel['auteur'] = $cie->getNaam();
			} else if ($soort == '220') {
				// voc: Extern artikel in bookweb
				$artikel['auteur'] = _('Extern');
			} else {
				user_error(_('Onbekend EAN-soort'), E_USER_ERROR);
			}

			if($updEAN) {
				sqlInsBoek($artikel, $updEAN);
				$artikel['EAN'] = $updEAN;
			} else {
				$artikel['EAN'] = sqlGetNextPrivateEAN(requirePar('soort'));
				sqlInsBoek($artikel);
			}
			//voc: %s staat respectievelijk voor titel, ISBN, auteur
			printHTML(sprintf(_('Het artikel %s (%s) van %s is ingevoerd.'),
				"<b>$artikel[titel]</b>",
				print_EAN_bi($artikel['EAN']),
				"<i>$artikel[auteur]</i>"));

			// alleen voor nieuwe artikelen, van a-eskwadraat, en met aantal ingevuld,
			// voeren we een nieuwe levering in
			if ( !$updEAN && $soort == '210' && tryPar('aantal')) {
				$aantal = requirePar('aantal');
				$prijs = Money::checkMoney(tryPar('levprijs'));
				if(tryPar('incex') == 'Y') {
					$btwtarief = tryPar('btwtarief');
					$btw = $prijs - round($prijs / ((100+$btwtarief)/100),2);
				} else {
					$btwtarief = tryPar('btwtarief');
					$btw = round($prijs * $btwtarief/100,2);
					$prijs += $btw;
				}
				if ( !is_numeric($aantal) || !$prijs ) {
					print_warning(_("Aantal / prijs niet geldig, geen levering ingevoerd."));
				} elseif (
					sqlInsLevering(
						$artikel['EAN'],'voorraad', $aantal,$prijs,null,$btwtarief,$btw,
							date('Y-m-d'),LEVNR_AES2,null) ) {
					// voc: %d = aantal, %s = prijs
					printHTML(sprintf(_("Levering van %d stuks voor %s ingevoerd."), $aantal, Money::addPrice($prijs)));
				} else {
					print_warning(_("Fout bij invoeren levering."));
				}
			} else {
				printHTML(_("Er is geen levering ingevoerd."));
			}

			printHTML(makeBookRef('insartikel', _('Nog een artikel invoeren'))."<br>\n".
					makeBookRef('insboek', _('Een boek invoeren'))
					);
			break;
		default:
			printHTML(makeBookRef('insboek', '(' . _('boek invoeren') . ')'));
			printHTML(_('Voer hieronder de gegevens in van het artikel.'));
			simpleForm();

			$EAN = tryPar('EAN');
			if($EAN) {
				$data = sqlBoekGegevens($EAN);
				if(!$data) {
					print_error(sprintf(_('Geen artikel gevonden met EAN %s'), $EAN));
				}
				echo "<tr><td>" . _('EAN') . ":</td><td>".print_EAN_bi($EAN)."</td></tr>\n".
					addHidden('updEAN', $EAN) .
					addHidden('soort', substr($EAN,0,3));
			} else {
				// adhv soort bepalen we de juiste private EAN
				echo '<tr><td>' . _('Soort') . ':</td><td>'.
					addSelect('soort', array('210' => _('A-Eskwadraat-artikel'),
						'220' => _('Extern artikel')), '210', TRUE) . "</td></tr>\n";
			}
			$bbaar = (isset($data['bestelbaar']) && $data['bestelbaar'] == 'Y');
			$tbaar = (isset($data['telbaar']) && $data['telbaar'] == 'Y');
			inputText(_('Omschrijving'), 'titel', @$data['titel']);
			echo '<tr><td>' . _('Commissie') . ':</td><td>'.
				addCiesSelect(@$data['cienr']) ."</td></tr>\n";
			echo '<tr><td>' . _('Bestelbaar') . ':</td><td>'.
				addRadiobox('bestelbaar',$bbaar,'Y').' <label for="bestelbaarY">' . _('Ja') . '</label> '.
				addRadiobox('bestelbaar',!$bbaar,'N').' <label for="bestelbaarN">' . _('Nee') . '</label> '.
				" (" . _('in principe zijn artikelen bijna nooit bestelbaar') . ")</td></tr>\n";
			echo '<tr><td>Telbaar:</td><td>'.
				addRadiobox('telbaar',$tbaar,'Y').' <label for="telbaarY">' . _('Ja') . '</label> '.
				addRadiobox('telbaar',!$tbaar,'N').' <label for="telbaarN">' . _('Nee') . '</label> '.
				" (" . _('dient het artikel geteld te worden bij de boekentelling?') . ")</td></tr>\n";

			if ( empty($data) ) {
				echo "<tr><td colspan=\"2\"><hr /></td></tr>\n" .
					"<tr><td colspan=\"2\">"
					. _('Direct de eerste levering invoeren (optioneel, alleen A-Eskwadraatartikelen)')
					. ":</td></tr>" ;
				inputText(_('Aantal'), 'aantal');
				inputText(_('(Lev.)prijs'), 'levprijs');
				echo '<tr><td>Prijs inclusief BTW:</td><td>'.
					addRadiobox('incex',true,'Y').' <label for="inc">' . _('Ja') . '</label> '.
					addRadiobox('incex',false,'N').' <label for="exc">' . _('Nee') . '</label> ';
				echo '<tr><td>BTW-tarief:</td><td>'.
					addSelect('btwtarief',array('0','6','21'),0);
				echo "<tr><td>" . _('Leverancier') . ":</td><td>" . LEVNR_AES2 . " - A-Eskwadraat</td></tr>\n";
			}
			endForm(_('Voer in!'), FALSE);
			break;
	}
}

function insertboek() {

	echo "<h2>" . _('Boek invoeren') . "</h2>\n";

	switch(tryPar('action')) {
		case 'go':
			$updEAN = tryPar('updEAN');
			$boek['titel']  = requirePar('titel');
			$boek['auteur'] = requirePar('auteur');
			$boek['druk']   = tryPar('druk');
			$boek['uitgever'] = tryPar('uitgever');
			$boek['bestelbaar'] = requirePar('bestelbaar');
			$boek['telbaar'] = requirePar('telbaar');
			if($updEAN) {
				sqlInsBoek($boek, $updEAN);
				$boek['EAN'] = $updEAN;
			} else {
				$boek['EAN'] = requireEAN();
				if (sqlBoekGegevens($boek['EAN']) != NULL) {
					print_error(sprintf(_('Er is in Bookweb al een boek bekend met dit ISBN(%s)'), $boek['EAN']));
				}
				sqlInsBoek($boek);
			}
			//voc: %s staat respectievelijk voor titel, ISBN, auteur
			printHTML(sprintf(_('Het boek %s (%s) van %s is ingevoerd.'),
				"<b>$boek[titel]</b>",
				print_EAN_bi($boek['EAN']),
				"<i>$boek[auteur]</i>"));
			printHTML(makeBookRef('insboek', _('Nog een boek invoeren'))."<br>\n".
					makeBookRef('insvak',	_('Een vak invoeren')) . "<br>\n".
					makeBookRef('insboekvak', _('Dit boek aan een vak koppelen'), 'EAN='.$boek['EAN'])
					);
			break;
		default:
			printHTML(makeBookRef('insartikel', '(' . _('voer een A-Es2artikel in') . ')'));
			printHTML(_('Voer hieronder de gegevens in van het boek. De
				uitgever en druk zijn niet verplicht.'));
			simpleForm();

			$EAN = tryPar('EAN');
			if($EAN) {
				$data = sqlBoekGegevens($EAN);
				if(!$data) {
					print_error(sprintf(_('Geen boek gevonden met ISBN %s'), $EAN));
				}
				echo "<tr><td>" . _('ISBN') . ":</td><td>".print_EAN_bi($EAN)."</td></tr>\n".
					addHidden('updEAN', $EAN) ;
			} else {
				echo "<tr>";

				echo "<td valign=\"top\">" . _('ISBN') . ":</td>";

				echo "<td>";
				echo addInput1('EAN', tryPar("insEAN"));
				echo "<br/><font size=\"1\">";
				echo "(<a href=\"http://books.google.com\"
				onMouseOver=\"this.href = 'http://books.google.com/print?isbn=' + encodeURIComponent(insboek.EAN.value)\" target=\"_blank\">Google Books</a>, ";
				echo "<a href=\"http://www.amazon.co.uk\" onMouseOver=\"this.href = 'http://www.amazon.co.uk/exec/obidos/ASIN/' + encodeURIComponent(insboek.EAN.value)\" target=\"_blank\">Amazon.co.uk</a>)";
				echo "</font>";
				echo "</td>";
				echo "</tr>\n";
			}
			inputText(_('Titel'), 'titel', @$data['titel']);
			inputText(_('Auteur'), 'auteur', @$data['auteur'],
				_('Notatie: Wolffelaar, J.J.A. van/Kinkhorst, T.'));
			inputText(_('Druk'), 'druk', @$data['druk'],
				_('Notatie: 3e editie (hardcover)'));
			inputText(_('Uitgever'), 'uitgever', @$data['uitgever']);
			$bbaar = (!isset($data['bestelbaar']) || $data['bestelbaar'] == 'Y');
			$tbaar = (!isset($data['telbaar']) || $data['telbaar'] == 'Y');
			echo '<tr><td>' . _('Bestelbaar') . ':</td><td>'.
				addRadiobox('bestelbaar',$bbaar,'Y').' <label for="bestelbaarY">' . _('Ja') . '</label> '.
				addRadiobox('bestelbaar',!$bbaar,'N').' <label for="bestelbaarN">' . _('Nee') . '</label> '.
				"</td></tr>\n";
			echo '<tr><td>' . _('Telbaar') . ':</td><td>'.
				addRadiobox('telbaar',$tbaar,'Y').' <label for="telbaarY">' . _('Ja') . '</label> '.
				addRadiobox('telbaar',!$tbaar,'N').' <label for="telbaarN">' . _('Nee') . '</label> '.
				"(" . _('dient het artikel geteld te worden bij de boekentelling?') . ")</td></tr>\n";

			endForm(_('Voer in!'), FALSE);
			break;
	}
}

function insertpakket()
{
	global $BOEKENPLAATSEN;

	$pakketnr = tryPar('pakketnr');
	echo '<h2>'.(isset($pakketnr)?
		(tryPar('action') == 'wis'?_('Pakket wissen') : _('Pakket wijzigen'))
		: _('Pakket invoeren'))
		."</h2>\n";

	switch(tryPar('action')) {
		case 'wisgo':
			checkRedo();
			sqlDeletePakket($pakketnr);
			printHTML(_('Het pakket is verwijderd!'));
			return;
		case 'insert':
			$naam = requirePar('naam');
			if (isset($pakketnr)) {
				/* Het betreft een update */

				/* Wijzig de naam van het pakket */
				$oldnaam = requirePar('oldnaam');
				if ($oldnaam != $naam) {
					sqlUpdatePakket($pakketnr, array('naam' => $naam));
				}

				/* Wis alle voorraadnrs uit $wis en/of pas de aantallen in
				 * het pakket aan uit $aantallen */
				$wis = tryPar('wis', array());
				$aantallen = tryPar('aantal', array());
				/* Geen array_merge(), want dan gaan de keys verloren */
				$nieuweaantallen = $wis + $aantallen;

				sqlUpdatePakketItems($pakketnr, $nieuweaantallen);
			} else {
				/* Het betreft een nieuw pakket */
				$pakketnr = sqlInsertPakket($naam);
			}

			$aantalperean = requirePar('aantalperEAN');

			// parameter voorraadnrs_voorraadlocaties is een array met waardes als '12345-ejbv_voorraad',
			// wat betekent: voorraadnr 12345, te verkopen vanuit voorraadlocatie ejbv_voorraad.
			$voorraadnrs_voorraadlocaties = requirePar('voorraadnrs_voorraadlocaties');

			$pakketitemdata = array();
			foreach ($aantalperean as $EAN => $aantal) {
				$voorraadnr_voorraadlocatie = explode("-", $voorraadnrs_voorraadlocaties[$EAN]);
				$voorraadnr = $voorraadnr_voorraadlocatie[0];
				$voorraadlocatie = $voorraadnr_voorraadlocatie[1];
				Voorraad::isGeldigeVoorraadLocatie($voorraadlocatie); // gooit exception

				$pakketitemdata[$voorraadnr] = array(
					"aantal" => $aantal,
					"voorraadlocatie" => $voorraadlocatie
				);

			}
			sqlInsertPakketItems($pakketnr, $pakketitemdata);

			printHTML(_('De wijzigingen zijn doorgevoerd.'), TRUE);
			break;
		case 'go':
			$pakketnr = tryPar('updpakket');
			$data = tryPar('data');
			checkRedo();

			if (!isset($data)) {
				if (isset($pakketnr)) {
					/* Wijzig de naam van het pakket */
					$oldnaam = requirePar('oldnaam');
					$naam = requirePar('naam');
					if ($oldnaam != $naam) {
						sqlUpdatePakket($pakketnr, array('naam' => $naam));
					}

					/* Wis alle voorraadnrs uit $wis en/of pas de aantallen in
					 * het pakket aan uit $aantallen */
					$wis = tryPar('wis', array());
					$aantallen = tryPar('aantal', array());
					/* Geen array_merge(), want dan gaan de keys verloren */
					$nieuweaantallen = $wis + $aantallen;

					sqlUpdatePakketItems($pakketnr, $nieuweaantallen);
				} else {
					$pakketnr = sqlInsertPakket(requirePar('naam'));
				}

				printHTML(_('De wijzigingen zijn doorgevoerd.'), TRUE);
			}

			if (isset($data)) {
				/* Probeer EANs te parsen */
				$EANdata = parseEANdata($data);

				if (is_array($EANdata)) {
					printHTML(_('Kies hieronder de hoeveelheden, prijzen en voorraadlocaties van de artikelen in het pakket. Let op dat je de juiste voorraadlocatie kiest voor EJBV-pakketten!'));

					simpleForm('insert');
					echo '<tr><td>';

					echo "<ul>\n";
					$mogelijke_voorraadlocaties = array("voorraad", "ejbv_voorraad");
					foreach ($EANdata as $EAN => $artikelinfo) {
						$voorraden = Voorraad::searchByEAN($EAN, false); // array met Voorraad-objecten

						echo '<li>'
							// voc: 'titel' van auteur (ISBN)
							. sprintf(_('\'%s\' van %s (%s)'),
								$artikelinfo['titel'],
								"<i>$artikelinfo[auteur]</i>",
								print_EAN_bi($EAN))
							. ":\n";

						if (count($voorraden) == 0 ) {
							print_error(_("Van dit artikel is nog geen levering
								bekend. Het artikel moet eerst geleverd zijn
								voordat het in een pakket gestopt kan
								worden."));
						}
						echo "<table>\n<tr><td align=right>" . _('Aantal') . ":</td><td>"
							.addInput1("aantalperEAN[$EAN]", 1, NULL, 2)
							."</td></tr>\n"
							.'<tr><td align=right valign=top rowspan='
							.(sizeOf($voorraden) + 1)
							.">" . _('Prijs/leverancier/voorraadlocatie') . ":</td><td><table>\n";

						foreach ($voorraden as $voorraadnr => $voorraad) {
							$vkprijs = $voorraad->berekenVerkoopPrijs();

							foreach ($mogelijke_voorraadlocaties as $voorraadlocatie){
								echo '<tr><td>' .
									addRadioBox(
										"voorraadnrs_voorraadlocaties[$EAN]",
										false,
										$voorraadnr . "-$voorraadlocatie"
									) .
									Money::addPrice($vkprijs) .
									'<font size=-1> (' .
									sprintf( // voc: leverancier, voorraadlocatie, aantal
										_("geleverd door %s naar voorraadlocatie '%s': %d artikelen beschikbaar"),
										$voorraad->getLeverancierNaam(),
										$BOEKENPLAATSEN[$voorraadlocatie], // voorraadlocatie
										$voorraad->getVoorraad($voorraadlocatie) // aantal beschikbaar
									) .
									")</font>" .
									"</td></tr>\n";
							} // end foreach voorraadlocaties
						} // end foreach voorraden
						echo "</table></td></tr>";

						echo "</table><br />\n";
					}
					echo "</ul>\n";

					echo'</td></tr>';
					if (isset($pakketnr)) {
						foreach (tryPar('wis', array()) as
							$voorraadnr => $aantal)
						{
							echo addHidden("wis[$voorraadnr]", $aantal);
						}
						foreach (tryPar('aantal', array()) as
							$voorraadnr => $aantal)
						{
							echo addHidden("aantal[$voorraadnr]", $aantal);
						}
						echo addHidden('oldnaam', tryPar('oldnaam'));
					}
					echo addHidden('naam', tryPar('naam'));
					endForm(_('Voer in'), _('Reset deze pagina'));
					return;
				} else {
					printHTML(_('Controleer wat je hebt ingevuld, en corrigeer
						het eventueel (de > regels kun je negeren), en voer dan
						opnieuw in.'),
						TRUE);
				}
			}

			$pakketnaam = tryPar('naam');
			break;
	}

	$wis = tryPar('action') == 'wis';
	simpleForm($wis?'wisgo':'');

	if(isset($pakketnr)) {
		$pakketinfo = sqlGetPakketten($pakketnr);
		$pakketdata = sqlGetPakketData($pakketnr);
		if(sizeOf($pakketinfo) == 0) {
			echo '</table></form>';
			print_error(sprintf(_('Geen pakket gevonden met pakketnummer %d'), $pakketnr));
		}
		$pakketnaam = $pakketinfo[$pakketnr]['naam'];
		echo addHidden('updpakket', $pakketnr)
			.addHidden('oldnaam', $pakketnaam);
	}

	if ($wis) {
		echo '<tr><td><h3>'.$pakketnaam.'</h3></td></tr>';
	} else {
		inputText(_('Naam van het pakket'), 'naam', @$pakketnaam, null, 35);
	}

	echo "</table>\n";

	if (!isset($pakketdata) ||
		(sizeOf($pakketdata) == 1 && isset($pakketdata[''])) )
	{
		printHTML(_('Er zitten (nog) geen artikelen in dit pakket.'));
	} else {
		echo "<table>\n";
		echo '<tr><th>' . _('Aantal') . '</th><th>' . _('ISBN')
			. '</th><th>' . _('Titel') . '</th>' .'<th>' . _('Auteur')
			. '</th><th>' . _('Verkoopprijs') . '</th>'
			. '</th><th>' . _('Voorraadlocatie') . '</th>'
			. ($wis?'':'<th>(' . _('wis') . ')</th>')
			."</tr>\n";
		$leveranciersdata = sqlLeveranciers();
		$pakketprijs = 0;
		foreach ($pakketdata as $voorraadnr => $voorraaddata) {
			$boekenmarge = $leveranciersdata[$voorraaddata['leveranciernr']]
				['boekenmarge'];
			$vkprijs = berekenVerkoopPrijs($voorraaddata['levprijs'],
					$boekenmarge, @$voorraaddata['adviesprijs']);
			$pakketprijs += $vkprijs;
			echo '<tr><td align=center>'
				.($wis?$voorraaddata['aantal']:
					addInput1('aantal['.$voorraadnr.']',
						$voorraaddata['aantal'], NULL, 2))
				.'</td><td>'.print_EAN_bi($voorraaddata['EAN'])
				.'</td><td>'.$voorraaddata['titel']
				.'</td><td>'.$voorraaddata['auteur']
				.'</td><td align=right>'.Money::addPrice($vkprijs)
				.'</td><td align=right>'.$BOEKENPLAATSEN[$voorraaddata['voorraadlocatie']]
				.($wis?'':'</td><td align=center>'
					.addCheckBox('wis['.$voorraadnr.']', FALSE, 0))
				."</td></tr>\n";
		}
		echo "</table>\n";

		printHTML('<strong>'
			. _('Totale pakketprijs:') .' ' . Money::addPrice($pakketprijs)
			. '</strong>');
	}

	addRedo();
	if (!$wis) {
		echo '<p>' . _('Voeg artikelen toe (&eacute;&eacute;n ISBN/EAN per regel)') . ':'
			."<br />\n";
		echo addTextArea('data', @$EANdata, 90, 10)."</p>\n";
	} else {
		printHTML(_('Weet je zeker dat je het bovenstaande pakket in z\'n geheel wilt wissen?')
				. '<br />'
				. _('Dit kan niet ongedaan gemaakt worden!'),
			TRUE);
	}
	normalEndForm($wis?_('Ja, ik wil dit hele pakket wissen'):
		_('Voer wijzigingen door!'));
}

function insertnieuws() {
	switch(tryPar('action')) {
		case 'insert':
			$EAN = requireEAN();
			$boek = sqlBoekGegevens($EAN);
			$text = requirePar('nieuws');
			sqlInsNieuws($EAN, $text);

			$leden = sqlGetLeden123($EAN);
			echo "<p>"
				// voc: titel, auteur, isbn
				. sprintf(_('Nieuws is ingevoerd voor %s van %s (ISBN:&nbsp;%s)'),
					$boek['titel'],
					"<i>$boek[auteur]</i>",
					print_EAN_bi($EAN)) . ':</p>';
			echo '<TT>'.$text.'</TT>';

			if (sizeOf($leden) > 0) {
				echo sprintf(_('Nu nog even %d leden mailen...'), count($leden));

				foreach($leden as $lidnr) {
					bwmail($lidnr, BWMAIL_BOEKENNIEUWS, array(
						'auteur' => $boek['auteur'],
						'titel'  => $boek['titel'],
						'nieuws' => $text) );
				}

				echo ' ' . _('Klaar') . '<br></p>';
			}
			if ($docenten = tryPar('maildocenten')) {
				printHTML(_('Nu nog even de docenten mailen.'));

				$boekvakdata = sqlGetBoekVakData($EAN, TRUE);

				foreach($boekvakdata as $vakdata) {
					bwmail($vakdata['email'], BWMAIL_DOCENT_BOEKENNIEUWS,
						array
							('auteur'    => $boek['auteur']
							,'titel'     => $boek['titel']
							,'druk'      => $boek['druk']
							,'nieuws'    => $text
							,'docent'    => $vakdata['docent']
							,'vak'       => $vakdata['naam']
							,'verplicht' => $vakdata['verplicht']
							) );
				}

				printHTML(_('Klaar'));
			}

			printHTML(makeBookRef('boekinfo',_('BoekInfo'),'EAN='.$EAN));

			return;
		case 'go':
			$EAN = requireEAN();

			$gegevens = sqlBoekGegevens($EAN);
			if (sizeOf($gegevens) == 0) {
				print_error(_('Dit ISBN is niet bekend in onze database.'));
			}
			$nieuws = sqlGetBoekenNieuws(array($EAN));
			if (sizeOf($nieuws) > 0) {
				echo '<br/>' . _('Voor dit ISBN is al eerder nieuws ingevoerd:');
				echo arrayToHTMLTable($nieuws,
					array('nieuws' => _('Nieuws'),'datum'  => _('Datum')),
					array('nieuws'=>'htmlspecialchars',
					'datum'=>'print_date'), null, null,
					"bw_datatable fullwidth");
			}
			echo "<br/><br/>";
			// voc: titel, auteur, isbn
			printHTML(sprintf(_("Nieuws invoeren voor %s van %s (ISBN:&nbsp;%s)."),
				$gegevens['titel'],
				"<i>$gegevens[auteur]</i>",
				print_EAN_bi($gegevens['EAN'])
				));

			simpleForm('insert');
			echo addHidden('EAN',$EAN);
			echo _('Nieuws:') . '<br>'.addTextArea('nieuws', "", 80, 10);

			$leden = sqlGetLeden123($EAN);
			printHTML(sprintf(_('Dit nieuws zal gemaild worden naar %d studenten.'), sizeOf($leden)));
			echo addCheckBox('maildocenten', TRUE). '<label for="maildocenten">'
				._('Mail ook de docenten van vakken waarbij dit boek gebruikt wordt.')
				.'</label>';
			endForm(_('Voer in, stuur op'));
			return;
		default:
			printHTML(_('Over welk boek wil je nieuws invoeren?'));
			simpleForm();
			inputText(_('EAN, ISBN, titel of auteur'),'EAN');
			endForm(_('Voer in'));
	}
}
//bedacht door Tom^_^
function annuleren()
{
	printHTML("<h3>" . _('Bekijk annuleringen') . "</h3>");
	printHTML("<table width=\"50%\">");
	echo addForm('');
	inputText(_('Datum vanaf: '),'begin_datum',tryPar('begin_datum'));
	inputText(_('Datum tot: '),'eind_datum',tryPar('eind_datum'));
	$opties = array("datum","lid","boek","reden");
	printHTML("<tr><td>" . _('Sorteermethode') . ": </td><td>");
	echo addSelect('sorteer',$opties,tryPar('sorteer'));
	printHTML("</td></tr><tr><td>");
	printHTML(_("andersom") . "</td><td>");
	echo addCheckBox('draaien', tryPar('draaien') == 'ja', 'ja');
	echo "</td></tr><tr><td>";
	echo addFormEnd(_('Jetzt geht Loss!'));
	echo "</td><td>";
	echo "</td></tr></table>";


	$annulering = sqlGetAnnuleringen();
	if( count($annulering)==0){
		echo "Geen annuleringen voor deze periode gevonden:(</br>";
	} else {
		echo arrayToHTMLTable($annulering, array(
			"vervaldatum" => _("Vervaldatum"),
			"EAN" => _("EAN"),
			"lidnr"=> _("Lidnr"),
			"titel" => _("Titel"),
			"vervalreden" => _("Reden")), array('EAN'=>'print_EAN'),NULL,TRUE);
	}
}

// verplaats boeken van en naar voorraad, buitenvk, ejbv_voorraad
// hierdoor worden de aantallen van elk in voorraad aangepast en
// een row toegevoegd aan 'verplaatsing'.
function verplaatsing()
{
	global $BOEKENPLAATSEN;

	if (tryPar('action') == 'getvoorraad') {
		$van  = requirePar('van');
		$naar = requirePar('naar');

		if($van == $naar) {
			print_error(_("Van en naar zijn hetzelfde. Ga terug en probeer het opnieuw."));
		} elseif (!Voorraad::isGeldigeVoorraadLocatie($van)){
			print_error("Voorraadlocatie '$van' is ongeldig.");
		} elseif (!Voorraad::isGeldigeVoorraadLocatie($naar)){
			print_error("Voorraadlocatie '$naar' is ongeldig.");
		}

		$EAN = requireEAN();
		$boekinfo = sqlBoekGegevens($EAN);

		// Zoek alle voorraad-objecten die (op welke locatie dan ook)
		// nog artikelen op voorraad hebben
		$voorraden = Voorraad::searchByEAN($EAN);

		if(count($voorraden) == 0) {
			// voc: titel, auteur, isbn
			print_error(sprintf(_('Er is geen voorraad gevonden voor dit artikel, %s van %s (%s).'),
				$boekinfo['titel'],
				$boekinfo['auteur'],
				print_EAN_bi($EAN)));
		}
		$van_p = $BOEKENPLAATSEN[$van];
		$naar_p = $BOEKENPLAATSEN[$naar];

		// voc: titel, isbn, locatie A, locatie B
		printHTML(sprintf(_("Artikel %s (%s) verplaatsen van %s naar %s."),
				$boekinfo['titel'],
				print_EAN_bi($EAN),
				$van_p,
				$naar_p
				)
			. "<br>\n"
			. _("Geef aan hoeveel je er wil verplaatsen:"));

		simpleForm();
		echo addHidden('van',$van).
			addHidden('naar',$naar).
			addHidden('EAN', $EAN);

		$hasvalidrows = FALSE;
		echo "<table border=\"1\">";
		// voc: aantal boeken op voorraad
		echo '<tr><th>' . _('aantal')
			// voc: locatie 'voorraad'
			. '</th><th>' . _('In reguliere voorraad')
			// voc: locatie 'buiten verkoop'
			. '</th><th>' . _('Buiten verkoop')
			// voc: locatie 'EJBV-voorraad'
			. '</th><th>' . _('EJBV-voorraad')
			// voc: kolomkop 'leveringsprijs'
			. '</th><th>' . _('Leverprijs')
			// voc: kolomkop 'adviesprijs'
			. '</th><th>' . _('Adviesprijs')
			// voc: kolomkop 'leverancier'
			. '</th><th>' . _('Leverancier')
			. "</th></tr>\n";

		foreach($voorraden as $voorraad) {
			echo "<tr><td>";
			if($voorraad->getVoorraad($van) > 0) {
				echo addInput1('voorraadnrs['. $voorraad->getNr() .']', '', FALSE, 4);
				$hasvalidrows = TRUE;
			} else {
				echo FALSE;
			}
			echo "</td>";

			echo "<td align=\"right\">". $voorraad->getVoorraad()."</td>" .
				"<td align=\"right\">".	$voorraad->getVoorraad("buitenvk") ."</td>".
				"<td align=\"right\">".	$voorraad->getVoorraad("ejbv_voorraad") ."</td>".
				"<td align=\"right\">". Money::addPrice($voorraad->getLevPrijs(), true, true, true). "</td>" .
				"<td align=\"right\">".	Money::addPrice($voorraad->getAdviesPrijs(), true, true, true)."</td>" .
				"<td align=\"right\">".	$voorraad->getLeverancierNaam() . "</td>" .
				"</tr>\n";
		}

		echo "</table>\n";

		if(!$hasvalidrows) {
			print_error(sprintf(_('Er zijn geen artikelen op plaats \'%s\' om te verplaatsen.'), $van_p));
		}

		printHTML(_("Wat is de reden? ").addInput1('reden'));
		addRedo();
		endForm(_('Verplaats'));

		return;
	} elseif (go()) {
		// Daadwerkelijk artikelen verplaatsen in database
		checkRedo();
		$van	= requirePar('van');
		$naar	= requirePar('naar');
		$reden	= tryPar('reden');
		$EAN	= requireEAN();
		$voorraadnrs = requirePar('voorraadnrs');

		$van_p = $BOEKENPLAATSEN[$van];
		$naar_p = $BOEKENPLAATSEN[$naar];
		$verplaatst = 0;

		foreach($voorraadnrs as $voorraadnr => $aantal){
			$aantal = trim($aantal);
			if (is_numeric($aantal) && $aantal > 0){
				$voorraad = new Voorraad($voorraadnr);

				$voorraad->verplaats($van, $naar, $aantal, $reden);
				$verplaatst += $aantal;
			}
		}

		// Door deze verplaatsing zouden er nieuwe artikelen aan
		// studentbestellingen gekoppeld kunnen worden, processTo4!
		processTo4(array($EAN));

		printHTML(_("De volgende verplaatsing is geregistreerd:"));
		// voc: voorbeeld: '3 stuks van ISBN 123-456789-1 van locatie a naar locatie b'
		printHTML(sprintf(_('%s stuks van ISBN %s van %s naar %s.'),
				'<b>' . $verplaatst . '</b>',
				'<b>' . print_EAN_bi($EAN) . '</b>',
				"<b>$van_p</b>",
				"<b>$naar_p</b>"
				)
			. "<br>\n"
			. _("Reden:") . "<em>" . htmlspecialchars($reden) . "</em>\n");
		printHTML(makeBookRef('verplaatsing', _('Meer artikelen verplaatsen')));

		return;
	}

	if(tryPar('EAN')) {
		$EAN = requireEAN();
		$boekinfo = sqlBoekGegevens($EAN);
	}

	printHTML(_("Vul de verplaatsinggegevens in en klik op 'verplaats' aub."));
	simpleForm('getvoorraad');
	if(isset($EAN)) {
		echo "<tr><td>" . _('Artikel') . ":</td>".
			"<td>".print_EAN_bi($EAN).": "
			// voc: voorbeeld: 'De Ontdekking van de Hemel' van 'Harry Mullisch'
				. sprintf(_('%s van %s'),
					htmlspecialchars($boekinfo['titel']),
					htmlspecialchars($boekinfo['auteur']))
				. addHidden('EAN', $EAN).
			"</td></tr>\n";
	} else {
		echo "<tr><td>" . _('ISBN, titel, ..') . ":</td><td>".addInput1('EAN').
			"</td></tr>\n";
	}
	// voc: Van <locatie A> (naar locatie B)
	echo "<TR><TD>" . _('Van') . ":</TD><TD>".
		addSelect('van',$BOEKENPLAATSEN, null, true).
		"</TD></TR>\n";
	// voc: (Van locatie A) naar <locatie B>
	echo "<TR><TD>" . _('Naar') . ":</TD><TD>".
		addSelect('naar',$BOEKENPLAATSEN,'buitenvk',true).
		"</TD></TR>\n";
	endForm(_('Verder'));

}

/* Voer een offerte voor een x aantal EANs/ISBNs in. */
function offerte()
{
	global $BWDB;
	$offertenr = tryPar('offertenr');

	echo "<h2>" . _('Offerte invoeren') . "</h2>\n";
	printHTML('<em>'
		. _('Let op: Bestaande offertes van een artikel worden overschreven')
		. '.</em>');

	if (go()) {
		/* Zijn er prijzen ingevoerd? */
		if ($prijsdata = tryPar('prijs')) {
			$levcrnr   = requirePar('leveranciernr');
			$levoffnr  = requirePar('leveranciersoffertenr');
			$offdatum  = requirePar('offertedatum');
			$verldatum = requirePar('verloopdatum');

			$boekinfo = sqlBoekenGegevens(array_keys($prijsdata));

			/* Parse de prijsvelden en geef evt een foutmelding */
			foreach ($prijsdata as $EAN => $prijs) {
				$newprijsdata[$EAN] = Money::parseMoney($prijs,
					FALSE,
					sprintf(_('Prijs van \'%s\''), $boekinfo[$EAN]['titel'])
					);
			}

			/* Voer alle offertes in */
			foreach ($newprijsdata as $EAN => $prijs) {
				sqlVervalOffertes($EAN, $offdatum);
				sqlInsertOfferte($levcrnr, $levoffnr, $EAN, $prijs,
					$offdatum, $verldatum);
				// voc: prijs, boektitel
				printHTML(sprintf(_('Offerte van %s voor \'%s\' ingevoerd.'),
					Money::addPrice($prijs),
					makeBookRef('boekinfo', $boekinfo[$EAN]['titel'], 'EAN='.$EAN)
					));
			}

			printHTML(_('Alle offertes zijn ingevoerd.'));
			return;

		}

		/* Er zijn nog geen prijzen ingevoerd. Maak dat dan nu mogelijk. */
		$data = requirePar('data');
		$EANdata = parseEANdata($data);

		if (is_array($EANdata)) {
			printHTML(_('Voer nu per artikel de offerteprijs in en de algemene
				gegevens van de offerte.'));
			simpleForm("", "class=\"bw_datatable_medium\" width=\"100%\"");
			echo "<tr><th>" . _('Titel') . "</th>" .
				"<th>" . _('Auteur') . "</th>" .
				"<th>" . _('Laatste<br/>offerte') . "</th>" .
				"<th>" . _('Nieuwe<br/>offerteprijs') . "</th></tr>\n";

			$offertesInfo = sqlGetValidOffertesInfo(array_keys($EANdata));

			foreach($EANdata as $EAN => $boekinfo) {
				echo "<tr>" .
					"<td>" . $boekinfo["titel"] . "</td>" .
					"<td>" . $boekinfo["auteur"] . "</td>";

					// Oude offerte tonen
					if (isset($offertesInfo[$EAN]["levprijs"])){
						echo "<td width=\"1\">" .
							Money::addPrice($offertesInfo[$EAN]["levprijs"]) .
							"</td>";
					} else {
						echo "<td>&nbsp;</td>";
					}

				echo '<td align="center">' . 
					addInput1("prijs[$EAN]",tryPar("prijs[$EAN]"), NULL, 6) .
					"</td>" .
					"</tr>\n";
			}
			echo "</table><br />\n";

			echo "<table>\n";
			echo '<tr><td colspan=2><strong>'
				._('Algemene offerte informatie')
				."</strong></td></tr>\n";
			echo "<TR><TD>" . _('Leverancier') . ":</TD><TD>".getLeveranciersSelect()
				."</TD></TR>";
			inputText(_('Offertenummer'),'leveranciersoffertenr', tryPar('leveranciersoffertenr'));
			inputText(_('Offertedatum'), 'offertedatum', tryPar('offertedatum',getActionDate()),
				_('(JJJJ-MM-DD)'));
			inputText(_('Verloopdatum'), 'verloopdatum', tryPar('verloopdatum',strftime('%Y-%m-%d',
				strtotime('6 months', strtotime(getActionDate())))), _('(JJJJ-MM-DD)'));
			endForm(_('Verwerk gegevens'), FALSE);
			return;
		}

		/* Er waren nog fouten bij de invoer van de artikelen waarvoor een
		 * offerte binnen is gekomen. */
		printHTML(_('Controleer wat je hebt ingevuld, en corrigeer het
				eventueel (de > regels kun je negeren), en voer dan opnieuw
				in.'),
			TRUE);

	}

	/* Toon het invoerveld om EANs/ISBNs in te voeren. */
	echo simpleForm();
	echo '<p>'
		. _('Voer een offerte in voor volgende artikelen: (&eacute;&eacute;n ISBN/EAN per regel)')
		. "<br />\n";
	echo addTextArea('data', @$EANdata, 90, 10)."</p>\n";
	echo "</TABLE>";
	normalEndForm(_('Voer in'));
}

function tebestellen()
{
	echo "<h2>" . _('Boeken bestellen') . "</h2>\n\n";

	// of we alle boeken laten zien
	$show_alle_boeken = FALSE;
	
	switch (tryPar('action')) {
		case 'alles':
			$show_alle_boeken = TRUE;
			// geen break
		case '':
			$tebestellen = sqlFaseInfo('1');

			// de tabel is de invultabel, die je kunt gaan invullen dus
			$tabel = array();
			$aantal_boeken = 0;
			$aantal_versch_boeken = 0;
			
			//Haal de prijzen op, zodat bepaald kan worden of het boek wel bestelbaar kan worden.
			$EANs = array();
			foreach ($tebestellen as $EAN=>$info) {
				$EANs[] = $EAN;
			}
			$prijzen = sqlOffertePrijzen($EANs);

			//interpocessing:			
			foreach ($tebestellen as $EAN=>$info) {
				// niet de boeken die niet besteld hoeven worden
				if ($info['aantal'] <= 0) {
					if (!$show_alle_boeken)
						continue;

					// Als we alle boeken laten zien, voorinvullen op 0
					$info['aantal'] = 0;
				}

				$aantal_versch_boeken++;
				$aantal_boeken += $info['aantal'];
				
				$tabelrij = array
					( 'EAN'		 => $EAN
					, 'titel'    => htmlspecialchars($info['titel']    )
					, 'auteur'   => htmlspecialchars($info['auteur']   )
					, 'druk'     => htmlspecialchars($info['druk']     )
					, 'uitgever' => htmlspecialchars($info['uitgever'] )
					);
					
				//boeken zonder offerteprijs kunnen niet besteld worden.
				if($prijzen[$EAN] == NULL)
					$tabelrij['input'] = '';
				else
				{
					$tabelrij['input'] = addInput1("aantal[$EAN]", $info['aantal'],
					FALSE, 5)
						.(!$info['bestelbaar']?
						'<span class=waarschuwing>*</span>':'');
				}
					
				$tabel[] = $tabelrij;
			}

			if (!$show_alle_boeken) {
				echo makeBookRef('tebestellen', _('Klik hier om ook de boeken
					die volgens BookWeb niet besteld hoeven te worden weer te
					geven (LET OP: de huidige selectie wordt weer gereset!)'),
					'action=alles');
			}
			if (!$tabel) {
				// er zijn geen boeken besteld door studenten
				printHTML(_("Er zijn geen te bestellen boeken!"));
				break;
			}

			printHTML(sprintf(_("Er moeten nog %d boeken besteld worden, waarvan %s verschillende."),
				$aantal_boeken,
				$aantal_versch_boeken));
			echo addForm(STARTPAGE);
			addRedo();
			echo addHidden('page', 'tebestellen');
			echo addHidden('action', 'go');
			echo arrayToHTMLTable($tabel, array
				( 'input'    => _('Hoeveelheid')
				, 'EAN'		 => _('ISBN')
				, 'titel'    => _('Titel')
				, 'auteur'   => _('Auteur')
				, 'druk'     => _('Druk')
				, 'uitgever' => _('Uitgever')
				), array('EAN' => 'print_EAN_bi'), null, null, "bw_datatable_medium");
			echo _('Leverancier') . ': ' . getLeveranciersSelect();
			normalEndForm(_("Bestel"));
			printHTML('<span class="waarschuwing">*</span> '
				. _('Let op: Dit artikel staat op \'niet-bestelbaar\' in Bookweb.'));
			break;
		case 'go':
			checkRedo();
			// array_filter haalt alle 0-waarden eruit.
			$aantallen = array_filter(requirePar('aantal'),'intval');

			$leveranciernr=requirePar('leveranciernr');

			$key = sqlLeverancierBestel($aantallen, $leveranciernr);

			printHTML(_("De onderstaande bestelling is ingevoerd:"));

			showBestelling($key, $leveranciernr);
			break;
		default:
			user_error(_("Onbekende action in tebestellen()"), E_USER_ERROR);
	}
}

// Geeft een lijst met leveringen in de afgelopen tijd (met simpele
// selectie), uiteraard klikbaar voor de details
function showLeveringenList(){
	$levermomenteninfo = sqlGetLeverMomenten(null, true);

	echo '<br/><br/><table class="bw_datatable_medium_100">'
		. '	<tr>'
		. '		<th>' . _('Datum') . '</th>'
		. '		<th>' . _('Leveranciers') . '</th>'
		. '		<th>' . _('Geleverde producten') . '</th>'
		. '	</tr>';

	foreach ($levermomenteninfo as $datum => $info){
		$leveranciernrs = $info["leveranciernrs"];
		$EANs = $info["EANs"];

		if (sizeof($leveranciernrs) > 3){
			$leverancieranmen = _("(meer dan 3 verschillende leveranciers)");
		} else {
			$leveranciernamen = array();
			foreach ($leveranciernrs as $leveranciernr){
				$leveranciernamen[] = getLeverancierNaam($leveranciernr);
			}
			$leveranciernamen = implode("<br/>", $leveranciernamen);
		}

		if (sizeof($EANs) > 5){
			$artikelen = _("(meer dan 5 verschillende artikelen)");
		} else {
			$artikelen = array();
			foreach ($EANs as $EAN){
				$gegevens = sqlBoekGegevens($EAN);
				$artikelen[] = $gegevens["titel"]; //cut_titel($gegevens["titel"]);
			}
			$artikelen = implode("<br/>", $artikelen);
		}

		echo '<tr>'
			. '<td valign="top">'
			. makeBookRef("Showleveringen", $datum, "datum=$datum")
			. '</td>'
			. "<td valign=\"top\">$leveranciernamen</td>"
			. "<td valign=\"top\">$artikelen</td>"
			. '</tr>';

	}

	echo "</table>";
}

// Geeft een levering/leveringen weer op het scherm op basis van
// datumselectie en leverancienr. Datumselectie is verplicht, leveranciernr
// niet
function showLeveringen($datum = null, $leveranciernr = null, $datumeind = false, $output = false ,$style = null){
	if ($datum == null){
		user_error(_("Je hebt geen datum opgegeven"),
		E_USER_ERROR);
	}

	$orderby = "";
	if ($style == "pakbon") $orderby = "EAN";
	
	if($datumeind == false)
	{
		$datumeind = $datum;
	}
	$info = sqlGetLeveringen($datum, null, null, $datumeind);

	$vorigedatum = $datum;
	$alt = 0;
	// csv output
	if($output == 'csv') 
	{
		putCsvLine(array("EAN/ISBN","Naam","Leverprijs","Aantal","Bedrag excl. BTW","Bedrag totaal excl. BTW", "Verkoopdatum", "Omschrijving", "Grootboek","Getalletje"));
		foreach($info as $row) 
		{
		
			if($row['btw'] == 6)//Alleen dictaten
			{
				$titel = $row['titel'];
				$aantal = $row['aantal'];
				$leverdatum = substr($row['leveringdatum'],0,10);
				$btw = round($row['levprijs'] * (0.06/1.06),2);
				$leverprijsexbtw = $row['levprijs'] -$btw;
				$totaalprijsexbtw = $leverprijsexbtw * $row['aantal'];
				$omschrijving = $titel.", ".$aantal." stuks a ".$leverprijsexbtw. " excl btw";
				
				if($leverdatum !== $vorigedatum)
				{
					$alt= ++$alt;//Geen uniek getal per dag. 
				}
			
putCsvLine(array(print_EAN($row['EAN']),$titel,$row['levprijs'],$aantal,
$leverprijsexbtw,$totaalprijsexbtw, $leverdatum, $omschrijving, "3102",$alt));

putCsvLine(array(print_EAN($row['EAN']),$titel,"",$aantal,
"","", $leverdatum, "", "1303",$alt));

$omschrijving2 = $titel.", ".$aantal." stuks";
putCsvLine(array(print_EAN($row['EAN']),$titel,"",$aantal,
"","", $leverdatum, $omschrijving2, "0102",$alt));
		
				$vorigedatum = $leverdatum;
			}
		}
		sendfile("levering_{$datum}_{$datumeind}.csv");
		exit;			
	}
	else
	{
		// Checken of er leveringen waren van meerdere leveranciers,
		// dat kunnen we gebruiken in de interface.
		$leveranciernrs = array();

		foreach ($info as $rij){
			$leveranciernrs[] = $rij["leveranciernr"];
		}
		$leveranciernrs = array_unique($leveranciernrs);


		if (sizeof($leveranciernrs) > 1){
			echo '<br/>'
				. _('Let op! Op deze dag is er voor meerdere leveranciers een
					levering ingevoerd, namelijk:')
				. '<br/>';
			echo '<div style="margin-left: 20px">';
			foreach ($leveranciernrs as $thisLeveranciernr){
				echo makeBookRef("Showleveringen",
				getLeverancierNaam($thisLeveranciernr),
				"datum=$datum&leveranciernr=$thisLeveranciernr")
					. '<br/>';
			}
			echo '</div><br/>';

			echo _('Klik op de leveranciernaam om alleen de leveringen van die
				leverancier te zien.');

			if ($leveranciernr != null){
				echo sprintf(
					// voc: leveranciernaam
					_('Op dit moment worden alleen de artikelen getoond die door %s
						zijn geleverd.'),
					getLeverancierNaam($leveranciernr));
			} else {
				echo _('Hieronder zie je alle leveringen bij elkaar.');
			}

			echo '<br/>';
		}

		echo '<h2>';
		if ($leveranciernr != null){
			// voc: leveranciersnaam
			echo sprintf(_('Leveringen door %s'), getLeverancierNaam($leveranciernr));
		} else if (sizeof($leveranciernrs) > 1){
			echo _('Leveringen door meerdere leveranciers');
		} elseif (sizeof($leveranciernrs) == 1){
			// voc: leveranciersnaam
			echo sprintf(_('Leveringen door %s'), getLeverancierNaam($leveranciernrs[0]));
		}

		// voc: op <datum>
		echo sprintf(_(" op %s"), $datum) . ":</h2>";

		echo '<table class="bw_datatable_medium_100">';
		echo '	<tr>';
		echo '<th>' . _('ISBN/EAN') . '</th>';
		echo '<th>' . _('Titel') . '</th>';
		echo '<th>' . _('Auteur') . '</th>';
		echo '<th>' . _('Prijs') . '</th>';
		echo '<th>' . _('Aantal') . '<br/><span class="tiny">(' . _('geleverd') . ')</span></th>';
		echo '<th>' . _('Aantal') . '<br/><span class="tiny">(' . _('besteld') . ')</span></th>';
		echo '<th width="1">' . _('besteldatum') . '</th>';
		echo "	</tr>\n";


		$bonnendata = array(); // tijdelijke array met bonneninformatie

		foreach ($info as $rij){
			$bestellingdatum = $rij["bestellingdatum"];
			$levbestelnr = $rij["levbestelnr"];
			$voorraadnr = $rij["voorraadnr"];
			$aantal = $rij["aantal"];
			$aantalbesteld = $rij["bestellingaantal"];
			$thisLeveranciernr = $rij["leveranciernr"];

			// Goede leveranciernr? Tonen of niet tonen?
			if ($leveranciernr != null && $thisLeveranciernr != $leveranciernr)
				continue;

			echo "<tr>";
			echo "<td>" . print_EAN_bi($rij["EAN"]) . "</td>";
			echo "<td>" . cut_titel($rij["titel"]) . "</td>";
			echo "<td>" . cut_auteur($rij["auteur"]) . "</td>";
			echo "<td>" . Money::addPrice($rij["levprijs"]) . "</td>";
			echo "<td>$aantal</td>";
			echo "<td>$aantalbesteld</td>";

			echo "<td>";
			if ($levbestelnr){
				echo makeBookRef("showbestelling", $bestellingdatum,
				"datum=$bestellingdatum&leveranciernr=$leveranciernr");
			}
			echo "</td>";
			echo "</tr>";

			if (isset($bonnendata[$voorraadnr])){
				$bonnendata[$voorraadnr] = $bonnendata[$voorraadnr] + $aantal;
			} else {
				$bonnendata[$voorraadnr] = $aantal;
			}

		}

		$bonnenvars = "page=export&action=bonnen&";
		foreach ($bonnendata as $voorraadnr => $aantal){
			$bonnenvars .= "voorraadnrs[]=$voorraadnr&aantal[$voorraadnr]=$aantal&";
		}

		echo "</table>";
		echo "<br/>";

		// terug naar lijst
		echo makeBookRef("Showleveringen", _("Toon lijst met alle leveringen"))
			. "<br/>";

		// bonnen exporteren
		echo "<a href=\"/Onderwijs/Boeken/index.php?$bonnenvars\" target=\"_SELF\">"
			// voc: leverdatum
			. sprintf(_("Bonnen exporteren van alle leveringen op %s"), $datum)
			. "</a>";
	
		echo "<br>";
			// bonnen exporteren
		echo "<a href=\"/Onderwijs/Boeken/index.php?page=showleveringen&output=csv&datum=".$datum."&datumeind=".$datumeind."\" target=\"_SELF\">"
			// voc: leverdatum
			. sprintf(_("Poep een csv van de dictaten in de bovenstaande data uit"))
			. "</a>";
		echo "<br>Als je in je url '&datumeind=2013-09-20' erachter plakt, dan kun je de bovenstaande data uitbreiden zodat alle leveringen tot die datum ook getoond worden, dit veranderd ook de data die in je csv komt. Als je deze niet invult, dan wordt datumeind hetzelfde als je begindatum";
	}
}


// kink: geeft een bestelling weer op het scherm of als CSV
// in 'bestelling' is datum de key.
function showBestelling($datum = null, $leveranciernr = null) {

	if(!$datum || !$leveranciernr) {

		echo _("Welke bestelling gaat het om?") . "\n<table>".
			addForm(STARTPAGE, 'GET') .
			addHidden('page', 'showbestelling').
			'<tr><td>' . _('Leverancier') . ':</td><td>'.
			getLeveranciersSelect($leveranciernr).
			"</td></tr>\n<tr><td>" . _('Datum') . ":</td><td>".
			addInput1('datum', ($datum?$datum:getActionDate())).
			"</td></tr>\n</table><p>\n\n";
		normalEndForm(_('Engage'));
		return;
	}

	$bdata = sqlGetBestellingByDate($datum, $leveranciernr);

	if(count($bdata) == 0) {
		print_error(_('Geen bestelgegevens gevonden.'));
	}

	// csv output
	if(tryPar('output') == 'csv') {
		sendfile('bestelling_'.$datum.'_'.$leveranciernr.'.csv');
		echo "aantal;ISBN/EAN;titel;auteur;druk;uitgever\n";
		foreach($bdata as $row) {
			putCsvLine($row['aantal'],
				print_EAN($row['EAN']) ,
				$row['titel'],
				$row['auteur'],
				@$row['druk'],
				@$row['uitgever'] );
		}
		return;
	}

	// voc: besteldatum
	echo "<h2>" . sprintf(_('Bestelling van %s'), print_date($datum)) . "</h2>";

	echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n".
		'<tr><th>' . _('Aantal') . '</th><th>' . _('Geleverd') . '</th><th>'
		. _('ISBN') . '</th><th>' . _('Titel') . '</th><th>' . _('Auteur')
		. '</th><th>' . _('Druk') . "</th></tr>\n";
	foreach($bdata as $row) {
		$rowstyle = ($row['aantal'] == $row['aantalgeleverd'])
			?'bw_onbelangrijk':'bw_belangrijk';
		echo '<tr class="'.$rowstyle.'"><td align="right">'.
			$row['aantal'].
			'</td><td align="right">'.
			$row['aantalgeleverd'].
			'</td><td>'.print_EAN_bi($row['EAN']).'</td><td>'.
			htmlspecialchars($row['titel']).
			'</td><td>'.
			htmlspecialchars($row['auteur']).
			'</td><td>'.
			((isset($row['druk']) && $row['druk']) ? htmlspecialchars($row['druk']) : '&nbsp;').
			"</td></tr>\n";
	}
	echo "</table>\n\n";

	printHTML(makeRef('index.php',_('Download deze bestelling in CSV-formaat'),
		array( 'page' => 'showbestelling',
				'datum' => $datum,
				'leveranciernr' => $leveranciernr,
				'output' => 'csv')));

	return;

}


/* Tabel met de te leveren boeken door de boekenleverancier
 * Jacob 11-03-2003 */
function televeren() {
	$newrows = array();

	$mergedatums = tryPar("mergedatums", false);
	$orderby = tryPar("orderby", "normaal");
	$from = tryPar("from");
	$to = tryPar("to");
	$statusoverzicht = tryPar("statusoverzicht");

	$rows = sqlTeLeveren($orderby, $mergedatums, $from, $to);

	// EANs verzamelen voor faseninfo
	$EANs = array();
	foreach ($rows as $row){
		$EANs[] = $row["EAN"];
	}

	$fase1_info = sqlFaseInfo("1", $EANs);
	$fase2_info = sqlFaseInfo("2", $EANs);

	foreach ($rows as $row) {
		// 2006-01-19 Bas en Jeroen twijfelen over de correctheid van
		// onderstaande code. Uniekheid levbestelnr is niet gegarandeerd.
		// Boekcom kan bestelling wijzigen via boekinfo
		/*$row['besteld'] = makeBookRef('alterbestelling', $row['besteld'],
			'action=alter&bestelnr='.$row['levbestelnr']);*/

		if (strpos($row['datum'], ",") === false){
			// Geen komma, dus enkele datum: link en datum printen
			$row['datum'] = makeBookRef('showbestelling', print_date($row['datum']),
				'datum='.$row['datum'].'&leveranciernr='.$row['leveranciernr']);
		} else {
			// Meerdere datums
			$row['datum'] = "(meerdere)";
		}

		// fase1: aantal bestelde boeken door studenten besteld
		$fase1 = $fase1_info[$row["EAN"]]["aantal"];

		// fase2: aantal boeken in bestelling bij de leverancier
		$fase2 = $fase2_info[$row["EAN"]]["aantal"]; // aantal in bestelling bij leverancier

		// fase12 >= 0  er zijn geen boeken over
		// fase12 < 0	er zijn boeken over
		$fase12 = $fase1 + $fase2;

		if ($fase12 >= 0){
			// Er zijn nog boeken nodig (of niet over)
			if ($fase1 >= 0){
				// niet teveel besteld
				$overschot = 0;
			} else {
				// te veel besteld
				$overschot = -$fase1;
			}
		} else{
			// er zijn boeken over
			$overschot = $fase2;
		}

		$row["overschot"] = $overschot;

		$newrows[] = $row;
	}

	printHTML('Hieronder vind je een tabel met alle boeken die in bestelling '
		.'staan. Gebruik het formulier om een selectie te maken of om de '
		.'opmaak van de lijst te wijzigen.');
	echo "<hr/>\n";

	$selSort = array(
		"normaal" => "besteldatum, auteur",
		"EAN" => "ISBN/EAN");

	echo addForm("", "GET");
	echo addHidden("page","televeren");

	echo "<table width=\"600\">\n";

	echo "	<tr>\n";
	echo "		<td>";
	echo "Selectie besteldatum:";
	echo "</td>\n";
	echo "		<td>";
	echo addInput1("from", $from, false, 10, 10);
	echo " tot ";
	echo addInput1("to", $to, false, 10, 10);
	echo " (YYYY-MM-DD)";
	echo "</td>\n";
	echo "	</tr>\n";

	echo "	<tr>\n";
	echo "		<td>";
	echo "Sorteren op:";
	echo "</td>\n";
	echo "		<td>";
	echo addSelect("orderby", $selSort, $orderby, true);
	echo "</td>\n";
	echo "	</tr>\n";

	echo "	<tr>\n";
	echo "		<td>&nbsp;</td>\n";
	echo "		<td>";
	echo addCheckbox("mergedatums", $mergedatums);
	echo " Verschillende besteldatums op 1 regel";
	echo "</td>\n";
	echo "	</tr>\n";

	echo "	<tr>\n";
	echo "		<td>&nbsp;</td>\n";
	echo "		<td>";
	echo addCheckbox("statusoverzicht", $statusoverzicht);
	echo " Toon data in &quot;statusoverzicht&quot;-vorm";
	echo "</td>\n";
	echo "	</tr>\n";

	echo "	<tr>\n";
	echo "		<td>&nbsp;</td>\n";
	echo "		<td>";
	echo addFormEnd("Zoeken!", false);
	echo "</td>\n";
	echo "	</tr>\n";
	echo "</table>\n";
	echo "<hr/>\n";

	if (!$statusoverzicht){
		echo arrayToHTMLTable($newrows, array
			('besteld' => 'Besteld'
			,'nogleveren' => 'Nog leveren'
			,'overschot' => 'Annuleerbaar'
			,'titel' => 'Titel'
			,'auteur' => 'Auteur'
			,'EAN' => 'ISBN'
			,'leveranciernr' => 'Leverancier'
			,'datum' => 'Besteldatum'),
			array(
				'EAN' => 'print_EAN_bi',
				'titel' => 'cut_titel',
				'auteur' => 'cut_auteur',
				'leveranciernr' => 'getLeverancierNaam'
			),
			array('datum' => 'right'),
			"", "bw_datatable_medium_100");
	} else {
		echo arrayToHTMLTable($newrows, array
			('EAN' => 'ISBN'
			,'titel' => 'Titel'
			,'auteur' => 'Auteur'
			,'nogleveren' => 'Nog leveren'
			,'leveranciernr' => 'Leverancier'
			,'datum' => 'Besteldatum'),
			array(
				'EAN' => 'print_EAN_bi',
				'leveranciernr' => 'getLeverancierNaam'
			),
			array('datum' => 'right'),
			"", "bw_datatable_medium_100");
	}

}

function totalevoorraad()
{
	$leveranciers = tryPar("leveranciers", array(BOEKLEVNR));
	$datum = tryPar("datum");

	echo "<h2>Boekenvoorraad</h2>\n";
	echo addForm("", "GET");
	echo "<table width=\"100%\"><tr>" .
		"<td width=\"50%\">" . bwfLeveranciers("leveranciers") . "</td>" .
		"<td width=\"50%\" valign=\"top\">" .
			"<table width=\"100%\" class=\"bw_datatable_medium\"><tr><th>Datum en tijd</th></tr>" .
			"<tr><td align=\"center\">" .
				addInput1("datum", $datum, false, 30) . "<br/>" .
				"<span style=\"font-size: 10pt\">(yyyy-mm-dd hh:mm:ss)</span>" .
			"</td></tr>" .
			"</table>" .
		"</td></tr>" .
		"<tr><td colspan=\"2\" align=\"center\">" .
			"<input type=\"submit\" value=\"Zoeken!\">" .
		"</td></tr></table>";
	echo "</form>";

	$voorl = sqlVoorraadLijst(false, true, $leveranciers, $datum);
	foreach($voorl as $id => $row) {
		$voorl[$id]['advprijs'] = berekenVerkoopPrijs (
			$row['levprijs'], $row['boekenmarge'], @$row['adviesprijs']);
	}

	$selLeveranciers = array();
	$levData = sqlLeveranciers();
	foreach ($leveranciers as $leveranciernr){
		$selLeveranciers[] = $levData[$leveranciernr]["naam"];
	}

	if (!$datum) $strDatum = "dit moment";
	else $strDatum = $datum;

	echo "<div align=\"center\">" .
		"Voorraad van leverancier(s) <em>" . implode(", ", $selLeveranciers) . "</em> op <em>" . $strDatum . "</em>" .
		"</div>";

	if (!$datum){
		echo arrayToHTMLTable( $voorl,
			array(
				'voorraad'=>'voorraad',
				'buitenvk'=>'buiten vk',
				'auteur' => 'auteur',
				'titel' => 'titel',
				'EAN' => 'ISBN',
				'advprijs' => 'adviesprijs'
				),
			array('EAN'=>'print_EAN_bi', 'auteur' => 'htmlspecialchars',
				'titel' => 'htmlspecialchars', 'advprijs'=>'Money::addPrice'),
			array('voorraad' => 'center', 'buitenvk' => 'center',
				'advprijs' => 'right'), null, "bw_datatable_medium_100");
	} else {
		echo arrayToHTMLTable( $voorl,
			array(
				'aantal'=>'aantal',
				'auteur' => 'auteur',
				'titel' => 'titel',
				'EAN' => 'ISBN',
				'advprijs' => 'adviesprijs'
				),
			array('EAN'=>'print_EAN_bi', 'auteur' => 'htmlspecialchars',
				'titel' => 'htmlspecialchars', 'advprijs'=>'Money::addPrice'),
			array('aantal' => 'center',
				'advprijs' => 'right'), null, "bw_datatable_medium_100");
	}

	echo "<table>\n";
	echo "<tr>\n<td>";
	startForm('export', 'index.php','','GET', 'voorraadtotaal');
	endForm('Voorraad als CSV-bestand', FALSE);
	echo "</td>\n<td>";
	startForm('export', 'index.php','','GET', 'voorraadgescheiden');
	endForm('Voorraad per locatie als CSV-bestand', FALSE);
	echo "</td>\n</tr>\n";
	echo "</table>\n";
}


// veel info over 1 boek (kink)

function showBoekInfo()
{
	global $logger;

	if(tryPar('EAN')) {
		$EAN = requireEAN();
	}

	echo 'EAN/ISBN/A123/E123/auteur/titel: '.
		addForm('', 'GET').
		addHidden('page', 'boekinfo').
		addInput1('EAN', htmlspecialchars(@$EAN), FALSE, 30);
	normalEndForm('opzoeken');

	if (!isset($EAN) || !$EAN) return;

	//echo "<span class=\"header1\">Boekinfo</span><br/>\n";

	// verzamel de benodigde info
	$info		= sqlAlleFasenInfo(array($EAN));
	$fase		= @$info[$EAN];
	if (!$fase) {

		if (hasAuth("boekcom")){
			$isbn = EAN_to_ISBN($EAN);
			$link = "/Onderwijs/Boeken/Insboek?insEAN=$isbn";
			print_error(print_EAN($EAN).
				" onbekend bij Bookweb, als boekencommissaris (of vice) ben
				je &eacute;&eacute;n van de gelukkige mensen die een <a
				href=\"$link\">nieuw artikel</a> kunnen invoeren met deze
				artikelcode!"
			);
		} else {
			print_error(print_EAN($EAN) .
				" onbekend bij Bookweb, misschien kun je de boekcom lief
				aankijken voor meer informatie over dit boek/artikel?"
			);
		}
	}
	$bvinfo			= sqlGetBoekVakData($EAN);
	$offertes		= sqlGetOffertesInfo(array($EAN));
	$papiermolen	= count(sqlPapierMolenAangeboden(array($EAN)));
	$bestellingen	= sqlGetBestellingenByEAN($EAN);
	$nieuws			= sqlGetBoekenNieuws(array($EAN), TRUE);
	$isbn = ean_to_isbn($EAN);


	// algemene info: druk/uitgave, en vakken
	echo "<table class=\"bw_datatable_medium\" width=\"100%\">\n".
		"<tr><th colspan=\"3\">".
		"<span class=\"header2\">'".
		htmlspecialchars($fase['boek_titel'])."' van '".
		htmlspecialchars($fase['boek_auteur'])."'</span>".
		"</td></tr><tr>";

	// Boekinfo
	echo "<td valign=\"top\" width=\"40%\">" .
		"<table><tr>\n";

	// Plaatje van het boek, alleen als er een ISBN is (= geen A-ES2 artikel)
	if ($isbn){
		echo "<td valign=\"middle\" rowspan=\"6\">" .
//			"<img src=\"http://www.broese.net/domains/bgn/content/productimages/$isbn.jpg\" alt=\"\" />" .
			"<a href=\"http://lib.syndetics.com/index.aspx?isbn=$isbn/LC.JPG&client=bccls&type=xw12\">" .
			"<img src=\"http://lib.syndetics.com/index.aspx?isbn=$isbn/MC.JPG&client=bccls&type=xw12\">" .
			"</a></td>";
	}

	echo "<td><b>Artikelnummer:</b></td><td>".print_EAN($EAN)."</td></tr>\n".
		"<tr><td valign=\"top\"><b>Druk:</b></td><td>".@$fase['boek_druk']."</td></tr>\n".
		"<tr><td valign=\"top\"><b>Uitgever:</b></td><td>".@$fase['boek_uitgever']."</td></tr>\n".
		"<tr><td valign=\"top\"><b>Bestelbaar:</b></td><td>".print_yn($fase['boek_bestelbaar'])."</td></tr>\n".
		"<tr><td valign=\"top\"><b>Telbaar:</b></td><td>".print_yn($fase['boek_telbaar'])."</td></tr>\n".
		"<tr><td></td><td align=\"right\">";

	if (hasAuth('boekcom')) {
		echo '<small>'.makeBookRef((substr($EAN,0,3) == '210' ? 'insartikel' : 'insboek'),
			'(wijzig)','EAN='.$EAN).'</small>';
	}

	echo "</td></tr></table>\n";

	if (hasAuth('verkoper') && ean_to_isbn($EAN)) {
		echo "<small>(";
		echo makeExternLink("http://lib.syndetics.com/index.aspx?isbn=$isbn/index.html&client=bccls&type=xw12", "Syndetics") . ", ";
		echo makeExternLink("http://books.google.com/books?q=isbn:$isbn", "Google Books") . ", ";
		echo "Amazon.{" . makeExternLink("http://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=$isbn", "com") . ", ";
		echo makeExternLink("http://www.amazon.co.uk/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=$isbn", "co.uk") . ", ";
		echo makeExternLink("http://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=$isbn", "de") . ", ";
		echo makeExternLink("http://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=$isbn", "fr") . "}, ";
		echo makeBroeseLink($EAN, "Selexyz");
		echo ")</small>";
	}

	echo "</td><td valign=\"top\" style=\"border-left: 1px dashed lightgray\">\n".

	// vakken

		"<table>\n".
		"<tr><td valign=\"top\"><b>".(count($bvinfo)>1?'Vakken':'Vak').":</b></td><td><table>" ;
	foreach($bvinfo as $vak) {
		echo '<tr><td>['.$vak['collegejaar'].'&nbsp;'.implode(',',array_keys($vak['studies'])).']</td><td>'.
			makeBookRef('vakinfo', htmlspecialchars($vak['naam']), 'vaknr='.$vak['vaknr']).
			'</td><td>'.
			htmlspecialchars($vak['docent']).
			"</td></tr>\n";
	}

	echo	"</table>\n";
	if (hasAuth('boekcom')) {
		echo '<small>'.makeBookRef('insboekvak','(voeg vak toe)','EAN='.$EAN)
			.'</small>';
	}
	echo
			"</td></tr>\n".
			"</table>\n</td></tr>\n</table>\n"; // pff wat een nesting zeg

	echo makeBookRef('voorraadmutaties', 'Voorraadhistorie', 'EAN='.$EAN)
		. ", \n";
	echo makeBookRef('toonverkopenperproduct', 'Verkoophistorie',
		'EAN=' . $EAN) . "<br /><br/>";


	/* Boekennieuws */
	echo "<span class=\"header3\">Nieuws</span><br/>\n\n" ;

	if(count($nieuws) == 0) {
		echo '<em>geen nieuws</em><br/>';
	} else {
		echo "<table class=\"bw_datatable_medium\" width=\"100%\">\n";

		foreach($nieuws as $nieuw) {
			echo "<tr>".
			"<td valign='top' style=\"border-right: 1px dashed lightgray\">".
			"<strong>".
			print_date($nieuw['datum']).
			"</strong>".
			"</td>".

			"<td>".
			replaceISBNs($nieuw['nieuws']).
			"</td></tr>\n";

		}
		echo "</table>\n\n";
	}
	if (hasAuth('boekcom')) {
		echo makeBookRef('insnieuws', 'nieuws invoeren', 'action=go&EAN='.$EAN);
	}
	echo "<br/><br/><br/>";

	// Aantallen (faseinfo)
	extract($fase);

	$fase_12 = $fase_1 + $fase_2;
	$fase_1234 = $sb_123 + $fase_4;
	$fase_34g = $vr_34 + $fase_g;
	$fase_34eg = $vr_34 + $fase_e + $fase_g;

	// fase 4 splitsen in 4a (daadwerkelijk voorraadig) en 4b
	// (voorraadprobleem)
	$fase_4a = min($fase_4, $vr_34);
	$fase_4b = $fase_4 - $fase_4a;
	// Vrij voorraad: voorraad minus 4a, = 3 + 4b
	$fase_34b = $vr_34 - $fase_4a;

	/*
		Zowel vrij, niet-geleverd en onrechtbeloofd zijn niet-negatief.

		Nog nodig
			= niet-geleverd(123) + onrechtbeloofd(4b) - vrij(34b)
			= 12
			= te bestellen(1) + in bestelling(2)

		Als vrij positief is, is nietgeleverd en onrechtbeloofd allebei 0,
		anders was er wel doorgeschoven.

		Dus, hetzij vrij is positief, en is het omgekeerde van 'nog nodig'
		(nognodig<0), hetzij nietgeleverd en onrechtbeloofd kunnen positief
		zijn, en vrij is 0 (nognodig >=0).
	*/
	// check bovenstaande
	if ($fase_34b > 0 && ($sb_123 != 0 || $fase_4b != 0)) {
		if (PT4_ENABLED){
			user_error('Interne fout: Er zijn boeken die doorgeschoven (hadden) '
				.'kunnen worden, maar dit is niet gebeurd!',
				E_USER_ERROR);
		} else {
			// else: ProcessTo4 is uit, geen wonder dat er niets is doorgeschoven
			$logger->warn("Let op! ProcessTo4 is uitgeschakeld!");
		}
	}
	if ($fase_34b < 0 || $sb_123 < 0 || $fase_4b < 0) {
		user_error('Interne fout: $fase_34b < 0 || $sb_123 < 0 || $fase_4b < 0',
			E_USER_ERROR);
	}

	$kleurlinks = $kleurrechts = '';
	if ($fase_12<0) {
		// er zijn boeken vrij
		$kleurrechts = ' bgcolor=red';
	}
	if ($fase_12>0) {
		// er zijn nog boeken nodig
		$kleurlinks = ' bgcolor=red';
	}

	echo  <<<HEREDOC

	<span class="header3">Aantallen</span>
	<table width='100%' class='bw_datatable_medium'>
		<tr><td valign='top' align="center" width="33%">
			<table>
				<tr><th colspan='2'>Studenten</th></tr>
				<tr><td>In bestelling door student:</td>
					<td align='right'>$fase_1234</td></tr>
				<tr><td>&nbsp;&nbsp;Nog niet geleverd:</td>
					<td align='right'$kleurlinks>$sb_123</td></tr>
				<tr><td>&nbsp;&nbsp;Beloofd:</td>
					<td align='right'>$fase_4</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Daadwerkelijk op voorraad:</td>
					<td align='right' bgcolor=green>$fase_4a</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;Niet meer op voorraad:</td>
					<td align='right'$kleurlinks>$fase_4b</td></tr>
			</table>
		</td><td valign='top' align="center" width="33%">

			<table>
				<tr><th colspan='4'>Voorraad</th></tr>
				<tr><td colspan=3>Totale voorraad:</td>
					<td align='right'>$fase_34eg</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td colspan=2>Buiten verkoop:</td>
					<td align='right'>$fase_g</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td colspan=2>EJBV-voorraad:</td>
					<td align='right'>$fase_e</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td colspan=2>Reguliere voorraad:</td>
					<td align='right'>$vr_34</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
					<td bgcolor=green>Beloofd:</td>
					<td align='right'>$fase_4a</td></tr>
				<tr><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>Vrij:</td>
					<td align='right'$kleurrechts>$fase_34b</td></tr>
			</table>
		</td><td valign='top' align="center" width="33%">



		<table>
				<tr><th colspan='3'>Leveranciers</th></tr>
HEREDOC;

	$nfase_1 = -$fase_1;
	$nfase_12 = -$fase_12;

	if ($fase_12>=0) {
		// er zijn nog boeken nodig (of iig niet over)
		if ($fase_1>=0) {
			// er zijn er niet teveel besteld
			echo <<<HEREDOC
				<tr><td colspan='2' bgcolor=red>Nog te leveren:</td>
					<td align='right'>$fase_12</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;</td><td>In bestelling bij lev.:</td>
					<td align='right'>$fase_2</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;</td><td>Nog te bestellen:</td>
					<td align='right'>$fase_1</td></tr>
HEREDOC;
		} else {
			// er zijn er teveel besteld
			echo <<<HEREDOC
				<tr><td colspan='2'>In bestelling bij lev.:</td>
					<td align='right'>$fase_2</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;</td><td bgcolor=red>Nog te leveren:</td>
					<td align='right'>$fase_12</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;</td><td>Kunnen geannuleerd worden:</td>
					<td align='right'>$nfase_1</td></tr>
HEREDOC;
		}
	} else {
		// er zijn boeken over
		echo <<<HEREDOC
			<tr><td colspan='2'>Boekenoverschot:</td>
				<td align='right'>$nfase_1</td></tr>
			<tr><td>&nbsp;&nbsp;&nbsp;</td><td bgcolor=red>Vrij:</td>
				<td align='right'>$nfase_12</td></tr>
			<tr><td>&nbsp;&nbsp;&nbsp;</td><td>In bestelling bij lev.:</td>
				<td align='right'>$fase_2</td></tr>
			<tr><td colspan=3><small>Retourneren danwel annuleren dus</small></td></tr>
HEREDOC;
	}

	if ($vr_e > 0){
		echo <<<HEREDOC
			<tr><td colspan=3><small>Let op! Er is nog EJBV-voorraad!</small></td></tr>
HEREDOC;
	}

	if ($fase_g) {
		echo "<tr><td colspan=3><small>Let ook op buiten verkoop!</small></td></tr>";
	}

	echo <<<HEREDOC
			</table>
		</td><tr>
	</table>

HEREDOC;

	if(hasAuth('boekcom')) {
		echo makeCondBookRef('annuleerSBs', 'Openstaande studentbestellingen annuleren',
			$sb_123<1, false, 'EAN='.$EAN) . ", ";
		echo makeCondBookRef("retour", "Retour invoeren",
			$fase_g + $fase_e + $fase_34b < 1, false, "EAN=$EAN") . ", ";
		echo makeBookRef("levering", "Levering invoeren", "EAN=$EAN");
	}

	echo

		// prijsinformatie

		"<br/><br/><br/>" .
		"<span class=\"header3\">Prijsinfo</span>\n\n".
		"<table class=\"bw_datatable_medium\" width=\"100%\">\n". // Tabel om offerteprijzen en grafiek te scheiden
		"<tr><td valign=\"top\" style=\"border-right: 1px solid lightgrey\" align=\"center\">" .
		"<table class=\"bw_datatable_medium\" style=\"border: none\">\n\n".
		"<tr><th colspan=\"4\">Offertes</th></tr>\n".
		"<tr><th>datum</th><th>verloop</th><th>lev.prijs</th><th>stud.prijs</th></tr>\n";

		foreach($offertes as $off) {
			// indien offerte geldig is, geef deze weer in bold
			if(strtotime($off['verloopdatum']) > time() &&
				strtotime($off['offertedatum']) <= time() ) {
				$b1 = '<em>'; $b2 = '</em>';
			} else {
				$b1 = $b2 = '';
			}
			echo "<tr><td>$b1".print_date($off['offertedatum']).
				"$b2</td><td>$b1".print_date($off['verloopdatum']).
				"$b2</td><td align=\"right\">$b1".Money::addPrice($off['levprijs']).
				"$b2</td><td align=\"right\">$b1".Money::addPrice($off['studprijs']).
				"$b2</td></tr>\n\n";
		}
		if (sizeof($offertes) == 0){
			echo "<tr><td colspan=\"4\"><em>Er zijn geen offertes bekend
			voor dit artikel</td></tr>";
		}
		echo "<tr><td colspan=\"4\" align=\"center\">".
			makeBookRef("offerte", "(offerte invoeren)",
			"data=$EAN&action=go") .
			"</td></tr>";

		echo "</table>\n\n";
		echo "</td><td align=\"center\" valign=\"center\">";
		if (sizeof($offertes) > 1){
			// Grafiek tonen
			echo "<img
			src=\"/Onderwijs/Boeken/statsimages.php?imagetype=artikelenoffertevergelijking&eans=$EAN\"
			alt=\"Offertegeschiedenis\">";
		} else {
			// Geen grafiek
			echo "<em>Er is te weinig informatie beschikbaar voor een
			grafiek met offertegeschiedenis</em>";
		}
		echo "</td></tr></table></p>" .

		// geef aantal boeken dat nu te koop is via PapierMolen
		"<a href=\"Papiermolen/view.html?EAN=".$EAN."\">Papiermolen</a>: ".
		$papiermolen." exemplaren<br/>\n\n";
		if ($isbn = ean_to_isbn($EAN)) {
			// link om de prijs te vergelijken via vergelijk.nl
			echo makeExternLink(makeVergelijkLink($isbn),
			"Prijsvergelijking") . "\n\n";
		}
		echo "<br/><br/><br/>";
		// bestellingen bij leverancier

		echo "<span class=\"header3\">Bestellingen</span><br/>";

		if(count($bestellingen) == 0) {
			echo "<em>geen bestellingen</em>";
		} else {
			echo "<table class=\"bw_datatable_medium\">\n".
				"<tr><th>datum</th><th>aantal</th><th>nog
				leveren</th><th>&nbsp;</th></tr>\n".
				"<tr><th colspan=\"4\">".
					(hasAuth('boekcom') ?
						makeBookRef('televeren', 'lopende bestellingen') :
						'lopende bestellingen') ."</th></tr>\n";
				$rowsPrinted = 0;
				foreach($bestellingen as $bestel) {
					if($bestel['aantal'] > $bestel['aantalgeleverd']) {
						echo '<tr><td>'.
							(hasAuth('boekcom') ?
								makeBookRef('showbestelling',
									print_date($bestel['datum']),
								'datum='.$bestel['datum'].'&leveranciernr='.
								$bestel['leveranciernr']) :
								print_date($bestel['datum']) ) .
							'</td><td align="right">'.
							$bestel['aantal'].
							'</td><td align="right">'.
							($bestel['aantal'] - $bestel['aantalgeleverd']).
							'</td><td>'.
							(hasAuth('boekcom') ?
								makeBookRef('alterbestelling',
								'(wijzig)',
								'action=alter&bestelnr='.$bestel['levbestelnr'])
								: '' ).
							"</td></tr>\n";
						$rowsPrinted++;
					}
				}
				if ($rowsPrinted == 0){
					echo "<tr><td colspan=\"3\" align=\"center\">(geen
					lopende bestellingen)</td></tr>";
				}

				echo "<tr><th colspan=\"4\">oude bestellingen</th></tr>\n";
				foreach($bestellingen as $bestel) {
					if($bestel['aantal'] <= $bestel['aantalgeleverd']) {
						echo '<tr><td>'.
							(hasAuth('boekcom') ?
								makeBookRef('showbestelling',
									print_date($bestel['datum']),
									'datum='.$bestel['datum'].'&leveranciernr='.
									$bestel['leveranciernr']) : print_date($bestel['datum'])).
							'</td><td align="right">'.
							$bestel['aantal'].
							'</td><td align="right">'.
							($bestel['aantal'] - $bestel['aantalgeleverd']).
							'</td><td>&nbsp;'.
							"</td></tr>\n";
					}
				}

				echo "</table>\n\n";
		}

		echo "<br/><br/><br/>";
		echo "<span class=\"header3\">Voorraad</span>\n\n";

		echo "<table class=\"bw_datatable_medium\" style=\"border: 1px
		solid lightgray\">\n<tr><th>&nbsp;</th>";

		$voorr = sqlGetEANVoorraad($EAN);

		$bonnenstr = '';
		foreach($voorr as $vrow) {
			if(!isset($LEVS[$vrow['leveranciernr']]))
				$LEVS[$vrow['leveranciernr']] = sqlLeverancierData($vrow['leveranciernr']);

			echo "<th title=\"".$vrow['voorraadnr']."\">". $LEVS[$vrow['leveranciernr']]['naam']."<br>\n".
				Money::addPrice($vrow['levprijs'])."</th>";
			if ($vrow['voorraad'] > 0) {
				$nr = $vrow['voorraadnr'];
				$bonnenstr .= "&voorraadnrs[]=$nr&aantal[$nr]=$vrow[voorraad]";
			}
		}
		echo "<th>totaal</th></tr>\n";

		global $BOEKENPLAATSEN;
		foreach($BOEKENPLAATSEN as $plaats => $lang) {
			echo "<tr><td>".$lang."</td>";
			$totaal = 0;
			foreach($voorr as $vrow) {
				echo "<td align=\"center\">".$vrow[$plaats].
					"</td>";
					$totaal += $vrow[$plaats];
			}
			echo "<td align=\"center\"><strong>".$totaal."</strong></td>";
			echo "</tr>\n";
		}
		echo "</table>".
			makeBookRef('voorraadmutaties', 'Voorraadhistorie', 'EAN='.$EAN);

		if(hasAuth('boekcom')) {
			echo ", ".
				makeBookRef('verplaatsing', 'Verplaatsen', 'EAN='.$EAN);
			if (strlen($bonnenstr) > 0) {
				echo ", ".
					makeRef('/Onderwijs/Boeken/index.php?page=export&action=bonnen' . $bonnenstr, 'Bonnen van deze voorraad');
			}
		}
		echo "<br/><br/><br/>\n";

		// Verkoopgeschiedenis
		echo "<span class=\"header3\">Verkoopgeschiedenis</span><br/>\n\n";
		$geschiedenis = sqlGetVerkopenPerProduct($EAN, true, true);

		if (sizeof($geschiedenis) > 0)
		{
			global $BWDB;

			$titel = $BWDB->q("VALUE
				SELECT titel
				FROM boeken
				WHERE EAN=%s", $EAN);

			$titel = str_cut($titel, 25);

			$verkopen = BookwebStatistieken::getVerkopen(array($EAN));
			$verkopenCuml = array();
			$alleDatums = array();

			$verkoopinfo = $verkopen[$EAN];
			$lastAantal = 0;

			foreach ($verkoopinfo as $datum => $aantal)
			{
				$lastAantal = $lastAantal + $aantal;
				$verkopenCuml["$datum"] = $lastAantal;
				$alleDatums[] = $datum;
			}

			$alleDatums = array_unique($alleDatums);

			if (sizeof($alleDatums) == 1)
			{
				$alleDatums[] = "0000-01-01";
			}

			sort($alleDatums);

			$laatsteWaarde = 0;

			foreach ($alleDatums as $datum)
			{
				if (isset($verkopenCuml["$datum"]))
				{
					$laatsteWaarde = $verkopenCuml["$datum"];
				}

				$graphData[] = $laatsteWaarde;
			}

			// Grafiekje tonen
			echo '<script src="'.Page::minifiedFile('Chart.js', 'js').'" type="text/javascript"></script>';
			echo maakGrafiek($alleDatums, array($titel => $graphData));
		}
		else
		{
			echo "<em>Er zijn in het verleden nog geen verkopen geweest van
			deze titel</em>";
		}

		echo "<br/><br/><br/>";
}


function voorraadmutaties()
{

	$EAN = requireEAN();
	$levermomenten = sqlGetLeverMomenten($EAN);
	$begin = tryPar('begin');
	$eind  = tryPar('eind');

	$boekdata = sqlBoekGegevens($EAN);
	/* Voorraadmutaties */
	echo "<h2>Voorraadmutaties ".print_EAN($EAN)."</h2>\n"
		."<h3>".$boekdata['auteur'].": ".$boekdata['titel']
		.(@$boekdata['druk'] ? " (".$boekdata['druk'].")" : '')
		."</h3>\n\n";

	echo 'Interval:';
	startForm(NULL, NULL, NULL, 'GET');
	echo addHidden('EAN', $EAN);
	inputText('Begindatum', 'begin', $begin, '(JJJJ-MM-DD)', 10);
	inputText('Einddatum',  'eind',  $eind,  '(JJJJ-MM-DD)', 10);
	endForm('Engage', FALSE);

	$EANvoorraadnrs = unzip(sqlGetEANVoorraad($EAN), 'voorraadnr');
	$leverancierdata = $leveranciersvoorraadnrs = array();
	$head_aantprijs = '';
	foreach ($EANvoorraadnrs as $voorraadnr) {
		$voorraaddata  = sqlGetVoorraadInfo($voorraadnr);
		$leveranciernr = $voorraaddata['leveranciernr'];
		if (!isset($leverancierdata[$leveranciernr])) {
			$leverancierdata[$leveranciernr] =
				sqlLeverancierData($leveranciernr);
			$head_voorraadnrs[$leveranciernr] = '';
		}

		$leveranciersvoorraadnrs[$leveranciernr][] =  $voorraadnr;
		$head_voorraadnrs[$leveranciernr] .= '<th colspan=4 title='.$voorraadnr.'>'
			.Money::addPrice($voorraaddata['levprijs']).'</th>';
		$head_aantprijs   .= '<th>mut.</th><th>prijs</th><th>btwtarief</th><th>btw</th>';

		$voorraadnr_aantal[$voorraadnr] = 0;
	}

	echo "<table border=1>\n\n";
	echo "<tr><th rowspan=3>Moment</th>";
	foreach ($leveranciersvoorraadnrs as $leveranciersnr => $voorraadnrs) {
		echo '<th colspan='.(4 * sizeOf($voorraadnrs))
			.'>'.$leverancierdata[$leveranciersnr]['naam'].'</th>';
	}
	echo "</tr>\n<tr>";
	foreach ($leveranciersvoorraadnrs as $leveranciersnr => $voorraadnrs) {
		echo $head_voorraadnrs[$leveranciersnr];
	}
	echo "</tr>\n";
	echo "<tr>$head_aantprijs</tr>\n";

	/* Zorg dat er oneindig ver terug begonnen wordt met events zoeken en dat
	 * er tot oneindig ver vooruit gezocht wordt. */
	$vorigetelling = $begin;
	$tellingen = sqlGetTellingenByEAN($EAN, $begin, $eind);
	$tellingen['eind'] = array('wanneer' => $eind);

	/* $soortaantal: houdt per moment per voorraadnr de mutaties bij
	 * $totaal_mutaties: houdt gaandeweg het totaal van de mutaties bij */
	$totaal_mutaties =
		array('levering' => 0, 'retour' => 0, 'verkoop' => 0, 'terugkoop' => 0);
	$soortaantal = array();

	/* Toon een rij voor 'eerder' */
	if (isset($begin)) {
		echo '<tr><th>Eerder</th>';
		$mutaties_eerder = finBookEvents(NULL, $begin, $EAN);
		foreach ($leveranciersvoorraadnrs as $voorraadnrs) {
			foreach ($voorraadnrs as $voorraadnr) {
				if (isset($mutaties_eerder[$voorraadnr])) {
					echo '<td align=center>'
						.finShowMutaties($mutaties_eerder[$voorraadnr])
						.'</td><td align=center>~</td>'
						.'<td align=center>~</td>'
						.'<td align=center>~</td>';
					foreach (array('levering', 'retour') as $soort) {
						@$voorraadsaldo[$voorraadnr][$soort] +=
							$mutaties_eerder[$voorraadnr][$soort];
						$totaal_mutaties[$soort] +=
							$mutaties_eerder[$voorraadnr][$soort];
					}
					foreach (array('verkoop', 'terugkoop') as $soort) {
						@$voorraadsaldo[$voorraadnr][$soort] +=
							array_sum($mutaties_eerder[$voorraadnr][$soort]);
						$totaal_mutaties[$soort] +=
							array_sum($mutaties_eerder[$voorraadnr][$soort]);
					}
				} else {
					echo '<td></td><td></td><td></td><td></td>';
				}
			}
		}
		echo "</tr>\n";
	}

	/* Haal alle events binnen tussen twee tellingen */
	foreach ($tellingen as $tellingnr => $telling) {
		$events[$tellingnr] = finBookEvents($vorigetelling,
			$telling['wanneer'], $EAN, TRUE);

		if (sizeOf($events[$tellingnr]) > 0) {
			if (sizeOf($events[$tellingnr]) > 0) {
			/* Er zijn events voor deze telling */
				/* Loop alle momenten af */
				foreach($events[$tellingnr] as $moment => $voorraaddata)
				{
					foreach ($voorraaddata as $voorraadnr => $soortdata) {
						/* Loop alle mutatiesoorten van dit moment af */
						foreach ($soortdata as $soort => $prijsdata) {
							foreach ($prijsdata as $prijs => $aantal) {
								if (!isset($soortaantal[$voorraadnr][$soort])) {
									$soortaantal[$voorraadnr][$soort] = 0;
								}
								$soortaantal[$voorraadnr][$soort] += $aantal;
								$totaal_mutaties[$soort] += $aantal;
								@$voorraadsaldo[$voorraadnr][$soort] +=
									$aantal;
								$momentprijs[$voorraadnr] = $prijs;
							}
						}
					}

					if ($soortaantal === array()) {
						continue;
					}

					$isLevering = in_array($moment, $levermomenten);

					echo '<tr><td align="center">';
					if ($isLevering){
						$momentsub = substr($moment, 0, 10);

						echo makeBookRef("Showleveringen", $moment,
						"datum=$momentsub");
					} else {
						echo $moment;
					}
					echo '</td>';
					foreach ($leveranciersvoorraadnrs as $voorraadnrs) {
						foreach ($voorraadnrs as $voorraadnr) {
							if (isset($soortaantal[$voorraadnr])) {
								$btwtarief = getBtwtarief($voorraadnr);
								$btw = getBtw($voorraadnr);
								echo '<td align="center">'
									.finShowMutaties($soortaantal[$voorraadnr])
									.'</td><td align="right">'
									.Money::addPrice($momentprijs[$voorraadnr]).'</td>'
									.'<td align="right">'.$btwtarief.'%</td>'
									.'<td align="right">'.Money::addPrice($btw).'</td>';
							} else {
								echo '<td></td><td></td><td></td><td></td>';
							}
						}
					}
					echo "</tr>\n";

					$soortaantal = array();
				}
			}
		}

		/* De telling */
		if ($tellingnr == 'eind') {
			break;
		}
		$totaal_telling = $telling['aantal_voorraad'] +
			$telling['aantal_buitenvk'];
		echo '<tr title="Telling door '.PersoonView::naam(Lid::geef($telling['lidnr']))
			.(isset($telling['opmerkingen'])?': '
			.htmlspecialchars($telling['opmerkingen']):'').'"><td>{'
			.$telling['wanneer'].'}</td>';
		$fout = finCalcMutaties($totaal_mutaties) != $totaal_telling?
			finCalcMutaties($totaal_mutaties) - $totaal_telling:'';
		$fout = $fout > 0?"+$fout":$fout;
		echo '<td colspan='.(2*sizeOf($EANvoorraadnrs)) ."
			align=center><span class=text-danger strongtext>$fout</span>{"
			.(isset($telling['aantal_buitenvk']) &&
			$telling['aantal_buitenvk'] != 0
			?"$telling[aantal_voorraad] + $telling[aantal_buitenvk]"
			:$totaal_telling).'}</td>';
		echo "</tr>\n";

		$vorigetelling = $telling['wanneer'];
	}

	/* Toon een rij voor 'later' */
	if (isset($eind)) {
		echo '<tr><th>Later</th>';
		$mutaties_later = finBookEvents($eind, NULL, $EAN);
		foreach ($leveranciersvoorraadnrs as $voorraadnrs) {
			foreach ($voorraadnrs as $voorraadnr) {
				if (isset($mutaties_later[$voorraadnr])) {
					echo '<td align=center>'
						.finShowMutaties($mutaties_later[$voorraadnr])
						.'</td><td align=center>~</td>'
						.'<td align=center>~</td>'
						.'<td align=center>~</td>';
					foreach (array('levering', 'retour') as $soort) {
						@$voorraadsaldo[$voorraadnr][$soort] +=
							$mutaties_later[$voorraadnr][$soort];
					}
					foreach (array('verkoop', 'terugkoop') as $soort) {
						@$voorraadsaldo[$voorraadnr][$soort] +=
							array_sum($mutaties_later[$voorraadnr][$soort]);
					}
				} else {
					echo '<td></td><td></td><td></td><td></td>';
				}
			}
		}
		echo "</tr>\n";
	}
	echo '<tr><th>Huidige voorraad:</th>';
	foreach ($leveranciersvoorraadnrs as $voorraadnrs) {
		foreach ($voorraadnrs as $voorraadnr) {
			echo '<th colspan=4>'.finShowMutaties($voorraadsaldo[$voorraadnr]).' = '
				.finCalcMutaties($voorraadsaldo[$voorraadnr]).'</th>';
		}
	}
	echo "</tr>\n</table>";

	echo "<p>" . makeBookRef('boekinfo', 'BoekInfo', 'EAN='.$EAN) ."</p>";
}


/* Kijkt of er boeken in fase 1-2-3 zijn die naar fase 4 mogen. Oftewel: Zijn
 * er boeken die aan student(en) toegewezen kunnen worden? Zo ja, wijs ze dan
 * toe en mail de betreffende student(en)
 */
function processTo4($EANs=null, $cron=FALSE)
{
	if (!PT4_ENABLED){
		if ($cron) echo "ProcessTo4 uitgeschakeld in bookweb/constants.php";
		return;
	}

	global $BWDB;
	global $logger;

	$vandaag = strftime('%Y-%m-%d');

	$infos = sqlAlleFasenInfo($EANs);
	$logger->info("processTo4 op infos: " . print_r($infos, true));

	$offertes = sqlOffertePrijzen(array_keys($infos));

	foreach ($infos as $EAN => $info) {
		// Berekenen hoeveel boeken er toegewezen kunnen worden aan een student,
		// dat is dus het aantal in de vrije verkoop in fase 3 of 4 minus het aantal
		// dat toegewezen is aan een studentbestelling (en dus in fase 4 zit).
		//
		// Als dat getal positief is, dan zijn er dus boeken beschikbaar om toegewezen
		// te worden en moet er gezocht worden naar een geschikte studentbestelling
		$aantal_3 = $info['vr_34'] - $info['sb_4'];

		if ($aantal_3 <= 0) continue; // geen vrije boeken...

		$boekinfo = $infos[$EAN];

		$res = $BWDB->q('TABLE SELECT * FROM studentbestelling
			WHERE (vervaldatum IS NULL OR TO_DAYS(vervaldatum) > TO_DAYS(now()))
				AND gekochtdatum IS NULL
				AND gemailddatum IS NULL
				AND EAN=%s
			ORDER BY prioriteit DESC, bestelddatum ASC
			LIMIT %i', $EAN, $aantal_3);

		if ($cron && sizeof($res) > 0) {
			$cronmail .= "\n$EAN: ".sizeof($res)." ($aantal_3)";
		}
		foreach ($res as $tuple) {
			$offdata = $offertes[$EAN];
			bwmail($tuple['lidnr'], BWMAIL_BOEKBINNEN, array
				( 'titel' => $boekinfo['boek_titel']
				, 'auteur' => $boekinfo['boek_auteur']
				, 'druk' => @$boekinfo['boek_druk']
				, 'EAN' => $EAN
				, 'prijs' => ($offdata ? number_format(ceil($offdata), 0, ',','.') : '')
				, 'verlooptop' => sb_calcVervaldatum($vandaag, '%e %B %Y')
				));
			$BWDB->q("UPDATE `studentbestelling` SET %S WHERE `bestlnr` = %s"
				, array('gemailddatum' => $vandaag
					,'vervalreden' => 'Verlopen'
					,'vervaldatum' => sb_calcVervaldatum($vandaag))
				, $tuple['bestelnr']);

			// TODO: koppel NU aan een levering
		}
	}

	if ($cron && isset($cronmail)) {
		$cronexplain = <<<CRON
Let op: Er zijn boeken die zich in fase 3 bevonden door CRON naar fase 4
gestuurd. Het feit dat dit gebeurd is, is hoogstwaarschijnlijk een fout in
Bookweb.

Hieronder staat per EAN hoeveel exemplaren er in fase 3 waren en zijn veranderd
naar fase 4 (tussen haakjes het totaal aantal in fase 3 van dat EAN)

CRON;

		sendmail('"Cron" <www-data@a-eskwadraat.nl>',
			'"WebCie" <www@a-eskwadraat.nl>',
			'ProcessTo4 door CRON aangeroepen!', $cronexplain.$cronmail);
	}
}


// hiermee kun je alle openstaande student- en leveranciersbestellingen
// van 1 ISBN omzetten naar een ander ISBN. Dit is te gebruiken als van
// een bepaald boek een nieuwere editie geleverd wordt.          (kink)

function updatebestellingen() {

	if(go()) {
		global $BWDB;

		$updlev = (bool)tryPar('updlev');
		$updstu = (bool)tryPar('updstu');
		$EANo = requireEAN('EANoud');
		$EANn = requireEAN('EANnieuw');

		if($updstu) {
			echo "Bijwerken openstaande studentbestellingen... ";
			flush();
			$num = $BWDB->q('RETURNAFFECTED UPDATE studentbestelling SET EAN = %s WHERE
				EAN = %s AND
				gemailddatum IS NULL AND gekochtdatum IS NULL AND
				(vervaldatum IS NULL OR vervaldatum > NOW())',
				$EANn, $EANo);
			echo $num. " updated<br>\n\n";
		}
		if ($updlev) {
			echo "Bijwerken openstaande leveranciersbestellingen... ";
			flush();
			// pak alle openstaande bestellingen
			$open = $BWDB->q('TABLE SELECT * FROM bestelling WHERE
				EAN = %s AND
				aantalgeleverd < aantal AND aantal != 0',
				$EANo);

			$num = 0;
			foreach($open as $oldbest) {
				// voor elk, zet de 'oude' row op 'compleet geleverd' (aantal:=aantalgeleverd)
				// en voeg een nieuwe row toe voor de rest van de nog niet geleverde boeken.
				// deze UPDATE+INSERT moet eigenlijk in een transaction gewrapt.
				$num++;
				$BWDB->q('UPDATE bestelling SET aantal = aantalgeleverd
					WHERE EAN = %s', $EANo);

				$newbest = array();
				$newbest['aantal'] = $oldbest['aantal'] - $oldbest['aantalgeleverd'];
				$newbest['EAN'] = $EANn;
				$newbest['aantalgeleverd'] = 0;
				// rest blijft hetzelfde
				$newbest['datum'] = $oldbest['datum'];
				$newbest['leveranciernr'] = $oldbest['leveranciernr'];

				$BWDB->q("INSERT INTO `bestelling` SET %S", $newbest);
			}
			echo $num. " updated<br>\n\n";
		}

		processTo4(array($EANo,$EANn));

		echo "<p>Zo.</p>".
			makeBookRef('updatebestellingen',
				'Nog meer bestellingen van ISBN veranderen');

		return;
	}


	if(tryPar('action') == 'check') {

		$updlev = (bool)tryPar('updlev');
		$updstu = (bool)tryPar('updstu');

		if(!$updlev && !$updstu) {
			print_error('Argh, wat voor zin heeft deze exercitie als je helemaal niks wilt updaten?!?!');
		}

		$EANo = requireEAN('EANoud');
		$EANn = requireEAN('EANnieuw');

		if($EANo == $EANn) {
			print_error('HUH?!? Het huidige en de nieuwe ISBN zijn hetzelfde...');
		}

		$boekinfo = sqlBoekenGegevens(array($EANo, $EANn));
		$fase = sqlAlleFasenInfo(array($EANo));

		echo "<p>Je wilt dus het volgende ISBN omzetten:</p>";

		simpleForm('go');
		echo addHidden('EANoud', $EANo).
			addHidden('EANnieuw', $EANn).
			addHidden('updlev', $updlev).
			addHidden('updstu', $updstu);

		echo "<tr><td>Huidig ISBN:</td><td><b>".
				print_EAN_bi($boekinfo[$EANo]['EAN']).
				"</b> - ".$boekinfo[$EANo]['auteur'].': '.$boekinfo[$EANo]['titel'].
					' ('.@$boekinfo[$EANo]['druk'].')'.
				"</td></tr>
			<tr><td>Nieuw ISBN:</td><td><b>".
				print_EAN_bi($boekinfo[$EANn]['EAN']).
				"</b> - ".$boekinfo[$EANn]['auteur'].': '.$boekinfo[$EANn]['titel'].
					' ('.@$boekinfo[$EANn]['druk'].')'.
				"</td></tr>
			<tr><td valign=\"top\">Update:</td><td>";
		if($updlev) {
			echo '- Leveranciersbestellingen ('. $fase[$EANo]['fase_2'] ." stuks)<br>\n";
		}
		if($updstu) {
			echo '- Studentbestellingen ('. $fase[$EANo]['sb_123'] ." stuks)\n";
		}
		echo "</td></tr>\n";

		endForm('jajajaja doe het, NU!!! (ook bekend als de OK-knop)', FALSE);
		return;
	}

	echo "<p>Met deze functie kun je alle openstaande student- en leveranciersbestellingen
		van een ISBN omzetten naar een ander ISBN. Dit is te gebruiken als van
		een bepaald boek een nieuwere editie geleverd wordt.</p>";

	simpleForm('check');

	inputText('Huidig ISBN', 'EANoud');
	inputText('Nieuw ISBN', 'EANnieuw');

	echo "<tr><td valign=\"top\">Wijzig:</td><td>\n".
		 	addCheckBox('updlev', TRUE, '1')." <label for=\"updlev\">Leveranciersbestellingen</label><br>\n".
			addCheckBox('updstu', TRUE, '1')." <label for=\"updstu\">Studentbestellingen</label></td></tr>";

	endForm('check it');

}

/**
 * Nieuwe leverancier toevoegen of bestaande bewerken
 */
function updateleverancier() {
	global $BWDB;

	switch (tryPar('action')) {
		case 'new':

			$vals = array (
				'naam' => requirePar('naam'),
				'adres' => tryPar('adres'),
				'contactpersoon' => tryPar('contactpersoon'),
				'telefoon' => tryPar('telefoon'),
				'email' => tryPar('email'),
				'boekenmarge' => tryPar('boekenmarge'),
				'rubriek' => tryPar('rubriek')
			);

			$BWDB->q('INSERT INTO leverancier SET %S', $vals);

			printHTML('Nieuwe leverancier opgeslagen.');
			showleveranciers();
			break;
		case 'update':
			$vals = array (
				'naam' => requirePar('naam'),
				'adres' => tryPar('adres'),
				'contactpersoon' => tryPar('contactpersoon'),
				'telefoon' => tryPar('telefoon'),
				'email' => tryPar('email'),
				'boekenmarge' => tryPar('boekenmarge'),
				'rubriek' => tryPar("rubriek")
			);

			$BWDB->q('UPDATE leverancier SET %S WHERE leveranciernr = %i',
				$vals, requirePar('leveranciernr'));

			printHTML('Wijzigingen zijn opgeslagen.');
			showleveranciers();
			break;
		default:
			global $RUBRIEKEN;
			$id = tryPar('leveranciernr');
			startForm('leverancier', null, null, null, $id ? 'update' : 'new');
			if($id) {
				$row = $BWDB->q('TUPLE SELECT * FROM leverancier WHERE leveranciernr = %i', $id);
				echo addHidden('leveranciernr', $id);
			}
			inputText('Naam','naam',@$row['naam'], null, 40);
			inputText('Adres','adres',@$row['adres'],null,60);
			inputText('Contactpersoon', 'contactpersoon', @$row['contactpersoon'], null, 40);
			inputText('Telefoon', 'telefoon', @$row['telefoon'], null, 40);
			inputText('E-mail', 'email', @$row['email'], null, 40);
			inputText('Boekenmarge', 'boekenmarge',
				(isset($row['boekenmarge']) ? $row['boekenmarge'] : '0.00'), '(0.01 = 1%)', 7
			);
			echo "<tr><td>Rubriek:</td><td>" .
				addSelect("rubriek", $RUBRIEKEN, @$row["rubriek"]) .
				"</td></tr>";
			endForm('opslaan', FALSE);
	}

}

/**
 * Laat alle leveranciers uit de DB zien.
 */
function showleveranciers()
{
	echo "<h2>Leveranciers</h2>\n\n";

	global $BWDB;

	$res = $BWDB->q('TABLE SELECT * FROM leverancier');
	$tab = array();
	foreach($res as $row) {
		if ( $row['leveranciernr'] == BOEKLEVNR ) {
			$row['naam'] = '<b>'.$row['naam'].'</b>';
		}
		$row['naam'] = makeBookRef('leverancier', $row['naam'], 'leveranciernr='.$row['leveranciernr']);
		if ( isset($row['contactpersoon']) && $row['contactpersoon'] ) {
			$row['contactpersoon'] = "<a href=\"mailto:".urlencode($row['email'])."\">".
				htmlspecialchars($row['contactpersoon'])."</a>";
		}
		$row['boekenmarge'] = (100*$row['boekenmarge']).'%';
		$tab[] = $row;
	}
	echo arrayToHTMLTable($tab,
		array ('leveranciernr' => 'nr',
			'naam' => 'naam',
			'contactpersoon' => 'contactpersoon',
			'telefoon' => 'telefoon',
			'boekenmarge' => 'marge',
			'rubriek' => 'rubriek'),
		null,
		array ('leveranciernr' => 'right',
			'boekenmarge' => 'right' ) );

	printHTML(makeBookRef('leverancier', 'Nieuwe leverancier toevoegen'));

}

/**
 * Laat alle pakketten uit de DB zien.
 */
function toonPakketten()
{
	global $BWDB;

	echo "<h2>Pakketten</h2>\n\n";
	$res = $BWDB->q('TABLE SELECT * FROM pakket ORDER BY pakketnr');
	$tab = array();
	foreach($res as $row) {
		$row['naam'] = makeBookRef('pakket', $row['naam'], 'pakketnr='
			.$row['pakketnr']);
		$row['wis'] = makeBookRef('pakket', '<font size=-1>(wis)</font>',
			'action=wis&pakketnr='.$row['pakketnr']);
		$tab[] = $row;
	}

	if (sizeOf($tab) > 0) {
		echo arrayToHTMLTable($tab,
			array ('pakketnr' => 'Nr',
				'naam' => 'Naam',
				'wis' => ''),
			null,
			array ('pakketnr' => 'right'));
	} else {
		printHTML('Geen pakketten gevonden.');
	}

	printHTML(makeBookRef('pakket', 'Nieuw pakket toevoegen'));
}

function showspecialconstants()
{

	global $COLJAREN, $PERIODES, $VAKANTIES, $BESTELDATA, $RUBRIEKEN;

?>
<h2>Algemeen</h2>

<table border="1">
<tr><td>Boekcom:</td><td><?=BOEKCOMNAME?></td></tr>
<tr><td>Hoofdleverancier:</td><td><?=BOEKLEVNR .' - '.BOEKLEVERANCIER?></td></tr>
</table>


<h2>Data</h2>

<h3>Periodes waarvoor studenten boeken kunnen bestellen</h3>

<table border="1">
<tr><th>jaar</th><th colspan="2">begin</th></tr>
<?php
foreach($PERIODES as $jaar => $perlist) {
	echo "<tr><td>".$COLJAREN[$jaar]."</td><td colspan=\"2\">&nbsp;</td></tr>";
	foreach($perlist as $nr => $begin) {
		echo "<tr><td>&nbsp;</td><td>".$nr .".</td><td>".
			print_date($begin)."</td></tr>\n";
	}
}
?>
</table>

<h3>Vakanties</h3>

<table border="1">
<tr><th>Begin</th><th>Eind</th></tr>
<?php
foreach ($VAKANTIES as $begin => $end) {
	echo "<tr><td>".print_date($begin)."</td><td>".
		print_date($end)."</td></tr>\n";
}?>
</table>

<p>De eerstejaarsboekverkoop is op: <?php echo print_date(EJBV); ?>.</p>

<p>Een studentbestelling verloopt na: <?php echo SB_VERLOOPDAGEN; ?> werkdagen.</p>


<h3>Besteldata voor de boekcom:</h3>

<!-- LET OP LET OP: text duplicatie van space/specialconstants.php (r119),
graag gelijktijdig updaten!! -->

<p>Besteldata van de BoekCom: array(besteldatum =&gt; leverdatum)<br>
Als er een leveranciersbestelling gedaan wordt, wordt de meest recente
besteldatum gekozen die hooguit een week (7 dagen) in de toekomst ligt.
Dus, van een bestelling enkele dagen na de besteldatum in deze array wordt
aangenomen dat 'ie toch geleverd kan worden alsof 'ie wel op tijd gedaan
is.
<p>Op dit moment, wordt ook als er nog geen bestelling is gedaan, voor de
student een projectie gemaakt alsof hij een week de besteldeadline mag
missen. Dus hooguit een week te laat bestellen, en toch is de verwachtte
leverdatum alsof de student op tijd bestelde.

<!-- LET OP LET OP: text duplicatie van space/specialconstants.php, graag
gelijktijdig updaten!! -->

<table border="1">
<tr><th>Besteldatum</th><th>Leverdatum</th></tr>
<?php
foreach($BESTELDATA as $best => $lev) {
	echo "<tr><td>".print_date($best)."</td><td>".print_date($lev)."</td></tr>\n";
} ?>
</table>

<h3>In BookWeb bekende rubrieken</h3>
<?php
echo implode(", ", $RUBRIEKEN);
?>

<?php
}

// een array met elk element een array van 3 elementen:
// vaknr/vakinfo/boekvakinfo
function docentenBoekenMailPerVak($data)
{
	$ontberekendedata = array();
	$mails = array();
	$embedded = FALSE;
	foreach ($data as $datum) {
		list($vaknr, $vakinfo, $boekvakinfo) = $datum;

		if (!isset($vakinfo)) {
			$t=sqlVakkenGegevens(array($vaknr));
			$vakinfo = array_shift($t);
			$embedded = TRUE;
		}
		if (!isset($boekvakinfo)) {
			$boekvakinfo = sqlGetVakBoeken($vaknr);
			$embedded = TRUE;
		}

		if (!$embedded && (!isset($vakinfo['email']) || !$vakinfo['email'])) {
			$ontberekendedata[] = "Bij het vak $vakinfo[naam] ($vaknr) is geen "
				."e-mailadres bekend.\n";
		}
		if (!$embedded && (!isset($vakinfo['docent']) || !$vakinfo['docent'])) {
			$ontberekendedata[] = "Bij het vak $vakinfo[naam] ($vaknr) is geen "
				."docent bekend.\n";
		}


		if ($embedded || sizeOf($ontberekendedata) == 0) {
			$mails[$vakinfo['email']][] = array
				('docent' => $vakinfo['docent']
				,'vak'    => $vakinfo['naam']
				,'afkorting' => $vakinfo['afkorting']
				,'dictaat' => $vakinfo['dictaat']
				,'boeken' => $boekvakinfo
			);
		} else if (!$embedded) {
			printHTML(implode("<br />\n", $ontberekendedata), TRUE);
		}
	}
	foreach ($mails as $email => $emaildata) {
		$vakken = array();
		foreach ($emaildata as $emaildatum) {
			$vakken[] = $emaildatum['vak'];
			// we gaan er hier van uit dat als email gelijk is, docentnaam ook
			// gelijk zal zijn
			$docent = $emaildatum['docent'];
		}
		echo "<p>Docent voor <em>".implode(', ', $vakken)."</em> ".
			"$docent &lt;$email&gt;) wordt nu gemaild... ";
		bwmail($email, BWMAIL_DOCENT_BOEKVAK_INFO, $emaildata);
		echo "ok</p>\n";
	}
}

/**
	Implementeert de functie "boekenlijst": toont dus alle boeken voor
	een gegeven periode in een mooie tabel, zodat die gecopy-pasted kan worden
	naar de leverancier
*/
function boekenLijst(){
	global $PERIODES;

	echo "<h2>Boekenlijst opvragen</h2>\n";
	echo "Selecteer een jaar en een periode om een lijst op te vragen met boeken die voor die periode gekoppeld zijn aan vakken. Let op! Een vak dat over twee periodes loopt, wordt slechts in de eerste periode getoond!<br/><hr/>\n";
	$requestedJaar = tryPar("lijstJaar");
	if (is_numeric($requestedJaar)){
		$requestedCollegeJaar = "$requestedJaar" . "-" . ($requestedJaar + 1);
	}

	$requestedPeriode = tryPar("lijstPeriode");
	$showPlain = tryPar("lijstPlain");
	$showBrunaTabel = tryPar("lijstBruna");
	$showPrijsVergelijking = tryPar("lijstPrijsVergelijking");
	$showDictaatDocenten = tryPar("lijstDictaatDocenten");

	if ($showPlain != "") $showPlain = true;
	if ($showBrunaTabel != "") $showBrunaTabel = true;

	// Formulier neerzetten
	echo addForm(STARTPAGE, "GET");
	echo addHidden("page", "boekenlijst");

	echo "<table width=\"100%\">\n";
	echo "	<tr>\n";
	echo "		<td>Collegejaar:</td>\n";
	echo "		<td>" . addSelectCollegejaar("lijstJaar", $requestedJaar, true) . "</td>\n";
	echo "	</tr>\n";

	echo "	<tr>\n";
	echo "		<td>Periode:</td>\n";
	echo "		<td>" . addSelectPeriodes("lijstPeriode", $requestedJaar, $requestedPeriode, true) . "</td>\n";
	echo "		<td>&nbsp;</td>\n";
	echo "	</tr>\n";

	echo "	<tr><td>&nbsp;</td><td>" . addCheckBox("lijstPlain", $showPlain) .
		" <label for=\"lijstPlain\">Geen tabel, maar platte opmaak (geen vakinfo)</label></td></tr>\n";
	echo "	<tr><td>&nbsp;</td><td>" . addCheckBox("lijstBruna", $showBrunaTabel) .
		" <label for=\"lijstBruna\">Toon speciale Bruna.nl-tabel voor offerte-template</label></td></tr>\n";
	echo "	<tr><td>&nbsp;</td><td>" . addCheckBox("lijstPrijsVergelijking", $showPrijsVergelijking) .
		" <label for=\"lijstPrijsVergelijking\">Toon lijst om makkelijk prijzen te kunnen vergelijken</label></td></tr>\n";
	echo "	<tr><td>&nbsp;</td><td>" . addCheckBox("lijstDictaatDocenten", $showDictaatDocenten) .
		" <label for=\"lijstDictaatDocenten\">Toon lijst van de docenten bij de vakken die een dictaat hebben</label></td></tr>\n";


	echo "	<tr><td>&nbsp;</td><td>" . addFormEnd("Toon boekenlijst", false) . "</td></tr>\n";

	echo "</table>\n";
	echo "</form>\n";
	echo "<hr/>\n";

	echo "<br />\n";
	if($showDictaatDocenten)
	{
		$boekenLijst = sqlGetBoekenLijst($requestedJaar,$requestedPeriode);
		if ($boekenLijst === false){
			echo "Er is een fout opgetreden!";
		} else {
			echo "Hieronder volgt een lijst met alle vakken die een dictaat hebben.
			Er staat ook meteen per vak welke docent het vak geeft en het email-adres
			van de docent voor handige referentie.\n";

			echo "<table width=\"100%\" class=\"bw_datatable_medium smallfont\">\n";

			echo "	<tr>\n";
			echo "		<th>Vakcode</th>\n";
			echo "		<th>Vak</th>\n";
			echo "		<th width=\"1\">Studie(s)</th>\n";
			echo "		<th>Looptijd</th>\n";
			echo "		<th>Dictaat</th>\n";
			echo "		<th>Docent</th>\n";
			echo "		<th>Email</th>\n";
			echo "	</tr>\n";

			foreach($boekenLijst as $vaknr=>$eans){
				foreach($eans as $ean=>$datums){
					$boekinfo = sqlBoekGegevens($ean);
					$beginDatum = $datums["begindatum"];
					$eindDatum = $datums["einddatum"];

					if (is_numeric($vaknr)){
						$vakdata = sqlVakkenGegevens(array($vaknr));
						$vakdata = $vakdata[$vaknr];
					} else{
						$vakdata = null;
					}

					if(!isset($vakdata["dictaat"]) || $vakdata["dictaat"] === "N")
						continue;

					// Rij in tabel tonen
					echo "	<tr>\n";

					// VakInfo eerst printen

					// Vakcode
					echo "		<td valign=\"top\">";
					if ($vakdata["afkorting"]){
						echo makeOsirisLink($vakdata["afkorting"], $requestedCollegeJaar, $vakdata["afkorting"]);
					} else {
						echo "&nbsp;";
					}
					echo "</td>\n";

					// Vaknaam (met link)
					echo "		<td valign=\"top\">";
					echo makeBookRef('vakinfo', htmlspecialchars($vakdata['naam']), 'vaknr='.$vaknr);
					echo "</td>\n";

					// Studies
					echo "		<td valign=\"top\">";
					$vakstudies = sqlGetVakStudies($vaknr);
					foreach ($vakstudies as $studie=>$jaren){
						echo "$studie&nbsp;";
						echo implode(",&nbsp;", $jaren);
						echo "<br/>";
					}
					echo "</td>\n";

					if (!$showPrijsVergelijking){
						// Looptijd
						echo "		<td valign=\"top\">\n";
						echo "			$beginDatum tot $eindDatum\n";
						echo "		</td>\n";
					}

					echo "<td valgin=\"top\">\n";
					echo "$vakdata[dictaat]\n";
					echo "</td>";

					echo "<td valgin=\"top\">\n";
					echo "$vakdata[docent]\n";
					echo "</td>";

					echo "<td valgin=\"top\">\n";
					echo "$vakdata[email]\n";
					echo "</td>";
				}
			}
		}
	}
	else if (is_numeric($requestedJaar) && is_numeric($requestedPeriode)){
		$boekenLijst = sqlGetBoekenLijst($requestedJaar,$requestedPeriode);
		if ($boekenLijst === false){
			echo "Er is een fout opgetreden!";
		} else {

			echo "Hieronder zie je een lijst met alle vakken die gegeven
			worden in periode $requestedPeriode van het collegejaar
			$requestedCollegeJaar en daarbij alle boeken die gebruikt
			worden. Als de titel/auteur van een vak rood gekleurd is, dan
			is het boek volgens BookWeb niet bestelbaar.\n";

			if ($showBrunaTabel){
				// De tabel op de Bruna-manier maken. In volgorde:
				// ISBN -- Titel -- Auteur -- Editie (druk)
				echo "<table width=\"100%\" class=\"bw_datatable_medium smallfont\">\n";
				echo "	<tr>\n";
				echo "		<th>ISBN</th>\n";
				echo "		<th>Titel</th>\n";
				echo "		<th>Auteur</th>\n";
				echo "		<th>Editie (druk)</th>\n";
				echo "		<th>Uitgever</th>\n";
				echo "	</tr>\n";

				// Aangezien dit systeem vak-georienteerd is, kunnen
				// sommige boeken dubbel in de lijst terecht komen.
				// Vakinformatie kan bovendien weg uit de array: filteren
				// dus!
				$nieuweLijst = array();
				$passedEans = array();
				foreach($boekenLijst as $vaknr=>$eans){
					foreach($eans as $ean=>$datums){
						if (!in_array($ean, $passedEans)){
							$passedEans[] = $ean;
							$nieuweLijst[null][$ean]["begindatum"] = "";
							$nieuweLijst[null][$ean]["einddatum"] = "";
						}
					}
				}
				$boekenLijst = $nieuweLijst;
			} elseif (!$showPlain){
				echo "<table width=\"100%\" class=\"bw_datatable_medium smallfont\">\n";

				if ($showPrijsVergelijking){
					// vak, studierichting, Dictaat, ISBN, titel (auteur) onze prijs (laatste offerte) en links naar externe winkels
					echo "<tr>\n" .
							"<th>Vak</th>" .
							"<th width=\"1\">Studie(s)</th>" .
							"<th>Dictaat</th>\n" .
							"<th>ISBN</th>\n" .
							"<th>Titel (auteur)</th>\n" .
							"<th>Offerte</th>\n" .
							"<th>(extern)</th>\n" .
						"</tr>";
				} else {
					// Mooie tabel maken voor boekcom (vakcode, vak, studies, looptijd, dictaat, isbn, titel (auteur)
					echo "	<tr>\n";
					echo "		<th>Vakcode</th>\n";
					echo "		<th>Vak</th>\n";
					echo "		<th width=\"1\">Studie(s)</th>\n";
					echo "		<th>Looptijd</th>\n";
					echo "		<th>Dictaat</th>\n";
					echo "		<th>ISBN</th>\n";
					echo "		<th>Titel (auteur)</th>\n";
					echo "	</tr>\n";
				}
			}

			foreach($boekenLijst as $vaknr=>$eans){
				foreach($eans as $ean=>$datums){
					$boekinfo = sqlBoekGegevens($ean);
					$beginDatum = $datums["begindatum"];
					$eindDatum = $datums["einddatum"];

					if (is_numeric($vaknr)){
						$vakdata = sqlVakkenGegevens(array($vaknr));
						$vakdata = $vakdata[$vaknr];
					} else{
						$vakdata = null;
					}

					if (!$showPlain){

						if ( ($showBrunaTabel && $ean != null) || !$showBrunaTabel) {
							// Rij in tabel tonen
							echo "	<tr>\n";

							if (!$showBrunaTabel){
								// VakInfo eerst printen

								if (!$showPrijsVergelijking){
									// Vakcode
									echo "		<td valign=\"top\">";
									if ($vakdata["afkorting"]){
										echo makeOsirisLink($vakdata["afkorting"], $requestedCollegeJaar, $vakdata["afkorting"]);
									} else {
										echo "&nbsp;";
									}
									echo "</td>\n";
								}

								// Vaknaam (met link)
								echo "		<td valign=\"top\">";
								echo makeBookRef('vakinfo', htmlspecialchars($vakdata['naam']), 'vaknr='.$vaknr);
								echo "</td>\n";

								// Studies
								echo "		<td valign=\"top\">";
								$vakstudies = sqlGetVakStudies($vaknr);
								foreach ($vakstudies as $studie=>$jaren){
									echo "$studie&nbsp;";
									echo implode(",&nbsp;", $jaren);
									echo "<br/>";
								}
								echo "</td>\n";

								if (!$showPrijsVergelijking){
									// Looptijd
									echo "		<td valign=\"top\">\n";
									echo "			$beginDatum tot $eindDatum\n";
									echo "		</td>\n";
								}

								echo "<td valgin=\"top\">\n";
								echo "$vakdata[dictaat]\n";
								echo "</td>";

							} // else: Bruna-tabel heeft geen vakinfo

							if ($showBrunaTabel && $ean != null){
								// Informatie tonen op de Bruna-manier
								// (aparte kolommen voor titel en auteur,
								// vakinfo wordt toch niet getoond)
								echo "		<td valign=\"top\">\n";

								// Let op! Onderstaande enkele quote is
								// niet voor niets! Op deze manier kun je
								// copy-pasten in oocalc (o.i.d.) zonder
								// dat oocalc het ziet als een getal
								echo "			'";
								echo ean_to_isbn($boekinfo["EAN"]) . "\n";
								echo "		</td>\n";
								echo "		<td valign=\"top\">\n";
								echo "			" . $boekinfo["titel"] . "\n";
								echo "		</td>\n";
								echo "		<td valign=\"top\">\n";
								echo "			" . $boekinfo["auteur"] . "\n";
								echo "		</td>\n";
								echo "		<td valign=\"top\">\n";
								echo "			'" . $boekinfo["druk"] .	"\n";
								echo "		</td>\n";
								echo "		<td valign=\"top\">\n";
								echo "			" . $boekinfo["uitgever"] .	"\n";
								echo "		</td>\n";

							} elseif (!$showBrunaTabel && $ean != null) {
								// Tabel voor boekcom: andere manier van weergeven boekinfo

								// Informatie tonen op compacte manier,
								// zodat de tabel niet te breed wordt (dus
								// de titel en auteur in 1 veld, geen druk)

								// Boekinfo (isbn, titel+auteur)
								$isBestelbaar = $boekinfo["bestelbaar"] === "Y";

								echo "		<td valign=\"top\">" . print_EAN_bi($ean) . "</td>\n";

								if (!$isBestelbaar){
									echo "		<td valign=\"top\" class=\"waarschuwing\">";
								} else {
									echo "		<td valign=\"top\">";
								}

								echo $boekinfo["titel"];
								echo "<br/>(" . $boekinfo["auteur"] .	")";
								echo "</td>\n";

								if ($showPrijsVergelijking){
									// laatste offerte
									$offertes = sqlOffertePrijzen(array($ean));
									$offerte = @$offertes[$ean];

									echo "<td valign=\"top\">" .
										($offerte ? Money::addPrice($offerte) : "(onbekend)") .
										"</td>";

									// externe links
									$isbn = ean_to_isbn($ean);
									echo "<td valign=\"top\">" .
										makeExternLink(makeBroeseLink($ean), "Broese (Selexyz)") . " " .
										makeExternLink(makeVergelijkLink($ean), "Vergelijk.nl") .
										"</td>";
								} // else: geen prijsvergelijking-kolommen
							} elseif (!$showBrunaTabel && $ean == null) {
								// Boekcomtabel, maar het vak heeft geen boek
								echo "		<td valign=\"top\">n.v.t.</td>\n";
								echo "		<td valign=\"top\">n.v.t.</td>\n";
							}

							echo "	</tr>\n";
						}
						// else: een rij tonen, een vak zonder boeken is
						// voor bruna niet interessant
					} elseif ($ean != null) {
						// Lijst plaintext tonen
						echo ean_to_isbn($ean) . " -- ";
						echo "\"" . $boekinfo["titel"] . "\"" . " door ";
						echo "\"" . $boekinfo["auteur"] . "\"" . "<br />\n";
					}
				}
			}

			if (!$showPlain){
				// Tabel afsluiten
				echo "</table>\n";
			}
		}
	}
}

/**
	Implementeert de functie "offerteproblemen": toont dus
	boeken met een verlopen offerte of zonder offerte
*/
function offerteProblemen(){
	$verloopDagen = tryPar("verloopdagen", "31");
	if (!is_numeric($verloopDagen)) $verloopDagen = "31";

	echo "<h2>Boeken met een offerteprobleem</h2>\n";
	echo "<form action=\"\" method=\"GET\">\n";
	echo "<input type=\"hidden\" name=\"page\"
	value=\"offerteproblemen\">\n";
	echo "De boeken in de onderstaande lijst hebben een offerteprobleem. Dat wil zeggen:\n";
	echo "<ul>\n";
	echo "<li>er is nooit een offerte ingevoerd voor het boek</li>\n";
	echo "<li>de laatste offerte voor het boek is verlopen</li>\n";
	echo "<li>";
	echo "de offerte verloopt binnen <input name=\"verloopdagen\" type=\"text\" size=\"3\"
	maxlength=\"4\" value=\"$verloopDagen\"> dag(en)";
	echo "</li>\n";
	echo "</ul>\n";
	echo "</form>\n";


	echo "In de onderstaande tabel worden alleen boeken getoond die op dit
	moment gebruikt worden voor een vak, of die gebruikt gaan worden in de
	toekomst. Ook staan er boeken in die besteld zijn door studenten, maar
	waar geen offerte van bekend is. ISBNs die gevolgd worden door een rood
	sterretje zijn niet (meer) leverbaar volgens BookWeb.<br/>\n";

	$offerteproblemen = sqlGetOfferteProblemen($verloopDagen);

	echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
	echo "	<tr>\n";
	echo "		<th><strong>ISBN</strong></th>\n";
	echo "		<th><strong>Auteur</strong></th>\n";
	echo "		<th><strong>Titel</strong></th>\n";
	echo "		<th><strong>Uitgever</strong></th>\n";
	echo "		<th><strong>Verloopdatum<br/> laatste offerte</strong></th>\n";
	echo "	</tr>\n";

	foreach ($offerteproblemen as $ean=>$datum){
		$boekinfo = sqlBoekGegevens($ean);
		$boekUsed = sqlGetBookUsed($ean);

		if ($boekUsed){
			echo "	<tr>\n";

			echo "		<td>";
			echo print_EAN_bi($ean);
			if ($boekinfo["bestelbaar"] != "Y"){
				echo "<font color=\"red\">*</font>\n";
			}
			echo "</td>\n";

			echo "		<td>" . $boekinfo["auteur"] . "</td>\n";
			echo "		<td>" . $boekinfo["titel"] . "</td>\n";
			echo "		<td>" . $boekinfo["uitgever"] . "</td>\n";

			echo "		<td>\n";
			if ($datum == false){
				echo "n.v.t.";
			} else {
				echo "$datum";
			}
			echo "		</td>\n";

			echo "	</tr>\n";
		}
	}
	echo "</table><br/><br/>\n";

}


/** Stuurt de daadwerkelijke mails te deur uit, kiest je juiste functie ahv
 * bepaalde tryPar-statements */
function stuurDocentenBoekenMail(){

	global $PERIODES;

	// Mail sturen over een specifiek vak
	if ($vaknr = tryPar("vaknr")){
		docentenBoekenMailPerVak(array(array($vaknr, null, null)));
		return true;
	}

	// Mail sturen naar alle docenten uit een gegeven set periodes en jaren
	if (tryPar('action') == 'go') {
		$jaarperiodes = tryPar('jaarperiodes');
		if (!isset($jaarperiodes) || sizeOf($jaarperiodes) == 0) {
			printHTML('Er waren geen periodes geselecteerd!', TRUE);
			return false;
		}

		$mails = array();
		foreach ($jaarperiodes as $jaar => $periodes) {
			foreach ($periodes as $periode => $dummy) {
				$begindatum = $PERIODES[$jaar][$periode];
				$einddatum = periodeEinddatum($jaar, $periode);
				if ( !$einddatum ) user_error("Geen einddatum gevonden voor ($jaar,$periode). Is er wel een periode na $periode gedefinieerd?", E_USER_ERROR);
				$vakkkendata = sqlGetHuidigeVakken($jaar, NULL, NULL,
					$begindatum, $einddatum);
				$boekendata = sqlBoeken($jaar, NULL, NULL,
					$begindatum, $einddatum);
				$ontberekendedata = array();
				$voorstudie = tryPar('studie');

				foreach ($vakkkendata as $vaknr => $vakinfo) {
					// vak begint niet in deze periode, dus het loopt nog.
					// dan nemen we het niet op.
					if ( !( $vakinfo['begindatum'] >= $begindatum && $vakinfo['begindatum'] <= $einddatum )) {
						continue;
					}

					if (!isset($vakinfo['email']) || !$vakinfo['email']) {
						$ontberekendedata[$vaknr][] = "Bij het vak $vakinfo[naam] ($vaknr) is geen e-mailadres bekend.";
					}
					if (!isset($vakinfo['docent']) || !$vakinfo['docent']) {
						$ontberekendedata[$vaknr][] = "Bij het vak $vakinfo[naam] ($vaknr) is geen docent bekend.";
					}

					// specifieke studie geselecteerd?
					if ( $voorstudie != 'alle' ) {
						if ( $vakinfo['studie'] != $voorstudie ) continue;
					}

					$boeken = array();
					if (!isset($boekendata[$vaknr])) $boekendata[$vaknr] = array();
					foreach ($boekendata[$vaknr] as $boekvaknr => $boekvakdata) {
						$boeken[$boekvaknr]['titel'] = $boekvakdata['titel'];
						$boeken[$boekvaknr]['auteur'] = $boekvakdata['auteur'];
						$boeken[$boekvaknr]['druk'] = @$boekvakdata['druk'];
						$boeken[$boekvaknr]['EAN'] = $boekvakdata['EAN'];
						$boeken[$boekvaknr]['verplicht'] = $boekvakdata['verplicht'];
					}

					if (!isset($ontberekendedata[$vaknr])) {
						$mails[] = array($vaknr, $vakinfo, $boeken);
					}
				}
			}
		}
		if(count($mails) == 0) {
			printHTML('Er zijn geen vakken die beginnen in de geselecteerde
			periode(s).  Komen de begin data van de vakken wel overeen met die
			van de periodes?');
		} else {
			docentenBoekenMailPerVak($mails);
		}

		if (sizeOf($ontberekendedata) > 0) {
			printHTML('De volgende problemen zijn opgetreden. Deze docenten van '
				.'deze vakken zijn niet gemaild:', TRUE);

			echo "<ul>\n";
			foreach ($ontberekendedata as $ontberekendevakdata) {
				foreach ($ontberekendevakdata as $regel) {
					echo "<li class=\"waarschuwing\">$regel\n";
				}
			}
			echo "</ul>\n";
		}
		return true;
	}

	// Mail sturen over boeken die nog in de vrije verkoop liggen
	if (tryPar("mailtype") == "vrijeverkoop"){
		// Alle vrije boeken opzoeken
		$alleTitels = sqlFaseInfo(3);
		foreach ($alleTitels as $EAN=>$data){
			if ($alleTitels["$EAN"]["aantal"] == 0) continue;

			$teverkopen["$EAN"] = $data;
		}


		// Vakdata
		$boekvakdata = sqlGetBoekVakData(array_keys($teverkopen), true);

		// Vaknrs verzamelen
		echo "<h3>Docenten mailen over boeken in vrije verkoop:</h3>";
		echo "Onderstaande docenten hebben een mail ontvangen over de
		boeken in de vrije verkoop:";
		echo "<table class=\"bw_datatable_medium\">" .
			"	<tr>\n" .
			"		<th>ISBN/EAN</th>\n" .
			"		<th>Titel</th>\n" .
			"		<th>Auteur</th>\n" .
			"		<th>Vak</th>\n" .
			"		<th>Docent(en)</th>\n" .
			"		<th>Mailadres</th>\n" .
			"		<th>&nbsp;</th>\n" .
			"	</tr>\n";

		$negativeTD = '<td valign="top"><img src="/Layout/Images/Icons/negative.png" alt="Fout!" /></td>';
		$positiveTD = '<td valign="top"><img src="/Layout/Images/Icons/positive.png" alt="Goed!" /></td>';

		$vaknrs = array();
		foreach ($boekvakdata as $vakdata){
			$vaknr = $vakdata["vaknr"];
			$vaknaam = $vakdata["naam"];
			$docent = $vakdata["docent"];
			$mailadr = $vakdata["email"];
			$EAN = $vakdata["EAN"];

			$boektitel = $teverkopen["$EAN"]["titel"];
			$boekauteur = $teverkopen["$EAN"]["auteur"];
			$boekdruk = $teverkopen["$EAN"]["druk"];

			echo "	<tr>";
			echo '<td valign="top">' . print_EAN_bi($EAN) . "</td>" .
				'<td valign="top">' . cut_titel($boektitel) . "</td>" .
				'<td valign="top">' . cut_auteur($boekauteur) . "</td>" .
				'<td valign="top">' . "$vaknaam</td>" .
				'<td valign="top">' . "$docent</td>" .
				'<td valign="top">' . "$mailadr</td>";

			// Mail daadwerkelijk versturen
			$maildata["boektitel"] = $boektitel;
			$maildata["vaknaam"] = $vaknaam;

			if ($maildata["boektitel"] && $maildata["vaknaam"]){
				echo $positiveTD;
				bwmail($mailadr, BWMAIL_DOCENT_VRIJEVERKOOP, $maildata);
			} else {
				echo $negativeTD;
			}
			echo "</tr>\n";
		}
		echo "</table>";



		return true;
	}

	return false;
}

function docentenBoekenMail()
{
	global $PERIODES;

	// Stuurt de mails de deur uit, deze functie zoekt zelf uit welke
	// mails. Als het resultaat "true" is, dan zijn er mails verstuurd,
	// anders niet.
	$res = stuurDocentenBoekenMail();
	if ($res) return;


	$nu = strftime('%Y-%m-%d %H:%M:%S');
	$vakkkendata = sqlGetHuidigeVakken(colJaar(), NULL, NULL, $nu, NULL);

	/* Over welk interval moeten de periodes weergegeven worden */
	$mindatum = $maxdatum = substr($nu, 0, 10);
	foreach ($vakkkendata as $vakdata) {
		if ($vakdata['begindatum'] < $mindatum) {
			$mindatum = $vakdata['begindatum'];
		}
		if ($vakdata['einddatum'] > $maxdatum) {
			$maxdatum = $vakdata['einddatum'];
		}
	}

	$mincoljaar = fromColJaar(substr($mindatum, 5, 2), substr($mindatum, 0, 4));
	$maxcoljaar = fromColJaar(substr($maxdatum, 5, 2), substr($maxdatum, 0, 4));
	$huidigcoljaar = colJaar();
	$huidigeper    = huidigePeriode();

	echo '<h2 class="text-warning bg-warning">Waarschuwing! Dit is de oude, achterhaalde docentenmailing die je niet zelf kan aanpassen. Probeer veel liever <a href="/Onderwijs/Boekweb/Docentenmail">Boekenweb2</a>.</h2>';

	echo "<h3>Controlemail boeken bij vakken</h3>";
	echo "Met onderstaand formulier kun je docenten een controlemail sturen
	met daarin de vraag of de informatie die A-Eskwadraat heeft over de
	boeken bij een vak correct is.<br/>";
	echo "Vink hieronder de periodes aan waarvoor je de controlemail naar
	de docenten wilt sturen. Er wordt gemaild over elk vak dat <em>begint</em>
	in die periode.";

	simpleForm("", "class=\"bw_datatable_medium\"");
	echo addHidden("mailtype", "controlemail");
	echo "<tr><th>Collegejaar</th><th>Periodes</th></tr>\n";
	for ($i = $mincoljaar; $i <= $maxcoljaar; $i++) {
		echo '<tr><td><strong>'.$i.'-'.($i+1).'</strong></td><td>';
		foreach($PERIODES[$i] as $periode => $begindatum) {
			echo $periode.addCheckBox("jaarperiodes[$i][$periode]",
				($i > $huidigcoljaar || $periode > $huidigeper), true).' ';
		}
		echo "</td></tr>\n";
	}

	global $STUDIENAMEN;
	$stds = array_merge(array('alle' => 'alle studies'), $STUDIENAMEN);

	echo "<tr><td>Voor vakken van:</td><td>" .addSelect('studie', $stds, NULL, TRUE) ."</td></tr>\n";

	endForm('Stuur mail!', FALSE);
	echo "<hr><h3>Mail over boeken in vrije verkoop</h3>";
	echo "Met onderstaande knop worden alle docenten gemaild die vakken
	doceren waarvan er boeken over zijn (in de vrije verkoop liggen).";

	echo addForm("", "POST");
	echo addHidden("mailtype","vrijeverkoop");
	echo addFormEnd("Docenten op de hoogte stellen van boeken in de vrije verkoop", false);

}

