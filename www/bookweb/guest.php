<?php
// $Id$

/*
 * Functies tbv het bestellen van boeken door leden.
 */

function bestelform()
{
	requireAuth('bijnalid');

	if(tryPar('action') == 'go')
	{
		$ISBN = strtoupper(tryPar('ISBN'));
		// ISBN optioneel
		if($ISBN) {
			$ISBN = preg_replace('/\W/','', $ISBN);
			if (!is_isbn($ISBN))
				print_error(_('Ongeldige ISBN'));
			$EAN = isbn_to_ean($ISBN);
			if (sqlBoekGegevens($EAN)) {
				printHTML(_("Dit boek is bij ons bekend. Je kunt het nu ")).
				makeBookRef("bestellen"
					   ,_("bestellen")
					   ,"action=confirmbestel&EANs[$EAN]=1").".";
				return;
			}
		}
		$data = array();
		$data['ISBN']   = $ISBN;
		$data['titel']  = requirePar('titel');
		$data['auteur'] = requirePar('auteur');
		$druk = tryPar('druk');
		$data['druk']   = $druk ? $druk : _('n.v.t.');
		$uitgever = tryPar('uitgever');
		$opmerking = tryPar('opmerking');
		$data['opmerking'] = $opmerking ? $opmerking : _('Leeg');
		$data['uitgever']   = $uitgever ? $uitgever : _('onbekend');

		bwmail(getSelLid(), BWMAIL_STUDENT_SPECIALE_BESTELLING, $data);
		printHTML(_("De bestelling is naar de Boekencommissaris gestuurd."));
		return;
	}

	printHTML(_("Vul hier in welk boek je wilt bestellen. Opmerking: alleen "
		."boeken die niet in het bestelmenu te vinden zijn, kunnen op deze "
		."manier besteld worden."));
	simpleForm(_('go'));
	inputText(_('Titel'),'titel',tryPar('titel'));
	inputText(_('Auteur'),'auteur',tryPar('auteur'));
	inputText(_('ISBN'),'ISBN',strtoupper(tryPar('ISBN')));
	inputText(_('Druk'),'druk',tryPar('druk'));
	inputText(_('Uitgever'),'uitgever',tryPar('uitgever'));
	printHTML("<tr><td>" . _("Opmerkingen") . "</td><td>");
	echo addTextArea('opmerking',tryPar("opmerking"));
	printHTML("</td></tr>");
	endForm(_('Bestel'));
}

/**
 * Print een array met boeken; dit wordt gebruikt in de wel- en niet-vakgebonden
 * boekenbestellijsten.
 */
function printBoekenBestelLijst($boeken, $vakken) {

	global $STUDIEJAREN,$STUDIENAMEN,$STUDIENAMEN_en;

	$pervak = (boolean) $vakken;

	// Maak lijst met alle te tonen EANs om bij calcVerwachteLeverdatum
	// niet onnodig veel queries uit te voeren.
	$EANs = array();
	foreach ($boeken as $vakdata) {
		if($pervak) {
			foreach ($vakdata as $boekvaknr => $boekvakdata) {
				if ($boekvaknr == 'studies') continue;
				$EANs[$boekvakdata['EAN']] = TRUE;
			}
		} else {
			$EANs[$vakdata['EAN']] = TRUE;
		}
	}
	ksort($EANs);
	$verwachtelevdatum = calcVerwachteLeverdatums(array_keys($EANs));
	$offprijzen        = sqlOffertePrijzen(array_keys($EANs));

	printHTML(_('Vink de boeken aan die je wilt bestellen, en klik dan op <em>Verder</em>.'));

	simpleForm('confirmbestel', 'border=1');
	echo "<table style=\"font-size: 10pt\">\n"
		."<tr><th colspan='2'>"._("Boek")."</th><th>"._("Auteur")."</th><th>"._("Druk")."</th><th>" . _("ISBN") . "</th>"
		.($pervak ? "<th>"._("Verplicht")."</th>" : '')
		."<th>"._("Verwachte<br>leverdatum*")."</th>"
		."<th>"._("Verwachte<br>prijs**")."</th></tr>\n";

	if($pervak) {
		$lastvak = -1;
		foreach($vakken as $vaknr => $vakdata) {
			if (!isset($boeken[$vaknr])) {
				$boeken[$vaknr] = array();
			}

			// als lastvak niet gelijk is aan dit vak, dan begint hier een nieuw
			// vak: print dus de naam van het vak en de studievakinfo.
			if($lastvak != $vaknr) {
				echo "\n<tr><td colspan=\"8\"><br><b>";
				if (hasAuth('verkoper')) {
					echo makeBookRef('vakinfo', htmlspecialchars($vakdata['naam']),
						"vaknr=$vaknr")." ";
				} else {
					echo htmlspecialchars($vakdata['naam']);
				}

				echo "</b> <small>(";
				$studies = array();
				foreach($vakdata['studies'] as $studie => $jaren) {
					$jarenlang = array();
					foreach($jaren as $jaar) {
						$jarenlang[] = $STUDIEJAREN[$jaar];
					}// FIXME: gebruik reguliere STUDIENAMEN
					$studies[] = (getLang()=='en'?$STUDIENAMEN_en[$studie]:$STUDIENAMEN[$studie]).' '.
						implode('/',$jarenlang);
				}
				echo implode('; ', $studies);
				if (!empty($vakdata['url'])) {
					echo '; <a href="'.htmlspecialchars($vakdata['url']).'">website</a>';
				}
				if (!empty($vakdata['docent'])) {
					echo '; ' . _('docent') . ': ';
					if (!empty($vakdata['email'])) {
						echo '<a href="mailto:'.htmlspecialchars($vakdata['email']).
							'">';
					}
					echo htmlspecialchars($vakdata['docent']);
					if (!empty($vakdata['email'])) {
						echo '</a>';
					}
				}
				echo ")</small></td></tr>\n";
			}

			if (sizeOf($boeken[$vaknr]) == 0) {
				echo '<tr><td></td><td colspan="7">'
					."<em>"._("Voor dit vak zijn geen boeken bekend")."</em></td></tr>\n";
			}
			foreach ($boeken[$vaknr] as $boekvaknr => $boekvakdata) {
				if ($boekvaknr == 'studies') continue;

				printBoekenLijstRow($boekvakdata, $verwachtelevdatum[$boekvakdata['EAN']],
					$offprijzen[$boekvakdata['EAN']], $pervak);
			}
			if (vakGebruiktDictaat($vaknr)){
				$ook = '';
				if (sizeof($boeken[$vaknr]) > 0){
					$ook = _('ook');
				}
				echo '<tr><td></td><td colspan="7">' .
					'<em>'.sprintf(_('Bij dit vak wordt %s een dictaat gebruikt'),$ook) .
					"</em></td>";
			}

			$lastvak = $vaknr;
		}
	} else {
		// niet pervak, dus lijst met alle niet-vakgebonden boeken
		foreach ($boeken as $boekvaknr => $boekvakdata) {
			printBoekenLijstRow($boekvakdata, $verwachtelevdatum[$boekvakdata['EAN']],
				$offprijzen[$boekvakdata['EAN']], $pervak);
		}
	}

	endForm(_('Verder'));
}

/**
 * Print 1 regel in de boekenlijst met vinkvakje, titel, EAN enzo, verwachte
 * leverdatum, verwachte prijs
 */
function printBoekenLijstRow($boekvakdata, $verwachtelevdatum, $offprijs, $pervak) {

	$bestelbaar = ($boekvakdata['bestelbaar'] == 'Y');

	echo "<tr";
	if (!$bestelbaar) {
		echo ' class="disabled"';
	}
	echo "><td nowrap=\"nowrap\">".
		($pervak ?
				addCheckBox("boekvaknrs[$boekvakdata[boekvaknr]]",FALSE,1,$bestelbaar) :
				addCheckBox("EANs[$boekvakdata[EAN]]",FALSE,1, $bestelbaar) )
			. "</td><td>".
		htmlspecialchars($boekvakdata['titel'])."</td><td>".
		htmlspecialchars($boekvakdata['auteur'])."</td><td>".
		htmlspecialchars(@$boekvakdata['druk'])."</td><td>";
	if (hasAuth('verkoper')) {
		echo print_EAN_bi($boekvakdata['EAN']);
	} else {
		echo print_EAN($boekvakdata['EAN']);
	}
	echo ($pervak ? "</td><td align='center'>".
		($boekvakdata['verplicht'] == 'Y' ? _('verplicht') : _('optioneel')):'')
		.'</td>';
	if ( $bestelbaar ) {
		echo "<td align='center'>".print_date($verwachtelevdatum)
			.'</td>';
		echo "<td align='right'>";
		echo makeVergelijkLink($boekvakdata["EAN"],
			($offprijs?Money::addPrice($offprijs,true,true):_('onbekend')),
			_("Klik hier voor een prijsvergelijking met andere aanbieders"));
	} else {
		echo "<td colspan=\"2\"><em>"._("helaas niet bestelbaar")."</em>";
	}
	echo "</td></tr>\n";
	return;
}


function bestelmenu() {
	global $session, $COLJAREN, $STUDIEJAREN, $PERIODES, $STUDIENAMEN, $STUDIENAMEN_en;

	switch(tryPar('action')) {
		case '':
			$STUDIENAMEN[''] = _('Alle');
			// FIXME: gebruik studienamen ipv _en
			$STUDIENAMEN_en[''] = 'All';

			$studiejar = $STUDIEJAREN;
			$studiejar[''] = _('Alle');

			$period = array_keys($PERIODES[colJaar()]);
			$aantperiodes = sizeOf($period);
			$period[''] = _('Alle');

			global $BWDB;
			$vakjaren = $BWDB->q('TABLE SELECT IF (MONTH(begindatum) < '
				.COLJAARSWITCHMAAND.
				',YEAR(begindatum)-1, YEAR(begindatum)) AS coljaar
				FROM vak WHERE einddatum >= NOW()
				GROUP BY coljaar');
			$jaren = array();
			foreach ($vakjaren as $row) {
				$jaren[$row['coljaar']] = $COLJAREN[$row['coljaar']];
			}

			printHTML(_('Voordat je boeken kunt bestellen willen we graag weten '
				.'voor welk jaar en periode en voor welke studie je boeken '
				.'wilt bestellen. Je kunt eventueel ook zoeken op een vak, '
				.'titel, auteur of ISBN.'));
			/**
				Er zijn drie verschillende "zoekfuncties":
				1) lijsten per studie e.d.
				2) zoeken op vak, titel, auteur of ISBN
				3) alles tonen
				Deze zijn gescheiden in verschillende tabelcellen, dus
				alsvolgt:

				-------------------
				| form 1 | form 2 |
				-------------------

				(toon alles)
			*/
			echo "<table width=\"100%\">\n";
			echo "	<tr><td width=\"50%\" style=\"border-right: solid black
			1px;\" valign=\"top\">\n";
			echo "		<div align=\"center\">\n";
			echo "		<strong>"._("Boekenlijsten opvragen:")."</strong><br/>\n";
			echo "		</div>\n";

			// -------- LIJSTEN PER STUDIE --------- //
			simpleForm('boekenlijst','','','GET');

			echo "<TR><TD>"._("Studie")."</TD><TD>".
				addSelect('studie', getLang()=='en'?$STUDIENAMEN_en:$STUDIENAMEN, @$session->get('mystudie'), TRUE).
				"</TD></TR>\n";
			echo "<TR><TD>Ba/Ma</TD><TD>".
				addSelect('jaar', $studiejar, @$session->get('myjaar'), TRUE).
				"</TD></TR>\n";
			echo "<TR><TD>"._("Periode")."</TD><TD>".
				addSelect('periode',$period,
					($session->has('myperiod') ?
						$session->get('myperiod'):
						(1 + huidigePeriode()%$aantperiodes)), FALSE).
				"</TD></tr>\n";
			echo "<TR><TD>"._("Collegejaar")."</TD><TD>".
				addSelect('coljaar', $jaren, ($session->has('mycoljaar') ?
				$session->get('mycoljaar'):
				colJaar()), true).
				"</TD></tr>\n";
			endForm(_('Toon boekenlijst'),FALSE);

			echo "	</td><td width=\"50%\" align=\"center\" valign=\"top\">\n";
			echo "		<strong>"._("Zoeken op boek")."</strong><br/>\n";

			// -------- Zoeken op titel, auteur, isbn ----- //
			echo addForm('', 'GET');
			echo addHidden("page", "bestellen");
			echo addHidden("action", "zoekboeken");
			echo addHidden("zoeksoort", "boeken");
			echo _("Titel, auteur, ISBN:"). "<br/>";
			echo addInput1('zoekterm', '', false, 30);
			echo "<br/>\n";
			echo addFormEnd(_('Zoek boeken'), false);
			echo "<br/>\n";

			// --------- Zoeken op vak ------ //
			echo "<strong>"._("Zoeken op vak")."</strong>\n";
			echo addForm('', 'GET');
			echo addHidden("page", "bestellen");
			echo addHidden("action", "zoekboeken");
			echo addHidden("zoeksoort", "vakken");
			echo _("Vaknaam, docent:")."<br/>";
			echo addInput1('zoekterm', '', false, 30);
			echo "<br/>\n";
			echo addFormEnd(_('Zoek vakken'), false);


			echo "	</td></tr>\n";
			echo "</table>\n";

			// -------- Alles tonen --------- //
			alleBoekenText();
		break;

		case 'boekenlijst':

			$coljaar      = tryPar('coljaar', colJaar());
			$aantperiodes = sizeOf($PERIODES[$coljaar]);
			$periode      = tryPar('periode', 'Alle');

			if (($periode != 'Alle' && $periode != 'All') && ($periode < 1 || $periode > $aantperiodes)) {
				print_error('Interne Fout! Er bestaat geen periode '.$periode
					.' in collegejaar '.$coljaar.'!');
			}
			if ($periode == 'Alle' || $periode == 'All') {
				$begindatum = $PERIODES[$coljaar][1];
			}
			else {
				$begindatum = $PERIODES[$coljaar][$periode];
			}

			if (($periode != 'Alle' && $periode != 'All') && $periode < $aantperiodes) {
				$einddatum = $PERIODES[$coljaar][$periode+1];
			}
			else {
				$einddatum = @$PERIODES[$coljaar+1][1];
			}

			$jaar = tryPar('jaar');
			$studie = tryPar('studie');
			$session->set('myjaar', $jaar);
			$session->set('mystudie', $studie);
			$session->set('myperiod', $periode);
			$session->set('mycoljaar', $coljaar);

			$boeken = sqlBoeken($coljaar
				,$jaar
				,$studie
				,$begindatum
				,$einddatum);

			$vakken = sqlGetHuidigeVakken($coljaar
				,$jaar
				,$studie
				,$begindatum
				,$einddatum);
			if (sizeof($boeken) == 0) {
				printHTML(_('Geen boeken gevonden die aan je eisen voldoen.'));
				break;
			}

			printBoekenBestelLijst($boeken, $vakken);

			alleBoekenText();
			voetnootText();
			break;
		case 'zoekboeken':
			// Om de SQL-functies niet onnodig opnieuw te schrijven met
			// zoekfunctie (bestelbare boeken, lopende vakken) worden hier
			// alle bestelbare boeken en lopende vakken opgevraagd en wordt
			// er in PHP-code gezocht
			$zoekterm = strtolower(tryPar("zoekterm"));
			$zoeksoort = strtolower(tryPar("zoeksoort"));

			if (!$zoekterm){
				echo _("Je hebt geen zoekterm opgegeven!");
			} else {
				$gevondenboeken = array();
				$gevondenvakken = false;
				if ($zoeksoort == "vakken"){
					// Eerst vakken opzoeken, dan kijken welke boeken
					// daarbij horen

					$allevakken = sqlGetHuidigeVakken("","","","","");

					foreach ($allevakken as $vak){
						$hit = false;
						$vaknaam = strtolower($vak["naam"]);
						$vakdocent = strtolower($vak["docent"]);

						$hit = (strpos($vaknaam, $zoekterm) !== FALSE) || $hit;
						$hit = (strpos($vakdocent, $zoekterm) !== FALSE) || $hit;

						if ($hit){
							$gevondenvakken[$vak["vaknr"]] = $vak;
						}
					}

					foreach ($gevondenvakken as $vak){
						$vakboeken = sqlGetVakBoeken($vak["vaknr"]);

						// Array herschrijven
						foreach ($vakboeken as $boek){
							$vaknr = $vak["vaknr"];
							$boekvaknr = $boek["boekvaknr"];

							$gevondenboeken[$vaknr][$boekvaknr] = $boek;
						}
					}
				} else {
					// Zoeken op titel, auteur of isbn
					$isbn = str_replace('-', '', $zoekterm);
					$ean = (is_isbn($isbn) ? isbn_to_ean($isbn) : null);
					$gevondenboeken = sqlAlikeBoeken(array('EAN' => $ean,
						'titel' => $zoekterm, 'auteur' => $zoekterm),
						true); // true = alleenbestelbaar
				}

				if (sizeof($gevondenboeken) > 0){
					printBoekenBestelLijst($gevondenboeken,$gevondenvakken);
				} else {
					echo "<br/>\n";
					echo _("Helaas! Er zijn geen boeken of vakken gevonden die voldoen aan je zoekterm");
				}
			}
			alleBoekenText();
			voetnootText();
			break;
		case 'alleboekenlijst':
			$boeken=sqlAlleBestelbareArtikelen();
			if (sizeof($boeken)==0)
			{
				printHTML(_("Geen boeken gevonden!"));
				break;
			}

			printBoekenBestelLijst($boeken, FALSE);

			printHTML(sprintf(_("Als het boek dat je wilt bestellen niet in deze lijst staat, vul dan een %s in."),
				makeBookRef("bestelform",_("bestelformulier"))));

			voetnootText();
			break;
		case 'confirmbestel':

			$boekvaknrs = tryPar('boekvaknrs');
			$EANs 		= tryPar('EANs');
			if (!$boekvaknrs) $boekvaknrs = array();
			if (!$EANs) $EANs = array();
			if (sizeOf($boekvaknrs) == 0 && sizeOf($EANs) == 0) {
				printHTML(_('Je hebt geen boeken geselecteerd!'));
				break;
			}

			if(count($boekvaknrs) > 0) {
				$boekvaknrsinfo = sqlBoekVakGegevens(array_keys($boekvaknrs));
			} else {
				$boekvaknrsinfo = array();
			}
			if(count($EANs) > 0) {
				$eansinfo = sqlBoekenGegevens(array_keys($EANs));
			} else {
				$eansinfo = array();
			}

			$allEANs = array_merge(unzip($boekvaknrsinfo,'EAN'),
				array_keys($EANs));
			$allInfo = array_merge($boekvaknrsinfo,$eansinfo);

			$reedsEANs = sqlCheckReedsBesteld($allEANs, getSelLid());
			$nieuws = sqlGetBoekenNieuws($allEANs);
			$papiermolen = sqlPapierMolenAangeboden($allEANs);
			$offertes = sqlOffertePrijzen($allEANs);

			printHTML('<h3>'._('Je wilt dus de volgende boeken bestellen:').'</h3>');
			echo addForm('').
				addHidden('page','bestellen').
				addHidden('action','bestel').
				"\n<ul>\n\n";

			foreach($allInfo as  $bvinfo) {

				if($bvinfo['bestelbaar'] != 'N') {
					if (!hasAuth('boekcom')) {
						if(isset($bvinfo['boekvaknr'])) {
							echo addHidden("boekvaknrs[$bvinfo[boekvaknr]]", 1);
						} else {
							echo addHidden("EANs[$bvinfo[EAN]]", 1);
						}
					}
				}

				echo "<li><strong>".
					htmlspecialchars($bvinfo['auteur'].": ".$bvinfo['titel'])."</strong><br />\n".
					"<em>ISBN ".print_EAN($bvinfo['EAN']).
					(@$bvinfo['boekvaknr']?", vak: ".htmlspecialchars($bvinfo['vak']) : '');
					if($offer = $offertes[$bvinfo['EAN']]) {
						echo ", "._("richtprijs").": ".Money::addPrice($offer,true,true);
					}
					echo "</em>\n\n<p><ul>";


					// bestelbaar?
					if($bvinfo['bestelbaar'] == 'N') {
						echo "<li><p>"._("Dit boek is <b>op het moment niet bij ons bestelbaar</b>!<br />"
							."<strong>Het wordt niet in je bestelling opgenomen.</strong>")
							."</p>\n\n";
					} elseif(hasAuth('boekcom')) {
						if (isset($bvinfo['boekvaknr'])) {
							echo "<li><p>Aantal: ".addInput1('boekvaknrs['
								.$bvinfo['boekvaknr'].']', '1', FALSE, 4)
								."<p>\n\n";
						} else {
							echo "<li><p>Aantal: ".addInput1('EANs['
								.$bvinfo['EAN'].']', '1', FALSE, 4)."<p>\n\n";
						}
					}

					// al eens besteld?
					if(in_array($bvinfo['EAN'], $reedsEANs)) {
						echo "<li><p>"._("<b>LET OP:</b> Je hebt dit boek al een keer bij ons gekocht!")."</p>\n";
					}
					// papiermolen?
					if(in_array($bvinfo['EAN'], $papiermolen)) {
						echo "<li><p>"._("Dit boek wordt ook door iemand tweedehands aangeboden op ")
								.makeRef('Papiermolen/view.html?EAN='.$bvinfo['EAN'],_('de PapierMolen')).".</p>";
					}
					// is er nieuws?
					foreach($nieuws as $nwsitem) {
						if($bvinfo['EAN'] == $nwsitem['EAN']) {
							echo "<li><p>".$nwsitem['nieuws']." [".trim(print_date($nwsitem['datum']))."]</p>\n";
						}
					}

				echo "</ul><p><br></li>\n\n";
			}

			echo "</ul>\n\n";
			addRedo();
			if (!Lid::geef(getSelLid())->getEmail()) {
				$je = hasAuth('boekcom')?_('dit lid'):_('je');
				printHTML(sprintf(_("Let op: Wij hebben geen e-mailadres van $je. "
					.'Zodra de bestelling bij ons klaar ligt, kunnen we '
					."$je daarvan niet op de hoogte stellen. Zorg ervoor dat "
					."$je via de <a href=\"/Leden/%s"
					.'/Wijzig">Leden</a> afdeling een e-mailadres toevoegt, '
					."zodat we $je op de hoogte kunnen houden van de status "
					.'van de bestelling.'), getSelLid()), true);
			}
			if (count($allEANs) == 1) {
				normalEndForm(_('Ja, ik wil bovenstaand boek bestellen'));
			} else {
				normalEndForm(_('Ja, ik wil bovenstaande boeken bestellen'));
			}
			printHTML(sprintf(_('Lees voor je bestelt aandachtig de %s.'),
				makeRef('/boekcom/', _('voorwaarden'))));
			break;

		case 'bestel':
			checkRedo();

			$boekvaknrs = tryPar('boekvaknrs');
			$EANs = tryPar('EANs');
			if (!$boekvaknrs) {
				$boekvaknrs = array();
			}
			if (!$EANs) {
				$EANs = array();
			}

			if(hasAuth('verkoper')) {
				$prioriteit = (int) tryPar('prioriteit', SB_PRIO_REGULIER);
			} else {
				$prioriteit = SB_PRIO_REGULIER;
			}

			if (sizeof($boekvaknrs) == 0 && sizeof($EANs) == 0) {
				printHTML(_("Je hebt geen boeken geselecteerd!"));
				break;
			}

			bestel($boekvaknrs, $EANs, getSelLid(), $prioriteit);
			break;
		default:
			bwnietherkend(sprintf(_("Bestel %s"), $action));
	}
}

// wordt niet direct uit dispatch oid aangeroepen, maar alleen uit bestelmenu
function bestel($boekvaknrs, $EANs, $lidnr, $prioriteit) {

	if (!is_array($boekvaknrs)) {
		user_error("boekvaknrs is geen array, maar $boekvaknrs", E_USER_ERROR);
	}
	if (!is_array($EANs)) {
		user_error("EANs is geen array, maar $EANs", E_USER_ERROR);
	}

	sqlBestel($lidnr, $EANs, $boekvaknrs, $prioriteit);

	if (sizeOf($EANs) > 0 ) {
		processTo4(array_keys($EANs));
	}

	if (sizeOf($boekvaknrs) > 0 ) {
		$multiboekvakinfo = sqlBoekVakGegevens(array_keys($boekvaknrs));
		foreach ($multiboekvakinfo as $boekvak => $boekvakinfo) {
			$boekvakEANs[] = $boekvakinfo['EAN'];
		}
		processTo4($boekvakEANs);
	}

	printHTML(_("De bestelling is verwerkt, zie je bestellingsoverzicht hieronder voor de status van jouw bestellingen."));

	bestelling($lidnr);

}

function alleBoekenText()
{
	printHTML(_("Mocht je het boek dat je wilt bestellen niet kunnen vinden,
		dan is hier een stappenplan om je te helpen het te vinden:"));
	echo "<ol>\n<li>"
		._("Probeer een andere studie-, ba/ma- en periodecombinatie.")
		."\n<LI>"._("Bekijk ")
		.makeBookRef("bestellen?action=alleboekenlijst",
				_('de lijst met alle niet-vakgebonden boeken'))."."
		."\n<LI>"
		//voc: Vul een (bestelformulier) in.
		.sprintf(_("Vul een %s in. (Alleen voor boeken die niet in onze
		database staan!)"),
			makeBookRef("bestelform", _("bestelformulier")))
		."\n</OL>\n";

}

function voetnootText()
{
	global $BESTUUR;

	printHTML(_('* Naar ons beste weten is dit een zo dicht mogelijke benadering van de leverdatum van onze leverancier. De werkelijke leverdatum kan hier iets van afwijken.'));
	printHTML(sprintf(_('** Naar ons beste weten is dit een zo dicht mogelijke benadering van de prijs die het boek bij ons gaat kosten. De werkelijke prijs kan hier iets van afwijken. We bieden daarom hier geen prijsgarantie. Zie je ergens anders hetzelfde boek goedkoper, %slaat het dan even weten%s: vaak valt er nog wel iets te regelen. Klik op de prijs voor een prijsvergelijking met andere aanbieders.'),
		'<a href="mailto:' . $BESTUUR['boekencommissaris']->getEmailAdres() . '">','</a>'));
}

function alleBoekenOpenbaar()
{
/* 20020204 kink
 * Geeft een lijst met alle te bestellen boeken, voor iedereen
 * toegankelijk.
 */
	printHTML(_('<p>Hieronder vind je alle boeken die momenteel bij '
			.'A-Eskwadraat te bestellen zijn. Om ze ook daadwerkelijk te kunnen bestellen '
			.'moet je inloggen op BookWeb. </p><p><i>Het is ook mogelijk om boeken te bestellen die niet in deze lijst staan, neem daarvoor contact op met de <a mailto="boeken@a-eskwadraat.nl">BoekenCommissaris</a>.</i></p>'));
	$boeken = sqlAlleBestelbareArtikelen(FALSE);
	if (sizeof($boeken) == 0) {
		printHTML(_('Momenteel kun je geen boeken bestellen!'));
		return;
	}
	echo arrayToHTMLTable($boeken, array
		('EAN'   => 'ISBN'
		,'titel'  => _('Titel')
		,'auteur' => _('Auteur')
		,'druk'   => _('Druk')),
		array('EAN'=>'print_EAN'),
		array('druk' => 'center'),
		TRUE, "table table-striped table-condensed");
}

/* 20030111 Jacob
 * geeft een lijst met alle artikelen die bij de verkoop liggen en niet besteld
 * zijn. Deze zijn dus momenteel in de losse verkoop.
 *
 * 20060511: splitsing in twee tabellen: boeken (artikelen met ISBN) en
 * overige artikelen
 */
function alleLosseVerkoopBoeken() {
	$fases = sqlFaseInfo(3);

	$boeken = array();
	$artikelen = array();
	$EANs = array_keys($artikelen);
	$leverancierdata = array();

	foreach ($fases as $EAN => $fase) {
		//Geen boeken in fase 3
		if ($fase['aantal'] <= 0) continue;

		$voorraden = sqlGetEANVoorraad($EAN);
		$minprijs = null;
		$maxprijs = null;
		foreach ($voorraden as $voorraad){
			if ($voorraad["voorraad"] <= 0) continue;
			$levnr = $voorraad["leveranciernr"];

			if (!isset($leverancierdata[$levnr])) $leverancierdata[$levnr] = sqlLeverancierData($levnr);
			$marge = $leverancierdata[$levnr]["boekenmarge"];

			$voorraadPrijs = berekenVerkoopPrijs(
				$voorraad["levprijs"], $marge, $voorraad["adviesprijs"]
			);

			if ($minprijs == null || $minprijs >= $voorraadPrijs) $minprijs = $voorraadPrijs;
			if ($maxprijs == null || $maxprijs <= $voorraadPrijs) $maxprijs = $voorraadPrijs;
		}


		//Maak een link als persoon ingelogd is
		if (hasAuth('bijnalid')){
			if ($fase['bestelbaar']) {
				$fase['titel'] = makeBookRef
					('bestellen'
					,$fase['titel']
					,"action=confirmbestel&EANs[$EAN]=1");
			}

			if ($minprijs == null){
				$fase["prijs"] = _("onbekend");
			} elseif ($minprijs == $maxprijs){
				$fase["prijs"] = Money::addPrice($minprijs);
			} else {
				$fase["prijs"] = sprintf(_("tussen %s en %s"), Money::addPrice($minprijs)
							, Money::addPrice($maxprijs));
			}
		} else {
			// geen lid?
			$fase["prijs"] = "&nbsp;";
		}
		$fase['EAN'] = hasAuth('verkoper')?print_EAN_bi($EAN):print_EAN($EAN);

		if (ean_to_isbn($EAN)){
			$boeken[] = $fase;
		} else {
			$artikelen[] = $fase;
		}
	}

	if (sizeOf($boeken) + sizeof($artikelen) == 0) {
       	echo _('Momenteel zijn er geen artikelen die direct leverbaar zijn.');
		return;
	}

	echo "<p>"._("De volgende artikelen zijn op voorraad bij de boekverkoop en zijn "
		."niet besteld door leden.<br />\nAls je een van de boeken wilt kopen, "
		."is het aan te raden 'm te bestellen. Dan is het boek aan jou "
		."gekoppeld en weet je zeker dat 'ie voor jou is als je 'm komt "
		."ophalen.")
		."</p>\n\n";
	echo "<p>"._("Je kunt een boek bestellen door ")
		.(hasAuth('bijnalid')?_("het boek aan te klikken.<br />\nNiet aanklikbare "
			."artikelen zijn niet vantevoren te bestellen en kunnen alleen "
			."direct bij de boekverkoop worden gekocht")
			:_('in te loggen'))
		.".</p>\n\n";

	echo "<h3>"._("Voorradige boeken")."</h3>\n\n";
	if (sizeof($boeken) == 0){
		echo "<em>" . _("Op dit moment liggen er geen boeken in de vrije voorraad") . "</em>";
	} else {
		echo arrayToHTMLTable($boeken, array
			('aantal' => _('Aantal')
			,'titel'  => _('Titel')
			,'auteur' => _('Auteur')
			,'druk'   => _('Druk')
			,'EAN'    => _('ISBN')
			,'prijs'  => _('Prijs')), null, null, null, "table table-striped table-condensed");
	}

	echo "<br/><br/>\n\n<h3>" . _("Overige artikelen") . "</h3>\n\n";
	if (sizeof($artikelen) == 0){
		echo "<em>" . _('Op dit moment liggen er geen overige artikelen in de vrije verkoop') . "</em>";
	} else {
		echo arrayToHTMLTable($artikelen, array
			('aantal' => _('Aantal')
			,'titel'  => _('Titel')
			,'auteur' => _('Commissie / Auteur')
			,'EAN'    => _('Artikelcode')
			,'prijs'  => _('Prijs')), null, null, null, "table table-striped table-condensed");
	}
}

