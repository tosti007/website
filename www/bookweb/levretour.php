<?php

/**
 *
 * Invoeren
 * (en management)
 * van leveringen en retouren
 *
 * $Id$
 */


/* Herzien door Jacob op 30-04-2002
 *                 en op 12-04-2003
 *     en door Jeroen op 25-09-2003
 *      en door Thijs op 26-11-2003
 */
// levering danwel retour, functionaliteit is in zoverre hetzelfde dat
// dezelfde functie gebruikt wordt, en slechts op basis van requirePar('page')
// onderscheid gemaakt wordt
function levretour()
{
	if (tryPar('action') == 'process') {
		if (requirePar('page') == 'levering') {
			leveringprocess();
		} else {
			retourprocess();
		}
		return;
	}

	if (tryPar('page') == "levering") $isLevering = true;
	else $isLevering = false;

	if (!go()) {
		levretourform();
		return;
	}

	//Er is een levering/retour ingevoerd

	// ff checken of de gebruiker 'hetzij' begrepen heeft
	if ((tryPar('EAN') || tryPar('aantal') || tryPar('allesinbvk')) && tryPar('data')) {
		print_warning("Pak de Dikke Van Dale, en zoek 'Hetzij' op.");
		levretourform(null,tryPar('factuurnr',null),tryPar('plaats'),tryPar('data'));
		return;
	}

	if (!tryPar('EAN') && !tryPar('allesinbvk') && !tryPar('data')){
		print_warning("Je hebt onvoldoende ingevuld, je moet wel even vertellen wat je wil " .
		($isLevering ? " leveren." : "retour sturen"));
		levretourform(null,tryPar('factuurnr',null),tryPar('plaats'),tryPar('data'));
		return;
	}

	$data = tryPar('data');
	if(!($levnr = tryPar('leveranciernr'))) {
		print_warning("Geef een waarde voor Leverancier op.");
		levretourform(null,tryPar('factuurnr',null),tryPar('plaats'),tryPar('data'));
		return;
	}
	if(!($factuurnr = tryPar('factuurnr'))) {
		print_warning("Geef een waarde voor Factuurnr op.");
		levretourform(null,tryPar('factuurnr',null),tryPar('plaats'),tryPar('data'));
		return;
	}
	if(!($plaats = tryPar('plaats'))) {
		print_warning("Geef een waarde voor Plaats op.");
		levretourform(null,tryPar('factuurnr',null),tryPar('plaats'),tryPar('data'));
		return;
	}

	// het eerste geval van de hetzij omvormen tot het tweede
	if (tryPar('EAN')) {
		$data = requireEAN();
	} elseif( tryPar('allesinbvk') ) {
		global $BWDB;
		$result = $BWDB->q('COLUMN SELECT EAN FROM voorraad WHERE leveranciernr = %i AND buitenvk > 0', $levnr);
		$data = implode("\n", $result);
		$plaats = 'buitenvk';
	}

	$leveringen = parseEANdata($data);

	if (!is_array($leveringen)) {
		levretourform($levnr, $factuurnr, $plaats, $leveringen);
		return;
	}

	// sorteer op EAN, zelfde volgorde als pakbonnen (hoopt men)
	ksort($leveringen, SORT_NUMERIC);

	$offertes = sqlGetValidOffertesInfo(array_keys($leveringen));

	simpleForm('process');
	echo addHidden("leveranciernr", $levnr);
	echo addHidden("factuurnr", $factuurnr);
	echo addHidden("plaats", $plaats);
	foreach ($leveringen as $EAN => $gegevens) {
		echo "<tr><td colspan=2><h2>".print_EAN_bi($EAN)."</h2></td></tr>\n";
		echo "<tr><td colspan=2><h4>$gegevens[auteur]: $gegevens[titel] ".
			(@$gegevens['druk'] ? '('.$gegevens['druk'].')':'').
			"</h4></td></tr>\n";

		echo addHidden("EANs[]", $EAN);

		if (requirePar('page') == 'levering') {
			// <levering>

			@$offerte  = $offertes[$EAN];
			echo inputText('Leveranciersprijs', "levprijs$EAN",
				@$offerte['levprijs'], (@$offerte?'(offerte van '.print_date($offerte['offertedatum']).')':''));
			if (!$offerte) {
				echo '<tr><td colspan=2 class="waarschuwing">(Helaas is er geen (geldige) offerte
					van dit ISBN beschikbaar)</td></tr>';
			}
			echo inputText('Adviesprijs *', "adviesprijs$EAN");
			echo '<tr><td colspan=3>* Vul <b>alleen</b> een prijs in als je
			echt <i>die</i> prijs wilt, en niet het standaard-algoritme
			(leveranciersprijs + boekenbuffer) wilt gebruiken</td></tr>';
			echo '<tr><td>Bovenstaande prijs is:</td>'
				. '<td><input type="radio" name="incex'.$EAN.'" value=0 checked>exclusief btw</td>'
				. '<td><input type="radio" name="incex'.$EAN.'" value=1>inclusief btw</td></tr>';
			echo '<tr><td>BTW-tarief</td>'
				. '<td><select name="tarief'.$EAN.'"><option value=0>0%</option>'
				. '<option value=6 selected>6%</option>'
				. '<option value=21>21%</option></select></td></tr>';
			echo '<tr><td colspan=3>&nbsp;</td></tr>';

			//Bevat info over hoeveel boeken nog moeten komen voor een bestelling
			$bestellingen = sqlFindlevbestelnr($EAN, requirePar('leveranciernr'));
			foreach ($bestellingen as $bestelling) {
				echo inputText("$bestelling[aantal] besteld op ".
					print_date($bestelling['datum']).($bestelling['nognodig'] !=
					$bestelling['aantal']?", nog $bestelling[nognodig] nodig":""),
					"vanbestelling{$EAN}[$bestelling[levbestelnr]]");
			}

			echo inputText("Losse levering", "los$EAN");

			// </levering>
		} else {
			// <retour>

			$leveringen = sqlFindActiveStock($EAN,
				requirePar('leveranciernr'), $plaats);

			global $BOEKENPLAATSEN;
			if (sizeOf($leveringen) == 0) {
				$leverancierdata = sqlLeverancierData(requirePar('leveranciernr'));
				echo '</table>';
				print_error('Er zijn geen exemplaren van \''.$gegevens['titel']
					.'\' in \''.$BOEKENPLAATSEN[$plaats].'\' die geleverd '
					.'zijn door '.$leverancierdata['naam']);
			}

			foreach ($leveringen as $levering) {
				echo inputText("Nog ".$levering[$plaats].
					" stuk".($levering[$plaats] == 1 ? '': 's')." in <em>".
					$BOEKENPLAATSEN[$plaats]."</em> (".Money::addPrice($levering['levprijs']).
					" per stuk)", "levering{$EAN}[$levering[voorraadnr]]", $levering[$plaats], null,'5');
			}

			// </retour>
		}
	}
	addRedo();
	endForm('Voer in', 'Reset');
}

// verwerk de levering in de DB
function leveringprocess()
{
	global $request;

	checkRedo();

	$leveranciernr = requirePar('leveranciernr');
	$factuurnr     = requirePar('factuurnr');
	$plaats        = requirePar('plaats');

	// Hele levering wordt weergegeven als een tabel
	echo "<br/><span class=\"header3\">Verwerken nieuwe levering:</span><br/>";
	echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n" .
		"	<tr>\n" .
		"		<th>Artikelnr.</th>\n" .
		"		<th>Titel</th>\n" .
		"		<th>Auteur</th>\n" .
		"		<th>&nbsp;</th>\n" .
		"	</tr>\n";

	foreach ($request->get('EANs') as $EAN) {
		// array om alle mislukte leveringen in te stoppen
		$errorEANs = array();

		// Controleren of BookWeb dit EAN wel kent
		$artikelInfo = sqlBoekGegevens($EAN);
		$negativeTD = "<td><img src=\"/Layout/Images/Icons/negative.png\" alt=\"Fout!\" /></td>";
		$positiveTD = "<td><img src=\"/Layout/Images/Icons/positive.png\" alt=\"Goed!\" /></td>";

		echo "<tr>";
		if (!$artikelInfo){
			// Onbekend EAN/ISBN

			// EAN/ISBN neerzetten
			echo "<td>" . print_EAN($EAN) . "</td>";

			// Geen titel en auteur, maar foutmelding
			echo "<td colspan=\"2\">";
			print_warning("Het boek met ISBN " . print_EAN($EAN) . " is
			onbekend bij BookWeb. Alle leveringen van dit EAN
			worden overgeslagen");
			echo "</td>";

			// Rood kruis neerzetten
			echo $negativeTD;

			echo "</tr>\n";

			// Verder met volgende EAN
			$errorEANs[] = $EAN;
			continue;
		} else {
			// EAN/ISBN plus titel en auteur neerzetten
			echo "<td>" . print_EAN_bi($EAN) . "</td>";
			echo "<td>" . $artikelInfo["titel"] . "</td>";
			echo "<td>" . $artikelInfo["auteur"] . "</td>";

			//echo $positiveTD;
			echo "<td></td>";
			echo "</tr>\n";
		}

		$levprijs  = Money::checkMoney(tryPar("levprijs$EAN"));
		$adviesprijs = Money::checkMoney(tryPar("adviesprijs$EAN"), true);
		$btwtarief = tryPar("tarief$EAN");

		if ($levprijs === false || $adviesprijs === false){
			// Ongeldige levprijs of adviesprijs?
			echo "<tr>";

			// EAN/ISBN niet herhalen
			echo "<td></td>";

			echo "<td colspan=\"2\">";

			if ($levprijs === false){
				print_warning("De opgegeven leveranciersprijs (" .
				tryPar("levprijs$EAN") . ") is ongeldig. Alle leveringen van
				dit artikel worden overgeslagen");
			} else {
				print_warning("De opgegeven adviesprijs (" .
				tryPar("adviesprijs$EAN") . ") is ongeldig. Alle leveringen van
				dit artikel worden overgeslagen.");
			}

			echo "</td>";
			echo $negativeTD;
			echo "</tr>";

			// Volgende EAN
			continue;
		}

		// adviesprijs wijkt teveel af?
		if (isset($adviesprijs) && $adviesprijs) {

			if ($adviesprijs/$levprijs > 1.2) {
				echo "<tr><td></td><td colspan=\"2\">";
				print_warning("Fout: adviesprijs meer dan 20% hoger dan
					levprijs, mogelijk tikfout gemaakt. Alle leveringen van
	                dit artikel worden overgeslagen.");
				echo "</td>$negativeTD</tr>";
				continue;
			}
			if ($adviesprijs/$levprijs < 0.9) {
				echo "<tr><td></td><td colspan=\"2\">";
				print_warning("Fout: adviesprijs meer dan 10% lager dan
					levprijs, mogelijk tikfout gemaakt. Alle leveringen van
                    dit artikel worden overgeslagen.");
				echo "</td>$negativeTD</tr>";
				continue;
			}
		}

		if (!$adviesprijs) {
			$adviesprijs = NULL;
			if(tryPar("incex$EAN") == 0) {
				$btw = round($levprijs*$btwtarief/100,2);
				$levprijs += $btw;
			} else {
				$btw = round($levprijs/(1+$btwtarief/100)*$btwtarief/100,2);
			}
		} else {
			if(tryPar("incex$EAN") == 0) {
				$btw = round($adviesprijs*$btwtarief/100,2);
				$levprijs += $btw;
			} else {
				$btw = round($adviesprijs/(1+$btwtarief/100)*$btwtarief/100,2);
			}
		}

		$verdeling = @$request->get("vanbestelling$EAN");
		if (!$verdeling) {
			// kan als er geen enkele bestelling openstond
			$verdeling = array();
		}
		$los       = (int)tryPar("los$EAN");
		$aantal = array_sum($verdeling) + $los;

		if (!$aantal){
			// Totaalaantal is leeg/nul?
			echo "<tr><td></td>";

			echo "<td colspan=\"2\">";
			print_warning("Er zijn geen aantallen ingevoerd om te leveren.
			Leveringen van deze titel worden niet opgeslagen.");
			echo "</td>";
			echo $negativeTD; // rood kruis

			$errorEANs[] = $EAN;
			continue;
		}

		foreach ($verdeling as $bestelnr => $subaantal) {
			$subaantal = (int)$subaantal;
			if ($subaantal === 0) continue;

			// Nieuwe rij. Niet EAN herhalen
			echo "<tr><td></td>";

			if ($subaantal<0) {
				// Subaantal te leveren (op bestelling) is niet correct.
				// Foutmelding neerzetten in kolommen voor titel, auteur
				echo "<td colspan=\"2\">";
				print_warning("Aantal exemplaren in de levering van "
				. print_EAN_bi($EAN)	. " ($subaantal) kan niet negatief
				zijn. Deze (deel)levering is niet opgeslagen.");
				echo "</td>";
				echo $negativeTD; // rood kruis

				$errorEANs[] = $EAN;
			} elseif (!sqlInsLevering($EAN
						  ,$plaats
						  ,$subaantal
						  ,$levprijs
						  ,$adviesprijs
						  ,$btwtarief
						  ,$btw
						  ,$factuurnr
						  ,$leveranciernr
						  ,$bestelnr)) {

				// Foutmelding neerzetten in kolommen voor titel en auteur
				echo "<td colspan=\"2\">";
				print_warning("Kan geen $subaantal leveren bij een bestelling
					van " . print_EAN_bi($EAN) . ", zoveel waren er niet
					meer in bestelling! Er zijn geen exemplaren van deze
					(deel)levering opgeslagen in BookWeb");
				echo "</td>";
				echo $negativeTD;

				$errorEANs[] = $EAN;
			} else {
				// Levering goed ingevoerd!

				// Titel en auteur-kolom gebruiken voor leveringsinfo
				echo "<td colspan=\"2\" style=\"color: green\">";
				echo "(Deel)levering van $subaantal bestelde exemplaren
				opgeslagen";
				echo "</td>";
				echo $positiveTD;
			}
			echo "<tr>\n";
		}

		if ($los<0) {
			echo "<td colspan=\"2\">";
			print_warning("Het aantal los geleverde exemplaren van " .
				print_EAN_bi($EAN) . " ($los) mag niet negatief zijn.
				Deze losse levering is niet opgeslagen.");
			echo "</td>";
			echo $negativeTD;

			$errorEANs[] = $EAN;
		}
		if ($los) {
			if (sqlInsLevering($EAN
						  ,$plaats
						  ,$los
						  ,$levprijs
						  ,$adviesprijs
						  ,$btwtarief
						  ,$btw
						  ,$factuurnr
						  ,$leveranciernr)){
				// Geen EAN/ISBN herhalen
				echo "<td></td>";

				echo "<td colspan=\"2\" style=\"color: green\">";
				echo "Levering van $los losse exemplaren ingevoerd.";
				echo "</td>";
				echo $positiveTD;
			}
		}
		processTo4(array($EAN));
	}
	echo "</table>";

	printHTML('De leveringen die gemarkeerd zijn met een groene checkmark
	zijn nu geregistreerd!');

	printHTML(makeBookRef('levering', 'Nog een levering invoeren'));
}

// verwerk de retour in de DB
function retourprocess()
{
	global $request;
	checkRedo();

	$leveranciernr = requirePar('leveranciernr');
	$factuurnr     = requirePar('factuurnr');
	$plaats        = requirePar('plaats');

	foreach ($request->get('EANs') as $EAN) {
		$verdeling = @$request->get("levering$EAN");
		if (!$verdeling) {
			// Je stuurt bij nader inzien niks terug
			continue;
		}
		$aantal = array_sum($verdeling);

		// TODO: maak hier een bevestigingsformpje van, als extra check
		// maak het minder foutgevoelig (#834)

		foreach ($verdeling as $voorraadnr => $subaantal) {
			if (!$subaantal) continue;
			if (!sqlInsRetour($voorraadnr
						  ,$plaats
						  ,$subaantal
						  ,$factuurnr)) {
				print_error("Je wilt van voorraaditem $voorraadnr meer artikelen (nl.
				$subaantal) retourzenden dan er zijn! (kopieer deze hele zin
				in de bug die je zo gaat reporten)");
			}
		}
		printHTML("In totaal $aantal exemplaren ".print_EAN_bi($EAN)."
			teruggestuurd.");
	}

	printHTML('De retouren zijn nu geregistreerd.');

	printHTML(makeBookRef('retour', 'Nog een retour invoeren'));
}

// kan ook gebruikt worden voor een retour, is nl. exact hetzelfde
function levretourform($lev=null, $fact=null, $plaats=null, $data=null)
{
	global $BOEKENPLAATSEN;

	$what = requirePar('page');
	//Hier het invulformulier om leveringen/retouren in te voeren.
	printHTML("<h2>".ucfirst($what)." invoeren</h2>");
	if ($lev) {
		printHTML('Controleer wat je hebt ingevuld, en corrigeer het '
			.'eventueel (de &gt; regels kun je negeren), en voer dan opnieuw '
			.'in.', TRUE);
	}

	startForm($what,'/Onderwijs/Boeken/' . ucfirst($what), '', '', 'go', '');
		echo '<TR><TD>Leverancier:</TD><TD>'.
			getLeveranciersSelect($lev) ."</TD></TR>\n";
		echo inputText('Factuurnr', 'factuurnr', $fact);
		echo '<TR><TD>'.($what=='levering'?'Naar':'Van').' plaats:</TD><TD>'
			.addSelect('plaats',$BOEKENPLAATSEN, $plaats, true)
			."</TD></TR>\n";

	if (!$lev) {
		$EAN = "";
		/*if (!$data) */$EAN = tryPar("EAN", "");

		echo inputText('<b>Hetzij</b>, EAN, ISBN, Titel of Auteur', 'EAN', $EAN);
		if($what == 'retour') {
			echo '<tr><td>'.
				'<b>Hetzij</b>, <label for="allesinbvk">alles in buiten verkoop</label>:</td><td>'.
				addCheckBox('allesinbvk', FALSE, '1') .
				"(van deze leverancier)</td></tr>\n";
		}
	}


	echo "<tr><td colspan=2>".($lev?"V":"<b>Hetzij</b>, v")."ul per regel een
		EAN of ISBN van dat artikel in. Lege regels en regels die met een &gt; of
		#-symbool beginnen, worden genegeerd.  Het is natuurlijk niet
		noodzakelijk om alles in &eacute;&eacute;n keer in te voeren, deze
		feature loopt niet weg.</td><td></td></tr>\n";
	echo "<tr><td colspan=2>".addTextArea('data', $data, 60, 20, 'virtual')
		."</td><td></td></tr>";
	endForm('Voer in');
}

// parst leverdata. Geeft het parselog terug als het parselog niet identiek is
// aan de invoer (of fouten bevat), anders een array {EAN => EANdata}
function parseEANdata($data)
{
	if ($data == '') {
		return array();
	}
	$data = str_replace("\r", "", $data);
	$regels = explode("\n", $data);
	$nieuwregels = array();
	$fouten = $titels = $boeken = 0;
	$result = array();
	foreach ($regels as $regel) {
		$regel = trim($regel);
		if ( $regel == "" || $regel{0} == '>' || $regel{0} == '#')
			continue;

		$nieuwregels[] = '';
		$nieuwregels[] = "$regel";
		if(preg_match("/Extern\-?(\d+)/i", $regel, $enum)) {
			$EAN = ean_addcheck('22'.sprintf('%010s',$enum[1]));
		} elseif( preg_match("/A\-?(\d+)/i", $regel, $anum)) {
			$EAN = ean_addcheck('21'.sprintf('%010s',$anum[1]));
		} else {
			$EAN = strtoupper(str_replace('-', '', $regel));
			if (is_isbn($EAN)) $EAN = isbn_to_ean($EAN);
		}
		if (!($boekgegevens = sqlBoekGegevens($EAN))) {
			$nieuwregels[] = "> FOUT: EAN/ISBN '$EAN' onbekend bij bookweb";
			$fouten++;
			continue;
		}
		if (isset($result[$EAN])) {
			$nieuwregels[] = "> FOUT: Dit artikel is al genoemd";
			$fouten++;
			continue;
		}

		$nieuwregels[] = "> OK: ".print_EAN($EAN);
		$nieuwregels[] = ">         Titel:  $boekgegevens[titel]";
		$nieuwregels[] = ">         Auteur: $boekgegevens[auteur]";
		$nieuwregels[] = ">         Druk:   $boekgegevens[druk]";

		$result[$EAN] = $boekgegevens;

		$titels++;
	}

	$nieuwdata  = "> $fouten fouten, $titels titels\n";
	$nieuwdata .= implode("\n", $nieuwregels). "\n";

	if ($fouten || $data != $nieuwdata) {
		return $nieuwdata;
	}

	return $result;
}

