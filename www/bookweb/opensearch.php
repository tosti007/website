<?php
global $request;
if (strpos($request->server->get("REQUEST_URI"), "/Onderwijs/Boeken/opensearch.php") !== 0){
	user_error("De file opensearch.php kan niet gebruikt worden vanuit een vorm van dispatch!");
} else {
	require_once("init.php");
	require_once("booksql.php");

	requireAuth("verkoper");

	$searchstr = requirePar( "suggest" );

	header( 'Content-type: application/x-suggestions+json' );

	/* is dit misschien een van onze interne dingen:
	 * Extern-123 of A-Es2-456 ? */
	if(preg_match("/Extern\-?(\d+)/i", $searchstr, $enum)) {
		$boekdata['EAN'] = ean_addcheck('22'.sprintf('%010s',$enum[1]));
	} elseif( preg_match("/A\-?Es.\-?(\d+)/i", $searchstr, $anum)) {
		$boekdata['EAN'] = ean_addcheck('21'.sprintf('%010s',$anum[1]));
	} else {
		$boekdata['EAN'] = $searchstr;
	}
	$boekdata['titel'] = $searchstr;
	$boekdata['auteur'] = $searchstr;
	$boeken = sqlAlikeBoeken($boekdata);

	$output = "[\"" . addslashes( $searchstr ) . "\",[";

	foreach ( $boeken as $boek ) {
	       $output .= "\"" . addslashes( $boek['titel'] ) . "\",";
	}

	$output = substr( $output, 0, -1 ) . "]]";

	echo $output;
}
