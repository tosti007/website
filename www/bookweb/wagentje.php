<?php

/**
 * Functies voor het omgaan met de shoppingkart
 * Dit zou ook wel een class kunnen zijn. Of niet juist.
 * $Id$
 */

function bw_initKart() {
	global $session;
	$session->set('wagentje', array());
}

function bw_dropFromKart($voorraadnr) {
	global $session;
	$wagentje = $session->get('wagentje');
	unset($wagentje[$voorraadnr]);
	$session->set('wagentje', $wagentje);
}

/**
	Retourneert het aantal artikelen met het gegeven voorraadnummer
	in de kart. Bij voorraadlocatie == null doet de voorraadlocatie
	van de items in de kart er niet toe.

	Let op het specifieke selecteren van de voorraadlocatie!
	Als er gevraagd wordt om een aantal van locatie 'voorraad', dan
	tellen producten in de kart uit locatie 'ejbv_voorraad' dus niet mee!
**/
function bw_checkKart($voorraadnr, $voorraadlocatie = null) {
	global $session;

	if ($voorraadlocatie) Voorraad::isGeldigeVoorraadLocatie($voorraadlocatie);

	if($session->has('wagentje/'.$voorraadnr)) {
		if ($session->get('wagentje/'.$voorraadnr.'/plaats') == $voorraadlocatie || $voorraadlocatie == null) {
			return $session->get('wagentje/'.$voorraadnr.'/aantal');
		} // else: artikel wel in kart, maar vanuit andere voorraadlocatie
	}
	return 0;
}

/**
	Voegt een artikel toe aan de BookWeb-kart:
	$voorr = voorraadnummer (bij terugkoop met prefix 'tk', bijv 'tk123' voor een terugkoop van verkoopnr 123)
	$descr = omschrijving
	$num = aantal artikelen
	$price = prijs waarvoor ze verkocht worden
	$bestelnrs = eventueel bestelnummer van een studentbestelling
	$plaats = bij verkoop: voorraadlocatie waarvan verkocht wordt ('voorraad', 'ejbv_voorraad'),
		bij terugkoop: voorraadlocatie waar het artikel wordt teruggelegd ('voorraad', 'ejbv_voorraad', 'buitenvk')
*/
function bw_addToKart($voorr, $descr, $num, $price, $btwtarief, $btw, $sbestelnrs = null, $plaats = null) {
	global $session;

	// komt deze EAN/prijs combi al voor, dan tellen we alleen $num bij aantal op
	if($session->has('wagentje/'.$voorr)) {
		if ($session->get('wagentje/'.$voorr.'/plaats') != $plaats) {
			// Kan niet hetzelde artikel meerdere malen aan kart toevoegen
			// als het van meerdere voorraadlocaties afkomstig is!
			user_error("Je probeert een product in je kart te stoppen dat reeds
			in je kart zit, maar vanaf een andere voorraadlocatie. Dit is niet
			toegestaan!", E_USER_ERROR);
		}

		// Aantal ophogen
		$session->set('wagentje/'.$voorr.'/aantal', $session->get('wagentje/'.$voorr.'/aantal') + $num);

		// Eventueel studentbestelnummer toevoegen aan bestaande array
		if($sbestelnrs){
			$session->set('wagentje/'.$voorr.'/sbnrs',
				array_merge($session->get('wagentje/'.$voorr.'/sbnrs'), $sbestelnrs));
		}

		return;
	}

	if(!$sbestelnrs) $sbestelnrs = array();

	// anders een nieuw item.
	$session->set('wagentje/'.$voorr, array(
		'omschrijving' => $descr,
		'aantal' => $num,
		'prijs' => $price,
		'btwtarief' => $btwtarief,
		'btw' => $btw,
		'sbnrs' => $sbestelnrs,
		'plaats' => $plaats
	));
}

function bw_getKart() {
	global $session;
	if (!$session->has('wagentje') || is_null($session->get('wagentje'))) {
		bw_initKart();
	}
	return $session->get('wagentje');
}

function bw_toggleBTWKart() {
	global $session;
	if (!$session->has('wagentjeuseBTW'))
		$session->set('wagentjeuseBTW', false);
	else
		$session->set('wagentjeuseBTW', !($session->get('wagentjeuseBTW')));
}
