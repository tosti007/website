<?php
/**
	Klasse waarin alles gebeurt m.b.t. statistieken in bookweb. Er
	staan voornamelijk statische methoden in die data retourneren van
	een bepaald gebied.
*/

class BookwebStatistieken
{
	/**
		Deze functie print het statistiekenmenu naar stdout en roept alle
		functies aan die de gebruiker wenst te zien.
	*/
	static function dispatch()
	{
		$statstypes = array(
			""=>"",
			"vergelijkboekjaren"=>"Boekjaren vergelijken (omzet)",
			"vergelijkboekjarenboeken"=>"Boekjaren vergelijken (aantal verkochte boeken)",
			"vergelijkboekjarencuml"=>"Boekjaren vergelijken (cumulatieve omzet)",
			"vergelijkboekjarencumlboeken"=>"Boekjaren vergelijken (cumulatief aantal verkochte boeken)",
			"vergelijkperiodes"=>"Periodes vergelijken (omzet)",
			"vergelijkperiodesboeken"=>"Periodes vergelijken (aantal verkochte boeken)",
			"gemiddeldeprijs" => "Gemiddelde prijs per boek"
		);
		$statstype = tryPar("statstype", null);
		$boekjaren = tryPar("boekjaren", array());
		$boekjarenStr = implode(",", $boekjaren);

		$leveranciers = tryPar("leveranciers", array(BOEKLEVNR));
		$leveranciersStr = implode(",", $leveranciers);

		echo "<h3>BookWeb Statistieken</h3>\n";
		echo "Welkom bij BookWeb Statistieken! Wat wil je graag
		weten?<br/><br/>\n";
		echo addForm("", "POST");
		echo addHidden("page", "stats");

		echo "<table width=\"100%\">\n";
		echo "	<tr>\n";
		echo "		<td align=\"center\" colspan=\"2\" style=\"border-bottom: double black\">\n";
		echo "Type statistieken:<br/>\n";
		echo addSelect("statstype", $statstypes, $statstype, true, true);
		echo "		</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td width=\"50%\" align=\"center\" valign=\"top\" style=\"border-right: 1px solid black\">\n";
		// Welke boekjaren?
		BookwebStatistieken::printSubformBoekjaren();
		echo "		</td>\n";
		echo "		<td width=\"50%\" align=\"center\" valign=\"top\">\n";
		// Welke leveranciers?
		BookwebStatistieken::printSubformLeveranciers();
		echo "		</td>\n";

		echo "	<tr>\n";
		echo "		<td colspan=\"2\" align=\"center\" style=\"border-top: 1px solid black\">";
		echo "<input type=\"submit\" value=\"Doen!\">";
		echo "</td>\n";
		echo "	</tr>\n";
		echo "</table>\n";
		echo "</form>\n";

		if ($statstype != null) echo "<hr>\n";
		if ($statstype == "vergelijkboekjaren"){
			if (sizeof($boekjaren) > 0){
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=boekjaarvergelijking&boekjaren=$boekjarenStr&leveranciers=$leveranciersStr\"
				/>";
			}
		} elseif ($statstype == "vergelijkboekjarencuml"){
			if (sizeof($boekjaren) > 0){
				$boekjarenStr = implode(",", $boekjaren);
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=boekjaarvergelijkingcuml&boekjaren=$boekjarenStr&leveranciers=$leveranciersStr\"
				/>";
				echo "<br/><br/>";
				echo "Totaalomzetten:<br/>".
					"<table class=\"bw_datatable_medium\">" .
						"<tr><th>Boekjaar</th><th>Omzet totaal</th></tr>";

				$jaaromzetten = BookwebStatistieken::computeOmzet("jaar", $boekjaren, $leveranciers);
				foreach ($jaaromzetten as $jaar => $bedrag){
					echo "<tr>" .
						"<td>$jaar-" . ($jaar + 1) . "</td>" .
						"<td>" . Money::addPrice($bedrag) . "</td>" .
						"</tr>";
				}
				echo "</table>";
			}
		} elseif ($statstype == "vergelijkperiodes"){
			if (sizeof($boekjaren) > 0){
				$boekjaren = implode(",", $boekjaren);
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=periodevergelijking&boekjaren=$boekjaren&leveranciers=$leveranciersStr\"
				/>";
			}
		} elseif ($statstype == "vergelijkboekjarenboeken"){
			if (sizeof($boekjaren) > 0){
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=boekjaarvergelijkingboeken&boekjaren=$boekjarenStr&leveranciers=$leveranciersStr\"
				/>";
			}
		} elseif ($statstype == "vergelijkboekjarencumlboeken"){
			if (sizeof($boekjaren) > 0){
				$boekjarenStr = implode(",", $boekjaren);
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=boekjaarvergelijkingcumlboeken&boekjaren=$boekjarenStr&leveranciers=$leveranciersStr\"
				/>";
				echo "<br/><br/>";
				echo "Totaalomzetten:<br/>".
					"<table class=\"bw_datatable_medium\">" .
						"<tr><th>Boekjaar</th><th>Omzet totaal</th></tr>";

				$jaaromzetten = BookwebStatistieken::computeOmzet("jaar", $boekjaren, $leveranciers, false);
				foreach ($jaaromzetten as $jaar => $bedrag){
					echo "<tr>" .
						"<td>$jaar-" . ($jaar + 1) . "</td>" .
						"<td>" . Money::addPrice($bedrag) . "</td>" .
						"</tr>";
				}
				echo "</table>";
			}
		} elseif ($statstype == "vergelijkperiodesboeken"){
			if (sizeof($boekjaren) > 0){
				$boekjaren = implode(",", $boekjaren);
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=periodevergelijkingboeken&boekjaren=$boekjaren&leveranciers=$leveranciersStr\"
				/>";
			}
		} elseif ($statstype == "gemiddeldeprijs"){
			if (sizeof($boekjaren) > 0){
				$boekjaren = implode(",", $boekjaren);
				echo "<img
				src=\"/Onderwijs/Boeken/statsimages.php?imagetype=gemiddeldeprijs&boekjaren=$boekjaren&leveranciers=$leveranciersStr\"
				/>";
			}
		}
	}


	static function printSubformBoekjaren(){
		$dezeMaand = date("n");
		$ditKalenderjaar = date("Y");

		if ($dezeMaand > 8){
			// Maand is bijv. oktober in het kalenderjaar 2005. Dan is het
			// boekjaar ook 2005.
			$ditBoekjaar = $ditKalenderjaar;
		} else {
			$ditBoekjaar = $ditKalenderjaar - 1;
		}

		$boekjaren = tryPar("boekjaren", $ditBoekjaar);


		for ($jaarcounter = 2001; $jaarcounter <= $ditBoekjaar;	$jaarcounter++){
			$ditBoekjaarTitle = $jaarcounter . "-" . ($jaarcounter + 1);
			$mogelijkeBoekjaren[$jaarcounter] = $ditBoekjaarTitle;
		}
		echo "Boekjaren:<br/>\n";
		echo addSelect("boekjaren[]", $mogelijkeBoekjaren, $boekjaren, true,
		false, true);
	}

	static function printSubformLeveranciers(){
		$leveranciers = tryPar("leveranciers", array(BOEKLEVNR));

		$alleLeveranciers = sqlLeveranciers();
		foreach ($alleLeveranciers as $thisLeverancier){
			$leverancierNamen[$thisLeverancier["leveranciernr"]] = $thisLeverancier["naam"];
		}

		echo "Leveranciers:<br/>\n";
		echo addSelect("leveranciers[]", $leverancierNamen,
		$leveranciers, true, false, true);

	}

	static function artikelenOffertes($eans){
		global $BWDB;

		$data = $BWDB->q("TABLE SELECT
			EAN, levprijs, offertedatum
			FROM offerte
			WHERE EAN IN(%As)",
			$eans);

		$res = array();
		foreach ($data as $row){
			$EAN = $row["EAN"];
			$levprijs = $row["levprijs"];
			$offertedatum = $row["offertedatum"];

			$res["$EAN"]["$offertedatum"] = $levprijs;
		}

		return $res;
	}
	/**
		Deze functie retournert een array met de volgende opzet:
		array[jaar][resolutie] = omzet

		waarbij de parameter $resolutie de inhoud van de arraykey
		"resolutie" bepaald. Mogelijk waarden voor $resolutie:
		- jaar
		- maand
		- dag

		in de parameter $jaren wordt een array verwacht van welke
		jaren informatie geretourneerd moet worden. Het jaar 2005 staat
		voor het _boekjaar_ 2005-2006. Dus 2005-08-01 tot 2006-08-01

		$leverancierNrs is een array van leveranciernummers waarvan
		de omzet berekend moet worden. Als deze parameter null is
		(default) dan wordt de omzet van alle leveranciers bij
		elkaar berekend.

	*/
	static function computeOmzet($resolutie, $jaren, $leverancierNrs = null, $type = 1){
		global $BWDB;

		$res = array();

		// Elk van de jaren doorlopen en een query per jaar uitvoeren
		foreach ($jaren as $jaar){
			$jaarres = array();
			$where = array();

			if (!is_numeric($jaar)){
				BookwebBase::addError(new BookwebError("Het opgegeven jaar ($jaar) is ongeldig!"));
				return false;
			}

			$jaarbegin = $jaar;
			$jaareinde = $jaar + 1;

			$where[] = "wanneer >= \"$jaarbegin-08-01\"";
			$where[] = "wanneer <= \"$jaareinde-08-01\"";
                        $where[] = "NOT EXISTS (SELECT * FROM terugkoop AS t WHERE t.verkoopnr = v.verkoopnr)";
			if ($leverancierNrs != null){
				$where[] = "leveranciernr IN (" .
				implode(",",$leverancierNrs) . ")";

				/*foreach ($leverancierNrs as $levnr){
					if (!is_numeric($levnr)){
						BookwebBase::addError(new BookwebError("Het opgegeven leveranciernr ($levnr) is	ongeldig!"));
						return false;
					}

					$where[] = "leveranciernr = $levnr";
				}*/
			}

			if ($resolutie == "jaar"){
				$wherestring = implode(" AND ", $where);
				$jaarres = $BWDB->q("
					VALUE SELECT SUM(aantal" . ($type != 2 ? "* studprijs" : "") . ") as resultaat
					FROM verkoop AS v
					LEFT JOIN voorraad USING(voorraadnr)
					WHERE $wherestring");
			} elseif ($resolutie == "maand"){
				$wherestring = implode(" AND ", $where);

				$alles = $BWDB->q("
					TABLE SELECT 	SUBSTRING(wanneer, 1, 7) as maand,
							SUM(aantal" . ($type != 2 ? "* studprijs" : "") . ") as resultaat
					FROM verkoop AS v
					LEFT JOIN voorraad USING(voorraadnr)
					WHERE $wherestring
					GROUP BY maand
					ORDER BY maand");

				foreach ($alles as $row){
					$jaarres[$row["maand"]] = $row["resultaat"];
				}
			} elseif ($resolutie == "dag"){
				$wherestring = implode(" AND ", $where);
				$alles = $BWDB->q("
					TABLE SELECT 	SUBSTRING(wanneer, 1, 10) as dag,
							SUM(aantal" . ($type != 2 ? "* studprijs" : "") . ") as resultaat
					FROM verkoop AS v
					LEFT JOIN voorraad USING(voorraadnr)
					WHERE $wherestring
					GROUP BY dag
					ORDER BY dag");

				foreach ($alles as $row){
					$jaarres[substr($row["dag"], 5)] = $row["resultaat"];
				}
			} elseif ($resolutie == "periode"){
				// Mysql heeft geen weet van collegeperiodes: zelf in PHP
				// doen en losse queries uitvoeren
				global $PERIODES;
				$wherestring = implode(" AND ", $where);

				if (isset($PERIODES[$jaar])){
					// Van dit jaar is periodeinfo beschikbaar
					$jaarPeriodes = $PERIODES[$jaar];

					foreach ($jaarPeriodes as $periodeNr=>$periodeStart){
						// Alle periodes doorlopen...
						if ($periodeNr < 4){
							$periodeEind = $jaarPeriodes[$periodeNr + 1];
						} else {
							$periodeEind = $PERIODES[$jaar + 1][1];
						}

						$periodeRes = $BWDB->q("
							VALUE SELECT SUM(aantal" . ($type != 2 ? "* studprijs" : "") . ")
							FROM verkoop AS v
							LEFT JOIN voorraad USING(voorraadnr)
							WHERE $wherestring AND
							wanneer >= FROM_DAYS(TO_DAYS(%s)-7) AND
							wanneer <= FROM_DAYS(TO_DAYS(%s)-7)",
							$periodeStart, $periodeEind);

						$jaarres[$periodeNr] = $periodeRes;
					}
				} else {
					$jaarres = 0;
				}
			} else {
				BookwebBase::addError(new BookwebError("De
				opgegeven resolutie ($resolutie) in
				BookwebStatistieken::computeJaarOmzet(...)
				is ongeldig"));

				return false;
			}

			$res[$jaar] = $jaarres;

		}

		return $res;

	}

	/*
		Retourneert een array:
		array (
			EAN1 => array (
				datum1 => aantalverkocht,
				datum2 => aantalverkocht,
				datum3 => aantalverkocht
				),
			EAN2 => array (...)
			);
	*/
	static function getVerkopen($EANs){
		global $BWDB;

		$data = $BWDB->q("
			TABLE SELECT
				voorraad.EAN, SUM(verkoop.aantal) as aantal, DATE(verkoop.wanneer) as datum
			FROM voorraad
			LEFT JOIN verkoop USING(voorraadnr)
			WHERE voorraad.EAN IN (%As)
			GROUP BY voorraad.EAN, datum
			ORDER BY datum", $EANs);

		$res = array();
		foreach ($data as $row){
			$EAN = $row["EAN"];
			$datum = $row["datum"];
			$aantal = $row["aantal"];

			$res["$EAN"]["$datum"] = $aantal;
		}

		return $res;
	}
}

