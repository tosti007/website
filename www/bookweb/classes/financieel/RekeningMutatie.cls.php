<?php

/**
	Klasse die een rekening-mutatie representeert
*/
class RekeningMutatie extends FinancieelBase {
	var $_mutatienr = -1; // Primary key
	var $_afschriftnr = -1; // Foreign key
	var $_boekdatum;
	var $_bedrag;
	var $_omschrijving;
	var $_tegenrekening;
	var $_transactieType;

	function __construct($mutatienr = -1){
		if ($mutatienr >= 0){
			$this->_mutatienr = $mutatienr;
			$this->update();
		} else {
			$this->_mutatienr = -1;
		}
	}

	/**
		Set-functie voor alle mogelijke velden
		 afschrift_volgnr -- Het volgnummer van het afschrift waarvan de
		 mutatie afkomstig is
		 afschrift_datum -- De datum van het afschrift
		 rekeningnr -- De rekening waarop het afschrift betrekking heeft
		 boekdatum -- De datum waarop de mutatie heeft plaatsgevonden
		 bedrag -- Het bedrag van de mutatie (negatief is een afschrijving,
		 positief is een bijschrijving)
	**/
	function setValues($afschriftnr, $boekdatum, $bedrag, $omschrijving, $tegenrekening, $transactieType){
		$res = true;
		$res = $this->setAfschriftNr($afschriftnr);
		$res = $this->setBoekdatum($boekdatum) && $res;
		$res = $this->setBedrag($bedrag) && $res;
		$res = $this->setOmschrijving($omschrijving) && $res;

		if ($transactieType != "chip" && $transactieType != "pin"){
			$res = $this->setTegenrekening($tegenrekening) && $res;
		}

		$res = $this->setTransactieType($transactieType) && $res;

		return $res;
	}

	function deleteFromDatabase(){
		$nr = $this->getNr();

		if (is_numeric($nr) && $nr >= 0){
			global $BWDB;
			$BWDB->q("
				DELETE FROM fin_rekeningmutaties
				WHERE mutatienr = %i",
				$nr);
		}

		return true;
	}

	/**
		accepteert: "pin", "chip", NULL
	*/
	function setTransactieType($type){
		if ($type != "pin" && $type != "chip" && $type != null){
			BookwebBase::addError(new BookwebError("Het opgegeven
			transactietype ($type) in
			RekeningMutatie->setTransactietype(...) is ongeldig"));

			return false;
		} elseif ($type == "pin" || $type == "chip"){
			$this->setTegenrekening(null);
		}

		$this->_transactieType = $type;

		return true;
	}

	function isChipTransactie(){
		return ($this->getTransactieType() == "chip");
	}

	function isPinTransactie(){
		return ($this->getTransactieType() == "pin");
	}

	function getTransactieType(){
		return $this->_transactieType;
	}

	function setAfschrift($afschrift){
		if ($afschrift instanceof RekeningAfschrift){
			$this->_afschriftnr = $afschrift->getNr();
		} else {
			BookwebBase::addError(new BookwebError("Het opgegeven object is
			geen geldig afschrift-object"));
			return false;
		}

		return true;
	}

	function setAfschriftNr($afschriftnr){
		if (is_numeric($afschriftnr)){
			$this->_afschriftnr = $afschriftnr;
		} else {
			BookwebBase::addError(new BookwebError("Het opgegeven afschriftnr
			($afschriftnr) is ongeldig"));
			return false;
		}

		return true;
	}


	function update(){
		if ($this->getNr() < 0){
			BookwebBase::addError(new BookwebError("De functie RekeningMutatie->update() kan
			niet aangeroepen worden als het object zich niet in de database
			bevindt"));
			return false;
		}
		global $BWDB;

		$row = $BWDB->q("
			MAYBETUPLE SELECT *
			FROM fin_rekeningmutaties
			WHERE mutatienr=%i", $this->getNr());

		$res = $this->setValues(
			$row["afschriftnr"], $row["boekdatum"],	$row["bedrag"],
			$row["omschrijving"], $row["tegenrekening"], $row["transactietype"]);

		if (!$res){
			parent::printErrors();
		}
	}

	function setOmschrijving($omschr){
		$this->_omschrijving = $omschr;
		return true;
	}

	function getOmschrijving(){
		return $this->_omschrijving;
	}

	function getTegenrekening(){
		return $this->_tegenrekening;
	}

	/**
		Retourneert het RekeningAfschrift-object waar deze
		mutatie bij hoort
	*/
	function getAfschrift(){
		$afschrifnr = $this->getAfschriftNr();

		return new RekeningAfschrift($afschriftnr);
	}

	function setTegenrekening($tegenrekening){
		$tegenrekening = trim($tegenrekening);

		if (!is_numeric($tegenrekening) && !$tegenrekening == null){
			parent::addError(new BookwebError("Het opgegeven
			tegenrekeningnummer ($tegenrekening) is niet geldig"));
			return false;
		} elseif ($tegenrekening === 0){
			$this->_tegenrekening = null;
		} else {
			$this->_tegenrekening = $tegenrekening;
		}

		return true;
	}

	function setBoekdatum($datum){
		$parsedDate = parseDate($datum);

		if ($parsedDate === false){
			// fout
			BookwebBase::addError(new BookwebError("De opgegeven boekdatum
			($datum) is ongeldig."));
			return false;
		} else {
			$this->_boekdatum = $parsedDate;
		}

		return true;
	}

	function setBedrag($bedrag){
		$newbedrag = str_replace(",", ".", $bedrag);

		if (!is_numeric($newbedrag)){
			BookwebBase::addError(
				new BookwebError("Het opgegeven bedrag ($bedrag) is
				ongeldig!")
			);
			return false;
		} else {
			$this->_bedrag = $bedrag;
			return true;
		}
	}


	/**
		Controleert of dit object geschikt is om op te slaan in de
		database. Als dat niet zo is, dan worden de errors opgeslagen en
		wordt er 'false' geretourneerd. Is het object wel geschikt, dan
		return true
	**/
	function validate(){
		//TODO schrijven
		return true;
	}

	/**
		Slaat dit object op in de database. Als de primary key != -1, dan
		wordt er een "update"-query gedaan, anders een insert.
	*/
	function store(){
		global $BWDB;

		$primkey = $this->getId();

		// Controleren of dit object geschikt is om op te slaan. Zo niet:
		// return false. De functie validate() is verantwoordelijk voor het
		// opslaan van errors
		if (!$this->validate()){
			return false;
		}
		if ($primkey >= 0){
			// UPDATE
			$BWDB->q("
					UPDATE fin_rekeningmutaties
					SET boekdatum=%s, bedrag=%s, omschrijving=%s,
					tegenrekening=%s, transactietype=%s,
					debcrednr=-1
					WHERE mutatienr=%i",
				$this->getBoekdatum(), $this->getBedrag(),
				$this->getOmschrijving(), $this->getTegenrekening(),
				$this->getTransactieType(), $this->getNr());
		} else {
			// INSERT
			$this->_mutatienr = $BWDB->q("
					RETURNID INSERT INTO fin_rekeningmutaties
					SET	afschriftnr=%i, boekdatum=%s, bedrag=%s,
					omschrijving=%s, tegenrekening=%s, transactietype=%s, debcrednr=-1",
					$this->getAfschriftNr(), $this->getBoekdatum(),
					$this->getBedrag(), $this->getOmschrijving(),
					$this->getTegenrekening(), $this->getTransactieType());
		}

		return true;
	}

	function getAfschriftNr(){
		return $this->_afschriftnr;
	}

	function getId(){
		return $this->getMutatieNr();
	}

	function getNr(){
		return $this->getMutatieNr();
	}

	function getMutatieNr(){
		return $this->_mutatienr;
	}


	function getBedrag(){
		return $this->_bedrag;
	}

	function getBoekdatum(){
		return $this->_boekdatum;
	}



	/*
		Maakt een lijst met rekeningmutaties, met een kleine
		selectiemogelijkheid
	*/
	function listRekeningMutaties(){
		global $BWDB;

		$listselect_start = parseDate(tryPar("listselect_start", false));
		$listselect_end = parseDate(tryPar("listselect_end", false));
		$listmarkup_afrekening = tryPar("listmarkup_afrekening", false);

		$mutaties =
		RekeningMutatie::getRekeningMutatiesByPeriod($listselect_start,
		$listselect_end);

		if ($mutaties === false){
			// geen mutaties opgehaald, blijkbaar geen start en eind opgegeven
			$mutaties =
			RekeningMutatie::getLastAfschriftenMutaties(10);
		}


		echo "<h3>Opgeslagen rekeningmutaties</h3>\n";
		echo "<table class=\"bw_datatable_100\">\n";
		echo "	<tr>\n";
		echo "		<td width=\"60%\">\n";
		echo "			Begindatum (van):\n";
		echo "		</td>\n";
		echo "		<td>\n";
		echo "			" . addInput1("listselect_start", "");
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td>\n";
		echo "			Einddatum (tot en met):\n";
		echo "		</td>\n";
		echo "		<td>\n";
		echo "			" . addInput1("listselect_end", "");
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td colspan=\"2\">\n";
		echo "			" . addCheckbox("listmarkup_afrekening");
		echo "			Lijst weergeven in afrekening-formaat";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "</table><br/><hr/><br/>\n";

		echo "<table width=\"100%\">\n";
		echo "	<tr>\n";
		echo "		<td><strong>Datum afschrift</strong>\n";
		echo "		<td><strong>Volgnummer afschrift</strong>\n";
		echo "		<td><strong>Rekeningnummer</strong>\n";
		echo "		<td><strong>Boekdatum</strong>\n";
		echo "		<td><strong>Bedrag</strong>\n";
		echo "	</tr>\n";
		$lastAfschriftvolgnr = "";
		$lastAfschriftdatum = "";

		foreach ($mutaties as $mutatie){
			$afschriftdatum = $mutatie->getAfschriftDatum();
			$afschriftvolgnr = $mutatie->getAfschriftVolgnr();
			$rekeningnr = $mutatie->getRekeningnr();
			$boekdatum = $mutatie->getBoekdatum();
			$bedrag = $mutatie->getBedrag();

			echo "	<tr>\n";

			// Afschriftinformatie (volgnr, datum, rekeningnr) wordt maar
			// 1x getoond.
			if ($afschriftvolgnr == $lastAfschriftvolgnr && $afschriftdatum == $lastAfschriftdatum){
				// De informatie van dit afschrift is al getoond, niet twee
				// keer tonen (zodat duidelijk is dat de mutaties van
				// hetzelfde afschrift afkomstig zijn)
				echo "		<td>&nbsp;</td>\n";
				echo "		<td>&nbsp;</td>\n";
				echo "		<td>&nbsp;</td>\n";
			} else {
				if ($lastAfschriftvolgnr != ""){
					// Totaalbedrag vorig afschrift tonen
					$totaalbedrag =
					RekeningMutatie::getAfschriftTotaalBedrag($lastAfschriftdatum,
					$lastAfschriftvolgnr);

					echo "		<td colspan=\"4\">&nbsp;</td>\n";
					echo "		<td align=\"right\" style=\"border-top: double black\">\n";
					echo "			$totaalbedrag<br/><br/>\n";
					echo "		</td>\n";
					echo "	</tr>\n";
					echo "	<tr>\n";
				}

				// Nieuwe afschriftinformatie eenmalig tonen
				$lastAfschriftvolgnr = $afschriftvolgnr;
				$lastAfschriftdatum = $afschriftdatum;

				echo "		<td>$afschriftdatum</td>\n";
				echo "		<td>$afschriftvolgnr</td>\n";
				echo "		<td>$rekeningnr</td>\n";
			}
			echo "		<td>$boekdatum</td>\n";
			echo "		<td align=\"right\">" . FinancieelBaseClass::bedrag($bedrag) . "</td>\n";

			echo "	</tr>\n";
		}
		echo "</table>\n";
		echo "<br/><hr/><br/>\n";
		RekeningMutatie::menu();
	}
}

