<?php
/**
	Basisklasse voor (bijna) alle financiele klassen
*/

class FinancieelBase extends BookwebBase
{
	/**
		Retourneert een link voor een financiele module. Als de caption
		leeg is, dan wordt slechts de juiste ?page=, fin_action= en
		fin_subaction= teruggegeven in een string, bijv:
		"?page=financieelmenu&fin_action=rekeningmutaties&fin_subaction=blaat"
	*/
	static function finLink($action, $subaction, $caption = "", $extravars = "")
	{
		return makeBookRef('financieel', $caption,
			"fin_action=$action&fin_subaction=$subaction$extravars");
	}

	/**
		Zorgt dat de goede functie wordt aangeroepen a.h.v. de opgegeven
		parameters
	*/
	static function dispatch()
	{
		$fin_action = tryPar("fin_action");

		switch($fin_action){
			case "afschriften":
				requireAuth("boekcom");
				RekeningAfschrift::dispatch();
				break;
			case "facturen":
				requireAuth("boekcom");
				LeverancierFactuur::dispatch();
				break;
		}
	}
}

