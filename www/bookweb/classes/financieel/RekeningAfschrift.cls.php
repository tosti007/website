<?php

/**
	Klasse die een rekeningafschrift representeert
*/
class RekeningAfschrift extends BookwebBase{
	var $_afschriftnr = -1; // Primary key
	var $_volgnr;
	var $_datum;
	var $_rekeningnr;

	function __construct($afschriftnr = -1){
		if ($afschriftnr >= 0){
			$this->_afschriftnr = $afschriftnr;
			$this->update();
		} else {
			$this->_afschriftnr = -1;
		}
	}

	/**
		Set-functie voor alle mogelijke velden
		 volgnr -- Het volgnummer van het afschrift
		 datum -- De datum van het afschrift
		 rekeningnr -- De rekening waarop het afschrift betrekking heeft
	**/
	function setValues($volgnr, $datum, $rekeningnr){
		$res = true;
		$res = $this->setVolgnr($volgnr) && $res;
		$res = $this->setDatum($datum) && $res;
		$res = $this->setRekeningNr($rekeningnr) && $res;

		return $res;
	}




	function update(){
		$afschriftnr = $this->getNr();

		if (!is_numeric($afschriftnr) || $afschriftnr < 0){
			parent::addError(new BookwebError("Het gevraagde afschrift is niet
			opgeslagen in de database en er kan dus geen informatie uit de
			database gehaald worden over dat afschrift. Voer eerst de
			functie store() uit op het afschrift-object"));
			return false;
		}
		global $BWDB;

		$row = $BWDB->q("MAYBETUPLE SELECT * FROM fin_rekeningafschriften
			WHERE afschriftnr=%i", $afschriftnr);

		if (!$row){
			parent::addError(new BookwebError("Er kon geen informatie gevonden worden in de
			database voor het afschrift met afschriftnr $afschriftnr"));
			return false;
		}

		$res = $this->setValues(
			$row["volgnr"], $row["datum"], $row["rekeningnr"]
		);

		return $res;
	}

	function setVolgnr($volgnr){
		if (is_numeric($volgnr)){
			$this->_volgnr = $volgnr;
		} else {
			parent::addError(new BookwebError("Het opgegeven volgnummer
			is ongeldig!"));

			return false;
		}

		return true;
	}


	function setDatum($date){
		$parsedDate = parseDate($date);
		if ($parsedDate === false){
			// Error
			BookwebBase::addError(new BookwebError("De opgegeven datum
			($date) is ongeldig"));
			return false;
		} else {
			// OK
			$this->_datum = $parsedDate;
		}

		return true;
	}

	function setRekeningNr($rekeningnr){
		$newrekeningnr = str_replace("." , "", $rekeningnr);
		$newrekeningnr = str_replace(" ", "", $newrekeningnr);
		if (checkRekeningnrFormat($newrekeningnr)){
			$this->_rekeningnr = $newrekeningnr;
		} else {
			parent::addError(new BookwebError("Het opgegeven rekeningnr
			($rekeningnr) is ongeldig!"));
			return false;
		}

		return true;
	}

	/**
		Controleert of dit object geschikt is om op te slaan in de
		database. Als dat niet zo is, dan worden de errors opgeslagen en
		wordt er 'false' geretourneerd. Is het object wel geschikt, dan
		return true
	**/
	function validate(){
		//TODO schrijven
		return true;
	}

	function existsInDatabase(){
		$nr = $this->getNr();

		if (is_numeric($nr) && $nr >= 0){
			global $BWDB;

			$res = $BWDB->q("
				VALUE SELECT COUNT(*)
				FROM fin_rekeningafschriften
				WHERE afschriftnr = %i",
				$nr);

			return (is_numeric($res) && $res == 1);
		}

		return false;
	}

	/**
		Slaat dit object op in de database. Als de primary key != -1, dan
		wordt er een "update"-query gedaan, anders een insert.
	*/
	function store(){
		global $BWDB;

		$primkey = $this->getId();

		if (is_numeric($primkey) && $primkey >= 0){
			// UPDATE van bestaande ding
			$BWDB->q("
				UPDATE fin_rekeningafschriften
				SET volgnr=%s, datum=%s, rekeningnr=%s
				WHERE afschriftnr=%i",
				$this->getVolgnr(), $this->getDatum(),
				$this->getRekeningNr(), $primkey);
		} else {
			// INSERT
			$this->_afschriftnr = $BWDB->q("RETURNID INSERT INTO fin_rekeningafschriften SET
				volgnr=%s, datum=%s, rekeningnr=%s",
				$this->getVolgnr(), $this->getDatum(),
				$this->getRekeningNr());

		}

		return true;
	}

	function getId(){
		return $this->getAfschriftNr();
	}

	function getNr(){
		return $this->getAfschriftNr();
	}

	function getAfschriftNr(){
		return $this->_afschriftnr;
	}

	/*
		Retourneert het laatste afschrift (van een jaar, als van
		toepassing) uit de database
	*/
	static function getLastAfschrift($jaar = null)
	{
		global $BWDB;
		$where = "";
		if (is_numeric($jaar)){
			$where = "WHERE YEAR(datum) = $jaar";
		}

		$lastAfschriftNr = $BWDB->q("
			MAYBEVALUE SELECT MAX(afschriftnr)
			FROM fin_rekeningafschriften
			$where");

		return new RekeningAfschrift($lastAfschriftNr);
	}


	function getVolgnr(){
		return $this->_volgnr;
	}

	function getDatum(){
		return $this->_datum;
	}

	function getRekeningnr(){
		return $this->_rekeningnr;
	}

	/**
		Geeft het totaalbedrag van dit afschrift
	*/
	function getTotaalBedrag(){
		global $BWDB;

		$totaalbedrag = $BWDB->q("
			MAYBEVALUE SELECT SUM(bedrag) as totaalbedrag
			FROM fin_rekeningmutaties
			WHERE afschriftnr=%i",
			$this->getNr());

		if ( $totaalbedrag == "" )
			return 0;
		else
			return $totaalbedrag;
	}

	/*
		STATIC
		Haalt mutaties op die bij afschriften horen met een datum
		die tussen $start en $end liggen. Als $start niet wordt opgegeven,
		dan worden alle mutaties tot $end geretourneerd. Als $end niet wordt
		opgegeven, dan worden alle afschriften vanaf $start geretourneerd.
		Er worden altijd slechts $max afschriften geretourneerd (jongste
		eerst)
	*/
	static function getAfschriftenByPeriod($rekeningnummer, $start, $end, $max = null)
	{
		global $BWDB;

		$parsedStart = "";
		$parsedEnd = "";

		if ($start != ""){
			$parsedStart = parseDate($start);

			if (!$parsedStart){
				return false;
			}
		}
		if ($end != ""){
			$parsedEnd = parseDate($end);

			if (!$parsedEnd){
				return false;
			}
		}

		if (!is_numeric($rekeningnummer)) return false;

		$query = "COLUMN SELECT afschriftnr FROM fin_rekeningafschriften
			WHERE rekeningnr=$rekeningnummer";

		if ($parsedStart){
			$query .= " AND datum>='$parsedStart'";
		}
		if ($parsedEnd){
			$query .= "	AND datum<='$parsedEnd'";
		}

		$query .= " ORDER BY rekeningnr, datum DESC, afschriftnr DESC";

		if (is_numeric($max)){
			$query .= " LIMIT $max";
		}

		$afschriftnrs = $BWDB->q($query);
		$afschriften = array();
		foreach ($afschriftnrs as $afschriftnr){
			$afschriften[] = new RekeningAfschrift($afschriftnr);
		}

		return $afschriften;
	}

	/**
		Retourneert een array met rekeningmutatie-objecten die
		bij dit rekeningafschrift horen
	*/
	function getMutaties($jongsteEerst = true){
		global $BWDB;

		if ($jongsteEerst){
			$order = "ORDER BY boekdatum DESC, mutatienr ASC";
		} else {
			$order = "ORDER BY boekdatum ASC, mutatienr DESC";
		}

		$mutatienrs = $BWDB->q("COLUMN SELECT mutatienr
			FROM fin_rekeningmutaties
			WHERE afschriftnr=%i
			$order", $this->getNr());

		$mutaties = array();
		foreach ($mutatienrs as $mutatienr){
			$mutatie = new RekeningMutatie($mutatienr);
			$mutaties[$mutatienr] = $mutatie;
		}

		return $mutaties;
	}

	/**
		Berekent het saldo op de rekening na dit afschrift
	*/
	function getNieuwSaldo(){
		global $BWDB;

		$datum = $this->getDatum();

		// Eigenlijk is het nieuwe saldo op de rekening de som van alle
		// totaalbedragen van afschriften tot en met dit afschrift, ofwel
		// de som van alle mutaties tot en met dit afschrift
		return $BWDB->q("
			MAYBEVALUE SELECT SUM(bedrag)
			FROM fin_rekeningmutaties
			LEFT JOIN fin_rekeningafschriften USING(afschriftnr)
			WHERE fin_rekeningafschriften.datum<=%s
			AND rekeningnr=%i",
			$datum, $this->getRekeningnr());
	}

	static function dispatch()
	{
		$fin_action = tryPar("fin_action");
		$fin_subaction = tryPar("fin_subaction");
		if ($fin_action != "afschriften") return false;

		switch ($fin_subaction){
			case "list":
				RekeningAfschrift::listAfschriften();
				break;
			case "edit":
				RekeningAfschrift::formEdit();
				break;
			case 'processupload':
				RekeningAfschrift::processUpload();
				break;
			case "processedit":
				RekeningAfschrift::processEdit();
				break;
			case "delete":
				$afschriftnr = tryPar("afschriftnr", false);
				$errors = false;
				if (!$afschriftnr){
					BookwebBase::addError(new BookwebError("Er is geen
					afschriftnr opgegeven om te verwijderen"));
					$errors = true;
				} else {
					$afschrift = new RekeningAfschrift($afschriftnr);
					if (!$afschrift->existsInDatabase()){
						BookwebBase::addError(new BookwebError("Het opgegeven
						afschriftnr bestaat niet in de database"));
						$errors = true;
					} else {
						$errors = !$afschrift->deleteFromDatabase();
					}

					if ($errors){
						BookwebBase::printErrors();
					} else {
						echo "Het afschrift is succesvol verwijderd uit de
						database, klik " .
						FinancieelBase::finLink("", "", "hier") . " om terug te gaan naar
						het menu, of klik ".
						FinancieelBase::finLink("afschriften", "list",
						"hier") ." om terug te gaan naar
						de lijst met afschriften.";
					}
					break;
				}
			default:
				BookwebBase::addError(new BookwebError("De door jou
				gevraagde actie &quot;$fin_subaction&quot; is ongeldig!"));
				BookwebBase::printErrors();
				break;
		}
	}

	/**
		Toont een scherm waarin afschriften bekeken kunnen worden. Er wordt
		door deze functie ook een kleine form geprint die de output kan
		aanpassen
	*/
	static function listAfschriften(){
		global $BOEKENREKENINGEN;

		$maxAantal = tryPar("limit", 20);
		if (!is_numeric($maxAantal)) $maxAantal = 10;
		$selrange_begin = parseDate(tryPar("selrange_begin"));
		$selrange_end = parseDate(tryPar("selrange_end"));
		$showmutaties = tryPar("showmutaties", false);
		$format_liquidemiddelen = tryPar("format_liquidemiddelen", false);
		$selrekeningnr = tryPar("selrekeningnr", false);

		$afschriften = array();
		if (!$selrekeningnr){
			foreach ($BOEKENREKENINGEN as $rekeningnr=>$rekeningnaam){
				$afschriften = array_merge(
					$afschriften,
					RekeningAfschrift::getAfschriftenByPeriod($rekeningnr, $selrange_begin, $selrange_end, $maxAantal)
				);
			}
		} else {
			$afschriften = RekeningAfschrift::getAfschriftenByPeriod($selrekeningnr, $selrange_begin, $selrange_end, $maxAantal);
		}

		echo "<h3>Rekeningafschriften tonen</h3>\n";

		echo addForm("", "GET");
		echo addHidden("page", "financieel");
		echo addHidden("fin_action", "afschriften");
		echo addHidden("fin_subaction", "list");
		echo "<table width=\"600\">\n";
		echo "	<tr>\n";
		echo "		<td>Begindatum selectie:</td>\n";
		echo "		<td>" . addInput1("selrange_begin", $selrange_begin) . "</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td>Einddatum selectie:</td>\n";
		echo "		<td>" . addInput1("selrange_end", $selrange_end) . "</td>\n";
		echo "	</tr>\n";


		echo "	<tr>\n";
		echo "		<td>Bankrekening:</td>\n";
		echo "		<td>" .
						addSelect("selrekeningnr", array(null=>"alle") + $BOEKENREKENINGEN, $selrekeningnr, TRUE);
					"</td>\n";
		echo "	</tr>\n";

		echo "	<tr>\n";
		echo "		<td>&nbsp;</td>\n";
		echo "		<td>\n";
		echo "			" . addCheckBox("showmutaties", $showmutaties);
		echo "Toon mutaties<br/>\n";
		echo "			" . addCheckBox("format_liquidemiddelen", $format_liquidemiddelen);
		echo "Toon als &quot;Liquide Middelen&quot;";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td>&nbsp;</td>\n";
		echo "		<td>\n";
		echo "			<input type=\"submit\" value=\"Toon afschriften\">\n";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "</table>\n";
		echo "</form>\n";
		echo "<br/><hr/><br/>\n";


		$tblHeader = "";
		if ($format_liquidemiddelen){
			// Datum, volgnr, omschrijving, pin/chip-inkomsten, overig,
			// totaal
			$tblHeader = "		<th width=\"1\">Datum</th>\n" .
				"		<th width=\"1\">Volgnr.</th>\n" .
				"		<th>Omschrijving</th>\n" .
				"		<th>PIN/Chip-inkomsten</th>\n" .
				"		<th>Overige mutaties</th>\n" .
				"		<th>Totaal</th>\n";
		} else {
			// datum, volgnr, rekeningnummer, (boekdatum), bij/af, nieuw
			// saldo
			$tblHeader = "		<th width=\"1\">Datum</th>\n" .
				"		<th width=\"1\">Volgnr.</th>\n" .
				"		<th width=\"1\">Rekeningnr.</th>\n";
			if ($showmutaties){
				$tblHeader .= "		<th width=\"1\">Boekdatum</th>\n" .
					"		<th>Omschrijving</th>\n";
			}
			$tblHeader .= "		<th width=\"1\">Bij/af</th>\n" .
				"		<th width=\"1\">Nieuw saldo</th>\n";
		}

		$lastReknr = null;
		foreach ($afschriften as $afschrift){
			$afschriftnr = $afschrift->getNr();
			$datum = $afschrift->getDatum();
			$volgnr = $afschrift->getVolgnr();
			$rekeningnr = $afschrift->getRekeningnr();
			$totaalbedrag = BookwebBase::bedrag($afschrift->getTotaalBedrag());
			$nieuwsaldo = BookwebBase::bedrag($afschrift->getNieuwSaldo());

			if ($lastReknr != $rekeningnr){
				if ($lastReknr != null) echo "</table><br/>\n";

				echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n" .
					"<tr>$tblHeader</tr>";
			}
			$lastReknr = $rekeningnr;

			if ($format_liquidemiddelen){
				// Tonen als formaatje "liquide middelen" van
				// BoekCom-afrekening
				$mutaties = $afschrift->getMutaties(true);

				foreach ($mutaties as $mutatie){
					$isPinChip = ($mutatie->isPinTransactie() ||
						$mutatie->isChipTransactie());

					echo "	<tr>\n";
					echo "		<td>";
					echo $mutatie->getBoekDatum();
					echo "</td>\n";

					echo "		<td>";
					echo $afschrift->getVolgnr();
					echo "</td>\n";

					echo "		<td>";
					if ($mutatie->isPinTransactie()){
						echo "Betaalautomaat boekverkoop";
					} elseif ($mutatie->isChipTransactie()) {
						echo "Chipknipautomaat boekverkoop";
					} else {
						echo $mutatie->getOmschrijving();
					}
					echo "</td>\n";

					echo "		<td align=\"right\">";
					if ($isPinChip){
						echo $mutatie->getBedrag();
					} else {
						echo "&nbsp;";
					}
					echo "</td>";

					echo "		<td align=\"right\">";
					if (!$isPinChip){
						echo $mutatie->getBedrag();
					} else {
						echo "&nbsp;";
					}
					echo "</td>\n";


					echo "		<td align=\"right\">";
					echo $mutatie->getBedrag();
					echo "</td>\n";

					echo "	</tr>\n";
				}
			} elseif ($showmutaties){
				// Alle mutaties bij dit afschrift tonen. De
				// afschriftinformatie wordt slechts 1x getoond en aan het
				// einde van alle mutatiebedragen volgt er een dubbele
				// streep met daaronder het totaal van het afschrift
				$mutaties = $afschrift->getMutaties();
				$isFirstMutatie = true;
				foreach ($mutaties as $mutatie){
					$boekdatum = $mutatie->getBoekdatum();
					$bedrag = BookwebBase::bedrag($mutatie->getBedrag());
					$omschrijving = $mutatie->getOmschrijving();
					$ispinchiptransactie =
						$mutatie->isPinTransactie() || $mutatie->isChipTransactie();

					echo "	<tr>\n";
					if ($isFirstMutatie){
						$isFirstMutatie = false;
						echo "		<td>\n";
						echo FinancieelBase::finLink("afschriften", "edit", $datum,
						"&afschriftnr=$afschriftnr");
						echo "</td>\n";
						echo "		<td>\n";
						echo "			$volgnr\n";
						echo "		</td>\n";
						echo "		<td>\n";
						echo "			$rekeningnr\n";
						echo "		</td>\n";
					} else {
						echo "		<td>&nbsp;</td>\n";
						echo "		<td>&nbsp;</td>\n";
						echo "		<td>&nbsp;</td>\n";
					}

					echo "		<td>$boekdatum</td>\n";

					echo "		<td>";
					if (!$ispinchiptransactie){
						echo "$omschrijving";
					} elseif ($mutatie->isChipTransactie()) {
						echo "<em>(Chip-transactie)</em>";
					} elseif ($mutatie->isPinTransactie()) {
						echo "<em>(Pin-transactie)</em>";
					}
					echo "</td>\n";

					echo "		<td align=\"right\">$bedrag</td>\n";

					echo "	</tr>\n";
				}

				// Klaar met het printen van afzonderlijke mutaties: nu
				// totaalbedrag van dit afschrift neerzetten
				echo "	<tr>\n";
				echo "		<td>&nbsp;</td>\n";
				echo "		<td>&nbsp;</td>\n";
				echo "		<td>&nbsp;</td>\n";
				echo "		<td>&nbsp;</td>\n";
				echo "		<td>&nbsp;</td>\n";
				echo "		<td style=\"border-top: double black\" align=\"right\">\n";
				echo "			$totaalbedrag";
				echo "		</td>\n";
				echo "		<td align=\"right\">\n";
				echo "			$nieuwsaldo";
				echo "		</td>\n";
				echo "	</tr>\n";
				echo "	<tr>\n";
				echo "		<td colspan=\"20\" style=\"border-top: 1px solid
				lightgray\">&nbsp;</td>\n";
				echo "	</tr>\n";
			} else {
				// Enkel en alleen de afschriftinformatie tonen en het
				// totaal van het afschrift. Alles in normaal formaat
				$afschriftnr = $afschrift->getNr();
				$editUrl = BookwebBase::makeUrl(array(
					"page" => "financieel",
					"fin_action" => "afschriften",
					"fin_subaction" => "edit",
					"afschriftnr" => "$afschriftnr"));
				$deleteUrl = BookwebBase::makeUrl(array(
					"page" => "financieel",
					"fin_action" => "afschriften",
					"fin_subaction" => "delete",
					"afschriftnr" => "$afschriftnr"));

				echo "	<tr>\n";
				echo "		<td>\n";
				echo "			<a href=\"$editUrl\" target=\"_self\">";
				echo $datum;
				echo "</a>";
				echo "		</td>\n";
				echo "		<td>\n";
				echo "			$volgnr\n";
				echo "		</td>\n";
				echo "		<td>\n";
				echo "			$rekeningnr\n";
				echo "		</td>\n";
				echo "		<td align=\"right\">\n";
				echo "			$totaalbedrag\n";
				echo "		</td>\n";
				echo "		<td align=\"right\">\n";
				echo "			$nieuwsaldo\n";
				echo "		</td>\n";
				echo "		<td width=\"1\">\n";
				echo "			<a href=\"$deleteUrl\" target=\"_self\">";
				echo "<img src=\"/Layout/Images/Buttons/delete.png\" border=\"0\">";
				echo "</a>\n";
				echo "		</td>\n";
				echo "	</tr>\n";
			}
		}

		if (sizeof($afschriften) == 0){
			echo "<tr><td colspan=\"20\" align=\"center\">";
			echo "<em>Er zijn geen afschriften om weer te geven</em>";
			echo "</td></tr>";
		}

		echo "</table>\n";
		echo "<a href=\"" . BookwebBase::makeUrl(array(
			"page" => "financieel",
			"fin_action" => "afschriften",
			"fin_subaction" => "edit")) .
		"\" target=\"_self\">(nieuw afschrift invoeren)</a>";

	}

	
	static function formFromUpload()
	{
		
		echo "<h4>Afschrift door Text input</h4>" . "\n";
		echo addForm(FinancieelBase::finlink("afschriften",
		"processupload", ""), "POST", "multipart/form-data") . "\n";
		echo addFileInput("data") . "\n";
		echo "<input type=\"submit\" value=\"Afschrift inlezen\">";
		echo "</form>" . "\n";
	}	
	
	static function processUpload()
	{
		$file = $_FILES['data'];
		if ($file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name'])) {
			$contents = trim(file_get_contents($file['tmp_name'])); 
			$contents = explode("\n", $contents);
			$data = array();
			echo addForm(FinancieelBase::finlink("afschriften",
			"processedit", ""), "POST");
			echo "<table border='1'>" . "\n";
			echo "	<tr><th>Rekening</th><th>Datum</th><th>Bedrag</th><th>Tegenrekening</th><th>Pinbetaling</th><th>Omschrijving</th></tr>" . "\n";
			$rowData = array();
			$nr = 1;
			foreach($contents as $line) {
				$columns = explode("\t", $line);
				$rowData = null;
				
				$rowData['bedrag'] = str_replace(',', '.', $columns[6]);
				$rowData['datum'] = substr($columns[2], 0, 4) . '-' . substr($columns[2], 4, 2) . '-' . substr($columns[2], 6, 2);
				$oms = trim($columns[7]);
				if(stripos($oms, "BETAALAUTOMAAT") > 0) {
					$rowData['omschrijving'] = '';
					$rowData['tegenrekening'] = '';
					$rowData['pin'] = true;
				} else {
					$matches = array();
					if(stripos($oms, "GIRO") === 0) {
						preg_match("/^GIRO\s*([0-9]*)/", $oms, $matches);
						$rowData['omschrijving'] = substr($oms, strlen($matches[0]));
						$rowData['tegenrekening'] = $matches[1];
					} elseif(preg_match("/^([0-9\.]*)\s/", $oms, $matches) > 0) {
						$rowData['omschrijving'] = trim(substr($oms, strlen($matches[0])));
						$rowData['tegenrekening'] = str_replace('.', '', $matches[1]);
					} else {
						$rowData['omschrijving'] = $oms;
						$rowData['tegenrekening'] = '';
					}
					$rowData['pin'] = false;
				}

					
				echo "<tr>" . "\n";
				echo "	<td>" . $columns[0] . "</td>" . "\n";
				echo "	<td>" . $rowData['datum'] . "</td>" . "\n";
				echo "	<td>" . $rowData['bedrag'] . "</td>" . "\n";
				echo "	<td>" . $rowData['tegenrekening'] . "</td>" . "\n";
				echo "	<td>" . ($rowData['pin'] ? 'JA' : '') . "</td>" . "\n";
				echo "	<td>" . $rowData['omschrijving'] . "</td>" . "\n";
				echo "</tr>" . "\n";
				echo addHidden("mutatienr_$nr", -1);
				echo addHidden("boekdatum_$nr", $rowData['datum']);
				echo addHidden("tegenrekening_$nr", $rowData['tegenrekening']);
				echo addHidden("bedrag_$nr", $rowData['bedrag']);
				echo addHidden("omschrijving_$nr", $rowData['omschrijving']);
				echo addHidden("ispintransactie_$nr", $rowData['pin']);
				$nr++;
			}
			echo addHidden("rekeningnr", $contents[0][0]);
			echo addHidden("aantalmutaties", count($contents));
			echo addHidden("fin_action", "afschriften");
			echo addHidden("fin_subaction", "processedit");
			echo addHidden("fromupload", "true");
			echo "</table>";
			echo "<input type=\"submit\" value=\"Volgende stap\">";
			echo "</form>";
		}			
		
	}

	/**
		Geeft een edit-afschrift formulier weer met (standaard) 20
		invoervelden voor afzonderlijke mutaties.
		In principe zijn alle mutatievelden leeg, tenzij er
		pagerequest-parameters zijn die de velden
		invullen (in geval van fouten in de invoer).
		Let op! De velden afschriftvolgnr, afschriftdatum en rekeningnr
		zijn "algemeen", dus niet specifiek per mutatie!
	*/
	static function formEdit()
	{
		$aantalInvoerVelden = 60;
		$afschriftnr = tryPar("afschriftnr", -1);
		$afschriftVolgnr = "";
		$afschriftDatum = "";
		$rekeningnr = "";
		$mutaties = array();

		if ($afschriftnr != -1){
			// Te bewerken afschrift opvragen
			$nieuwAfschrift = false;
			$afschrift = new RekeningAfschrift($afschriftnr);

			if (!$afschrift->existsInDatabase()){
				// AfschriftID bestaat niet in database?
				BookwebBase::addError(new BookwebError("Het opgegeven
				afschriftnr is ongeldig"));
				BookwebBase::printErrors();
				return false;
			} else {
				$mutaties = $afschrift->getMutaties();
				$afschriftVolgnr = $afschrift->getVolgnr();
				$afschriftDatum = $afschrift->getDatum();
				$rekeningnr = $afschrift->getRekeningnr();
			}
		} else {
			$afschrift = null;
			$nieuwAfschrift = true;
			// Nieuwe mutaties invoeren: volgnr is eentje hoger dan de
			// laatste volgnr en het rekeningnummer blijft hetzelfde
			$lastAfschrift =
			RekeningAfschrift::getLastAfschrift(strftime("%Y"));
			if ($lastAfschrift instanceof RekeningAfschrift){
				$afschriftVolgnr = $lastAfschrift->getVolgnr() + 1;
				$rekeningnr = $lastAfschrift->getRekeningnr();
			}
		}

		echo "<h3>";
		if ($nieuwAfschrift){
			echo "Nieuw rekeningafschrift invoeren";
		} else {
			echo "Rekeningafschrift bewerken";
		}
		echo "</h3>";
		
		echo RekeningAfschrift::formFromUpload();

		echo addForm(FinancieelBase::finlink("afschriften",
		"processedit", ""), "POST");
		echo addHidden("aantalmutaties", $aantalInvoerVelden);
		echo addHidden("fin_action", "afschriften");
		echo addHidden("fin_subaction", "processedit");
		if (!$nieuwAfschrift){
			echo addHidden("afschriftnr", $afschriftnr);
		}

		echo "Algemene informatie over het rekeningafschrift:\n";
		echo "<table width=\"600\">\n";
		echo "	<tr>\n";
		echo "		<td width=\"50%\">\n";
		echo "			Volgnummer afschrift:\n";
		echo "		</td>\n";
		echo "		<td width=\"50%\">\n";
		echo "			" . addInput1("afschriftvolgnr", $afschriftVolgnr);
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td valign=\"top\">\n";
		echo "			Datum afschrift:\n";
		echo "		</td>\n";
		echo "		<td valign=\"top\">\n";
		echo "			" . addInput1("afschriftdatum",	$afschriftDatum) . "<br/>\n";
		echo "			<font size=\"1\">(formaat: YYYY-MM-DD)</font>\n";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "	<tr>\n";
		echo "		<td width=\"50%\">\n";
		echo "			Rekeningnummer afschrift:\n";
		echo "		</td>\n";
		echo "		<td>\n";
		echo "			" . addInput1("rekeningnr",	$rekeningnr) . "\n";
		echo "		</td>\n";
		echo "	</tr>\n";
		echo "</table><br/><br/>\n";

		if ($aantalInvoerVelden > 1){
			echo "In de velden hieronder kun je de losse mutaties invullen.
			Lege velden worden niet verwerkt, dus je kunt gewoon op
			&quot;Rekeningmutataties opslaan&quot; drukken als je klaar bent
			met het invoeren.";
		}

		/* Er wordt 1 tabel gebruikt om n afzonderlijke tabelletjes (met
		 * rekeningmutaties) te tonen per tabelregel (tr) staan er twee
		 * tabelletjes, ieder in een aparte cell (td)
		 */

		// Tip: elke oneven $i (dus $i % 2 == 1) is een nieuwe tr en iedere
		// even $1 (dus $i % 2 == 0) is een einde tr
		$mutaties = array_values($mutaties);
		echo "<table width=\"100%\">\n";
		for ($i = 1; $i <= $aantalInvoerVelden; $i++){
			if (sizeof($mutaties) >= $i && sizeof($mutaties) != 0){
				// Blijkbaar worden er mutaties vanuit de database bewerkt,
				// er zijn iniedergeval RekeningMutatie-objecten
				// beschikbaar om informatie uit te halen voor in de form.
				$currentMutatie = $mutaties[$i - 1];

				$mutatienr = $currentMutatie->getNr();
				$boekdatum = $currentMutatie->getBoekdatum();
				$bedrag = $currentMutatie->getBedrag();
				$omschrijving = $currentMutatie->getOmschrijving();
				$tegenrekening = $currentMutatie->getTegenRekening();
				$ispintransactie = $currentMutatie->isPinTransactie();
				$ischiptransactie = $currentMutatie->isChipTransactie();
				$currentMutatie->isChipTransactie();
			} else {
				// Lege velden, tenzij er pagerequest-parameters zijn voor
				// de velden (in geval van foute invoer dus opnieuw invoer
				// tonen)
				$mutatienr = -1;
				$boekdatum = tryPar("boekdatum_$i", "");
				$bedrag = tryPar("bedrag_$i", "");
				$omschrijving = tryPar("omschrijving_$i", "");
				$tegenrekening = tryPar("tegenrekening_$i", "");
				$ispintransactie = tryPar("ispintransactie_$i", false);
				$ischiptransactie = tryPar("ischiptransactie_$i", false);
				if ($ispintransactie) $ispintransactie = true;
				if ($ischiptransactie) $ischiptransactie = true;
			}

			if ($i % 2 == 1){
				// TR starten
				echo "	<tr>\n";
			}
			echo "		<td width=\"50%\" style=\"border: 1px solid black\">\n";

			// Hieronder staat de tabel met een rekeningmutatie
			echo addHidden("mutatienr_$i", "$mutatienr");
			echo "<div style=\"font-size: 10pt\">";
			echo "<table width=\"100%\">\n";
			echo "	<tr>\n";
			echo "		<td width=\"30%\" valign=\"middle\">\n";
			echo "			Boekdatum:\n";
			echo "		</td>\n";
			echo "		<td>\n";
			echo "			" . addInput1("boekdatum_$i", $boekdatum);
			echo "<font size=\"1\">(YYYY-MM-DD of DD-MM)</font>";
			echo "		</td>\n";
			echo "	</tr>\n";
			echo "	<tr>\n";
			echo "		<td>\n";
			echo "			Tegenrekening:\n";
			echo "		</td>\n";
			echo "		<td>\n";
			echo "			" . addInput1("tegenrekening_$i", $tegenrekening) . "\n";
			echo "		</td>\n";
			echo "	</tr>\n";
			echo "	<tr>\n";
			echo "		<td>\n";
			echo "			Bedrag:\n";
			echo "		</td>\n";
			echo "		<td>\n";
			echo "			" . addInput1("bedrag_$i", $bedrag) . "\n";
			echo "		</td>\n";
			echo "	</tr>\n";
			echo "	<tr>\n";
			echo "		<td valign=\"top\">\n";
			echo "			Omschrijving:\n";
			echo "		</td>\n";
			echo "		<td>\n";
			echo "			" . addTextArea("omschrijving_$i", $omschrijving, 25, 2) . "\n";
			echo "		</td>\n";
			echo "	</tr>\n";
			echo "	<tr>\n";
			echo "		<td>\n";
			echo "			&nbsp\n";
			echo "		</td>\n";
			echo "		<td>\n";
			echo "			" . addCheckbox("ispintransactie_$i", $ispintransactie) . "\n";
			echo " <label for='ispintransactie_$i'>Pinautomaat boekverkoop</label><br/>\n";
			echo "			" . addCheckbox("ischiptransactie_$i", $ischiptransactie) . "\n";
			echo " <label for='ischiptransactie_$i'>Chipautomaat boekverkoop</label>\n";
			echo "		</td>\n";
			echo "	</tr>\n";
			echo "</table>\n";
			echo "</div>";

			echo "		</td>\n";

			if ($i % 2 == 0){
				// TR einde
				echo "	</tr>\n";
			}
		}
		if ($i % 2 == 0){
			// Blijkbaar is er een oneven aantal rekeningmutataties
			// getoond, dus moet er nog een lege cell (<td>...</td>) en een
			// einde-rij (</tr>) ingevoegd worden
			echo "		<td>&nbsp;</td>\n";
			echo "	</tr>\n";
		}
		echo "</table>\n";
		echo "<div align=\"center\">\n";
		echo "<input type=\"submit\" value=\"Rekeningafschrift opslaan\">";
		echo "</form>";
		echo "</div>\n";

	}


	/**
		Verwerkt een ingevuld formulier van een rekeningafschrift. Als het
		niet gelukt is om de gegevens op te slaan, dan retourneert deze
		functie false. Dan wordt tevens opnieuw het goede form getoond. Als
		alles in orde is, dan is het resultaat true.
		In het geval van zeer vreemde errors (die niet mogen voorkomen,
		maar wel gechecked worden) wordt er een return false gedaan, zonder
		nieuwe form
	*/
	static function processEdit()
	{

		// Het totaal aantal mutaties dat mogelijk ingevoerd is (dus
		// eigenlijk het aantal mutaties dat ingevuld kan worden)
		$aantalMutaties = tryPar("aantalmutaties");
		$afschriftnr = tryPar("afschriftnr");
		$nieuwAfschrift = !(is_numeric($afschriftnr) && $afschriftnr >= 0);

		if (is_numeric($aantalMutaties)){
			echo "<h3>";
			if ($nieuwAfschrift){
				echo "Nieuw rekeningafschrift verwerken:";
			} else {
				echo "Wijzigingen rekeningafschrift verwerken:";
			}
			echo "</h3>";

			$afschriftvolgnr = tryPar("afschriftvolgnr");
			$rekeningnr = tryPar("rekeningnr");
			$afschriftdatum = tryPar("afschriftdatum");

			if (tryPar("fromupload") == "true") {
				echo "<div>De geuploade data is ge&iuml;nterpreteerd. Vul de juiste afschriftdatum, het volgnummer en rekeningnummer in en wijzig indien gewenst enkele velden hieronder en sla het afschrift vervolgens op.</div>";
				$errors = true;
			} elseif (!is_numeric($afschriftvolgnr)){
				BookwebBase::addError(new BookwebError("Het
				opgegeven afschriftvolgnummer is ongeldig"));
				$errors = true;
			} elseif (!is_numeric($rekeningnr)){
				BookwebBase::addError(new BookwebError("Het
				opgegeven rekeningnummer van het afschrift is ongeldig"));
				$errors = true;
			} elseif (parseDate($afschriftdatum) === false){
				BookwebBase::addError(new BookwebError("De
				opgegeven afschriftdatum is ongeldig"));
				$errors = true;
			} else {
				// OK, opslaan

				if (is_numeric($afschriftnr)){
					// Bestaand afschrift bewerken
					$afschrift = new RekeningAfschrift($afschriftnr);

					if (!$afschrift->existsInDatabase()){
						BookwebBase::printErrors();
						return false;
					}
				} else {
					$afschrift = new RekeningAfschrift();
				}


				if ($afschrift->setValues($afschriftvolgnr, $afschriftdatum, $rekeningnr)){
					$afschrift->store();
				} else {
					BookwebBase::addError(new BookwebError("Er is
					iets foutgegaan bij het opslaan van een rekeningafschrift"));
				}

				if (BookwebBase::hasNewErrors()){
					BookwebBase::printErrors();
					RekeningAfschrift::formEdit();
					return false;
				}



				echo "<strong>Algemene informatie over het
				rekeningafschrift:</strong><br />\n";
				echo "Volgnummer afschrift: $afschriftvolgnr<br/>\n";
				echo "Datum afschrift: $afschriftdatum<br/>\n";
				echo "Rekeningnummer: $rekeningnr<br/>\n";
				echo "<br/><br/>\n";

				$mutaties = array();
				$errors = false;
				// Iedere mutatie afzonderlijk processen en de complete
				// verwerking stoppen als er fouten in een invoer zitten.
				echo "<table width=\"100%\" class=\"bw_datatable_medium\">\n";
				echo "	<tr>\n";
				echo "		<th>Boekdatum</th>\n";
				echo "		<th>Bedrag</th>\n";
				echo "		<th>Tegenrekening</th>\n";
				echo "		<th>Opmerkingen</th>\n";
				echo "		<th>&nbsp;</th>\n"; // Kolom voor errors
				echo "	</tr>\n";

				for ($i = 1; $i <= $aantalMutaties; $i++){
					$errorsInMutatie = false;

					$mutatienr = tryPar("mutatienr_$i");
					$boekdatum = tryPar("boekdatum_$i");
					$bedrag = tryPar("bedrag_$i");
					$omschrijving = tryPar("omschrijving_$i");
					$tegenrekening = tryPar("tegenrekening_$i");
					$ispintransactie = tryPar("ispintransactie_$i");
					$ischiptransactie = tryPar("ischiptransactie_$i");

					if (strlen($boekdatum) == 5){
						// Blijkbaar is $boekdatum op DD-MM ingevuld, dat
						// mag...
						$boekdatum_oud = $boekdatum;

						// Goede jaar opzoeken
						$boekdatum_jaar = substr(parseDate($afschriftdatum), 0,	4);

						// DD en MM omdraaien:
						$dd = substr($boekdatum, 0, 2);
						$mm = substr($boekdatum, 3, 2);

						$boekdatum = "$boekdatum_jaar-$mm-$dd";

					} // YYYY-MM-DD

					// Controleren of deze mutatie wel is ingevuld: lege velden
					// moeten namelijk genegeerd worden
					if ($boekdatum != "" && $bedrag != ""){
						// Deze mutatie kan verwerkt worden

						if (!parseDate($boekdatum)){
							BookwebBase::addError(new BookwebError("De
							opgegeven boekdatum &quot;$boekdatum&quot; is
							ongeldig"));
							$errorsInMutatie = true;
						}
						if ($ischiptransactie && $ispintransactie){
							BookwebBase::addError(new BookwebError("Een mutatie kan
							niet zowel een chip- als pintransactie zijn"));
							$errorsInMutatie = true;
						}
						echo "	<tr>\n";
						echo "		<td valign=\"top\">$boekdatum</td>\n";
						echo "		<td valign=\"top\">" . Money::addPrice($bedrag) . "</td>\n";

						echo "		<td valign=\"top\">";
						if ($ispintransactie){
							echo "(PIN-transactie)";
						} elseif ($ischiptransactie){
							echo "(Chip-transactie)";
						} else {
							echo "$tegenrekening";
						}
						echo "<td>";
						echo $omschrijving;
						echo "</td>\n";
						if ($mutatienr >= 0){
							// De mutatie bestaat al in de database: bijwerken!
							$mutatie = new RekeningMutatie($mutatienr);
						} else {
							// Gehele nieuwe mutatie voor in de database
							$mutatie = new RekeningMutatie();
						}

						$transactieType = null;
						if ($ispintransactie) $transactieType = "pin";
						if ($ischiptransactie) $transactieType = "chip";

						// Proberen een object te construeren en daar waardes in te
						// stoppen. Als het misgaat, dan alles afbreken (en niets
						// opslaan!)
						$success = $mutatie->setValues(
							$afschrift->getNr(), $boekdatum, $bedrag, $omschrijving,
							$tegenrekening, $transactieType
						);

						if (!$success){
							// Oh jee! Problemen!
							$errorsInMutatie = true;
						} else {
							// Nieuw object toevoegen aan array
							$mutaties[] = $mutatie;
							// Let op! Deze mutatatie wordt expres niet opgeslagen,
							// ook als er geen errors waren met het ophalen van
							// gegevens uit de form. Als er namelijk ergens een
							// fout zit, dan wordt alle informatie weer
							// teruggespeeld naar de gebruiker, die moet het dan
							// opnieuw proberen!
						}

						if ($errorsInMutatie){
							echo "<td bgcolor=\"red\" valign=\"middle\"
							align=\"center\">";
							// Kolommetje maken met een rood uitroepteken
							echo "<font color=\"white\"><strong>";
							echo "!!\n";
							echo "</strong></font>\n";
							$errors = true;
						} else {
							echo "<td>\n";
							echo "&nbsp;";
						}
						echo "</td>\n";
						echo "	</tr>\n";

					} // else: lege mutatie, niet verwerken
				} // end for
			} // end if
			echo "</table>\n";

			if ($errors){
				// Er waren errors bij het verwerken van de mutaties: alle
				// gegevens weer terugspelen naar de gebruiker
				BookwebBase::printErrors();
				print_warning ("Let op! Geen enkele rekeningmutatie is opgeslagen,
				omdat er een fout in de invoer zit. Hieronder zie je
				opnieuw het invoerformulier met de ingevoerde data.
				Corrigeer daar de fout, en probeer de mutaties opnieuw op
				te slaan.<br/><hr>\n");
				RekeningAfschrift::formEdit();
				return false;
			} else {
				// Alle invoer was in orde: dus alle gemaakte
				// RekeningMutatie-objecten opslaan in de
				// database

				foreach ($mutaties as $mutatie){
					$mutatie->store();
				}

				if (!$nieuwAfschrift){
					echo "Het rekeningafschrift is succesvol opgeslagen in de
					database.";
				} else {
					echo "De wijzigingen zijn succesvol opgeslagen in de
					database.";
				}
				echo " Klik " .	FinancieelBase::finlink("afschrift",
				"edit", "hier") . " om nog meer rekeningafschriften in
				te voeren, ";

				echo "klik " .
				FinancieelBase::finlink("afschriften", "list",
				"hier") . " om rekeningafschriften te bekijken, ";

				echo "of klik " .
				FinancieelBase::finlink("afschriften", "",
				"hier") . " om terug te gaan naar het menu.";

			}
		} // else: met rust laten, geen mutaties om te verwerken

		return true;
	}

	/**
		Verwijdert alle mutaties die aan dit afschrift hangen uit de
		database
	*/
	function deleteAllMutaties(){
		$mutaties = $this->getMutaties();

		foreach ($mutaties as $mutatie){
			$mutatie->deleteFromDatabase();
		}
	}

	// Verwijdert dit afschrift en alle mutaties ervan uit de db
	function deleteFromDatabase(){
		global $BWDB;

		if (!$this->existsInDatabase()){
			BookwebBase::addError(new BookwebError("Kan deleteFromDatabase
			niet uitvoeren over een object dat niet in de database
			bestaat!"));
			return false;
		}

		$this->deleteAllMutaties();

		$BWDB->q("
			DELETE FROM fin_rekeningafschriften
			WHERE afschriftnr=%i",
			$this->getNr());

		return true;
	}
	/**
		Geeft een stringrepresentatie van dit object
	*/
	function toString(){
		return "AfschriftNr " . $this->getNr() . ": " . $this->getDatum() . " " . $this->getVolgnr() . " " .
		$this->getRekeningnr() . " " . $this->getTotaalBedrag();
	}
}

