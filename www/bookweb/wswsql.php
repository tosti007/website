<?php
// $Id$

/* Alle BookWeb SQL-calls die Who 's Who uitlezen */

function addCiesSelect($cienr = null)
{
	$cies = CommissieVerzameling::huidige()->sorteer('naam')->geefArrayString();

	if (!is_null($cienr))
		$cies[$cienr] = Commissie::geef($cienr)->getNaam();

	return addSelect('cienr', $cies, $cienr, TRUE);
}

function sqlLidStudies($lidnr)
{
	global $STUDIENAMEN;

	$lid = Lid::geef($lidnr);
	$result = array();

	if($lid){
		foreach ($lid->getStudies() as $studie)
		{
			if ($studie->getStatus() == 'STUDEREND')
				$result[] = StudieView::waardeNaam($studie->getStudie());
		}
	}
	return $result;
}

