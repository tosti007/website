<?php
// $Id$


/*     <<<< zet voor deze regel een '//' in geval van onderhoud

?>
<h2>Bookweb tijdelijk gesloten wegens onderhoudswerzaamheden.</h2>
Probeer het over een uurtje nog eens.

<?php
exit;
/* */

/**
 * Deze file includet alles wat we nodig hebben.
 */
global $SQLDB;

//bootstrap-includes
require_once("bookweb/constants.php");
require_once("bookweb/dispatch.php");

// nodig voor alle functies?
require_once("bookweb/bookfuncs.php");
require_once("bookweb/money.php");
require_once("bookweb/calendar.php");
require_once("bookweb/mail.php");

// BookWeb functies
require_once("bookweb/bookhome.php");
require_once("bookweb/forms.php");

// voorlopig nog niet...
//require_once("bookweb/logging.php");

// dit is alleen nodig als je >= verkoper bent
if ( hasAuth('verkoper') ) {
	require_once("bookweb/financieel.php");
	require_once("bookweb/classes/includes.php");
	require_once("bookweb/wagentje.php");
	require_once("bookweb/export.php");
	require_once("bookweb/verkoop.php");
	require_once("bookweb/verkooplijst.php");
	require_once("bookweb/levretour.php");
}

// kan altijd nodig zijn, dus altijd includen:
require_once("bookweb/booksql.php");
require_once("bookweb/wswsql.php");
require_once("bookweb/guest.php");
require_once("bookweb/lid.php");
// boekcom.php: niet strict boekcom helaas
require_once("bookweb/boekcom.php");
require_once("bookweb/artikelcodes.php");
// cuecat.php: alleen backward compatibility
require_once("bookweb/cuecat.php");
require_once("bookweb/interface.php");

// alleen voor >=verkoper?
require_once("bookweb/vakken.php");

// voor addFoto() en een paar naam dingetjes
require_once("bookweb/oldwhofuncs.php");
