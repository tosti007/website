<?php
// $Id$

/**

functies:

- spawn(). Bekijkt de inhoud van het 'page' veld in de request en roept de
bijbehorende functie aan, als er tenminste ingelogd is met het vereiste
level.

- bwnietherkend($page): als er geen of een onbekende $page opgegeven is,
wordt dit hier afgevangen.

**/

function bwnietherkend($page)
{
	// voc: Vraag aan de *WebCie* ...
	$action=tryPar('action');
	printf(_("<P>De functie die je opvroeg (%s) is onbekend. Heeft het
		misschien te maken met je securitylevel?

		<P>Vraag aan de %s wat er aan de hand kan zijn.</p>
		"), $page.($action?",".$action:"")
		,  makeRef("mailto:".WWWEMAIL,'WebCie'));
}

function statsimages_content($res){
	$GLOBALS["gd2"] = true;
	require_once('bookweb/statsimages.php');
}

function boekinfo_image_content($rest){
	require_once('bookweb/boekinfo-image.php');
}

function opensearch_content($res){
	require_once('bookweb/opensearch.php');
}


function bookweb_content($rest)
{
	global $request, $BESTUUR;

	require_once('bookweb/init.php');

	$inlayout = true;

	if ($rest == array('index.php')) {
		$inlayout = false;
	} elseif (count($rest) == 1) {
		$page = $rest[0];
	} elseif ($rest && $rest != array('home.html')) {
		spaceHTTP(404);
	}

	if (!@$page || tryPar('page')) $page = tryPar('page');

	$page = strtolower($page);
	if (strtolower($page) == "boekinfo-image"){
		boekinfo_image_content(null);
		die();
	}

	// compat (FIXME)
	$request->request->set('page', $page);

	if ($inlayout)
	{
		Page::getInstance()->useCSP = false;
		// Dit mag niet
		// Dit moet niet
		// Dit wil je niet
		// En toch gebeurt het
		Page::getInstance()->start("Boeken" . (empty($page)?'':' > ' . ucfirst($page)), null, null, false, TRUE);
	}

	if (!hasAuth('bijnalid') && !$page) {
?>


<h2>Bookweb</h2>

<?
printf(_("
<p>Sinds het collegejaar 2015-2016 kun je je boeken online bestellen via %shttps://aeskwadraat.itdepartment.nl/%s. Als lid krijg je 20%% korting op alle buitenlandse boeken! Word je eerstejaars en ben je nog geen lid, lees dan de instructies op %sde website van de intro%s hoe je toch met korting kan bestellen! Als je vragen hebt, neem dan contact op met %s.</p>

<p>Voor dictaten, kaartjes voor activiteiten of leuke A–Eskwadraatspullen kun je elke werkdag terecht bij de Boekverkoop. Deze bevindt zich tegenover kamer 2.69 van het Buys Ballot Gebouw en is elke dag open van 12.30 tot 13.30. Let op: je kunt hier alleen pinnen! Zien wat er op dit moment te koop is bij de Boekverkoop? Ga naar de %s.</p>
"),
	'<a href="https://aeskwadraat.itdepartment.nl/">', '</a>',
	'<a href="http://bachelor.intro-utrecht.nl/">', '</a>',
	makeRef('mailto:'.$BESTUUR['boekencommissaris']->getEmailAdres(), _('de Boekencommissaris')),
	makeRef('/Onderwijs/Boeken/Vrijeverkoop', _('Lijst met alle artikelen die direct te koop zijn'))
);

		bookweb_showfooter();
		bookweb_levelswitch();
		Page::getInstance()->end();
		return;
	}
	if ( $inlayout && hasAuth('verkoper')) {
			echo ' <table class="bw_boekvakinfo"><tr><td valign="middle"
			width="50%" align="center"> ' .
				addForm('/Onderwijs/Boeken/Boekinfo', 'GET') .
				addInput1('EAN', '', FALSE, 25);
			echo addFormEnd(_("BoekInfo"), false, "bw_button");
			echo ' </td><td valign="middle" width="50%" align="center"> ' .
				addForm('Vakinfo', 'GET') .
				addInput1('vakzoek', '', FALSE, 25);
			echo addFormEnd(_("VakInfo"), false, "bw_button");
			echo ' </td></tr></table>';
	}

	/* start output buffering zodat we straks de clientbar eerst kunnen outputten */
	ob_start();

	switch($page) {
		case '':
			requireAuth('bijnalid');
			hoofdmenu();
			break;

//lid/verkoper
		case 'bestellen':
			requireAuth('bijnalid');
			bestelmenu();
			break;
		case 'bestelling':
			requireAuth('bijnalid');
			bestelling(getSelLid());
			break;
		case 'bestelform':
			requireAuth('bijnalid');
			bestelform();
			break;
		case 'alleboeken':
			alleBoekenOpenbaar();
			break;
		case 'bestelstatus':
			requireAuth('bijnalid');
			bestelstatus();
			break;
		case 'afbestellen':
			requireAuth('bijnalid');
			bestelaf();
			break;
		case 'annuleersbs':
			requireAuth('boekcom');
			annuleerOpenSBs();
			break;
		case 'vervalwijzig':
			requireAuth('boekcom');
			vervalwijzig();
			break;
		case 'voorraad':
			requireAuth('bijnalid');
			voorraad();
			break;
		case 'allelossevk':
			alleLosseVerkoopBoeken();
			break;

//verkoper
		case 'openverkoop':
			requireAuth('verkoper');
			openverkoop();
			break;
		case 'verkoop':
			requireAuth('verkoper');
			verkoopmenu();
			break;
		case 'verkoopbesteld':
			requireAuth('verkoper');
			verkoopbesteld();
			break;
		case 'verkooponbesteld':
			requireAuth('verkoper');
			verkooponbesteld();
			break;
		case 'verkooppakket':
			requireAuth('verkoper');
			verkooppakket();
			break;
		case 'terugkoop':
			requireAuth('verkoper');
			terugkoop();
			break;
		case 'omruil':
			requireAuth('verkoper');
			omruilen();
			break;


		case 'toonverkopen':
			requireAuth('verkoper');

			// De boekcom kan terugzoeken in de geschiedenis
			if (hasAuth('boekcom')){
				$datum = tryPar("datum", null);
			} else {
				$datum = null;
			}

			toonverkopen($datum, null, tryPar('verkopers'));
			break;
		case 'toonverkopenperproduct':
			requireAuth('verkoper');
			menuToonVerkopenPerProduct();
			break;
		case 'teverkopenbestellingen':
			requireAuth('verkoper');
			toonTeVerkopenBesteldeBoeken(time());
			break;
		case 'sluitverkoop':
			requireAuth('verkoper');
			sluitverkoop();
			break;

		// clientbaracties
		case 'changeactiondatetime':
			requireAuth('boekcom');
			changeactiondatetime();
			hoofdmenu();
			break;
		case 'changeselectedlid':
			requireAuth('verkoper');
			changeselectedlid(tryPar('data'));
			if(tryVerkoopOpen())
				verkoopMenu();
			else
				hoofdMenu();
			break;
		case 'kartdrop':
			requireAuth('verkoper');
			bw_dropFromKart(requirePar('nr'));
			verkoopMenu();
			break;
		case 'togglebtw':
			requireAuth('verkoper');
			bw_toggleBTWKart();
			break;
		case 'checkout':
			requireAuth('verkoper');
			checkout();
			break;

		case 'boekinfo':
			requireAuth('verkoper');
			showBoekInfo();
			break;
		case 'voorraadmutaties':
			requireAuth('verkoper');
			voorraadmutaties();
			break;
		case 'telling':
			requireAuth('verkoper');
			telling(sqlForceTelling());
			break;
		case 'kasverschillen':
			requireAuth('verkoper');
			verklaardeKasverschillen(getActionDate());
			break;
		case 'afrekening':
			requireAuth('verkoper');
			finDispatch();
			break;
			
//boekcom
		case 'geschiedenis':
			requireAuth('boekcom');
			Geschiedenis::printGeschiedenis();
			break;
		case 'totalevoorraad':
			requireAuth('boekcom');
			totalevoorraad();
			break;
		case 'showleveringen':
			requireAuth('boekcom');
			if (tryPar('datum')){
				showLeveringen(tryPar('datum'), tryPar('leveranciernr'),tryPar('datumeind'),tryPar('output'));
			} else {
				showLeveringenList();
			}
			break;
		case 'showbestelling':
			requireAuth('boekcom');
			showBestelling(tryPar('datum'), tryPar('leveranciernr'));
			break;
		case 'tebestellen':
			requireAuth('boekcom');
			tebestellen();
			break;
		case 'televeren':
			requireAuth('boekcom');
			televeren();
			break;
		case 'vakinfo':
			requireAuth('verkoper');
			vakInfo();
			break;
		case 'insboekvak':
			requireAuth('boekcom');
			insertboekvak();
			break;
		case 'updboekvak':
			requireAuth('boekcom');
			wijzigboekvak();
			break;
		case 'delboekvak':
			requireAuth('boekcom');
			delboekvak();
			break;
		case 'delvak':
			requireAuth('boekcom');
			delvak();
			break;
		case 'insvak':
			requireAuth('boekcom');
			insertvak();
			break;
		case 'insboek':
			requireAuth('boekcom');
			insertboek();
			break;
		case 'insartikel':
			requireAuth('boekcom');
			insertartikel();
			break;
		case 'pakketten':
			requireAuth('boekcom');
			toonpakketten();
			break;
		case 'pakket':
			requireAuth('boekcom');
			insertpakket();
			break;
		case 'insnieuws':
			requireAuth('boekcom');
			insertnieuws();
			break;
		case 'levering':
			requireAuth('boekcom');
			levretour();
			break;
		case 'offerte':
			requireAuth('boekcom');
			offerte();
			break;
		case 'retour':
			requireAuth('boekcom');
			levretour();
			break;
		case 'docentenboekenmail':
			requireAuth('boekcom');
			docentenBoekenMail();
			break;
		case 'boekenlijst':
			requireAuth('boekcom');
			boekenLijst();
			break;
		case 'offerteproblemen':
			requireAuth('boekcom');
			offerteProblemen();
			break;
		case 'alterbestelling':
			requireAuth('boekcom');
			alterbestelling();
			break;
		case 'verplaatsing':
			requireAuth('boekcom');
			verplaatsing();
			break;
		case 'annuleren':
			requireAuth('boekcom');
			annuleren();
			break;
		case 'alterboekvak':
			requireAuth('boekcom');
			alterboekvak();
			break;
		case 'updatebestellingen':
			requireAuth('boekcom');
			updatebestellingen();
			break;
		case 'financieel':
			requireAuth('boekcom');
			FinancieelBase::dispatch();
			break;
		case 'export':
			requireAuth('boekcom');
			export();
			break;
		case 'leveranciers':
			requireAuth('boekcom');
			showleveranciers();
			break;
		case 'leverancier':
			requireAuth('boekcom');
			updateleverancier();
			break;
		case 'specialconstants':
			requireAuth('boekcom');
			showspecialconstants();
			break;
		case 'verkoopoverzicht':
			requireAuth('verkoper');
			printVerkoopOverzicht();
			break;
		case 'stats':
			requireAuth('boekcom');
			BookwebStatistieken::dispatch();
			break;

//god
		case 'processto4':
			requireAuth('god');
			echo "Processing to 4..."; flush();
			processTo4();
			echo " done.";
			break;
		case 'test':
			requireAuth('god');
			require('bookweb/test.php');
			test();
			break;
		case 'vars':
			requireAuth('god');
			print_r(array_merge($request->request->all(), $request->query->all()));
			break;
		default:
			bwnietherkend($page);
			break;
	}

	/* stop met bufferen */
	$bodytext = ob_get_contents();
	ob_end_clean();

	// geef de verkopersinterfacebalk weer, behalve als we een file
	// exporteren, of als het "bookweb financieel" betreft
	if($inlayout && $page != "financieel") {
		showClientBar();
	} elseif ($page == "financieel"){
		// Even lijntje tonen, anders zijn de velden van boekinfo zo gek
		echo "<hr>";
	}

	/* bok de output NA de clientbar */
	echo $bodytext;

	/* Plaats Sponsorlogo, als we geen files exporteren */
	if ($inlayout) {
		//bookweb_showfooter();
		Page::getInstance()->end();
	}
}

function bookweb_levelswitch()
{
	// FIXME
	if (FALSE && hasRealLevel('god', BOEKEN_DB)) {
		global $levels;

		echo "\n<hr>";
		startForm('setlevel','','','GET');
		echo "\n<tr><td><LABEL for=\"level\">Nieuw level:</LABEL></td>"
			."\n<td>".addSelect('newlevel',$levels,'lid')
			."\n<INPUT TYPE='hidden' name='database' value='" . BOEKEN_DB . "'>"
			."\n</td>";
		endForm("Stel nieuw level in",'',true);
	}
}

function bookweb_showfooter()
{
	echo sponsorLogoSW2();
}
