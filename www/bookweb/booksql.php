<?php
// $Id$

/*

Hier zitten *alle* BookWeb SQL-calls  (not)

*/

// hele coole functie
function sqlGetNextPrivateEAN($prefix) {
	global $BWDB;

	$max = $BWDB->q('VALUE SELECT MAX(EAN) FROM boeken '.
			'WHERE EAN LIKE "'.$prefix.'%%"');
	$newean = (int)substr($max, 3, 9)+1000000001;

	return ean_addcheck($prefix.substr($newean,1));
}

/**
	Retourneert het totaalbedrag aan verkopen aan een gegeven lid,
	gecorrigeerd met het bedrag aan terugkopen
*/
function sqlGetVerkopenBedrag($lidnr){
	global $BWDB;

	$verkoop = $BWDB->q("
		VALUE SELECT SUM(aantal * studprijs) as totaal
		FROM verkoop
		WHERE lidnr = %i", $lidnr);

	$terugkoop = $BWDB->q("
		VALUE SELECT SUM(aantal * studprijs) as totaal
		FROM terugkoop
		WHERE lidnr = %i", $lidnr);

	return ($verkoop - $terugkoop);
}


/**
	Retourneert alle ver- en terugkoopaantallen in een associatieve array met het
	voorraadnr as arraykey
*/
function sqlGetVerTerugkopen($van, $tot, $leveranciernrs = null, $orderby = "EAN"){
	global $BWDB;
	if (!$leveranciernrs) $leveranciernrs = array(BOEKLEVNR);

	$res = array();

	$allowed_orderby = array("EAN", "EAN, voorraadnr");
	if (!in_array($orderby, $allowed_orderby)) $orderby = "EAN";

	$verkopen = $BWDB->q("
		KEYTABLE SELECT
			verkoop.voorraadnr as ARRAYKEY, voorraad.*, SUM(aantal) as verkopen,
			voorraad.EAN as EAN, voorraad.levprijs as levprijs
		FROM verkoop
		LEFT JOIN voorraad USING(voorraadnr)
		WHERE wanneer >= %s
		AND wanneer <= %s
		AND leveranciernr IN (%Ai)
		GROUP BY verkoop.voorraadnr
		ORDER BY $orderby",
		$van, $tot, $leveranciernrs);

	$terugkopen = $BWDB->q("
		KEYTABLE SELECT
			terugkoop.voorraadnr as ARRAYKEY, voorraad.*, SUM(aantal) as terugkopen,
			voorraad.EAN as EAN, voorraad.levprijs as levprijs
		FROM terugkoop
		LEFT JOIN voorraad USING(voorraadnr)
		WHERE wanneer >= %s
		AND wanneer <= %s
		AND leveranciernr IN (%Ai)
		GROUP BY terugkoop.voorraadnr
		ORDER BY $orderby",
		$van, $tot, $leveranciernrs);

	$voorraadnrs = array_unique(array_merge(
		array_keys($verkopen),
		array_keys($terugkopen)
	));


	$voorraadvelden = array("EAN", "levprijs", "leveranciernr", "levprijs");
	foreach ($voorraadnrs as $voorraadnr){
		$aantalVerkopen = @$verkopen[$voorraadnr]["verkopen"];
		$aantalTerugkopen = @$terugkopen[$voorraadnr]["terugkopen"];

		foreach ($voorraadvelden as $veld){
			$value = @$verkopen[$voorraadnr][$veld];
			if (!$value) $value = @$terugkopen[$voorraadnr][$veld];
			$res[$voorraadnr][$veld] = $value;
		}

		$res[$voorraadnr]["verkopen"] = $aantalVerkopen;
		$res[$voorraadnr]["terugkopen"] = $aantalTerugkopen;
	}

	return $res;
}

// deze functie geeft me alle keren dat een EAN is verkocht aan een lidnr,
// wordt gebruikt voor terugkopen. (kink)
function sqlFindVerkopen($EAN, $lidnr) {
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT v.*, v.verkoopnr AS ARRAYKEY FROM verkoop v LEFT JOIN voorraad USING(voorraadnr)
			WHERE EAN = %s AND lidnr = %i',
			$EAN, $lidnr);
}


// zelfde als sqlFindVerkopen maar dan natuurlijk terugkopen,
function sqlFindTerugkopen($EAN, $lidnr) {
	global $BWDB;

	return $BWDB->q('TABLE SELECT t.* FROM terugkoop t LEFT JOIN voorraad USING (voorraadnr)
			WHERE EAN = %s AND lidnr = %i',
			$EAN, $lidnr);
}

function sqlGetVerkoopData($verkoopnr) {
	global $BWDB;

	return $BWDB->q('TUPLE SELECT * FROM verkoop WHERE verkoopnr = %i', $verkoopnr);
}

/*// geeft de ver/terugkoopaantallen voor een bepaald levernr
function sqlGetVerkoopRetourdataByLevering($levernr) {
	global $BWDB;

	$ret['verkoop']	= (int)$BWDB->q('VALUE SELECT sum(aantal) '.
					'FROM verkoop WHERE levernr = %i', $levernr);
	$ret['terugkoop'] = (int)$BWDB->q('VALUE SELECT sum(aantal) '.
					'FROM terugkoop WHERE levernr = %i', $levernr);
	$ret['retour'] = (int)$BWDB->q('VALUE SELECT sum(aantal) '.
					'FROM retour WHERE levernr = %i', $levernr);
	return $ret;
}*/

/* registreert een terugkoop en past de voorraad dienovereenkomstig aan */
function sqlInsTerugKoop($voorraadnr, $aantal, $plaats, $lidnr, $verkoopnr,
	$studprijs, $methode)
{
	global $BWDB, $BOEKENPLAATSEN;
	global $auth;

	Voorraad::checkVoorraadLocatie($plaats); // Geeft error bij ongeldige voorraadlocatie

	// Specifiek terugkopen naar reguliere voorraad, t.b.v. goede
	// administratie. Eventueel worden producten later verplaatst.
	$voorraad = new Voorraad($voorraadnr);
	$voorraad->incrVoorraad($aantal, "voorraad");
	$voorraad->store();

	// Terugkoop registreren in daarvoor bestemde tabel
	$BWDB->q('INSERT INTO terugkoop SET %S', array (
		'voorraadnr' => $voorraadnr,
		'aantal'	 => $aantal,
		'wanneer'    => getActionDateTime(),
		'lidnr'      => $lidnr,
		'verkoopnr'  => $verkoopnr,
		'studprijs'  => $studprijs,
		'wie'        => $auth->getLidnr(),
		'betaalmethode' => $methode)
	);

	if ($plaats != "voorraad") {
		// Terugkoop vindt niet plaats naar reguliere voorraad, dus moeten er
		// nog artikelen verplaatst worden...
		$voorraad->verplaats("voorraad", $plaats, $aantal, "Terugkoop naar $plaats");
	}
}

/* sla een kastelling op bij een reeds geopende rij in verkoopgeld en
 * vul sluitstamp in; m.a.w. SluitBoekverkoop().
 * Als de rij echter nog NIET gesloten is, strepen we 'm door, voegen een
 * gecorrigeerde versie toe */
function sqlInsKastelling($verkoopdag,$cash,$pin1,$pin2,$debiteuren,$crediteuren,
	$mutaties,$donaties,$kasverschil, $kv_reden ="")
{
	global $BWDB;

	$ouderij = sqlGetVerkoopGeld($verkoopdag);

	$newvalues = array();

	$newvalues['cash'] = $cash;
	$newvalues['pin1'] = $pin1;
	$newvalues['pin2'] = $pin2;
	$newvalues['chip'] = '0.00'; //Chippen is gewoon een pininkomst
	$newvalues['debiteuren'] = $debiteuren;
	$newvalues['crediteuren'] = $crediteuren;
	$newvalues['donaties'] = $donaties;
	$newvalues['mutaties'] = $mutaties;
	$newvalues['kasverschil'] = $kasverschil;
	$newvalues['kv_reden'] = "Onverklaard kasverschil: ". $kv_reden;
	
	if ($ouderij && $ouderij['sluitstamp']) {
		// er is al iets ingevuld, dus halen we niks weg!

		$BWDB->q('UPDATE verkoopgeld SET %S
				WHERE verkoopdag=%s AND geldigtot IS NULL'
			, array('geldigtot' => strftime('%Y-%m-%d %H:%M:%S'))
			, $verkoopdag
			);

		// zorg dat in de nieuwe rij ook de verkoper&openstamp worden
		// overgenomen
		$newvalues['verkoopdag'] = $verkoopdag;
		$newvalues['verkoper'] = $ouderij['verkoper'];
		$newvalues['openstamp'] = $ouderij['openstamp'];
		$newvalues['sluitstamp'] = $ouderij['sluitstamp'];

		$BWDB->q('INSERT INTO verkoopgeld SET %S', $newvalues);
		return;
	}

	// als er nog niks is ingevuld, zet sluitstamp op NU
	// (niet actiondatetime, dit wil je wel correct houden)
	$newvalues['sluitstamp'] = strftime('%Y-%m-%d %H:%M:%S');

	$BWDB->q('UPDATE verkoopgeld SET %S
		WHERE verkoopdag=%s AND geldigtot IS NULL'
		, $newvalues
		, $verkoopdag);
}

/* wis een kastelling, in essentie een heropening van de boekverkoop.
 * deze functie vult de "geldigtot" kolom in van een verkoopdag en
 * voegt een nieuwe rij in zonder de kastellinggegevens. */
function sqlWisKastelling($verkoopdag)
{
	global $BWDB;

	$row = $BWDB->q('TUPLE SELECT * FROM verkoopgeld
		WHERE geldigtot IS NULL AND verkoopdag=%s'
		, $verkoopdag);
	$BWDB->q('UPDATE verkoopgeld SET %S
		WHERE verkoopdag=%s AND geldigtot IS NULL'
		, array('geldigtot' => strftime('%Y-%m-%d %H:%M:%S'))
		, $verkoopdag);
	$BWDB->q('INSERT verkoopgeld SET %S',
		array('verkoopdag'=>$row['verkoopdag'],
		'verkoper'=>$row['verkoper'],
		'openstamp'=>$row['openstamp']));
}

//Geef een 2-dim array met besteldata a.d.h.v. onverplichte invoer
function sqlZoekbestelling($EAN, $datum)
{
	global $BWDB;
	$orlist = '';

	if ($EAN) {
		$orlist .= ' `b`.`ean` = %s';
	} else {
		$orlist .= ' %_ ';
	}
	if ($datum) {
		$orlist .= ' `datum` = %s';
	} else {
		$orlist .= ' %_ ';
	}

	$result = $BWDB->q("TABLE SELECT `levbestelnr` AS bestelnr"
		.", `titel`"
		.", `auteur`"
		.", `b`.`EAN` as EAN"
		.", `aantal`"
		.", `aantalgeleverd`"
		." FROM `boeken` b, `bestelling` bs"
		." WHERE 1=1 AND ( " . $orlist . " )"
		." AND `b`.`EAN` = `bs`.`EAN`"
		." AND `aantalgeleverd` < `aantal`"
		, $ean
		, $datum);
	return $result;
}

function sqlChangeBoekVak($boekvaknr,$EAN)
{
	global $BWDB;

	$BWDB->q('UPDATE boekvak SET %S WHERE boekvaknr=%i'
		, array('EAN' => $EAN)
		, $boekvaknr);
	$BWDB->q('UPDATE studentbestelling SET %S WHERE boekvaknr=%i'
		, array('EAN' => $EAN)
		, $boekvaknr);
}

/* geeft de boeken die op een bepaalde dag ver/terugkocht zijn
 * array met twee keys: verkoop en terugkoop
 * elk heeft een array met daarin boeken die ver/teruggekocht zijn:
 * EAN,auteur,titel,aantal,studprijs,totaal
 *
 * $verkopers is een array van verkopers (lidnrs) waarvan de verkopen
 * getoond moeten worden. Null betekent alle verkopers tonen.
 */
function sqlVerTeruggekocht($verkoopdag, $verkopers = null)
{

	// let op: alle verkopen OP 23:59:59 worden niet meegenomen.
	$events = finBookEvents($verkoopdag.' 00:00:00',$verkoopdag.' 23:59:59', null, null, $verkopers);
	$boekinfo = sqlBoekenGegevens(unzip(array_values($events),'EAN'));
	$ret = array('verkoop' => array(), 'terugkoop' => array());

	foreach($events as $event) {
		foreach(array('verkoop','terugkoop') as $type) {
			foreach ($event[$type] as $studprijs => $aantal) {
				$ret[$type][] = array(
					'EAN' => $event['EAN'],
					'auteur' => $boekinfo[$event['EAN']]['auteur'],
					'titel' => $boekinfo[$event['EAN']]['titel'],
					'aantal' => $aantal,
					'studprijs' => $studprijs,
					'btw' => $event['btw'],
					'totaal' => $aantal * $studprijs,
					'totaalbtw' => $aantal * $event['btw']
				);
			}
		}
	}

	return $ret;
}

// geeft info over hoeveel boeken er nog moeten komen voor een bestelling
function sqlFindlevbestelnr($EAN,$leveranciernr)
{
	global $BWDB;

	return $BWDB->q('TABLE SELECT levbestelnr, datum,
		aantal, aantal-aantalgeleverd AS nognodig
		FROM bestelling
		WHERE EAN=%s AND aantalgeleverd<aantal AND leveranciernr=%i',
		$EAN, $leveranciernr);
}

// voer boekennieuws in voor een gegeven EAN
function sqlInsNieuws($EAN, $text)
{
	global $BWDB;
	global $auth;

	$BWDB->q("INSERT INTO `nieuws`"
		." SET `EAN` = %s"
		.	", `nieuws` = %s"
		.	", `wie` = %s"
		.	", `wanneer` = %s"
		, $EAN
		, $text
		, $auth->getLidnr()
		, getActionDateTime());
}

/* open een nieuwe boekverkoopdag */
function sqlNieuweVerkoop($lidnr)
{
	global $BWDB;

	$BWDB->q("INSERT INTO `verkoopgeld`"
		." SET `verkoper` = %s"
		.	", `openstamp` = %s"
		.	", `verkoopdag` = %s"
		, $lidnr
		, getActionDateTime()
		, getActionDate());
}

/* haalt de gegevens op van de meest recente verkoopdag */
function sqlLastVerkoop()
{
	global $BWDB;

	return $BWDB->q("TUPLE SELECT * FROM `verkoopgeld`"
		." WHERE `geldigtot` IS NULL ORDER BY `verkoopdag` DESC LIMIT 1");
}

// vraag de verkoopgeld-totalen op van een bepaalde dag
function sqlGetVerkoopGeld($verkoopdag)
{
	global $BWDB;

	return $BWDB->q('MAYBETUPLE SELECT * FROM verkoopgeld
		WHERE geldigtot IS NULL AND verkoopdag=%s', $verkoopdag);
}

/* Vraag de verklaarde kasverschillen op van een bepaalde dag */
function sqlGetVerklaardeKasverschillen($verkoopdag, $totaalbedrag = FALSE)
{
	global $BWDB;

	return $BWDB->q(
		($totaalbedrag?'MAYBEVALUE SELECT sum(kasverschil) as kasverschil':
			'KEYTABLE SELECT *, volgnr as ARRAYKEY').'
		FROM kasverschil
		WHERE geldigtot IS NULL AND verkoopdag=%s'.
		($totaalbedrag?' GROUP BY verkoopdag':''), $verkoopdag);
}

/* Voeg een kasverschil toe van een bepaalde dag */
function sqlInsertKasverschil($verkoopdag, $verschildata)
{
	global $BWDB;

	$BWDB->q('INSERT INTO kasverschil SET %S', array
		('verkoopdag' => $verkoopdag
		,'kasverschil' => $verschildata['kasverschil']
		,'reden'       => $verschildata['reden']
		,'wie'         => $verschildata['wie']));
}

/* Verwijder een kasverschil van een bepaalde dag */
function sqlDeleteKasverschillen($verkoopdag, $volgnrs)
{
	global $BWDB;

	$BWDB->q('UPDATE kasverschil
		SET geldigtot = now()
		WHERE verkoopdag = %s AND volgnr IN (%Ai)', $verkoopdag, $volgnrs);
}

/**
 * Gegeven een set voorraadnrs, geef alle info daarover om bonnen te kunnen
 * printen.
 */
function sqlBonnen($voorraadnrs)
{
	global $BWDB;

	return $BWDB->q('TABLE SELECT voorraad, titel, auteur, adviesprijs,
			levprijs, boekenmarge, voorraadnr
		FROM voorraad v NATURAL JOIN boeken, leverancier l
		WHERE voorraadnr IN (%Ai) AND v.leveranciernr=l.leveranciernr',
		$voorraadnrs);
}

// Returnt FALSE als er onvoldoende boeken waren bij dit voorraadnr
function sqlInsRetour($voorraadnr,$plaats,$aantal,$factuurnr)
{
	global $BWDB, $BOEKENPLAATSEN;
	global $auth;

	Voorraad::checkVoorraadLocatie($plaats); // geeft error bij ongeldige voorraadlocatie

	$voorraad = new Voorraad($voorraadnr);

	if ($plaats != 'voorraad') {
		/* Het betreft hier eigenlijk een retourzending uit $orig_plaats */
		$voorraad->verplaats($plaats, "voorraad", $aantal);
	}

	$voorraad->decrVoorraad($aantal);
	$voorraad->store();

	$BWDB->q("INSERT INTO `retour`"
		." SET `factuurnr` = %s"
		.	", `voorraadnr` = %s"
		.	", `aantal` = %s"
		.	", `wanneer` = %s"
		.	", `wie` = %s"
		, $factuurnr
		, $voorraadnr
		, $aantal
		, getActionDateTime()
		, $auth->getLidnr());

	return true;
}


/**
 * Een levering invoeren.
 * Geen 10e argument: levering niet op bestelling
 * Returnt FALSE als er onvoldoende boeken waren bij deze bestelling
 *
 * TODO: transactions
 */

function sqlInsLevering($EAN, $plaats, $aantal, $levprijs, $adviesprijs, $btwtarief, $btw, $factuur,
	$leveranciernr, $levbestelnr = null)
{

	global $BWDB, $BOEKENPLAATSEN;
	global $auth;
	Voorraad::checkVoorraadLocatie($plaats); // geeft error bij ongeldige voorraadlocatie

	if (!sqlBoekGegevens($EAN)) {
		user_error("Deze EAN ($EAN) is onbekend", E_USER_ERROR);
	}

	/* Het betreft hier eigenlijk een levering naar $orig_plaats */
	if ($plaats != 'voorraad') {
		$orig_plaats = $plaats;
		$plaats = 'voorraad';
	}

	if ($levbestelnr) {
		if (!$BWDB->q('RETURNAFFECTED UPDATE bestelling
			 SET aantalgeleverd = aantalgeleverd + %i
			 WHERE levbestelnr = %i
			 	AND aantal >= aantalgeleverd+%i',
			$aantal, $levbestelnr, $aantal)) {

			return FALSE;
		}
	}

	$voorraadnr = $BWDB->q('MAYBEVALUE SELECT voorraadnr FROM voorraad
		WHERE leveranciernr=%s AND levprijs=%s AND adviesprijs=%s AND EAN=%s AND btwtarief=%s' . (!empty($adviesprijs)?' AND adviesprijs=%s':''),
		$leveranciernr,	$levprijs,$adviesprijs, $EAN, $btwtarief, $adviesprijs);

	if($voorraadnr) {
		// er bestaat al een voorraad van deze EAN,leverancier,btwtarief,prijs -> update
		$BWDB->q("UPDATE voorraad SET $plaats = $plaats + %i
			WHERE voorraadnr = %i",
			$aantal, $voorraadnr);
	} else {
		// anders nieuwe voorraadregel toevoegen
		$voorraadnr = $BWDB->q('RETURNID INSERT INTO voorraad SET %S',
			array('EAN' => $EAN
				, 'levprijs' => $levprijs
				, 'adviesprijs' => $adviesprijs
				, 'btwtarief' => $btwtarief
				, 'btw' => $btw
				, $plaats => $aantal
				, 'leveranciernr' => $leveranciernr));
	}

	$BWDB->q('INSERT INTO levering SET %S',
		array('voorraadnr' => $voorraadnr
			, 'aantal' => $aantal
			, 'factuur' => $factuur
			, 'levbestelnr' => $levbestelnr
			, 'wanneer' => getActionDateTime()
			, 'wie' => $auth->getLidnr()
			, 'invoerdatum' => strftime('%Y-%m-%d %H:%M:%S') ));


	/* Het betreft hier eigenlijk een levering naar $orig_plaats */
	if (isset($orig_plaats)) {
		$voorraad = new Voorraad($voorraadnr);
		$voorraad->verplaats("voorraad", $orig_plaats, $aantal, "Levering naar $orig_plaats");
	}

	return TRUE;
}

// geef een overzicht van de huidge boekenvoorraad. sum() van 0 rijen levert
// NULL ipv 0, vandaar dat er soms niks staat, ipv een '0'. IMO is dat een bug
// in MySQL --jeroen
// param telling: als true dan worden alleen alle telbare boeken weergegeven.
// param metprijs: als true dan wordt de prijs ook vermeld.
// dit heeft tot gevolg dat er voor een boek meerdere rows in kunnen
// zitten (als er verschillende prijzen zijn voor 1 boek). Bij telling is
// dat juist ongewenst, dus dan is metprijs FALSE zodat er per boek altijd
// precies 1 row is met het totaal voor dat EAN. (kink)
// param leveranciernrs: alleen voorraad van de gegeven leveranciernrs retourneren (sjeik)
// param datum: voorraad op het gegeven moment retourneren, i.p.v. "nu"
function sqlVoorraadLijst($telling = FALSE, $metprijs = TRUE, $leveranciernrs = null, $datum = null)
{
	global $BWDB;

	if ($datum == null){
		// simpele query uit voorraadtabel
		return $BWDB->q('TABLE SELECT voorraadnr, sum(voorraad) as voorraad, sum(buitenvk) as buitenvk, sum(ejbv_voorraad) as ejbv_voorraad, '.
			 ($metprijs ? 'adviesprijs, levprijs, boekenmarge, ':'').
			 'b.*, b.EAN '.
			 'FROM voorraad v NATURAL JOIN boeken b, leverancier l '.
			 'WHERE (voorraad != 0 or buitenvk != 0 or ejbv_voorraad != 0)
				 AND l.leveranciernr = v.leveranciernr '.
			 ($leveranciernrs && sizeof($leveranciernrs) > 0 ? "AND l.leveranciernr IN (%Ai) ":"").
			 ($telling ? 'AND b.telbaar = \'Y\' ':'').
			 'GROUP BY v.EAN'.
			 ($metprijs ? ', adviesprijs ':' ').
			 'ORDER BY v.leveranciernr, auteur ',

			 $leveranciernrs);
	} else {
		// blijkbaar wil de gebruiker een andere datum hebben. Zucht, dat wordt veel werk!

		$voorraadnrs = array();
		$voorraadnrs = array_merge($voorraadnrs,
			$BWDB->q("
			COLUMN SELECT DISTINCT voorraadnr
			FROM levering
			WHERE wanneer > %s", $datum));

		$voorraadnrs = array_merge($voorraadnrs,
			$BWDB->q("
			COLUMN SELECT DISTINCT voorraadnr
			FROM retour
			WHERE wanneer > %s", $datum));

		$voorraadnrs = array_merge($voorraadnrs,
			$BWDB->q("
			COLUMN SELECT DISTINCT voorraadnr
			FROM verkoop
			WHERE wanneer > %s", $datum));

		$voorraadnrs = array_merge($voorraadnrs,
			$BWDB->q("
			COLUMN SELECT DISTINCT voorraadnr
			FROM terugkoop
			WHERE wanneer > %s", $datum));

		$voorraadnrs = array_merge($voorraadnrs,
			$BWDB->q("
			COLUMN SELECT DISTINCT voorraadnr
			FROM voorraad
			WHERE voorraad != 0 OR buitenvk != 0"));
		$voorraadnrs = array_unique($voorraadnrs);

		$voorraadnrs = $BWDB->q("
			COLUMN SELECT DISTINCT voorraadnr
			FROM voorraad
			WHERE leveranciernr IN (%Ai)
			AND voorraadnr IN (%Ai)
			ORDER BY EAN", $leveranciernrs, $voorraadnrs);

		// Informatie over gegeven voorraadnrs opsnorren
		$voorraadInfo = $BWDB->q("
			KEYTABLE SELECT voorraadnr as ARRAYKEY,
				boeken.*, adviesprijs, levprijs, boekenmarge
			FROM voorraad
			LEFT JOIN boeken ON (voorraad.EAN = boeken.EAN)
			LEFT JOIN leverancier ON (voorraad.leveranciernr = leverancier.leveranciernr)
			WHERE voorraadnr IN (%Ai)
			ORDER BY voorraad.EAN", $voorraadnrs);

		if (sizeof($voorraadInfo) == 0) return array();

		// Van de geselecteerde voorraadnrs alle leveringen opsnorren
		$leveringen = $BWDB->q("
			KEYTABLE SELECT SUM(aantal) as aantal, voorraadnr as ARRAYKEY
			FROM levering
			WHERE wanneer <= %s
			AND voorraadnr IN (%Ai)
			GROUP BY voorraadnr",
			$datum, $voorraadnrs);

		$retouren = $BWDB->q("
			KEYTABLE SELECT SUM(aantal) as aantal, voorraadnr as ARRAYKEY
			FROM retour
			WHERE wanneer <= %s
			AND voorraadnr IN (%Ai)
			GROUP BY voorraadnr",
			$datum, $voorraadnrs);

		$verkopen = $BWDB->q("
			KEYTABLE SELECT SUM(aantal) as aantal, voorraadnr as ARRAYKEY
			FROM verkoop
			WHERE wanneer <= %s
			AND voorraadnr IN (%Ai)
			GROUP BY voorraadnr",
			$datum, $voorraadnrs);

		$terugkopen = $BWDB->q("
			KEYTABLE SELECT SUM(aantal) as aantal, voorraadnr as ARRAYKEY
			FROM terugkoop
			WHERE wanneer <= %s
			AND voorraadnr IN (%Ai)
			GROUP BY voorraadnr",
			$datum, $voorraadnrs);

		$res = array();
		foreach ($voorraadInfo as $voorraadnr=>$info){
			$netto = @($leveringen[$voorraadnr]["aantal"] +
				$terugkopen[$voorraadnr]["aantal"] -
				$retouren[$voorraadnr]["aantal"] -
				$verkopen[$voorraadnr]["aantal"]);

			if ($netto != 0){
				$res[$voorraadnr]["voorraad"] = null;
				$res[$voorraadnr]["buitenvk"] = null;
				$res[$voorraadnr]["aantal"] = $netto;
				$res[$voorraadnr]["EAN"] = $voorraadInfo[$voorraadnr]["EAN"];
				$res[$voorraadnr]["auteur"] = $voorraadInfo[$voorraadnr]["auteur"];
				$res[$voorraadnr]["titel"] = $voorraadInfo[$voorraadnr]["titel"];
				$res[$voorraadnr]["boekenmarge"] = $voorraadInfo[$voorraadnr]["boekenmarge"];
				$res[$voorraadnr]["levprijs"] = $voorraadInfo[$voorraadnr]["levprijs"];
			} // else: voorraad = 0, niet opnemen in resultaat
		}

		return $res;
	}
}

/* kink: een nieuwe telling, geeft het tellingnr terug.
 * lidnr: het lid dat de telling heeft uitgevoerd.
 * opm: eventuele opmerkigen ingevoegd door de teller
 */
function sqlCreateTelling($lidnr, $opm = null, $telling_start = null)
{
	global $BWDB;
	global $auth;

	if(!trim($opm)) $opm = null;

	return $BWDB->q("RETURNID INSERT INTO `telling` SET %S"
		, array('lidnr' => $auth->getLidnr(),
			'telling_start' => $telling_start,
			'wanneer' => getActionDateTime(),
			'opmerkingen' => $opm));
}

/* Insert getelde aantallen van boeken in $data
 * (EAN => { voorraad => int, buitenvk => int} )
 * De cast naar int is ervoor om van lege values 0 te maken.
 * -- kink */
function sqlInsertTellingItems($tellingnr, $data)
{
	global $BWDB;

	foreach($data as $EAN => $aantallen) {
		$BWDB->q("INSERT INTO `tellingitem` SET %S",
			array('tellingnr' => $tellingnr, 'EAN' => $EAN,
				'aantal_voorraad' => ((int)trim($aantallen['voorraad'])),
				'aantal_buitenvk' => ((int)trim($aantallen['buitenvk'])) ));

	}
}

/* kink: aan het begin van de maand moet een telling geforceerd worden.
 * deze functie returnt true als het moment daar is.
 */
function sqlForceTelling()
{
	global $BWDB;

	$res = $BWDB->q("MAYBEVALUE SELECT `tellingnr` FROM `telling`"
		." WHERE `wanneer` > %s LIMIT 1", date('Y-m') . '-01 00:00:00');

	return is_null($res);
}

function sqlGetTellingen($datum) {
	global $BWDB;
	return $BWDB->q('TABLE SELECT * FROM telling '.
		'WHERE wanneer >= %s AND wanneer <= %s',
		$datum.' 00:00:00', $datum.' 23:59:59');
}


// geef van (een) bepaald(e) boek(en) zo ongeveer alle totaalinformatie, op
// ongeveer alle mogelijke manieren berekend --jeroen
// totaalinformatie == hoeveel boeken er in elk van de fasen zitten

// het argument $EANs MOET een array zijn van EANs zijn!

// het formaat is een array(EAN=>info) waarbij info een array is (boek_{veld}
// => waarde, fase_{id}=>aantal, {tabel}_{fases}=>aantal --jacob

// TODO: ook echte variabelenstructuur aanpassen aan voorraad/levering
// splitsing, maakt het wel wat consequenter (maar, is niet echt belangrijk
// aangezien voor deze functie het niet uitmaakt in principe) --Jeroen

function sqlAlleFasenInfo($EANs=NULL)
{
	if ($EANs && !is_array($EANs)) user_error('Argument van sqlAlleFasenInfo moet een array zijn!', E_USER_ERROR);

	/* Uitleg: er zijn 9 verschillende 'states' die een boek kan hebben, nl:

	 1: Boek besteld door student, maar nog niet geleverd
	 2: Hij is door A-Eskwadraat besteld bij een leverancier
	 3: Hij is geleverd aan A-Eskwadraat --> is alpha (geleverd, maar nog niet
	    'toegekend'
	 4: De student heeft bericht gehad om hem te kunnen komen ophalen
	 5: De student heeft 'm gekoct
	 L: (los) boek is los gekocht
	 a: (alpha) het boek is bij a-eskwadraat, maar op dit moment niet aan een
		bestelling gekoppeld (losse verkoop boeken, en boeken die nog even
		'zweven' totdat ze '4'(oid) worden
	 e: (voorraad_ejbv) gereserveerd voor EJBV, mag niet toegewezen worden
	 	aan studenten met een bestelling. Blijft dus vrije voorraad in fase 3.
	 g: (buitenvk) klaar voor retour naar de leverancier, niet in de verkoop
	 r: (retour) boek is niet meer bij a-eskwadraat, maar terug bij de
	    leverancier

			1	2	3	4	5	L	t	r	g	e
-------------------------------------------------
sb			*	*	*			 	 				(niet gemaild)
						*		 	 				(gemaild, niet verkocht)
							*	 	 				(gekocht)
-------------------------------------------------
bestelling		*	*	*	*	*	- 	*	*	*	(aantal)
					*	*	*	*	- 	*	*	*	(aantalgeleverd)
-------------------------------------------------
voorraad			*	*		 	 				(voorraad)
								 	 		*		(buitenvk)
					*							*	(voorraad_ejbv)
-------------------------------------------------
levering			*	*	*	*	- 	*	*	*	(aantal)
-------------------------------------------------
retour							 	 	*			(aantal)
-------------------------------------------------
verkoop						*	*	 				(alles)
-------------------------------------------------
terugkoop						 	*				(count)
-------------------------------------------------
realiteit			*	*		 	 				(voorraad)
								 	 		*		(buitenvk)
												*	(voorraad_ejbv)
-------------------------------------------------


Terugkoop (definitie):
- A-Eskwadraat koopt een boek terug van een lid (bijv omdat het beschadigd is)
- BELANGRIJK: terugkoop is niet alleen maar administratief, bij bestelling en
  levering moet je nl. weten hoeveel er verkocht zijn maar nog niet
  teruggekocht.

Retour (definitie)
- A-Eskwadraat stuurt (overtollige) boeken terug naar de leverancier (vanuit
  buiten verkoop doorgaans)

	 */

	$selectall = $EANs === NULL; // of je alle boeken wilt hebben

	$where = $EANs ? 'EAN in (%As)' : '1';

	global $BWDB;

	$boeken = $BWDB->q("TABLE SELECT
				    EAN,
				 	titel    as boek_titel,
					auteur   as boek_auteur,
					druk     as boek_druk,
					uitgever as boek_uitgever,
					bestelbaar as boek_bestelbaar,
					telbaar as boek_telbaar
				FROM boeken WHERE $where ORDER BY auteur, titel, EAN", $EANs);

	$vars = array(); // een array <table> => array-of-fields die teruggegeven
					 // moeten worden.

	// 'sb' = studentbestelling
	$vars['sb'] = array('sb_123' ,'sb_4' ,'sb_5' ,'sb_verlopen');
	$sb = $BWDB->q("KEYTABLE SELECT
					EAN as ARRAYKEY,
					 sum(gemailddatum is null AND
						 gekochtdatum is null AND
						 (vervaldatum is null OR
						  TO_DAYS(vervaldatum) > TO_DAYS(now())
						 )) as sb_123,
					 sum(gemailddatum is not null AND
						 gekochtdatum is null AND
						 (vervaldatum is null OR
						  TO_DAYS(vervaldatum) > TO_DAYS(now())
						 )) as sb_4,
					 sum(gekochtdatum is not null AND
						 vervaldatum is null AND
						 vervalreden is NULL) as sb_5,
					 sum(TO_DAYS(vervaldatum) <= TO_DAYS(now()) AND
					 	 vervalreden = 'Verlopen') as sb_verlopen
					FROM studentbestelling WHERE $where GROUP BY EAN", $EANs);

	// 'vr' = voorraad
	// vr_34 = reguliere voorraad
	//		(dus in fase 3 of 4 = geleverd maar nog niet toegekend +
	//		geleverd en toegekend. EJBV-voorraad wordt niet meegeteld!)
	// vr_g = voorraad buiten verkoop
	// vr_e = voorraad t.b.v. EJBV
	$vars['vr'] = array('vr_34' ,'vr_g', 'vr_e');
	$vr = $BWDB->q("KEYTABLE SELECT
					 EAN as ARRAYKEY,
					 sum(voorraad) as vr_34,
					 sum(buitenvk) as vr_g,
					 sum(ejbv_voorraad) as vr_e
					 FROM voorraad
					 WHERE $where
					 GROUP BY EAN", $EANs);

	// 'lev' = levering
	$vars['lev'] = array('lev_345Ltgr');
	$lev = $BWDB->q("KEYTABLE SELECT
					 EAN as ARRAYKEY,
					 sum(aantal)   as lev_345Ltgr
					 FROM voorraad INNER JOIN levering USING (voorraadnr)
					 WHERE $where
					 GROUP BY EAN", $EANs);

	$vars['best'] = array('best_2345Ltgr' ,'best_345Ltgr');
	$best = $BWDB->q("KEYTABLE SELECT
					 EAN as ARRAYKEY,
					 sum(aantal)         as best_2345Ltgr,
					 sum(aantalgeleverd) as best_345Ltgr
					 FROM bestelling
					 WHERE $where
					 GROUP BY EAN", $EANs);

	$vars['vk'] = array('vk_5L');
	$vk = $BWDB->q("KEYTABLE SELECT EAN as ARRAYKEY, sum(v.aantal) as vk_5L
					FROM voorraad INNER JOIN verkoop v USING (voorraadnr)
					WHERE $where
					GROUP BY EAN", $EANs);

	$vars['tk'] = array('tk_t');
	$tk = $BWDB->q("KEYTABLE SELECT EAN as ARRAYKEY, sum(tk.aantal) as tk_t
					FROM voorraad INNER JOIN terugkoop tk USING (voorraadnr)
					WHERE $where
					GROUP BY EAN", $EANs);

	$vars['retour'] = array('retour_r');
	$retour = $BWDB->q("KEYTABLE SELECT EAN as ARRAYKEY, sum(r.aantal) as retour_r
					FROM voorraad INNER JOIN retour r USING (voorraadnr)
					WHERE $where
					GROUP BY EAN", $EANs);

	$result = array();
	$EANs  = array();

	foreach($boeken as $tuple) {
		$EANs[] = $tuple['EAN'];
		$result[$tuple['EAN']]['boek_titel']  = $tuple['boek_titel'];
		$result[$tuple['EAN']]['boek_auteur'] = $tuple['boek_auteur'];
		$result[$tuple['EAN']]['boek_druk']   =@$tuple['boek_druk'];
		$result[$tuple['EAN']]['boek_uitgever']   =@$tuple['boek_uitgever'];
		$result[$tuple['EAN']]['boek_bestelbaar'] = $tuple['boek_bestelbaar'];
		$result[$tuple['EAN']]['boek_telbaar'] = $tuple['boek_telbaar'];
	}

	// hier wordt het resultaat in 1 grote array gegooid
	foreach ($vars as $table => $fields) {
		foreach ($fields as $field) {
			foreach ($EANs as $EAN) {
				$result[$EAN][$field] = (int)@${$table}[$EAN][$field];
			}
		}
		unset(${$table});
	}
	foreach ($EANs as $EAN) {
		// Fase 2 = door A-Eskwadraat besteld
		$result[$EAN]['fase_2'] = $result[$EAN]['best_2345Ltgr']
								 - $result[$EAN]['best_345Ltgr'];

		// Fase 4 = geleverd aan A-Eskwadraat (en dus op voorraad), maar
		// inmiddels toegewezen aan een student. Niet voor vrije verkoop dus.
		$result[$EAN]['fase_4'] = $result[$EAN]['sb_4'];

		// Fase 3 = geleverd aan A-Eskwadraat maar (nog) niet toegewezen aan
		// een student. Ook boeken op EJBV-voorraad vallen hieronder!
		$result[$EAN]['fase_3'] = $result[$EAN]['vr_34']
									+ $result[$EAN]['vr_e']
									- $result[$EAN]['fase_4'];

		// Fase 1 = besteld door student, nog niet geleverd of besteld bij leverancier
		$result[$EAN]['fase_1'] = $result[$EAN]['sb_123']
								 - $result[$EAN]['fase_2']
								 - ($result[$EAN]['fase_3'] - $result[$EAN]['vr_e']);

		// Fase 5 = gekocht door een student
		$result[$EAN]['fase_5'] = $result[$EAN]['sb_5'];

		// Fase L = verkoop van los boek
		$result[$EAN]['fase_L'] = $result[$EAN]['vk_5L'] - $result[$EAN]['fase_5'];

		// Fase r = boek is retour naar leverancier
		$result[$EAN]['fase_r'] = $result[$EAN]['retour_r'];

		// Fase t = boek is teruggekocht
		$result[$EAN]['fase_t'] = $result[$EAN]['tk_t'];

		// Fase g = boek ligt op locatie 'buiten verkoop' en kan dus op geen
		// enkele manier verkocht worden
		$result[$EAN]['fase_g'] = $result[$EAN]['vr_g'];

		// Fase e = boek is gereserveerd voor EJBV en zal dus nimmer
		// automatisch naar fase 4 (koppeling aan student) gestuurd worden door
		// het aanroepen van ProcessTo4
		$result[$EAN]['fase_e'] = $result[$EAN]['vr_e'];
	}

	return $result;
}

/* On-lever een bestelling, bijvoorbeeld omdat op het moment supreme (het boek
 * gaat verkocht worden!) het boek er toch niet blijkt te zijn */
function sqlOnlever($bestelnr, $prio = 0) {
	global $BWDB;

	$BWDB->q("UPDATE `studentbestelling` SET %S WHERE `bestlnr` = %s"
		, array('vervaldatum' => null
			,'vervalreden' => null
			,'gemailddatum' => null
			,'prioriteit' => $prio)
		, $bestelnr);
}

// alle niet-geleverde studentbestellingen van EAN
function sqlFindOpenSBs($EAN) {
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT bestelnr as ARRAYKEY,
		lidnr FROM studentbestelling WHERE
		vervaldatum IS NULL AND gemailddatum IS NULL
		AND EAN = %s', $EAN);
}

/* Alle nog niet gekochte studentbestellingen van een EAN.
 * $alleengeleverd geeft aan of ook de nog niet geleverde artikelen
 * geretourneerd dienen te worden. */
function sqlNogNietGekochteSBs($EAN, $alleengeleverd = FALSE) {
	global $BWDB;

	return $BWDB->q('TABLE SELECT *
		FROM studentbestelling
		WHERE (vervaldatum IS NULL OR TO_DAYS(vervaldatum) > TO_DAYS(now()))
			AND gekochtdatum IS NULL '
			.($alleengeleverd?'AND gemailddatum IS NOT NULL ':'')
			.'AND EAN=%s
		ORDER BY prioriteit DESC, bestelddatum ASC', $EAN);
}

/* Annuleer de bestelling van de student. Als reden wordt opgegeven wie de
 * bestelling geannuleerd heeft. $bestelnr kan int of array zijn. */
function sqlBestelAf($bestelnr, $afbesteller, $reden = 'reden onbekend') {
	global $BWDB;

	$BWDB->q("UPDATE `studentbestelling` SET %S WHERE `bestelnr` = %s"
		, array('vervaldatum' => date('Y-m-d')
			,'vervalreden' => 'Geannuleerd door '.$afbesteller.': '.$reden)
		, $bestelnr);
}

/**
 * Wijzig een vervaldatum. (kink)
 */
function sqlUpdateVervaldatum($bestelnr, $verval) {
	global $BWDB;

	$BWDB->q("UPDATE `studentbestelling` SET %S WHERE `bestlnr` = %s"
		, array ('vervaldatum' => $verval)
		, $bestelnr);
}

/* kink: geeft de te bestellen boeken gebruikt hierbij de tabellen studievak en
 * boekvak om het vak voor een boek weer te geven). De boeken moeten aan een
 * vak gekoppeld zijn om weergegeven te worden. Er wordt gecheckt of de einddatum
 * van het vak (1) niet sowieso al verstreken is en (2) binnen de begin/eind van
 * de geselecteerde periode valt.
 * Offerteprijs wordt apart opgezocht.
 */
function sqlBoeken($coljaar, $studiejaar, $studie, $begindatum, $einddatum) {
	global $BWDB;

	$rows = $BWDB->q('TABLE SELECT boekvaknr,titel,auteur,druk,b.EAN as EAN,
				v.vaknr, naam as vak, verplicht, sv.studie, sv.jaar, bestelbaar
			FROM vak v
			INNER JOIN studievak sv USING(vaknr)
			INNER JOIN boekvak bv USING(vaknr)
			INNER JOIN boeken b USING(EAN)
			WHERE v.collegejaar = %s
			AND TO_DAYS(v.einddatum) >= TO_DAYS(%s)
			' .
			($begindatum ?
				"AND TO_DAYS(v.einddatum) >= TO_DAYS('$begindatum') ":'').
	        ($einddatum ?
				"AND TO_DAYS(v.begindatum) < TO_DAYS('$einddatum') ":'').
			($studie ?
				"AND sv.studie = '".mysql_real_escape_string($studie)."' ":'').
			($studiejaar ?
				"AND sv.jaar = '".mysql_real_escape_string($studiejaar)."' ":'').
			" ORDER BY vak ", $coljaar, getActionDateTime());
			;

	// wat gebeurt hier precies? is deze extra loop nodig?
	$result = array();
	foreach ($rows as $row) {
		if(!isset($result[$row['vaknr']][$row['boekvaknr']])) {
			$result[$row['vaknr']][$row['boekvaknr']] = $row;
		}
	}

	return $result;
}

/**
 * Een lijst met alle artikelen die door een lid besteld kunnen worden.
 */
function sqlAlleBestelbareArtikelen()
{
	global $BWDB;
	return $BWDB->q('KEYTABLE SELECT *, EAN AS ARRAYKEY '
			.'FROM boeken '
			.'WHERE bestelbaar = "Y" ORDER BY auteur');
}

// geeft alle studenten die een boekvaknr in bestelling hebben (bvbesteld)
// wordt gebruikt voor alterboek.
function sqlBesteldeBoekvakken($boekvaknr)
{
	global $BWDB;

	return $BWDB->q('TABLE SELECT * FROM studentbestelling s
		WHERE (s.vervaldatum IS NULL OR TO_DAYS(s.vervaldatum) > TO_DAYS(now())) AND
			s.gekochtdatum IS NULL AND
			boekvaknr = %i', $boekvaknr);
}

function sqlBoekVakGegevens($boekvaknrs) {
	global $BWDB;
	$rows = array();

	$result = $BWDB->q("TABLE SELECT `titel`"
		.", `auteur`"
		.", `b`.`EAN` as EAN"
		.", `naam` as vak"
		.", `docent`"
		.", `email`"
		.", `boekvaknr`"
		.", `bestelbaar`"
		.", `druk`"
		." FROM `boeken` b, `boekvak` bv, `vak` v"
		." WHERE `b`.`EAN` = `bv`.`EAN`"
		." AND `v`.`vaknr` = `bv`.`vaknr`"
		." AND `bv`.`boekvaknr` IN (%As)"
		, $boekvaknrs);

	foreach($result as $row) {
		$rows[$row['boekvaknr']] = $row;
	}
	return $rows;
}

//jeroen
function sqlBoekGegevens($EAN) {
	$result = sqlBoekenGegevens(array($EAN));

	if(count($result) > 0) {
		return $result[$EAN];
	} else {
		return FALSE;
	}
}

//jeroen
function sqlBoekenGegevens($EANs)
{
	global $BWDB;

	$ret = array();

	if(empty($EANs))
		return array();

	$res = $BWDB->q("TABLE SELECT * FROM `boeken`"
		." WHERE `EAN` IN (%As) ORDER BY `auteur`, `titel`, `druk`", $EANs);
	foreach($res as $tuple) {
		$ret[$tuple['EAN']] = $tuple;
	}

	return $ret;
}

function sqlFindVak($vakzoek) {
	global $BWDB;

	$res = $BWDB->q('TABLE SELECT * FROM vak WHERE naam LIKE %c '.
		' OR afkorting LIKE %c OR docent LIKE %c'.
		' ORDER BY collegejaar DESC, begindatum DESC', $vakzoek, $vakzoek, $vakzoek);
	$result = array();
	// voor elke gevonden row, voeg studies en studiejaren toe.
	foreach($res as $tuple) {
		$result[$tuple['vaknr']] = $tuple;
		$result[$tuple['vaknr']]['studies']		= sqlGetVakStudies($tuple['vaknr']);
	}
	return $result;
}

function sqlVakkenGegevens($vaknrs) {
	global $BWDB;

	$result = array();

	$res = $BWDB->q("TABLE SELECT * FROM `vak` WHERE `vaknr` IN (%As)", $vaknrs);

	// voeg de benodigde fields studies en studiejaren erbij
	foreach($res as $tuple) {
		$result[$tuple['vaknr']] = $tuple;
		$result[$tuple['vaknr']]['studies']		= sqlGetVakStudies($tuple['vaknr']);
	}

	return $result;
}

/**
	Geeft een simpele array terug met alle momenten waarop er een levering
	is geweest van het gevraagde EAN. Als $EAN null is (default) dan worden
	alle leveringen in het verleden (ongeacht een EAN) geretourneerd.
	Als $uitgebreid true is, dan wordt de datum als key gebruikt voor een
	array met daarin ook nog leveranciernrs en geleverde EANS:

	array (
		leverdatum1 => array (
			leveranciernrs => array(1,2,3),
			EANs => array(EAN1,EAN2,EAN3)
			),
		leverdatum2 => array(...)
	)

*/
function sqlGetLeverMomenten($EAN = null, $uitgebreid = false){
	global $BWDB;

	$where = "";
	if ($EAN != null) $where = "WHERE voorraad.EAN = %s";

	if (!$uitgebreid){
		return $BWDB->q("
			COLUMN SELECT wanneer
			FROM levering
			LEFT JOIN voorraad USING (voorraadnr)
			$where
			GROUP BY wanneer
			ORDER BY wanneer DESC", $EAN);
	} else {
		$data = $BWDB->q("
			TABLE SELECT
				DATE(levering.wanneer) as leverdatum,
				voorraad.EAN as EAN,
				voorraad.leveranciernr as leveranciernr
			FROM levering
			LEFT JOIN voorraad USING (voorraadnr)
			GROUP BY DATE(wanneer), voorraad.leveranciernr,
			voorraad.voorraadnr
			ORDER BY wanneer DESC");

		$res = array();
		foreach ($data as $rij){
			$leverdatum = $rij["leverdatum"];
			$leveranciernr = $rij["leveranciernr"];
			$EAN = $rij["EAN"];

			$res["$leverdatum"]["leveranciernrs"][] = $leveranciernr;
			$res["$leverdatum"]["EANs"][] = $EAN;
		}
		foreach ($res as $leverdatum => $dummy){
			$leveranciernrs = array_unique($res["$leverdatum"]["leveranciernrs"]);
			$EANs = array_unique($res["$leverdatum"]["EANs"]);

			$res["$leverdatum"]["leveranciernrs"] = $leveranciernrs;
			$res["$leverdatum"]["EANs"] = $EANs;
		}
		return $res;
	}
}

/**
	Retourneert leveringen op een dag als array:

	array (
		array(	"voorraadnr" => voorraadnr,
				"EAN" => EAN,
				"aantal" => aantal,
				"titel" => titel,
				"auteur" => auteur,
				"bestellingdatum" => datum van levbestelnr,
				"bestellingaantal" => totaalaantal besteld,
				"levbestelnr" => levbestelnr,
				"levprijs" => levprijs,
				"leveranciernr" => leveranciernr)
		, array(...))

	Er wordt gegroepeerd op voorraadnr. Als er dus op een dag twee
	leveringen zijn geweest met hetzelfde voorraadnr dan worden die bij
	elkaar gepakt (aantallen bij elkaar opgeteld, uiteraard)
*/
function sqlGetLeveringen($datumbegin, $leveranciernr, $orderby = null, $datumeind = false){
	global $BWDB;

	if ($orderby == "EAN"){
		$orderby = "voorraad.leveranciernr, boeken.EAN";
	} elseif($orderby == "auteur") {
		$orderby = "voorraad.leveranciernr, boeken.auteur";
	}
	else
	{
		$orderby = 'levering.wanneer';
	}
	$orderby = "ORDER BY $orderby";

	// leveranciernr is optioneel
	$checkLevnr = "";
	if (is_numeric($leveranciernr))
		$checkLevnr = "AND voorraad.leveranciernr = %i";

	$res = $BWDB->q("
		TABLE SELECT
			levering.voorraadnr as voorraadnr,
			boeken.titel as titel,
			boeken.auteur as auteur,
			boeken.EAN as EAN,
			SUM(levering.aantal) as aantal,
			levering.levbestelnr as levbestelnr,
			levering.wanneer as leveringdatum,
			bestelling.aantal as bestellingaantal,
			bestelling.datum as bestellingdatum,
			voorraad.levprijs as levprijs,
			voorraad.leveranciernr as leveranciernr,
			voorraad.btwtarief as btw
		FROM levering
		LEFT JOIN bestelling
			ON (levering.levbestelnr = bestelling.levbestelnr)
		LEFT JOIN voorraad
			ON (levering.voorraadnr = voorraad.voorraadnr)
		LEFT JOIN boeken
			ON (voorraad.EAN = boeken.EAN) 

		WHERE 	(DATEDIFF(levering.wanneer, %s)>= 0)
		AND (DATEDIFF(%s,levering.wanneer) >= 0)
		$checkLevnr

		GROUP BY levering.voorraadnr, levering.levbestelnr
		$orderby", $datumbegin, $datumeind,$leveranciernr);

	return $res;
}

// get info over de bestellingen van 1 lid
function sqlGetBestellingen($lidnr) {
	// jeroen 12 feb 2002
	// en jacob 8 dec '02
	// en weer gewijzigd door Thijs 17 nov 03
	global $BWDB;

	$result = $BWDB->q('TABLE SELECT *,
		(gekochtdatum IS NOT NULL) as gekocht,
		(gekochtdatum IS NULL AND
			(vervaldatum IS NOT NULL AND to_days(vervaldatum) <= to_days(NOW()))) as vervallen,
		(gemailddatum IS NOT NULL) as geleverd
		FROM boeken b NATURAL JOIN studentbestelling s '.
		'WHERE lidnr = %i ORDER BY bestelddatum DESC', $lidnr);

	$ret = array();
	foreach ($result as $tuple)
	{
		if (!isset($tuple['boekvaknr']) || !$tuple['boekvaknr'] ) {
			$vak = 'n.v.t.';
		} else {
			list($url, $vak) = $BWDB->q("TUPLE SELECT `url`, `naam`"
				." FROM `vak` NATURAL JOIN `boekvak`"
				." WHERE `boekvaknr` = %s"
				, @$tuple['boekvaknr']);
		}

		$ret[$tuple['bestelnr']] = array
			( 'gekocht' => $tuple['gekocht']
			, 'vervallen' => $tuple['vervallen']
			, 'geleverd' => $tuple['geleverd']
			, 'titel'  => $tuple['titel']
			, 'auteur' => $tuple['auteur']
			, 'druk'   =>@$tuple['druk']
			, 'EAN'   => $tuple['EAN']
			, 'vak'    => $vak
			, 'url'    =>@$url
			, 'lidnr'  => $tuple['lidnr']
			, 'bestelddatum' => $tuple['bestelddatum']
			, 'vervaldatum'  => @$tuple['vervaldatum']
			, 'boekvaknr'    => @$tuple['boekvaknr']
			);
	}

	return $ret;
}

/* Geeft een 2-dimensionale array met boeken die verkocht mogen worden aan een
 * bepaald lid of alle leden.
 * Herschreven door Jacob op 18-12-2002 met de hoop dat het nu een
 * overzichtelijker functie is.
 * Aangepast door Thijs op 22-02-2003 om LEFT JOIN te gebruiken zodat er niet
 * allerlei hacks gebruikt hoeven worden om niet-vakgebonden boeken toch te
 * selecteren.
 */
function sqlTeVerkopenBoeken($lidnr)
{
	global $BWDB;

	$select  = 'DISTINCT bestelnr, adviesprijs as euro, levprijs as	levprijs, '
		.'titel, auteur, druk, b.EAN as EAN, '
		.'url, naam as vak';

	$from    = 'boeken b '.
		   'LEFT JOIN studentbestelling s USING (EAN) '.
		   'LEFT JOIN voorraad vr USING (EAN) '.
		   'LEFT JOIN boekvak bv ON (s.boekvaknr=bv.boekvaknr) '.
		   'LEFT JOIN vak v ON (bv.vaknr=v.vaknr)';

	$andlist = " (`vervaldatum` IS NULL OR TO_DAYS(`vervaldatum`) > TO_DAYS(now()))"
		." AND `gemailddatum` IS NOT NULL"
		." AND `voorraad` > 0"
		." AND `gekochtdatum` IS NULL";


	if ($lidnr) {
		$andlist .= " AND `lidnr` = %s";
		$orderby   = 'wanneer';
	}
	else {
		$select   .= ', ld.contactID as lidnr, ld.voornaam, s.vervaldatum';
		$from     .= ", ".WHOSWHO4_DB.".Persoon ld";
		$andlist  .= ' AND `ld`.`contactID` = `s`.`lidnr` %_';
		$orderby   = 'ld.voornaam';
	}

	$result = $BWDB->q("TABLE SELECT " . $select . " FROM " . $from
		." WHERE " . $andlist . " ORDER BY " . $orderby, $lidnr);

	return $result;
}

// Geeft voorraadnrs van bestellingen. Let er niet op of bestellingen nog wel
// geldig zijn... Returnt array({bestelnr => {levernr => leverdata}})
function sqlBestelnrs2Voorraadnrs($bestelnrs)
{
	global $BWDB;

	// geen bestelnrs? Dan ook geen voorraadnrs.
	if(count($bestelnrs) == 0) {
		return array();
	}

	$res = $BWDB->q('TABLE SELECT bestelnr, v.*, boekenmarge
		FROM studentbestelling sb LEFT JOIN voorraad v USING(EAN)
			LEFT JOIN leverancier l USING (leveranciernr)
		WHERE bestelnr IN (%Ai) AND voorraad > 0', $bestelnrs);

	$result = array();
	foreach ($res as $row) {
		$result[$row['bestelnr']][$row['voorraadnr']] = $row;
	}

	return $result;
}

/* Levert een associatieve array met {voorraadnr => EAN} op */
function sqlVoorraadnrs2EAN($voorraadnrs = array())
{
	if (count($voorraadnrs) == 0) {
		return array();
	}

	global $BWDB;

	foreach($voorraadnrs as $index => $voorraadnr) {
		// check of we te maken hebben met een terugkoop
		if(substr($voorraadnr, 0, 2) === "tk") {
			$verkoopnr = substr($voorraadnr, 2, strlen($voorraadnr));
			$voorraadnrs[$index] = $BWDB->q('VALUE SELECT voorraadnr
				FROM verkoop
				WHERE verkoopnr = %i',
				$verkoopnr);
		}
	}

	$res = $BWDB->q('TABLE SELECT voorraadnr, EAN
		FROM voorraad
		WHERE voorraadnr IN (%Ai)',
		$voorraadnrs);

	$ret = array();
	foreach ($res as $row) {
		$ret[$row['voorraadnr']] = $row['EAN'];
	}

	return $ret;
}

/* Zoekt een voorraadnummer van een voorraad van dit EAN op waarvan er nog
 * boeken op voorraad zijn. */
// LET OP! DEPRECATED! ZIE Voorraad::searchByEAN(...)
function sqlEAN2Voorraadnrs($EAN, $opvoorraad = TRUE)
{
	print_error("De functie sqlEAN2Voorraadnrs wordt niet meer gebruikt, zie Voorraad::searchByEAN(...)");
}

/* Voeg een verkoop toe in de verkoop tabel aan de hand van een aaray van
 * voorraadnummers=>('aantal'=> a, 'vkprijs'=> p, 'voorraadlocatie' => 'voorraad'
 * of 'ejbv'). Ook de voorraad tabel wordt aangepast.
 * Als er een (corresponderende) array met bestelnrs opgegeven is, worden deze
 * ook geupdate.
 */
function sqlInsVerkoop(/*array*/ $voorraadnrs, $lidnr, $betaalmethode = 'pin1', $bestelnrs = array()){
	global $BWDB, $BOEKENPLAATSEN;
	global $auth;

	$wanneer = getActionDateTime();

	foreach ($voorraadnrs as $voorraadnr => $verkoopinfo) {
		$voorraaddata = sqlGetVoorraadInfo($voorraadnr);
		$EAN = $voorraaddata['EAN'];

		$voorraadlocatie = $verkoopinfo['voorraadlocatie'];
		Voorraad::checkVoorraadLocatie($voorraadlocatie); // geeft error bij ongeldige voorraadlocatie

		$aantal_op_voorraad = $voorraaddata[$voorraadlocatie];
		if ($aantal_op_voorraad < $verkoopinfo['aantal']) {
			// Onvoldoende boeken op voorraad!
			user_error("Interne fout: er zijn nog $aantal_op_voorraad artikelen op locatie $voorraadlocatie in voorraad ".
				"en je wilt er $verkoopinfo[aantal] verkopen, dat kan uiteraard niet.", E_USER_ERROR);
		}

		$verkoopdata['voorraadnr']	= $voorraadnr;
		$verkoopdata['lidnr']		= $lidnr;
		$verkoopdata['wanneer']		= $wanneer;
		$verkoopdata['aantal']		= $verkoopinfo['aantal'];
		$verkoopdata['voorraadlocatie'] = $voorraadlocatie;
		$verkoopdata['betaalmethode']	= $betaalmethode;
		$verkoopdata['studprijs']	= $verkoopinfo['vkprijs'];
		$verkoopdata['btw']			= $verkoopinfo['btw'];
		$verkoopdata['wie']			= $auth->getLidnr();

		// BEGIN TRANSACTION
		//Voeg een verkoop toe in de 'verkoop' tabel
		$BWDB->q("INSERT INTO `verkoop` SET %S", $verkoopdata);

		//Laat de voorraad afnemen
		$BWDB->q("UPDATE voorraad SET $voorraadlocatie = $voorraadlocatie - %i WHERE voorraadnr = %i",
			$verkoopinfo['aantal'], $voorraadnr);
		// END TRANSACTION
	}
	if (sizeof($bestelnrs)>0) {
		$orstr = ' `bestelnr` = ' . implode(' OR `bestelnr` = ', $bestlnrs);
		$BWDB->q("UPDATE `studentbestelling` SET %S"
			." WHERE ( " . $orstr . " )"
			." AND (`vervaldatum` IS NULL"
			.	" OR `TO_DAYS(`vervaldatum`) > TO_DAYS(now()))"
			." AND `gekochtdatum` IS NULL"
			." AND `lidnr` = %i"
			, array('gekochtdatum' => date('Y-m-d')
				, 'vervaldatum' => NULL
				, 'vervalreden' => NULL)
			, $lidnr);
	}

}

// geeft bestelstatus van een boekbestelling voor een student
function sqlStatus($bestelnr)
{
	global $BWDB;

	return $BWDB->q('MAYBETUPLE SELECT *,
		(gekochtdatum IS NOT NULL) as gekocht,
		(gekochtdatum IS NULL AND
			(vervaldatum IS NOT NULL AND to_days(vervaldatum) <= to_days(NOW()))) as vervallen,
		(gemailddatum IS NOT NULL) as geleverd
		FROM boeken b NATURAL JOIN studentbestelling s
		WHERE bestelnr = %i',
		$bestelnr);
}

/* geeft een array terug met het nieuws voor een of meerdere gegeven boeken in
 * een array */
function sqlGetBoekenNieuws($EANs, $showold = FALSE) {
	global $BWDB;
	if (!is_array($EANs) || sizeOf($EANs) == 0) {
		user_error('sqlGetBoekenNieuws: Er moet ten minste 1 EAN opgegeven '
			.'worden', E_USER_ERROR);
	}

	if($showold) {
		$qrecent = '';
	} else {
		$qrecent = 'AND wanneer > "'.colJaar().'-08-01"';
	}

	$res = $BWDB->q('TABLE SELECT wanneer as datum,nieuws,EAN FROM nieuws WHERE
			EAN IN (%As) '.$qrecent.' ORDER BY wanneer DESC',  $EANs);

	if(!$res) {
		return;
	}

	return $res;
}

// 1 of meer bestelnrs in array, zorgt zelf voor security
function sqlBestelBoekInfoRes($bestelvoorraadnrs) {
	global $BWDB;

	$boekinfo = $BWDB->q('TABLE SELECT titel, auteur, b.EAN, sb.gemailddatum, sb.bestelnr '.
		'FROM studentbestelling sb NATURAL JOIN boeken b '.
		'WHERE sb.bestelnr IN (%Ai) AND gekochtdatum IS NULL AND '.
		'( vervaldatum IS NULL OR TO_DAYS(vervaldatum) > TO_DAYS(NOW())) AND '.
		'lidnr = %i', array_keys($bestelvoorraadnrs), getSelLid());

	$ret = array();
	foreach($boekinfo as $info){
		$prijsje =
			$BWDB->q('TUPLE SELECT levprijs, boekenmarge, adviesprijs
				FROM voorraad NATURAL JOIN leverancier WHERE voorraadnr = %i',
				$bestelvoorraadnrs[$info['bestelnr']]);

		$info['prijs'] =
			berekenVerkoopPrijs($prijsje['levprijs'], $prijsje['boekenmarge'],
			@$prijsje['adviesprijs']);
		$info['btwtarief'] = getBtwtarief($bestelvoorraadnrs[$info['bestelnr']]);
		$info['btw'] = getBtw($bestelvoorraadnrs[$info['bestelnr']]);
		$ret[] = $info;
	}
	return $ret;
}

/* zowel EANs als boekvaknrs zijn arrays van die nummers, die allebei
 * geinsert worden. Als je maar 1 gebruikt, moet dus de ander een lege array
 * zijn.
 * De vorm van beide is array{nr => aantal}
 */

function sqlBestel($lidnr, $EANs, $boekvaknrs, $prioriteit)
{
	global $BWDB;
	global $auth;

	$values = array();

	if (!$lidnr) {
		user_error("Lidnummer ($lidnr) is niet geldig", E_USER_ERROR);
	}

	$values['lidnr'] = (int) $lidnr;
	$values['bestelddatum'] = getActionDate();
	$values['wie'] = $auth->getLidnr();
	if (isset($prioriteit)) {
		$values['prioriteit'] = (int) $prioriteit;
	}

	foreach ($EANs as $EAN => $aantal) {
		for ($i = 0; $i < $aantal; $i++) {
			$values['EAN'] = $EAN;

			// dit kan ook in 1 SQL call, maar what the hack...
			$BWDB->q("INSERT INTO `studentbestelling` SET %S", $values);
		}
	}
	foreach ($boekvaknrs as $boekvaknr => $aantal) {
		for ($i = 0; $i < $aantal; $i++) {
			$values['boekvaknr'] = $boekvaknr;
			$values['EAN']       = $BWDB->q('VALUE SELECT EAN FROM boekvak
				WHERE boekvaknr=%i', $boekvaknr);

			$BWDB->q("INSERT INTO `studentbestelling` SET %S", $values);
		}
	}

	return;
}

// geeft alle EANs uit $EANs die al ooit eens besteld zijn door $lidnr
function sqlCheckReedsBesteld($EANs, $lidnr) {
	global $BWDB;

	return $BWDB->q('COLUMN SELECT EAN FROM studentbestelling '.
		'WHERE lidnr = %i AND ( '.
		'( vervaldatum IS NULL OR TO_DAYS(vervaldatum) > TO_DAYS(now()) ) '.
		'OR gekochtdatum IS NOT NULL ) AND EAN IN (%As)',
		$lidnr, $EANs);
}

function sqlAlikeBoeken($boekdata, $alleenbestelbaar = false) {
//Geeft een 2-dim array met boeken die lijken op de $boekdata
	global $BWDB;

	$result =  $BWDB->q("
		TABLE SELECT * FROM boeken
		WHERE (EAN = %s " .
		(($boekdata['titel']) ? "OR titel LIKE %s" : "%_") . " " .
		(($boekdata['auteur']) ? "OR auteur LIKE %s" : "%_") . ") " .
		($alleenbestelbaar ? " AND bestelbaar = \"Y\" AND cienr IS NULL " : " ") .
		"ORDER BY auteur, titel, EAN"
		, $boekdata['EAN']
		, "%" . $boekdata['titel'] . "%"
		, "%" . $boekdata['auteur'] . "%");

	return $result;
}

// voer een vak in met vakdata en studie/jaar info (matrix)
function sqlInsVak($vakdata, $studiejaar, $vaknr = null) {
	global $BWDB;

	// als vaknr geset, update info en verwijder alles uit studievak
	// (voegen we hieronder weer toe)
	if($vaknr) {
		$BWDB->q("UPDATE `vak` SET %S WHERE `vaknr` = %i", $vakdata, $vaknr);
		$BWDB->q("DELETE FROM `studievak` WHERE `vaknr` = %i", $vaknr);
	} else {
		$vaknr = $BWDB->q("RETURNID INSERT INTO `vak` SET %S", $vakdata);
	}

	// breng juiste relaties aan in studievak
	foreach($studiejaar as $studie => $jaren) {
		foreach($jaren as $jaar) {
			$BWDB->q("INSERT INTO `studievak` SET %S"
				, array('vaknr' => $vaknr,
						'studie' => $studie,
						'jaar' => $jaar));
		}
	}

	return $vaknr;
}

function sqlInsBoek($boekdata, $EAN=null) {
//$boekdata in de vorm array(['veldnaam'=>'velddata']*)
	global $BWDB;
	if($EAN) {
		return $BWDB->q("UPDATE `boeken` SET %S WHERE `EAN` = %s", $boekdata, $EAN);
	}
	return $BWDB->q("INSERT INTO `boeken` SET %S", $boekdata);
}

function sqlDelVak($vaknr) {
	global $BWDB;

	$BWDB->q('DELETE FROM studievak WHERE vaknr = %i', $vaknr);
	$BWDB->q('DELETE FROM vak WHERE vaknr = %i', $vaknr);
}

function sqlInsBoekVak($EAN, $vaknr, $verplicht=false) {
	global $BWDB;
	$verplYN = $verplicht?'Y':'N';
	$BWDB->q("INSERT INTO `boekvak` SET %S",
		array('EAN'     =>$EAN,
		'vaknr'    =>$vaknr,
		'verplicht'=>$verplYN));
}

function sqlUpdBoekVak($boekvaknr, $data) {
	global $BWDB;

	$BWDB->q("UPDATE `boekvak` SET %S WHERE `boekvaknr` = %i", $data, $boekvaknr);
}

function sqlDelBoekVak($boekvaknr) {
	global $BWDB;

	$BWDB->q('DELETE FROM boekvak WHERE boekvaknr = %i', $boekvaknr);
}

function sqlBoekVakData($boekvaknr) {
	global $BWDB;

	return $BWDB->q('TUPLE SELECT * FROM boekvak WHERE boekvaknr = %i',$boekvaknr);
}

// kink: alle data betreffende vak, studies voor een EAN
// sjeik: als $EAN een array is, dan wordt er een efficiente query
// uitgevoerd om alle data betreffende vakken en studies op te zoeken voor
// een EAN.
function sqlGetBoekVakData($EAN, $alleenhuidig = FALSE) {
	global $BWDB;

	if (is_array($EAN)){
		// Meerdere EANs
		$result = $BWDB->q('
			TABLE SELECT * FROM boekvak bv
			LEFT JOIN vak v USING (vaknr)
			WHERE EAN IN (%As)'.($alleenhuidig?'
			AND einddatum >= NOW()':''), $EAN);
	} else {
		$result = $BWDB->q('
			TABLE SELECT * FROM boekvak bv
			LEFT JOIN vak v USING (vaknr)
			WHERE EAN = %s'.($alleenhuidig?'
			AND einddatum >= now()':''), $EAN);
	}

	$ret = array();
	foreach($result as $row) {
		$ret[] = array_merge($row, array('studies' => sqlGetVakStudies($row['vaknr'])));
	}
	return $ret;
}

// geeft matrix met studies/jaren die in de studievak-relatie gekoppeld zijn aan dit vaknr
function sqlGetVakStudies($vaknr) {
	global $BWDB;

	$res = $BWDB->q('TABLE SELECT studie,jaar FROM studievak WHERE vaknr = %i', $vaknr);
	$return = array();
	foreach($res as $row) {
		$return[$row['studie']][] = $row['jaar'];
	}
	// sorteer op studienaam
	ksort($return);
	return $return;
}

// geeft de boeken die bij dit vaknr horen uit boekvak
function sqlGetVakBoeken($vaknr) {
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT *, boekvaknr AS ARRAYKEY
		FROM boekvak LEFT JOIN boeken USING(EAN) WHERE vaknr = %i', $vaknr);
}

// wordt alleen gebruikt in alterbestelling
function sqlLevBestelRes($bestelnrs) {

	global $BWDB;

	$res = $BWDB->q("TABLE SELECT `datum` AS besteldatum"
		.", `titel`"
		.", `auteur`"
		.", `b`.`EAN` as EAN"
		.", `aantal` AS besteld"
		.", `aantalgeleverd` AS geleverd"
		.", `levbestelnr`"
		." FROM `boeken` b, `bestelling` bs"
		." WHERE `b`.`EAN` = `bs`.`EAN`"
		." AND `bs`.`levbestelnr` IN (%As)"
		, $bestelnrs);

	foreach($res as $rij) {
		$result[$rij['levbestelnr']] = $rij;
	}
	return $result;
}

// verander het aantal boeken van een bestelling bij leverancier
function sqlChangeLevBestelling($bestelnr, $aantal) {
	global $BWDB;

	$BWDB->q("UPDATE `bestelling` SET %S WHERE `levbestelnr` = %i"
		, array('aantal' => $aantal)
		, $bestelnr);
}

function sqlFaseInfo($fase, $EANs=null)
{
	// jeroen feb 2002

	$fase = (string)$fase;
	if (strlen($fase) != 1) {
		user_error("sqlFaseInfo: Fase moet een 1-letterig/cijferig
			fase-aanduiding zijn, en niet '$fase'", E_USER_ERROR);
	}

	$info = sqlAlleFasenInfo($EANs);
	$ret = array();

	foreach ($info as $EAN => $vars) {
		$aantal = $vars["fase_$fase"];
		$ret[$EAN] = array
			( 'aantal' => $aantal
			, 'titel'  => $vars['boek_titel']
			, 'auteur' => $vars['boek_auteur']
			, 'druk'   => $vars['boek_druk']
			, 'uitgever' => $vars['boek_uitgever']
			, 'bestelbaar' => $vars['boek_bestelbaar'] == 'Y'
			);
	}

	return $ret;
}

// in bestelling is datum de key.
function sqlLeverancierBestel($aantallen, $leveranciernr)
{
	global $BWDB;
	global $auth;

	$datum = getActionDate();

	foreach ($aantallen as $EAN=>$aantal) {
		$BWDB->q("INSERT INTO `bestelling` SET %S"
			, array( 'EAN'          => $EAN
				, 'aantal'        => $aantal
				, 'datum'         => $datum
				, 'leveranciernr' => $leveranciernr
				, 'wie'           => $auth->getLidnr()
			));
	}

	return $datum;
}

// geeft bestelinfo adhv een besteldatum.
function sqlGetBestellingByDate($datum, $leveranciernr) {
	global $BWDB;

	return $BWDB->q('TABLE SELECT * FROM bestelling NATURAL JOIN boeken '.
		'WHERE datum = %s AND leveranciernr = %i ORDER BY auteur',
		$datum, $leveranciernr);
}

function sqlGetBestellingenByEAN($EAN, $alleenlopend = FALSE) {
	global $BWDB;

	return $BWDB->q('TABLE SELECT *
		FROM bestelling
		WHERE EAN = %s '
		.($alleenlopend?' AND aantalgeleverd < aantal ':'').
		'ORDER BY datum', $EAN);
}

function sqlGetBestellingInfo($levbestelnr) {
	global $BWDB;

	return $BWDB->q("TUPLE SELECT * FROM `bestelling` NATURAL JOIN `boeken`"
		." WHERE `levbestelnr` = %i", $levbestelnr);
}

function sqlLeveranciers() {
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT *, leveranciernr AS ARRAYKEY '
		.'FROM leverancier');
}

/* input een leveranciernr, output een array met keys:
 * naam,leveranciernr,adres,email,telefoon,contactpersoon,stamp_leverancier */
function sqlLeverancierData($leveranciernr) {
	global $BWDB;

	$result = $BWDB->q("TUPLE SELECT * FROM `leverancier`"
		." WHERE `leveranciernr` = %i", $leveranciernr);
	if ($result === FALSE) {
		user_error("Geen leverancier gevonden ($leveranciernr)!", E_USER_ERROR);
	}
	return $result;
}

// kink: geeft de lidnrs die nog wachten op een bepaald boek
// wordt gebruikt bij boekennieuws; deze mensen moeten nieuws ontvangen.
function sqlGetLeden123($EAN)
{
	global $BWDB;

	$res = $BWDB->q('COLUMN SELECT lidnr FROM studentbestelling '.
		'WHERE EAN = %s AND gemailddatum IS NULL AND vervaldatum IS NULL',
		$EAN);

	return $res;
}

/* Alle boeken die in bestelling zijn, maar nog niet geleverd zijn
Mogelijke waardes voor $orderby:
"normaal": sorteren op besteldatum, auteur (default behaviour)
"ean": sorteren op ean

als $mergeDates true is, dan worden besteldatums genegeerd. De default
behaviour (false) is dat er verschillende regels worden geretourneerd voor
verschillende besteldatums.

$from en $to maken een selectie op besteldatum. Default behaviour is
respectievelijk "0000-00-00" en "NOW"
*/
function sqlTeLeveren($orderby = "normaal", $mergeDates = false, $from = "0000-00-00", $to = "NOW()") {
	global $BWDB;

	if ($orderby == "EAN"){
		$orderby = "ORDER BY EAN";
	} else {
		$orderby = "ORDER BY bs.datum, b.auteur";
	}

	if ($mergeDates){
		// Datums samenvoegen, dus GROUP BY niet over datum.
		$groupby = "GROUP BY b.EAN, leveranciernr";
	} else {
		$groupby = "GROUP BY b.EAN, leveranciernr, bs.datum";
	}

	if (!parseDate($from)) $from = "0000-00-00";

	if (!parseDate($to)){
		$to = "NOW()";
	} else {
		$to = "'$to'";
	}

	$result = $BWDB->q(" TABLE SELECT
				SUM(aantal) AS besteld,
				(SUM(aantal)-SUM(aantalgeleverd)) AS nogleveren,
				titel, auteur, b.EAN, datum, leveranciernr,
				levbestelnr, b.auteur,
				GROUP_CONCAT(bs.datum ORDER BY bs.datum SEPARATOR ', ') AS datum
		FROM bestelling bs LEFT JOIN boeken b USING (EAN)
		WHERE bs.aantalgeleverd < bs.aantal
		AND bs.datum >= '$from' AND bs.datum <= $to
		$groupby
		$orderby");

	return $result;
}

/**
 * Geef voor een lijst met EANs de geoffreerde studprijzen terug.
 * Studprijs wordt bepaald door bij de levprijs de boekenmarge op te tellen.
 */
function sqlOffertePrijzen($EANs) {

	global $BWDB;

	$table = $BWDB->q('KEYTABLE SELECT EAN as ARRAYKEY, round((levprijs*(1+boekenmarge)),2) AS studprijs
		FROM offerte NATURAL JOIN leverancier
		WHERE TO_DAYS(verloopdatum) > TO_DAYS(NOW()) AND EAN in (%As)', $EANs);

	// postprocess om er een mooie map uit te krijgen
	$result = array();
	foreach($EANs as $EAN) {
		$result[$EAN]=@$table[$EAN]['studprijs'];
	}

	return $result;
}

/* Gegegeven een lijst EANs, returnt de subset daarvan die aangeboden
 * wordt op de PapierMolen. */
function sqlPapierMolenAangeboden($EANs) {
	global $BWDB;

	if(count($EANs) == 0) {
		return array();
	}

	$eancol = $BWDB->q('COLUMN SELECT EAN FROM papiermolen
		WHERE verloopdatum > NOW() AND EAN IN (%As)',
		$EANs);

	return $eancol;
}

function sqlGetArtikelen()
{
	global $BWDB;
	return $BWDB->q('TABLE SELECT b.*,btw,leveranciernr
		FROM boeken b
		INNER JOIN voorraad USING (EAN)
		WHERE voorraad>0
		GROUP BY b.EAN
		ORDER BY auteur, titel, EAN');
}

function sqlDeletePakket($pakketnr)
{
	global $BWDB;

	$BWDB->q('DELETE FROM pakketitem WHERE pakketnr = %i',
		$pakketnr);
	$BWDB->q('DELETE FROM pakket WHERE pakketnr = %i',
		$pakketnr);
}

// Voor specificatie $pakketitems, zie sqlInsertPakketItems
function sqlInsertPakket($pakketnaam, $pakketitems = array())
{
	global $BWDB;

	$pakketnr = $BWDB->q('RETURNID INSERT INTO pakket SET naam = %s',
		$pakketnaam);

	sqlInsertPakketItems($pakketnr, $pakketitems);

	return $pakketnr;
}

/**
	Voegt items toe aan een pakket. Format $pakketitems:
	array(
		1234 => array(
			"aantal" => aantal items van voorraadnr 1234 in dit pakket
			"voorraadlocatie" => voorraadlocatie voor verkoop van deze items
		),
		5678 => array(
			"aantal" => aantal items van voorraadnr 5678 in dit pakket
			"voorraadlocatie" => voorraadlocatie voor verkoop van deze items
		)
	)
**/
function sqlInsertPakketItems($pakketnr, $pakketitems)
{
	global $BWDB;

	foreach ($pakketitems as $voorraadnr => $itemdetails) {
		Voorraad::isGeldigeVoorraadLocatie($itemdetails["voorraadlocatie"]); // gooit exception

		$BWDB->q('INSERT INTO pakketitem
			SET pakketnr = %i, voorraadnr = %i,
			aantal = %i, voorraadlocatie = %s',
			$pakketnr, $voorraadnr, $itemdetails['aantal'], $itemdetails['voorraadlocatie']);
	}
}

function sqlGetPakketten($pakketnr = NULL)
{
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT *, pakketnr as ARRAYKEY FROM pakket p '
		.(isset($pakketnr)?'WHERE pakketnr = %i':'ORDER BY naam'), $pakketnr);
}

/* $data in de vorm array(key => value) */
function sqlUpdatePakket($pakketnr, $data)
{
	global $BWDB;

	$BWDB->q('UPDATE pakket SET %S WHERE pakketnr = %i', $data, $pakketnr);
}

function sqlUpdatePakketItems($pakketnr, $pakketitems)
{
	global $BWDB;

	foreach ($pakketitems as $voorraadnr => $aantal) {
		if ($aantal == 0) {
			$BWDB->q('DELETE FROM pakketitem WHERE pakketnr = %i AND '
				.'voorraadnr = %i', $pakketnr, $voorraadnr);
		} else {
			$BWDB->q('UPDATE pakketitem SET aantal = %i WHERE pakketnr = %i '
				.'AND voorraadnr = %i', $aantal, $pakketnr, $voorraadnr);
		}
	}
}

function sqlGetPakketData($pakketnr)
{
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT *, v.voorraadnr AS ARRAYKEY
		FROM pakket p LEFT JOIN pakketitem pi USING(pakketnr)
		LEFT JOIN voorraad v USING (voorraadnr) LEFT JOIN boeken b USING (EAN)
		WHERE p.pakketnr = %i', $pakketnr);
}

function sqlGetRetourenByEAN($EAN)
{
	global $BWDB;

	return $BWDB->q('TABLE SELECT SUBSTRING(retour.wanneer, 1, 10) as datum,
		sum(aantal) as aantal, voorraad.voorraadnr
		FROM retour LEFT JOIN voorraad USING (voorraadnr)
		WHERE EAN = %s
		GROUP BY datum, voorraad.leveranciernr
		ORDER BY datum', $EAN);
}

/**
	Retourneert informatie uit 'tellingen'-tabel over tellingen
	die gedaan zijn bij het openen of sluiten (of spontaan) van
	de boekverkoop.
**/
function sqlGetTellingenByEAN($EAN, $vanaf = NULL, $totaan = NULL)
{
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT
		SUBSTRING(telling.wanneer, 1, 10) as datum,
		SUBSTRING(telling.wanneer, 12, 8) as tijd,
		wanneer, lidnr, opmerkingen, aantal_voorraad,
		aantal_buitenvk, aantal_ejbv_voorraad, telling.tellingnr AS ARRAYKEY

		FROM tellingitem LEFT
		JOIN telling USING (tellingnr)
		WHERE EAN = %s '
		.($vanaf?'AND wanneer >= \''.$vanaf.' 00:00:00\' ':'')
		.($totaan?'AND wanneer < \''.$totaan.' 00:00:00\' ':'')
		.'ORDER BY datum, tijd', $EAN);
}

function sqlGetVoorraadInfo($voorraadnr)
{
	global $BWDB;

	return $BWDB->q('MAYBETUPLE SELECT *
		FROM voorraad v LEFT  JOIN leverancier l USING (leveranciernr)
			LEFT JOIN boeken b ON (b.EAN = v.EAN)
		WHERE voorraadnr = %i', $voorraadnr);
}

// kink
// geef alle offertedata van alle gegeven EANs
function sqlGetOffertesInfo($EANs) {
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT *, offertenr as ARRAYKEY,
			round(levprijs*(1+boekenmarge),2) AS studprijs
		FROM offerte NATURAL JOIN leverancier
		WHERE EAN in (%As) ORDER BY offertedatum DESC, verloopdatum DESC',
		$EANs);
}

/**
	Sjeik: vertelt of het EAN nog gebruikt wordt in de toekomst bij een
	vak. Ook boeken die niet bij een vak gebruikt worden, maar door een
	student besteld zijn worden geretourneerd.
*/
function sqlGetBookUsed($EAN){
	global $BWDB;

	$query = "VALUE SELECT count(*) FROM boekvak,vak WHERE EAN=$EAN AND boekvak.vaknr=vak.vaknr AND vak.einddatum > NOW();";
	$count = $BWDB->q($query);

	if ($count == "0"){
		// Kijken of er toevallig een studentbestelling is voor dit boek
		$count = $BWDB->q("VALUE SELECT count(*) FROM studentbestelling
		WHERE EAN=%s AND gekochtdatum IS NULL;", $EAN);

	}

	return ($count != 0);
}


/** Sjeik: retourneert een koele array met de boek- en vakinfo
	van boeken behorend bij vakken die gegeven worden in een gegeven
	$periode van $jaar. De opmaak van de array:

	$array[$vaknr][$EAN]["begindatum"] = begindatum vak
	$array[$vaknr][$EAN]["einddatum"] = einddatum vak

	Ook vakken zonder boeken worden getoond:
	$array[$vaknr][null]["begindatum"] = begindatum vak
	$array[$vaknr][null]["einddatum"] = einddatum vak
*/
function sqlGetBoekenLijst($jaar, $periode){
	global $BWDB;

	$vakkenLijst = sqlGetVakkenLijst($jaar, $periode);
	if ($vakkenLijst === false) return false;


	$boekenLijst = array();

	foreach ($vakkenLijst as $vak){
		$vaknr = $vak["vaknr"];
		$begindatum = $vak["begindatum"];
		$einddatum = $vak["einddatum"];

		$eans = $BWDB->q("COLUMN
			SELECT boekvak.EAN
			FROM boekvak
			LEFT JOIN vak USING(vaknr)
			WHERE vak.vaknr=%i
			ORDER BY vak.afkorting;", $vaknr);

		$boekcount = 0;
		foreach ($eans as $ean){
			$boekcount++;
			$boekenLijst[$vaknr][$ean]["begindatum"] = $begindatum;
			$boekenLijst[$vaknr][$ean]["einddatum"] = $einddatum;
		}

		if ($boekcount == 0){
			// Geen boeken voor dit vak
			$boekenLijst[$vaknr][null]["begindatum"] = $begindatum;
			$boekenLijst[$vaknr][null]["einddatum"] = $einddatum;
		}
	}

	return $boekenLijst;
}


/**
	Sjeik: retourneert een array met vakken die in de gegeven $periode van
	het jaar $jaar gegeven worden.
*/
function sqlGetVakkenLijst($jaar, $periode){
	global $BWDB, $PERIODES;

	if (!isset($PERIODES[$jaar][$periode])) return false;

	$periode_startdatum = $PERIODES[$jaar][$periode];
	$periode_einddatum = periodeEindDatum($jaar, $periode);

	// Een vak valt binnen een periode, als:
	// startdatum_vak >= startdatum_periode en startdatum_vak < (einddatum_periode - 2 weken)
	// Er wordt bewust niets gespecificeerd over de einddatum van een vak, omdat
	// sommige vakken over twee periodes (1+2 of 3+4) heenvallen.

	//$vakkenlijst = $BWDB->q("TABLE SELECT * FROM vak WHERE DATE_ADD(begindatum, INTERVAL 30 DAY)>=%s AND DATE_ADD(einddatum, INTERVAL -30 DAY)<=%s;", $periode_startdatum, $periode_einddatum);

	if (!$periode_einddatum){
		// Er is geen einddatum van een periode beschikbaar
		// Het vak wordt opgenomen in het resultaat als:
		// vak_begindatum >= (periode_startdatum - 14 dagen)
		// Dit om te voorkomen dat een vak dat begint op 31 oktober niet getoond wordt
		// als de periode officieel pas op 1 november begint.
		$vakkenlijst = $BWDB->q("TABLE
			SELECT * FROM vak
			WHERE begindatum <= ADDDATE(%s, INTERVAL -14 DAY)
			ORDER BY afkorting;", $periode_startdatum);
	} else {
		// Een vak wordt opgenomen als:
		// vak_begindatum >= (periode_startdatum - 14 dagen) and vak_begindatum <= (periode_einddatum - 14 dagen)
		// Dit om te voorkomen dat een vak dat begint op 31 oktober niet getoond wordt
		// als de periode officieel pas op 1 november begint.
		$vakkenlijst = $BWDB->q("TABLE
			SELECT * FROM vak
			WHERE begindatum >= ADDDATE(%s, INTERVAL -14 DAY)
			AND begindatum <= ADDDATE(%s, INTERVAL -14 DAY)
			ORDER BY afkorting;", $periode_startdatum, $periode_einddatum);
	}


	return $vakkenlijst;
}
/**
	Sjeik: retourneert een array met EANs waarvan:
	- de laatste offerte verloopt binnen $verloopTermijn (aantal dagen, standaard 31 dagen)
	- de laatste offerte reeds verlopen is
	- er helemaal geen offerte is

	Het formaat is alsvolgd:
	$array[$EAN] = false    --- als er geen offerte is
	$array[$EAN] = datum    --- als er een verloopdatum is die verlopen is of bijna verloopt

	EANs die niet in de "gevarenzone" zitten zitten niet in de array.
*/
function sqlGetOfferteProblemen($verloopTermijn = 31){
	global $BWDB;

	$probleemboeken = array();

	// Subqueries kunnen nog niet in MySQL 4.0, dus dan maar per boek bekijken...
	$alleEANs = $BWDB->q("COLUMN SELECT boeken.EAN FROM boeken;");
	foreach ($alleEANs as $EAN){
		// Heeft dit EAN een offerte?
		$query = "VALUE SELECT count(*) FROM offerte WHERE offerte.EAN=$EAN;";
		$count = $BWDB->q($query);

		if ($count == 0){
			// Blijkbaar heeft dit EAN helemaal geen offerte
			$probleemBoeken[$EAN] = false;
		} else {
			// Uitzoeken wat de datum van de laatste offerte is van dit boek
			if (!is_numeric($verloopTermijn)) $verloopTermijn = 31;
			$query = "MAYBEVALUE SELECT verloopdatum FROM offerte WHERE verloopdatum >= DATE_ADD(NOW(), INTERVAL $verloopTermijn DAY) AND EAN=$EAN ORDER BY verloopdatum DESC;";
			$qres = $BWDB->q($query);
			if (!$qres) {
				// Geen resultaat, er is dus geen recente offerte!
				$probleemBoeken[$EAN] = $BWDB->q("VALUE SELECT verloopdatum FROM offerte WHERE EAN=$EAN ORDER BY verloopdatum DESC LIMIT 1;");
			}
		}
	}

	// Nu is er een array in de vorm:
	// $probleemBoeken[$EAN] = datum || false
	// Dit moet eigenlijk gesorteerd worden op datum. Nieuwste datum eerst, oudste datum als laatste
	arsort($probleemBoeken);
	return $probleemBoeken;
}

/**
 * Geef voor EANs een lijst met geldige offerteprijzen
 * kink
 */
function sqlGetValidOffertesInfo($EANs) {
	global $BWDB;

	return $BWDB->q('KEYTABLE SELECT *, EAN as ARRAYKEY,
			round(levprijs*(1+boekenmarge),2) AS studprijs
		FROM offerte NATURAL JOIN leverancier
		WHERE verloopdatum > NOW() AND
			EAN in (%As) ORDER BY offertedatum DESC, verloopdatum DESC',
		$EANs);
}

/* Laat alle offertes, die voor $vervaldatum begonnen zijn, verlopen.
 * Handig bij het laten verlopen van oude offertes wanneer er een nieuwe
 * offerte ingevoerd is. */
function sqlVervalOffertes($EAN, $vervaldatum)
{
	global $BWDB;

	$offertenrs = $BWDB->q('COLUMN SELECT offertenr FROM offerte WHERE '
		.'EAN=%s AND offertedatum <= %s AND verloopdatum >= %s',
		$EAN, $vervaldatum, getActionDate());

	if( count($offertenrs) > 0 ) {
		$BWDB->q('UPDATE offerte SET verloopdatum = %s '
			.'WHERE offertenr in (%Ai)', $vervaldatum, $offertenrs);
	}
}

/* Voeg een offerte toe */
function sqlInsertOfferte($leveranciernr, $levoffnr, $EAN, $levprijs,
	$offdatum, $verloopdatum)
{
	global $BWDB;
	global $auth;

	return $BWDB->q('RETURNID INSERT INTO offerte SET leveranciernr = %i, '
		.'leveranciersoffertenr = %s, EAN = %s, levprijs = %s, offertedatum = %s, '
		.'verloopdatum = %s, wie = %i, wanneer = %s', $leveranciernr,
		$levoffnr, $EAN, $levprijs, $offdatum, $verloopdatum, $auth->getLidnr(),
		getActionDate());
}
// zoek annuleringen
function sqlGetAnnuleringen()
{
	global $BWDB;
	$begin = date("Y-m-d",time());
	$sorteer=tryPar("sorteer","datum");
	$begin_datum=parseDate(tryPar("begin_datum"));
	$eind_datum=parseDate(tryPar("eind_datum"));


	if(!$begin_datum || !$eind_datum) { //begin- of eindatum niet ingevoerd
		$begin_datum = date("Y-m-d",mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1));
		$eind_datum = date("Y-m-d", time());
		printHTML("Ongeldige data-range! Gebruikt [" . $begin_datum .",". $eind_datum . "]");

	}

	$sort="";
	switch ($sorteer) {
		case "lid":
		    $sort= " order by studentbestelling.lidnr";
	    break;
		case "boek":
		    $sort= " order by studentbestelling.EAN";
		break;
		case "reden":
			$sort= " order by studentbestelling.vervalreden";
		break;
		case "datum":
		   $sort= " order by studentbestelling.vervaldatum";
		break;
	}
	if(tryPar('draaien')=="ja"){
		$sort.=" DESC";
	}

	return $BWDB->q('TABLE SELECT studentbestelling.*, boeken.titel
		FROM studentbestelling LEFT JOIN boeken USING (EAN)
		WHERE vervaldatum >= %s  AND  vervaldatum <= %s' . $sort,
		$begin_datum, $eind_datum);
}


// geef alle voorraden voor een EAN.
function sqlGetEANVoorraad($EAN) {
	global $BWDB;
	return $BWDB->q('KEYTABLE SELECT *, voorraadnr as ARRAYKEY
		FROM voorraad
		WHERE EAN = %s
		ORDER BY voorraadnr', $EAN
	);
}

// vind actieve (>0) voorraad voor een EAN op een plaats
function sqlFindActiveStock($EAN, $leveranciernr, $plaats)
{
	global $BWDB, $BOEKENPLAATSEN;
	Voorraad::checkVoorraadLocatie($plaats); // geeft error als $plaats ongeldig is

	return $BWDB->q("TABLE SELECT voorraadnr,levprijs,$plaats FROM voorraad
		WHERE $plaats > 0 AND leveranciernr = %i AND EAN=%s",
		$leveranciernr, $EAN);


}

// voor terugkoop: zoek een verkoop op aan dit lid van dit artikel
// geeft lijst van verkoopnr, voorraadnr, datum, vkprijs, aantal
// maar laat die, die reeds teruggekocht zijn weg.
function sqlFindVerkoopForTerugkoop($EAN, $lidnr) {

	global $BWDB;

	$vrows = sqlFindVerkopen($EAN, $lidnr);

	if(count($vrows) == 0) return array();

	$trows = $BWDB->q('KEYTABLE SELECT verkoopnr AS ARRAYKEY, SUM(aantal) as aantal FROM terugkoop
		WHERE verkoopnr IN (%Ai) GROUP BY verkoopnr', array_keys($vrows));

	$ret = array();
	foreach ($vrows as $verkoopnr => $vkdata) {
		if(isset($trows[$verkoopnr])) {
			if($trows[$verkoopnr]['aantal'] == $vkdata['aantal']) {
				// deze vkrow is al helemaal teruggekocht, skip
				continue;
			}
			$vkdata['aantal'] -= $trows[$verkoopnr]['aantal'];
		}

		$ret[$verkoopnr] = $vkdata;
	}

	return $ret;

}

function sqlGetBetaalmethodenByDate($datum)
{
	global $BWDB;

	$result = array();

	foreach (array('verkoop', 'terugkoop') as $type) {
		$res = $BWDB->q('TABLE SELECT betaalmethode, '
				.'sum(studprijs * aantal) as totaal '
			."FROM $type "
			.'WHERE TO_DAYS(wanneer) = TO_DAYS(%s) '
			.'GROUP BY betaalmethode', $datum);
		foreach ($res as $row) {
			if (!isset($result[$row['betaalmethode']])) {
				$result[$row['betaalmethode']] = 0.00;
			}

			/* Alles in verkoop behalve crediteuren is positief
			 * Alles in terugkoop behalve crediteuren is negatief */
			if ( ($type == 'verkoop' || $row['betaalmethode'] == 'crediteur') &&
				($type == 'terugkoop' || $row['betaalmethode'] != 'crediteur') ) {
				$result[$row['betaalmethode']] += $row['totaal'];
			} else {
				$result[$row['betaalmethode']] -= $row['totaal'];
			}
		}
	}

	return $result;
}

/**
 * Levert alle vakken op die momenteel of in de toekomst worden gegegeven.
 * LET OP: $begindatum gaat VOOR $coljaar
 */
function sqlGetHuidigeVakken($coljaar, $studiejaar, $studie, $begindatum,
	$einddatum)
{
	global $BWDB;

	$result = array();
	$rows = $BWDB->q('TABLE SELECT * FROM vak v '
		.'LEFT JOIN studievak sv USING(vaknr) '
		.'WHERE einddatum >= now() '.
		($begindatum ?
			"AND TO_DAYS(v.einddatum) >= TO_DAYS('$begindatum') ":
			($coljaar ?
				"AND v.collegejaar = $coljaar ":'')
			).
		($einddatum ?
			"AND TO_DAYS(v.begindatum) < TO_DAYS('$einddatum') ":'').
		($studie ?
			"AND sv.studie = '".mysql_real_escape_string($studie)."' ":'').
		($studiejaar ?
			"AND sv.jaar = '".mysql_real_escape_string($studiejaar)."' ":'').
		" ORDER BY naam");

	foreach ($rows as $row) {
		if (!isset($result[$row['vaknr']])) {
			$result[$row['vaknr']] = $row;
		}
		$result[$row['vaknr']]['studies'][$row['studie']][] = $row['jaar'];
	}

	return $result;
}

function sqlGetDagVerkopers($verkoopdag)
{
	global $BWDB;

	$verkopers = $BWDB->q('COLUMN SELECT DISTINCT wie FROM verkoop '
		. 'WHERE TO_DAYS(wanneer) = TO_DAYS(%s)', $verkoopdag);
	$terugkopers = $BWDB->q('COLUMN SELECT DISTINCT wie FROM terugkoop '
		. 'WHERE TO_DAYS(wanneer) = TO_DAYS(%s)', $verkoopdag);

	return array_unique(array_merge($verkopers, $terugkopers));
}

/**
	Resultaat:

	array[datetimestamp][lidnr]["verkopen"] = verkoop

	verkoop = array(
		"wanneer" => datetime
		"lidnr" => student
		"studprijs" => prijs
		"betaalmethode" => pin/chip/etc
		"verkoopnr" => verkoopnr
		"wie" => verkoper
		"voorraadnr" => voorraadnr
		"titel" => titel artikel
		"auteur" => auteur artikel
		"EAN" => EAN artikel)
*/
function sqlGetVerkopenPerTransactieGroep($datum = null, $verkopers = null){
	global $BWDB;
	if ($datum == null){
		$WHERE = "WHERE DATE(wanneer) >= CURDATE() AND DATE(wanneer) <= CURDATE()";
	} else {
		$WHERE = "WHERE DATE(wanneer) >= %s AND DATE(wanneer) <= %s";
	}

	if (is_array($verkopers) && sizeof($verkopers) > 0){
		$WHERE .= "AND verkoop.wie IN (%Ai)";
	}

	$data = $BWDB->q("
		TABLE SELECT verkoop.*, boeken.* FROM verkoop
		LEFT JOIN voorraad USING (voorraadnr)
		LEFT JOIN boeken USING (EAN)
		$WHERE
		ORDER BY verkoop.wanneer, verkoop.lidnr",
		$datum, $datum, $verkopers);

	$res = array();
	foreach ($data as $row){
		$date = $row["wanneer"];
		$lidnr = $row["lidnr"];

		$res["$date"][$lidnr][] = $row;
	}

	return $res;
}

/**
	Geeft een array terug met verkoopinformatie van de gegeven EANs. EANs
	zou een array moeten zijn met EANs, maar mag ook een enkel EAN zijn.
	In principe is er een limiet van 31 dagen terug in de geschiedenis
	(privacy), maar deze is uit te schakelen met de parameter
	ignorePrivacy.
	**/
function sqlGetVerkopenPerProduct($eans, $laatsteVerkoopEerst = true,
$ignorePrivacy = false, $datumvan = null, $datumtot = null){
	global $BWDB;
	global $logger;

	if (!is_array($eans)) $eans = array($eans);

	if ($laatsteVerkoopEerst){
		$DESC = "DESC";
	} else {
		$DESC = "";
	}

	if (!$ignorePrivacy){
		$PRIVACY = " AND wanneer > ADDDATE(NOW(), INTERVAL -31 DAY)";
	} else {
		$PRIVACY = "";
	}

	$DATUMSEL = "";
	if ($datumvan && strlen($datumvan) == 10){
		$DATUMSEL .= " AND wanneer >= \"$datumvan\"";
	}
	if ($datumtot && strlen($datumtot) == 10){
		$DATUMSEL .= " AND wanneer <= \"$datumtot\"";
	}

	$query = "
		TABLE SELECT verkoop.*, voorraad.*
		FROM verkoop
		LEFT JOIN voorraad USING(voorraadnr)
		WHERE EAN IN (%As)
		$PRIVACY
		$DATUMSEL
		ORDER BY wanneer $DESC";
	$logger->debug("GetVerkopenPerProduct: $query");
	$verkopen = $BWDB->q($query, $eans);

	$query = "
		TABLE SELECT terugkoop.*, voorraad.*
		FROM terugkoop
		LEFT JOIN voorraad USING(voorraadnr)
		WHERE EAN IN (%As)
		$PRIVACY
		$DATUMSEL
		ORDER BY wanneer $DESC";
	$terugkopen = $BWDB->q($query, $eans);
	foreach($terugkopen as $id => $row) {
		$terugkopen[$id]['aantal'] = -$row['aantal'];
	}

	return array_merge($verkopen,$terugkopen);
}

/**
	Levert een array met de volgende (voorbeeld)inhoud:

	$lijst[voorraadnr][] =
		array(
			["betaalmethode"] = betaalmethode,  // optioneel, zie $groupbyBetaalmethode
			["nettoverkocht"] = aantalverkocht - aantalteruggekocht,
			["levprijs"] = levprijs
			["EAN"] = ean
		)

	$order kan "EAN" of "auteur" zijn, by default EAN

	$groupbyBetaalmethode geeft aan of het resultaat gegroepeerd moet zijn
	op betaalmethode, dat betekent dat het mogelijk wordt om van een EAN
	niet het totaal aantal verkopen wordt getoond, maar dat van een EAN het
	totaal aantal verkopen per betaalmethode wordt getoond.
*/

function sqlVerkoopOverzicht($datum_vanaf, $datum_tot, $leveranciernrs =
null, $order = "", $groupbyBetaalmethode = false){
	global $BWDB;

	if ($order == "auteur"){
		$order = "ORDER BY voorraad.leveranciernr, boeken.auteur";
	} else {
		$order = "ORDER BY voorraad.leveranciernr, voorraad.EAN";
	}

	if ($groupbyBetaalmethode !== false){
		$groupbyBetaalmethode = ", verkoop.betaalmethode";
	} else {
		$groupbyBetaalmethode = false;
	}

	$query = "TABLE SELECT
			voorraad.EAN,
			verkoop.voorraadnr as voorraadnr,
			SUM(verkoop.aantal) as aantalverkocht,
			voorraad.levprijs as levprijs,
			voorraad.leveranciernr as leveranciernr,
			verkoop.betaalmethode as betaalmethode
		FROM verkoop
		LEFT JOIN voorraad ON (verkoop.voorraadnr = voorraad.voorraadnr)
		LEFT JOIN boeken ON (voorraad.EAN = boeken.EAN)
		WHERE (verkoop.wanneer >= %s AND verkoop.wanneer <= %s)";
	if ($leveranciernrs != null){
		$query .= " AND voorraad.leveranciernr IN(%As) ";
	}
	$query .= "GROUP BY verkoop.voorraadnr $groupbyBetaalmethode $order";
	$verkopen = $BWDB->q($query, $datum_vanaf, $datum_tot, $leveranciernrs);

	if ($groupbyBetaalmethode !== false){
		$groupbyBetaalmethode = ", terugkoop.betaalmethode";
	} else {
		$groupbyBetaalmethode = false;
	}

	$terugkopenQuery = "TABLE SELECT
			terugkoop.voorraadnr,
			SUM(terugkoop.aantal) as aantalteruggekocht,
			voorraad.EAN,
			voorraad.levprijs,
			voorraad.leveranciernr as leveranciernr,
			terugkoop.betaalmethode as betaalmethode
		FROM terugkoop
		LEFT JOIN voorraad USING(voorraadnr)
		LEFT JOIN boeken USING(EAN)
		WHERE wanneer >= %s AND wanneer <= %s ";
	if ($leveranciernrs != null){
		$terugkopenQuery .= "AND voorraad.leveranciernr IN(%As) ";
	}
	$terugkopenQuery .= "GROUP BY terugkoop.voorraadnr $groupbyBetaalmethode $order";
	$terugkopen = $BWDB->q($terugkopenQuery, $datum_vanaf, $datum_tot, $leveranciernrs);


	$verkoopoverzicht = array();
	$voorraadnrs = array();
	foreach ($verkopen as $verkooprow){
		$voorraadnr = $verkooprow["voorraadnr"];
		$aantalverkocht = $verkooprow["aantalverkocht"];
		$EAN = $verkooprow["EAN"];
		$levprijs = $verkooprow["levprijs"];
		$leveranciernr = $verkooprow["leveranciernr"];
		$betaalmethode = $verkooprow["betaalmethode"];

		if (!$groupbyBetaalmethode) $betaalmethode = 0;

		$verkoopoverzicht[$voorraadnr]["EAN"] = $EAN;
		$verkoopoverzicht[$voorraadnr]["levprijs"] = $levprijs;
		$verkoopoverzicht[$voorraadnr]["leveranciernr"] = $leveranciernr;
		$verkoopoverzicht[$voorraadnr]["mutaties"][$betaalmethode]["aantalverkocht"] = $aantalverkocht;
		$verkoopoverzicht[$voorraadnr]["mutaties"][$betaalmethode]["aantalteruggekocht"] = 0;
	}

	foreach ($terugkopen as $row){
		$voorraadnr = $row["voorraadnr"];
		$aantalteruggekocht = $row["aantalteruggekocht"];
		$EAN = $row["EAN"];
		$levprijs = $row["levprijs"];
		$leveranciernr = $row["leveranciernr"];
		$betaalmethode = $row["betaalmethode"];

		if (!$groupbyBetaalmethode) $betaalmethode = 0;

		$tempinfo["aantalteruggekocht"] = $aantalteruggekocht;

		$verkoopoverzicht[$voorraadnr]["EAN"] = $EAN;
		$verkoopoverzicht[$voorraadnr]["levprijs"] = $levprijs;
		$verkoopoverzicht[$voorraadnr]["leveranciernr"] = $leveranciernr;
		$verkoopoverzicht[$voorraadnr]["mutaties"][$betaalmethode]["aantalteruggekocht"] = $aantalteruggekocht;

		if (!isset($verkoopoverzicht[$voorraadnr]["mutaties"][$betaalmethode]["aantalverkocht"])){
			$verkoopoverzicht[$voorraadnr]["mutaties"][$betaalmethode]["aantalverkocht"] = 0;
		}

	}


	return $verkoopoverzicht;
}

function getLeverancierNaam($leveranciernr){
	global $BWDB;

	return $BWDB->q("MAYBEVALUE SELECT naam FROM leverancier WHERE
	leveranciernr=%i", $leveranciernr);
}

function getbtwtarief($voorraadnr) {
	global $BWDB;
	return $BWDB->q("MAYBEVALUE SELECT btwtarief FROM voorraad WHERE voorraadnr=%i",$voorraadnr);
}

function getBtw($voorraadnr) {
	global $BWDB;
	return $BWDB->q("MAYBEVALUE SELECT btw FROM voorraad WHERE voorraadnr=%i",$voorraadnr);
}
