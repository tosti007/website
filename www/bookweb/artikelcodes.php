<?php
// $Id$

/*

Functies om met artikelcodes te kunnen werken. Bij A-Eskwadraat gebruiken we
twee codes, EAN13 en ISBN-13. ISBN-13 is een subset van EAN13.

Over EAN13:
EAN13 staat voor 'European Article Number' (oid), en is als het goed is uniek
over de wereld. Het kan makkelijk gecodeerd worden in een streepjescode.

Dat bestaat uit 12 effectieve cijfers, en 1 checkdigit. De eerste 2-3 cijfers
geven de categorie aan. Voor ons is van belang:
- Beginnende met 2: in-store numbers, dus voor prive gebruik
- Beginnende met 977: ISSN (International Standard Serial Number for
  Periodicals, de 'periodieke ISBN", zie www.kb.nl voor info en aanvraag
  daarover)
- Beginnende met 978: ISBN (International Standard Book Numbering)
- Beginnende met 979: ISBN (sinds 1 januari 2007)

De meeste overige categorieen geven landen aan, die verantwoordelijk zijn voor
de uitgifte van die nummers.

Meer info: http://www.barcodeisland.com/ean13.phtml

Over ISBN-13: een ISBN-13 bestaat uit 13 cijfers, verdeeld over 5 groepen:
- De eerste groep is de productprefix (altijd lengte 3, momenteel 978 of 979).
- De tweede groep is het land van de uitgeverij (flexibele lengte)
- De derde groep is de uitgever (flexibele lengte)
- De vierde groep is een uniek nummer voor die uitgever binnen het land
  (flexibele lengte)
- De vijfde groep is het checkdigit (altijd lengte 1)

Voor 1 januari 2007 bestond uitsluitend ISBN-10, wat toen slechts ISBN heette.
ISBN-10 is gelijk aan ISBN-13 met uitzondering van de eerste drie cijfers. Voor
2007 bestond er slechts een categorie 'boek'. Van een ISBN-13 dat begint met
978 kun je ISBN-10 maken door deze drie cijfers weg te laten.

In de EAN13 code wordt het checkdigit altijd weggelaten, omdat dat al in EAN
zelf zit.

Meer info: http://www.cs.queensu.ca/home/bradbury/checkdigit/isbncheck.htm


Voor de prive-serie van EAN, zijn de volgende prefixen gedefinieerd:

200: ledenpasje, revisie 1
201: ledenpasje, revisie 2
210: A-Eskwadraat artikelen
220: Externe artikelen zonder eigen EAN die wij toch willen registreren

*/

// returnt de $data die 12 cijfers is, plus een EAN13 checkdigit erachter
function ean_addcheck($data, $fatal = true)
{
	if (strlen($data) != 12) {
		if ( $fatal ) {
			user_error(sprintf(_('ean_addcheck: data geen 12 tekens lang (%s)'), $data),
				E_USER_ERROR);
		}
		return false;
	}
	if (preg_match('/[^0-9-]/', $data)) {
		if ( $fatal ) {
			user_error(sprintf(_('ean_addcheck: niet-cijfer gevonden in data (%s)'), $data),
				E_USER_ERROR);
		}
		return false;
	}
	$odd=$even=0;
	for ($i=0;$i<12;$i++) {
		if ($i%2) {
			$odd+=$data[$i];
		} else {
			$even+=$data[$i];
		}
	}
	$checkdigit = (-3*$odd - $even) % 10;
	//% kan ook negatief opleveren
	if ($checkdigit<0) $checkdigit += 10;
	return "$data$checkdigit";
}

// returnt de 12 significante cijfers van een EAN13 code, en (fatale?) error
// indien de checkdigit niet klopt
function ean13_get($data)
{
	$ean13 = substr($data,0,13);
	$significant = substr($ean13,0,12);
	if (ean_addcheck($significant) != $ean13) {
		user_error(sprintf(_('ean13_get: checkdigit klopt niet (%s)'), $ean13),
			E_USER_ERROR);
	}
	return $significant;
}

// Probeert de lidgegevens uit de ean13 code van een ledenpasje te halen.
function ean13_to_liddata($data)
{
	$result = array();

	// zie whoswho/ledencls.php:printLedenPasCSV voor definitie
	switch (substr($data,0,3)) {
		case '200':
			$result['revisienr']    = 0;
			$result['uitgiftejaar'] = (int) substr($data, 3, 1);
			$result['lidnr']        = (int) substr($data, 4, 8);
			break;
		case '201':
			$result['revisienr'] = 1;
			$result['serienr']   = (int) substr($data, 3, 2);
			$result['beginjaar'] = (int) substr($data, 5, 2);
			$result['lidnr']     = (int) substr($data, 7, 5);
			break;
		default:
			user_error(sprintf(_('ean13_to_liddata: eancode bevat geen lidgegevens '
				.'(%s)'), $data), E_USER_ERROR);
	}

	return $result;
}

// returnt lidnr, anders user_error.
function ean13_to_lidnr($data)
{
	$data = ean13_to_liddata($data);
	if (!$data['lidnr']) {
		user_error(sprintf(_('ean13_to_lidnr: eancode bevat geen lidnr (%s)'), $data),
			E_USER_ERROR);
	}

	return $data['lidnr'];
}

// returnt isbn, anders false. Input moet een ean zijn
function ean_to_isbn($data)
{
	if (substr($data,0,3) != '978' && substr($data,0,3) != '979') {
		return false;
	}

	return ean_addcheck(substr($data, 0, 12));
}

// returnt de $data die 9 cijfers is, plus een ISBN checkdigit erachter
function isbn10_addcheck($data)
{
	if (strlen($data) != 9) {
		user_error(sprintf(_('isbn10_addcheck: data geen 9 tekens lang (%s)'), $data),
			E_USER_ERROR);
	}
	if (preg_match('/[^0-9]/', $data)) {
		user_error(sprintf(_('isbn10_addcheck: niet-cijfer gevonden in data (%s)'), $data),
			E_USER_ERROR);
	}

	$checkdigit=0;
	for ($i=0;$i<9;$i++) {
		$checkdigit += (10 - $i) * $data[$i];
	}

	$checkdigit%=11;
	$checkdigit=11-$checkdigit;
	if ($checkdigit==10)
		$checkdigit='X';
	else if ($checkdigit==11)
		$checkdigit='0';

	return "$data$checkdigit";
}

// Of iets een ISBN code is, of niet
// @param $strict13 geeft aan of de input per se ISBN-13 moet zijn. Bij false wordt
// ook ISBN-10 geaccepteerd.
function is_isbn($data, $strict13 = false)
{
	// Even streepjes uit ISBN halen
	$data = str_replace("-", "", $data);

	if (strlen($data) != 13 || !preg_match('/^\d{12}\w/', $data)) {
		if ($strict13 || strlen($data) != 10 || !preg_match('/^\d{9}\w/', $data)) {
			return FALSE;
		}
	}

	return ($strict13 ? FALSE : isbn10_addcheck(substr($data,0,9)) == strtoupper($data))
		|| (strlen($data) == 13 && ean_to_isbn($data) === $data);
}

// Of iets een artnr is, of niet
function is_artnr($data)
{
	return $data && preg_match('/^[AE][0-9]+$/', $data);
}

// Of iets een EAN code is, of niet
function is_EAN($data)
{
	if (strlen($data) != 13 || preg_match('/[^0-9]/', $data)) {
		return FALSE;
	}
	return EAN_addcheck(substr($data,0,12)) == $data;
}

// ISBN prettyprinter hulpfunctie
// Robert D. Cameron, 2001
function prefix_length_from_map ($s, $map) {
	// Fix voor PHP 7: sizeof van niet-array mag niet.
	if (!$map)
	{
		return 0;
	}

	for ($i = 1; $i < sizeof($map); $i++) {
		if (strcmp($s, $map[$i]) < 0) {
			return strlen($map[$i-1]);
		}
	}
	return strlen($map[$i-1]);
}

// print een ean met een link naar boekinfo over dat ean.
function print_EAN_bi($EAN) {
	return makeBookRef('boekinfo', print_EAN($EAN), 'EAN='.$EAN);
}

// ISBN prettyprinter
// gedeeltelijk gebaseerd op code van Robert D. Cameron, 2001.
function print_EAN($EAN, $restype = 'html')
{
	if (substr($EAN, 0, 3) == '210') { //A-Eskwadraat intern artikel
		return 'A-'.((int) substr($EAN, 3, 9));
	}
	if (substr($EAN, 0, 3) == '220') { // Extern artikel dat we verkopen, bv dictaat
		return 'E-'.((int) substr($EAN,3,9));
	}

	// anders is het een ISBN-13
	global $ISBN_COUNTRY_MAP, $ISBN_PUBL_MAP;

	if (! ($isbn = ean_to_isbn($EAN))) {
		return $EAN;
	}

	// categorie
	$catlen = 3;
	$pi[] = substr($isbn, 0, $catlen);
	$isbn = substr($isbn, $catlen);

	// country code
	$cclen = prefix_length_from_map($isbn, $ISBN_COUNTRY_MAP);
	$cc  = substr($isbn, 0, $cclen);
	$pi[] = $cc;
	$isbn = substr($isbn, $cclen);

	// publisher & book
	if (is_array($ISBN_PUBL_MAP[$cc])) {
		$publen = prefix_length_from_map($isbn, $ISBN_PUBL_MAP[$cc][0]);
		if ($cclen + $publen == 9) {
			$pi[] = substr($isbn, 0, -1);
		}
		else {
			$pi[] = substr($isbn, 0, $publen);
			$pi[] = substr($isbn, $publen, -1);
		}
	} else {
		$pi[] = substr($isbn, 0, -1);
	}

	// checkdigit
	$pi[] = substr($isbn, -1);

	return implode('-', $pi);
}


// de 12 significante cijfers van de gescande EAN13 code, of FALSE indien geen
// cuecat code (geen notice oid!)
function cuecat_decode($data)
{
	// Copyright (c) 2000  Dustin Sallings <dustin@spy.net>
	// Any work derived from this code must retain the above copyright.
	// Zie: http://bleu.west.spy.net/~dustin/kittycode/

	$parts=explode('.',$data);
	// Valid data will have five parts (the first and last are empty)
	// Parts are as follows:
	// 0 = empty (ALT-F10)
	// 1 = id (constant serienummer van het apparaat)
	// 2 = type
	//	E13 bij ons
	//		('.C3nZC3nZC3nZE3fZE3nlCNnY.bNjW.CxnYC3jZC3nWCxj2CG.')
	//	IBN bij boeken
	//		('.C3nZC3nZC3nZE3fZE3n1CNnY.cGen.ENr7C3fZCNb2C3T3DG.')
	//	IB5 bij boeken met een extraatje
	//		('.C3nZC3nZC3nZE3fZE3n1CNnY.cGf2.ENr7C3fZCNb2C3T3DNPZC3nZ.')
	//	C39 bij collegekaarten
	//		('.C3nZC3nZC3nZE3fZE3n1CNnY.ahb6.C3fYE3nWCW.')
	// 3 = code
	// 4 = empty
	// hier checken we of de 'vaste' parts 0, 2 en 4 wel correct zijn.
	// niet per se nodig, maar als die al niet goed zijn is de rest van het
	// resultaat ook niet te vertrouwen.
	if ( count($parts) != 5 || $parts[0] !== '' || $parts[4] !== '' ||
		!in_array(cuecat_convertbits($parts[2]),
			array('E13', 'IBN', 'IB5', 'C39'))) {
		return FALSE;
	}

	/*
	De code bestaat uit steeds een teken, dat 6 bits bevat. Elke 8 bits kun je
	dan een byte uitlezen, als je dat xor'd met 67 heb je de ascii code van
	het cijfer waar het om gaat.
	*/
	if (in_array(cuecat_convertbits($parts[2]), array('E13', 'IBN'))
			&& strlen($parts[3]) != 18) {
		user_error(sprintf(_("cuecat_decode: code dient 18 tekens te zijn ("), $parts[3]) . ")", E_USER_ERROR);
	}
	elseif (cuecat_convertbits($parts[2]) == 'IB5' && strlen($parts[3]) != 24) {
		user_error(sprintf(_("cuecat_decode: code dient 24 tekens te zijn ("), $parts[3]) . ")", E_USER_ERROR);
	}
	elseif (cuecat_convertbits($parts[2]) == 'C39' && strlen($parts[3]) != 10) {
		user_error(sprintf(_("cuecat_decode: code dient 10 tekens te zijn ("), $parts[3]) . ")", E_USER_ERROR);
	}

	if (in_array(cuecat_convertbits($parts[2]), array('E13', 'IBN', 'IB5'))) {
		if (!is_EAN($EAN = substr(cuecat_convertbits($parts[3]), 0, 13))) {
			user_error(_('Ongeldige EAN') . ": $EAN", E_USER_ERROR);
		}

		return $EAN;
	}
	else {
		//Geeft het studentnummer
		return cuecat_convertbits($parts[3]);
	}
}

function cuecat_convertbits($data) {
	// Copyright (c) 2000  Dustin Sallings <dustin@spy.net>
	// Any work derived from this code must retain the above copyright.
	// Zie: http://bleu.west.spy.net/~dustin/kittycode/

	$m = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-';
	$packer = 0; $result = '';
	for ($i=0; $i < strlen($data); $i++) {
		// Get the offset to the current character in our map
		$x = strpos($m,$data[$i]);
		if ($x<0) {
			user_error("cuecat_readbits: ongeldig teken ($data)", E_USER_ERROR);
		}

		// Pack them bits.
		$packer = $packer << 6 | $x;

		// every four bytes, we have three valid characters.
		if ($i % 4 == 3) {
			$result .= chr(($packer >> 16) ^ 67);
			$result .= chr(($packer >> 8 & 255) ^ 67);
			$result .= chr(($packer & 255) ^ 67);
			$packer=0;
		}
	}
	// Handel nu de laatste bits af.
	if ($i % 4 == 2) {
		$result .= chr(( $packer >> 4) ^ 67);
	}
	elseif ($i % 4 == 1) {
		$result .= chr(( $packer >> 10) ^ 67);
		$result .= chr((( $packer >> 2) & 255) ^ 67);
	}

	return $result;
}

function bookweb_get_EAN($data, $codeonly = false)
{
	// trailing spaces enzo wegdoen
	$data = trim($data);

	// is dit cuecat data?
	$ean = cuecat_decode($data);
	if ($ean && is_ean($ean)) return $ean;

	// strip koppeltekens die evt in een EAN/ISBN zitten
	$data = str_replace('-', '', $data);

	// is dit een isbn?
	if (is_isbn($data)) return isbn_to_ean($data);

	// is dit een niet-ISBN EAN?
	if (is_EAN($data)) return $data;

	// is dit een artikelnr?
	if (is_artnr($data)) return artnr_to_EAN($data);

	// niet proberen te guessen, alleen gevalideerde codes toegestaan
	if ( $codeonly ) return false;

	// probeer EAN te vinden adhv boekentabel
	return getBoeken($data);
}

// Converteer een ISBN naar een EAN
function isbn_to_ean($isbn, $fatal = TRUE)
{
	$isbn = str_replace("-", "", $isbn);

	if (!is_isbn($isbn) && $fatal) {
		user_error(_("Is geen isbn!"), E_USER_ERROR);
	}

	$eandata = (strlen($isbn) == 10)
		? '978' . substr($isbn, 0, 9)
		: substr($isbn, 0, 12);

	return ean_addcheck($eandata, $fatal);
}

function artnr_to_ean($artnr)
{
	if (!is_artnr($artnr)) {
		user_error(_("Is geen artnr!"), E_USER_ERROR);
	}
	if (strtolower($artnr{0}) == 'a') {
		return ean_addcheck("210".substr(1000000000+substr($artnr,1), 1));
	} else {
		return ean_addcheck("220".substr(1000000000+substr($artnr,1), 1));
	}
}

/**
	Zoekt ISBNs uit een tekst en retourneert een array met gevonden ISBNs.
*/
function findISBNs($text){
	preg_match_all("/(\d+-)?(\d+)-(\d+)-(\d+)-([0-9xX])/", $text, $out);

	// in $out zitten nu mogelijke ISBNs
	$res = array();
	foreach ($out as $set){
		foreach ($set as $isbn){
			// Is het een geldige ISBN?
			if (is_isbn($isbn)){
				$res[] = $isbn;
			}
		}
	}

	return $res;
}

/**
	Zoekt ISBNs in een tekst en retourneert dezelfde tekst, waarin de ISBNs
	zijn vervangen door links naar boekinfo.
*/
function replaceISBNs($text){
	global $logger;

	if (!hasAuth("bestuur")) return $text; // alleen >= bestuur heeft wat aan boekinfo

	$isbns = findISBNs($text);
	foreach ($isbns as $isbn){
		$logger->info("replace $isbn");
		$EAN = isbn_to_ean($isbn);
		$link = print_EAN_bi($EAN);

		$text = str_replace($isbn, $link, $text);
	}

	return $text;
}

