#!/usr/bin/php
<?php
require_once(__DIR__ . '/minify/src/Minify.php');
require_once(__DIR__ . '/minify/src/CSS.php');
require_once(__DIR__ . '/minify/src/JS.php');
require_once(__DIR__ . '/minify/src/Exception.php');
require_once(__DIR__ . '/minify/src/Converter.php');

use MatthiasMullie\Minify;
$minifier = new Minify\CSS(file_get_contents($argv[1]));
$min_string = $minifier->minify();
$file = fopen($argv[2], 'w');
fwrite($file, $min_string);
fclose($file);
