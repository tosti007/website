$(function() {
	$('.to-check').each(function() {
		var $cieId = $('.cie-id').attr('id');
		var $lidId = $(this).attr('id').replace('plane-', '');
		$.ajax({
			url: '/Ajax/Cie/systeemCheck?cieid='+$cieId+'&lidid='+$lidId,
			async: true,
			success: function(data)
			{
				if(data.code == 'OK')
				{
					$('#plane-'+data.lidid).parent().append('<span class="fa fa-check" style="color: green;"></span>');
					$('#plane-'+data.lidid).remove();
				}
				else if(data.code == 'WAARSCHUWING')
				{
					$('#plane-'+data.lidid).parent().append(
						'<a href="'+data.url+'"><span class="fa fa-exclamation-triangle" style="color: dark-yellow;"></span></a>');
					$('#plane-'+data.lidid).remove();
				}
				else
				{
					$('#plane-'+data.lidid).parent().append(
						'<a href="'+data.url+'"><span class="fa fa-times" style="color: red;"></span></a>');
					$('#plane-'+data.lidid).remove();
				}
			}
		});
	});
});
