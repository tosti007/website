function formatCurrency(value) {
	return "€" + value.toFixed(2);
}

var BetalingRow = function(data) {
	var self = this;

	self.id = data.id;
	self.verdeling = ko.observable(data.verdeling);
	self.naam = ko.observable(data.naam);
	self.moetBetalen = ko.observable(data.moetBetalen);
	self.heeftIngelegd = ko.observable(data.heeftIngelegd);
};

var mapping = {
	'betalingen': {
		create: function(options) {
			return new BetalingRow(options.data);
		}
	}
};

var Betaling = function() {
	var self = this;

	ko.mapping.fromJS(bonJSON, mapping, self);

	self.totaalBetalen = ko.pureComputed(function() {
		var total = 0;
		$.each(self.betalingen(), function() { total += parseFloat(this.moetBetalen()); })
		total += parseFloat(self.verdeelBedrag());
		total = 0.01 * Math.round(total * 100);
		return total;
	});

	self.totaalIngelegd = ko.pureComputed(function() {
		var total = 0;
		$.each(self.betalingen(), function() { total += parseFloat(this.heeftIngelegd()); })
		total = 0.01 * Math.round(total * 100);
		return total;
	});

	self.addBetaling = function(id, naam) {
		self.betalingen.push(new BetalingRow({id: id, naam: naam, verdeling: false, moetBetalen: 0, heeftIngelegd: 0}));
	};

	self.removeBetaling = function(id) {
		var toRemove = self.betalingen().filter(function(e) { return e.id == id; });
		$.each(toRemove, function() {
			self.betalingen.remove(this);
		});
	};

	self.heeftNegatief = ko.pureComputed(function() {
		return self.betalingen().filter(function(e) {
			return e.heeftIngelegd() < 0 || e.moetBetalen() < 0;
		}).length > 0
	});

	self.heeftTeGroot = ko.pureComputed(function() {
		return self.betalingen().filter(function(e) {
			return e.heeftIngelegd() >= 1000000 || e.moetBetalen() >= 1000000;
		}).length > 0
	});

	self.ingelegdWordtVerdeeld = ko.pureComputed(function() {
		if(self.verdeelBedrag() == 0) {
			return true;
		}
		return self.betalingen().filter(function(e) { return e.verdeling(); }).length >= 1;
	});

	self.isBetrokken = ko.pureComputed(function() {
		var lidRow = self.betalingen().filter(function(e) { return e.id == $('input[name=lidid]').val(); });
		if(lidRow.length == 0) {
			return false;
		} else {
			lidRow = lidRow[0];
		}
		return lidRow.moetBetalen() > 0 || lidRow.heeftIngelegd() > 0 || lidRow.verdeling();
	});

	self.kanNietInvoeren = ko.pureComputed(function() {
		if((self.totaalIngelegd() == 0 && self.totaalBetalen() == 0)
			|| self.totaalIngelegd() != self.totaalBetalen()
			|| self.betalingen().length < 2
			|| self.omschrijving().length == 0
			|| self.heeftNegatief()
			|| self.heeftTeGroot()
			|| !self.ingelegdWordtVerdeeld()
			|| !self.isBetrokken()) {
			return true;
		}
	});
};

var betaling = new Betaling();
ko.applyBindings(betaling);

$(document).ready(function() {
	$(".persoonDropdown").on("change", ".PersoonSelectOptie>.PersoonVerzamelingSelectieBox", function() {
		var str = $(this).attr('name');
		str = str.split('[');
		str = str[1];
		str = str.slice(0,-1);
		name = $(this).siblings('span').eq(0).children().eq(0).html();
		if($(this).is(":checked")) {
			betaling.addBetaling(str, name);
		} else {
			betaling.removeBetaling(str);
		}
	});

	$('select[name="CommissieVerzameling[betrokkenCie]"]').change(function() {
		zetCieTussenOpties('LidVerzameling', 0);
	});
	zetCieTussenOpties('LidVerzameling', 0);

	$('[name=checkbox-all]').click(function() {
		var val = $(this).is(':checked');

		$('[name^=bonVerdeelOver]').each(function() {
			if($(this).is(':checked') != val) {
				$(this).trigger('click');
			}
		});
	});
});

function zetCieTussenOpties(naam, single) {
	var cie = $('select[name="CommissieVerzameling[betrokkenCie]"] :selected')[0].value;
	$.post("/Ajax/Form/defaultFormPersoonVerzameling/CieZoek",
		{cieId: cie, naam: naam, single: single},
		Form_defaultFormPersoonVerzameling_vervangOngeselecteerden(naam)
	);
}
