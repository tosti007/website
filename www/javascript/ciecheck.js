$(function() {
	var $lidId = $('.lid-id').attr('id');
	$.ajax({
		url: '/Ajax/Cie/cieCheck?lidid='+$lidId,
		async: true,
		success: function(data)
		{
			if(data.status == 'OK' || data.cies === false)
			{
				$.each(data.cies, function(k, v) {
					if($('#wel-in #plane-' + v).length == 0) {
						$('#niet-in').append('<tr id=tr-'+v+'></tr>');
						$.ajax({
							url: '/Ajax/Cie/getCieAnchor?cielogin=' + v,
							async: true,
							success: function(data) {
								$('#tr-'+data.cielogin).append('<td>'+data.a+'</td>');
							}
						});
					} else {
						$('#wel-in #plane-'+v).remove();
					}
				});

				$('.rotating-plane').each(function() {
					$(this).removeClass('rotating-plane rotating-plane-small');
					$(this).addClass('fa fa-times');
					$(this).css({'color': 'red'});
				});
			}
			else
			{
				alert('Niet gelukt om de cies op te halen!');
			}
		}
	});
});
