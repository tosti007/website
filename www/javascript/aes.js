var timeout = null;

function closeAll(loginMenu) {
	$('.dropdown.menu-large').each(function(index, e2) {
		var el2 = $(e2).children('.dropdown-menu.megamenu').first();
		$(el2).removeClass('hovered');
		if (loginMenu === true)
			$(e2).removeClass('open');
	});
}

$(function() {
	$('.collapse.navbar-collapse').mouseleave(function() {
		closeAll();
	});

	// laat bootstrap de daarvoor aangewezen tooltips afhandelen.
	$('[data-toggle="tooltip"]').tooltip();

	$('.vlaggetje').hover(function() {
		closeAll();
	});

	$('tr[data-url]').click(function() {
		document.location = $(this).data('url');
	});

	$('.dropdown.menu-large').each(function(i, e) {
		$(this).mouseenter(function() {
			// Als we een menu enteren wat niet het login menu is, dan sluiten we het login menu
			closeAll($(this).parents('.navbar-right').length == 0);

			var el = $(e).children('.dropdown-menu.megamenu').first();
			el.addClass('hovered');
			clearTimeout(timeout);
		});
		$(this).mouseleave(function() {
			var el = $(e).children('.dropdown-menu.megamenu').first();
			clearTimeout(timeout);
			if($(e).hasClass('menu-last')) {
				timeout = setTimeout(function() {
					el.removeClass('hovered');
				}, 500);
			} else {
				el.removeClass('hovered');
			}
		});
	});

	$('.dropdown-menu.megamenu').click(function(e) {
		if (e.ctrlKey || e.metaKey) {
			$(this).removeClass('hovered');
		}
	});

	$('.dropdown-menu.megamenu').dblclick(function(e) {
		e.stopPropagation();
	});

	$('.dropdown.menu-large').dblclick(function(e) {
		// Probeer de eerste link in het menu te vinden wat eigenlijk linkt
		// naar wat het menukopje zegt
		//TODO Dit kan vast en zeker veel mooier!
		var url = $(this).find('ul:first li:first ul:first li:first a:first').attr('href');
		// Als er geen link is gevonden is de persoon niet ingelogd en 
		// gaan we dus naar de logindatapagina
		if (url === undefined) {
			url = "/Service/Intern/Logindata";
		}
		window.location.href = url;
	});

	$('.navbar-right .dropdown-menu .inlog-form .login-field').click(function() {
		$(this).parents('li.dropdown.menu-large').addClass('open');
	});

	$(document).click(function (event) {
		var clickover = $(event.target);
		var _opened = $(".navbar-collapse-small .navbar-collapse").hasClass("in");
		if (_opened === true && !clickover.hasClass("navbar-toggle")) {
			$("button.navbar-toggle").click();
		}
	});

	// De cies/groepen/disputen submenuutjes in het persoonlijke menu klappen
	// uit/in als je erop klikt
	$('.hideableCiesHeader').click(function() {
		if($(this).hasClass('expandedCies')) {
			$(this).find('li.hideableCies').attr('style', 'display: none !important');
			$(this).find('.caretClass').removeClass('dropup');
			$(this).removeClass('expandedCies');
		} else {
			$(this).find('li.hideableCies').attr('style', 'display: block !important');
			$(this).find('.caretClass').addClass('dropup');
			$(this).addClass('expandedCies');
		}
	});

	$('.datepicker-input').datepicker({
		showOtherMonths: true,
		selectOtherMonths: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});
	$('.timepicker-input').timepicker({
		timeFormat: 'HH:mm:ss',
	});
	$('.datetimepicker-input').datetimepicker({
		showOtherMonths: true,
		selectOtherMonths: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		timeInput: true,
		timeFormat: 'HH:mm:ss'
	});

	$('map').imageMapResize();

	$('img.lazy').lazyload().removeClass('lazy');

	if ($(".lastBadges")[0]){
		$(".lastBadges").children('p').each(function() {
			$("#achievementsInfoBox").append($(this).html() + "<br/>");
		});
		$("#achievementsInfoBox").show();
	}

	$('.systeemCheckbox').click(function() {
		if($('.systeemCheckbox').is(":checked")) {
			$('.inlog-form').attr('action', '/space/auth/systeem.php');
		} else {
			$('.inlog-form').attr('action', '/space/auth/login.php');
		}
	});

	$("input[type='file']").fileinput({
		'showUpload': false,
		'language': 'nl',
	});
	$('a[href*="iba.a-eskwadraat.nl"]').click(function(e){
		e.preventDefault();
		$.ajax('/Ajax/Achievements/Ibablog')
		window.location = $(this).attr('href');
	});
	$('#checkbox-all[type="checkbox"]').click(function(e){
		var alleCheckboxen = $(this).closest("form").find('input[type="checkbox"]');
		if($(this).is(":checked")){
			alleCheckboxen.prop('checked', true);
		}
		else{
			alleCheckboxen.prop('checked', false);	
		}
	});
});
