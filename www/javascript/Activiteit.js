(function($){
	$.checkAllBoxes = function(event) {
		event.data.form.find('input[type=checkbox]').each(function() {
			$(this).prop('checked', event.data.check);
		});
		return false;
	};

	$.doAction = function(event) {
		event.data.form.find('input[name="Deelnemers[Actie]"]').val(event.data.value);
		event.data.form.submit();
		return false;
	};
})(jQuery);

$(function() {
	var $form = $('form[name="DeelnemersLijst"]');

	$('#koppenbladDeelnemers').click({form: $form, value: 'koppenblad'}, $.doAction);
	$('#kartDeelnemers').click({form: $form, value: 'kart'}, $.doAction);
	$('#delDeelnemers').click({form: $form, value: 'uitschrijven'}, $.doAction);

	$('#selDeelnemers').click({form: $form, check: true}, $.checkAllBoxes);
	$('#deselDeelnemers').click({form: $form, check: false}, $.checkAllBoxes);

	$('input[name="ActiviteitInformatie[inschrijfbaar][check]"').change(function() {
		if($(this).is(":checked")) {
			$('#Activiteit_Vragen').hide();
			$('#Activiteit_Wijzig_Inschrijfbaar').show();
		} else {
			$('#Activiteit_Vragen').show();
			$('#Activiteit_Wijzig_Inschrijfbaar').hide();
		}
	});

	var vraagCounter = 1 + parseInt($('input[name="aantalVragen"]').val());

	$('input[name="ActiviteitInformatie[Herhalend][check]"').change(function() {
		if($(this).is(":checked")) {
			$('#toevoegen_Wel').show();
			$('#toevoegen_Nee').hide();
			// neem de data van niet-herhalend over naar wel-herhalend
			$('input[name="ActiviteitInformatie[Herhalend][vanaf]"]').val($('input[name="ActiviteitInformatie[MomentBegin]"]').val());
			$('input[name="ActiviteitInformatie[Herhalend][begin]"]').val($('input[name="TimeActiviteitInformatie[MomentBegin]"]').val());
			$('input[name="ActiviteitInformatie[Herhalend][tot]"]').val($('input[name="ActiviteitInformatie[MomentEind]"]').val());
			$('input[name="ActiviteitInformatie[Herhalend][eind]"]').val($('input[name="TimeActiviteitInformatie[MomentEind]"]').val());
		} else {
			$('#toevoegen_Wel').hide();
			$('#toevoegen_Nee').show();
			// neem de data van herhalend over naar niet-herhalend
			$('input[name="ActiviteitInformatie[MomentBegin]"]').val($('input[name="ActiviteitInformatie[Herhalend][vanaf]"]').val());
			$('input[name="TimeActiviteitInformatie[MomentBegin]"]').val($('input[name="ActiviteitInformatie[Herhalend][begin]"]').val());
			$('input[name="ActiviteitInformatie[MomentEind]"]').val($('input[name="ActiviteitInformatie[Herhalend][tot]"]').val());
			$('input[name="TimeActiviteitInformatie[MomentEind]"]').val($('input[name="ActiviteitInformatie[Herhalend][eind]"]').val());
		}
	});

	var Optie = function(data) {
		var self = this;

		self.optie = ko.observable(data);
	}

	var mapping2 = {
		'opties': {
			create: function(options) {
				return new Optie(options.data);
			}
		}
	};

	var ActiviteitVraag = function(data) {
		var self = this;

		ko.mapping.fromJS(data, mapping2, self);

		self.name_NL = ko.computed(function() {
			return 'ActiviteitVraag['+self.id()+'][Vraag][nl]';
		}, this);

		self.name_EN = ko.computed(function() {
			return 'ActiviteitVraag['+self.id()+'][Vraag][en]';
		}, this);

		self.name_type = ko.computed(function() {
			return 'ActiviteitVraag['+self.id()+'][Type]';
		}, this);

		self.addOptie = function() {
			self.opties.push(new Optie(''));
		};

		self.removeOptie = function(optie) {
			self.opties.remove(optie);
		};
	};

	var mapping = {
		create: function(options) {
			return new ActiviteitVraag(options.data);
		}
	};

	var ViewModel = function() {
		var self = this;

		self.vragen = ko.mapping.fromJS(vragenJSON, mapping);

		self.zichtbaar = ko.observable(zichtbaarJSON);

		self.momentBegin = ko.observable(momentBeginJSON);
		self.momentEind = ko.observable(momentEindJSON);

		self.momentBegin.subscribe(function() {
			var mb = moment(self.momentBegin());
			var me = moment(self.momentEind());
			if(mb.diff(me) > 0)
			{
				me = mb;
				me.add(moment.duration(1, 'hour'));
				self.momentEind(me.format('YYYY-MM-DD HH:mm:ss'));
			}
		});

		self.addVraag = function() {
			self.vragen.push(new ActiviteitVraag({
				id: 1+self.vragen().length,
				type: 'STRING',
				vraag_NL: '',
				vraag_EN: '',
				opties: ko.observableArray(),
			}));
		};
		self.removeVraag = function(vraag) {
			// TODO....
			vraag.vraag_NL = vraag.vraag_EN = '';
			self.vragen.remove(vraag);
		};
	}

	ko.applyBindings(new ViewModel());
});
