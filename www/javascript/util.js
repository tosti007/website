var hiddenStyle = 'none';
var showStyle   = 'inline';

function fotoPreview()
{
	xOffset = 10;
	yOffset = 30;

	// these 2 variable determine popup's distance from the cursor
	// you might want to adjust to get the right result

	/* END CONFIG */
	$(".persoonLink").hover(function(e){
		this.t = this.title;
		this.title = "";
		var c = (this.t != "") ? "<br/>" + this.t : "";
		var $p = $('<p id="persoonLink">');
		var $div = $('<div class="rotating-plane">');
		var $img = $('<img src="' + this.rel + '" alt="foto" style="max-width: 139px; max-height: 100px; height: auto; width: auto;" />');
		$("body").append($p.append($div).append($img));
		$img.load(function() {
			$div.remove();
		});
		//$("body").append("<p id='persoonLink'><img src='"+ this.rel +"' alt='url preview' />"+ c +"</p>");
		$("#persoonLink")
		.css("top",(e.pageY - xOffset) + "px")
		.css("left",(e.pageX + yOffset) + "px")
		.css("position", "absolute")
		.fadeIn("fast");
	},
	function(){
		this.title = this.t;
		$("#persoonLink").remove();
	});
	$(".persoonLink").mouseout(function() {
		this.title = this.t;
		$("#persoonLink").remove();
	});
	$(".persoonLink").mousemove(function(e){
		$("#persoonLink")
		.css("top",(e.pageY - xOffset) + "px")
		.css("left",(e.pageX + yOffset) + "px");
	});
};

$(document).ready( 
	function()
	{
		//Voor oude browsers: de placeholder hint voor gebruikersnaam en wachtwoord
		setupPlaceHolder();
		//Zorg dat je op de logopagina terecht komt bij rechtsklik op het logo
		logoRechtsKlik();
		//Zorg dat de datepicker geactiveerd wordt
		jQueryInitDatePicker();
		initFeedbackFormulier();
		//Kijk of er formulieren zijn met een token, en zet de timeout
		$('form[data-tokenTTL]').each(function() {
			var token = $(this).find('input[name="token"]').val();
			setTimeout(
				function(){formTimeout(token)}, //We gebruiken een closure om parameters mee te geven
				this.getAttribute('data-tokenTTL')*1000
			);
		});
		//Kijk of er inputs zijn die hun form autosubmitten
		$('*[data-autosubmit=true]').each(function() {
			$(this).change(function() {
				$(this).closest("form").submit();
			});
		});
		//Hideable divs afwerken
		fixHideableDivs();
		//Confirm buttons activeren
		activateConfirms();
		//CKEditor aanzetten
		if($('textarea[data-ckeditor=true]').length)
		{
			//Pad goedzetten anders denkt ckeditor.js dat hij op de huidige pagina leeft
			window.CKEDITOR_BASEPATH = '/javascript/extern/ckeditor/';
			//CKEditor dynamisch inladen
			$.getScript(CKEDITOR_BASEPATH + 'ckeditor.js', function(){
				$('textarea[data-ckeditor=true]').each(function() {
					CKEDITOR.replace(this);
				});
			});
		}
		//Maak de jumbotron hideable
		$('.jumbotron-arrow').click(hideHomeJumbotron);
		//Maak rekeningnummer doubleklikselecteerbaar
		$('.rekeningnummer').dblclick(function() {
			win = window;
			el = $(this).get(0);
			var doc = win.document, sel, range;
			if (win.getSelection && doc.createRange) {
				sel = win.getSelection();
				range = doc.createRange();
				range.selectNodeContents(el);
				sel.removeAllRanges();
				sel.addRange(range);
			} else if (doc.body.createTextRange) {
				range = doc.body.createTextRange();
				range.moveToElementText(el);
				range.select();
			}
		});
		fotoPreview();

		/*$('.aes-container a').each(function() {
			var href = $(this).attr('href');

			if(href === undefined || href === null || href === '#' || href === 'javascript:void()' || href.charAt(0) != '/')
				return;

			$.ajax({ url: href,
				type: 'HEAD',
				async: true,
				beforeSend: function(jqXHR, settings) {
					jqXHR.url = settings.url;
				},
				error: function(jqXHR, exception) {
					var $curr = $('.aes-container a[href="' + jqXHR.url + '"]');
					if($curr.attr('style') === undefined)
						$curr.attr('style', 'cursor: not-allowed;');
					else
						$curr.attr('style', $curr.attr('style') + ' cursor: not-allowed;');
					$curr.click(function() {
						event.preventDefault();
					});
				},
				success: function(data){}
			});
		});*/
	} 
);

function formButton(idname, event)
{
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (event) keycode = event.which;
	else return true;
	
	if (keycode == 13)
	{
		document.getElementById(idname).click();
		return false;
	}
	return true;
}

function trim(text){
	return text.replace(/^\s+|\s+$/g,"");
}
	
function testPlaceHolder()
{
	var input = document.createElement("input");
	if ("placeholder" in input)
		return true;
	else
		return false;
}

function setupPlaceHolder() {
	var login = document.getElementById("loginnaam");
	if (login && !testPlaceHolder())
	{
		login.value = "Login";
		login.style.color = "#AAA";
		login.onfocus = function() {
			if (login.value == "Login")
			{
				login.value = "";
				login.style.color = "#000";
			}
		};
		login.onblur = function() {
			if (login.value == "")
			{
				login.value = "Login";
				login.style.color = "#AAA";
			}
		};
		
		var wachtwoord = document.getElementById("password");
		wachtwoord.value = "Wachtwoord";
		wachtwoord.style.color = "#AAA";
		wachtwoord.onfocus = function() {
			if (wachtwoord.value == "Wachtwoord")
			{
				wachtwoord.value = "";
				wachtwoord.style.color = "#000";
			}
		};
		wachtwoord.onblur = function() {
			if (wachtwoord.value == "")
			{
				wachtwoord.value = "Wachtwoord";
				wachtwoord.style.color = "#AAA";
			}
		};
	}
	if(login)
		login.focus();
}

//Ajax shopfuncties
var shopList = [];
function shopLidAjax(lid, ref)
{
	shopList.push(ref);
	$.ajax({ type: "POST", url: '/Ajax/Persoon/Shop', data: {lid: lid} }).done(function(html) {shopLidAjaxKlaar(html);});
}
function unShopLidAjax(lid, ref)
{
	shopList.push(ref);
	$.ajax({ type: "POST", url: '/Ajax/Persoon/UnShop', data: {lid: lid} }).done(function(html) {shopLidAjaxKlaar(html);});
}
function shopLidAjaxKlaar(html)
{
	$(shopList.shift()).replaceWith(html);
}
function shopCieAjax(cie, icon, ref)
{
	shopList.push(ref);
	$.ajax({ type: "POST", url: '/Ajax/Cie/Shop', data: {cie: cie, icon: icon} }).done(function(html) {shopCieAjaxKlaar(html);});
}
function unShopCieAjax(cie, icon, ref)
{
	shopList.push(ref);
	$.ajax({ type: "POST", url: '/Ajax/Cie/UnShop', data: {cie: cie, icon: icon} }).done(function(html) {shopCieAjaxKlaar(html);});
}
function shopCieAjaxKlaar(html)
{
	$(shopList.shift()).replaceWith(html);
}

/* Dutch (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Mathias Bynens <http://mathiasbynens.be/> */
$.datepicker.regional.nl = {
	closeText: 'Sluiten',
	prevText: 'Vorige',
	nextText: 'Volgende',
	currentText: 'Vandaag',
	monthNames: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni',
	'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
	monthNamesShort: ['Jan', 'Feb', 'Mrt', 'Apr', 'Mei', 'Jun',
	'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
	dayNames: ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'],
	dayNamesShort: ['Zon', 'Maa', 'Din', 'Woe', 'Don', 'Vri', 'Zat'],
	dayNamesMin: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
	weekHeader: 'Wk',
	dateFormat: 'dd-mm-yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''
};

// Ja, een OR...
// Als $.datepicker.regional[lang] undefined is, fallback op engels
if (typeof lang !== "undefined") {
	$.datepicker.setDefaults($.datepicker.regional[lang] || $.datepicker.regional[""]);
}

jQueryInitDatePicker = function()
{
	var dateTest = document.createElement('input');
	var errors = false;
	try
	{
		//Gedonder met IE
		dateTest.type = 'date';
	}
	catch(err)
	{
		errors = true;
	}
	if(errors || dateTest.type != 'date')
	{
		//de browser geeft zelf geen kalendertje weer, gebruik jQuery-UI
		$('.date').datepicker({
			dateFormat:'yy-mm-dd',
			firstDay: 1
			});
	}
	//Hier geen check, want .dateUitgeklapt is een div dus zal nooit van de browser een kalender krijgen
	$('.dateUitgeklapt').datepicker({
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		numberOfMonths: 2,
		onSelect: Form_defaultFormDatetimeVerzameling_selecteer
	});
};

function initFeedbackFormulier()
{
	$("#fbOpenerPos").click(function(){return openFeedbackFormuliertje('pos');});
	$("#fbOpenerNeg").click(function(){return openFeedbackFormuliertje('neg');});
}

function openFeedbackFormuliertje(mood)
{
	$.ajax('/Ajax/Achievements/FeedbackForm');
}

function logoRechtsKlik()
{
	$('#aeslogo').contextmenu(function(e)
	{
		// check if right button is clicked
		if(e.button === 2)
		{
			$("#logoModal").modal( "show" );
			e.preventDefault();
		}
	})
}

function fixHideableDivs()
{
	$('div[data-hideable]').each(function() {
		var icon = $('#' + this.id + '_icon');
		//Ze staan standaard visible voor niet JS-gebruikers, maak ze hidden indien gewenst:
		if(this.getAttribute('data-hideable') == 'hidden')
		{
			this.style.display = 'none';
			icon.attr("src", '/Layout/Images/Icons/expand.png');
		}
		//Als anchor wordt gedrukt ondernemen we actie, dat is de parent van de icon
		icon.parent().click(
			{'div': this, 'icon': icon}, 
			function(event) {
				var state = event.data.div.getAttribute('data-hideable')
				if(state == 'hidden')
				{
					event.data.div.style.display = 'block';
					event.data.div.setAttribute('data-hideable', 'visible');
					event.data.icon.attr("src", '/Layout/Images/Icons/collapse.png');
				}
				else
				{
					event.data.div.style.display = 'none';
					event.data.div.setAttribute('data-hideable', 'hidden');
					event.data.icon.attr("src", '/Layout/Images/Icons/expand.png');
				}
				//Stop de event propagation
				return false;
			}
		);
	});
}

function activateConfirms()
{
	$('*[data-confirmation]').each(function() {
		$(this).click(
			{'element': this},
			function(event) {
				return confirm($(event.data.element).attr('data-confirmation'));
			}
		);
	});
}

function hideHomeJumbotron()
{
	$('.jumbotron-arrow').css({
		"-webkit-transform": "rotate(270deg)",
		"-moz-transform": "rotate(270deg)",
		"transform": "rotate(270deg)" /* For modern browsers(CSS3)  */
	});
	$('.jumbotron-home').slideUp(500, function() {
		$('.jumbotron-arrow').off('click');
		$('.jumbotron-arrow').click(showHomeJumbotron);
	});
	$('.jumbotron-main').find('.btn-lg').each(function() {
		$(this).addClass('btn-sm', 500).removeClass('btn-lg', 500);
	});
	$('.jumbotron').animate({'padding': '15px 60px 1px 60px'}, 500);
	return;
}

function showHomeJumbotron()
{
	$('.jumbotron-arrow').css({
		"-webkit-transform": "rotate(0deg)",
		"-moz-transform": "rotate(0deg)",
		"transform": "rotate(0deg)" /* For modern browsers(CSS3)  */
	});
	$('.jumbotron-home').slideDown(500, function() {
		$('.jumbotron-arrow').off('click');
		$('.jumbotron-arrow').click(hideHomeJumbotron);
	});
	$('.jumbotron-main').find('.btn-sm').each(function() {
		$(this).removeClass('btn-sm', 500).addClass('btn-lg', 500);
	});
	$('.jumbotron').animate({'padding': '48px 60px 48px 60px'}, 500);
	return;
}

// maak een nieuw dialoogje
function showDialog(title, msg)
{
	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_DEFAULT,
		title: title,
		message: msg
	});
}
