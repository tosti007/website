var isMouseDown = false;
$(document).ready( function () {
	$("[type=radio]")
	.mouseover(function () {
		if (isMouseDown) {
		$(this).attr('checked',true);
		}
	})
	$("[type=radio]")
	.mousedown(function () {
		$(this).attr('checked',true);
	})
});

$(document)
.mouseup(function () {
	isMouseDown = false;
})
.mousedown(function () {
	isMouseDown = true;
});

$(function(){
	$.extend($.fn.disableTextSelect = function() {
		return this.each(function(){
			if($.browser.mozilla){//Firefox
				$(this).css('MozUserSelect','none');
			}else if($.browser.msie){//IE
				$(this).bind('selectstart',function(){isMouseDown =true; return false;});
			}else{//Opera, etc.
				$(this).mousedown(function(){isMouseDown = true; return false;});
			}
		});
	});
	$('.noSelect').disableTextSelect();//No text selection on elements with a class of 'noSelect'
});
