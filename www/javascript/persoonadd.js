function mensjeaddHomedir(persoonid, login)
{
	$.ajax({
		url: '/Ajax/Persoon/MensjeaddHomedir?id=' + persoonid + '&login=' + login,
		success: function(data) {
			if(data.status == 'OK') {
				$('.paragraph').append('Homedir aangemaakt!<br/>Overgaan op wachtwoord...<br/><br/>');
				mensjeaddPassword(persoonid, login);
			} else {
				$('.paragraph').append([
					'Homedir aanmaken niet gelukt!<br/>De foutmelding was: ',
					$('<pre>').text(data.fout.join("<br/>"))
				]);
				$('.paragraph').css('color', 'red');
				$('.rotating-plane').remove();
			}
		}
	});
}

function mensjeaddPassword(persoonid, login)
{
	$.ajax({
		url: '/Ajax/Persoon/MensjeaddPassword?id=' + persoonid + '&login=' + login,
		success: function(data) {
			$('.rotating-plane').remove();
			if(data.status == 'OK') {
				$('.paragraph').append([
					'Random wachtwoord gegenereerd:',
					'<br/><b>' + data.output + '</b><br/>',
					'<br/>Mensje heeft nu een systeemaccount!<br/>Gefeliciteerd!<br/>',
					'<a class="btn btn-primary" href="' + $('.persoonurl').html() + '">Terug naar het lid</a>'
				]);
				mensjeaddSendmail(persoonid, login);
			} else {
				$('.paragraph').append([
					'Random wachtwoord aanmaken niet gelukt!<br/>De foutmelding was: ',
					$('<pre>').text(data.fout.join("<br/>"))
				]);
				$('.paragraph').css('color', 'red');
			}
		}
	});
}

function mensjeaddSendmail(persoonid, login)
{
	$.ajax({
		url: '/Ajax/Persoon/MensjeaddSendmail?id=' + persoonid + '&login=' + login
	});
}

$(function() {
	var persoonid = $('.persoon-id').html();
	var login = $('.login-name').html();

	$.ajax({
		url: '/Ajax/Persoon/MensjeaddUser?id=' + persoonid + '&login=' + login,
		success: function(data) {
			if(data.status == 'OK') {
				$('.paragraph').append('Account aangemaakt!<br/>Overgaan op de homedir...<br/><br/>');
				mensjeaddHomedir(persoonid, login);
			} else {
				$('.paragraph').append([
					'Account aanmaken niet gelukt!<br/>De foutmelding was: ',
					$('<pre>').text(data.fout.join("<br/>"))
				]);
				$('.paragraph').css('color', 'red');
				$('.rotating-plane').remove();
			}
		}
	});
});
