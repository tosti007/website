(function($){
	var margin = 75;
	var marginTop = 75;

	var loadingID = 0;
	/*
	 * Functie om een nieuwe activiteit te setten als een nieuwe act geselecteerd is
	 */
 	$.fn.setNewAct = function($eventElem) {
		if($(this).hasClass('maand-row')) {
			return this;
		}

		$('#act-omschrijving').html('<div class="rotating-plane"></div>');

		// Deselecteer de oude geselecteerde
		$('#selected').attr('id', '');
		// Selecteerd de nieuwe
		$(this).attr('id', 'selected');
		// Kopieer de actdata naar de rechter-div
		if($(this).find('.act-data').html().length == 0)
		{
			var $elem = $(this);
			loadingID = $elem.find('.act-data').attr('id');
			$.ajax({
					url: "/Ajax/Activiteiten/geef_ajax",
					data: { activiteitID: $elem.find('.act-data').attr('id') }
				}).done(function(data) {
					$elem.find('.act-data').html(data);
					if(loadingID)
						$('#act-omschrijving').html($elem.find('.act-data').html());

					$('img.lazy').lazyload().removeClass('lazy');
			});
		}
		else
			$('#act-omschrijving').html($(this).find('.act-data').html());

		// Als we in telefoon-overzicht zitten willen we de act weer inklappen als
		// daarop geklikt is
		if($.getViewport().width < 992 && $(this).find('.act-data').hasClass('showable-act')) {
			if(!$.contains($(this).find('.act-data')[0], $eventElem[0])) {
				$('.showable-act').removeClass('showable-act');
			}
		} else{
			// Haal de class verantwoordelijk voor het laten zien van de act
			// op de mobiel weg...
			$('.showable-act').removeClass('showable-act');
			// ... en stop die class bij de nieuwe act
			$(this).find('.act-data').addClass('showable-act');
		}

		return this;
	};

	/*
	 * Functie die de hoogte van een element veranderd zodat het mooi
	 * responsive gedraagt
	 */
	$.fn.changeHeight = function() {
		var actRowTop = $('#act-row').offset().top;
		var scrollTop = $(window).scrollTop();

		margin = 5+Math.max(5, $(window).height()-$('.footer-scheiding').offset().top+scrollTop);

		if(actRowTop - scrollTop < margin) {
			$(this).height($(window).height() - margin - marginTop);
		} else {
			$(this).height($(window).height() - actRowTop + scrollTop - margin);
		}

		return this;
	}

	$.fn.changePosition = function() {
		var actRowTop = $('#act-row').offset().top;
		var scrollTop = $(window).scrollTop();

		if(actRowTop - scrollTop < marginTop) {
			var offsetMax = Math.min(marginTop, Math.max($(this).offset().top - scrollTop, marginTop));
			$(this).offset({top: offsetMax + scrollTop});
		} else {
			$(this).offset({top: actRowTop});
		}

		$(this).changeHeight();

		return this;
	}

	/*
	 * Functie die de huidige hoofte en breedte van de viewport teruggeeft
	 */
	$.getViewport = function() {
		var e = window, a = 'inner';
		if(!('innerWidth' in window)) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
	}

	/*
	 * Functie die element resizet als dat nodig is
	 */
	$.doResize = function() {
		// Als we op een klein scherm hoeft er niets geresizet te worden
		// alleen moet de act-omschrijving niet meer worden laten zien en de
		// act-tabel krijgt de maximale hoogte
		if($.getViewport().width < 992) {
			$("#act-omschrijving").hide();
			$("#act-table-div").css({"height": ""});
		} else {
			$("#act-omschrijving").show();
			//$("#act-table-div").changeHeight();
			//$("#act-omschrijving").changeHeight();
			$("#act-omschrijving").changePosition();
		}
	}
})(jQuery);

$(function() {
	$.doResize();

	$(window).resize(function() {
		$.doResize();
	});

	$(window).scroll(function() {
		if($.getViewport().width >= 992) {
			$("#act-omschrijving").changePosition();
		}
	});

	$('#act-table > tbody > tr').click(function(e) {
		$(this).setNewAct(e.target);
		location.href = '#' + $(this).find('.act-data').attr('id');
	});

	// Als er bij het laden van de pagina al een hash gezet is spring dan meteen
	// naar dat element
	if(window.location.hash) {
		var id = window.location.hash.substring(1);
		$('#'+id).parent().parent().parent().setNewAct($('#'+id).parent());
		// Hier doen we eerst een show omdat we niet met een hash naar een
		// hidden element kunnen springen
		$('#'+id).show();
		location.href = '#' + id;
		$('#'+id).hide();
	} else {
		$('#act-table > tbody > tr:not(.maand-row):first').setNewAct($('#act-table'));
	}
});
