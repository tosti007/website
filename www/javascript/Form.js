var form_defaultFormDatetimeVerzameling_data = new Array();
var form_defaultFormDatetimeVerzameling_geselecteerd = new Array();
var delay;

function Form_defaultFormDatetimeVerzameling_verander (elem)
{
	var regex = /DatetimeVerzameling\[([0-9]{9,11})\]/i;
	var id = regex.exec(elem.name)[1];

	if (elem.checked == true)
		form_defaultFormDatetimeVerzameling_geselecteerd.push(id);
	else
		for (var x in form_defaultFormDatetimeVerzameling_geselecteerd)
			if (form_defaultFormDatetimeVerzameling_geselecteerd[x] == id)
				form_defaultFormDatetimeVerzameling_geselecteerd.splice(x, 1);
}

function Form_defaultFormDatetimeVerzameling_selecteer(datum, herp, derp, tijdOverride)
{	//herp en derp zijn parameters die door jQuery gevuld kunnen worden, ik heb ze niet nodig!
	
	var tijd;
	//Als er al data waren ingevuld (bijv wijzigform), set de tijd dan meteen
	if(typeof tijdOverride !== 'undefined')
	{
		tijd = tijdOverride;
	}
	else //parse het tijdsveldje
	{
		tijd = document.getElementById('TimeForm_defaultFormDatetime_toevoegen').value;
		if(tijd.match(/^( |0|1|2)?[0-9]\:[0-6][0-9]$/) == null)
		{
			alert("De ingevoerde tijd is niet geldig! Gebruik bijvoorbeeld 17:00 of 9:30");
			document.getElementById('TimeForm_defaultFormDatetime_toevoegen').focus();
			return;
		}
	}
	var datumParts = datum.split("-");
	var tijdParts = tijd.split(":");
	
	//De maanden worden van 0 tot 11 geteld, dus trek daar één van af -_-
	var datumObject = new Date(datumParts[0], datumParts[1]-1, datumParts[2], tijdParts[0], tijdParts[1]);

	label = datumObject.toLocaleString("en-GB"); //voor weergeven
	id = datum + ' ' + tijd; //voor opslaan, php handelt de tijdzone af

	if(datumObject < new Date()){
		label += ". <strong>Let op: In het verleden!</strong>";	
	}
	
	//Geen datum toevoegen als deze al geselecteerd is
	for (var x in form_defaultFormDatetimeVerzameling_data)
		if (form_defaultFormDatetimeVerzameling_data[x] == id)
			return;
	
	form_defaultFormDatetimeVerzameling_data.push(id);

	var lijst = document.getElementById('Form_defaultFormDatetimeVerzameling_select_lijst');
	if (lijst == null)
	{
		var select = document.getElementById('Form_defaultFormDatetimeVerzameling_select');
		select.innerHTML = "<ul id='Form_defaultFormDatetimeVerzameling_select_lijst'></ul>";
		lijst = document.getElementById('Form_defaultFormDatetimeVerzameling_select_lijst');
	}

	var li = document.createElement('li');
	li.innerHTML = '<input type="hidden" value="0" name="DatetimeVerzameling['+id+']">'
			+ '<input type="checkbox" name="DatetimeVerzameling['+id+']" onchange="Form_defaultFormDatetimeVerzameling_verander(this)" checked>'
			+ '<span>'+label+'</span>';
	form_defaultFormDatetimeVerzameling_geselecteerd.push(id);

	lijst.appendChild(li);
}

function Form_defaultFormPersoonVerzameling_vervangOngeselecteerden(formID) {
	return function(data) {
		// reset selectAll zodat we alle resultaten ook meteen kunnen selecteren
		$("#PersoonVerzameling_selectAll").prop('checked', false);

		// onthoud alleen de geselecteerden
		var selected = $("#PersoonVerzamelingDropDown_"+formID+" div .PersoonVerzamelingSelectieBox:checked").parent();

		// en zet die terug in je lijst
		$("#PersoonVerzamelingDropDown_" + formID)
			.empty()
			.append(selected)
			.append(data);

		// verwijder duplicate mensen
		var alTegengekomen = [];
		$("#PersoonVerzamelingDropDown_" + formID).children().each(function (index) {
			if (alTegengekomen.indexOf($(this).attr("id")) !== -1) {
				$(this).detach();
			} else {
				alTegengekomen.push($(this).attr("id"));
			}
		});
	}
}

function Form_defaultFormPersoonVerzameling_selectAll() {
	var aanOfUit = $("#PersoonVerzameling_selectAll").prop("checked");
	$(".PersoonVerzamelingSelectieBox").each(function (index) {
		var oudeWaarde = $(this).prop("checked");
		if (oudeWaarde ^ aanOfUit) {
			$(this).prop("checked", aanOfUit);
			$(this).change();
		}
	});
	// als checkboxen veranderen, deselecteren ze selectAll
	$("#PersoonVerzameling_selectAll").prop("checked", aanOfUit);
}

function Form_defaultFormPersoonVerzameling_checkboxChecked() {
	$("#PersoonVerzameling_selectAll").prop("checked", false);
}

var searchRequest = null;
var changeTimer = false;

function Form_defaultFormPersoonVerzameling_zoek(formNaam, formID, single) {
	if(changeTimer !== false)
		clearTimeout(changeTimer);

	changeTimer = setTimeout(function()
	{

		if(searchRequest != null)
			searchRequest.abort();

		searchRequest = $.ajax({ type: "POST"
			, url: "/Ajax/Form/defaultFormPersoonVerzameling/Zoek"
			, data: {str: $("#PersoonVerzamelingZoekVak_" + formID).val(), naam: formNaam, single: single}
		}).done(function (html)
		{
			(Form_defaultFormPersoonVerzameling_vervangOngeselecteerden(formID))(html);
		});
	}, 300);
}

function Form_defaultFormPersoonVerzameling_laadKart(formNaam, formID, single) {
	$.post(
		"/Ajax/Form/defaultFormPersoonVerzameling/LaadKart",
		{naam: formNaam, single: single},
		Form_defaultFormPersoonVerzameling_vervangOngeselecteerden(formID)
	);
}

function Form_onDonaWijzigSubmit(formData) {
	// escaping attribute values in jquery :)
	checked = $(formData).find('input[name=Donateur\\[AnderPersoon\\]]:checked');
	if (checked.length == 0) {
		$('<input/>').attr('type', 'hidden').attr('name', 'Donateur[AnderPersoon]').appendTo(formData);
	}
	return true;
}

function formTimeout(token)
{
	var post = $.post('/Ajax/VernieuwToken', {'token':token})
	post.done(
		function(resultaat)
		{
			//resultaat 0 betekent dat de Token niet gevonden is, de gebruiker is echter wel nog ingelogd. Een zeer vreemde situatie! 
			if(resultaat == 0)
			{
				$('input[type="submit"]:not(.noDisable)').attr('disabled', true);
				showDialog("Je sessie is verlopen", "Er is iets misgegaan in ons sessiemanagement, zou je dit willen melden aan www@a-eskwadraat.nl? Sla je werk op, vernieuw de pagina en probeer het opnieuw.");
			}
			//Een ander resultaat betekent gelukt! resultaat bevat nu de nieuwe TTL
			else
			{
				setTimeout(
					function(){formTimeout(token)}, //We gebruiken een closure om parameters mee te geven, 
					(resultaat-5)*1000
				); //zet de nieuwe timer
			}
		}
	);
	//Als de gebruiker niet meer is ingelogd krijgt ie een 403 op de tokenRefresh Ajax call:
	post.fail(
		function()
		{
			$('input[type="submit"]:not(.noDisable)').attr('disabled', true);
			showDialog("Je sessie is verlopen", "Het lijkt er op dat je niet meer ingelogd bent, hierdoor kan je dit formulier niet meer gebruiken. Sla je werk op, log weer in en probeer het opnieuw.");
		}
	);
		
}

$(function() {
	$('#PersoonVerzamelingZoekVak_LidVerzameling').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
});
