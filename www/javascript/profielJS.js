function changeTo($item) {
	$(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
	$item.removeClass("btn-default").addClass("btn-primary");
	window.location = $item.attr('href');
}

if(window.location.hash) {
	var hash = window.location.hash;
	$('a[href="'+hash+'"]').trigger('click');
	changeTo($('a[href="'+hash+'"]'));
}

function initProfielFoto() {
	$("#profielFoto").click(function() {
		$("#profielFotoModal").show();
	});

	$("#close-modal").click(function() {
		$("#profielFotoModal").hide();
	});
	$("#profielFotoModal").click(function() {
		$("#profielFotoModal").hide();
	});
	$("#profielFotoModal > img, #profielFotoModal > #caption").click(function(event) {
		event.stopPropagation();
	});
}

$(function() {

	$(".btn-pref .btn").click(function () {
		changeTo($(this));
	});

	initProfielFoto();
});
