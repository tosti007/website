$(function() {
	$("#myCarousel2").carousel({
		interval: 5000
	});

	var counter2 = 0;
	var progressBar2;

	function progressBarRun2() {
		progressBar2 = setInterval(function() {
			counter2 += 25;
			if (counter2 > 100) {
				clearInterval(progressBar2);
				counter2 = 100;
			}
			$(".progressbar2").css("width", counter2 + "%");
		}, 1000);
	}
	progressBarRun2();

	$("#myCarousel2").on("slid.bs.carousel", function () {
		counter2 = 0;
		clearInterval(progressBar2);
		progressBarRun2();
	});
});
