$(document).ready(function() {
	setInterval( function() {
		var h = new Date().getHours();
		var m = new Date().getMinutes();
		$("#tijd").html(h + ":" + (m < 10 ? "0" : "") + m);
	;}, 1000);

	$("#aeslogo").click(function() {
		if ($('#ticker').width() != 0) hide_ticker();
		else                           show_ticker();
	});

	initSlideshow();
	show_ticker();
});

function initSlideshow() {
	var firstimage = true;
	$('.slideshow img').load(function() {
		$(this).parent().show();
		if ($(this).height() != 0)
			$(this).css('margin-top', (928 - $(this).height())/2);

		if (firstimage) {
			if ($(this).parent() != $(this).parent().parent().children().first()) {
				$(this).parent().hide();
				$(this).parent().parent().children().first().show();
			}
			firstimage = false;
		} else {
			$(this).parent().hide();
		}
	});
	$('.slideshow').cycle({
		fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
		timeout: 4000,
		after: onAfter, 
		before: onBefore
	});
}

function show_ticker() {
	$("#ticker").animate({"width": "1292px"},750, function() {
		$('#ticker_text').fadeIn();
		$("#ticker").animate({"background-color": "rgba(0,0,0,0.7)"}, 500, function() {});
	});
	$('.slideshow img').css('max-height','900px');
}

function hide_ticker() {
	$('#ticker_text').fadeOut();
	$("#ticker").animate({"background-color": "rgba(0,0,0,0)"}, 500, function() {
		$("#ticker").animate({"width": "0px"},750, function() {});
	});
	$('.slideshow img').css('max-height','1000px');
}

function onBefore() { 
	$('#ticker_text').fadeOut();
}

function onAfter() {
	$('#ticker_text').html("" + $(this).find('img').attr('alt'));
	$('#ticker_text').fadeIn();
}

function cmpDate(d1, d2)
{
	d1 = new Date(d1.valueOf());
	d2 = new Date(d2.valueOf());
	d1.setHours(0, 0, 0, 0);
	d2.setHours(0, 0, 0, 0);
	if (d1 < d2) return -1;
	if (d1 > d2) return 1;
	return 0;
}

function loadPosters() {
	$.getJSON('/Ajax/Activiteiten/tv_posters.json').done(function(data) {
		var slaLegeDagenOver = true;
		var dayNames = [
			"", // dagen beginnen bij de 1
			"Maandag",
			"Dinsdag",
			"Woensdag",
			"Donderdag",
			"Vrijdag"
		];
		testd = data;

		var monday = new Date();

		// set day to monday:
		// monday.setDate(monday.getDate() - monday.getDay() + 1);

		var days = [];
		while (monday.getDay() <= 5) {
			days.push({ 'date': new Date(monday.valueOf()), 'acts': []});
			monday.setDate(monday.getDate() + 1);
		}
		
		for (var i = 0; i < data.length; i++) {
			if (data[i].type == 'activiteit') {
				for (var j = 0; j < days.length; j++) {
					var day = days[j].date;
					if (cmpDate(data[i].momentBegin, day) <= 0 && cmpDate(day, data[i].momentEind) <= 0) {
						days[j].acts.push(data[i]);
						break;
					}
				}
			}
		}

		var namen = $('#text');
		namen.empty();

		$("<h1>Deze week</h1>").appendTo(namen);
		for (var i = 0; i < days.length; i++) {
			if (days[i].acts.length == 0 && slaLegeDagenOver) {
				continue;
			}

			$("<h2>" + dayNames[days[i].date.getDay()] + "</h2>").appendTo(namen);
			var list = $("<ul></ul>");
			for (var j = 0; j < days[i].acts.length; j++) {
				var liElem = $("<li></li>")
				liElem.html(days[i].acts[j].naam);
				liElem.appendTo(list);
			}
			list.appendTo(namen);
		}

		var slides = $(".slideshow");

		for (var i = 0; i < data.length; i++) {
			if (data[i].type == 'extra-poster' || (data[i].type == 'activiteit' && data[i].heeftPoster)) {
				var img = $("<img/>");
				img.attr('src', data[i].pad);
				img.attr('alt', data[i].beschrijving);
				$("<div></div>").append(img).appendTo(slides);
			}
		}

		initSlideshow();
	});
}

$(document).ready(function() {
	loadPosters();
	setInterval(loadPosters, 6 * 60 * 60 * 1000);
	// elke zes uur gaan we alles lekker opnieuw binnenhalen
});

