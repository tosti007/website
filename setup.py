#!/usr/bin/env python3

from setuptools import setup

setup(
    name='whoswhopy',
    version='0.1',
    description='WhosWho voor Python',
    author='WebCie A–Eskwadraat',
    author_email='webcie@a-eskwadraat.nl',
    packages=['whoswhopy'],
    install_requires=[], # voor benodigdheden zie requirements.txt
    scripts = [
        'docs/dia2py',
        'scripts/debugconfig.py',
    ],
)
