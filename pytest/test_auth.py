"""test_auth - Tests voor authorisatielogica."""

from whoswhopy.auth import AuthRecht, GodAuth, InstelbareAuth
import whoswhopy.config as config

def test_godrechten():
    """Met godrechten kun je echt alles."""
    auth = GodAuth()
    level, wachtwoord = auth.geef_db_auth()
    assert level == config.geef('DB_USER_PREFIX', '') + 'god'
    for recht in AuthRecht:
        assert auth.check_recht(recht)

def test_rechten_instellen():
    """Bij instelbare rechten kun je kiezen of je media mag bekijken."""
    geen_media_bekijken = InstelbareAuth('ingelogd', set())
    wel_media_bekijken = InstelbareAuth('ingelogd', {AuthRecht.media_bekijken})

    ingelogde_db_user = config.geef('DB_USER_PREFIX', '') + 'ingelogd'
    assert geen_media_bekijken.geef_db_auth()[0] == ingelogde_db_user
    assert wel_media_bekijken.geef_db_auth()[0] == ingelogde_db_user

    assert not geen_media_bekijken.check_recht(AuthRecht.media_bekijken)
    assert wel_media_bekijken.check_recht(AuthRecht.media_bekijken)
