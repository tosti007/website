"""test_wachtwoord - Tests voor het hashen en vergelijken van wachtwoorden."""

import contextlib
import hypothesis
import hypothesis.strategies as st

from whoswhopy.wachtwoord import genereer, verifieer

def test_verifieer_hash_php():
    """Controleer dat een hash door PHP gemaakt in Python verifieert."""
    tekst_hash = b'$2a$12$rgPGJdNNv/J7VOnyeroLx.JMOdj0UsLGV1MdAN7x3Kg/QlRHaS6oe'
    assert verifieer(b'mies', tekst_hash, salt=b'')

@hypothesis.given(st.binary(), st.binary())
@hypothesis.settings(deadline=None, max_examples=2) # Deze test kost veel tijd...
def test_verifieer_hash(echte_tekst, foute_tekst):
    """Controleer dat we een hash kunnen verifiëren met het origineel."""
    hypothesis.assume(echte_tekst != foute_tekst)
    hypothesis.assume(b'\x00' not in echte_tekst)
    hypothesis.assume(b'\x00' not in foute_tekst)

    tekst_hash = genereer(echte_tekst)
    assert verifieer(echte_tekst, tekst_hash)
    assert not verifieer(foute_tekst, tekst_hash)

@hypothesis.given(st.text(), st.text())
@hypothesis.example('A–Eskwadraat: 🤔')
@hypothesis.settings(deadline=None, max_examples=2) # Deze test kost veel tijd...
def test_verifieer_unicode_parameters(tekst, salt):
    """Controleer dat alles in Unicodestrings stoppen nog steeds goede verificatie geeft."""
    hypothesis.assume('\x00' not in tekst)
    hypothesis.assume('\x00' not in salt)

    tekst_hash = genereer(tekst, salt=salt).decode('ascii')
    assert verifieer(tekst, tekst_hash, salt=salt)
