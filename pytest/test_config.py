"""test_config - Tests voor config inlezen."""

import whoswhopy.config as config

def test_bestuursleden_inlezen():
    """Lees wat bestuursliddefinities in."""
    zonder_boeken = config._load_bestuurslid({'contactid': 111, 'functie': "Koekjescommissaris", 'email': "koek"}, version=1)
    assert zonder_boeken.boeken == False
    assert zonder_boeken.emailadres == "koek@a-eskwadraat.nl"
    assert zonder_boeken.fancy_email == "Koekjescommissaris A-Eskwadraat <koek@a-eskwadraat.nl>"

    met_wel_boeken = config._load_bestuurslid({'contactid': 111, 'functie': "Boekjescommissaris", 'email': "boek", 'boeken': True}, version=1)
    assert met_wel_boeken.boeken == True

    met_geen_boeken = config._load_bestuurslid({'contactid': 111, 'functie': "Hoekjescommissaris", 'email': "hoek", 'boeken': False}, version=1)
    assert met_geen_boeken.boeken == False

def test_webcieleden_inlezen():
    """Lees wat WebCieliddefinities in."""
    lodewijk = config._load_webcielid({'contactid': 111, 'login': "lida", 'debugsite': "boom", 'mountpoint': '/mnt/wwwdebug2'}, version=1)
