"""Revisie gegenereerd door dia2py

Revision ID: b46a09a05984
Revises: 173352efad12
Create Date: 2018-04-29 19:21:42.009556

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'b46a09a05984'
down_revision = '173352efad12'
branch_labels = None
depends_on = None


def upgrade():
    # De volgende kolommen zouden NULL mogen zijn volgens de dia...
    op.alter_column('ActiviteitInformatie', 'categorie_actCategorieID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    op.alter_column('ActiviteitInformatie', 'titel_NL',
               existing_type=mysql.VARCHAR(length=255),
               nullable=True,
               existing_server_default=sa.text("''"))
    op.alter_column('Bedrijfsprofiel', 'contractonderdeel_id',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    op.alter_column('IntroGroep', 'cluster_clusterID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    op.alter_column('Mededeling', 'commissie_commissieID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)
    op.alter_column('Mededeling', 'mededeling_NL',
               existing_type=mysql.TEXT(),
               nullable=True)
    op.alter_column('Mededeling', 'omschrijving_NL',
               existing_type=mysql.VARCHAR(length=255),
               nullable=True,
               existing_server_default=sa.text("''"))
    op.alter_column('Vacature', 'contractonderdeel_id',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True)

    # Om namen zinniger te krijgen, moeten we veldnamen aanpassen.

    # Stap 1 is oude constraints weggooien
    # (Om de een of andere reden bestaan die nog niet...)
    op.drop_constraint('DeelnemerAntwoord_Vraag', 'DeelnemerAntwoord', type_='foreignkey')
    op.drop_constraint('DeelnemerAntwoord_Deelnemer', 'DeelnemerAntwoord', type_='foreignkey')
    op.drop_constraint('Interactie_Spook', 'Interactie', type_='foreignkey')
    op.drop_constraint('Interactie_BedrijfPersoon', 'Interactie', type_='foreignkey')
    op.drop_constraint('KartVoorwerp_PersoonKart', 'KartVoorwerp', type_='foreignkey')
    op.drop_constraint('iDealAntwoord_Vraag', 'iDealAntwoord', type_='foreignkey')

    # Stap 2 is kolommen hernoemen
    op.alter_column('DeelnemerAntwoord', 'vraag_activiteitID', new_column_name='vraag_activiteit_activiteitID',
            existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('DeelnemerAntwoord', 'deelnemer_activiteitID', new_column_name='deelnemer_activiteit_activiteitID',
            existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('DeelnemerAntwoord', 'deelnemer_contactID', new_column_name= 'deelnemer_persoon_contactID',
            existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'spook_contactID', new_column_name='spook_persoon_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'spook_contactID2', new_column_name='spook_organisatie_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'bedrijfPersoon_contactID', new_column_name='bedrijfPersoon_persoon_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'bedrijfPersoon_contactID2', new_column_name='bedrijfPersoon_organisatie_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('KartVoorwerp', 'persoonKart_contactID', new_column_name='persoonKart_persoon_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('iDealAntwoord', 'vraag_activiteitID', new_column_name='vraag_activiteit_activiteitID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )

    # Stap 3 is nieuwe constraints maken
    op.create_foreign_key('DeelnemerAntwoord_Deelnemer', 'DeelnemerAntwoord', 'Deelnemer', ['deelnemer_persoon_contactID', 'deelnemer_activiteit_activiteitID'], ['persoon_contactID', 'activiteit_activiteitID'])
    op.create_foreign_key('DeelnemerAntwoord_Vraag', 'DeelnemerAntwoord', 'ActiviteitVraag', ['vraag_activiteit_activiteitID', 'vraag_vraagID'], ['activiteit_activiteitID', 'vraagID'])
    op.create_foreign_key('Interactie_BedrijfPersoon', 'Interactie', 'ContactPersoon', ['bedrijfPersoon_persoon_contactID', 'bedrijfPersoon_organisatie_contactID', 'bedrijfPersoon_type'], ['persoon_contactID', 'organisatie_contactID', 'type'])
    op.create_foreign_key('Interactie_Spook', 'Interactie', 'ContactPersoon', ['spook_persoon_contactID', 'spook_organisatie_contactID', 'spook_type'], ['persoon_contactID', 'organisatie_contactID', 'type'])
    op.create_foreign_key('KartVoorwerp_PersoonKart', 'KartVoorwerp', 'PersoonKart', ['persoonKart_persoon_contactID', 'persoonKart_naam'], ['persoon_contactID', 'naam'])
    op.create_foreign_key('iDealAntwoord_Vraag', 'iDealAntwoord', 'ActiviteitVraag', ['vraag_activiteit_activiteitID', 'vraag_vraagID'], ['activiteit_activiteitID', 'vraagID'])


def downgrade():
    # Maak NULLbaarheid ongedaan
    op.alter_column('Vacature', 'contractonderdeel_id',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.alter_column('Mededeling', 'omschrijving_NL',
               existing_type=mysql.VARCHAR(length=255),
               nullable=False,
               existing_server_default=sa.text("''"))
    op.alter_column('Mededeling', 'mededeling_NL',
               existing_type=mysql.TEXT(),
               nullable=False)
    op.alter_column('Mededeling', 'commissie_commissieID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.alter_column('IntroGroep', 'cluster_clusterID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.alter_column('Bedrijfsprofiel', 'contractonderdeel_id',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)
    op.alter_column('ActiviteitInformatie', 'titel_NL',
               existing_type=mysql.VARCHAR(length=255),
               nullable=False,
               existing_server_default=sa.text("''"))
    op.alter_column('ActiviteitInformatie', 'categorie_actCategorieID',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False)

    # Stap -3: gooi nieuwe constraints weer weg
    op.drop_constraint('iDealAntwoord_Vraag', 'iDealAntwoord', type_='foreignkey')
    op.drop_constraint('KartVoorwerp_PersoonKart', 'KartVoorwerp', type_='foreignkey')
    op.drop_constraint('Interactie_Spook', 'Interactie', type_='foreignkey')
    op.drop_constraint('Interactie_BedrijfPersoon', 'Interactie', type_='foreignkey')
    op.drop_constraint('DeelnemerAntwoord_Vraag', 'DeelnemerAntwoord', type_='foreignkey')
    op.drop_constraint('DeelnemerAntwoord_Deelnemer', 'DeelnemerAntwoord', type_='foreignkey')

    # Stap -2: hernoem kolommen
    op.alter_column('iDealAntwoord', 'vraag_activiteit_activiteitID', new_column_name='vraag_activiteitID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('KartVoorwerp', 'persoonKart_persoon_contactID', new_column_name='persoonKart_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'spook_persoon_contactID', new_column_name='spook_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'spook_organisatie_contactID', new_column_name='spook_contactID2',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'bedrijfPersoon_persoon_contactID', new_column_name='bedrijfPersoon_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('Interactie', 'bedrijfPersoon_organisatie_contactID', new_column_name='bedrijfPersoon_contactID2',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('DeelnemerAntwoord', 'vraag_activiteit_activiteitID', new_column_name='vraag_activiteitID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('DeelnemerAntwoord', 'deelnemer_persoon_contactID', new_column_name='deelnemer_contactID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )
    op.alter_column('DeelnemerAntwoord', 'deelnemer_activiteit_activiteitID', new_column_name='deelnemer_activiteitID',
        existing_type=mysql.INTEGER(display_width=11), existing_nullable=False,
    )

    # Stap -1: maak oude constraints
    # om de een of andere reden bestonden deze niet...
    op.create_foreign_key('iDealAntwoord_Vraag', 'iDealAntwoord', 'ActiviteitVraag', ['vraag_activiteitID', 'vraag_vraagID'], ['activiteit_activiteitID', 'vraagID'])
    op.create_foreign_key('KartVoorwerp_PersoonKart', 'KartVoorwerp', 'PersoonKart', ['persoonKart_contactID', 'persoonKart_naam'], ['persoon_contactID', 'naam'])
    op.create_foreign_key('Interactie_BedrijfPersoon', 'Interactie', 'ContactPersoon', ['bedrijfPersoon_contactID', 'bedrijfPersoon_contactID2', 'bedrijfPersoon_type'], ['persoon_contactID', 'organisatie_contactID', 'type'])
    op.create_foreign_key('Interactie_Spook', 'Interactie', 'ContactPersoon', ['spook_contactID', 'spook_contactID2', 'spook_type'], ['persoon_contactID', 'organisatie_contactID', 'type'])
    op.create_foreign_key('DeelnemerAntwoord_Deelnemer', 'DeelnemerAntwoord', 'Deelnemer', ['deelnemer_contactID', 'deelnemer_activiteitID'], ['persoon_contactID', 'activiteit_activiteitID'])
    op.create_foreign_key('DeelnemerAntwoord_Vraag', 'DeelnemerAntwoord', 'ActiviteitVraag', ['vraag_activiteitID', 'vraag_vraagID'], ['activiteit_activiteitID', 'vraagID'])
