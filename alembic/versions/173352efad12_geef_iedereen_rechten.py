"""geef iedereen rechten

Revision ID: 173352efad12
Revises: 15023145d7b0
Create Date: 2018-04-25 01:13:40.622356

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '173352efad12'
down_revision = '15023145d7b0'
branch_labels = None
depends_on = None

# Alle gebruikers en hun rechten op tabellen.
db_users_tabellen = {
    'dibstablet': {
        'Artikel': ['Select'],
        "ColaProduct": ["Select"],
        "Contact": ["Select"],
        "DibsInfo": ["Select,Update"],
        "DibsTransactie": ["Insert"],
        "Lid": ["Select"],
        "Pas": ["Select"],
        "Persoon": ["Select"],
        "PersoonVoorkeur": ["Select"],
        "Register": ["Select,Update"],
        "Transactie": ["Insert"],
        "Verkoop": ["Select,Insert,Update"],
        "VerkoopPrijs": ["Select"],
        "Voorraad": ["Select,Insert,Update"],
        "VoorraadMutatie": ["Select,Insert,Update"],
    },
    'gast': {
        "Artikel": ["Insert"],
        "Bug": ["Select,Insert,Update"],
        "BugBericht": ["Select,Insert,Update"],
        "CSPReport": ["Insert"],
        "Contact": ["Insert"],
        "ContactAdres": ["Insert"],
        "ContactMailingList": ["Select,Insert"],
        "ContactTelnr": ["Insert"],
        "DibsInfo": ["Update"],
        "DibsTransactie": ["Select,Insert"],
        "Dictaat": ["Insert"],
        "Giro": ["Select,Insert,Update"],
        "IntroDeelnemer": ["Insert,Update"],
        "Jaargang": ["Insert"],
        "JaargangArtikel": ["Insert"],
        "Lid": ["Insert"],
        "LidStudie": ["Insert"],
        "Persoon": ["Insert"],
        "RatingObject": ["Select,Insert,Update"],
        "Register": ["Update"],
        "Studie": ["Insert"],
        "TentamenUitwerking": ["Select,Insert,Update"],
        "TinyUrlHit": ["Insert"],
        "Transactie": ["Select,Insert,Update"],
        "Vacature": ["Update"],
        "Vak": ["Insert"],
        "Verkoop": ["Insert"],
        "VoorraadMutatie": ["Insert"],
        "Wachtwoord": ["Insert","Update"],
        "iDeal": ["Select,Insert,Update"],
        "iDealAntwoord": ["Select,Insert,Update"],
        "iDealKaartje": ["Select,Insert,Update"],
        "iDealKaartjeCode": ["Select,Insert,Update"],
    },
    'gitwebhook': {
        "Bug": ["Select,Insert,Update,References"],
        "BugBericht": ["Select,Insert,Update,References"],
    },
    'ingelogd': {
        "Activiteit": ["Delete"],
        "ActiviteitHerhaling": ["Delete"],
        "ActiviteitInformatie": ["Delete"],
        "ActiviteitVraag": ["Delete"],
        "Bedrijf": ["Delete"],
        "BedrijfStudie": ["Delete"],
        "Bedrijfsprofiel": ["Delete"],
        "BugToewijzing": ["Delete"],
        "BugVolger": ["Delete"],
        "Collectie": ["Delete"],
        "Commissie": ["Select"],
        "CommissieActiviteit": ["Delete"],
        "Contact": ["Delete"],
        "ContactAdres": ["Delete"],
        "ContactMailingList": ["Delete"],
        "ContactPersoon": ["Delete"],
        "ContactTelnr": ["Delete"],
        "Contract": ["Delete"],
        "Contractonderdeel": ["Delete"],
        "Deelnemer": ["Delete"],
        "DeelnemerAntwoord": ["Delete"],
        "IOUBetaling": ["Delete"],
        "IOUBon": ["Delete"],
        "IOUSplit": ["Delete"],
        "Interactie": ["Delete"],
        "IntroCluster": ["Delete"],
        "IntroGroep": ["Delete"],
        "KartVoorwerp": ["Delete"],
        "LidStudie": ["Delete"],
        "Mailing_MailingList": ["Delete"],
        "Media": ["Delete"],
        "Mentor": ["Delete"],
        "Organisatie": ["Delete"],
        "PapierMolen": ["Select,Insert,Update,Delete"],
        "Persoon": ["Delete"],
        "PersoonKart": ["Delete"],
        "PersoonVoorkeur": ["Delete"],
        "Planner": ["Delete"],
        "PlannerData": ["Delete"],
        "PlannerDeelnemer": ["Delete"],
        "ProfielFoto": ["Select,Insert,Update,Delete"],
        "Studie": ["Delete"],
        "Tag": ["Delete"],
        "TagArea": ["Delete"],
        "Tentamen": ["Delete"],
        "TentamenUitwerking": ["Delete"],
        "Vacature": ["Delete"],
        "Voornaamwoord": ["Delete"],
    },
    'mollie': {
        "Activiteit": ["Select"],
        "ActiviteitHerhaling": ["Select"],
        "ActiviteitInformatie": ["Select"],
        "ActiviteitVraag": ["Select"],
        "Artikel": ["Select"],
        "Bug": ["Select"],
        "Commissie": ["Select"],
        "CommissieActiviteit": ["Select"],
        "CommissieCategorie": ["Select"],
        "CommissieLid": ["Select"],
        "Contact": ["Select,Update"],
        "ContactAdres": ["Select,Delete"],
        "ContactTelnr": ["Select,Insert,Update,Delete"],
        "Deelnemer": ["Select,Insert,Update"],
        "DeelnemerAntwoord": ["Select,Insert,Update"],
        "DibsInfo": ["Select,Insert,Update,References"],
        "DibsProduct": ["Select"],
        "DibsTransactie": ["Select,Insert,Update,References"],
        "Donateur": ["Select"],
        "Giro": ["Select,Insert,Update,References"],
        "IntroDeelnemer": ["Select,Update"],
        "KartVoorwerp": ["Select"],
        "Lid": ["Select,Update"],
        "LidStudie": ["Select,Delete"],
        "LidmaatschapsBetaling": ["Select"],
        "Media": ["Select"],
        "Mentor": ["Select"],
        "Persoon": ["Select,Update"],
        "PersoonBadge": ["Select"],
        "PersoonVoorkeur": ["Select"],
        "PlannerData": ["Select"],
        "Rating": ["Select"],
        "Register": ["Select,Insert,Update,References"],
        "Tag": ["Select"],
        "TentamenUitwerking": ["Select"],
        "Transactie": ["Select,Insert,Update,References"],
        "Verkoop": ["Select,Insert,Update,References"],
        "VerkoopPrijs": ["Select"],
        "Verplaatsing": ["Select,Insert,Update,References"],
        "Voorraad": ["Select,Update"],
        "VoorraadMutatie": ["Select,Insert,Update,References"],
        "Wachtwoord": ["Select"],
        "iDeal": ["Select,Insert,Update,References"],
        "iDealAntwoord": ["Select,Insert,Update"],
        "iDealKaartje": ["Select,Insert,Update,References"],
        "iDealKaartjeCode": ["Select,Insert,Update,References"],
    }
}

# Alle users en hun rechten op elke tabel tegelijk.
db_users_overal = {
    'gitwebhook': ['select', 'insert', 'update', 'delete'],
    'god': ['select', 'insert', 'update', 'delete'],
    'ingelogd': ['select', 'insert', 'update'],
    'gast': ['select'],
}

def upgrade():
    for user, tables in db_users_tabellen.items():
        for table, rights in tables.items():
            op.execute('GRANT {} ON {} TO \'{}\'@\'localhost\';'.format(','.join(rights), table, user))
    for user, rights in db_users_overal.items():
        op.execute('GRANT {} ON * TO \'{}\'@\'localhost\';'.format(','.join(rights), user))

def downgrade():
    for user, tables in db_users_tabellen.items():
        for table, rights in tables.items():
            op.execute('REVOKE {} ON {} FROM \'{}\'@\'localhost\';'.format(','.join(rights), table, user))
    for user, rights in db_users_overal.items():
        op.execute('REVOKE {} ON * FROM \'{}\'@\'localhost\';'.format(','.join(rights), user))
