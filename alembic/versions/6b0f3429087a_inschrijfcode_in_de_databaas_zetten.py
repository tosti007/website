"""Inschrijfcode in de databaas zetten

Revision ID: 6b0f3429087a
Revises: a4b18e8ea296
Create Date: 2018-06-19 21:06:58.262319

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '6b0f3429087a'
down_revision = 'a4b18e8ea296'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('IntroDeelnemer', sa.Column('magischeCode', mysql.VARCHAR(length=196), nullable=False))
    op.create_index('magischeCode', 'IntroDeelnemer', ['magischeCode'], unique=True)

def downgrade():
    op.drop_index('magischeCode', table_name='IntroDeelnemer')
    op.drop_column('IntroDeelnemer', 'magischeCode')
